﻿using CFDI33;
using ModeloSincronizacion;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Serialization;

namespace GeneracionCxP_SAP.Clases
{
    class cPayableLine
    {
        public string LINE_NO { get; set; }
        public string REFERENCE { get; set; }
        public decimal QTY { get; set; }
        public decimal AMOUNT { get; set; }
        public decimal VAT_PERCENT { get; set; }
        public decimal VAT_AMOUNT { get; set; }
        public string VAT_GL_ACCT_ID { get; set; }
        public string GL_ACCOUNT_ID { get; set; }
        public int FIXED_CHARGE { get; internal set; }
        public string DISC_ALLOWED { get; internal set; }
        public string COST_CATEGORY { get; internal set; }
        public int DUTY_PERCENT { get; internal set; }
        public int DUTY_AMOUNT { get; internal set; }
        public string DUTY_GL_ACCT_ID { get; internal set; }
        public string RECEIVER_ID { get; internal set; }
    }
    class cPayable
    {
        string SITE = "";
        public string VOUCHER_ID { get; set; }
        public string CURRENCY_ID { get; set; }
        public string INVOICE_ID { get; set; }
        public DateTime INVOICE_DATE { get; set; }
        public decimal TOTAL_AMOUNT { get; private set; }
        public List<cPayableLine> lineas { get; set; }
        public string UUID { get; private set; }
        public string FechaTimbrado { get; private set; }
        public string OC { get; private set; }

        private string noCertificadoSAT;
        private string selloSAT;

        public cPayable()
        {

        }

        public CFDI33.Comprobante oComprobante;
        
        public bool ProcesarCFDI(string filename, bool detallado)
        {
            //Pasear el comprobante
            try
            {
                XmlSerializer serializer_CFD_3 = new XmlSerializer(typeof(CFDI33.Comprobante));
                TextReader reader = new StreamReader(filename);
                oComprobante = (CFDI33.Comprobante)serializer_CFD_3.Deserialize(reader);
                reader.Dispose();
                reader.Close();
                if (true)
                {
                    INVOICE_ID = string.Empty;
                    if (oComprobante.Serie != null)
                    {
                        INVOICE_ID += oComprobante.Serie;
                    }
                    if (oComprobante.Folio != null)
                    {
                        INVOICE_ID += oComprobante.Folio;
                    }

                    INVOICE_DATE = oComprobante.Fecha;
                    TOTAL_AMOUNT = oComprobante.Total;

                    if (detallado)
                    {
                        CargarConceptos(oComprobante);
                    }
                    else
                    {
                        CargarLineas(oComprobante);
                    }
                    //Cargar dato fiscals
                    XmlDocument doc = new XmlDocument();
                    doc.Load(filename);
                    XmlNodeList TimbreFiscalDigital = doc.GetElementsByTagName("tfd:TimbreFiscalDigital");
                    if (TimbreFiscalDigital != null)
                    {
                        UUID = TimbreFiscalDigital[0].Attributes["UUID"].Value;
                        FechaTimbrado = TimbreFiscalDigital[0].Attributes["FechaTimbrado"].Value;
                        noCertificadoSAT = TimbreFiscalDigital[0].Attributes["NoCertificadoSAT"].Value;
                        selloSAT = TimbreFiscalDigital[0].Attributes["SelloSAT"].Value;
                    }

                    if (!String.IsNullOrEmpty(UUID))
                    {
                        //Obtener la OC
                        OC = ObtenerOC(UUID);
                    }

                    return true;
                }
                return false;
            }
            catch (Exception e)
            {
                throw e;
            }

        }

        private string ObtenerOC(string uuid)
        {
            ModeloSincronizacionContainer Db = new ModeloSincronizacionContainer();
            archivoSync Archivo = Db.archivoSyncSet.Where(a => a.UUID.Equals(uuid)).FirstOrDefault();
            if (Archivo!=null)
            {
                return Archivo.oc;
            }
            return string.Empty;
        }

        private void Constantes(cPayableLine OcPayableLine)
        {
            OcPayableLine.FIXED_CHARGE = 0;
            OcPayableLine.DISC_ALLOWED = "Y";
            OcPayableLine.COST_CATEGORY = "M";
            OcPayableLine.DUTY_PERCENT = 0;
            OcPayableLine.DUTY_AMOUNT = 0;
            OcPayableLine.DUTY_GL_ACCT_ID = "M";
        }

        private void CargarImpuesto(cPayableLine OcPayableLine, ComprobanteImpuestos impuestos)
        {
            if (impuestos != null)
            {
                if (impuestos.TotalImpuestosTrasladados != 0)
                {
                    foreach (ComprobanteImpuestosTraslado impuesto in impuestos.Traslados)
                    {
                        OcPayableLine.VAT_PERCENT = Math.Round(impuesto.TasaOCuota * 100, 3);
                        OcPayableLine.VAT_AMOUNT = impuesto.Importe;

                        if (impuesto.TasaOCuota == (decimal)0.16)
                        {
                            //Agregar regla                            
                            OcPayableLine.VAT_GL_ACCT_ID = "10400002";
                        }
                    }
                }
            }
        }

        private void CargarLineas(Comprobante oComprobante)
        {

            lineas = new List<cPayableLine>();
            //Agregar una linea
            cPayableLine OcPayableLine = new cPayableLine();
            Constantes(OcPayableLine);
            OcPayableLine.QTY = 1;
            OcPayableLine.REFERENCE = "CFDI";
            OcPayableLine.AMOUNT = oComprobante.SubTotal;
            OcPayableLine.GL_ACCOUNT_ID = "54000007";

            //Cargar el ültimo recibo que tiene el proveedor
            //OcPayableLine.RECEIVER_ID = CargarUltimoRecibo(OVENDOR.ID, oComprobante.SubTotal);

            CargarImpuesto(OcPayableLine, oComprobante.Impuestos);
            lineas.Add(OcPayableLine);
            //Validar las retenciones
            if (oComprobante.Impuestos != null)
            {
                if (oComprobante.Impuestos.TotalImpuestosRetenidos != 0)
                {
                    AgregarRetencionGeneral(oComprobante.Impuestos.TotalImpuestosRetenidos);
                }
            }
        }

        private object CargarUltimoRecibo(string iD, decimal subTotal)
        {
            throw new NotImplementedException();
        }

        private void AgregarRetencionGeneral(decimal retencion)
        {
            cPayableLine OcPayableLine = new cPayableLine();
            Constantes(OcPayableLine);
            OcPayableLine.QTY = 1;
            OcPayableLine.REFERENCE = "Retencion";
            OcPayableLine.AMOUNT = Math.Abs(retencion) * -1;
            OcPayableLine.GL_ACCOUNT_ID = "20103007";
            OcPayableLine.VAT_PERCENT = 0;
            OcPayableLine.VAT_AMOUNT = 0;
            OcPayableLine.VAT_GL_ACCT_ID = "10400006";
            lineas.Add(OcPayableLine);
        }

        private void CargarConceptos(Comprobante oComprobante)
        {
            lineas = new List<cPayableLine>();
            foreach (CFDI33.ComprobanteConcepto Concepto in oComprobante.Conceptos)
            {
                cPayableLine OcPayableLine = new cPayableLine();
                OcPayableLine.QTY = Concepto.Cantidad;
                OcPayableLine.REFERENCE = Concepto.Descripcion;
                OcPayableLine.AMOUNT = Concepto.Importe;
                OcPayableLine.VAT_PERCENT = 0;
                OcPayableLine.VAT_AMOUNT = 0;
                OcPayableLine.VAT_GL_ACCT_ID = "10400014";
                OcPayableLine.FIXED_CHARGE = 0;
                OcPayableLine.DISC_ALLOWED = "Y";
                OcPayableLine.COST_CATEGORY = "M";
                OcPayableLine.DUTY_PERCENT = 0;
                OcPayableLine.DUTY_AMOUNT = 0;
                OcPayableLine.DUTY_GL_ACCT_ID = "M";
                OcPayableLine.GL_ACCOUNT_ID = "";
                //Validar las reglas de las cuenta contable basado por proveedor, Clave 
                //Validar los Impuestos
                cPayableLine OcPayableLineRetencion = new cPayableLine();

                if (Concepto.Impuestos != null)
                {
                    if (Concepto.Impuestos.Traslados != null)
                    {
                        foreach (CFDI33.ComprobanteConceptoImpuestosTraslado impuesto in Concepto.Impuestos.Traslados)
                        {
                            OcPayableLine.VAT_PERCENT = impuesto.TasaOCuota;
                            OcPayableLine.VAT_AMOUNT = impuesto.Importe;
                            OcPayableLine.VAT_GL_ACCT_ID = "10400014";
                        }
                    }

                    if (Concepto.Impuestos.Retenciones != null)
                    {
                        foreach (CFDI33.ComprobanteConceptoImpuestosRetencion impuesto in Concepto.Impuestos.Retenciones)
                        {
                            OcPayableLineRetencion = new cPayableLine();
                            OcPayableLineRetencion.QTY = 1;
                            OcPayableLineRetencion.AMOUNT = impuesto.Importe;
                            OcPayableLineRetencion.REFERENCE = "Tasa " + impuesto.TasaOCuota.ToString();

                            //Cargar la regla de los impuestos de retencion 
                            OcPayableLineRetencion.GL_ACCOUNT_ID = "10400014";
                        }
                    }
                }

                lineas.Add(OcPayableLine);

                if (OcPayableLineRetencion.AMOUNT != 0)
                {
                    lineas.Add(OcPayableLineRetencion);
                }
            }

        }

    }

}
