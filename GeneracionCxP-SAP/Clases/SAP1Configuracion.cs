﻿using SAPbobsCOM;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CxP.Clases
{
    internal sealed class SAP1Configuracion
    {
        public static Company OCompany { get; set; }
        public static string Server { get; set; }
        public static string CompanyDB { get; set; }
        public static bool UseTrusted { get; set; }
        public static string UserName { get; set; }
        public static string Password { get; set; }

        public static string Error { get; set; }
        public static bool Obtener()
        {
            try
            {
                Server = ConfigurationManager.AppSettings["SAPServer"].ToString();
                UseTrusted = false;
                CompanyDB = ConfigurationManager.AppSettings["SAPCompanyDB"].ToString();
                UserName = ConfigurationManager.AppSettings["SAPUserName"].ToString();
                Password = ConfigurationManager.AppSettings["SAPPassword"].ToString();
                OCompany = new Company();
                OCompany.DbServerType = BoDataServerTypes.dst_MSSQL2012;//BoDataServerTypes.dst_MSSQL2016;
                OCompany.Server= Server;
                OCompany.CompanyDB = CompanyDB;
                OCompany.UserName = UserName;
                OCompany.Password = Password;
                OCompany.language = BoSuppLangs.ln_Spanish_La;

                if (OCompany.Connect()==0)
                {
                    return true;
                }
                Error = OCompany.GetLastErrorCode().ToString() 
                    + " " +  OCompany.GetLastErrorDescription();
                return false;
            }
            catch (Exception error)
            {
                string mensaje = "SAP1Configuracion - 33 - obtener ";
                mensaje += error.Message.ToString();
                if (error.InnerException != null)
                {
                    mensaje += " " + error.InnerException.Message.ToString();
                }
                throw new Exception(mensaje);
            }
            return false;
        }

        internal static bool Desconectar()
        {
            try
            {
                if (OCompany != null)
                {
                    OCompany.Disconnect();
                }                
                return true;
            }
            catch (Exception error)
            {
                string mensaje = "SAP1Configuracion - 40 - desconectar ";
                mensaje += error.Message.ToString();
                if (error.InnerException != null)
                {
                    mensaje += " " + error.InnerException.Message.ToString();
                }
                throw new Exception(mensaje);
            }
        }
    }
}
