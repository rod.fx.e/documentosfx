﻿using CxP.Clases;
using CxP.Formularios;
using GeneracionCxP_SAP.Clases;
using SAPbobsCOM;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GeneracionCxP_SAP.Formularios
{
    public partial class GenerarCxP : Form
    {
        public GenerarCxP()
        {
            InitializeComponent();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            CargarCFDI();
            ValidarSAPB1();
            CargarBussinessPartner();
        }
        #region 

        private void CargarCFDI()
        {
            try
            {
                OpenFileDialog ofd = new OpenFileDialog();
                ofd.Filter = "XML Files (*.xml)|*.xml";
                ofd.FilterIndex = 0;
                ofd.DefaultExt = "xml";
                if (ofd.ShowDialog() == DialogResult.OK)
                {
                    if (!String.Equals(Path.GetExtension(ofd.FileName),
                                       ".xml",
                                       StringComparison.OrdinalIgnoreCase))
                    {
                        // Invalid file type selected; display an error.
                        MessageBox.Show("El archivo no es un CFDI.",
                                        "Invalid File Type",
                                        MessageBoxButtons.OK,
                                        MessageBoxIcon.Error);
                        return;
                    }
                    else
                    {
                        cPayable OcPAYABLE = new cPayable();
                        Limpiar();
                        if (OcPAYABLE.ProcesarCFDI(ofd.FileName, Detallado.Checked))
                        {
                            this.CFDIPath.Text = ofd.FileName;
                            INVOICE_ID.Text = OcPAYABLE.INVOICE_ID;
                            INVOICE_DATE.Value = OcPAYABLE.INVOICE_DATE;
                            VENDOR_ID.Text = OcPAYABLE.oComprobante.Emisor.Rfc;
                            NAME.Text = OcPAYABLE.oComprobante.Emisor.Nombre;
                            Total.Text = OcPAYABLE.TOTAL_AMOUNT.ToString("C4");
                            Subtotal.Text = OcPAYABLE.oComprobante.SubTotal.ToString("C4");
                            DocCurrency.Text = OcPAYABLE.oComprobante.Moneda;
                            Retencion.Text = (0).ToString("C4");
                            Impuesto.Text = (0).ToString("C4");
                            if (OcPAYABLE.oComprobante.Impuestos != null)
                            {
                                Impuesto.Text = (OcPAYABLE.oComprobante.Impuestos.TotalImpuestosTrasladados).ToString("C4");
                                Retencion.Text = (OcPAYABLE.oComprobante.Impuestos.TotalImpuestosRetenidos).ToString("C4");
                            }
                            CURRENCY_ID.Text = OcPAYABLE.CURRENCY_ID;
                            UUID.Text = OcPAYABLE.UUID;
                            OCtxt.Text = OcPAYABLE.OC;
                            //Cargar el DocEntry de la OC
                            DocEntryOC.Text = ObtenerDocEntryPO(OCtxt.Text);
                            ReciboTxt.Text = ObtenerRecibo(UUID.Text);

                            //Cargar las Lineas
                            if (OcPAYABLE.lineas != null)
                            {
                                foreach (cPayableLine linea in OcPAYABLE.lineas)
                                {
                                    int n = Dtg.Rows.Add();
                                    Dtg.Rows[n].Tag = linea;
                                    Dtg.Rows[n].Cells["QTY"].Value = linea.QTY.ToString();
                                    Dtg.Rows[n].Cells["DESCRIPTION"].Value = linea.REFERENCE.ToString();
                                    Dtg.Rows[n].Cells["Importe"].Value = linea.AMOUNT.ToString();
                                    Dtg.Rows[n].Cells["GL_ACCOUNT_ID"].Value = linea.GL_ACCOUNT_ID;
                                    Dtg.Rows[n].Cells["VAT_GL_ACCT_ID"].Value = linea.VAT_GL_ACCT_ID;
                                    Dtg.Rows[n].Cells["VAT_AMOUNT"].Value = linea.VAT_AMOUNT.ToString();
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception error)
            {
                string mensaje = "";
                mensaje += error.Message.ToString();
                if (error.InnerException != null)
                {
                    mensaje += " " + error.InnerException.Message.ToString();
                }
                MessageBox.Show(this, mensaje, Application.ProductName
                            , MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private string ObtenerRecibo(string uuid)
        {
            try
            {
                ModeloSincronizacion.ModeloSincronizacionContainer Db = new ModeloSincronizacion.ModeloSincronizacionContainer();


                ModeloSincronizacion.archivoSync Registro = Db.archivoSyncSet.Where(a => a.UUID.ToString().Equals(uuid)).FirstOrDefault();
                if (Registro != null)
                {
                    return Registro.recibo;
                }
                return string.Empty;

            }
            catch (Exception Error)
            {
                MessageBox.Show(this, Error.Message.ToString()
                    ,
                    Application.ProductVersion + " " + Application.ProductName
                    , MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            return string.Empty;
        }
        #endregion

        private void button4_Click(object sender, EventArgs e)
        {
            Generar();
        }

        private void button5_Click(object sender, EventArgs e)
        {
            Limpiar();
        }

        private void Limpiar()
        {
            Dtg.Rows.Clear();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            AbrirXML();
        }

        private void AbrirXML()
        {
            if (File.Exists(CFDIPath.Text))
            {
                string path = System.IO.Path.Combine(
                     System.IO.Directory.GetCurrentDirectory(),
                     CFDIPath.Text);
                System.Diagnostics.Process.Start("iexplore.exe", path);
            }
            
        }

        private void button6_Click(object sender, EventArgs e)
        {
            frmReglas OfrmReglas = new frmReglas();
            OfrmReglas.Show();
        }

        private void Dtg_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            //
            if( (e.ColumnIndex==3) | (e.ColumnIndex == 5))
            {

            }

            if (e.ColumnIndex == 6)
            {

            }
        }

        private void button3_Click(object sender, EventArgs e)
        {

        }

        private void button7_Click(object sender, EventArgs e)
        {
            ValidarSAPB1();
        }

        private void ValidarSAPB1()
        {
            if (!SAP1Configuracion.Obtener())
            {
                MessageBox.Show(this, SAP1Configuracion.Error,
                    Application.ProductVersion + " " + Application.ProductName
                    , MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else
            {
                //MessageBox.Show(this, "Conectado",
                //    Application.ProductVersion + " " + Application.ProductName
                //    , MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private void CargarBussinessPartner()
        {
            try
            {
                SAPbobsCOM.BusinessPartners oBP = null;
                oBP = SAP1Configuracion.OCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oBusinessPartners);
                SAPbobsCOM.Recordset oRecordSet = SAP1Configuracion.OCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);
                BusinessPartners oBusinessPartner = SAP1Configuracion.OCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oBusinessPartners);
                oRecordSet.DoQuery("SELECT CardCode FROM OCRD WHERE LicTradNum='" + VENDOR_ID.Text + "' AND CardType='S'"); //Traer todos los datos de la tabla de SN                
                oRecordSet.MoveFirst();
                while (!oRecordSet.EoF)
                {
                    CardCode.Text = oRecordSet.Fields.Item(0).Value;
                    break;
                }
            }
            catch (Exception Error)
            {
                int lErrCode = 0;
                string sErrMsg = "";

                SAP1Configuracion.OCompany.GetLastError(out lErrCode, out sErrMsg);

                MessageBox.Show(this, Error.Message.ToString()
                    + Environment.NewLine + lErrCode.ToString()
                    + Environment.NewLine + sErrMsg.ToString()
                    ,
                    Application.ProductVersion + " " + Application.ProductName
                    , MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        //https://codewithedd.com/di-api-sap-business-one/crear-un-documento-en-sapb1-con-di-api/

        private void button8_Click(object sender, EventArgs e)
        {
            Generar();

        }

        private void Generar()
        {
            if (String.IsNullOrEmpty(DocEntryOC.Text))
            {
                GenerarSinOC();
            }
            else
            {
                if (String.IsNullOrEmpty(this.ReciboTxt.Text))
                {
                    GenerarConOC();
                }
                else
                {
                    GenerarConRecibo();
                }
            }
        }
        private void GenerarConRecibo()
        {
            Informacion.Text = string.Empty;

            Informacion.Text += "Realizar AP Invoice - Recibo " + ReciboTxt.Text + " " + Environment.NewLine;

            int lErrCode = 0;
            string sErrMsg = "";
            try
            {
                SAP1Configuracion.Obtener();

                SAPbobsCOM.Documents PO = (SAPbobsCOM.Documents)SAP1Configuracion.OCompany
                .GetBusinessObject(SAPbobsCOM.BoObjectTypes.oPurchaseDeliveryNotes);

                int DocEntryPO = int.Parse(ReciboTxt.Text);// ObtenerDocEntryPO(OCtxt.Text);

                if (PO.GetByKey(DocEntryPO))
                {
                    Informacion.Text += "Generar  Recibo " + PO.DocEntry.ToString()
                            + " " + Environment.NewLine;

                    SAPbobsCOM.Documents oDocto = (SAPbobsCOM.Documents)SAP1Configuracion.OCompany
                        .GetBusinessObject(SAPbobsCOM.BoObjectTypes.oPurchaseInvoices);
                    oDocto.DocType = SAPbobsCOM.BoDocumentTypes.dDocument_Items;
                    oDocto.CardCode = PO.CardCode;
                    oDocto.DocDate = DateTime.Now.Date;
                    oDocto.DocCurrency = DocCurrency.Text;
                    oDocto.NumAtCard = INVOICE_ID.Text;
                    oDocto.Confirmed = SAPbobsCOM.BoYesNoEnum.tYES;
                    oDocto.UserFields.Fields.Item("LicTradNum").Value = VENDOR_ID.Text;
                    oDocto.UserFields.Fields.Item("EDocNum").Value = UUID.Text;
                    oDocto.Comments = "DocumentosFX Recibo";

                    for (int i = 0; i <= PO.Lines.Count - 1; i++)
                    {
                        PO.Lines.SetCurrentLine(i);
                        //if (PO.Lines.LineStatus!= BoStatus.bost_Close)
                        //{
                        Informacion.Text += "Recibo" + PO.Lines.ItemCode

                            + " " + Environment.NewLine;
                        if (!String.IsNullOrEmpty(PO.Lines.ItemCode))
                        {
                            Informacion.Text += "Linea  Recibo " + PO.Lines.ItemCode
                            + " " + Environment.NewLine;

                            oDocto.Lines.SetCurrentLine(i);
                            oDocto.Lines.ItemCode = PO.Lines.ItemCode;
                            oDocto.Lines.Quantity = PO.Lines.Quantity;
                            oDocto.Lines.WarehouseCode = PO.Lines.WarehouseCode;

                            oDocto.Lines.BaseType = 20;
                            oDocto.Lines.BaseEntry = PO.DocEntry;
                            oDocto.Lines.BaseLine = PO.Lines.LineNum;

                            Informacion.Text += "Linea  Recibo " + PO.DocEntry
                            + " Linea: " + PO.Lines.LineNum.ToString()
                            + " " + Environment.NewLine;
                            oDocto.Lines.Add();
                        }
                        //}
                    }

                    int lRetCode = oDocto.Add();
                    if (lRetCode != 0)
                    {
                        oDocto.Close();
                        SAP1Configuracion.OCompany.Disconnect();
                        System.Runtime.InteropServices.Marshal.ReleaseComObject(oDocto);
                        SAP1Configuracion.OCompany.GetLastError(out lErrCode, out sErrMsg);
                        MessageBox.Show(this, Environment.NewLine + lErrCode.ToString()
                            + Environment.NewLine + sErrMsg.ToString()
                            ,
                            Application.ProductVersion + " " + Application.ProductName
                            , MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                    else
                    {
                        oDocto.Close();
                        SAP1Configuracion.OCompany.Disconnect();
                        System.Runtime.InteropServices.Marshal.ReleaseComObject(oDocto);
                        string DocEntry = SAP1Configuracion.OCompany.GetNewObjectKey();
                        VOUCHER_ID.Text = ObtenerDocNum(DocEntry);
                        MessageBox.Show(this, DocEntry
                            ,
                            Application.ProductVersion + " " + Application.ProductName
                            , MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                    SAP1Configuracion.Desconectar();
                }
                else
                {
                    SAP1Configuracion.OCompany.GetLastError(out lErrCode, out sErrMsg);

                    MessageBox.Show(this, Environment.NewLine + lErrCode.ToString()
                        + Environment.NewLine + sErrMsg.ToString()
                        ,
                        Application.ProductVersion + " " + Application.ProductName
                        , MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
            catch (Exception Error)
            {
                SAP1Configuracion.OCompany.GetLastError(out lErrCode, out sErrMsg);
                MessageBox.Show(this, Error.Message.ToString()
                    + Environment.NewLine + lErrCode.ToString()
                    + Environment.NewLine + sErrMsg.ToString()
                    ,
                    Application.ProductVersion + " " + Application.ProductName
                    , MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void GenerarConOC()
        {
            Informacion.Text = string.Empty;

            if (!VerificarOC())
            {
                MessageBox.Show(this, "La Orden de Compra esta cerrada o no tiene ninguna de las líneas para recibir"
                    ,
                    Application.ProductVersion + " " + Application.ProductName
                    , MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            Informacion.Text += "Realizar AP Invoice - Desde OC oPurchaseInvoices "+ Environment.NewLine;

            int lErrCode = 0;
            string sErrMsg = "";
            try
            {
                SAP1Configuracion.Obtener();

                SAPbobsCOM.Documents PO = (SAPbobsCOM.Documents)SAP1Configuracion.OCompany
                .GetBusinessObject(SAPbobsCOM.BoObjectTypes.oPurchaseOrders);

                int DocEntryPO = int.Parse(DocEntryOC.Text);// ObtenerDocEntryPO(OCtxt.Text);
                
                if (PO.GetByKey(DocEntryPO))
                {
                    Informacion.Text += "Generar  oPurchaseInvoices " + PO.DocEntry.ToString()
                            + " " + Environment.NewLine;

                    SAPbobsCOM.Documents oDocto = (SAPbobsCOM.Documents)SAP1Configuracion.OCompany
                        .GetBusinessObject(SAPbobsCOM.BoObjectTypes.oPurchaseInvoices);
                    oDocto.DocType = SAPbobsCOM.BoDocumentTypes.dDocument_Items;
                    oDocto.CardCode = PO.CardCode;
                    oDocto.DocDate = DateTime.Now.Date;
                    oDocto.DocCurrency = DocCurrency.Text;
                    oDocto.NumAtCard = INVOICE_ID.Text;
                    oDocto.Confirmed = SAPbobsCOM.BoYesNoEnum.tYES;
                    oDocto.UserFields.Fields.Item("LicTradNum").Value = VENDOR_ID.Text;
                    oDocto.UserFields.Fields.Item("EDocNum").Value = UUID.Text;
                    oDocto.Comments = "DocumentosFX";

                    for (int i=0;i<=PO.Lines.Count - 1;i++)
                    {
                        PO.Lines.SetCurrentLine(i);
                        //if (PO.Lines.LineStatus!= BoStatus.bost_Close)
                        //{
                            Informacion.Text += "Linea  Abierta " + PO.Lines.ItemCode

                                + " " + Environment.NewLine;
                            if (!String.IsNullOrEmpty(PO.Lines.ItemCode))
                            {
                                Informacion.Text += "Linea  oPurchaseInvoices " + PO.Lines.ItemCode
                                + " " + Environment.NewLine;

                                oDocto.Lines.SetCurrentLine(i);
                                oDocto.Lines.ItemCode = PO.Lines.ItemCode;
                                oDocto.Lines.Quantity = PO.Lines.Quantity;
                                oDocto.Lines.WarehouseCode = PO.Lines.WarehouseCode;

                                oDocto.Lines.BaseType = 22;
                                oDocto.Lines.BaseEntry = PO.DocEntry;
                                oDocto.Lines.BaseLine = PO.Lines.LineNum;

                                Informacion.Text += "Linea  oPurchaseInvoices " + PO.DocEntry
                                + " Linea: " + PO.Lines.LineNum.ToString()
                                + " " + Environment.NewLine;
                                oDocto.Lines.Add();
                            }
                        //}
                    }

                    int lRetCode = oDocto.Add();
                    if (lRetCode != 0)
                    {
                        oDocto.Close();
                        SAP1Configuracion.OCompany.Disconnect();
                        System.Runtime.InteropServices.Marshal.ReleaseComObject(oDocto);
                        SAP1Configuracion.OCompany.GetLastError(out lErrCode, out sErrMsg);
                        MessageBox.Show(this, Environment.NewLine + lErrCode.ToString()
                            + Environment.NewLine + sErrMsg.ToString()
                            ,
                            Application.ProductVersion + " " + Application.ProductName
                            , MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                    else
                    {
                        oDocto.Close();
                        SAP1Configuracion.OCompany.Disconnect();
                        System.Runtime.InteropServices.Marshal.ReleaseComObject(oDocto);
                        string DocEntry = SAP1Configuracion.OCompany.GetNewObjectKey();
                        VOUCHER_ID.Text = ObtenerDocNum(DocEntry);
                        MessageBox.Show(this, DocEntry
                            ,
                            Application.ProductVersion + " " + Application.ProductName
                            , MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                    SAP1Configuracion.Desconectar();
                }
                else
                {
                    SAP1Configuracion.OCompany.GetLastError(out lErrCode, out sErrMsg);

                    MessageBox.Show(this, Environment.NewLine + lErrCode.ToString()
                        + Environment.NewLine + sErrMsg.ToString()
                        ,
                        Application.ProductVersion + " " + Application.ProductName
                        , MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
            catch (Exception Error)
            {
                SAP1Configuracion.OCompany.GetLastError(out lErrCode, out sErrMsg);
                MessageBox.Show(this, Error.Message.ToString()
                    + Environment.NewLine + lErrCode.ToString()
                    + Environment.NewLine + sErrMsg.ToString()
                    ,
                    Application.ProductVersion + " " + Application.ProductName
                    , MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private bool VerificarOC()
        {
            int lErrCode = 0;
            string sErrMsg = "";
            try
            {
                SAP1Configuracion.Obtener();

                SAPbobsCOM.Documents PO = (SAPbobsCOM.Documents)SAP1Configuracion.OCompany
                .GetBusinessObject(SAPbobsCOM.BoObjectTypes.oPurchaseOrders);
                int DocEntryPO = int.Parse(DocEntryOC.Text);// ObtenerDocEntryPO(OCtxt.Text);
                Informacion.Text+= "Verificando PO " + DocEntryPO.ToString("N0") + Environment.NewLine;

                if (PO.GetByKey(DocEntryPO))
                {
                    if(PO.DocumentStatus== BoStatus.bost_Close)
                    {
                        Informacion.Text += "Cerrado PO " + DocEntryPO.ToString("N0") 
                            + " Cerrada "+ Environment.NewLine;
                        return false;
                    }
                    Informacion.Text += "Abierta PO " + DocEntryPO.ToString("N0")
                            + " " + Environment.NewLine;

                    for (int i = 0; i <= PO.Lines.Count - 1; i++)
                    {
                        Informacion.Text += " PO " + DocEntryPO.ToString("N0") + " Lineas " 
                            + i.ToString("N0") + Environment.NewLine;

                        PO.Lines.SetCurrentLine(i);
                        if (PO.Lines.LineStatus != BoStatus.bost_Close)
                        {
                            Informacion.Text += " Abierto PO " + DocEntryPO.ToString("N0") + " Lineas " +
                            i.ToString("N0") + Environment.NewLine;
                            return true;
                        }
                    }
                    return false;
                }
                else
                {
                    SAP1Configuracion.OCompany.GetLastError(out lErrCode, out sErrMsg);

                    MessageBox.Show(this, Environment.NewLine + lErrCode.ToString()
                        + Environment.NewLine + sErrMsg.ToString()
                        ,
                        Application.ProductVersion + " " + Application.ProductName
                        , MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                return false;
            }
            catch (Exception Error)
            {
                SAP1Configuracion.OCompany.GetLastError(out lErrCode, out sErrMsg);
                MessageBox.Show(this, Error.Message.ToString()
                    + Environment.NewLine + lErrCode.ToString()
                    + Environment.NewLine + sErrMsg.ToString()
                    ,
                    Application.ProductVersion + " " + Application.ProductName
                    , MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            return false;
        }

        private string ObtenerDocNum(string docEntry)
        {
            try{
                ModeloSAP92.SAPEntities OSAPEntities = new ModeloSAP92.SAPEntities();
                ModeloSAP92.OPCH OOOPCH = OSAPEntities.OPCH.Where(a => a.DocEntry.ToString().Equals(docEntry)).FirstOrDefault();
                if (OOOPCH!=null)
                {
                    return OOOPCH.DocNum.ToString();
                }
                return string.Empty;

            }
            catch (Exception Error)
            {
                MessageBox.Show(this, Error.Message.ToString()
                    ,
                    Application.ProductVersion + " " + Application.ProductName
                    , MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            return string.Empty;
        }

        private string ObtenerDocEntryPO(string docNum)
        {
            try
            {
                ModeloSAP92.SAPEntities OSAPEntities = new ModeloSAP92.SAPEntities();
                ModeloSAP92.OPOR Documento = OSAPEntities.OPOR.Where(a => a.DocNum.ToString().Equals(docNum)).FirstOrDefault();
                if (Documento != null)
                {
                    return Documento.DocEntry.ToString();
                }
                return string.Empty;

            }
            catch (Exception Error)
            {
                MessageBox.Show(this, Error.Message.ToString()
                    ,
                    Application.ProductVersion + " " + Application.ProductName
                    , MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            return string.Empty;
        }

        private void GenerarSinOC()
        {
            Informacion.Text += "Realizar AP Invoice - Sin Factrura " + Environment.NewLine;

            SAPbobsCOM.Documents oDocto;
            oDocto = (SAPbobsCOM.Documents)SAP1Configuracion.OCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oPurchaseInvoices);

            oDocto.DocType = SAPbobsCOM.BoDocumentTypes.dDocument_Items;
            oDocto.CardCode = CardCode.Text;
            oDocto.DocDate = DateTime.Now.Date;

            oDocto.DocCurrency = DocCurrency.Text;
            oDocto.NumAtCard = INVOICE_ID.Text;
            oDocto.Confirmed = SAPbobsCOM.BoYesNoEnum.tYES;
            oDocto.UserFields.Fields.Item("LicTradNum").Value = VENDOR_ID.Text;
            oDocto.UserFields.Fields.Item("EDocNum").Value = UUID.Text;

            int Line = 0;
            foreach (DataGridViewRow Row in Dtg.Rows)
            {
                oDocto.Lines.SetCurrentLine(Line);
                oDocto.Lines.ItemCode = "509999";
                oDocto.Lines.Quantity = ConvertirDouble(Row.Cells["QTY"].Value.ToString());
                oDocto.Lines.Price = ConvertirDouble(Row.Cells["Importe"].Value.ToString());
                oDocto.Lines.UnitPrice = oDocto.Lines.Price / oDocto.Lines.Quantity;
                oDocto.Lines.Currency = DocCurrency.Text;
                oDocto.Lines.Add();
            }

            int lRetCode = oDocto.Add();
            int lErrCode = 0;
            string sErrMsg = "";
            if (lRetCode != 0)
            {
                oDocto.Close();
                SAP1Configuracion.OCompany.Disconnect();
                System.Runtime.InteropServices.Marshal.ReleaseComObject(oDocto);
                SAP1Configuracion.OCompany.GetLastError(out lErrCode, out sErrMsg);
                MessageBox.Show(this, Environment.NewLine + lErrCode.ToString()
                    + Environment.NewLine + sErrMsg.ToString()
                    ,
                    Application.ProductVersion + " " + Application.ProductName
                    , MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else
            {
                oDocto.Close();
                SAP1Configuracion.OCompany.Disconnect();
                System.Runtime.InteropServices.Marshal.ReleaseComObject(oDocto);
                string DocEntry = SAP1Configuracion.OCompany.GetNewObjectKey();
                VOUCHER_ID.Text = DocEntry;//ObtenerDocNum(DocEntry);
                MessageBox.Show(this, DocEntry
                    ,
                    Application.ProductVersion + " " + Application.ProductName
                    , MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            SAP1Configuracion.Desconectar();
        }


        private double ConvertirDouble(string valor)
        {
            decimal x = decimal.Parse(valor);
            return (double)x;
        }

        private void button9_Click(object sender, EventArgs e)
        {
            CargarBussinessPartner();
        }

        private void button10_Click(object sender, EventArgs e)
        {
            SAP1Configuracion.Desconectar();
        }
    }
}
