﻿
namespace GeneracionCxP_SAP.Formularios
{
    partial class GenerarCxP
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(GenerarCxP));
            this.label6 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.Subtotal = new System.Windows.Forms.Label();
            this.Impuesto = new System.Windows.Forms.Label();
            this.Total = new System.Windows.Forms.Label();
            this.Dtg = new System.Windows.Forms.DataGridView();
            this.QTY = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.DESCRIPTION = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Importe = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.GL_ACCOUNT_ID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.VAT_AMOUNT = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.VAT_GL_ACCT_ID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Recibo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.oc = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Linea = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.buttonAdv4 = new Syncfusion.Windows.Forms.ButtonAdv();
            this.buttonAdv2 = new Syncfusion.Windows.Forms.ButtonAdv();
            this.POSTING_DATE = new System.Windows.Forms.DateTimePicker();
            this.INVOICE_DATE = new System.Windows.Forms.DateTimePicker();
            this.label1 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.button3 = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.NAME = new System.Windows.Forms.TextBox();
            this.INVOICE_ID = new System.Windows.Forms.TextBox();
            this.VENDOR_ID = new System.Windows.Forms.TextBox();
            this.CFDIPath = new System.Windows.Forms.TextBox();
            this.button2 = new System.Windows.Forms.Button();
            this.button4 = new System.Windows.Forms.Button();
            this.VOUCHER_ID = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.button5 = new System.Windows.Forms.Button();
            this.label7 = new System.Windows.Forms.Label();
            this.button6 = new System.Windows.Forms.Button();
            this.Detallado = new System.Windows.Forms.CheckBox();
            this.Retencion = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.CURRENCY_ID = new System.Windows.Forms.ComboBox();
            this.button7 = new System.Windows.Forms.Button();
            this.CardCode = new System.Windows.Forms.TextBox();
            this.button8 = new System.Windows.Forms.Button();
            this.button9 = new System.Windows.Forms.Button();
            this.button10 = new System.Windows.Forms.Button();
            this.DocCurrency = new System.Windows.Forms.TextBox();
            this.UUID = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.OCtxt = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.ReciboTxt = new System.Windows.Forms.TextBox();
            this.label13 = new System.Windows.Forms.Label();
            this.DocEntryOC = new System.Windows.Forms.TextBox();
            this.Informacion = new System.Windows.Forms.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this.Dtg)).BeginInit();
            this.SuspendLayout();
            // 
            // label6
            // 
            this.label6.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(16, 420);
            this.label6.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(93, 24);
            this.label6.TabIndex = 516;
            this.label6.Text = "Subtotal";
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label4
            // 
            this.label4.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(257, 420);
            this.label4.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(98, 24);
            this.label4.TabIndex = 517;
            this.label4.Text = "Impuesto";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label2
            // 
            this.label2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(753, 420);
            this.label2.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(58, 24);
            this.label2.TabIndex = 518;
            this.label2.Text = "Total";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // Subtotal
            // 
            this.Subtotal.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.Subtotal.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.Subtotal.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Subtotal.Location = new System.Drawing.Point(113, 420);
            this.Subtotal.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.Subtotal.Name = "Subtotal";
            this.Subtotal.Size = new System.Drawing.Size(140, 24);
            this.Subtotal.TabIndex = 519;
            this.Subtotal.Text = "0.0";
            this.Subtotal.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // Impuesto
            // 
            this.Impuesto.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.Impuesto.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.Impuesto.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Impuesto.Location = new System.Drawing.Point(359, 420);
            this.Impuesto.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.Impuesto.Name = "Impuesto";
            this.Impuesto.Size = new System.Drawing.Size(140, 24);
            this.Impuesto.TabIndex = 520;
            this.Impuesto.Text = "0.0";
            this.Impuesto.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // Total
            // 
            this.Total.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.Total.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.Total.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Total.Location = new System.Drawing.Point(815, 420);
            this.Total.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.Total.Name = "Total";
            this.Total.Size = new System.Drawing.Size(140, 24);
            this.Total.TabIndex = 521;
            this.Total.Text = "0.0";
            this.Total.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // Dtg
            // 
            this.Dtg.AllowUserToAddRows = false;
            this.Dtg.AllowUserToDeleteRows = false;
            this.Dtg.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.Dtg.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.Dtg.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.Dtg.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.QTY,
            this.DESCRIPTION,
            this.Importe,
            this.GL_ACCOUNT_ID,
            this.VAT_AMOUNT,
            this.VAT_GL_ACCT_ID,
            this.Recibo,
            this.oc,
            this.Linea});
            dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle5.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle5.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle5.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle5.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle5.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.Dtg.DefaultCellStyle = dataGridViewCellStyle5;
            this.Dtg.Location = new System.Drawing.Point(11, 192);
            this.Dtg.Margin = new System.Windows.Forms.Padding(2);
            this.Dtg.Name = "Dtg";
            this.Dtg.ReadOnly = true;
            dataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle6.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle6.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle6.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle6.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle6.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle6.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.Dtg.RowHeadersDefaultCellStyle = dataGridViewCellStyle6;
            this.Dtg.RowTemplate.Height = 24;
            this.Dtg.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.Dtg.Size = new System.Drawing.Size(488, 221);
            this.Dtg.TabIndex = 515;
            this.Dtg.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.Dtg_CellDoubleClick);
            // 
            // QTY
            // 
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle2.Format = "N2";
            dataGridViewCellStyle2.NullValue = "0";
            this.QTY.DefaultCellStyle = dataGridViewCellStyle2;
            this.QTY.HeaderText = "Cantidad";
            this.QTY.Name = "QTY";
            this.QTY.ReadOnly = true;
            // 
            // DESCRIPTION
            // 
            this.DESCRIPTION.HeaderText = "Descripción";
            this.DESCRIPTION.Name = "DESCRIPTION";
            this.DESCRIPTION.ReadOnly = true;
            // 
            // Importe
            // 
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle3.NullValue = "C4";
            this.Importe.DefaultCellStyle = dataGridViewCellStyle3;
            this.Importe.HeaderText = "Importe";
            this.Importe.Name = "Importe";
            this.Importe.ReadOnly = true;
            // 
            // GL_ACCOUNT_ID
            // 
            this.GL_ACCOUNT_ID.FillWeight = 150F;
            this.GL_ACCOUNT_ID.HeaderText = "Cuenta Contable";
            this.GL_ACCOUNT_ID.Name = "GL_ACCOUNT_ID";
            this.GL_ACCOUNT_ID.ReadOnly = true;
            this.GL_ACCOUNT_ID.Visible = false;
            this.GL_ACCOUNT_ID.Width = 150;
            // 
            // VAT_AMOUNT
            // 
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle4.Format = "N4";
            this.VAT_AMOUNT.DefaultCellStyle = dataGridViewCellStyle4;
            this.VAT_AMOUNT.HeaderText = "Impuesto";
            this.VAT_AMOUNT.Name = "VAT_AMOUNT";
            this.VAT_AMOUNT.ReadOnly = true;
            // 
            // VAT_GL_ACCT_ID
            // 
            this.VAT_GL_ACCT_ID.HeaderText = "Impuesto Cuenta Contable";
            this.VAT_GL_ACCT_ID.Name = "VAT_GL_ACCT_ID";
            this.VAT_GL_ACCT_ID.ReadOnly = true;
            this.VAT_GL_ACCT_ID.Visible = false;
            this.VAT_GL_ACCT_ID.Width = 180;
            // 
            // Recibo
            // 
            this.Recibo.HeaderText = "Recibo";
            this.Recibo.Name = "Recibo";
            this.Recibo.ReadOnly = true;
            this.Recibo.Visible = false;
            // 
            // oc
            // 
            this.oc.HeaderText = "OC";
            this.oc.Name = "oc";
            this.oc.ReadOnly = true;
            this.oc.Visible = false;
            // 
            // Linea
            // 
            this.Linea.FillWeight = 50F;
            this.Linea.HeaderText = "Linea";
            this.Linea.Name = "Linea";
            this.Linea.ReadOnly = true;
            this.Linea.Visible = false;
            this.Linea.Width = 50;
            // 
            // buttonAdv4
            // 
            this.buttonAdv4.Appearance = Syncfusion.Windows.Forms.ButtonAppearance.Metro;
            this.buttonAdv4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(219)))), ((int)(((byte)(94)))), ((int)(((byte)(94)))));
            this.buttonAdv4.BeforeTouchSize = new System.Drawing.Size(31, 28);
            this.buttonAdv4.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonAdv4.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonAdv4.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.buttonAdv4.IsBackStageButton = false;
            this.buttonAdv4.Location = new System.Drawing.Point(49, 159);
            this.buttonAdv4.Name = "buttonAdv4";
            this.buttonAdv4.Size = new System.Drawing.Size(31, 28);
            this.buttonAdv4.TabIndex = 514;
            this.buttonAdv4.Text = "-";
            // 
            // buttonAdv2
            // 
            this.buttonAdv2.Appearance = Syncfusion.Windows.Forms.ButtonAppearance.Metro;
            this.buttonAdv2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(153)))), ((int)(((byte)(181)))), ((int)(((byte)(105)))));
            this.buttonAdv2.BeforeTouchSize = new System.Drawing.Size(32, 28);
            this.buttonAdv2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonAdv2.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonAdv2.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.buttonAdv2.IsBackStageButton = false;
            this.buttonAdv2.Location = new System.Drawing.Point(11, 159);
            this.buttonAdv2.Name = "buttonAdv2";
            this.buttonAdv2.Size = new System.Drawing.Size(32, 28);
            this.buttonAdv2.TabIndex = 513;
            this.buttonAdv2.Text = "+";
            // 
            // POSTING_DATE
            // 
            this.POSTING_DATE.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.POSTING_DATE.Location = new System.Drawing.Point(264, 127);
            this.POSTING_DATE.Margin = new System.Windows.Forms.Padding(2);
            this.POSTING_DATE.Name = "POSTING_DATE";
            this.POSTING_DATE.Size = new System.Drawing.Size(116, 20);
            this.POSTING_DATE.TabIndex = 512;
            // 
            // INVOICE_DATE
            // 
            this.INVOICE_DATE.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.INVOICE_DATE.Location = new System.Drawing.Point(264, 103);
            this.INVOICE_DATE.Margin = new System.Windows.Forms.Padding(2);
            this.INVOICE_DATE.Name = "INVOICE_DATE";
            this.INVOICE_DATE.Size = new System.Drawing.Size(116, 20);
            this.INVOICE_DATE.TabIndex = 511;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(50, 107);
            this.label1.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(43, 13);
            this.label1.TabIndex = 510;
            this.label1.Text = "Factura";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(217, 130);
            this.label3.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(43, 13);
            this.label3.TabIndex = 509;
            this.label3.Text = "Postear";
            // 
            // button3
            // 
            this.button3.BackColor = System.Drawing.Color.DarkGreen;
            this.button3.FlatAppearance.BorderSize = 0;
            this.button3.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button3.ForeColor = System.Drawing.Color.White;
            this.button3.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.button3.Location = new System.Drawing.Point(11, 72);
            this.button3.Margin = new System.Windows.Forms.Padding(2);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(82, 24);
            this.button3.TabIndex = 508;
            this.button3.Text = "Proveedor";
            this.button3.UseVisualStyleBackColor = false;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // button1
            // 
            this.button1.BackColor = System.Drawing.Color.Teal;
            this.button1.FlatAppearance.BorderSize = 0;
            this.button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button1.ForeColor = System.Drawing.Color.White;
            this.button1.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.button1.Location = new System.Drawing.Point(691, 43);
            this.button1.Margin = new System.Windows.Forms.Padding(2);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(48, 24);
            this.button1.TabIndex = 507;
            this.button1.Text = "Ver";
            this.button1.UseVisualStyleBackColor = false;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // NAME
            // 
            this.NAME.Location = new System.Drawing.Point(217, 74);
            this.NAME.Margin = new System.Windows.Forms.Padding(2);
            this.NAME.Name = "NAME";
            this.NAME.ReadOnly = true;
            this.NAME.Size = new System.Drawing.Size(470, 20);
            this.NAME.TabIndex = 505;
            // 
            // INVOICE_ID
            // 
            this.INVOICE_ID.Location = new System.Drawing.Point(97, 103);
            this.INVOICE_ID.Margin = new System.Windows.Forms.Padding(2);
            this.INVOICE_ID.Name = "INVOICE_ID";
            this.INVOICE_ID.Size = new System.Drawing.Size(116, 20);
            this.INVOICE_ID.TabIndex = 504;
            // 
            // VENDOR_ID
            // 
            this.VENDOR_ID.Location = new System.Drawing.Point(97, 74);
            this.VENDOR_ID.Margin = new System.Windows.Forms.Padding(2);
            this.VENDOR_ID.Name = "VENDOR_ID";
            this.VENDOR_ID.ReadOnly = true;
            this.VENDOR_ID.Size = new System.Drawing.Size(116, 20);
            this.VENDOR_ID.TabIndex = 503;
            // 
            // CFDIPath
            // 
            this.CFDIPath.Location = new System.Drawing.Point(97, 46);
            this.CFDIPath.Margin = new System.Windows.Forms.Padding(2);
            this.CFDIPath.Name = "CFDIPath";
            this.CFDIPath.Size = new System.Drawing.Size(590, 20);
            this.CFDIPath.TabIndex = 506;
            // 
            // button2
            // 
            this.button2.BackColor = System.Drawing.Color.DarkGreen;
            this.button2.FlatAppearance.BorderSize = 0;
            this.button2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button2.ForeColor = System.Drawing.Color.White;
            this.button2.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.button2.Location = new System.Drawing.Point(11, 43);
            this.button2.Margin = new System.Windows.Forms.Padding(2);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(82, 24);
            this.button2.TabIndex = 502;
            this.button2.Text = "CFDI";
            this.button2.UseVisualStyleBackColor = false;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // button4
            // 
            this.button4.BackColor = System.Drawing.Color.DarkOliveGreen;
            this.button4.FlatAppearance.BorderSize = 0;
            this.button4.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button4.ForeColor = System.Drawing.Color.White;
            this.button4.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.button4.Location = new System.Drawing.Point(11, 11);
            this.button4.Margin = new System.Windows.Forms.Padding(2);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(82, 24);
            this.button4.TabIndex = 502;
            this.button4.Text = "Guardar";
            this.button4.UseVisualStyleBackColor = false;
            this.button4.Click += new System.EventHandler(this.button4_Click);
            // 
            // VOUCHER_ID
            // 
            this.VOUCHER_ID.Location = new System.Drawing.Point(97, 127);
            this.VOUCHER_ID.Margin = new System.Windows.Forms.Padding(2);
            this.VOUCHER_ID.Name = "VOUCHER_ID";
            this.VOUCHER_ID.Size = new System.Drawing.Size(116, 20);
            this.VOUCHER_ID.TabIndex = 523;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(46, 130);
            this.label5.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(47, 13);
            this.label5.TabIndex = 510;
            this.label5.Text = "Voucher";
            // 
            // button5
            // 
            this.button5.BackColor = System.Drawing.Color.DarkOliveGreen;
            this.button5.FlatAppearance.BorderSize = 0;
            this.button5.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button5.ForeColor = System.Drawing.Color.White;
            this.button5.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.button5.Location = new System.Drawing.Point(97, 11);
            this.button5.Margin = new System.Windows.Forms.Padding(2);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(82, 24);
            this.button5.TabIndex = 502;
            this.button5.Text = "Limpiar";
            this.button5.UseVisualStyleBackColor = false;
            this.button5.Click += new System.EventHandler(this.button5_Click);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(217, 107);
            this.label7.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(37, 13);
            this.label7.TabIndex = 509;
            this.label7.Text = "Fecha";
            // 
            // button6
            // 
            this.button6.BackColor = System.Drawing.Color.DarkOliveGreen;
            this.button6.FlatAppearance.BorderSize = 0;
            this.button6.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button6.ForeColor = System.Drawing.Color.White;
            this.button6.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.button6.Location = new System.Drawing.Point(183, 11);
            this.button6.Margin = new System.Windows.Forms.Padding(2);
            this.button6.Name = "button6";
            this.button6.Size = new System.Drawing.Size(82, 24);
            this.button6.TabIndex = 524;
            this.button6.Text = "Reglas";
            this.button6.UseVisualStyleBackColor = false;
            this.button6.Click += new System.EventHandler(this.button6_Click);
            // 
            // Detallado
            // 
            this.Detallado.AutoSize = true;
            this.Detallado.Location = new System.Drawing.Point(275, 16);
            this.Detallado.Name = "Detallado";
            this.Detallado.Size = new System.Drawing.Size(160, 17);
            this.Detallado.TabIndex = 525;
            this.Detallado.Text = "Cargar detalle de Conceptos";
            this.Detallado.UseVisualStyleBackColor = true;
            // 
            // Retencion
            // 
            this.Retencion.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.Retencion.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.Retencion.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Retencion.Location = new System.Drawing.Point(609, 420);
            this.Retencion.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.Retencion.Name = "Retencion";
            this.Retencion.Size = new System.Drawing.Size(140, 24);
            this.Retencion.TabIndex = 520;
            this.Retencion.Text = "0.0";
            this.Retencion.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label9
            // 
            this.label9.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(507, 420);
            this.label9.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(98, 24);
            this.label9.TabIndex = 517;
            this.label9.Text = "Retención";
            this.label9.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // CURRENCY_ID
            // 
            this.CURRENCY_ID.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.CURRENCY_ID.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CURRENCY_ID.FormattingEnabled = true;
            this.CURRENCY_ID.Items.AddRange(new object[] {
            "MXP",
            "USD"});
            this.CURRENCY_ID.Location = new System.Drawing.Point(960, 418);
            this.CURRENCY_ID.Name = "CURRENCY_ID";
            this.CURRENCY_ID.Size = new System.Drawing.Size(91, 28);
            this.CURRENCY_ID.TabIndex = 522;
            this.CURRENCY_ID.Text = "MXN";
            // 
            // button7
            // 
            this.button7.BackColor = System.Drawing.Color.Goldenrod;
            this.button7.FlatAppearance.BorderSize = 0;
            this.button7.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button7.ForeColor = System.Drawing.Color.White;
            this.button7.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.button7.Location = new System.Drawing.Point(440, 11);
            this.button7.Margin = new System.Windows.Forms.Padding(2);
            this.button7.Name = "button7";
            this.button7.Size = new System.Drawing.Size(120, 24);
            this.button7.TabIndex = 524;
            this.button7.Text = "Conectar SAP B1";
            this.button7.UseVisualStyleBackColor = false;
            this.button7.Visible = false;
            this.button7.Click += new System.EventHandler(this.button7_Click);
            // 
            // CardCode
            // 
            this.CardCode.Location = new System.Drawing.Point(691, 74);
            this.CardCode.Margin = new System.Windows.Forms.Padding(2);
            this.CardCode.Name = "CardCode";
            this.CardCode.ReadOnly = true;
            this.CardCode.Size = new System.Drawing.Size(82, 20);
            this.CardCode.TabIndex = 526;
            // 
            // button8
            // 
            this.button8.BackColor = System.Drawing.Color.Goldenrod;
            this.button8.FlatAppearance.BorderSize = 0;
            this.button8.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button8.ForeColor = System.Drawing.Color.White;
            this.button8.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.button8.Location = new System.Drawing.Point(691, 11);
            this.button8.Margin = new System.Windows.Forms.Padding(2);
            this.button8.Name = "button8";
            this.button8.Size = new System.Drawing.Size(131, 24);
            this.button8.TabIndex = 524;
            this.button8.Text = "Generar CxP SAP B1";
            this.button8.UseVisualStyleBackColor = false;
            this.button8.Visible = false;
            this.button8.Click += new System.EventHandler(this.button8_Click);
            // 
            // button9
            // 
            this.button9.BackColor = System.Drawing.Color.Goldenrod;
            this.button9.FlatAppearance.BorderSize = 0;
            this.button9.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button9.ForeColor = System.Drawing.Color.White;
            this.button9.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.button9.Location = new System.Drawing.Point(567, 11);
            this.button9.Margin = new System.Windows.Forms.Padding(2);
            this.button9.Name = "button9";
            this.button9.Size = new System.Drawing.Size(120, 24);
            this.button9.TabIndex = 524;
            this.button9.Text = "Validar Card Code";
            this.button9.UseVisualStyleBackColor = false;
            this.button9.Visible = false;
            this.button9.Click += new System.EventHandler(this.button9_Click);
            // 
            // button10
            // 
            this.button10.BackColor = System.Drawing.Color.Goldenrod;
            this.button10.FlatAppearance.BorderSize = 0;
            this.button10.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button10.ForeColor = System.Drawing.Color.White;
            this.button10.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.button10.Location = new System.Drawing.Point(826, 11);
            this.button10.Margin = new System.Windows.Forms.Padding(2);
            this.button10.Name = "button10";
            this.button10.Size = new System.Drawing.Size(120, 24);
            this.button10.TabIndex = 524;
            this.button10.Text = "Desconectar SAP B1";
            this.button10.UseVisualStyleBackColor = false;
            this.button10.Visible = false;
            this.button10.Click += new System.EventHandler(this.button10_Click);
            // 
            // DocCurrency
            // 
            this.DocCurrency.Location = new System.Drawing.Point(777, 74);
            this.DocCurrency.Margin = new System.Windows.Forms.Padding(2);
            this.DocCurrency.Name = "DocCurrency";
            this.DocCurrency.ReadOnly = true;
            this.DocCurrency.Size = new System.Drawing.Size(69, 20);
            this.DocCurrency.TabIndex = 526;
            // 
            // UUID
            // 
            this.UUID.Location = new System.Drawing.Point(449, 103);
            this.UUID.Margin = new System.Windows.Forms.Padding(2);
            this.UUID.Name = "UUID";
            this.UUID.ReadOnly = true;
            this.UUID.Size = new System.Drawing.Size(397, 20);
            this.UUID.TabIndex = 505;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(386, 106);
            this.label8.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(59, 13);
            this.label8.TabIndex = 509;
            this.label8.Text = "Folio Fiscal";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(386, 130);
            this.label10.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(22, 13);
            this.label10.TabIndex = 527;
            this.label10.Text = "OC";
            // 
            // OCtxt
            // 
            this.OCtxt.Location = new System.Drawing.Point(412, 127);
            this.OCtxt.Margin = new System.Windows.Forms.Padding(2);
            this.OCtxt.Name = "OCtxt";
            this.OCtxt.ReadOnly = true;
            this.OCtxt.Size = new System.Drawing.Size(65, 20);
            this.OCtxt.TabIndex = 528;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(606, 130);
            this.label12.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(41, 13);
            this.label12.TabIndex = 529;
            this.label12.Text = "Recibo";
            // 
            // ReciboTxt
            // 
            this.ReciboTxt.Location = new System.Drawing.Point(651, 127);
            this.ReciboTxt.Margin = new System.Windows.Forms.Padding(2);
            this.ReciboTxt.Name = "ReciboTxt";
            this.ReciboTxt.ReadOnly = true;
            this.ReciboTxt.Size = new System.Drawing.Size(102, 20);
            this.ReciboTxt.TabIndex = 530;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(481, 130);
            this.label13.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(51, 13);
            this.label13.TabIndex = 527;
            this.label13.Text = "DocEntry";
            // 
            // DocEntryOC
            // 
            this.DocEntryOC.Location = new System.Drawing.Point(536, 127);
            this.DocEntryOC.Margin = new System.Windows.Forms.Padding(2);
            this.DocEntryOC.Name = "DocEntryOC";
            this.DocEntryOC.ReadOnly = true;
            this.DocEntryOC.Size = new System.Drawing.Size(65, 20);
            this.DocEntryOC.TabIndex = 528;
            // 
            // Informacion
            // 
            this.Informacion.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Informacion.Location = new System.Drawing.Point(511, 192);
            this.Informacion.Margin = new System.Windows.Forms.Padding(2);
            this.Informacion.Multiline = true;
            this.Informacion.Name = "Informacion";
            this.Informacion.Size = new System.Drawing.Size(540, 221);
            this.Informacion.TabIndex = 531;
            // 
            // GenerarCxP
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1061, 453);
            this.Controls.Add(this.Informacion);
            this.Controls.Add(this.ReciboTxt);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.DocEntryOC);
            this.Controls.Add(this.label13);
            this.Controls.Add(this.OCtxt);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.DocCurrency);
            this.Controls.Add(this.CardCode);
            this.Controls.Add(this.Detallado);
            this.Controls.Add(this.button9);
            this.Controls.Add(this.button8);
            this.Controls.Add(this.button10);
            this.Controls.Add(this.button7);
            this.Controls.Add(this.button6);
            this.Controls.Add(this.VOUCHER_ID);
            this.Controls.Add(this.CURRENCY_ID);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.Subtotal);
            this.Controls.Add(this.Retencion);
            this.Controls.Add(this.Impuesto);
            this.Controls.Add(this.Total);
            this.Controls.Add(this.Dtg);
            this.Controls.Add(this.buttonAdv4);
            this.Controls.Add(this.buttonAdv2);
            this.Controls.Add(this.POSTING_DATE);
            this.Controls.Add(this.INVOICE_DATE);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.button3);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.UUID);
            this.Controls.Add(this.NAME);
            this.Controls.Add(this.INVOICE_ID);
            this.Controls.Add(this.VENDOR_ID);
            this.Controls.Add(this.CFDIPath);
            this.Controls.Add(this.button5);
            this.Controls.Add(this.button4);
            this.Controls.Add(this.button2);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "GenerarCxP";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Generar CxP";
            ((System.ComponentModel.ISupportInitialize)(this.Dtg)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label Subtotal;
        private System.Windows.Forms.Label Impuesto;
        private System.Windows.Forms.Label Total;
        private System.Windows.Forms.DataGridView Dtg;
        private Syncfusion.Windows.Forms.ButtonAdv buttonAdv4;
        private Syncfusion.Windows.Forms.ButtonAdv buttonAdv2;
        private System.Windows.Forms.DateTimePicker POSTING_DATE;
        private System.Windows.Forms.DateTimePicker INVOICE_DATE;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.TextBox NAME;
        private System.Windows.Forms.TextBox INVOICE_ID;
        private System.Windows.Forms.TextBox VENDOR_ID;
        private System.Windows.Forms.TextBox CFDIPath;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.TextBox VOUCHER_ID;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Button button5;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Button button6;
        private System.Windows.Forms.CheckBox Detallado;
        private System.Windows.Forms.Label Retencion;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.ComboBox CURRENCY_ID;
        private System.Windows.Forms.Button button7;
        private System.Windows.Forms.TextBox CardCode;
        private System.Windows.Forms.Button button8;
        private System.Windows.Forms.Button button9;
        private System.Windows.Forms.Button button10;
        private System.Windows.Forms.TextBox DocCurrency;
        private System.Windows.Forms.TextBox UUID;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox OCtxt;
        private System.Windows.Forms.DataGridViewTextBoxColumn QTY;
        private System.Windows.Forms.DataGridViewTextBoxColumn DESCRIPTION;
        private System.Windows.Forms.DataGridViewTextBoxColumn Importe;
        private System.Windows.Forms.DataGridViewTextBoxColumn GL_ACCOUNT_ID;
        private System.Windows.Forms.DataGridViewTextBoxColumn VAT_AMOUNT;
        private System.Windows.Forms.DataGridViewTextBoxColumn VAT_GL_ACCT_ID;
        private System.Windows.Forms.DataGridViewTextBoxColumn Recibo;
        private System.Windows.Forms.DataGridViewTextBoxColumn oc;
        private System.Windows.Forms.DataGridViewTextBoxColumn Linea;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.TextBox ReciboTxt;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.TextBox DocEntryOC;
        private System.Windows.Forms.TextBox Informacion;
    }
}