﻿using ClasificadorProductos.formularios;
using Generales;
using ModeloDocumentosFX;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;

namespace NDC
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main(string[] args)
        {
            try
            {


                Application.EnableVisualStyles();
                Application.SetCompatibleTextRenderingDefault(false);
                //if (args.Length != 3)
                //{
                //    ErrorFX.mostrar("Los argumentos para llamar la NDC son: 1-Entidad 2-DOcumento 3-Cliente Ejemplo: TIGLA SL11231 BOEING", true, true);
                //    Application.Exit();
                //}
                string entity_id = args[0];
                //MessageBox.Show(entity_id);
                string invoice_id = args[1];
                //MessageBox.Show(invoice_id);
                string customer_id = args[2];
                //MessageBox.Show(customer_id);
                ModeloDocumentosFXContainer dbContext = new ModeloDocumentosFXContainer();

                string conection = dbContext.Database.Connection.ConnectionString;
                //MessageBox.Show(conection);
                Application.Run(new ClasificadorProductos.formularios.frmTipoRelacionNDCnormalSimplificado(entity_id, invoice_id, customer_id));
                //Application.Exit();
            }
            catch (Exception err)
            {
                ErrorFX.mostrar(err, false, true, "Program - 42 - ");
            }
        }
    }
}
