﻿using Generales;
using Ionic.Zip;
using System;
using System.IO;
using System.Text;
using System.Xml;
using System.Xml.Linq;

namespace Timbrado
{
    public class Timbrar
    {
        public string error = "";
        public string mensaje = "";
        public Timbrar()
        {

        }

        public byte[] ReadBinaryFile(string fileName)
        {
            if (File.Exists(fileName))
            {
                try
                {
                    ///Open and read a file&#12290;
                    FileStream fileStream = File.OpenRead(fileName);
                    byte[] archivo = ConvertStreamToByteBuffer(fileStream);
                    fileStream.Close();
                    return archivo;
                }
                catch (Exception ex)
                {
                    return new byte[0];
                }
            }
            else
            {
                return new byte[0];
            }
        }

        public byte[] ConvertStreamToByteBuffer(System.IO.Stream theStream)
        {
            int b1;
            System.IO.MemoryStream tempStream = new System.IO.MemoryStream();
            while ((b1 = theStream.ReadByte()) != -1)
            {
                tempStream.WriteByte(((byte)b1));
            }
            return tempStream.ToArray();
        }

        public string CreateMD5(string input)
        {
            // Use input string to calculate MD5 hash
            using (System.Security.Cryptography.MD5 md5 = System.Security.Cryptography.MD5.Create())
            {
                byte[] inputBytes = System.Text.Encoding.ASCII.GetBytes(input);
                byte[] hashBytes = md5.ComputeHash(inputBytes);

                // Convert the byte array to hexadecimal string
                StringBuilder sb = new StringBuilder();
                for (int i = 0; i < hashBytes.Length; i++)
                {
                    sb.Append(hashBytes[i].ToString("X2"));
                }
                return sb.ToString().ToLower();
            }
        }

        #region EDICOM
        public bool timbrarEDICOM(int opcion, string archivo, string factura, string ROW_ID_EMPRESA)
        {
            try
            {
                //Buscar archivo
                if (!File.Exists(archivo))
                {
                    ErrorFX.mostrar("El archivo " + archivo + " no existe ", true, false,false);
                    return false;
                }
                //Buscar usuarios de EDICOM
                bool existe=Globales.cargarTimbrado("EDICOM",ROW_ID_EMPRESA);
                if (!existe)
                {
                    //ErrorFX.mostrar("La configuración para EDICOM no existe ", false, false, false);
                    //return false;
                    //}
                    //else
                    //{
                    //Timbrar por Solucion Factible
                    BitacoraFX.Log("Pack SL");
                    pac = " SL ";
                    return timbrarSolucionFactible(opcion, archivo, factura,ROW_ID_EMPRESA);
                }
                pac = " EDICOM ";
                BitacoraFX.Log("Pack EDICOM ");
                //Comprimir Archivo
                string Archivo_Tmp = string.Format(@"{0}.xml", factura);
                File.Copy(archivo, Archivo_Tmp, true);

                string ARCHIVO_zip = Archivo_Tmp.ToUpper().Replace("XML", "ZIP");

                using (ZipFile zip = new ZipFile())
                {
                    zip.AddFile(Archivo_Tmp);
                    zip.Save(ARCHIVO_zip);
                }

                //Fin de Compresion de Archivo
                File.Delete(Archivo_Tmp);

                byte[] ARCHIVO_Leido = ReadBinaryFile(ARCHIVO_zip);
                File.Delete(ARCHIVO_zip);

                EDICOM_Servicio.CFDiClient oCFDiClient = new EDICOM_Servicio.CFDiClient();

                byte[] PROCESO;
                //Rodrigo Escalona 04/11/2019 Desencriptar el password
                string passwordLocal = cENCRIPTACION.Decrypt(Globales.oCFDI_USUARIO.PASSWORD);

                BitacoraFX.Log("Pack EDICOM ");
                BitacoraFX.Log("P: "+ passwordLocal);
                switch (opcion)
                {
                    case 1:
                        try
                        {
                            PROCESO = oCFDiClient.getCfdi(Globales.oCFDI_USUARIO.USUARIO, passwordLocal, ARCHIVO_Leido);
                            guardarArchivoEDICOM(PROCESO, archivo, factura);

                        }
                        catch (Exception oException)
                        {
                            cargarErrorCompleto(oException);
                            ErrorFX.mostrar(oException, true, true, "Factura: " + factura, true);
                            return false;
                        }

                        break;
                    case 2:
                        try
                        {
                            PROCESO = oCFDiClient.getCfdiTest(Globales.oCFDI_USUARIO.USUARIO, passwordLocal, ARCHIVO_Leido);
                            guardarArchivoEDICOM(PROCESO, archivo, factura);
                        }
                        catch (Exception oException)
                        {
                            cargarErrorCompleto(oException);
                            ErrorFX.mostrar(oException, true, true, "Factura: " + factura, true);
                            return false;
                        }
                        break;
                    case 3:
                        try
                        {
                            PROCESO = oCFDiClient.getTimbreCfdi(Globales.oCFDI_USUARIO.USUARIO, passwordLocal, ARCHIVO_Leido);
                            guardarArchivoEDICOM(PROCESO, archivo, factura);
                        }
                        catch (Exception oException)
                        {
                            cargarErrorCompleto(oException);
                            ErrorFX.mostrar(oException, true, true, "Factura: " + factura, true);
                            return false;
                        }
                        break;
                    case 4:
                        try
                        {
                            PROCESO = oCFDiClient.getTimbreCfdiTest(Globales.oCFDI_USUARIO.USUARIO, passwordLocal, ARCHIVO_Leido);
                            guardarArchivoEDICOM(PROCESO, archivo,factura);
                        }
                        catch (Exception oException)
                        {
                            cargarErrorCompleto(oException);
                            ErrorFX.mostrar(oException, true, true, "Factura: " + factura, true);
                            return false;
                        }
                        break;
                }
                return true;
            }
            catch (Exception oException)
            {
                cargarErrorCompleto(oException);
                ErrorFX.mostrar(oException, true, true, "Timbrar.cs - 143", true);
                return false;
            }
        }

        private bool timbrarSolucionFactible(int opcion, string archivo, string factura, string ROW_ID_EMPRESA)
        {
            string prod_endpoint = "TimbradoEndpoint_PRODUCCION";
            string test_endpoint = "TimbradoEndpoint_TESTING";

            //Si recibe error 417 deberá descomentar la linea a continuación
            System.Net.ServicePointManager.Expect100Continue = false;

            //El paquete o namespace en el que se encuentran las clases
            //será el que se define al agregar la referencia al WebService,
            //en este ejemplo es: com.sf.ws.Timbrado

            Solucion_Factible.TimbradoPortTypeClient portClient = null;
            portClient = (opcion==1)
                ? new Solucion_Factible.TimbradoPortTypeClient(prod_endpoint)
                : portClient = new Solucion_Factible.TimbradoPortTypeClient(test_endpoint);
            pac = "SL";
            try
            {
                System.Console.WriteLine("Sending request...");
                System.Console.WriteLine("EndPoint = " + portClient.Endpoint.Address);
                
                //Comprimir Archivo
                string Archivo_Tmp = factura + ".xml";
                File.Copy(archivo, Archivo_Tmp, true);
                string ARCHIVO_zip = Archivo_Tmp.ToUpper().Replace("XML", "ZIP");
                using (ZipFile zip = new ZipFile())
                {
                    zip.AddFile(Archivo_Tmp);
                    zip.Save(ARCHIVO_zip);
                }
                //Fin de Compresion de Archivo
                File.Delete(Archivo_Tmp);

                byte[] ARCHIVO_Leido = ReadBinaryFile(ARCHIVO_zip);
                File.Delete(ARCHIVO_zip);
                Solucion_Factible.CFDICertificacion response;
                if (opcion == 2)
                {
                    response = portClient.timbrar("testing@solucionfactible.com", "timbrado.SF.16672", ARCHIVO_Leido, true);
                }
                else
                {
                    //Cargar los datos del SL
                    Globales.cargarUsuarioSL(ROW_ID_EMPRESA);
                    if (String.IsNullOrEmpty(Globales.oCFDI_USUARIO.USUARIO))
                    {
                        ErrorFX.mostrar("Timbrar.cs - 233 - Timbrado Solución Factible - Error en la carga del Usuario en CFDI Usuario", true,true,true);
                        return false;
                    }
                    response = portClient.timbrar(Globales.oCFDI_USUARIO.USUARIO, Globales.oCFDI_USUARIO.PASSWORD, ARCHIVO_Leido, true);
                    
                }
                if (response==null)
                {
                    //Error SL archivo no generado
                    Exception oException = new Exception("Error SL el proceso de timbrado esta en 0");
                    cargarErrorCompleto(oException);
                    ErrorFX.mostrar(oException, true, true, "Factura: " + factura, true);
                    return false;
                }
                
                BitacoraFX.Log(response.status.ToString());
                BitacoraFX.Log(response.mensaje.ToString());
                BitacoraFX.Log("Resultados recibidos" + response.resultados.Length);

                if (response.status==601)
                {
                    //Error SL archivo no generado
                    Exception oException = new Exception(response.mensaje.ToString() + Globales.oCFDI_USUARIO.USUARIO + "Password:" + Globales.oCFDI_USUARIO.PASSWORD);
                    cargarErrorCompleto(oException);
                    ErrorFX.mostrar(oException, true, true, "Factura: " + factura, true);
                    return false;
                }

                Solucion_Factible.CFDIResultadoCertificacion[] resultados = response.resultados;
                if (resultados[0] != null)
                {
                    if (resultados[0].status == 200)
                    {
                        if (resultados[0].cfdiTimbrado.Length==0)
                        {
                            //Error SL archivo no generado
                            Exception oException = new Exception("Error SL el proceso de timbrado esta en 0");
                            cargarErrorCompleto(oException);
                            ErrorFX.mostrar(oException, true, true, "Factura: " + factura, true);
                            return false;
                        }
                        guardarArchivoSolucionFactible(resultados[0].cfdiTimbrado, archivo, factura);
                    }
                    else
                    {
                        Exception oException = new Exception(resultados[0].mensaje);
                        cargarErrorCompleto(oException);
                        ErrorFX.mostrar(oException, true, true, "Factura: " + factura, true);
                        return false;
                    }
                }
                return true;
            }
            catch (Exception oException)
            {
                cargarErrorCompleto(oException);
                ErrorFX.mostrar(oException, true, true, "Timbrar.cs - 194 - Timbrado Solución Factible", true);
                return false;
            }
        }

        #endregion

        private void cargarErrorCompleto(Exception error)
        {
            string mensaje = error.Message.ToString();
            if (error.InnerException != null)
            {
                mensaje += Environment.NewLine + error.InnerException.Message.ToString();
            }
            mensaje=mensaje.Replace("'", "");
            this.error = mensaje;
        }

        private void guardarArchivoEDICOM(byte[] PROCESO, string archivo, string factura)
        {
            try
            {
                //Guardar los PROCESO
                File.WriteAllBytes("TEMPORAL.ZIP", PROCESO);
                //Descomprimir
                String TargetDirectory = "TEMPORAL";
                using (Ionic.Zip.ZipFile zip = Ionic.Zip.ZipFile.Read("TEMPORAL.ZIP"))
                {
                    zip.ExtractAll(TargetDirectory, Ionic.Zip.ExtractExistingFileAction.OverwriteSilently);
                }

                //Copiar el ARCHIVO Descrompimido por otro
                string ARCHIVO_descomprimido = TargetDirectory + @"\" + "SIGN_" + factura + ".XML";
                File.Copy(ARCHIVO_descomprimido, archivo, true);
                File.Delete(ARCHIVO_descomprimido);
                File.Delete("TEMPORAL.ZIP");
            }
            catch (Exception oException)
            {
                ErrorFX.mostrar(oException, true, true, "Timbrar.cs - 179",true);
            }

        }

        private void guardarArchivoSolucionFactible(byte[] PROCESO, string archivo, string factura)
        {
            try
            {
                File.WriteAllBytes(archivo, PROCESO);
                
            }
            catch (Exception oException)
            {
                ErrorFX.mostrar(oException, true, true, "Timbrar.cs - 306 - Solucion Factible", true);
            }

        }
        string pac = "STO ";
        public bool timbrar(int opcion, string xml, string invoiceId, string ROW_ID_EMPRESA)
        {
            try
            {
                bool resultado = timbrarSTO(opcion, xml, invoiceId);
                pac = "STO ";
                if (!resultado)
                {
                    //Validar el mensaje 
                    if (this.mensaje == "")
                    {
                        BitacoraFX.Log("timbrarEDICOM - 364");
                        resultado = timbrarEDICOM(opcion, xml, invoiceId, ROW_ID_EMPRESA);
                        //if (resultado)
                        //{
                            
                        //}
                    }
                }



                if (resultado)
                {
                    if (opcion==1)
                    {
                        mensaje = "Sellada " + pac  + " " +  DateTime.Now.ToShortDateString() + " " + DateTime.Now.ToLongTimeString();
                    }
                    else
                    {
                        mensaje = "Test  " + pac + " " +  DateTime.Now.ToShortDateString() + " " + DateTime.Now.ToLongTimeString();
                    }
                }
                else
                {
                    mensaje = "Error  " + pac + " " + mensaje  + " " + DateTime.Now.ToShortDateString() + " " + DateTime.Now.ToLongTimeString();
                }

                return resultado;
            }
            catch(Exception error)
            {
                ErrorFX.mostrar(error, true, true, "Timbrar.cs - 314",true);
            }
            return false;
        }
        #region STO
        private bool timbrarSTO(int opcion, string xml, string factura)
        {
            try
            {
                //Buscar archivo
                if (!File.Exists(xml))
                {
                    ErrorFX.mostrar("El archivo " + xml + " no existe ", true, true,false);
                    return false;
                }
                //Buscar usuarios de STO
                bool existe = Globales.cargarTimbrado("STO");
                if (!existe)
                {
                    //ErrorFX.mostrar("La configuración para STO no existe ", false, false,false);
                    return false;
                }

                //Comprimir Archivo
                string Archivo_Tmp = string.Format(@"{0}.xml", factura);
                File.Copy(xml, Archivo_Tmp, true);
                byte[] ARCHIVO_Leido = ReadBinaryFile(Archivo_Tmp);
                File.Delete(Archivo_Tmp);

                byte[] PROCESO;

                switch (opcion)
                {
                    case 1:
                        try
                        {
                            timbradoSTO.STOProduccion.TimbradoClient cliente = new timbradoSTO.STOProduccion.TimbradoClient();
                            timbradoSTO.STOProduccion.timbrarCfdiRequest otimbrarCfdiRequest = new timbradoSTO.STOProduccion.timbrarCfdiRequest();
                            PROCESO = cliente.timbrarCfdi(Globales.oCFDI_USUARIO.USUARIO, CreateMD5(Globales.oCFDI_USUARIO.PASSWORD), ARCHIVO_Leido);
                            return guardarArchivoSTO(PROCESO, xml, factura);
                        }
                        catch (Exception pe)
                        {
                            ErrorFX.mostrar(pe, true, true,"Factura: " + factura, true);
                            return false;
                        }
                        break;
                    case 2:
                        try
                        {
                            timbradoSTO.STOPrueba.TimbradoClient cliente = new timbradoSTO.STOPrueba.TimbradoClient();
                            timbradoSTO.STOPrueba.timbrarCfdiRequest otimbrarCfdiRequest = new timbradoSTO.STOPrueba.timbrarCfdiRequest();
                            PROCESO = cliente.timbrarCfdi(Globales.oCFDI_USUARIO.USUARIO_PRUEBA, CreateMD5(Globales.oCFDI_USUARIO.PASSWORD_PRUEBA), ARCHIVO_Leido);
                            return guardarArchivoSTO(PROCESO, xml, factura);

                        }
                        catch (Exception pe)
                        {
                            ErrorFX.mostrar(pe, true, true, "Factura: " + factura, true);
                            return false;
                        }
                        break;
                }
                return true;

            }
            catch (Exception error)
            {
                ErrorFX.mostrar(error, false, true, "Timbrar.cs - 334",true);
            }
            return false;
        }

        private bool guardarArchivoSTO(byte[] PROCESO, string archivo, string factura)
        {
            this.mensaje = "";
            //Guardar los PROCESO
            string archivoTMP = System.IO.Path.GetTempPath() + Guid.NewGuid().ToString() + ".xml";
            try
            {
                File.WriteAllBytes(archivoTMP, PROCESO);
                //Cargar el XML y parsearlo
                XDocument doc = XDocument.Load(archivoTMP);
                string codigo = doc.Root.Element("Codigo").Value.ToString();
                if (codigo == "10")
                {
                    //Cargar el XML
                    string CFDI = doc.Root.Element("CFDI").Value.ToString();
                    XmlDocument xdoc = new XmlDocument();
                    xdoc.LoadXml(CFDI);
                    xdoc.Save(archivo);
                }
                else
                {
                    //Mostrar el error STO
                    string Mensaje = doc.Root.Element("Mensaje").Value.ToString();
                    this.mensaje = codigo + " - " + Mensaje;
                }
            }
            catch (Exception oException)
            {
                ErrorFX.mostrar(oException, true, true, "Timbrar.cs - 179",false);
            }
            finally
            {
                if (File.Exists(archivoTMP))
                {
                    File.Delete(archivoTMP);
                }
            }
            if (this.mensaje != "")
            {
                return false;
            }
            return true;
        }

        #endregion
    }
}
