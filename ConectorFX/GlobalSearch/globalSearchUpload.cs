﻿using RestSharp;
using RestSharp.Authenticators;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace ConectorFX.GlobalSearch
{
    class globalSearchUpload
    {
        public string log = "";
        string token = "";
        string JobID = "";
        RestClient ApiClient;
        public void subir(string archivo)
        {
            string token = "";
            try
            {
                globalSearch oglobalSearch = new globalSearch();

                ApiClient = new RestClient(oglobalSearch.ocSqUsuario.servidor);
                //basic authentication
                ApiClient.Authenticator = new HttpBasicAuthenticator(oglobalSearch.ocSqUsuario.usuario, oglobalSearch.ocSqUsuario.password);
                token = oglobalSearch.GetLicense();

                //Abrir el chunck para cargar
                //upload?database={dbID}&archive={archiveID}&time={time}&filetype={type}&token={token}
                var requestArchivo = new RestRequest("api/upload?database={db}&archive={archiveID}&time={time}&filetype={type}&token={token}", Method.POST);
                ///dbs/{Database}/archives?token={token}&format={json|xml}
                //requestArchivo.AddParameter("token", token, ParameterType.UrlSegment); //have to specifiy type on POST
                requestArchivo.AddParameter("db", oglobalSearch.ocSqUsuario.databaseId, ParameterType.UrlSegment);
                requestArchivo.AddParameter("archiveID", oglobalSearch.ocSqUsuario.archiveId, ParameterType.UrlSegment);
                requestArchivo.AddParameter("time", DateTime.Now.ToLocalTime(), ParameterType.UrlSegment);
                requestArchivo.AddParameter("type", "text", ParameterType.UrlSegment);
                requestArchivo.AddParameter("token", token, ParameterType.UrlSegment);
                var resultado = ApiClient.Execute<cChunkInicio>(requestArchivo);
                if (resultado.StatusCode != HttpStatusCode.OK)
                {
                    log = "Error:" + resultado.Content;
                    return;
                }
                JobID = resultado.Data.JobID;


                //Enviar el archivo
                var requestArchivoEnviar = new RestRequest("api/upload?time={time}&token={token}", Method.POST);
                requestArchivoEnviar.AddParameter("time", DateTime.Now.ToLocalTime(), ParameterType.UrlSegment);
                requestArchivoEnviar.AddParameter("token", token, ParameterType.UrlSegment);
                requestArchivoEnviar.AddFile("file", archivo);
                requestArchivoEnviar.AddHeader("Content-Type", "multipart/form-data");
                requestArchivoEnviar.AlwaysMultipartFormData = true;

                var responseArchivoEnviar = ApiClient.Execute(requestArchivoEnviar);
                if (responseArchivoEnviar.StatusCode != HttpStatusCode.OK)
                {
                    log = "Error:" + responseArchivoEnviar.Content;
                }
                //Cerrar el archivo



            }
            catch (Exception ex)
            {
                log += ex.Message + Environment.NewLine;
            }
            finally
            {
                if (!String.IsNullOrEmpty(token))
                {
                    globalSearch oglobalSearch = new globalSearch();
                    oglobalSearch.ReleaseLicense(token);
                }
            }
        }


    }
}
