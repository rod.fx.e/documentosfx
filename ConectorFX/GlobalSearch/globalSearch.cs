﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using RestSharp;
using RestSharp.Authenticators;
using System.Net;
using System.IO;

namespace ConectorFX.GlobalSearch
{
    class globalSearch
    {
        public cSqUsuario ocSqUsuario;
        public RestClient ApiClient;
        public globalSearch(){
            string path = System.AppDomain.CurrentDomain.BaseDirectory + "\\GlobalSearch.cnfg";
            if (System.IO.File.Exists(path))
            {
                using (Stream stream = System.IO.File.Open(path, FileMode.Open))
                {
                    var binaryFormatter = new System.Runtime.Serialization.Formatters.Binary.BinaryFormatter();
                    try
                    {
                        ocSqUsuario = (cSqUsuario)binaryFormatter.Deserialize(stream);
                        ApiClient = new RestClient(ocSqUsuario.servidor);
                    }
                    catch
                    {

                    }
                }
            }
        }

        public String GetLicense()
        {
            ApiClient.Authenticator = new HttpBasicAuthenticator(ocSqUsuario.usuario, ocSqUsuario.password);
            var request = new RestRequest("api/licenses");
            var license = ApiClient.Execute<License>(request);
            if (license.StatusCode != HttpStatusCode.OK)
            {
                if (license.StatusCode == HttpStatusCode.Unauthorized)
                {
                    throw new Exception("Unable to get a License: The passed user is Unauthorized.");
                }
                else if (license.StatusCode == HttpStatusCode.BadRequest)
                {
                    throw new Exception("Unable to get a License: " + license.Content);
                }
                else if (license.StatusCode == HttpStatusCode.NotFound)
                {
                    throw new Exception("Unable to get a License: Unable to connect to the license server, server not found.");
                }
                else
                {
                    throw new Exception("Unable to get a License: " + license.Content);
                }
            }
            return license.Data.Token;
        }
        
        public void ReleaseLicense(String Token)
        {
            var request = new RestRequest("api/licenses/" + Token);
            var response = ApiClient.Execute(request);
            if (response.ErrorException != null)
            {
                throw new Exception("Unable to release license token. ", response.ErrorException);
            }
            else if (response.StatusCode != HttpStatusCode.OK)
            {
                throw new Exception("Unable to release license token. " + response.Content);
            }
        }


    }


    public class License
    {
        public License() { }

        public string AuthServer { get; set; }
        public DateTime DateAccessed { get; set; }
        public DateTime DateCreated { get; set; }
        public string Domain { get; set; }
        public string IPAddress { get; set; }
        public int Reg { get; set; }
        public string Token { get; set; }
        public int Type { get; set; }
        public string Username { get; set; }
    }

    public class FieldItem
    {
        public List<String> MVAL;
        public int ID { get; set; }
        public string VAL { get; set; }

        public FieldItem(int id, string val)
        {
            ID = id;
            VAL = val;
        }
    }

    public class Indexer
    {
        public List<Field> fields { get; set; }
        public List<File> files { get; set; }
        public Indexer()
        {
            fields = new List<Field>();
            files = new List<File>();
        }
    }

    public class File
    {
        public String name;
        public File() { }
        public File(String Name)
        {
            name = Name;
        }
    }

    public class cChunkInicio
    {
        public cChunkInicio() { }

        public int ChunkLimit { get; set; }
        public string JobID { get; set; }
    }
    public class Field
    {
        public String name;
        public String value;
        public Field() { }
        public Field(String Name, String Value)
        {
            this.name = Name;
            this.value = Value;
        }
    }

    public class UploadedFile
    {
        public String name { get; set; }
        public Boolean isEmail { get; set; }
        public Dictionary<string, string> oEmailData { get; set; }
        public String test { get; set; }
    }

    public class UploadedFileList
    {
        public List<UploadedFile> files { get; set; }
    }

}
