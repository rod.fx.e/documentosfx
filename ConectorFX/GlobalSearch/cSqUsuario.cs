﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConectorFX.GlobalSearch
{
    [Serializable]
    class cSqUsuario
    {
        public string servidor;
        public string usuario;
        public string password;
        public string databaseId;
        public string archiveId;
    }
}
