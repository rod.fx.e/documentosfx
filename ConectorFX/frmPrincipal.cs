#region Copyright Syncfusion Inc. 2001 - 2017
// Copyright Syncfusion Inc. 2001 - 2017. All rights reserved.
// Use of this code is subject to the terms of our license.
// A copy of the current license can be obtained at any time by e-mailing
// licensing@syncfusion.com. Any infringement will be prosecuted under
// applicable laws. 
#endregion
using Generales;
using ModeloSincronizacion;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Data.OleDb;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Windows.Forms;
using System.Data.Entity;
using System.Data.Entity.Validation;
using ConectorFX.GlobalSearch;
using RestSharp;
using CFDI33;
using System.Xml.Serialization;
using System.Globalization;
using System.Text.RegularExpressions;

namespace ConectorFX
{
    //TODO: Agregar el Auxliar contable a DocumentosFX
    public partial class frmPrincipal : Syncfusion.Windows.Forms.MetroForm
    {
        int segundos = 3600;
        string horaInicialtr = "";
        string horaFinaltr = "";

        public frmPrincipal()
        {
            InitializeComponent();

            this.notifyIcon.BalloonTipIcon = System.Windows.Forms.ToolTipIcon.Info;
            this.notifyIcon.BalloonTipText = Application.ProductName;
            this.notifyIcon.BalloonTipTitle = "Sincronizador minimizado";
            this.notifyIcon.Text = "Conextor - minimizado";



            try
            {
                horaInicialtr = ConfigurationManager.AppSettings["horaInicial"].ToString();
            }
            catch
            {

            }

            try
            {
                horaFinaltr = ConfigurationManager.AppSettings["horaFinal"].ToString();
            }
            catch
            {

            }

            fin.Value = DateTime.Now.AddYears(10);
            cargarSegundos();
        }

        private void cargarSegundos()
        {
            try
            {
                segundos = int.Parse(ConfigurationManager.AppSettings["segundos"]);
            }
            catch
            {

            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            cargar();
        }
        public string Fechammmyy(string pFecha)
        {
            DateTime Fecha = DateTime.Parse(pFecha.ToString());
            string FechaFormateada = "";
            string[] mMeses = new string[13];

            mMeses[1] = "Ene";
            mMeses[2] = "Feb";
            mMeses[3] = "Mar";
            mMeses[4] = "Abr";
            mMeses[5] = "May";
            mMeses[6] = "Jun";
            mMeses[7] = "Jul";
            mMeses[8] = "Agt";
            mMeses[9] = "Sep";
            mMeses[10] = "Oct";
            mMeses[11] = "Nov";
            mMeses[12] = "Dic";

            int Mes = int.Parse(Fecha.Month.ToString());

            FechaFormateada = mMeses[Mes] + AgregarCero(Fecha.Year.ToString());

            return FechaFormateada;
        }
        public string AgregarCero(string p)
        {

            String regreso = p;

            if (p.Length < 2)

                regreso = "0" + p;

            return regreso;
        }

        private void cargar()
        {
            string url = ConfigurationManager.AppSettings["portal"].ToString() + "Sync/documentos/";
            try
            {
                // You need to post the data as key value pairs:
                string postData = "inicio=" + inicio.Value.ToString("yyyy-MM-dd")
                    + "&fin=" + fin.Value.ToString("yyyy-MM-dd")
                    + "&rfc=" + rfc.Text
                    + "&estado=" + estado.Text
                    + "&uuid=" + uuid.Text
                    + "&folio=" + folioFiltro.Text
                    + "&tipoComprobante=" + this.filtroTipo.Text;
                byte[] byteArray = Encoding.UTF8.GetBytes(postData);

                // Post the data to the right place.
                Uri target = new Uri(url);
                WebRequest request = WebRequest.Create(target);

                request.Method = "POST";
                request.ContentType = "application/x-www-form-urlencoded";
                request.ContentLength = byteArray.Length;

                using (var dataStream = request.GetRequestStream())
                {
                    dataStream.Write(byteArray, 0, byteArray.Length);
                }

                using (var response = (HttpWebResponse)request.GetResponse())
                {
                    Console.WriteLine("Error code: {0}", response.StatusCode);
                    using (Stream data = response.GetResponseStream())
                    using (var reader = new StreamReader(data))
                    {
                        string text = reader.ReadToEnd();
                        //DataTable table = (DataTable)JsonConvert.DeserializeObject(text, (typeof(Documento)));

                        DataTable table = JsonConvert.DeserializeObject<DataTable>(text);

                        vaciarDatos(table);
                    }
                }

            }
            catch (Exception error)
            {
                ErrorFX.mostrar(error, true, true);
            }
        }

        private void vaciarDatos(DataTable table)
        {
            try
            {
                dtgrdGeneral.Rows.Clear();
                foreach (DataRow registro in table.Rows)
                {
                    string uuidtr = registro["UUID"].ToString();
                    int n = dtgrdGeneral.Rows.Add();
                    dtgrdGeneral.Rows[n].Cells["tipoComprobante"].Value = registro["tipoComprobante"].ToString();
                    dtgrdGeneral.Rows[n].Cells["CFDI"].Value = uuidtr;
                    dtgrdGeneral.Rows[n].Cells["PDF"].Value = registro["PDF"].ToString();
                    dtgrdGeneral.Rows[n].Cells["XML"].Value = registro["XML"].ToString();
                    dtgrdGeneral.Rows[n].Cells["folio"].Value = registro["folio"].ToString();
                    dtgrdGeneral.Rows[n].Cells["rfcReceptor"].Value = registro["rfcReceptor"].ToString();
                    dtgrdGeneral.Rows[n].Cells["rfcEmisor"].Value = registro["rfcEmisor"].ToString();
                    dtgrdGeneral.Rows[n].Cells["row_id"].Value = registro["id"].ToString();
                    dtgrdGeneral.Rows[n].Cells["fechaFactura"].Value = registro["fechaFactura"].ToString();
                    try
                    {
                        dtgrdGeneral.Rows[n].Cells["OC"].Value = registro["oc"].ToString();
                        dtgrdGeneral.Rows[n].Cells["Recibo"].Value = registro["recibo"].ToString();
                    }
                    catch
                    {

                    }

                }
            }
            catch (Exception error)
            {
                ErrorFX.mostrar(error, true, true);
            }
        }

        private void frmPrincipal_HelpButtonClicked(object sender, CancelEventArgs e)
        {

        }

        private void button1_Click_1(object sender, EventArgs e)
        {
            cargar();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            syncArchivos();
        }
        BackgroundWorker bgwArchivos;
        private void syncArchivos()
        {
            button2.Enabled = false;
            bgwArchivos = new BackgroundWorker();
            bgwArchivos.WorkerSupportsCancellation = true;
            bgwArchivos.WorkerReportsProgress = true;
            bgwArchivos.DoWork += new DoWorkEventHandler(bgwArchivos_DoWork);
            bgwArchivos.ProgressChanged += new ProgressChangedEventHandler(bgwArchivos_ProgressChanged);
            bgwArchivos.RunWorkerCompleted += new RunWorkerCompletedEventHandler(bgwArchivos_RunWorkerCompleted);
            bgwArchivos.RunWorkerAsync();
        }
        private void sincronizarGlobalSearch(DataGridViewRow registro, string archivoXML)
        {

            bool GlobalSearch = false;
            try
            {
                GlobalSearch = bool.Parse(ConfigurationManager.AppSettings["GlobalSearch"].ToString());
            }
            catch
            {

            }
            if (!GlobalSearch)
            {
                return;
            }

            //Subir el archivo
            //http://www.square-9.com/api/?api=REST%20API%20Resources
            globalSearchUpload oglobalSearchUpload = new globalSearchUpload();
            oglobalSearchUpload.subir(archivoXML);
            if (!String.IsNullOrEmpty(oglobalSearchUpload.log))
            {
                asigar_texto_log(oglobalSearchUpload.log);
            }

        }

        public string UTF8toASCII(string text)
        {
            System.Text.Encoding utf8 = System.Text.Encoding.UTF8;
            Byte[] encodedBytes = utf8.GetBytes(text);
            Byte[] convertedBytes =
                    Encoding.Convert(Encoding.UTF8, Encoding.ASCII, encodedBytes);
            System.Text.Encoding ascii = System.Text.Encoding.ASCII;

            return ascii.GetString(convertedBytes);
        }

        private bool sincronizarAP(DataGridViewRow registro, string archivoXML)
        {

            bool resultado = false;
            bool AdminPaq = false;
            try
            {
                AdminPaq = bool.Parse(ConfigurationManager.AppSettings["AdminPaq"].ToString());
            }
            catch
            {

            }
            if (!AdminPaq)
            {
                return true;
            }
            try
            {

                ModeloSincronizacionContainer dbContext = new ModeloSincronizacionContainer();
                //Buscar la configuraci�n del receptor
                string rfcReceptor = registro.Cells["rfcReceptor"].Value.ToString();
                AdminPaqConfig oAdminPaqConfig = new AdminPaqConfig();
                oAdminPaqConfig = (from r in dbContext.AdminPaqConfigSet.Where
                        (a => a.rfc.Contains(rfcReceptor))
                                   select r).FirstOrDefault();

                if (oAdminPaqConfig == null)
                {
                    asigar_registro_datagrid("La empresa " + rfcReceptor + " no esta configurada, configurela en el .config ", registro.Index);
                    return false;
                }
                //Ir al archivo de acceso para crear la CxP
                //Validar si es DB o SQL
                if (oAdminPaqConfig.tipo.Contains("SQL"))
                {
                    resultado = AdminPaqSQLInsert(registro, archivoXML, oAdminPaqConfig);
                }
                else
                {
                    resultado=AdminPaqDBF(registro, archivoXML, oAdminPaqConfig);
                }

            }
            finally
            {

            }
            // Close connection.
            return resultado;


        }
        private bool AdminPaqSQLInsert(DataGridViewRow registro, string archivoXML, AdminPaqConfig oAdminPaqConfig)
        {

            ModeloSincronizacionContainer dbContext = new ModeloSincronizacionContainer();
            //Descargar el CFDI y cargarlo
            Comprobante oComprobante;
            try
            {
                XmlSerializer serializer_CFD_3 = new XmlSerializer(typeof(Comprobante));
                TextReader reader = new StreamReader(archivoXML);

                oComprobante = (Comprobante)serializer_CFD_3.Deserialize(reader);
                if (oComprobante.TipoDeComprobante.Equals("P"))
                {
                    //No guardar
                    return true;
                }
                reader.Dispose();
                reader.Close();
            }
            catch
            {
                return false;
            }
            //Validar registro del Emisor
            AdminPaqConfig oRegistroEmisor = dbContext.AdminPaqConfigSet
                .Where(a => a.rfcProveedor.Contains(oComprobante.Emisor.Rfc)
                & a.rfc.Contains(oComprobante.Receptor.Rfc)).FirstOrDefault();
            if (oRegistroEmisor == null)
            {
                asigar_registro_datagrid("El Emisor " + oComprobante.Emisor.Rfc + " no esta configurado para el RFC Receptor " +
                    @oComprobante.Receptor.Rfc, registro.Index);

                return false;
            }
            if (!String.IsNullOrEmpty(oRegistroEmisor.bdSQL))
            {
                //Generar registro SQL Server
                return AdminPaqSQL(oRegistroEmisor, oComprobante);
            }

            cCONEXCION oData = new cCONEXCION(oAdminPaqConfig.servidor, oAdminPaqConfig.bdSQL, oAdminPaqConfig.usuario, oAdminPaqConfig.password);
            try
            {
                //Documentos de prueba:
                //30451
                //Open connection.
                //oData.CrearConexion();
                //if (oData.oConn.State == ConnectionState.Open)
                //{
                //    using (OleDbCommand command = dbConn.CreateCommand())
                //    {
                //        //Validar RFC del Proveedor
                //        DataTable oDataTableMWG10002 = validarRFCSQL(oComprobante.Emisor.Rfc, dbConn);
                //        if (oDataTableMWG10002 == null)
                //        {
                //            asigar_registro_datagrid("Error no existe el Proveedor: " + oComprobante.Emisor.Rfc, registro.Index);
                //            return false;
                //        }
                //        if (oDataTableMWG10002.Rows.Count == 0)
                //        {
                //            asigar_registro_datagrid("Error no existe el Proveedor: " + oComprobante.Emisor.Rfc, registro.Index);
                //            return false;
                //        }
                //        string folioCFDI = registro.Cells["folio"].Value.ToString().Replace(".", "")
                //            .Replace(" ", "");
                //        folioCFDI = Regex.Replace(folioCFDI, "[^0-9.]", "");
                //        //if (!String.IsNullOrEmpty(oComprobante.Folio))
                //        //{
                //        //    folioCFDI = oComprobante.Folio;
                //        //}
                //        //if (!String.IsNullOrEmpty(oComprobante.Serie))
                //        //{
                //        //    folioCFDI = oComprobante.Serie;
                //        //    if (!String.IsNullOrEmpty(oComprobante.Folio))
                //        //    {
                //        //        folioCFDI = oComprobante.Serie+oComprobante.Folio;
                //        //    }
                //        //}


                //        //Buscar el ultimo consecutivo 
                //        DataTable oDataTableMWG10006 = obtenerMWG10006("Compra", dbConn);
                //        if (oDataTableMWG10006 == null)
                //        {
                //            string mensaje = "Error no documento Compra en MWG10006 " + oComprobante.Emisor.Rfc;
                //            asigar_registro_datagrid(mensaje, registro.Index);
                //            ErrorFX.mostrar(mensaje, false, true, true);
                //            return false;
                //        }
                //        if (oDataTableMWG10006.Rows.Count == 0)
                //        {
                //            string mensaje = "Error no documento Compra en MWG10006 " + oComprobante.Emisor.Rfc;
                //            asigar_registro_datagrid(mensaje, registro.Index);
                //            ErrorFX.mostrar(mensaje, false, true, true);
                //            return false;
                //        }

                //        //Aumentar el folio 10 en 10
                //        int folio = int.Parse(oDataTableMWG10006.Rows[0]["CNOFOLIO"].ToString()) + 1;
                //        string CIDCONCE01 = oDataTableMWG10006.Rows[0]["CIDCONCE01"].ToString();
                //        aumentarFolio(CIDCONCE01, folio, dbConn);

                //        string consecutivo = "1";
                //        consecutivo = consecutivoDocumento(dbConn, CIDCONCE01);
                //        string CFOLIO = Regex.Replace(folioCFDI, "[^0-9.]", "");

                //        //folio.ToString();
                //        string CIDDOCUM01 = oDataTableMWG10006.Rows[0]["CIDDOCUM01"].ToString();
                //        string CIDMONEDA = "1";
                //        string CTIPOCAM01 = "1";

                //        string CIDCLIEN01 = oDataTableMWG10002.Rows[0]["CIDCLIEN01"].ToString();
                //        string CRAZONSO01 = oDataTableMWG10002.Rows[0]["CRAZONSO01"].ToString();

                //        string CRFC = oDataTableMWG10002.Rows[0]["CRFC"].ToString();

                //        string CREFEREN01 = "Folio: " + Regex.Replace(folioCFDI, "[^0-9.]", ""); ;
                //        string COBSERVA01 = "";

                //        string CNATURAL01 = "1";
                //        string CIDDOCUM03 = "0";
                //        string CPLANTILLA = "0";
                //        string CUSACLIE01 = "0";
                //        string CUSAPROV01 = "1";
                //        string CAFECTADO = "1";
                //        string CIMPRESO = "0";
                //        string CCANCELADO = "0";
                //        string CDEVUELTO = "0";
                //        string CIDPREPO01 = "0";
                //        string CIDPREPO02 = "0";
                //        //30

                //        //CESTADOC01

                //        //N

                //        //6

                //        //Estado del documento en el proceso de interfaz contable:

                //        //                            1 = No Contabilizado.

                //        //2 = Pertenece a una Prep�liza de documento.

                //        //3 = Pertenece a una Prep�liza Diaria.

                //        //4 = Pertenece a una Prep�liza por Periodo.

                //        //5 = Pertenece a una P�liza de documento.

                //        //6 = Pertenece a una P�liza Diaria.

                //        //7 = Pertenece a una P�liza por Periodo.

                //        //8 = Pertenece a una P�liza modificada (contabilizada libremente


                //        string CESTADOC01 = "1"; //Rodrigo Escalona cambio a 1 
                //        string CNETO = oComprobante.SubTotal.ToString();
                //        if (oComprobante.DescuentoSpecified)
                //        {
                //            CNETO = (oComprobante.SubTotal - oComprobante.Descuento).ToString();
                //        }


                //        string CIMPUESTO1 = "0";
                //        if (oComprobante.Impuestos != null)
                //        {
                //            CIMPUESTO1 = oComprobante.Impuestos.TotalImpuestosTrasladados.ToString();
                //        }


                //        string CIMPUESTO2 = "0";
                //        string CIMPUESTO3 = "0";
                //        string CRETENCI01 = "0";
                //        string CRETENCI02 = "0";
                //        if (oComprobante.Impuestos != null)
                //        {
                //            if (oComprobante.Impuestos.TotalImpuestosRetenidosSpecified)
                //            {
                //                CRETENCI02 = oComprobante.Impuestos.TotalImpuestosRetenidos.ToString();
                //            }

                //        }

                //        string CDESCUEN01 = "0";
                //        string CDESCUEN02 = "0";
                //        string CDESCUEN03 = "0";
                //        string CGASTO1 = "0";
                //        string CGASTO2 = "0";
                //        string CGASTO3 = "0";
                //        string CTOTAL = oComprobante.Total.ToString();

                //        string CPENDIENTE = CTOTAL;
                //        string CTOTALUN01 = "1";
                //        string CDESCUEN04 = "0";
                //        string CPORCENT01 = "0";
                //        string CPORCENT02 = "0";
                //        string CPORCENT03 = "0";
                //        string CPORCENT04 = "0";
                //        string CPORCENT05 = "0";
                //        string CPORCENT06 = "0";


                //        string CTEXTOEX01 = "";

                //        COBSERVA01 = oComprobante.Conceptos[0].Descripcion
                //            .Replace(Environment.NewLine, " ")
                //            .Replace("\n", " ")
                //            .Replace("/", " ")
                //             .Replace("-", " ")
                //             .Replace("!", " ")
                //             .Replace("#", " ")
                //            .Replace("<", " ")
                //            .Replace(">", " ")
                //            .Replace("(", " ")
                //            .Replace(")", " ")
                //            .Replace(".", "")
                //            .Replace(",", "")
                //            .Replace("�", "i")
                //            .Replace("�", "a")
                //            .Replace("�", "i")
                //            .Replace("�", "o")
                //            .Replace("�", "u")
                //            .Replace("�", "e")
                //            .Replace("�", "I")
                //            .Replace("�", "A")
                //            .Replace("�", "O")
                //            .Replace("�", "U")
                //            .Replace("�", "E")
                //            .Replace("'", "");
                //        COBSERVA01 = UTF8toASCII(COBSERVA01);

                //        if (COBSERVA01.Length >= 200)
                //        {
                //            COBSERVA01 = COBSERVA01.Substring(0, 199);

                //        }
                //        CTEXTOEX01 = COBSERVA01;

                //        string CTEXTOEX02 = "";//registro.Cells["oc"].Value.ToString();
                //        string CTEXTOEX03 = "";
                //        string CFECHAEX01 = DateTime.Now.ToShortDateString();

                //        string CIMPORTE01 = oComprobante.Total.ToString();


                //        string CIMPORTE02 = "0";
                //        string CIMPORTE03 = "0";
                //        string CIMPORTE04 = "0";

                //        string CDESTINA01 = "";
                //        string CNUMEROG01 = "";
                //        string CMENSAJE01 = "";
                //        string CCUENTAM01 = "";
                //        string CNUMEROC01 = "0";
                //        string CPESO = "0";
                //        string CBANOBSE01 = "0";
                //        string CBANDATO01 = "0";
                //        string CBANCOND01 = "0";
                //        string CBANGASTOS = "0";
                //        string CUNIDADE01 = "0";
                //        string CTIMESTAMP = "";
                //        string CIMPCHEQ01 = "0";


                //        string CSISTORIG = "0";
                //        string CIDMONEDCA = "0";
                //        string CTIPOCAMCA = "0";
                //        string CESCFD = "0";
                //        string CTIENECFD = "0";
                //        string CLUGAREXPE = "";
                //        string CMETODOPAG = "";
                //        string CNUMPARCIA = "0";
                //        string CCANTPARCI = "0";
                //        string CCONDIPAGO = "";
                //        string CNUMCTAPAG = "";
                //        string CIDPROYE01 = "0";
                //        string CIDCUENTA = "0";
                //        string CSERIEDO01 = "";
                //        if (oComprobante.Serie != null)
                //        {
                //            CSERIEDO01 = oComprobante.Serie
                //                .Replace(".", "")
                //                .Replace(" ", "")
                //                .Replace("-", "");
                //        }


                //        //Reindexar
                //        ////OleDbConnection con = new OleDbConnection(ConectarBD.cadenaConexionDBF2);
                //        ////OleDbCommand cmd1 = new OleDbCommand("USE CCREDE1.DBF INDEX CCREDE1.CDX ", con);

                //        ////OleDbCommand cmd2 = new OleDbCommand("INSERT INTO CCREDE1 " +
                //        ////"values ('1',123);

                //        ////con.Open();

                //        ////Console.Write(cmd2);
                //        ////cmd1.ExecuteNonQuery();
                //        ////cmd2.ExecuteNonQuery();


                //        ////con.Close();
                //        ////con.Dispose();

                //        //DateTime.Now.ToString("yyyy").Substring(2, 2);
                //        string sSQL = "";                              //Buscar consecutivo y aumentar
                //        try
                //        {
                //            dbConn = new OleDbConnection(conexion);
                //            //Documentos de prueba:
                //            //30451
                //            //Open connection.
                //            dbConn.Open();
                //            if (dbConn.State == ConnectionState.Open)
                //            {
                //                string fechaCompromisor = "{'" + oComprobante.Fecha.ToString("yyyy-MM-dd") + @"'}";
                //                fechaCompromisor = "DATE(" + oComprobante.Fecha.ToString("yyyy") + ", " + oComprobante.Fecha.ToString("MM") + ", " + oComprobante.Fecha.ToString("dd") + ")";
                //                //fechaCompromisor = "'" + oComprobante.Fecha.ToString("yyyy-MM-dd") +"'";

                //                string fehcaHoy = "{'" + DateTime.Now.ToString("yyyy-MM-dd") + @"'}"; // "DATE(" + DateTime.Now.ToString("yyyy") + ", " + DateTime.Now.ToString("MM") + ", " + DateTime.Now.ToString("dd") + ")";
                //                                                                                      //,{'" + DateTime.Now.ToString("yyyy-MM-dd") + @"'},{'" + DateTime.Now.ToString("yyyy-MM-dd") + @"'},{'" + DateTime.Now.ToString("yyyy-MM-dd") + @"'},{'" + DateTime.Now.ToString("yyyy-MM-dd") + @"'}
                //                                                                                      //{ '" + CFECHAEX01 + @"'}
                //                fehcaHoy = "DATE(" + DateTime.Now.ToString("yyyy") + ", " + DateTime.Now.ToString("MM") + ", " + DateTime.Now.ToString("dd") + ")";
                //                //fehcaHoy = "'" + DateTime.Now.ToString("yyyy-MM-dd") + @"'";
                //                //Insertar cabecera
                //                sSQL = @"
                //            INSERT INTO MGW10008.DBF
                //            (
                //                CIDDOCUM01,CIDDOCUM02,CIDCONCE01
                //                ,CSERIEDO01,CFOLIO,CFECHA
                //                ,CIDCLIEN01,CRAZONSO01,CRFC
                //                ,CIDAGENTE
                //                ,CFECHAVE01,CFECHAPR01,CFECHAEN01,CFECHAUL01
                //                ,CIDMONEDA,CTIPOCAM01
                //                ,CREFEREN01,COBSERVA01
                //                ,CNATURAL01,CIDDOCUM03,CPLANTILLA,CUSACLIE01
                //                ,CUSAPROV01,CAFECTADO ,CIMPRESO,CCANCELADO ,CDEVUELTO ,CIDPREPO01,CIDPREPO02,CESTADOC01
                //                ,CNETO ,CIMPUESTO1,CIMPUESTO2,CIMPUESTO3,CRETENCI01,CRETENCI02,CDESCUEN01,CDESCUEN02,CDESCUEN03,CGASTO1,CGASTO2,CGASTO3,CTOTAL
                //                ,CPENDIENTE,CTOTALUN01,CDESCUEN04,CPORCENT01,CPORCENT02,CPORCENT03,CPORCENT04,CPORCENT05,CPORCENT06
                //                ,CTEXTOEX01,CTEXTOEX02,CTEXTOEX03 ,CFECHAEX01,CIMPORTE01,CIMPORTE02 ,CIMPORTE03,CIMPORTE04
                //                ,CDESTINA01,CNUMEROG01,CMENSAJE01,CCUENTAM01,CNUMEROC01,CPESO
                //                ,CBANOBSE01,CBANDATO01,CBANCOND01,CBANGASTOS,CUNIDADE01,CTIMESTAMP,CIMPCHEQ01
                //                ,CSISTORIG,CIDMONEDCA,CTIPOCAMCA,CESCFD,CTIENECFD,CLUGAREXPE,CMETODOPAG,CNUMPARCIA,CCANTPARCI,CCONDIPAGO,CNUMCTAPAG,CIDPROYE01,CIDCUENTA
                //            ) 
                //            VALUES
                //            (
                //                " + consecutivo + @"," + CIDDOCUM01.Trim() + "," + CIDCONCE01.Trim() + @"
                //                ,'" + CSERIEDO01.Trim().Trim() + "'," + CFOLIO.Trim() + ", " + fechaCompromisor + @"
                //                ," + CIDCLIEN01.Trim() + @",'" + CRAZONSO01.Trim() + @"','" + CRFC.Trim() + @"'
                //                ,0
                //                , " + fehcaHoy + @"," + fehcaHoy + @"," + fehcaHoy + @"," + fehcaHoy + @"
                //                ," + CIDMONEDA.Trim() + @"," + CTIPOCAM01.Trim() + @"
                //                ,'" + CREFEREN01.Trim() + @"','" + COBSERVA01.Trim() + @"'
                //                ," + CNATURAL01.Trim() + @"," + CIDDOCUM03.Trim() + @"," + CPLANTILLA.Trim() + @"," + CUSACLIE01.Trim() + @"
                //                ," + CUSAPROV01 + @"," + CAFECTADO + @"," + CIMPRESO.Trim() + @"," + CCANCELADO.Trim() + @"," + CDEVUELTO.Trim() + @"," + CIDPREPO01 + @"," + CIDPREPO02 + @"," + CESTADOC01 + @"
                //                ," + CNETO + @"," + CIMPUESTO1.Trim() + @"," + CIMPUESTO2.Trim() + @"," + CIMPUESTO3 + @"," + CRETENCI01 + @"," + CRETENCI02 + @"," + CDESCUEN01 + @"," + CDESCUEN02 + @"," + CDESCUEN03 + @"," + CGASTO1 + @"," + CGASTO2 + @"," + CGASTO3 + @"," + CTOTAL + @"
                //                ," + CPENDIENTE.Trim() + @"," + CTOTALUN01.Trim() + @"," + CDESCUEN04.Trim() + @"," + CPORCENT01 + @"," + CPORCENT02 + @"," + CPORCENT03 + @"," + CPORCENT04 + @"," + CPORCENT05 + @"," + CPORCENT06 + @"
                //                ,'" + CTEXTOEX01.Trim() + @"','" + CTEXTOEX02.Trim() + @"','" + CTEXTOEX03 + @"'," + fehcaHoy + "," + CIMPORTE01 + @"," + CIMPORTE02 + @"," + CIMPORTE03 + @"," + CIMPORTE04 + @"
                //                ,'" + CDESTINA01.Trim() + @"','" + CNUMEROG01.Trim() + @"','" + CMENSAJE01 + @"','" + CCUENTAM01 + @"'," + CNUMEROC01 + @"," + CPESO + @"
                //                ," + CBANOBSE01.Trim() + @"," + CBANDATO01.Trim() + @"," + CBANCOND01 + @"," + CBANGASTOS + @"," + CUNIDADE01 + @",'" + CTIMESTAMP + @"'," + CIMPCHEQ01 + @"
                //                ," + CSISTORIG.Trim() + @"," + CIDMONEDCA.Trim() + @"," + CTIPOCAMCA + @"," + CESCFD + @"," + CTIENECFD + @",'" + CLUGAREXPE + @"','" + CMETODOPAG + @"'," + CNUMPARCIA + @"," + CCANTPARCI + @",'" + CCONDIPAGO + @"','" + CNUMCTAPAG + @"'," + CIDPROYE01 + @"," + CIDCUENTA + @"
                //            ) ";

                //                var insertCommand = new OleDbCommand(sSQL, dbConn);
                //                Int32 rowsAffected = insertCommand.ExecuteNonQuery();
                //            }
                //        }
                //        catch (OleDbException e)
                //        {
                //            string mensaje = "ucCxP - 484 Insertando en MGW10008.DBF " + sSQL;
                //            asigar_registro_datagrid(mensaje, registro.Index);
                //            ErrorFX.mostrar(e, false, true, mensaje, true);
                //            return false;
                //        }
                //        catch (Exception e)
                //        {
                //            string mensaje = "ucCxP - 580 Insertando en MGW10008.DBF Logintud " + sSQL;
                //            asigar_registro_datagrid(mensaje, registro.Index);
                //            ErrorFX.mostrar(e, false, true, mensaje, true);
                //            return false;
                //        }
                //        finally
                //        {
                //            // Close connection.
                //            dbConn.Close();
                //        }
                //        //MGW10010
                //        try
                //        {
                //            dbConn = new OleDbConnection(conexion);
                //            //Documentos de prueba:
                //            //30451
                //            //Open connection.
                //            dbConn.Open();
                //            if (dbConn.State == ConnectionState.Open)
                //            {
                //                string CIDMOVIM01 = consecutivoLinea(dbConn);
                //                CIDDOCUM01 = consecutivo;
                //                string CNUMEROM01 = "100";
                //                string CIDDOCUM02 = "19";
                //                string CIDPRODU01 = cargarProducto(dbConn, oRegistroEmisor.codigo);//"22";
                //                string CIDALMACEN = "1";
                //                string CUNIDADES = "1";
                //                string CUNIDADE02 = "1";
                //                string CIDUNIDAD = "0";
                //                string CIDUNIDA01 = "0";
                //                string CPRECIO = CTOTAL;
                //                string CPRECIOC01 = CTOTAL;
                //                string CCOSTOCA01 = "0";
                //                string CCOSTOES01 = "0";
                //                string CPORCENT07 = "0";
                //                string CPORCENT08 = "0";
                //                string CPORCENT09 = "0";
                //                string CDESCUEN05 = "0";
                //                string CPORCENT10 = "0";
                //                string CPORCENT11 = "0";
                //                string CAFECTAE01 = "1";
                //                string CAFECTAD01 = "1";
                //                string CAFECTAD02 = "1";
                //                string CMOVTOOC01 = "0";
                //                string CIDMOVTO01 = "0";
                //                string CIDMOVTO02 = "0";
                //                string CUNIDADE03 = "1";
                //                string CUNIDADE04 = "0";
                //                string CUNIDADE05 = "0";
                //                string CUNIDADE06 = "0";
                //                string CTIPOTRA01 = "1";
                //                string CIDVALOR01 = "0";
                //                //Validar impuestos
                //                CPORCENT01 = "16";

                //                string CGTOMOVTO = "0";
                //                string CSCMOVTO = "0";
                //                string CCOMVENTA = "0";
                //                string CIDMOVDEST = "0";
                //                string CNUMCONSOL = "0";

                //                sSQL = @"
                //            INSERT INTO MGW10010.DBF
                //            (
                //                 CIDMOVIM01
                //                ,CIDDOCUM01
                //                ,CNUMEROM01
                //                ,CIDDOCUM02
                //                ,CIDPRODU01 
                //                ,CIDALMACEN
                //                ,CUNIDADES
                //                ,CUNIDADE01 
                //                ,CUNIDADE02
                //                ,CIDUNIDAD 
                //                ,CIDUNIDA01
                //                ,CPRECIO
                //                ,CPRECIOC01
                //                ,CCOSTOCA01 
                //                ,CCOSTOES01
                //                ,CNETO
                //                ,CIMPUESTO1,CPORCENT01
                //                ,CIMPUESTO2,CPORCENT02
                //                ,CIMPUESTO3,CPORCENT03
                //                ,CRETENCI01,CPORCENT04
                //                ,CRETENCI02,CPORCENT05
                //                ,CDESCUEN01,CPORCENT06
                //                ,CDESCUEN02,CPORCENT07
                //                ,CDESCUEN03,CPORCENT08
                //                ,CDESCUEN04,CPORCENT09
                //                ,CDESCUEN05,CPORCENT10
                //                ,CTOTAL
                //                ,CPORCENT11
                //                ,CREFEREN01
                //                ,COBSERVA01
                //                ,CAFECTAE01,CAFECTAD01,CAFECTAD02
                //                ,CFECHA
                //                ,CMOVTOOC01,CIDMOVTO01,CIDMOVTO02
                //                ,CUNIDADE03,CUNIDADE04,CUNIDADE05,CUNIDADE06
                //                ,CTIPOTRA01,CIDVALOR01
                //                ,CTEXTOEX01,CTEXTOEX02,CTEXTOEX03
                //                ,CFECHAEX01
                //                ,CIMPORTE01,CIMPORTE02,CIMPORTE03,CIMPORTE04
                //                ,CTIMESTAMP
                //                ,CGTOMOVTO,CSCMOVTO,CCOMVENTA
                //                ,CIDMOVDEST,CNUMCONSOL
                //            )
                //            VALUES
                //            (
                //            " + CIDMOVIM01 + @"
                //            ," + CIDDOCUM01 + @"
                //            ," + CNUMEROM01 + @"
                //            ," + CIDDOCUM02 + @"
                //            ," + CIDPRODU01 + @"
                //            ," + CIDALMACEN + @"
                //            ," + CUNIDADES + @"
                //            ," + CUNIDADE01 + @"
                //            ," + CUNIDADE02 + @"
                //            ," + CIDUNIDAD + @"
                //            ," + CIDUNIDA01 + @"
                //            ," + CPRECIO + @"
                //            ," + CPRECIOC01 + @"
                //            ," + CCOSTOCA01 + @"
                //            ," + CCOSTOES01 + @"
                //            ," + CNETO + @"
                //            ," + CIMPUESTO1 + @"
                //            ," + CPORCENT01 + @"
                //            ," + CIMPUESTO2 + @"
                //            ," + CPORCENT02 + @"
                //            ," + CIMPUESTO3 + @"
                //            ," + CPORCENT03 + @"
                //            ," + CRETENCI01 + @"
                //            ," + CPORCENT04 + @"
                //            ," + CRETENCI02 + @"
                //            ," + CPORCENT05 + @"
                //            ," + CDESCUEN01 + @"
                //            ," + CPORCENT06 + @"
                //            ," + CDESCUEN02 + @"
                //            ," + CPORCENT07 + @"
                //            ," + CDESCUEN03 + @"
                //            ," + CPORCENT08 + @"
                //            ," + CDESCUEN04 + @"
                //            ," + CPORCENT09 + @"
                //            ," + CDESCUEN05 + @"
                //            ," + CPORCENT10 + @"
                //            ," + CTOTAL + @"
                //            ," + CPORCENT11 + @"
                //            ,'" + CREFEREN01 + @"','" + COBSERVA01 + @"'
                //            ," + CAFECTAE01 + @"
                //            ," + CAFECTAD01 + @"
                //            ," + CAFECTAD02 + @"
                //            ,{^" + oComprobante.Fecha.ToString("yyyy/MM/dd") + @"}
                //            ," + CMOVTOOC01 + @"
                //            ," + CIDMOVTO01 + @"
                //            ," + CIDMOVTO02 + @"
                //            ," + CUNIDADE03 + @"," + CUNIDADE04 + @"," + CUNIDADE05 + @"," + CUNIDADE06 + @"
                //            ," + CTIPOTRA01 + @"," + CIDVALOR01 + @"
                //            ,'" + CTEXTOEX01 + @"','" + CTEXTOEX02 + @"','" + CTEXTOEX03 + @"',{" + CFECHAEX01 + @"}
                //            ," + CIMPORTE01 + @"," + CIMPORTE02 + @"," + CIMPORTE03 + @"," + CIMPORTE04 + @"
                //            ,'" + CTIMESTAMP + @"'
                //            ," + CGTOMOVTO + @"
                //            ,'" + CSCMOVTO + @"'
                //            ," + CCOMVENTA + @"
                //            ," + CIDMOVDEST + @"
                //            ," + CNUMCONSOL + @")
                //            ";
                //                OleDbCommand insertCommand = new OleDbCommand(sSQL, dbConn);
                //                Int32 rowsAffectedLine = insertCommand.ExecuteNonQuery();
                //            }
                //            //Insertar la linea de cargo
                //            //proxima.Text = "Ingrese " + consecutivo;


                //            //asigar_registro_datagrid("La empresa " + rfcReceptor + " no esta configurada", registro.Index);
                //            //dtgrdGeneral.Rows[registro.Index].Cells["payableId"].Value = CFOLIO;

                //            //Poner Generada
                //            registrarFacturaPortal(CIDDOCUM01, folioCFDI, oComprobante.Fecha.ToString("yyyy-MM-dd")
                //            , "Registrada", oComprobante.Emisor.Rfc, "");

                //        }
                //        catch (OleDbException e)
                //        {
                //            string mensaje = "ucCxP - 650 MGW10010.DBF " + sSQL;
                //            asigar_registro_datagrid(mensaje, registro.Index);
                //            ErrorFX.mostrar(e, false, true, mensaje, true);
                //            return false;

                //        }
                //        catch (Exception e)
                //        {
                //            string mensaje = "ucCxP - 763 Insertando en MGW10010.DBF Logintud " + sSQL;
                //            asigar_registro_datagrid(mensaje, registro.Index);
                //            ErrorFX.mostrar(e, false, true, mensaje, true);
                //            return false;
                //        }
                //        finally
                //        {
                //            // Close connection.
                //            dbConn.Close();
                //        }
                //    }
                //}
                //dbContext.Dispose();
                return true;
            }
            catch (Exception error)
            {
                ErrorFX.mostrar(error, true, true, "AdminSQL - 945 ");
            }


            return false;

        }


        private bool AdminPaqDBF(DataGridViewRow registro, string archivoXML, AdminPaqConfig oAdminPaqConfig)
        {

            ModeloSincronizacionContainer dbContext = new ModeloSincronizacionContainer();
            //Descargar el CFDI y cargarlo
            Comprobante oComprobante;
            try
            {
                XmlSerializer serializer_CFD_3 = new XmlSerializer(typeof(Comprobante));
                TextReader reader = new StreamReader(archivoXML);

                oComprobante = (Comprobante)serializer_CFD_3.Deserialize(reader);
                if (oComprobante.TipoDeComprobante.Equals("P"))
                {
                    //No guardar
                    return true;
                }
                reader.Dispose();
                reader.Close();
            }
            catch
            {
                return false;
            }
            //Validar registro del Emisor
            AdminPaqConfig oRegistroEmisor = dbContext.AdminPaqConfigSet
                .Where(a => a.rfcProveedor.Contains(oComprobante.Emisor.Rfc)
                & a.rfc.Contains(oComprobante.Receptor.Rfc)).FirstOrDefault();
            if (oRegistroEmisor == null)
            {
                asigar_registro_datagrid("El Emisor " + oComprobante.Emisor.Rfc + " no esta configurado para el RFC Receptor " +
                    @oComprobante.Receptor.Rfc, registro.Index);

                return false;
            }
            if (!String.IsNullOrEmpty(oRegistroEmisor.bdSQL))
            {
                //Generar registro SQL Server
                return AdminPaqSQL(oRegistroEmisor, oComprobante);
            }

            OleDbConnection dbConn;
            try
            {

                string conexion = @"Provider=vfpoledb.1;Data Source=" + oAdminPaqConfig.directorio + ";Collating Sequence=general;";
                conexion = @"Provider=vfpoledb.1;Data Source=" + oAdminPaqConfig.directorio + ";Collating Sequence=machine;Mode=ReadWrite;";

                dbConn = new OleDbConnection(conexion);
                //Documentos de prueba:
                //30451
                //Open connection.
                dbConn.Open();
                if (dbConn.State == ConnectionState.Open)
                {
                    using (OleDbCommand command = dbConn.CreateCommand())
                    {
                        //Validar RFC del Proveedor
                        DataTable oDataTableMWG10002 = validarRFC(oComprobante.Emisor.Rfc, dbConn);
                        if (oDataTableMWG10002 == null)
                        {
                            asigar_registro_datagrid("Error no existe el Proveedor: " + oComprobante.Emisor.Rfc, registro.Index);
                            return false;
                        }
                        if (oDataTableMWG10002.Rows.Count == 0)
                        {
                            asigar_registro_datagrid("Error no existe el Proveedor: " + oComprobante.Emisor.Rfc, registro.Index);
                            return false;
                        }
                        string folioCFDI = registro.Cells["folio"].Value.ToString().Replace(".", "")
                            .Replace(" ", "");
                        folioCFDI = Regex.Replace(folioCFDI, "[^0-9.]", "");
                        //if (!String.IsNullOrEmpty(oComprobante.Folio))
                        //{
                        //    folioCFDI = oComprobante.Folio;
                        //}
                        //if (!String.IsNullOrEmpty(oComprobante.Serie))
                        //{
                        //    folioCFDI = oComprobante.Serie;
                        //    if (!String.IsNullOrEmpty(oComprobante.Folio))
                        //    {
                        //        folioCFDI = oComprobante.Serie+oComprobante.Folio;
                        //    }
                        //}


                        //Buscar el ultimo consecutivo 
                        DataTable oDataTableMWG10006 = obtenerMWG10006("Compra", dbConn);
                        if (oDataTableMWG10006 == null)
                        {
                            string mensaje = "Error no documento Compra en MWG10006 " + oComprobante.Emisor.Rfc;
                            asigar_registro_datagrid(mensaje, registro.Index);
                            ErrorFX.mostrar(mensaje, false, true, true);
                            return false;
                        }
                        if (oDataTableMWG10006.Rows.Count == 0)
                        {
                            string mensaje = "Error no documento Compra en MWG10006 " + oComprobante.Emisor.Rfc;
                            asigar_registro_datagrid(mensaje, registro.Index);
                            ErrorFX.mostrar(mensaje, false, true, true);
                            return false;
                        }

                        //Aumentar el folio 10 en 10
                        int folio = int.Parse(oDataTableMWG10006.Rows[0]["CNOFOLIO"].ToString()) + 1;
                        string CIDCONCE01 = oDataTableMWG10006.Rows[0]["CIDCONCE01"].ToString();
                        aumentarFolio(CIDCONCE01, folio, dbConn);

                        string consecutivo = "1";
                        consecutivo = consecutivoDocumento(dbConn, CIDCONCE01);
                        string CFOLIO = Regex.Replace(folioCFDI, "[^0-9.]", "");

                        //folio.ToString();
                        string CIDDOCUM01 = oDataTableMWG10006.Rows[0]["CIDDOCUM01"].ToString();
                        string CIDMONEDA = "1";
                        string CTIPOCAM01 = "1";

                        string CIDCLIEN01 = oDataTableMWG10002.Rows[0]["CIDCLIEN01"].ToString();
                        string CRAZONSO01 = oDataTableMWG10002.Rows[0]["CRAZONSO01"].ToString();

                        string CRFC = oDataTableMWG10002.Rows[0]["CRFC"].ToString();

                        string CREFEREN01 = "Folio: " + Regex.Replace(folioCFDI, "[^0-9.]", ""); ;
                        string COBSERVA01 = "";

                        string CNATURAL01 = "1";
                        string CIDDOCUM03 = "0";
                        string CPLANTILLA = "0";
                        string CUSACLIE01 = "0";
                        string CUSAPROV01 = "1";
                        string CAFECTADO = "1";
                        string CIMPRESO = "0";
                        string CCANCELADO = "0";
                        string CDEVUELTO = "0";
                        string CIDPREPO01 = "0";
                        string CIDPREPO02 = "0";
                        //30

                        //CESTADOC01

                        //N

                        //6

                        //Estado del documento en el proceso de interfaz contable:

                        //                            1 = No Contabilizado.

                        //2 = Pertenece a una Prep�liza de documento.

                        //3 = Pertenece a una Prep�liza Diaria.

                        //4 = Pertenece a una Prep�liza por Periodo.

                        //5 = Pertenece a una P�liza de documento.

                        //6 = Pertenece a una P�liza Diaria.

                        //7 = Pertenece a una P�liza por Periodo.

                        //8 = Pertenece a una P�liza modificada (contabilizada libremente


                        string CESTADOC01 = "1"; //Rodrigo Escalona cambio a 1 
                        string CNETO = oComprobante.SubTotal.ToString();
                        if (oComprobante.DescuentoSpecified)
                        {
                            CNETO = (oComprobante.SubTotal - oComprobante.Descuento).ToString();
                        }


                        string CIMPUESTO1 = "0";
                        if (oComprobante.Impuestos != null)
                        {
                            CIMPUESTO1 = oComprobante.Impuestos.TotalImpuestosTrasladados.ToString();
                        }


                        string CIMPUESTO2 = "0";
                        string CIMPUESTO3 = "0";
                        string CRETENCI01 = "0";
                        string CRETENCI02 = "0";
                        if (oComprobante.Impuestos != null)
                        {
                            if (oComprobante.Impuestos.TotalImpuestosRetenidosSpecified)
                            {
                                CRETENCI02 = oComprobante.Impuestos.TotalImpuestosRetenidos.ToString();
                            }

                        }

                        string CDESCUEN01 = "0";
                        string CDESCUEN02 = "0";
                        string CDESCUEN03 = "0";
                        string CGASTO1 = "0";
                        string CGASTO2 = "0";
                        string CGASTO3 = "0";
                        string CTOTAL = oComprobante.Total.ToString();

                        string CPENDIENTE = CTOTAL;
                        string CTOTALUN01 = "1";
                        string CDESCUEN04 = "0";
                        string CPORCENT01 = "0";
                        string CPORCENT02 = "0";
                        string CPORCENT03 = "0";
                        string CPORCENT04 = "0";
                        string CPORCENT05 = "0";
                        string CPORCENT06 = "0";


                        string CTEXTOEX01 = "";

                        COBSERVA01 = oComprobante.Conceptos[0].Descripcion
                            .Replace(Environment.NewLine, " ")
                            .Replace("\n", " ")
                            .Replace("/", " ")
                             .Replace("-", " ")
                             .Replace("!", " ")
                             .Replace("#", " ")
                            .Replace("<", " ")
                            .Replace(">", " ")
                            .Replace("(", " ")
                            .Replace(")", " ")
                            .Replace(".", "")
                            .Replace(",", "")
                            .Replace("�", "i")
                            .Replace("�", "a")
                            .Replace("�", "i")
                            .Replace("�", "o")
                            .Replace("�", "u")
                            .Replace("�", "e")
                            .Replace("�", "I")
                            .Replace("�", "A")
                            .Replace("�", "O")
                            .Replace("�", "U")
                            .Replace("�", "E")
                            .Replace("'", "");
                        COBSERVA01 = UTF8toASCII(COBSERVA01);

                        if (COBSERVA01.Length >= 200)
                        {
                            COBSERVA01 = COBSERVA01.Substring(0, 199);

                        }
                        CTEXTOEX01 = COBSERVA01;

                        string CTEXTOEX02 = "";//registro.Cells["oc"].Value.ToString();
                        string CTEXTOEX03 = "";
                        string CFECHAEX01 = DateTime.Now.ToShortDateString();

                        string CIMPORTE01 = oComprobante.Total.ToString();


                        string CIMPORTE02 = "0";
                        string CIMPORTE03 = "0";
                        string CIMPORTE04 = "0";

                        string CDESTINA01 = "";
                        string CNUMEROG01 = "";
                        string CMENSAJE01 = "";
                        string CCUENTAM01 = "";
                        string CNUMEROC01 = "0";
                        string CPESO = "0";
                        string CBANOBSE01 = "0";
                        string CBANDATO01 = "0";
                        string CBANCOND01 = "0";
                        string CBANGASTOS = "0";
                        string CUNIDADE01 = "0";
                        string CTIMESTAMP = "";
                        string CIMPCHEQ01 = "0";


                        string CSISTORIG = "0";
                        string CIDMONEDCA = "0";
                        string CTIPOCAMCA = "0";
                        string CESCFD = "0";
                        string CTIENECFD = "0";
                        string CLUGAREXPE = "";
                        string CMETODOPAG = "";
                        string CNUMPARCIA = "0";
                        string CCANTPARCI = "0";
                        string CCONDIPAGO = "";
                        string CNUMCTAPAG = "";
                        string CIDPROYE01 = "0";
                        string CIDCUENTA = "0";
                        string CSERIEDO01 = "";
                        if (oComprobante.Serie != null)
                        {
                            CSERIEDO01 = oComprobante.Serie
                                .Replace(".", "")
                                .Replace(" ", "")
                                .Replace("-", "");
                        }


                        //Reindexar
                        ////OleDbConnection con = new OleDbConnection(ConectarBD.cadenaConexionDBF2);
                        ////OleDbCommand cmd1 = new OleDbCommand("USE CCREDE1.DBF INDEX CCREDE1.CDX ", con);

                        ////OleDbCommand cmd2 = new OleDbCommand("INSERT INTO CCREDE1 " +
                        ////"values ('1',123);

                        ////con.Open();

                        ////Console.Write(cmd2);
                        ////cmd1.ExecuteNonQuery();
                        ////cmd2.ExecuteNonQuery();


                        ////con.Close();
                        ////con.Dispose();

                        //DateTime.Now.ToString("yyyy").Substring(2, 2);
                        string sSQL = "";                              //Buscar consecutivo y aumentar
                        try
                        {
                            dbConn = new OleDbConnection(conexion);
                            //Documentos de prueba:
                            //30451
                            //Open connection.
                            dbConn.Open();
                            if (dbConn.State == ConnectionState.Open)
                            {
                                string fechaCompromisor = "{'" + oComprobante.Fecha.ToString("yyyy-MM-dd") + @"'}";
                                fechaCompromisor = "DATE(" + oComprobante.Fecha.ToString("yyyy") + ", " + oComprobante.Fecha.ToString("MM") + ", " + oComprobante.Fecha.ToString("dd") + ")";
                                //fechaCompromisor = "'" + oComprobante.Fecha.ToString("yyyy-MM-dd") +"'";

                                string fehcaHoy = "{'" + DateTime.Now.ToString("yyyy-MM-dd") + @"'}"; // "DATE(" + DateTime.Now.ToString("yyyy") + ", " + DateTime.Now.ToString("MM") + ", " + DateTime.Now.ToString("dd") + ")";
                                                                                                      //,{'" + DateTime.Now.ToString("yyyy-MM-dd") + @"'},{'" + DateTime.Now.ToString("yyyy-MM-dd") + @"'},{'" + DateTime.Now.ToString("yyyy-MM-dd") + @"'},{'" + DateTime.Now.ToString("yyyy-MM-dd") + @"'}
                                                                                                      //{ '" + CFECHAEX01 + @"'}
                                fehcaHoy = "DATE(" + DateTime.Now.ToString("yyyy") + ", " + DateTime.Now.ToString("MM") + ", " + DateTime.Now.ToString("dd") + ")";
                                //fehcaHoy = "'" + DateTime.Now.ToString("yyyy-MM-dd") + @"'";
                                //Insertar cabecera
                                sSQL = @"
                            INSERT INTO MGW10008.DBF
                            (
                                CIDDOCUM01,CIDDOCUM02,CIDCONCE01
                                ,CSERIEDO01,CFOLIO,CFECHA
                                ,CIDCLIEN01,CRAZONSO01,CRFC
                                ,CIDAGENTE
                                ,CFECHAVE01,CFECHAPR01,CFECHAEN01,CFECHAUL01
                                ,CIDMONEDA,CTIPOCAM01
                                ,CREFEREN01,COBSERVA01
                                ,CNATURAL01,CIDDOCUM03,CPLANTILLA,CUSACLIE01
                                ,CUSAPROV01,CAFECTADO ,CIMPRESO,CCANCELADO ,CDEVUELTO ,CIDPREPO01,CIDPREPO02,CESTADOC01
                                ,CNETO ,CIMPUESTO1,CIMPUESTO2,CIMPUESTO3,CRETENCI01,CRETENCI02,CDESCUEN01,CDESCUEN02,CDESCUEN03,CGASTO1,CGASTO2,CGASTO3,CTOTAL
                                ,CPENDIENTE,CTOTALUN01,CDESCUEN04,CPORCENT01,CPORCENT02,CPORCENT03,CPORCENT04,CPORCENT05,CPORCENT06
                                ,CTEXTOEX01,CTEXTOEX02,CTEXTOEX03 ,CFECHAEX01,CIMPORTE01,CIMPORTE02 ,CIMPORTE03,CIMPORTE04
                                ,CDESTINA01,CNUMEROG01,CMENSAJE01,CCUENTAM01,CNUMEROC01,CPESO
                                ,CBANOBSE01,CBANDATO01,CBANCOND01,CBANGASTOS,CUNIDADE01,CTIMESTAMP,CIMPCHEQ01
                                ,CSISTORIG,CIDMONEDCA,CTIPOCAMCA,CESCFD,CTIENECFD,CLUGAREXPE,CMETODOPAG,CNUMPARCIA,CCANTPARCI,CCONDIPAGO,CNUMCTAPAG,CIDPROYE01,CIDCUENTA
                            ) 
                            VALUES
                            (
                                " + consecutivo + @"," + CIDDOCUM01.Trim() + "," + CIDCONCE01.Trim() + @"
                                ,'" + CSERIEDO01.Trim().Trim() + "'," + CFOLIO.Trim() + ", " + fechaCompromisor + @"
                                ," + CIDCLIEN01.Trim() + @",'" + CRAZONSO01.Trim() + @"','" + CRFC.Trim() + @"'
                                ,0
                                , " + fehcaHoy + @"," + fehcaHoy + @"," + fehcaHoy + @"," + fehcaHoy + @"
                                ," + CIDMONEDA.Trim() + @"," + CTIPOCAM01.Trim() + @"
                                ,'" + CREFEREN01.Trim() + @"','" + COBSERVA01.Trim() + @"'
                                ," + CNATURAL01.Trim() + @"," + CIDDOCUM03.Trim() + @"," + CPLANTILLA.Trim() + @"," + CUSACLIE01.Trim() + @"
                                ," + CUSAPROV01 + @"," + CAFECTADO + @"," + CIMPRESO.Trim() + @"," + CCANCELADO.Trim() + @"," + CDEVUELTO.Trim() + @"," + CIDPREPO01 + @"," + CIDPREPO02 + @"," + CESTADOC01 + @"
                                ," + CNETO + @"," + CIMPUESTO1.Trim() + @"," + CIMPUESTO2.Trim() + @"," + CIMPUESTO3 + @"," + CRETENCI01 + @"," + CRETENCI02 + @"," + CDESCUEN01 + @"," + CDESCUEN02 + @"," + CDESCUEN03 + @"," + CGASTO1 + @"," + CGASTO2 + @"," + CGASTO3 + @"," + CTOTAL + @"
                                ," + CPENDIENTE.Trim() + @"," + CTOTALUN01.Trim() + @"," + CDESCUEN04.Trim() + @"," + CPORCENT01 + @"," + CPORCENT02 + @"," + CPORCENT03 + @"," + CPORCENT04 + @"," + CPORCENT05 + @"," + CPORCENT06 + @"
                                ,'" + CTEXTOEX01.Trim() + @"','" + CTEXTOEX02.Trim() + @"','" + CTEXTOEX03 + @"'," + fehcaHoy + "," + CIMPORTE01 + @"," + CIMPORTE02 + @"," + CIMPORTE03 + @"," + CIMPORTE04 + @"
                                ,'" + CDESTINA01.Trim() + @"','" + CNUMEROG01.Trim() + @"','" + CMENSAJE01 + @"','" + CCUENTAM01 + @"'," + CNUMEROC01 + @"," + CPESO + @"
                                ," + CBANOBSE01.Trim() + @"," + CBANDATO01.Trim() + @"," + CBANCOND01 + @"," + CBANGASTOS + @"," + CUNIDADE01 + @",'" + CTIMESTAMP + @"'," + CIMPCHEQ01 + @"
                                ," + CSISTORIG.Trim() + @"," + CIDMONEDCA.Trim() + @"," + CTIPOCAMCA + @"," + CESCFD + @"," + CTIENECFD + @",'" + CLUGAREXPE + @"','" + CMETODOPAG + @"'," + CNUMPARCIA + @"," + CCANTPARCI + @",'" + CCONDIPAGO + @"','" + CNUMCTAPAG + @"'," + CIDPROYE01 + @"," + CIDCUENTA + @"
                            ) ";

                                var insertCommand = new OleDbCommand(sSQL, dbConn);
                                Int32 rowsAffected = insertCommand.ExecuteNonQuery();
                            }
                        }
                        catch (OleDbException e)
                        {
                            string mensaje = "ucCxP - 484 Insertando en MGW10008.DBF " + sSQL;
                            asigar_registro_datagrid(mensaje, registro.Index);
                            ErrorFX.mostrar(e, false, true, mensaje, true);
                            return false;
                        }
                        catch (Exception e)
                        {
                            string mensaje = "ucCxP - 580 Insertando en MGW10008.DBF Logintud " + sSQL;
                            asigar_registro_datagrid(mensaje, registro.Index);
                            ErrorFX.mostrar(e, false, true, mensaje, true);
                            return false;
                        }
                        finally
                        {
                            // Close connection.
                            dbConn.Close();
                        }
                        //MGW10010
                        try
                        {
                            dbConn = new OleDbConnection(conexion);
                            //Documentos de prueba:
                            //30451
                            //Open connection.
                            dbConn.Open();
                            if (dbConn.State == ConnectionState.Open)
                            {
                                string CIDMOVIM01 = consecutivoLinea(dbConn);
                                CIDDOCUM01 = consecutivo;
                                string CNUMEROM01 = "100";
                                string CIDDOCUM02 = "19";
                                string CIDPRODU01 = cargarProducto(dbConn, oRegistroEmisor.codigo);//"22";
                                string CIDALMACEN = "1";
                                string CUNIDADES = "1";
                                string CUNIDADE02 = "1";
                                string CIDUNIDAD = "0";
                                string CIDUNIDA01 = "0";
                                string CPRECIO = CTOTAL;
                                string CPRECIOC01 = CTOTAL;
                                string CCOSTOCA01 = "0";
                                string CCOSTOES01 = "0";
                                string CPORCENT07 = "0";
                                string CPORCENT08 = "0";
                                string CPORCENT09 = "0";
                                string CDESCUEN05 = "0";
                                string CPORCENT10 = "0";
                                string CPORCENT11 = "0";
                                string CAFECTAE01 = "1";
                                string CAFECTAD01 = "1";
                                string CAFECTAD02 = "1";
                                string CMOVTOOC01 = "0";
                                string CIDMOVTO01 = "0";
                                string CIDMOVTO02 = "0";
                                string CUNIDADE03 = "1";
                                string CUNIDADE04 = "0";
                                string CUNIDADE05 = "0";
                                string CUNIDADE06 = "0";
                                string CTIPOTRA01 = "1";
                                string CIDVALOR01 = "0";
                                //Validar impuestos
                                CPORCENT01 = "16";

                                string CGTOMOVTO = "0";
                                string CSCMOVTO = "0";
                                string CCOMVENTA = "0";
                                string CIDMOVDEST = "0";
                                string CNUMCONSOL = "0";

                                sSQL = @"
                            INSERT INTO MGW10010.DBF
                            (
                                 CIDMOVIM01
                                ,CIDDOCUM01
                                ,CNUMEROM01
                                ,CIDDOCUM02
                                ,CIDPRODU01 
                                ,CIDALMACEN
                                ,CUNIDADES
                                ,CUNIDADE01 
                                ,CUNIDADE02
                                ,CIDUNIDAD 
                                ,CIDUNIDA01
                                ,CPRECIO
                                ,CPRECIOC01
                                ,CCOSTOCA01 
                                ,CCOSTOES01
                                ,CNETO
                                ,CIMPUESTO1,CPORCENT01
                                ,CIMPUESTO2,CPORCENT02
                                ,CIMPUESTO3,CPORCENT03
                                ,CRETENCI01,CPORCENT04
                                ,CRETENCI02,CPORCENT05
                                ,CDESCUEN01,CPORCENT06
                                ,CDESCUEN02,CPORCENT07
                                ,CDESCUEN03,CPORCENT08
                                ,CDESCUEN04,CPORCENT09
                                ,CDESCUEN05,CPORCENT10
                                ,CTOTAL
                                ,CPORCENT11
                                ,CREFEREN01
                                ,COBSERVA01
                                ,CAFECTAE01,CAFECTAD01,CAFECTAD02
                                ,CFECHA
                                ,CMOVTOOC01,CIDMOVTO01,CIDMOVTO02
                                ,CUNIDADE03,CUNIDADE04,CUNIDADE05,CUNIDADE06
                                ,CTIPOTRA01,CIDVALOR01
                                ,CTEXTOEX01,CTEXTOEX02,CTEXTOEX03
                                ,CFECHAEX01
                                ,CIMPORTE01,CIMPORTE02,CIMPORTE03,CIMPORTE04
                                ,CTIMESTAMP
                                ,CGTOMOVTO,CSCMOVTO,CCOMVENTA
                                ,CIDMOVDEST,CNUMCONSOL
                            )
                            VALUES
                            (
                            " + CIDMOVIM01 + @"
                            ," + CIDDOCUM01 + @"
                            ," + CNUMEROM01 + @"
                            ," + CIDDOCUM02 + @"
                            ," + CIDPRODU01 + @"
                            ," + CIDALMACEN + @"
                            ," + CUNIDADES + @"
                            ," + CUNIDADE01 + @"
                            ," + CUNIDADE02 + @"
                            ," + CIDUNIDAD + @"
                            ," + CIDUNIDA01 + @"
                            ," + CPRECIO + @"
                            ," + CPRECIOC01 + @"
                            ," + CCOSTOCA01 + @"
                            ," + CCOSTOES01 + @"
                            ," + CNETO + @"
                            ," + CIMPUESTO1 + @"
                            ," + CPORCENT01 + @"
                            ," + CIMPUESTO2 + @"
                            ," + CPORCENT02 + @"
                            ," + CIMPUESTO3 + @"
                            ," + CPORCENT03 + @"
                            ," + CRETENCI01 + @"
                            ," + CPORCENT04 + @"
                            ," + CRETENCI02 + @"
                            ," + CPORCENT05 + @"
                            ," + CDESCUEN01 + @"
                            ," + CPORCENT06 + @"
                            ," + CDESCUEN02 + @"
                            ," + CPORCENT07 + @"
                            ," + CDESCUEN03 + @"
                            ," + CPORCENT08 + @"
                            ," + CDESCUEN04 + @"
                            ," + CPORCENT09 + @"
                            ," + CDESCUEN05 + @"
                            ," + CPORCENT10 + @"
                            ," + CTOTAL + @"
                            ," + CPORCENT11 + @"
                            ,'" + CREFEREN01 + @"','" + COBSERVA01 + @"'
                            ," + CAFECTAE01 + @"
                            ," + CAFECTAD01 + @"
                            ," + CAFECTAD02 + @"
                            ,{^" + oComprobante.Fecha.ToString("yyyy/MM/dd") + @"}
                            ," + CMOVTOOC01 + @"
                            ," + CIDMOVTO01 + @"
                            ," + CIDMOVTO02 + @"
                            ," + CUNIDADE03 + @"," + CUNIDADE04 + @"," + CUNIDADE05 + @"," + CUNIDADE06 + @"
                            ," + CTIPOTRA01 + @"," + CIDVALOR01 + @"
                            ,'" + CTEXTOEX01 + @"','" + CTEXTOEX02 + @"','" + CTEXTOEX03 + @"',{" + CFECHAEX01 + @"}
                            ," + CIMPORTE01 + @"," + CIMPORTE02 + @"," + CIMPORTE03 + @"," + CIMPORTE04 + @"
                            ,'" + CTIMESTAMP + @"'
                            ," + CGTOMOVTO + @"
                            ,'" + CSCMOVTO + @"'
                            ," + CCOMVENTA + @"
                            ," + CIDMOVDEST + @"
                            ," + CNUMCONSOL + @")
                            ";
                                OleDbCommand insertCommand = new OleDbCommand(sSQL, dbConn);
                                Int32 rowsAffectedLine = insertCommand.ExecuteNonQuery();
                            }
                            //Insertar la linea de cargo
                            //proxima.Text = "Ingrese " + consecutivo;


                            //asigar_registro_datagrid("La empresa " + rfcReceptor + " no esta configurada", registro.Index);
                            //dtgrdGeneral.Rows[registro.Index].Cells["payableId"].Value = CFOLIO;

                            //Poner Generada
                            registrarFacturaPortal(CIDDOCUM01, folioCFDI, oComprobante.Fecha.ToString("yyyy-MM-dd")
                            , "Registrada", oComprobante.Emisor.Rfc, "");

                        }
                        catch (OleDbException e)
                        {
                            string mensaje = "ucCxP - 650 MGW10010.DBF " + sSQL;
                            asigar_registro_datagrid(mensaje, registro.Index);
                            ErrorFX.mostrar(e, false, true, mensaje, true);
                            return false;

                        }
                        catch (Exception e)
                        {
                            string mensaje = "ucCxP - 763 Insertando en MGW10010.DBF Logintud " + sSQL;
                            asigar_registro_datagrid(mensaje, registro.Index);
                            ErrorFX.mostrar(e, false, true, mensaje, true);
                            return false;
                        }
                        finally
                        {
                            // Close connection.
                            dbConn.Close();
                        }
                    }
                }
                dbContext.Dispose();
                return true;
            }
            catch (Exception error)
            {
                ErrorFX.mostrar(error, true, true, "ucCxP - 245 ");
            }


            return false;

        }

        private bool AdminPaqSQL(AdminPaqConfig oRegistroEmisor, Comprobante oComprobante)
        {
            return false;
        }

        private void registrarFacturaPortal(string voucher, string invoice, string invoiceDate, string estado, string rfc, string fechaProgramada)
        {
            string url = ConfigurationManager.AppSettings["portal"].ToString() + "Conector/payable?";
            try
            {
                string html = string.Empty;
                string postData = url + "voucher=" + voucher + "&invoice=" + invoice
                   + @"&invoiceDate=" + invoiceDate + "&estado=" + estado + "&rfc=" + rfc + "&fechaEstimada=" + fechaProgramada;

                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(postData);
                request.AutomaticDecompression = DecompressionMethods.GZip;

                using (HttpWebResponse response = (HttpWebResponse)request.GetResponse())
                using (Stream stream = response.GetResponseStream())
                using (StreamReader reader = new StreamReader(stream))
                {
                    string text = reader.ReadToEnd();
                    ErrorFX.mostrar(text, false, false, false);
                    resultado.Text = text;
                }
            }
            catch (Exception error)
            {
                ErrorFX.mostrar(error, true, true);
            }
        }

        private string cargarProducto(OleDbConnection dbConn, string codigo)
        {
            try
            {
                string sSQL = "SELECT CIDPRODU01 FROM MGW10005.DBF WHERE CCODIGOP01='" + codigo + "'";
                DataTable oDataTable = EjecutarConsulta(sSQL, dbConn);
                if (oDataTable != null)
                {
                    foreach (DataRow oDataRow in oDataTable.Rows)
                    {
                        return oDataRow["CIDPRODU01"].ToString();
                    }

                }
            }
            catch (Exception error)
            {
                ErrorFX.mostrar(error, true, true, "Consecutivo MGW10010 - 987");
            }
            return "";
        }

        private void bgwArchivos_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            asigar_texto_actualizacion("Archivos:" + DateTime.Now.ToShortDateString() + " " + DateTime.Now.ToShortTimeString());
            timer1.Start();
            textoHora.Text = "Fin Actualizaci�n de Archivos " + DateTime.Now.ToShortDateString() + " " + DateTime.Now.ToShortTimeString();
            button2.Enabled = true;
        }

        private void asigar_registro_datagrid(string texto, int index)
        {
            if (dtgrdGeneral.InvokeRequired)
            {
                MethodInvoker invoker = new MethodInvoker(delegate
                {
                    dtgrdGeneral.Rows[index].ErrorText = texto;
                    dtgrdGeneral.Rows[index].DefaultCellStyle.BackColor = Color.Red;
                });
                dtgrdGeneral.Invoke(invoker);
            }
            else
            {
                dtgrdGeneral.Rows[index].ErrorText = texto;
                dtgrdGeneral.Rows[index].DefaultCellStyle.BackColor = Color.Red;
            }
        }

        private void asigar_texto_log(string texto)
        {
            if (textoHora.InvokeRequired)
            {
                MethodInvoker invoker = new MethodInvoker(delegate
                {
                    log.Text = texto;
                });
                log.Invoke(invoker);
            }
            else
            {
                log.Text = texto;
            }
        }

        private void asigar_texto_actualizacion(string texto)
        {
            if (textoHora.InvokeRequired)
            {
                MethodInvoker invoker = new MethodInvoker(delegate
                {
                    textoHora.Text = texto;
                });
                textoHora.Invoke(invoker);
            }
            else
            {
                textoHora.Text = texto;
            }
        }

        private void bgwArchivos_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            textoHora.Text = e.UserState.ToString();
        }

        private void bgwArchivos_DoWork(object sender, DoWorkEventArgs e)
        {
            try
            {
                ModeloSincronizacionContainer dbContext = new ModeloSincronizacionContainer();
                foreach (DataGridViewRow registro in dtgrdGeneral.Rows)
                {
                    try
                    {
                        string row_idtr = registro.Cells["row_id"].Value.ToString();
                        archivoSync l = new archivoSync();
                        l = (from r in dbContext.archivoSyncSet.Where
                                                (a => a.idDocumentoSet.Equals(row_idtr))
                             select r).FirstOrDefault();

                        //Validar que exista
                        if (l == null)
                        {
                            //Descargar los archivos guardarlo donde deben estar 
                            string direccion = ConfigurationManager.AppSettings["portal"].ToString();

                            string directorio = "";
                            try
                            {
                                directorio = ConfigurationManager.AppSettings["directorio"].ToString();
                            }
                            catch
                            {
                                directorio = System.IO.Path.GetDirectoryName(Application.ExecutablePath);
                            }

                            string directorioPersonalizado = "";
                            try
                            {
                                directorioPersonalizado = ConfigurationManager.AppSettings["directorioConfiguracion"].ToString();

                            }
                            catch
                            {

                            }

                            string directorioPago = "";
                            try
                            {
                                directorioPago = ConfigurationManager.AppSettings["directorioPago"].ToString();
                            }
                            catch
                            {
                                directorioPago = System.IO.Path.GetDirectoryName(Application.ExecutablePath);
                            }
                            string directorioPersonalizadoPago = "";
                            try
                            {
                                directorioPersonalizadoPago = ConfigurationManager.AppSettings["directorioConfiguracionPago"].ToString();

                            }
                            catch
                            {

                            }
                            //Validar el TIPO
                            if (registro.Cells["tipoComprobante"].Value.ToString().Equals("P"))
                            {
                                directorio = directorioPago;
                                directorioPersonalizado = directorioPersonalizadoPago;
                            }


                            string direccion_xml = direccion + "/" + registro.Cells["XML"].Value.ToString().Replace("./", "");
                            string direccion_pdf = direccion + "/" + registro.Cells["PDF"].Value.ToString().Replace("./", "");


                            bgwArchivos.ReportProgress(0, "Copiando archivo Id: " + registro.Cells["row_id"].Value.ToString() + " RFC: " + registro.Cells["rfcEmisor"].Value.ToString());


                            string rfcEmisortr = registro.Cells["rfcEmisor"].Value.ToString();
                            string rfcReceptortr = registro.Cells["rfcReceptor"].Value.ToString();

                            //Directorio base
                            string directorioBase = "";
                            if (!String.IsNullOrEmpty(directorioPersonalizado))
                            {
                                DateTimeFormatInfo usDtfi = new CultureInfo("es-MX", false).DateTimeFormat;
                                DateTime fechaFacturatr = DateTime.Parse(registro.Cells["fechaFactura"].Value.ToString());


                                directorioPersonalizado = directorioPersonalizado.Replace("yyyy", fechaFacturatr.ToString("yyyy", usDtfi));
                                directorioPersonalizado = directorioPersonalizado.Replace("MMMM", fechaFacturatr.ToString("MMMM", usDtfi)).ToUpper();
                                directorioPersonalizado = directorioPersonalizado.Replace("MM", fechaFacturatr.ToString("MM", usDtfi));
                                directorioPersonalizado = directorioPersonalizado.Replace("RFCEMISOR", rfcEmisortr);

                                directorioBase = directorio + "/" + directorioPersonalizado + "/";
                            }
                            else
                            {
                                directorio += "/" + Fechammmyy(registro.Cells["fechaFactura"].Value.ToString());
                                directorioBase = directorio + "/" + rfcReceptortr + "/" + rfcEmisortr + "/";
                            }

                            if (!Directory.Exists(directorioBase))
                            {
                                Directory.CreateDirectory(directorioBase);
                            }
                            if (!Directory.Exists(directorioBase + "/XML/"))
                            {
                                Directory.CreateDirectory(directorioBase + "/XML/");
                            }
                            if (!Directory.Exists(directorioBase + "/PDF/"))
                            {
                                Directory.CreateDirectory(directorioBase + "/PDF/");
                            }

                            //Nombre del archivo
                            string foliotr = registro.Cells["folio"].Value.ToString();

                            foliotr = foliotr.Replace(".", "").Replace(" ", "");

                            string directorio_xml = "";
                            string directorio_pdf = "";
                            try
                            {
                                WebClient webClient = new WebClient();
                                if (registro.Cells["XML"].Value.ToString().Replace("./", "").Trim().Length != 0)
                                {
                                    directorio_xml = directorioBase + "/XML/" + foliotr + ".xml";
                                    webClient.Headers.Add("user-agent", "Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.2; .NET CLR 1.0.3705;)");
                                    webClient.DownloadFile(direccion_xml, directorio_xml);
                                }
                                
                                if (registro.Cells["PDF"].Value.ToString().Replace("./", "").Trim().Length != 0)
                                {
                                    directorio_pdf = directorioBase + "/PDF/" + foliotr + ".pdf";

                                    webClient.Headers.Add("user-agent", "Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.2; .NET CLR 1.0.3705;)");
                                    webClient.DownloadFile(direccion_pdf, directorio_pdf);
                                }
                            }
                            catch (Exception error)
                            {
                                ErrorFX.mostrar(error, true, true, "frmPrincipal - 782 ");
                            }

                            //Agregar el registro
                            archivoSync registroNuevo = new archivoSync();
                            registroNuevo.idDocumentoSet = registro.Cells["row_id"].Value.ToString();
                            registroNuevo.folio = foliotr;
                            registroNuevo.xml = directorio_xml;
                            registroNuevo.pdf = directorio_pdf;
                            registroNuevo.rfcEmisor = rfcEmisortr;
                            registroNuevo.rfcReceptor = rfcReceptortr;
                            registroNuevo.UUID = registro.Cells["CFDI"].Value.ToString();
                            registroNuevo.proceso = 1;
                            registroNuevo.invoice = registroNuevo.folio;
                            registroNuevo.tipoComprobante = registro.Cells["tipoComprobante"].Value.ToString();
                            try
                            {
                                registroNuevo.oc = registro.Cells["oc"].Value.ToString();
                                registroNuevo.recibo = registro.Cells["recibo"].Value.ToString();
                            }
                            catch
                            {

                            }
                            dbContext.archivoSyncSet.Add(registroNuevo);
                            dbContext.SaveChanges();


                            if (!sincronizarAP(registro, directorio_xml))
                            {
                                dbContext.archivoSyncSet.Remove(registroNuevo);
                                dbContext.SaveChanges();
                            }
                            sincronizarGlobalSearch(registro, directorio_xml);


                        }


                    }
                    catch (DbEntityValidationException error)
                    {
                        string errorString = "";
                        foreach (var eve in error.EntityValidationErrors)
                        {
                            Console.WriteLine("Entity of type \"{0}\" in state \"{1}\" has the following validation errors:",
                                eve.Entry.Entity.GetType().Name, eve.Entry.State);
                            foreach (var ve in eve.ValidationErrors)
                            {
                                Console.WriteLine("- Property: \"{0}\", Error: \"{1}\"",
                                    ve.PropertyName, ve.ErrorMessage);
                                errorString += " - Property: " + ve.PropertyName + ", Error: " + ve.ErrorMessage + Environment.NewLine;
                            }
                        }
                        ErrorFX.mostrar(error, true, true, "frmPrincipal - 465 " + Environment.NewLine + errorString);
                    }
                    catch (System.Data.DataException error)
                    {
                        ErrorFX.mostrar(error, true, true, "frmPrincipal - 777 ");
                    }
                    catch (Exception error)
                    {
                        ErrorFX.mostrar(error, true, true, "frmPrincipal - 782 ");
                    }
                    finally
                    {
                        // Close connection.

                    }
                    //Insertar el registro de la cabecera
                }

            }
            catch (Exception error)
            {
                ErrorFX.mostrar(error, true, true, "frmPrincipal - 477 ");
            }
        }
        private bool ContainsInvalidPathCharacters(string filePath)
        {
            for (var i = 0; i < filePath.Length; i++)
            {
                int c = filePath[i];

                if (c == '\"' || c == '<' || c == '>' || c == '|' || c == '*' || c == '?' || c < 32)
                    return true;
            }

            return false;
        }
        private int counter = 0;
        private void ejecutarAutomatico_CheckedChanged(object sender, EventArgs e)
        {
            if (ejecutarAutomatico.Checked)
            {

                counter = 0;
                timer1.Start();
            }
            else
            {
                counter = 0;
                timer1.Stop();
            }
        }


        private void aumentarFolio(string v, int folio, OleDbConnection oConn)
        {
            //Validar si es extranjero en todos traer el consecutivo
            try
            {
                string sSQL = "UPDATE MGW10006.DBF SET CNOFOLIO=" + folio.ToString() + " WHERE CIDCONCE01=" + v + "";
                EjecutarConsulta(sSQL, oConn);
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message.ToString());
            }
        }
        private DataTable validarRFCSQL(string rfc, cCONEXCION oDatap)
        {
            //Validar si es extranjero en todos traer el consecutivo
            try
            {
                string sSQL = "SELECT CIDCLIEN01,CCODIGOC01,CRAZONSO01,CRFC FROM MGW10002.DBF WHERE cRFC='" + rfc + "'";
                DataTable oDataTable = oDatap.EjecutarConsulta(sSQL);
                if (oDataTable != null)
                {
                    return oDataTable;

                }
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message.ToString());
            }
            return null;
        }

        private DataTable validarRFC(string rfc, OleDbConnection oConn)
        {
            //Validar si es extranjero en todos traer el consecutivo
            try
            {
                string sSQL = "SELECT CIDCLIEN01,CCODIGOC01,CRAZONSO01,CRFC FROM MGW10002.DBF WHERE cRFC='" + rfc + "'";
                DataTable oDataTable = EjecutarConsulta(sSQL, oConn);
                if (oDataTable != null)
                {
                    return oDataTable;

                }
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message.ToString());
            }
            return null;
        }

        private DataTable obtenerMWG10006(string v, OleDbConnection oConn)
        {
            try
            {
                string sSQL = "SELECT CIDCONCE01,CCODIGOC01,CIDDOCUM01,CNOFOLIO  FROM MGW10006.DBF WHERE CNOMBREC01='" + v + "'";
                DataTable oDataTable = EjecutarConsulta(sSQL, oConn);
                if (oDataTable != null)
                {
                    return oDataTable;

                }
            }
            catch (Exception error)
            {
                ErrorFX.mostrar(error, true, true);
            }
            return null;
        }

        public DataTable EjecutarConsulta(string pQuery, OleDbConnection oConn)
        {
            try
            {
                DataTable oDT = new DataTable();
                //Ejecutar para SQL Server
                oDT = new DataTable();
                OleDbDataAdapter oDataAdapter = new OleDbDataAdapter();
                OleDbCommand oSqlCommand = new OleDbCommand(pQuery, oConn);
                oSqlCommand.CommandTimeout = 10000000;
                oDataAdapter.SelectCommand = oSqlCommand;
                oDataAdapter.Fill(oDT);
                return oDT;
            }
            catch (OleDbException e)
            {
                //MessageBox.Show(e.Message + "\n" + pQuery, "Exepci�n " + Application.ProductName + "-" + Application.ProductVersion, MessageBoxButtons.OK, MessageBoxIcon.Stop);
                ErrorFX.mostrar(e.Message + "\n" + pQuery, true, true, false);
                return null;
            }

        }

        private string consecutivoDocumento(OleDbConnection dbConn, string CIDCONCE01)
        {
            try
            {
                //string sSQL = "SELECT CIDDOCUM01 FROM MGW10008.DBF WHERE CIDDOCUM02=19 AND CIDCONCE01=" + CIDCONCE01 + " ORDER BY CIDDOCUM01 DESC";
                string sSQL = "SELECT CIDDOCUM01 FROM MGW10008.DBF WHERE CIDCONCE01=" + CIDCONCE01 + " ORDER BY CIDDOCUM01 DESC";
                DataTable oDataTable = EjecutarConsulta(sSQL, dbConn);
                if (oDataTable != null)
                {
                    foreach (DataRow oDataRow in oDataTable.Rows)
                    {
                        //Validar que exista
                        int consecutivo = int.Parse(oDataRow["CIDDOCUM01"].ToString()) + 1; //oDataTable.Rows.Count + 10;//

                        bool existe = false;
                        while (!existe)
                        {
                            sSQL = "SELECT CIDDOCUM01 FROM MGW10008.DBF WHERE CIDDOCUM01=" + consecutivo + " ORDER BY CIDDOCUM01 DESC";
                            DataTable oDataTableExiste = EjecutarConsulta(sSQL, dbConn);
                            if (oDataTableExiste != null)
                            {
                                if (oDataTableExiste.Rows.Count == 0)
                                {
                                    existe = true;
                                }
                                else
                                {
                                    foreach (DataRow oDataRowExiste in oDataTableExiste.Rows)
                                    {
                                        consecutivo++;
                                        break;
                                    }
                                }
                            }
                        }
                        return (consecutivo).ToString();
                    }

                }
            }
            catch (Exception error)
            {
                ErrorFX.mostrar(error, true, true, "Consecutivo MGW10008 - 965");
            }
            return "1";
        }

        private string consecutivoLinea(OleDbConnection dbConn)
        {
            try
            {
                string sSQL = "SELECT CIDMOVIM01 FROM MGW10010.DBF WHERE CIDDOCUM02=19 ORDER BY CIDMOVIM01 DESC";
                DataTable oDataTable = EjecutarConsulta(sSQL, dbConn);
                if (oDataTable != null)
                {
                    foreach (DataRow oDataRow in oDataTable.Rows)
                    {
                        return (int.Parse(oDataRow["CIDMOVIM01"].ToString()) + 1).ToString();
                    }

                }
            }
            catch (Exception error)
            {
                ErrorFX.mostrar(error, true, true, "Consecutivo MGW10010 - 987");
            }
            return "1";
        }

        private string consecutivoFolio(OleDbConnection dbConn)
        {
            try
            {
                string sSQL = "SELECT CFOLIO FROM MGW10008.DBF WHERE CIDDOCUM02=19 AND CIDCONCE01=21 ORDER BY CFOLIO DESC";
                DataTable oDataTable = EjecutarConsulta(sSQL, dbConn);
                if (oDataTable != null)
                {
                    foreach (DataRow oDataRow in oDataTable.Rows)
                    {
                        return (int.Parse(oDataRow["CFOLIO"].ToString()) + 1).ToString();
                    }

                }
            }
            catch (Exception error)
            {
                ErrorFX.mostrar(error, true, true);
            }
            return "1";
        }
        private void timer1_Tick(object sender, EventArgs e)
        {
            if (ejecutarAutomatico.Checked)
            {
                counter++;

                proxima.Text = (segundos - counter).ToString();


                if (counter == segundos)
                {
                    //Fecha Actua con hora
                    DateTime fechaActual = fin.Value;

                    if (!String.IsNullOrEmpty(horaInicialtr))
                    {
                        DateTime inicioTr = new DateTime(DateTime.Now.Year, DateTime.Now.Month
                            , DateTime.Now.Day, int.Parse(horaInicialtr), 0, 0);

                        DateTime finTr = new DateTime(DateTime.Now.Year, DateTime.Now.Month
                            , DateTime.Now.Day, int.Parse(horaFinaltr), 0, 0);

                        if (inicioTr < DateTime.Now)
                        {
                            if (finTr > DateTime.Now)
                            {
                                timer1.Stop();
                                cargar();
                                syncArchivos();
                                proxima.Text = (segundos - counter).ToString();
                                fin.Value = DateTime.Now.AddYears(10);
                            }
                        }
                    }
                    else
                    {
                        timer1.Stop();
                        cargar();
                        syncArchivos();

                        proxima.Text = (segundos - counter).ToString();
                        fin.Value = DateTime.Now.AddYears(10);
                    }
                    counter = 0;
                }
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            GlobalSearchCrearJob();
        }

        private void GlobalSearchCrearJob()
        {
            globalSearch oglobalSearch = new globalSearch();
            string token = "";
            log2.Text = "Creando archivos " + DateTime.Now.ToShortTimeString();
            try
            {
                token = oglobalSearch.GetLicense();
                log2.Text += "Token: " + token + Environment.NewLine;

                //Abrir el chunck para cargar
                //http://www.square-9.com/api/?api=REST%20API%20Resources#collapse_ChunkingUpload_CreateArchiveJob
                //upload?database={dbID}&archive={archiveID}&time={time}&filetype={type}&token={token}
                //var requestArchivo = new RestRequest("api/upload?database={db}&archive={archiveID}&time={time}&filetype={type}&token={token}", Method.POST);
                var requestArchivo = new RestRequest("http://192.168.110.107/Square9API/api/dbs/" + oglobalSearch.ocSqUsuario.databaseId
                    + "/archives/" + oglobalSearch.ocSqUsuario.archiveId
                    + "/upload?token=" + token, Method.POST);

                ///dbs/{Database}/archives?token={token}&format={json|xml}
                //requestArchivo.AddParameter("token", token, ParameterType.UrlSegment); //have to specifiy type on POST
                //requestArchivo.AddParameter("db", oglobalSearch.ocSqUsuario.databaseId, ParameterType.UrlSegment);
                //requestArchivo.AddParameter("archiveID", oglobalSearch.ocSqUsuario.archiveId, ParameterType.UrlSegment);
                //requestArchivo.AddParameter("time", DateTime.Now.ToLocalTime(), ParameterType.UrlSegment);
                //requestArchivo.AddParameter("type", "text", ParameterType.UrlSegment);
                //requestArchivo.AddParameter("token", token, ParameterType.UrlSegment);
                var resultado = oglobalSearch.ApiClient.Execute<cChunkInicio>(requestArchivo);
                if (resultado.StatusCode != HttpStatusCode.OK)
                {
                    log.Text = "Error:" + resultado.Content;
                    log2.Text += "Error llamando el metodo Upload: " + resultado.Content + Environment.NewLine;
                    return;
                }
                string JobID = resultado.Data.JobID;
                log2.Text += "JobID: " + JobID + Environment.NewLine;
                log2.Text += "ChunkLimit: " + resultado.Data.ChunkLimit + Environment.NewLine;

                //Enviar el archivo
                //Headers: content - type = text / json
                //Parameters:
                //[RAW]:
                //The body of this POST must contain a JSON object with the following keys:
                // {
                //  "JobID": "", //JobID obtained in the initial upload call
                //  "Index": 1, //number of the chunk being uploaded.
                //  "RawData": "" //A base64 encoded string of the chunk of the file being uploaded
                //}

                FileStream fstream = new
                FileStream(archivo.Text, FileMode.Open, FileAccess.Read);
                StreamReader sreader = new StreamReader(fstream);

                string fileContent = sreader.ReadToEnd();

                var requestArchivoEnviar = new RestRequest("api/upload?time={time}&token={token}", Method.POST);
                requestArchivoEnviar.AddParameter("time", DateTime.Now.ToLocalTime(), ParameterType.UrlSegment);
                requestArchivoEnviar.AddParameter("token", token, ParameterType.UrlSegment);
                requestArchivoEnviar.AddFile("file", fileContent);
                requestArchivoEnviar.AddHeader("Content-Type", "multipart/form-data");
                requestArchivoEnviar.AlwaysMultipartFormData = true;
                requestArchivoEnviar.AddBody(new { JobID = JobID, Index = 1, RawData = fileContent });

                var responseArchivoEnviar = oglobalSearch.ApiClient.Execute(requestArchivoEnviar);
                if (responseArchivoEnviar.StatusCode != HttpStatusCode.OK)
                {
                    log.Text = "Error:" + responseArchivoEnviar.Content;
                    log2.Text += "Error: " + responseArchivoEnviar.Content + Environment.NewLine;
                }
                //Cerrar el archivo

            }
            catch (Exception ex)
            {
                log.Text += ex.Message + Environment.NewLine;
                log2.Text += "Error: " + ex.Message + Environment.NewLine;
            }
            finally
            {
                if (!String.IsNullOrEmpty(token))
                {
                    oglobalSearch.ReleaseLicense(token);
                }
            }
        }

        private void button4_Click(object sender, EventArgs e)
        {
            OpenFileDialog theDialog = new OpenFileDialog();
            theDialog.Title = "Any File";
            theDialog.Filter = "*.*|*.*";
            if (theDialog.ShowDialog() == DialogResult.OK)
            {
                try
                {
                    archivo.Text = theDialog.FileName;
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Error: Could not read file from disk. Original error: " + ex.Message);
                }
            }
        }

        private void frmPrincipal_Resize(object sender, EventArgs e)
        {
            if (this.WindowState == FormWindowState.Minimized)
            {
                notifyIcon.Visible = true;
                notifyIcon.ShowBalloonTip(3000);
                this.ShowInTaskbar = false;
            }
        }

        private void notifyIcon_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            this.WindowState = FormWindowState.Normal;
            this.ShowInTaskbar = true;
            notifyIcon.Visible = false;
        }

        private void frmPrincipal_FormClosed(object sender, FormClosedEventArgs e)
        {
            Application.Exit();
        }

        private void button5_Click(object sender, EventArgs e)
        {
            dblist();
        }

        private void dblist()
        {
            try
            {

            }
            catch (Exception e)
            {

            }
        }

        private void tabControl2_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void button6_Click(object sender, EventArgs e)
        {
            registrarFacturaPortal(voucher.Text, invoice.Text, fecha.Value.ToString("yyyy-MM-dd")
                                , "Registrada", rfcTr.Text, "");
        }
    }
}
