﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConectorFX
{
    using System;
    using System.Collections.Generic;
    using System.Net;

    using System.Globalization;
    using Newtonsoft.Json;
    using Newtonsoft.Json.Converters;

    public partial class Documento
    {
        [JsonProperty("emisor")]
        public string Emisor { get; set; }

        [JsonProperty("alias")]
        public string Alias { get; set; }

        [JsonProperty("XML")]
        public object Xml { get; set; }

        [JsonProperty("PDF")]
        public string Pdf { get; set; }

        [JsonProperty("UUID")]
        public string Uuid { get; set; }
    }

    public partial class Documento
    {
        public static Documento[] FromJson(string json) => JsonConvert.DeserializeObject<Documento[]>(json, ConectorFX.Converter.Settings);
    }

    public static class Serialize
    {
        public static string ToJson(this Documento[] self) => JsonConvert.SerializeObject(self, ConectorFX.Converter.Settings);
    }

    internal class Converter
    {
        public static readonly JsonSerializerSettings Settings = new JsonSerializerSettings
        {
            MetadataPropertyHandling = MetadataPropertyHandling.Ignore,
            DateParseHandling = DateParseHandling.None,
            Converters = {
                new IsoDateTimeConverter()
                {
                    DateTimeStyles = DateTimeStyles.AssumeUniversal,
                },
            },
        };
    }
}
