#region Copyright Syncfusion Inc. 2001 - 2017
// Copyright Syncfusion Inc. 2001 - 2017. All rights reserved.
// Use of this code is subject to the terms of our license.
// A copy of the current license can be obtained at any time by e-mailing
// licensing@syncfusion.com. Any infringement will be prosecuted under
// applicable laws. 
#endregion
namespace ConectorFX
{
    partial class frmPrincipal
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmPrincipal));
            this.dtgrdGeneral = new System.Windows.Forms.DataGridView();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.faltaSincronizar = new System.Windows.Forms.CheckBox();
            this.button2 = new System.Windows.Forms.Button();
            this.filtroTipo = new System.Windows.Forms.ComboBox();
            this.label13 = new System.Windows.Forms.Label();
            this.estado = new System.Windows.Forms.ComboBox();
            this.label4 = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.uuid = new System.Windows.Forms.TextBox();
            this.folioFiltro = new System.Windows.Forms.TextBox();
            this.rfc = new System.Windows.Forms.TextBox();
            this.fin = new System.Windows.Forms.DateTimePicker();
            this.inicio = new System.Windows.Forms.DateTimePicker();
            this.label3 = new System.Windows.Forms.Label();
            this.ejecutarAutomatico = new System.Windows.Forms.CheckBox();
            this.label7 = new System.Windows.Forms.Label();
            this.proxima = new System.Windows.Forms.TextBox();
            this.textoHora = new System.Windows.Forms.Label();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.tabPage7 = new System.Windows.Forms.TabPage();
            this.log = new System.Windows.Forms.TextBox();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.tabControl2 = new System.Windows.Forms.TabControl();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.fecha = new System.Windows.Forms.DateTimePicker();
            this.label10 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.rfcTr = new System.Windows.Forms.TextBox();
            this.invoice = new System.Windows.Forms.TextBox();
            this.resultado = new System.Windows.Forms.TextBox();
            this.voucher = new System.Windows.Forms.TextBox();
            this.button6 = new System.Windows.Forms.Button();
            this.ucGlobalSearchConfiguracion1 = new ConectorFX.ucGlobalSearchConfiguracion();
            this.tabPage4 = new System.Windows.Forms.TabPage();
            this.ucConfiguracionAdminPaq1 = new ConectorFX.AdminPaq.ucConfiguracionAdminPaq();
            this.tabPage5 = new System.Windows.Forms.TabPage();
            this.tabPage6 = new System.Windows.Forms.TabPage();
            this.tabPage8 = new System.Windows.Forms.TabPage();
            this.button5 = new System.Windows.Forms.Button();
            this.log2 = new System.Windows.Forms.TextBox();
            this.button4 = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.archivo = new System.Windows.Forms.TextBox();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.notifyIcon = new System.Windows.Forms.NotifyIcon(this.components);
            this.CFDI = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.folio = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PDF = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.XML = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.FormatoAdicional = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.rfcEmisor = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.rfcReceptor = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.row_id = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.fechaFactura = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tipoComprobante = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.OC = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Recibo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            ((System.ComponentModel.ISupportInitialize)(this.dtgrdGeneral)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.tabPage7.SuspendLayout();
            this.tabPage2.SuspendLayout();
            this.tabControl2.SuspendLayout();
            this.tabPage3.SuspendLayout();
            this.tabPage4.SuspendLayout();
            this.tabPage8.SuspendLayout();
            this.SuspendLayout();
            // 
            // dtgrdGeneral
            // 
            this.dtgrdGeneral.AllowUserToAddRows = false;
            this.dtgrdGeneral.AllowUserToDeleteRows = false;
            this.dtgrdGeneral.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dtgrdGeneral.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.CFDI,
            this.folio,
            this.PDF,
            this.XML,
            this.FormatoAdicional,
            this.rfcEmisor,
            this.rfcReceptor,
            this.row_id,
            this.fechaFactura,
            this.tipoComprobante,
            this.OC,
            this.Recibo});
            this.dtgrdGeneral.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dtgrdGeneral.Location = new System.Drawing.Point(3, 3);
            this.dtgrdGeneral.Margin = new System.Windows.Forms.Padding(2);
            this.dtgrdGeneral.Name = "dtgrdGeneral";
            this.dtgrdGeneral.RowTemplate.Height = 24;
            this.dtgrdGeneral.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dtgrdGeneral.Size = new System.Drawing.Size(873, 282);
            this.dtgrdGeneral.TabIndex = 13;
            // 
            // groupBox1
            // 
            this.groupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox1.Controls.Add(this.faltaSincronizar);
            this.groupBox1.Controls.Add(this.button2);
            this.groupBox1.Controls.Add(this.filtroTipo);
            this.groupBox1.Controls.Add(this.label13);
            this.groupBox1.Controls.Add(this.estado);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.button1);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.uuid);
            this.groupBox1.Controls.Add(this.folioFiltro);
            this.groupBox1.Controls.Add(this.rfc);
            this.groupBox1.Controls.Add(this.fin);
            this.groupBox1.Controls.Add(this.inicio);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Location = new System.Drawing.Point(0, 34);
            this.groupBox1.Margin = new System.Windows.Forms.Padding(2);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Padding = new System.Windows.Forms.Padding(2);
            this.groupBox1.Size = new System.Drawing.Size(887, 76);
            this.groupBox1.TabIndex = 14;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Busqueda del Portal";
            // 
            // faltaSincronizar
            // 
            this.faltaSincronizar.AutoSize = true;
            this.faltaSincronizar.Location = new System.Drawing.Point(370, 49);
            this.faltaSincronizar.Name = "faltaSincronizar";
            this.faltaSincronizar.Size = new System.Drawing.Size(137, 17);
            this.faltaSincronizar.TabIndex = 39;
            this.faltaSincronizar.Text = "Faltantes de sincronizar";
            this.faltaSincronizar.UseVisualStyleBackColor = true;
            // 
            // button2
            // 
            this.button2.BackColor = System.Drawing.Color.YellowGreen;
            this.button2.FlatAppearance.BorderSize = 0;
            this.button2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button2.ForeColor = System.Drawing.Color.White;
            this.button2.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.button2.Location = new System.Drawing.Point(4, 45);
            this.button2.Margin = new System.Windows.Forms.Padding(2);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(82, 24);
            this.button2.TabIndex = 38;
            this.button2.Text = "Sincronizar";
            this.button2.UseVisualStyleBackColor = false;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // filtroTipo
            // 
            this.filtroTipo.AutoCompleteCustomSource.AddRange(new string[] {
            "",
            "P",
            "T",
            "I",
            "E"});
            this.filtroTipo.FormattingEnabled = true;
            this.filtroTipo.Location = new System.Drawing.Point(555, 47);
            this.filtroTipo.Name = "filtroTipo";
            this.filtroTipo.Size = new System.Drawing.Size(70, 21);
            this.filtroTipo.TabIndex = 37;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(522, 51);
            this.label13.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(28, 13);
            this.label13.TabIndex = 36;
            this.label13.Text = "Tipo";
            // 
            // estado
            // 
            this.estado.FormattingEnabled = true;
            this.estado.Location = new System.Drawing.Point(537, 19);
            this.estado.Name = "estado";
            this.estado.Size = new System.Drawing.Size(88, 21);
            this.estado.TabIndex = 37;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(492, 23);
            this.label4.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(40, 13);
            this.label4.TabIndex = 36;
            this.label4.Text = "Estado";
            // 
            // button1
            // 
            this.button1.BackColor = System.Drawing.Color.Goldenrod;
            this.button1.FlatAppearance.BorderSize = 0;
            this.button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button1.ForeColor = System.Drawing.Color.White;
            this.button1.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.button1.Location = new System.Drawing.Point(4, 17);
            this.button1.Margin = new System.Windows.Forms.Padding(2);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(82, 24);
            this.button1.TabIndex = 35;
            this.button1.Text = "Buscar";
            this.button1.UseVisualStyleBackColor = false;
            this.button1.Click += new System.EventHandler(this.button1_Click_1);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(229, 51);
            this.label6.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(34, 13);
            this.label6.TabIndex = 2;
            this.label6.Text = "UUID";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(92, 51);
            this.label5.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(29, 13);
            this.label5.TabIndex = 2;
            this.label5.Text = "Folio";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(367, 23);
            this.label1.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(28, 13);
            this.label1.TabIndex = 2;
            this.label1.Text = "RFC";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(92, 23);
            this.label2.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(32, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "Inicio";
            // 
            // uuid
            // 
            this.uuid.Location = new System.Drawing.Point(264, 47);
            this.uuid.Margin = new System.Windows.Forms.Padding(2);
            this.uuid.Name = "uuid";
            this.uuid.Size = new System.Drawing.Size(86, 20);
            this.uuid.TabIndex = 3;
            // 
            // folioFiltro
            // 
            this.folioFiltro.Location = new System.Drawing.Point(127, 47);
            this.folioFiltro.Margin = new System.Windows.Forms.Padding(2);
            this.folioFiltro.Name = "folioFiltro";
            this.folioFiltro.Size = new System.Drawing.Size(95, 20);
            this.folioFiltro.TabIndex = 3;
            // 
            // rfc
            // 
            this.rfc.Location = new System.Drawing.Point(402, 19);
            this.rfc.Margin = new System.Windows.Forms.Padding(2);
            this.rfc.Name = "rfc";
            this.rfc.Size = new System.Drawing.Size(86, 20);
            this.rfc.TabIndex = 3;
            // 
            // fin
            // 
            this.fin.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.fin.Location = new System.Drawing.Point(254, 19);
            this.fin.Margin = new System.Windows.Forms.Padding(2);
            this.fin.Name = "fin";
            this.fin.Size = new System.Drawing.Size(96, 20);
            this.fin.TabIndex = 4;
            // 
            // inicio
            // 
            this.inicio.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.inicio.Location = new System.Drawing.Point(127, 19);
            this.inicio.Margin = new System.Windows.Forms.Padding(2);
            this.inicio.Name = "inicio";
            this.inicio.Size = new System.Drawing.Size(95, 20);
            this.inicio.TabIndex = 4;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(229, 23);
            this.label3.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(21, 13);
            this.label3.TabIndex = 2;
            this.label3.Text = "Fin";
            // 
            // ejecutarAutomatico
            // 
            this.ejecutarAutomatico.AutoSize = true;
            this.ejecutarAutomatico.Location = new System.Drawing.Point(0, 12);
            this.ejecutarAutomatico.Name = "ejecutarAutomatico";
            this.ejecutarAutomatico.Size = new System.Drawing.Size(128, 17);
            this.ejecutarAutomatico.TabIndex = 45;
            this.ejecutarAutomatico.Text = "Ejecución automatica";
            this.ejecutarAutomatico.UseVisualStyleBackColor = true;
            this.ejecutarAutomatico.CheckedChanged += new System.EventHandler(this.ejecutarAutomatico_CheckedChanged);
            // 
            // label7
            // 
            this.label7.Location = new System.Drawing.Point(134, 13);
            this.label7.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(93, 15);
            this.label7.TabIndex = 43;
            this.label7.Text = "Proxima ejecución";
            // 
            // proxima
            // 
            this.proxima.Location = new System.Drawing.Point(231, 10);
            this.proxima.Margin = new System.Windows.Forms.Padding(2);
            this.proxima.Name = "proxima";
            this.proxima.ReadOnly = true;
            this.proxima.Size = new System.Drawing.Size(119, 20);
            this.proxima.TabIndex = 42;
            // 
            // textoHora
            // 
            this.textoHora.Location = new System.Drawing.Point(4, 432);
            this.textoHora.Name = "textoHora";
            this.textoHora.Size = new System.Drawing.Size(487, 13);
            this.textoHora.TabIndex = 15;
            // 
            // tabControl1
            // 
            this.tabControl1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage7);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Controls.Add(this.tabPage8);
            this.tabControl1.Location = new System.Drawing.Point(0, 115);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(887, 314);
            this.tabControl1.TabIndex = 16;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.dtgrdGeneral);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(879, 288);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Documentos";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // tabPage7
            // 
            this.tabPage7.Controls.Add(this.log);
            this.tabPage7.Location = new System.Drawing.Point(4, 22);
            this.tabPage7.Name = "tabPage7";
            this.tabPage7.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage7.Size = new System.Drawing.Size(879, 288);
            this.tabPage7.TabIndex = 2;
            this.tabPage7.Text = "Log";
            this.tabPage7.UseVisualStyleBackColor = true;
            // 
            // log
            // 
            this.log.Dock = System.Windows.Forms.DockStyle.Fill;
            this.log.Location = new System.Drawing.Point(3, 3);
            this.log.Multiline = true;
            this.log.Name = "log";
            this.log.Size = new System.Drawing.Size(873, 282);
            this.log.TabIndex = 0;
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.tabControl2);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(879, 288);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Configuración";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // tabControl2
            // 
            this.tabControl2.Controls.Add(this.tabPage3);
            this.tabControl2.Controls.Add(this.tabPage4);
            this.tabControl2.Controls.Add(this.tabPage5);
            this.tabControl2.Controls.Add(this.tabPage6);
            this.tabControl2.Location = new System.Drawing.Point(8, 6);
            this.tabControl2.Name = "tabControl2";
            this.tabControl2.SelectedIndex = 0;
            this.tabControl2.Size = new System.Drawing.Size(863, 276);
            this.tabControl2.TabIndex = 0;
            this.tabControl2.SelectedIndexChanged += new System.EventHandler(this.tabControl2_SelectedIndexChanged);
            // 
            // tabPage3
            // 
            this.tabPage3.Controls.Add(this.fecha);
            this.tabPage3.Controls.Add(this.label10);
            this.tabPage3.Controls.Add(this.label11);
            this.tabPage3.Controls.Add(this.label9);
            this.tabPage3.Controls.Add(this.label12);
            this.tabPage3.Controls.Add(this.label8);
            this.tabPage3.Controls.Add(this.rfcTr);
            this.tabPage3.Controls.Add(this.invoice);
            this.tabPage3.Controls.Add(this.resultado);
            this.tabPage3.Controls.Add(this.voucher);
            this.tabPage3.Controls.Add(this.button6);
            this.tabPage3.Controls.Add(this.ucGlobalSearchConfiguracion1);
            this.tabPage3.Location = new System.Drawing.Point(4, 22);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage3.Size = new System.Drawing.Size(855, 250);
            this.tabPage3.TabIndex = 0;
            this.tabPage3.Text = "Global Search";
            this.tabPage3.UseVisualStyleBackColor = true;
            // 
            // fecha
            // 
            this.fecha.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.fecha.Location = new System.Drawing.Point(496, 131);
            this.fecha.Margin = new System.Windows.Forms.Padding(2);
            this.fecha.Name = "fecha";
            this.fecha.Size = new System.Drawing.Size(96, 20);
            this.fecha.TabIndex = 42;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(458, 134);
            this.label10.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(37, 13);
            this.label10.TabIndex = 40;
            this.label10.Text = "Fecha";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(596, 134);
            this.label11.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(28, 13);
            this.label11.TabIndex = 40;
            this.label11.Text = "RFC";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(308, 133);
            this.label9.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(42, 13);
            this.label9.TabIndex = 40;
            this.label9.Text = "Invoice";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(5, 153);
            this.label12.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(55, 13);
            this.label12.TabIndex = 40;
            this.label12.Text = "Resultado";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(158, 134);
            this.label8.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(47, 13);
            this.label8.TabIndex = 40;
            this.label8.Text = "Voucher";
            // 
            // rfcTr
            // 
            this.rfcTr.Location = new System.Drawing.Point(628, 131);
            this.rfcTr.Margin = new System.Windows.Forms.Padding(2);
            this.rfcTr.Name = "rfcTr";
            this.rfcTr.Size = new System.Drawing.Size(124, 20);
            this.rfcTr.TabIndex = 41;
            // 
            // invoice
            // 
            this.invoice.Location = new System.Drawing.Point(359, 131);
            this.invoice.Margin = new System.Windows.Forms.Padding(2);
            this.invoice.Name = "invoice";
            this.invoice.Size = new System.Drawing.Size(95, 20);
            this.invoice.TabIndex = 41;
            // 
            // resultado
            // 
            this.resultado.Location = new System.Drawing.Point(8, 168);
            this.resultado.Margin = new System.Windows.Forms.Padding(2);
            this.resultado.Multiline = true;
            this.resultado.Name = "resultado";
            this.resultado.Size = new System.Drawing.Size(842, 77);
            this.resultado.TabIndex = 41;
            // 
            // voucher
            // 
            this.voucher.Location = new System.Drawing.Point(209, 131);
            this.voucher.Margin = new System.Windows.Forms.Padding(2);
            this.voucher.Name = "voucher";
            this.voucher.Size = new System.Drawing.Size(95, 20);
            this.voucher.TabIndex = 41;
            // 
            // button6
            // 
            this.button6.BackColor = System.Drawing.Color.DarkKhaki;
            this.button6.FlatAppearance.BorderSize = 0;
            this.button6.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button6.ForeColor = System.Drawing.Color.White;
            this.button6.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.button6.Location = new System.Drawing.Point(6, 127);
            this.button6.Margin = new System.Windows.Forms.Padding(2);
            this.button6.Name = "button6";
            this.button6.Size = new System.Drawing.Size(150, 24);
            this.button6.TabIndex = 39;
            this.button6.Text = "Probar cambio de Estado";
            this.button6.UseVisualStyleBackColor = false;
            this.button6.Click += new System.EventHandler(this.button6_Click);
            // 
            // ucGlobalSearchConfiguracion1
            // 
            this.ucGlobalSearchConfiguracion1.Location = new System.Drawing.Point(6, 6);
            this.ucGlobalSearchConfiguracion1.Name = "ucGlobalSearchConfiguracion1";
            this.ucGlobalSearchConfiguracion1.Size = new System.Drawing.Size(586, 116);
            this.ucGlobalSearchConfiguracion1.TabIndex = 0;
            // 
            // tabPage4
            // 
            this.tabPage4.Controls.Add(this.ucConfiguracionAdminPaq1);
            this.tabPage4.Location = new System.Drawing.Point(4, 22);
            this.tabPage4.Name = "tabPage4";
            this.tabPage4.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage4.Size = new System.Drawing.Size(855, 250);
            this.tabPage4.TabIndex = 1;
            this.tabPage4.Text = "AdminPaq";
            this.tabPage4.UseVisualStyleBackColor = true;
            // 
            // ucConfiguracionAdminPaq1
            // 
            this.ucConfiguracionAdminPaq1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ucConfiguracionAdminPaq1.Location = new System.Drawing.Point(3, 3);
            this.ucConfiguracionAdminPaq1.Name = "ucConfiguracionAdminPaq1";
            this.ucConfiguracionAdminPaq1.Size = new System.Drawing.Size(849, 244);
            this.ucConfiguracionAdminPaq1.TabIndex = 0;
            // 
            // tabPage5
            // 
            this.tabPage5.Location = new System.Drawing.Point(4, 22);
            this.tabPage5.Name = "tabPage5";
            this.tabPage5.Size = new System.Drawing.Size(855, 250);
            this.tabPage5.TabIndex = 2;
            this.tabPage5.Text = "Visual Manufacturing";
            this.tabPage5.UseVisualStyleBackColor = true;
            // 
            // tabPage6
            // 
            this.tabPage6.Location = new System.Drawing.Point(4, 22);
            this.tabPage6.Name = "tabPage6";
            this.tabPage6.Size = new System.Drawing.Size(855, 250);
            this.tabPage6.TabIndex = 3;
            this.tabPage6.Text = "SAP B1";
            this.tabPage6.UseVisualStyleBackColor = true;
            // 
            // tabPage8
            // 
            this.tabPage8.Controls.Add(this.button5);
            this.tabPage8.Controls.Add(this.log2);
            this.tabPage8.Controls.Add(this.button4);
            this.tabPage8.Controls.Add(this.button3);
            this.tabPage8.Controls.Add(this.archivo);
            this.tabPage8.Location = new System.Drawing.Point(4, 22);
            this.tabPage8.Name = "tabPage8";
            this.tabPage8.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage8.Size = new System.Drawing.Size(879, 288);
            this.tabPage8.TabIndex = 3;
            this.tabPage8.Text = "Global Search Tester";
            this.tabPage8.UseVisualStyleBackColor = true;
            // 
            // button5
            // 
            this.button5.Location = new System.Drawing.Point(101, 46);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(87, 20);
            this.button5.TabIndex = 4;
            this.button5.Text = "List DB\'s";
            this.button5.UseVisualStyleBackColor = true;
            this.button5.Click += new System.EventHandler(this.button5_Click);
            // 
            // log2
            // 
            this.log2.Location = new System.Drawing.Point(6, 72);
            this.log2.Multiline = true;
            this.log2.Name = "log2";
            this.log2.Size = new System.Drawing.Size(865, 210);
            this.log2.TabIndex = 3;
            // 
            // button4
            // 
            this.button4.Location = new System.Drawing.Point(8, 20);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(87, 20);
            this.button4.TabIndex = 2;
            this.button4.Text = "Archivo";
            this.button4.UseVisualStyleBackColor = true;
            this.button4.Click += new System.EventHandler(this.button4_Click);
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(8, 46);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(87, 20);
            this.button3.TabIndex = 2;
            this.button3.Text = "Crear Job";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // archivo
            // 
            this.archivo.Location = new System.Drawing.Point(101, 20);
            this.archivo.Name = "archivo";
            this.archivo.Size = new System.Drawing.Size(770, 20);
            this.archivo.TabIndex = 1;
            // 
            // timer1
            // 
            this.timer1.Interval = 1000;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // notifyIcon
            // 
            this.notifyIcon.Icon = ((System.Drawing.Icon)(resources.GetObject("notifyIcon.Icon")));
            this.notifyIcon.Text = "notifyIcon1";
            this.notifyIcon.Visible = true;
            this.notifyIcon.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.notifyIcon_MouseDoubleClick);
            // 
            // CFDI
            // 
            this.CFDI.HeaderText = "CFDI";
            this.CFDI.Name = "CFDI";
            this.CFDI.ReadOnly = true;
            this.CFDI.Width = 300;
            // 
            // folio
            // 
            this.folio.HeaderText = "Folio";
            this.folio.Name = "folio";
            this.folio.ReadOnly = true;
            // 
            // PDF
            // 
            this.PDF.HeaderText = "PDF";
            this.PDF.Name = "PDF";
            this.PDF.ReadOnly = true;
            this.PDF.Visible = false;
            // 
            // XML
            // 
            this.XML.HeaderText = "XML";
            this.XML.Name = "XML";
            this.XML.ReadOnly = true;
            this.XML.Visible = false;
            // 
            // FormatoAdicional
            // 
            this.FormatoAdicional.HeaderText = "FormatoAdicional";
            this.FormatoAdicional.Name = "FormatoAdicional";
            this.FormatoAdicional.ReadOnly = true;
            this.FormatoAdicional.Visible = false;
            // 
            // rfcEmisor
            // 
            this.rfcEmisor.HeaderText = "Emisor";
            this.rfcEmisor.Name = "rfcEmisor";
            this.rfcEmisor.ReadOnly = true;
            // 
            // rfcReceptor
            // 
            this.rfcReceptor.HeaderText = "Receptor";
            this.rfcReceptor.Name = "rfcReceptor";
            this.rfcReceptor.ReadOnly = true;
            // 
            // row_id
            // 
            this.row_id.HeaderText = "row_id";
            this.row_id.Name = "row_id";
            this.row_id.ReadOnly = true;
            this.row_id.Visible = false;
            // 
            // fechaFactura
            // 
            this.fechaFactura.HeaderText = "Fecha";
            this.fechaFactura.Name = "fechaFactura";
            this.fechaFactura.ReadOnly = true;
            // 
            // tipoComprobante
            // 
            this.tipoComprobante.HeaderText = "Tipo";
            this.tipoComprobante.Name = "tipoComprobante";
            this.tipoComprobante.ReadOnly = true;
            // 
            // OC
            // 
            this.OC.HeaderText = "OC";
            this.OC.Name = "OC";
            this.OC.ReadOnly = true;
            // 
            // Recibo
            // 
            this.Recibo.HeaderText = "Recibo";
            this.Recibo.Name = "Recibo";
            this.Recibo.ReadOnly = true;
            // 
            // frmPrincipal
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(887, 448);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.ejecutarAutomatico);
            this.Controls.Add(this.proxima);
            this.Controls.Add(this.tabControl1);
            this.Controls.Add(this.textoHora);
            this.Controls.Add(this.groupBox1);
            this.HelpButton = true;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MetroColor = System.Drawing.Color.Transparent;
            this.Name = "frmPrincipal";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Conector de Documentos FX";
            this.HelpButtonClicked += new System.ComponentModel.CancelEventHandler(this.frmPrincipal_HelpButtonClicked);
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.frmPrincipal_FormClosed);
            this.Resize += new System.EventHandler(this.frmPrincipal_Resize);
            ((System.ComponentModel.ISupportInitialize)(this.dtgrdGeneral)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage7.ResumeLayout(false);
            this.tabPage7.PerformLayout();
            this.tabPage2.ResumeLayout(false);
            this.tabControl2.ResumeLayout(false);
            this.tabPage3.ResumeLayout(false);
            this.tabPage3.PerformLayout();
            this.tabPage4.ResumeLayout(false);
            this.tabPage8.ResumeLayout(false);
            this.tabPage8.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView dtgrdGeneral;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.CheckBox ejecutarAutomatico;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox proxima;
        private System.Windows.Forms.CheckBox faltaSincronizar;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.ComboBox estado;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox uuid;
        private System.Windows.Forms.TextBox folioFiltro;
        private System.Windows.Forms.TextBox rfc;
        private System.Windows.Forms.DateTimePicker fin;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label textoHora;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.TabControl tabControl2;
        private System.Windows.Forms.TabPage tabPage3;
        private System.Windows.Forms.TabPage tabPage4;
        private System.Windows.Forms.TabPage tabPage5;
        private System.Windows.Forms.TabPage tabPage6;
        private ucGlobalSearchConfiguracion ucGlobalSearchConfiguracion1;
        private System.Windows.Forms.TabPage tabPage7;
        private System.Windows.Forms.TextBox log;
        private System.Windows.Forms.TabPage tabPage8;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.TextBox log2;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.TextBox archivo;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.DateTimePicker inicio;
        private System.Windows.Forms.NotifyIcon notifyIcon;
        private AdminPaq.ucConfiguracionAdminPaq ucConfiguracionAdminPaq1;
        private System.Windows.Forms.Button button5;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox voucher;
        private System.Windows.Forms.Button button6;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox invoice;
        private System.Windows.Forms.DateTimePicker fecha;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.TextBox rfcTr;
        private System.Windows.Forms.TextBox resultado;
        private System.Windows.Forms.ComboBox filtroTipo;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.DataGridViewTextBoxColumn CFDI;
        private System.Windows.Forms.DataGridViewTextBoxColumn folio;
        private System.Windows.Forms.DataGridViewTextBoxColumn PDF;
        private System.Windows.Forms.DataGridViewTextBoxColumn XML;
        private System.Windows.Forms.DataGridViewTextBoxColumn FormatoAdicional;
        private System.Windows.Forms.DataGridViewTextBoxColumn rfcEmisor;
        private System.Windows.Forms.DataGridViewTextBoxColumn rfcReceptor;
        private System.Windows.Forms.DataGridViewTextBoxColumn row_id;
        private System.Windows.Forms.DataGridViewTextBoxColumn fechaFactura;
        private System.Windows.Forms.DataGridViewTextBoxColumn tipoComprobante;
        private System.Windows.Forms.DataGridViewTextBoxColumn OC;
        private System.Windows.Forms.DataGridViewTextBoxColumn Recibo;
    }
}