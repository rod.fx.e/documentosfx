﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using Newtonsoft.Json.Linq;
using System.Linq;
using System.Data.Entity.Validation;
using ModeloSincronizacion;

namespace ConectorFX.AdminPaq
{
    public partial class ucConfiguracionAdminPaq : UserControl
    {
        public ucConfiguracionAdminPaq()
        {
            InitializeComponent();
        }

        private void cargar()
        {
            try
            {
                ModeloSincronizacion.ModeloSincronizacionContainer dbContext = new ModeloSincronizacion.ModeloSincronizacionContainer();

                string bustartr = buscar.Text;
                int toptr = int.Parse(top.Text);
                var result = (from b in dbContext.AdminPaqConfigSet
                             .Where(a => a.rfc.Contains(bustartr))
                                             .OrderBy(a => a.rfc)
                             select b).Take(toptr);

                List<AdminPaqConfig> dt = result.ToList();
                dtg.Rows.Clear();
                foreach (AdminPaqConfig registro in dt)
                {
                    if (registro != null)
                    {
                        int n = dtg.Rows.Add();
                        dtg.Rows[n].Tag = registro;
                        dtg.Rows[n].Cells["rfc"].Value = registro.rfc;
                        dtg.Rows[n].Cells["directorio"].Value = registro.directorio;
                        dtg.Rows[n].Cells["codigoServicio"].Value = registro.codigo;
                        dtg.Rows[n].Cells["rfcProveedor"].Value = registro.rfcProveedor;
                        dtg.Rows[n].Cells["bdSQL"].Value = registro.bdSQL;
                        dtg.Rows[n].Cells["tipo"].Value = registro.tipo;
                    }

                }
                dbContext.Dispose();
                this.dtg.AutoResizeColumns( DataGridViewAutoSizeColumnsMode.AllCells); 
            }
            catch (DbEntityValidationException error)
            {
                foreach (var eve in error.EntityValidationErrors)
                {
                    foreach (var ve in eve.ValidationErrors)
                    {
                        MessageBox.Show(this, ve.PropertyName + " Error: " + ve.ErrorMessage, Application.ProductName
                            , MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            frmConfiguracionAdminPaqAgregar o = new frmConfiguracionAdminPaqAgregar();
            o.ShowDialog();
            cargar();
        }

        private void button1_Click(object sender, EventArgs e)
        {

        }
        
        private void dtg_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            cargar_registro(e);
        }

        private void cargar_registro(DataGridViewCellEventArgs e)
        {
            if (e.RowIndex > -1)
            {
                AdminPaqConfig registro = (AdminPaqConfig)dtg.Rows[e.RowIndex].Tag;
                frmConfiguracionAdminPaqAgregar o = new frmConfiguracionAdminPaqAgregar(registro);
                o.ShowDialog();
                cargar();
            }
        }

        private void ucConfiguracionAdminPaq_Enter(object sender, EventArgs e)
        {
            //Boolean isInWpfDesignerMode = (LicenseManager.UsageMode == LicenseUsageMode.Designtime);
            //if (!isInWpfDesignerMode)
            //{
            //    cargar();
            //}
            
        }

        private void button3_Click(object sender, EventArgs e)
        {
            cargar();
        }

        private void button1_Click_1(object sender, EventArgs e)
        {
            eliminar();
        }

        private void eliminar()
        {
            dtg.EndEdit();
            if (dtg.SelectedCells.Count > 0)
            {
                DialogResult Resultado = MessageBox.Show("¿Desea eliminar las seleccionada?", Application.ProductName + "-" + Application.ProductVersion, MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                if (Resultado.ToString() != "Yes")
                {
                    return;
                }
                foreach (DataGridViewRow drv in dtg.SelectedRows)
                {
                    AdminPaqConfig oRegistroTMP = (AdminPaqConfig)drv.Tag;
                    ModeloSincronizacion.ModeloSincronizacionContainer dbContext = new ModeloSincronizacion.ModeloSincronizacionContainer();

                    AdminPaqConfig oRegistro = dbContext.AdminPaqConfigSet
                        .Where(a => a.Id == oRegistroTMP.Id)
                        .FirstOrDefault();


                    dbContext.AdminPaqConfigSet.Attach(oRegistro);
                    dbContext.AdminPaqConfigSet.Remove(oRegistro);
                    dbContext.SaveChanges();
                    dtg.Rows.Remove(drv);
                }
            }
            else
            {
                MessageBox.Show("Seleccione alguna línea.", Application.ProductName + "-" + Application.ProductVersion, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }
    }
}
