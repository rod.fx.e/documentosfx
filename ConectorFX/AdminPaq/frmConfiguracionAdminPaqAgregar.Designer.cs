#region Copyright Syncfusion Inc. 2001 - 2017
// Copyright Syncfusion Inc. 2001 - 2017. All rights reserved.
// Use of this code is subject to the terms of our license.
// A copy of the current license can be obtained at any time by e-mailing
// licensing@syncfusion.com. Any infringement will be prosecuted under
// applicable laws. 
#endregion
namespace ConectorFX.AdminPaq
{
    partial class frmConfiguracionAdminPaqAgregar
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmConfiguracionAdminPaqAgregar));
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.rfc = new System.Windows.Forms.TextBox();
            this.codigoServicio = new System.Windows.Forms.TextBox();
            this.button1 = new System.Windows.Forms.Button();
            this.directorio = new System.Windows.Forms.TextBox();
            this.btnDeleteItem = new Syncfusion.Windows.Forms.ButtonAdv();
            this.buttonAdv1 = new Syncfusion.Windows.Forms.ButtonAdv();
            this.btnAddItem = new Syncfusion.Windows.Forms.ButtonAdv();
            this.label3 = new System.Windows.Forms.Label();
            this.rfcProveedor = new System.Windows.Forms.TextBox();
            this.verPassword = new System.Windows.Forms.CheckBox();
            this.tipo = new System.Windows.Forms.ComboBox();
            this.button5 = new System.Windows.Forms.Button();
            this.label19 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.password = new System.Windows.Forms.TextBox();
            this.usuario = new System.Windows.Forms.TextBox();
            this.bdSQL = new System.Windows.Forms.TextBox();
            this.label18 = new System.Windows.Forms.Label();
            this.servidor = new System.Windows.Forms.TextBox();
            this.label16 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(39, 54);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(72, 13);
            this.label1.TabIndex = 3;
            this.label1.Text = "RFC Empresa";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(15, 80);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(96, 13);
            this.label2.TabIndex = 5;
            this.label2.Text = "C�digo de Servicio";
            // 
            // contextMenuStrip1
            // 
            this.contextMenuStrip1.Name = "contextMenuStrip1";
            this.contextMenuStrip1.Size = new System.Drawing.Size(61, 4);
            // 
            // rfc
            // 
            this.rfc.Location = new System.Drawing.Point(117, 51);
            this.rfc.Name = "rfc";
            this.rfc.Size = new System.Drawing.Size(131, 20);
            this.rfc.TabIndex = 4;
            // 
            // codigoServicio
            // 
            this.codigoServicio.Location = new System.Drawing.Point(117, 77);
            this.codigoServicio.Name = "codigoServicio";
            this.codigoServicio.Size = new System.Drawing.Size(131, 20);
            this.codigoServicio.TabIndex = 6;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(36, 102);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 7;
            this.button1.Text = "Directorio";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // directorio
            // 
            this.directorio.Location = new System.Drawing.Point(117, 103);
            this.directorio.Name = "directorio";
            this.directorio.Size = new System.Drawing.Size(428, 20);
            this.directorio.TabIndex = 8;
            // 
            // btnDeleteItem
            // 
            this.btnDeleteItem.Appearance = Syncfusion.Windows.Forms.ButtonAppearance.Metro;
            this.btnDeleteItem.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(219)))), ((int)(((byte)(94)))), ((int)(((byte)(94)))));
            this.btnDeleteItem.BeforeTouchSize = new System.Drawing.Size(94, 28);
            this.btnDeleteItem.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnDeleteItem.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnDeleteItem.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnDeleteItem.IsBackStageButton = false;
            this.btnDeleteItem.Location = new System.Drawing.Point(202, 12);
            this.btnDeleteItem.Name = "btnDeleteItem";
            this.btnDeleteItem.Size = new System.Drawing.Size(94, 28);
            this.btnDeleteItem.TabIndex = 2;
            this.btnDeleteItem.Text = "Eliminar";
            this.btnDeleteItem.Click += new System.EventHandler(this.btnDeleteItem_Click);
            // 
            // buttonAdv1
            // 
            this.buttonAdv1.Appearance = Syncfusion.Windows.Forms.ButtonAppearance.Metro;
            this.buttonAdv1.BackColor = System.Drawing.Color.SteelBlue;
            this.buttonAdv1.BeforeTouchSize = new System.Drawing.Size(91, 28);
            this.buttonAdv1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonAdv1.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonAdv1.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.buttonAdv1.IsBackStageButton = false;
            this.buttonAdv1.Location = new System.Drawing.Point(9, 12);
            this.buttonAdv1.Name = "buttonAdv1";
            this.buttonAdv1.Size = new System.Drawing.Size(91, 28);
            this.buttonAdv1.TabIndex = 0;
            this.buttonAdv1.Text = "Limpiar";
            this.buttonAdv1.Click += new System.EventHandler(this.buttonAdv1_Click);
            // 
            // btnAddItem
            // 
            this.btnAddItem.Appearance = Syncfusion.Windows.Forms.ButtonAppearance.Metro;
            this.btnAddItem.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(153)))), ((int)(((byte)(181)))), ((int)(((byte)(105)))));
            this.btnAddItem.BeforeTouchSize = new System.Drawing.Size(91, 28);
            this.btnAddItem.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnAddItem.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAddItem.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnAddItem.IsBackStageButton = false;
            this.btnAddItem.Location = new System.Drawing.Point(105, 12);
            this.btnAddItem.Name = "btnAddItem";
            this.btnAddItem.Size = new System.Drawing.Size(91, 28);
            this.btnAddItem.TabIndex = 1;
            this.btnAddItem.Text = "Guardar";
            this.btnAddItem.Click += new System.EventHandler(this.btnAddItem_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(31, 133);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(80, 13);
            this.label3.TabIndex = 9;
            this.label3.Text = "RFC Proveedor";
            // 
            // rfcProveedor
            // 
            this.rfcProveedor.Location = new System.Drawing.Point(117, 129);
            this.rfcProveedor.Name = "rfcProveedor";
            this.rfcProveedor.Size = new System.Drawing.Size(131, 20);
            this.rfcProveedor.TabIndex = 10;
            // 
            // verPassword
            // 
            this.verPassword.AutoSize = true;
            this.verPassword.Location = new System.Drawing.Point(257, 262);
            this.verPassword.Name = "verPassword";
            this.verPassword.Size = new System.Drawing.Size(98, 17);
            this.verPassword.TabIndex = 496;
            this.verPassword.Text = "Ver contrase�a";
            this.verPassword.UseVisualStyleBackColor = true;
            this.verPassword.CheckedChanged += new System.EventHandler(this.verPassword_CheckedChanged);
            // 
            // tipo
            // 
            this.tipo.FormattingEnabled = true;
            this.tipo.Items.AddRange(new object[] {
            "SQLSERVER",
            "ACCESS",
            "ORACLE"});
            this.tipo.Location = new System.Drawing.Point(117, 155);
            this.tipo.Name = "tipo";
            this.tipo.Size = new System.Drawing.Size(131, 21);
            this.tipo.TabIndex = 485;
            // 
            // button5
            // 
            this.button5.Location = new System.Drawing.Point(257, 154);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(98, 23);
            this.button5.TabIndex = 495;
            this.button5.Text = "Probar Conexi�n";
            this.button5.UseVisualStyleBackColor = true;
            this.button5.Click += new System.EventHandler(this.button5_Click);
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(34, 263);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(53, 13);
            this.label19.TabIndex = 494;
            this.label19.Text = "Password";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(34, 237);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(43, 13);
            this.label17.TabIndex = 493;
            this.label17.Text = "Usuario";
            // 
            // password
            // 
            this.password.Location = new System.Drawing.Point(117, 260);
            this.password.Name = "password";
            this.password.Size = new System.Drawing.Size(134, 20);
            this.password.TabIndex = 492;
            this.password.UseSystemPasswordChar = true;
            // 
            // usuario
            // 
            this.usuario.Location = new System.Drawing.Point(117, 234);
            this.usuario.Name = "usuario";
            this.usuario.Size = new System.Drawing.Size(238, 20);
            this.usuario.TabIndex = 491;
            // 
            // bdSQL
            // 
            this.bdSQL.Location = new System.Drawing.Point(117, 208);
            this.bdSQL.Name = "bdSQL";
            this.bdSQL.Size = new System.Drawing.Size(238, 20);
            this.bdSQL.TabIndex = 490;
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(34, 211);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(77, 13);
            this.label18.TabIndex = 489;
            this.label18.Text = "Base de Datos";
            // 
            // servidor
            // 
            this.servidor.Location = new System.Drawing.Point(117, 182);
            this.servidor.Name = "servidor";
            this.servidor.Size = new System.Drawing.Size(238, 20);
            this.servidor.TabIndex = 488;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(34, 185);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(46, 13);
            this.label16.TabIndex = 487;
            this.label16.Text = "Servidor";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(34, 159);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(28, 13);
            this.label14.TabIndex = 486;
            this.label14.Text = "Tipo";
            // 
            // frmConfiguracionAdminPaqAgregar
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(586, 292);
            this.Controls.Add(this.verPassword);
            this.Controls.Add(this.tipo);
            this.Controls.Add(this.button5);
            this.Controls.Add(this.label19);
            this.Controls.Add(this.label17);
            this.Controls.Add(this.password);
            this.Controls.Add(this.usuario);
            this.Controls.Add(this.bdSQL);
            this.Controls.Add(this.label18);
            this.Controls.Add(this.servidor);
            this.Controls.Add(this.label16);
            this.Controls.Add(this.label14);
            this.Controls.Add(this.btnDeleteItem);
            this.Controls.Add(this.buttonAdv1);
            this.Controls.Add(this.btnAddItem);
            this.Controls.Add(this.directorio);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.codigoServicio);
            this.Controls.Add(this.rfcProveedor);
            this.Controls.Add(this.rfc);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MetroColor = System.Drawing.Color.Transparent;
            this.Name = "frmConfiguracionAdminPaqAgregar";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Configuraci�n AdminPaq";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip1;
        private System.Windows.Forms.TextBox rfc;
        private System.Windows.Forms.TextBox codigoServicio;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.TextBox directorio;
        private Syncfusion.Windows.Forms.ButtonAdv btnDeleteItem;
        private Syncfusion.Windows.Forms.ButtonAdv buttonAdv1;
        private Syncfusion.Windows.Forms.ButtonAdv btnAddItem;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox rfcProveedor;
        private System.Windows.Forms.CheckBox verPassword;
        private System.Windows.Forms.ComboBox tipo;
        private System.Windows.Forms.Button button5;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.TextBox password;
        private System.Windows.Forms.TextBox usuario;
        private System.Windows.Forms.TextBox bdSQL;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.TextBox servidor;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label14;
    }
}