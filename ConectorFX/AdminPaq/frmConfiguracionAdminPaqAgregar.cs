#region Copyright Syncfusion Inc. 2001 - 2017
// Copyright Syncfusion Inc. 2001 - 2017. All rights reserved.
// Use of this code is subject to the terms of our license.
// A copy of the current license can be obtained at any time by e-mailing
// licensing@syncfusion.com. Any infringement will be prosecuted under
// applicable laws. 
#endregion
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using ModeloSincronizacion;
using System.Text;
using System.Windows.Forms;
using System.Linq;
using System.Data.Entity.Validation;
using System.IO;
using Generales;

namespace ConectorFX.AdminPaq
{
    public partial class frmConfiguracionAdminPaqAgregar : Syncfusion.Windows.Forms.MetroForm
    {
        bool modificado = false;
        public string productop = "";
        public AdminPaqConfig oRegistro;
        public frmConfiguracionAdminPaqAgregar()
        {
            InitializeComponent();
            limpiar();
        }
        public frmConfiguracionAdminPaqAgregar(AdminPaqConfig oRegistrop)
        {
            InitializeComponent();
            limpiar();
            oRegistro = oRegistrop;
            cargar();
        }

        private void cargar()
        {
            codigoServicio.Text = oRegistro.codigo;
            rfc.Text = oRegistro.rfc;
            directorio.Text = oRegistro.directorio;
            rfcProveedor.Text = oRegistro.rfcProveedor;
            bdSQL.Text = oRegistro.bdSQL;
            servidor.Text = oRegistro.servidor;
            tipo.Text = oRegistro.tipo;
            usuario.Text = oRegistro.usuario;
            password.Text = oRegistro.password;

        }

        private void btnAddItem_Click(object sender, EventArgs e)
        {
            guardar();
        }

        private void buttonAdv1_Click(object sender, EventArgs e)
        {
            limpiar();
        }

        private void btnDeleteItem_Click(object sender, EventArgs e)
        {
            eliminar();
        }

        #region metodos
        public void limpiar()
        {
            if (modificado)
            {

                DialogResult Resultado = MessageBox.Show(this, "�Desea guardar los cambios?", Application.ProductName + "-" + Application.ProductVersion, MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                if (Resultado.ToString() == "Yes")
                {
                    guardar();
                }
            }
            oRegistro = new AdminPaqConfig();
            modificado = false;
            foreach (var c in this.Controls)
            {
                if (c is TextBox)
                {
                    ((TextBox)c).Text = "";
                }
            }
        }
        public void eliminar()
        {
            DialogResult Resultado = MessageBox.Show(this, "�Esta seguro de eliminar?", Application.ProductName + "-" + Application.ProductVersion, MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            if (Resultado.ToString() != "Yes")
            {
                return;
            }
            ModeloSincronizacion.ModeloSincronizacionContainer dbContext = new ModeloSincronizacion.ModeloSincronizacionContainer();
            dbContext.AdminPaqConfigSet.Attach(oRegistro);
            dbContext.AdminPaqConfigSet.Remove(oRegistro);
            dbContext.SaveChanges();
            MessageBox.Show(this, "Eliminado.", Application.ProductName + "-" + Application.ProductVersion, MessageBoxButtons.OK, MessageBoxIcon.Information);
            limpiar();
        }
        public void guardar()
        {
            ModeloSincronizacion.ModeloSincronizacionContainer dbContext = new ModeloSincronizacion.ModeloSincronizacionContainer();
            try
            {
                AdminPaqConfig registro = new AdminPaqConfig();
                modificado = false;
                if (oRegistro.Id != 0)
                {
                    //Cargar registro
                    int valor = oRegistro.Id;
                    registro = (from r in dbContext.AdminPaqConfigSet.Where
                                            (a => a.Id == valor)
                                select r).FirstOrDefault();
                    modificado = true;
                }
                registro.rfc = rfc.Text;
                registro.directorio = directorio.Text;
                registro.codigo = codigoServicio.Text;
                registro.rfcProveedor = rfcProveedor.Text;
                registro.bdSQL= bdSQL.Text;

                registro.servidor = servidor.Text;
                registro.tipo = tipo.Text;
                registro.usuario = usuario.Text;
                registro.password = password.Text;

                if (!modificado)
                {
                    dbContext.AdminPaqConfigSet.Add(registro);
                }
                else
                {
                    dbContext.AdminPaqConfigSet.Attach(registro);
                    dbContext.Entry(registro).State = System.Data.Entity.EntityState.Modified;
                }

                dbContext.SaveChanges();
                oRegistro = registro;
                modificado = false;
                MessageBox.Show(this, "Datos guardados", Application.ProductName + "-" + Application.ProductVersion, MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            catch (DbEntityValidationException e)
            {
                foreach (var eve in e.EntityValidationErrors)
                {
                    foreach (var ve in eve.ValidationErrors)
                    {
                        MessageBox.Show(this, ve.PropertyName + " Error: " + ve.ErrorMessage, Application.ProductName
                            , MessageBoxButtons.OK, MessageBoxIcon.Error);
                        return;
                    }
                }
            }
        }
        #endregion

        private void button1_Click(object sender, EventArgs e)
        {
            using (var fbd = new FolderBrowserDialog())
            {
                DialogResult result = fbd.ShowDialog();
                directorio.Text = fbd.SelectedPath.ToString();
            }
        }

        private void button5_Click(object sender, EventArgs e)
        {
            probar();
        }

        public void probar()
        {
            cCONEXCION ocCONEXCION = new cCONEXCION(tipo.Text, servidor.Text, bdSQL.Text, usuario.Text, password.Text);
            if (ocCONEXCION.CrearConexion())
            {
                MessageBox.Show("Conexi�n exitosa");
            }
            else
            {
                MessageBox.Show("Verifique el server.");
            }
        }

        private void verPassword_CheckedChanged(object sender, EventArgs e)
        {
            if (verPassword.Checked)
            {
                password.UseSystemPasswordChar = false;
            }
            else
            {
                password.UseSystemPasswordChar = true;
            }
        }
    }
}
