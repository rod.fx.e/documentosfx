﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using ConectorFX.GlobalSearch;

namespace ConectorFX
{
    public partial class ucGlobalSearchConfiguracion : UserControl
    {
        string path = "";
        public ucGlobalSearchConfiguracion()
        {
            InitializeComponent();
            path = System.AppDomain.CurrentDomain.BaseDirectory + "\\GlobalSearch.cnfg";
            cargarArchivo();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            guardar();
        }

        private void cargarArchivo()
        {
            if (System.IO.File.Exists(path))
            {
                using (Stream stream = System.IO.File.Open(path, FileMode.Open))
                {
                    var binaryFormatter = new System.Runtime.Serialization.Formatters.Binary.BinaryFormatter();
                    try
                    {
                        cSqUsuario objeto = (cSqUsuario)binaryFormatter.Deserialize(stream);
                        servidor.Text = objeto.servidor;
                        password.Text = objeto.password;
                        usuario.Text = objeto.usuario;
                        databaseId.Text = objeto.databaseId;
                        archiveId.Text = objeto.archiveId;
                    }
                    catch
                    {

                    }
                }
            }

        }

        private void guardar()
        {


            cSqUsuario ocSqUsuario = new cSqUsuario();
            ocSqUsuario.servidor = servidor.Text;
            ocSqUsuario.usuario = usuario.Text;
            ocSqUsuario.password = password.Text;
            ocSqUsuario.databaseId = databaseId.Text;
            ocSqUsuario.archiveId = archiveId.Text;
            using (Stream stream = System.IO.File.Open(path, FileMode.Create))
            {
                var binaryFormatter = new System.Runtime.Serialization.Formatters.Binary.BinaryFormatter();
                binaryFormatter.Serialize(stream, ocSqUsuario);
            }

            MessageBox.Show("Datos guardados.");
        }

        private void button2_Click(object sender, EventArgs e)
        {
            button2.BackColor = Color.RoyalBlue;
            globalSearch o = new globalSearch();
            try
            {
                o.GetLicense();
                button2.BackColor = Color.LawnGreen;
            }
            catch(Exception error)
            {
                button2.BackColor = Color.Red;
                MessageBox.Show(error.Message.ToString());
            }
            
        }
    }
}
