﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using TipoCambioMultiple.Clases;

namespace TipoCambioMultiple
{
    public partial class Principal : Form
    {
        string Moneda = string.Empty;
        string Entity = string.Empty;
        public Principal(bool automatico = false)
        {
            InitializeComponent();
            CargarMoneda();
            Inicio.Value = DateTime.Now;
            if (automatico)
            {
                Cargar();
                Sincronizar();
            }
        }

        private void CargarMoneda()
        {
            try
            {
                Moneda = ConfigurationManager.AppSettings["Moneda"].ToString();
            }
            catch
            {

            }
            try
            {
                Entity = ConfigurationManager.AppSettings["Entity"].ToString();
            }
            catch
            {

            }
        }

        #region Metodos
        private void Cargar()
        {
            try
            {
                Banxico OBanxico = new Banxico();
                Informacion.Text = "Cargando...";
                Informacion.ForeColor = Color.Black;
                decimal TipoCambioTr = OBanxico.TipoCambio(Inicio.Value);
                TipoCambio.Text = TipoCambioTr.ToString("N4");
                Informacion.Text = "Fin";
            }
            catch (Exception Error)
            {
                Informacion.Text = "Error en carga" + Environment.NewLine
                    + Error.Message;
                EmailService.Send("Documentos FX: Error - Principal - 59 - Fecha: " + DateTime.Now.ToString("yyyy-MM-dd"), Error);
                Informacion.ForeColor = Color.Red;
            }
        }

        private void Sincronizar()
        {
            string Sql = string.Empty;
            try
            {
                Conexion OData = new Conexion();
                string fechaActual = DateTime.Now.ToString("yyyy-MM-dd HH:mm");
                Informacion.Text = "Cargando ERP ..";
                Informacion.ForeColor = Color.Black;
                Sql = @" SELECT TOP 1 * 
                FROM CURRENCY_EXCHANGE 
                WHERE [CURRENCY_ID]='" + Moneda + "' AND DATEDIFF(d,[EFFECTIVE_DATE],GETDATE())=0 ";
                DataTable oDataTable = OData.EjecutarConsulta(Sql);
                if (oDataTable != null)
                {
                    decimal SellRate = decimal.Parse(TipoCambio.Text);
                    if (SellRate == 0)
                    {
                        return;
                    }
                    if (oDataTable.Rows.Count == 0)
                    {
                        Banxico OBanxico = new Banxico();

                        decimal BuyRate = 1 / SellRate;
                        //Sql = @"
                        //INSERT INTO [dbo].[CURRENCY_EXCHANGE]
                        //           ([CURRENCY_ID]
                        //           ,[EFFECTIVE_DATE]
                        //           ,[SELL_RATE]
                        //           ,[BUY_RATE]
                        //           ,[ENTITY_ID])
                        //     VALUES
                        //           ('USD'
                        //           , CONVERT(DATETIME, '" + DateTime.Now.ToString("yyyy-MM-dd") + @" 00:00:00', 102)
                        //           ," + SellRate.ToString("N4") + @"
                        //           ," + BuyRate.ToString("N4") + @"
                        //           ,'OLB');
                        //";

                        Sql = @"
                        INSERT INTO [dbo].[CURRENCY_EXCHANGE]
                                   ([CURRENCY_ID]
                                   ,[EFFECTIVE_DATE]
                                   ,[SELL_RATE]
                                   ,[BUY_RATE]
                                    ,[ENTITY_ID]
                                   )
                             VALUES
                                   ('" + Moneda + @"'
                                   , CONVERT(DATETIME, '" + DateTime.Now.ToString("yyyy-MM-dd") + @" 00:00:00', 102)
                                   ," + SellRate.ToString("N4") + @"
                                   ," + BuyRate.ToString("N4") + @"
                                    ,'" + Entity + @"'
                                   );
                        ";

                        OData.EjecutarConsulta(Sql);

                        EmailService.Send("Documentos FX: Exito Tipo de Cambio " + SellRate.ToString("#.####") + "  - Banxico - Fecha: " + fechaActual
                        , null, "Tipo de Cambio " + SellRate.ToString("#.####") + " Cargado exitosamente.");
                    }
                    else
                    {
                        EmailService.Send("Documentos FX: Existe un tipo de cambio cargado. Tipo de Cambio: " + SellRate.ToString("#.####") + "  - Banxico - Fecha: " + fechaActual
                        , null, "Tipo de Cambio " + SellRate.ToString("#.####") + " existe cargado manualmente.");
                    }
                }
                Informacion.Text = "Finalizado";
            }
            catch (Exception Error)
            {
                Informacion.Text = "Error en ERP ";
                Informacion.Text += Environment.NewLine + Sql + Environment.NewLine + Error.Message.ToString();
                if (Error.InnerException!=null)
                {
                    Informacion.Text += Environment.NewLine + Error.InnerException.Message.ToString();
                }                
                Informacion.ForeColor = Color.Red;
            }

        }
        #endregion

        private void button1_Click(object sender, EventArgs e)
        {
            Cargar();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            Sincronizar();
        }
    }
}
