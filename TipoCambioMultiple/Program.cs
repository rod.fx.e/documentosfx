﻿using Generales;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TipoCambioMultiple
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main(string[] args)
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            try
            {
                if (args.Length == 1)
                {
                    Application.Run(new Principal(true));
                    Application.Exit();
                }
                else
                {
                    Application.Run(new Principal());
                }

            }
            catch (Exception Error)
            {
                ErrorFX.mostrar(Error, false, true, "Program - 42 - ");
            }

        }
    }
}
