﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TipoCambioMultiple.Clases
{
    public class ConfigurationEnvio
    {
        public string UserName { get; set; }
        public string Password { get; set; }
        public string Smtp { get; set; }
        public int Port { get; set; }
        public string ToAddress { get; set; }
    }
}
