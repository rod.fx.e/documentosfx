﻿using Newtonsoft.Json;
using ServiciosWeb.Class;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Web;
using System.Web.Mvc;
using System.IO;
using System.Security.Cryptography;
using System.Text;

namespace ServiciosWeb.Controllers
{
    public class DescargaController : Controller
    {
        public JsonStringResult certificado(string rfc, string cer64, string key64, string password)
        {
            try
            {
                X509Certificate2 certificate = new X509Certificate2();
                
                

                string mensaje = "Archivo no validado";
                bool status = false;

                return new JsonStringResult(JsonConvert.SerializeObject(
                    new
                    {
                        status,
                        mensaje
                    }
                ));
            }
            catch (Exception e)
            {
                string mensaje = e.Message.ToString();
                if (e.InnerException != null)
                {
                    mensaje += Environment.NewLine + e.InnerException.Message.ToString();
                }
                return new JsonStringResult(JsonConvert.SerializeObject(
                    new
                    {
                        status = false,
                        mensaje
                    }
                ));
            }
        }


        public JsonStringResult certificateData(string rfc)
        {
            try
            {
                X509Certificate2 certificate = ObtenerX509Certificado(rfc);
                string certificateBase64 = Convert.ToBase64String(certificate.RawData);

                return new JsonStringResult(JsonConvert.SerializeObject(
                    new
                    {
                        status=true,
                        certificate.SerialNumber,
                        certificate.Issuer,
                        certificateBase64
                    }
                ));
            }
            catch (Exception e)
            {
                string mensaje = e.Message.ToString();
                if (e.InnerException != null)
                {
                    mensaje += Environment.NewLine + e.InnerException.Message.ToString();
                }
                return new JsonStringResult(JsonConvert.SerializeObject(
                    new
                    {
                        status = false,
                        mensaje
                    }
                ));
            }
        }

        private static X509Certificate2 ObtenerX509Certificado(string rfc)
        {
            string password = "abc123";
            byte[] pfx = System.IO.File.ReadAllBytes(@"C:\DescargaCFDI\pfx\EAUR810302714.pfx");
            X509Certificate2 certificate = new X509Certificate2();
            return new X509Certificate2(pfx, password, X509KeyStorageFlags.MachineKeySet |
                            X509KeyStorageFlags.PersistKeySet |
                            X509KeyStorageFlags.Exportable);
        }

        [HttpPost]
        [ValidateInput(false)]
        public JsonStringResult signature(string rfc, string digestValue)
        {
            bool status = true;
            try
            {

                X509Certificate2 certificate = ObtenerX509Certificado(rfc);
                string signature = Sign(digestValue, certificate);                
                return new JsonStringResult(JsonConvert.SerializeObject(
                    new
                    {
                        status,
                        signature
                    }
                ));
            }
            catch (Exception e)
            {
                status = false;
                string mensaje = e.Message.ToString();
                if (e.InnerException != null)
                {
                    mensaje += Environment.NewLine + e.InnerException.Message.ToString();
                }
                return new JsonStringResult(JsonConvert.SerializeObject(
                    new
                    {
                        status,
                        mensaje
                    }
                ));
            }
        }

        private string Sign(string sourceData, X509Certificate2 certificate)
        {
            byte[] data = GetBytes(sourceData);
            byte[] signature = null;

            using (RSA rsaCryptoServiceProvider = certificate.GetRSAPrivateKey())
            {
                signature = rsaCryptoServiceProvider.SignData(data, HashAlgorithmName.SHA1, RSASignaturePadding.Pkcs1);
            }
            return System.Convert.ToBase64String(signature);
        }

        private byte[] GetBytes(string sourceData)
        {
            return Encoding.Default.GetBytes(sourceData);
        }
    }
}