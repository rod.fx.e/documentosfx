﻿using Newtonsoft.Json;
using ServiciosWeb.Class;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ServiciosWeb.Controllers
{
    public class ServicioController : Controller
    {
        // GET: Servicio
        public ActionResult Index()
        {
            return View();
        }
        public JsonStringResult descargar()
        { 
            try
            {
                string url = "http://omawww.sat.gob.mx/cifras_sat/Documents/Listado_Completo_69-B.csv";
                string mensaje = "Archivo no validado";
                bool status = false;

                return new JsonStringResult(JsonConvert.SerializeObject(
                    new
                    {
                        status,
                        mensaje
                    }
                ));
            }
            catch (Exception e)
            {
                string mensaje = e.Message.ToString();
                if (e.InnerException != null)
                {
                    mensaje += Environment.NewLine + e.InnerException.Message.ToString();
                }
                return new JsonStringResult(JsonConvert.SerializeObject(
                    new
                    {
                        status = false,
                        mensaje
                    }
                ));
            }
        }


        public JsonStringResult validar(string rfc)
        {
            try
            {
                bool efos = false;
                bool edos = false;
                cEDO ocEDO = new cEDO();
                //Buscar 
                if (rfc.Equals("TAP100922DP3"))
                {
                    efos = true;
                    edos = false;
                }

                if (rfc.Equals("AEGS550417KD9"))
                {
                    efos = false;
                    edos = true;
                }

                if (rfc.Equals("VCL020419DQ5"))
                {
                    efos = true;
                    edos = true;
                }


                return new JsonStringResult(JsonConvert.SerializeObject(
                    new
                    {
                        efos,
                        edos,
                        informacion = ocEDO
                    }
                ));
            }
            catch (Exception e)
            {
                string mensaje = e.Message.ToString();
                if (e.InnerException != null)
                {
                    mensaje += Environment.NewLine + e.InnerException.Message.ToString();
                }
                return new JsonStringResult(JsonConvert.SerializeObject(
                    new
                    {
                        status = false,
                        mensaje
                    }
                ));
            }
        }

    }
}