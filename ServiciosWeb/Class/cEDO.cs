﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ServiciosWeb.Class
{
    public class cEDO
    {
        public string NombreContribuyente { get; set; }
        public string Situacioncontribuyente { get; set; }
        public string PublicacionpaginaSATpresuntos { get; set; }
        public string Numerofechaoficioglobalpresuncion { get; set; }
        public string PublicacionDOFpresuntos { get; set; }
        public string PublicacionpaginaSATdesvirtuados { get; set; }
        public string Numerofechaoficioglobalcontribuyentesdesvirtuaron { get; set; }
        public string PublicacionDOFdesvirtuados { get; set; }
        public string Numerofechaoficioglobaldefinitivos { get; set; }
        public string PublicacionpaginaSATdefinitivos { get; set; }
        public string PublicacionDOFdefinitivos { get; set; }
        public string Numerofechaoficioglobalsentenciaavorable { get; set; }
        public string PublicacionpaginaSATsentenciafavorable { get; set; }
        public string Numerofechaoficioglobalsentenciafavorable { get; set; }
        public string PublicacionDOFsentenciafavorable { get; set; }
    }
}