﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Validacion69
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);

            string archivoListado_Completo_69_B
            = AppDomain.CurrentDomain.BaseDirectory + "\\Listado_Completo_69-B" + DateTime.Today.ToString("yyyyMM") + ".csv";

            if (File.Exists(archivoListado_Completo_69_B))
            {
                Application.Run(new frmPrincipal());
            }
            else
            {
                Application.Run(new frmBajarValidacion());
            }

            
        }
    }
}
