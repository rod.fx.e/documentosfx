#region Copyright Syncfusion Inc. 2001 - 2017
// Copyright Syncfusion Inc. 2001 - 2017. All rights reserved.
// Use of this code is subject to the terms of our license.
// A copy of the current license can be obtained at any time by e-mailing
// licensing@syncfusion.com. Any infringement will be prosecuted under
// applicable laws. 
#endregion
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;

using System.Text;
using System.Windows.Forms;
using System.Diagnostics;
using System.IO;
using System.Net;
using Generales;

namespace Validacion69
{
    public partial class frmBajarValidacion : Syncfusion.Windows.Forms.MetroForm
    {
        WebClient webClient;               // Our WebClient that will be doing the downloading for us
        Stopwatch sw = new Stopwatch();
        string globalPresuntos = "https://siat.sat.gob.mx/PTSC/consultapdc/articulo69b/presuntos/fis_mor/TODAS/globalPresuntos.xlsx";
        string globalDefinitivos = "https://siat.sat.gob.mx/PTSC/consultapdc/articulo69b/definitivos/fis_mor/TODAS/globalDefinitivos.xlsx";
        string globalDesvirtuaron = "https://siat.sat.gob.mx/PTSC/consultapdc/articulo69b/desvirtuaron/fis_mor/TODAS/globalDesvirtuaron.xlsx";
        string globalPresuntostr = AppDomain.CurrentDomain.BaseDirectory + "\\globalPresuntos.xlsx";
        string globalDefinitivostr = AppDomain.CurrentDomain.BaseDirectory + "\\globalDefinitivos.xlsx";
        string globalDesvirtuarontr = AppDomain.CurrentDomain.BaseDirectory + "\\globalDesvirtuaron.xlsx";
        string actual = "";

        string linkListado_Completo_69_B = "http://www.sat.gob.mx/cifras_sat/Documents/Listado_Completo_69-B.csv?web=1";
        string archivoListado_Completo_69_B = AppDomain.CurrentDomain.BaseDirectory + "\\Listado_Completo_69-B.csv";
        bool mostrarPrincipal = true;
        public frmBajarValidacion(bool mostrarPrincipalp = false)
        {
            InitializeComponent();
            mostrarPrincipal = false;
        }


        public void DownloadFile(string urlAddress, string location)
        {
            using (webClient = new WebClient())
            {
                webClient.Headers.Add("user-agent", "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko)");
                webClient.DownloadFileCompleted += new AsyncCompletedEventHandler(Completed);
                webClient.DownloadProgressChanged += new DownloadProgressChangedEventHandler(ProgressChanged);

                // The variable that will be holding the url address (making sure it starts with http://)
                Uri URL = urlAddress.StartsWith("http://", StringComparison.OrdinalIgnoreCase) ? new Uri(urlAddress) : new Uri( urlAddress);
                actual = location;
                // Start the stopwatch which we will be using to calculate the download speed
                sw.Start();

                try
                {
                    // Start downloading the file
                    webClient.DownloadFileAsync(URL, location);
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
            }
        }

        // The event that will fire whenever the progress of the WebClient is changed
        private void ProgressChanged(object sender, DownloadProgressChangedEventArgs e)
        {
            // Calculate download speed and output it to labelSpeed.
            labelSpeed.Text = string.Format("{0} kb/s", (e.BytesReceived / 1024d / sw.Elapsed.TotalSeconds).ToString("0.00"));

            // Update the progressbar percentage only when the value is not the same.
            progressBar.Value = e.ProgressPercentage;

            // Show the percentage on our label.
            labelPerc.Text = e.ProgressPercentage.ToString() + "%";

            // Update the label with how much data have been downloaded so far and the total size of the file we are currently downloading
            labelDownloaded.Text = string.Format("{0} MB's / {1} MB's",
                (e.BytesReceived / 1024d / 1024d).ToString("0.00"),
                (e.TotalBytesToReceive / 1024d / 1024d).ToString("0.00"));
        }

        // The event that will trigger when the WebClient is completed
        private void Completed(object sender, AsyncCompletedEventArgs e)
        {
            // Reset the stopwatch.
            sw.Reset();
            irPrincipal();

            //if (actual.Contains("globalPresuntos"))
            //{
            //    validar0Kb(globalPresuntostr);
            //    globalDefinitivostr = AppDomain.CurrentDomain.BaseDirectory + "\\globalDefinitivos" + DateTime.Today.ToString("yyyyMMdd") + ".xlsx";
            //    DownloadFile(globalDefinitivos, globalDefinitivostr);
            //}
            //else
            //{
            //    if (actual.Contains("globalDefinitivos"))
            //    {
            //        validar0Kb(globalDefinitivostr);
            //        globalDesvirtuarontr = AppDomain.CurrentDomain.BaseDirectory + "\\globalDesvirtuaron" + DateTime.Today.ToString("yyyyMMdd")+ ".xlsx";
            //        DownloadFile(globalDesvirtuaron, globalDesvirtuarontr);
            //    }
            //    else
            //    {
            //        if (actual.Contains("globalDesvirtuaro"))
            //        {
            //            validar0Kb(globalDesvirtuarontr);
            //            frmPrincipal o = new frmPrincipal();
            //            o.Show();
            //            this.Visible = false;
            //        }
            //    }
            //}



        }

        private void irPrincipal()
        {
            if (mostrarPrincipal)
            {
                frmPrincipal o = new frmPrincipal();
                o.Show();
                this.Visible = false;
            }
            else
            {
                this.Close();
            }
        }

        private void validar0Kb(string archivo)
        {
            try
            {
                DirectoryInfo di = new DirectoryInfo(AppDomain.CurrentDomain.BaseDirectory);
                foreach (FileInfo fi in di.GetFiles("*"+DateTime.Today.ToString("yyyyMMdd") + ".xlsx", SearchOption.TopDirectoryOnly))
                {
                    if (fi.Length == 0)
                    {
                        File.Delete(fi.FullName);
                    }
                }
            }
            catch(Exception error)
            {
                ErrorFX.mostrar(error, true, true, true);
            }
        }

        private void frmBajarValidacion_Load(object sender, EventArgs e)
        {
            //globalPresuntostr = AppDomain.CurrentDomain.BaseDirectory + "\\globalPresuntos" + DateTime.Today.ToString("yyyyMMdd") + ".xlsx";
            //DownloadFile(globalPresuntos, globalPresuntostr);

            archivoListado_Completo_69_B
            = AppDomain.CurrentDomain.BaseDirectory + "\\Listado_Completo_69-B" + DateTime.Today.ToString("yyyyMM") + ".csv";

            DownloadFile(linkListado_Completo_69_B, archivoListado_Completo_69_B);

        }
    }
}
