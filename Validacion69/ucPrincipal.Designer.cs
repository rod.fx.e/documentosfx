﻿namespace Validacion69
{
    partial class ucPrincipal
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.resultado = new System.Windows.Forms.TextBox();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.log = new System.Windows.Forms.TextBox();
            this.checkBuscar = new System.Windows.Forms.CheckBox();
            this.buttonAdv1 = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.estado = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.btnAddItem = new System.Windows.Forms.Button();
            this.horas = new System.Windows.Forms.TextBox();
            this.correos = new System.Windows.Forms.TextBox();
            this.directorio = new System.Windows.Forms.TextBox();
            this.timer = new System.Windows.Forms.Timer(this.components);
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.tabPage2.SuspendLayout();
            this.SuspendLayout();
            // 
            // tabControl1
            // 
            this.tabControl1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Location = new System.Drawing.Point(3, 101);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(848, 214);
            this.tabControl1.TabIndex = 22;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.resultado);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(840, 188);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Resultado";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // resultado
            // 
            this.resultado.Dock = System.Windows.Forms.DockStyle.Fill;
            this.resultado.Location = new System.Drawing.Point(3, 3);
            this.resultado.Multiline = true;
            this.resultado.Name = "resultado";
            this.resultado.Size = new System.Drawing.Size(834, 182);
            this.resultado.TabIndex = 9;
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.log);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(840, 188);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Log";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // log
            // 
            this.log.Dock = System.Windows.Forms.DockStyle.Fill;
            this.log.Location = new System.Drawing.Point(3, 3);
            this.log.Multiline = true;
            this.log.Name = "log";
            this.log.Size = new System.Drawing.Size(834, 182);
            this.log.TabIndex = 10;
            // 
            // checkBuscar
            // 
            this.checkBuscar.Appearance = System.Windows.Forms.Appearance.Button;
            this.checkBuscar.AutoSize = true;
            this.checkBuscar.BackColor = System.Drawing.Color.Orange;
            this.checkBuscar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.checkBuscar.ForeColor = System.Drawing.Color.White;
            this.checkBuscar.Location = new System.Drawing.Point(614, 70);
            this.checkBuscar.Name = "checkBuscar";
            this.checkBuscar.Size = new System.Drawing.Size(119, 23);
            this.checkBuscar.TabIndex = 17;
            this.checkBuscar.Text = "Ejecución automático";
            this.checkBuscar.UseVisualStyleBackColor = false;
            this.checkBuscar.CheckedChanged += new System.EventHandler(this.checkBuscar_CheckedChanged_1);
            // 
            // buttonAdv1
            // 
            this.buttonAdv1.BackColor = System.Drawing.Color.SteelBlue;
            this.buttonAdv1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonAdv1.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonAdv1.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.buttonAdv1.Location = new System.Drawing.Point(3, 67);
            this.buttonAdv1.Name = "buttonAdv1";
            this.buttonAdv1.Size = new System.Drawing.Size(92, 28);
            this.buttonAdv1.TabIndex = 16;
            this.buttonAdv1.Text = "Buscar";
            this.buttonAdv1.UseVisualStyleBackColor = false;
            this.buttonAdv1.Click += new System.EventHandler(this.buttonAdv1_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(818, 75);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(33, 13);
            this.label3.TabIndex = 20;
            this.label3.Text = "horas";
            // 
            // estado
            // 
            this.estado.BackColor = System.Drawing.Color.LightSteelBlue;
            this.estado.Location = new System.Drawing.Point(3, 318);
            this.estado.Name = "estado";
            this.estado.Size = new System.Drawing.Size(848, 13);
            this.estado.TabIndex = 21;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(739, 75);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(32, 13);
            this.label2.TabIndex = 18;
            this.label2.Text = "Cada";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(3, 40);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(195, 13);
            this.label1.TabIndex = 14;
            this.label1.Text = "Listado de correos electrónicos a enviar";
            // 
            // btnAddItem
            // 
            this.btnAddItem.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(153)))), ((int)(((byte)(181)))), ((int)(((byte)(105)))));
            this.btnAddItem.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnAddItem.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAddItem.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnAddItem.Location = new System.Drawing.Point(3, 3);
            this.btnAddItem.Name = "btnAddItem";
            this.btnAddItem.Size = new System.Drawing.Size(194, 28);
            this.btnAddItem.TabIndex = 12;
            this.btnAddItem.Text = "Directorío de CFDI\'s a escanear";
            this.btnAddItem.UseVisualStyleBackColor = false;
            this.btnAddItem.Click += new System.EventHandler(this.btnAddItem_Click);
            // 
            // horas
            // 
            this.horas.Location = new System.Drawing.Point(777, 72);
            this.horas.Name = "horas";
            this.horas.Size = new System.Drawing.Size(35, 20);
            this.horas.TabIndex = 19;
            this.horas.Text = "24";
            // 
            // correos
            // 
            this.correos.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.correos.Location = new System.Drawing.Point(203, 37);
            this.correos.Name = "correos";
            this.correos.Size = new System.Drawing.Size(644, 20);
            this.correos.TabIndex = 15;
            // 
            // directorio
            // 
            this.directorio.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.directorio.Location = new System.Drawing.Point(203, 8);
            this.directorio.Name = "directorio";
            this.directorio.Size = new System.Drawing.Size(644, 20);
            this.directorio.TabIndex = 13;
            // 
            // timer
            // 
            this.timer.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // ucPrincipal
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.tabControl1);
            this.Controls.Add(this.checkBuscar);
            this.Controls.Add(this.buttonAdv1);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.estado);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.btnAddItem);
            this.Controls.Add(this.horas);
            this.Controls.Add(this.correos);
            this.Controls.Add(this.directorio);
            this.Name = "ucPrincipal";
            this.Size = new System.Drawing.Size(857, 335);
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage1.PerformLayout();
            this.tabPage2.ResumeLayout(false);
            this.tabPage2.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TextBox resultado;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.TextBox log;
        private System.Windows.Forms.CheckBox checkBuscar;
        private System.Windows.Forms.Button buttonAdv1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label estado;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btnAddItem;
        private System.Windows.Forms.TextBox horas;
        private System.Windows.Forms.TextBox correos;
        private System.Windows.Forms.TextBox directorio;
        private System.Windows.Forms.Timer timer;
    }
}
