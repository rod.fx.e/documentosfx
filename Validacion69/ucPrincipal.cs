﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Generales;
using OfficeOpenXml;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net.Mail;
using System.Runtime.InteropServices;
using System.Text;
using System.Windows.Forms;
using System.Xml;
using System.Xml.Serialization;

namespace Validacion69
{
    public partial class ucPrincipal : UserControl
    {
        //TODO: Cargar el 69
        //TODO: Los directorios se deben guardar en una parte
        //TODO: Guardar toda los archivos en tabla en SQL, archiv, rfc, uuid, monto, moneda, monto mx, folio, serie, concepto todos
        //TODO: Automatico el tiempo y la escala
        //TODO: Se muestre el error por tabla separada
        //TODO: Mostrar el resumen y al expotar es un detalle.

        //TODO: 69B - Inactivar en Visual, Validar si hay facturas pendiente de pago y que las ponga en OnHOld
        //, OC y Requisicines liberadas y por liberar y mandar un resuemn de todo, facturas con balance pendiente
        //Si las quiere guardar o modificar las valido para que no entren en el sistema
        //Bloqueo de recibos de OC - REquisi , FACTURAs o PAGO, deshabilitar y habilitarlo
        //Agregar excepciones
         




        string path = System.AppDomain.CurrentDomain.BaseDirectory + "\\69B.cnfg";
        public ucPrincipal()
        {
            InitializeComponent();

            string archivoListado_Completo_69_B
            = AppDomain.CurrentDomain.BaseDirectory + "\\Listado_Completo_69-B" + DateTime.Today.ToString("yyyyMM") + ".csv";
            if (!File.Exists(archivoListado_Completo_69_B))
            {
                frmBajarValidacion o=new frmBajarValidacion(false);
                o.Show();
            }
            //cargarConfiguracion();
        }

        private void buttonAdv1_Click(object sender, EventArgs e)
        {
            procesarCSV();
        }

        private void checkBuscar_CheckedChanged(object sender, EventArgs e)
        {
            timer.Enabled = checkBuscar.Checked;
        }
        int counter = 0;
        private void timer1_Tick(object sender, EventArgs e)
        {
            int segundos = int.Parse(horas.Text) * 60 * 60;
            if (checkBuscar.Checked)
            {
                counter++;

                estado.Text = (segundos - counter).ToString();


                if (counter == segundos)
                {
                    timer.Stop();
                    counter = 0;
                    procesarCSV();
                    estado.Text = (segundos - counter).ToString();
                }
            }
        }

        #region codigo

        private void seleccionarDirectorio()
        {
            using (var fbd = new FolderBrowserDialog())
            {
                DialogResult result = fbd.ShowDialog();

                if (result == DialogResult.OK && !string.IsNullOrWhiteSpace(fbd.SelectedPath))
                {
                    directorio.Text = fbd.SelectedPath;
                }
            }
        }


        c69B oc69B;
        private void cargarConfiguracion()
        {
            try
            {
                oc69B = new c69B();

                if (System.IO.File.Exists(path))
                {
                    using (Stream stream = System.IO.File.Open(path, FileMode.Open))
                    {
                        var binaryFormatter = new System.Runtime.Serialization.Formatters.Binary.BinaryFormatter();
                        try
                        {
                            oc69B = (c69B)binaryFormatter.Deserialize(stream);
                            directorio.Text = oc69B.directorios;
                            correos.Text = oc69B.correos;
                        }
                        catch (Exception error)
                        {
                            ErrorFX.mostrar(error, true, true);
                        }
                    }
                }
            }
            catch
            {

            }
        }
        private void guardar()
        {
            try
            {
                c69B oc69B = new c69B();
                oc69B.directorios = directorio.Text;
                oc69B.correos = correos.Text;
                using (Stream stream = System.IO.File.Open(path, FileMode.Create))
                {
                    var binaryFormatter = new System.Runtime.Serialization.Formatters.Binary.BinaryFormatter();
                    binaryFormatter.Serialize(stream, oc69B);
                }
            }
            catch
            {

            }
        }

        private void procesarCSV()
        {
            try
            {
                string archivoListado_Completo_69_B
                = AppDomain.CurrentDomain.BaseDirectory + "\\Listado_Completo_69-B" + DateTime.Now.ToString("yyyyMM") + ".csv";
                //Cargar 69_B
                DataTable oDataTable = ConvertCSVtoDataTable(archivoListado_Completo_69_B);
                //Cargar CFDI
                cargarCFDI();


                var results69 = from table1 in oDataTable.AsEnumerable()
                                join table2 in odtCFDIDataTable.AsEnumerable() on table1[1] equals table2["rfcEmisor"]
                                select new cResultado
                                {
                                    rfc = (string)table2["rfcEmisor"]
                                    ,
                                    razon = (string)table1[2]
                                    ,
                                    tipo = (string)table1[3]
                                };

                resultado.Text = DateTime.Now.ToShortDateString() + " " + DateTime.Now.ToShortTimeString() + Environment.NewLine;
                resultado.Text += "Resultado: " + Environment.NewLine;

                if (results69 == null)
                {
                    resultado.Text += "Existente: 0 " + Environment.NewLine;
                }
                else
                {
                    List<cResultado> dt = results69.ToList();
                    resultado.Text += "Existente: " + dt.Count.ToString() + Environment.NewLine;
                    foreach (cResultado r in dt)
                    {
                        resultado.Text += "RFC:" + r.rfc + " Razon: " + r.razon + " Tipo: " + r.tipo + Environment.NewLine;
                    }
                }
                sendSMTPMail(Application.ProductName + "-" + Application.ProductVersion + " " + DateTime.Now.ToShortDateString() + " " + DateTime.Now.ToShortTimeString()
                    , resultado.Text);
                resultado.Text += "Finalizado " + DateTime.Now.ToShortTimeString() + Environment.NewLine;


            }
            catch (Exception error)
            {
                asigar_log_actualizacion(error.Message.ToString());
                //ErrorFX.mostrar(error, true, true, true);
            }
        }

        private void cargarCFDI()
        {
            try
            {
                odtCFDIDataTable = new DataSetCFDI.dtCFDIDataTable();


                string[] directorios = directorio.Text.ToString().Split(';');
                for (int i = 0; i < directorios.Length; i++)
                {
                    if (directorios[i] != "")
                    {
                        string[] allfiles = System.IO.Directory.GetFiles(directorios[i], "*.xml", System.IO.SearchOption.AllDirectories);
                        foreach (var file in allfiles)
                        {
                            FileInfo info = new FileInfo(file);
                            validarXML(file);
                        }
                    }
                }
            }
            catch (Exception error)
            {
                asigar_log_actualizacion(error.Message.ToString());
                //ErrorFX.mostrar(error, true, true, true);
            }
        }

        public static DataTable ConvertCSVtoDataTable(string strFilePath)
        {
            DataTable dt = new DataTable();
            using (StreamReader sr = new StreamReader(strFilePath))
            {
                //Leer dos lineas
                sr.ReadLine().Split(',');
                sr.ReadLine().Split(',');

                string[] headers = sr.ReadLine().Split(',');
                int contador = 0;
                foreach (string header in headers)
                {
                    dt.Columns.Add("Contador" + contador.ToString());
                    contador++;
                }
                while (!sr.EndOfStream)
                {
                    string[] rows = sr.ReadLine().Split(',');
                    DataRow dr = dt.NewRow();
                    for (int i = 0; i < headers.Length; i++)
                    {
                        dr[i] = rows[i];
                    }
                    dt.Rows.Add(dr);
                }

            }
            return dt;
        }

        string globalPresuntostr = AppDomain.CurrentDomain.BaseDirectory + "\\globalPresuntos.xlsx";
        string globalDefinitivostr = AppDomain.CurrentDomain.BaseDirectory + "\\globalDefinitivos.xlsx";
        string globalDesvirtuarontr = AppDomain.CurrentDomain.BaseDirectory + "\\globalDesvirtuaron.xlsx";
        DataSetCFDI.dtCFDIDataTable odtCFDIDataTable;

        const int ERROR_SHARING_VIOLATION = 32;
        const int ERROR_LOCK_VIOLATION = 33;
        private bool IsFileLocked(string file)
        {
            //check that problem is not in destination file
            if (File.Exists(file) == true)
            {
                FileStream stream = null;
                try
                {
                    stream = File.Open(file, FileMode.Open, FileAccess.ReadWrite, FileShare.None);
                }
                catch (Exception ex2)
                {
                    //_log.WriteLog(ex2, "Error in checking whether file is locked " + file);
                    int errorCode = Marshal.GetHRForException(ex2) & ((1 << 16) - 1);
                    if ((ex2 is IOException) && (errorCode == ERROR_SHARING_VIOLATION || errorCode == ERROR_LOCK_VIOLATION))
                    {
                        return true;
                    }
                }
                finally
                {
                    if (stream != null)
                        stream.Close();
                }
            }
            return false;
        }

        public DataTable ImportToDataTable(string FilePath)
        {
            DataTable dt = new DataTable();
            FileInfo fi = new FileInfo(FilePath);

            // Check if the file exists
            if (!fi.Exists)
                throw new Exception("File " + FilePath + " Does Not Exists");

            using (ExcelPackage xlPackage = new ExcelPackage(fi))
            {
                // get the first worksheet in the workbook
                ExcelWorksheet worksheet = xlPackage.Workbook.Worksheets[xlPackage.Workbook.Worksheets.Count];

                // Fetch the WorkSheet size
                ExcelCellAddress startCell = worksheet.Dimension.Start;
                ExcelCellAddress endCell = worksheet.Dimension.End;

                // create all the needed DataColumn
                for (int col = startCell.Column; col <= endCell.Column; col++)
                    dt.Columns.Add(col.ToString());

                // place all the data into DataTable
                for (int row = startCell.Row; row <= endCell.Row; row++)
                {
                    DataRow dr = dt.NewRow();
                    int x = 0;
                    for (int col = startCell.Column; col <= endCell.Column; col++)
                    {
                        dr[x++] = worksheet.Cells[row, col].Value;
                    }
                    dt.Rows.Add(dr);
                }
            }
            return dt;
        }

        private bool validarXML(string archivo)
        {
            //Pasear el comprobante
            try
            {
                if (!IsFileLocked(archivo))
                {
                    XmlSerializer serializer_CFD_3 = new XmlSerializer(typeof(CFDI33.Comprobante));
                    TextReader reader = new StreamReader(archivo);
                    CFDI33.Comprobante oComprobante = (CFDI33.Comprobante)serializer_CFD_3.Deserialize(reader);
                    reader.Dispose();
                    reader.Close();

                    DataSetCFDI.dtCFDIRow toInsert = odtCFDIDataTable.NewdtCFDIRow();
                    toInsert.archivo = archivo;
                    toInsert.rfcEmisor = oComprobante.Emisor.Rfc;
                    toInsert.rfcReceptor = oComprobante.Receptor.Rfc;

                    XmlDocument docXML = new XmlDocument();
                    docXML.Load(archivo);
                    XmlNodeList TimbreFiscalDigital = docXML.GetElementsByTagName("tfd:TimbreFiscalDigital");
                    string UUIDtr = "";
                    if (TimbreFiscalDigital != null)
                    {
                        if (TimbreFiscalDigital[0] != null)
                        {
                            UUIDtr = TimbreFiscalDigital[0].Attributes["UUID"].Value;
                        }

                    }
                    toInsert.uuid = UUIDtr;
                    odtCFDIDataTable.AdddtCFDIRow(toInsert);


                    return true;
                }
                return false;

            }
            catch (Exception error)
            {
                asigar_log_actualizacion(error.Message.ToString());
                return validarXML32(archivo);
            }
        }

        private bool validarXML32(string archivo)
        {
            try
            {
                if (!IsFileLocked(archivo))
                {
                    XmlSerializer serializer_CFD_3 = new XmlSerializer(typeof(CFDI32.Comprobante));
                    TextReader reader = new StreamReader(archivo);
                    CFDI32.Comprobante oComprobante = (CFDI32.Comprobante)serializer_CFD_3.Deserialize(reader);
                    reader.Dispose();
                    reader.Close();

                    DataSetCFDI.dtCFDIRow toInsert = odtCFDIDataTable.NewdtCFDIRow();
                    toInsert.archivo = archivo;
                    toInsert.rfcEmisor = oComprobante.Emisor.rfc;
                    toInsert.rfcReceptor = oComprobante.Receptor.rfc;

                    XmlDocument docXML = new XmlDocument();
                    docXML.Load(archivo);
                    XmlNodeList TimbreFiscalDigital = docXML.GetElementsByTagName("tfd:TimbreFiscalDigital");
                    string UUIDtr = "";
                    if (TimbreFiscalDigital != null)
                    {
                        if (TimbreFiscalDigital[0] != null)
                        {
                            UUIDtr = TimbreFiscalDigital[0].Attributes["UUID"].Value;
                        }

                    }
                    toInsert.uuid = UUIDtr;
                    odtCFDIDataTable.AdddtCFDIRow(toInsert);


                    return true;
                }
                return false;
            }
            catch (Exception error)
            {
                asigar_log_actualizacion(error.Message.ToString());
                return false;
                //return validarXML32(archivo);
            }
        }

        private void correos_Leave(object sender, EventArgs e)
        {
            //guardar();
        }

        public void sendSMTPMail(string asunto, string mensaje)
        {

            MailMessage mm = new MailMessage();
            mm.IsBodyHtml = true;
            mm.From = new MailAddress(cENCRIPTACION.Decrypt(Properties.Settings.Default.errorFrom), Properties.Settings.Default.errorFromName, Encoding.UTF8);
            mm.Subject = asunto;

            byte[] bytes = Encoding.Default.GetBytes(mensaje);
            mensaje = Encoding.UTF8.GetString(bytes);

            mm.Body = mensaje;
            mm.SubjectEncoding = System.Text.Encoding.UTF8;
            mm.IsBodyHtml = true;
            mm.Priority = MailPriority.High;

            string[] rEmailCC = correos.Text.ToString().Split(';');
            for (int i = 0; i < rEmailCC.Length; i++)
            {
                if (!String.IsNullOrEmpty(rEmailCC[i].ToString().Trim()))
                {
                    if (Globales.isEmail(rEmailCC[i].ToString().Trim()))
                    {
                        mm.To.Add(rEmailCC[i].ToString().Trim());
                    }
                }
            }



            if (mm.To.Count == 0)
            {
                return;
            }

            try
            {
                var smtp = new SmtpClient
                {
                    Host = Properties.Settings.Default.errorHost,
                    Port = 587,
                    EnableSsl = true,
                    DeliveryMethod = SmtpDeliveryMethod.Network,
                    UseDefaultCredentials = false,
                    Credentials = new System.Net.NetworkCredential(cENCRIPTACION.Decrypt(Properties.Settings.Default.errorUser), cENCRIPTACION.Decrypt(Properties.Settings.Default.errorPassword))
                };
                smtp.Send(mm);
            }
            catch (Exception error)
            {
                string mensajetr = error.Message.ToString();
                if (error.InnerException != null)
                {
                    mensajetr += Environment.NewLine + error.InnerException.Message.ToString();
                }
                BitacoraFX.Log("Envio de correo ErrorFX: " + mensajetr);
            }

        }
        private void asigar_texto_actualizacion(string texto)
        {
            if (resultado.InvokeRequired)
            {
                MethodInvoker invoker = new MethodInvoker(delegate
                {
                    resultado.Text = texto + " " + DateTime.Now.ToShortDateString() + " " + DateTime.Now.ToShortTimeString();
                });
                resultado.Invoke(invoker);
            }
            else
            {
                resultado.Text = texto;
            }
        }

        private void asigar_log_actualizacion(string texto)
        {
            if (log.InvokeRequired)
            {
                MethodInvoker invoker = new MethodInvoker(delegate
                {
                    log.Text = texto + " " + DateTime.Now.ToShortDateString() + " " + DateTime.Now.ToShortTimeString();
                });
                log.Invoke(invoker);
            }
            else
            {
                log.Text = texto;
            }
        }

        #endregion

        private void btnAddItem_Click(object sender, EventArgs e)
        {
            seleccionarDirectorio();
        }

        private void checkBuscar_CheckedChanged_1(object sender, EventArgs e)
        {
            timer.Enabled = checkBuscar.Checked;
        }
    }
}
