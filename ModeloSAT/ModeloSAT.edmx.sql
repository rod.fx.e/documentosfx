
-- --------------------------------------------------
-- Entity Designer DDL Script for SQL Server 2005, 2008, 2012 and Azure
-- --------------------------------------------------
-- Date Created: 07/03/2017 23:50:04
-- Generated from EDMX file: D:\Proyectos\DocumentosFX\SAT\ImportarCatalogos\ModeloSAT\ModeloSAT.edmx
-- --------------------------------------------------

SET QUOTED_IDENTIFIER OFF;
GO
USE [documentosfx];
GO
IF SCHEMA_ID(N'dbo') IS NULL EXECUTE(N'CREATE SCHEMA [dbo]');
GO

-- --------------------------------------------------
-- Dropping existing FOREIGN KEY constraints
-- --------------------------------------------------


-- --------------------------------------------------
-- Dropping existing tables
-- --------------------------------------------------


-- --------------------------------------------------
-- Creating all tables
-- --------------------------------------------------

-- Creating table 'satc_AduanaSet'
CREATE TABLE [dbo].[satc_AduanaSet] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [c_Aduana] nvarchar(10)  NOT NULL,
    [descripcion] nvarchar(250)  NOT NULL
);
GO

-- Creating table 'satc_ClaveProdServSet'
CREATE TABLE [dbo].[satc_ClaveProdServSet] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [c_ClaveProdServ] nvarchar(max)  NOT NULL,
    [descripcion] nvarchar(250)  NOT NULL,
    [fechaInicioVigencia] datetime  NULL,
    [fechaFinVigencia] datetime  NULL,
    [incluirIVAtrasladado] nvarchar(max)  NOT NULL,
    [incluirIEPStrasladado] nvarchar(max)  NOT NULL,
    [complemento] nvarchar(max)  NOT NULL
);
GO

-- Creating table 'satc_ClaveUnidadSet'
CREATE TABLE [dbo].[satc_ClaveUnidadSet] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [c_ClaveUnidad] nvarchar(10)  NOT NULL,
    [descripcion] nvarchar(250)  NOT NULL,
    [nombre] nvarchar(max)  NOT NULL,
    [nota] nvarchar(max)  NOT NULL,
    [fechaInicioVigencia] datetime  NULL,
    [fechaFinVigencia] datetime  NULL,
    [simbolo] nvarchar(max)  NOT NULL
);
GO

-- Creating table 'satc_CodigoPostalSet'
CREATE TABLE [dbo].[satc_CodigoPostalSet] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [c_CodigoPostal] nvarchar(10)  NOT NULL,
    [c_Estado] nvarchar(max)  NOT NULL,
    [c_Municipio] nvarchar(max)  NOT NULL,
    [c_Localidad] nvarchar(max)  NOT NULL
);
GO

-- Creating table 'satc_FormaPagoSet'
CREATE TABLE [dbo].[satc_FormaPagoSet] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [c_FormaPago] nvarchar(max)  NOT NULL,
    [descripcion] nvarchar(250)  NOT NULL,
    [bancarizado] bit  NOT NULL,
    [numeroOperacion] bit  NOT NULL,
    [rfcEmisorCuentaOrdenante] bit  NOT NULL,
    [cuentaOrdenante] bit  NOT NULL,
    [patronCuentaOrdenante] nvarchar(max)  NOT NULL,
    [fechaInicioVigencia] datetime  NULL,
    [fechaFinVigencia] datetime  NULL,
    [rfcEmisorCuentaBeneficiario] bit  NOT NULL,
    [cuentaBeneficiario] bit  NOT NULL,
    [patronCuentaBeneficiaria] nvarchar(max)  NOT NULL,
    [tipoCadenaPago] bit  NOT NULL
);
GO

-- Creating table 'satc_ImpuestoSet'
CREATE TABLE [dbo].[satc_ImpuestoSet] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [c_Impuesto] nvarchar(max)  NOT NULL,
    [descripcion] nvarchar(250)  NOT NULL,
    [retencion] bit  NOT NULL,
    [traslado] bit  NOT NULL,
    [localOfederal] nvarchar(max)  NOT NULL,
    [entidad] nvarchar(max)  NULL
);
GO

-- Creating table 'satc_MetodoPagoSet'
CREATE TABLE [dbo].[satc_MetodoPagoSet] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [c_MetodoPago] nvarchar(max)  NOT NULL,
    [descripcion] nvarchar(250)  NOT NULL,
    [fechaInicioVigencia] datetime  NULL,
    [fechaFinVigencia] datetime  NULL
);
GO

-- Creating table 'satc_MonedaSet'
CREATE TABLE [dbo].[satc_MonedaSet] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [c_Moneda] nvarchar(max)  NOT NULL,
    [descripcion] nvarchar(250)  NOT NULL,
    [decimales] nvarchar(max)  NOT NULL,
    [porcentajeVariacion] nvarchar(max)  NOT NULL
);
GO

-- Creating table 'satc_NumPedimentoAduanaSet'
CREATE TABLE [dbo].[satc_NumPedimentoAduanaSet] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [c_Aduana] nvarchar(max)  NOT NULL,
    [patente] nvarchar(max)  NOT NULL,
    [ejercicio] nvarchar(max)  NOT NULL,
    [cantidad] nvarchar(max)  NOT NULL,
    [fechaInicioVigencia] datetime  NULL,
    [fechaFinVigencia] datetime  NULL
);
GO

-- Creating table 'satc_UsoCFDISet'
CREATE TABLE [dbo].[satc_UsoCFDISet] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [descripcion] nvarchar(250)  NOT NULL,
    [fechaInicioVigencia] datetime  NULL,
    [fechaFinVigencia] datetime  NULL,
    [c_UsoCFDI] nvarchar(max)  NOT NULL,
    [fisica] bit  NOT NULL,
    [moral] bit  NOT NULL
);
GO

-- Creating table 'satc_TipoRelacionSet'
CREATE TABLE [dbo].[satc_TipoRelacionSet] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [descripcion] nvarchar(250)  NOT NULL,
    [fechaInicioVigencia] datetime  NULL,
    [fechaFinVigencia] datetime  NULL,
    [c_TipoRelacion] nvarchar(max)  NOT NULL
);
GO

-- Creating table 'satc_TipoFactorSet'
CREATE TABLE [dbo].[satc_TipoFactorSet] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [fechaInicioVigencia] datetime  NULL,
    [fechaFinVigencia] datetime  NULL,
    [c_TipoFactor] nvarchar(max)  NOT NULL
);
GO

-- Creating table 'satc_TipoDeComprobanteSet'
CREATE TABLE [dbo].[satc_TipoDeComprobanteSet] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [descripcion] nvarchar(250)  NOT NULL,
    [fechaInicioVigencia] datetime  NULL,
    [fechaFinVigencia] datetime  NULL,
    [c_TipoDeComprobante] nvarchar(max)  NOT NULL,
    [valorMaximo] decimal(18,0)  NOT NULL,
    [ns] nvarchar(max)  NOT NULL,
    [nds] nvarchar(max)  NOT NULL
);
GO

-- Creating table 'satc_TasaOCuotaSet'
CREATE TABLE [dbo].[satc_TasaOCuotaSet] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [fechaInicioVigencia] datetime  NULL,
    [fechaFinVigencia] datetime  NULL,
    [retencion] bit  NOT NULL,
    [traslado] bit  NOT NULL,
    [factor] nvarchar(max)  NOT NULL,
    [impuesto] nvarchar(max)  NOT NULL,
    [c_TasaOCuota] decimal(18,0)  NOT NULL,
    [minimo] decimal(18,0)  NOT NULL,
    [maximo] decimal(18,0)  NOT NULL,
    [rangoOFijo] nvarchar(max)  NOT NULL
);
GO

-- Creating table 'satc_RegimenFiscalSet'
CREATE TABLE [dbo].[satc_RegimenFiscalSet] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [fechaInicioVigencia] datetime  NULL,
    [descripcion] nvarchar(250)  NOT NULL,
    [fechaFinVigencia] datetime  NULL,
    [fisica] bit  NOT NULL,
    [moral] bit  NOT NULL,
    [c_RegimenFiscal] nvarchar(max)  NOT NULL
);
GO

-- Creating table 'satc_PatenteAduanalSet'
CREATE TABLE [dbo].[satc_PatenteAduanalSet] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [fechaInicioVigencia] datetime  NULL,
    [fechaFinVigencia] datetime  NULL,
    [c_PatenteAduanal] nvarchar(max)  NOT NULL
);
GO

-- Creating table 'satc_PaisSet'
CREATE TABLE [dbo].[satc_PaisSet] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [fechaInicioVigencia] datetime  NULL,
    [fechaFinVigencia] datetime  NULL,
    [descripcion] nvarchar(250)  NOT NULL,
    [validacionRegistro] nvarchar(max)  NOT NULL,
    [formatoRegistro] nvarchar(max)  NOT NULL,
    [formatoCodigo] nvarchar(max)  NOT NULL,
    [c_Pais] nvarchar(max)  NOT NULL
);
GO

-- --------------------------------------------------
-- Creating all PRIMARY KEY constraints
-- --------------------------------------------------

-- Creating primary key on [Id] in table 'satc_AduanaSet'
ALTER TABLE [dbo].[satc_AduanaSet]
ADD CONSTRAINT [PK_satc_AduanaSet]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'satc_ClaveProdServSet'
ALTER TABLE [dbo].[satc_ClaveProdServSet]
ADD CONSTRAINT [PK_satc_ClaveProdServSet]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'satc_ClaveUnidadSet'
ALTER TABLE [dbo].[satc_ClaveUnidadSet]
ADD CONSTRAINT [PK_satc_ClaveUnidadSet]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'satc_CodigoPostalSet'
ALTER TABLE [dbo].[satc_CodigoPostalSet]
ADD CONSTRAINT [PK_satc_CodigoPostalSet]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'satc_FormaPagoSet'
ALTER TABLE [dbo].[satc_FormaPagoSet]
ADD CONSTRAINT [PK_satc_FormaPagoSet]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'satc_ImpuestoSet'
ALTER TABLE [dbo].[satc_ImpuestoSet]
ADD CONSTRAINT [PK_satc_ImpuestoSet]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'satc_MetodoPagoSet'
ALTER TABLE [dbo].[satc_MetodoPagoSet]
ADD CONSTRAINT [PK_satc_MetodoPagoSet]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'satc_MonedaSet'
ALTER TABLE [dbo].[satc_MonedaSet]
ADD CONSTRAINT [PK_satc_MonedaSet]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'satc_NumPedimentoAduanaSet'
ALTER TABLE [dbo].[satc_NumPedimentoAduanaSet]
ADD CONSTRAINT [PK_satc_NumPedimentoAduanaSet]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'satc_UsoCFDISet'
ALTER TABLE [dbo].[satc_UsoCFDISet]
ADD CONSTRAINT [PK_satc_UsoCFDISet]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'satc_TipoRelacionSet'
ALTER TABLE [dbo].[satc_TipoRelacionSet]
ADD CONSTRAINT [PK_satc_TipoRelacionSet]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'satc_TipoFactorSet'
ALTER TABLE [dbo].[satc_TipoFactorSet]
ADD CONSTRAINT [PK_satc_TipoFactorSet]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'satc_TipoDeComprobanteSet'
ALTER TABLE [dbo].[satc_TipoDeComprobanteSet]
ADD CONSTRAINT [PK_satc_TipoDeComprobanteSet]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'satc_TasaOCuotaSet'
ALTER TABLE [dbo].[satc_TasaOCuotaSet]
ADD CONSTRAINT [PK_satc_TasaOCuotaSet]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'satc_RegimenFiscalSet'
ALTER TABLE [dbo].[satc_RegimenFiscalSet]
ADD CONSTRAINT [PK_satc_RegimenFiscalSet]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'satc_PatenteAduanalSet'
ALTER TABLE [dbo].[satc_PatenteAduanalSet]
ADD CONSTRAINT [PK_satc_PatenteAduanalSet]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'satc_PaisSet'
ALTER TABLE [dbo].[satc_PaisSet]
ADD CONSTRAINT [PK_satc_PaisSet]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- --------------------------------------------------
-- Creating all FOREIGN KEY constraints
-- --------------------------------------------------

-- --------------------------------------------------
-- Script has ended
-- --------------------------------------------------