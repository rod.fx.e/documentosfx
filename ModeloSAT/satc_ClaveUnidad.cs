//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace ModeloSAT
{
    using System;
    using System.Collections.Generic;
    
    public partial class satc_ClaveUnidad
    {
        public int Id { get; set; }
        public string c_ClaveUnidad { get; set; }
        public string descripcion { get; set; }
        public string nombre { get; set; }
        public string nota { get; set; }
        public Nullable<System.DateTime> fechaInicioVigencia { get; set; }
        public Nullable<System.DateTime> fechaFinVigencia { get; set; }
        public string simbolo { get; set; }
    }
}
