namespace Desarrollo
{
    partial class frmPrincipal
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmPrincipal));
            this.metroShell1 = new DevComponents.DotNetBar.Metro.MetroShell();
            this.metroTabPanel2 = new DevComponents.DotNetBar.Metro.MetroTabPanel();
            this.buttonX5 = new DevComponents.DotNetBar.ButtonX();
            this.labelX2 = new DevComponents.DotNetBar.LabelX();
            this.filtroFactura = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.buttonX2 = new DevComponents.DotNetBar.ButtonX();
            this.dtgCFDI = new DevComponents.DotNetBar.Controls.DataGridViewX();
            this.buttonX4 = new DevComponents.DotNetBar.ButtonX();
            this.metroTabItem2 = new DevComponents.DotNetBar.Metro.MetroTabItem();
            this.tabControlPanel2 = new DevComponents.DotNetBar.TabControlPanel();
            this.buttonX3 = new DevComponents.DotNetBar.ButtonX();
            this.LbServidor = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.CREDENCIALES = new System.Windows.Forms.CheckBox();
            this.LbUsuario = new System.Windows.Forms.Label();
            this.SSL = new System.Windows.Forms.CheckBox();
            this.LbPass = new System.Windows.Forms.Label();
            this.PASSWORD_EMAIL = new System.Windows.Forms.TextBox();
            this.LbPuerto = new System.Windows.Forms.Label();
            this.USUARIO_SMTP = new System.Windows.Forms.TextBox();
            this.SMTP = new System.Windows.Forms.TextBox();
            this.PUERTO = new System.Windows.Forms.TextBox();
            this.styleManager1 = new DevComponents.DotNetBar.StyleManager(this.components);
            this.estadoCFDI = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Serie = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Folio = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.rfcReceptorCFDI = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.nombreReceptorCFDI = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.uuidCFDI = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.timbradoCFDI = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ClaveProdServ = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ClaveUnidad = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Cantidad = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Unidad = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.NoIdentificacion = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Descripcion = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ValorUnitario = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Importe = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.TrasladoBase = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.TrasladoImpuesto = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.TipoFactor = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.TasaOCuota = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ImporteImpuesto = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.RetencionBase = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.RetencionImpuesto = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.RetencionImporte = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.metroShell1.SuspendLayout();
            this.metroTabPanel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dtgCFDI)).BeginInit();
            this.tabControlPanel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // metroShell1
            // 
            this.metroShell1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            // 
            // 
            // 
            this.metroShell1.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.metroShell1.CaptionVisible = true;
            this.metroShell1.Controls.Add(this.metroTabPanel2);
            this.metroShell1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.metroShell1.ForeColor = System.Drawing.Color.Black;
            this.metroShell1.HelpButtonText = "AYUDA";
            this.metroShell1.HelpButtonVisible = false;
            this.metroShell1.Items.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.metroTabItem2});
            this.metroShell1.KeyTipsFont = new System.Drawing.Font("Tahoma", 7F);
            this.metroShell1.Location = new System.Drawing.Point(0, 1);
            this.metroShell1.Name = "metroShell1";
            this.metroShell1.SettingsButtonVisible = false;
            this.metroShell1.Size = new System.Drawing.Size(1028, 450);
            this.metroShell1.SystemText.MaximizeRibbonText = "&Maximize the Ribbon";
            this.metroShell1.SystemText.MinimizeRibbonText = "Mi&nimize the Ribbon";
            this.metroShell1.SystemText.QatAddItemText = "&Add to Quick Access Toolbar";
            this.metroShell1.SystemText.QatCustomizeMenuLabel = "<b>Customize Quick Access Toolbar</b>";
            this.metroShell1.SystemText.QatCustomizeText = "&Customize Quick Access Toolbar...";
            this.metroShell1.SystemText.QatDialogAddButton = "&Add >>";
            this.metroShell1.SystemText.QatDialogCancelButton = "Cancel";
            this.metroShell1.SystemText.QatDialogCaption = "Customize Quick Access Toolbar";
            this.metroShell1.SystemText.QatDialogCategoriesLabel = "&Choose commands from:";
            this.metroShell1.SystemText.QatDialogOkButton = "OK";
            this.metroShell1.SystemText.QatDialogPlacementCheckbox = "&Place Quick Access Toolbar below the Ribbon";
            this.metroShell1.SystemText.QatDialogRemoveButton = "&Remove";
            this.metroShell1.SystemText.QatPlaceAboveRibbonText = "&Place Quick Access Toolbar above the Ribbon";
            this.metroShell1.SystemText.QatPlaceBelowRibbonText = "&Place Quick Access Toolbar below the Ribbon";
            this.metroShell1.SystemText.QatRemoveItemText = "&Remove from Quick Access Toolbar";
            this.metroShell1.TabIndex = 0;
            this.metroShell1.TabStripFont = new System.Drawing.Font("Segoe UI", 10.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.metroShell1.Text = "metroShell1";
            // 
            // metroTabPanel2
            // 
            this.metroTabPanel2.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.metroTabPanel2.Controls.Add(this.buttonX5);
            this.metroTabPanel2.Controls.Add(this.labelX2);
            this.metroTabPanel2.Controls.Add(this.filtroFactura);
            this.metroTabPanel2.Controls.Add(this.buttonX2);
            this.metroTabPanel2.Controls.Add(this.dtgCFDI);
            this.metroTabPanel2.Controls.Add(this.buttonX4);
            this.metroTabPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.metroTabPanel2.Location = new System.Drawing.Point(0, 51);
            this.metroTabPanel2.Name = "metroTabPanel2";
            this.metroTabPanel2.Padding = new System.Windows.Forms.Padding(3, 0, 3, 3);
            this.metroTabPanel2.Size = new System.Drawing.Size(1028, 399);
            // 
            // 
            // 
            this.metroTabPanel2.Style.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.metroTabPanel2.StyleMouseDown.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.metroTabPanel2.StyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.metroTabPanel2.TabIndex = 2;
            // 
            // buttonX5
            // 
            this.buttonX5.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.buttonX5.BackColor = System.Drawing.SystemColors.Control;
            this.buttonX5.ColorTable = DevComponents.DotNetBar.eButtonColor.Blue;
            this.buttonX5.Location = new System.Drawing.Point(153, 31);
            this.buttonX5.Name = "buttonX5";
            this.buttonX5.Size = new System.Drawing.Size(59, 23);
            this.buttonX5.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.buttonX5.TabIndex = 481;
            this.buttonX5.Text = "Filtrar";
            this.buttonX5.Click += new System.EventHandler(this.buttonX5_Click);
            // 
            // labelX2
            // 
            this.labelX2.AutoSize = true;
            this.labelX2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            // 
            // 
            // 
            this.labelX2.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX2.ForeColor = System.Drawing.Color.Black;
            this.labelX2.Location = new System.Drawing.Point(3, 33);
            this.labelX2.Name = "labelX2";
            this.labelX2.Size = new System.Drawing.Size(38, 17);
            this.labelX2.TabIndex = 480;
            this.labelX2.Text = "Factura";
            // 
            // filtroFactura
            // 
            this.filtroFactura.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            // 
            // 
            // 
            this.filtroFactura.Border.Class = "TextBoxBorder";
            this.filtroFactura.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.filtroFactura.ForeColor = System.Drawing.Color.Black;
            this.filtroFactura.Location = new System.Drawing.Point(56, 31);
            this.filtroFactura.Name = "filtroFactura";
            this.filtroFactura.Size = new System.Drawing.Size(91, 22);
            this.filtroFactura.TabIndex = 479;
            // 
            // buttonX2
            // 
            this.buttonX2.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.buttonX2.BackColor = System.Drawing.Color.DarkSeaGreen;
            this.buttonX2.ColorTable = DevComponents.DotNetBar.eButtonColor.Blue;
            this.buttonX2.Location = new System.Drawing.Point(218, 4);
            this.buttonX2.Name = "buttonX2";
            this.buttonX2.Size = new System.Drawing.Size(86, 23);
            this.buttonX2.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.buttonX2.TabIndex = 478;
            this.buttonX2.Text = "Exportar";
            this.buttonX2.Click += new System.EventHandler(this.buttonX2_Click);
            // 
            // dtgCFDI
            // 
            this.dtgCFDI.AllowUserToAddRows = false;
            this.dtgCFDI.AllowUserToDeleteRows = false;
            this.dtgCFDI.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dtgCFDI.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dtgCFDI.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.dtgCFDI.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dtgCFDI.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.estadoCFDI,
            this.Serie,
            this.Folio,
            this.rfcReceptorCFDI,
            this.nombreReceptorCFDI,
            this.uuidCFDI,
            this.timbradoCFDI,
            this.ClaveProdServ,
            this.ClaveUnidad,
            this.Cantidad,
            this.Unidad,
            this.NoIdentificacion,
            this.Descripcion,
            this.ValorUnitario,
            this.Importe,
            this.TrasladoBase,
            this.TrasladoImpuesto,
            this.TipoFactor,
            this.TasaOCuota,
            this.ImporteImpuesto,
            this.RetencionBase,
            this.RetencionImpuesto,
            this.RetencionImporte});
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dtgCFDI.DefaultCellStyle = dataGridViewCellStyle2;
            this.dtgCFDI.EnableHeadersVisualStyles = false;
            this.dtgCFDI.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(170)))), ((int)(((byte)(170)))), ((int)(((byte)(170)))));
            this.dtgCFDI.Location = new System.Drawing.Point(6, 58);
            this.dtgCFDI.Name = "dtgCFDI";
            this.dtgCFDI.ReadOnly = true;
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle3.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dtgCFDI.RowHeadersDefaultCellStyle = dataGridViewCellStyle3;
            this.dtgCFDI.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dtgCFDI.Size = new System.Drawing.Size(1016, 336);
            this.dtgCFDI.TabIndex = 477;
            // 
            // buttonX4
            // 
            this.buttonX4.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.buttonX4.BackColor = System.Drawing.SystemColors.Control;
            this.buttonX4.ColorTable = DevComponents.DotNetBar.eButtonColor.Blue;
            this.buttonX4.Location = new System.Drawing.Point(6, 4);
            this.buttonX4.Name = "buttonX4";
            this.buttonX4.Size = new System.Drawing.Size(206, 23);
            this.buttonX4.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.buttonX4.TabIndex = 476;
            this.buttonX4.Text = "1- Seleccione todos los archivos CFDI";
            this.buttonX4.TextAlignment = DevComponents.DotNetBar.eButtonTextAlignment.Left;
            this.buttonX4.Click += new System.EventHandler(this.buttonX4_Click);
            // 
            // metroTabItem2
            // 
            this.metroTabItem2.Checked = true;
            this.metroTabItem2.Name = "metroTabItem2";
            this.metroTabItem2.Panel = this.metroTabPanel2;
            this.metroTabItem2.Text = "CFDI Generales";
            // 
            // tabControlPanel2
            // 
            this.tabControlPanel2.Controls.Add(this.buttonX3);
            this.tabControlPanel2.Controls.Add(this.LbServidor);
            this.tabControlPanel2.Controls.Add(this.label6);
            this.tabControlPanel2.Controls.Add(this.pictureBox1);
            this.tabControlPanel2.Controls.Add(this.CREDENCIALES);
            this.tabControlPanel2.Controls.Add(this.LbUsuario);
            this.tabControlPanel2.Controls.Add(this.SSL);
            this.tabControlPanel2.Controls.Add(this.LbPass);
            this.tabControlPanel2.Controls.Add(this.PASSWORD_EMAIL);
            this.tabControlPanel2.Controls.Add(this.LbPuerto);
            this.tabControlPanel2.Controls.Add(this.USUARIO_SMTP);
            this.tabControlPanel2.Controls.Add(this.SMTP);
            this.tabControlPanel2.Controls.Add(this.PUERTO);
            this.tabControlPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControlPanel2.Location = new System.Drawing.Point(0, 27);
            this.tabControlPanel2.Name = "tabControlPanel2";
            this.tabControlPanel2.Padding = new System.Windows.Forms.Padding(1);
            this.tabControlPanel2.Size = new System.Drawing.Size(910, 273);
            this.tabControlPanel2.Style.BackColor1.Color = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.tabControlPanel2.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine;
            this.tabControlPanel2.Style.BorderColor.Color = System.Drawing.Color.FromArgb(((int)(((byte)(211)))), ((int)(((byte)(211)))), ((int)(((byte)(211)))));
            this.tabControlPanel2.Style.BorderSide = ((DevComponents.DotNetBar.eBorderSide)(((DevComponents.DotNetBar.eBorderSide.Left | DevComponents.DotNetBar.eBorderSide.Right) 
            | DevComponents.DotNetBar.eBorderSide.Bottom)));
            this.tabControlPanel2.Style.GradientAngle = 90;
            this.tabControlPanel2.TabIndex = 2;
            // 
            // buttonX3
            // 
            this.buttonX3.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.buttonX3.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.buttonX3.Location = new System.Drawing.Point(95, 178);
            this.buttonX3.Name = "buttonX3";
            this.buttonX3.Size = new System.Drawing.Size(113, 24);
            this.buttonX3.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.buttonX3.TabIndex = 11;
            // 
            // LbServidor
            // 
            this.LbServidor.AutoSize = true;
            this.LbServidor.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.LbServidor.ForeColor = System.Drawing.Color.Black;
            this.LbServidor.Location = new System.Drawing.Point(4, 9);
            this.LbServidor.Name = "LbServidor";
            this.LbServidor.Size = new System.Drawing.Size(0, 13);
            this.LbServidor.TabIndex = 0;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.label6.ForeColor = System.Drawing.Color.Black;
            this.label6.Location = new System.Drawing.Point(92, 162);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(0, 13);
            this.label6.TabIndex = 10;
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.pictureBox1.ForeColor = System.Drawing.Color.Black;
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(212, 181);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(22, 19);
            this.pictureBox1.TabIndex = 160;
            this.pictureBox1.TabStop = false;
            // 
            // CREDENCIALES
            // 
            this.CREDENCIALES.AutoSize = true;
            this.CREDENCIALES.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.CREDENCIALES.ForeColor = System.Drawing.Color.Black;
            this.CREDENCIALES.Location = new System.Drawing.Point(95, 142);
            this.CREDENCIALES.Name = "CREDENCIALES";
            this.CREDENCIALES.Size = new System.Drawing.Size(184, 17);
            this.CREDENCIALES.TabIndex = 9;
            this.CREDENCIALES.Text = "Servidor Requiere Autentificación";
            this.CREDENCIALES.UseVisualStyleBackColor = false;
            // 
            // LbUsuario
            // 
            this.LbUsuario.AutoSize = true;
            this.LbUsuario.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.LbUsuario.ForeColor = System.Drawing.Color.Black;
            this.LbUsuario.Location = new System.Drawing.Point(4, 37);
            this.LbUsuario.Name = "LbUsuario";
            this.LbUsuario.Size = new System.Drawing.Size(0, 13);
            this.LbUsuario.TabIndex = 2;
            // 
            // SSL
            // 
            this.SSL.AutoSize = true;
            this.SSL.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.SSL.ForeColor = System.Drawing.Color.Black;
            this.SSL.Location = new System.Drawing.Point(95, 118);
            this.SSL.Name = "SSL";
            this.SSL.Size = new System.Drawing.Size(220, 17);
            this.SSL.TabIndex = 8;
            this.SSL.Text = "El Servidor requiere una conexión segura";
            this.SSL.UseVisualStyleBackColor = false;
            // 
            // LbPass
            // 
            this.LbPass.AutoSize = true;
            this.LbPass.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.LbPass.ForeColor = System.Drawing.Color.Black;
            this.LbPass.Location = new System.Drawing.Point(4, 65);
            this.LbPass.Name = "LbPass";
            this.LbPass.Size = new System.Drawing.Size(0, 13);
            this.LbPass.TabIndex = 4;
            // 
            // PASSWORD_EMAIL
            // 
            this.PASSWORD_EMAIL.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.PASSWORD_EMAIL.ForeColor = System.Drawing.Color.Black;
            this.PASSWORD_EMAIL.Location = new System.Drawing.Point(95, 62);
            this.PASSWORD_EMAIL.Name = "PASSWORD_EMAIL";
            this.PASSWORD_EMAIL.PasswordChar = '*';
            this.PASSWORD_EMAIL.Size = new System.Drawing.Size(237, 20);
            this.PASSWORD_EMAIL.TabIndex = 5;
            // 
            // LbPuerto
            // 
            this.LbPuerto.AutoSize = true;
            this.LbPuerto.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.LbPuerto.ForeColor = System.Drawing.Color.Black;
            this.LbPuerto.Location = new System.Drawing.Point(4, 93);
            this.LbPuerto.Name = "LbPuerto";
            this.LbPuerto.Size = new System.Drawing.Size(0, 13);
            this.LbPuerto.TabIndex = 6;
            // 
            // USUARIO_SMTP
            // 
            this.USUARIO_SMTP.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.USUARIO_SMTP.ForeColor = System.Drawing.Color.Black;
            this.USUARIO_SMTP.Location = new System.Drawing.Point(95, 34);
            this.USUARIO_SMTP.Name = "USUARIO_SMTP";
            this.USUARIO_SMTP.Size = new System.Drawing.Size(237, 20);
            this.USUARIO_SMTP.TabIndex = 1;
            // 
            // SMTP
            // 
            this.SMTP.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.SMTP.ForeColor = System.Drawing.Color.Black;
            this.SMTP.Location = new System.Drawing.Point(95, 6);
            this.SMTP.Name = "SMTP";
            this.SMTP.Size = new System.Drawing.Size(237, 20);
            this.SMTP.TabIndex = 1;
            // 
            // PUERTO
            // 
            this.PUERTO.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.PUERTO.ForeColor = System.Drawing.Color.Black;
            this.PUERTO.Location = new System.Drawing.Point(95, 90);
            this.PUERTO.Name = "PUERTO";
            this.PUERTO.Size = new System.Drawing.Size(56, 20);
            this.PUERTO.TabIndex = 7;
            this.PUERTO.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // styleManager1
            // 
            this.styleManager1.ManagerStyle = DevComponents.DotNetBar.eStyle.Metro;
            this.styleManager1.MetroColorParameters = new DevComponents.DotNetBar.Metro.ColorTables.MetroColorGeneratorParameters(System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255))))), System.Drawing.Color.FromArgb(((int)(((byte)(33)))), ((int)(((byte)(115)))), ((int)(((byte)(70))))));
            // 
            // estadoCFDI
            // 
            this.estadoCFDI.HeaderText = "Estado";
            this.estadoCFDI.Name = "estadoCFDI";
            this.estadoCFDI.ReadOnly = true;
            // 
            // Serie
            // 
            this.Serie.HeaderText = "Serie";
            this.Serie.Name = "Serie";
            this.Serie.ReadOnly = true;
            // 
            // Folio
            // 
            this.Folio.HeaderText = "Folio";
            this.Folio.Name = "Folio";
            this.Folio.ReadOnly = true;
            // 
            // rfcReceptorCFDI
            // 
            this.rfcReceptorCFDI.HeaderText = "RFC Receptor";
            this.rfcReceptorCFDI.Name = "rfcReceptorCFDI";
            this.rfcReceptorCFDI.ReadOnly = true;
            // 
            // nombreReceptorCFDI
            // 
            this.nombreReceptorCFDI.HeaderText = "Nombre Receptor";
            this.nombreReceptorCFDI.Name = "nombreReceptorCFDI";
            this.nombreReceptorCFDI.ReadOnly = true;
            // 
            // uuidCFDI
            // 
            this.uuidCFDI.HeaderText = "UUID";
            this.uuidCFDI.Name = "uuidCFDI";
            this.uuidCFDI.ReadOnly = true;
            // 
            // timbradoCFDI
            // 
            this.timbradoCFDI.HeaderText = "Timbrado";
            this.timbradoCFDI.Name = "timbradoCFDI";
            this.timbradoCFDI.ReadOnly = true;
            // 
            // ClaveProdServ
            // 
            this.ClaveProdServ.HeaderText = "ClaveProdServ";
            this.ClaveProdServ.Name = "ClaveProdServ";
            this.ClaveProdServ.ReadOnly = true;
            // 
            // ClaveUnidad
            // 
            this.ClaveUnidad.HeaderText = "ClaveUnidad";
            this.ClaveUnidad.Name = "ClaveUnidad";
            this.ClaveUnidad.ReadOnly = true;
            // 
            // Cantidad
            // 
            this.Cantidad.HeaderText = "Cantidad";
            this.Cantidad.Name = "Cantidad";
            this.Cantidad.ReadOnly = true;
            // 
            // Unidad
            // 
            this.Unidad.HeaderText = "Unidad";
            this.Unidad.Name = "Unidad";
            this.Unidad.ReadOnly = true;
            // 
            // NoIdentificacion
            // 
            this.NoIdentificacion.HeaderText = "NoIdentificacion";
            this.NoIdentificacion.Name = "NoIdentificacion";
            this.NoIdentificacion.ReadOnly = true;
            // 
            // Descripcion
            // 
            this.Descripcion.HeaderText = "Descripcion";
            this.Descripcion.Name = "Descripcion";
            this.Descripcion.ReadOnly = true;
            // 
            // ValorUnitario
            // 
            this.ValorUnitario.HeaderText = "ValorUnitario";
            this.ValorUnitario.Name = "ValorUnitario";
            this.ValorUnitario.ReadOnly = true;
            // 
            // Importe
            // 
            this.Importe.HeaderText = "Importe";
            this.Importe.Name = "Importe";
            this.Importe.ReadOnly = true;
            // 
            // TrasladoBase
            // 
            this.TrasladoBase.HeaderText = "TrasladoBase";
            this.TrasladoBase.Name = "TrasladoBase";
            this.TrasladoBase.ReadOnly = true;
            // 
            // TrasladoImpuesto
            // 
            this.TrasladoImpuesto.HeaderText = "TrasladoImpuesto";
            this.TrasladoImpuesto.Name = "TrasladoImpuesto";
            this.TrasladoImpuesto.ReadOnly = true;
            // 
            // TipoFactor
            // 
            this.TipoFactor.HeaderText = "TrasladoTipoFactor";
            this.TipoFactor.Name = "TipoFactor";
            this.TipoFactor.ReadOnly = true;
            // 
            // TasaOCuota
            // 
            this.TasaOCuota.HeaderText = "TrasladoTasaOCuota";
            this.TasaOCuota.Name = "TasaOCuota";
            this.TasaOCuota.ReadOnly = true;
            // 
            // ImporteImpuesto
            // 
            this.ImporteImpuesto.HeaderText = "TrasladoImporte";
            this.ImporteImpuesto.Name = "ImporteImpuesto";
            this.ImporteImpuesto.ReadOnly = true;
            // 
            // RetencionBase
            // 
            this.RetencionBase.HeaderText = "RetencionBase";
            this.RetencionBase.Name = "RetencionBase";
            this.RetencionBase.ReadOnly = true;
            // 
            // RetencionImpuesto
            // 
            this.RetencionImpuesto.HeaderText = "RetencionImpuesto";
            this.RetencionImpuesto.Name = "RetencionImpuesto";
            this.RetencionImpuesto.ReadOnly = true;
            // 
            // RetencionImporte
            // 
            this.RetencionImporte.HeaderText = "RetencionImporte";
            this.RetencionImporte.Name = "RetencionImporte";
            this.RetencionImporte.ReadOnly = true;
            // 
            // frmPrincipal
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1029, 452);
            this.Controls.Add(this.metroShell1);
            this.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "frmPrincipal";
            this.Sizable = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Impresión de CFDI- Versión 1.0.0";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.frmPrincipal_FormClosed);
            this.metroShell1.ResumeLayout(false);
            this.metroShell1.PerformLayout();
            this.metroTabPanel2.ResumeLayout(false);
            this.metroTabPanel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dtgCFDI)).EndInit();
            this.tabControlPanel2.ResumeLayout(false);
            this.tabControlPanel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevComponents.DotNetBar.Metro.MetroShell metroShell1;
        private DevComponents.DotNetBar.StyleManager styleManager1;
        private DevComponents.DotNetBar.ButtonX buttonX3;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.CheckBox CREDENCIALES;
        private System.Windows.Forms.CheckBox SSL;
        private System.Windows.Forms.TextBox PASSWORD_EMAIL;
        private System.Windows.Forms.TextBox SMTP;
        private System.Windows.Forms.TextBox PUERTO;
        private System.Windows.Forms.Label LbPuerto;
        private System.Windows.Forms.Label LbPass;
        private System.Windows.Forms.Label LbUsuario;
        private System.Windows.Forms.Label LbServidor;
        private System.Windows.Forms.PictureBox pictureBox1;
        private DevComponents.DotNetBar.TabControlPanel tabControlPanel2;
        private System.Windows.Forms.TextBox USUARIO_SMTP;
        private DevComponents.DotNetBar.Metro.MetroTabPanel metroTabPanel2;
        private DevComponents.DotNetBar.Metro.MetroTabItem metroTabItem2;
        private DevComponents.DotNetBar.ButtonX buttonX2;
        private DevComponents.DotNetBar.Controls.DataGridViewX dtgCFDI;
        private DevComponents.DotNetBar.ButtonX buttonX4;
        private DevComponents.DotNetBar.ButtonX buttonX5;
        private DevComponents.DotNetBar.LabelX labelX2;
        private DevComponents.DotNetBar.Controls.TextBoxX filtroFactura;
        private System.Windows.Forms.DataGridViewTextBoxColumn estadoCFDI;
        private System.Windows.Forms.DataGridViewTextBoxColumn Serie;
        private System.Windows.Forms.DataGridViewTextBoxColumn Folio;
        private System.Windows.Forms.DataGridViewTextBoxColumn rfcReceptorCFDI;
        private System.Windows.Forms.DataGridViewTextBoxColumn nombreReceptorCFDI;
        private System.Windows.Forms.DataGridViewTextBoxColumn uuidCFDI;
        private System.Windows.Forms.DataGridViewTextBoxColumn timbradoCFDI;
        private System.Windows.Forms.DataGridViewTextBoxColumn ClaveProdServ;
        private System.Windows.Forms.DataGridViewTextBoxColumn ClaveUnidad;
        private System.Windows.Forms.DataGridViewTextBoxColumn Cantidad;
        private System.Windows.Forms.DataGridViewTextBoxColumn Unidad;
        private System.Windows.Forms.DataGridViewTextBoxColumn NoIdentificacion;
        private System.Windows.Forms.DataGridViewTextBoxColumn Descripcion;
        private System.Windows.Forms.DataGridViewTextBoxColumn ValorUnitario;
        private System.Windows.Forms.DataGridViewTextBoxColumn Importe;
        private System.Windows.Forms.DataGridViewTextBoxColumn TrasladoBase;
        private System.Windows.Forms.DataGridViewTextBoxColumn TrasladoImpuesto;
        private System.Windows.Forms.DataGridViewTextBoxColumn TipoFactor;
        private System.Windows.Forms.DataGridViewTextBoxColumn TasaOCuota;
        private System.Windows.Forms.DataGridViewTextBoxColumn ImporteImpuesto;
        private System.Windows.Forms.DataGridViewTextBoxColumn RetencionBase;
        private System.Windows.Forms.DataGridViewTextBoxColumn RetencionImpuesto;
        private System.Windows.Forms.DataGridViewTextBoxColumn RetencionImporte;
    }
}

