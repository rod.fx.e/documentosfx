using System;
using System.Data;
using System.Windows.Forms;
using DevComponents.DotNetBar;
using System.IO;
using System.Xml.Serialization;
using System.Xml;
using CFDI33;
using System.Xml.Linq;
using System.Linq;
using Desarrollo.clases;

namespace Desarrollo
{
    public partial class frmPrincipal : DevComponents.DotNetBar.Metro.MetroAppForm
    {
        public frmPrincipal()
        {
            InitializeComponent();
        }

        private void buttonItem2_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private bool IsDate(string fecha)
        {
            try
            {
                DateTime fecha_tmp=DateTime.Parse(fecha);
                return true;
            }
            catch
            {
                return false;
            }

        }
        private void frmPrincipal_FormClosed(object sender, FormClosedEventArgs e)
        {
            Application.Exit();
        }

        private void mostrar_mensaje(string mensaje, bool mostrar_error)
        {
            ToastNotification.Show(this,
                mensaje,
                mostrar_error ? global::Desarrollo.Properties.Resources.error : global::Desarrollo.Properties.Resources.guardar,
                3000,
                mostrar_error ? eToastGlowColor.Red : eToastGlowColor.Green,
                eToastPosition.TopCenter);
        }

        private void BUSCAR_KeyPress(object sender, KeyPressEventArgs e)
        {
            //Precionar Enter buscar
            if (e.KeyChar.ToString() == "\r")
            {
                //cargar();
            }
        }

        private void buttonX1_Click(object sender, EventArgs e)
        {
            filtrar();
        }

        private void filtrar()
        {
            if (filtroFactura.Text.Trim()!="")
            {

                foreach (DataGridViewRow row in this.dtgCFDI.Rows)
                {
                    if (row.Cells["Folio"].Value.ToString().Contains(dtgCFDI.Text))
                    {
                        row.Visible = true;
                    }
                    else
                    {
                        row.Visible = false;
                    }

                }
            }
            else
            {
                foreach (DataGridViewRow row in this.dtgCFDI.Rows)
                {
                    row.Visible = true;
                }
            }
        }

        private void cargarArchivo(DataGridViewCellEventArgs e)
        {
            if (e.RowIndex > -1)
            {
                string filename = this.dtgCFDI.Rows[e.RowIndex].Tag.ToString();
                if (File.Exists(filename))
                {
                    System.Diagnostics.Process.Start(filename);
                }
            }
        }

        private void buttonX2_Click(object sender, EventArgs e)
        {
            ExcelFX.exportar(dtgCFDI, "CFDI");
        }

        private void buttonX4_Click(object sender, EventArgs e)
        {
            cargar_cfdi();
        }

        private void cargar_cfdi()
        {
            OpenFileDialog openFileDialog = new OpenFileDialog();
            openFileDialog.Multiselect = true;
            openFileDialog.Filter = "Archivos CFDI  (*.xml)|*.xml";
            openFileDialog.InitialDirectory = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments);
            if (openFileDialog.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                this.dtgCFDI.Rows.Clear();
                foreach (string filename in openFileDialog.FileNames)
                {
                    int n = this.dtgCFDI.Rows.Add();

                    CFDI33.Comprobante oComprobante;
                    //Pasear el comprobante
                    try
                    {
                        XmlSerializer serializer_CFD_3 = new XmlSerializer(typeof(CFDI33.Comprobante));
                        TextReader reader = new StreamReader(filename);
                        oComprobante = (CFDI33.Comprobante)serializer_CFD_3.Deserialize(reader);
                        reader.Dispose();
                        reader.Close();
                        this.dtgCFDI.Rows[n].Tag = filename;
                        this.dtgCFDI.Rows[n].Cells["rfcReceptorCFDI"].Value = oComprobante.Receptor.Rfc;
                        this.dtgCFDI.Rows[n].Cells["nombreReceptorCFDI"].Value = oComprobante.Receptor.Nombre;

                        this.dtgCFDI.Rows[n].Cells["Folio"].Value = oComprobante.Folio;
                        this.dtgCFDI.Rows[n].Cells["Serie"].Value = oComprobante.Serie;

                        //Determinar si es Timbrado
                        XmlDocument docXML = new XmlDocument();
                        docXML.Load(filename);
                        XmlNodeList TimbreFiscalDigital = docXML.GetElementsByTagName("tfd:TimbreFiscalDigital");
                        if (TimbreFiscalDigital != null)
                        {

                            if (TimbreFiscalDigital[0] != null)
                            {
                                this.dtgCFDI.Rows[n].Cells["uuidCFDI"].Value = TimbreFiscalDigital[0].Attributes["UUID"].Value;
                                this.dtgCFDI.Rows[n].Cells["timbradoCFDI"].Value = TimbreFiscalDigital[0].Attributes["FechaTimbrado"].Value;
                            }
                        }
                        //Conceptos
                        bool agregarLinea = false;
                        foreach (ComprobanteConcepto concepto in oComprobante.Conceptos)
                        {
                            if (agregarLinea)
                            {
                                n= n = this.dtgCFDI.Rows.Add();
                            }
                            this.dtgCFDI.Rows[n].Cells["Cantidad"].Value = concepto.Cantidad;
                            this.dtgCFDI.Rows[n].Cells["ClaveProdServ"].Value = concepto.ClaveProdServ;
                            this.dtgCFDI.Rows[n].Cells["ClaveUnidad"].Value = concepto.ClaveUnidad;
                            this.dtgCFDI.Rows[n].Cells["Descripcion"].Value = concepto.Descripcion;
                            this.dtgCFDI.Rows[n].Cells["Importe"].Value = concepto.Importe;
                            this.dtgCFDI.Rows[n].Cells["NoIdentificacion"].Value = concepto.NoIdentificacion;

                            foreach (ComprobanteConceptoImpuestosRetencion impuestoTr in concepto.Impuestos.Retenciones)
                            {
                                this.dtgCFDI.Rows[n].Cells["RetencionImporte"].Value = concepto.Importe;
                            }
                            foreach (ComprobanteConceptoImpuestosTraslado impuestoTr in concepto.Impuestos.Traslados)
                            {
                                this.dtgCFDI.Rows[n].Cells["TrasladoImporte"].Value = concepto.Importe;
                            }

                            agregarLinea = true;
                        }


                        this.dtgCFDI.Rows[n].Cells["estadoCFDI"].Value = "Valido";
                    }
                    catch (Exception e)
                    {
                        dtgCFDI.Rows[n].ErrorText = "Cargar Comprobante Automatica: El archivo " + filename + " no es un CFDI v�lido.";
                        dtgCFDI.Rows[n].Cells["estado"].Value = "No valido";
                    }

                }

            }
        }

        private void buttonX5_Click(object sender, EventArgs e)
        {
            filtrar();
        }
    }
}