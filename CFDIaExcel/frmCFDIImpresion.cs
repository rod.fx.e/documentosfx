﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.IO;
using System.Windows.Forms;
using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;

namespace FE_FX
{
    public partial class frmCFDIImpresion : DevComponents.DotNetBar.Metro.MetroForm
    {
        ReportDocument oReport;
        
        public frmCFDIImpresion(ReportDocument oReportp)
        {
            oReport = oReportp;
            InitializeComponent();
        }

        private void frmCFDIImpresion_Load(object sender, EventArgs e)
        {
            crViewer.ReportSource = oReport;
        }
    }
}
