﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using ModeloDocumentosFX;

namespace PortalProveedores.Controllers
{
    public class socioNegociosController : ApiController
    {
        private ModeloDocumentosFXContainer db = new ModeloDocumentosFXContainer();

        // GET: api/socioNegocios
        public IQueryable<socioNegocio> GetsocioNegocioSet()
        {
            return db.socioNegocioSet;
        }

        // GET: api/socioNegocios/5
        [ResponseType(typeof(socioNegocio))]
        public IHttpActionResult GetsocioNegocio(int id)
        {
            socioNegocio socioNegocio = db.socioNegocioSet.Find(id);
            if (socioNegocio == null)
            {
                return NotFound();
            }

            return Ok(socioNegocio);
        }

        // PUT: api/socioNegocios/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutsocioNegocio(int id, socioNegocio socioNegocio)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != socioNegocio.Id)
            {
                return BadRequest();
            }

            db.Entry(socioNegocio).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!socioNegocioExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/socioNegocios
        [ResponseType(typeof(socioNegocio))]
        public IHttpActionResult PostsocioNegocio(socioNegocio socioNegocio)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.socioNegocioSet.Add(socioNegocio);
            db.SaveChanges();

            return CreatedAtRoute("DefaultApi", new { id = socioNegocio.Id }, socioNegocio);
        }

        // DELETE: api/socioNegocios/5
        [ResponseType(typeof(socioNegocio))]
        public IHttpActionResult DeletesocioNegocio(int id)
        {
            socioNegocio socioNegocio = db.socioNegocioSet.Find(id);
            if (socioNegocio == null)
            {
                return NotFound();
            }

            db.socioNegocioSet.Remove(socioNegocio);
            db.SaveChanges();

            return Ok(socioNegocio);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool socioNegocioExists(int id)
        {
            return db.socioNegocioSet.Count(e => e.Id == id) > 0;
        }
    }
}