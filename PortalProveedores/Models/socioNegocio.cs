﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace PortalProveedores.Models
{
    public class socioNegocio
    {
        [Display(Name = "Id")]
        public int Id { get; set; }
        [Display(Name = "RFC")]
        public string rfc { get; set; }
        [Display(Name = "Razón")]
        public string razon { get; set; }        
    }
}