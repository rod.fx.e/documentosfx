
-- --------------------------------------------------
-- Entity Designer DDL Script for SQL Server 2005, 2008, 2012 and Azure
-- --------------------------------------------------
-- Date Created: 05/05/2023 12:15:12
-- Generated from EDMX file: D:\Proyectos\DocumentosFX\SAT\ImportarCatalogos\ModeloCartaPorte\CartaPorteModelo.edmx
-- --------------------------------------------------

SET QUOTED_IDENTIFIER OFF;
GO
USE [documentosfx];
GO
IF SCHEMA_ID(N'dbo') IS NULL EXECUTE(N'CREATE SCHEMA [dbo]');
GO

-- --------------------------------------------------
-- Dropping existing FOREIGN KEY constraints
-- --------------------------------------------------

IF OBJECT_ID(N'[dbo].[FK_TransporteCartaPorte]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[CartaPorteSet] DROP CONSTRAINT [FK_TransporteCartaPorte];
GO
IF OBJECT_ID(N'[dbo].[FK_TransportistaCartaPorte]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[CartaPorteSet] DROP CONSTRAINT [FK_TransportistaCartaPorte];
GO

-- --------------------------------------------------
-- Dropping existing tables
-- --------------------------------------------------

IF OBJECT_ID(N'[dbo].[TransporteSet]', 'U') IS NOT NULL
    DROP TABLE [dbo].[TransporteSet];
GO
IF OBJECT_ID(N'[dbo].[TransportistaSet]', 'U') IS NOT NULL
    DROP TABLE [dbo].[TransportistaSet];
GO
IF OBJECT_ID(N'[dbo].[CartaPorteSet]', 'U') IS NOT NULL
    DROP TABLE [dbo].[CartaPorteSet];
GO

-- --------------------------------------------------
-- Creating all tables
-- --------------------------------------------------

-- Creating table 'TransporteSet'
CREATE TABLE [dbo].[TransporteSet] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [PermisoSCT] nvarchar(max)  NOT NULL,
    [NumeroPermisoSCT] nvarchar(max)  NOT NULL,
    [Placa] nvarchar(max)  NOT NULL,
    [Anio] nvarchar(max)  NOT NULL,
    [AseguradoraResponsabilidadCivil] nvarchar(max)  NOT NULL,
    [PolizaResponsabilidadCivil] nvarchar(max)  NOT NULL,
    [AseguradoraMedioAmbiente] nvarchar(max)  NOT NULL,
    [PolizaMedioAmbiente] nvarchar(max)  NOT NULL,
    [AseguradoraCarga] nvarchar(max)  NOT NULL,
    [PolizaCarga] nvarchar(max)  NOT NULL,
    [ValorMateriaPrimaSeguro] nvarchar(max)  NOT NULL,
    [SubtipoRemolque] nvarchar(max)  NOT NULL,
    [PlacaRemolque] nvarchar(max)  NOT NULL
);
GO

-- Creating table 'TransportistaSet'
CREATE TABLE [dbo].[TransportistaSet] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Tipos] nvarchar(max)  NOT NULL,
    [RFC] nvarchar(max)  NOT NULL,
    [NumeroLicencia] nvarchar(max)  NOT NULL,
    [Nombre] nvarchar(max)  NOT NULL,
    [Residencia] nvarchar(max)  NOT NULL,
    [Domicilio] nvarchar(max)  NOT NULL,
    [Estado] nvarchar(max)  NOT NULL,
    [Pais] nvarchar(max)  NOT NULL,
    [CodigoPostal] nvarchar(max)  NOT NULL
);
GO

-- Creating table 'CartaPorteSet'
CREATE TABLE [dbo].[CartaPorteSet] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [InvoiceId] nvarchar(max)  NOT NULL,
    [FechaCreacion] datetime  NOT NULL,
    [Usuario] nvarchar(max)  NOT NULL,
    [Xml] nvarchar(max)  NULL,
    [Pdf] nvarchar(max)  NULL,
    [Estado] nvarchar(max)  NULL,
    [TotalDistRec] nvarchar(max)  NULL,
    [RFCRemitenteDestinatario] nvarchar(max)  NULL,
    [NombreRemitenteDestinatario] nvarchar(max)  NULL,
    [Transporte_Id] int  NOT NULL,
    [Transportista_Id] int  NOT NULL
);
GO

-- --------------------------------------------------
-- Creating all PRIMARY KEY constraints
-- --------------------------------------------------

-- Creating primary key on [Id] in table 'TransporteSet'
ALTER TABLE [dbo].[TransporteSet]
ADD CONSTRAINT [PK_TransporteSet]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'TransportistaSet'
ALTER TABLE [dbo].[TransportistaSet]
ADD CONSTRAINT [PK_TransportistaSet]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'CartaPorteSet'
ALTER TABLE [dbo].[CartaPorteSet]
ADD CONSTRAINT [PK_CartaPorteSet]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- --------------------------------------------------
-- Creating all FOREIGN KEY constraints
-- --------------------------------------------------

-- Creating foreign key on [Transporte_Id] in table 'CartaPorteSet'
ALTER TABLE [dbo].[CartaPorteSet]
ADD CONSTRAINT [FK_TransporteCartaPorte]
    FOREIGN KEY ([Transporte_Id])
    REFERENCES [dbo].[TransporteSet]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_TransporteCartaPorte'
CREATE INDEX [IX_FK_TransporteCartaPorte]
ON [dbo].[CartaPorteSet]
    ([Transporte_Id]);
GO

-- Creating foreign key on [Transportista_Id] in table 'CartaPorteSet'
ALTER TABLE [dbo].[CartaPorteSet]
ADD CONSTRAINT [FK_TransportistaCartaPorte]
    FOREIGN KEY ([Transportista_Id])
    REFERENCES [dbo].[TransportistaSet]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_TransportistaCartaPorte'
CREATE INDEX [IX_FK_TransportistaCartaPorte]
ON [dbo].[CartaPorteSet]
    ([Transportista_Id]);
GO

-- --------------------------------------------------
-- Script has ended
-- --------------------------------------------------