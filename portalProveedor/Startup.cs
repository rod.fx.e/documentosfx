﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(portalProveedor.Startup))]
namespace portalProveedor
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
