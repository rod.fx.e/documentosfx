﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.ModelBinding;
using System.Web.Http.OData;
using System.Web.Http.OData.Routing;
using ModeloDocumentosFX;

namespace portalProveedor.Controllers
{
    /*
    The WebApiConfig class may require additional changes to add a route for this controller. Merge these statements into the Register method of the WebApiConfig class as applicable. Note that OData URLs are case sensitive.

    using System.Web.Http.OData.Builder;
    using System.Web.Http.OData.Extensions;
    using ModeloDocumentosFX;
    ODataConventionModelBuilder builder = new ODataConventionModelBuilder();
    builder.EntitySet<socioNegocio>("socioNegocios");
    builder.EntitySet<notificacion>("notificacionSet"); 
    builder.EntitySet<configuracion>("configuracionSet"); 
    builder.EntitySet<documento>("documentoSet"); 
    builder.EntitySet<usuario>("usuarioSet"); 
    config.Routes.MapODataServiceRoute("odata", "odata", builder.GetEdmModel());
    */
    public class socioNegociosController : ODataController
    {
        private ModeloDocumentosFXContainer db = new ModeloDocumentosFXContainer();

        // GET: odata/socioNegocios
        [EnableQuery]
        public IQueryable<socioNegocio> GetsocioNegocios()
        {
            return db.socioNegocioSet;
        }

        // GET: odata/socioNegocios(5)
        [EnableQuery]
        public SingleResult<socioNegocio> GetsocioNegocio([FromODataUri] int key)
        {
            return SingleResult.Create(db.socioNegocioSet.Where(socioNegocio => socioNegocio.Id == key));
        }

        // PUT: odata/socioNegocios(5)
        public IHttpActionResult Put([FromODataUri] int key, Delta<socioNegocio> patch)
        {
            Validate(patch.GetEntity());

            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            socioNegocio socioNegocio = db.socioNegocioSet.Find(key);
            if (socioNegocio == null)
            {
                return NotFound();
            }

            patch.Put(socioNegocio);

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!socioNegocioExists(key))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return Updated(socioNegocio);
        }

        // POST: odata/socioNegocios
        public IHttpActionResult Post(socioNegocio socioNegocio)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.socioNegocioSet.Add(socioNegocio);
            db.SaveChanges();

            return Created(socioNegocio);
        }

        // PATCH: odata/socioNegocios(5)
        [AcceptVerbs("PATCH", "MERGE")]
        public IHttpActionResult Patch([FromODataUri] int key, Delta<socioNegocio> patch)
        {
            Validate(patch.GetEntity());

            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            socioNegocio socioNegocio = db.socioNegocioSet.Find(key);
            if (socioNegocio == null)
            {
                return NotFound();
            }

            patch.Patch(socioNegocio);

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!socioNegocioExists(key))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return Updated(socioNegocio);
        }

        // DELETE: odata/socioNegocios(5)
        public IHttpActionResult Delete([FromODataUri] int key)
        {
            socioNegocio socioNegocio = db.socioNegocioSet.Find(key);
            if (socioNegocio == null)
            {
                return NotFound();
            }

            db.socioNegocioSet.Remove(socioNegocio);
            db.SaveChanges();

            return StatusCode(HttpStatusCode.NoContent);
        }

        // GET: odata/socioNegocios(5)/notificacion
        [EnableQuery]
        public IQueryable<notificacion> Getnotificacion([FromODataUri] int key)
        {
            return db.socioNegocioSet.Where(m => m.Id == key).SelectMany(m => m.notificacion);
        }

        // GET: odata/socioNegocios(5)/configuracion
        [EnableQuery]
        public IQueryable<configuracion> Getconfiguracion([FromODataUri] int key)
        {
            return db.socioNegocioSet.Where(m => m.Id == key).SelectMany(m => m.configuracion);
        }

        // GET: odata/socioNegocios(5)/documentoRfcEmisor
        [EnableQuery]
        public IQueryable<documento> GetdocumentoRfcEmisor([FromODataUri] int key)
        {
            return db.socioNegocioSet.Where(m => m.Id == key).SelectMany(m => m.documentoRfcEmisor);
        }

        // GET: odata/socioNegocios(5)/documentoRfcReceptor
        [EnableQuery]
        public IQueryable<documento> GetdocumentoRfcReceptor([FromODataUri] int key)
        {
            return db.socioNegocioSet.Where(m => m.Id == key).SelectMany(m => m.documentoRfcReceptor);
        }

        // GET: odata/socioNegocios(5)/usuario
        [EnableQuery]
        public IQueryable<usuario> Getusuario([FromODataUri] int key)
        {
            return db.socioNegocioSet.Where(m => m.Id == key).SelectMany(m => m.usuario);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool socioNegocioExists(int key)
        {
            return db.socioNegocioSet.Count(e => e.Id == key) > 0;
        }
    }
}
