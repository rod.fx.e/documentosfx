
-- --------------------------------------------------
-- Entity Designer DDL Script for SQL Server 2005, 2008, 2012 and Azure
-- --------------------------------------------------
-- Date Created: 07/19/2023 16:16:13
-- Generated from EDMX file: D:\Proyectos\DocumentosFX\SAT\ImportarCatalogos\ModeloComprobantePago\ModeloComprobantePago.edmx
-- --------------------------------------------------

SET QUOTED_IDENTIFIER OFF;
GO
USE [documentosfx];
GO
IF SCHEMA_ID(N'dbo') IS NULL EXECUTE(N'CREATE SCHEMA [dbo]');
GO

-- --------------------------------------------------
-- Dropping existing FOREIGN KEY constraints
-- --------------------------------------------------

IF OBJECT_ID(N'[dbo].[FK_pagopagoLinea]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[pagoLineaSet] DROP CONSTRAINT [FK_pagopagoLinea];
GO
IF OBJECT_ID(N'[dbo].[FK_pagopagoTotales]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[pagoTotalesSet] DROP CONSTRAINT [FK_pagopagoTotales];
GO
IF OBJECT_ID(N'[dbo].[FK_pagoLineapagoLineaImpuesto]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[pagoLineaImpuestoSet] DROP CONSTRAINT [FK_pagoLineapagoLineaImpuesto];
GO
IF OBJECT_ID(N'[dbo].[FK_pagopagoImpuesto]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[pagoImpuestoSet] DROP CONSTRAINT [FK_pagopagoImpuesto];
GO
IF OBJECT_ID(N'[dbo].[FK_pagoLineapagoLineaRetencion]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[pagoLineaRetencionSet] DROP CONSTRAINT [FK_pagoLineapagoLineaRetencion];
GO
IF OBJECT_ID(N'[dbo].[FK_pagopagoRetencion]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[pagoRetencionSet] DROP CONSTRAINT [FK_pagopagoRetencion];
GO

-- --------------------------------------------------
-- Dropping existing tables
-- --------------------------------------------------

IF OBJECT_ID(N'[dbo].[pagoSet]', 'U') IS NOT NULL
    DROP TABLE [dbo].[pagoSet];
GO
IF OBJECT_ID(N'[dbo].[pagoLineaSet]', 'U') IS NOT NULL
    DROP TABLE [dbo].[pagoLineaSet];
GO
IF OBJECT_ID(N'[dbo].[pagoRelacionadoSet]', 'U') IS NOT NULL
    DROP TABLE [dbo].[pagoRelacionadoSet];
GO
IF OBJECT_ID(N'[dbo].[pagoTotalesSet]', 'U') IS NOT NULL
    DROP TABLE [dbo].[pagoTotalesSet];
GO
IF OBJECT_ID(N'[dbo].[pagoLineaImpuestoSet]', 'U') IS NOT NULL
    DROP TABLE [dbo].[pagoLineaImpuestoSet];
GO
IF OBJECT_ID(N'[dbo].[pagoImpuestoSet]', 'U') IS NOT NULL
    DROP TABLE [dbo].[pagoImpuestoSet];
GO
IF OBJECT_ID(N'[dbo].[pagoLineaRetencionSet]', 'U') IS NOT NULL
    DROP TABLE [dbo].[pagoLineaRetencionSet];
GO
IF OBJECT_ID(N'[dbo].[pagoRetencionSet]', 'U') IS NOT NULL
    DROP TABLE [dbo].[pagoRetencionSet];
GO

-- --------------------------------------------------
-- Creating all tables
-- --------------------------------------------------

-- Creating table 'pagoSet'
CREATE TABLE [dbo].[pagoSet] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [emisor] nvarchar(max)  NOT NULL,
    [receptor] nvarchar(max)  NOT NULL,
    [serie] nvarchar(max)  NOT NULL,
    [folio] nvarchar(max)  NOT NULL,
    [fecha] datetime  NOT NULL,
    [sello] nvarchar(max)  NULL,
    [noCertificado] nvarchar(max)  NOT NULL,
    [certificado] nvarchar(max)  NULL,
    [lugarExpedicion] nvarchar(max)  NOT NULL,
    [confirmacion] nvarchar(max)  NULL,
    [emisorNombre] nvarchar(max)  NOT NULL,
    [emisorRegimenFiscal] nvarchar(max)  NOT NULL,
    [receptorNombre] nvarchar(max)  NOT NULL,
    [receptorResidenciaFiscal] nvarchar(max)  NULL,
    [receptorNumRegIdTrib] nvarchar(max)  NULL,
    [receptorCodigoPostal] nvarchar(max)  NULL,
    [receptorPais] nvarchar(max)  NULL,
    [fechaPago] datetime  NOT NULL,
    [formaDePagoP] nvarchar(max)  NULL,
    [monedaP] nvarchar(max)  NULL,
    [tipoCambioP] decimal(18,4)  NULL,
    [monto] decimal(18,4)  NULL,
    [NumOperacion] nvarchar(max)  NULL,
    [RfcEmisorCtaOrd] nvarchar(max)  NULL,
    [NomBancoOrdExt] nvarchar(max)  NULL,
    [CtaOrdenante] nvarchar(max)  NULL,
    [RfcEmisorCtaBen] nvarchar(max)  NULL,
    [CtaBeneficiario] nvarchar(max)  NULL,
    [estado] nvarchar(max)  NOT NULL,
    [xml] nvarchar(max)  NULL,
    [pdf] nvarchar(max)  NULL,
    [observaciones] nvarchar(max)  NULL,
    [uuid] nvarchar(50)  NULL,
    [error] nvarchar(max)  NULL,
    [checkId] nvarchar(max)  NULL,
    [deposit] nvarchar(max)  NULL,
    [customerId] nvarchar(20)  NULL,
    [cancelado] nvarchar(max)  NULL,
    [pdfEstado] nvarchar(max)  NULL,
    [editado] bit  NOT NULL,
    [editadoTotal] bit  NOT NULL,
    [version] nvarchar(max)  NOT NULL,
    [DomicilioFiscalReceptor] nvarchar(max)  NULL,
    [RegimenFiscalReceptor] nvarchar(max)  NULL
);
GO

-- Creating table 'pagoLineaSet'
CREATE TABLE [dbo].[pagoLineaSet] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [factura] nvarchar(50)  NOT NULL,
    [UUID] nvarchar(50)  NOT NULL,
    [MonedaDR] nvarchar(max)  NOT NULL,
    [NumParcialidad] nvarchar(max)  NULL,
    [ImpSaldoAnt] decimal(18,4)  NULL,
    [ImpPagado] decimal(18,4)  NULL,
    [EquivalenciaDR] decimal(18,10)  NOT NULL,
    [ImpSaldoInsoluto] decimal(18,8)  NULL,
    [TipoCambioDR] decimal(18,4)  NULL,
    [folio] nvarchar(15)  NULL,
    [serie] nvarchar(15)  NULL,
    [pago_Id] int  NOT NULL
);
GO

-- Creating table 'pagoRelacionadoSet'
CREATE TABLE [dbo].[pagoRelacionadoSet] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [UUID] nvarchar(50)  NOT NULL,
    [tipoRelacion] nvarchar(50)  NOT NULL,
    [linea] int  NOT NULL,
    [checkId] nvarchar(40)  NULL,
    [customerId] nvarchar(20)  NULL
);
GO

-- Creating table 'pagoTotalesSet'
CREATE TABLE [dbo].[pagoTotalesSet] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [TotalRetencionesIVA] decimal(18,4)  NOT NULL,
    [TotalRetencionesISR] decimal(18,4)  NOT NULL,
    [TotalRetencionesIEPS] decimal(18,4)  NOT NULL,
    [TotalTrasladosBaseIVA16] decimal(18,4)  NOT NULL,
    [TotalTrasladosImpuestoIVA16] decimal(18,4)  NOT NULL,
    [TotalTrasladosBaseIVA8] decimal(18,4)  NOT NULL,
    [TotalTrasladosImpuestoIVA8] decimal(18,4)  NOT NULL,
    [TotalTrasladosBaseIVA0] decimal(18,4)  NOT NULL,
    [TotalTrasladosImpuestoIVA0] decimal(18,4)  NOT NULL,
    [TotalTrasladosBaseIVAExento] decimal(18,4)  NOT NULL,
    [MontoTotalPagos] decimal(18,4)  NOT NULL,
    [pago_Id] int  NOT NULL
);
GO

-- Creating table 'pagoLineaImpuestoSet'
CREATE TABLE [dbo].[pagoLineaImpuestoSet] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [BaseDR] decimal(18,4)  NOT NULL,
    [ImpuestoDR] nvarchar(50)  NOT NULL,
    [TipoFactorDR] nvarchar(50)  NOT NULL,
    [TasaOCuotaDR] decimal(18,4)  NOT NULL,
    [ImporteDR] decimal(18,4)  NOT NULL,
    [pagoLinea_Id] int  NOT NULL
);
GO

-- Creating table 'pagoImpuestoSet'
CREATE TABLE [dbo].[pagoImpuestoSet] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [BaseP] decimal(18,4)  NOT NULL,
    [ImpuestoP] nvarchar(50)  NOT NULL,
    [TipoFactorP] nvarchar(50)  NOT NULL,
    [TasaOCuotaP] decimal(18,4)  NOT NULL,
    [ImporteP] decimal(18,4)  NOT NULL,
    [pago_Id] int  NOT NULL
);
GO

-- Creating table 'pagoLineaRetencionSet'
CREATE TABLE [dbo].[pagoLineaRetencionSet] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [BaseDR] decimal(18,4)  NOT NULL,
    [ImpuestoDR] nvarchar(50)  NOT NULL,
    [TipoFactorDR] nvarchar(50)  NOT NULL,
    [TasaOCuotaDR] decimal(18,4)  NOT NULL,
    [ImporteDR] decimal(18,4)  NOT NULL,
    [pagoLinea_Id] int  NOT NULL
);
GO

-- Creating table 'pagoRetencionSet'
CREATE TABLE [dbo].[pagoRetencionSet] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [BaseP] decimal(18,4)  NOT NULL,
    [ImpuestoP] nvarchar(50)  NOT NULL,
    [TipoFactorP] nvarchar(50)  NOT NULL,
    [TasaOCuotaP] decimal(18,4)  NOT NULL,
    [ImporteP] decimal(18,4)  NOT NULL,
    [pago_Id] int  NOT NULL
);
GO

-- --------------------------------------------------
-- Creating all PRIMARY KEY constraints
-- --------------------------------------------------

-- Creating primary key on [Id] in table 'pagoSet'
ALTER TABLE [dbo].[pagoSet]
ADD CONSTRAINT [PK_pagoSet]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'pagoLineaSet'
ALTER TABLE [dbo].[pagoLineaSet]
ADD CONSTRAINT [PK_pagoLineaSet]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'pagoRelacionadoSet'
ALTER TABLE [dbo].[pagoRelacionadoSet]
ADD CONSTRAINT [PK_pagoRelacionadoSet]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'pagoTotalesSet'
ALTER TABLE [dbo].[pagoTotalesSet]
ADD CONSTRAINT [PK_pagoTotalesSet]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'pagoLineaImpuestoSet'
ALTER TABLE [dbo].[pagoLineaImpuestoSet]
ADD CONSTRAINT [PK_pagoLineaImpuestoSet]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'pagoImpuestoSet'
ALTER TABLE [dbo].[pagoImpuestoSet]
ADD CONSTRAINT [PK_pagoImpuestoSet]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'pagoLineaRetencionSet'
ALTER TABLE [dbo].[pagoLineaRetencionSet]
ADD CONSTRAINT [PK_pagoLineaRetencionSet]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'pagoRetencionSet'
ALTER TABLE [dbo].[pagoRetencionSet]
ADD CONSTRAINT [PK_pagoRetencionSet]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- --------------------------------------------------
-- Creating all FOREIGN KEY constraints
-- --------------------------------------------------

-- Creating foreign key on [pago_Id] in table 'pagoLineaSet'
ALTER TABLE [dbo].[pagoLineaSet]
ADD CONSTRAINT [FK_pagopagoLinea]
    FOREIGN KEY ([pago_Id])
    REFERENCES [dbo].[pagoSet]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_pagopagoLinea'
CREATE INDEX [IX_FK_pagopagoLinea]
ON [dbo].[pagoLineaSet]
    ([pago_Id]);
GO

-- Creating foreign key on [pago_Id] in table 'pagoTotalesSet'
ALTER TABLE [dbo].[pagoTotalesSet]
ADD CONSTRAINT [FK_pagopagoTotales]
    FOREIGN KEY ([pago_Id])
    REFERENCES [dbo].[pagoSet]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_pagopagoTotales'
CREATE INDEX [IX_FK_pagopagoTotales]
ON [dbo].[pagoTotalesSet]
    ([pago_Id]);
GO

-- Creating foreign key on [pagoLinea_Id] in table 'pagoLineaImpuestoSet'
ALTER TABLE [dbo].[pagoLineaImpuestoSet]
ADD CONSTRAINT [FK_pagoLineapagoLineaImpuesto]
    FOREIGN KEY ([pagoLinea_Id])
    REFERENCES [dbo].[pagoLineaSet]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_pagoLineapagoLineaImpuesto'
CREATE INDEX [IX_FK_pagoLineapagoLineaImpuesto]
ON [dbo].[pagoLineaImpuestoSet]
    ([pagoLinea_Id]);
GO

-- Creating foreign key on [pago_Id] in table 'pagoImpuestoSet'
ALTER TABLE [dbo].[pagoImpuestoSet]
ADD CONSTRAINT [FK_pagopagoImpuesto]
    FOREIGN KEY ([pago_Id])
    REFERENCES [dbo].[pagoSet]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_pagopagoImpuesto'
CREATE INDEX [IX_FK_pagopagoImpuesto]
ON [dbo].[pagoImpuestoSet]
    ([pago_Id]);
GO

-- Creating foreign key on [pagoLinea_Id] in table 'pagoLineaRetencionSet'
ALTER TABLE [dbo].[pagoLineaRetencionSet]
ADD CONSTRAINT [FK_pagoLineapagoLineaRetencion]
    FOREIGN KEY ([pagoLinea_Id])
    REFERENCES [dbo].[pagoLineaSet]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_pagoLineapagoLineaRetencion'
CREATE INDEX [IX_FK_pagoLineapagoLineaRetencion]
ON [dbo].[pagoLineaRetencionSet]
    ([pagoLinea_Id]);
GO

-- Creating foreign key on [pago_Id] in table 'pagoRetencionSet'
ALTER TABLE [dbo].[pagoRetencionSet]
ADD CONSTRAINT [FK_pagopagoRetencion]
    FOREIGN KEY ([pago_Id])
    REFERENCES [dbo].[pagoSet]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_pagopagoRetencion'
CREATE INDEX [IX_FK_pagopagoRetencion]
ON [dbo].[pagoRetencionSet]
    ([pago_Id]);
GO

-- --------------------------------------------------
-- Script has ended
-- --------------------------------------------------