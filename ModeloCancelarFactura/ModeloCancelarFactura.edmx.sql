
-- --------------------------------------------------
-- Entity Designer DDL Script for SQL Server 2005, 2008, 2012 and Azure
-- --------------------------------------------------
-- Date Created: 07/11/2023 09:48:22
-- Generated from EDMX file: D:\Proyectos\DocumentosFX\SAT\ImportarCatalogos\ModeloCancelarFactura\ModeloCancelarFactura.edmx
-- --------------------------------------------------

SET QUOTED_IDENTIFIER OFF;
GO
USE [documentosfx];
GO
IF SCHEMA_ID(N'dbo') IS NULL EXECUTE(N'CREATE SCHEMA [dbo]');
GO

-- --------------------------------------------------
-- Dropping existing FOREIGN KEY constraints
-- --------------------------------------------------


-- --------------------------------------------------
-- Dropping existing tables
-- --------------------------------------------------

IF OBJECT_ID(N'[dbo].[FacturaCanceladaSet]', 'U') IS NOT NULL
    DROP TABLE [dbo].[FacturaCanceladaSet];
GO

-- --------------------------------------------------
-- Creating all tables
-- --------------------------------------------------

-- Creating table 'FacturaCanceladaSet'
CREATE TABLE [dbo].[FacturaCanceladaSet] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [factura] nvarchar(30)  NOT NULL,
    [rfcE] nvarchar(200)  NOT NULL,
    [status] nvarchar(200)  NOT NULL,
    [statusCode] nvarchar(200)  NOT NULL,
    [uuid] nvarchar(200)  NOT NULL,
    [ack] nvarchar(max)  NOT NULL,
    [fechaCreacion] datetime  NOT NULL
);
GO

-- --------------------------------------------------
-- Creating all PRIMARY KEY constraints
-- --------------------------------------------------

-- Creating primary key on [Id] in table 'FacturaCanceladaSet'
ALTER TABLE [dbo].[FacturaCanceladaSet]
ADD CONSTRAINT [PK_FacturaCanceladaSet]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- --------------------------------------------------
-- Creating all FOREIGN KEY constraints
-- --------------------------------------------------

-- --------------------------------------------------
-- Script has ended
-- --------------------------------------------------