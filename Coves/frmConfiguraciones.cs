﻿using OfficeOpenXml;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.Entity.Validation;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Coves
{
    public partial class frmConfiguraciones : Form
    {
        public frmConfiguraciones()
        {
            InitializeComponent();
            cargar();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            frmConfiguracion o = new frmConfiguracion();
            o.ShowDialog();
            cargar();
        }

        private void eliminar()
        {
            dtg.EndEdit();
            if (dtg.SelectedCells.Count > 0)
            {
                DialogResult Resultado = MessageBox.Show("¿Desea eliminar las seleccionada?", Application.ProductName + "-" + Application.ProductVersion, MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                if (Resultado.ToString() != "Yes")
                {
                    return;
                }
                foreach (DataGridViewRow drv in dtg.SelectedRows)
                {
                    unidadMedida oRegistroTMP = (unidadMedida)drv.Tag;
                    COVEEntities dbContext = new COVEEntities();

                    unidadMedida oRegistro = dbContext.unidadMedidaSet
                        .Where(a => a.Id == oRegistroTMP.Id)
                        .FirstOrDefault();

                    dbContext.unidadMedidaSet.Attach(oRegistro);
                    dbContext.unidadMedidaSet.Remove(oRegistro);
                    dbContext.SaveChanges();
                    dtg.Rows.Remove(drv);
                }
            }
            else
            {
                MessageBox.Show("Seleccione alguna línea.", Application.ProductName + "-" + Application.ProductVersion, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }

        private void cargar()
        {
            dtg.Rows.Clear();
            COVEEntities dbContext = new COVEEntities();
            try
            {
                var result = from b in dbContext.unidadMedidaSet
                             .Where(a => a.um.Contains(buscar.Text))
                             select b;

                List<unidadMedida> dt = result.ToList();
                dtg.Rows.Clear();
                foreach (unidadMedida registro in dt)
                {
                    int n = dtg.Rows.Add();
                    dtg.Rows[n].Tag = registro;
                    dtg.Rows[n].Cells["umCol"].Value = registro.um;
                    dtg.Rows[n].Cells["umVisualCol"].Value = registro.umVisual;
                    dtg.Rows[n].Cells["descripcionCol"].Value = registro.descripcion;
                }
                dbContext.Dispose();
                this.dtg.Columns[0].AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;
                this.dtg.Columns[1].AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;
                this.dtg.Columns[2].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
            }
            catch (DbEntityValidationException e)
            {
                foreach (var eve in e.EntityValidationErrors)
                {
                    foreach (var ve in eve.ValidationErrors)
                    {
                        MessageBox.Show(this, ve.PropertyName + " Error: " + ve.ErrorMessage, Application.ProductName
                            , MessageBoxButtons.OK, MessageBoxIcon.Error);
                        return;
                    }
                }
            }


        }

        private void buscar_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)13)
            {
                cargar();
            }
        }

        private void dtg_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            cargar_registro(e);
        }

        private void cargar_registro(DataGridViewCellEventArgs e)
        {
            if (e.RowIndex > -1)
            {
                unidadMedida registro = (unidadMedida)dtg.Rows[e.RowIndex].Tag;
                frmConfiguracion oObjeto = new frmConfiguracion(registro);
                oObjeto.ShowDialog();
                cargar();
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            eliminar();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            importar();
        }

        private void importar()
        {
            OpenFileDialog openFileDialog1 = new OpenFileDialog();
            openFileDialog1.Filter = "Excel files (*.xlsx)|*.xlsx|All files (*.*)|*.*";
            openFileDialog1.FilterIndex = 1;
            openFileDialog1.RestoreDirectory = true;

            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                try
                {
                    importar(dtg, 1, openFileDialog1.FileName);
                }
                catch (Exception)
                {
                    MessageBox.Show("No existe el Archivo seleccionado", Application.ProductName + "-" + Application.ProductVersion, MessageBoxButtons.OK);
                }
                finally
                {
                    
                    Application.DoEvents();
                }
            }
        }

        private void importar(DataGridView dtg, int hoja, string archivo)
        {


            string tmpFile = Path.GetTempFileName();
            try
            {
                if (!File.Exists(archivo))
                {
                    MessageBox.Show(this, "El archivo " + archivo + " no existe.", Application.ProductName
                        , MessageBoxButtons.OK, MessageBoxIcon.Error);
                }

                File.Delete(tmpFile);
                File.Copy(archivo, tmpFile);

                dtg.Rows.Clear();
                int rowIndex = 1;

                FileInfo existingFile = new FileInfo(tmpFile);
                using (ExcelPackage package = new ExcelPackage(existingFile))
                {
                    ExcelWorksheet worksheet = package.Workbook.Worksheets[hoja];
                    while (worksheet.Cells[rowIndex, 2].Value != null)
                    {

                        //Agregar las lineas de la cabecera
                        if (rowIndex > 2)
                        {

                            try
                            {
                                int anio = int.Parse(worksheet.Cells[1, 2].Value.ToString());

                                for (int i = 3; i < 15; i++)
                                {

                                    if (true)//(Globales.IsNumeric(worksheet.Cells[rowIndex, i].Text))
                                    {
                                        //double monto = double.Parse(worksheet.Cells[rowIndex, i].Text);
                                        string umVisual = worksheet.Cells[rowIndex, 1].Text;
                                        string um = worksheet.Cells[rowIndex, 2].Text;
                                        string descripcion = worksheet.Cells[rowIndex, 3].Text;

                                        guardar(umVisual, um, descripcion);
                                    }
                                }
                            }
                            catch (DbEntityValidationException e)
                            {
                                foreach (var eve in e.EntityValidationErrors)
                                {
                                    foreach (var ve in eve.ValidationErrors)
                                    {
                                        MessageBox.Show(this, ve.PropertyName + " Error: " + ve.ErrorMessage, Application.ProductName
                                            , MessageBoxButtons.OK, MessageBoxIcon.Error);
                                    }
                                }
                            }
                            finally
                            {
                                File.Delete(tmpFile);
                            }
                        }
                        rowIndex++;

                    }

                }
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message.ToString());
            }
            finally
            {
                File.Delete(tmpFile);
                cargar();
            }

        }

        private void guardar(string um, string umVisual, string descripcion)
        {
            COVEEntities dbContext = new COVEEntities();
            try
            {
                bool modificado = false;
                unidadMedida registro = new unidadMedida();
                registro = (from r in dbContext.unidadMedidaSet.Where
                                            (a => a.umVisual.Equals(umVisual))
                                select r).FirstOrDefault();
                if (registro!=null)
                {
                    modificado = true;
                }
                registro.um = um;
                registro.descripcion = descripcion;
                registro.umVisual = umVisual;

                if (!modificado)
                {
                    dbContext.unidadMedidaSet.Add(registro);
                }
                else
                {
                    dbContext.unidadMedidaSet.Attach(registro);
                    dbContext.Entry(registro).State = System.Data.Entity.EntityState.Modified;
                }

                dbContext.SaveChanges();
            }
            catch (DbEntityValidationException e)
            {
                foreach (var eve in e.EntityValidationErrors)
                {
                    foreach (var ve in eve.ValidationErrors)
                    {
                        MessageBox.Show(this, ve.PropertyName + " Error: " + ve.ErrorMessage, Application.ProductName
                            , MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
            }
        }


    }
}
