﻿namespace Coves
{
    partial class frmCoves
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmCoves));
            this.ucPlantillaCOVE1 = new DocumentosFX.uc.ucPlantillaCOVE();
            this.SuspendLayout();
            // 
            // ucPlantillaCOVE1
            // 
            this.ucPlantillaCOVE1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ucPlantillaCOVE1.Location = new System.Drawing.Point(0, 0);
            this.ucPlantillaCOVE1.Name = "ucPlantillaCOVE1";
            this.ucPlantillaCOVE1.Size = new System.Drawing.Size(948, 538);
            this.ucPlantillaCOVE1.TabIndex = 0;
            // 
            // frmCoves
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(948, 538);
            this.Controls.Add(this.ucPlantillaCOVE1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "frmCoves";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "COVEs";
            this.ResumeLayout(false);

        }

        #endregion

        private DocumentosFX.uc.ucPlantillaCOVE ucPlantillaCOVE1;
    }
}

