﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Coves;
using Generales;
using System.Data.Entity.Validation;

namespace Coves
{
    public partial class frmConfiguracion : Form
    {
        bool modificado = false;
        unidadMedida oRegistro;

        public frmConfiguracion()
        {
            InitializeComponent();
            limpiar();
        }
        public frmConfiguracion(unidadMedida oRegistrop)
        {
            InitializeComponent();
            limpiar();
            oRegistro = oRegistrop;
            cargar();
        }
        private void button1_Click(object sender, EventArgs e)
        {
            limpiar();
        }

        #region Logica
        private void cargar()
        {
            um.Text = oRegistro.um;
            umVisual.Text = oRegistro.umVisual;
            descripcion.Text = oRegistro.descripcion;
        }

        public void limpiar()
        {
            if (modificado)
            {

                DialogResult Resultado = MessageBox.Show(this, "¿Desea guardar los cambios?", Application.ProductName + "-" + Application.ProductVersion, MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                if (Resultado.ToString() == "Yes")
                {
                    guardar();
                }
            }
            oRegistro = new unidadMedida();
            modificado = false;
            foreach (var c in this.Controls)
            {
                if (c is TextBox)
                {
                    ((TextBox)c).Text = "";
                }
            }
        }
        private void guardar()
        {

            COVEEntities dbContext = new COVEEntities();
            try
            {
                unidadMedida registro = new unidadMedida();
                modificado = false;
                if (oRegistro.Id != 0)
                {
                    //Cargar registro
                    int valor = oRegistro.Id;
                    registro = (from r in dbContext.unidadMedidaSet.Where
                                            (a => a.Id == valor)
                                select r).FirstOrDefault();
                    modificado = true;
                }
                registro.um = um.Text;
                registro.descripcion = descripcion.Text;
                registro.umVisual = umVisual.Text;

                if (!modificado)
                {
                    dbContext.unidadMedidaSet.Add(registro);
                }
                else
                {
                    dbContext.unidadMedidaSet.Attach(registro);
                    dbContext.Entry(registro).State = System.Data.Entity.EntityState.Modified;
                }

                dbContext.SaveChanges();
                oRegistro = registro;
                modificado = false;
                MessageBox.Show(this, "Datos guardados", Application.ProductName + "-" + Application.ProductVersion, MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            catch (DbEntityValidationException e)
            {
                foreach (var eve in e.EntityValidationErrors)
                {
                    foreach (var ve in eve.ValidationErrors)
                    {
                        MessageBox.Show(this, ve.PropertyName + " Error: " + ve.ErrorMessage, Application.ProductName
                            , MessageBoxButtons.OK, MessageBoxIcon.Error);
                        return;
                    }
                }
            }
        }

        private void eliminar()
        {

            DialogResult Resultado = MessageBox.Show(this, "¿Esta seguro de eliminar?", Application.ProductName + "-" + Application.ProductVersion, MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            if (Resultado.ToString() != "Yes")
            {
                return;
            }
            COVEEntities dbContext = new COVEEntities();
            dbContext.unidadMedidaSet.Attach(oRegistro);
            dbContext.unidadMedidaSet.Remove(oRegistro);
            dbContext.SaveChanges();
            MessageBox.Show(this, "Eliminado.", Application.ProductName + "-" + Application.ProductVersion, MessageBoxButtons.OK, MessageBoxIcon.Information);
            limpiar();
        }

        #endregion

        private void button2_Click(object sender, EventArgs e)
        {
            guardar();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            eliminar();
        }
    }
}
