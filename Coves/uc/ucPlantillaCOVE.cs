﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Coves;
using Generales;
using System.IO;
using OfficeOpenXml;
using System.Xml.Serialization;
using CFDI32;
using System.Xml;
using System.Collections;

namespace DocumentosFX.uc
{
    public partial class ucPlantillaCOVE : UserControl
    {
        public ucPlantillaCOVE()
        {
            InitializeComponent();
        }
        public string convertir_fecha(DateTime pfecha, string como = "n")
        {
            string fecha = "";
            //SQLServer
            fecha = "CONVERT(DATETIME,'" + pfecha.Year.ToString() + "-" + pfecha.Month.ToString() + "-" + pfecha.Day.ToString() + " " + pfecha.Hour.ToString() + ":" + pfecha.Minute.ToString() + ":" + pfecha.Second.ToString() + "',102)";
            if (como == "i")
            {
                fecha = "CONVERT(DATETIME,'" + pfecha.Year.ToString() + "-" + pfecha.Month.ToString() + "-" + pfecha.Day.ToString() + " 00:00:00',102)";
            }
            if (como == "f")
            {
                fecha = "CONVERT(DATETIME,'" + pfecha.Year.ToString() + "-" + pfecha.Month.ToString() + "-" + pfecha.Day.ToString() + " 23:59:59',102)";
            }
            return fecha;
        }
        private void ucPlantillaCOVE_Load(object sender, EventArgs e)
        {

        }

        private void ucPlantillaCOVE_Resize(object sender, EventArgs e)
        {
            this.tabControl1.Height = this.Height - this.groupBox1.Height-5;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            cargarFacturas();
        }
        private void cargarFacturas()
        {
            try
            {
                string fechaInicial = convertir_fecha(this.inicio.Value);
                string fechaFinal = convertir_fecha(this.fin.Value);

                //id* Codigo *Descripcion    UMC* Descripcion UMC Fraccion    TIPO
                VisualEntities dbContext = new VisualEntities();
                string conection = dbContext.Database.Connection.ConnectionString;
                cCONEXCION oData = new cCONEXCION(conection);

                string sSQL = @"
                SELECT DISTINCT 
                R.INVOICE_ID as 'Factura', feFX.XML as 'XML'
                FROM RECEIVABLE AS R
                INNER JOIN VMX_FE as feFX ON feFX.INVOICE_ID=R.INVOICE_ID
                WHERE 
                R.CREATE_DATE >= " + fechaInicial + @"
                AND R.CREATE_DATE <= " + fechaFinal + @"
                ";

                DataTable oDataTable = oData.EjecutarConsulta(sSQL);
                this.dtgFactura.DataSource = oDataTable;
                this.dtgFactura.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.AllCells;
            }
            catch (Exception error)
            {
                string mensaje = error.Message.ToString();
                if (error.InnerException != null)
                {
                    mensaje += Environment.NewLine + error.InnerException.ToString();
                }
                MessageBox.Show(this, mensaje, Application.ProductName
                            , MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }


        private DataTable cargarProducto(string INVOICE_ID="")
        {
            try
            {
                string fechaInicial = convertir_fecha(this.inicio.Value);
                string fechaFinal = convertir_fecha(this.fin.Value);

                //id* Codigo *Descripcion    UMC* Descripcion UMC Fraccion    TIPO
                VisualEntities dbContext = new VisualEntities();
                string conection = dbContext.Database.Connection.ConnectionString;
                cCONEXCION oData = new cCONEXCION(conection);

                string sSQL = @"
                SELECT '' as ID
                , COL.PART_ID as 'Codigo'
                , RL.REFERENCE as 'Descripcion'
                , ISNULL((select um.um FROM [documentosfx].[dbo].[unidadMedidaSet] um WHERE um.umVisual=ISNULL(COL.SELLING_UM,P.STOCK_UM)),'NO EXISTE ' + ISNULL(COL.SELLING_UM,P.STOCK_UM)) as 'UMC'
                , ISNULL((select um.descripcion FROM [documentosfx].[dbo].[unidadMedidaSet] um WHERE um.umVisual=ISNULL(COL.SELLING_UM,P.STOCK_UM)),'NO EXISTE ' + ISNULL(COL.SELLING_UM,P.STOCK_UM))  as 'Descripcion UMC'
                , ISNULL((SELECT UDF.STRING_VAL FROM USER_DEF_FIELDS UDF WHERE UDF.PROGRAM_ID='VMPRTMNT' AND UDF.ID='UDF-0000100' AND dOCUMENT_ID=COL.PART_ID),'') as 'Fraccion', 'EXPORTACION' as 'Tipo'
                FROM RECEIVABLE_LINE AS RL 
                INNER JOIN CUST_ORDER_LINE AS COL ON RL.CUST_ORDER_ID = COL.CUST_ORDER_ID AND RL.CUST_ORDER_LINE_NO = COL.LINE_NO 
                INNER JOIN RECEIVABLE AS R ON RL.INVOICE_ID = R.INVOICE_ID
                INNER JOIN PART AS P ON P.ID=COL.PART_ID
                WHERE 
                1=1
                ";

                if (INVOICE_ID!="")
                {
                    sSQL += " AND R.INVOICE_ID = '" + INVOICE_ID + @"' ";
                }
                else
                {
                    
                    string filtroFacturas = @"
                        (SELECT DISTINCT 
                        R.CUSTOMER_ID
                        FROM RECEIVABLE AS R
                        INNER JOIN VMX_FE as feFX ON feFX.INVOICE_ID=R.INVOICE_ID
                        WHERE 
                        R.CREATE_DATE >= " + fechaInicial + @"
                        AND R.CREATE_DATE <= " + fechaFinal + @")";

                    sSQL += " AND R.CUSTOMER_ID IN (" + filtroFacturas  + ")";
                }

                DataTable oDataTable = oData.EjecutarConsulta(sSQL);
                return oDataTable;

            }
            catch (Exception error)
            {
                string mensaje = error.Message.ToString();
                if (error.InnerException!=null)
                {
                    mensaje+= Environment.NewLine+ error.InnerException.ToString();
                }
                MessageBox.Show(this, mensaje, Application.ProductName
                            , MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            return null;

        }

        private void button7_Click(object sender, EventArgs e)
        {
            //plantilla();
            //cargarProducto();
            //cargarEmisor();
            //cargarDestinatario();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            frmConfiguraciones o = new frmConfiguraciones();
            o.ShowDialog();
        }

        private void dtgFactura_MouseClick(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Right)
            {
                contextMenuStrip1.Show(dtgFactura, new Point(e.X, e.Y));
            }
        }

        private void formatoPorClienteToolStripMenuItem_Click(object sender, EventArgs e)
        {
            exportarANA();
        }

        private void exportarANA()
        {
            try
            {

                if (dtgFactura.SelectedRows.Count > 0)
                {
                    //Selecionar la carpeta para migrarlo
                    FolderBrowserDialog folderBrowserDialog1 = new FolderBrowserDialog();
                    if (folderBrowserDialog1.ShowDialog() == DialogResult.OK)
                    {
                        string fileName = folderBrowserDialog1.SelectedPath;
                        foreach (DataGridViewRow drv in dtgFactura.SelectedRows)
                        {
                            string invoice_id = drv.Cells[0].Value.ToString();
                            string archivo = drv.Cells[1].Value.ToString();
                            string directorio = fileName + "\\" + invoice_id;

                            if (!Directory.Exists(directorio))
                            {
                                Directory.CreateDirectory(directorio);
                            }
                            //Copiar el archivo XML alli
                            if (File.Exists(directorio + "\\" + invoice_id + ".XML"))
                            {
                                File.Delete(directorio + "\\" + invoice_id + ".XML");
                            }
                            File.Copy(archivo, directorio + "\\" + invoice_id + ".XML");
                            //Generar el archivo de XML
                            DataTable oDataProducto = new DataTable();
                            oDataProducto = cargarProducto(invoice_id);
                            oDataProducto.TableName = "PRODUCTOS";

                            DataTable oDataDestinatario = new DataTable();
                            oDataDestinatario = cargarDestinatario(archivo);
                            oDataDestinatario.TableName = "DESTINATARIO";

                            DataTable oDataEmisor = new DataTable();
                            oDataEmisor = cargarEmisor(archivo);
                            oDataEmisor.TableName = "EMISOR";

                            string archivoExcel = fileName + "\\" + invoice_id +"\\"+ invoice_id+".xlsx";
                            System.IO.Stream myStream = new System.IO.FileStream(archivoExcel, System.IO.FileMode.Create, System.IO.FileAccess.ReadWrite, System.IO.FileShare.None);
                            using (ExcelPackage pck = new ExcelPackage())
                            {
                                ExcelWorksheet ws1;
                                ws1 = pck.Workbook.Worksheets.Add("CATALOGO PRODUCTOS");
                                ws1.Cells["A1"].LoadFromDataTable(oDataProducto, true, OfficeOpenXml.Table.TableStyles.Light13);
                                ExcelWorksheet ws2;
                                ws2 = pck.Workbook.Worksheets.Add("CATALOGO EMISOR");
                                ws2.Cells["A1"].LoadFromDataTable(oDataEmisor, true, OfficeOpenXml.Table.TableStyles.Light13);
                                
                                ExcelWorksheet ws3;
                                ws3 = pck.Workbook.Worksheets.Add("CATALOGO DESTINATARIO");
                                ws3.Cells["A1"].LoadFromDataTable(oDataDestinatario, true, OfficeOpenXml.Table.TableStyles.Light13);
                                pck.SaveAs(myStream);
                            }



                            myStream.Close();
                        }
                        MessageBox.Show(this, "Archivos de exportación migrados.", Application.ProductName
                           , MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                }
            }
            catch (Exception error)
            {
                string mensaje = error.Message.ToString();
                if (error.InnerException != null)
                {
                    mensaje += Environment.NewLine + error.InnerException.ToString();
                }
                MessageBox.Show(this, mensaje, Application.ProductName
                            , MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private DataTable cargarEmisor(string archivo)
        {
            //Añadir la addenda
            XmlSerializer serializer_CFD_3 = new XmlSerializer(typeof(Comprobante));
            TextReader reader = new StreamReader(archivo);
            Comprobante oComprobante;
            plantillaSocio.socioDataTable osocioDataTable = new plantillaSocio.socioDataTable();
            try
            {
                oComprobante = (Comprobante)serializer_CFD_3.Deserialize(reader);

                osocioDataTable.AddsocioRow(
                    ""
                    , "1"
                    , "RFC"
                    , "TLA010227C50"
                    , "TIGHITCO LATINOAMERICA S.A.DE C.V."
                    , ""
                    , ""
                    , "AV.COMISION FEDERAL DE ELECTRICIDAD"
                    , "635"
                    , "05"
                    , "028"
                    , "SLP"
                    , "024"
                    , "2270"
                    , "78395"
                    , "MEX"
                    );
                //osocioDataTable.AddsocioRow(
                //    ""
                //    , "1"
                //    , "RFC"
                //    , oComprobante.Emisor.rfc
                //    , oComprobante.Emisor.nombre
                //    , ""
                //    , ""
                //    , oComprobante.Emisor.DomicilioFiscal.calle
                //    , oComprobante.Emisor.DomicilioFiscal.noExterior
                //    , oComprobante.Emisor.DomicilioFiscal.noInterior
                //    , oComprobante.Emisor.DomicilioFiscal.localidad
                //    , oComprobante.Emisor.DomicilioFiscal.municipio
                //    , oComprobante.Emisor.DomicilioFiscal.referencia
                //    , oComprobante.Emisor.DomicilioFiscal.colonia
                //    , oComprobante.Emisor.DomicilioFiscal.codigoPostal
                //    , oComprobante.Emisor.DomicilioFiscal.pais
                //    );
            }
            catch (XmlException error)
            {
                string mensaje = error.Message.ToString();
                if (error.InnerException != null)
                {
                    mensaje += Environment.NewLine + error.InnerException.ToString();
                }
                MessageBox.Show(this, mensaje, Application.ProductName
                            , MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            reader.Dispose();
            reader.Close();
            return osocioDataTable;
        }

        private DataTable cargarDestinatario(string archivo)
        {
            //Añadir la addenda
            XmlSerializer serializer_CFD_3 = new XmlSerializer(typeof(Comprobante));
            TextReader reader = new StreamReader(archivo);
            Comprobante oComprobante;
            plantillaSocio.socioDataTable osocioDataTable = new plantillaSocio.socioDataTable();
            try
            {
                oComprobante = (Comprobante)serializer_CFD_3.Deserialize(reader);

                osocioDataTable.AddsocioRow(
                    ""
                    , ""
                    , "RFC"
                    , oComprobante.Receptor.rfc
                    , oComprobante.Receptor.nombre
                    , ""
                    , ""
                    , oComprobante.Receptor.Domicilio.calle
                    , (oComprobante.Receptor.Domicilio.noExterior == null) ? "" : oComprobante.Receptor.Domicilio.noExterior
                    , (oComprobante.Receptor.Domicilio.noInterior == null) ? "" : oComprobante.Receptor.Domicilio.noInterior
                    , (oComprobante.Receptor.Domicilio.localidad == null) ? "" : oComprobante.Receptor.Domicilio.localidad
                    , (oComprobante.Receptor.Domicilio.municipio == null) ? "" : oComprobante.Receptor.Domicilio.municipio
                    , (oComprobante.Receptor.Domicilio.referencia == null) ? "" : oComprobante.Receptor.Domicilio.referencia
                    , (oComprobante.Receptor.Domicilio.colonia == null) ? "" : oComprobante.Receptor.Domicilio.colonia
                    , (oComprobante.Receptor.Domicilio.codigoPostal == null) ? "" : oComprobante.Receptor.Domicilio.codigoPostal
                    , (oComprobante.Receptor.Domicilio.pais == null) ? "" : oComprobante.Receptor.Domicilio.pais
                    );
            }
            catch (XmlException error)
            {
                string mensaje = error.Message.ToString();
                if (error.InnerException != null)
                {
                    mensaje += Environment.NewLine + error.InnerException.ToString();
                }
                MessageBox.Show(this, mensaje, Application.ProductName
                            , MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            reader.Dispose();
            reader.Close();
            return osocioDataTable;
        }

        private void button3_Click(object sender, EventArgs e)
        {
            procesarTodos();
        }

        private void procesarTodos()
        {
            try
            {
                //Selecionar la carpeta para migrarlo
                FolderBrowserDialog folderBrowserDialog1 = new FolderBrowserDialog();
                if (folderBrowserDialog1.ShowDialog() == DialogResult.OK)
                {
                    string fileName = folderBrowserDialog1.SelectedPath;
                    string archivoExcel = fileName + "\\Plantilla.xlsx";
                    DataTable oDataProducto = new DataTable();
                    oDataProducto = cargarProducto();
                    oDataProducto.TableName = "PRODUCTOS";
                    oDataProducto = RemoveDuplicateRows(oDataProducto, "Codigo");

                    DataTable oDataDestinatario = new DataTable();
                    oDataDestinatario = cargarDestinatario();
                    oDataDestinatario.TableName = "DESTINATARIO";

                    DataTable oDataEmisor = new DataTable();
                    oDataEmisor = cargarEmisor();
                    oDataEmisor.TableName = "EMISOR";
                    System.IO.Stream myStream = new System.IO.FileStream(archivoExcel, System.IO.FileMode.Create, System.IO.FileAccess.ReadWrite, System.IO.FileShare.None);
                    using (ExcelPackage pck = new ExcelPackage())
                    {
                        ExcelWorksheet ws1;
                        ws1 = pck.Workbook.Worksheets.Add("CATALOGO PRODUCTOS");
                        ws1.Cells["A1"].LoadFromDataTable(oDataProducto, true, OfficeOpenXml.Table.TableStyles.Light13);
                        ExcelWorksheet ws2;
                        ws2 = pck.Workbook.Worksheets.Add("CATALOGO EMISOR");
                        ws2.Cells["A1"].LoadFromDataTable(oDataEmisor, true, OfficeOpenXml.Table.TableStyles.Light13);

                        ExcelWorksheet ws3;
                        ws3 = pck.Workbook.Worksheets.Add("CATALOGO DESTINATARIO");
                        ws3.Cells["A1"].LoadFromDataTable(oDataDestinatario, true, OfficeOpenXml.Table.TableStyles.Light13);
                        pck.SaveAs(myStream);
                    }



                    myStream.Close();
                    MessageBox.Show(this, "Archivos de exportación migrados.", Application.ProductName
                        , MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
            catch (Exception error)
            {
                string mensaje = error.Message.ToString();
                if (error.InnerException != null)
                {
                    mensaje += Environment.NewLine + error.InnerException.ToString();
                }
                MessageBox.Show(this, mensaje, Application.ProductName
                            , MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public DataTable RemoveDuplicateRows(DataTable dTable, string colName)
        {
            Hashtable hTable = new Hashtable();
            ArrayList duplicateList = new ArrayList();

            //Add list of all the unique item value to hashtable, which stores combination of key, value pair.
            //And add duplicate item value in arraylist.
            foreach (DataRow drow in dTable.Rows)
            {
                if (hTable.Contains(drow[colName]))
                    duplicateList.Add(drow);
                else
                    hTable.Add(drow[colName], string.Empty);
            }

            //Removing a list of duplicate items from datatable.
            foreach (DataRow dRow in duplicateList)
                dTable.Rows.Remove(dRow);

            //Datatable which contains unique records will be return as output.
            return dTable;
        }
        private DataTable cargarEmisor()
        {
            plantillaSocio.socioDataTable osocioDataTable = new plantillaSocio.socioDataTable();
            try
            {
                osocioDataTable.AddsocioRow(
                    ""
                    , "1"
                    , "RFC"
                    , "TLA010227C50"
                    , "TIGHITCO LATINOAMERICA S.A.DE C.V."
                    , ""
                    , ""
                    , "AV.COMISION FEDERAL DE ELECTRICIDAD"
                    , "635"
                    , "05"
                    , "028"
                    , "SLP"
                    , "024"
                    , "2270"
                    , "78395"
                    , "MEX"
                    );              


            }
            catch (Exception error)
            {
                string mensaje = error.Message.ToString();
                if (error.InnerException != null)
                {
                    mensaje += Environment.NewLine + error.InnerException.ToString();
                }
                MessageBox.Show(this, mensaje, Application.ProductName
                            , MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            return osocioDataTable;
        }

        private DataTable cargarDestinatario()
        {
            string sSQL = @"
            SELECT 
            '' as ID
            ,ID as Clave
            ,
            CASE 
              WHEN TAX_ID_NUMBER IS null THEN 1
              ELSE 0
            END  as 'Tipo Identificador'
            ,ISNULL(TAX_ID_NUMBER,VAT_REGISTRATION) as 'Identificador'
            ,NAME as 'Nombre'
            ,'' as 'Apellido Paterno'
            ,'' as 'Apellido Materno'
            , BILL_TO_ADDR_1 as 'Calle'
            ,'' as 'Número Ext'
            ,'' as 'Número Int'
            ,'' as 'Localidad'
            ,BILL_TO_STATE as 'Municipio'
            ,'' as 'Entidad Federativa'
            ,BILL_TO_ADDR_2 as 'Colonia'
            ,BILL_TO_ZIPCODE as 'C.P.'
            ,BILL_TO_COUNTRY as 'País'
            FROM CUSTOMER
            ";
            VisualEntities dbContext = new VisualEntities();
            string conection = dbContext.Database.Connection.ConnectionString;
            cCONEXCION oData = new cCONEXCION(conection);
            DataTable oDataTable = oData.EjecutarConsulta(sSQL);
            return oDataTable;
        }
    }
}
