
-- --------------------------------------------------
-- Entity Designer DDL Script for SQL Server 2005, 2008, 2012 and Azure
-- --------------------------------------------------
-- Date Created: 12/03/2020 16:09:45
-- Generated from EDMX file: D:\Proyectos\DocumentosFX\SAT\ImportarCatalogos\ModeloEFOEDO\ModeloEFOEDO.edmx
-- --------------------------------------------------

SET QUOTED_IDENTIFIER OFF;
GO
USE [EFOEDO];
GO
IF SCHEMA_ID(N'dbo') IS NULL EXECUTE(N'CREATE SCHEMA [dbo]');
GO

-- --------------------------------------------------
-- Dropping existing FOREIGN KEY constraints
-- --------------------------------------------------


-- --------------------------------------------------
-- Dropping existing tables
-- --------------------------------------------------


-- --------------------------------------------------
-- Creating all tables
-- --------------------------------------------------

-- Creating table 'sincronizacionEFOEDOSet'
CREATE TABLE [dbo].[sincronizacionEFOEDOSet] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [fechaCreacion] datetime  NOT NULL,
    [NombreContribuyente] nvarchar(max)  NOT NULL,
    [Situacioncontribuyente] nvarchar(max)  NULL,
    [Numerofechaoficioglobalpresuncion] nvarchar(max)  NULL,
    [PublicacionDOFpresuntos] nvarchar(max)  NULL,
    [PublicacionpaginaSATdesvirtuados] nvarchar(max)  NULL,
    [Numerofechaoficioglobalcontribuyentesdesvirtuaron] nvarchar(max)  NULL,
    [PublicacionDOFdesvirtuados] nvarchar(max)  NULL,
    [Numerofechaoficioglobaldefinitivos] nvarchar(max)  NULL,
    [PublicacionpaginaSATdefinitivos] nvarchar(max)  NOT NULL,
    [PublicacionDOFdefinitivos] nvarchar(max)  NULL,
    [Numerofechaoficioglobalsentenciaavorable] nvarchar(max)  NULL,
    [PublicacionpaginaSATsentenciafavorable] nvarchar(max)  NULL,
    [Numerofechaoficioglobalsentenciafavorable] nvarchar(max)  NULL,
    [PublicacionDOFsentenciafavorable] nvarchar(max)  NULL
);
GO

-- Creating table 'configuracionRFCSet'
CREATE TABLE [dbo].[configuracionRFCSet] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [rfc] nvarchar(max)  NOT NULL,
    [cert] nvarchar(max)  NOT NULL,
    [key] nvarchar(max)  NOT NULL,
    [password] nvarchar(max)  NOT NULL
);
GO

-- --------------------------------------------------
-- Creating all PRIMARY KEY constraints
-- --------------------------------------------------

-- Creating primary key on [Id] in table 'sincronizacionEFOEDOSet'
ALTER TABLE [dbo].[sincronizacionEFOEDOSet]
ADD CONSTRAINT [PK_sincronizacionEFOEDOSet]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'configuracionRFCSet'
ALTER TABLE [dbo].[configuracionRFCSet]
ADD CONSTRAINT [PK_configuracionRFCSet]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- --------------------------------------------------
-- Creating all FOREIGN KEY constraints
-- --------------------------------------------------

-- --------------------------------------------------
-- Script has ended
-- --------------------------------------------------