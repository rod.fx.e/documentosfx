#region Copyright Syncfusion Inc. 2001 - 2017
// Copyright Syncfusion Inc. 2001 - 2017. All rights reserved.
// Use of this code is subject to the terms of our license.
// A copy of the current license can be obtained at any time by e-mailing
// licensing@syncfusion.com. Any infringement will be prosecuted under
// applicable laws. 
#endregion
using Generales;
using ModeloDocumentosFX;
using OfficeOpenXml;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.Entity.Validation;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Windows.Forms;

namespace ClasificadorProductos.formularios
{
    public partial class frmFacturasLineas : Form//Syncfusion.Windows.Forms.MetroForm
    {
        public DataGridViewSelectedRowCollection selecionados;
        public string CUSTOMER_IDtr;
        public decimal monto;
        string facturas="";
        string facturaOrigen = "";
        string tipoRelaciontr = "";

        public frmFacturasLineas(cCONEXCION oDatap, string facturasp)
        {
            this.Text = this.Text + " " + Application.ProductVersion.ToString();

            facturas = facturasp;
            oData = oDatap;
            InitializeComponent();
            //cargar();
        }


        public frmFacturasLineas(cCONEXCION oDatap, string CUSTOMER_IDp, decimal montop, string facturaOrigenp, string tipoRelaciontrp)
        {
            this.Text = this.Text + " " + Application.ProductVersion.ToString();

            tipoRelaciontr = tipoRelaciontrp;
            facturaOrigen = facturaOrigenp;
            monto = montop;
            CUSTOMER_IDtr = CUSTOMER_IDp;
            oData = oDatap;
            InitializeComponent();
            //cargar();
        }

        cCONEXCION oData;
        private void button1_Click(object sender, EventArgs e)
        {
            cargar();
        }

        private void cargar()
        {
            try
            {
                dtg.Rows.Clear();
                facturas = buscar.Text.Trim();
                if (!facturas.Contains("%")) {
                    facturas = Regex.Replace(facturas, "[^0-9.]", "");
                    if (String.IsNullOrEmpty(facturas))
                    {
                        return;
                    }
                    facturaOrigen = Regex.Replace(facturaOrigen, "[^0-9.]", "");
                    //Validar que sea la nota de crediito cuando sea 2 
                    if (!tipoRelaciontr.Equals("2"))
                    {
                        //if (int.Parse(facturaOrigen) < int.Parse(facturas))
                        //{
                        //    ErrorFX.mostrar("La factura " + buscar.Text.Trim() + " es posterior a la factura " + facturaOrigen, true, false);
                        //    return;
                        //}
                    }

                }

                string sSQL = @"

                SELECT TOP 5 rl.INVOICE_ID, rl.LINE_NO, col.PART_ID, rl.AMOUNT, p.PRODUCT_CODE
                FROM RECEIVABLE_LINE AS rl 
                LEFT OUTER JOIN CUST_ORDER_LINE AS col ON rl.CUST_ORDER_ID = col.CUST_ORDER_ID AND rl.CUST_ORDER_LINE_NO = col.LINE_NO
                LEFT OUTER JOIN PART as p ON p.ID=col.PART_ID
                WHERE rl.INVOICE_ID like '" + facturas + @"' ";
                sSQL+= @"
                ORDER BY rl.INVOICE_ID, rl.LINE_NO
                ";

                //Validar la opción 2 

                if (CUSTOMER_IDtr != "")
                {
                    sSQL = @"
                    SELECT TOP 10 rl.INVOICE_ID, rl.LINE_NO, col.PART_ID, rl.AMOUNT, 
                    ISNULL(col.PART_ID, rl.GL_ACCOUNT_ID) as PART_ID, r.INVOICE_DATE, v.UUID, p.PRODUCT_CODE
                    FROM RECEIVABLE_LINE AS rl 
                    INNER JOIN RECEIVABLE r ON r.INVOICE_ID=rl.INVOICE_ID
                    INNER JOIN VMX_FE v ON v.INVOICE_ID=rl.INVOICE_ID
                    LEFT OUTER JOIN CUST_ORDER_LINE AS col ON rl.CUST_ORDER_ID = col.CUST_ORDER_ID AND rl.CUST_ORDER_LINE_NO = col.LINE_NO
                    LEFT OUTER JOIN PART as p ON p.ID=col.PART_ID
                    WHERE 
                    r.CUSTOMER_ID IN ('" + CUSTOMER_IDtr + @"')
                    AND rl.INVOICE_ID like '" + buscar.Text.Trim() + @"' 
                    AND  NOT(v.UUID IS NULL)
                    AND ISNULL((SELECT TOP 1 r1.INVOICE_DATE FROM RECEIVABLE r1 WHERE r1.INVOICE_ID='" + facturaOrigen+ @"'),GETDATE())>=r.INVOICE_DATE
                    AND ABS(rl.AMOUNT)>=" + monto.ToString()  + @"
                    AND r.TYPE='I'
                    ";
                    sSQL += @"
                    ORDER BY rl.INVOICE_ID DESC, rl.LINE_NO
                    ";
                }
                DataTable oDataTable = oData.EjecutarConsulta(sSQL);
                if (oDataTable != null)
                {
                    if (oDataTable.Rows.Count == 0)
                    {
                        ErrorFX.mostrar("Esta factura no cumple los siguientes requisitos: "
                            + Environment.NewLine + " El monto es menor a " + monto.ToString()
                            + Environment.NewLine + " El cliente debe ser es " + CUSTOMER_IDtr
                            + Environment.NewLine + " Verifique los datos de la factura"
                            , true, false, true);
                        return;
                    }
                    foreach (DataRow registro in oDataTable.Rows)
                    {
                        //Cargar el folio fiscal
                        //string uuidtr = buscarUUID(registro["INVOICE_ID"].ToString());
                        //if (!String.IsNullOrEmpty(uuidtr))
                        //{
                            int n = dtg.Rows.Add();
                            dtg.Rows[n].Cells["INVOICE_ID"].Value = registro["INVOICE_ID"].ToString();
                            dtg.Rows[n].Cells["LINE_NO"].Value = registro["LINE_NO"].ToString();
                            dtg.Rows[n].Cells["PART_ID"].Value = registro["PART_ID"].ToString();
                            dtg.Rows[n].Cells["AMOUNT"].Value = registro["AMOUNT"].ToString();
                            dtg.Rows[n].Cells["UUID"].Value = registro["UUID"].ToString();
                            dtg.Rows[n].Cells["PRODUCT_CODE"].Value = registro["PRODUCT_CODE"].ToString();
                        //}
                    }

                }
            }
            catch (DbEntityValidationException error)
            {
                foreach (var eve in error.EntityValidationErrors)
                {
                    foreach (var ve in eve.ValidationErrors)
                    {
                        MessageBox.Show(this, ve.PropertyName + " Error: " + ve.ErrorMessage, Application.ProductName
                            , MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
            }
        }

        private string buscarUUID(string invoice)
        {
            try
            {
                string resultado = "";
                ModeloDocumentosFX.ModeloDocumentosFXContainer dbContext = new ModeloDocumentosFXContainer();
                VMX_FE registrotr = (from b in dbContext.VMX_FE
                                             .Where(a => a.INVOICE_ID.Equals(invoice))
                             select b).FirstOrDefault();


                if (registrotr != null)
                {
                    resultado = registrotr.UUID;
                }
                dbContext.Dispose();
                if (resultado!="")
                {
                    return resultado;
                }
            }
            catch (DbEntityValidationException error)
            {
                foreach (var eve in error.EntityValidationErrors)
                {
                    foreach (var ve in eve.ValidationErrors)
                    {
                        MessageBox.Show(this, ve.PropertyName + " Error: " + ve.ErrorMessage, Application.ProductName
                            , MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
            }
            return null;
        }

        private void importar()
        {
            Application.DoEvents();
            OpenFileDialog openFileDialog1 = new OpenFileDialog();
            openFileDialog1.Filter = "Excel files (*.xlsx)|*.xlsx|All files (*.*)|*.*";
            openFileDialog1.FilterIndex = 1;
            openFileDialog1.RestoreDirectory = true;

            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                try
                {
                    procesar(dtg, 1, openFileDialog1.FileName);
                }
                catch (Exception)
                {
                    MessageBox.Show("No existe el Archivo seleccionado", Application.ProductName + "-" + Application.ProductVersion, MessageBoxButtons.OK);
                }
                finally
                {
                    Application.DoEvents();
                }
            }
        }

        private void procesar(DataGridView dtg, int hoja, string archivo)
        {
            string tmpFile = Path.GetTempFileName();
            try
            {
                if (!File.Exists(archivo))
                {
                    MessageBox.Show(this, "El archivo " + archivo + " no existe.", Application.ProductName
                        , MessageBoxButtons.OK, MessageBoxIcon.Error);
                }

                File.Delete(tmpFile);
                File.Copy(archivo, tmpFile);

                dtg.Rows.Clear();
                int rowIndex = 2;
                FileInfo existingFile = new FileInfo(tmpFile);
                using (ExcelPackage package = new ExcelPackage(existingFile))
                {
                    ExcelWorksheet worksheet = package.Workbook.Worksheets[hoja];
                    while (worksheet.Cells[rowIndex, 2].Value != null)
                    {
                        try
                        {
                            string productotr = worksheet.Cells[rowIndex, 1].Text;
                            string clasificacion = worksheet.Cells[rowIndex, 2].Text;

                            guardar(productotr, clasificacion);
                        }
                        catch (DbEntityValidationException e)
                        {
                            foreach (var eve in e.EntityValidationErrors)
                            {
                                foreach (var ve in eve.ValidationErrors)
                                {
                                    MessageBox.Show(this, ve.PropertyName + " Error: " + ve.ErrorMessage, Application.ProductName
                                        , MessageBoxButtons.OK, MessageBoxIcon.Error);
                                }
                            }
                        }
                        rowIndex++;
                    }
                }
            }
            catch (Exception e)
            {
                ErrorFX.mostrar(e, true, false,
                "frmFacturasLineas - 150 - "
                );
            }
            finally
            {
                File.Delete(tmpFile);
                cargar();
            }

        }

        private void guardar(string productotr, string clasificacion)
        {
            ModeloDocumentosFX.ModeloDocumentosFXContainer dbContext = new ModeloDocumentosFXContainer();
            try
            {
                configuracionProducto registro = new configuracionProducto();

                registro = (from r in dbContext.configuracionProductoSet.Where
                                        (a => a.producto == productotr
                                        & a.clasificacionSAT == clasificacion)
                            select r).FirstOrDefault();

                if (registro == null)
                {
                    registro = new configuracionProducto();
                    registro.producto = productotr;
                    registro.clasificacionSAT = clasificacion;
                    registro.fechaCreacion = DateTime.Now;
                    dbContext.configuracionProductoSet.Add(registro);
                    dbContext.SaveChanges();
                }
                //else
                //{
                //    dbContext.configuracionProductoSet.Attach(registro);
                //    dbContext.Entry(registro).State = System.Data.Entity.EntityState.Modified;
                //}

                
            }
            catch (DbEntityValidationException e)
            {
                foreach (var eve in e.EntityValidationErrors)
                {
                    foreach (var ve in eve.ValidationErrors)
                    {
                        MessageBox.Show(this, ve.PropertyName + " Error: " + ve.ErrorMessage, Application.ProductName
                            , MessageBoxButtons.OK, MessageBoxIcon.Error);
                        return;
                    }
                }
            }
        }

        private void dtg_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            cargar_registro(e);
        }

        private void cargar_registro(DataGridViewCellEventArgs e)
        {
            if (e.RowIndex > -1)
            {
                configuracionProducto registro = (configuracionProducto)dtg.Rows[e.RowIndex].Tag;
                frmProductoClasificacion o = new frmProductoClasificacion(registro);
                o.ShowDialog();
                cargar();
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            importar();
        }

        private void button7_Click(object sender, EventArgs e)
        {
            plantillaProductCode();
        }

        private void plantillaProductCode()
        {
            try
            {
                MessageBox.Show("La configuración de  la Plantilla para migrar Clasificación de productos es la siguiente:" + Environment.NewLine
                + "Fila 1 - Encabezado" + Environment.NewLine
                + "Columna 1: Producto " + Environment.NewLine
                + "Columna 1: Clasificación " + Environment.NewLine
                + "Si no tiene esta configuración no será cargada la información. "
                , Application.ProductName + "-" + Application.ProductVersion.ToString()
                , MessageBoxButtons.OK
                );

                var fileName = " Plantilla de Clasificación de Productos " + DateTime.Now.ToString("yyyy-MM-dd-hh-mm-ss") + ".xlsx";
                if (folderBrowserDialog1.ShowDialog() == DialogResult.OK)
                {
                    fileName = folderBrowserDialog1.SelectedPath + "\\" + fileName;
                    System.IO.Stream myStream = new System.IO.FileStream(fileName, System.IO.FileMode.Create, System.IO.FileAccess.ReadWrite, System.IO.FileShare.None);

                    using (ExcelPackage pck = new ExcelPackage())
                    {

                        ExcelWorksheet ws1 = pck.Workbook.Worksheets.Add("Plantilla");
                        ws1.Cells[1, 1].Value = "Producto";
                        ws1.Cells[1, 2].Value = "Producto";
                        pck.SaveAs(myStream);
                    }
                    myStream.Close();
                    MessageBox.Show("Creación de plantilla realizada " + Environment.NewLine + fileName + " finalizada.", Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
            catch(Exception e)
            {
                ErrorFX.mostrar(e,true,false,
                "frmFacturasLineas - 264 - "
                    );
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            frmProductoClasificacion o = new frmProductoClasificacion();
            o.Show();
        }

        private void dtg_CellDoubleClick_1(object sender, DataGridViewCellEventArgs e)
        {
            if (dtg.SelectedRows.Count > 0)
            {
                DataGridViewSelectedRowCollection arrSelectedRows = dtg.SelectedRows;
                selecionados = arrSelectedRows;
                this.DialogResult = DialogResult.OK;
            }
        }

        private void button2_Click_1(object sender, EventArgs e)
        {
            if (dtg.SelectedRows.Count > 0)
            {
                DataGridViewSelectedRowCollection arrSelectedRows = dtg.SelectedRows;
                selecionados = arrSelectedRows;
                this.DialogResult = DialogResult.OK;
            }
        }
    }
}
