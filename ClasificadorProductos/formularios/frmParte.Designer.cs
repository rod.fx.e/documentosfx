#region Copyright Syncfusion Inc. 2001 - 2017
// Copyright Syncfusion Inc. 2001 - 2017. All rights reserved.
// Use of this code is subject to the terms of our license.
// A copy of the current license can be obtained at any time by e-mailing
// licensing@syncfusion.com. Any infringement will be prosecuted under
// applicable laws. 
#endregion
namespace ClasificadorProductos.formularios
{
    partial class frmParte
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmParte));
            this.label5 = new System.Windows.Forms.Label();
            this.producto = new System.Windows.Forms.TextBox();
            this.btnDeleteItem = new Syncfusion.Windows.Forms.ButtonAdv();
            this.buttonAdv1 = new Syncfusion.Windows.Forms.ButtonAdv();
            this.btnAddItem = new Syncfusion.Windows.Forms.ButtonAdv();
            this.ClaveUnidad = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.parte = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.NoIdentificacion = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.Cantidad = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.ValorUnitario = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.Importe = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.ClaveProdServ = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.Unidad = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.Descripcion = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(19, 51);
            this.label5.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(92, 13);
            this.label5.TabIndex = 3;
            this.label5.Text = "Producto principal";
            // 
            // producto
            // 
            this.producto.Location = new System.Drawing.Point(115, 48);
            this.producto.Margin = new System.Windows.Forms.Padding(2);
            this.producto.Name = "producto";
            this.producto.Size = new System.Drawing.Size(195, 20);
            this.producto.TabIndex = 4;
            // 
            // btnDeleteItem
            // 
            this.btnDeleteItem.Appearance = Syncfusion.Windows.Forms.ButtonAppearance.Metro;
            this.btnDeleteItem.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(219)))), ((int)(((byte)(94)))), ((int)(((byte)(94)))));
            this.btnDeleteItem.BeforeTouchSize = new System.Drawing.Size(94, 28);
            this.btnDeleteItem.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnDeleteItem.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnDeleteItem.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnDeleteItem.IsBackStageButton = false;
            this.btnDeleteItem.Location = new System.Drawing.Point(119, 12);
            this.btnDeleteItem.Name = "btnDeleteItem";
            this.btnDeleteItem.Size = new System.Drawing.Size(94, 28);
            this.btnDeleteItem.TabIndex = 1;
            this.btnDeleteItem.Text = "Eliminar";
            this.btnDeleteItem.Click += new System.EventHandler(this.btnDeleteItem_Click);
            // 
            // buttonAdv1
            // 
            this.buttonAdv1.Appearance = Syncfusion.Windows.Forms.ButtonAppearance.Metro;
            this.buttonAdv1.BackColor = System.Drawing.Color.SteelBlue;
            this.buttonAdv1.BeforeTouchSize = new System.Drawing.Size(91, 28);
            this.buttonAdv1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonAdv1.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonAdv1.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.buttonAdv1.IsBackStageButton = false;
            this.buttonAdv1.Location = new System.Drawing.Point(219, 12);
            this.buttonAdv1.Name = "buttonAdv1";
            this.buttonAdv1.Size = new System.Drawing.Size(91, 28);
            this.buttonAdv1.TabIndex = 2;
            this.buttonAdv1.Text = "Nuevo";
            this.buttonAdv1.Click += new System.EventHandler(this.buttonAdv1_Click);
            // 
            // btnAddItem
            // 
            this.btnAddItem.Appearance = Syncfusion.Windows.Forms.ButtonAppearance.Metro;
            this.btnAddItem.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(153)))), ((int)(((byte)(181)))), ((int)(((byte)(105)))));
            this.btnAddItem.BeforeTouchSize = new System.Drawing.Size(91, 28);
            this.btnAddItem.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnAddItem.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAddItem.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnAddItem.IsBackStageButton = false;
            this.btnAddItem.Location = new System.Drawing.Point(22, 12);
            this.btnAddItem.Name = "btnAddItem";
            this.btnAddItem.Size = new System.Drawing.Size(91, 28);
            this.btnAddItem.TabIndex = 0;
            this.btnAddItem.Text = "Guardar";
            this.btnAddItem.Click += new System.EventHandler(this.btnAddItem_Click);
            // 
            // ClaveUnidad
            // 
            this.ClaveUnidad.FormattingEnabled = true;
            this.ClaveUnidad.Location = new System.Drawing.Point(95, 193);
            this.ClaveUnidad.Name = "ClaveUnidad";
            this.ClaveUnidad.Size = new System.Drawing.Size(214, 21);
            this.ClaveUnidad.TabIndex = 16;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(19, 196);
            this.label2.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(71, 13);
            this.label2.TabIndex = 15;
            this.label2.Text = "Clave Unidad";
            // 
            // parte
            // 
            this.parte.Location = new System.Drawing.Point(55, 72);
            this.parte.Margin = new System.Windows.Forms.Padding(2);
            this.parte.Name = "parte";
            this.parte.Size = new System.Drawing.Size(255, 20);
            this.parte.TabIndex = 6;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(19, 75);
            this.label1.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(32, 13);
            this.label1.TabIndex = 5;
            this.label1.Text = "Parte";
            // 
            // NoIdentificacion
            // 
            this.NoIdentificacion.Location = new System.Drawing.Point(110, 96);
            this.NoIdentificacion.Margin = new System.Windows.Forms.Padding(2);
            this.NoIdentificacion.Name = "NoIdentificacion";
            this.NoIdentificacion.Size = new System.Drawing.Size(200, 20);
            this.NoIdentificacion.TabIndex = 8;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(19, 99);
            this.label4.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(87, 13);
            this.label4.TabIndex = 7;
            this.label4.Text = "No Identificación";
            // 
            // Cantidad
            // 
            this.Cantidad.Location = new System.Drawing.Point(133, 120);
            this.Cantidad.Margin = new System.Windows.Forms.Padding(2);
            this.Cantidad.Name = "Cantidad";
            this.Cantidad.Size = new System.Drawing.Size(177, 20);
            this.Cantidad.TabIndex = 10;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(19, 123);
            this.label6.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(112, 13);
            this.label6.TabIndex = 9;
            this.label6.Text = "Cantidad por producto";
            // 
            // ValorUnitario
            // 
            this.ValorUnitario.Location = new System.Drawing.Point(91, 144);
            this.ValorUnitario.Margin = new System.Windows.Forms.Padding(2);
            this.ValorUnitario.Name = "ValorUnitario";
            this.ValorUnitario.Size = new System.Drawing.Size(219, 20);
            this.ValorUnitario.TabIndex = 12;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(19, 147);
            this.label7.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(68, 13);
            this.label7.TabIndex = 11;
            this.label7.Text = "Valor unitario";
            // 
            // Importe
            // 
            this.Importe.Location = new System.Drawing.Point(65, 168);
            this.Importe.Margin = new System.Windows.Forms.Padding(2);
            this.Importe.Name = "Importe";
            this.Importe.Size = new System.Drawing.Size(245, 20);
            this.Importe.TabIndex = 14;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(19, 171);
            this.label8.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(42, 13);
            this.label8.TabIndex = 13;
            this.label8.Text = "Importe";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(19, 270);
            this.label9.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(144, 13);
            this.label9.TabIndex = 17;
            this.label9.Text = "Clave de Producto y Servicio";
            // 
            // ClaveProdServ
            // 
            this.ClaveProdServ.Location = new System.Drawing.Point(168, 267);
            this.ClaveProdServ.Margin = new System.Windows.Forms.Padding(2);
            this.ClaveProdServ.Name = "ClaveProdServ";
            this.ClaveProdServ.Size = new System.Drawing.Size(142, 20);
            this.ClaveProdServ.TabIndex = 18;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(19, 222);
            this.label3.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(41, 13);
            this.label3.TabIndex = 17;
            this.label3.Text = "Unidad";
            // 
            // Unidad
            // 
            this.Unidad.Location = new System.Drawing.Point(64, 219);
            this.Unidad.Margin = new System.Windows.Forms.Padding(2);
            this.Unidad.Name = "Unidad";
            this.Unidad.Size = new System.Drawing.Size(245, 20);
            this.Unidad.TabIndex = 18;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(19, 246);
            this.label10.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(63, 13);
            this.label10.TabIndex = 17;
            this.label10.Text = "Descripción";
            // 
            // Descripcion
            // 
            this.Descripcion.Location = new System.Drawing.Point(86, 243);
            this.Descripcion.Margin = new System.Windows.Forms.Padding(2);
            this.Descripcion.Name = "Descripcion";
            this.Descripcion.Size = new System.Drawing.Size(224, 20);
            this.Descripcion.TabIndex = 18;
            // 
            // frmParte
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(321, 316);
            this.Controls.Add(this.ClaveUnidad);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.Unidad);
            this.Controls.Add(this.Descripcion);
            this.Controls.Add(this.ClaveProdServ);
            this.Controls.Add(this.Importe);
            this.Controls.Add(this.ValorUnitario);
            this.Controls.Add(this.Cantidad);
            this.Controls.Add(this.NoIdentificacion);
            this.Controls.Add(this.parte);
            this.Controls.Add(this.producto);
            this.Controls.Add(this.btnDeleteItem);
            this.Controls.Add(this.buttonAdv1);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.btnAddItem);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.label2);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MetroColor = System.Drawing.Color.Transparent;
            this.Name = "frmParte";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Parte";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.frmParte_FormClosed);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox producto;
        private Syncfusion.Windows.Forms.ButtonAdv btnDeleteItem;
        private Syncfusion.Windows.Forms.ButtonAdv buttonAdv1;
        private Syncfusion.Windows.Forms.ButtonAdv btnAddItem;
        private System.Windows.Forms.ComboBox ClaveUnidad;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox parte;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox NoIdentificacion;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox Cantidad;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox ValorUnitario;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox Importe;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox ClaveProdServ;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox Unidad;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox Descripcion;
    }
}