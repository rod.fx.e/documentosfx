#region Copyright Syncfusion Inc. 2001 - 2017
// Copyright Syncfusion Inc. 2001 - 2017. All rights reserved.
// Use of this code is subject to the terms of our license.
// A copy of the current license can be obtained at any time by e-mailing
// licensing@syncfusion.com. Any infringement will be prosecuted under
// applicable laws. 
#endregion
using Generales;
using ModeloDocumentosFX;
using OfficeOpenXml;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.Entity.Validation;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows.Forms;
//TODO: Validar la carga de facturas el monto que el monto de la linea no sea mayor a la monto de la linea de la factura


namespace ClasificadorProductos.formularios
{
    public partial class frmFacturaManual : Form//Syncfusion.Windows.Forms.MetroForm
    {
        public string FacturaTr { get; set; }
        public string UUIDTr { get; set; }
        public frmFacturaManual()
        {
            InitializeComponent();            
        }

        private void Guardar()
        {
            if ((!String.IsNullOrEmpty(Factura.Text)) && (!String.IsNullOrEmpty(UUID.Text)))
            {
                FacturaTr = Factura.Text;
                UUIDTr = UUID.Text;
                DialogResult = DialogResult.OK;
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            Guardar();
        }
    }
}
