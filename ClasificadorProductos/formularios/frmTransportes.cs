#region Copyright Syncfusion Inc. 2001 - 2017
// Copyright Syncfusion Inc. 2001 - 2017. All rights reserved.
// Use of this code is subject to the terms of our license.
// A copy of the current license can be obtained at any time by e-mailing
// licensing@syncfusion.com. Any infringement will be prosecuted under
// applicable laws. 
#endregion
using Generales;
using ModeloCartaPorte;
using OfficeOpenXml;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.Entity.Validation;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace ClasificadorProductos.formularios
{
    public partial class frmTransportes : Syncfusion.Windows.Forms.MetroForm
    {
        public frmTransportes()
        {
            InitializeComponent();
            Cargar();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Cargar();
        }

        private void Cargar()
        {
            try
            {
                ModeloCartaPorte.CartaPorteEntities Db = new ModeloCartaPorte.CartaPorteEntities();
                List <Clases.Transporte> Lista= Db.TransporteSet
                                             .Where(a => a.Placa.Contains(buscar.Text))
                                             .OrderBy(a => a.Placa)
                                             .ToList()
                                             .Select(a => new Clases.Transporte
                              (
                                    a.Placa
                                    , a.Anio
                              ))
                          .ToList();

                Dtg.Rows.Clear();
                foreach (Clases.Transporte registro in Lista)
                {
                    if (registro != null)
                    {
                        int n = Dtg.Rows.Add();
                        Dtg.Rows[n].Tag = registro.Id;
                        Dtg.Rows[n].Cells["Placa"].Value = registro.Placa ;
                        Dtg.Rows[n].Cells["Anio"].Value = registro.Anio;
                    }
                }
                Db.Dispose();
                this.Dtg.Columns[0].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
            }
            catch (DbEntityValidationException error)
            {
                foreach (var eve in error.EntityValidationErrors)
                {
                    foreach (var ve in eve.ValidationErrors)
                    {
                        MessageBox.Show(this, ve.PropertyName + " Error: " + ve.ErrorMessage, Application.ProductName
                            , MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
            }
        }

        private void Importar()
        {
            Application.DoEvents();
            OpenFileDialog openFileDialog1 = new OpenFileDialog();
            openFileDialog1.Filter = "Excel files (*.xlsx)|*.xlsx|All files (*.*)|*.*";
            openFileDialog1.FilterIndex = 1;
            openFileDialog1.RestoreDirectory = true;

            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                try
                {
                    Procesar(Dtg, 1, openFileDialog1.FileName);
                }
                catch (Exception)
                {
                    MessageBox.Show("No existe el Archivo seleccionado", Application.ProductName + "-" + Application.ProductVersion, MessageBoxButtons.OK);
                }
                finally
                {
                    Application.DoEvents();
                }
            }
        }

        private void Procesar(DataGridView dtg, int hoja, string archivo)
        {
            string tmpFile = Path.GetTempFileName();
            try
            {
                if (!File.Exists(archivo))
                {
                    MessageBox.Show(this, "El archivo " + archivo + " no existe.", Application.ProductName
                        , MessageBoxButtons.OK, MessageBoxIcon.Error);
                }

                File.Delete(tmpFile);
                File.Copy(archivo, tmpFile);

                dtg.Rows.Clear();
                int rowIndex = 2;
                FileInfo existingFile = new FileInfo(tmpFile);
                using (ExcelPackage package = new ExcelPackage(existingFile))
                {
                    ExcelWorksheet worksheet = package.Workbook.Worksheets["Transporte"];
                    while (worksheet.Cells[rowIndex, 2].Value != null)
                    {
                        try
                        {

                            Transporte O = new Transporte();
                            O.PermisoSCT=worksheet.Cells[rowIndex, 1].Text;
                            O.NumeroPermisoSCT=worksheet.Cells[rowIndex, 2].Text;
                            O.Placa=worksheet.Cells[rowIndex, 3].Text;
                            O.Anio=worksheet.Cells[rowIndex, 4].Text;
                            O.AseguradoraResponsabilidadCivil=worksheet.Cells[rowIndex, 5].Text;
                            O.PolizaResponsabilidadCivil=worksheet.Cells[rowIndex, 6].Text;
                            O.AseguradoraMedioAmbiente=worksheet.Cells[rowIndex, 7].Text;
                            O.PolizaMedioAmbiente=worksheet.Cells[rowIndex, 8].Text;
                            O.AseguradoraCarga=worksheet.Cells[rowIndex, 9].Text;
                            O.PolizaCarga=worksheet.Cells[rowIndex, 10].Text;
                            O.ValorMateriaPrimaSeguro=worksheet.Cells[rowIndex, 11].Text;
                            O.SubtipoRemolque=worksheet.Cells[rowIndex, 12].Text;
                            O.PlacaRemolque=worksheet.Cells[rowIndex, 13].Text;
                            Guardar(O);
                        }
                        catch (DbEntityValidationException e)
                        {
                            foreach (var eve in e.EntityValidationErrors)
                            {
                                foreach (var ve in eve.ValidationErrors)
                                {
                                    MessageBox.Show(this, ve.PropertyName + " Error: " + ve.ErrorMessage, Application.ProductName
                                        , MessageBoxButtons.OK, MessageBoxIcon.Error);
                                }
                            }
                        }
                        rowIndex++;
                    }
                }
            }
            catch (Exception e)
            {
                ErrorFX.mostrar(e, true, false,
                "frmTransportes - 150 - "
                );
            }
            finally
            {
                File.Delete(tmpFile);
                Cargar();
            }

        }

        private void Guardar(Transporte Registro)
        {
            ModeloCartaPorte.CartaPorteEntities Db = new ModeloCartaPorte.CartaPorteEntities();
            try
            {
                ModeloCartaPorte.Transporte O = new ModeloCartaPorte.Transporte();

                O = (from r in Db.TransporteSet.Where
                                        (a => a.Placa == Registro.Placa)
                            select r).FirstOrDefault();
                if (O == null)
                {
                    Db.TransporteSet.Add(Registro);
                    Db.SaveChanges();
                }
            }
            catch (DbEntityValidationException e)
            {
                foreach (var eve in e.EntityValidationErrors)
                {
                    foreach (var ve in eve.ValidationErrors)
                    {
                        MessageBox.Show(this, ve.PropertyName + " Error: " + ve.ErrorMessage, Application.ProductName
                            , MessageBoxButtons.OK, MessageBoxIcon.Error);
                        return;
                    }
                }
            }
        }

        private void dtg_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            Cargar_registro(e);
        }

        private void Cargar_registro(DataGridViewCellEventArgs e)
        {
            if (e.RowIndex > -1)
            {
                //Clases.Transportista Registro = (Clases.Transportista)Dtg.Rows[e.RowIndex].Tag;
                //frmTransportista o = new frmTransportista(Registro);
                //o.ShowDialog();
                //Cargar();
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            Importar();
        }

        private void button7_Click(object sender, EventArgs e)
        {
            GenerarPlantilla();
        }

        private void GenerarPlantilla()
        {
            try
            {
                var fileName = " Plantilla de Transporte " + DateTime.Now.ToString("yyyy-MM-dd-hh-mm-ss") + ".xlsx";
                if (folderBrowserDialog1.ShowDialog() == DialogResult.OK)
                {
                    fileName = folderBrowserDialog1.SelectedPath + "\\" + fileName;
                    System.IO.Stream myStream = new System.IO.FileStream(fileName, System.IO.FileMode.Create, System.IO.FileAccess.ReadWrite, System.IO.FileShare.None);

                    using (ExcelPackage pck = new ExcelPackage())
                    {

                        ExcelWorksheet ws1 = pck.Workbook.Worksheets.Add("Transporte");
                        ws1.Cells[1, 1].Value = "Permiso SCT";
                        ws1.Cells[1, 2].Value = "N�mero de Permiso SCT";
                        ws1.Cells[1, 3].Value = "Placa VM";
                        ws1.Cells[1, 4].Value = "A�o Modelo";
                        ws1.Cells[1, 5].Value = "Aseguradora de Responsabilidad Civil";
                        ws1.Cells[1, 6].Value = "P�liza de Responsabilidad Civil";
                        ws1.Cells[1, 7].Value = "Aseguradora Medio Ambiente";
                        ws1.Cells[1, 8].Value = "P�liza del Medio Ambiente";
                        ws1.Cells[1, 9].Value = "Aseguradora de Carga";
                        ws1.Cells[1, 10].Value = "P�liza de Carga";
                        ws1.Cells[1, 10].Value = "P�liza de Carga";
                        ws1.Cells[1, 10].Value = "P�liza de Carga";
                        ws1.Cells[1, 10].Value = "P�liza de Carga";
                        ws1.Cells[1, 10].Value = "P�liza de Carga";
                        ws1.Cells[1, 10].Value = "P�liza de Carga";

                        pck.SaveAs(myStream);
                    }
                    myStream.Close();
                    MessageBox.Show("Creaci�n de plantilla realizada " + Environment.NewLine + fileName + " finalizada.", Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
            catch(Exception e)
            {
                ErrorFX.mostrar(e,true,false,
                "frmTransportes - 264 - "
                    );
            }
        }

        private void dtg_CellDoubleClick_1(object sender, DataGridViewCellEventArgs e)
        {
            Cargar_registro(e);
        }

        private void button2_Click_1(object sender, EventArgs e)
        {
            Eliminar();
        }

        private void Eliminar()
        {
            if (Dtg.SelectedRows.Count==0)
            {
                MessageBox.Show(this, "Seleccione los registros", Application.ProductName + "-" + Application.ProductVersion, MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
            DialogResult Resultado = MessageBox.Show(this, "�Esta seguro de eliminar?", Application.ProductName + "-" + Application.ProductVersion, MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            if (Resultado!= DialogResult.Yes)
            {
                return;
            }
            ModeloCartaPorte.CartaPorteEntities Db = new ModeloCartaPorte.CartaPorteEntities();
            foreach (DataGridViewRow Row in Dtg.SelectedRows)
            {
                int IdTr = (int)Row.Tag;
                Transporte Registro = Db.TransporteSet
                                             .Where(a => a.Id.Equals(IdTr))
                                             .FirstOrDefault();

                Db.TransporteSet.Attach(Registro);
                Db.TransporteSet.Remove(Registro);
                Db.SaveChanges();
            }
            
            MessageBox.Show(this, "Eliminado.", Application.ProductName + "-" + Application.ProductVersion, MessageBoxButtons.OK, MessageBoxIcon.Information);
            Cargar();
        }
    }
}
