﻿
namespace ClasificadorProductos.formularios
{
    partial class frmAnalisis
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmAnalisis));
            this.dtgCFDI = new System.Windows.Forms.DataGridView();
            this.button1 = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.INICIO = new System.Windows.Forms.DateTimePicker();
            this.label4 = new System.Windows.Forms.Label();
            this.FINAL = new System.Windows.Forms.DateTimePicker();
            this.button2 = new System.Windows.Forms.Button();
            this.directorio = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.rfcReceptorCFDI = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.nombreReceptorCFDI = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Folio = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Serie = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.uuidCFDI = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.timbradoCFDI = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ValorUnitario = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Descripcion = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.NoIdentificacion = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ClaveProdServ = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ClaveUnidad = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.STOCK_UM = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ProductCodeConfigurado = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PRODUCT_CODE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.estado = new System.Windows.Forms.DataGridViewTextBoxColumn();
            ((System.ComponentModel.ISupportInitialize)(this.dtgCFDI)).BeginInit();
            this.SuspendLayout();
            // 
            // dtgCFDI
            // 
            this.dtgCFDI.AllowUserToAddRows = false;
            this.dtgCFDI.AllowUserToDeleteRows = false;
            this.dtgCFDI.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dtgCFDI.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dtgCFDI.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.rfcReceptorCFDI,
            this.nombreReceptorCFDI,
            this.Folio,
            this.Serie,
            this.uuidCFDI,
            this.timbradoCFDI,
            this.ValorUnitario,
            this.Descripcion,
            this.NoIdentificacion,
            this.ClaveProdServ,
            this.ClaveUnidad,
            this.STOCK_UM,
            this.ProductCodeConfigurado,
            this.PRODUCT_CODE,
            this.estado});
            this.dtgCFDI.Location = new System.Drawing.Point(11, 65);
            this.dtgCFDI.Margin = new System.Windows.Forms.Padding(2);
            this.dtgCFDI.Name = "dtgCFDI";
            this.dtgCFDI.RowTemplate.Height = 24;
            this.dtgCFDI.Size = new System.Drawing.Size(778, 374);
            this.dtgCFDI.TabIndex = 11;
            // 
            // button1
            // 
            this.button1.BackColor = System.Drawing.Color.Goldenrod;
            this.button1.FlatAppearance.BorderSize = 0;
            this.button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button1.Location = new System.Drawing.Point(11, 38);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(94, 22);
            this.button1.TabIndex = 485;
            this.button1.Text = "Buscar";
            this.button1.UseVisualStyleBackColor = false;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.White;
            this.label3.Location = new System.Drawing.Point(112, 44);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(32, 13);
            this.label3.TabIndex = 481;
            this.label3.Text = "Inicio";
            // 
            // INICIO
            // 
            this.INICIO.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.INICIO.Location = new System.Drawing.Point(149, 40);
            this.INICIO.Name = "INICIO";
            this.INICIO.Size = new System.Drawing.Size(94, 20);
            this.INICIO.TabIndex = 483;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.BackColor = System.Drawing.Color.White;
            this.label4.Location = new System.Drawing.Point(248, 44);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(29, 13);
            this.label4.TabIndex = 482;
            this.label4.Text = "Final";
            // 
            // FINAL
            // 
            this.FINAL.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.FINAL.Location = new System.Drawing.Point(282, 40);
            this.FINAL.Name = "FINAL";
            this.FINAL.Size = new System.Drawing.Size(94, 20);
            this.FINAL.TabIndex = 484;
            // 
            // button2
            // 
            this.button2.BackColor = System.Drawing.Color.LawnGreen;
            this.button2.FlatAppearance.BorderSize = 0;
            this.button2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button2.Location = new System.Drawing.Point(382, 38);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(94, 22);
            this.button2.TabIndex = 486;
            this.button2.Text = "Exportar";
            this.button2.UseVisualStyleBackColor = false;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // directorio
            // 
            this.directorio.Location = new System.Drawing.Point(66, 12);
            this.directorio.Name = "directorio";
            this.directorio.Size = new System.Drawing.Size(410, 20);
            this.directorio.TabIndex = 487;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.White;
            this.label1.Location = new System.Drawing.Point(8, 15);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(52, 13);
            this.label1.TabIndex = 488;
            this.label1.Text = "Directorio";
            // 
            // rfcReceptorCFDI
            // 
            this.rfcReceptorCFDI.HeaderText = "RFC";
            this.rfcReceptorCFDI.Name = "rfcReceptorCFDI";
            this.rfcReceptorCFDI.ReadOnly = true;
            // 
            // nombreReceptorCFDI
            // 
            this.nombreReceptorCFDI.HeaderText = "Receptor";
            this.nombreReceptorCFDI.Name = "nombreReceptorCFDI";
            this.nombreReceptorCFDI.ReadOnly = true;
            // 
            // Folio
            // 
            this.Folio.HeaderText = "Folio";
            this.Folio.Name = "Folio";
            this.Folio.ReadOnly = true;
            // 
            // Serie
            // 
            this.Serie.HeaderText = "Serie";
            this.Serie.Name = "Serie";
            this.Serie.ReadOnly = true;
            // 
            // uuidCFDI
            // 
            this.uuidCFDI.HeaderText = "UUID";
            this.uuidCFDI.Name = "uuidCFDI";
            this.uuidCFDI.ReadOnly = true;
            // 
            // timbradoCFDI
            // 
            this.timbradoCFDI.HeaderText = "timbradoCFDI";
            this.timbradoCFDI.Name = "timbradoCFDI";
            this.timbradoCFDI.ReadOnly = true;
            // 
            // ValorUnitario
            // 
            this.ValorUnitario.HeaderText = "ValorUnitario";
            this.ValorUnitario.Name = "ValorUnitario";
            this.ValorUnitario.ReadOnly = true;
            // 
            // Descripcion
            // 
            this.Descripcion.HeaderText = "Descripcion";
            this.Descripcion.Name = "Descripcion";
            this.Descripcion.ReadOnly = true;
            // 
            // NoIdentificacion
            // 
            this.NoIdentificacion.HeaderText = "NoIdentificacion";
            this.NoIdentificacion.Name = "NoIdentificacion";
            this.NoIdentificacion.ReadOnly = true;
            // 
            // ClaveProdServ
            // 
            this.ClaveProdServ.HeaderText = "ClaveProdServ";
            this.ClaveProdServ.Name = "ClaveProdServ";
            this.ClaveProdServ.ReadOnly = true;
            // 
            // ClaveUnidad
            // 
            this.ClaveUnidad.HeaderText = "ClaveUnidad";
            this.ClaveUnidad.Name = "ClaveUnidad";
            this.ClaveUnidad.ReadOnly = true;
            // 
            // STOCK_UM
            // 
            this.STOCK_UM.HeaderText = "UM";
            this.STOCK_UM.Name = "STOCK_UM";
            this.STOCK_UM.ReadOnly = true;
            // 
            // ProductCodeConfigurado
            // 
            this.ProductCodeConfigurado.HeaderText = "UM Configurado";
            this.ProductCodeConfigurado.Name = "ProductCodeConfigurado";
            this.ProductCodeConfigurado.ReadOnly = true;
            // 
            // PRODUCT_CODE
            // 
            this.PRODUCT_CODE.HeaderText = "Product Code";
            this.PRODUCT_CODE.Name = "PRODUCT_CODE";
            this.PRODUCT_CODE.ReadOnly = true;
            // 
            // estado
            // 
            this.estado.HeaderText = "Estado";
            this.estado.Name = "estado";
            this.estado.ReadOnly = true;
            // 
            // frmAnalisis
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.directorio);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.INICIO);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.FINAL);
            this.Controls.Add(this.dtgCFDI);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "frmAnalisis";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Analisís";
            ((System.ComponentModel.ISupportInitialize)(this.dtgCFDI)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView dtgCFDI;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.DateTimePicker INICIO;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.DateTimePicker FINAL;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.TextBox directorio;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.DataGridViewTextBoxColumn rfcReceptorCFDI;
        private System.Windows.Forms.DataGridViewTextBoxColumn nombreReceptorCFDI;
        private System.Windows.Forms.DataGridViewTextBoxColumn Folio;
        private System.Windows.Forms.DataGridViewTextBoxColumn Serie;
        private System.Windows.Forms.DataGridViewTextBoxColumn uuidCFDI;
        private System.Windows.Forms.DataGridViewTextBoxColumn timbradoCFDI;
        private System.Windows.Forms.DataGridViewTextBoxColumn ValorUnitario;
        private System.Windows.Forms.DataGridViewTextBoxColumn Descripcion;
        private System.Windows.Forms.DataGridViewTextBoxColumn NoIdentificacion;
        private System.Windows.Forms.DataGridViewTextBoxColumn ClaveProdServ;
        private System.Windows.Forms.DataGridViewTextBoxColumn ClaveUnidad;
        private System.Windows.Forms.DataGridViewTextBoxColumn STOCK_UM;
        private System.Windows.Forms.DataGridViewTextBoxColumn ProductCodeConfigurado;
        private System.Windows.Forms.DataGridViewTextBoxColumn PRODUCT_CODE;
        private System.Windows.Forms.DataGridViewTextBoxColumn estado;
    }
}