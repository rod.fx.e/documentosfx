#region Copyright Syncfusion Inc. 2001 - 2017
// Copyright Syncfusion Inc. 2001 - 2017. All rights reserved.
// Use of this code is subject to the terms of our license.
// A copy of the current license can be obtained at any time by e-mailing
// licensing@syncfusion.com. Any infringement will be prosecuted under
// applicable laws. 
#endregion
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;

using System.Text;
using System.Windows.Forms;
using ModeloDocumentosFX;
using System.Text;
using System.Windows.Forms;
using System.Linq;
using System.Data.Entity.Validation;
using Generales;

namespace ClasificadorProductos.formularios
{
    public partial class frmConversinUMSingle : Syncfusion.Windows.Forms.MetroForm
    {
        bool modificado = false;
        public string ClaveProdServp = "";
        public string productop = "";
        configuracionUM oRegistro;
        public frmConversinUMSingle()
        {
            InitializeComponent();
            limpiar();
        }

        public frmConversinUMSingle(configuracionUM oRegistrop)
        {
            InitializeComponent();
            limpiar();
            oRegistro = oRegistrop;
            cargar();
        }


        #region Metodos
        private void cargar()
        {
            SAT.Text = oRegistro.SAT;
            UnidadMedida.Text = oRegistro.UnidadMedida;
            CE.Text = oRegistro.CE;
            FactorCE.Text = oRegistro.FactorCE.ToString();
        }

        public void limpiar()
        {
            if (modificado)
            {

                DialogResult Resultado = MessageBox.Show(this, "�Desea guardar los cambios?", Application.ProductName + "-" + Application.ProductVersion, MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                if (Resultado.ToString() == "Yes")
                {
                    guardar();
                }
            }
            oRegistro = new configuracionUM();
            modificado = false;
            foreach (var c in this.Controls)
            {
                if (c is TextBox)
                {
                    ((TextBox)c).Text = "";
                }
            }
            FactorCE.Text = "1";
        }
        private void guardar()
        {

            ModeloDocumentosFX.ModeloDocumentosFXContainer dbContext = new ModeloDocumentosFXContainer();
            try
            {
                configuracionUM registro = new configuracionUM();
                modificado = false;
                if (oRegistro.Id != 0)
                {
                    //Cargar registro
                    int valor = oRegistro.Id;
                    registro = (from r in dbContext.configuracionUMSet.Where
                                            (a => a.Id == valor)
                                select r).FirstOrDefault();
                    modificado = true;
                }
                registro.UnidadMedida = UnidadMedida.Text;
                registro.SAT = SAT.Text;
                registro.CE = CE.Text;
                if (Globales.IsNumeric(FactorCE.Text))
                {
                    registro.FactorCE = decimal.Parse(FactorCE.Text);
                }
                else
                {
                    registro.FactorCE = 1;
                }

                if (!modificado)
                {
                    dbContext.configuracionUMSet.Add(registro);
                }
                else
                {
                    dbContext.configuracionUMSet.Attach(registro);
                    dbContext.Entry(registro).State = System.Data.Entity.EntityState.Modified;
                }

                dbContext.SaveChanges();
                oRegistro = registro;
                modificado = false;
                MessageBox.Show(this, "Datos guardados", Application.ProductName + "-" + Application.ProductVersion, MessageBoxButtons.OK, MessageBoxIcon.Information);
                if (productop!="")
                {
                    ClaveProdServp = SAT.Text;
                    DialogResult = DialogResult.OK;
                }
            }
            catch (DbEntityValidationException e)
            {
                foreach (var eve in e.EntityValidationErrors)
                {
                    foreach (var ve in eve.ValidationErrors)
                    {
                        MessageBox.Show(this, ve.PropertyName + " Error: " + ve.ErrorMessage, Application.ProductName
                            , MessageBoxButtons.OK, MessageBoxIcon.Error);
                        return;
                    }
                }
            }
        }

        private void eliminar()
        {

            DialogResult Resultado = MessageBox.Show(this, "�Esta seguro de eliminar?", Application.ProductName + "-" + Application.ProductVersion, MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            if (Resultado.ToString() != "Yes")
            {
                return;
            }
            ModeloDocumentosFX.ModeloDocumentosFXContainer dbContext = new ModeloDocumentosFXContainer();
            dbContext.configuracionUMSet.Attach(oRegistro);
            dbContext.configuracionUMSet.Remove(oRegistro);
            dbContext.SaveChanges();
            MessageBox.Show(this, "Eliminado.", Application.ProductName + "-" + Application.ProductVersion, MessageBoxButtons.OK, MessageBoxIcon.Information);
            limpiar();
        }


        #endregion

        private void buttonAdv1_Click(object sender, EventArgs e)
        {
            limpiar();
        }

        private void btnAddItem_Click(object sender, EventArgs e)
        {
            guardar();
        }

        private void btnDeleteItem_Click(object sender, EventArgs e)
        {
            eliminar();
        }

        private void frmConversinUMSingle_FormClosed(object sender, FormClosedEventArgs e)
        {
            DialogResult = DialogResult.Cancel;
        }
    }
}
