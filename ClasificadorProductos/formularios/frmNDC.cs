#region Copyright Syncfusion Inc. 2001 - 2017
// Copyright Syncfusion Inc. 2001 - 2017. All rights reserved.
// Use of this code is subject to the terms of our license.
// A copy of the current license can be obtained at any time by e-mailing
// licensing@syncfusion.com. Any infringement will be prosecuted under
// applicable laws. 
#endregion
using Generales;
using ModeloDocumentosFX;
using OfficeOpenXml;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.Entity.Validation;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace ClasificadorProductos.formularios
{
    public partial class frmNDC : Syncfusion.Windows.Forms.MetroForm
    {
        public frmNDC()
        {
            InitializeComponent();
            cargar();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            cargar();
        }

        private void cargar()
        {
            try
            {
                ModeloDocumentosFX.ModeloDocumentosFXContainer dbContext = new ModeloDocumentosFXContainer();
                var result = from b in dbContext.configuracionProductoSet
                                             .Where(a => a.producto.Contains(buscar.Text))
                                             .OrderBy(a => a.producto)
                             select b;

                List<configuracionProducto> dt = result.ToList();
                dtg.Rows.Clear();
                foreach (configuracionProducto registro in dt)
                {
                    if (registro != null)
                    {
                        int n = dtg.Rows.Add();
                        dtg.Rows[n].Tag = registro;
                        dtg.Rows[n].Cells["producto"].Value = registro.producto;
                        dtg.Rows[n].Cells["clasificacionSAT"].Value = registro.clasificacionSAT;


                    }

                }
                dbContext.Dispose();
                this.dtg.Columns[0].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
            }
            catch (DbEntityValidationException error)
            {
                foreach (var eve in error.EntityValidationErrors)
                {
                    foreach (var ve in eve.ValidationErrors)
                    {
                        MessageBox.Show(this, ve.PropertyName + " Error: " + ve.ErrorMessage, Application.ProductName
                            , MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
            }
        }

        private void importar()
        {
            Application.DoEvents();
            OpenFileDialog openFileDialog1 = new OpenFileDialog();
            openFileDialog1.Filter = "Excel files (*.xlsx)|*.xlsx|All files (*.*)|*.*";
            openFileDialog1.FilterIndex = 1;
            openFileDialog1.RestoreDirectory = true;

            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                try
                {
                    procesar(dtg, 1, openFileDialog1.FileName);
                }
                catch (Exception)
                {
                    MessageBox.Show("No existe el Archivo seleccionado", Application.ProductName + "-" + Application.ProductVersion, MessageBoxButtons.OK);
                }
                finally
                {
                    Application.DoEvents();
                }
            }
        }

        private void procesar(DataGridView dtg, int hoja, string archivo)
        {
            string tmpFile = Path.GetTempFileName();
            try
            {
                if (!File.Exists(archivo))
                {
                    MessageBox.Show(this, "El archivo " + archivo + " no existe.", Application.ProductName
                        , MessageBoxButtons.OK, MessageBoxIcon.Error);
                }

                File.Delete(tmpFile);
                File.Copy(archivo, tmpFile);

                dtg.Rows.Clear();
                int rowIndex = 2;
                FileInfo existingFile = new FileInfo(tmpFile);
                using (ExcelPackage package = new ExcelPackage(existingFile))
                {
                    ExcelWorksheet worksheet = package.Workbook.Worksheets[hoja];
                    while (worksheet.Cells[rowIndex, 2].Value != null)
                    {
                        try
                        {
                            string productotr = worksheet.Cells[rowIndex, 1].Text;
                            string clasificacion = worksheet.Cells[rowIndex, 2].Text;

                            guardar(productotr, clasificacion);
                        }
                        catch (DbEntityValidationException e)
                        {
                            foreach (var eve in e.EntityValidationErrors)
                            {
                                foreach (var ve in eve.ValidationErrors)
                                {
                                    MessageBox.Show(this, ve.PropertyName + " Error: " + ve.ErrorMessage, Application.ProductName
                                        , MessageBoxButtons.OK, MessageBoxIcon.Error);
                                }
                            }
                        }
                        rowIndex++;
                    }
                }
            }
            catch (Exception e)
            {
                ErrorFX.mostrar(e, true, false,
                "frmNDC - 150 - "
                );
            }
            finally
            {
                File.Delete(tmpFile);
                cargar();
            }

        }

        private void guardar(string productotr, string clasificacion)
        {
            ModeloDocumentosFX.ModeloDocumentosFXContainer dbContext = new ModeloDocumentosFXContainer();
            try
            {
                configuracionProducto registro = new configuracionProducto();

                registro = (from r in dbContext.configuracionProductoSet.Where
                                        (a => a.producto == productotr
                                        & a.clasificacionSAT == clasificacion)
                            select r).FirstOrDefault();

                if (registro == null)
                {
                    registro = new configuracionProducto();
                    registro.producto = productotr;
                    registro.clasificacionSAT = clasificacion;
                    registro.fechaCreacion = DateTime.Now;
                    dbContext.configuracionProductoSet.Add(registro);
                    dbContext.SaveChanges();
                }
                //else
                //{
                //    dbContext.configuracionProductoSet.Attach(registro);
                //    dbContext.Entry(registro).State = System.Data.Entity.EntityState.Modified;
                //}

                
            }
            catch (DbEntityValidationException e)
            {
                foreach (var eve in e.EntityValidationErrors)
                {
                    foreach (var ve in eve.ValidationErrors)
                    {
                        MessageBox.Show(this, ve.PropertyName + " Error: " + ve.ErrorMessage, Application.ProductName
                            , MessageBoxButtons.OK, MessageBoxIcon.Error);
                        return;
                    }
                }
            }
        }

        private void dtg_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            cargar_registro(e);
        }

        private void cargar_registro(DataGridViewCellEventArgs e)
        {
            if (e.RowIndex > -1)
            {
                configuracionProducto registro = (configuracionProducto)dtg.Rows[e.RowIndex].Tag;
                frmProductoClasificacion o = new frmProductoClasificacion(registro);
                o.ShowDialog();
                cargar();
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            importar();
        }

        private void button7_Click(object sender, EventArgs e)
        {
            plantillaProductCode();
        }

        private void plantillaProductCode()
        {
            try
            {
                MessageBox.Show("La configuración de  la Plantilla para migrar Clasificación de productos es la siguiente:" + Environment.NewLine
                + "Fila 1 - Encabezado" + Environment.NewLine
                + "Columna 1: Producto " + Environment.NewLine
                + "Columna 1: Clasificación " + Environment.NewLine
                + "Si no tiene esta configuración no será cargada la información. "
                , Application.ProductName + "-" + Application.ProductVersion.ToString()
                , MessageBoxButtons.OK
                );

                var fileName = " Plantilla de Clasificación de Productos " + DateTime.Now.ToString("yyyy-MM-dd-hh-mm-ss") + ".xlsx";
                if (folderBrowserDialog1.ShowDialog() == DialogResult.OK)
                {
                    fileName = folderBrowserDialog1.SelectedPath + "\\" + fileName;
                    System.IO.Stream myStream = new System.IO.FileStream(fileName, System.IO.FileMode.Create, System.IO.FileAccess.ReadWrite, System.IO.FileShare.None);

                    using (ExcelPackage pck = new ExcelPackage())
                    {

                        ExcelWorksheet ws1 = pck.Workbook.Worksheets.Add("Plantilla");
                        ws1.Cells[1, 1].Value = "Producto";
                        ws1.Cells[1, 2].Value = "Producto";
                        pck.SaveAs(myStream);
                    }
                    myStream.Close();
                    MessageBox.Show("Creación de plantilla realizada " + Environment.NewLine + fileName + " finalizada.", Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
            catch(Exception e)
            {
                ErrorFX.mostrar(e,true,false,
                "frmNDC - 264 - "
                    );
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            frmProductoClasificacion o = new frmProductoClasificacion();
            o.Show();
        }

        private void dtg_CellDoubleClick_1(object sender, DataGridViewCellEventArgs e)
        {
            cargar_registro(e);
        }
    }
}
