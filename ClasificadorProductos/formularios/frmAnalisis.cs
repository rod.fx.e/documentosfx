﻿using CFDI33;
using Generales;
using ModeloDocumentosFX;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml;
using System.Xml.Linq;
using System.Xml.Serialization;

namespace ClasificadorProductos.formularios
{
    public partial class frmAnalisis : Form
    {
        cCONEXCION oData;
        public frmAnalisis()
        {
            InitializeComponent();
            cEMPRESA ocEMPRESA = new cEMPRESA();
            List<cEMPRESA> lista = ocEMPRESA.todos("");                      
            oData = new cCONEXCION(lista[0].SERVIDOR, lista[0].BD, lista[0].USUARIO_BD, lista[0].PASSWORD_BD);
        }

        private void cargar_cfdi()
        {
            if (!Directory.Exists(directorio.Text))
            {
                using (var fbd = new FolderBrowserDialog())
                {
                    DialogResult result = fbd.ShowDialog();
                    if (result == DialogResult.OK && !string.IsNullOrWhiteSpace(fbd.SelectedPath))
                    {
                        directorio.Text = fbd.SelectedPath;
                    }
                }
            }
            

            if (!String.IsNullOrEmpty(directorio.Text))
            {
                if (Directory.Exists(directorio.Text))
                {
                    List<string> _list;
                    _list = new List<string>();
                    this.dtgCFDI.Rows.Clear();
                    foreach (string file in Directory.GetFiles(directorio.Text, "*.xml", SearchOption.AllDirectories))
                    {
                        procesarCFDI(file);
                    }
                }
            }

            //OpenFileDialog openFileDialog = new OpenFileDialog();
            //openFileDialog.Multiselect = true;
            //openFileDialog.Filter = "Archivos CFDI  (*.xml)|*.xml";
            //openFileDialog.InitialDirectory = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments);
            //if (openFileDialog.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            //{
                
            //    foreach (string filename in openFileDialog.FileNames)
            //    {
                    
            //    }

            //}
        }

        private void procesarCFDI(string filename)
        {
            int n = this.dtgCFDI.Rows.Add();

            Comprobante oComprobante;
            //Pasear el comprobante
            try
            {
                XmlSerializer serializer_CFD_3 = new XmlSerializer(typeof(Comprobante));
                TextReader reader = new StreamReader(filename);
                oComprobante = (Comprobante)serializer_CFD_3.Deserialize(reader);
                reader.Dispose();
                reader.Close();
                this.dtgCFDI.Rows[n].Tag = filename;
                this.dtgCFDI.Rows[n].Cells["rfcReceptorCFDI"].Value = oComprobante.Receptor.Rfc;
                this.dtgCFDI.Rows[n].Cells["nombreReceptorCFDI"].Value = oComprobante.Receptor.Nombre;

                this.dtgCFDI.Rows[n].Cells["Folio"].Value = oComprobante.Folio;
                this.dtgCFDI.Rows[n].Cells["Serie"].Value = oComprobante.Serie;

                //Determinar si es Timbrado o Nomina12
                foreach (ComprobanteComplemento complemento in oComprobante.Complemento)
                {
                    foreach (XmlElement elemento in complemento.Any)
                    {
                        //Timbrado
                        if (elemento.OuterXml.Contains("tfd:TimbreFiscalDigital"))
                        {
                            var doc = XDocument.Parse(elemento.OuterXml);
                            string uuidtr = doc.Root.Attributes("UUID").First().Value.ToString();
                            this.dtgCFDI.Rows[n].Cells["uuidCFDI"].Value = uuidtr;
                            string FechaTimbradotr = doc.Root.Attributes("FechaTimbrado").First().Value.ToString();
                            this.dtgCFDI.Rows[n].Cells["timbradoCFDI"].Value = FechaTimbradotr;
                        }
                    }
                }

                //Conceptos
                bool agregarLinea = false;
                foreach (ComprobanteConcepto concepto in oComprobante.Conceptos)
                {
                    if (agregarLinea)
                    {
                        n = n = this.dtgCFDI.Rows.Add();
                    }
                    this.dtgCFDI.Rows[n].Cells["Descripcion"].Value = concepto.Descripcion;
                    this.dtgCFDI.Rows[n].Cells["NoIdentificacion"].Value = concepto.NoIdentificacion;
                    //Buscar UM de Visual
                    InfoProduct oInfoProduct = obtenerInfoPart(concepto.NoIdentificacion);
                    this.dtgCFDI.Rows[n].Cells["STOCK_UM"].Value = oInfoProduct.STOCK_UM;
                    this.dtgCFDI.Rows[n].Cells["PRODUCT_CODE"].Value = oInfoProduct.PRODUCT_CODE;
                    this.dtgCFDI.Rows[n].Cells["ProductCodeConfigurado"].Value = obtenerConfiguracion(oInfoProduct.PRODUCT_CODE);
                    
                    this.dtgCFDI.Rows[n].Cells["ValorUnitario"].Value = concepto.ValorUnitario;
                    this.dtgCFDI.Rows[n].Cells["ClaveProdServ"].Value = concepto.ClaveProdServ;
                    this.dtgCFDI.Rows[n].Cells["ClaveUnidad"].Value = concepto.ClaveUnidad;
                    
                    agregarLinea = true;
                }


                this.dtgCFDI.Rows[n].Cells["estado"].Value = "Valido";
            }
            catch (Exception e)
            {
                dtgCFDI.Rows[n].ErrorText = "Cargar Comprobante Automatica: El archivo " + filename + " no es un CFDI válido."
                    + e.Message.ToString();
                dtgCFDI.Rows[n].Cells["estado"].Value = "No valido";
            }

        }

        private string obtenerConfiguracion(string pRODUCT_CODE)
        {
            string resultado = "";
            ModeloDocumentosFXContainer dbContext = new ModeloDocumentosFXContainer();
            configuracionProducto registro = (from r in dbContext.configuracionProductoSet
                                                  .Where(a => a.producto == pRODUCT_CODE)
                                              select r).FirstOrDefault();
            if (registro != null)
            {
                return registro.unidadSAT;
            }
            return resultado;
        }

        private InfoProduct obtenerInfoPart(string noIdentificacion)
        {
            InfoProduct oInfoProduct = new InfoProduct();
            try
            {
                string sSQL = @"
                SELECT TOP 1 STOCK_UM, PRODUCT_CODE
                FROM PART p
                WHERE p.ID='" + noIdentificacion + @"'";

                DataTable oDataTable = oData.EjecutarConsulta(sSQL);
                if (oDataTable != null)
                {
                    foreach (DataRow registro in oDataTable.Rows)
                    {
                        oInfoProduct.STOCK_UM=registro["STOCK_UM"].ToString();
                        oInfoProduct.PRODUCT_CODE = registro["PRODUCT_CODE"].ToString();
                    }
                }

            }
            catch (Exception error)
            {
                MessageBox.Show(this, error.Message, Application.ProductName
                    , MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            return oInfoProduct;

        }

        private void button1_Click(object sender, EventArgs e)
        {
            cargar_cfdi();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            Generales.ExcelFX.exportar(dtgCFDI, "CFDI Exportacion");
        }

        class InfoProduct
        {
            public string STOCK_UM { get; set; }
            public string PRODUCT_CODE { get; set; }
        }

    }
}
