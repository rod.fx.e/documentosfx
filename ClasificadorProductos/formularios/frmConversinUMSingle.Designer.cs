#region Copyright Syncfusion Inc. 2001 - 2017
// Copyright Syncfusion Inc. 2001 - 2017. All rights reserved.
// Use of this code is subject to the terms of our license.
// A copy of the current license can be obtained at any time by e-mailing
// licensing@syncfusion.com. Any infringement will be prosecuted under
// applicable laws. 
#endregion
namespace ClasificadorProductos.formularios
{
    partial class frmConversinUMSingle
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmConversinUMSingle));
            this.label5 = new System.Windows.Forms.Label();
            this.SAT = new System.Windows.Forms.TextBox();
            this.UnidadMedida = new System.Windows.Forms.TextBox();
            this.btnDeleteItem = new Syncfusion.Windows.Forms.ButtonAdv();
            this.buttonAdv1 = new Syncfusion.Windows.Forms.ButtonAdv();
            this.btnAddItem = new Syncfusion.Windows.Forms.ButtonAdv();
            this.label3 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.CE = new System.Windows.Forms.TextBox();
            this.FactorCE = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(12, 52);
            this.label5.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(94, 13);
            this.label5.TabIndex = 3;
            this.label5.Text = "Unidad de Medida";
            // 
            // SAT
            // 
            this.SAT.Location = new System.Drawing.Point(155, 73);
            this.SAT.Margin = new System.Windows.Forms.Padding(2);
            this.SAT.Name = "SAT";
            this.SAT.Size = new System.Drawing.Size(145, 20);
            this.SAT.TabIndex = 6;
            // 
            // UnidadMedida
            // 
            this.UnidadMedida.Location = new System.Drawing.Point(155, 49);
            this.UnidadMedida.Margin = new System.Windows.Forms.Padding(2);
            this.UnidadMedida.Name = "UnidadMedida";
            this.UnidadMedida.Size = new System.Drawing.Size(145, 20);
            this.UnidadMedida.TabIndex = 4;
            // 
            // btnDeleteItem
            // 
            this.btnDeleteItem.Appearance = Syncfusion.Windows.Forms.ButtonAppearance.Metro;
            this.btnDeleteItem.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(219)))), ((int)(((byte)(94)))), ((int)(((byte)(94)))));
            this.btnDeleteItem.BeforeTouchSize = new System.Drawing.Size(94, 28);
            this.btnDeleteItem.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnDeleteItem.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnDeleteItem.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnDeleteItem.IsBackStageButton = false;
            this.btnDeleteItem.Location = new System.Drawing.Point(206, 12);
            this.btnDeleteItem.Name = "btnDeleteItem";
            this.btnDeleteItem.Size = new System.Drawing.Size(94, 28);
            this.btnDeleteItem.TabIndex = 2;
            this.btnDeleteItem.Text = "Eliminar";
            this.btnDeleteItem.Click += new System.EventHandler(this.btnDeleteItem_Click);
            // 
            // buttonAdv1
            // 
            this.buttonAdv1.Appearance = Syncfusion.Windows.Forms.ButtonAppearance.Metro;
            this.buttonAdv1.BackColor = System.Drawing.Color.SteelBlue;
            this.buttonAdv1.BeforeTouchSize = new System.Drawing.Size(91, 28);
            this.buttonAdv1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonAdv1.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonAdv1.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.buttonAdv1.IsBackStageButton = false;
            this.buttonAdv1.Location = new System.Drawing.Point(12, 12);
            this.buttonAdv1.Name = "buttonAdv1";
            this.buttonAdv1.Size = new System.Drawing.Size(91, 28);
            this.buttonAdv1.TabIndex = 0;
            this.buttonAdv1.Text = "Nuevo";
            this.buttonAdv1.Click += new System.EventHandler(this.buttonAdv1_Click);
            // 
            // btnAddItem
            // 
            this.btnAddItem.Appearance = Syncfusion.Windows.Forms.ButtonAppearance.Metro;
            this.btnAddItem.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(153)))), ((int)(((byte)(181)))), ((int)(((byte)(105)))));
            this.btnAddItem.BeforeTouchSize = new System.Drawing.Size(91, 28);
            this.btnAddItem.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnAddItem.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAddItem.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnAddItem.IsBackStageButton = false;
            this.btnAddItem.Location = new System.Drawing.Point(109, 12);
            this.btnAddItem.Name = "btnAddItem";
            this.btnAddItem.Size = new System.Drawing.Size(91, 28);
            this.btnAddItem.TabIndex = 1;
            this.btnAddItem.Text = "Guardar";
            this.btnAddItem.Click += new System.EventHandler(this.btnAddItem_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(12, 76);
            this.label3.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(82, 13);
            this.label3.TabIndex = 5;
            this.label3.Text = "Unidad del SAT";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 124);
            this.label1.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(124, 13);
            this.label1.TabIndex = 5;
            this.label1.Text = "Factor de conversión CE";
            // 
            // CE
            // 
            this.CE.Location = new System.Drawing.Point(155, 97);
            this.CE.Margin = new System.Windows.Forms.Padding(2);
            this.CE.Name = "CE";
            this.CE.Size = new System.Drawing.Size(145, 20);
            this.CE.TabIndex = 4;
            // 
            // FactorCE
            // 
            this.FactorCE.Location = new System.Drawing.Point(155, 121);
            this.FactorCE.Margin = new System.Windows.Forms.Padding(2);
            this.FactorCE.Name = "FactorCE";
            this.FactorCE.Size = new System.Drawing.Size(145, 20);
            this.FactorCE.TabIndex = 6;
            this.FactorCE.Text = "1";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(12, 100);
            this.label2.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(140, 13);
            this.label2.TabIndex = 3;
            this.label2.Text = "Unidad de Complemento CE";
            // 
            // frmConversinUMSingle
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(316, 150);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.FactorCE);
            this.Controls.Add(this.SAT);
            this.Controls.Add(this.CE);
            this.Controls.Add(this.UnidadMedida);
            this.Controls.Add(this.btnDeleteItem);
            this.Controls.Add(this.buttonAdv1);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.btnAddItem);
            this.Controls.Add(this.label3);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MetroColor = System.Drawing.Color.Transparent;
            this.Name = "frmConversinUMSingle";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Conversión de UM";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.frmConversinUMSingle_FormClosed);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox SAT;
        private System.Windows.Forms.TextBox UnidadMedida;
        private Syncfusion.Windows.Forms.ButtonAdv btnDeleteItem;
        private Syncfusion.Windows.Forms.ButtonAdv buttonAdv1;
        private Syncfusion.Windows.Forms.ButtonAdv btnAddItem;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox CE;
        private System.Windows.Forms.TextBox FactorCE;
        private System.Windows.Forms.Label label2;
    }
}