#region Copyright Syncfusion Inc. 2001 - 2017
// Copyright Syncfusion Inc. 2001 - 2017. All rights reserved.
// Use of this code is subject to the terms of our license.
// A copy of the current license can be obtained at any time by e-mailing
// licensing@syncfusion.com. Any infringement will be prosecuted under
// applicable laws. 
#endregion
using Generales;
using ModeloCartaPorte;
using OfficeOpenXml;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.Entity.Validation;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace ClasificadorProductos.formularios
{
    public partial class frmTransportistas : Syncfusion.Windows.Forms.MetroForm
    {
        public frmTransportistas()
        {
            InitializeComponent();
            Cargar();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Cargar();
        }

        private void Cargar()
        {
            try
            {
                ModeloCartaPorte.CartaPorteEntities Db = new ModeloCartaPorte.CartaPorteEntities();
                List <Clases.Transportista> Lista= Db.TransportistaSet
                                             .Where(a => a.Nombre.Contains(buscar.Text))
                                             .OrderBy(a => a.Nombre)
                                             .ToList()
                                             .Select(a => new Clases.Transportista
                              (
                                    a.Id
                                    , a.Tipos
                                    , a.RFC
                                    , a.NumeroLicencia
                                    , a.Nombre
                                    , a.Residencia
                                    , a.Domicilio
                                    , a.Estado
                                    , a.Pais
                                    , a.CodigoPostal
                              ))
                          .ToList();

                Dtg.Rows.Clear();
                foreach (Clases.Transportista registro in Lista)
                {
                    if (registro != null)
                    {
                        int n = Dtg.Rows.Add();
                        Dtg.Rows[n].Tag = registro.Id;
                        Dtg.Rows[n].Cells["RFC"].Value = registro.RFC;
                        Dtg.Rows[n].Cells["Nombre"].Value = registro.Nombre;
                    }
                }
                Db.Dispose();
                this.Dtg.Columns[0].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
            }
            catch (DbEntityValidationException error)
            {
                foreach (var eve in error.EntityValidationErrors)
                {
                    foreach (var ve in eve.ValidationErrors)
                    {
                        MessageBox.Show(this, ve.PropertyName + " Error: " + ve.ErrorMessage, Application.ProductName
                            , MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
            }
        }

        private void Importar()
        {
            Application.DoEvents();
            OpenFileDialog openFileDialog1 = new OpenFileDialog();
            openFileDialog1.Filter = "Excel files (*.xlsx)|*.xlsx|All files (*.*)|*.*";
            openFileDialog1.FilterIndex = 1;
            openFileDialog1.RestoreDirectory = true;

            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                try
                {
                    Procesar(Dtg, 1, openFileDialog1.FileName);
                }
                catch (Exception)
                {
                    MessageBox.Show("No existe el Archivo seleccionado", Application.ProductName + "-" + Application.ProductVersion, MessageBoxButtons.OK);
                }
                finally
                {
                    Application.DoEvents();
                }
            }
        }

        private void Procesar(DataGridView dtg, int hoja, string archivo)
        {
            string tmpFile = Path.GetTempFileName();
            try
            {
                if (!File.Exists(archivo))
                {
                    MessageBox.Show(this, "El archivo " + archivo + " no existe.", Application.ProductName
                        , MessageBoxButtons.OK, MessageBoxIcon.Error);
                }

                File.Delete(tmpFile);
                File.Copy(archivo, tmpFile);

                dtg.Rows.Clear();
                int rowIndex = 2;
                FileInfo existingFile = new FileInfo(tmpFile);
                using (ExcelPackage package = new ExcelPackage(existingFile))
                {
                    ExcelWorksheet worksheet = package.Workbook.Worksheets["Transportista"];
                    while (worksheet.Cells[rowIndex, 2].Value != null)
                    {
                        try
                        {
                            Clases.Transportista O = new Clases.Transportista();
                            O.Tipos= worksheet.Cells[rowIndex, 1].Text;
                            O.RFC = worksheet.Cells[rowIndex, 2].Text;
                            O.NumeroLicencia = worksheet.Cells[rowIndex, 3].Text;
                            O.Nombre = worksheet.Cells[rowIndex, 4].Text;                            
                            O.Residencia = worksheet.Cells[rowIndex, 5].Text;
                            O.Domicilio = worksheet.Cells[rowIndex, 6].Text;
                            O.Estado = worksheet.Cells[rowIndex, 7].Text;
                            O.Pais = worksheet.Cells[rowIndex, 8].Text;
                            O.CodigoPostal = worksheet.Cells[rowIndex, 9].Text;

                            Guardar(O);
                        }
                        catch (DbEntityValidationException e)
                        {
                            foreach (var eve in e.EntityValidationErrors)
                            {
                                foreach (var ve in eve.ValidationErrors)
                                {
                                    MessageBox.Show(this, ve.PropertyName + " Error: " + ve.ErrorMessage, Application.ProductName
                                        , MessageBoxButtons.OK, MessageBoxIcon.Error);
                                }
                            }
                        }
                        rowIndex++;
                    }
                }
            }
            catch (Exception e)
            {
                ErrorFX.mostrar(e, true, false,
                "frmTransportistas - 150 - "
                );
            }
            finally
            {
                File.Delete(tmpFile);
                Cargar();
            }

        }

        private void Guardar(Clases.Transportista Registro)
        {
            ModeloCartaPorte.CartaPorteEntities Db = new ModeloCartaPorte.CartaPorteEntities();
            try
            {
                ModeloCartaPorte.Transportista O = new ModeloCartaPorte.Transportista();

                O = (from r in Db.TransportistaSet.Where
                                        (a => a.RFC == Registro.RFC)
                            select r).FirstOrDefault();
                if (O == null)
                {
                    O = new ModeloCartaPorte.Transportista();
                    O.Tipos=Registro.Tipos;
                    O.RFC = Registro.RFC;
                    O.NumeroLicencia = Registro.NumeroLicencia;
                    O.Nombre = Registro.Nombre;                    
                    O.Residencia = Registro.Residencia;
                    O.Domicilio = Registro.Domicilio;
                    O.Estado = Registro.Estado;
                    O.Pais = Registro.Pais;
                    O.CodigoPostal = Registro.CodigoPostal;

                    Db.TransportistaSet.Add(O);
                    Db.SaveChanges();
                }
            }
            catch (DbEntityValidationException e)
            {
                foreach (var eve in e.EntityValidationErrors)
                {
                    foreach (var ve in eve.ValidationErrors)
                    {
                        MessageBox.Show(this, ve.PropertyName + " Error: " + ve.ErrorMessage, Application.ProductName
                            , MessageBoxButtons.OK, MessageBoxIcon.Error);
                        return;
                    }
                }
            }
        }

        private void dtg_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            Cargar_registro(e);
        }

        private void Cargar_registro(DataGridViewCellEventArgs e)
        {
            if (e.RowIndex > -1)
            {
                //Clases.Transportista Registro = (Clases.Transportista)Dtg.Rows[e.RowIndex].Tag;
                //frmTransportista o = new frmTransportista(Registro);
                //o.ShowDialog();
                //Cargar();
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            Importar();
        }

        private void button7_Click(object sender, EventArgs e)
        {
            GenerarPlantilla();
        }

        private void GenerarPlantilla()
        {
            try
            {
                var fileName = " Plantilla de Transportista " + DateTime.Now.ToString("yyyy-MM-dd-hh-mm-ss") + ".xlsx";
                if (folderBrowserDialog1.ShowDialog() == DialogResult.OK)
                {
                    fileName = folderBrowserDialog1.SelectedPath + "\\" + fileName;
                    System.IO.Stream myStream = new System.IO.FileStream(fileName, System.IO.FileMode.Create, System.IO.FileAccess.ReadWrite, System.IO.FileShare.None);

                    using (ExcelPackage pck = new ExcelPackage())
                    {

                        ExcelWorksheet ws1 = pck.Workbook.Worksheets.Add("Transportista");
                        ws1.Cells[1, 1].Value = "Tipos Figura ";
                        ws1.Cells[1, 2].Value = "RFC figura";
                        ws1.Cells[1, 3].Value = "N�mero de licencia";
                        ws1.Cells[1, 4].Value = "Nombre figura";
                        ws1.Cells[1, 5].Value = "N�mero de registro de identidad tributaria";
                        ws1.Cells[1, 6].Value = "Residencia Fiscal de la figura";
                        ws1.Cells[1, 7].Value = "Domicilio";
                        ws1.Cells[1, 8].Value = "Estado";
                        ws1.Cells[1, 9].Value = "Pa�s";
                        ws1.Cells[1, 10].Value = "C�digo Postal";

                        pck.SaveAs(myStream);
                    }
                    myStream.Close();
                    MessageBox.Show("Creaci�n de plantilla realizada " + Environment.NewLine + fileName + " finalizada.", Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
            catch(Exception e)
            {
                ErrorFX.mostrar(e,true,false,
                "frmTransportistas - 264 - "
                    );
            }
        }

        private void dtg_CellDoubleClick_1(object sender, DataGridViewCellEventArgs e)
        {
            Cargar_registro(e);
        }

        private void button2_Click_1(object sender, EventArgs e)
        {
            Eliminar();
        }

        private void Eliminar()
        {
            if (Dtg.SelectedRows.Count==0)
            {
                MessageBox.Show(this, "Seleccione los registros", Application.ProductName + "-" + Application.ProductVersion, MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
            DialogResult Resultado = MessageBox.Show(this, "�Esta seguro de eliminar?", Application.ProductName + "-" + Application.ProductVersion, MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            if (Resultado!= DialogResult.Yes)
            {
                return;
            }
            ModeloCartaPorte.CartaPorteEntities Db = new ModeloCartaPorte.CartaPorteEntities();
            foreach (DataGridViewRow Row in Dtg.SelectedRows)
            {
                int IdTr = (int)Row.Tag;
                Transportista Registro = Db.TransportistaSet
                                             .Where(a => a.Id.Equals(IdTr))
                                             .FirstOrDefault();

                Db.TransportistaSet.Attach(Registro);
                Db.TransportistaSet.Remove(Registro);
                Db.SaveChanges();
            }
            
            MessageBox.Show(this, "Eliminado.", Application.ProductName + "-" + Application.ProductVersion, MessageBoxButtons.OK, MessageBoxIcon.Information);
            Cargar();
        }
    }
}
