#region Copyright Syncfusion Inc. 2001 - 2017
// Copyright Syncfusion Inc. 2001 - 2017. All rights reserved.
// Use of this code is subject to the terms of our license.
// A copy of the current license can be obtained at any time by e-mailing
// licensing@syncfusion.com. Any infringement will be prosecuted under
// applicable laws. 
#endregion
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;

using System.Text;
using System.Windows.Forms;
using ModeloDocumentosFX;
using System.Text;
using System.Windows.Forms;
using System.Linq;
using System.Data.Entity.Validation;
using Generales;

namespace ClasificadorProductos.formularios
{


    public partial class frmTipoRelacionNDCfactura : Form //Syncfusion.Windows.Forms.MetroForm
    {
        EMPRESA oEMPRESA;

        string ENTITY_IDtr;
        string INVOICE_IDtr;
        string CUSTOMER_IDtr;
        string CUSTOMER_NAMEtr;

        cCONEXCION oData;
        bool modificado = false;
        //public frmTipoRelacionNDCfactura(cEMPRESA ocEMPRESAp)
        //{
        //    ocEMPRESA = ocEMPRESAp;
        //    InitializeComponent();
        //    cargarTipos();
        //    limpiar();
        //}

        public frmTipoRelacionNDCfactura(string ENTITY_IDp,string INVOICE_IDp, string CUSTOMER_IDp)
        {
            ENTITY_IDtr = ENTITY_IDp;
            INVOICE_IDtr = INVOICE_IDp;
            CUSTOMER_IDtr = CUSTOMER_IDp;

            InitializeComponent();

            cargarGeneral(CUSTOMER_IDp);
        }

        private void cargarGeneral(string CUSTOMER_IDp)
        {
            //Cargar datosd la empresa y la conexi�n
            ModeloDocumentosFXContainer dbContext = new ModeloDocumentosFXContainer();
            try
            {
                //Cargar el primer registro de configuraci�n de usuarios
                oEMPRESA = (from r in dbContext.EMPRESA.Where(b => b.ENTITY_ID == ENTITY_IDtr)
                            select r).FirstOrDefault();
                if (oEMPRESA == null)
                {
                    ErrorFX.mostrar("La empresa con entidad " + ENTITY_IDtr + " no existe, no puede cargar los datos", true, true, true);
                }

                //Creac la conexi�n a base de datos
                oData = new cCONEXCION("SQLSERVER", oEMPRESA.SERVIDOR, oEMPRESA.BD, oEMPRESA.USUARIO_BD, cENCRIPTACION.Decrypt(oEMPRESA.PASSWORD_BD));
                oData.CrearConexion();
            }
            catch (Exception err)
            {
                ErrorFX.mostrar(err, false, true, "frmTipoRelacionNDCfactura - 66 - ");
            }
            cargarTipos();
            //limpiar();
            invoice_id.Enabled = false;
            invoice_id.Text = INVOICE_IDtr;
            cliente.Text = CUSTOMER_IDp;
            cliente.Enabled = false;
            cargar();
        }

        //public frmTipoRelacionNDCfactura(cEMPRESA ocEMPRESAp, string INVOICE_IDp, string CUSTOMER_IDp, string CUSTOMER_NAME)
        //{
        //    ocEMPRESA = ocEMPRESAp;
        //    CUSTOMER_ID = CUSTOMER_IDp;
        //    InitializeComponent();

        //    cargarTipos();

        //    limpiar();
        //    invoice_id.Enabled = false;
        //    invoice_id.Text = INVOICE_IDp;
        //    cliente.Text=CUSTOMER_NAME;
        //    cliente.Enabled = false;

        //}

        private void cargarTipos()
        {
            tipoRelacion.Items.Add(new ComboItem { Name = "Nota de cr�dito de los documentos relacionados", Value = "01" });
            tipoRelacion.Items.Add(new ComboItem { Name = "Nota de d�bito de los documentos relacionados", Value = "02" });
            tipoRelacion.Items.Add(new ComboItem { Name = "Devoluci�n de mercanc�a sobre facturas o traslados previos", Value = "03" });
            tipoRelacion.Items.Add(new ComboItem { Name = "Sustituci�n de los CFDI previos", Value = "04" });
            tipoRelacion.Items.Add(new ComboItem { Name = "Traslados de mercancias facturados previamente", Value = "05" });
            tipoRelacion.Items.Add(new ComboItem { Name = "Factura generada por los traslados previos", Value = "06" });
            tipoRelacion.Items.Add(new ComboItem { Name = "CFDI por aplicaci�n de anticipo", Value = "07" });
            tipoRelacion.SelectedIndex = 0;
        }
        #region Metodos
        private void cargar()
        {
            //Cargar los datos
            ModeloDocumentosFXContainer dbContext = new ModeloDocumentosFXContainer();
            try
            {
                dtg.Rows.Clear();
                var result = from b in dbContext.ndcFacturaSet
                                         .Where(b => b.INVOICE_ID == INVOICE_IDtr)
                             select b
                                         ;
                if (result != null)
                {
                    List<ndcFactura> dt = result.ToList();
                    bool tipoCargado = false;
                    foreach (ndcFactura registro in dt)
                    {
                        int n = dtg.Rows.Add();
                        //Cargar el tipo
                        if (!tipoCargado)
                        {
                            foreach (ComboItem cbi in tipoRelacion.Items)
                            {
                                if (cbi.Value as String == registro.tipoRelacion)
                                {
                                    this.tipoRelacion.SelectedItem = cbi;
                                    break;
                                }
                            }
                            tipoCargado = true;
                        }

                        dtg.Rows[n].Cells["factura"].Value = registro.INVOICE_ID_relacionada;
                        dtg.Rows[n].Cells["UUID"].Value = registro.UUID.ToString();

                    }
                }
            }
            catch (Exception err)
            {
                ErrorFX.mostrar(err, false, true, "frmTipoRelacionNDCfactura - 117 - ", false);
            }

        }

        public void limpiar()
        {
            if (modificado)
            {

                DialogResult Resultado = MessageBox.Show(this, "�Desea guardar los cambios?", Application.ProductName + "-" + Application.ProductVersion, MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                if (Resultado.ToString() == "Yes")
                {
                    guardar();
                }
            }
            modificado = false;
            foreach (var c in this.Controls)
            {
                if (c is TextBox)
                {
                    ((TextBox)c).Text = "";
                }
            }
            dtg.Rows.Clear();
        }
        private void guardar()
        {
            ModeloDocumentosFX.ModeloDocumentosFXContainer dbContext = new ModeloDocumentosFXContainer();
            try
            {
                string tipoRelaciontr = (tipoRelacion.SelectedItem as ComboItem).Value.ToString();
                foreach (DataGridViewRow linea in dtg.Rows)
                {
                    if (!String.IsNullOrEmpty(linea.Cells["TipoRelacionFactura"].Value.ToString()))
                    {
                        tipoRelaciontr = linea.Cells["TipoRelacionFactura"].Value.ToString();
                    }
                    guardarLinea(tipoRelaciontr, linea);
                }
         
                MessageBox.Show(this, "Datos guardados", Application.ProductName + "-" + Application.ProductVersion, MessageBoxButtons.OK, MessageBoxIcon.Information);

                DialogResult = DialogResult.OK;

                
            }
            catch (Exception e)
            {
                ErrorFX.mostrar(e, true, true, "frmTipoRelacionNDCfactura - 90 ");
            }
        }

        private void guardarLinea(string tipoRelaciontr, DataGridViewRow linea)
        {
            ModeloDocumentosFX.ModeloDocumentosFXContainer dbContext = new ModeloDocumentosFXContainer();

            string conection = dbContext.Database.Connection.ConnectionString;
            cCONEXCION oData = new cCONEXCION(conection);
            eliminarLinea(linea, oData);

            string UUIDtr = linea.Cells["UUID"].Value.ToString();
            string INVOICE_ID_relacionada = linea.Cells["factura"].Value.ToString();
            ComboItem selected = (ComboItem)tipoRelacion.SelectedItem;

            string sSQL = @"INSERT INTO ndcFacturaSet (tipoRelacion,INVOICE_ID,INVOICE_ID_relacionada,UUID) VALUES 
            ('" + selected.Value + "','" + invoice_id.Text + "','" + INVOICE_ID_relacionada + "','" + UUIDtr + @"')";
            oData.EjecutarConsulta(sSQL);

        }
        private void guardarLineaProducto(DataGridViewRow linea)
        {
            ModeloDocumentosFX.ModeloDocumentosFXContainer dbContext = new ModeloDocumentosFXContainer();

            string conection = dbContext.Database.Connection.ConnectionString;
            cCONEXCION oData = new cCONEXCION(conection);
            eliminarLineaProducto(linea, oData);

            string LINE = linea.Cells["LINE"].Value.ToString();
            string invoice_id_relacionado = linea.Cells["invoice_id_relacionado"].Value.ToString();
            string invoice_id_relacionado_linea = linea.Cells["invoice_id_relacionado_linea"].Value.ToString();
            string PART_IDtr = linea.Cells["PART_ID"].Value.ToString();
            string clasificacion = linea.Cells["clasificacion"].Value.ToString();
            
            string sSQL = @"INSERT INTO ndcFacturaLineaSet (INVOICE_ID,LINE_NO,invoice_id_relacionado,invoice_id_relacionado_linea, PART_ID, clasificacion) 
            VALUES 
            ('" + invoice_id.Text + "','" + LINE + "','" + invoice_id_relacionado + "','" + invoice_id_relacionado_linea + "','" + PART_IDtr + "','" + clasificacion + "')";
            oData.EjecutarConsulta(sSQL);

        }

        private void eliminarLineaProducto(DataGridViewRow linea, cCONEXCION oData)
        {
            string LINE = linea.Cells["LINE"].Value.ToString();
            string sSQL = "DELETE FROM ndcFacturaLineaSet WHERE INVOICE_ID='" + invoice_id.Text + "' AND LINE_NO='" + LINE + "'";
            oData.EjecutarConsulta(sSQL);
        }

        private void eliminarLinea(DataGridViewRow linea, cCONEXCION oData)
        {
            string UUIDtr = linea.Cells["UUID"].Value.ToString();
            string sSQL = "DELETE FROM ndcFacturaSet WHERE INVOICE_ID='" + invoice_id.Text + "' AND UUID='" + UUIDtr + "'";
            oData.EjecutarConsulta(sSQL);
        }

        private void eliminar()
        {
            if(invoice_id.Text=="")
            {
                return;
            }
            DialogResult Resultado = MessageBox.Show(this, "�Esta seguro de eliminar?", Application.ProductName + "-" + Application.ProductVersion, MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            if (Resultado.ToString() != "Yes")
            {
                return;
            }
            ModeloDocumentosFX.ModeloDocumentosFXContainer dbContext = new ModeloDocumentosFXContainer();

            string conection = dbContext.Database.Connection.ConnectionString;
            cCONEXCION oData = new cCONEXCION(conection);

            string sSQL = "DELETE FROM ndcFacturaSet WHERE INVOICE_ID='" + invoice_id.Text + "'";
            oData.EjecutarConsulta(sSQL);

            MessageBox.Show(this, "Eliminado.", Application.ProductName + "-" + Application.ProductVersion, MessageBoxButtons.OK, MessageBoxIcon.Information);
            limpiar();
        }


        #endregion

        private void buttonAdv1_Click(object sender, EventArgs e)
        {
            limpiar();
        }

        private void btnAddItem_Click(object sender, EventArgs e)
        {
           guardar();
        }

        private void btnDeleteItem_Click(object sender, EventArgs e)
        {
            eliminar();
        }

        private void frmTipoRelacionNDCfactura_FormClosed(object sender, FormClosedEventArgs e)
        {
            DialogResult = DialogResult.Cancel;
        }

        private void buttonAdv3_Click(object sender, EventArgs e)
        {
            eliminarLinea();
        }

        private void eliminarLinea()
        {
            dtg.EndEdit();
            if (dtg.SelectedCells.Count > 0)
            {
                DialogResult Resultado = MessageBox.Show("�Desea eliminar las seleccionada?", Application.ProductName + "-" + Application.ProductVersion, MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                if (Resultado.ToString() != "Yes")
                {
                    return;
                }
                ModeloDocumentosFX.ModeloDocumentosFXContainer dbContext = new ModeloDocumentosFXContainer();
                string conection = dbContext.Database.Connection.ConnectionString;
                cCONEXCION oData = new cCONEXCION(conection);
                foreach (DataGridViewRow drv in dtg.SelectedRows)
                {
                    try
                    {
                        eliminarLinea(drv, oData);
                        //Limpiar lineas que tengan de productos asociadas
                        string invoice_id = drv.Cells["factura"].Value.ToString();
                        dtg.Rows.Remove(drv);
                    }
                    catch (Exception e)
                    {
                        Bitacora.Log(e.Message.ToString());
                    }

                }
            }
            else
            {
                MessageBox.Show("Seleccione alguna l�nea.", Application.ProductName + "-" + Application.ProductVersion, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }


        private void buttonAdv2_Click(object sender, EventArgs e)
        {
            cargarFacturas();
        }

        private void cargarFacturas()
        {
            cEMPRESA oEMPRESA = new cEMPRESA();
            oEMPRESA.cargarEntity(ENTITY_IDtr);
            string bdAuxiliar = "";
            if (oEMPRESA!=null)
            {
                if (!String.IsNullOrEmpty(oEMPRESA.BD_AUXILIAR))
                {
                    bdAuxiliar = oEMPRESA.BD_AUXILIAR;
                }
            }

            frmFacturas o = new frmFacturas(oData,CUSTOMER_IDtr, bdAuxiliar);
            if (o.ShowDialog() == DialogResult.OK)
            {
                foreach (DataGridViewRow linea in o.selecionados)
                {
                    int n = dtg.Rows.Add();
                    dtg.Rows[n].Cells["factura"].Value = linea.Cells["invoice_id"].Value.ToString();
                    dtg.Rows[n].Cells["UUID"].Value = linea.Cells["UUID"].Value.ToString();
                    dtg.Rows[n].Cells["TipoRelacionFactura"].Value = string.Empty;
                    try
                    {
                        string tipoRelaciontr = (tipoRelacion.SelectedItem as ComboItem).Value.ToString();
                        dtg.Rows[n].Cells["TipoRelacionFactura"].Value = tipoRelaciontr;
                    }
                    catch
                    {

                    }
                    

                    
                    
                }
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            AgregarManualmente();
        }

        private void AgregarManualmente()
        {
            frmFacturaManual OfrmFacturaManual = new frmFacturaManual();
            if (OfrmFacturaManual.ShowDialog()==DialogResult.OK)
            {
                int n = dtg.Rows.Add();
                dtg.Rows[n].Cells["factura"].Value = OfrmFacturaManual.FacturaTr;
                dtg.Rows[n].Cells["UUID"].Value = OfrmFacturaManual.UUIDTr;
                dtg.Rows[n].Cells["TipoRelacionFactura"].Value = string.Empty;
                try
                {
                    string tipoRelaciontr = (tipoRelacion.SelectedItem as ComboItem).Value.ToString();
                    dtg.Rows[n].Cells["TipoRelacionFactura"].Value = tipoRelaciontr;
                }
                catch
                {

                }
            }
        }
    }
}
