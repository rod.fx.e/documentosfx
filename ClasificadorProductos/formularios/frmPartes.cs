#region Copyright Syncfusion Inc. 2001 - 2017
// Copyright Syncfusion Inc. 2001 - 2017. All rights reserved.
// Use of this code is subject to the terms of our license.
// A copy of the current license can be obtained at any time by e-mailing
// licensing@syncfusion.com. Any infringement will be prosecuted under
// applicable laws. 
#endregion
using Generales;
using ModeloDocumentosFX;
using OfficeOpenXml;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.Entity.Validation;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace ClasificadorProductos.formularios
{
    public partial class frmPartes : Syncfusion.Windows.Forms.MetroForm
    {
        public frmPartes()
        {
            InitializeComponent();
            cargar();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            cargar();
        }

        private void cargar()
        {
            try
            {
                ModeloDocumentosFX.ModeloDocumentosFXContainer dbContext = new ModeloDocumentosFXContainer();
                var result = from b in dbContext.configuracionPartesSet
                                             .Where(a => a.producto.Contains(buscar.Text))
                                             .OrderBy(a => a.producto)
                             select b;

                List<configuracionPartes> dt = result.ToList();
                Dtg.Rows.Clear();
                foreach (configuracionPartes registro in dt)
                {
                    if (registro != null)
                    {
                        int n = Dtg.Rows.Add();
                        Dtg.Rows[n].Tag = registro;
                        Dtg.Rows[n].Cells["producto"].Value = registro.producto;
                        Dtg.Rows[n].Cells["parte"].Value = registro.parte;
                        Dtg.Rows[n].Cells["NoIdentificacion"].Value = registro.NoIdentificacion;
                        Dtg.Rows[n].Cells["ValorUnitario"].Value = registro.ValorUnitario;
                        Dtg.Rows[n].Cells["ClaveProdServ"].Value = registro.ClaveProdServ;
                        Dtg.Rows[n].Cells["Cantidad"].Value = registro.Cantidad;
                        Dtg.Rows[n].Cells["Unidad"].Value = registro.Unidad;
                        Dtg.Rows[n].Cells["ClaveUnidad"].Value = registro.ClaveUnidad;
                        Dtg.Rows[n].Cells["Importe"].Value = registro.Importe;
                        Dtg.Rows[n].Cells["Descripcion"].Value = registro.Descripcion;
                    }
                }
                dbContext.Dispose();
                this.Dtg.Columns[0].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
            }
            catch (DbEntityValidationException error)
            {
                foreach (var eve in error.EntityValidationErrors)
                {
                    foreach (var ve in eve.ValidationErrors)
                    {
                        MessageBox.Show(this, ve.PropertyName + " Error: " + ve.ErrorMessage, Application.ProductName
                            , MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
            }
            catch (Exception Error)
            {
                ErrorFX.mostrar(Error, true, true, "frmPartes - 80 - ", true);
            }
        
        }

        private void importar()
        {
            Application.DoEvents();
            OpenFileDialog openFileDialog1 = new OpenFileDialog();
            openFileDialog1.Filter = "Excel files (*.xlsx)|*.xlsx|All files (*.*)|*.*";
            openFileDialog1.FilterIndex = 1;
            openFileDialog1.RestoreDirectory = true;

            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                try
                {
                    procesar(Dtg, 1, openFileDialog1.FileName);
                }
                catch (Exception)
                {
                    MessageBox.Show("No existe el Archivo seleccionado", Application.ProductName + "-" + Application.ProductVersion, MessageBoxButtons.OK);
                }
                finally
                {
                    Application.DoEvents();
                }
            }
        }

        private void procesar(DataGridView dtg, int hoja, string archivo)
        {
            string tmpFile = Path.GetTempFileName();
            try
            {
                if (!File.Exists(archivo))
                {
                    MessageBox.Show(this, "El archivo " + archivo + " no existe.", Application.ProductName
                        , MessageBoxButtons.OK, MessageBoxIcon.Error);
                }

                File.Delete(tmpFile);
                File.Copy(archivo, tmpFile);
                ModeloDocumentosFX.ModeloDocumentosFXContainer dbContext = new ModeloDocumentosFXContainer();
                
                int rowIndex = 2;
                FileInfo existingFile = new FileInfo(tmpFile);
                using (ExcelPackage package = new ExcelPackage(existingFile))
                {
                    ExcelWorksheet worksheet = package.Workbook.Worksheets[hoja];
                    while (worksheet.Cells[rowIndex, 2].Value != null)
                    {
                        try
                        {
                            string ProductoTr = worksheet.Cells[rowIndex, 1].Text;
                            string ParteTr = worksheet.Cells[rowIndex, 2].Text;
                            string NoIdentificacionTr = worksheet.Cells[rowIndex, 3].Text;
                            string ValorUnitarioTr = worksheet.Cells[rowIndex, 4].Text;
                            string ClaveProdServTr = worksheet.Cells[rowIndex, 5].Text;
                            string CantidadTr = worksheet.Cells[rowIndex, 6].Text;
                            string UnidadTr = worksheet.Cells[rowIndex, 7].Text;
                            string ClaveUnidadTr = worksheet.Cells[rowIndex, 8].Text;
                            string ImporteTr = worksheet.Cells[rowIndex, 9].Text;
                            string DescripcionTr = worksheet.Cells[rowIndex, 10].Text;

                            configuracionPartes RegistroTmp = new configuracionPartes();                            
                            RegistroTmp.producto = ProductoTr;
                            RegistroTmp.parte = ParteTr;
                            RegistroTmp.NoIdentificacion = NoIdentificacionTr;
                            RegistroTmp.Cantidad = decimal.Parse(CantidadTr);
                            RegistroTmp.ValorUnitario = decimal.Parse(ValorUnitarioTr);
                            RegistroTmp.Importe = decimal.Parse(ImporteTr);
                            RegistroTmp.Unidad = UnidadTr;
                            RegistroTmp.Descripcion = DescripcionTr;
                            RegistroTmp.ClaveProdServ = ClaveProdServTr;
                            RegistroTmp.fechaCreacion = DateTime.Now;                            
                            RegistroTmp.ClaveUnidad = ClaveUnidadTr;
                            dbContext.configuracionPartesSet.Add(RegistroTmp);
                            dbContext.SaveChanges();
                        }
                        catch (DbEntityValidationException e)
                        {
                            foreach (var eve in e.EntityValidationErrors)
                            {
                                foreach (var ve in eve.ValidationErrors)
                                {
                                    MessageBox.Show(this, ve.PropertyName + " Error: " + ve.ErrorMessage, Application.ProductName
                                        , MessageBoxButtons.OK, MessageBoxIcon.Error);
                                }
                            }
                        }
                        catch (Exception e)
                        {
                            ErrorFX.mostrar(e, true, false,
                            "frmPartes - 177 - "
                                );
                        }
                        rowIndex++;
                    }
                }
            }
            catch (Exception e)
            {
                ErrorFX.mostrar(e, true, false,
                "frmPartes - 150 - "
                );
            }
            finally
            {
                File.Delete(tmpFile);
                cargar();
            }

        }

        private void dtg_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            cargar_registro(e);
        }

        private void cargar_registro(DataGridViewCellEventArgs e)
        {
            if (e.RowIndex > -1)
            {
                configuracionPartes registro = (configuracionPartes)Dtg.Rows[e.RowIndex].Tag;
                frmParte o = new frmParte(registro);
                o.ShowDialog();
                cargar();
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            importar();
        }

        private void button7_Click(object sender, EventArgs e)
        {
            plantillaProductCode();
        }

        private void plantillaProductCode()
        {
            try
            {
                MessageBox.Show("La configuraci�n de  la Plantilla para migrar Partes " + Environment.NewLine
                + "Fila 1 - Encabezado" + Environment.NewLine
                + "Columna 1: Producto " + Environment.NewLine
                + "Columna 2: Parte " + Environment.NewLine
                + "Columna 3: NoIdentificacion " + Environment.NewLine
                + "Columna 4: ValorUnitario " + Environment.NewLine
                + "Columna 5: ClaveProdServ " + Environment.NewLine
                + "Columna 6: Cantidad " + Environment.NewLine
                + "Columna 7: Unidad " + Environment.NewLine
                + "Columna 8: ClaveUnidad " + Environment.NewLine
                + "Columna 9: Importe " + Environment.NewLine
                + "Columna 10: Descripci�n " + Environment.NewLine
                + "Si no tiene esta configuraci�n no ser� cargada la informaci�n. "
                , Application.ProductName + "-" + Application.ProductVersion.ToString()
                , MessageBoxButtons.OK
                );

                var fileName = " Plantilla de Kits " + DateTime.Now.ToString("yyyy-MM-dd-hh-mm-ss") + ".xlsx";
                if (folderBrowserDialog1.ShowDialog() == DialogResult.OK)
                {
                    fileName = folderBrowserDialog1.SelectedPath + "\\" + fileName;
                    System.IO.Stream myStream = new System.IO.FileStream(fileName, System.IO.FileMode.Create, System.IO.FileAccess.ReadWrite, System.IO.FileShare.None);

                    using (ExcelPackage pck = new ExcelPackage())
                    {

                        ExcelWorksheet ws1 = pck.Workbook.Worksheets.Add("Plantilla");
                        ws1.Cells[1, 1].Value = "Producto";
                        ws1.Cells[1, 2].Value = "Parte";
                        ws1.Cells[1, 3].Value = "NoIdentificacion";
                        ws1.Cells[1, 4].Value = "ValorUnitario";
                        ws1.Cells[1, 5].Value = "ClaveProdServ";
                        ws1.Cells[1, 6].Value = "Cantidad";
                        ws1.Cells[1, 7].Value = "Unidad";
                        ws1.Cells[1, 8].Value = "ClaveUnidad";
                        ws1.Cells[1, 9].Value = "Importe";
                        ws1.Cells[1, 10].Value = "Descripcion";
                        pck.SaveAs(myStream);
                    }
                    myStream.Close();
                    MessageBox.Show("Creaci�n de plantilla realizada " + Environment.NewLine + fileName + " finalizada.", Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Information);

                    FileInfo fi = new FileInfo(fileName);
                    if (fi.Exists)
                    {
                        System.Diagnostics.Process.Start(fileName);
                    }

                }
            }
            catch(Exception e)
            {
                ErrorFX.mostrar(e,true,false,
                "frmPartes - 264 - "
                    );
            }
        }

        private void dtg_CellDoubleClick_1(object sender, DataGridViewCellEventArgs e)
        {
            cargar_registro(e);
        }

        private void buttonAdv2_Click(object sender, EventArgs e)
        {
            frmParte o = new frmParte();
            o.Show();
        }

        private void buttonAdv4_Click(object sender, EventArgs e)
        {
            EliminarLinea();
        }

        private void EliminarLinea()
        {
            try
            {
                Dtg.EndEdit();
                if (Dtg.SelectedCells.Count > 0)
                {
                    DialogResult Resultado = MessageBox.Show("�Esta seguro de eliminar la selecci�n?", Application.ProductName + "-" + Application.ProductVersion, MessageBoxButtons.YesNo);
                    if (Resultado.ToString() != "Yes")
                    {
                        return;
                    }

                    IEnumerable<DataGridViewRow> selectedRows = Dtg.SelectedCells.Cast<DataGridViewCell>()
                                           .Select(cell => cell.OwningRow)
                                           .Distinct();
                    foreach (DataGridViewRow Drv in selectedRows)
                    {
                        if (Drv.Tag!=null)
                        {
                            configuracionPartes RegistroTmp = (configuracionPartes)Drv.Tag;
                            ModeloDocumentosFX.ModeloDocumentosFXContainer dbContext = new ModeloDocumentosFXContainer();
                            dbContext.configuracionPartesSet.Attach(RegistroTmp);
                            dbContext.configuracionPartesSet.Remove(RegistroTmp);
                            dbContext.SaveChanges();
                        }
                        Dtg.Rows.Remove(Drv);
                    }

                }
            }
            catch (Exception e)
            {
                ErrorFX.mostrar(e, true, true, "frmRequisicion - 187 ");
            }
        }

        private void buscar_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)13)
            {
                cargar();
            }
        }
    }
}
