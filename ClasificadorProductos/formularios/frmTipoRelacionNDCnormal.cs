#region Copyright Syncfusion Inc. 2001 - 2017
// Copyright Syncfusion Inc. 2001 - 2017. All rights reserved.
// Use of this code is subject to the terms of our license.
// A copy of the current license can be obtained at any time by e-mailing
// licensing@syncfusion.com. Any infringement will be prosecuted under
// applicable laws. 
#endregion
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;

using System.Text;
using System.Windows.Forms;
using ModeloDocumentosFX;
using System.Text;
using System.Windows.Forms;
using System.Linq;
using System.Data.Entity.Validation;
using Generales;

namespace ClasificadorProductos.formularios
{


    public partial class frmTipoRelacionNDCnormal : Form //Syncfusion.Windows.Forms.MetroForm
    {
        EMPRESA oEMPRESA;

        string ENTITY_IDtr;
        string INVOICE_IDtr;
        string CUSTOMER_IDtr;
        string CUSTOMER_NAMEtr;

        cCONEXCION oData;
        bool modificado = false;
        //public frmTipoRelacionNDCnormal(cEMPRESA ocEMPRESAp)
        //{
        //    ocEMPRESA = ocEMPRESAp;
        //    InitializeComponent();
        //    cargarTipos();
        //    limpiar();
        //}
        public frmTipoRelacionNDCnormal(string ENTITY_IDp,string INVOICE_IDp, string CUSTOMER_IDp)
        {
            ENTITY_IDtr = ENTITY_IDp;
            INVOICE_IDtr = INVOICE_IDp;
            CUSTOMER_IDtr = CUSTOMER_IDp;
            
            InitializeComponent();

            //Cargar datosd la empresa y la conexi�n
            ModeloDocumentosFXContainer dbContext = new ModeloDocumentosFXContainer();
            try
            {
                //Cargar el primer registro de configuraci�n de usuarios
                oEMPRESA = (from r in dbContext.EMPRESA.Where(b => b.ENTITY_ID==ENTITY_IDtr)
                                 select r).FirstOrDefault();
                if (oEMPRESA==null)
                {
                    ErrorFX.mostrar("La empresa con entidad " + ENTITY_IDtr + " no existe, no puede cargar los datos", true, true, true);
                }

                //Creac la conexi�n a base de datos
                oData = new cCONEXCION("SQLSERVER", oEMPRESA.SERVIDOR, oEMPRESA.BD, oEMPRESA.USUARIO_BD, cENCRIPTACION.Decrypt(oEMPRESA.PASSWORD_BD));
                oData.CrearConexion();
            }
            catch (Exception err)
            {
                ErrorFX.mostrar(err, false, true, "frmTipoRelacionNDCnormal - 66 - ");
            }
            cargarTipos();
            //limpiar();
            invoice_id.Enabled = false;
            invoice_id.Text = INVOICE_IDtr;
            cliente.Text = CUSTOMER_IDp;
            cliente.Enabled = false;
            cargar();
        }

        //public frmTipoRelacionNDCnormal(cEMPRESA ocEMPRESAp, string INVOICE_IDp, string CUSTOMER_IDp, string CUSTOMER_NAME)
        //{
        //    ocEMPRESA = ocEMPRESAp;
        //    CUSTOMER_ID = CUSTOMER_IDp;
        //    InitializeComponent();

        //    cargarTipos();

        //    limpiar();
        //    invoice_id.Enabled = false;
        //    invoice_id.Text = INVOICE_IDp;
        //    cliente.Text=CUSTOMER_NAME;
        //    cliente.Enabled = false;

        //}

        private void cargarTipos()
        {
            tipoRelacion.Items.Add(new ComboItem { Name = "Nota de cr�dito de los documentos relacionados", Value = "01" });
            tipoRelacion.Items.Add(new ComboItem { Name = "Nota de d�bito de los documentos relacionados", Value = "02" });
            tipoRelacion.Items.Add(new ComboItem { Name = "Devoluci�n de mercanc�a sobre facturas o traslados previos", Value = "03" });
            tipoRelacion.Items.Add(new ComboItem { Name = "Sustituci�n de los CFDI previos", Value = "04" });
            tipoRelacion.Items.Add(new ComboItem { Name = "Traslados de mercancias facturados previamente", Value = "05" });
            tipoRelacion.Items.Add(new ComboItem { Name = "Factura generada por los traslados previos", Value = "06" });
            tipoRelacion.Items.Add(new ComboItem { Name = "CFDI por aplicaci�n de anticipo", Value = "07" });
            tipoRelacion.SelectedIndex = 0;
        }
        #region Metodos
        private void cargar()
        {
            //Cargar los datos
            ModeloDocumentosFXContainer dbContext = new ModeloDocumentosFXContainer();
            try
            {
                dtg.Rows.Clear();
                var result = from b in dbContext.ndcFacturaSet
                                         .Where(b => b.INVOICE_ID == INVOICE_IDtr)
                             select b
                                         ;
                if (result != null)
                {
                    List<ndcFactura> dt = result.ToList();
                    bool tipoCargado = false;
                    foreach (ndcFactura registro in dt)
                    {
                        int n = dtg.Rows.Add();
                        //Cargar el tipo
                        if (!tipoCargado)
                        {
                            foreach (ComboItem cbi in tipoRelacion.Items)
                            {
                                if (cbi.Value as String == registro.tipoRelacion)
                                {
                                    this.tipoRelacion.SelectedItem = cbi;
                                    break;
                                }
                            }
                            tipoCargado = true;
                        }

                        dtg.Rows[n].Cells["factura"].Value = registro.INVOICE_ID_relacionada;
                        dtg.Rows[n].Cells["UUID"].Value = registro.UUID.ToString();

                    }
                }
                

                //Cargar las lineas de la NDC
                dtgProductos.Rows.Clear();
                string sSQL = "select * from receivable_line rl where rl.invoice_id='" + INVOICE_IDtr + "' order by line_no ";

                DataTable oDataTable = oData.EjecutarConsulta(sSQL);
                if (oDataTable != null)
                {
                    foreach (DataRow oDataRow in oDataTable.Rows)
                    {
                        
                        int n = dtgProductos.Rows.Add();
                        dtgProductos.Rows[n].Cells["REFERENCE"].Value = oDataRow["REFERENCE"].ToString().Trim();
                        dtgProductos.Rows[n].Cells["LINE"].Value = oDataRow["LINE_NO"].ToString().Trim();
                        dtgProductos.Rows[n].Cells["AMOUNT"].Value = oDataRow["AMOUNT"].ToString().Trim();
                        dtgProductos.Rows[n].Cells["invoice_id_relacionado"].Value =
                        dtgProductos.Rows[n].Cells["invoice_id_relacionado_linea"].Value =
                        dtgProductos.Rows[n].Cells["PART_ID"].Value =
                        dtgProductos.Rows[n].Cells["clasificacion"].Value = "";

                        string LINE_NOtr = oDataRow["LINE_NO"].ToString().Trim();
                        //Buscar los datos aplicados con la linea y factura
                        var resultLinea = from b in dbContext.ndcFacturaLineaSet
                                                                 .Where(b => b.INVOICE_ID == INVOICE_IDtr & b.LINE_NO== LINE_NOtr)
                                     select b;
                        List<ndcFacturaLinea> dtLinea = resultLinea.ToList();
                        foreach (ndcFacturaLinea registro in dtLinea)
                        {
                            dtgProductos.Rows[n].Cells["invoice_id_relacionado"].Value = registro.invoice_id_relacionado;
                            dtgProductos.Rows[n].Cells["invoice_id_relacionado_linea"].Value = registro.invoice_id_relacionado_linea;
                            dtgProductos.Rows[n].Cells["PART_ID"].Value = registro.PART_ID;
                            dtgProductos.Rows[n].Cells["clasificacion"].Value = registro.clasificacion;
                        }
                    }
                }


            }
            catch (Exception err)
            {
                ErrorFX.mostrar(err, false, true, "frmTipoRelacionNDCnormal - 117 - ", false);
            }

        }

        public void limpiar()
        {
            if (modificado)
            {

                DialogResult Resultado = MessageBox.Show(this, "�Desea guardar los cambios?", Application.ProductName + "-" + Application.ProductVersion, MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                if (Resultado.ToString() == "Yes")
                {
                    guardar();
                }
            }
            modificado = false;
            foreach (var c in this.Controls)
            {
                if (c is TextBox)
                {
                    ((TextBox)c).Text = "";
                }
            }
            dtg.Rows.Clear();
        }
        private void guardar()
        {
            ModeloDocumentosFX.ModeloDocumentosFXContainer dbContext = new ModeloDocumentosFXContainer();
            try
            {
                string tipoRelaciontr = (tipoRelacion.SelectedItem as ComboItem).Value.ToString();
                foreach (DataGridViewRow linea in dtg.Rows)
                {
                    guardarLinea(tipoRelaciontr, linea);
                }

                foreach (DataGridViewRow linea in dtgProductos.Rows)
                {
                    guardarLineaProducto(linea);
                }
                
                MessageBox.Show(this, "Datos guardados", Application.ProductName + "-" + Application.ProductVersion, MessageBoxButtons.OK, MessageBoxIcon.Information);

                DialogResult = DialogResult.OK;

                
            }
            catch (Exception e)
            {
                ErrorFX.mostrar(e, true, true, "frmTipoRelacionNDCnormal - 90 ");
            }
        }

        private void guardarLinea(string tipoRelaciontr, DataGridViewRow linea)
        {
            ModeloDocumentosFX.ModeloDocumentosFXContainer dbContext = new ModeloDocumentosFXContainer();

            string conection = dbContext.Database.Connection.ConnectionString;
            cCONEXCION oData = new cCONEXCION(conection);
            eliminarLinea(linea, oData);

            string UUIDtr = linea.Cells["UUID"].Value.ToString();
            string INVOICE_ID_relacionada = linea.Cells["factura"].Value.ToString();
            ComboItem selected = (ComboItem)tipoRelacion.SelectedItem;

            string sSQL = @"INSERT INTO ndcFacturaSet (tipoRelacion,INVOICE_ID,INVOICE_ID_relacionada,UUID) VALUES 
            ('" + selected.Value + "','" + invoice_id.Text + "','" + INVOICE_ID_relacionada + "','" + UUIDtr + @"')";
            oData.EjecutarConsulta(sSQL);

        }
        private void guardarLineaProducto(DataGridViewRow linea)
        {
            ModeloDocumentosFX.ModeloDocumentosFXContainer dbContext = new ModeloDocumentosFXContainer();

            string conection = dbContext.Database.Connection.ConnectionString;
            cCONEXCION oData = new cCONEXCION(conection);
            eliminarLineaProducto(linea, oData);

            string LINE = linea.Cells["LINE"].Value.ToString();
            string invoice_id_relacionado = linea.Cells["invoice_id_relacionado"].Value.ToString();
            string invoice_id_relacionado_linea = linea.Cells["invoice_id_relacionado_linea"].Value.ToString();
            string PART_IDtr = linea.Cells["PART_ID"].Value.ToString();
            string clasificacion = linea.Cells["clasificacion"].Value.ToString();
            
            string sSQL = @"INSERT INTO ndcFacturaLineaSet (INVOICE_ID,LINE_NO,invoice_id_relacionado,invoice_id_relacionado_linea, PART_ID, clasificacion) 
            VALUES 
            ('" + invoice_id.Text + "','" + LINE + "','" + invoice_id_relacionado + "','" + invoice_id_relacionado_linea + "','" + PART_IDtr + "','" + clasificacion + "')";
            oData.EjecutarConsulta(sSQL);

        }

        private void eliminarLineaProducto(DataGridViewRow linea, cCONEXCION oData)
        {
            string LINE = linea.Cells["LINE"].Value.ToString();
            string sSQL = "DELETE FROM ndcFacturaLineaSet WHERE INVOICE_ID='" + invoice_id.Text + "' AND LINE_NO='" + LINE + "'";
            oData.EjecutarConsulta(sSQL);
        }

        private void eliminarLinea(DataGridViewRow linea, cCONEXCION oData)
        {
            string UUIDtr = linea.Cells["UUID"].Value.ToString();
            string sSQL = "DELETE FROM ndcFacturaSet WHERE INVOICE_ID='" + invoice_id.Text + "' AND UUID='" + UUIDtr + "'";
            oData.EjecutarConsulta(sSQL);
        }

        private void eliminar()
        {
            if(invoice_id.Text=="")
            {
                return;
            }
            DialogResult Resultado = MessageBox.Show(this, "�Esta seguro de eliminar?", Application.ProductName + "-" + Application.ProductVersion, MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            if (Resultado.ToString() != "Yes")
            {
                return;
            }
            ModeloDocumentosFX.ModeloDocumentosFXContainer dbContext = new ModeloDocumentosFXContainer();

            string conection = dbContext.Database.Connection.ConnectionString;
            cCONEXCION oData = new cCONEXCION(conection);

            string sSQL = "DELETE FROM INVOICE_ID='" + invoice_id.Text + "'";
            oData.EjecutarConsulta(sSQL);

            MessageBox.Show(this, "Eliminado.", Application.ProductName + "-" + Application.ProductVersion, MessageBoxButtons.OK, MessageBoxIcon.Information);
            limpiar();
        }


        #endregion

        private void buttonAdv1_Click(object sender, EventArgs e)
        {
            limpiar();
        }

        private void btnAddItem_Click(object sender, EventArgs e)
        {
           guardar();
        }

        private void btnDeleteItem_Click(object sender, EventArgs e)
        {
            eliminar();
        }

        private void frmTipoRelacionNDCnormal_FormClosed(object sender, FormClosedEventArgs e)
        {
            DialogResult = DialogResult.Cancel;
        }

        private void buttonAdv3_Click(object sender, EventArgs e)
        {
            eliminarLinea();
        }

        private void eliminarLinea()
        {
            dtg.EndEdit();
            if (dtg.SelectedCells.Count > 0)
            {
                DialogResult Resultado = MessageBox.Show("�Desea eliminar las seleccionada?", Application.ProductName + "-" + Application.ProductVersion, MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                if (Resultado.ToString() != "Yes")
                {
                    return;
                }
                ModeloDocumentosFX.ModeloDocumentosFXContainer dbContext = new ModeloDocumentosFXContainer();
                string conection = dbContext.Database.Connection.ConnectionString;
                cCONEXCION oData = new cCONEXCION(conection);
                foreach (DataGridViewRow drv in dtg.SelectedRows)
                {
                    try
                    {
                        eliminarLinea(drv, oData);
                        //Limpiar lineas que tengan de productos asociadas
                        string invoice_id = drv.Cells["factura"].Value.ToString();
                        limpiarProductos(invoice_id,oData);
                        dtg.Rows.Remove(drv);
                    }
                    catch (Exception e)
                    {
                        Bitacora.Log(e.Message.ToString());
                    }

                }
            }
            else
            {
                MessageBox.Show("Seleccione alguna l�nea.", Application.ProductName + "-" + Application.ProductVersion, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }

        private void limpiarProductos(string invoice_id, cCONEXCION oData)
        {
            foreach (DataGridViewRow drv in dtgProductos.Rows)
            {
                try
                {
                    if (drv.Cells["invoice_id_relacionado"].Value.ToString()== invoice_id)
                    {
                        eliminarLineaProducto(drv, oData);
                        drv.Cells["PART_ID"].Value =
                        drv.Cells["invoice_id_relacionado"].Value =
                        drv.Cells["invoice_id_relacionado_linea"].Value =
                        drv.Cells["clasificacion"].Value = "";

                    }
                }
                catch (Exception e)
                {
                    Bitacora.Log(e.Message.ToString());
                }

            }
        }

        private void buttonAdv2_Click(object sender, EventArgs e)
        {
            cargarFacturas();
        }

        private void cargarFacturas()
        {
            frmFacturas o = new frmFacturas(oData,CUSTOMER_IDtr);
            if (o.ShowDialog() == DialogResult.OK)
            {
                foreach (DataGridViewRow linea in o.selecionados)
                {
                    int n = dtg.Rows.Add();
                    dtg.Rows[n].Cells["factura"].Value = linea.Cells["invoice_id"].Value.ToString();
                    dtg.Rows[n].Cells["UUID"].Value = linea.Cells["UUID"].Value.ToString();
                }
            }
        }
        private string buscarProducto(string pART_ID)
        {
            string resultado = "";
            ModeloDocumentosFXContainer dbContext = new ModeloDocumentosFXContainer();
            try
            {
                configuracionProducto registro = (from r in dbContext.configuracionProductoSet
                                                  .Where(a => a.producto == pART_ID)
                                                  select r).FirstOrDefault();
                if (registro != null)
                {
                    resultado = registro.clasificacionSAT;
                    dbContext.Dispose();
                }
            }
            catch (Exception err)
            {
                ErrorFX.mostrar(err, true, true, "cFACTURA - 1265 - ", false);
            }
            return resultado;
        }

        private void buttonAdv4_Click(object sender, EventArgs e)
        {
            if (dtg.Rows.Count == 0)
            {
                MessageBox.Show("Busque las facturas a asociadas", Application.ProductName + "-" + Application.ProductVersion, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return;
            }

            if (this.dtgProductos.SelectedRows.Count == 0)
            {
                MessageBox.Show("Seleciona una l�nea para asociar el producto", Application.ProductName + "-" + Application.ProductVersion, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return;
            }
            string facturas = "";
            foreach (DataGridViewRow linea in dtg.Rows)
            {
                if (facturas != "")
                {
                    facturas = facturas + ",";
                }
                facturas = facturas + "'" + linea.Cells["factura"].Value.ToString() + "'";
            }
            if (facturas != "")
            {
                foreach (DataGridViewRow drv in dtgProductos.SelectedRows)
                {
                    frmFacturasLineas ofrmFacturasLineas = new frmFacturasLineas(oData, facturas);
                    if (ofrmFacturasLineas.ShowDialog() == DialogResult.OK)
                    {
                        foreach (DataGridViewRow linea in ofrmFacturasLineas.selecionados)
                        {
                            drv.Cells["PART_ID"].Value = linea.Cells["PART_ID"].Value.ToString();
                            drv.Cells["invoice_id_relacionado"].Value = linea.Cells["invoice_id"].Value.ToString();
                            drv.Cells["invoice_id_relacionado_linea"].Value = linea.Cells["line_no"].Value.ToString();
                            drv.Cells["clasificacion"].Value = buscarProducto(linea.Cells["PART_ID"].Value.ToString());

                        }
                    }
                }
            }
            

        }

        private void buttonAdv5_Click(object sender, EventArgs e)
        {
            eliminarLineaProducto();
        }

        private void eliminarLineaProducto()
        {
            dtgProductos.EndEdit();
            if (dtgProductos.SelectedCells.Count > 0)
            {
                DialogResult Resultado = MessageBox.Show("�Desea eliminar de productos seleccionada?", Application.ProductName + "-" + Application.ProductVersion, MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                if (Resultado.ToString() != "Yes")
                {
                    return;
                }
                ModeloDocumentosFX.ModeloDocumentosFXContainer dbContext = new ModeloDocumentosFXContainer();
                string conection = dbContext.Database.Connection.ConnectionString;
                cCONEXCION oData = new cCONEXCION(conection);
                foreach (DataGridViewRow drv in dtgProductos.SelectedRows)
                {
                    try
                    {
                        eliminarLineaProducto(drv, oData);
                        drv.Cells["PART_ID"].Value = "";
                        drv.Cells["invoice_id_relacionado"].Value = "";
                        drv.Cells["invoice_id_relacionado_linea"].Value = "";
                        drv.Cells["clasificacion"].Value = "";
                    }
                    catch (Exception e)
                    {
                        Bitacora.Log(e.Message.ToString());
                    }

                }
            }
            else
            {
                MessageBox.Show("Seleccione alguna l�nea.", Application.ProductName + "-" + Application.ProductVersion, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }

    }
}
