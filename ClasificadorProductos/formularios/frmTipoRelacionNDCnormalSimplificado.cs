#region Copyright Syncfusion Inc. 2001 - 2017
// Copyright Syncfusion Inc. 2001 - 2017. All rights reserved.
// Use of this code is subject to the terms of our license.
// A copy of the current license can be obtained at any time by e-mailing
// licensing@syncfusion.com. Any infringement will be prosecuted under
// applicable laws. 
#endregion
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;

using System.Text;
using System.Windows.Forms;
using ModeloDocumentosFX;
using System.Text;
using System.Windows.Forms;
using System.Linq;
using System.Data.Entity.Validation;
using Generales;
using OfficeOpenXml;
using System.IO;

namespace ClasificadorProductos.formularios
{


    public partial class frmTipoRelacionNDCnormalSimplificado : Form //Syncfusion.Windows.Forms.MetroForm
    {
        EMPRESA oEMPRESA;

        string ENTITY_IDtr;
        string INVOICE_IDtr;
        string CUSTOMER_IDtr;

        cCONEXCION oData;
        bool modificado = false;
        //public frmTipoRelacionNDCnormalSimplificado(cEMPRESA ocEMPRESAp)
        //{
        //    ocEMPRESA = ocEMPRESAp;
        //    InitializeComponent();
        //    cargarTipos();
        //    limpiar();
        //}
        public frmTipoRelacionNDCnormalSimplificado(string ENTITY_IDp,string INVOICE_IDp, string CUSTOMER_IDp)
        {
            ENTITY_IDtr = ENTITY_IDp;
            INVOICE_IDtr = INVOICE_IDp;
            CUSTOMER_IDtr = CUSTOMER_IDp;
            
            InitializeComponent();

            //Cargar datosd la empresa y la conexi�n
            ModeloDocumentosFXContainer dbContext = new ModeloDocumentosFXContainer();
            try
            {
                //Cargar el primer registro de configuraci�n de usuarios
                oEMPRESA = (from r in dbContext.EMPRESA.Where(b => b.ENTITY_ID==ENTITY_IDtr)
                                 select r).FirstOrDefault();
                if (oEMPRESA==null)
                {
                    ErrorFX.mostrar("La empresa con entidad " + ENTITY_IDtr + " no existe, no puede cargar los datos", true, true, true);
                }

                //Creac la conexi�n a base de datos
                oData = new cCONEXCION("SQLSERVER", oEMPRESA.SERVIDOR, oEMPRESA.BD, oEMPRESA.USUARIO_BD, cENCRIPTACION.Decrypt(oEMPRESA.PASSWORD_BD));
                oData.CrearConexion();
            }
            catch (Exception err)
            {
                ErrorFX.mostrar(err, false, true, "frmTipoRelacionNDCnormalSimplificado - 66 - ", false);
            }
            cargarTipos();
            //limpiar();
            invoice_id.Enabled = false;
            invoice_id.Text = INVOICE_IDtr;
            cliente.Text = CUSTOMER_IDp;
            cliente.Enabled = false;
            cargar();
        }

        //public frmTipoRelacionNDCnormalSimplificado(cEMPRESA ocEMPRESAp, string INVOICE_IDp, string CUSTOMER_IDp, string CUSTOMER_NAME)
        //{
        //    ocEMPRESA = ocEMPRESAp;
        //    CUSTOMER_ID = CUSTOMER_IDp;
        //    InitializeComponent();

        //    cargarTipos();

        //    limpiar();
        //    invoice_id.Enabled = false;
        //    invoice_id.Text = INVOICE_IDp;
        //    cliente.Text=CUSTOMER_NAME;
        //    cliente.Enabled = false;

        //}

        private void cargarTipos()
        {
            tipoRelacion.Items.Add(new ComboItem { Name = "Nota de cr�dito de los documentos relacionados", Value = "01" });
            tipoRelacion.Items.Add(new ComboItem { Name = "Nota de d�bito de los documentos relacionados", Value = "02" });
            tipoRelacion.Items.Add(new ComboItem { Name = "Devoluci�n de mercanc�a sobre facturas o traslados previos", Value = "03" });
            tipoRelacion.Items.Add(new ComboItem { Name = "Sustituci�n de los CFDI previos", Value = "04" });
            tipoRelacion.Items.Add(new ComboItem { Name = "Traslados de mercancias facturados previamente", Value = "05" });
            tipoRelacion.Items.Add(new ComboItem { Name = "Factura generada por los traslados previos", Value = "06" });
            tipoRelacion.Items.Add(new ComboItem { Name = "CFDI por aplicaci�n de anticipo", Value = "07" });
            tipoRelacion.SelectedIndex = 0;
        }
        #region Metodos
        private void cargar()
        {
            //Cargar los datos
            ModeloDocumentosFXContainer dbContext = new ModeloDocumentosFXContainer();
            try
            {
                //dtg.Rows.Clear();
                //var result = from b in dbContext.ndcFacturaSet
                //                         .Where(b => b.INVOICE_ID == INVOICE_IDtr)
                //             select b
                //                         ;
                //if (result != null)
                //{
                //    List<ndcFactura> dt = result.ToList();
                //    bool tipoCargado = false;
                //    foreach (ndcFactura registro in dt)
                //    {
                //        int n = dtg.Rows.Add();
                //        //Cargar el tipo
                //        if (!tipoCargado)
                //        {
                //            foreach (ComboItem cbi in tipoRelacion.Items)
                //            {
                //                if (cbi.Value as String == registro.tipoRelacion)
                //                {
                //                    this.tipoRelacion.SelectedItem = cbi;
                //                    break;
                //                }
                //            }
                //            tipoCargado = true;
                //        }

                //        dtg.Rows[n].Cells["factura"].Value = registro.INVOICE_ID_relacionada;
                //        dtg.Rows[n].Cells["UUID"].Value = registro.UUID.ToString();

                //    }
                //}
                

                //Cargar las lineas de la NDC
                dtgProductos.Rows.Clear();
                string sSQL = "select * from receivable_line rl where rl.invoice_id='" + INVOICE_IDtr + "' order by line_no ";

                DataTable oDataTable = oData.EjecutarConsulta(sSQL);
                if (oDataTable != null)
                {
                    foreach (DataRow oDataRow in oDataTable.Rows)
                    {
                        
                        int n = dtgProductos.Rows.Add();
                        dtgProductos.Rows[n].Cells["REFERENCE"].Value = oDataRow["REFERENCE"].ToString().Trim();
                        dtgProductos.Rows[n].Cells["LINE"].Value = oDataRow["LINE_NO"].ToString().Trim();
                        dtgProductos.Rows[n].Cells["AMOUNT"].Value = oDataRow["AMOUNT"].ToString().Trim();
                        dtgProductos.Rows[n].Cells["invoice_id_relacionado"].Value =
                        dtgProductos.Rows[n].Cells["invoice_id_relacionado_linea"].Value =
                        dtgProductos.Rows[n].Cells["PART_ID"].Value =
                        dtgProductos.Rows[n].Cells["clasificacion"].Value = "";

                        string LINE_NOtr = oDataRow["LINE_NO"].ToString().Trim();

                        //Rodrigo Escalona 20/10/2022 Cargar el folio sical
                        var resultFactura = (dbContext.ndcFacturaSet
                                                             .Where(b => b.INVOICE_ID == INVOICE_IDtr)).FirstOrDefault();
                        
                        if (resultFactura != null)
                        {
                            dtgProductos.Rows[n].Cells["UUIDcol"].Value = resultFactura.UUID;
                            foreach (ComboItem cbi in tipoRelacion.Items)
                            {
                                if (cbi.Value== resultFactura.tipoRelacion)
                                {
                                    tipoRelacion.SelectedItem = cbi;
                                    break;
                                }
                            }
                        }

                        //Buscar los datos aplicados con la linea y factura
                        var resultLinea = from b in dbContext.ndcFacturaLineaSet
                                                                 .Where(b => b.INVOICE_ID == INVOICE_IDtr & b.LINE_NO== LINE_NOtr)
                                     select b;
                        List<ndcFacturaLinea> dtLinea = resultLinea.ToList();
                        foreach (ndcFacturaLinea registro in dtLinea)
                        {
                            dtgProductos.Rows[n].Cells["invoice_id_relacionado"].Value = registro.invoice_id_relacionado;
                            dtgProductos.Rows[n].Cells["invoice_id_relacionado_linea"].Value = registro.invoice_id_relacionado_linea;
                            dtgProductos.Rows[n].Cells["PART_ID"].Value = registro.PART_ID;
                            dtgProductos.Rows[n].Cells["clasificacion"].Value = registro.clasificacion;

                            //Rodrigo Escalona 20/10/2022 Cargar el folio sical
                            resultFactura = (dbContext.ndcFacturaSet
                                                                 .Where(b => b.INVOICE_ID == registro.invoice_id_relacionado)).FirstOrDefault();
                            if (resultFactura != null)
                            {
                                dtgProductos.Rows[n].Cells["UUIDcol"].Value = resultFactura.UUID;
                                foreach (ComboItem cbi in tipoRelacion.Items)
                                {
                                    if (cbi.Value == resultFactura.tipoRelacion)
                                    {
                                        tipoRelacion.SelectedItem = cbi;
                                        break;
                                    }
                                }
                            }
                        }
                    }
                }


            }
            catch (Exception err)
            {
                ErrorFX.mostrar(err, false, true, "frmTipoRelacionNDCnormalSimplificado - 117 - ");
            }

        }

        public void limpiar()
        {
            if (modificado)
            {

                DialogResult Resultado = MessageBox.Show(this, "�Desea guardar los cambios?", Application.ProductName + "-" + Application.ProductVersion, MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                if (Resultado.ToString() == "Yes")
                {
                    guardar();
                }
            }
            modificado = false;
            foreach (var c in this.Controls)
            {
                if (c is TextBox)
                {
                    ((TextBox)c).Text = "";
                }
            }
            //dtg.Rows.Clear();
        }
        private void guardar()
        {
            ModeloDocumentosFX.ModeloDocumentosFXContainer dbContext = new ModeloDocumentosFXContainer();
            try
            {
                string tipoRelaciontr = (tipoRelacion.SelectedItem as ComboItem).Value.ToString();
                guardarLinea(tipoRelaciontr);
                //foreach (DataGridViewRow linea in dtg.Rows)
                //{
                //    guardarLinea(tipoRelaciontr, linea);
                //}

                foreach (DataGridViewRow linea in dtgProductos.Rows)
                {
                    guardarLineaProducto(linea);
                }
                
                MessageBox.Show(this, "Datos guardados", Application.ProductName + "-" + Application.ProductVersion, MessageBoxButtons.OK, MessageBoxIcon.Information);

                DialogResult = DialogResult.OK;
                this.Close();
                
            }
            catch (Exception e)
            {
                ErrorFX.mostrar(e, true, true, "frmTipoRelacionNDCnormalSimplificado - 90 ", false);
            }
        }
        ndcFactura[] ondcFacturas;
        private void guardarLinea(string tipoRelaciontr)
        {
            ModeloDocumentosFX.ModeloDocumentosFXContainer dbContext = new ModeloDocumentosFXContainer();

            string conection = dbContext.Database.Connection.ConnectionString;
            cCONEXCION oData = new cCONEXCION(conection);

            string sSQL = @"DELETE ndcFacturaSet WHERE INVOICE_ID='" + invoice_id.Text + "'";
            oData.EjecutarConsulta(sSQL);

            foreach (DataGridViewRow linea in dtgProductos.Rows)
            {
                string INVOICE_ID_relacionada = linea.Cells["invoice_id_relacionado"].Value.ToString();
                if (linea.Cells["UUIDcol"].Value != null)
                {
                    string UUIDtr = linea.Cells["UUIDcol"].Value.ToString();
                    sSQL = @"INSERT INTO ndcFacturaSet (tipoRelacion,INVOICE_ID,INVOICE_ID_relacionada,UUID) VALUES 
                ('" + tipoRelaciontr + "','" + invoice_id.Text + "','" + INVOICE_ID_relacionada + "','" + UUIDtr + @"')";
                    oData.EjecutarConsulta(sSQL);
                }
                
            }


        }

        private void guardarLinea(string tipoRelaciontr, DataGridViewRow linea)
        {
            ModeloDocumentosFX.ModeloDocumentosFXContainer dbContext = new ModeloDocumentosFXContainer();

            string conection = dbContext.Database.Connection.ConnectionString;
            cCONEXCION oData = new cCONEXCION(conection);
            eliminarLinea(linea, oData);

            string UUIDtr = linea.Cells["UUID"].Value.ToString();
            string INVOICE_ID_relacionada = linea.Cells["factura"].Value.ToString();
            ComboItem selected = (ComboItem)tipoRelacion.SelectedItem;

            string sSQL = @"INSERT INTO ndcFacturaSet (tipoRelacion,INVOICE_ID,INVOICE_ID_relacionada,UUID) VALUES 
            ('" + selected.Value + "','" + invoice_id.Text + "','" + INVOICE_ID_relacionada + "','" + UUIDtr + @"')";
            oData.EjecutarConsulta(sSQL);

        }
        private void guardarLineaProducto(DataGridViewRow linea)
        {
            ModeloDocumentosFX.ModeloDocumentosFXContainer dbContext = new ModeloDocumentosFXContainer();

            string conection = dbContext.Database.Connection.ConnectionString;
            cCONEXCION oData = new cCONEXCION(conection);
            eliminarLineaProducto(linea, oData);

            string LINE = linea.Cells["LINE"].Value.ToString();
            string invoice_id_relacionado = linea.Cells["invoice_id_relacionado"].Value.ToString();
            string invoice_id_relacionado_linea = linea.Cells["invoice_id_relacionado_linea"].Value.ToString();
            string PART_IDtr = linea.Cells["PART_ID"].Value.ToString();
            string clasificacion = linea.Cells["clasificacion"].Value.ToString();
            
            string sSQL = @"INSERT INTO ndcFacturaLineaSet (INVOICE_ID,LINE_NO,invoice_id_relacionado,invoice_id_relacionado_linea, PART_ID, clasificacion) 
            VALUES 
            ('" + invoice_id.Text + "','" + LINE + "','" + invoice_id_relacionado + "','" + invoice_id_relacionado_linea + "','" + PART_IDtr + "','" + clasificacion + "')";
            oData.EjecutarConsulta(sSQL);

        }

        private void eliminarLineaProducto(DataGridViewRow linea, cCONEXCION oData)
        {
            string LINE = linea.Cells["LINE"].Value.ToString();
            string sSQL = "DELETE FROM ndcFacturaLineaSet WHERE INVOICE_ID='" + invoice_id.Text + "' AND LINE_NO='" + LINE + "'";
            oData.EjecutarConsulta(sSQL);
        }

        private void eliminarLinea(DataGridViewRow linea, cCONEXCION oData)
        {
            string UUIDtr = linea.Cells["UUID"].Value.ToString();
            string sSQL = "DELETE FROM ndcFacturaSet WHERE INVOICE_ID='" + invoice_id.Text + "' AND UUID='" + UUIDtr + "'";
            oData.EjecutarConsulta(sSQL);
        }

        private void eliminar()
        {
            if(invoice_id.Text=="")
            {
                return;
            }
            DialogResult Resultado = MessageBox.Show(this, "�Esta seguro de eliminar?", Application.ProductName + "-" + Application.ProductVersion, MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            if (Resultado.ToString() != "Yes")
            {
                return;
            }
            ModeloDocumentosFX.ModeloDocumentosFXContainer dbContext = new ModeloDocumentosFXContainer();

            string conection = dbContext.Database.Connection.ConnectionString;
            cCONEXCION oData = new cCONEXCION(conection);

            //Eliminar la relacion preexistente
            string sSQL = @"DELETE FROM ndcFacturaSet WHERE INVOICE_ID='" + invoice_id.Text + "'";
            oData.EjecutarConsulta(sSQL);

            sSQL = @"DELETE FROM ndcFacturaLineaSet WHERE INVOICE_ID='" + invoice_id.Text + "'";
            oData.EjecutarConsulta(sSQL);
            

            MessageBox.Show(this, "Eliminado.", Application.ProductName + "-" + Application.ProductVersion, MessageBoxButtons.OK, MessageBoxIcon.Information);
            limpiar();
        }


        #endregion

        private void buttonAdv1_Click(object sender, EventArgs e)
        {
            limpiar();
        }

        private void btnAddItem_Click(object sender, EventArgs e)
        {
           guardar();
        }

        private void btnDeleteItem_Click(object sender, EventArgs e)
        {
            eliminar();
        }

        private void frmTipoRelacionNDCnormalSimplificado_FormClosed(object sender, FormClosedEventArgs e)
        {
            DialogResult = DialogResult.Cancel;
        }

        private void buttonAdv3_Click(object sender, EventArgs e)
        {
            eliminarLinea();
        }

        private void eliminarLinea()
        {
            //dtg.EndEdit();
            //if (dtg.SelectedCells.Count > 0)
            //{
            //    DialogResult Resultado = MessageBox.Show("�Desea eliminar las seleccionada?", Application.ProductName + "-" + Application.ProductVersion, MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            //    if (Resultado.ToString() != "Yes")
            //    {
            //        return;
            //    }
            //    ModeloDocumentosFX.ModeloDocumentosFXContainer dbContext = new ModeloDocumentosFXContainer();
            //    string conection = dbContext.Database.Connection.ConnectionString;
            //    cCONEXCION oData = new cCONEXCION(conection);
            //    foreach (DataGridViewRow drv in dtg.SelectedRows)
            //    {
            //        try
            //        {
            //            eliminarLinea(drv, oData);
            //            //Limpiar lineas que tengan de productos asociadas
            //            string invoice_id = drv.Cells["factura"].Value.ToString();
            //            limpiarProductos(invoice_id,oData);
            //            dtg.Rows.Remove(drv);
            //        }
            //        catch (Exception e)
            //        {
            //            Bitacora.Log(e.Message.ToString());
            //        }

            //    }
            //}
            //else
            //{
            //    MessageBox.Show("Seleccione alguna l�nea.", Application.ProductName + "-" + Application.ProductVersion, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            //}
        }

        private void limpiarProductos(string invoice_id, cCONEXCION oData)
        {
            foreach (DataGridViewRow drv in dtgProductos.Rows)
            {
                try
                {
                    if (drv.Cells["invoice_id_relacionado"].Value.ToString()== invoice_id)
                    {
                        eliminarLineaProducto(drv, oData);
                        drv.Cells["PART_ID"].Value =
                        drv.Cells["invoice_id_relacionado"].Value =
                        drv.Cells["invoice_id_relacionado_linea"].Value =
                        drv.Cells["clasificacion"].Value = "";

                    }
                }
                catch (Exception e)
                {
                    Bitacora.Log(e.Message.ToString());
                }

            }
        }

        private void buttonAdv2_Click(object sender, EventArgs e)
        {
            cargarFacturas();
        }

        private void cargarFacturas()
        {
            //frmFacturas o = new frmFacturas(oData,CUSTOMER_IDtr);
            //if (o.ShowDialog() == DialogResult.OK)
            //{
            //    foreach (DataGridViewRow linea in o.selecionados)
            //    {
            //        int n = dtg.Rows.Add();
            //        dtg.Rows[n].Cells["factura"].Value = linea.Cells["invoice_id"].Value.ToString();
            //        dtg.Rows[n].Cells["UUID"].Value = linea.Cells["UUID"].Value.ToString();
            //    }
            //}
        }
        private string buscarProducto(string pART_ID, string productCode)
        {
            string resultado = "";
            ModeloDocumentosFXContainer dbContext = new ModeloDocumentosFXContainer();
            try
            {
                bool buscar = true;
                //TODO: Buscar por product code
                configuracionProducto registro = (from r in dbContext.configuracionProductoSet
                                                  .Where(a => a.producto == productCode)
                                                  select r).FirstOrDefault();
                if (registro != null)
                {
                    if (registro.Id != 0)
                    {
                        buscar = false;
                        resultado = registro.clasificacionSAT;
                    }
                }

                if (buscar)
                {
                    registro = (from r in dbContext.configuracionProductoSet
                                                  .Where(a => a.producto == pART_ID)
                                select r).FirstOrDefault();
                    if (registro != null)
                    {
                        if (registro.Id != 0)
                        {
                            resultado = registro.clasificacionSAT;
                        }
                    }
                }

                dbContext.Dispose();
            }
            catch (Exception err)
            {
                ErrorFX.mostrar(err, true, true, "cFACTURA - 1265 - ");
            }
            return resultado;
        }

        private void buttonAdv4_Click(object sender, EventArgs e)
        {
            //if (dtg.Rows.Count == 0)
            //{
            //    MessageBox.Show("Busque las facturas a asociadas", Application.ProductName + "-" + Application.ProductVersion, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            //    return;
            //}

            if (this.dtgProductos.SelectedRows.Count == 0)
            {
                MessageBox.Show("Seleciona una l�nea para asociar el producto", Application.ProductName + "-" + Application.ProductVersion, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return;
            }
            string facturas = "";
            //foreach (DataGridViewRow linea in dtg.Rows)
            //{
            //    if (facturas != "")
            //    {
            //        facturas = facturas + ",";
            //    }
            //    facturas = facturas + "'" + linea.Cells["factura"].Value.ToString() + "'";
            //}

            //if (facturas != "")
            //{
            //    foreach (DataGridViewRow drv in dtgProductos.SelectedRows)
            //    {
            //        frmFacturasLineas ofrmFacturasLineas = new frmFacturasLineas(oData, facturas);
            //        if (ofrmFacturasLineas.ShowDialog() == DialogResult.OK)
            //        {
            //            foreach (DataGridViewRow linea in ofrmFacturasLineas.selecionados)
            //            {
            //                drv.Cells["PART_ID"].Value = linea.Cells["PART_ID"].Value.ToString();
            //                drv.Cells["invoice_id_relacionado"].Value = linea.Cells["invoice_id"].Value.ToString();
            //                drv.Cells["invoice_id_relacionado_linea"].Value = linea.Cells["line_no"].Value.ToString();
            //                drv.Cells["clasificacion"].Value = buscarProducto(linea.Cells["PART_ID"].Value.ToString());

            //            }
            //        }
            //    }
            //}

            foreach (DataGridViewRow drv in dtgProductos.SelectedRows)
            {
                //Ver el monto 
                decimal montotr = Math.Abs(decimal.Parse(drv.Cells["AMOUNT"].Value.ToString()));
                string tipoRelaciontr = (tipoRelacion.SelectedItem as ComboItem).Value.ToString();
                frmFacturasLineas ofrmFacturasLineas = new frmFacturasLineas(oData,CUSTOMER_IDtr, montotr,invoice_id.Text, tipoRelaciontr);
                if (ofrmFacturasLineas.ShowDialog() == DialogResult.OK)
                {
                    foreach (DataGridViewRow linea in ofrmFacturasLineas.selecionados)
                    {
                        drv.Cells["PART_ID"].Value = linea.Cells["PART_ID"].Value.ToString();
                        drv.Cells["invoice_id_relacionado"].Value = linea.Cells["invoice_id"].Value.ToString();
                        drv.Cells["invoice_id_relacionado_linea"].Value = linea.Cells["line_no"].Value.ToString();
                        drv.Cells["clasificacion"].Value = buscarProducto(linea.Cells["PART_ID"].Value.ToString(), linea.Cells["PRODUCT_CODE"].Value.ToString());
                        drv.Cells["UUIDcol"].Value = linea.Cells["UUID"].Value.ToString();
                    }
                }
            }

        }

        private void buttonAdv5_Click(object sender, EventArgs e)
        {
            eliminarLineaProducto();
        }

        private void eliminarLineaProducto()
        {
            dtgProductos.EndEdit();
            if (dtgProductos.SelectedCells.Count > 0)
            {
                DialogResult Resultado = MessageBox.Show("�Desea eliminar de productos seleccionada?", Application.ProductName + "-" + Application.ProductVersion, MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                if (Resultado.ToString() != "Yes")
                {
                    return;
                }
                ModeloDocumentosFX.ModeloDocumentosFXContainer dbContext = new ModeloDocumentosFXContainer();
                string conection = dbContext.Database.Connection.ConnectionString;
                cCONEXCION oData = new cCONEXCION(conection);
                foreach (DataGridViewRow drv in dtgProductos.SelectedRows)
                {
                    try
                    {
                        eliminarLineaProducto(drv, oData);
                        drv.Cells["PART_ID"].Value = "";
                        drv.Cells["invoice_id_relacionado"].Value = "";
                        drv.Cells["invoice_id_relacionado_linea"].Value = "";
                        drv.Cells["clasificacion"].Value = "";
                    }
                    catch (Exception e)
                    {
                        Bitacora.Log(e.Message.ToString());
                    }

                }
            }
            else
            {
                MessageBox.Show("Seleccione alguna l�nea.", Application.ProductName + "-" + Application.ProductVersion, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            DescargarPlatnillaAsociacionMasiva();
        }
        private void DescargarPlatnillaAsociacionMasiva()
        {
            var fileName = " Plantilla de Descarga Masiva " + DateTime.Now.ToString("yyyy-MM-dd-hh-mm-ss") + ".xlsx";
            if (folderBrowserDialog1.ShowDialog() == DialogResult.OK)
            {
                fileName = folderBrowserDialog1.SelectedPath + "\\" + fileName;
                System.IO.Stream myStream = new System.IO.FileStream(fileName, System.IO.FileMode.Create, System.IO.FileAccess.ReadWrite, System.IO.FileShare.None);

                using (ExcelPackage pck = new ExcelPackage())
                {

                    ExcelWorksheet ws1 = pck.Workbook.Worksheets.Add("Plantilla");

                    ws1.Cells[1, 1].Value = "Factura Relacionada Origen";
                    ws1.Cells[1, 2].Value = "UUID";
                    ws1.Cells[1, 3].Value = "Factura";
                    ws1.Cells[1, 4].Value = "Tipo Relacion";
                    ws1.Cells[1, 5].Value = "Clasificador";
                    ws1.Cells[1, 6].Value = "Producto";

                    pck.SaveAs(myStream);
                }
                myStream.Close();
                MessageBox.Show(this, "Creaci�n de plantilla realizada " + Environment.NewLine + fileName + " finalizada.", Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Importar();
        }

        private void Importar()
        {
            Application.DoEvents();
            OpenFileDialog openFileDialog1 = new OpenFileDialog();
            openFileDialog1.Filter = "Excel files (*.xlsx)|*.xlsx|All files (*.*)|*.*";
            openFileDialog1.FilterIndex = 1;
            openFileDialog1.RestoreDirectory = true;

            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                try
                {
                    Procesar( 1, openFileDialog1.FileName);
                }
                catch (Exception)
                {
                    MessageBox.Show("No existe el Archivo seleccionado", Application.ProductName + "-" + Application.ProductVersion, MessageBoxButtons.OK);
                }
                finally
                {
                    Application.DoEvents();
                }
            }
        }

        private void Procesar(int hoja, string archivo)
        {
            string tmpFile = Path.GetTempFileName();
            try
            {
                if (!File.Exists(archivo))
                {
                    MessageBox.Show(this, "El archivo " + archivo + " no existe.", Application.ProductName
                        , MessageBoxButtons.OK, MessageBoxIcon.Error);
                }

                File.Delete(tmpFile);
                File.Copy(archivo, tmpFile);
                int rowIndex = 2;
                FileInfo existingFile = new FileInfo(tmpFile);
                using (ExcelPackage package = new ExcelPackage(existingFile))
                {
                    ExcelWorksheet worksheet = package.Workbook.Worksheets[hoja];
                    if (worksheet.Cells[rowIndex, 1].Value != null)
                    {
                        try
                        {
                            foreach (DataGridViewRow linea in dtgProductos.Rows)
                            {
                                if (worksheet.Cells[rowIndex, 1].Value != null)
                                {
                                    dtgProductos.Rows[linea.Index].Cells["LINE"].Value = worksheet.Cells[rowIndex, 1].Value;
                                    dtgProductos.Rows[linea.Index].Cells["REFERENCE"].Value = worksheet.Cells[rowIndex, 2].Value;
                                    dtgProductos.Rows[linea.Index].Cells["AMOUNT"].Value = worksheet.Cells[rowIndex, 3].Value;
                                    dtgProductos.Rows[linea.Index].Cells["invoice_id_relacionado"].Value = worksheet.Cells[rowIndex, 4].Value;
                                    dtgProductos.Rows[linea.Index].Cells["invoice_id_relacionado_linea"].Value = worksheet.Cells[rowIndex, 5].Value;
                                    dtgProductos.Rows[linea.Index].Cells["PART_ID"].Value = worksheet.Cells[rowIndex, 6].Value;
                                    dtgProductos.Rows[linea.Index].Cells["clasificacion"].Value = worksheet.Cells[rowIndex, 7].Value;
                                    rowIndex++;
                                }
                            }
                        }
                        catch (DbEntityValidationException e)
                        {
                            foreach (var eve in e.EntityValidationErrors)
                            {
                                foreach (var ve in eve.ValidationErrors)
                                {
                                    MessageBox.Show(this, ve.PropertyName + " Error: " + ve.ErrorMessage, Application.ProductName
                                        , MessageBoxButtons.OK, MessageBoxIcon.Error);
                                }
                            }
                        }
                        return;
                    }
                }
            }
            catch (Exception e)
            {
                ErrorFX.mostrar(e, true, false,
                "frmConversionUM - 150 - ", false
                );
            }
            finally
            {
                File.Delete(tmpFile);
            }

        }

    }
}
