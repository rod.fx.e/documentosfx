﻿using Generales;
using OfficeOpenXml;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ClasificadorProductos.Clases
{
    public class RelacionFacturas
    {
        public void Generar()
        {
            try
            {
                FolderBrowserDialog folderBrowserDialog1 = new FolderBrowserDialog();
                var fileName = " Plantilla de Relacion " + DateTime.Now.ToString("yyyy-MM-dd-hh-mm-ss") + ".xlsx";
                if (folderBrowserDialog1.ShowDialog() == DialogResult.OK)
                {
                    fileName = folderBrowserDialog1.SelectedPath + "\\" + fileName;
                    System.IO.Stream myStream = new System.IO.FileStream(fileName, System.IO.FileMode.Create, System.IO.FileAccess.ReadWrite, System.IO.FileShare.None);

                    using (ExcelPackage pck = new ExcelPackage())
                    {

                        ExcelWorksheet ws1 = pck.Workbook.Worksheets.Add("Plantilla");
                        ws1.Cells[1, 1].Value = "Línea";
                        ws1.Cells[1, 2].Value = "Referencia";
                        ws1.Cells[1, 3].Value = "Monto";
                        ws1.Cells[1, 4].Value = "Factura asociada";
                        ws1.Cells[1, 5].Value = "Línea asociada";
                        ws1.Cells[1, 6].Value = "Producto";
                        ws1.Cells[1, 7].Value = "Clasificación";
                        pck.SaveAs(myStream);
                    }
                    myStream.Close();
                    MessageBox.Show("Creación de plantilla realizada " + Environment.NewLine + fileName + " finalizada.", Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
            catch (Exception e)
            {
                ErrorFX.mostrar(e, true, false,
                "RelacionFacturas - 56 - ", false
                    );
            }
        }

    }
}
