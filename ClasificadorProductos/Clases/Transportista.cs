﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClasificadorProductos.Clases
{
    public class Transportista
    {
        public Transportista()
        {
        }

        public Transportista(int id, string tipos, string rFC, string numeroLicencia, 
            string nombre, string residencia, string domicilio, string estado, string pais, string codigoPostal)
        {
            Id = id;
            Tipos = tipos;
            RFC = rFC;
            NumeroLicencia = numeroLicencia;
            Nombre = nombre;
            Residencia = residencia;
            Domicilio = domicilio;
            Estado = estado;
            Pais = pais;
            CodigoPostal = codigoPostal;
        }

        public int Id { get; set; }
        public string Tipos { get; set; }
        public string RFC { get; set; }
        public string NumeroLicencia { get; set; }
        public string Nombre { get; set; }
        public string Residencia { get; set; }
        public string Domicilio { get; set; }
        public string Estado { get; set; }
        public string Pais { get; set; }
        public string CodigoPostal { get; set; }
    }
}
