﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClasificadorProductos.Clases
{
    public class Transporte
    {
        public Transporte()
        {
        }

        public Transporte(string placa, string anio)
        {
            Placa = placa;
            Anio = anio;
        }

        public int Id { get; set; }
        public string PermisoSCT { get; set; }
        public string NumeroPermisoSCT { get; set; }
        public string Placa { get; set; }
        public string Anio { get; set; }
        public string AseguradoraResponsabilidadCivil { get; set; }
        public string PolizaResponsabilidadCivil { get; set; }
        public string AseguradoraMedioAmbiente { get; set; }
        public string PolizaMedioAmbiente { get; set; }
        public string AseguradoraCarga { get; set; }
        public string PolizaCarga { get; set; }
        public string ValorMateriaPrimaSeguro { get; set; }
        public string SubtipoRemolque { get; set; }
        public string PlacaRemolque { get; set; }
    }
}
