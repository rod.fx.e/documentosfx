
-- --------------------------------------------------
-- Entity Designer DDL Script for SQL Server 2005, 2008, 2012 and Azure
-- --------------------------------------------------
-- Date Created: 04/29/2024 11:13:42
-- Generated from EDMX file: D:\Proyectos\DocumentosFX\SAT\ImportarCatalogos\ModeloSincronizacion\ModeloSincronizacion.edmx
-- --------------------------------------------------

SET QUOTED_IDENTIFIER OFF;
GO
USE [documentosfx];
GO
IF SCHEMA_ID(N'dbo') IS NULL EXECUTE(N'CREATE SCHEMA [dbo]');
GO

-- --------------------------------------------------
-- Dropping existing FOREIGN KEY constraints
-- --------------------------------------------------


-- --------------------------------------------------
-- Dropping existing tables
-- --------------------------------------------------

IF OBJECT_ID(N'[dbo].[ocSet]', 'U') IS NOT NULL
    DROP TABLE [dbo].[ocSet];
GO
IF OBJECT_ID(N'[dbo].[socioNegocioSyncSet]', 'U') IS NOT NULL
    DROP TABLE [dbo].[socioNegocioSyncSet];
GO
IF OBJECT_ID(N'[dbo].[documentoSet]', 'U') IS NOT NULL
    DROP TABLE [dbo].[documentoSet];
GO
IF OBJECT_ID(N'[dbo].[payableSyncSet]', 'U') IS NOT NULL
    DROP TABLE [dbo].[payableSyncSet];
GO
IF OBJECT_ID(N'[dbo].[archivoSyncSet]', 'U') IS NOT NULL
    DROP TABLE [dbo].[archivoSyncSet];
GO
IF OBJECT_ID(N'[dbo].[AdminPaqConfigSet]', 'U') IS NOT NULL
    DROP TABLE [dbo].[AdminPaqConfigSet];
GO
IF OBJECT_ID(N'[dbo].[pagoProveedorLineaSet]', 'U') IS NOT NULL
    DROP TABLE [dbo].[pagoProveedorLineaSet];
GO
IF OBJECT_ID(N'[dbo].[receiverSyncSet]', 'U') IS NOT NULL
    DROP TABLE [dbo].[receiverSyncSet];
GO

-- --------------------------------------------------
-- Creating all tables
-- --------------------------------------------------

-- Creating table 'ocSet'
CREATE TABLE [dbo].[ocSet] (
    [rowId] int IDENTITY(1,1) NOT NULL,
    [id] nvarchar(30)  NOT NULL,
    [vendorId] nvarchar(30)  NOT NULL,
    [rfc] nvarchar(30)  NOT NULL,
    [proceso] tinyint  NULL
);
GO

-- Creating table 'socioNegocioSyncSet'
CREATE TABLE [dbo].[socioNegocioSyncSet] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [razon] nvarchar(250)  NOT NULL,
    [proceso] tinyint  NULL,
    [rfc] nvarchar(20)  NOT NULL,
    [email] nvarchar(250)  NULL,
    [idERP] nvarchar(max)  NULL
);
GO

-- Creating table 'documentoSet'
CREATE TABLE [dbo].[documentoSet] (
    [rowId] int IDENTITY(1,1) NOT NULL,
    [id] nvarchar(30)  NOT NULL,
    [vendorId] nvarchar(30)  NOT NULL,
    [rfc] nvarchar(30)  NOT NULL,
    [proceso] tinyint  NULL,
    [estado] tinyint  NULL
);
GO

-- Creating table 'payableSyncSet'
CREATE TABLE [dbo].[payableSyncSet] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [voucher] nvarchar(50)  NOT NULL,
    [invoice] nvarchar(50)  NOT NULL,
    [oc] nvarchar(50)  NULL,
    [rfc] nvarchar(20)  NOT NULL,
    [proceso] tinyint  NULL,
    [invoiceDate] datetime  NOT NULL,
    [estado] nvarchar(20)  NOT NULL,
    [fechaProgramada] datetime  NOT NULL
);
GO

-- Creating table 'archivoSyncSet'
CREATE TABLE [dbo].[archivoSyncSet] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [invoice] nvarchar(50)  NULL,
    [rfcReceptor] nvarchar(50)  NULL,
    [rfcEmisor] nvarchar(50)  NULL,
    [folio] nvarchar(50)  NULL,
    [xml] nvarchar(250)  NULL,
    [proceso] tinyint  NULL,
    [pdf] nvarchar(250)  NULL,
    [UUID] nvarchar(max)  NULL,
    [idDocumentoSet] nvarchar(max)  NOT NULL,
    [tipoComprobante] nvarchar(15)  NULL,
    [oc] nvarchar(30)  NULL,
    [recibo] nvarchar(30)  NULL
);
GO

-- Creating table 'AdminPaqConfigSet'
CREATE TABLE [dbo].[AdminPaqConfigSet] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [rfc] nvarchar(max)  NOT NULL,
    [directorio] nvarchar(max)  NOT NULL,
    [codigo] nvarchar(max)  NOT NULL,
    [rfcProveedor] nvarchar(max)  NULL,
    [bdSQL] nvarchar(max)  NULL,
    [servidor] nvarchar(max)  NULL,
    [usuario] nvarchar(max)  NULL,
    [password] nvarchar(max)  NULL,
    [tipo] nvarchar(max)  NULL
);
GO

-- Creating table 'pagoProveedorLineaSet'
CREATE TABLE [dbo].[pagoProveedorLineaSet] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [factura] nvarchar(50)  NOT NULL,
    [UUID] nvarchar(50)  NOT NULL,
    [MonedaDR] nvarchar(max)  NOT NULL,
    [NumParcialidad] nvarchar(max)  NULL,
    [ImpSaldoAnt] decimal(18,4)  NULL,
    [ImpPagado] decimal(18,4)  NULL,
    [ImpSaldoInsoluto] decimal(18,8)  NULL,
    [TipoCambioDR] decimal(18,4)  NULL,
    [folio] nvarchar(15)  NULL,
    [serie] nvarchar(15)  NULL,
    [archivoSyncId] int  NOT NULL
);
GO

-- Creating table 'receiverSyncSet'
CREATE TABLE [dbo].[receiverSyncSet] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [oc] nvarchar(50)  NULL,
    [rfc] nvarchar(20)  NOT NULL,
    [proceso] tinyint  NULL,
    [receiver] nvarchar(max)  NOT NULL,
    [monto] decimal(18,4)  NULL
);
GO

-- --------------------------------------------------
-- Creating all PRIMARY KEY constraints
-- --------------------------------------------------

-- Creating primary key on [rowId] in table 'ocSet'
ALTER TABLE [dbo].[ocSet]
ADD CONSTRAINT [PK_ocSet]
    PRIMARY KEY CLUSTERED ([rowId] ASC);
GO

-- Creating primary key on [Id] in table 'socioNegocioSyncSet'
ALTER TABLE [dbo].[socioNegocioSyncSet]
ADD CONSTRAINT [PK_socioNegocioSyncSet]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [rowId] in table 'documentoSet'
ALTER TABLE [dbo].[documentoSet]
ADD CONSTRAINT [PK_documentoSet]
    PRIMARY KEY CLUSTERED ([rowId] ASC);
GO

-- Creating primary key on [Id] in table 'payableSyncSet'
ALTER TABLE [dbo].[payableSyncSet]
ADD CONSTRAINT [PK_payableSyncSet]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'archivoSyncSet'
ALTER TABLE [dbo].[archivoSyncSet]
ADD CONSTRAINT [PK_archivoSyncSet]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'AdminPaqConfigSet'
ALTER TABLE [dbo].[AdminPaqConfigSet]
ADD CONSTRAINT [PK_AdminPaqConfigSet]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'pagoProveedorLineaSet'
ALTER TABLE [dbo].[pagoProveedorLineaSet]
ADD CONSTRAINT [PK_pagoProveedorLineaSet]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'receiverSyncSet'
ALTER TABLE [dbo].[receiverSyncSet]
ADD CONSTRAINT [PK_receiverSyncSet]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- --------------------------------------------------
-- Creating all FOREIGN KEY constraints
-- --------------------------------------------------

-- --------------------------------------------------
-- Script has ended
-- --------------------------------------------------