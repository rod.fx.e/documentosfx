//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace ModeloSincronizacion
{
    using System;
    using System.Collections.Generic;
    
    public partial class oc
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public oc()
        {
            this.proceso = 0;
        }
    
        public int rowId { get; set; }
        public string id { get; set; }
        public string vendorId { get; set; }
        public string rfc { get; set; }
        public Nullable<byte> proceso { get; set; }
    }
}
