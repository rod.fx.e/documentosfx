﻿using DevComponents.DotNetBar;
using Generales;
using ModeloDocumentosFX;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Desarrollo
{

    class cCheques
    {
        private string sSQL = "";
        public string ROW_ID { get; set; }

        public string ROW_ID_TRANSACCION { get; set; }

        public string numCtaField { get; set; }
        public string bancoField { get; set; }
        public string ctaOriField { get; set; }
        public DateTime fechaField { get; set; }
        public double montoField { get; set; }
        public string benefField { get; set; }
        public string rFCField { get; set; }


        
        private cCONEXCION oData = new cCONEXCION();
        public override string ToString() { return ROW_ID; }

        public cCheques()
        {
            ModeloDocumentosFXContainer dbContext = new ModeloDocumentosFXContainer();
            try
            {
                string conection = dbContext.Database.Connection.ConnectionString;
                oData = new cCONEXCION(conection);
            }
            catch (Exception err)
            {
                ErrorFX.mostrar(err, false, true, "frmEmpresa - 96 - ", false);
            }
            limpiar();
        }

        public void limpiar()
        {
            ROW_ID = "";
            ROW_ID_TRANSACCION = "";
            numCtaField="";
            fechaField = DateTime.Now;
            montoField = 0;
            bancoField="";
            ctaOriField="";
            benefField="";
            rFCField="";
        }


        public List<cCheques> todos(string ROW_ID_TRANSACCIONp,string numCtaFieldp = "")
        {

            List<cCheques> lista = new List<cCheques>();

            sSQL = " SELECT * ";
            sSQL += " FROM PolizasPolizaTransaccionCheque ";
            sSQL += " WHERE 1=1 AND ROW_ID_TRANSACCION=" + ROW_ID_TRANSACCIONp  + "";
            sSQL += " AND numCtaField LIKE '%" + numCtaFieldp + "%' ";
            sSQL += " ORDER BY numCtaField";
            DataTable oDataTable = oData.EjecutarConsulta(sSQL);
            foreach (DataRow oDataRow in oDataTable.Rows)
            {
                cCheques ocCatalogo = new cCheques();
                ocCatalogo.cargar(oDataRow["ROW_ID"].ToString());
                lista.Add(ocCatalogo);
            }
            return lista;

        }

        public bool verificar(string numCtaFieldp, string ROW_ID_TRANSACCIONp)
        {
            sSQL = " SELECT * ";
            sSQL += " FROM PolizasPolizaTransaccionCheque ";
            sSQL += " WHERE numCtaField='" + numCtaFieldp + "' ";
            sSQL += " AND ROW_ID_TRANSACCION=" + ROW_ID_TRANSACCIONp + "";
            DataTable oDataTable = oData.EjecutarConsulta(sSQL);
            if (oDataTable != null)
            {
                foreach (DataRow oDataRow in oDataTable.Rows)
                {                    
                    return true;
                }
            }
            return false;
        }

        public bool cargar(string ROW_IDp)
        {
            sSQL = " SELECT * ";
            sSQL += " FROM PolizasPolizaTransaccionCheque ";
            sSQL += " WHERE ROW_ID=" + ROW_IDp + " ";
            DataTable oDataTable = oData.EjecutarConsulta(sSQL);
            if (oDataTable != null)
            {
                foreach (DataRow oDataRow in oDataTable.Rows)
                {
                    //Añadir el Registro
                    ROW_ID = oDataRow["ROW_ID"].ToString();
                    ROW_ID_TRANSACCION = oDataRow["ROW_ID_TRANSACCION"].ToString();
                    numCtaField = oDataRow["numCtaField"].ToString();
                    bancoField = oDataRow["bancoField"].ToString();
                    ctaOriField = oDataRow["ctaOriField"].ToString();
                    fechaField = DateTime.Parse(oDataRow["fechaField"].ToString());
                    montoField = double.Parse(oDataRow["montoField"].ToString());
                    benefField = oDataRow["benefField"].ToString();
                    rFCField = oDataRow["rFCField"].ToString();
                    return true;
                }
            }
            return false;
        }

        public bool guardar(bool validar=true)
        {
            if (validar)
            {

                if (numCtaField.Trim() == "")
                {
                    MessageBoxEx.Show("La cuenta es requerida.", Application.ProductName + "-" + Application.ProductVersion, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                    return false;
                }
                
            }
            
            if (ROW_ID == "")
            {
                sSQL = " INSERT INTO PolizasPolizaTransaccionCheque ";
                sSQL += " ( ";
                sSQL += " [ROW_ID_TRANSACCION],[numCtaField],[bancoField]";
                sSQL += ",[montoField],[ctaOriField],[fechaField] ";
                sSQL += ",[benefField],[rFCField]";
                sSQL += " )";
                sSQL += " VALUES ";
                sSQL += "(";
                sSQL += " " + ROW_ID_TRANSACCION + ",'" + numCtaField + "','" + bancoField + "'";
                sSQL += "," + montoField.ToString("N4").Replace(",", "") + ",'" + ctaOriField  + "'," + oData.convertir_fecha(fechaField)+ "";
                sSQL += ",'" + benefField + "','" + rFCField + "' ";
                sSQL += ")";
                DataTable oDataTable = oData.EjecutarConsulta(sSQL);
                if (oDataTable != null)
                {

                    sSQL = "SELECT TOP 1 MAX(ROW_ID) as ROW_ID FROM PolizasPolizaTransaccionCheque";
                    oDataTable = oData.EjecutarConsulta(sSQL);
                    if (oDataTable != null)
                    {
                        foreach (DataRow oDataRow in oDataTable.Rows)
                        {
                            ROW_ID = oDataRow["ROW_ID"].ToString();
                        }
                    }
                }

            }
            else
            {
                sSQL = " UPDATE PolizasPolizaTransaccionCheque ";
                sSQL += " SET ROW_ID_TRANSACCION=" + ROW_ID_TRANSACCION + ",numCtaField='" + numCtaField + "',bancoField='" + bancoField + "'";
                sSQL += ",montoField=" + montoField.ToString("N4").Replace(",", "") + ",ctaOriField='" + ctaOriField + "'";
                sSQL += ",fechaField=" + oData.convertir_fecha(fechaField) + " ";
                sSQL += ",benefField='" + benefField + "',rFCField='" + rFCField + "' ";
                sSQL += " WHERE ROW_ID=" + ROW_ID;
                oData.EjecutarConsulta(sSQL);
            }
            return true;

        }

        public bool eliminar(string ROW_IDp)
        {
            if (ROW_IDp != "")
            {
                sSQL = "DELETE FROM PolizasPolizaTransaccionCheque ";
                sSQL += " WHERE ROW_ID='" + ROW_IDp + "' ";
                oData.EjecutarConsulta(sSQL);
                return true;
            }
            return false;
        }
    }
}
