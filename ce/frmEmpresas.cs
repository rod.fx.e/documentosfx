using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevComponents.DotNetBar;
using System.Threading;
using OfficeOpenXml;
using Generales;

namespace Desarrollo
{
    public partial class frmEmpresas : DevComponents.DotNetBar.Metro.MetroForm
    {
        private cCONEXCION oData = new cCONEXCION("");
        private cCONEXCION oData_ERP = new cCONEXCION("");
        string devolver = "";
        private cEMPRESA_CE oObjeto = new cEMPRESA_CE();
        public DataGridViewSelectedRowCollection selecionados;

        public frmEmpresas(string devolperp="")
        {
            oData = new cCONEXCION();

            devolver = devolperp;
            InitializeComponent();
            oObjeto = new cEMPRESA_CE();
            circularProgress1.Visible = false;
            backgroundWorker1.WorkerReportsProgress = true;
            buttonTop.Enabled = true;
            buttonX1.Enabled = false;

        }

        private void buttonTop_Click(object sender, EventArgs e)
        {
            cargar();
        }

        #region Acciones
        private void cargar()
        {
            circularProgress1.Visible = true;
            circularProgress1.IsRunning = true;            
            if (!backgroundWorker1.IsBusy)
            {              
                dtgrdGeneral.Rows.Clear();                
                backgroundWorker1.RunWorkerAsync(dtgrdGeneral);
                buttonTop.Enabled = false;
                buttonX1.Enabled = true;
            }
        }

        public void modificar()
        {
            if (dtgrdGeneral.SelectedRows.Count > 0)
            {
                DataGridViewSelectedRowCollection arrSelectedRows = dtgrdGeneral.SelectedRows;
                selecionados = arrSelectedRows;
                this.DialogResult = DialogResult.OK;
            }
        }
        #endregion

        private void frmRecursos_Tiempos_Load(object sender, EventArgs e)
        {
            cargar();
        }
        private void BUSCAR_KeyPress(object sender, KeyPressEventArgs e)
        {
            //Precionar Enter buscar
            if (e.KeyChar.ToString() == "\r")
            {
                cargar();
            }
        }

        private void backgroundWorker1_DoWork(object sender, DoWorkEventArgs e)
        {
            try
            {
                int n=0;
                oObjeto = new cEMPRESA_CE();
                foreach (cEMPRESA_CE registro in oObjeto.todos(BUSCAR.Text, BUSCAR_DESCRIPCION.Text))
                {
                    n++;
                    Thread.Sleep(100);
                    // To Report progress.
                    backgroundWorker1.ReportProgress(n, registro);
                    if (backgroundWorker1.CancellationPending)
                    {
                        e.Cancel = true;
                        backgroundWorker1.ReportProgress(0);
                        return;
                    }

                }
            
            }        
            catch (Exception ex)
            {
                MessageBoxEx.Show(ex.Message, Application.ProductName + "-" + Application.ProductVersion, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void backgroundWorker1_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            if (!backgroundWorker1.CancellationPending)
            {
                cEMPRESA_CE registro = (cEMPRESA_CE)e.UserState;
                if (registro != null)
                {
                    int n = dtgrdGeneral.Rows.Add();
                    dtgrdGeneral.Rows[n].Cells["ROW_ID"].Value = registro.ROW_ID;
                    dtgrdGeneral.Rows[n].Cells["ID"].Value = registro.ID;
                    dtgrdGeneral.Rows[n].Cells["RFC"].Value = registro.RFC;
                    circularProgress1.Value = e.ProgressPercentage;
                    circularProgress1.ProgressText = "Procesando.. " + e.ProgressPercentage.ToString();// + " de " + TotalRecords;
                }
            }
        }
        
        private void backgroundWorker1_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            if (e.Cancelled)
            {
                labelItem1.Text = "Cancelado por el usuario...";
                circularProgress1.Value = 0;
            }
            // Check to see if an error occurred in the background process.
            else if (e.Error != null)
            {
                labelItem1.Text = e.Error.Message;
            }
            else
            {
                //BackGround Task Completed with out Error
                labelItem1.Text = " Finalizado...";

            }
            buttonX1.Enabled = false;
            buttonTop.Enabled = true;
            circularProgress1.Visible = false;
        }

        private DataTable GetDataTableFromDGV(DataGridView dgv)
        {
            var dt = new DataTable();
            foreach (DataGridViewColumn column in dgv.Columns)
            {
                //if (column.Visible)
                //{
                // You could potentially name the column based on the DGV column name (beware of dupes)
                // or assign a type based on the data type of the data bound to this DGV column.
                dt.Columns.Add(column.HeaderText);
                //}
            }

            object[] cellValues = new object[dgv.Columns.Count];
            foreach (DataGridViewRow row in dgv.Rows)
            {
                for (int i = 0; i < row.Cells.Count; i++)
                {
                    cellValues[i] = row.Cells[i].Value;
                }
                dt.Rows.Add(cellValues);
            }

            return dt;
        }


        public void exportar()
        {

            var fileName = "Empresas " + DateTime.Now.ToString("yyyy-MM-dd-hh-mm-ss") + ".xlsx";
            if (folderBrowserDialog1.ShowDialog() == DialogResult.OK)
            {
                fileName = folderBrowserDialog1.SelectedPath + "\\" + fileName;
                System.IO.Stream myStream = new System.IO.FileStream(fileName, System.IO.FileMode.Create, System.IO.FileAccess.ReadWrite, System.IO.FileShare.None);

                using (ExcelPackage pck = new ExcelPackage())
                {
                    DataTable oDataTable = new DataTable();

                    string sSQL=" SELECT * FROM EMPRESAS ";

                    oDataTable = oData.EjecutarConsulta(sSQL);
                    ExcelWorksheet ws1 = pck.Workbook.Worksheets.Add("Configuracion");
                    ws1.Cells["A1"].LoadFromDataTable(oDataTable, true);
                    pck.SaveAs(myStream);
                }
                myStream.Close();
                MessageBoxEx.Show("Exportaci�n del archivo " + Environment.NewLine + fileName + " finalizada.", Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Information);
            }

        }



        private void buttonX1_Click(object sender, EventArgs e)
        {
            if (backgroundWorker1.IsBusy)
            {
                backgroundWorker1.CancelAsync();
                buttonX1.Enabled = false;
                buttonTop.Enabled = true;
                
            } 
        }

        private void buttonX2_Click(object sender, EventArgs e)
        {
            modificar();
        }

        private void buttonX2_Click_1(object sender, EventArgs e)
        {
            nuevo();   
        }

        private void nuevo()
        {
            frmEmpresa ofrmConfiguracion_CxP = new frmEmpresa();
            ofrmConfiguracion_CxP.ShowDialog();
            cargar();
        }

        private void dtgrdGeneral_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            editar();
        }

        private void editar()
        {
            dtgrdGeneral.EndEdit();
            if (dtgrdGeneral.SelectedRows.Count > 0)
            {
                foreach (DataGridViewRow drv in dtgrdGeneral.SelectedRows)
                {
                    string ROW_ID = drv.Cells["ROW_ID"].Value.ToString();
                    string ID_tr = drv.Cells["ID"].Value.ToString();
                    mostrar_mensaje("Cargando " + ID_tr, false);
                    frmEmpresa oObjeto = new frmEmpresa(ROW_ID);
                    oObjeto.ShowDialog();
                    cargar();
                }
            }
            else
            {
                MessageBoxEx.Show("Seleccione alguna l�nea.", Application.ProductName + "-" + Application.ProductVersion, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }

        private void mostrar_mensaje(string mensaje, bool mostrar_error)
        {
            ToastNotification.Show(this,
                mensaje,
                mostrar_error ? global::Desarrollo.Properties.Resources.error : global::Desarrollo.Properties.Resources.guardar,
                3000,
                mostrar_error ? eToastGlowColor.Red : eToastGlowColor.Green,
                eToastPosition.TopCenter);
        }

        private void buttonX3_Click(object sender, EventArgs e)
        {
            exportar();
        }
    }
}