using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevComponents.DotNetBar;
using Generales;

namespace Desarrollo
{
    public partial class frmEmpresa_Conexion : DevComponents.DotNetBar.Metro.MetroForm
    {
        bool modificado = false;
        private cCONEXCION oData = new cCONEXCION();
        cEMPRESA_CONEXION oObjeto = new cEMPRESA_CONEXION();
        string ROW_ID_EMPRESA = "";
        string ID = "";
        string RFC = "";

        public frmEmpresa_Conexion(string ROW_ID_EMPRESAp)
        {
            ROW_ID_EMPRESA = ROW_ID_EMPRESAp;
            oObjeto = new cEMPRESA_CONEXION();
            InitializeComponent();
            
        }
        public frmEmpresa_Conexion(string ROW_IDp, string ROW_ID_EMPRESAp)
        {
            ROW_ID_EMPRESA = ROW_ID_EMPRESAp;
            oObjeto = new cEMPRESA_CONEXION();
            oObjeto.ROW_ID = ROW_IDp;
            InitializeComponent();
            cargar(ROW_IDp);
        }

        private void cargar(string ROW_IDp)
        {
            if (oObjeto.cargar(ROW_IDp))
            {

                VISUAL_TIPO.Text = oObjeto.VISUAL_TIPO;
                VISUAL_SERVIDOR.Text = oObjeto.VISUAL_SERVIDOR;
                VISUAL_BD.Text = oObjeto.VISUAL_BD;
                VISUAL_USUARIO.Text = oObjeto.VISUAL_USUARIO;
                VISUAL_PASSWORD.Text = oObjeto.VISUAL_PASSWORD;
                VISUAL_VERSION.Text = oObjeto.VISUAL_VERSION;
                VISUAL_CURRENCY_ID.Text = oObjeto.VISUAL_CURRENCY_ID;
                VISUAL_ENTITY_ID.Text = oObjeto.VISUAL_ENTITY_ID;
                VISUAL_SITE_ID.Enabled = true;
                VISUAL_SITE_ID.Text = oObjeto.VISUAL_SITE_ID;

                auxiliarBaseDatos.Text=oObjeto.VISUAL_BD_AUXILIAR;



                this.auxiliarTipo.Text = oObjeto.auxiliarTipo;
                this.auxiliarServidor.Text = oObjeto.auxiliarServidor;
                this.auxiliarBaseDatos.Text = oObjeto.auxiliarBaseDatos;
                this.auxiliarBaseDatos.Text = oObjeto.VISUAL_BD_AUXILIAR;
                this.auxiliarUsuario.Text = oObjeto.auxiliarUsuario;
                this.auxiliarPassword.Text = oObjeto.auxiliarPassword;

                VISUAL_SITE_ID.Text = "";
                VISUAL_SITE_ID.Enabled = false;
                if (
                    (VISUAL_VERSION.Text == "7.1.0")
                    | (VISUAL_VERSION.Text == "7.1.2")
                    | (VISUAL_VERSION.Text == "7.1.1")
                    | (VISUAL_VERSION.Text == "8.0.0")
                )
                {
                    VISUAL_SITE_ID.Text = oObjeto.VISUAL_SITE_ID;
                    VISUAL_SITE_ID.Enabled = true;
                }


                toolStripStatusLabel1.Text = "Datos Actualizados " + DateTime.Now.ToShortDateString() + " " + DateTime.Now.ToLongTimeString();
                modificado = false;
            }
        }
        private void buttonX2_Click(object sender, EventArgs e)
        {
            limpiar();
        }

        private void limpiar()
        {
            if (modificado)
            {

                DialogResult Resultado = MessageBoxEx.Show("�Desea guardar los cambios?", Application.ProductName + "-" + Application.ProductVersion, MessageBoxButtons.YesNo);
                if (Resultado.ToString() == "Yes")
                {
                    guardar();
                }


            }
            modificado = false;
            oObjeto = new cEMPRESA_CONEXION();

            foreach (var c in this.Controls)
            {
                if (c is TextBox)
                {
                    ((TextBox)c).Text = "";
                }
            }
        }

        private void buttonX1_Click(object sender, EventArgs e)
        {
            guardar();
        }

        private void guardar()
        {
            oObjeto.ROW_ID_EMPRESA = ROW_ID_EMPRESA;
            oObjeto.VISUAL_TIPO = VISUAL_TIPO.Text;
            oObjeto.VISUAL_SERVIDOR = VISUAL_SERVIDOR.Text;
            oObjeto.VISUAL_BD = VISUAL_BD.Text;
            oObjeto.VISUAL_USUARIO = VISUAL_USUARIO.Text;
            oObjeto.VISUAL_PASSWORD = VISUAL_PASSWORD.Text;
            oObjeto.VISUAL_VERSION = VISUAL_VERSION.Text;
            oObjeto.VISUAL_CURRENCY_ID = VISUAL_CURRENCY_ID.Text;
            oObjeto.VISUAL_ENTITY_ID = VISUAL_ENTITY_ID.Text;
            oObjeto.VISUAL_SITE_ID = VISUAL_SITE_ID.Text;
            oObjeto.VISUAL_BD_AUXILIAR = auxiliarBaseDatos.Text;

            oObjeto.auxiliarTipo = this.auxiliarTipo.Text;
            oObjeto.auxiliarServidor = this.auxiliarServidor.Text;
            oObjeto.auxiliarBaseDatos = this.auxiliarBaseDatos.Text;
            oObjeto.VISUAL_BD_AUXILIAR = this.auxiliarBaseDatos.Text;
            oObjeto.auxiliarUsuario = this.auxiliarUsuario.Text;
            oObjeto.auxiliarPassword = this.auxiliarPassword.Text;

            if (oObjeto.guardar())
            {
                toolStripStatusLabel1.Text = "Datos Actualizados " + DateTime.Now.ToShortDateString() + " " + DateTime.Now.ToLongTimeString();
                modificado = false;
            }
        }

        private void buttonX3_Click(object sender, EventArgs e)
        {
            eliminar();
        }

        private void eliminar()
        {

            DialogResult Resultado = MessageBoxEx.Show("�Desea eliminar?", Application.ProductName + "-" + Application.ProductVersion, MessageBoxButtons.YesNo);
            if (Resultado.ToString() == "Yes")
            {
                if (oObjeto.eliminar(oObjeto.ROW_ID))
                {
                    modificado = false;
                    limpiar();
                }
            }
        }

        private void frmEmpresa_KeyPress(object sender, KeyPressEventArgs e)
        {
            modificado = true;
        }

        private void buttonX4_Click(object sender, EventArgs e)
        {
            verificar_conexion();
        }

        private void verificar_conexion()
        {
            //Realizar la conexcion
            oObjeto.VISUAL_TIPO = VISUAL_TIPO.Text;
            oObjeto.VISUAL_SERVIDOR = VISUAL_SERVIDOR.Text;
            oObjeto.VISUAL_BD = VISUAL_BD.Text;
            oObjeto.VISUAL_USUARIO = VISUAL_USUARIO.Text;
            oObjeto.VISUAL_PASSWORD = VISUAL_PASSWORD.Text;
            cCONEXCION ocCONEXCION=oObjeto.conectar();
            if (ocCONEXCION != null)
            {
                toolStripStatusLabel1.Text = "Base de datos Visual " + DateTime.Now.ToShortDateString() + " " + DateTime.Now.ToLongTimeString();

                MessageBoxEx.Show("Base de datos Visual conectada " + DateTime.Now.ToShortDateString() + " " + DateTime.Now.ToLongTimeString(), Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Information);


                //Traer los datos del Visual en el ID, RFC y las demas versiones
                string sSQL = "SELECT * FROM APPLICATION_GLOBAL";
                DataTable oDataTable = ocCONEXCION.EjecutarConsulta(sSQL);
                foreach (DataRow oDataRow in oDataTable.Rows)
                {
                    VISUAL_VERSION.Text=oDataRow["DBVERSION"].ToString();
                    ID = oDataRow["COMPANY_NAME"].ToString();
                    if (oDataTable.Columns.Contains("TAX_ID_NUMBER"))
                    {
                        RFC = oDataRow["TAX_ID_NUMBER"].ToString();
                    }

                }
                VISUAL_CURRENCY_ID.Items.Clear();
                sSQL = "SELECT * FROM CURRENCY";
                oDataTable = ocCONEXCION.EjecutarConsulta(sSQL);
                foreach (DataRow oDataRow in oDataTable.Rows)
                {
                    VISUAL_CURRENCY_ID.Items.Add(oDataRow["ID"].ToString());
                }
                if (VISUAL_CURRENCY_ID.SelectedIndex > 0)
                {
                    VISUAL_CURRENCY_ID.SelectedIndex = 0;
                }
                    

                VISUAL_SITE_ID.Text = "";
                VISUAL_SITE_ID.Enabled = false;
                if (String.IsNullOrEmpty(VISUAL_VERSION.Text))
                {
                    MessageBoxEx.Show("La versi�n del Visual no existe " + DateTime.Now.ToShortDateString() + " " + DateTime.Now.ToLongTimeString()
                        , Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error);

                    return;
                }
                int versionInt = int.Parse(VISUAL_VERSION.Text[0].ToString());
                if (versionInt >= 7 )
                {
                    VISUAL_SITE_ID.Enabled = true;
                    //Traer los datos del Visual en el ID, RFC y las demas versiones
                    sSQL = "SELECT * FROM [SITE]";
                    VISUAL_SITE_ID.Items.Clear();
                    oDataTable = ocCONEXCION.EjecutarConsulta(sSQL);
                    foreach (DataRow oDataRow in oDataTable.Rows)
                    {
                        VISUAL_SITE_ID.Items.Add(oDataRow["ID"].ToString());
                    }
                    //Buscar el RFC
                    sSQL = "SELECT VAT_REGISTRATION FROM ACCOUNTING_ENTITY";
                    RFC = "";
                    oDataTable = ocCONEXCION.EjecutarConsulta(sSQL);
                    foreach (DataRow oDataRow in oDataTable.Rows)
                    {
                        RFC = oDataRow["VAT_REGISTRATION"].ToString();
                    }

                    VISUAL_ENTITY_ID.Items.Clear();
                    sSQL = "SELECT * FROM ACCOUNTING_ENTITY";
                    oDataTable = ocCONEXCION.EjecutarConsulta(sSQL);
                    foreach (DataRow oDataRow in oDataTable.Rows)
                    {
                        VISUAL_ENTITY_ID.Items.Add(oDataRow["ID"].ToString());
                    }
                    if (VISUAL_ENTITY_ID.SelectedIndex > 0)
                    {
                        VISUAL_ENTITY_ID.SelectedIndex = 0;
                    }
                }
                else
                {
                    VISUAL_ENTITY_ID.Items.Clear();
                     sSQL = "SELECT * FROM ENTITY";
                     oDataTable = ocCONEXCION.EjecutarConsulta(sSQL);
                    foreach (DataRow oDataRow in oDataTable.Rows)
                    {
                        VISUAL_ENTITY_ID.Items.Add(oDataRow["ID"].ToString());
                    }
                    if (VISUAL_ENTITY_ID.SelectedIndex > 0)
                    {
                        VISUAL_ENTITY_ID.SelectedIndex = 0;
                    }
                }
            }
            else
            {
                MessageBoxEx.Show("Error conectandose a " + oObjeto.VISUAL_SERVIDOR + " Base de datos Visual " + oObjeto.VISUAL_BD + DateTime.Now.ToShortDateString() + " " + DateTime.Now.ToLongTimeString()
                    , Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        private void buttonX5_Click(object sender, EventArgs e)
        {
            verificar_conexion_auxiliar();
        }


        private void verificar_conexion_auxiliar()
        {
            //Realizar la conexcion
            oObjeto.auxiliarTipo = this.auxiliarTipo.Text;
            oObjeto.auxiliarServidor = this.auxiliarServidor.Text;
            oObjeto.auxiliarBaseDatos = this.auxiliarBaseDatos.Text;
            oObjeto.VISUAL_BD_AUXILIAR = this.auxiliarBaseDatos.Text;
            oObjeto.auxiliarUsuario = this.auxiliarUsuario.Text;
            oObjeto.auxiliarPassword = this.auxiliarPassword.Text;
            cCONEXCION ocCONEXCION = oObjeto.conectarAuxiliar();
            if (ocCONEXCION != null)
            {
                MessageBoxEx.Show("Base de datos Auxiliar conectada " + DateTime.Now.ToShortDateString() + " " + DateTime.Now.ToLongTimeString(), Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            else
            {
                MessageBoxEx.Show("Error conectandose a " + oObjeto.auxiliarServidor  + " Base de datos Auxiliar " + oObjeto.auxiliarBaseDatos + DateTime.Now.ToShortDateString() + " " + DateTime.Now.ToLongTimeString()
                    , Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void verPassword_CheckedChanged(object sender, EventArgs e)
        {
            if (verPassword.Checked)
            {
                this.VISUAL_PASSWORD.UseSystemPasswordChar = false;
            }
            else
            {
                VISUAL_PASSWORD.UseSystemPasswordChar = true;
            }
        }


        private void checkBox1_CheckedChanged_1(object sender, EventArgs e)
        {
            if (checkBox1.Checked)
            {
                this.auxiliarPassword.UseSystemPasswordChar = false;
            }
            else
            {
                auxiliarPassword.UseSystemPasswordChar = true;
            }
        }

        private void buttonX6_Click(object sender, EventArgs e)
        {
            Duplicar();
        }

        private void Duplicar()
        {
            oObjeto.ROW_ID = "";
        }
    }
}