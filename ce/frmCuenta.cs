using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevComponents.DotNetBar;
using System.IO;
using OfficeOpenXml;

namespace Desarrollo
{
    public partial class frmCuenta : DevComponents.DotNetBar.Metro.MetroForm
    {
        bool modificado = false;
        string ROW_ID_CATALOGO;
        cCatalogoCtas oObjeto = new cCatalogoCtas();
        
        public frmCuenta(string ROW_ID_CATALOGOp)
        {
            oObjeto = new cCatalogoCtas();
            ROW_ID_CATALOGO = ROW_ID_CATALOGOp;
            InitializeComponent();
            cargar_agrupador();
            cargar_naturaleza();
            limpiar();
        }

        public frmCuenta(string ROW_ID_CATALOGOp,string ROW_ID)
        {
            ROW_ID_CATALOGO = ROW_ID_CATALOGOp;
            oObjeto = new cCatalogoCtas();
            InitializeComponent();
            cargar_agrupador();
            cargar_naturaleza();
            cargar(ROW_ID);
        }

        #region Acciones
        private void cargar(string ROW_IDp)
        {
            if (oObjeto.cargar(ROW_IDp))
            {
                ROW_ID_CATALOGO = oObjeto.ROW_ID_CATALOGO;
                codAgrupField.SelectedValue=oObjeto.codAgrupField;
                numCtaField.Text=oObjeto.numCtaField;
                descField.Text = oObjeto.descField;
                subCtaDeField.Text = oObjeto.subCtaDeField;
                nivelField.Text=oObjeto.nivelField;
                naturField.SelectedValue=oObjeto.naturField;
            }

        }

        private void guardar()
        {
            DataRowView selected = (DataRowView)codAgrupField.SelectedItem;
            if (selected==null)
            {
                MessageBoxEx.Show("Selecione un C�digo de Agrupador v�lido.");
                return;
            }

            oObjeto.codAgrupField = selected.Row["VALUE"].ToString();
            oObjeto.numCtaField=numCtaField.Text;
            oObjeto.ROW_ID_CATALOGO = ROW_ID_CATALOGO;
            oObjeto.descField=descField.Text;
            oObjeto.subCtaDeField=subCtaDeField.Text;
            oObjeto.nivelField=nivelField.Text;
            selected = (DataRowView)naturField.SelectedItem;
            oObjeto.naturField = selected.Row["VALUE"].ToString();


            if (oObjeto.guardar())
            {
                labelItem1.Text = "Datos Actualizados " + DateTime.Now.ToShortDateString() + " " + DateTime.Now.ToLongTimeString();
                mostrar_mensaje("Datos Actualizados ", false);
                modificado = false;
            }

        }

        private void mostrar_mensaje(string mensaje, bool mostrar_error)
        {
            ToastNotification.Show(this,
                mensaje,
                mostrar_error ? global::Desarrollo.Properties.Resources.error : global::Desarrollo.Properties.Resources.guardar,
                3000,
                mostrar_error ? eToastGlowColor.Red : eToastGlowColor.Green,
                eToastPosition.TopCenter);
        }
        public void limpiar()
        {
            if (modificado)
            {

                DialogResult Resultado = MessageBoxEx.Show("�Desea guardar los cambios?", Application.ProductName + "-" + Application.ProductVersion, MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                if (Resultado.ToString() == "Yes")
                {
                    guardar();
                }
            }
            modificado = false;
            oObjeto = new cCatalogoCtas();
            codAgrupField.SelectedIndex = 0;
            naturField.SelectedIndex = 0;
            foreach (var c in this.Controls)
            {
                if (c is TextBox)
                {
                    ((TextBox)c).Text = "";
                }
            }
            

        }
        private void cargar_naturaleza()
        {
            //Crear datatable

            DataTable dt = new DataTable();
            dt.Columns.Add(new DataColumn("ID"));
            dt.Columns.Add(new DataColumn("VALUE"));

            DataRow registro = dt.NewRow();
            registro["ID"] = "D - Deudora";
            registro["VALUE"] = "D";
            dt.Rows.Add(registro);

            registro = dt.NewRow();
            registro["ID"] = "A - Acreedora";
            registro["VALUE"] = "A";
            dt.Rows.Add(registro);

            naturField.DataSource = dt;
            naturField.ValueMember = "VALUE";
            naturField.DisplayMember = "ID";

        }
        private void cargar_agrupador()
        {
            //Crear datatable
            codAgrupField.Items.Clear();
            cCatalogoCtas ocCatalogoCtas = new cCatalogoCtas();

            codAgrupField.DataSource = ocCatalogoCtas.obtener_agrupador();
            codAgrupField.ValueMember = "VALUE";
            codAgrupField.DisplayMember = "ID";
        }
        private void eliminar()
        {
            DialogResult Resultado = MessageBoxEx.Show("�Esta seguro de eliminar?", Application.ProductName + "-" + Application.ProductVersion, MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            if (Resultado.ToString() != "Yes")
            {
                return;
            }
            if (oObjeto.eliminar(oObjeto.ROW_ID))
            {
                modificado = false;
                
                MessageBoxEx.Show("Eliminado.", Application.ProductName + "-" + Application.ProductVersion, MessageBoxButtons.OK, MessageBoxIcon.Information);
                limpiar();
                
            }
        }
        #endregion

        private void buttonX3_Click(object sender, EventArgs e)
        {
            eliminar();
        }

        private void buttonX2_Click(object sender, EventArgs e)
        {
            limpiar();
        }

        private void buttonX1_Click(object sender, EventArgs e)
        {
            guardar();
        }

    }
}