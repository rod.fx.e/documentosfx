using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevComponents.DotNetBar;
using System.IO;
using OfficeOpenXml;
using Generales;

namespace Desarrollo
{
    public partial class frmAgrupador : DevComponents.DotNetBar.Metro.MetroForm
    {
        bool modificado = false;
        string ROW_ID_CATALOGO;
        public string codAgrupFieldp="";

        public frmAgrupador()
        {
            InitializeComponent();
            cargar_agrupador();
        }


        #region Acciones
        private void guardar()
        {
            DataRowView selected = (DataRowView)codAgrupField.SelectedItem;
            codAgrupFieldp = selected.Row["VALUE"].ToString();
            DialogResult = DialogResult.OK;
        }

        private void mostrar_mensaje(string mensaje, bool mostrar_error)
        {
            ToastNotification.Show(this,
                mensaje,
                mostrar_error ? global::Desarrollo.Properties.Resources.error : global::Desarrollo.Properties.Resources.guardar,
                3000,
                mostrar_error ? eToastGlowColor.Red : eToastGlowColor.Green,
                eToastPosition.TopCenter);
        }
        
        private void cargar_agrupador()
        {
            //Crear datatable

            //DataTable dt = new DataTable();
            //dt.Columns.Add(new DataColumn("ID"));
            //dt.Columns.Add(new DataColumn("VALUE"));

            //string archivo = Path.GetDirectoryName(Application.ExecutablePath)+@"\configuracion\Configuracion.xlsx";
            //if (!File.Exists(archivo))
            //{
            //    ErrorFX.mostrar("El archivo de Configuracion en la ruta " + archivo + " no existe",true,false,false);
            //    return;
            //}
            //string fileName = Path.ChangeExtension(Path.GetTempFileName(), "tr5");
            //File.Copy(archivo,fileName);
            //FileInfo existingFile = new FileInfo(fileName);
            //ExcelPackage package = new ExcelPackage(existingFile);
            //ExcelWorksheet worksheet = package.Workbook.Worksheets[1];
            //int rowIndex = 2;
            //codAgrupField.Items.Clear();
            //while (worksheet.Cells[rowIndex, 1].Value != null)
            //{
            //    DataRow registro = dt.NewRow();
            //    registro["ID"] = worksheet.Cells[rowIndex, 1].Text+" - "+worksheet.Cells[rowIndex, 2].Text;
            //    registro["VALUE"] = worksheet.Cells[rowIndex, 1].Text;
            //    dt.Rows.Add(registro);

            //    rowIndex++;
            //}
            //File.Delete(fileName);
            cCatalogoCtas OcCatalogoCtas = new cCatalogoCtas();
            DataTable dt = OcCatalogoCtas.obtener_agrupador();
            codAgrupField.DataSource = dt;
            codAgrupField.ValueMember = "VALUE";
            codAgrupField.DisplayMember = "ID";
        }
        #endregion

        private void buttonX3_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Cancel;
        }

        private void buttonX1_Click(object sender, EventArgs e)
        {
            guardar();
        }

    }
}