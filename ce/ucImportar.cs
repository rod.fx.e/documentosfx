﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using DevComponents.DotNetBar;
using System.IO;
using OfficeOpenXml;
using Generales;

namespace Desarrollo
{
    public partial class ucImportar : UserControl
    {
        private cEMPRESA_CE ocEMPRESA;
        private cCatalogo oObjeto;
        public ucImportar()
        {
            InitializeComponent();
            //oObjeto = new cCatalogo();
        }
        public ucImportar(string sConn)
        {
            
            InitializeComponent();
            cargar_empresas();
        }

        public void cargar_empresas()
        {
            //Limpiar el treeview
            cEMPRESA_CE ocEMPRESA = new cEMPRESA_CE();
            EMPRESA.Items.Clear();
            foreach (cEMPRESA_CE registro in ocEMPRESA.todos())
            {

                DevComponents.DotNetBar.ItemContainer oItem = new DevComponents.DotNetBar.ItemContainer();
                oItem.Text = registro.ID;
                oItem.Tag = registro;
                EMPRESA.Items.Add(oItem);
            }
            if (EMPRESA.Items.Count > 0)
            {
                EMPRESA.SelectedIndex = 0;

            }
        }

        private void asigar_texto(string texto)
        {
            if (resultado.InvokeRequired)
            {
                MethodInvoker invoker = new MethodInvoker(delegate
                {
                    resultado.Text = DateTime.Now.ToString() + " " + texto + Environment.NewLine + resultado.Text;
                });
                resultado.Invoke(invoker);
            }
            else
            {
                resultado.Text = DateTime.Now.ToString() + " " + texto + Environment.NewLine + resultado.Text;
            }
        }

        private void buttonX4_Click(object sender, EventArgs e)
        {
            importar();
        }
        private void cargar(string ROW_IDp)
        {
            if (oObjeto.cargar(ROW_IDp))
            {
                versionField.Text = oObjeto.versionField;
                rFCField.Text = oObjeto.rFCField;
                mesField.Text = oObjeto.mesField;
                anoField.Text = oObjeto.anoField;
                totalCtasField.Text = oObjeto.totalCtasField;
                ESTADO.Text = oObjeto.ESTADO;
            }

        }
        private bool guardar()
        {
            oObjeto = new cCatalogo();

            oObjeto.versionField = versionField.Text;
            oObjeto.rFCField = rFCField.Text;
            oObjeto.mesField = mesField.Text;
            oObjeto.anoField = anoField.Text;
            oObjeto.totalCtasField = totalCtasField.Text;
            oObjeto.ESTADO = ESTADO.Text;
            if (oObjeto.guardar())
            {
                procesar();
                mostrar_mensaje("Datos Actualizados ", false);
                return true;
            }
            return false;
        }

        private void procesar()
        {
            OpenFileDialog openFileDialog1 = new OpenFileDialog();
            openFileDialog1.Filter = "Excel files (*.xlsx)|*.xlsx|All files (*.*)|*.*";
            openFileDialog1.FilterIndex = 1;
            openFileDialog1.RestoreDirectory = true;

            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                try
                {
                    //Cuentas Hoja 1
                    procesar_cuentas(1, openFileDialog1.FileName);
                    //Balanza Hoja 2
                    procesar_balanza(2, openFileDialog1.FileName);
                    //Polizas Hoja 3
                    cPoliza ocPoliza = new cPoliza();
                    ocPoliza.ROW_ID_CATALOGO = oObjeto.ROW_ID;
                    ocPoliza.procesar_poliza(3, openFileDialog1.FileName);
                    //Transacciones
                    ocPoliza.procesar_transacciones(4, openFileDialog1.FileName, true);
                    //CompNal
                    ocPoliza.procesar_comprobantes(5, openFileDialog1.FileName,oObjeto);
                }
                catch (Exception e)
                {
                    MessageBoxEx.Show("Existe un error en el proceso " + e.Message.ToString(), Application.ProductName + "-" + Application.ProductVersion, MessageBoxButtons.OK);
                }
            }
        }
        private void procesar_cuentas(int hoja, string archivo)
        {
            try
            {
                int n;
                int rowIndex = 1;
                FileInfo existingFile = new FileInfo(archivo);
                using (ExcelPackage package = new ExcelPackage(existingFile))
                {
                    ExcelWorksheet worksheet = package.Workbook.Worksheets[hoja];
                    if (worksheet.Name!="Catalogo de Cuentas")
                    {
                        return;
                    }
                    while (worksheet.Cells[rowIndex, 2].Value != null)
                    {
                        labelItem1.Text = "Registro " + rowIndex.ToString();
                        //Agregar las lineas de la cabecera
                        if (rowIndex != 1)
                        {


                            cCatalogoCtas ocCatalogoCtas = new cCatalogoCtas();

                            ocCatalogoCtas.numCtaField = worksheet.Cells[rowIndex, 2].Text;
                            ocCatalogoCtas.cargar_numCta(ocCatalogoCtas.numCtaField);

                            ocCatalogoCtas.ROW_ID_CATALOGO = oObjeto.ROW_ID;
                            ocCatalogoCtas.codAgrupField = worksheet.Cells[rowIndex, 1].Text;

                            ocCatalogoCtas.descField = worksheet.Cells[rowIndex, 3].Text;
                            ocCatalogoCtas.subCtaDeField = worksheet.Cells[rowIndex, 4].Text;
                            ocCatalogoCtas.nivelField = worksheet.Cells[rowIndex, 5].Text;
                            ocCatalogoCtas.naturField = worksheet.Cells[rowIndex, 6].Text;
                            labelItem1.Text = "Cuenta " + ocCatalogoCtas.numCtaField;
                            if (ocCatalogoCtas.guardar(false))
                            {
                                
                            }
                        }
                        rowIndex++;

                    }
                    //MessageBoxEx.Show("Archivo Importado", Application.ProductName);
                }
            }
            catch (Exception e)
            {
                MessageBoxEx.Show(e.Message.ToString());
            }

        }
        private void procesar_balanza(int hoja, string archivo)
        {

            cCONEXCION oData = new cCONEXCION();
            int n;
            int rowIndex = 1;
            FileInfo existingFile = new FileInfo(archivo);
            using (ExcelPackage package = new ExcelPackage(existingFile))
            {
                ExcelWorksheet worksheet = package.Workbook.Worksheets[hoja];
                if (worksheet.Name != "Balanza de Comprobación")
                {
                    return;
                }
                while (worksheet.Cells[rowIndex, 1].Value != null)
                {
                    try
                    {
                        //Agregar las lineas de la cabecera
                        if (rowIndex != 1)
                        {
                            cBalanza ocBalanza = new cBalanza();
                            ocBalanza.ROW_ID_CATALOGO = oObjeto.ROW_ID;
                            ocBalanza.numCtaField = worksheet.Cells[rowIndex, 1].Value.ToString();


                            ocBalanza.saldoIniField = 0;
                            if (Globales.IsNumeric(worksheet.Cells[rowIndex, 2].Value.ToString()))
                            {
                                ocBalanza.saldoIniField = double.Parse(worksheet.Cells[rowIndex, 2].Value.ToString());
                            }
                            else
                            {
                                asigar_texto("Procesar Balanza de comprobación Error en Fila: " + rowIndex + " Columna: 2. Valor " + worksheet.Cells[rowIndex, 2].Value.ToString() + " no es numerico");
                            }

                            ocBalanza.debeField = 0;
                            if (Globales.IsNumeric(worksheet.Cells[rowIndex, 3].Value.ToString()))
                            {
                                ocBalanza.debeField = double.Parse(worksheet.Cells[rowIndex, 3].Value.ToString());
                            }
                            else
                            {
                                asigar_texto("Procesar Balanza de comprobación Error en Fila: " + rowIndex + " Columna: 3. Valor " + worksheet.Cells[rowIndex, 3].Value.ToString() + " no es numerico");
                            }
                            ocBalanza.haberField = 0;
                            if (Globales.IsNumeric(worksheet.Cells[rowIndex, 4].Value.ToString()))
                            {
                                ocBalanza.haberField = double.Parse(worksheet.Cells[rowIndex, 4].Value.ToString());
                            }
                            else
                            {
                                asigar_texto("Procesar Balanza de comprobación Error en Fila: " + rowIndex + " Columna: 4. Valor " + worksheet.Cells[rowIndex, 4].Value.ToString() + " no es numerico");
                            }
                            ocBalanza.saldoFinField = 0;
                            if (Globales.IsNumeric(worksheet.Cells[rowIndex, 5].Value.ToString()))
                            {
                                ocBalanza.saldoFinField = double.Parse(worksheet.Cells[rowIndex, 5].Value.ToString());
                            }
                            else
                            {
                                asigar_texto("Procesar Balanza de comprobación Error en Fila: " + rowIndex + " Columna: 5. Valor " + worksheet.Cells[rowIndex, 5].Value.ToString() + " no es numerico");
                            }

                            if (ocBalanza.guardar(false))
                            {
                                asigar_texto("Procesar Balanza de comprobación Guardada la fila : " + rowIndex);
                            }
                            else
                            {
                                asigar_texto("Procesar Balanza de comprobación Error en Fila: " + rowIndex + " No fue Guardado verificar los datos.");
                            }
                        }
                    }
                    catch (Exception e)
                    {
                        //MessageBox.Show(e.Message.ToString());
                        asigar_texto("Procesar Balanza de comprobación Linea " + rowIndex.ToString() + " Error : " + e.Message.ToString());
                    }

                    rowIndex++;
                }

            }

        }

        private void mostrar_mensaje(string mensaje, bool mostrar_error)
        {
            ToastNotification.Show(this,
                mensaje,
                mostrar_error ? global::Desarrollo.Properties.Resources.error : global::Desarrollo.Properties.Resources.guardar,
                3000,
                mostrar_error ? eToastGlowColor.Red : eToastGlowColor.Green,
                eToastPosition.TopCenter);
        }
        private void importar()
        {
            if(guardar())
            {

            }
        }

        private void buttonX1_Click(object sender, EventArgs e)
        {
            cargar_empresas();
        }

        private void EMPRESA_SelectedIndexChanged(object sender, EventArgs e)
        {
            seleccionar_empresa();
        }

        private void seleccionar_empresa()
        {
            if (EMPRESA.SelectedItem == null)
            {
                MessageBoxEx.Show("Selecione una empresa para continuar.", Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            DevComponents.DotNetBar.ItemContainer oItem = (ItemContainer)EMPRESA.SelectedItem;
            ocEMPRESA = (cEMPRESA_CE)oItem.Tag;
            rFCField.Text = ocEMPRESA.RFC;
            anoField.Text = DateTime.Now.Year.ToString();
            mesField.SelectedIndex = DateTime.Now.Month - 1;
            ESTADO.SelectedIndex = 0;
            totalCtasField.Text = "0";
        }
    }
}
