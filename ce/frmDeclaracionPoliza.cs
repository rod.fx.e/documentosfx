using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevComponents.DotNetBar;
using System.IO;
using OfficeOpenXml;
using System.Threading;
using System.Xml.Serialization;
using DevComponents.DotNetBar.Controls;
using System.Linq;
using Ionic.Zip;
using System.Configuration;
using Generales;

namespace Desarrollo
{
    public partial class frmDeclaracionPoliza : DevComponents.DotNetBar.Metro.MetroForm
    {
        bool modificado = false;
        cCatalogo oObjeto = new cCatalogo();
        cCONEXCION oData = new cCONEXCION();
        //cCONEXCION oData_ERP;
        cEMPRESA_CE ocEMPRESA;
        //cEMPRESA_CONEXION ocEMPRESA_CONEXION;
        List<BackgroundWorker> bgws;
        string USUARIO;

        public frmDeclaracionPoliza(Object ocEMPRESAp,string USUARIOp)
        {
            USUARIO = USUARIOp;
            oObjeto = new cCatalogo();
            InitializeComponent();
            cargar_configuracion();
            ocEMPRESA = new cEMPRESA_CE();
            ocEMPRESA = (cEMPRESA_CE)ocEMPRESAp;

            bgws = new List<BackgroundWorker>();

            limpiar();

        }

        public frmDeclaracionPoliza(Object ocEMPRESAp, string ROW_IDp, string USUARIOp)
        {
            USUARIO=USUARIOp;
            oObjeto = new cCatalogo();
            InitializeComponent();
            cargar_configuracion();
            ocEMPRESA = (cEMPRESA_CE)ocEMPRESAp;
            cargar(ROW_IDp);
        }

        private void cargar_configuracion()
        {
            bool VISUAL = true;
            try
            {
                VISUAL = bool.Parse(ConfigurationManager.AppSettings.Get("VISUAL").ToString());

            }
            catch
            {

            }
            if (!VISUAL)
            {
                buttonX16.Visible = false;
            }

        }

        #region Acciones
        private void cargar(string ROW_IDp)
        {
            if (oObjeto.cargar(ROW_IDp))
            {
                versionField.Text=oObjeto.versionField;
                rFCField.Text = oObjeto.rFCField;
                mesField.Text = oObjeto.mesField;
                anoField.Text = oObjeto.anoField;
                totalCtasField.Text = oObjeto.totalCtasField;
                ESTADO.Text = oObjeto.ESTADO;

                cargar_poliza();
                cargar_tipos();
            }

        }

        private void cargar_tipos()
        {
            dtgTipos.Rows.Clear();

            cPolizas ocPolizas = new cPolizas();
            foreach (cPolizas registros in ocPolizas.todos(oObjeto.ROW_ID))
            {
                int n = dtgTipos.Rows.Add();
                dtgTipos.Rows[n].Tag = registros;
                string tipo_solicitud = "";
                switch (registros.tipoSolicitudField)
                {
                    case "AF":
                        tipo_solicitud = "Acto de Fiscalizaci�n";
                        break;
                    case "FC":
                        tipo_solicitud = "Fiscalizaci�n Compulsa";
                        break;
                    case "DE":
                        tipo_solicitud = "Devoluci�n";
                        break;
                    case "CO":
                        tipo_solicitud = "Compensaci�n";
                        break;
                }
                dtgTipos.Rows[n].Cells["ROW_ID"].Value = registros.ROW_ID;
                dtgTipos.Rows[n].Cells["TIPO_SOLICITUD"].Value = tipo_solicitud;
                dtgTipos.Rows[n].Cells["TIPO_tr"].Value = "P�lizas";
                dtgTipos.Rows[n].Cells["numero"].Value = registros.numTramiteField + registros.numOrdenField;
            }

            cAuxiliarCtas ocAuxiliarCtas = new cAuxiliarCtas();
            foreach(cAuxiliarCtas registros in ocAuxiliarCtas.todos(oObjeto.ROW_ID))
            {
                int n = dtgTipos.Rows.Add();

                dtgTipos.Rows[n].Tag = registros;
                string tipo_solicitud = "";
                switch (registros.tipoSolicitudField)
                {
                    case "AF":
                        tipo_solicitud = "Acto de Fiscalizaci�n";
                       break;
                    case "FC":
                       tipo_solicitud = "Fiscalizaci�n Compulsa";
                       break;
                    case "DE":
                       tipo_solicitud = "Devoluci�n";
                       break;
                    case "CO":
                       tipo_solicitud = "Compensaci�n";
                       break;
                }
                dtgTipos.Rows[n].Cells["ROW_ID"].Value = registros.ROW_ID;
                dtgTipos.Rows[n].Cells["TIPO_SOLICITUD"].Value = tipo_solicitud;
                dtgTipos.Rows[n].Cells["TIPO_tr"].Value = "Auxiliar Cuentas";
                dtgTipos.Rows[n].Cells["numero"].Value = registros.numTramiteField + registros.numOrdenField;

            }

            cAuxiliarFolios ocAuxiliarFolios = new cAuxiliarFolios();
            foreach (cAuxiliarFolios registros in ocAuxiliarFolios.todos(oObjeto.ROW_ID))
            {
                int n = dtgTipos.Rows.Add();
                dtgTipos.Rows[n].Tag = registros;
                string tipo_solicitud = "";
                switch (registros.tipoSolicitudField)
                {
                    case "AF":
                        tipo_solicitud = "Acto de Fiscalizaci�n";
                        break;
                    case "FC":
                        tipo_solicitud = "Fiscalizaci�n Compulsa";
                        break;
                    case "DE":
                        tipo_solicitud = "Devoluci�n";
                        break;
                    case "CO":
                        tipo_solicitud = "Compensaci�n";
                        break;
                }
                dtgTipos.Rows[n].Cells["ROW_ID"].Value = registros.ROW_ID;
                dtgTipos.Rows[n].Cells["TIPO_SOLICITUD"].Value = tipo_solicitud;
                dtgTipos.Rows[n].Cells["TIPO_tr"].Value = "Auxiliar P�lizas";
                dtgTipos.Rows[n].Cells["numero"].Value = registros.numTramiteField + registros.numOrdenField;
            }

        }

        private bool guardar()
        {
            oObjeto.versionField = versionField.Text;
            oObjeto.rFCField = rFCField.Text;
            oObjeto.mesField = mesField.Text;
            oObjeto.anoField = anoField.Text;
            oObjeto.totalCtasField = totalCtasField.Text;
            oObjeto.ESTADO = ESTADO.Text;
            if (oObjeto.guardar())
            {
                labelItem1.Text = "Datos Actualizados " + DateTime.Now.ToShortDateString() + " " + DateTime.Now.ToLongTimeString();
                mostrar_mensaje("Datos Actualizados ", false);
                modificado = false;
                return true;
            }
            return false;
        }

        private void mostrar_mensaje(string mensaje, bool mostrar_error)
        {
            ToastNotification.Show(this,
                mensaje,
                mostrar_error ? global::Desarrollo.Properties.Resources.error : global::Desarrollo.Properties.Resources.guardar,
                3000,
                mostrar_error ? eToastGlowColor.Red : eToastGlowColor.Green,
                eToastPosition.TopCenter);
        }
        public void limpiar()
        {
            if (modificado)
            {

                DialogResult Resultado = MessageBoxEx.Show("�Desea guardar los cambios?", Application.ProductName + "-" + Application.ProductVersion, MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                if (Resultado.ToString() == "Yes")
                {
                    guardar();
                }
            }
            
            modificado = false;
            rFCField.Text = ocEMPRESA.RFC;
            //Buscar la ultima versi�n
            //Cargar los meses
            int index_selecionado = 0;
            mesField.Items.Clear();
            for (int i = 1; i < 13; i++)
            {
                string tr = "00" + i.ToString();
                mesField.Items.Add((tr).Substring(tr.Length - 2, 2));
                if (DateTime.Now.Month == i)
                {
                    index_selecionado = i - 1;
                }
            }
            mesField.SelectedIndex = index_selecionado;
            anoField.Text = DateTime.Now.Year.ToString();
            ESTADO.SelectedIndex = 0;
            totalCtasField.Text = "0";


            dtgPoliza.Rows.Clear();

        }
        private void eliminar()
        {
            DialogResult Resultado = MessageBoxEx.Show("�Esta seguro de eliminar?", Application.ProductName + "-" + Application.ProductVersion, MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            if (Resultado.ToString() != "Yes")
            {
                return;
            }
            if (oObjeto.eliminar(oObjeto.ROW_ID))
            {
                modificado = false;

                MessageBoxEx.Show("Eliminado.", Application.ProductName + "-" + Application.ProductVersion, MessageBoxButtons.OK, MessageBoxIcon.Information);
                limpiar();
            }
        }
        #endregion

        private void buttonX3_Click(object sender, EventArgs e)
        {
            eliminar();
        }

        private void buttonX2_Click(object sender, EventArgs e)
        {
            limpiar();
        }

        private void buttonX1_Click(object sender, EventArgs e)
        {
            guardar();
        }

        private void backgroundWorker3_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            if (e.Cancelled)
            {
                labelItem1.Text = "Cancelado por el usuario...";
                circularProgress3.Value = 0;
            }
            // Check to see if an error occurred in the background process.
            else if (e.Error != null)
            {
                labelItem1.Text = e.Error.Message;
            }
            else
            {
                //BackGround Task Completed with out Error
                labelItem1.Text = " Finalizado...";
            }
            cambiar_boton_poliza();
            cargar_poliza();
            circularProgress3.Visible = false;
        }

        private void buttonX14_Click(object sender, EventArgs e)
        {
            importar_general();
        }
        private void importar_general()
        {
            //if (folderBrowserDialog1.ShowDialog() == DialogResult.OK)
            //{
            //    importar_catalogo_polizas(folderBrowserDialog1.SelectedPath, false);
            //    MessageBoxEx.Show("Exportaci�n de los archivos finalizada en " + folderBrowserDialog1.SelectedPath.ToString(), Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Information);
            //}
            
        }

        private void importar_catalogo_polizas(string pathp, bool mostrar_mensaje_finalizacion)
        {
            string path = pathp;
            if (path.Trim() == "")
            {
                if (folderBrowserDialog1.ShowDialog() == DialogResult.OK)
                {
                    path = folderBrowserDialog1.SelectedPath;

                }
                else
                {
                    MessageBoxEx.Show("Generaci�n de archivo cancelada.", Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
                    return;
                }
            }

        }

        #region Balanza

                
        private void validar_poliza()
        {
            if (oObjeto.ROW_ID != "")
            {
                cPoliza ocPoliza = new cPoliza();
                bool entro = false;
                foreach (cPoliza registro in ocPoliza.validar(oObjeto.ROW_ID))
                {
                    int n = dtgValidador.Rows.Add();
                    entro = true;
                    dtgValidador.Rows[n].Cells["TIPO"].Value = "P�lizas";
                    dtgValidador.Rows[n].Cells["CUENTA"].Value = registro.numField;
                    dtgValidador.Rows[n].Cells["MENSAJE"].Value = registro.error;
                }
                if(!entro)
                {
                    MessageBoxEx.Show("No se encontraron errores de validaci�n.", Application.ProductName + "-" + Application.ProductVersion, MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
        }
        private void cargar_poliza()
        {
            if (oObjeto.ROW_ID != "")
            {
                dtgPoliza.Rows.Clear();
                cPoliza ocPoliza = new cPoliza();
                foreach (cPoliza registro in ocPoliza.todos(oObjeto.ROW_ID))
                {
                    int n = dtgPoliza.Rows.Add();
                    actualizar_linea_poliza(registro, n);
                }
            }
        }

        private void actualizar_linea_poliza(cPoliza registro, int n)
        {
            for (int i = 0; i < dtgPoliza.Columns.Count; i++)
            {
                dtgPoliza.Rows[n].Cells[i].Value = "";
            }
            dtgPoliza.Rows[n].Tag = registro;
            dtgPoliza.Rows[n].Cells["ROW_ID_p"].Value = registro.ROW_ID;
            dtgPoliza.Rows[n].Cells["tipoField"].Value = registro.tipoField;
            dtgPoliza.Rows[n].Cells["fechaField"].Value = registro.fechaField.ToShortDateString();
            dtgPoliza.Rows[n].Cells["conceptoField"].Value = registro.conceptoField.Replace("'", "");
            dtgPoliza.Rows[n].Cells["numField"].Value = registro.numField;

            dtgPoliza.Rows[n].Cells["DEBE"].Value = oData.formato_decimal(registro.DEBE.ToString(),"");
            dtgPoliza.Rows[n].Cells["HABER"].Value = oData.formato_decimal(registro.HABER.ToString(), "");

            dtgPoliza.Rows[n].Cells["Comprobantes"].Value = false;
            if(registro.comprobantes!=0)
            {
                dtgPoliza.Rows[n].Cells["Comprobantes"].Value = true;
            }
            if(registro.error!="")
            {
                dtgPoliza.Rows[n].ErrorText = registro.error;
            }
        }


        private double saldo(string fecha, string TIPO, string Cuenta
            , string SYSTEM_CURRENCY_ID
            , string MFG_ENTITY_ID
            , cCONEXCION oData_ERPp)
        {
            decimal CREDIT_AMOUNT = 0;
            decimal DEBIT_AMOUNT = 0;
            decimal CURR_BALANCE = 0;

            string sSQL = "";

            if (TIPO == "E" || TIPO == "R")
            {
                sSQL = " SELECT CAST (ACCOUNT_BALANCE.DEBIT_AMOUNT as decimal(18,4)) as DEBIT_AMOUNT";
                sSQL += " ,CAST(ACCOUNT_BALANCE.CREDIT_AMOUNT as decimal(18,4)) as CREDIT_AMOUNT ";
                sSQL += " FROM ACCOUNT_BALANCE INNER JOIN ACCOUNT_PERIOD ON ACCOUNT_BALANCE.ACCT_YEAR = ACCOUNT_PERIOD.ACCT_YEAR AND ACCOUNT_BALANCE.ACCT_PERIOD = ACCOUNT_PERIOD.ACCT_PERIOD  ";
                sSQL += " WHERE 1=1 ";
                if (MFG_ENTITY_ID != "")
                {
                    sSQL += " AND ACCOUNT_BALANCE.ENTITY_ID = '" + MFG_ENTITY_ID + "'";
                }
                sSQL += " AND ACCOUNT_BALANCE.CURRENCY_ID = '" + SYSTEM_CURRENCY_ID + "'";
                sSQL += " AND ACCOUNT_BALANCE.ACCOUNT_ID='" + Cuenta + "'";
                sSQL += " AND ACCOUNT_PERIOD.END_DATE<" + fecha + "";
                sSQL += " AND DATEPART(yyyy, ACCOUNT_PERIOD.END_DATE)=DATEPART(yyyy, " + fecha + ")";
                sSQL += " ORDER BY ACCOUNT_BALANCE.ACCOUNT_ID DESC, ACCOUNT_PERIOD.BEGIN_DATE DESC, ACCOUNT_PERIOD.END_DATE DESC ";
            }
            else
            {
                sSQL = " SELECT CAST (ACCOUNT_BALANCE.DEBIT_AMOUNT as decimal(18,4)) as DEBIT_AMOUNT";
                sSQL += " ,CAST(ACCOUNT_BALANCE.CREDIT_AMOUNT as decimal(18,4)) as CREDIT_AMOUNT "; 
                sSQL += " FROM ACCOUNT_BALANCE INNER JOIN ACCOUNT_PERIOD ON ACCOUNT_BALANCE.ACCT_YEAR = ACCOUNT_PERIOD.ACCT_YEAR AND ACCOUNT_BALANCE.ACCT_PERIOD = ACCOUNT_PERIOD.ACCT_PERIOD ";
                sSQL += " WHERE 1=1 ";
                if (MFG_ENTITY_ID != "")
                {
                    sSQL += " AND ACCOUNT_BALANCE.ENTITY_ID = '" + MFG_ENTITY_ID + "'";
                }
                sSQL += " AND ACCOUNT_BALANCE.CURRENCY_ID = '" + SYSTEM_CURRENCY_ID + "'";
                sSQL += " AND ACCOUNT_BALANCE.ACCOUNT_ID='" + Cuenta + "'";
                sSQL += " AND ACCOUNT_PERIOD.END_DATE<" + fecha + " ";
                sSQL += " ORDER BY ACCOUNT_BALANCE.ACCOUNT_ID DESC, ACCOUNT_PERIOD.BEGIN_DATE DESC, ACCOUNT_PERIOD.END_DATE DESC ";

            }

            DataTable oDataTable1 = oData_ERPp.EjecutarConsulta(sSQL);
            foreach (DataRow oDataRow1 in oDataTable1.Rows)
            {
                DEBIT_AMOUNT = decimal.Parse(oDataRow1["DEBIT_AMOUNT"].ToString());
                CREDIT_AMOUNT = decimal.Parse(oDataRow1["CREDIT_AMOUNT"].ToString());
                CURR_BALANCE = (DEBIT_AMOUNT + CURR_BALANCE) - CREDIT_AMOUNT;
            }

            return double.Parse(oData.Truncar_decimales(CURR_BALANCE).ToString());

        }

        private void cambiar_boton_poliza()
        {
            if (buttonX16.Text == "Importar: Visual")
            {
                buttonX16.Image = global::Desarrollo.Properties.Resources.stop_16;
                buttonX16.Text = "Detener";
            }
            else
            {
                buttonX16.Image = global::Desarrollo.Properties.Resources.visual;

                buttonX16.Text = "Importar: Visual";
            }
        }

#endregion Balanza

        private bool verificar_seguridad_poliza(string ROW_ID_POLIZAp,string USUARIOp)
        {
            //Verificar seguridad de ingreso
            cPoliza ocPoliza=new cPoliza();
            return ocPoliza.cargar_seguridad(ROW_ID_POLIZAp, USUARIOp);

        }


        private void editar_poliza()
        {
            dtgPoliza.EndEdit();
            if (dtgPoliza.SelectedRows.Count > 0)
            {
                foreach (DataGridViewRow drv in dtgPoliza.SelectedRows)
                {
                    string ROW_ID = drv.Cells["ROW_ID_p"].Value.ToString();
                    if (verificar_seguridad_poliza(ROW_ID, USUARIO))
                    {
                        frmPoliza ofrmPoliza = new frmPoliza(oObjeto.ROW_ID, ROW_ID, USUARIO);
                        ofrmPoliza.ShowDialog();
                        cPoliza ocPoliza = new cPoliza();
                        ocPoliza.cargar(ROW_ID);
                        actualizar_linea_poliza(ocPoliza, drv.Index);
                    }
                    else
                    {
                        MessageBoxEx.Show("El usuario "+USUARIO+" no tiene privilegios para editar la p�liza.", Application.ProductName + "-" + Application.ProductVersion, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    }
                }
            }
            else
            {
                MessageBoxEx.Show("Seleccione alguna l�nea.", Application.ProductName + "-" + Application.ProductVersion, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }
        private void buttonX18_Click(object sender, EventArgs e)
        {
            cargar_poliza();
        }

        private void dtgPoliza_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            editar_poliza();
        }

        private void buttonX19_Click(object sender, EventArgs e)
        {
            nueva_poliza();
        }

        private void nueva_poliza()
        {
            if (oObjeto.ROW_ID == "")
            {
                MessageBoxEx.Show("Guarde primero.", Application.ProductName + "-" + Application.ProductVersion, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return;
            }
            frmPoliza ofrmPoliza = new frmPoliza(oObjeto.ROW_ID,USUARIO);
            ofrmPoliza.ShowDialog();
            
            cargar_poliza();
        }

        private void buttonX16_Click(object sender, EventArgs e)
        {
            frmBatchs ofrmBatchs = new frmBatchs();
            DialogResult resultado = ofrmBatchs.ShowDialog();
            if (resultado == DialogResult.OK)
            {
                //Crear el string para consulta del Where
                string sSQL_WHERE_tipo = "";
                if (ofrmBatchs.APCp!="")
                {
                    if (sSQL_WHERE_tipo=="")
                    {
                        sSQL_WHERE_tipo = "('" + ofrmBatchs.APCp  + "' ";
                    }
                    else
                    {
                        sSQL_WHERE_tipo+= ",'" + ofrmBatchs.APCp + "' ";
                    }
                }
                if (ofrmBatchs.APIp != "")
                {
                    if (sSQL_WHERE_tipo == "")
                    {
                        sSQL_WHERE_tipo = "('" + ofrmBatchs.APIp + "' ";
                    }
                    else
                    {
                        sSQL_WHERE_tipo += ",'" + ofrmBatchs.APIp + "' ";
                    }
                }
                if (ofrmBatchs.ADJp != "")
                {
                    if (sSQL_WHERE_tipo == "")
                    {
                        sSQL_WHERE_tipo = "('" + ofrmBatchs.ADJp + "' ";
                    }
                    else
                    {
                        sSQL_WHERE_tipo += ",'" + ofrmBatchs.ADJp + "' ";
                    }
                }
                if (ofrmBatchs.GJp != "")
                {
                    if (sSQL_WHERE_tipo == "")
                    {
                        sSQL_WHERE_tipo = "('" + ofrmBatchs.GJp + "' ";
                    }
                    else
                    {
                        sSQL_WHERE_tipo += ",'" + ofrmBatchs.GJp + "' ";
                    }
                }
                if (ofrmBatchs.PURp != "")
                {
                    if (sSQL_WHERE_tipo == "")
                    {
                        sSQL_WHERE_tipo = "('" + ofrmBatchs.PURp + "' ";
                    }
                    else
                    {
                        sSQL_WHERE_tipo += ",'" + ofrmBatchs.PURp + "' ";
                    }
                }
                if (ofrmBatchs.SLSp != "")
                {
                    if (sSQL_WHERE_tipo == "")
                    {
                        sSQL_WHERE_tipo = "('" + ofrmBatchs.SLSp + "' ";
                    }
                    else
                    {
                        sSQL_WHERE_tipo += ",'" + ofrmBatchs.SLSp + "' ";
                    }
                }
                if (ofrmBatchs.INDp != "")
                {
                    if (sSQL_WHERE_tipo == "")
                    {
                        sSQL_WHERE_tipo = "('" + ofrmBatchs.INDp + "' ";
                    }
                    else
                    {
                        sSQL_WHERE_tipo += ",'" + ofrmBatchs.INDp + "' ";
                    }
                }
                if (ofrmBatchs.FGp != "")
                {
                    if (sSQL_WHERE_tipo == "")
                    {
                        sSQL_WHERE_tipo = "('" + ofrmBatchs.FGp + "' ";
                    }
                    else
                    {
                        sSQL_WHERE_tipo += ",'" + ofrmBatchs.FGp + "' ";
                    }
                }
                if (ofrmBatchs.WIPp != "")
                {
                    if (sSQL_WHERE_tipo == "")
                    {
                        sSQL_WHERE_tipo = "('" + ofrmBatchs.WIPp + "' ";
                    }
                    else
                    {
                        sSQL_WHERE_tipo += ",'" + ofrmBatchs.WIPp + "' ";
                    }
                }

                if (ofrmBatchs.RVJp != "")
                {
                    if (sSQL_WHERE_tipo == "")
                    {
                        sSQL_WHERE_tipo = "('" + ofrmBatchs.RVJp + "' ";
                    }
                    else
                    {
                        sSQL_WHERE_tipo += ",'" + ofrmBatchs.RVJp + "' ";
                    }
                }

                if (ofrmBatchs.ARIp != "")
                {
                    if (sSQL_WHERE_tipo == "")
                    {
                        sSQL_WHERE_tipo = "('" + ofrmBatchs.ARIp + "' ";
                    }
                    else
                    {
                        sSQL_WHERE_tipo += ",'" + ofrmBatchs.ARIp + "' ";
                    }
                }

                if (ofrmBatchs.ARCp != "")
                {
                    if (sSQL_WHERE_tipo == "")
                    {
                        sSQL_WHERE_tipo = "('" + ofrmBatchs.ARCp + "' ";
                    }
                    else
                    {
                        sSQL_WHERE_tipo += ",'" + ofrmBatchs.ARCp + "' ";
                    }
                }

                if (ofrmBatchs.BAJp != "")
                {
                    if (sSQL_WHERE_tipo == "")
                    {
                        sSQL_WHERE_tipo = "('" + ofrmBatchs.BAJp + "' ";
                    }
                    else
                    {
                        sSQL_WHERE_tipo += ",'" + ofrmBatchs.BAJp + "' ";
                    }
                }
                if (sSQL_WHERE_tipo != "")
                {
                    sSQL_WHERE_tipo+= ")";
                }
                importar_poliza_visual(ofrmBatchs.BATCH_INICIALp, ofrmBatchs.BATCH_FINALp, sSQL_WHERE_tipo);
            }
        }

        private void importar_poliza_visual(string BATCH_INICIALp = "", string BATCH_FINALp = "", string sSQL_WHERE_tipo="")
        {
            if (oObjeto.ROW_ID == "")
            {
                if (!guardar())
                {
                    return;
                }
            }
            if (buttonX16.Text == "Detener")
            {
                if (backgroundWorker3.IsBusy)
                {
                    backgroundWorker3.CancelAsync();
                }
                return;
            }
            circularProgress3.Visible = true;
            circularProgress3.IsRunning = true;
            cambiar_boton_poliza();
            if (!backgroundWorker3.IsBusy)
            {
                List<object> arguments = new List<object>();
                arguments.Add(mesField.Text);
                arguments.Add(anoField.Text);
                arguments.Add(BATCH_INICIALp);
                arguments.Add(BATCH_FINALp);
                arguments.Add(sSQL_WHERE_tipo);
                //Cargar el formulario si lo piden

                backgroundWorker3.RunWorkerAsync(arguments);
            }
        }

        #region Poliza
        private void backgroundWorker3_DoWork(object sender, DoWorkEventArgs e)
        {
            try
            {
                int n = 0;
                cCatalogo ocCatalogo = new cCatalogo();
                labelItem1.Text = "Migrando Polizas de Infor Visual Base de datos: " + ocEMPRESA.VISUAL_BD;
                try
                {

                    List<object> genericlist = e.Argument as List<object>;
                    //Cargar todas las cuentas contables
                    cEMPRESA_CONEXION ocEMPRESA_CONEXION_tr = new cEMPRESA_CONEXION();

                    //Calcular todos los Batch 
                    int cuenta_batch = 0;
                    labelItem1.Text = "Migrando de Infor Visual Base de datos: " + ocEMPRESA_CONEXION_tr.VISUAL_ENTITY_ID;
                    foreach (cEMPRESA_CONEXION registro in ocEMPRESA_CONEXION_tr.todos(ocEMPRESA.ROW_ID))
                    {
                        cEMPRESA_CONEXION ocEMPRESA_CONEXION = registro;
                        cCONEXCION oData_ERP = registro.conectar(registro.ROW_ID); ;
                        string sSQL = "SELECT COUNT(*) as Cuenta FROM JOURNAL_BATCH";
                        sSQL += " WHERE 1=1";
                        sSQL += " AND MONTH(BATCH_DATE)=" + genericlist[0].ToString();
                        sSQL += " AND YEAR(BATCH_DATE)=" + genericlist[1].ToString();
                        if (genericlist[2].ToString() != "")
                        {
                            sSQL += " AND ID>='" + genericlist[2].ToString() + "'";
                        }
                        if (genericlist[3].ToString() != "")
                        {
                            sSQL += " AND ID<='" + genericlist[3].ToString() + "'";

                        }
                        if (genericlist[4].ToString() != "")
                        {
                            sSQL += " AND [TYPE] IN " + genericlist[4].ToString() + " ";

                        }

                        DataTable oDataTable = oData_ERP.EjecutarConsulta(sSQL);
                        if (oDataTable != null)
                        {
                            foreach (DataRow oDataRow in oDataTable.Rows)
                            {
                                cuenta_batch += int.Parse(oDataRow["Cuenta"].ToString());
                            }
                        }
                    }
                    labelItem2.Text = " Total de Polizas a exportar " + cuenta_batch.ToString();
                    labelItem2.Refresh();
                    int cuenta_batch_regresiva = 1;


                    labelItem1.Text = "Migrando de Infor Visual Base de datos: " + ocEMPRESA_CONEXION_tr.VISUAL_ENTITY_ID;
                    foreach (cEMPRESA_CONEXION registro in ocEMPRESA_CONEXION_tr.todos(ocEMPRESA.ROW_ID))
                    {
                        cEMPRESA_CONEXION ocEMPRESA_CONEXION = registro;
                        cCONEXCION oData_ERP = registro.conectar(registro.ROW_ID); ;
                        string sSQL = "SELECT * FROM JOURNAL_BATCH";
                        sSQL += " WHERE 1=1";
                        sSQL += " AND MONTH(BATCH_DATE)=" + genericlist[0].ToString();
                        sSQL += " AND YEAR(BATCH_DATE)=" + genericlist[1].ToString();
                        if(genericlist[2].ToString()!="")
                        {
                            sSQL += " AND ID>='" + genericlist[2].ToString() + "'";
                        }
                        if (genericlist[3].ToString() != "")
                        {
                            sSQL += " AND ID<='" + genericlist[3].ToString() + "'";

                        }
                        DataTable oDataTable = oData_ERP.EjecutarConsulta(sSQL);
                        if (oDataTable != null)
                        {
                            foreach (DataRow oDataRow in oDataTable.Rows)
                            {
                                n++;
                                Thread.Sleep(100);
                                List<object> arguments = new List<object>();
                                arguments.Add(oDataRow);
                                arguments.Add(ocEMPRESA_CONEXION);
                                labelItem2.Text = " Total de Cuentas a migrar " + cuenta_batch.ToString() + ". Total " + cuenta_batch_regresiva.ToString() + "/" + cuenta_batch.ToString() + " ";
                                labelItem2.Refresh();
                                cuenta_batch_regresiva++;

                                backgroundWorker3.ReportProgress(n, arguments);
                                if (backgroundWorker3.CancellationPending)
                                {
                                    e.Cancel = true;
                                    backgroundWorker3.ReportProgress(0);
                                    return;
                                }
                            }
                        }
                    }



                }
                catch
                {
                    backgroundWorker3.CancelAsync();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        private void backgroundWorker3_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            try
            {
                if (!backgroundWorker3.CancellationPending)
                {
                    List<object> genericlist = e.UserState as List<object>;
                    DataRow oDataRow = (DataRow)genericlist[0];
                    cEMPRESA_CONEXION ocEMPRESA_CONEXIONp = (cEMPRESA_CONEXION)genericlist[1];
                    cCONEXCION ocCONEXCION=ocEMPRESA_CONEXIONp.conectar(ocEMPRESA_CONEXIONp.ROW_ID);

                    //DataRow oDataRow = (DataRow)e.UserState;

                    if (oDataRow != null)
                    {

                        cPoliza ocPoliza = new cPoliza();
                        ocPoliza.ROW_ID_CATALOGO = oObjeto.ROW_ID;
                        //Descripci�n Atributo requerido para expresar el tipo de la p�liza:
                        //1 - Ingresos, 2 - Egresos o 3 - Diario 
                        switch (oDataRow["TYPE"].ToString())
                        {
                            case "APC": 
                                //Egreso
                                ocPoliza.tipoField = "2";
                                break;
                            case "API":
                            case "ADJ":
                            case "GJ":
                            case "PUR":                 
                            case "SLS":
                            case "IND":
                            case "FG":
                            case "WIP":
                            case "RVJ":
                            case "ARI":
                                //Diario
                                ocPoliza.tipoField = "3";
                                break;
                            case "ARC":
                                //Ingreso
                                ocPoliza.tipoField = "1";
                                break;
                            case "BAJ":
                                //Verificar si es Credit o Debit
                                ocPoliza.tipoField = "-1";
                                break;
                        }
                        ocPoliza.fechaField = DateTime.Parse(oDataRow["BATCH_DATE"].ToString());
                        
                        ocPoliza.conceptoField = ocEMPRESA_CONEXIONp.VISUAL_ENTITY_ID + " " + cEMPRESA_DESCRIPCION.cambiar_descripcion(oDataRow["DESCRIPTION"].ToString()) ;
                        ocPoliza.numField = oDataRow["ID"].ToString();
                        if (ocPoliza.guardar(false))
                        {
                            generar_transacciones(ocPoliza.ROW_ID, oDataRow["ID"].ToString(), ocEMPRESA_CONEXIONp.VISUAL_CURRENCY_ID, ocCONEXCION, ocEMPRESA_CONEXIONp);
                        }
                        else
                        {
                            labelItem1.Text = "La P�liza: " + oDataRow["ID"].ToString() + "se encuentra registrada.";
                            //Eliminar los registros que tengan la misma poliza y diferente row_id
                            ocPoliza.eliminar_diferentes(oObjeto.ROW_ID, oDataRow["ID"].ToString());
                            //Verificar la duplicidad de las transacciones
                            cTransaccion ocTransaccion = new cTransaccion();
                            ocTransaccion.eliminar_duplicidad(oObjeto.ROW_ID);
                        }
                        circularProgress3.Value = e.ProgressPercentage;
                        labelItem1.Text = "Migrando Poliza: " + oDataRow["ID"].ToString();
                        labelItem1.Refresh();
                        circularProgress3.ProgressText = "Procesando.. " + e.ProgressPercentage.ToString();// + " de " + TotalRecords;
                        labelItem1.Refresh();
                    }
                }
            }
            catch (Exception excep)
            {
                MessageBoxEx.Show(excep.ToString());
                backgroundWorker3.CancelAsync();
                return;
            }
        }

        private void generar_transacciones(string ROW_IDp, string IDp, string VISUAL_CURRENCY_IDp, cCONEXCION ocCONEXCION, cEMPRESA_CONEXION ocEMPRESA_CONEXIONp)
        {
            //List<object> arguments = new List<object>();
            //arguments.Add(ROW_IDp);
            //arguments.Add(IDp);
            //arguments.Add(VISUAL_CURRENCY_IDp);
            //arguments.Add(ocCONEXCION);
            //arguments.Add(ocEMPRESA_CONEXIONp);
            //bW1.RunWorkerAsync(arguments);

            generar_WIP_ISSUE_DIST(ROW_IDp, IDp, VISUAL_CURRENCY_IDp, ocCONEXCION, ocEMPRESA_CONEXIONp);
            generar_CASH_DISBURSE_DIST(ROW_IDp, IDp, VISUAL_CURRENCY_IDp, ocCONEXCION, ocEMPRESA_CONEXIONp);
            generar_CASH_RECEIPT_DIST(ROW_IDp, IDp, VISUAL_CURRENCY_IDp, ocCONEXCION, ocEMPRESA_CONEXIONp);
            generar_PAYABLE_DIST(ROW_IDp, IDp, VISUAL_CURRENCY_IDp, ocCONEXCION, ocEMPRESA_CONEXIONp);
            generar_RECEIVABLE_DIST(ROW_IDp, IDp, VISUAL_CURRENCY_IDp, ocCONEXCION, ocEMPRESA_CONEXIONp);
            generar_INDIRECT_DIST(ROW_IDp, IDp, VISUAL_CURRENCY_IDp, ocCONEXCION, ocEMPRESA_CONEXIONp);
            generar_ADJUSTMENT_DIST(ROW_IDp, IDp, VISUAL_CURRENCY_IDp, ocCONEXCION, ocEMPRESA_CONEXIONp);
            generar_PURCHASE_DIST(ROW_IDp, IDp, VISUAL_CURRENCY_IDp, ocCONEXCION, ocEMPRESA_CONEXIONp);
            generar_SHIPMENT_DIST(ROW_IDp, IDp, VISUAL_CURRENCY_IDp, ocCONEXCION, ocEMPRESA_CONEXIONp);
            generar_WIP_RECEIPT_DIST(ROW_IDp, IDp, VISUAL_CURRENCY_IDp, ocCONEXCION, ocEMPRESA_CONEXIONp);
            generar_GJ_DIST(ROW_IDp, IDp, VISUAL_CURRENCY_IDp, ocCONEXCION, ocEMPRESA_CONEXIONp);
            generar_BANK_ADJ_DIST(ROW_IDp, IDp, VISUAL_CURRENCY_IDp, ocCONEXCION, ocEMPRESA_CONEXIONp);

        }
        private void bW1_DoWork(object sender, DoWorkEventArgs e)
        {
            try
            {
                int n = 0;
                try
                {

                    List<object> genericlist = e.Argument as List<object>;
                    

                }
                catch
                {
                    bW1.CancelAsync();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                bW1.CancelAsync();
            }
        }

        private void Workers_Complete(object sender, RunWorkerCompletedEventArgs e)
        {
            BackgroundWorker bgw = (BackgroundWorker)sender;
            bgws.Remove(bgw);
            bgw.Dispose();
            if (bgws.Count == 0) MessageBox.Show("Todos");
        }

        private void generar_WIP_ISSUE_DIST(string ROW_ID_POLIZA, string BATCH_ID, string CURRENCY_ID, cCONEXCION oData_ERPp, cEMPRESA_CONEXION ocEMPRESA_CONEXIONp)
        {
            string sSQL;
            sSQL = " SELECT ";
            sSQL += " CAST(WIP_ISSUE_DIST.WORKORDER_BASE_ID + '/' + WIP_ISSUE_DIST.WORKORDER_LOT_ID + '.' + WIP_ISSUE_DIST.WORKORDER_SPLIT_ID + '.' + WIP_ISSUE_DIST.WORKORDER_SUB_ID AS VARCHAR(500)) ";
            sSQL += " + ' ' + ISNULL((SELECT TOP 1 PART_ID FROM WORK_ORDER WHERE TYPE='W' AND BASE_ID=WIP_ISSUE_DIST.WORKORDER_BASE_ID AND LOT_ID=WIP_ISSUE_DIST.WORKORDER_LOT_ID AND SPLIT_ID=WIP_ISSUE_DIST.WORKORDER_SPLIT_ID AND SUB_ID='0'),'')  ";
            sSQL += " + ' ' + ISNULL((SELECT TOP 1 ID FROM PART WHERE ID=(SELECT TOP 1 PART_ID FROM WORK_ORDER WHERE TYPE='W' AND BASE_ID=WIP_ISSUE_DIST.WORKORDER_BASE_ID AND LOT_ID=WIP_ISSUE_DIST.WORKORDER_LOT_ID AND SPLIT_ID=WIP_ISSUE_DIST.WORKORDER_SPLIT_ID AND SUB_ID='0')),'')  ";
            sSQL += " + ' ' + ISNULL((SELECT TOP 1 DESCRIPTION FROM PART WHERE ID=(SELECT TOP 1 PART_ID FROM WORK_ORDER WHERE TYPE='W' AND BASE_ID=WIP_ISSUE_DIST.WORKORDER_BASE_ID AND LOT_ID=WIP_ISSUE_DIST.WORKORDER_LOT_ID AND SPLIT_ID=WIP_ISSUE_DIST.WORKORDER_SPLIT_ID AND SUB_ID='0')),'')  ";
            sSQL += " AS REFERENCIA ";
            sSQL += ", GL_ACCOUNT_ID ";
            sSQL += ", (SELECT TOP 1 DESCRIPTION FROM ACCOUNT WHERE  ID=GL_ACCOUNT_ID) as DESCRIPCION ";
            sSQL += ", 'CR' = CASE WHEN AMOUNT_TYPE = 'CR' THEN AMOUNT ELSE 0 END ";
            sSQL += ", 'DR' = CASE WHEN AMOUNT_TYPE = 'DR' THEN AMOUNT ELSE 0 END ";
            sSQL += ",POSTING_DATE ";
            sSQL += ",POSTING_STATUS ";
            sSQL += ",(SELECT TOP 1 TYPE FROM  JOURNAL_BATCH WHERE ID = BATCH_ID) as TYPE ";
            sSQL += ",(SELECT TOP 1 USER_ID FROM  JOURNAL_BATCH WHERE ID = BATCH_ID) as BATCH_USER_ID ";
            sSQL += ",BATCH_ID ";
            //sSQL += ",ENTITY_ID   ";
            sSQL += ",CURRENCY_ID ";
            sSQL += " FROM WIP_ISSUE_DIST ";
            sSQL += " WHERE 1=1 AND BATCH_ID='" + BATCH_ID + "' AND POSTING_STATUS='P' AND CURRENCY_ID='" + CURRENCY_ID + "'";
            DataTable oDataTable = oData_ERPp.EjecutarConsulta(sSQL);
            foreach (DataRow oDataRow in oDataTable.Rows)
            {
                cTransaccion ocTransaccion = new cTransaccion();
                ocTransaccion.numCtaField = oDataRow["GL_ACCOUNT_ID"].ToString();
                ocTransaccion.ROW_ID_POLIZA = ROW_ID_POLIZA;
                ocTransaccion.conceptoField = ocEMPRESA_CONEXIONp.VISUAL_ENTITY_ID + " " + cEMPRESA_DESCRIPCION.cambiar_descripcion(oDataRow["REFERENCIA"].ToString());
                ocTransaccion.debeField = double.Parse(oDataRow["DR"].ToString());
                ocTransaccion.haberField = double.Parse(oDataRow["CR"].ToString());

                if (ocTransaccion.debeField < 0)
                {
                    ocTransaccion.haberField += Math.Abs(ocTransaccion.debeField);
                    ocTransaccion.debeField = 0;
                }
                if (ocTransaccion.haberField < 0)
                {
                    ocTransaccion.debeField += Math.Abs(ocTransaccion.haberField);
                    ocTransaccion.haberField = 0;
                }

                ocTransaccion.monedaField = oDataRow["CURRENCY_ID"].ToString();                
                if (ocTransaccion.guardar(true,true))
                {
                    //Verificar la transaccion
                }
            }
        }
        private void generar_RECEIVABLE_DIST(string ROW_ID_POLIZA, string BATCH_ID, string CURRENCY_ID
            , cCONEXCION oData_ERPp, cEMPRESA_CONEXION ocEMPRESA_CONEXIONp)
        {
            string sSQL;

            sSQL = " SELECT ";
            sSQL += " ISNULL((SELECT CUSTOMER.ID FROM RECEIVABLE INNER JOIN CUSTOMER ON RECEIVABLE.CUSTOMER_ID = CUSTOMER.ID WHERE (RECEIVABLE.INVOICE_ID = RECEIVABLE_DIST.INVOICE_ID)),'') + ' ' + CAST(INVOICE_ID AS VARCHAR(500)) + ' ' + (SELECT ISNULL(CUSTOMER.NAME,'') FROM RECEIVABLE INNER JOIN  CUSTOMER ON RECEIVABLE.CUSTOMER_ID = CUSTOMER.ID WHERE (RECEIVABLE.INVOICE_ID = RECEIVABLE_DIST.INVOICE_ID)) ";
            sSQL += " AS REFERENCIA ";
            sSQL += ", GL_ACCOUNT_ID ";
            sSQL += ", (SELECT TOP 1 DESCRIPTION FROM ACCOUNT WHERE  ID=GL_ACCOUNT_ID) as DESCRIPCION ";
            sSQL += ", 'CR' = CASE WHEN AMOUNT_TYPE = 'CR' THEN AMOUNT ELSE 0 END ";
            sSQL += ", 'DR' = CASE WHEN AMOUNT_TYPE = 'DR' THEN AMOUNT ELSE 0 END ";
            sSQL += ",POSTING_DATE ";
            sSQL += ",POSTING_STATUS ";
            sSQL += ",(SELECT TOP 1 TYPE FROM  JOURNAL_BATCH WHERE ID = BATCH_ID) as TYPE ";
            sSQL += ",(SELECT TOP 1 USER_ID FROM  JOURNAL_BATCH WHERE ID = BATCH_ID) as BATCH_USER_ID ";
            sSQL += ",BATCH_ID ";
            //sSQL += ",ENTITY_ID   ";
            sSQL += ",CURRENCY_ID ";
            sSQL += ",INVOICE_ID ";
            sSQL += ", ISNULL((SELECT CUSTOMER.ID FROM RECEIVABLE INNER JOIN CUSTOMER ON RECEIVABLE.CUSTOMER_ID = CUSTOMER.ID WHERE (RECEIVABLE.INVOICE_ID = RECEIVABLE_DIST.INVOICE_ID)),'') as CUSTOMER_ID ";
            sSQL += " FROM RECEIVABLE_DIST ";
            sSQL += " WHERE 1=1 AND BATCH_ID='" + BATCH_ID + "' AND POSTING_STATUS='P' AND CURRENCY_ID='" + CURRENCY_ID + "'";

            DataTable oDataTable = oData_ERPp.EjecutarConsulta(sSQL);

            foreach (DataRow oDataRow in oDataTable.Rows)
            {
                cTransaccion ocTransaccion = new cTransaccion();
                ocTransaccion.numCtaField = oDataRow["GL_ACCOUNT_ID"].ToString();
                ocTransaccion.ROW_ID_POLIZA = ROW_ID_POLIZA;
                ocTransaccion.conceptoField = ocEMPRESA_CONEXIONp.VISUAL_ENTITY_ID + " " + cEMPRESA_DESCRIPCION.cambiar_descripcion(oDataRow["REFERENCIA"].ToString());
                ocTransaccion.debeField = double.Parse(oDataRow["DR"].ToString());
                ocTransaccion.haberField = double.Parse(oDataRow["CR"].ToString());

                if (ocTransaccion.debeField < 0)
                {
                    ocTransaccion.haberField += Math.Abs(ocTransaccion.debeField);
                    ocTransaccion.debeField = 0;
                }
                if (ocTransaccion.haberField < 0)
                {
                    ocTransaccion.debeField += Math.Abs(ocTransaccion.haberField);
                    ocTransaccion.haberField = 0;
                }

                ocTransaccion.DOCUMENTO = oDataRow["INVOICE_ID"].ToString();
                ocTransaccion.TIPO = "ARI";
                ocTransaccion.ENTE = oDataRow["CUSTOMER_ID"].ToString();
                
                ocTransaccion.monedaField = oDataRow["CURRENCY_ID"].ToString();
                if (ocTransaccion.guardar(true, true))
                {
                    //Guardar comprobantes
                    //if (importar.Checked)
                    //{
                    //    cPolizasPolizaTransaccionCompNal ocComprobante = new cPolizasPolizaTransaccionCompNal();
                    //    ocComprobante.generar_CFDI(ocTransaccion.ROW_ID, oDataRow["INVOICE_ID"].ToString(), oData_ERPp
                    //        , ocEMPRESA_CONEXIONp
                    //        );
                    //}
                    
                }
            }
        }
        private void generar_CASH_RECEIPT_DIST(string ROW_ID_POLIZA, string BATCH_ID, string CURRENCY_ID, cCONEXCION oData_ERPp, cEMPRESA_CONEXION ocEMPRESA_CONEXIONp)
        {
            string sSQL;
            sSQL = " SELECT ";
            sSQL += " CAST(CUSTOMER_ID + ' - ' + CHECK_ID AS VARCHAR(500)) + ' ' + ISNULL((SELECT TOP 1 NAME FROM CUSTOMER WHERE ID=CASH_RECEIPT_DIST.CUSTOMER_ID ),'') ";
            sSQL += " AS REFERENCIA ";
            sSQL += ", GL_ACCOUNT_ID ";
            sSQL += ", (SELECT TOP 1 DESCRIPTION FROM ACCOUNT WHERE  ID=GL_ACCOUNT_ID) as DESCRIPCION ";
            sSQL += ", 'CR' = CASE WHEN AMOUNT_TYPE = 'CR' THEN AMOUNT ELSE 0 END ";
            sSQL += ", 'DR' = CASE WHEN AMOUNT_TYPE = 'DR' THEN AMOUNT ELSE 0 END ";
            sSQL += ",POSTING_DATE ";
            sSQL += ",POSTING_STATUS ";
            sSQL += ",(SELECT TOP 1 TYPE FROM  JOURNAL_BATCH WHERE ID = BATCH_ID) as TYPE ";
            sSQL += ",(SELECT TOP 1 USER_ID FROM  JOURNAL_BATCH WHERE ID = BATCH_ID) as BATCH_USER_ID ";
            sSQL += ",BATCH_ID ";
            //sSQL += ",ENTITY_ID   ";
            sSQL += ",CURRENCY_ID,CUSTOMER_ID,CHECK_ID ";
            sSQL += " FROM CASH_RECEIPT_DIST ";
            sSQL += " WHERE 1=1 AND BATCH_ID='" + BATCH_ID + "' AND POSTING_STATUS='P' AND CURRENCY_ID='" + CURRENCY_ID + "'";

            DataTable oDataTable = oData_ERPp.EjecutarConsulta(sSQL);
            foreach (DataRow oDataRow in oDataTable.Rows)
            {
                cTransaccion ocTransaccion = new cTransaccion();
                ocTransaccion.numCtaField = oDataRow["GL_ACCOUNT_ID"].ToString();
                ocTransaccion.ROW_ID_POLIZA = ROW_ID_POLIZA;
                ocTransaccion.conceptoField = ocEMPRESA_CONEXIONp.VISUAL_ENTITY_ID + " " + cEMPRESA_DESCRIPCION.cambiar_descripcion(oDataRow["REFERENCIA"].ToString());
                ocTransaccion.debeField = double.Parse(oDataRow["DR"].ToString());
                ocTransaccion.haberField = double.Parse(oDataRow["CR"].ToString());

                if (ocTransaccion.debeField < 0)
                {
                    ocTransaccion.haberField += Math.Abs(ocTransaccion.debeField);
                    ocTransaccion.debeField = 0;
                }
                if (ocTransaccion.haberField < 0)
                {
                    ocTransaccion.debeField += Math.Abs(ocTransaccion.haberField);
                    ocTransaccion.haberField = 0;
                }
                ocTransaccion.DOCUMENTO = oDataRow["CHECK_ID"].ToString();
                ocTransaccion.TIPO = "ARC";
                ocTransaccion.ENTE = oDataRow["CUSTOMER_ID"].ToString();

                ocTransaccion.monedaField = oDataRow["CURRENCY_ID"].ToString();
                if (ocTransaccion.guardar(true, true))
                {
                    //Guardar comprobantes
                    sSQL =" SELECT CASH_RECEIPT_LINE.CUSTOMER_ID, CASH_RECEIPT_LINE.INVOICE_ID ";
                    sSQL += " FROM CASH_RECEIPT_LINE INNER JOIN CASH_RECEIPT_DIST ON CASH_RECEIPT_LINE.CUSTOMER_ID = CASH_RECEIPT_DIST.CUSTOMER_ID AND CASH_RECEIPT_LINE.CHECK_ID = CASH_RECEIPT_DIST.CHECK_ID ";
                    sSQL += " WHERE CASH_RECEIPT_LINE.CUSTOMER_ID='" + oDataRow["CUSTOMER_ID"] + "' AND CASH_RECEIPT_LINE.CHECK_ID='" + oDataRow["CHECK_ID"] + "'";
                    DataTable oDataTable2 = oData_ERPp.EjecutarConsulta(sSQL);
                    foreach (DataRow oDataRow2 in oDataTable2.Rows)
                    {
                        //if (importar.Checked)
                        //{
                        //    cPolizasPolizaTransaccionCompNal ocComprobante = new cPolizasPolizaTransaccionCompNal();
                        //    ocComprobante.generar_CFDI(ocTransaccion.ROW_ID, oDataRow2["INVOICE_ID"].ToString(), oData_ERPp
                        //        , ocEMPRESA_CONEXIONp);
                        //}
                    }
                    
                }
            }
        }
        private void generar_CASH_DISBURSE_DIST(string ROW_ID_POLIZA, string BATCH_ID, string CURRENCY_ID, cCONEXCION oData_ERPp, cEMPRESA_CONEXION ocEMPRESA_CONEXIONp)
        {
            string sSQL;
            sSQL = " SELECT ";
            sSQL += " BANK_ACCOUNT_ID + ' - ' + CAST(CONTROL_NO AS VARCHAR(500)) + ' ' + ISNULL((SELECT  TOP 1    ISNULL(CAST(CASH_DISBURSEMENT.CHECK_NO as varchar(50)), '') + ' ' + ISNULL(VENDOR.ID,'') + ' ' + ISNULL(VENDOR.NAME,'') FROM CASH_DISBURSEMENT INNER JOIN  VENDOR ON CASH_DISBURSEMENT.VENDOR_ID = VENDOR.ID WHERE (CASH_DISBURSEMENT.BANK_ACCOUNT_ID = CASH_DISBURSE_DIST.BANK_ACCOUNT_ID) AND (CASH_DISBURSEMENT.CONTROL_NO = CASH_DISBURSE_DIST.CONTROL_NO)),(SELECT  TOP 1 CASH_DISBURSEMENT.NAME FROM  CASH_DISBURSEMENT  WHERE  (CASH_DISBURSEMENT.BANK_ACCOUNT_ID = CASH_DISBURSE_DIST.BANK_ACCOUNT_ID) AND (CASH_DISBURSEMENT.CONTROL_NO = CASH_DISBURSE_DIST.CONTROL_NO))) ";
            sSQL += " AS REFERENCIA ";
            sSQL += ", GL_ACCOUNT_ID ";
            sSQL += ", (SELECT TOP 1 DESCRIPTION FROM ACCOUNT WHERE  ID=GL_ACCOUNT_ID) as DESCRIPCION ";
            sSQL += ", 'CR' = CASE WHEN AMOUNT_TYPE = 'CR' THEN AMOUNT ELSE 0 END ";
            sSQL += ", 'DR' = CASE WHEN AMOUNT_TYPE = 'DR' THEN AMOUNT ELSE 0 END ";
            sSQL += ",POSTING_DATE ";
            sSQL += ",POSTING_STATUS ";
            sSQL += ",(SELECT TOP 1 TYPE FROM  JOURNAL_BATCH WHERE ID = BATCH_ID) as TYPE ";
            sSQL += ",(SELECT TOP 1 USER_ID FROM  JOURNAL_BATCH WHERE ID = BATCH_ID) as BATCH_USER_ID ";
            sSQL += ",BATCH_ID,BANK_ACCOUNT_ID,CONTROL_NO ";
            sSQL += ", ISNULL((SELECT  TOP 1 ISNULL(VENDOR.ID,'') FROM CASH_DISBURSEMENT INNER JOIN  VENDOR ON CASH_DISBURSEMENT.VENDOR_ID = VENDOR.ID WHERE (CASH_DISBURSEMENT.BANK_ACCOUNT_ID = CASH_DISBURSE_DIST.BANK_ACCOUNT_ID) AND (CASH_DISBURSEMENT.CONTROL_NO = CASH_DISBURSE_DIST.CONTROL_NO)),(SELECT  TOP 1 CASH_DISBURSEMENT.NAME FROM  CASH_DISBURSEMENT  WHERE  (CASH_DISBURSEMENT.BANK_ACCOUNT_ID = CASH_DISBURSE_DIST.BANK_ACCOUNT_ID) AND (CASH_DISBURSEMENT.CONTROL_NO = CASH_DISBURSE_DIST.CONTROL_NO))) as VENDOR_ID ";
            sSQL += ",(select B.GL_ACCOUNT_ID from BANK_ACCOUNT B WHERE B.ID=CASH_DISBURSE_DIST.BANK_ACCOUNT_ID) as BANK_ACCOUNT_GL_ACCOUNT_ID   ";
            sSQL += ",CURRENCY_ID ";
            sSQL += " FROM CASH_DISBURSE_DIST ";
            sSQL += " WHERE 1=1 AND BATCH_ID='" + BATCH_ID + "' AND POSTING_STATUS='P' AND CURRENCY_ID='" + CURRENCY_ID + "'";

            DataTable oDataTable = oData_ERPp.EjecutarConsulta(sSQL);

            foreach (DataRow oDataRow in oDataTable.Rows)
            {
                cTransaccion ocTransaccion = new cTransaccion();
                ocTransaccion.numCtaField = oDataRow["GL_ACCOUNT_ID"].ToString();
                ocTransaccion.ROW_ID_POLIZA = ROW_ID_POLIZA;
                ocTransaccion.conceptoField = ocEMPRESA_CONEXIONp.VISUAL_ENTITY_ID + " " + cEMPRESA_DESCRIPCION.cambiar_descripcion(oDataRow["REFERENCIA"].ToString());
                ocTransaccion.debeField = double.Parse(oDataRow["DR"].ToString());
                ocTransaccion.haberField = double.Parse(oDataRow["CR"].ToString());

                if (ocTransaccion.debeField < 0)
                {
                    ocTransaccion.haberField += Math.Abs(ocTransaccion.debeField);
                    ocTransaccion.debeField = 0;
                }
                if (ocTransaccion.haberField < 0)
                {
                    ocTransaccion.debeField += Math.Abs(ocTransaccion.haberField);
                    ocTransaccion.haberField = 0;
                }

                ocTransaccion.DOCUMENTO = oDataRow["CONTROL_NO"].ToString() + "|" + oDataRow["BANK_ACCOUNT_ID"].ToString();
                ocTransaccion.TIPO = "APC";
                ocTransaccion.ENTE = oDataRow["VENDOR_ID"].ToString();

                ocTransaccion.monedaField = oDataRow["CURRENCY_ID"].ToString();
                if (ocTransaccion.guardar(true, true))
                {
                    //Guardar Cheques
                    //generar_Cheque(ocTransaccion.ROW_ID, oDataRow["CONTROL_NO"].ToString(), oDataRow["BANK_ACCOUNT_ID"].ToString());
                    //Guardar Transacciones
                    //Cargar todas las lineas que se le pagaron                    
                    sSQL = " SELECT PAYABLE.INVOICE_ID,PAYABLE.VENDOR_ID ";
                    sSQL += " FROM CASH_DISBURSE_LINE INNER JOIN CASH_DISBURSEMENT ON CASH_DISBURSE_LINE.BANK_ACCOUNT_ID = CASH_DISBURSEMENT.BANK_ACCOUNT_ID AND CASH_DISBURSE_LINE.CONTROL_NO = CASH_DISBURSEMENT.CONTROL_NO INNER JOIN PAYABLE ON CASH_DISBURSE_LINE.VOUCHER_ID = PAYABLE.VOUCHER_ID AND CASH_DISBURSEMENT.VENDOR_ID = PAYABLE.VENDOR_ID ";
                    sSQL+=" WHERE CASH_DISBURSEMENT.BANK_ACCOUNT_ID='" + oDataRow["BANK_ACCOUNT_ID"].ToString() + "' AND CASH_DISBURSEMENT.CONTROL_NO='" + oDataRow["CONTROL_NO"].ToString() + "'";
                    DataTable oDataTable2 = oData_ERPp.EjecutarConsulta(sSQL);
                    foreach (DataRow oDataRow2 in oDataTable2.Rows)
                    {
                        //if (importar.Checked)
                        //{
                        //    buscar_cfdi_proveedor(ocTransaccion.ROW_ID, "Proveedor", "APC", oDataRow["CONTROL_NO"].ToString() + "|" + oDataRow["BANK_ACCOUNT_ID"].ToString()
                        //        , ocTransaccion.ROW_ID_POLIZA, ocTransaccion.ENTE);
                        //}
                    }

                    //Cargar los pagos realizados y  ponerlo en la cuenta del Banco 
                    if (oDataRow["BANK_ACCOUNT_GL_ACCOUNT_ID"].ToString() == oDataRow["GL_ACCOUNT_ID"].ToString())
                    {
                        cPolizasPolizaTransaccionCompNal ocPolizasPolizaTransaccionCompNal = new cPolizasPolizaTransaccionCompNal();
                        ocPolizasPolizaTransaccionCompNal.actualizar("PAGO", oDataRow["CONTROL_NO"].ToString() + "|" + oDataRow["BANK_ACCOUNT_ID"].ToString()
                            , ocTransaccion.ROW_ID);
                    }

                    
                }
            }
        }
        private void generar_PAYABLE_DIST(string ROW_ID_POLIZA, string BATCH_ID, string CURRENCY_ID, cCONEXCION oData_ERPp, cEMPRESA_CONEXION ocEMPRESA_CONEXIONp)
        {
            string sSQL;
            sSQL = " SELECT ";
            sSQL += " CAST(VOUCHER_ID AS VARCHAR(500)) + ' ' + ISNULL ((SELECT TOP 1 ISNULL(PAYABLE.INVOICE_ID,'') + ' ' + ISNULL(VENDOR.ID,'')  + ' ' + ISNULL(VENDOR.NAME,'') FROM  PAYABLE INNER JOIN   VENDOR ON PAYABLE.VENDOR_ID = VENDOR.ID WHERE PAYABLE.VOUCHER_ID = PAYABLE_DIST.VOUCHER_ID),'') ";
            sSQL += " AS REFERENCIA ";
            sSQL += ", ISNULL ((SELECT TOP 1 PAYABLE.INVOICE_ID FROM  PAYABLE WHERE PAYABLE.VOUCHER_ID = PAYABLE_DIST.VOUCHER_ID),'') AS INVOICE_ID ";
            sSQL += ", GL_ACCOUNT_ID ";
            sSQL += ", (SELECT TOP 1 DESCRIPTION FROM ACCOUNT WHERE  ID=GL_ACCOUNT_ID) as DESCRIPCION ";
            sSQL += ", 'CR' = CASE WHEN AMOUNT_TYPE = 'CR' THEN AMOUNT ELSE 0 END ";
            sSQL += ", 'DR' = CASE WHEN AMOUNT_TYPE = 'DR' THEN AMOUNT ELSE 0 END ";
            sSQL += ",POSTING_DATE ";
            sSQL += ",POSTING_STATUS ";
            sSQL += ",(SELECT TOP 1 TYPE FROM  JOURNAL_BATCH WHERE ID = BATCH_ID) as TYPE ";
            sSQL += ",(SELECT TOP 1 USER_ID FROM  JOURNAL_BATCH WHERE ID = BATCH_ID) as BATCH_USER_ID ";
            sSQL += ",BATCH_ID ";
            //sSQL += ",ENTITY_ID   ";
            sSQL += ",CURRENCY_ID,VOUCHER_ID";
            sSQL += ",(SELECT TOP 1 ISNULL(VENDOR.ID,'') FROM  PAYABLE INNER JOIN   VENDOR ON PAYABLE.VENDOR_ID = VENDOR.ID WHERE PAYABLE.VOUCHER_ID = PAYABLE_DIST.VOUCHER_ID) as VENDOR_ID ";
            sSQL += ",(SELECT TOP 1 PAYABLE.TOTAL_AMOUNT FROM  PAYABLE INNER JOIN   VENDOR ON PAYABLE.VENDOR_ID = VENDOR.ID WHERE PAYABLE.VOUCHER_ID = PAYABLE_DIST.VOUCHER_ID) as TOTAL_AMOUNT ";
            sSQL += " FROM PAYABLE_DIST ";
            sSQL += " WHERE 1=1 AND BATCH_ID='" + BATCH_ID + "' AND POSTING_STATUS='P' AND CURRENCY_ID='" + CURRENCY_ID + "'";

            DataTable oDataTable = oData_ERPp.EjecutarConsulta(sSQL);

            foreach (DataRow oDataRow in oDataTable.Rows)
            {
                cTransaccion ocTransaccion = new cTransaccion();
                ocTransaccion.numCtaField = oDataRow["GL_ACCOUNT_ID"].ToString();
                ocTransaccion.ROW_ID_POLIZA = ROW_ID_POLIZA;
                ocTransaccion.conceptoField = ocEMPRESA_CONEXIONp.VISUAL_ENTITY_ID + " " + cEMPRESA_DESCRIPCION.cambiar_descripcion(oDataRow["REFERENCIA"].ToString());
                ocTransaccion.debeField = double.Parse(oDataRow["DR"].ToString());
                ocTransaccion.haberField = double.Parse(oDataRow["CR"].ToString());

                if (ocTransaccion.debeField < 0)
                {
                    ocTransaccion.haberField += Math.Abs(ocTransaccion.debeField);
                    ocTransaccion.debeField = 0;
                }
                if (ocTransaccion.haberField < 0)
                {
                    ocTransaccion.debeField += Math.Abs(ocTransaccion.haberField);
                    ocTransaccion.haberField = 0;
                }

                ocTransaccion.DOCUMENTO = oDataRow["VOUCHER_ID"].ToString();
                ocTransaccion.TIPO = "API";
                ocTransaccion.ENTE = oDataRow["VENDOR_ID"].ToString();

                ocTransaccion.monedaField = oDataRow["CURRENCY_ID"].ToString();
                if (ocTransaccion.guardar(true, true))
                {
                    //Guardar comprobantes
                    //if (importar.Checked)
                    //{
                    //    buscar_cfdi_proveedor(ocTransaccion.ROW_ID, "Proveedor", "API", oDataRow["INVOICE_ID"].ToString()                        
                    //    , ocTransaccion.ROW_ID_POLIZA, ocTransaccion.ENTE);

                    //}
                }
            }
        }

        private void generar_INDIRECT_DIST(string ROW_ID_POLIZA, string BATCH_ID, string CURRENCY_ID, cCONEXCION oData_ERPp, cEMPRESA_CONEXION ocEMPRESA_CONEXIONp)
        {
            string sSQL;
            sSQL = " SELECT ";
            sSQL += " CAST(TRANSACTION_ID AS VARCHAR(500)) + ' ' + ISNULL((SELECT EMPLOYEE_ID FROM LABOR_TICKET WHERE (TRANSACTION_ID = INDIRECT_DIST.TRANSACTION_ID)),'')  ";
            sSQL += " AS REFERENCIA ";
            sSQL += ", GL_ACCOUNT_ID ";
            sSQL += ", (SELECT TOP 1 DESCRIPTION FROM ACCOUNT WHERE  ID=GL_ACCOUNT_ID) as DESCRIPCION ";
            sSQL += ", 'CR' = CASE WHEN AMOUNT_TYPE = 'CR' THEN AMOUNT ELSE 0 END ";
            sSQL += ", 'DR' = CASE WHEN AMOUNT_TYPE = 'DR' THEN AMOUNT ELSE 0 END ";
            sSQL += ",POSTING_DATE ";
            sSQL += ",POSTING_STATUS ";
            sSQL += ",(SELECT TOP 1 TYPE FROM  JOURNAL_BATCH WHERE ID = BATCH_ID) as TYPE ";
            sSQL += ",(SELECT TOP 1 USER_ID FROM  JOURNAL_BATCH WHERE ID = BATCH_ID) as BATCH_USER_ID ";
            sSQL += ",BATCH_ID ";
            //sSQL += ",ENTITY_ID   ";
            sSQL += ",CURRENCY_ID ";
            sSQL += " FROM INDIRECT_DIST ";
            sSQL += " WHERE 1=1 AND BATCH_ID='" + BATCH_ID + "' AND POSTING_STATUS='P' AND CURRENCY_ID='" + CURRENCY_ID + "'";

            DataTable oDataTable = oData_ERPp.EjecutarConsulta(sSQL);
            foreach (DataRow oDataRow in oDataTable.Rows)
            {
                cTransaccion ocTransaccion = new cTransaccion();
                ocTransaccion.numCtaField = oDataRow["GL_ACCOUNT_ID"].ToString();
                ocTransaccion.ROW_ID_POLIZA = ROW_ID_POLIZA;
                ocTransaccion.conceptoField = ocEMPRESA_CONEXIONp.VISUAL_ENTITY_ID + " " + cEMPRESA_DESCRIPCION.cambiar_descripcion(oDataRow["REFERENCIA"].ToString());
                ocTransaccion.debeField = double.Parse(oDataRow["DR"].ToString());
                ocTransaccion.haberField = double.Parse(oDataRow["CR"].ToString());

                if (ocTransaccion.debeField < 0)
                {
                    ocTransaccion.haberField += Math.Abs(ocTransaccion.debeField);
                    ocTransaccion.debeField = 0;
                }
                if (ocTransaccion.haberField < 0)
                {
                    ocTransaccion.debeField += Math.Abs(ocTransaccion.haberField);
                    ocTransaccion.haberField = 0;
                }

                ocTransaccion.monedaField = oDataRow["CURRENCY_ID"].ToString();
                if (ocTransaccion.guardar(true, true))
                {
                    //Guardar comprobantes
                    //generar_CFDI(ocTransaccion.ROW_ID, oDataRow["INVOICE_ID"].ToString());
                }
            }
        }

        private void generar_ADJUSTMENT_DIST(string ROW_ID_POLIZA, string BATCH_ID, string CURRENCY_ID, cCONEXCION oData_ERPp, cEMPRESA_CONEXION ocEMPRESA_CONEXIONp)
        {
            string sSQL;
            sSQL = " SELECT ";
            sSQL += " CAST(TRANSACTION_ID AS VARCHAR(500)) + ' ' + ISNULL((SELECT ISNULL(PART.ID,'') + ' '+  ISNULL(PART.DESCRIPTION,'') FROM INVENTORY_TRANS INNER JOIN PART ON INVENTORY_TRANS.PART_ID = PART.ID WHERE INVENTORY_TRANS.TRANSACTION_ID = ADJUSTMENT_DIST.TRANSACTION_ID),'') ";
            sSQL += " AS REFERENCIA ";
            sSQL += ", GL_ACCOUNT_ID ";
            sSQL += ", (SELECT TOP 1 DESCRIPTION FROM ACCOUNT WHERE  ID=GL_ACCOUNT_ID) as DESCRIPCION ";
            sSQL += ", 'CR' = CASE WHEN AMOUNT_TYPE = 'CR' THEN AMOUNT ELSE 0 END ";
            sSQL += ", 'DR' = CASE WHEN AMOUNT_TYPE = 'DR' THEN AMOUNT ELSE 0 END ";
            sSQL += ",POSTING_DATE ";
            sSQL += ",POSTING_STATUS ";
            sSQL += ",(SELECT TOP 1 TYPE FROM  JOURNAL_BATCH WHERE ID = BATCH_ID) as TYPE ";
            sSQL += ",(SELECT TOP 1 USER_ID FROM  JOURNAL_BATCH WHERE ID = BATCH_ID) as BATCH_USER_ID ";
            sSQL += ",BATCH_ID ";
            //sSQL += ",ENTITY_ID   ";
            sSQL += ",CURRENCY_ID ";
            sSQL += " FROM ADJUSTMENT_DIST ";
            sSQL += " WHERE 1=1 AND BATCH_ID='" + BATCH_ID + "' AND POSTING_STATUS='P' AND CURRENCY_ID='" + CURRENCY_ID + "'";

            DataTable oDataTable = oData_ERPp.EjecutarConsulta(sSQL);
            foreach (DataRow oDataRow in oDataTable.Rows)
            {
                cTransaccion ocTransaccion = new cTransaccion();
                ocTransaccion.numCtaField = oDataRow["GL_ACCOUNT_ID"].ToString();
                ocTransaccion.ROW_ID_POLIZA = ROW_ID_POLIZA;

                ocTransaccion.conceptoField = ocEMPRESA_CONEXIONp.VISUAL_ENTITY_ID + " " + cEMPRESA_DESCRIPCION.cambiar_descripcion(oDataRow["REFERENCIA"].ToString());


                ocTransaccion.debeField = double.Parse(oDataRow["DR"].ToString());
                ocTransaccion.haberField = double.Parse(oDataRow["CR"].ToString());

                if (ocTransaccion.debeField < 0)
                {
                    ocTransaccion.haberField += Math.Abs(ocTransaccion.debeField);
                    ocTransaccion.debeField = 0;
                }
                if (ocTransaccion.haberField < 0)
                {
                    ocTransaccion.debeField += Math.Abs(ocTransaccion.haberField);
                    ocTransaccion.haberField = 0;
                }

                ocTransaccion.monedaField = oDataRow["CURRENCY_ID"].ToString();
                if (ocTransaccion.guardar(true, true))
                {
                    //Guardar comprobantes
                    //generar_CFDI(ocTransaccion.ROW_ID, oDataRow["INVOICE_ID"].ToString());
                }
            }
        }

        private void generar_PURCHASE_DIST(string ROW_ID_POLIZA, string BATCH_ID, string CURRENCY_ID, cCONEXCION oData_ERPp, cEMPRESA_CONEXION ocEMPRESA_CONEXIONp)
        {
            string sSQL;
            sSQL = " SELECT ";
            sSQL += " CAST(PURC_ORDER_ID AS VARCHAR(500)) ";
            sSQL += " + ' ' + ISNULL((SELECT ISNULL(PURCHASE_ORDER.VENDOR_ID,'') + ' ' + ISNULL(VENDOR.NAME,'') FROM PURCHASE_ORDER INNER JOIN VENDOR ON PURCHASE_ORDER.VENDOR_ID = VENDOR.ID WHERE PURCHASE_ORDER.ID = PURCHASE_DIST.PURC_ORDER_ID),'') ";
            sSQL += " AS REFERENCIA ";
            sSQL += ", GL_ACCOUNT_ID ";
            sSQL += ", (SELECT TOP 1 DESCRIPTION FROM ACCOUNT WHERE  ID=GL_ACCOUNT_ID) as DESCRIPCION ";
            sSQL += ", 'CR' = CASE WHEN AMOUNT_TYPE = 'CR' THEN AMOUNT ELSE 0 END ";
            sSQL += ", 'DR' = CASE WHEN AMOUNT_TYPE = 'DR' THEN AMOUNT ELSE 0 END ";
            sSQL += ",POSTING_DATE ";
            sSQL += ",POSTING_STATUS ";
            sSQL += ",(SELECT TOP 1 TYPE FROM  JOURNAL_BATCH WHERE ID = BATCH_ID) as TYPE ";
            sSQL += ",(SELECT TOP 1 USER_ID FROM  JOURNAL_BATCH WHERE ID = BATCH_ID) as BATCH_USER_ID ";
            sSQL += ",BATCH_ID ";
            //sSQL += ",ENTITY_ID   ";
            sSQL += ",CURRENCY_ID ";
            sSQL += " FROM PURCHASE_DIST ";
            sSQL += " WHERE 1=1 AND BATCH_ID='" + BATCH_ID + "' AND POSTING_STATUS='P' AND CURRENCY_ID='" + CURRENCY_ID + "'";

            DataTable oDataTable = oData_ERPp.EjecutarConsulta(sSQL);
            foreach (DataRow oDataRow in oDataTable.Rows)
            {
                cTransaccion ocTransaccion = new cTransaccion();
                ocTransaccion.numCtaField = oDataRow["GL_ACCOUNT_ID"].ToString();
                ocTransaccion.ROW_ID_POLIZA = ROW_ID_POLIZA;
                ocTransaccion.conceptoField = ocEMPRESA_CONEXIONp.VISUAL_ENTITY_ID + " " + cEMPRESA_DESCRIPCION.cambiar_descripcion(oDataRow["REFERENCIA"].ToString());
                ocTransaccion.debeField = double.Parse(oDataRow["DR"].ToString());
                ocTransaccion.haberField = double.Parse(oDataRow["CR"].ToString());

                if (ocTransaccion.debeField < 0)
                {
                    ocTransaccion.haberField += Math.Abs(ocTransaccion.debeField);
                    ocTransaccion.debeField = 0;
                }
                if (ocTransaccion.haberField < 0)
                {
                    ocTransaccion.debeField += Math.Abs(ocTransaccion.haberField);
                    ocTransaccion.haberField = 0;
                }

                ocTransaccion.monedaField = oDataRow["CURRENCY_ID"].ToString();
                if (ocTransaccion.guardar(true, true))
                {
                    //Guardar comprobantes
                    //generar_CFDI(ocTransaccion.ROW_ID, oDataRow["INVOICE_ID"].ToString());
                }
            }
        }

        private void generar_SHIPMENT_DIST(string ROW_ID_POLIZA, string BATCH_ID, string CURRENCY_ID, cCONEXCION oData_ERPp, cEMPRESA_CONEXION ocEMPRESA_CONEXIONp)
        {
            string sSQL;
            sSQL = " SELECT ";
            sSQL += " ISNULL(CUST_ORDER_ID,'') + ' - ' + ISNULL(PART_ID,'') + ' ' + ";
            sSQL += " ISNULL((	SELECT TOP 1 ISNULL(CUSTOMER_ORDER.CUSTOMER_ID,'') + ' ' + ISNULL(CUSTOMER.NAME,'') FROM CUSTOMER_ORDER INNER JOIN CUSTOMER ON CUSTOMER_ORDER.CUSTOMER_ID = CUSTOMER.ID ";
            sSQL += " WHERE (CUSTOMER_ORDER.ID = SHIPMENT_DIST.CUST_ORDER_ID)),'') ";
            sSQL += " AS REFERENCIA ";
            sSQL += ", GL_ACCOUNT_ID ";
            sSQL += ", (SELECT TOP 1 DESCRIPTION FROM ACCOUNT WHERE  ID=GL_ACCOUNT_ID) as DESCRIPCION ";
            sSQL += ", 'CR' = CASE WHEN AMOUNT_TYPE = 'CR' THEN AMOUNT ELSE 0 END ";
            sSQL += ", 'DR' = CASE WHEN AMOUNT_TYPE = 'DR' THEN AMOUNT ELSE 0 END ";
            sSQL += ",POSTING_DATE ";
            sSQL += ",POSTING_STATUS ";
            sSQL += ",(SELECT TOP 1 TYPE FROM  JOURNAL_BATCH WHERE ID = BATCH_ID) as TYPE ";
            sSQL += ",(SELECT TOP 1 USER_ID FROM  JOURNAL_BATCH WHERE ID = BATCH_ID) as BATCH_USER_ID ";
            sSQL += ",BATCH_ID ";
            //sSQL += ",ENTITY_ID   ";
            sSQL += ",CURRENCY_ID ";
            sSQL += " FROM SHIPMENT_DIST ";
            sSQL += " WHERE 1=1 AND BATCH_ID='" + BATCH_ID + "' AND POSTING_STATUS='P' AND CURRENCY_ID='" + CURRENCY_ID + "'";

            DataTable oDataTable = oData_ERPp.EjecutarConsulta(sSQL);
            foreach (DataRow oDataRow in oDataTable.Rows)
            {
                cTransaccion ocTransaccion = new cTransaccion();
                ocTransaccion.numCtaField = oDataRow["GL_ACCOUNT_ID"].ToString();
                ocTransaccion.ROW_ID_POLIZA = ROW_ID_POLIZA;
                ocTransaccion.conceptoField = ocEMPRESA_CONEXIONp.VISUAL_ENTITY_ID + " " + cEMPRESA_DESCRIPCION.cambiar_descripcion(oDataRow["REFERENCIA"].ToString());
                ocTransaccion.debeField = double.Parse(oDataRow["DR"].ToString());
                ocTransaccion.haberField = double.Parse(oDataRow["CR"].ToString());

                if (ocTransaccion.debeField < 0)
                {
                    ocTransaccion.haberField += Math.Abs(ocTransaccion.debeField);
                    ocTransaccion.debeField = 0;
                }
                if (ocTransaccion.haberField < 0)
                {
                    ocTransaccion.debeField += Math.Abs(ocTransaccion.haberField);
                    ocTransaccion.haberField = 0;
                }
                ocTransaccion.monedaField = oDataRow["CURRENCY_ID"].ToString();
                if (ocTransaccion.guardar(true, true))
                {
                    //Guardar comprobantes
                    //generar_CFDI(ocTransaccion.ROW_ID, oDataRow["INVOICE_ID"].ToString());
                }
            }

        }

        private void generar_WIP_RECEIPT_DIST(string ROW_ID_POLIZA, string BATCH_ID, string CURRENCY_ID, cCONEXCION oData_ERPp, cEMPRESA_CONEXION ocEMPRESA_CONEXIONp)
        {
            string sSQL;
            sSQL = " SELECT ";
            sSQL += " CAST(WORKORDER_BASE_ID + '/' + WORKORDER_LOT_ID + '.' + WORKORDER_SPLIT_ID + '.' + WORKORDER_SUB_ID AS VARCHAR(500)) ";
            sSQL += " + ' ' + ISNULL((SELECT TOP 1 PART_ID FROM WORK_ORDER WHERE TYPE='W' AND BASE_ID=WIP_RECEIPT_DIST.WORKORDER_BASE_ID AND LOT_ID=WIP_RECEIPT_DIST.WORKORDER_LOT_ID AND SPLIT_ID=WIP_RECEIPT_DIST.WORKORDER_SPLIT_ID AND SUB_ID='0'),'') ";
            sSQL += " + ' ' + ISNULL((SELECT TOP 1 ID FROM PART WHERE ID=(SELECT TOP 1 PART_ID FROM WORK_ORDER WHERE TYPE='W' AND BASE_ID=WIP_RECEIPT_DIST.WORKORDER_BASE_ID AND LOT_ID=WIP_RECEIPT_DIST.WORKORDER_LOT_ID AND SPLIT_ID=WIP_RECEIPT_DIST.WORKORDER_SPLIT_ID AND SUB_ID='0')),'') ";
            sSQL += " + ' ' + ISNULL((SELECT TOP 1 DESCRIPTION FROM PART WHERE ID=(SELECT TOP 1 PART_ID FROM WORK_ORDER WHERE TYPE='W' AND BASE_ID=WIP_RECEIPT_DIST.WORKORDER_BASE_ID AND LOT_ID=WIP_RECEIPT_DIST.WORKORDER_LOT_ID AND SPLIT_ID=WIP_RECEIPT_DIST.WORKORDER_SPLIT_ID AND SUB_ID='0')),'')  ";
            sSQL += " AS REFERENCIA ";
            sSQL += ", GL_ACCOUNT_ID ";
            sSQL += ", (SELECT TOP 1 DESCRIPTION FROM ACCOUNT WHERE  ID=GL_ACCOUNT_ID) as DESCRIPCION ";
            sSQL += ", 'CR' = CASE WHEN AMOUNT_TYPE = 'CR' THEN AMOUNT ELSE 0 END ";
            sSQL += ", 'DR' = CASE WHEN AMOUNT_TYPE = 'DR' THEN AMOUNT ELSE 0 END ";
            sSQL += ",POSTING_DATE ";
            sSQL += ",POSTING_STATUS ";
            sSQL += ",(SELECT TOP 1 TYPE FROM  JOURNAL_BATCH WHERE ID = BATCH_ID) as TYPE ";
            sSQL += ",(SELECT TOP 1 USER_ID FROM  JOURNAL_BATCH WHERE ID = BATCH_ID) as BATCH_USER_ID ";
            sSQL += ",BATCH_ID ";
            //sSQL += ",ENTITY_ID   ";
            sSQL += ",CURRENCY_ID ";
            sSQL += " FROM WIP_RECEIPT_DIST ";
            sSQL += " WHERE 1=1 AND BATCH_ID='" + BATCH_ID + "' AND POSTING_STATUS='P' AND CURRENCY_ID='" + CURRENCY_ID + "'";


            DataTable oDataTable = oData_ERPp.EjecutarConsulta(sSQL);
            foreach (DataRow oDataRow in oDataTable.Rows)
            {
                cTransaccion ocTransaccion = new cTransaccion();
                ocTransaccion.numCtaField = oDataRow["GL_ACCOUNT_ID"].ToString();
                ocTransaccion.ROW_ID_POLIZA = ROW_ID_POLIZA;
                ocTransaccion.conceptoField = ocEMPRESA_CONEXIONp.VISUAL_ENTITY_ID + " " + cEMPRESA_DESCRIPCION.cambiar_descripcion(oDataRow["REFERENCIA"].ToString());
                ocTransaccion.debeField = double.Parse(oDataRow["DR"].ToString());
                ocTransaccion.haberField = double.Parse(oDataRow["CR"].ToString());

                if (ocTransaccion.debeField < 0)
                {
                    ocTransaccion.haberField += Math.Abs(ocTransaccion.debeField);
                    ocTransaccion.debeField = 0;
                }
                if (ocTransaccion.haberField < 0)
                {
                    ocTransaccion.debeField += Math.Abs(ocTransaccion.haberField);
                    ocTransaccion.haberField = 0;
                }

                ocTransaccion.monedaField = oDataRow["CURRENCY_ID"].ToString();
                if (ocTransaccion.guardar(true, true))
                {
                    //Guardar comprobantes
                    //generar_CFDI(ocTransaccion.ROW_ID, oDataRow["INVOICE_ID"].ToString());
                }
            }
        }

        private void generar_GJ_DIST(string ROW_ID_POLIZA, string BATCH_ID, string CURRENCY_ID, cCONEXCION oData_ERPp, cEMPRESA_CONEXION ocEMPRESA_CONEXIONp)
        {
            string sSQL;
            sSQL = " SELECT GJ_DIST.GJ_ID,";
            sSQL += " 'TR:' + ISNULL(CAST(GJ_DIST.GJ_ID AS VARCHAR(500)),'') + ISNULL(GJ_LINE.REFERENCE,'') ";
            sSQL += " AS REFERENCIA ";
            sSQL += ", GJ_DIST.GL_ACCOUNT_ID as GL_ACCOUNT_ID ";
            sSQL += ", (SELECT TOP 1 DESCRIPTION FROM ACCOUNT WHERE ID = GJ_DIST.GL_ACCOUNT_ID) as DESCRIPCION ";
            sSQL += ", CASE WHEN AMOUNT_TYPE = 'CR' THEN ISNULL(GJ_LINE.CREDIT_AMOUNT,0) * ISNULL((SELECT TOP 1 SELL_RATE FROM GJ WHERE GJ.ID=GJ_LINE.GJ_ID AND '" + ocEMPRESA_CONEXIONp.VISUAL_CURRENCY_ID+ "'=GJ_DIST.CURRENCY_ID),1) END AS 'CR' ";
            sSQL += ", CASE WHEN AMOUNT_TYPE = 'DR' THEN ISNULL(GJ_LINE.DEBIT_AMOUNT,0) * ISNULL((SELECT TOP 1 SELL_RATE FROM GJ WHERE GJ.ID=GJ_LINE.GJ_ID AND '" + ocEMPRESA_CONEXIONp.VISUAL_CURRENCY_ID + "'=GJ_DIST.CURRENCY_ID),1) END AS 'DR' ";
            sSQL += ", GJ_DIST.POSTING_DATE as POSTING_DATE ";
            sSQL += ", GJ_DIST.POSTING_STATUS as POSTING_STATUS ";
            sSQL += ",(SELECT TOP 1 TYPE FROM  JOURNAL_BATCH WHERE ID = GJ_DIST.BATCH_ID) as TYPE ";
            sSQL += ",(SELECT TOP 1 USER_ID FROM  JOURNAL_BATCH WHERE ID = GJ_DIST.BATCH_ID) as BATCH_USER_ID ";
            sSQL += ",GJ_DIST.BATCH_ID as BATCH_ID ";
            sSQL += ",GJ_DIST.CURRENCY_ID as CURRENCY_ID ";
            sSQL += " FROM GJ_DIST INNER JOIN GJ_LINE ON GJ_DIST.GJ_ID = GJ_LINE.GJ_ID AND GJ_DIST.GL_ACCOUNT_ID = GJ_LINE.GL_ACCOUNT_ID ";
            sSQL += " WHERE 1=1 AND GJ_DIST.BATCH_ID='" + BATCH_ID + "' AND GJ_DIST.POSTING_STATUS='P'  AND CURRENCY_ID='" + CURRENCY_ID + "'";
            DataTable oDataTable = oData_ERPp.EjecutarConsulta(sSQL);
            foreach (DataRow oDataRow in oDataTable.Rows)
            {
                cTransaccion ocTransaccion = new cTransaccion();
                ocTransaccion.numCtaField = oDataRow["GL_ACCOUNT_ID"].ToString();
                ocTransaccion.ROW_ID_POLIZA = ROW_ID_POLIZA;
                ocTransaccion.conceptoField = ocEMPRESA_CONEXIONp.VISUAL_ENTITY_ID + " " + cEMPRESA_DESCRIPCION.cambiar_descripcion(oDataRow["REFERENCIA"].ToString());
                ocTransaccion.debeField = double.Parse(oData.IsNullNumero(oDataRow["DR"].ToString()));
                ocTransaccion.haberField = double.Parse(oData.IsNullNumero(oDataRow["CR"].ToString()));

                if (ocTransaccion.debeField < 0)
                {
                    ocTransaccion.haberField += Math.Abs(ocTransaccion.debeField);
                    ocTransaccion.debeField = 0;
                }
                if (ocTransaccion.haberField < 0)
                {
                    ocTransaccion.debeField += Math.Abs(ocTransaccion.haberField);
                    ocTransaccion.haberField = 0;
                }

                ocTransaccion.monedaField = oDataRow["CURRENCY_ID"].ToString();
                if (ocTransaccion.guardar(true, true))
                {
                    //Guardar comprobantes
                    //generar_CFDI(ocTransaccion.ROW_ID, oDataRow["INVOICE_ID"].ToString());
                    //Cargar los pagos realizados
                    cPolizasPolizaTransaccionCompNal ocPolizasPolizaTransaccionCompNal = new cPolizasPolizaTransaccionCompNal();
                    ocPolizasPolizaTransaccionCompNal.actualizar("POLIZA", oDataRow["GJ_ID"].ToString()
                        , ocTransaccion.ROW_ID);
                }
            }
        }

        private void generar_BANK_ADJ_DIST(string ROW_ID_POLIZA, string BATCH_ID, string CURRENCY_ID, cCONEXCION oData_ERPp, cEMPRESA_CONEXION ocEMPRESA_CONEXIONp)
        {
            string sSQL;
            sSQL = " SELECT ";
            sSQL += " CAST(BANK_ACCOUNT_ID + ' - ' + ADJUSTMENT_ID AS VARCHAR(500)) + ' ' + ISNULL((SELECT REFERENCE FROM BANK_ADJUSTMENT WHERE (BANK_ACCOUNT_ID = BANK_ADJ_DIST.BANK_ACCOUNT_ID) AND (ADJUSTMENT_ID = BANK_ADJ_DIST.ADJUSTMENT_ID)),'') ";
            sSQL += " AS REFERENCIA ";
            sSQL += ", GL_ACCOUNT_ID ";
            sSQL += ", (SELECT TOP 1 DESCRIPTION FROM ACCOUNT WHERE  ID=GL_ACCOUNT_ID) as DESCRIPCION ";
            sSQL += ", 'CR' = CASE WHEN AMOUNT_TYPE = 'CR' THEN AMOUNT ELSE 0 END ";
            sSQL += ", 'DR' = CASE WHEN AMOUNT_TYPE = 'DR' THEN AMOUNT ELSE 0 END ";
            sSQL += ",POSTING_DATE ";
            sSQL += ",POSTING_STATUS ";
            sSQL += ",(SELECT TOP 1 TYPE FROM  JOURNAL_BATCH WHERE ID = BATCH_ID) as TYPE ";
            sSQL += ",(SELECT TOP 1 USER_ID FROM  JOURNAL_BATCH WHERE ID = BATCH_ID) as BATCH_USER_ID ";
            sSQL += ",BATCH_ID ";
            //sSQL += ",ENTITY_ID   ";
            sSQL += ",CURRENCY_ID ";
            sSQL += " FROM BANK_ADJ_DIST ";
            sSQL += " WHERE 1=1 AND BATCH_ID='" + BATCH_ID + "' AND POSTING_STATUS='P' AND CURRENCY_ID='" + CURRENCY_ID + "'";

            DataTable oDataTable = oData_ERPp.EjecutarConsulta(sSQL);
            foreach (DataRow oDataRow in oDataTable.Rows)
            {
                //Agregar las transferencias
                cTransaccion ocTransaccion = new cTransaccion();
                ocTransaccion.numCtaField = oDataRow["GL_ACCOUNT_ID"].ToString();
                ocTransaccion.ROW_ID_POLIZA = ROW_ID_POLIZA;
                ocTransaccion.conceptoField = ocEMPRESA_CONEXIONp.VISUAL_ENTITY_ID + " " + cEMPRESA_DESCRIPCION.cambiar_descripcion(oDataRow["REFERENCIA"].ToString());

                ocTransaccion.debeField = double.Parse(oDataRow["DR"].ToString());
                ocTransaccion.haberField = double.Parse(oDataRow["CR"].ToString());

                if (ocTransaccion.debeField < 0)
                {
                    ocTransaccion.haberField += Math.Abs(ocTransaccion.debeField);
                    ocTransaccion.debeField = 0;
                }
                if (ocTransaccion.haberField < 0)
                {
                    ocTransaccion.debeField += Math.Abs(ocTransaccion.haberField);
                    ocTransaccion.haberField = 0;
                }


                ocTransaccion.monedaField = oDataRow["CURRENCY_ID"].ToString();
                if (ocTransaccion.guardar(true, true))
                {
                    //Guardar comprobantes
                    //generar_CFDI(ocTransaccion.ROW_ID, oDataRow["INVOICE_ID"].ToString());
                }
            }
        }

        #endregion

        private void buttonX17_Click(object sender, EventArgs e)
        {
            eliminar_poliza();
        }

        private void eliminar_poliza()
        {
            dtgPoliza.EndEdit();
            if (dtgPoliza.SelectedCells.Count > 0)
            {
                DialogResult Resultado = MessageBoxEx.Show("�Desea eliminar las seleccionada?", Application.ProductName + "-" + Application.ProductVersion, MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                if (Resultado.ToString() != "Yes")
                {
                    return;
                }
                foreach (DataGridViewRow drv in dtgPoliza.SelectedRows)
                {
                    string ROW_ID = drv.Cells["ROW_ID_p"].Value.ToString();
                    cPoliza ocPoliza = (cPoliza)drv.Tag;
                    ocPoliza.eliminar(ocPoliza.ROW_ID);
                    dtgPoliza.Rows.Remove(drv);
                }
            }
            else
            {
                MessageBoxEx.Show("Seleccione alguna l�nea.", Application.ProductName + "-" + Application.ProductVersion, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }

        private void buttonX22_Click(object sender, EventArgs e)
        {
            importar_catalogo_polizas("", true);
        }

        private void FILTRO_DESCRIPCION_KeyPress(object sender, KeyPressEventArgs e)
        {
            filtrar();
        }

        private void FILTRO_CUENTA_KeyPress(object sender, KeyPressEventArgs e)
        {
            filtrar();
        }

        private void filtrar()
        {
            
            foreach(DataGridViewRow row in dtgPoliza.Rows)
            {
                if (row.Cells["numField"].Value.ToString().Contains(BUSCAR_BATCH_ID.Text))
                {
                    row.Visible=true;
                }
                else
                {
                    row.Visible=false;
                }

            }

        }

        private DataTable GetDataTableFromDGV(DataGridView dgv)
        {
            var dt = new DataTable();
            foreach (DataGridViewColumn column in dgv.Columns)
            {
                //if (column.Visible)
                //{
                // You could potentially name the column based on the DGV column name (beware of dupes)
                // or assign a type based on the data type of the data bound to this DGV column.

                //}
                if (column.DefaultCellStyle.Alignment == DataGridViewContentAlignment.MiddleRight)
                {
                    dt.Columns.Add(column.Name, typeof(decimal));
                }
                else
                {
                    dt.Columns.Add(column.Name);
                }
            }

            object[] cellValues = new object[dgv.Columns.Count];
            foreach (DataGridViewRow row in dgv.Rows)
            {
                for (int i = 0; i < row.Cells.Count; i++)
                {

                    cellValues[i] = row.Cells[i].Value;
                }
                dt.Rows.Add(cellValues);
            }

            return dt;
        }


        public void exportar(string nombre, DataGridView dtg)
        {

            var fileName = nombre + " " + DateTime.Now.ToString("yyyy-MM-dd-hh-mm-ss") + ".xlsx";
            if (folderBrowserDialog1.ShowDialog() == DialogResult.OK)
            {
                fileName = folderBrowserDialog1.SelectedPath + "\\" + fileName;
                System.IO.Stream myStream = new System.IO.FileStream(fileName, System.IO.FileMode.Create, System.IO.FileAccess.ReadWrite, System.IO.FileShare.None);

                using (ExcelPackage pck = new ExcelPackage())
                {

                    DataTable oDataTable = new DataTable();

                    oDataTable = GetDataTableFromDGV(dtg);
                    ExcelWorksheet ws1 = pck.Workbook.Worksheets.Add("Datos");
                    ws1.Cells["A1"].LoadFromDataTable(oDataTable, true);
                    pck.SaveAs(myStream);
                }
                myStream.Close();
                MessageBoxEx.Show("Exportaci�n del archivo " + Environment.NewLine + fileName + " finalizada.", Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private void buttonX25_Click(object sender, EventArgs e)
        {
            filtrar();
        }


        private void BUSCAR_BATCH_ID_KeyPress(object sender, KeyPressEventArgs e)
        {
            //Precionar Enter buscar
            if (e.KeyChar.ToString() == "\r")
            {
                filtrar();
            }
        }


        private void buttonX28_Click(object sender, EventArgs e)
        {
            exportar_excel_poliza();
        }

        private void exportar_excel_poliza()
        {
            var fileName = "Polizas " + DateTime.Now.ToString("yyyy-MM-dd-hh-mm-ss") + ".xlsx";
            if (folderBrowserDialog1.ShowDialog() == DialogResult.OK)
            {
                fileName = folderBrowserDialog1.SelectedPath + "\\" + fileName;
                System.IO.Stream myStream = new System.IO.FileStream(fileName, System.IO.FileMode.Create, System.IO.FileAccess.ReadWrite, System.IO.FileShare.None);

                using (ExcelPackage pck = new ExcelPackage())
                {
                    DataTable oDataTable = new DataTable();

                    string sSQL = " SELECT Poliza.ROW_ID_CATALOGO, Poliza.numField, Poliza.conceptoField AS Expr1, PolizasPolizaTransaccion.numCtaField, PolizasPolizaTransaccion.conceptoField, ";
                    sSQL+= " PolizasPolizaTransaccion.debeField, PolizasPolizaTransaccion.haberField, PolizasPolizaTransaccion.tipCambField,  ";
                    sSQL+= " PolizasPolizaTransaccion.tipCambFieldSpecified, PolizasPolizaTransaccion.ROW_ID_POLIZA, PolizasPolizaTransaccion.monedaField ";
                    sSQL += " FROM PolizasPolizaTransaccion INNER JOIN Poliza ON PolizasPolizaTransaccion.ROW_ID_POLIZA = Poliza.ROW_ID ";
                    sSQL += " WHERE Poliza.ROW_ID_CATALOGO=" + oObjeto.ROW_ID;
                    oDataTable = oData.EjecutarConsulta(sSQL);
                    ExcelWorksheet ws1 = pck.Workbook.Worksheets.Add("Exportar");
                    ws1.Cells["A1"].LoadFromDataTable(oDataTable, true);
                    pck.SaveAs(myStream);
                }
                myStream.Close();
                MessageBoxEx.Show("Exportaci�n del archivo " + Environment.NewLine + fileName + " finalizada.", Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Information);
            }

        }

        private void buttonItem7_Click(object sender, EventArgs e)
        {
            exportar_excel_cfdi();
        }

        private void exportar_excel_cfdi()
        {
            var fileName = "Polizas con CFDI" + DateTime.Now.ToString("yyyy-MM-dd-hh-mm-ss") + ".xlsx";
            if (folderBrowserDialog1.ShowDialog() == DialogResult.OK)
            {
                fileName = folderBrowserDialog1.SelectedPath + "\\" + fileName;
                System.IO.Stream myStream = new System.IO.FileStream(fileName, System.IO.FileMode.Create, System.IO.FileAccess.ReadWrite, System.IO.FileShare.None);

                using (ExcelPackage pck = new ExcelPackage())
                {
                    DataTable oDataTable = new DataTable();

                    string sSQL = " SELECT Poliza.numField, Poliza.conceptoField AS Expr1, PolizasPolizaTransaccion.numCtaField, PolizasPolizaTransaccion.conceptoField, ";
                    sSQL += " PolizasPolizaTransaccion.debeField, PolizasPolizaTransaccion.haberField, PolizasPolizaTransaccion.tipCambField,  ";
                    sSQL += " PolizasPolizaTransaccion.tipCambFieldSpecified, PolizasPolizaTransaccion.monedaField, PolizasPolizaTransaccionCompNal.uUID_CFDIField,  ";
                    sSQL += " PolizasPolizaTransaccionCompNal.rFCField, PolizasPolizaTransaccionCompNal.RFC_RECEPTOR, PolizasPolizaTransaccionCompNal.montoTotalField  ";
                    sSQL += " FROM PolizasPolizaTransaccion INNER JOIN Poliza ON PolizasPolizaTransaccion.ROW_ID_POLIZA = Poliza.ROW_ID INNER JOIN PolizasPolizaTransaccionCompNal ON PolizasPolizaTransaccion.ROW_ID = PolizasPolizaTransaccionCompNal.ROW_ID_TRANSACCION ";
                    sSQL += " WHERE Poliza.ROW_ID_CATALOGO=" + oObjeto.ROW_ID;
                    oDataTable = oData.EjecutarConsulta(sSQL);
                    ExcelWorksheet ws1 = pck.Workbook.Worksheets.Add("Exportar");
                    ws1.Cells["A1"].LoadFromDataTable(oDataTable, true);
                    pck.SaveAs(myStream);
                }
                myStream.Close();
                MessageBoxEx.Show("Exportaci�n del archivo " + Environment.NewLine + fileName + " finalizada.", Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private void buttonX22_Click_1(object sender, EventArgs e)
        {
            frmXML ofrmXML = new frmXML(oObjeto.ROW_ID, "Polizas", oObjeto.rFCField);
            ofrmXML.ShowDialog();
            cargar_tipos();
        }

        private void buttonX4_Click(object sender, EventArgs e)
        {
            frmXML ofrmXML = new frmXML(oObjeto.ROW_ID, "AuxiliarCtas", oObjeto.rFCField);
            ofrmXML.ShowDialog();
            cargar_tipos();
        }

        private void buttonX6_Click(object sender, EventArgs e)
        {
            cargar_tipos();
        }

        private void buttonX5_Click(object sender, EventArgs e)
        {
            frmXML ofrmXML = new frmXML(oObjeto.ROW_ID, "AuxiliarFolios", oObjeto.rFCField);
            ofrmXML.ShowDialog();
        }

        private void eliminar_cuenta()
        {
            dtgTipos.EndEdit();
            if (dtgTipos.SelectedCells.Count > 0)
            {
                DialogResult Resultado = MessageBoxEx.Show("�Desea eliminar las seleccionada?", Application.ProductName + "-" + Application.ProductVersion, MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                if (Resultado.ToString() != "Yes")
                {
                    return;
                }
                foreach (DataGridViewRow drv in dtgTipos.SelectedRows)
                {
                    string ROW_ID = drv.Cells["ROW_ID"].Value.ToString();
                    if (drv.Cells["TIPO_tr"].Value.ToString() == "Auxiliar Cuentas")
                    {
                        cAuxiliarCtas ocAuxiliarCtas = (cAuxiliarCtas)drv.Tag;
                        ocAuxiliarCtas.eliminar(ROW_ID);
                        dtgTipos.Rows.Remove(drv);
                    }
                    else
                    {

                        if (drv.Cells["TIPO_tr"].Value.ToString() == "Auxiliar P�lizas")
                        {
                            cAuxiliarFolios ocAuxiliarFolios = (cAuxiliarFolios)drv.Tag;
                            ocAuxiliarFolios.eliminar(ROW_ID);
                            dtgTipos.Rows.Remove(drv);
                        }
                        else
                        {

                            if (drv.Cells["TIPO_tr"].Value.ToString() == "P�lizas")
                            {
                                cPolizas ocPolizas = (cPolizas)drv.Tag;
                                ocPolizas.eliminar(ROW_ID);
                                dtgTipos.Rows.Remove(drv);
                            }
                    
                        }
                    }
                }
            }
            else
            {
                MessageBoxEx.Show("Seleccione alguna l�nea.", Application.ProductName + "-" + Application.ProductVersion, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }

        private void metroStatusBar1_ItemClick(object sender, EventArgs e)
        {

        }

        private void buttonX23_Click(object sender, EventArgs e)
        {
            validar_poliza();
        }

        private void buttonX7_Click(object sender, EventArgs e)
        {
            importar_comprobantes();
        }

        private void importar_comprobantes()
        {
            if (oObjeto.ROW_ID == "")
            {
                MessageBoxEx.Show("Guarde primero.", Application.ProductName + "-" + Application.ProductVersion, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return;
            }

            OpenFileDialog openFileDialog1 = new OpenFileDialog();
            openFileDialog1.Filter = "Excel files (*.xlsx)|*.xlsx|All files (*.*)|*.*";
            openFileDialog1.FilterIndex = 1;
            openFileDialog1.RestoreDirectory = true;

            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                try
                {
                    procesar(1, openFileDialog1.FileName);
                }
                catch (Exception)
                {
                    MessageBoxEx.Show("No existe el Archivo seleccionado", Application.ProductName + "-" + Application.ProductVersion, MessageBoxButtons.OK);
                }
            }
        }
        private void procesar(int hoja, string archivo)
        {
            try
            {
                int n;
                int rowIndex = 1;
                FileInfo existingFile = new FileInfo(archivo);
                using (ExcelPackage package = new ExcelPackage(existingFile))
                {
                    ExcelWorksheet worksheet = package.Workbook.Worksheets[hoja];
                    while (worksheet.Cells[rowIndex, 2].Value != null)
                    {
                        labelItem1.Text = "Registro " + rowIndex.ToString();
                        //Agregar las lineas de la cabecera
                        if (rowIndex != 1)
                        {
                            string INVOICE_ID_t = worksheet.Cells[rowIndex, 1].Text;
                            string INVOICE_DATE_t = worksheet.Cells[rowIndex, 2].Text;
                            string RFC_EMISOR_t = worksheet.Cells[rowIndex, 3].Text;
                            string UUID_t = worksheet.Cells[rowIndex, 4].Text;
                            string ENTE_t = worksheet.Cells[rowIndex, 5].Text;
                            string montoTotalField_t = oData.formato_decimal(worksheet.Cells[rowIndex, 6].Text,"n");

                            string sSQL = "INSERT INTO CFDI_PROVEEDORES(INVOICE_ID,INVOICE_DATE,RFC_EMISOR,UUID,VENDOR_ID,[montoTotalField]) VALUES (";
                            sSQL += "'" + INVOICE_ID_t + "','" + INVOICE_DATE_t + "','" + RFC_EMISOR_t + "','" + UUID_t + "','" + ENTE_t + "'," + montoTotalField_t + "";
                            sSQL += ")";
                            if (oData.EjecutarConsulta(sSQL) != null)
                            {
                                //Buscar las facturas para APC
                                cPolizasPolizaTransaccionCompNal ocPolizasPolizaTransaccionCompNal=new cPolizasPolizaTransaccionCompNal();
                                ocPolizasPolizaTransaccionCompNal.vincular(INVOICE_ID_t, RFC_EMISOR_t, UUID_t, decimal.Parse(montoTotalField_t),"APC");

                                cEMPRESA_CONEXION ocEMPRESA_CONEXION_tr = new cEMPRESA_CONEXION();
                                foreach (cEMPRESA_CONEXION registro_tr in ocEMPRESA_CONEXION_tr.todos(ocEMPRESA.ROW_ID))
                                {
                                    labelItem1.Text = "Migrando de CFDI de Infor Visual " + registro_tr.VISUAL_ENTITY_ID;
                                    cEMPRESA_CONEXION ocEMPRESA_CONEXION = registro_tr;
                                    cCONEXCION oData_ERPp = registro_tr.conectar(registro_tr.ROW_ID);
                                    //Buscar las facturas para ARC
                                    sSQL = " SELECT DISTINCT CASH_RECEIPT_LINE.CUSTOMER_ID, CASH_RECEIPT_LINE.INVOICE_ID,CASH_RECEIPT_LINE.CHECK_ID ";
                                    sSQL += " FROM CASH_RECEIPT_LINE INNER JOIN CASH_RECEIPT_DIST ON CASH_RECEIPT_LINE.CUSTOMER_ID = CASH_RECEIPT_DIST.CUSTOMER_ID AND CASH_RECEIPT_LINE.CHECK_ID = CASH_RECEIPT_DIST.CHECK_ID ";
                                    sSQL += " WHERE CASH_RECEIPT_LINE.CUSTOMER_ID='" + ENTE_t + "' AND CASH_RECEIPT_LINE.INVOICE_ID='" + INVOICE_ID_t + "'";
                                    DataTable oDataTable2 = oData_ERPp.EjecutarConsulta(sSQL);
                                    foreach (DataRow oDataRow2 in oDataTable2.Rows)
                                    {
                                        ocPolizasPolizaTransaccionCompNal.vincular(oDataRow2["CHECK_ID"].ToString(), RFC_EMISOR_t, UUID_t
                                            , decimal.Parse(montoTotalField_t),"ARC");
                                    }
                                    //Buscar todos los Pagos CONTROL_NO and BANK_ACCOUNT
                                    //Buscar por INVOICE_ID
                                    sSQL = " SELECT DISTINCT CAST(CASH_DISBURSEMENT.CONTROL_NO as varchar(10))+'|'+CASH_DISBURSEMENT.BANK_ACCOUNT_ID as DOCUMENTO, PAYABLE.INVOICE_ID,PAYABLE.VENDOR_ID ";
                                    sSQL += " FROM CASH_DISBURSE_LINE INNER JOIN CASH_DISBURSEMENT ON CASH_DISBURSE_LINE.BANK_ACCOUNT_ID = CASH_DISBURSEMENT.BANK_ACCOUNT_ID AND CASH_DISBURSE_LINE.CONTROL_NO = CASH_DISBURSEMENT.CONTROL_NO INNER JOIN PAYABLE ON CASH_DISBURSE_LINE.VOUCHER_ID = PAYABLE.VOUCHER_ID AND CASH_DISBURSEMENT.VENDOR_ID = PAYABLE.VENDOR_ID ";
                                    sSQL += " WHERE PAYABLE.INVOICE_ID='" + INVOICE_ID_t + "' AND PAYABLE.VENDOR_ID='" + ENTE_t + "'";
                                    DataTable oDataTable3 = oData_ERPp.EjecutarConsulta(sSQL);
                                    foreach (DataRow oDataRow3 in oDataTable3.Rows)
                                    {
                                        ocPolizasPolizaTransaccionCompNal.vincular(oDataRow3["DOCUMENTO"].ToString(), RFC_EMISOR_t, UUID_t
                                            , decimal.Parse(montoTotalField_t),"APC");
                                    }
                                    //Buscar por VOUCHER_ID
                                    sSQL = " SELECT DISTINCT CAST(CASH_DISBURSEMENT.CONTROL_NO as varchar(10))+'|'+CASH_DISBURSEMENT.BANK_ACCOUNT_ID as DOCUMENTO, PAYABLE.INVOICE_ID,PAYABLE.VENDOR_ID ";
                                    sSQL += " FROM CASH_DISBURSE_LINE INNER JOIN CASH_DISBURSEMENT ON CASH_DISBURSE_LINE.BANK_ACCOUNT_ID = CASH_DISBURSEMENT.BANK_ACCOUNT_ID AND CASH_DISBURSE_LINE.CONTROL_NO = CASH_DISBURSEMENT.CONTROL_NO INNER JOIN PAYABLE ON CASH_DISBURSE_LINE.VOUCHER_ID = PAYABLE.VOUCHER_ID AND CASH_DISBURSEMENT.VENDOR_ID = PAYABLE.VENDOR_ID ";
                                    sSQL += " WHERE PAYABLE.VOUCHER_ID='" + INVOICE_ID_t + "' AND PAYABLE.VENDOR_ID='" + ENTE_t + "'";
                                    DataTable oDataTable4 = oData_ERPp.EjecutarConsulta(sSQL);
                                    foreach (DataRow oDataRow4 in oDataTable4.Rows)
                                    {
                                        ocPolizasPolizaTransaccionCompNal.vincular(oDataRow4["DOCUMENTO"].ToString(), RFC_EMISOR_t, UUID_t
                                            , decimal.Parse(montoTotalField_t), "APC");
                                    }
                                }

                            }
                        }
                        rowIndex++;

                    }
                    MessageBoxEx.Show("Archivo de CFDI Importado", Application.ProductName);
                }
            }
            catch (Exception e)
            {
                MessageBoxEx.Show(e.Message.ToString());
            }

        }


        private void buttonX15_Click(object sender, EventArgs e)
        {
            cPoliza ocPoliza = new cPoliza();
            ocPoliza.importar_poliza();
        }

        private void buttonItem3_Click(object sender, EventArgs e)
        {

        }

        private void exportar_excel()
        {

            MessageBoxEx.Show("La configuraci�n de  la Plantilla es la siguiente:" + Environment.NewLine
            + "Fila 1 - Encabezado" + Environment.NewLine
            + "Columna 1: INVOICE_ID " + Environment.NewLine
            + "Columna 2: INVOICE_DATE " + Environment.NewLine
            + "Columna 3: RFC_EMISOR " + Environment.NewLine
            + "Columna 4: UUID " + Environment.NewLine
            + "Columna 5: VENDOR_ID/CUSTOMER_ID " + Environment.NewLine
            + "Columna 6: montoTotalField " + Environment.NewLine
            + "Si no tiene esta configuraci�n no ser� cargada la informaci�n. "
            + "Ser� generada una plantilla de uso. "
            , Application.ProductName + "-" + Application.ProductVersion.ToString()
            , MessageBoxButtons.OK
            );

            var fileName = " Plantilla de Cuentas " + DateTime.Now.ToString("yyyy-MM-dd-hh-mm-ss") + ".xlsx";
            if (folderBrowserDialog1.ShowDialog() == DialogResult.OK)
            {
                fileName = folderBrowserDialog1.SelectedPath + "\\" + fileName;
                System.IO.Stream myStream = new System.IO.FileStream(fileName, System.IO.FileMode.Create, System.IO.FileAccess.ReadWrite, System.IO.FileShare.None);

                using (ExcelPackage pck = new ExcelPackage())
                {

                    ExcelWorksheet ws1 = pck.Workbook.Worksheets.Add("Plantilla");

                    ws1.Cells[1, 1].Value = "INVOICE_ID";
                    ws1.Cells[1, 2].Value = "INVOICE_DATE";
                    ws1.Cells[1, 3].Value = "RFC_EMISOR";
                    ws1.Cells[1, 4].Value = "UUID";
                    ws1.Cells[1, 5].Value = "VENDOR_ID";
                    ws1.Cells[1, 6].Value = "montoTotalField";
                   
                    pck.SaveAs(myStream);
                }
                myStream.Close();
                MessageBoxEx.Show("Creaci�n de plantilla realizada " + Environment.NewLine + fileName + " finalizada.", Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private void buttonItem1_Click(object sender, EventArgs e)
        {
            exportar_excel();
        }

        private void buttonX8_Click(object sender, EventArgs e)
        {
            eliminar_cuenta();
        }

        private void buttonX9_Click(object sender, EventArgs e)
        {
            exportar_validacion();
        }

        private void exportar_validacion(bool mostrar_diferencias=true)
        {
            string sSQL="";
            try
            {

                var fileName = " Validaci�n " + DateTime.Now.ToString("yyyy-MM-dd-hh-mm-ss") + ".xlsx";
                if (folderBrowserDialog1.ShowDialog() == DialogResult.OK)
                {
                    fileName = folderBrowserDialog1.SelectedPath + "\\" + fileName;
                    System.IO.Stream myStream = new System.IO.FileStream(fileName, System.IO.FileMode.Create, System.IO.FileAccess.ReadWrite, System.IO.FileShare.None);

                    using (ExcelPackage pck = new ExcelPackage())
                    {
                        sSQL = " SELECT * ";
                        sSQL += " ,(debe_Balanza-haber_Balanza) as 'Movimientos Balanza'";
                        sSQL += " ,(debe_Transacciones-haber_Transacciones) as 'Movimientos Transacciones' ";
                        sSQL += " ,(debe_Balanza-haber_Balanza)-(debe_Transacciones-haber_Transacciones) as 'Diferencia' ";
                        sSQL += " FROM [dbo].[ENT_CE_POLIZA_BALANZA] ";
                        sSQL += " WHERE 1=1 ";
                        sSQL += " AND ROW_ID_CATALOGO=" + oObjeto.ROW_ID;

                        if (mostrar_diferencias)
                        {
                            sSQL += " AND floor((debe_Balanza-haber_Balanza)-(debe_Transacciones-haber_Transacciones))<>0";
                        }
                        sSQL += " ORDER BY numCtaField ";

                        DataTable oDataTable2 = oData.EjecutarConsulta(sSQL);
                        //string sSQL_IN = "";
                        if (oDataTable2 != null)
                        {
                            ExcelWorksheet ws1 = pck.Workbook.Worksheets.Add("General");
                            ws1.Cells["A1"].LoadFromDataTable(oDataTable2, true);

                            //foreach (DataRow oDataRow in oDataTable2.Rows)
                            //{

                            //    if(sSQL_IN=="")
                            //    {
                            //        sSQL_IN += "'" + oDataRow["numCtaField"].ToString() + "'";
                            //    }
                            //    else
                            //    {
                            //        sSQL_IN += ",'" + oDataRow["numCtaField"].ToString() + "'";
                            //    }

                            //}
                        }

                        //if (sSQL_IN != "")
                        //{
                        //    sSQL = " SELECT  Poliza.numField, Poliza.fechaField, PolizasPolizaTransaccion.numCtaField, PolizasPolizaTransaccion.conceptoField, PolizasPolizaTransaccion.debeField, PolizasPolizaTransaccion.haberField";
                        //    sSQL += " FROM Poliza INNER JOIN PolizasPolizaTransaccion ON Poliza.ROW_ID = PolizasPolizaTransaccion.ROW_ID_POLIZA";
                        //    sSQL += " WHERE ROW_ID_CATALOGO=" + oObjeto.ROW_ID + " AND PolizasPolizaTransaccion.numCtaField IN (" + sSQL_IN + ")";
                        //    sSQL += " ORDER BY PolizasPolizaTransaccion.numCtaField ";
                        //    DataTable oDataTable3 = oData.EjecutarConsulta(sSQL);
                        //    if (oDataTable3 != null)
                        //    {
                        //        ExcelWorksheet ws1 = pck.Workbook.Worksheets.Add("Detalle");
                        //        ws1.Cells["A1"].LoadFromDataTable(oDataTable3, true);
                        //    }
                        //}
                        pck.SaveAs(myStream);

                    }
                    myStream.Close();
                    MessageBoxEx.Show("Exportaci�n del archivo " + Environment.NewLine + fileName + " finalizada.", Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
            catch(Exception e)
            {
                MessageBoxEx.Show("Execepci�n: " + e.Message.ToString() + Environment.NewLine, Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }


        }

        private void buttonItem2_Click(object sender, EventArgs e)
        {
            exportar_validacion(false);
        }

        private void buttonX11_Click(object sender, EventArgs e)
        {
            cargar_cfdi(true,false);
        }

        private void cargar_cfdi(bool buscar=true, bool importar=false)
        {
            
            if (!backgroundWorker1.IsBusy)
            {
                frmComprobante_Filtro ofrmComprobante_Filtro = new frmComprobante_Filtro();
                DialogResult resultado = ofrmComprobante_Filtro.ShowDialog();
                if (resultado == DialogResult.OK)
                {
                    circularProgress1.Visible = true;
                    circularProgress1.IsRunning = true;
                    cambiar_boton();

                    string sSQL_BATCH = "";
                    if (ofrmComprobante_Filtro.BATCH_INICIALp != "")
                    {
                        sSQL_BATCH += " AND Poliza.[numField]>='" + ofrmComprobante_Filtro.BATCH_INICIALp + "'";
                    }
                    if (ofrmComprobante_Filtro.BATCH_FINALp != "")
                    {
                        sSQL_BATCH += " AND Poliza.[numField]<='" + ofrmComprobante_Filtro.BATCH_FINALp + "'";

                    }


                    string sSQL_WHERE_tipo = "";
                    if (ofrmComprobante_Filtro.APCp != "")
                    {
                        if (sSQL_WHERE_tipo == "")
                        {
                            sSQL_WHERE_tipo = "('" + ofrmComprobante_Filtro.APCp + "' ";
                        }
                        else
                        {
                            sSQL_WHERE_tipo += ",'" + ofrmComprobante_Filtro.APCp + "' ";
                        }
                    }
                    if (ofrmComprobante_Filtro.APIp != "")
                    {
                        if (sSQL_WHERE_tipo == "")
                        {
                            sSQL_WHERE_tipo = "('" + ofrmComprobante_Filtro.APIp + "' ";
                        }
                        else
                        {
                            sSQL_WHERE_tipo += ",'" + ofrmComprobante_Filtro.APIp + "' ";
                        }
                    }

                    if (ofrmComprobante_Filtro.ARIp != "")
                    {
                        if (sSQL_WHERE_tipo == "")
                        {
                            sSQL_WHERE_tipo = "('" + ofrmComprobante_Filtro.ARIp + "' ";
                        }
                        else
                        {
                            sSQL_WHERE_tipo += ",'" + ofrmComprobante_Filtro.ARIp + "' ";
                        }
                    }

                    if (ofrmComprobante_Filtro.ARCp != "")
                    {
                        if (sSQL_WHERE_tipo == "")
                        {
                            sSQL_WHERE_tipo = "('" + ofrmComprobante_Filtro.ARCp + "' ";
                        }
                        else
                        {
                            sSQL_WHERE_tipo += ",'" + ofrmComprobante_Filtro.ARCp + "' ";
                        }
                    }

                    if (sSQL_WHERE_tipo != "")
                    {
                        sSQL_WHERE_tipo += ")";
                    }

                    dtgComprobantes.Rows.Clear();
                    List<object> arguments = new List<object>();
                    arguments.Add(buscar);
                    arguments.Add(importar);
                    arguments.Add(sSQL_WHERE_tipo);
                    arguments.Add(sSQL_BATCH);


                    backgroundWorker1.RunWorkerAsync(arguments);

                }
            }
        }

        private void cambiar_boton()
        {
            if (buttonX10.Text == "Importar: Comprobantes")
            {
                buttonX10.Image = global::Desarrollo.Properties.Resources.stop_16;
                buttonX10.Text = "Detener";
            }
            else
            {
                buttonX10.Image = global::Desarrollo.Properties.Resources.visual;

                buttonX10.Text = "Importar: Comprobantes";
            }        
        }

        private void buscar_cfdi_proveedor(string ROW_ID_TRANSACCION, string tipo_tr, string TIPO, string DOCUMENTO, string numCtaFieldp, string ENTE)
        {
            cPolizasPolizaTransaccionCompNal ocComprobante;
            cEMPRESA_CONEXION ocEMPRESA_CONEXION_tr = new cEMPRESA_CONEXION();
            string RFC_EMISORp = "";
            foreach (cEMPRESA_CONEXION registro_tr in ocEMPRESA_CONEXION_tr.todos(ocEMPRESA.ROW_ID))
            {
                labelItem1.Text = "Migrando de CFDI de Infor Visual " + registro_tr.VISUAL_ENTITY_ID;
                cEMPRESA_CONEXION ocEMPRESA_CONEXION = registro_tr;
                cCONEXCION oData_ERPp = registro_tr.conectar(registro_tr.ROW_ID);
                //Cargar el RFC del Proveedor
                string sSQL_VAT_REGISTRATION = "SELECT TOP 1 VAT_REGISTRATION FROM VENDOR WHERE ID='" + ENTE  + "'";
                DataTable oDataTable_vendor = oData_ERPp.EjecutarConsulta(sSQL_VAT_REGISTRATION);
                foreach (DataRow oDataRow_vendor in oDataTable_vendor.Rows)
                {
                    RFC_EMISORp = oDataRow_vendor["VAT_REGISTRATION"].ToString();
                }

                switch (TIPO)
                {
                    case "API":
                        //Buscar la factura en VMX_FE
                        ocComprobante = new cPolizasPolizaTransaccionCompNal();
                        ocComprobante.TIPO = TIPO;                        
                        //Si es VOUCHER_ID selecionar el INVOICE_ID
                        string sSQL1 = "SELECT INVOICE_ID FROM PAYABLE WHERE VOUCHER_ID='" + DOCUMENTO  + "'";
                        DataTable oDataTable = oData_ERPp.EjecutarConsulta(sSQL1);
                        foreach (DataRow oDataRow1 in oDataTable.Rows)
                        {
                            DOCUMENTO = oDataRow1["INVOICE_ID"].ToString();
                        }
                        ocComprobante.LLAVE = DOCUMENTO;
                        //Verificar en PAYABLE_BINARY
                        if (ocComprobante.generar_PAYABLE_BINARY(ROW_ID_TRANSACCION, DOCUMENTO, ENTE, ocEMPRESA_CONEXION))
                        {
                            int n = dtgComprobantes.Rows.Add();
                            dtgComprobantes.Rows[n].Cells["TIPO_CFDI"].Value = tipo_tr;
                            dtgComprobantes.Rows[n].Cells["INVOICE_ID"].Value = DOCUMENTO;
                            dtgComprobantes.Rows[n].Cells["UUID"].Value = ocComprobante.uUID_CFDIField;
                            dtgComprobantes.Rows[n].Cells["RFC"].Value = ocComprobante.rFCField;
                            dtgComprobantes.Rows[n].Cells["monto"].Value = ocComprobante.montoTotalField;
                            dtgComprobantes.Rows[n].Cells["TRANSACCIONES"].Value = numCtaFieldp + " TR:" + ROW_ID_TRANSACCION;
                        }


                        if (ocComprobante.generar_CFDI_PROVEEDORES(ROW_ID_TRANSACCION, DOCUMENTO, RFC_EMISORp, ocEMPRESA_CONEXION))
                        {
                            int n = dtgComprobantes.Rows.Add();
                            dtgComprobantes.Rows[n].Cells["TIPO_CFDI"].Value = tipo_tr;
                            dtgComprobantes.Rows[n].Cells["INVOICE_ID"].Value = DOCUMENTO;
                            dtgComprobantes.Rows[n].Cells["UUID"].Value = ocComprobante.uUID_CFDIField;
                            dtgComprobantes.Rows[n].Cells["RFC"].Value = ocComprobante.rFCField;
                            dtgComprobantes.Rows[n].Cells["monto"].Value = ocComprobante.montoTotalField;
                            dtgComprobantes.Rows[n].Cells["TRANSACCIONES"].Value = numCtaFieldp + " TR:" + ROW_ID_TRANSACCION;
                        }
                        
                        break;
                    case "APC":
                        string sSQL = "";
                        sSQL = " SELECT PAYABLE.INVOICE_ID,PAYABLE.VENDOR_ID ";
                        sSQL += " FROM CASH_DISBURSE_LINE INNER JOIN CASH_DISBURSEMENT ON CASH_DISBURSE_LINE.BANK_ACCOUNT_ID = CASH_DISBURSEMENT.BANK_ACCOUNT_ID AND CASH_DISBURSE_LINE.CONTROL_NO = CASH_DISBURSEMENT.CONTROL_NO INNER JOIN PAYABLE ON CASH_DISBURSE_LINE.VOUCHER_ID = PAYABLE.VOUCHER_ID AND CASH_DISBURSEMENT.VENDOR_ID = PAYABLE.VENDOR_ID ";
                        sSQL += " WHERE CAST(CASH_DISBURSEMENT.CONTROL_NO as varchar(10))+'|'+CASH_DISBURSEMENT.BANK_ACCOUNT_ID='" + DOCUMENTO + "'";
                        DataTable oDataTable2 = oData_ERPp.EjecutarConsulta(sSQL);
                        foreach (DataRow oDataRow2 in oDataTable2.Rows)
                        {
                            ocComprobante = new cPolizasPolizaTransaccionCompNal();
                            ocComprobante.TIPO = TIPO;
                            ocComprobante.LLAVE = DOCUMENTO;
                            if (ocComprobante.generar_CFDI_PROVEEDORES(ROW_ID_TRANSACCION, oDataRow2["INVOICE_ID"].ToString(), RFC_EMISORp, ocEMPRESA_CONEXION))                         
                            {
                                int n = dtgComprobantes.Rows.Add();
                                dtgComprobantes.Rows[n].Cells["TIPO_CFDI"].Value = tipo_tr;
                                dtgComprobantes.Rows[n].Cells["INVOICE_ID"].Value = DOCUMENTO;
                                dtgComprobantes.Rows[n].Cells["UUID"].Value = ocComprobante.uUID_CFDIField;
                                dtgComprobantes.Rows[n].Cells["RFC"].Value = ocComprobante.rFCField;
                                dtgComprobantes.Rows[n].Cells["monto"].Value = ocComprobante.montoTotalField;
                                dtgComprobantes.Rows[n].Cells["TRANSACCIONES"].Value = numCtaFieldp + " TR:" + ROW_ID_TRANSACCION;
                            }
                        }
                        break;
                }

            }
        }

        private bool generar_PAYABLE_BINARY(string ROW_ID_TRANSACCION, string DOCUMENTO, string ENTE, cEMPRESA_CONEXION ocEMPRESA_CONEXION)
        {
            throw new NotImplementedException();
        }

        private void buscar_cfdi_cliente(string ROW_ID_TRANSACCION, string tipo_tr, string TIPO
            , string DOCUMENTO, string numCtaFieldp, string ENTE)
        {
            cPolizasPolizaTransaccionCompNal ocComprobante;
            cEMPRESA_CONEXION ocEMPRESA_CONEXION_tr = new cEMPRESA_CONEXION();
            foreach (cEMPRESA_CONEXION registro_tr in ocEMPRESA_CONEXION_tr.todos(ocEMPRESA.ROW_ID))
            {
                labelItem1.Text = "Migrando de CFDI de Infor Visual " + registro_tr.VISUAL_ENTITY_ID;
                cEMPRESA_CONEXION ocEMPRESA_CONEXION = registro_tr;
                cCONEXCION oData_ERPp = registro_tr.conectar(registro_tr.ROW_ID);
                switch (TIPO)
                {
                    case "ARI":
                        //Buscar la factura en VMX_FE
                        ocComprobante = new cPolizasPolizaTransaccionCompNal();
                        ocComprobante.TIPO = TIPO;
                        ocComprobante.LLAVE = DOCUMENTO;
                        if (ocComprobante.generar_CFDI(ROW_ID_TRANSACCION, DOCUMENTO, oData_ERPp, ocEMPRESA_CONEXION))
                        {
                            int n = dtgComprobantes.Rows.Add();
                            dtgComprobantes.Rows[n].Cells["TIPO_CFDI"].Value = tipo_tr;
                            dtgComprobantes.Rows[n].Cells["INVOICE_ID"].Value = DOCUMENTO;
                            dtgComprobantes.Rows[n].Cells["UUID"].Value = ocComprobante.uUID_CFDIField;
                            dtgComprobantes.Rows[n].Cells["RFC"].Value = ocComprobante.rFCField;
                            dtgComprobantes.Rows[n].Cells["monto"].Value = ocComprobante.montoTotalField;
                            dtgComprobantes.Rows[n].Cells["TRANSACCIONES"].Value = numCtaFieldp + " TR:" + ROW_ID_TRANSACCION;
                        }
                        
                        break;
                    case "ARC":
                        //Buscar la factura en CASH_RECEIPT_LINE y luego en VMX_FE
                        string sSQL = "";
                        sSQL =" SELECT CASH_RECEIPT_LINE.CUSTOMER_ID, CASH_RECEIPT_LINE.INVOICE_ID ";
                        sSQL += " FROM CASH_RECEIPT_LINE INNER JOIN CASH_RECEIPT_DIST ON CASH_RECEIPT_LINE.CUSTOMER_ID = CASH_RECEIPT_DIST.CUSTOMER_ID AND CASH_RECEIPT_LINE.CHECK_ID = CASH_RECEIPT_DIST.CHECK_ID ";
                        sSQL += " WHERE CASH_RECEIPT_LINE.CUSTOMER_ID='" + ENTE + "' AND CASH_RECEIPT_LINE.CHECK_ID='" + DOCUMENTO + "'";
                        DataTable oDataTable2 = oData_ERPp.EjecutarConsulta(sSQL);
                        foreach (DataRow oDataRow2 in oDataTable2.Rows)
                        {

                            ocComprobante = new cPolizasPolizaTransaccionCompNal();
                            ocComprobante.TIPO = TIPO;
                            ocComprobante.LLAVE = DOCUMENTO;
                            if (ocComprobante.generar_CFDI(ROW_ID_TRANSACCION, oDataRow2["INVOICE_ID"].ToString(), oData_ERPp
                                , ocEMPRESA_CONEXION))
                            {
                                int n = dtgComprobantes.Rows.Add();
                                dtgComprobantes.Rows[n].Cells["TIPO_CFDI"].Value = tipo_tr;
                                dtgComprobantes.Rows[n].Cells["INVOICE_ID"].Value = DOCUMENTO;
                                dtgComprobantes.Rows[n].Cells["UUID"].Value = ocComprobante.uUID_CFDIField;
                                dtgComprobantes.Rows[n].Cells["RFC"].Value = ocComprobante.rFCField;
                                dtgComprobantes.Rows[n].Cells["TRANSACCIONES"].Value = numCtaFieldp + " TR:" + ROW_ID_TRANSACCION;                                
                                dtgComprobantes.Rows[n].Cells["monto"].Value = ocComprobante.montoTotalField;
                            }
                        }
                        break;
                }

            }
        }


        private void cargar_cfdi_clientes()
        {
            string sSQL;
            sSQL = " select * from information_schema.tables where table_name = 'VMX_FE'";
            DataTable oDataTable = oData.EjecutarConsulta(sSQL);
            foreach (DataRow oDataRow in oDataTable.Rows)
            {
                
            }
        }


        private void cargar_API()
        {
            //Cargar las API del Mes
            string sSQL = "";
            //sSQL = "SELECT * FROM PAYABLE WHERE YEAR(INVOICE_DATE)="+;

        }


        private void cargar_cfdi_proveedores()
        {
            string sSQL;
            sSQL = " select * from information_schema.tables where table_name = 'CFDI_PROVEEDORES'";
            DataTable oDataTable = oData.EjecutarConsulta(sSQL);
            foreach (DataRow oDataRow in oDataTable.Rows)
            {
                sSQL = " SELECT * ";
                sSQL += " FROM CFDI_PROVEEDORES ";
                sSQL += " WHERE 1=1 AND YEAR(INVOICE_DATE)='" + oObjeto.anoField + "' AND MONTH(INVOICE_DATE)='" + oObjeto.mesField + "'";
                

                oDataTable = oData.EjecutarConsulta(sSQL);
                if (oDataTable.Rows.Count != 0)
                {
                    foreach (DataRow oDataRow2 in oDataTable.Rows)
                    {
                        int n = dtgComprobantes.Rows.Add();
                        dtgComprobantes.Rows[n].Cells["TIPO_CFDI"].Value = "Proveedores";
                        dtgComprobantes.Rows[n].Cells["TRANSACCIONES"].Value = "";
                        dtgComprobantes.Rows[n].Cells["UUID"].Value = oDataRow2["UUID"].ToString();
                        dtgComprobantes.Rows[n].Cells["INVOICE_ID"].Value = oDataRow2["INVOICE_ID"].ToString();
                        dtgComprobantes.Rows[n].Cells["RFC_EMISOR"].Value = oDataRow2["RFC_EMISOR"].ToString();
                        
                        dtgComprobantes.Rows[n].Cells["INVOICE_DATE"].Value = oDataRow2["INVOICE_DATE"].ToString();
                        dtgComprobantes.Rows[n].Cells["monto"].Value = oDataRow2["montoTotalField"].ToString();

                    }
                }
            } 
        }

        private void buttonX13_Click(object sender, EventArgs e)
        {
            frmComprobantes ofrmComprobantes = new frmComprobantes(oObjeto.ROW_ID);
            ofrmComprobantes.ShowDialog();
        }

        private void buttonItem1_Click_1(object sender, EventArgs e)
        {
            exportar_excel();
        }

        private void buttonItem5_Click(object sender, EventArgs e)
        {
            importar_comprobantes();
        }

        private void buttonX10_Click(object sender, EventArgs e)
        {
            if (buttonX10.Text == "Detener")
            {
                if (backgroundWorker1.IsBusy)
                {
                    backgroundWorker1.CancelAsync();
                    cambiar_boton();

                }
                return;
            }
            cargar_cfdi(false,true);
        }

        private void buttonX7_Click_1(object sender, EventArgs e)
        {
            filtrar_uuid();
        }

        private void filtrar_uuid()
        {

            foreach (DataGridViewRow row in dtgComprobantes.Rows)
            {
                if (row.Cells["UUID"].Value.ToString().Contains(BUSCAR_UUID.Text))
                {
                    row.Visible = true;
                }
                else
                {
                    row.Visible = false;
                }

            }

        }

        private void BUSCAR_UUID_KeyPress(object sender, KeyPressEventArgs e)
        {
            //Precionar Enter buscar
            if (e.KeyChar.ToString() == "\r")
            {
                filtrar_uuid();
            }
        }

        private void backgroundWorker1_DoWork(object sender, DoWorkEventArgs e)
        {
            List<object> genericlist = e.Argument as List<object>;
            bool buscar=bool.Parse(genericlist[0].ToString());
            bool importar = bool.Parse(genericlist[1].ToString());
            string sSQL_WHERE_TIPO = genericlist[2].ToString();
            string sSQL_WHERE_BATCH = genericlist[3].ToString();
            
            //Cargar la busqueda o informe de transacciones;
            //Buscar todas las cuentas de Bancos 
            string sSQL = "";
            //Buscar todas las cuentas de Clientes
            string sSQL_in = "";
            cEMPRESA_CONEXION ocEMPRESA_CONEXION_tr = new cEMPRESA_CONEXION();
            foreach (cEMPRESA_CONEXION registro_tr in ocEMPRESA_CONEXION_tr.todos(ocEMPRESA.ROW_ID))
            {
                labelItem1.Text = "Migrando de CFDI de Infor Visual " + registro_tr.VISUAL_ENTITY_ID;
                cEMPRESA_CONEXION ocEMPRESA_CONEXION = registro_tr;
                cCONEXCION oData_ERPp = registro_tr.conectar(registro_tr.ROW_ID);

                sSQL = "SELECT DISTINCT RECV_GL_ACCT_ID FROM RECEIVABLE WHERE STATUS<>'X'";
                DataTable oDataTable = oData_ERPp.EjecutarConsulta(sSQL);
                foreach (DataRow oDataRow in oDataTable.Rows)
                {
                    if (sSQL_in == "")
                    {
                        sSQL_in = "'" + oDataRow["RECV_GL_ACCT_ID"].ToString() + "'";
                    }
                    else
                    {
                        sSQL_in += ",'" + oDataRow["RECV_GL_ACCT_ID"].ToString() + "'";
                    }
                }

                //Buscar todas las cuentas de Proveedores
                sSQL = "SELECT DISTINCT PAYB_GL_ACCT_ID FROM PAYABLE WHERE PAY_STATUS<>'X'";
                oDataTable = oData_ERPp.EjecutarConsulta(sSQL);
                foreach (DataRow oDataRow in oDataTable.Rows)
                {
                    if (sSQL_in == "")
                    {
                        sSQL_in = "'" + oDataRow["PAYB_GL_ACCT_ID"].ToString() + "'";
                    }
                    else
                    {
                        sSQL_in += ",'" + oDataRow["PAYB_GL_ACCT_ID"].ToString() + "'";
                    }
                }

                //Buscar todas las cuentas de Bancos
                sSQL = "SELECT DISTINCT GL_ACCOUNT_ID FROM BANK_ACCOUNT";
                oDataTable = oData_ERPp.EjecutarConsulta(sSQL);
                foreach (DataRow oDataRow in oDataTable.Rows)
                {
                    if (sSQL_in == "")
                    {
                        sSQL_in = "'" + oDataRow["GL_ACCOUNT_ID"].ToString() + "'";
                    }
                    else
                    {
                        sSQL_in += ",'" + oDataRow["GL_ACCOUNT_ID"].ToString() + "'";
                    }

                }
            }
            string sSQL_condicion = "";//" (TIPO='API' OR TIPO='ARI' OR TIPO='APC' OR TIPO='ARC') ";
            //if (SOLO_CLIENTES.Checked)
            //{
            //    sSQL_condicion += " PolizasPolizaTransaccion.TIPO='ARI' OR PolizasPolizaTransaccion.TIPO='ARC' ";
            //}
            //if (SOLO_PROVEEDORES.Checked)
            //{
            //    if (sSQL_condicion != "")
            //    {
            //        sSQL_condicion += " OR ";
            //    }
            //    sSQL_condicion += " PolizasPolizaTransaccion.TIPO='API' OR PolizasPolizaTransaccion.TIPO='APC' ";
            //}
            sSQL_condicion += " PolizasPolizaTransaccion.TIPO IN " + sSQL_WHERE_TIPO;

            if (sSQL_in != "")
            {
                sSQL_condicion += " AND numCtaField IN (" + sSQL_in + ")";
            }
            sSQL = "";
            sSQL = " SELECT PolizasPolizaTransaccion.numCtaField,PolizasPolizaTransaccion.TIPO, PolizasPolizaTransaccion.ROW_ID, Poliza.ROW_ID_CATALOGO,PolizasPolizaTransaccion.DOCUMENTO,PolizasPolizaTransaccion.ENTE ";
            sSQL += ",Poliza.numField ";
            sSQL += " FROM PolizasPolizaTransaccion INNER JOIN Poliza ON PolizasPolizaTransaccion.ROW_ID_POLIZA = Poliza.ROW_ID ";
            sSQL += " WHERE 1=1 ";
            sSQL += " AND  Poliza.ROW_ID_CATALOGO=" + oObjeto.ROW_ID;
            if (sSQL_condicion != "")
            {
                sSQL += " AND " + sSQL_condicion;
            }
            if(sSQL_WHERE_BATCH!="")
            {
                sSQL += sSQL_WHERE_BATCH;
            }

            //Cambiar la vista del SQL
            if(buscar)
            {
                sSQL = " SELECT PolizasPolizaTransaccion.numCtaField,PolizasPolizaTransaccion.TIPO, PolizasPolizaTransaccion.ROW_ID, Poliza.ROW_ID_CATALOGO,PolizasPolizaTransaccion.DOCUMENTO,PolizasPolizaTransaccion.ENTE ";
                sSQL += ",Poliza.numField,PolizasPolizaTransaccionCompNal.uUID_CFDIField,PolizasPolizaTransaccionCompNal.rFCField,PolizasPolizaTransaccionCompNal.montoTotalField ";                
                sSQL += " FROM PolizasPolizaTransaccion INNER JOIN Poliza ON PolizasPolizaTransaccion.ROW_ID_POLIZA = Poliza.ROW_ID INNER JOIN PolizasPolizaTransaccionCompNal ON PolizasPolizaTransaccion.ROW_ID = PolizasPolizaTransaccionCompNal.ROW_ID_TRANSACCION ";
                sSQL += " WHERE 1=1 ";
                sSQL += " AND  Poliza.ROW_ID_CATALOGO=" + oObjeto.ROW_ID;
                if (sSQL_condicion != "")
                {
                    sSQL += " AND " + sSQL_condicion;
                }

            }


            //foreach (cTransaccion registro in ocTransaccion.todos("", "", "", sSQL_condicion))
            int n = 0;
            DataTable oDataTable2 = oData.EjecutarConsulta(sSQL);
            foreach (DataRow oDataRow in oDataTable2.Rows)
            {
                List<object> arguments = new List<object>();
                arguments.Add(buscar);
                arguments.Add(importar);
                arguments.Add(oDataRow);
                n++;
                backgroundWorker1.ReportProgress(n, arguments);
                if (backgroundWorker1.CancellationPending)
                {
                    e.Cancel = true;
                    backgroundWorker1.ReportProgress(0);
                    return;
                }
            }


        }

        private void backgroundWorker1_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            if (e.Cancelled)
            {
                labelItem1.Text = "Cancelado por el usuario...";
                circularProgress1.Value = 0;
                cambiar_boton();
            }
            // Check to see if an error occurred in the background process.
            else if (e.Error != null)
            {
                labelItem1.Text = e.Error.Message;
            }
            else
            {
                //BackGround Task Completed with out Error
                labelItem1.Text = " Finalizado...";
            }
            cambiar_boton();

            circularProgress1.Visible = false;
        }

        private void backgroundWorker1_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {   
            try
            {
                List<object> parametros = (List<object>)e.UserState;
                bool buscar = bool.Parse(parametros[0].ToString());
                bool importar = bool.Parse(parametros[1].ToString());
                DataRow oDataRow = (DataRow)parametros[2];

                string tipo_tr = "";
                switch (oDataRow["TIPO"].ToString())
                {
                    case "API":
                    case "APC":
                        tipo_tr = "Proveedores";
                        break;
                    case "ARI":
                    case "ARC":

                        tipo_tr = "Cliente";
                        break;
                }

                //Buscar por empresa todos los CFDI
                if (buscar)
                {
                    //cPolizasPolizaTransaccionCompNal ocComprobantes = new cPolizasPolizaTransaccionCompNal();
                    //foreach (cPolizasPolizaTransaccionCompNal comprobante in ocComprobantes.todos(oDataRow["ROW_ID"].ToString()))
                    //{
                        int n = dtgComprobantes.Rows.Add();
                        dtgComprobantes.Rows[n].Cells["TIPO_CFDI"].Value = tipo_tr;
                        dtgComprobantes.Rows[n].Cells["INVOICE_ID"].Value = oDataRow["DOCUMENTO"].ToString();
                        dtgComprobantes.Rows[n].Cells["UUID"].Value = oDataRow["uUID_CFDIField"].ToString();
                        dtgComprobantes.Rows[n].Cells["RFC"].Value = oDataRow["rFCField"].ToString();
                        dtgComprobantes.Rows[n].Cells["monto"].Value = oDataRow["montoTotalField"].ToString();
                        dtgComprobantes.Rows[n].Cells["TRANSACCIONES"].Value = oDataRow["numField"].ToString() + " TR:" + oDataRow["ROW_ID"].ToString();
                        dtgComprobantes.Rows[n].Cells["numFieldc"].Value = oDataRow["numField"].ToString();
                        dtgComprobantes.Rows[n].Cells["INVOICE_DATE"].Value = oDataRow["numField"].ToString();
                    //}
                }

                if (importar)
                {
                    //Buscar los documentos de clientes y agregarlos
                    if (tipo_tr == "Cliente")
                    {
                        buscar_cfdi_cliente(oDataRow["ROW_ID"].ToString(), tipo_tr
                            , oDataRow["TIPO"].ToString(), oDataRow["DOCUMENTO"].ToString(), oDataRow["numCtaField"].ToString(), oDataRow["ENTE"].ToString());
                    }
                    if (tipo_tr == "Proveedores")
                    {
                        buscar_cfdi_proveedor(oDataRow["ROW_ID"].ToString(), tipo_tr
                            , oDataRow["TIPO"].ToString(), oDataRow["DOCUMENTO"].ToString(), oDataRow["numCtaField"].ToString()
                            , oDataRow["ENTE"].ToString());
                    }
                }
            }
            catch (Exception excep)
            {
                MessageBoxEx.Show(excep.ToString());
                backgroundWorker1.CancelAsync();
                cambiar_boton();
                return;
            }
        }



        private void SOLO_CLIENTES_Click(object sender, EventArgs e)
        {

        }

        private void checkBoxX1_CheckedChanged(object sender, EventArgs e)
        {

        }

        private void buttonItem8_Click(object sender, EventArgs e)
        {

        }

        private void buttonX12_Click(object sender, EventArgs e)
        {
            cPoliza ocPoliza = new cPoliza();
            ocPoliza.comprobantes_API(oObjeto.ROW_ID, ocEMPRESA);
        }

        private void filtrarComprobantes_CheckedChanged(object sender, EventArgs e)
        {
            if (filtrarComprobantes.Checked)
            {
                filtrar_error();
            }
            else
            {

                foreach (DataGridViewRow row in dtgPoliza.Rows)
                {
                    row.Visible = true;
                }

            }
        }

        public void filtrar_error()
        {
            foreach (DataGridViewRow row in dtgPoliza.Rows)
            {
                if (row.ErrorText != "")
                {
                    row.Visible = true;
                }
                else
                {
                    row.Visible = false;
                }

            }

        }

        private void frmDeclaracionPoliza_FormClosing(object sender, FormClosingEventArgs e)
        {
            DialogResult Resultado = MessageBoxEx.Show("�Desea cerrar la Declaraci�n de P�lizas?", Application.ProductName + "-" + Application.ProductVersion, MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            if (Resultado.ToString() != "Yes")
            {
                e.Cancel = true;
            }
        }

    }
}