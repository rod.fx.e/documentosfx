namespace Desarrollo
{
    partial class frmBalanza_Comprobacion
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            DevComponents.DotNetBar.ButtonX buttonX3;
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmBalanza_Comprobacion));
            this.metroStatusBar1 = new DevComponents.DotNetBar.Metro.MetroStatusBar();
            this.labelItem1 = new DevComponents.DotNetBar.LabelItem();
            this.buttonX22 = new DevComponents.DotNetBar.ButtonX();
            this.TIPO_BALANZA = new DevComponents.DotNetBar.Controls.ComboBoxEx();
            this.BN = new DevComponents.Editors.ComboItem();
            this.BC = new DevComponents.Editors.ComboItem();
            this.labelX11 = new DevComponents.DotNetBar.LabelX();
            this.labelX10 = new DevComponents.DotNetBar.LabelX();
            this.dateTimePicker1 = new System.Windows.Forms.DateTimePicker();
            buttonX3 = new DevComponents.DotNetBar.ButtonX();
            this.SuspendLayout();
            // 
            // buttonX3
            // 
            buttonX3.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            buttonX3.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            buttonX3.Image = global::Desarrollo.Properties.Resources.Eliminar_18;
            buttonX3.Location = new System.Drawing.Point(98, 0);
            buttonX3.Name = "buttonX3";
            buttonX3.Size = new System.Drawing.Size(68, 24);
            buttonX3.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            buttonX3.TabIndex = 1;
            buttonX3.Text = "Cancelar";
            buttonX3.TextAlignment = DevComponents.DotNetBar.eButtonTextAlignment.Right;
            buttonX3.Click += new System.EventHandler(this.buttonX3_Click);
            // 
            // metroStatusBar1
            // 
            this.metroStatusBar1.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.metroStatusBar1.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.metroStatusBar1.ContainerControlProcessDialogKey = true;
            this.metroStatusBar1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.metroStatusBar1.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.metroStatusBar1.ForeColor = System.Drawing.Color.Black;
            this.metroStatusBar1.Items.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.labelItem1});
            this.metroStatusBar1.Location = new System.Drawing.Point(0, 92);
            this.metroStatusBar1.Name = "metroStatusBar1";
            this.metroStatusBar1.Size = new System.Drawing.Size(313, 21);
            this.metroStatusBar1.TabIndex = 8;
            // 
            // labelItem1
            // 
            this.labelItem1.Name = "labelItem1";
            // 
            // buttonX22
            // 
            this.buttonX22.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.buttonX22.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.buttonX22.Image = global::Desarrollo.Properties.Resources.sat_18;
            this.buttonX22.Location = new System.Drawing.Point(0, 0);
            this.buttonX22.Name = "buttonX22";
            this.buttonX22.Size = new System.Drawing.Size(92, 24);
            this.buttonX22.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.buttonX22.TabIndex = 587;
            this.buttonX22.Text = "Generar XML";
            this.buttonX22.Click += new System.EventHandler(this.buttonX22_Click);
            // 
            // TIPO_BALANZA
            // 
            this.TIPO_BALANZA.AutoCompleteCustomSource.AddRange(new string[] {
            "Abierto",
            "Cerrado"});
            this.TIPO_BALANZA.DisplayMember = "Text";
            this.TIPO_BALANZA.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.TIPO_BALANZA.ForeColor = System.Drawing.Color.Black;
            this.TIPO_BALANZA.FormattingEnabled = true;
            this.TIPO_BALANZA.ItemHeight = 16;
            this.TIPO_BALANZA.Items.AddRange(new object[] {
            this.BN,
            this.BC});
            this.TIPO_BALANZA.Location = new System.Drawing.Point(97, 30);
            this.TIPO_BALANZA.Name = "TIPO_BALANZA";
            this.TIPO_BALANZA.Size = new System.Drawing.Size(211, 22);
            this.TIPO_BALANZA.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.TIPO_BALANZA.TabIndex = 599;
            this.TIPO_BALANZA.SelectedIndexChanged += new System.EventHandler(this.TIPO_POLIZA_SelectedIndexChanged);
            // 
            // BN
            // 
            this.BN.Text = "Balanza de comprobación normal";
            this.BN.Value = "BN";
            // 
            // BC
            // 
            this.BC.Text = "Balanza de comprobación complementaria";
            this.BC.Value = "BC";
            // 
            // labelX11
            // 
            this.labelX11.AutoSize = true;
            this.labelX11.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.labelX11.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX11.ForeColor = System.Drawing.Color.Black;
            this.labelX11.Location = new System.Drawing.Point(12, 61);
            this.labelX11.Name = "labelX11";
            this.labelX11.Size = new System.Drawing.Size(189, 17);
            this.labelX11.TabIndex = 597;
            this.labelX11.Text = "Fecha de última modificación contable";
            // 
            // labelX10
            // 
            this.labelX10.AutoSize = true;
            this.labelX10.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.labelX10.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX10.ForeColor = System.Drawing.Color.Black;
            this.labelX10.Location = new System.Drawing.Point(12, 32);
            this.labelX10.Name = "labelX10";
            this.labelX10.Size = new System.Drawing.Size(79, 17);
            this.labelX10.TabIndex = 598;
            this.labelX10.Text = "Tipo de Balanza";
            // 
            // dateTimePicker1
            // 
            this.dateTimePicker1.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dateTimePicker1.Location = new System.Drawing.Point(207, 58);
            this.dateTimePicker1.Name = "dateTimePicker1";
            this.dateTimePicker1.Size = new System.Drawing.Size(101, 22);
            this.dateTimePicker1.TabIndex = 600;
            // 
            // frmBalanza_Comprobacion
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.BottomLeftCornerSize = 0;
            this.ClientSize = new System.Drawing.Size(313, 113);
            this.Controls.Add(this.dateTimePicker1);
            this.Controls.Add(this.TIPO_BALANZA);
            this.Controls.Add(this.labelX11);
            this.Controls.Add(this.labelX10);
            this.Controls.Add(this.buttonX22);
            this.Controls.Add(buttonX3);
            this.Controls.Add(this.metroStatusBar1);
            this.Cursor = System.Windows.Forms.Cursors.Default;
            this.DoubleBuffered = true;
            this.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "frmBalanza_Comprobacion";
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Información para el Balanza Comprobación";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevComponents.DotNetBar.Metro.MetroStatusBar metroStatusBar1;
        private DevComponents.DotNetBar.LabelItem labelItem1;
        private DevComponents.DotNetBar.ButtonX buttonX22;
        private DevComponents.DotNetBar.Controls.ComboBoxEx TIPO_BALANZA;
        private DevComponents.Editors.ComboItem BN;
        private DevComponents.Editors.ComboItem BC;
        private DevComponents.DotNetBar.LabelX labelX11;
        private DevComponents.DotNetBar.LabelX labelX10;
        private System.Windows.Forms.DateTimePicker dateTimePicker1;

    }
}