namespace Desarrollo
{
    partial class frmEmpresa_Conexion
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            DevComponents.DotNetBar.ButtonX buttonX4;
            DevComponents.DotNetBar.ButtonX buttonX3;
            DevComponents.DotNetBar.ButtonX buttonX1;
            DevComponents.DotNetBar.ButtonX buttonX2;
            DevComponents.DotNetBar.ButtonX buttonX5;
            DevComponents.DotNetBar.ButtonX buttonX6;
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmEmpresa_Conexion));
            this.metroStatusBar1 = new DevComponents.DotNetBar.Metro.MetroStatusBar();
            this.labelItem1 = new DevComponents.DotNetBar.LabelItem();
            this.toolStripStatusLabel1 = new DevComponents.DotNetBar.LabelItem();
            this.VISUAL_SITE_ID = new DevComponents.DotNetBar.Controls.ComboBoxEx();
            this.VISUAL_ENTITY_ID = new DevComponents.DotNetBar.Controls.ComboBoxEx();
            this.VISUAL_TIPO = new DevComponents.DotNetBar.Controls.ComboBoxEx();
            this.comboItem1 = new DevComponents.Editors.ComboItem();
            this.comboItem2 = new DevComponents.Editors.ComboItem();
            this.VISUAL_BD = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.labelX15 = new DevComponents.DotNetBar.LabelX();
            this.labelX14 = new DevComponents.DotNetBar.LabelX();
            this.labelX13 = new DevComponents.DotNetBar.LabelX();
            this.labelX12 = new DevComponents.DotNetBar.LabelX();
            this.labelX6 = new DevComponents.DotNetBar.LabelX();
            this.labelX5 = new DevComponents.DotNetBar.LabelX();
            this.VISUAL_VERSION = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.VISUAL_PASSWORD = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.labelX4 = new DevComponents.DotNetBar.LabelX();
            this.VISUAL_USUARIO = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.labelX3 = new DevComponents.DotNetBar.LabelX();
            this.VISUAL_SERVIDOR = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.backgroundWorker1 = new System.ComponentModel.BackgroundWorker();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.VISUAL_CURRENCY_ID = new DevComponents.DotNetBar.Controls.ComboBoxEx();
            this.labelX1 = new DevComponents.DotNetBar.LabelX();
            this.auxiliarBaseDatos = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.labelX2 = new DevComponents.DotNetBar.LabelX();
            this.labelX7 = new DevComponents.DotNetBar.LabelX();
            this.labelX8 = new DevComponents.DotNetBar.LabelX();
            this.auxiliarServidor = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.auxiliarTipo = new DevComponents.DotNetBar.Controls.ComboBoxEx();
            this.comboItem7 = new DevComponents.Editors.ComboItem();
            this.comboItem8 = new DevComponents.Editors.ComboItem();
            this.labelX9 = new DevComponents.DotNetBar.LabelX();
            this.auxiliarPassword = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.labelX10 = new DevComponents.DotNetBar.LabelX();
            this.auxiliarUsuario = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.labelX11 = new DevComponents.DotNetBar.LabelX();
            this.verPassword = new System.Windows.Forms.CheckBox();
            this.checkBox1 = new System.Windows.Forms.CheckBox();
            buttonX4 = new DevComponents.DotNetBar.ButtonX();
            buttonX3 = new DevComponents.DotNetBar.ButtonX();
            buttonX1 = new DevComponents.DotNetBar.ButtonX();
            buttonX2 = new DevComponents.DotNetBar.ButtonX();
            buttonX5 = new DevComponents.DotNetBar.ButtonX();
            buttonX6 = new DevComponents.DotNetBar.ButtonX();
            this.SuspendLayout();
            // 
            // buttonX4
            // 
            buttonX4.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            buttonX4.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            buttonX4.Image = global::Desarrollo.Properties.Resources.Nueno_18;
            buttonX4.Location = new System.Drawing.Point(67, 114);
            buttonX4.Name = "buttonX4";
            buttonX4.Size = new System.Drawing.Size(137, 22);
            buttonX4.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            buttonX4.TabIndex = 10;
            buttonX4.Text = "Verificar configuración";
            buttonX4.Click += new System.EventHandler(this.buttonX4_Click);
            // 
            // buttonX3
            // 
            buttonX3.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            buttonX3.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            buttonX3.Image = global::Desarrollo.Properties.Resources.Eliminar_18;
            buttonX3.Location = new System.Drawing.Point(141, 0);
            buttonX3.Name = "buttonX3";
            buttonX3.Size = new System.Drawing.Size(68, 24);
            buttonX3.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            buttonX3.TabIndex = 2;
            buttonX3.Text = "Eliminar";
            buttonX3.TextAlignment = DevComponents.DotNetBar.eButtonTextAlignment.Right;
            buttonX3.Click += new System.EventHandler(this.buttonX3_Click);
            // 
            // buttonX1
            // 
            buttonX1.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            buttonX1.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            buttonX1.Image = global::Desarrollo.Properties.Resources.Nueno_18;
            buttonX1.Location = new System.Drawing.Point(67, 0);
            buttonX1.Name = "buttonX1";
            buttonX1.Size = new System.Drawing.Size(68, 24);
            buttonX1.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            buttonX1.TabIndex = 1;
            buttonX1.Text = "Guardar";
            buttonX1.TextAlignment = DevComponents.DotNetBar.eButtonTextAlignment.Right;
            buttonX1.Click += new System.EventHandler(this.buttonX1_Click);
            // 
            // buttonX2
            // 
            buttonX2.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            buttonX2.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            buttonX2.Image = global::Desarrollo.Properties.Resources.Nuevo_18_19;
            buttonX2.Location = new System.Drawing.Point(0, 0);
            buttonX2.Name = "buttonX2";
            buttonX2.Size = new System.Drawing.Size(61, 24);
            buttonX2.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            buttonX2.TabIndex = 0;
            buttonX2.Text = "Nuevo";
            buttonX2.TextAlignment = DevComponents.DotNetBar.eButtonTextAlignment.Right;
            buttonX2.Click += new System.EventHandler(this.buttonX2_Click);
            // 
            // buttonX5
            // 
            buttonX5.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            buttonX5.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            buttonX5.Image = global::Desarrollo.Properties.Resources.Nueno_18;
            buttonX5.Location = new System.Drawing.Point(66, 224);
            buttonX5.Name = "buttonX5";
            buttonX5.Size = new System.Drawing.Size(137, 22);
            buttonX5.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            buttonX5.TabIndex = 10;
            buttonX5.Text = "Verificar configuración";
            buttonX5.Click += new System.EventHandler(this.buttonX5_Click);
            // 
            // metroStatusBar1
            // 
            this.metroStatusBar1.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.metroStatusBar1.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.metroStatusBar1.ContainerControlProcessDialogKey = true;
            this.metroStatusBar1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.metroStatusBar1.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.metroStatusBar1.ForeColor = System.Drawing.Color.Black;
            this.metroStatusBar1.Items.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.labelItem1,
            this.toolStripStatusLabel1});
            this.metroStatusBar1.Location = new System.Drawing.Point(0, 259);
            this.metroStatusBar1.Name = "metroStatusBar1";
            this.metroStatusBar1.Size = new System.Drawing.Size(669, 21);
            this.metroStatusBar1.TabIndex = 8;
            // 
            // labelItem1
            // 
            this.labelItem1.Name = "labelItem1";
            // 
            // toolStripStatusLabel1
            // 
            this.toolStripStatusLabel1.Name = "toolStripStatusLabel1";
            // 
            // VISUAL_SITE_ID
            // 
            this.VISUAL_SITE_ID.DisplayMember = "Text";
            this.VISUAL_SITE_ID.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.VISUAL_SITE_ID.ForeColor = System.Drawing.Color.Black;
            this.VISUAL_SITE_ID.FormattingEnabled = true;
            this.VISUAL_SITE_ID.ItemHeight = 16;
            this.VISUAL_SITE_ID.Location = new System.Drawing.Point(380, 86);
            this.VISUAL_SITE_ID.Name = "VISUAL_SITE_ID";
            this.VISUAL_SITE_ID.Size = new System.Drawing.Size(81, 22);
            this.VISUAL_SITE_ID.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.VISUAL_SITE_ID.TabIndex = 16;
            // 
            // VISUAL_ENTITY_ID
            // 
            this.VISUAL_ENTITY_ID.DisplayMember = "Text";
            this.VISUAL_ENTITY_ID.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.VISUAL_ENTITY_ID.ForeColor = System.Drawing.Color.Black;
            this.VISUAL_ENTITY_ID.FormattingEnabled = true;
            this.VISUAL_ENTITY_ID.ItemHeight = 16;
            this.VISUAL_ENTITY_ID.Location = new System.Drawing.Point(232, 86);
            this.VISUAL_ENTITY_ID.Name = "VISUAL_ENTITY_ID";
            this.VISUAL_ENTITY_ID.Size = new System.Drawing.Size(94, 22);
            this.VISUAL_ENTITY_ID.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.VISUAL_ENTITY_ID.TabIndex = 14;
            // 
            // VISUAL_TIPO
            // 
            this.VISUAL_TIPO.DisplayMember = "Text";
            this.VISUAL_TIPO.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.VISUAL_TIPO.ForeColor = System.Drawing.Color.Black;
            this.VISUAL_TIPO.FormattingEnabled = true;
            this.VISUAL_TIPO.ItemHeight = 16;
            this.VISUAL_TIPO.Items.AddRange(new object[] {
            this.comboItem1,
            this.comboItem2});
            this.VISUAL_TIPO.Location = new System.Drawing.Point(67, 30);
            this.VISUAL_TIPO.Name = "VISUAL_TIPO";
            this.VISUAL_TIPO.Size = new System.Drawing.Size(168, 22);
            this.VISUAL_TIPO.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.VISUAL_TIPO.TabIndex = 1;
            // 
            // comboItem1
            // 
            this.comboItem1.Text = "SQLServer";
            // 
            // comboItem2
            // 
            this.comboItem2.Text = "Oracle";
            // 
            // VISUAL_BD
            // 
            this.VISUAL_BD.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.VISUAL_BD.Border.Class = "TextBoxBorder";
            this.VISUAL_BD.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.VISUAL_BD.ForeColor = System.Drawing.Color.Black;
            this.VISUAL_BD.Location = new System.Drawing.Point(535, 30);
            this.VISUAL_BD.Name = "VISUAL_BD";
            this.VISUAL_BD.Size = new System.Drawing.Size(113, 22);
            this.VISUAL_BD.TabIndex = 5;
            this.VISUAL_BD.TabStop = false;
            this.VISUAL_BD.WatermarkBehavior = DevComponents.DotNetBar.eWatermarkBehavior.HideNonEmpty;
            // 
            // labelX15
            // 
            this.labelX15.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.labelX15.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX15.ForeColor = System.Drawing.Color.Black;
            this.labelX15.Location = new System.Drawing.Point(344, 86);
            this.labelX15.Name = "labelX15";
            this.labelX15.Size = new System.Drawing.Size(30, 22);
            this.labelX15.TabIndex = 15;
            this.labelX15.Text = "Site";
            this.labelX15.TextAlignment = System.Drawing.StringAlignment.Far;
            // 
            // labelX14
            // 
            this.labelX14.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.labelX14.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX14.ForeColor = System.Drawing.Color.Black;
            this.labelX14.Location = new System.Drawing.Point(16, 84);
            this.labelX14.Name = "labelX14";
            this.labelX14.Size = new System.Drawing.Size(44, 22);
            this.labelX14.TabIndex = 11;
            this.labelX14.Text = "Versión";
            this.labelX14.TextAlignment = System.Drawing.StringAlignment.Far;
            // 
            // labelX13
            // 
            this.labelX13.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.labelX13.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX13.ForeColor = System.Drawing.Color.Black;
            this.labelX13.Location = new System.Drawing.Point(168, 86);
            this.labelX13.Name = "labelX13";
            this.labelX13.Size = new System.Drawing.Size(49, 22);
            this.labelX13.TabIndex = 13;
            this.labelX13.Text = "Entidad";
            this.labelX13.TextAlignment = System.Drawing.StringAlignment.Far;
            // 
            // labelX12
            // 
            this.labelX12.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.labelX12.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX12.ForeColor = System.Drawing.Color.Black;
            this.labelX12.Location = new System.Drawing.Point(12, 30);
            this.labelX12.Name = "labelX12";
            this.labelX12.Size = new System.Drawing.Size(49, 22);
            this.labelX12.TabIndex = 0;
            this.labelX12.Text = "Tipo";
            this.labelX12.TextAlignment = System.Drawing.StringAlignment.Far;
            // 
            // labelX6
            // 
            this.labelX6.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.labelX6.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX6.ForeColor = System.Drawing.Color.Black;
            this.labelX6.Location = new System.Drawing.Point(454, 30);
            this.labelX6.Name = "labelX6";
            this.labelX6.Size = new System.Drawing.Size(75, 22);
            this.labelX6.TabIndex = 4;
            this.labelX6.Text = "Base de datos";
            // 
            // labelX5
            // 
            this.labelX5.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.labelX5.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX5.ForeColor = System.Drawing.Color.Black;
            this.labelX5.Location = new System.Drawing.Point(168, 58);
            this.labelX5.Name = "labelX5";
            this.labelX5.Size = new System.Drawing.Size(57, 22);
            this.labelX5.TabIndex = 8;
            this.labelX5.Text = "Contraseña";
            // 
            // VISUAL_VERSION
            // 
            this.VISUAL_VERSION.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.VISUAL_VERSION.Border.Class = "TextBoxBorder";
            this.VISUAL_VERSION.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.VISUAL_VERSION.ForeColor = System.Drawing.Color.Black;
            this.VISUAL_VERSION.Location = new System.Drawing.Point(66, 86);
            this.VISUAL_VERSION.Name = "VISUAL_VERSION";
            this.VISUAL_VERSION.Size = new System.Drawing.Size(98, 22);
            this.VISUAL_VERSION.TabIndex = 12;
            this.VISUAL_VERSION.TabStop = false;
            this.VISUAL_VERSION.WatermarkBehavior = DevComponents.DotNetBar.eWatermarkBehavior.HideNonEmpty;
            // 
            // VISUAL_PASSWORD
            // 
            this.VISUAL_PASSWORD.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.VISUAL_PASSWORD.Border.Class = "TextBoxBorder";
            this.VISUAL_PASSWORD.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.VISUAL_PASSWORD.ForeColor = System.Drawing.Color.Black;
            this.VISUAL_PASSWORD.Location = new System.Drawing.Point(231, 58);
            this.VISUAL_PASSWORD.Name = "VISUAL_PASSWORD";
            this.VISUAL_PASSWORD.Size = new System.Drawing.Size(95, 22);
            this.VISUAL_PASSWORD.TabIndex = 9;
            this.VISUAL_PASSWORD.TabStop = false;
            this.VISUAL_PASSWORD.UseSystemPasswordChar = true;
            this.VISUAL_PASSWORD.WatermarkBehavior = DevComponents.DotNetBar.eWatermarkBehavior.HideNonEmpty;
            // 
            // labelX4
            // 
            this.labelX4.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.labelX4.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX4.ForeColor = System.Drawing.Color.Black;
            this.labelX4.Location = new System.Drawing.Point(11, 58);
            this.labelX4.Name = "labelX4";
            this.labelX4.Size = new System.Drawing.Size(49, 22);
            this.labelX4.TabIndex = 6;
            this.labelX4.Text = "Usuario";
            this.labelX4.TextAlignment = System.Drawing.StringAlignment.Far;
            // 
            // VISUAL_USUARIO
            // 
            this.VISUAL_USUARIO.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.VISUAL_USUARIO.Border.Class = "TextBoxBorder";
            this.VISUAL_USUARIO.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.VISUAL_USUARIO.ForeColor = System.Drawing.Color.Black;
            this.VISUAL_USUARIO.Location = new System.Drawing.Point(66, 58);
            this.VISUAL_USUARIO.Name = "VISUAL_USUARIO";
            this.VISUAL_USUARIO.Size = new System.Drawing.Size(98, 22);
            this.VISUAL_USUARIO.TabIndex = 7;
            this.VISUAL_USUARIO.TabStop = false;
            this.VISUAL_USUARIO.WatermarkBehavior = DevComponents.DotNetBar.eWatermarkBehavior.HideNonEmpty;
            // 
            // labelX3
            // 
            this.labelX3.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.labelX3.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX3.ForeColor = System.Drawing.Color.Black;
            this.labelX3.Location = new System.Drawing.Point(241, 30);
            this.labelX3.Name = "labelX3";
            this.labelX3.Size = new System.Drawing.Size(49, 22);
            this.labelX3.TabIndex = 2;
            this.labelX3.Text = "Servidor";
            this.labelX3.TextAlignment = System.Drawing.StringAlignment.Far;
            // 
            // VISUAL_SERVIDOR
            // 
            this.VISUAL_SERVIDOR.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.VISUAL_SERVIDOR.Border.Class = "TextBoxBorder";
            this.VISUAL_SERVIDOR.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.VISUAL_SERVIDOR.ForeColor = System.Drawing.Color.Black;
            this.VISUAL_SERVIDOR.Location = new System.Drawing.Point(296, 30);
            this.VISUAL_SERVIDOR.Name = "VISUAL_SERVIDOR";
            this.VISUAL_SERVIDOR.Size = new System.Drawing.Size(152, 22);
            this.VISUAL_SERVIDOR.TabIndex = 3;
            this.VISUAL_SERVIDOR.TabStop = false;
            this.VISUAL_SERVIDOR.WatermarkBehavior = DevComponents.DotNetBar.eWatermarkBehavior.HideNonEmpty;
            // 
            // backgroundWorker1
            // 
            this.backgroundWorker1.WorkerReportsProgress = true;
            this.backgroundWorker1.WorkerSupportsCancellation = true;
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.FileName = "openFileDialog1";
            // 
            // VISUAL_CURRENCY_ID
            // 
            this.VISUAL_CURRENCY_ID.DisplayMember = "Text";
            this.VISUAL_CURRENCY_ID.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.VISUAL_CURRENCY_ID.ForeColor = System.Drawing.Color.Black;
            this.VISUAL_CURRENCY_ID.FormattingEnabled = true;
            this.VISUAL_CURRENCY_ID.ItemHeight = 16;
            this.VISUAL_CURRENCY_ID.Location = new System.Drawing.Point(572, 86);
            this.VISUAL_CURRENCY_ID.Name = "VISUAL_CURRENCY_ID";
            this.VISUAL_CURRENCY_ID.Size = new System.Drawing.Size(81, 22);
            this.VISUAL_CURRENCY_ID.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.VISUAL_CURRENCY_ID.TabIndex = 16;
            // 
            // labelX1
            // 
            this.labelX1.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.labelX1.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX1.ForeColor = System.Drawing.Color.Black;
            this.labelX1.Location = new System.Drawing.Point(467, 86);
            this.labelX1.Name = "labelX1";
            this.labelX1.Size = new System.Drawing.Size(99, 22);
            this.labelX1.TabIndex = 15;
            this.labelX1.Text = "Moneda para CE";
            this.labelX1.TextAlignment = System.Drawing.StringAlignment.Far;
            // 
            // auxiliarBaseDatos
            // 
            this.auxiliarBaseDatos.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.auxiliarBaseDatos.Border.Class = "TextBoxBorder";
            this.auxiliarBaseDatos.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.auxiliarBaseDatos.ForeColor = System.Drawing.Color.Black;
            this.auxiliarBaseDatos.Location = new System.Drawing.Point(535, 168);
            this.auxiliarBaseDatos.Name = "auxiliarBaseDatos";
            this.auxiliarBaseDatos.Size = new System.Drawing.Size(113, 22);
            this.auxiliarBaseDatos.TabIndex = 12;
            this.auxiliarBaseDatos.WatermarkBehavior = DevComponents.DotNetBar.eWatermarkBehavior.HideNonEmpty;
            // 
            // labelX2
            // 
            this.labelX2.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.labelX2.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX2.ForeColor = System.Drawing.Color.Black;
            this.labelX2.Location = new System.Drawing.Point(454, 168);
            this.labelX2.Name = "labelX2";
            this.labelX2.Size = new System.Drawing.Size(64, 22);
            this.labelX2.TabIndex = 15;
            this.labelX2.Text = "BD Auxiliar";
            this.labelX2.TextAlignment = System.Drawing.StringAlignment.Far;
            // 
            // labelX7
            // 
            this.labelX7.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.labelX7.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX7.ForeColor = System.Drawing.Color.Black;
            this.labelX7.Location = new System.Drawing.Point(12, 168);
            this.labelX7.Name = "labelX7";
            this.labelX7.Size = new System.Drawing.Size(49, 22);
            this.labelX7.TabIndex = 0;
            this.labelX7.Text = "Tipo";
            this.labelX7.TextAlignment = System.Drawing.StringAlignment.Far;
            // 
            // labelX8
            // 
            this.labelX8.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.labelX8.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX8.ForeColor = System.Drawing.Color.Black;
            this.labelX8.Location = new System.Drawing.Point(241, 168);
            this.labelX8.Name = "labelX8";
            this.labelX8.Size = new System.Drawing.Size(49, 22);
            this.labelX8.TabIndex = 2;
            this.labelX8.Text = "Servidor";
            this.labelX8.TextAlignment = System.Drawing.StringAlignment.Far;
            // 
            // auxiliarServidor
            // 
            this.auxiliarServidor.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.auxiliarServidor.Border.Class = "TextBoxBorder";
            this.auxiliarServidor.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.auxiliarServidor.ForeColor = System.Drawing.Color.Black;
            this.auxiliarServidor.Location = new System.Drawing.Point(296, 168);
            this.auxiliarServidor.Name = "auxiliarServidor";
            this.auxiliarServidor.Size = new System.Drawing.Size(152, 22);
            this.auxiliarServidor.TabIndex = 3;
            this.auxiliarServidor.TabStop = false;
            this.auxiliarServidor.WatermarkBehavior = DevComponents.DotNetBar.eWatermarkBehavior.HideNonEmpty;
            // 
            // auxiliarTipo
            // 
            this.auxiliarTipo.DisplayMember = "Text";
            this.auxiliarTipo.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.auxiliarTipo.ForeColor = System.Drawing.Color.Black;
            this.auxiliarTipo.FormattingEnabled = true;
            this.auxiliarTipo.ItemHeight = 16;
            this.auxiliarTipo.Items.AddRange(new object[] {
            this.comboItem7,
            this.comboItem8});
            this.auxiliarTipo.Location = new System.Drawing.Point(67, 168);
            this.auxiliarTipo.Name = "auxiliarTipo";
            this.auxiliarTipo.Size = new System.Drawing.Size(168, 22);
            this.auxiliarTipo.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.auxiliarTipo.TabIndex = 1;
            // 
            // comboItem7
            // 
            this.comboItem7.Text = "SQLServer";
            // 
            // comboItem8
            // 
            this.comboItem8.Text = "Oracle";
            // 
            // labelX9
            // 
            this.labelX9.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.labelX9.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX9.ForeColor = System.Drawing.Color.Black;
            this.labelX9.Location = new System.Drawing.Point(169, 196);
            this.labelX9.Name = "labelX9";
            this.labelX9.Size = new System.Drawing.Size(57, 22);
            this.labelX9.TabIndex = 8;
            this.labelX9.Text = "Contraseña";
            // 
            // auxiliarPassword
            // 
            this.auxiliarPassword.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.auxiliarPassword.Border.Class = "TextBoxBorder";
            this.auxiliarPassword.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.auxiliarPassword.ForeColor = System.Drawing.Color.Black;
            this.auxiliarPassword.Location = new System.Drawing.Point(232, 196);
            this.auxiliarPassword.Name = "auxiliarPassword";
            this.auxiliarPassword.Size = new System.Drawing.Size(95, 22);
            this.auxiliarPassword.TabIndex = 9;
            this.auxiliarPassword.TabStop = false;
            this.auxiliarPassword.UseSystemPasswordChar = true;
            this.auxiliarPassword.WatermarkBehavior = DevComponents.DotNetBar.eWatermarkBehavior.HideNonEmpty;
            // 
            // labelX10
            // 
            this.labelX10.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.labelX10.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX10.ForeColor = System.Drawing.Color.Black;
            this.labelX10.Location = new System.Drawing.Point(12, 196);
            this.labelX10.Name = "labelX10";
            this.labelX10.Size = new System.Drawing.Size(49, 22);
            this.labelX10.TabIndex = 6;
            this.labelX10.Text = "Usuario";
            this.labelX10.TextAlignment = System.Drawing.StringAlignment.Far;
            // 
            // auxiliarUsuario
            // 
            this.auxiliarUsuario.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.auxiliarUsuario.Border.Class = "TextBoxBorder";
            this.auxiliarUsuario.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.auxiliarUsuario.ForeColor = System.Drawing.Color.Black;
            this.auxiliarUsuario.Location = new System.Drawing.Point(67, 196);
            this.auxiliarUsuario.Name = "auxiliarUsuario";
            this.auxiliarUsuario.Size = new System.Drawing.Size(98, 22);
            this.auxiliarUsuario.TabIndex = 7;
            this.auxiliarUsuario.TabStop = false;
            this.auxiliarUsuario.WatermarkBehavior = DevComponents.DotNetBar.eWatermarkBehavior.HideNonEmpty;
            // 
            // labelX11
            // 
            this.labelX11.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.labelX11.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX11.ForeColor = System.Drawing.Color.Black;
            this.labelX11.Location = new System.Drawing.Point(17, 140);
            this.labelX11.Name = "labelX11";
            this.labelX11.Size = new System.Drawing.Size(357, 22);
            this.labelX11.TabIndex = 17;
            this.labelX11.Text = "Base de datos Auxiliar, servirá para guardar los datos del sistema";
            // 
            // verPassword
            // 
            this.verPassword.AutoSize = true;
            this.verPassword.BackColor = System.Drawing.Color.White;
            this.verPassword.ForeColor = System.Drawing.Color.Black;
            this.verPassword.Location = new System.Drawing.Point(332, 61);
            this.verPassword.Name = "verPassword";
            this.verPassword.Size = new System.Drawing.Size(102, 17);
            this.verPassword.TabIndex = 485;
            this.verPassword.Text = "Ver contraseña";
            this.verPassword.UseVisualStyleBackColor = false;
            this.verPassword.CheckedChanged += new System.EventHandler(this.verPassword_CheckedChanged);
            // 
            // checkBox1
            // 
            this.checkBox1.AutoSize = true;
            this.checkBox1.BackColor = System.Drawing.Color.White;
            this.checkBox1.ForeColor = System.Drawing.Color.Black;
            this.checkBox1.Location = new System.Drawing.Point(332, 199);
            this.checkBox1.Name = "checkBox1";
            this.checkBox1.Size = new System.Drawing.Size(102, 17);
            this.checkBox1.TabIndex = 486;
            this.checkBox1.Text = "Ver contraseña";
            this.checkBox1.UseVisualStyleBackColor = false;
            this.checkBox1.CheckedChanged += new System.EventHandler(this.checkBox1_CheckedChanged_1);
            // 
            // buttonX6
            // 
            buttonX6.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            buttonX6.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            buttonX6.Image = global::Desarrollo.Properties.Resources.Poliza;
            buttonX6.Location = new System.Drawing.Point(215, 0);
            buttonX6.Name = "buttonX6";
            buttonX6.Size = new System.Drawing.Size(68, 24);
            buttonX6.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            buttonX6.TabIndex = 487;
            buttonX6.Text = "Duplicar";
            buttonX6.TextAlignment = DevComponents.DotNetBar.eButtonTextAlignment.Right;
            buttonX6.Click += new System.EventHandler(this.buttonX6_Click);
            // 
            // frmEmpresa_Conexion
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.BottomLeftCornerSize = 0;
            this.ClientSize = new System.Drawing.Size(669, 280);
            this.Controls.Add(buttonX6);
            this.Controls.Add(this.checkBox1);
            this.Controls.Add(this.verPassword);
            this.Controls.Add(this.labelX11);
            this.Controls.Add(this.VISUAL_CURRENCY_ID);
            this.Controls.Add(this.VISUAL_SITE_ID);
            this.Controls.Add(buttonX3);
            this.Controls.Add(this.VISUAL_ENTITY_ID);
            this.Controls.Add(buttonX1);
            this.Controls.Add(this.auxiliarTipo);
            this.Controls.Add(this.VISUAL_TIPO);
            this.Controls.Add(this.VISUAL_BD);
            this.Controls.Add(buttonX2);
            this.Controls.Add(buttonX5);
            this.Controls.Add(buttonX4);
            this.Controls.Add(this.metroStatusBar1);
            this.Controls.Add(this.labelX2);
            this.Controls.Add(this.labelX1);
            this.Controls.Add(this.labelX15);
            this.Controls.Add(this.auxiliarServidor);
            this.Controls.Add(this.VISUAL_SERVIDOR);
            this.Controls.Add(this.labelX8);
            this.Controls.Add(this.labelX14);
            this.Controls.Add(this.labelX3);
            this.Controls.Add(this.auxiliarUsuario);
            this.Controls.Add(this.VISUAL_USUARIO);
            this.Controls.Add(this.labelX13);
            this.Controls.Add(this.labelX10);
            this.Controls.Add(this.labelX7);
            this.Controls.Add(this.labelX4);
            this.Controls.Add(this.auxiliarPassword);
            this.Controls.Add(this.labelX12);
            this.Controls.Add(this.VISUAL_PASSWORD);
            this.Controls.Add(this.labelX6);
            this.Controls.Add(this.auxiliarBaseDatos);
            this.Controls.Add(this.labelX9);
            this.Controls.Add(this.VISUAL_VERSION);
            this.Controls.Add(this.labelX5);
            this.Cursor = System.Windows.Forms.Cursors.Default;
            this.DoubleBuffered = true;
            this.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "frmEmpresa_Conexion";
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Empresa Conexión";
            this.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.frmEmpresa_KeyPress);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevComponents.DotNetBar.Metro.MetroStatusBar metroStatusBar1;
        private DevComponents.DotNetBar.LabelItem labelItem1;
        private DevComponents.DotNetBar.LabelX labelX6;
        private DevComponents.DotNetBar.Controls.TextBoxX VISUAL_BD;
        private DevComponents.DotNetBar.LabelX labelX5;
        private DevComponents.DotNetBar.Controls.TextBoxX VISUAL_PASSWORD;
        private DevComponents.DotNetBar.LabelX labelX4;
        private DevComponents.DotNetBar.Controls.TextBoxX VISUAL_USUARIO;
        private DevComponents.DotNetBar.LabelX labelX3;
        private DevComponents.DotNetBar.Controls.TextBoxX VISUAL_SERVIDOR;
        private DevComponents.DotNetBar.Controls.ComboBoxEx VISUAL_SITE_ID;
        private DevComponents.DotNetBar.Controls.ComboBoxEx VISUAL_ENTITY_ID;
        private DevComponents.DotNetBar.Controls.ComboBoxEx VISUAL_TIPO;
        private DevComponents.Editors.ComboItem comboItem1;
        private DevComponents.Editors.ComboItem comboItem2;
        private DevComponents.DotNetBar.LabelX labelX15;
        private DevComponents.DotNetBar.LabelX labelX14;
        private DevComponents.DotNetBar.LabelX labelX13;
        private DevComponents.DotNetBar.LabelX labelX12;
        private DevComponents.DotNetBar.Controls.TextBoxX VISUAL_VERSION;
        private DevComponents.DotNetBar.LabelItem toolStripStatusLabel1;
        private System.ComponentModel.BackgroundWorker backgroundWorker1;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private DevComponents.DotNetBar.Controls.ComboBoxEx VISUAL_CURRENCY_ID;
        private DevComponents.DotNetBar.LabelX labelX1;
        private DevComponents.DotNetBar.Controls.TextBoxX auxiliarBaseDatos;
        private DevComponents.DotNetBar.LabelX labelX2;
        private DevComponents.DotNetBar.LabelX labelX7;
        private DevComponents.DotNetBar.LabelX labelX8;
        private DevComponents.DotNetBar.Controls.TextBoxX auxiliarServidor;
        private DevComponents.DotNetBar.Controls.ComboBoxEx auxiliarTipo;
        private DevComponents.Editors.ComboItem comboItem7;
        private DevComponents.Editors.ComboItem comboItem8;
        private DevComponents.DotNetBar.LabelX labelX9;
        private DevComponents.DotNetBar.Controls.TextBoxX auxiliarPassword;
        private DevComponents.DotNetBar.LabelX labelX10;
        private DevComponents.DotNetBar.Controls.TextBoxX auxiliarUsuario;
        private DevComponents.DotNetBar.LabelX labelX11;
        private System.Windows.Forms.CheckBox verPassword;
        private System.Windows.Forms.CheckBox checkBox1;
    }
}