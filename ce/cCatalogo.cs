﻿using DevComponents.DotNetBar;
using Generales;
using ModeloDocumentosFX;
using OfficeOpenXml;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Desarrollo
{
    class cCatalogo
    {
        private string sSQL = "";
        public string ROW_ID { get; set; }
        public string versionField { get; set; }
        public string rFCField { get; set; }
        public string mesField { get; set; }
        public string anoField { get; set; }
        public string totalCtasField { get; set; }

        public string ESTADO { get; set; }

        public bool usarPeriodo { get; set; }
        public string periodo { get; set; }
        

        private cCONEXCION oData;
        public override string ToString() { return ROW_ID; }

        public cCatalogo()
        {
            ModeloDocumentosFXContainer dbContext = new ModeloDocumentosFXContainer();
            try
            {
                string conection = dbContext.Database.Connection.ConnectionString;
                oData = new cCONEXCION(conection);
            }
            catch (Exception err)
            {
                ErrorFX.mostrar(err, false, true, "frmEmpresa - 96 - ", false);
            }
            limpiar();
        }

        public void limpiar()
        {
            ROW_ID = "";
            versionField = "";
            rFCField = "";
            mesField = "";
            anoField = "";
            totalCtasField = "";
            ESTADO = "";
            usarPeriodo = false;
            periodo = "";
            periodoFinal = "";
        }


        public List<cCatalogo> todos(string rFCFieldp, string anio, string mes
                        , string estado)
        {

            List<cCatalogo> lista = new List<cCatalogo>();

            sSQL = " SELECT * ";
            sSQL += " FROM Catalogo ";
            sSQL += " WHERE 1=1 ";
            sSQL += " AND rFCField LIKE '%" + rFCFieldp + "%' ";

            if (!String.IsNullOrEmpty(anio))
            {
                sSQL += " AND CAST(anoField as varchar(max)) LIKE '%" + anio + "%' ";
            }
            if (!String.IsNullOrEmpty(mes))
            {
                sSQL += " AND CAST(mesField as varchar(max)) LIKE '%" + mes + "%' ";
            }
            if (!String.IsNullOrEmpty(estado))
            {
                sSQL += " AND ESTADO LIKE '%" + estado + "%' ";
            }


            sSQL += " ORDER BY rFCField,anoField,mesField";
            DataTable oDataTable = oData.EjecutarConsulta(sSQL);
            foreach (DataRow oDataRow in oDataTable.Rows)
            {
                cCatalogo ocCatalogo = new cCatalogo();
                ocCatalogo.cargar(oDataRow["ROW_ID"].ToString());
                lista.Add(ocCatalogo);
            }
            return lista;

        }

        public bool cargar(string ROW_IDp)
        {
            sSQL = " SELECT * ";
            sSQL += " FROM Catalogo ";
            sSQL += " WHERE ROW_ID=" + ROW_IDp + " ";
            DataTable oDataTable = oData.EjecutarConsulta(sSQL);
            if (oDataTable != null)
            {
                foreach (DataRow oDataRow in oDataTable.Rows)
                {
                    //Añadir el Registro
                    ROW_ID = oDataRow["ROW_ID"].ToString();
                    versionField = oDataRow["versionField"].ToString();
                    rFCField = oDataRow["rFCField"].ToString();
                    mesField = oDataRow["mesField"].ToString();
                    anoField = oDataRow["anoField"].ToString();
                    totalCtasField = oDataRow["totalCtasField"].ToString();
                    ESTADO = oDataRow["ESTADO"].ToString();

                    usarPeriodo = false;
                    try
                    {
                        usarPeriodo = bool.Parse(oDataRow["usarPeriodo"].ToString());
                    }
                    catch
                    {

                    }
                    periodo = oDataRow["periodo"].ToString();
                    periodoFinal = oDataRow["periodoFinal"].ToString();

                    return true;
                }
            }
            return false;
        }

        public bool guardar()
        {
            //if (versionField.Trim() == "")
            //{
            //    MessageBoxEx.Show("La versión es requerida.", Application.ProductName + "-" + Application.ProductVersion, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            //    return false;
            //}

            if (rFCField.Trim() == "")
            {
                MessageBoxEx.Show("Escriba un RFC.", Application.ProductName + "-" + Application.ProductVersion, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return false;
            }
            rFCField = rFCField.Replace("'", "");
            if (mesField.Trim() == "")
            {
                MessageBoxEx.Show("Escriba un Mes.", Application.ProductName + "-" + Application.ProductVersion, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return false;
            }
            if (anoField.Trim() == "")
            {
                MessageBoxEx.Show("Escriba un Año.", Application.ProductName + "-" + Application.ProductVersion, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return false;
            }



            if (ROW_ID == "")
            {
                if (existe(rFCField, anoField, mesField))
                {
                    MessageBoxEx.Show("La declaración del año " + anoField + " y mes " + mesField + " del RFC: " + rFCField + " ya existe.", Application.ProductName + "-" + Application.ProductVersion, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                    return false;
                }

                sSQL = " INSERT INTO Catalogo ";
                sSQL += " ( ";
                sSQL += " [versionField],[rFCField] ";
                sSQL += " ,[mesField],[anoField],[totalCtasField] ";
                sSQL += " ,[ESTADO]";
                sSQL += " ,[usarPeriodo],[periodo],[periodoFinal]"; 
                sSQL += " )";
                sSQL += " VALUES ";
                sSQL += "(";
                sSQL += " '" + versionField + "','" + rFCField + "' ";
                sSQL += " ,'" + mesField + "','" + anoField + "','" + totalCtasField + "' ";
                sSQL += " ,'" + ESTADO + "'";
                sSQL += " ,'" + usarPeriodo + "','" + periodo + "','" + periodoFinal + "'"; 
                sSQL += ")";
                DataTable oDataTable = oData.EjecutarConsulta(sSQL);
                if (oDataTable != null)
                {

                    sSQL = "SELECT TOP 1 MAX(ROW_ID) as ROW_ID FROM Catalogo";
                    oDataTable = oData.EjecutarConsulta(sSQL);
                    if (oDataTable != null)
                    {
                        foreach (DataRow oDataRow in oDataTable.Rows)
                        {
                            ROW_ID = oDataRow["ROW_ID"].ToString();
                        }
                    }
                }

            }
            else
            {
                sSQL = " UPDATE Catalogo ";
                sSQL += " SET versionField='" + versionField + "',rFCField='" + rFCField + "' ";
                sSQL += " ,mesField='" + mesField + "',anoField='" + anoField + "',totalCtasField='" + totalCtasField + "' ";
                sSQL += " ,ESTADO='" + ESTADO + "'";
                sSQL += " ,usarPeriodo='" + usarPeriodo + "',periodo='" + periodo + "',periodoFinal='" + periodoFinal + "'";  
                sSQL += " WHERE ROW_ID=" + ROW_ID;
                oData.EjecutarConsulta(sSQL);

            }
            return true;

        }

        private bool existe(string rFCFieldp, string anoFieldp, string mesFieldp)
        {
            string sSQL = "SELECT TOP 1 1 as ROW_ID FROM Catalogo WHERE rFCField='" + rFCFieldp + "' and usarPeriodo='" + usarPeriodo  + "'"
            + " AND anoField='" + anoFieldp + "' AND  mesField='" + mesFieldp + "' AND periodo='" + periodo + "'";
            DataTable oDataTable = oData.EjecutarConsulta(sSQL);
            if (oDataTable != null)
            {
                foreach (DataRow oDataRow in oDataTable.Rows)
                {
                    return true;
                }
            }
            return false;
        }

        public bool eliminar(string ROW_IDp)
        {
            if (ROW_IDp != "")
            {
                sSQL = "DELETE FROM Catalogo ";
                sSQL += " WHERE ROW_ID='" + ROW_IDp + "' ";
                oData.EjecutarConsulta(sSQL);
                return true;
            }
            return false;
        }

        public string periodoFinal { get; set; }
    }
}
