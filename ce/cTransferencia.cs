﻿using DevComponents.DotNetBar;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Generales;
using ModeloDocumentosFX;

namespace Desarrollo
{

    class cTransferencia
    {
        private string sSQL = "";
        public string ROW_ID { get; set; }

        public string ROW_ID_TRANSACCION { get; set; }

        public string ctaDestField { get; set; }
        public string bancoOriField { get; set; }
        public string ctaOriField { get; set; }
        public DateTime fechaField { get; set; }
        public double montoField { get; set; }
        public string bancoDestField { get; set; }
        public string benefField { get; set; }
        public string rFCField { get; set; }

        private cCONEXCION oData = new cCONEXCION();
        public override string ToString() { return ROW_ID; }

        public cTransferencia()
        {
            ModeloDocumentosFXContainer dbContext = new ModeloDocumentosFXContainer();
            try
            {
                string conection = dbContext.Database.Connection.ConnectionString;
                oData = new cCONEXCION(conection);
            }
            catch (Exception err)
            {
                ErrorFX.mostrar(err, false, true, "frmEmpresa - 96 - ", false);
            }
            limpiar();
        }


        public void limpiar()
        {
            ROW_ID = "";
            ROW_ID_TRANSACCION = "";
            ctaDestField="";
            fechaField = DateTime.Now;
            montoField = 0;
            bancoOriField="";
            ctaOriField="";
            bancoDestField="";
            benefField="";
            rFCField="";

        }


        public List<cTransferencia> todos(string ROW_ID_TRANSACCIONp,string numCtaFieldp = "")
        {

            List<cTransferencia> lista = new List<cTransferencia>();

            sSQL = " SELECT * ";
            sSQL += " FROM PolizasPolizaTransaccionTransferencia ";
            sSQL += " WHERE 1=1 AND ROW_ID_TRANSACCION=" + ROW_ID_TRANSACCIONp  + "";
            DataTable oDataTable = oData.EjecutarConsulta(sSQL);
            foreach (DataRow oDataRow in oDataTable.Rows)
            {
                cTransferencia ocCatalogo = new cTransferencia();
                ocCatalogo.cargar(oDataRow["ROW_ID"].ToString());
                lista.Add(ocCatalogo);
            }
            return lista;

        }

        public bool cargar(string ROW_IDp)
        {
            sSQL = " SELECT * ";
            sSQL += " FROM PolizasPolizaTransaccionTransferencia ";
            sSQL += " WHERE ROW_ID=" + ROW_IDp + " ";
            DataTable oDataTable = oData.EjecutarConsulta(sSQL);
            if (oDataTable != null)
            {
                foreach (DataRow oDataRow in oDataTable.Rows)
                {
                    //Añadir el Registro
                    ROW_ID = oDataRow["ROW_ID"].ToString();
                    ROW_ID_TRANSACCION = oDataRow["ROW_ID_TRANSACCION"].ToString();
                    ctaDestField = oDataRow["ctaDestField"].ToString();
                    bancoOriField = oDataRow["bancoOriField"].ToString();
                    ctaOriField = oDataRow["ctaOriField"].ToString();
                    fechaField = DateTime.Parse(oDataRow["fechaField"].ToString());
                    montoField = double.Parse(oDataRow["montoField"].ToString());
                    bancoDestField = oDataRow["bancoDestField"].ToString();
                    rFCField = oDataRow["rFCField"].ToString();
                    benefField = oDataRow["benefField"].ToString();

                    return true;
                }
            }
            return false;
        }

        public bool guardar(bool validar=true)
        {
            if (ROW_ID == "")
            {
                sSQL = " INSERT INTO PolizasPolizaTransaccionTransferencia ";
                sSQL += " ( ";
                sSQL += " [ROW_ID_TRANSACCION],[ctaDestField],[bancoOriField]";
                sSQL += ",[montoField],[ctaOriField],[fechaField] ";
                sSQL += ",[bancoDestField],[rFCField],[benefField]";
                sSQL += " )";
                sSQL += " VALUES ";
                sSQL += "(";
                sSQL += " " + ROW_ID_TRANSACCION + ",'" + ctaDestField + "','" + bancoOriField + "'";
                sSQL += "," + montoField.ToString("N4").Replace(",", "") + ",'" + ctaOriField  + "'," + oData.convertir_fecha(fechaField)+ "";
                sSQL += ",'" + bancoDestField + "','" + rFCField + "','" + benefField + "' ";
                sSQL += ")";
                DataTable oDataTable = oData.EjecutarConsulta(sSQL);
                if (oDataTable != null)
                {

                    sSQL = "SELECT TOP 1 MAX(ROW_ID) as ROW_ID FROM PolizasPolizaTransaccionTransferencia";
                    oDataTable = oData.EjecutarConsulta(sSQL);
                    if (oDataTable != null)
                    {
                        foreach (DataRow oDataRow in oDataTable.Rows)
                        {
                            ROW_ID = oDataRow["ROW_ID"].ToString();
                        }
                    }
                }

            }
            else
            {
                sSQL = " UPDATE PolizasPolizaTransaccionTransferencia ";
                sSQL += " SET ROW_ID_TRANSACCION=" + ROW_ID_TRANSACCION + ",ctaDestField='" + ctaDestField + "',bancoOriField='" + bancoOriField + "'";
                sSQL += ",montoField=" + montoField.ToString("N4").Replace(",", "") + ",ctaOriField='" + ctaOriField + "'";
                sSQL += ",fechaField=" + oData.convertir_fecha(fechaField) + " ";
                sSQL += ",bancoDestField='" + bancoDestField + "',rFCField='" + rFCField + "',benefField='" + benefField + "' ";
                sSQL += " WHERE ROW_ID=" + ROW_ID;
                oData.EjecutarConsulta(sSQL);
            }
            return true;

        }

        public bool eliminar(string ROW_IDp)
        {
            if (ROW_IDp != "")
            {
                sSQL = "DELETE FROM PolizasPolizaTransaccionTransferencia ";
                sSQL += " WHERE ROW_ID='" + ROW_IDp + "' ";
                oData.EjecutarConsulta(sSQL);
                return true;
            }
            return false;
        }
    }
}
