using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevComponents.DotNetBar;
using System.IO;
using OfficeOpenXml;
using System.Collections;

namespace Desarrollo
{
    public partial class frmXML : DevComponents.DotNetBar.Metro.MetroForm
    {
        string ROW_ID_CATALOGO = "";
        cEMPRESA_CE ocEMPRESA;
        string tipo = "";

        public frmXML(string ROW_ID_CATALOGOp, string tipop, string ROW_ID_EMPRESA)
        {
            ROW_ID_CATALOGO = ROW_ID_CATALOGOp;

            ocEMPRESA = new cEMPRESA_CE();
            ocEMPRESA.cargar_rfc(ROW_ID_EMPRESA);
            //AuxiliarCtas
            //AuxiliarFolios
            //Polizas
            tipo = tipop;
            InitializeComponent();
           
        }


        #region Acciones

        private void mostrar_mensaje(string mensaje, bool mostrar_error)
        {
            ToastNotification.Show(this,
                mensaje,
                mostrar_error ? global::Desarrollo.Properties.Resources.error : global::Desarrollo.Properties.Resources.guardar,
                3000,
                mostrar_error ? eToastGlowColor.Red : eToastGlowColor.Green,
                eToastPosition.TopCenter);
        }
        
        #endregion

        private void buttonX3_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Cancel;
        }

        private void buttonX1_Click(object sender, EventArgs e)
        {
            guardar();
        }

        private void guardar()
        {
            
        }

        private void TIPO_POLIZA_SelectedIndexChanged(object sender, EventArgs e)
        {
            verificar();
        }

        private void verificar()
        {
            NumOrden.Text = "";
            NumTramite.Text = "";
            DevComponents.Editors.ComboItem oComboItem = (DevComponents.Editors.ComboItem)TIPO_POLIZA.Items[TIPO_POLIZA.SelectedIndex];

            if(TIPO_POLIZA.SelectedItem!=null)
            {
                if(
                    (oComboItem.Value == "AF")
                    | (oComboItem.Value == "FC"))
                {
                    NumOrden.Enabled = true;
                    NumTramite.Enabled = false;
                }
                else
                {
                    NumOrden.Enabled = false;
                    NumTramite.Enabled = true;
                }


            }
        }

        private void buttonX22_Click(object sender, EventArgs e)
        {
            generar_poliza();
        }

        private void generar_poliza()
        {
            if (TIPO_POLIZA.SelectedIndex== null)
            {
                MessageBoxEx.Show("Seleccione el tipo a declarar.", Application.ProductName + "-" + Application.ProductVersion, MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
            if (NumOrden.Enabled)
            {
                if (NumOrden.Text == "")
                {
                    MessageBoxEx.Show("Escriba el N�mero de Orden.", Application.ProductName + "-" + Application.ProductVersion, MessageBoxButtons.OK, MessageBoxIcon.Information);
                    return;
                }
                //Validar el Patron
                if (!System.Text.RegularExpressions.Regex.IsMatch(NumOrden.Text, "[A-Z]{3}[0-6][0-9][0-9]{5}(/)[0-9]{2}", System.Text.RegularExpressions.RegexOptions.IgnoreCase))
                {
                    MessageBoxEx.Show("El N�mero de Orden no coincide con el patron requerido por el SAT (3 Letras 5 n�meros seguido '/' 2 n�meros).", Application.ProductName + "-" + Application.ProductVersion, MessageBoxButtons.OK, MessageBoxIcon.Information);
                    return;
                }


            }
            if (NumTramite.Enabled)
            {
                if (NumTramite.Text == "")
                {
                    MessageBoxEx.Show("Escriba el N�mero de Tramite.", Application.ProductName + "-" + Application.ProductVersion, MessageBoxButtons.OK, MessageBoxIcon.Information);
                    return;
                }
                //Validar el Patron
                if (!System.Text.RegularExpressions.Regex.IsMatch(NumTramite.Text, "[0-9]{10}", System.Text.RegularExpressions.RegexOptions.IgnoreCase))
                {
                    MessageBoxEx.Show("El N�mero de Tramite no coincide con el patron requerido por el SAT (10 n�meros).", Application.ProductName + "-" + Application.ProductVersion, MessageBoxButtons.OK, MessageBoxIcon.Information);
                    return;
                }
            }
            //AuxiliarCtas
            //AuxiliarFolios
            //Polizas
            switch(tipo)
            {
                case "AuxiliarCtas":
                    generar_auxiliar_cuentas();
                break;
                case "AuxiliarFolios":
                    generar_auxiliar_folios();
                break;
                case "Polizas":
                    generar_auxiliar_polizas();
                break;
            }

        }

        private void generar_auxiliar_polizas()
        {
            cPolizas ocPolizas = new cPolizas();
            DevComponents.Editors.ComboItem oComboItem = (DevComponents.Editors.ComboItem)TIPO_POLIZA.Items[TIPO_POLIZA.SelectedIndex];
            ocPolizas.ROW_ID_CATALOGO = ROW_ID_CATALOGO;            
            ocPolizas.tipoSolicitudField = oComboItem.Value.ToString();
            ocPolizas.numTramiteField = NumTramite.Text;
            ocPolizas.numOrdenField = NumOrden.Text;

            if (ocPolizas.guardar())
            {
                cPoliza ocPoliza = new cPoliza();
                cCatalogo ocCatalogo=new cCatalogo();
                ocCatalogo.cargar(ROW_ID_CATALOGO);
                ocPoliza.generar_archivo(ocCatalogo, "", true, ocPolizas);
                DialogResult = DialogResult.OK;
            }
        }

        private void generar_auxiliar_folios()
        {
            cAuxiliarFolios ocAuxiliarFolios = new cAuxiliarFolios();
            DevComponents.Editors.ComboItem oComboItem = (DevComponents.Editors.ComboItem)TIPO_POLIZA.Items[TIPO_POLIZA.SelectedIndex];
            ocAuxiliarFolios.ROW_ID_CATALOGO = ROW_ID_CATALOGO;
            ocAuxiliarFolios.tipoSolicitudField = oComboItem.Value.ToString();
            ocAuxiliarFolios.numTramiteField = NumTramite.Text;
            ocAuxiliarFolios.numOrdenField = NumOrden.Text;

            if (ocAuxiliarFolios.guardar())
            {
                ocAuxiliarFolios.generar(ROW_ID_CATALOGO);
                DialogResult = DialogResult.OK;
            }
        }

        private void generar_auxiliar_cuentas()
        {
            
            cAuxiliarCtas ocAuxiliarCtas=new cAuxiliarCtas();
            DevComponents.Editors.ComboItem oComboItem = (DevComponents.Editors.ComboItem)TIPO_POLIZA.Items[TIPO_POLIZA.SelectedIndex];
            ocAuxiliarCtas.ROW_ID_CATALOGO = ROW_ID_CATALOGO;
            ocAuxiliarCtas.tipoSolicitudField = oComboItem.Value.ToString();
            ocAuxiliarCtas.numTramiteField = NumTramite.Text;
            ocAuxiliarCtas.numOrdenField = NumOrden.Text;
            
            if(ocAuxiliarCtas.guardar())
            {
                ocAuxiliarCtas.generar(ROW_ID_CATALOGO);
                DialogResult = DialogResult.OK;
            }
            

        }

    }
}