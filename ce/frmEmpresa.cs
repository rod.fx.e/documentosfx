using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevComponents.DotNetBar;
using Generales;

namespace Desarrollo
{
    public partial class frmEmpresa : DevComponents.DotNetBar.Metro.MetroForm
    {
        bool modificado = false;
        private cCONEXCION oData = new cCONEXCION();
        cEMPRESA_CE oObjeto = new cEMPRESA_CE();
        
        public frmEmpresa()
        {
            oObjeto = new cEMPRESA_CE();
            InitializeComponent();
            
        }
        public frmEmpresa(string ROW_IDp)
        {
            oObjeto = new cEMPRESA_CE();
            oObjeto.ROW_ID = ROW_IDp;
            InitializeComponent();
            cargar(ROW_IDp);
        }

        private void cargar(string ROW_IDp)
        {
            if (oObjeto.cargar(ROW_IDp))
            {
                ID.Text = oObjeto.ID;
                RFC.Text = oObjeto.RFC;
                DIRECTORIO_XML_CLIENTES.Text = oObjeto.DIRECTORIO_XML_CLIENTES;
                DIRECTORIO_XML_PROVEEDORES.Text = oObjeto.DIRECTORIO_XML_PROVEEDORES;
                CERTIFICADO.Text=oObjeto.CERTIFICADO;
                LLAVE.Text=oObjeto.LLAVE;
                PASSWORD.Text=oObjeto.PASSWORD;
                NIVEL.Text = oObjeto.NIVEL;
                NO_CERTIFICADO.Text = oObjeto.NO_CERTIFICADO;
                ACCOUNT_CUSTOMER.Text = oObjeto.ACCOUNT_CUSTOMER;
                ACCOUNT_VENDOR.Text = oObjeto.ACCOUNT_VENDOR;
                PERIODO.Checked = oObjeto.PERIODO;
                cargar_descripciones();
                cargar_conexiones();
                cargar_moneda();
                toolStripStatusLabel1.Text = "Datos Actualizados " + DateTime.Now.ToShortDateString() + " " + DateTime.Now.ToLongTimeString();
                modificado = false;
            }
        }
        private void buttonX2_Click(object sender, EventArgs e)
        {
            limpiar();
        }

        private void limpiar()
        {
            if (modificado)
            {

                DialogResult Resultado = MessageBoxEx.Show("�Desea guardar los cambios?", Application.ProductName + "-" + Application.ProductVersion, MessageBoxButtons.YesNo);
                if (Resultado.ToString() == "Yes")
                {
                    guardar();
                }


            }
            modificado = false;
            oObjeto = new cEMPRESA_CE();

            foreach (var c in this.Controls)
            {
                if (c is TextBox)
                {
                    ((TextBox)c).Text = "";
                }
            }

            foreach (var c in superTabControlPanel1.Controls)
            {
                if (c is TextBox)
                {
                    ((TextBox)c).Text = "";
                }
            }
            foreach (var c in superTabControlPanel2.Controls)
            {
                if (c is TextBox)
                {
                    ((TextBox)c).Text = "";
                }
            }
        }

        private void buttonX1_Click(object sender, EventArgs e)
        {
            guardar();
        }

        private void guardar()
        {
            oObjeto.ID = ID.Text;
            oObjeto.RFC = RFC.Text;
            oObjeto.DIRECTORIO_XML_CLIENTES = DIRECTORIO_XML_CLIENTES.Text;
            oObjeto.DIRECTORIO_XML_PROVEEDORES = DIRECTORIO_XML_PROVEEDORES.Text;
            oObjeto.CERTIFICADO = CERTIFICADO.Text;
            oObjeto.LLAVE = LLAVE.Text;
            oObjeto.PASSWORD = PASSWORD.Text;
            if (!String.IsNullOrEmpty(NIVEL.Text))
            {

            }
            oObjeto.NIVEL = NIVEL.Text;
            oObjeto.PERIODO = PERIODO.Checked;
            oObjeto.ACCOUNT_VENDOR=ACCOUNT_VENDOR.Text;
            oObjeto.ACCOUNT_CUSTOMER = ACCOUNT_CUSTOMER.Text;
            if (oObjeto.guardar())
            {
                NO_CERTIFICADO.Text=oObjeto.NO_CERTIFICADO;
                toolStripStatusLabel1.Text = "Datos Actualizados " + DateTime.Now.ToShortDateString() + " " + DateTime.Now.ToLongTimeString();
                modificado = false;
            }
        }

        private void buttonX3_Click(object sender, EventArgs e)
        {
            eliminar();
        }

        private void eliminar()
        {

            DialogResult Resultado = MessageBoxEx.Show("�Desea eliminar?", Application.ProductName + "-" + Application.ProductVersion, MessageBoxButtons.YesNo);
            if (Resultado.ToString() == "Yes")
            {
                if (oObjeto.eliminar(oObjeto.ROW_ID))
                {
                    modificado = false;
                    limpiar();
                }
            }
        }

        private void frmEmpresa_KeyPress(object sender, KeyPressEventArgs e)
        {
            modificado = true;
        }

        private void buttonX4_Click(object sender, EventArgs e)
        {
            //verificar_conexion();
        }

        //private void verificar_conexion()
        //{
        //    //Realizar la conexcion
        //    oObjeto.VISUAL_TIPO = VISUAL_TIPO.Text;
        //    oObjeto.VISUAL_SERVIDOR = VISUAL_SERVIDOR.Text;
        //    oObjeto.VISUAL_BD = VISUAL_BD.Text;
        //    oObjeto.VISUAL_USUARIO = VISUAL_USUARIO.Text;
        //    oObjeto.VISUAL_PASSWORD = VISUAL_PASSWORD.Text;
        //    cCONEXCION ocCONEXCION=oObjeto.conectar();
        //    if (ocCONEXCION != null)
        //    {


        //        //Traer los datos del Visual en el ID, RFC y las demas versiones
        //        string sSQL = "SELECT * FROM APPLICATION_GLOBAL";
        //        DataTable oDataTable = ocCONEXCION.EjecutarConsulta(sSQL);
        //        foreach (DataRow oDataRow in oDataTable.Rows)
        //        {
        //            VISUAL_VERSION.Text=oDataRow["DBVERSION"].ToString();
        //            ID.Text = oDataRow["COMPANY_NAME"].ToString();
        //            if (oDataTable.Columns.Contains("TAX_ID_NUMBER"))
        //            {
        //                RFC.Text = oDataRow["TAX_ID_NUMBER"].ToString();
        //            }

        //        }

        //        VISUAL_SITE_ID.Text = "";
        //        VISUAL_SITE_ID.Enabled = false;
        //        if (
        //            (VISUAL_VERSION.Text == "7.1.0")
        //            | (VISUAL_VERSION.Text == "7.1.2")
        //            | (VISUAL_VERSION.Text == "7.1.1")
        //        )
        //        {
        //            VISUAL_SITE_ID.Enabled = true;
        //            //Traer los datos del Visual en el ID, RFC y las demas versiones
        //            sSQL = "SELECT * FROM [SITE]";
        //            VISUAL_SITE_ID.Items.Clear();
        //            oDataTable = ocCONEXCION.EjecutarConsulta(sSQL);
        //            foreach (DataRow oDataRow in oDataTable.Rows)
        //            {
        //                VISUAL_SITE_ID.Items.Add(oDataRow["ID"].ToString());
        //            }
        //            //Buscar el RFC
        //            sSQL = "SELECT VAT_REGISTRATION FROM ACCOUNTING_ENTITY";
        //            RFC.Text = "";
        //            oDataTable = ocCONEXCION.EjecutarConsulta(sSQL);
        //            foreach (DataRow oDataRow in oDataTable.Rows)
        //            {
        //                RFC.Text = oDataRow["VAT_REGISTRATION"].ToString();
        //            }

        //            VISUAL_ENTITY_ID.Items.Clear();
        //            sSQL = "SELECT * FROM ACCOUNTING_ENTITY";
        //            oDataTable = ocCONEXCION.EjecutarConsulta(sSQL);
        //            foreach (DataRow oDataRow in oDataTable.Rows)
        //            {
        //                VISUAL_ENTITY_ID.Items.Add(oDataRow["ID"].ToString());
        //            }
        //            if (VISUAL_ENTITY_ID.SelectedIndex > 0)
        //            {
        //                VISUAL_ENTITY_ID.SelectedIndex = 0;
        //            }
        //        }
        //        else
        //        {
        //            VISUAL_ENTITY_ID.Items.Clear();
        //             sSQL = "SELECT * FROM ENTITY";
        //             oDataTable = ocCONEXCION.EjecutarConsulta(sSQL);
        //            foreach (DataRow oDataRow in oDataTable.Rows)
        //            {
        //                VISUAL_ENTITY_ID.Items.Add(oDataRow["ID"].ToString());
        //            }
        //            if (VISUAL_ENTITY_ID.SelectedIndex > 0)
        //            {
        //                VISUAL_ENTITY_ID.SelectedIndex = 0;
        //            }
        //        }
        //    }


        //}

        private void buttonX6_Click(object sender, EventArgs e)
        {
            FolderBrowserDialog folderBrowserDialog1 = new FolderBrowserDialog();
            if (folderBrowserDialog1.ShowDialog() == DialogResult.OK)
            {
                DIRECTORIO_XML_CLIENTES.Text = folderBrowserDialog1.SelectedPath;
            }
        }

        private void buttonX5_Click(object sender, EventArgs e)
        {
            FolderBrowserDialog folderBrowserDialog1 = new FolderBrowserDialog();
            if (folderBrowserDialog1.ShowDialog() == DialogResult.OK)
            {
                DIRECTORIO_XML_PROVEEDORES.Text = folderBrowserDialog1.SelectedPath;
            }
        }


        private void buttonX18_Click(object sender, EventArgs e)
        {
            cargar_conexiones();
        }

        private void cargar_conexiones()
        {
            if (oObjeto.ROW_ID != "")
            {
                dtgGeneral.Rows.Clear();
                cEMPRESA_CONEXION ocEMPRESA_CONEXION = new cEMPRESA_CONEXION();
                foreach (cEMPRESA_CONEXION registro in ocEMPRESA_CONEXION.todos(oObjeto.ROW_ID))
                {
                    int n = dtgGeneral.Rows.Add();
                    actualizar_linea(registro, n);
                }
            }
        }

        private void actualizar_linea(cEMPRESA_CONEXION registro, int n)
        {
            for (int i = 0; i < dtgGeneral.Columns.Count; i++)
            {
                dtgGeneral.Rows[n].Cells[i].Value = "";
            }
            dtgGeneral.Rows[n].Tag = registro;
            dtgGeneral.Rows[n].Cells["ROW_ID"].Value = registro.ROW_ID;
            dtgGeneral.Rows[n].Cells["VISUAL_ENTITY_ID"].Value = registro.VISUAL_ENTITY_ID;
            dtgGeneral.Rows[n].Cells["VISUAL_SITE_ID"].Value = registro.VISUAL_SITE_ID;
            dtgGeneral.Rows[n].Cells["VISUAL_BD"].Value = registro.VISUAL_BD;
            dtgGeneral.Rows[n].Cells["VISUAL_TIPO"].Value = registro.VISUAL_TIPO;
        }

        private void buttonX19_Click(object sender, EventArgs e)
        {
            nueva_cuenta();
        }

        private void nueva_cuenta()
        {
            if (oObjeto.ROW_ID == "")
            {
                MessageBoxEx.Show("Guarde primero.", Application.ProductName + "-" + Application.ProductVersion, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return;
            }
            frmEmpresa_Conexion ofrmEmpresa_Conexion = new frmEmpresa_Conexion(oObjeto.ROW_ID);
            ofrmEmpresa_Conexion.ShowDialog();
            cargar_conexiones();
        }

        private void buttonX17_Click(object sender, EventArgs e)
        {
            eliminar_conexion();
        }

        private void eliminar_conexion()
        {
            dtgGeneral.EndEdit();
            if (dtgGeneral.SelectedCells.Count > 0)
            {
                DialogResult Resultado = MessageBoxEx.Show("�Desea eliminar las seleccionada?", Application.ProductName + "-" + Application.ProductVersion, MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                if (Resultado.ToString() != "Yes")
                {
                    return;
                }
                foreach (DataGridViewRow drv in dtgGeneral.SelectedRows)
                {
                    string ROW_ID = drv.Cells["ROW_ID"].Value.ToString();
                    cEMPRESA_CONEXION ocEMPRESA_CONEXION = (cEMPRESA_CONEXION)drv.Tag;
                    ocEMPRESA_CONEXION.eliminar(ocEMPRESA_CONEXION.ROW_ID);
                    dtgGeneral.Rows.Remove(drv);
                }
            }
            else
            {
                MessageBoxEx.Show("Seleccione alguna l�nea.", Application.ProductName + "-" + Application.ProductVersion, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }

        private void dtgGeneral_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            editar();
        }
        private void editar()
        {
            dtgGeneral.EndEdit();
            if (dtgGeneral.SelectedRows.Count > 0)
            {
                foreach (DataGridViewRow drv in dtgGeneral.SelectedRows)
                {
                    string ROW_ID = drv.Cells["ROW_ID"].Value.ToString();
                    frmEmpresa_Conexion ofrm = new frmEmpresa_Conexion(ROW_ID, oObjeto.ROW_ID);
                    ofrm.ShowDialog();
                    cargar_conexiones();
                }
            }
            else
            {
                MessageBoxEx.Show("Seleccione alguna l�nea.", Application.ProductName + "-" + Application.ProductVersion, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }
        private void editar_descripcion()
        {
            this.dtgDescripciones.EndEdit();
            if (dtgDescripciones.SelectedRows.Count > 0)
            {
                foreach (DataGridViewRow drv in dtgDescripciones.SelectedRows)
                {
                    string ROW_ID = drv.Cells["ROW_ID"].Value.ToString();
                    frmEmpresa_Descripcion ofrm = new frmEmpresa_Descripcion(ROW_ID, oObjeto.ROW_ID);
                    ofrm.ShowDialog();
                    cargar_descripciones();
                }
            }
            else
            {
                MessageBoxEx.Show("Seleccione alguna l�nea.", Application.ProductName + "-" + Application.ProductVersion, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }
        private void buttonX7_Click(object sender, EventArgs e)
        {
            OpenFileDialog openFileDialog1 = new OpenFileDialog();
            openFileDialog1.Filter = "key files (*.key)|*.key|All files (*.*)|*.*";
            openFileDialog1.FilterIndex = 1;
            openFileDialog1.RestoreDirectory = true;
            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                try
                {
                    LLAVE.Text = openFileDialog1.FileName;
                }
                catch (Exception)
                {
                    MessageBox.Show("No existe el archivo seleccionado", Application.ProductName + "-" + Application.ProductVersion, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                }
            }
        }

        private void buttonX4_Click_1(object sender, EventArgs e)
        {
            OpenFileDialog openFileDialog1 = new OpenFileDialog();

            //            openFileDialog1.InitialDirectory = "c:\\";
            openFileDialog1.Filter = "cer files (*.cer)|*.cer|All files (*.*)|*.*";
            openFileDialog1.FilterIndex = 1;
            openFileDialog1.RestoreDirectory = true;

            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                try
                {
                    CERTIFICADO.Text = openFileDialog1.FileName;
                }
                catch (Exception)
                {
                    MessageBox.Show("No existe el Archivo seleccionado", Application.ProductName + "-" + Application.ProductVersion, MessageBoxButtons.OK);
                }
            }
        }

        private void buttonX9_Click(object sender, EventArgs e)
        {
            cargar_descripciones();
        }

        private void cargar_descripciones()
        {
            if (oObjeto.ROW_ID != "")
            {
                dtgDescripciones.Rows.Clear();
                cEMPRESA_DESCRIPCION ocEMPRESA_DESCRIPCION = new cEMPRESA_DESCRIPCION();
                foreach (cEMPRESA_DESCRIPCION registro in ocEMPRESA_DESCRIPCION.todos(oObjeto.ROW_ID))
                {
                    int n = dtgDescripciones.Rows.Add();
                    actualizar_linea_descripciones(registro, n);
                }
            }
        }

        private void actualizar_linea_descripciones(cEMPRESA_DESCRIPCION registro, int n)
        {
            for (int i = 0; i < dtgDescripciones.Columns.Count; i++)
            {
                dtgDescripciones.Rows[n].Cells[i].Value = "";
            }
            dtgDescripciones.Rows[n].Tag = registro;
            dtgDescripciones.Rows[n].Cells["ROW_ID_tr"].Value = registro.ROW_ID;
            dtgDescripciones.Rows[n].Cells["VISUAL_DESCRIPCION"].Value = registro.VISUAL_DESCRIPCION;
            dtgDescripciones.Rows[n].Cells["DESCRIPCION"].Value = registro.DESCRIPCION;
        }

        private void buttonX10_Click(object sender, EventArgs e)
        {
            nueva_descripcion();
        }

        private void nueva_descripcion()
        {
            if (oObjeto.ROW_ID == "")
            {
                MessageBoxEx.Show("Guarde primero.", Application.ProductName + "-" + Application.ProductVersion, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return;
            }
            frmEmpresa_Descripcion ofrmEmpresa_Descripcion = new frmEmpresa_Descripcion(oObjeto.ROW_ID);
            ofrmEmpresa_Descripcion.ShowDialog();
            cargar_descripciones();
        }

        private void buttonX12_Click(object sender, EventArgs e)
        {
            cargar_moneda();
        }

        private void cargar_moneda()
        {
            if (oObjeto.ROW_ID != "")
            {
                dtgMonedas.Rows.Clear();
                cEMPRESA_MONEDA ocEMPRESA_MONEDA = new cEMPRESA_MONEDA();
                foreach (cEMPRESA_MONEDA registro in ocEMPRESA_MONEDA.todos(oObjeto.ROW_ID))
                {
                    int n = dtgMonedas.Rows.Add();
                    actualizar_linea_moneda(registro, n);
                }
            }
        }

        private void actualizar_linea_moneda(cEMPRESA_MONEDA registro, int n)
        {
            for (int i = 0; i < dtgMonedas.Columns.Count; i++)
            {
                dtgMonedas.Rows[n].Cells[i].Value = "";
            }
            dtgMonedas.Rows[n].Tag = registro;
            dtgMonedas.Rows[n].Cells["ROW_ID_mnd"].Value = registro.ROW_ID;
            dtgMonedas.Rows[n].Cells["VISUAL_MONEDA"].Value = registro.VISUAL_MONEDA;
            dtgMonedas.Rows[n].Cells["MONEDA"].Value = registro.MONEDA;
        }

        private void buttonX13_Click(object sender, EventArgs e)
        {
            nueva_moneda();
        }

        private void nueva_moneda()
        {
            if (oObjeto.ROW_ID == "")
            {
                MessageBoxEx.Show("Guarde primero.", Application.ProductName + "-" + Application.ProductVersion, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return;
            }
            frmEmpresa_Moneda ofrmEmpresa_Moneda = new frmEmpresa_Moneda(oObjeto.ROW_ID);
            ofrmEmpresa_Moneda.ShowDialog();
            cargar_moneda();
        }

        private void buttonX8_Click(object sender, EventArgs e)
        {
            eliminar_descripcion();
        }

        private void eliminar_descripcion()
        {
            dtgDescripciones.EndEdit();
            if (dtgDescripciones.SelectedCells.Count > 0)
            {
                DialogResult Resultado = MessageBoxEx.Show("�Desea eliminar las seleccionada?", Application.ProductName + "-" + Application.ProductVersion, MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                if (Resultado.ToString() != "Yes")
                {
                    return;
                }
                foreach (DataGridViewRow drv in dtgDescripciones.SelectedRows)
                {
                    string ROW_ID = drv.Cells["ROW_ID_tr"].Value.ToString();
                    cEMPRESA_DESCRIPCION ocEMPRESA_DESCRIPCION = (cEMPRESA_DESCRIPCION)drv.Tag;
                    ocEMPRESA_DESCRIPCION.eliminar(ocEMPRESA_DESCRIPCION.ROW_ID);
                    dtgDescripciones.Rows.Remove(drv);
                }
            }
            else
            {
                MessageBoxEx.Show("Seleccione alguna l�nea.", Application.ProductName + "-" + Application.ProductVersion, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }

        private void eliminar_moneda()
        {
            dtgMonedas.EndEdit();
            if (dtgMonedas.SelectedCells.Count > 0)
            {
                DialogResult Resultado = MessageBoxEx.Show("�Desea eliminar las seleccionada?", Application.ProductName + "-" + Application.ProductVersion, MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                if (Resultado.ToString() != "Yes")
                {
                    return;
                }
                foreach (DataGridViewRow drv in dtgMonedas.SelectedRows)
                {
                    string ROW_ID = drv.Cells["ROW_ID_mnd"].Value.ToString();
                    cEMPRESA_MONEDA ocEMPRESA_MONEDA = (cEMPRESA_MONEDA)drv.Tag;
                    ocEMPRESA_MONEDA.eliminar(ocEMPRESA_MONEDA.ROW_ID);
                    dtgMonedas.Rows.Remove(drv);
                }
            }
            else
            {
                MessageBoxEx.Show("Seleccione alguna l�nea.", Application.ProductName + "-" + Application.ProductVersion, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }

        private void buttonX11_Click(object sender, EventArgs e)
        {
            eliminar_moneda();
        }

    }
}