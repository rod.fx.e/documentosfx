namespace Desarrollo
{
    partial class frmTransaccion
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            DevComponents.DotNetBar.ButtonX buttonX3;
            DevComponents.DotNetBar.ButtonX buttonX1;
            DevComponents.DotNetBar.ButtonX buttonX2;
            DevComponents.DotNetBar.ButtonX buttonX9;
            DevComponents.DotNetBar.ButtonX buttonX11;
            DevComponents.DotNetBar.ButtonX buttonX14;
            DevComponents.DotNetBar.ButtonX buttonX21;
            DevComponents.DotNetBar.ButtonX buttonX4;
            DevComponents.DotNetBar.ButtonX buttonX6;
            DevComponents.DotNetBar.ButtonX buttonX16;
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle7 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle8 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle9 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmTransaccion));
            this.buttonItem2 = new DevComponents.DotNetBar.ButtonItem();
            this.buttonItem4 = new DevComponents.DotNetBar.ButtonItem();
            this.buttonItem1 = new DevComponents.DotNetBar.ButtonItem();
            this.metroStatusBar1 = new DevComponents.DotNetBar.Metro.MetroStatusBar();
            this.labelItem1 = new DevComponents.DotNetBar.LabelItem();
            this.numCtaField = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.labelX1 = new DevComponents.DotNetBar.LabelX();
            this.conceptoField = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.labelX16 = new DevComponents.DotNetBar.LabelX();
            this.labelX18 = new DevComponents.DotNetBar.LabelX();
            this.monedaField = new DevComponents.DotNetBar.Controls.ComboBoxEx();
            this.comboItem1 = new DevComponents.Editors.ComboItem();
            this.A = new DevComponents.Editors.ComboItem();
            this.labelX5 = new DevComponents.DotNetBar.LabelX();
            this.haberField = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.labelX4 = new DevComponents.DotNetBar.LabelX();
            this.debeField = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.superTabControl1 = new DevComponents.DotNetBar.SuperTabControl();
            this.superTabControlPanel2 = new DevComponents.DotNetBar.SuperTabControlPanel();
            this.circularProgress2 = new DevComponents.DotNetBar.Controls.CircularProgress();
            this.buttonX10 = new DevComponents.DotNetBar.ButtonX();
            this.buttonX12 = new DevComponents.DotNetBar.ButtonX();
            this.buttonX13 = new DevComponents.DotNetBar.ButtonX();
            this.dtgCheque = new DevComponents.DotNetBar.Controls.DataGridViewX();
            this.ROW_ID_b = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.numCtaField_c = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.bancoField = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ctaOriField = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.fechaField = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.montoField = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.benefField = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.rFCField = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.superTabItem2 = new DevComponents.DotNetBar.SuperTabItem();
            this.superTabControlPanel4 = new DevComponents.DotNetBar.SuperTabControlPanel();
            this.circularProgress1 = new DevComponents.DotNetBar.Controls.CircularProgress();
            this.buttonX5 = new DevComponents.DotNetBar.ButtonX();
            this.buttonX7 = new DevComponents.DotNetBar.ButtonX();
            this.buttonX8 = new DevComponents.DotNetBar.ButtonX();
            this.dtgComprobantes = new DevComponents.DotNetBar.Controls.DataGridViewX();
            this.ROW_ID_c = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.uUID_CFDIField = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.rFCField_c = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.superTabItem3 = new DevComponents.DotNetBar.SuperTabItem();
            this.superTabControlPanel3 = new DevComponents.DotNetBar.SuperTabControlPanel();
            this.circularProgress4 = new DevComponents.DotNetBar.Controls.CircularProgress();
            this.buttonX20 = new DevComponents.DotNetBar.ButtonX();
            this.buttonX22 = new DevComponents.DotNetBar.ButtonX();
            this.buttonX23 = new DevComponents.DotNetBar.ButtonX();
            this.dtgTranferencia = new DevComponents.DotNetBar.Controls.DataGridViewX();
            this.ROW_ID_t = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ctaOriField_t = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.bancoOriField_t = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.montoField_t = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ctaDestField_t = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.bancoDestField_t = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.fechaField_t = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.benefField_t = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.rFCField_t = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Polizas = new DevComponents.DotNetBar.SuperTabItem();
            this.ASIGNADO = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.labelX2 = new DevComponents.DotNetBar.LabelX();
            this.folderBrowserDialog1 = new System.Windows.Forms.FolderBrowserDialog();
            buttonX3 = new DevComponents.DotNetBar.ButtonX();
            buttonX1 = new DevComponents.DotNetBar.ButtonX();
            buttonX2 = new DevComponents.DotNetBar.ButtonX();
            buttonX9 = new DevComponents.DotNetBar.ButtonX();
            buttonX11 = new DevComponents.DotNetBar.ButtonX();
            buttonX14 = new DevComponents.DotNetBar.ButtonX();
            buttonX21 = new DevComponents.DotNetBar.ButtonX();
            buttonX4 = new DevComponents.DotNetBar.ButtonX();
            buttonX6 = new DevComponents.DotNetBar.ButtonX();
            buttonX16 = new DevComponents.DotNetBar.ButtonX();
            ((System.ComponentModel.ISupportInitialize)(this.superTabControl1)).BeginInit();
            this.superTabControl1.SuspendLayout();
            this.superTabControlPanel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dtgCheque)).BeginInit();
            this.superTabControlPanel4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dtgComprobantes)).BeginInit();
            this.superTabControlPanel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dtgTranferencia)).BeginInit();
            this.SuspendLayout();
            // 
            // buttonX3
            // 
            buttonX3.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            buttonX3.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            buttonX3.Image = global::Desarrollo.Properties.Resources.Eliminar_18;
            buttonX3.Location = new System.Drawing.Point(141, 0);
            buttonX3.Name = "buttonX3";
            buttonX3.Size = new System.Drawing.Size(68, 24);
            buttonX3.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            buttonX3.TabIndex = 1;
            buttonX3.Text = "Eliminar";
            buttonX3.TextAlignment = DevComponents.DotNetBar.eButtonTextAlignment.Right;
            buttonX3.Click += new System.EventHandler(this.buttonX3_Click);
            // 
            // buttonX1
            // 
            buttonX1.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            buttonX1.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            buttonX1.Image = global::Desarrollo.Properties.Resources.Nueno_18;
            buttonX1.Location = new System.Drawing.Point(67, 0);
            buttonX1.Name = "buttonX1";
            buttonX1.Size = new System.Drawing.Size(68, 24);
            buttonX1.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            buttonX1.TabIndex = 1;
            buttonX1.Text = "Guardar";
            buttonX1.TextAlignment = DevComponents.DotNetBar.eButtonTextAlignment.Right;
            buttonX1.Click += new System.EventHandler(this.buttonX1_Click);
            // 
            // buttonX2
            // 
            buttonX2.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            buttonX2.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            buttonX2.Image = global::Desarrollo.Properties.Resources.Nuevo_18_19;
            buttonX2.Location = new System.Drawing.Point(0, 0);
            buttonX2.Name = "buttonX2";
            buttonX2.Size = new System.Drawing.Size(61, 24);
            buttonX2.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            buttonX2.TabIndex = 1;
            buttonX2.Text = "Nuevo";
            buttonX2.TextAlignment = DevComponents.DotNetBar.eButtonTextAlignment.Right;
            buttonX2.Click += new System.EventHandler(this.buttonX2_Click);
            // 
            // buttonX9
            // 
            buttonX9.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            buttonX9.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            buttonX9.Image = global::Desarrollo.Properties.Resources.excel;
            buttonX9.Location = new System.Drawing.Point(241, 3);
            buttonX9.Name = "buttonX9";
            buttonX9.Size = new System.Drawing.Size(114, 24);
            buttonX9.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            buttonX9.SubItems.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.buttonItem2});
            buttonX9.TabIndex = 574;
            buttonX9.Text = "Importar: Excel";
            buttonX9.TextAlignment = DevComponents.DotNetBar.eButtonTextAlignment.Right;
            buttonX9.Click += new System.EventHandler(this.buttonX9_Click);
            // 
            // buttonItem2
            // 
            this.buttonItem2.GlobalItem = false;
            this.buttonItem2.Image = global::Desarrollo.Properties.Resources.wizard;
            this.buttonItem2.Name = "buttonItem2";
            this.buttonItem2.Text = "Generar Plantilla";
            this.buttonItem2.Click += new System.EventHandler(this.buttonItem2_Click);
            // 
            // buttonX11
            // 
            buttonX11.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            buttonX11.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            buttonX11.Image = global::Desarrollo.Properties.Resources.Eliminar_18;
            buttonX11.Location = new System.Drawing.Point(167, 3);
            buttonX11.Name = "buttonX11";
            buttonX11.Size = new System.Drawing.Size(68, 24);
            buttonX11.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            buttonX11.TabIndex = 576;
            buttonX11.Text = "Eliminar";
            buttonX11.TextAlignment = DevComponents.DotNetBar.eButtonTextAlignment.Right;
            buttonX11.Click += new System.EventHandler(this.buttonX11_Click);
            // 
            // buttonX14
            // 
            buttonX14.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            buttonX14.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            buttonX14.Image = global::Desarrollo.Properties.Resources.excel;
            buttonX14.Location = new System.Drawing.Point(241, 3);
            buttonX14.Name = "buttonX14";
            buttonX14.Size = new System.Drawing.Size(114, 24);
            buttonX14.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            buttonX14.SubItems.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.buttonItem4});
            buttonX14.TabIndex = 581;
            buttonX14.Text = "Importar: Excel";
            buttonX14.TextAlignment = DevComponents.DotNetBar.eButtonTextAlignment.Right;
            buttonX14.Click += new System.EventHandler(this.buttonX14_Click);
            // 
            // buttonItem4
            // 
            this.buttonItem4.GlobalItem = false;
            this.buttonItem4.Image = global::Desarrollo.Properties.Resources.wizard;
            this.buttonItem4.Name = "buttonItem4";
            this.buttonItem4.Text = "Generar Plantilla";
            this.buttonItem4.Click += new System.EventHandler(this.buttonItem4_Click);
            // 
            // buttonX21
            // 
            buttonX21.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            buttonX21.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            buttonX21.Image = global::Desarrollo.Properties.Resources.Eliminar_18;
            buttonX21.Location = new System.Drawing.Point(167, 3);
            buttonX21.Name = "buttonX21";
            buttonX21.Size = new System.Drawing.Size(68, 24);
            buttonX21.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            buttonX21.TabIndex = 583;
            buttonX21.Text = "Eliminar";
            buttonX21.TextAlignment = DevComponents.DotNetBar.eButtonTextAlignment.Right;
            buttonX21.Click += new System.EventHandler(this.buttonX21_Click);
            // 
            // buttonX4
            // 
            buttonX4.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            buttonX4.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            buttonX4.Image = global::Desarrollo.Properties.Resources.excel;
            buttonX4.Location = new System.Drawing.Point(241, 3);
            buttonX4.Name = "buttonX4";
            buttonX4.Size = new System.Drawing.Size(114, 24);
            buttonX4.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            buttonX4.SubItems.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.buttonItem1});
            buttonX4.TabIndex = 581;
            buttonX4.Text = "Importar: Excel";
            buttonX4.TextAlignment = DevComponents.DotNetBar.eButtonTextAlignment.Right;
            buttonX4.Visible = false;
            buttonX4.Click += new System.EventHandler(this.buttonX4_Click);
            // 
            // buttonItem1
            // 
            this.buttonItem1.GlobalItem = false;
            this.buttonItem1.Image = global::Desarrollo.Properties.Resources.wizard;
            this.buttonItem1.Name = "buttonItem1";
            this.buttonItem1.Text = "Generar Plantilla";
            this.buttonItem1.Click += new System.EventHandler(this.buttonItem1_Click);
            // 
            // buttonX6
            // 
            buttonX6.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            buttonX6.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            buttonX6.Image = global::Desarrollo.Properties.Resources.Eliminar_18;
            buttonX6.Location = new System.Drawing.Point(167, 3);
            buttonX6.Name = "buttonX6";
            buttonX6.Size = new System.Drawing.Size(68, 24);
            buttonX6.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            buttonX6.TabIndex = 583;
            buttonX6.Text = "Eliminar";
            buttonX6.TextAlignment = DevComponents.DotNetBar.eButtonTextAlignment.Right;
            buttonX6.Click += new System.EventHandler(this.buttonX6_Click);
            // 
            // buttonX16
            // 
            buttonX16.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            buttonX16.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            buttonX16.Image = global::Desarrollo.Properties.Resources.envio_mail_18;
            buttonX16.Location = new System.Drawing.Point(501, 3);
            buttonX16.Name = "buttonX16";
            buttonX16.Size = new System.Drawing.Size(135, 24);
            buttonX16.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            buttonX16.TabIndex = 585;
            buttonX16.Text = "Buscar Comprobantes";
            buttonX16.TextAlignment = DevComponents.DotNetBar.eButtonTextAlignment.Right;
            buttonX16.Click += new System.EventHandler(this.buttonX16_Click);
            // 
            // metroStatusBar1
            // 
            this.metroStatusBar1.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.metroStatusBar1.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.metroStatusBar1.ContainerControlProcessDialogKey = true;
            this.metroStatusBar1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.metroStatusBar1.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.metroStatusBar1.ForeColor = System.Drawing.Color.Black;
            this.metroStatusBar1.Items.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.labelItem1});
            this.metroStatusBar1.Location = new System.Drawing.Point(0, 367);
            this.metroStatusBar1.Name = "metroStatusBar1";
            this.metroStatusBar1.Size = new System.Drawing.Size(728, 21);
            this.metroStatusBar1.TabIndex = 8;
            // 
            // labelItem1
            // 
            this.labelItem1.Name = "labelItem1";
            // 
            // numCtaField
            // 
            this.numCtaField.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.numCtaField.Border.Class = "TextBoxBorder";
            this.numCtaField.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.numCtaField.ForeColor = System.Drawing.Color.Black;
            this.numCtaField.Location = new System.Drawing.Point(67, 32);
            this.numCtaField.Name = "numCtaField";
            this.numCtaField.Size = new System.Drawing.Size(290, 22);
            this.numCtaField.TabIndex = 548;
            this.numCtaField.TabStop = false;
            this.numCtaField.WatermarkBehavior = DevComponents.DotNetBar.eWatermarkBehavior.HideNonEmpty;
            // 
            // labelX1
            // 
            this.labelX1.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.labelX1.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX1.ForeColor = System.Drawing.Color.Black;
            this.labelX1.Location = new System.Drawing.Point(0, 30);
            this.labelX1.Name = "labelX1";
            this.labelX1.Size = new System.Drawing.Size(61, 22);
            this.labelX1.TabIndex = 579;
            this.labelX1.Text = "Cuenta";
            this.labelX1.TextAlignment = System.Drawing.StringAlignment.Far;
            // 
            // conceptoField
            // 
            this.conceptoField.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.conceptoField.Border.Class = "TextBoxBorder";
            this.conceptoField.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.conceptoField.ForeColor = System.Drawing.Color.Black;
            this.conceptoField.Location = new System.Drawing.Point(67, 60);
            this.conceptoField.Name = "conceptoField";
            this.conceptoField.Size = new System.Drawing.Size(657, 22);
            this.conceptoField.TabIndex = 548;
            this.conceptoField.TabStop = false;
            this.conceptoField.WatermarkBehavior = DevComponents.DotNetBar.eWatermarkBehavior.HideNonEmpty;
            // 
            // labelX16
            // 
            this.labelX16.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.labelX16.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX16.ForeColor = System.Drawing.Color.Black;
            this.labelX16.Location = new System.Drawing.Point(0, 58);
            this.labelX16.Name = "labelX16";
            this.labelX16.Size = new System.Drawing.Size(61, 22);
            this.labelX16.TabIndex = 579;
            this.labelX16.Text = "Concepto";
            this.labelX16.TextAlignment = System.Drawing.StringAlignment.Far;
            // 
            // labelX18
            // 
            this.labelX18.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.labelX18.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX18.ForeColor = System.Drawing.Color.Black;
            this.labelX18.Location = new System.Drawing.Point(360, 30);
            this.labelX18.Name = "labelX18";
            this.labelX18.Size = new System.Drawing.Size(60, 22);
            this.labelX18.TabIndex = 583;
            this.labelX18.Text = "Moneda";
            this.labelX18.TextAlignment = System.Drawing.StringAlignment.Far;
            // 
            // monedaField
            // 
            this.monedaField.DisplayMember = "Text";
            this.monedaField.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.monedaField.ForeColor = System.Drawing.Color.Black;
            this.monedaField.FormattingEnabled = true;
            this.monedaField.ItemHeight = 16;
            this.monedaField.Items.AddRange(new object[] {
            this.comboItem1,
            this.A});
            this.monedaField.Location = new System.Drawing.Point(425, 32);
            this.monedaField.Name = "monedaField";
            this.monedaField.Size = new System.Drawing.Size(70, 22);
            this.monedaField.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.monedaField.TabIndex = 585;
            // 
            // comboItem1
            // 
            this.comboItem1.Text = "MXN";
            this.comboItem1.Value = "D";
            // 
            // A
            // 
            this.A.Text = "USD";
            this.A.Value = "A";
            // 
            // labelX5
            // 
            this.labelX5.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.labelX5.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX5.ForeColor = System.Drawing.Color.Black;
            this.labelX5.Location = new System.Drawing.Point(215, 86);
            this.labelX5.Name = "labelX5";
            this.labelX5.Size = new System.Drawing.Size(39, 22);
            this.labelX5.TabIndex = 588;
            this.labelX5.Text = "Haber";
            this.labelX5.TextAlignment = System.Drawing.StringAlignment.Far;
            // 
            // haberField
            // 
            this.haberField.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.haberField.Border.Class = "TextBoxBorder";
            this.haberField.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.haberField.ForeColor = System.Drawing.Color.Black;
            this.haberField.Location = new System.Drawing.Point(260, 88);
            this.haberField.Name = "haberField";
            this.haberField.Size = new System.Drawing.Size(143, 22);
            this.haberField.TabIndex = 589;
            this.haberField.TabStop = false;
            this.haberField.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.haberField.WatermarkBehavior = DevComponents.DotNetBar.eWatermarkBehavior.HideNonEmpty;
            // 
            // labelX4
            // 
            this.labelX4.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.labelX4.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX4.ForeColor = System.Drawing.Color.Black;
            this.labelX4.Location = new System.Drawing.Point(-4, 86);
            this.labelX4.Name = "labelX4";
            this.labelX4.Size = new System.Drawing.Size(61, 22);
            this.labelX4.TabIndex = 586;
            this.labelX4.Text = "Debe";
            this.labelX4.TextAlignment = System.Drawing.StringAlignment.Far;
            // 
            // debeField
            // 
            this.debeField.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.debeField.Border.Class = "TextBoxBorder";
            this.debeField.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.debeField.ForeColor = System.Drawing.Color.Black;
            this.debeField.Location = new System.Drawing.Point(66, 88);
            this.debeField.Name = "debeField";
            this.debeField.Size = new System.Drawing.Size(143, 22);
            this.debeField.TabIndex = 587;
            this.debeField.TabStop = false;
            this.debeField.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.debeField.WatermarkBehavior = DevComponents.DotNetBar.eWatermarkBehavior.HideNonEmpty;
            // 
            // superTabControl1
            // 
            this.superTabControl1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.superTabControl1.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            // 
            // 
            // 
            this.superTabControl1.ControlBox.CloseBox.Name = "";
            // 
            // 
            // 
            this.superTabControl1.ControlBox.MenuBox.Name = "";
            this.superTabControl1.ControlBox.Name = "";
            this.superTabControl1.ControlBox.SubItems.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.superTabControl1.ControlBox.MenuBox,
            this.superTabControl1.ControlBox.CloseBox});
            this.superTabControl1.Controls.Add(this.superTabControlPanel4);
            this.superTabControl1.Controls.Add(this.superTabControlPanel3);
            this.superTabControl1.Controls.Add(this.superTabControlPanel2);
            this.superTabControl1.ForeColor = System.Drawing.Color.Black;
            this.superTabControl1.Location = new System.Drawing.Point(0, 116);
            this.superTabControl1.Name = "superTabControl1";
            this.superTabControl1.ReorderTabsEnabled = true;
            this.superTabControl1.SelectedTabFont = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Bold);
            this.superTabControl1.SelectedTabIndex = 0;
            this.superTabControl1.Size = new System.Drawing.Size(724, 245);
            this.superTabControl1.TabFont = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.superTabControl1.TabIndex = 590;
            this.superTabControl1.Tabs.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.superTabItem2,
            this.Polizas,
            this.superTabItem3});
            this.superTabControl1.TabStyle = DevComponents.DotNetBar.eSuperTabStyle.Office2010BackstageBlue;
            this.superTabControl1.Text = "superTabControl1";
            // 
            // superTabControlPanel2
            // 
            this.superTabControlPanel2.Controls.Add(this.circularProgress2);
            this.superTabControlPanel2.Controls.Add(buttonX9);
            this.superTabControlPanel2.Controls.Add(this.buttonX10);
            this.superTabControlPanel2.Controls.Add(buttonX11);
            this.superTabControlPanel2.Controls.Add(this.buttonX12);
            this.superTabControlPanel2.Controls.Add(this.buttonX13);
            this.superTabControlPanel2.Controls.Add(this.dtgCheque);
            this.superTabControlPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.superTabControlPanel2.Location = new System.Drawing.Point(0, 25);
            this.superTabControlPanel2.Name = "superTabControlPanel2";
            this.superTabControlPanel2.Size = new System.Drawing.Size(724, 220);
            this.superTabControlPanel2.TabIndex = 0;
            this.superTabControlPanel2.TabItem = this.superTabItem2;
            // 
            // circularProgress2
            // 
            this.circularProgress2.AnimationSpeed = 50;
            this.circularProgress2.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.circularProgress2.BackgroundStyle.BackgroundImageAlpha = ((byte)(64));
            this.circularProgress2.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.circularProgress2.FocusCuesEnabled = false;
            this.circularProgress2.Font = new System.Drawing.Font("Segoe UI", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.circularProgress2.Location = new System.Drawing.Point(470, 3);
            this.circularProgress2.Name = "circularProgress2";
            this.circularProgress2.ProgressBarType = DevComponents.DotNetBar.eCircularProgressType.Donut;
            this.circularProgress2.ProgressColor = System.Drawing.Color.Orange;
            this.circularProgress2.ProgressTextFormat = "{0}";
            this.circularProgress2.Size = new System.Drawing.Size(25, 24);
            this.circularProgress2.Style = DevComponents.DotNetBar.eDotNetBarStyle.OfficeXP;
            this.circularProgress2.TabIndex = 577;
            this.circularProgress2.Value = 100;
            this.circularProgress2.Visible = false;
            // 
            // buttonX10
            // 
            this.buttonX10.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.buttonX10.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.buttonX10.Image = global::Desarrollo.Properties.Resources.visual;
            this.buttonX10.Location = new System.Drawing.Point(361, 3);
            this.buttonX10.Name = "buttonX10";
            this.buttonX10.Size = new System.Drawing.Size(106, 24);
            this.buttonX10.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.buttonX10.TabIndex = 575;
            this.buttonX10.Text = "Importar: Visual";
            // 
            // buttonX12
            // 
            this.buttonX12.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.buttonX12.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.buttonX12.Image = global::Desarrollo.Properties.Resources.actualizar_18;
            this.buttonX12.Location = new System.Drawing.Point(3, 3);
            this.buttonX12.Name = "buttonX12";
            this.buttonX12.Size = new System.Drawing.Size(78, 24);
            this.buttonX12.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.buttonX12.TabIndex = 572;
            this.buttonX12.Text = "Refrescar";
            this.buttonX12.Click += new System.EventHandler(this.buttonX12_Click);
            // 
            // buttonX13
            // 
            this.buttonX13.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.buttonX13.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.buttonX13.Image = global::Desarrollo.Properties.Resources.Nueno_18;
            this.buttonX13.Location = new System.Drawing.Point(87, 3);
            this.buttonX13.Name = "buttonX13";
            this.buttonX13.Size = new System.Drawing.Size(74, 24);
            this.buttonX13.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.buttonX13.TabIndex = 573;
            this.buttonX13.Text = "Agregar";
            this.buttonX13.Click += new System.EventHandler(this.buttonX13_Click);
            // 
            // dtgCheque
            // 
            this.dtgCheque.AllowUserToAddRows = false;
            this.dtgCheque.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dtgCheque.BackgroundColor = System.Drawing.Color.White;
            dataGridViewCellStyle7.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle7.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle7.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle7.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle7.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle7.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle7.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dtgCheque.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle7;
            this.dtgCheque.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dtgCheque.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.ROW_ID_b,
            this.numCtaField_c,
            this.bancoField,
            this.ctaOriField,
            this.fechaField,
            this.montoField,
            this.benefField,
            this.rFCField});
            dataGridViewCellStyle8.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle8.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle8.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle8.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle8.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle8.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle8.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dtgCheque.DefaultCellStyle = dataGridViewCellStyle8;
            this.dtgCheque.EnableHeadersVisualStyles = false;
            this.dtgCheque.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(170)))), ((int)(((byte)(170)))), ((int)(((byte)(170)))));
            this.dtgCheque.Location = new System.Drawing.Point(3, 33);
            this.dtgCheque.Name = "dtgCheque";
            dataGridViewCellStyle9.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle9.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle9.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle9.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle9.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle9.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle9.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dtgCheque.RowHeadersDefaultCellStyle = dataGridViewCellStyle9;
            this.dtgCheque.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dtgCheque.Size = new System.Drawing.Size(718, 184);
            this.dtgCheque.TabIndex = 571;
            this.dtgCheque.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dtgCheque_CellDoubleClick);
            // 
            // ROW_ID_b
            // 
            this.ROW_ID_b.HeaderText = "ROW_ID";
            this.ROW_ID_b.Name = "ROW_ID_b";
            this.ROW_ID_b.Visible = false;
            // 
            // numCtaField_c
            // 
            this.numCtaField_c.HeaderText = "Cuenta";
            this.numCtaField_c.Name = "numCtaField_c";
            this.numCtaField_c.ReadOnly = true;
            // 
            // bancoField
            // 
            this.bancoField.HeaderText = "Banco";
            this.bancoField.Name = "bancoField";
            this.bancoField.ReadOnly = true;
            // 
            // ctaOriField
            // 
            this.ctaOriField.HeaderText = "Cuenta";
            this.ctaOriField.Name = "ctaOriField";
            this.ctaOriField.ReadOnly = true;
            // 
            // fechaField
            // 
            this.fechaField.HeaderText = "Fecha";
            this.fechaField.Name = "fechaField";
            this.fechaField.ReadOnly = true;
            // 
            // montoField
            // 
            this.montoField.HeaderText = "Monto";
            this.montoField.Name = "montoField";
            this.montoField.ReadOnly = true;
            // 
            // benefField
            // 
            this.benefField.HeaderText = "Beneficiario";
            this.benefField.Name = "benefField";
            this.benefField.ReadOnly = true;
            // 
            // rFCField
            // 
            this.rFCField.HeaderText = "RFC";
            this.rFCField.Name = "rFCField";
            this.rFCField.ReadOnly = true;
            // 
            // superTabItem2
            // 
            this.superTabItem2.AttachedControl = this.superTabControlPanel2;
            this.superTabItem2.GlobalItem = false;
            this.superTabItem2.Name = "superTabItem2";
            this.superTabItem2.Text = "Cheques";
            // 
            // superTabControlPanel4
            // 
            this.superTabControlPanel4.Controls.Add(buttonX16);
            this.superTabControlPanel4.Controls.Add(this.circularProgress1);
            this.superTabControlPanel4.Controls.Add(buttonX4);
            this.superTabControlPanel4.Controls.Add(this.buttonX5);
            this.superTabControlPanel4.Controls.Add(buttonX6);
            this.superTabControlPanel4.Controls.Add(this.buttonX7);
            this.superTabControlPanel4.Controls.Add(this.buttonX8);
            this.superTabControlPanel4.Controls.Add(this.dtgComprobantes);
            this.superTabControlPanel4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.superTabControlPanel4.Location = new System.Drawing.Point(0, 25);
            this.superTabControlPanel4.Name = "superTabControlPanel4";
            this.superTabControlPanel4.Size = new System.Drawing.Size(724, 220);
            this.superTabControlPanel4.TabIndex = 0;
            this.superTabControlPanel4.TabItem = this.superTabItem3;
            // 
            // circularProgress1
            // 
            this.circularProgress1.AnimationSpeed = 50;
            this.circularProgress1.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.circularProgress1.BackgroundStyle.BackgroundImageAlpha = ((byte)(64));
            this.circularProgress1.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.circularProgress1.FocusCuesEnabled = false;
            this.circularProgress1.Font = new System.Drawing.Font("Segoe UI", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.circularProgress1.Location = new System.Drawing.Point(470, 3);
            this.circularProgress1.Name = "circularProgress1";
            this.circularProgress1.ProgressBarType = DevComponents.DotNetBar.eCircularProgressType.Donut;
            this.circularProgress1.ProgressColor = System.Drawing.Color.Orange;
            this.circularProgress1.ProgressTextFormat = "{0}";
            this.circularProgress1.Size = new System.Drawing.Size(25, 24);
            this.circularProgress1.Style = DevComponents.DotNetBar.eDotNetBarStyle.OfficeXP;
            this.circularProgress1.TabIndex = 584;
            this.circularProgress1.Value = 100;
            this.circularProgress1.Visible = false;
            // 
            // buttonX5
            // 
            this.buttonX5.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.buttonX5.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.buttonX5.Image = global::Desarrollo.Properties.Resources.visual;
            this.buttonX5.Location = new System.Drawing.Point(361, 3);
            this.buttonX5.Name = "buttonX5";
            this.buttonX5.Size = new System.Drawing.Size(106, 24);
            this.buttonX5.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.buttonX5.TabIndex = 582;
            this.buttonX5.Text = "Importar: Visual";
            this.buttonX5.Visible = false;
            // 
            // buttonX7
            // 
            this.buttonX7.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.buttonX7.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.buttonX7.Image = global::Desarrollo.Properties.Resources.actualizar_18;
            this.buttonX7.Location = new System.Drawing.Point(3, 3);
            this.buttonX7.Name = "buttonX7";
            this.buttonX7.Size = new System.Drawing.Size(78, 24);
            this.buttonX7.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.buttonX7.TabIndex = 579;
            this.buttonX7.Text = "Refrescar";
            this.buttonX7.Click += new System.EventHandler(this.buttonX7_Click);
            // 
            // buttonX8
            // 
            this.buttonX8.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.buttonX8.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.buttonX8.Image = global::Desarrollo.Properties.Resources.Nueno_18;
            this.buttonX8.Location = new System.Drawing.Point(87, 3);
            this.buttonX8.Name = "buttonX8";
            this.buttonX8.Size = new System.Drawing.Size(74, 24);
            this.buttonX8.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.buttonX8.TabIndex = 580;
            this.buttonX8.Text = "Agregar";
            this.buttonX8.Click += new System.EventHandler(this.buttonX8_Click);
            // 
            // dtgComprobantes
            // 
            this.dtgComprobantes.AllowUserToAddRows = false;
            this.dtgComprobantes.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dtgComprobantes.BackgroundColor = System.Drawing.Color.White;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dtgComprobantes.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.dtgComprobantes.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dtgComprobantes.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.ROW_ID_c,
            this.uUID_CFDIField,
            this.rFCField_c});
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dtgComprobantes.DefaultCellStyle = dataGridViewCellStyle2;
            this.dtgComprobantes.EnableHeadersVisualStyles = false;
            this.dtgComprobantes.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(170)))), ((int)(((byte)(170)))), ((int)(((byte)(170)))));
            this.dtgComprobantes.Location = new System.Drawing.Point(3, 33);
            this.dtgComprobantes.Name = "dtgComprobantes";
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle3.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dtgComprobantes.RowHeadersDefaultCellStyle = dataGridViewCellStyle3;
            this.dtgComprobantes.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dtgComprobantes.Size = new System.Drawing.Size(718, 184);
            this.dtgComprobantes.TabIndex = 578;
            this.dtgComprobantes.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dtgComprobantes_CellDoubleClick);
            // 
            // ROW_ID_c
            // 
            this.ROW_ID_c.HeaderText = "ROW_ID";
            this.ROW_ID_c.Name = "ROW_ID_c";
            this.ROW_ID_c.Visible = false;
            // 
            // uUID_CFDIField
            // 
            this.uUID_CFDIField.HeaderText = "UUID";
            this.uUID_CFDIField.Name = "uUID_CFDIField";
            this.uUID_CFDIField.ReadOnly = true;
            this.uUID_CFDIField.Width = 250;
            // 
            // rFCField_c
            // 
            this.rFCField_c.HeaderText = "RFC";
            this.rFCField_c.Name = "rFCField_c";
            this.rFCField_c.ReadOnly = true;
            this.rFCField_c.Width = 170;
            // 
            // superTabItem3
            // 
            this.superTabItem3.AttachedControl = this.superTabControlPanel4;
            this.superTabItem3.GlobalItem = false;
            this.superTabItem3.Name = "superTabItem3";
            this.superTabItem3.Text = "Comprobantes";
            // 
            // superTabControlPanel3
            // 
            this.superTabControlPanel3.Controls.Add(this.circularProgress4);
            this.superTabControlPanel3.Controls.Add(buttonX14);
            this.superTabControlPanel3.Controls.Add(this.buttonX20);
            this.superTabControlPanel3.Controls.Add(buttonX21);
            this.superTabControlPanel3.Controls.Add(this.buttonX22);
            this.superTabControlPanel3.Controls.Add(this.buttonX23);
            this.superTabControlPanel3.Controls.Add(this.dtgTranferencia);
            this.superTabControlPanel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.superTabControlPanel3.Location = new System.Drawing.Point(0, 25);
            this.superTabControlPanel3.Name = "superTabControlPanel3";
            this.superTabControlPanel3.Size = new System.Drawing.Size(724, 220);
            this.superTabControlPanel3.TabIndex = 0;
            this.superTabControlPanel3.TabItem = this.Polizas;
            // 
            // circularProgress4
            // 
            this.circularProgress4.AnimationSpeed = 50;
            this.circularProgress4.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.circularProgress4.BackgroundStyle.BackgroundImageAlpha = ((byte)(64));
            this.circularProgress4.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.circularProgress4.FocusCuesEnabled = false;
            this.circularProgress4.Font = new System.Drawing.Font("Segoe UI", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.circularProgress4.Location = new System.Drawing.Point(470, 3);
            this.circularProgress4.Name = "circularProgress4";
            this.circularProgress4.ProgressBarType = DevComponents.DotNetBar.eCircularProgressType.Donut;
            this.circularProgress4.ProgressColor = System.Drawing.Color.Orange;
            this.circularProgress4.ProgressTextFormat = "{0}";
            this.circularProgress4.Size = new System.Drawing.Size(25, 24);
            this.circularProgress4.Style = DevComponents.DotNetBar.eDotNetBarStyle.OfficeXP;
            this.circularProgress4.TabIndex = 584;
            this.circularProgress4.Value = 100;
            this.circularProgress4.Visible = false;
            // 
            // buttonX20
            // 
            this.buttonX20.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.buttonX20.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.buttonX20.Image = global::Desarrollo.Properties.Resources.visual;
            this.buttonX20.Location = new System.Drawing.Point(361, 3);
            this.buttonX20.Name = "buttonX20";
            this.buttonX20.Size = new System.Drawing.Size(106, 24);
            this.buttonX20.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.buttonX20.TabIndex = 582;
            this.buttonX20.Text = "Importar: Visual";
            // 
            // buttonX22
            // 
            this.buttonX22.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.buttonX22.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.buttonX22.Image = global::Desarrollo.Properties.Resources.actualizar_18;
            this.buttonX22.Location = new System.Drawing.Point(3, 3);
            this.buttonX22.Name = "buttonX22";
            this.buttonX22.Size = new System.Drawing.Size(78, 24);
            this.buttonX22.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.buttonX22.TabIndex = 579;
            this.buttonX22.Text = "Refrescar";
            this.buttonX22.Click += new System.EventHandler(this.buttonX22_Click);
            // 
            // buttonX23
            // 
            this.buttonX23.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.buttonX23.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.buttonX23.Image = global::Desarrollo.Properties.Resources.Nueno_18;
            this.buttonX23.Location = new System.Drawing.Point(87, 3);
            this.buttonX23.Name = "buttonX23";
            this.buttonX23.Size = new System.Drawing.Size(74, 24);
            this.buttonX23.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.buttonX23.TabIndex = 580;
            this.buttonX23.Text = "Agregar";
            this.buttonX23.Click += new System.EventHandler(this.buttonX23_Click);
            // 
            // dtgTranferencia
            // 
            this.dtgTranferencia.AllowUserToAddRows = false;
            this.dtgTranferencia.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dtgTranferencia.BackgroundColor = System.Drawing.Color.White;
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle4.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle4.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle4.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle4.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle4.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dtgTranferencia.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle4;
            this.dtgTranferencia.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dtgTranferencia.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.ROW_ID_t,
            this.ctaOriField_t,
            this.bancoOriField_t,
            this.montoField_t,
            this.ctaDestField_t,
            this.bancoDestField_t,
            this.fechaField_t,
            this.benefField_t,
            this.rFCField_t});
            dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle5.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle5.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle5.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle5.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle5.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle5.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dtgTranferencia.DefaultCellStyle = dataGridViewCellStyle5;
            this.dtgTranferencia.EnableHeadersVisualStyles = false;
            this.dtgTranferencia.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(170)))), ((int)(((byte)(170)))), ((int)(((byte)(170)))));
            this.dtgTranferencia.Location = new System.Drawing.Point(3, 33);
            this.dtgTranferencia.Name = "dtgTranferencia";
            dataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle6.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle6.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle6.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle6.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle6.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle6.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dtgTranferencia.RowHeadersDefaultCellStyle = dataGridViewCellStyle6;
            this.dtgTranferencia.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dtgTranferencia.Size = new System.Drawing.Size(718, 184);
            this.dtgTranferencia.TabIndex = 578;
            this.dtgTranferencia.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dtgTranferencia_CellDoubleClick);
            // 
            // ROW_ID_t
            // 
            this.ROW_ID_t.HeaderText = "ROW_ID";
            this.ROW_ID_t.Name = "ROW_ID_t";
            this.ROW_ID_t.Visible = false;
            // 
            // ctaOriField_t
            // 
            this.ctaOriField_t.HeaderText = "Cuenta Origen";
            this.ctaOriField_t.Name = "ctaOriField_t";
            this.ctaOriField_t.ReadOnly = true;
            this.ctaOriField_t.Width = 150;
            // 
            // bancoOriField_t
            // 
            this.bancoOriField_t.HeaderText = "Banco Origen";
            this.bancoOriField_t.Name = "bancoOriField_t";
            this.bancoOriField_t.ReadOnly = true;
            this.bancoOriField_t.Width = 150;
            // 
            // montoField_t
            // 
            this.montoField_t.HeaderText = "Monto";
            this.montoField_t.Name = "montoField_t";
            this.montoField_t.ReadOnly = true;
            // 
            // ctaDestField_t
            // 
            this.ctaDestField_t.HeaderText = "Cuenta Destino";
            this.ctaDestField_t.Name = "ctaDestField_t";
            this.ctaDestField_t.ReadOnly = true;
            this.ctaDestField_t.Width = 150;
            // 
            // bancoDestField_t
            // 
            this.bancoDestField_t.HeaderText = "Banco Destino";
            this.bancoDestField_t.Name = "bancoDestField_t";
            this.bancoDestField_t.ReadOnly = true;
            this.bancoDestField_t.Width = 150;
            // 
            // fechaField_t
            // 
            this.fechaField_t.HeaderText = "Fecha";
            this.fechaField_t.Name = "fechaField_t";
            this.fechaField_t.ReadOnly = true;
            // 
            // benefField_t
            // 
            this.benefField_t.HeaderText = "Beneficiario";
            this.benefField_t.Name = "benefField_t";
            this.benefField_t.ReadOnly = true;
            // 
            // rFCField_t
            // 
            this.rFCField_t.HeaderText = "RFC";
            this.rFCField_t.Name = "rFCField_t";
            this.rFCField_t.ReadOnly = true;
            // 
            // Polizas
            // 
            this.Polizas.AttachedControl = this.superTabControlPanel3;
            this.Polizas.GlobalItem = false;
            this.Polizas.Name = "Polizas";
            this.Polizas.Text = "Transferencias";
            // 
            // ASIGNADO
            // 
            this.ASIGNADO.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.ASIGNADO.Border.Class = "TextBoxBorder";
            this.ASIGNADO.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.ASIGNADO.ForeColor = System.Drawing.Color.Black;
            this.ASIGNADO.Location = new System.Drawing.Point(475, 88);
            this.ASIGNADO.Name = "ASIGNADO";
            this.ASIGNADO.Size = new System.Drawing.Size(92, 22);
            this.ASIGNADO.TabIndex = 592;
            this.ASIGNADO.TabStop = false;
            this.ASIGNADO.WatermarkBehavior = DevComponents.DotNetBar.eWatermarkBehavior.HideNonEmpty;
            // 
            // labelX2
            // 
            this.labelX2.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.labelX2.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX2.ForeColor = System.Drawing.Color.Black;
            this.labelX2.Location = new System.Drawing.Point(409, 88);
            this.labelX2.Name = "labelX2";
            this.labelX2.Size = new System.Drawing.Size(60, 22);
            this.labelX2.TabIndex = 593;
            this.labelX2.Text = "Asginado";
            this.labelX2.TextAlignment = System.Drawing.StringAlignment.Far;
            // 
            // frmTransaccion
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.BottomLeftCornerSize = 0;
            this.ClientSize = new System.Drawing.Size(728, 388);
            this.Controls.Add(this.labelX2);
            this.Controls.Add(this.ASIGNADO);
            this.Controls.Add(this.superTabControl1);
            this.Controls.Add(this.labelX5);
            this.Controls.Add(this.haberField);
            this.Controls.Add(this.labelX4);
            this.Controls.Add(this.debeField);
            this.Controls.Add(this.monedaField);
            this.Controls.Add(this.labelX18);
            this.Controls.Add(this.labelX16);
            this.Controls.Add(this.labelX1);
            this.Controls.Add(this.conceptoField);
            this.Controls.Add(this.numCtaField);
            this.Controls.Add(buttonX3);
            this.Controls.Add(buttonX1);
            this.Controls.Add(buttonX2);
            this.Controls.Add(this.metroStatusBar1);
            this.Cursor = System.Windows.Forms.Cursors.Default;
            this.DoubleBuffered = true;
            this.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "frmTransaccion";
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Transacción";
            ((System.ComponentModel.ISupportInitialize)(this.superTabControl1)).EndInit();
            this.superTabControl1.ResumeLayout(false);
            this.superTabControlPanel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dtgCheque)).EndInit();
            this.superTabControlPanel4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dtgComprobantes)).EndInit();
            this.superTabControlPanel3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dtgTranferencia)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevComponents.DotNetBar.Metro.MetroStatusBar metroStatusBar1;
        private DevComponents.DotNetBar.LabelItem labelItem1;
        private DevComponents.DotNetBar.Controls.TextBoxX numCtaField;
        private DevComponents.DotNetBar.LabelX labelX1;
        private DevComponents.DotNetBar.Controls.TextBoxX conceptoField;
        private DevComponents.DotNetBar.LabelX labelX16;
        private DevComponents.DotNetBar.LabelX labelX18;
        private DevComponents.DotNetBar.Controls.ComboBoxEx monedaField;
        private DevComponents.Editors.ComboItem comboItem1;
        private DevComponents.Editors.ComboItem A;
        private DevComponents.DotNetBar.LabelX labelX5;
        private DevComponents.DotNetBar.Controls.TextBoxX haberField;
        private DevComponents.DotNetBar.LabelX labelX4;
        private DevComponents.DotNetBar.Controls.TextBoxX debeField;
        private DevComponents.DotNetBar.SuperTabControl superTabControl1;
        private DevComponents.DotNetBar.SuperTabControlPanel superTabControlPanel2;
        private DevComponents.DotNetBar.Controls.CircularProgress circularProgress2;
        private DevComponents.DotNetBar.ButtonItem buttonItem2;
        private DevComponents.DotNetBar.ButtonX buttonX10;
        private DevComponents.DotNetBar.ButtonX buttonX12;
        private DevComponents.DotNetBar.ButtonX buttonX13;
        private DevComponents.DotNetBar.Controls.DataGridViewX dtgCheque;
        private DevComponents.DotNetBar.SuperTabItem superTabItem2;
        private DevComponents.DotNetBar.SuperTabControlPanel superTabControlPanel3;
        private DevComponents.DotNetBar.Controls.CircularProgress circularProgress4;
        private DevComponents.DotNetBar.ButtonItem buttonItem4;
        private DevComponents.DotNetBar.ButtonX buttonX20;
        private DevComponents.DotNetBar.ButtonX buttonX22;
        private DevComponents.DotNetBar.ButtonX buttonX23;
        private DevComponents.DotNetBar.Controls.DataGridViewX dtgTranferencia;
        private DevComponents.DotNetBar.SuperTabItem Polizas;
        private DevComponents.DotNetBar.SuperTabControlPanel superTabControlPanel4;
        private DevComponents.DotNetBar.Controls.CircularProgress circularProgress1;
        private DevComponents.DotNetBar.ButtonItem buttonItem1;
        private DevComponents.DotNetBar.ButtonX buttonX5;
        private DevComponents.DotNetBar.ButtonX buttonX7;
        private DevComponents.DotNetBar.ButtonX buttonX8;
        private DevComponents.DotNetBar.Controls.DataGridViewX dtgComprobantes;
        private DevComponents.DotNetBar.SuperTabItem superTabItem3;
        private System.Windows.Forms.DataGridViewTextBoxColumn ROW_ID_b;
        private System.Windows.Forms.DataGridViewTextBoxColumn numCtaField_c;
        private System.Windows.Forms.DataGridViewTextBoxColumn bancoField;
        private System.Windows.Forms.DataGridViewTextBoxColumn ctaOriField;
        private System.Windows.Forms.DataGridViewTextBoxColumn fechaField;
        private System.Windows.Forms.DataGridViewTextBoxColumn montoField;
        private System.Windows.Forms.DataGridViewTextBoxColumn benefField;
        private System.Windows.Forms.DataGridViewTextBoxColumn rFCField;
        private System.Windows.Forms.DataGridViewTextBoxColumn ROW_ID_c;
        private System.Windows.Forms.DataGridViewTextBoxColumn uUID_CFDIField;
        private System.Windows.Forms.DataGridViewTextBoxColumn rFCField_c;
        private System.Windows.Forms.DataGridViewTextBoxColumn ROW_ID_t;
        private System.Windows.Forms.DataGridViewTextBoxColumn ctaOriField_t;
        private System.Windows.Forms.DataGridViewTextBoxColumn bancoOriField_t;
        private System.Windows.Forms.DataGridViewTextBoxColumn montoField_t;
        private System.Windows.Forms.DataGridViewTextBoxColumn ctaDestField_t;
        private System.Windows.Forms.DataGridViewTextBoxColumn bancoDestField_t;
        private System.Windows.Forms.DataGridViewTextBoxColumn fechaField_t;
        private System.Windows.Forms.DataGridViewTextBoxColumn benefField_t;
        private System.Windows.Forms.DataGridViewTextBoxColumn rFCField_t;
        private DevComponents.DotNetBar.Controls.TextBoxX ASIGNADO;
        private DevComponents.DotNetBar.LabelX labelX2;
        private System.Windows.Forms.FolderBrowserDialog folderBrowserDialog1;

    }
}