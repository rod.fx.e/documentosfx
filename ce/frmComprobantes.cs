using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevComponents.DotNetBar;
using System.IO;
using OfficeOpenXml;
using System.Threading;
using System.Xml.Serialization;
using DevComponents.DotNetBar.Controls;
using System.Linq;
using Generales;

namespace Desarrollo
{
    public partial class frmComprobantes : DevComponents.DotNetBar.Metro.MetroForm
    {
        bool modificado = false;
        string TIPO="";
        string LLAVE = "";
        string USUARIO = "";
        string ROW_ID_TRANSACCION;
        cTransaccion ocTransaccion = new cTransaccion();
        cPolizasPolizaTransaccionCompNal oObjeto = new cPolizasPolizaTransaccionCompNal();
        cCONEXCION oData = new cCONEXCION();

        public frmComprobantes(string ROW_ID_TRANSACCIONp)
        {
            oObjeto = new cPolizasPolizaTransaccionCompNal();
            ROW_ID_TRANSACCION = ROW_ID_TRANSACCIONp;
            InitializeComponent();
            limpiar();
        }

        public frmComprobantes(string TIPOp, string LLAVEp, string USUARIOp)
        {           
            TIPO=TIPOp;
            LLAVE = LLAVEp;
            USUARIO = USUARIOp;
            oObjeto = new cPolizasPolizaTransaccionCompNal();
            InitializeComponent();
            ROW_ID_TRANSACCION = "-1";
            this.Text = "CFDI para " + TIPO + " " + LLAVE + " " + Application.ProductName + "-" + Application.ProductVersion;
            verificar_si_cargo_comprobantes();
        }

        private void verificar_si_cargo_comprobantes()
        {
            labelX1.Text = "";
            string sSQL = "SELECT  * FROM [PolizasPolizaTransaccionCompNal] WHERE [LLAVE]='" + LLAVE + "'";
            DataTable oDataTable = oData.EjecutarConsulta(sSQL);
            foreach (DataRow oDataRow in oDataTable.Rows)
            {
                labelX1.Text = "<font color=\"#ED1C24\">La transacci�n ya tiene Comprobantes cargados.</font>";
                int n = dtgrdGeneral.Rows.Add();
                dtgrdGeneral.Rows[n].Cells["UUID"].Value = oDataRow["uUID_CFDIField"].ToString();
                dtgrdGeneral.Rows[n].Cells["RFC_EMISOR"].Value = oDataRow["rFCField"].ToString();
                dtgrdGeneral.Rows[n].Cells["RFC_RECEPTOR"].Value = oDataRow["RFC_RECEPTOR"].ToString();
                dtgrdGeneral.Rows[n].Cells["TOTAL_AMOUNT"].Value = oDataRow["montoTotalField"].ToString();


            }

            cargar_total();
        }

        #region Acciones
        private void cargar(string ROW_IDp)
        {
            if (oObjeto.cargar(ROW_IDp))
            {
                ROW_ID_TRANSACCION = oObjeto.ROW_ID_TRANSACCION;
                
            }

        }

        private void guardar()
        {
            oObjeto.ROW_ID_TRANSACCION = ROW_ID_TRANSACCION;
            oObjeto.TIPO = TIPO;
            oObjeto.LLAVE = LLAVE;
            if (oObjeto.guardar())
            {
                labelItem1.Text = "Datos Actualizados " + DateTime.Now.ToShortDateString() + " " + DateTime.Now.ToLongTimeString();
                mostrar_mensaje("Datos Actualizados ", false);
                modificado = false;
            }

        }

        private void mostrar_mensaje(string mensaje, bool mostrar_error)
        {
            ToastNotification.Show(this,
                mensaje,
                mostrar_error ? global::Desarrollo.Properties.Resources.error : global::Desarrollo.Properties.Resources.guardar,
                3000,
                mostrar_error ? eToastGlowColor.Red : eToastGlowColor.Green,
                eToastPosition.TopCenter);
        }
        public void limpiar()
        {
            if (modificado)
            {

                DialogResult Resultado = MessageBoxEx.Show("�Desea guardar los cambios?", Application.ProductName + "-" + Application.ProductVersion, MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                if (Resultado.ToString() == "Yes")
                {
                    guardar();
                }
            }
            modificado = false;
            oObjeto = new cPolizasPolizaTransaccionCompNal();
            foreach (var c in this.Controls)
            {
                if (c is TextBox)
                {
                    ((TextBox)c).Text = "";
                }
            }
            

        }
        
        private void eliminar()
        {
            DialogResult Resultado = MessageBoxEx.Show("�Esta seguro de eliminar?", Application.ProductName + "-" + Application.ProductVersion, MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            if (Resultado.ToString() != "Yes")
            {
                return;
            }
            if (oObjeto.eliminar(oObjeto.ROW_ID))
            {
                modificado = false;
                
                MessageBoxEx.Show("Eliminado.", Application.ProductName + "-" + Application.ProductVersion, MessageBoxButtons.OK, MessageBoxIcon.Information);
                limpiar();
                
            }
        }
        #endregion

        private void buttonX3_Click(object sender, EventArgs e)
        {
            eliminar();
        }

        private void buttonX2_Click(object sender, EventArgs e)
        {
            limpiar();
        }

        private void buttonX1_Click(object sender, EventArgs e)
        {
            guardar();
        }

        private void buttonX4_Click(object sender, EventArgs e)
        {
            buscar_comprobantes();
        }
        private void cargar_total()
        {
            TOTAL.Text = oData.formato_decimal("0", "S");
            double resultado = 0;
            resultado = dtgrdGeneral.Rows.Cast<DataGridViewRow>().Sum(x => Convert.ToDouble(x.Cells["TOTAL_AMOUNT"].Value));
            TOTAL.Text = oData.formato_decimal(resultado.ToString(), "");

        }
        private void buscar_comprobantes()
        {
            

            OpenFileDialog openFileDialog1 = new OpenFileDialog();
            openFileDialog1.Filter = "CFDI (*.xml)|*.xml|Todos los archivos (*.*)|*.*";
            openFileDialog1.FilterIndex = 1;
            openFileDialog1.Multiselect = true;
            openFileDialog1.RestoreDirectory = true;
            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                // Read the files 
                foreach (String file in openFileDialog1.FileNames)
                {
                    cPolizasPolizaTransaccionCompNal ocPolizasPolizaTransaccionCompNal = new Desarrollo.cPolizasPolizaTransaccionCompNal();
                    ocPolizasPolizaTransaccionCompNal.ROW_ID_TRANSACCION = ROW_ID_TRANSACCION;
                    ocPolizasPolizaTransaccionCompNal.TIPO = TIPO;
                    ocPolizasPolizaTransaccionCompNal.LLAVE = LLAVE;
                    ocPolizasPolizaTransaccionCompNal.USUARIO = USUARIO;
                    if (ocPolizasPolizaTransaccionCompNal.ingresar_comprobante(file))
                    {
                        int n = dtgrdGeneral.Rows.Add();
                        dtgrdGeneral.Rows[n].Cells["UUID"].Value = ocPolizasPolizaTransaccionCompNal.uUID_CFDIField;
                        dtgrdGeneral.Rows[n].Cells["RFC_EMISOR"].Value = ocPolizasPolizaTransaccionCompNal.rFCField;
                        dtgrdGeneral.Rows[n].Cells["RFC_RECEPTOR"].Value = ocPolizasPolizaTransaccionCompNal.RFC_RECEPTOR;
                        dtgrdGeneral.Rows[n].Cells["TOTAL_AMOUNT"].Value = oData.formato_decimal(ocPolizasPolizaTransaccionCompNal.montoTotalField.ToString(),"");


                    }
                     
                }
                cargar_total();
                if (dtgrdGeneral.Rows.Count != 0)
                {
                    MessageBoxEx.Show("Comprobantes cargados.", Application.ProductName + "-" + Application.ProductVersion, MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                
            }
        }
    }
}