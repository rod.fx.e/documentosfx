namespace Desarrollo
{
    partial class frmComprobante_Filtro
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            DevComponents.DotNetBar.ButtonX buttonX3;
            DevComponents.DotNetBar.ButtonX buttonX1;
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmComprobante_Filtro));
            this.metroStatusBar1 = new DevComponents.DotNetBar.Metro.MetroStatusBar();
            this.labelItem1 = new DevComponents.DotNetBar.LabelItem();
            this.labelX19 = new DevComponents.DotNetBar.LabelX();
            this.BATCH_INICIAL = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.BATCH_FINAL = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.labelX1 = new DevComponents.DotNetBar.LabelX();
            this.labelX2 = new DevComponents.DotNetBar.LabelX();
            this.APC = new DevComponents.DotNetBar.Controls.CheckBoxX();
            this.ARC = new DevComponents.DotNetBar.Controls.CheckBoxX();
            this.API = new DevComponents.DotNetBar.Controls.CheckBoxX();
            this.ARI = new DevComponents.DotNetBar.Controls.CheckBoxX();
            buttonX3 = new DevComponents.DotNetBar.ButtonX();
            buttonX1 = new DevComponents.DotNetBar.ButtonX();
            this.SuspendLayout();
            // 
            // buttonX3
            // 
            buttonX3.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            buttonX3.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            buttonX3.Image = global::Desarrollo.Properties.Resources.Eliminar_18;
            buttonX3.Location = new System.Drawing.Point(74, 175);
            buttonX3.Name = "buttonX3";
            buttonX3.Size = new System.Drawing.Size(68, 24);
            buttonX3.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            buttonX3.TabIndex = 5;
            buttonX3.Text = "Cancelar";
            buttonX3.TextAlignment = DevComponents.DotNetBar.eButtonTextAlignment.Right;
            buttonX3.Click += new System.EventHandler(this.buttonX3_Click);
            // 
            // buttonX1
            // 
            buttonX1.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            buttonX1.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            buttonX1.Image = global::Desarrollo.Properties.Resources.Nueno_18;
            buttonX1.Location = new System.Drawing.Point(0, 175);
            buttonX1.Name = "buttonX1";
            buttonX1.Size = new System.Drawing.Size(68, 24);
            buttonX1.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            buttonX1.TabIndex = 4;
            buttonX1.Text = "Importar";
            buttonX1.TextAlignment = DevComponents.DotNetBar.eButtonTextAlignment.Right;
            buttonX1.Click += new System.EventHandler(this.buttonX1_Click);
            // 
            // metroStatusBar1
            // 
            this.metroStatusBar1.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.metroStatusBar1.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.metroStatusBar1.ContainerControlProcessDialogKey = true;
            this.metroStatusBar1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.metroStatusBar1.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.metroStatusBar1.ForeColor = System.Drawing.Color.Black;
            this.metroStatusBar1.Items.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.labelItem1});
            this.metroStatusBar1.Location = new System.Drawing.Point(0, 204);
            this.metroStatusBar1.Name = "metroStatusBar1";
            this.metroStatusBar1.Size = new System.Drawing.Size(405, 21);
            this.metroStatusBar1.TabIndex = 6;
            // 
            // labelItem1
            // 
            this.labelItem1.Name = "labelItem1";
            // 
            // labelX19
            // 
            this.labelX19.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.labelX19.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX19.ForeColor = System.Drawing.Color.Black;
            this.labelX19.Location = new System.Drawing.Point(0, 3);
            this.labelX19.Name = "labelX19";
            this.labelX19.Size = new System.Drawing.Size(60, 22);
            this.labelX19.TabIndex = 0;
            this.labelX19.Text = "Lote Inicial";
            this.labelX19.TextAlignment = System.Drawing.StringAlignment.Far;
            // 
            // BATCH_INICIAL
            // 
            this.BATCH_INICIAL.AcceptsTab = true;
            this.BATCH_INICIAL.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.BATCH_INICIAL.Border.Class = "TextBoxBorder";
            this.BATCH_INICIAL.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.BATCH_INICIAL.ForeColor = System.Drawing.Color.Black;
            this.BATCH_INICIAL.Location = new System.Drawing.Point(66, 3);
            this.BATCH_INICIAL.Name = "BATCH_INICIAL";
            this.BATCH_INICIAL.Size = new System.Drawing.Size(76, 22);
            this.BATCH_INICIAL.TabIndex = 1;
            this.BATCH_INICIAL.WatermarkBehavior = DevComponents.DotNetBar.eWatermarkBehavior.HideNonEmpty;
            // 
            // BATCH_FINAL
            // 
            this.BATCH_FINAL.AcceptsTab = true;
            this.BATCH_FINAL.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.BATCH_FINAL.Border.Class = "TextBoxBorder";
            this.BATCH_FINAL.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.BATCH_FINAL.ForeColor = System.Drawing.Color.Black;
            this.BATCH_FINAL.Location = new System.Drawing.Point(215, 3);
            this.BATCH_FINAL.Name = "BATCH_FINAL";
            this.BATCH_FINAL.Size = new System.Drawing.Size(76, 22);
            this.BATCH_FINAL.TabIndex = 3;
            this.BATCH_FINAL.WatermarkBehavior = DevComponents.DotNetBar.eWatermarkBehavior.HideNonEmpty;
            // 
            // labelX1
            // 
            this.labelX1.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.labelX1.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX1.ForeColor = System.Drawing.Color.Black;
            this.labelX1.Location = new System.Drawing.Point(149, 3);
            this.labelX1.Name = "labelX1";
            this.labelX1.Size = new System.Drawing.Size(60, 22);
            this.labelX1.TabIndex = 2;
            this.labelX1.Text = "Lote Final";
            this.labelX1.TextAlignment = System.Drawing.StringAlignment.Far;
            // 
            // labelX2
            // 
            this.labelX2.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.labelX2.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX2.ForeColor = System.Drawing.Color.Black;
            this.labelX2.Location = new System.Drawing.Point(9, 31);
            this.labelX2.Name = "labelX2";
            this.labelX2.Size = new System.Drawing.Size(94, 22);
            this.labelX2.TabIndex = 599;
            this.labelX2.Text = "Importar de tipo:";
            // 
            // APC
            // 
            this.APC.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.APC.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.APC.Checked = true;
            this.APC.CheckState = System.Windows.Forms.CheckState.Checked;
            this.APC.CheckValue = "Y";
            this.APC.ForeColor = System.Drawing.Color.Black;
            this.APC.Location = new System.Drawing.Point(9, 59);
            this.APC.Name = "APC";
            this.APC.Size = new System.Drawing.Size(377, 23);
            this.APC.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.APC.TabIndex = 603;
            this.APC.Text = "Pagos (APC). Requiere comprobantes fiscales. (Proveedores)";
            // 
            // ARC
            // 
            this.ARC.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.ARC.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.ARC.Checked = true;
            this.ARC.CheckState = System.Windows.Forms.CheckState.Checked;
            this.ARC.CheckValue = "Y";
            this.ARC.ForeColor = System.Drawing.Color.Black;
            this.ARC.Location = new System.Drawing.Point(9, 117);
            this.ARC.Name = "ARC";
            this.ARC.Size = new System.Drawing.Size(377, 23);
            this.ARC.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.ARC.TabIndex = 602;
            this.ARC.Text = "Cobranza (ARC). Requiere comprobantes fiscales. (Clientes)";
            // 
            // API
            // 
            this.API.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.API.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.API.Checked = true;
            this.API.CheckState = System.Windows.Forms.CheckState.Checked;
            this.API.CheckValue = "Y";
            this.API.ForeColor = System.Drawing.Color.Black;
            this.API.Location = new System.Drawing.Point(9, 88);
            this.API.Name = "API";
            this.API.Size = new System.Drawing.Size(389, 23);
            this.API.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.API.TabIndex = 601;
            this.API.Text = "Cuentas por  Pagar (API). Requiere comprobantes fiscales. (Proveedores)";
            this.API.CheckedChanged += new System.EventHandler(this.API_CheckedChanged);
            // 
            // ARI
            // 
            this.ARI.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.ARI.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.ARI.Checked = true;
            this.ARI.CheckState = System.Windows.Forms.CheckState.Checked;
            this.ARI.CheckValue = "Y";
            this.ARI.ForeColor = System.Drawing.Color.Black;
            this.ARI.Location = new System.Drawing.Point(9, 146);
            this.ARI.Name = "ARI";
            this.ARI.Size = new System.Drawing.Size(362, 23);
            this.ARI.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.ARI.TabIndex = 600;
            this.ARI.Text = "Cuentas por Cobrar (ARI). Requiere comprobantes fiscales. (Clientes)";
            // 
            // frmComprobante_Filtro
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.BottomLeftCornerSize = 0;
            this.ClientSize = new System.Drawing.Size(405, 225);
            this.Controls.Add(this.APC);
            this.Controls.Add(this.ARC);
            this.Controls.Add(this.API);
            this.Controls.Add(this.ARI);
            this.Controls.Add(this.labelX2);
            this.Controls.Add(this.BATCH_FINAL);
            this.Controls.Add(this.labelX1);
            this.Controls.Add(this.BATCH_INICIAL);
            this.Controls.Add(this.labelX19);
            this.Controls.Add(buttonX3);
            this.Controls.Add(buttonX1);
            this.Controls.Add(this.metroStatusBar1);
            this.Cursor = System.Windows.Forms.Cursors.Default;
            this.DoubleBuffered = true;
            this.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "frmComprobante_Filtro";
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Filtro de importación de comprobantes";
            this.Load += new System.EventHandler(this.frmComprobante_Filtro_Load);
            this.ResumeLayout(false);

        }

        #endregion

        private DevComponents.DotNetBar.Metro.MetroStatusBar metroStatusBar1;
        private DevComponents.DotNetBar.LabelItem labelItem1;
        private DevComponents.DotNetBar.LabelX labelX19;
        private DevComponents.DotNetBar.Controls.TextBoxX BATCH_INICIAL;
        private DevComponents.DotNetBar.Controls.TextBoxX BATCH_FINAL;
        private DevComponents.DotNetBar.LabelX labelX1;
        private DevComponents.DotNetBar.LabelX labelX2;
        private DevComponents.DotNetBar.Controls.CheckBoxX APC;
        private DevComponents.DotNetBar.Controls.CheckBoxX ARC;
        private DevComponents.DotNetBar.Controls.CheckBoxX API;
        private DevComponents.DotNetBar.Controls.CheckBoxX ARI;

    }
}