using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevComponents.DotNetBar;
using System.IO;
using OfficeOpenXml;
using Generales;

namespace Desarrollo
{
    public partial class frmBalanza : DevComponents.DotNetBar.Metro.MetroForm
    {
        bool modificado = false;
        string ROW_ID_CATALOGO;
        cBalanza oObjeto = new cBalanza();
        cCONEXCION oData = new cCONEXCION();
        public frmBalanza(string ROW_ID_CATALOGOp)
        {
            oObjeto = new cBalanza();
            ROW_ID_CATALOGO = ROW_ID_CATALOGOp;
            InitializeComponent();
            limpiar();
        }

        public frmBalanza(string ROW_ID_CATALOGOp,string ROW_ID)
        {
            ROW_ID_CATALOGO = ROW_ID_CATALOGOp;
            oObjeto = new cBalanza();
            InitializeComponent();
            cargar(ROW_ID);
        }

        #region Acciones
        private void Solo_Numero_KeyPress(object sender, KeyPressEventArgs e)
        {
            try
            {
                modificado = true;
                if (!char.IsDigit(e.KeyChar) && !char.IsControl(e.KeyChar) && !char.IsPunctuation(e.KeyChar)) e.Handled = true;
            }
            catch
            {
                e.Handled = false;
            }
        }
        private void COSTO_Leave(object sender, EventArgs e)
        {
            TextBox tb = (TextBox)sender;
            tb.Text = oData.formato_decimal(tb.Text, "");
        }
        private void cargar(string ROW_IDp)
        {
            if (oObjeto.cargar(ROW_IDp))
            {
                ROW_ID_CATALOGO = oObjeto.ROW_ID_CATALOGO;
                numCtaField.Text=oObjeto.numCtaField;
                saldoIniField.Text= oData.formato_decimal(oObjeto.saldoIniField.ToString(), "");
                debeField.Text= oData.formato_decimal(oObjeto.debeField.ToString(), "");
                haberField.Text= oData.formato_decimal(oObjeto.haberField.ToString(), "");
                saldoFinField.Text = oData.formato_decimal(oObjeto.saldoFinField.ToString(), "");
            }

        }

        private void guardar()
        {            
            oObjeto.numCtaField=numCtaField.Text;
            oObjeto.ROW_ID_CATALOGO = ROW_ID_CATALOGO;
            oObjeto.saldoIniField = double.Parse(saldoIniField.Text);
            oObjeto.debeField= double.Parse(debeField.Text);
            oObjeto.haberField= double.Parse(haberField.Text);
            oObjeto.saldoFinField= double.Parse(saldoFinField.Text);

            if (oObjeto.guardar())
            {
                labelItem1.Text = "Datos Actualizados " + DateTime.Now.ToShortDateString() + " " + DateTime.Now.ToLongTimeString();
                mostrar_mensaje("Datos Actualizados ", false);
                modificado = false;
            }

        }

        private void mostrar_mensaje(string mensaje, bool mostrar_error)
        {
            ToastNotification.Show(this,
                mensaje,
                mostrar_error ? global::Desarrollo.Properties.Resources.error : global::Desarrollo.Properties.Resources.guardar,
                3000,
                mostrar_error ? eToastGlowColor.Red : eToastGlowColor.Green,
                eToastPosition.TopCenter);
        }
        public void limpiar()
        {
            if (modificado)
            {

                DialogResult Resultado = MessageBoxEx.Show("�Desea guardar los cambios?", Application.ProductName + "-" + Application.ProductVersion, MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                if (Resultado.ToString() == "Yes")
                {
                    guardar();
                }
            }
            modificado = false;
            oObjeto = new cBalanza();
            
            foreach (var c in this.Controls)
            {
                if (c is TextBox)
                {
                    ((TextBox)c).Text = "";
                }
            }
            

        }
        

        private void eliminar()
        {
            DialogResult Resultado = MessageBoxEx.Show("�Esta seguro de eliminar?", Application.ProductName + "-" + Application.ProductVersion, MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            if (Resultado.ToString() != "Yes")
            {
                return;
            }
            if (oObjeto.eliminar(oObjeto.ROW_ID))
            {
                modificado = false;
                
                MessageBoxEx.Show("Eliminado.", Application.ProductName + "-" + Application.ProductVersion, MessageBoxButtons.OK, MessageBoxIcon.Information);
                limpiar();
                
            }
        }
        #endregion

        private void buttonX3_Click(object sender, EventArgs e)
        {
            eliminar();
        }

        private void buttonX2_Click(object sender, EventArgs e)
        {
            limpiar();
        }

        private void buttonX1_Click(object sender, EventArgs e)
        {
            guardar();
        }

    }
}