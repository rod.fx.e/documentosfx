namespace Desarrollo
{
    partial class frmDeclaracion
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            DevComponents.DotNetBar.ButtonX buttonX4;
            DevComponents.DotNetBar.ButtonX buttonX6;
            DevComponents.DotNetBar.ButtonX buttonX9;
            DevComponents.DotNetBar.ButtonX buttonX11;
            DevComponents.DotNetBar.ButtonX buttonX3;
            DevComponents.DotNetBar.ButtonX buttonX1;
            DevComponents.DotNetBar.ButtonX buttonX2;
            DevComponents.DotNetBar.ButtonX buttonX26;
            DevComponents.DotNetBar.ButtonX buttonX27;
            DevComponents.DotNetBar.ButtonX buttonX15;
            DevComponents.DotNetBar.ButtonX buttonX14;
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle7 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle8 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle9 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle10 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle11 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle12 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle13 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmDeclaracion));
            this.buttonItem1 = new DevComponents.DotNetBar.ButtonItem();
            this.buttonItem3 = new DevComponents.DotNetBar.ButtonItem();
            this.buttonItem2 = new DevComponents.DotNetBar.ButtonItem();
            this.metroStatusBar1 = new DevComponents.DotNetBar.Metro.MetroStatusBar();
            this.labelItem1 = new DevComponents.DotNetBar.LabelItem();
            this.mesField = new DevComponents.DotNetBar.Controls.ComboBoxEx();
            this.comboItem1 = new DevComponents.Editors.ComboItem();
            this.comboItem2 = new DevComponents.Editors.ComboItem();
            this.comboItem5 = new DevComponents.Editors.ComboItem();
            this.comboItem6 = new DevComponents.Editors.ComboItem();
            this.comboItem7 = new DevComponents.Editors.ComboItem();
            this.comboItem8 = new DevComponents.Editors.ComboItem();
            this.comboItem9 = new DevComponents.Editors.ComboItem();
            this.comboItem10 = new DevComponents.Editors.ComboItem();
            this.comboItem11 = new DevComponents.Editors.ComboItem();
            this.comboItem12 = new DevComponents.Editors.ComboItem();
            this.comboItem13 = new DevComponents.Editors.ComboItem();
            this.comboItem14 = new DevComponents.Editors.ComboItem();
            this.comboItem15 = new DevComponents.Editors.ComboItem();
            this.labelX4 = new DevComponents.DotNetBar.LabelX();
            this.totalCtasField = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.labelX3 = new DevComponents.DotNetBar.LabelX();
            this.anoField = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.labelX2 = new DevComponents.DotNetBar.LabelX();
            this.labelX1 = new DevComponents.DotNetBar.LabelX();
            this.rFCField = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.versionField = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.label1 = new DevComponents.DotNetBar.LabelX();
            this.superTabControl1 = new DevComponents.DotNetBar.SuperTabControl();
            this.superTabControlPanel2 = new DevComponents.DotNetBar.SuperTabControlPanel();
            this.labelX9 = new DevComponents.DotNetBar.LabelX();
            this.SALDO_FINAL_TOTAL = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.labelX8 = new DevComponents.DotNetBar.LabelX();
            this.HABER_TOTAL = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.labelX7 = new DevComponents.DotNetBar.LabelX();
            this.DEBE_TOTAL = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.labelX6 = new DevComponents.DotNetBar.LabelX();
            this.SALDO_INICIAL_TOTAL = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.buttonX21 = new DevComponents.DotNetBar.ButtonX();
            this.circularProgress2 = new DevComponents.DotNetBar.Controls.CircularProgress();
            this.buttonX10 = new DevComponents.DotNetBar.ButtonX();
            this.buttonX12 = new DevComponents.DotNetBar.ButtonX();
            this.dtgBalanza = new DevComponents.DotNetBar.Controls.DataGridViewX();
            this.ROW_ID_b = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.numCtaField_b = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.saldoIniField_b = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.debeField_b = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.haberField_b = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.saldoFinField_b = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.SUMMARY_ACCOUNT = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.buttonX13 = new DevComponents.DotNetBar.ButtonX();
            this.superTabItem2 = new DevComponents.DotNetBar.SuperTabItem();
            this.superTabControlPanel1 = new DevComponents.DotNetBar.SuperTabControlPanel();
            this.chkPeriodo = new DevComponents.DotNetBar.Controls.CheckBoxX();
            this.buttonX24 = new DevComponents.DotNetBar.ButtonX();
            this.buttonX20 = new DevComponents.DotNetBar.ButtonX();
            this.circularProgress1 = new DevComponents.DotNetBar.Controls.CircularProgress();
            this.dtgCuentas = new DevComponents.DotNetBar.Controls.DataGridViewX();
            this.ROW_ID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.codAgrupField = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.numCtaField = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.descField = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.subCtaDeField = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.nivelField = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.naturField = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.buttonX5 = new DevComponents.DotNetBar.ButtonX();
            this.buttonItem5 = new DevComponents.DotNetBar.ButtonItem();
            this.buttonItem4 = new DevComponents.DotNetBar.ButtonItem();
            this.buttonX8 = new DevComponents.DotNetBar.ButtonX();
            this.buttonX7 = new DevComponents.DotNetBar.ButtonX();
            this.superTabItem1 = new DevComponents.DotNetBar.SuperTabItem();
            this.superTabControlPanel4 = new DevComponents.DotNetBar.SuperTabControlPanel();
            this.buttonX23 = new DevComponents.DotNetBar.ButtonX();
            this.dtgValidador = new DevComponents.DotNetBar.Controls.DataGridViewX();
            this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.TIPO = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CUENTA = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.MENSAJE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.superTabItem3 = new DevComponents.DotNetBar.SuperTabItem();
            this.Polizas = new DevComponents.DotNetBar.SuperTabItem();
            this.ESTADO = new DevComponents.DotNetBar.Controls.ComboBoxEx();
            this.comboItem3 = new DevComponents.Editors.ComboItem();
            this.comboItem4 = new DevComponents.Editors.ComboItem();
            this.labelX5 = new DevComponents.DotNetBar.LabelX();
            this.folderBrowserDialog1 = new System.Windows.Forms.FolderBrowserDialog();
            this.backgroundWorker1 = new System.ComponentModel.BackgroundWorker();
            this.backgroundWorker2 = new System.ComponentModel.BackgroundWorker();
            this.usarPeriodo = new DevComponents.DotNetBar.Controls.CheckBoxX();
            this.periodoInicial = new DevComponents.DotNetBar.Controls.ComboBoxEx();
            this.labelX10 = new DevComponents.DotNetBar.LabelX();
            this.labelX11 = new DevComponents.DotNetBar.LabelX();
            this.periodoFinal = new DevComponents.DotNetBar.Controls.ComboBoxEx();
            buttonX4 = new DevComponents.DotNetBar.ButtonX();
            buttonX6 = new DevComponents.DotNetBar.ButtonX();
            buttonX9 = new DevComponents.DotNetBar.ButtonX();
            buttonX11 = new DevComponents.DotNetBar.ButtonX();
            buttonX3 = new DevComponents.DotNetBar.ButtonX();
            buttonX1 = new DevComponents.DotNetBar.ButtonX();
            buttonX2 = new DevComponents.DotNetBar.ButtonX();
            buttonX26 = new DevComponents.DotNetBar.ButtonX();
            buttonX27 = new DevComponents.DotNetBar.ButtonX();
            buttonX15 = new DevComponents.DotNetBar.ButtonX();
            buttonX14 = new DevComponents.DotNetBar.ButtonX();
            ((System.ComponentModel.ISupportInitialize)(this.superTabControl1)).BeginInit();
            this.superTabControl1.SuspendLayout();
            this.superTabControlPanel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dtgBalanza)).BeginInit();
            this.superTabControlPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dtgCuentas)).BeginInit();
            this.superTabControlPanel4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dtgValidador)).BeginInit();
            this.SuspendLayout();
            // 
            // buttonX4
            // 
            buttonX4.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            buttonX4.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            buttonX4.Image = global::Desarrollo.Properties.Resources.excel;
            buttonX4.Location = new System.Drawing.Point(472, 32);
            buttonX4.Name = "buttonX4";
            buttonX4.Size = new System.Drawing.Size(113, 24);
            buttonX4.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            buttonX4.SubItems.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.buttonItem1});
            buttonX4.TabIndex = 10;
            buttonX4.Text = "Importar: Excel";
            buttonX4.TextAlignment = DevComponents.DotNetBar.eButtonTextAlignment.Right;
            buttonX4.Visible = false;
            buttonX4.Click += new System.EventHandler(this.buttonX4_Click);
            // 
            // buttonItem1
            // 
            this.buttonItem1.GlobalItem = false;
            this.buttonItem1.Image = global::Desarrollo.Properties.Resources.wizard;
            this.buttonItem1.Name = "buttonItem1";
            this.buttonItem1.Text = "Generar Plantilla";
            this.buttonItem1.Click += new System.EventHandler(this.buttonItem1_Click);
            // 
            // buttonX6
            // 
            buttonX6.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            buttonX6.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            buttonX6.Image = global::Desarrollo.Properties.Resources.Eliminar_18;
            buttonX6.Location = new System.Drawing.Point(576, 3);
            buttonX6.Name = "buttonX6";
            buttonX6.Size = new System.Drawing.Size(84, 24);
            buttonX6.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            buttonX6.SubItems.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.buttonItem3});
            buttonX6.TabIndex = 12;
            buttonX6.Text = "Eliminar";
            buttonX6.TextAlignment = DevComponents.DotNetBar.eButtonTextAlignment.Right;
            buttonX6.Click += new System.EventHandler(this.buttonX6_Click);
            // 
            // buttonItem3
            // 
            this.buttonItem3.GlobalItem = false;
            this.buttonItem3.Name = "buttonItem3";
            this.buttonItem3.Text = "Eliminar cuentas que no existan en Balanza";
            this.buttonItem3.Click += new System.EventHandler(this.buttonItem3_Click);
            // 
            // buttonX9
            // 
            buttonX9.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            buttonX9.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            buttonX9.Image = global::Desarrollo.Properties.Resources.excel;
            buttonX9.Location = new System.Drawing.Point(592, 3);
            buttonX9.Name = "buttonX9";
            buttonX9.Size = new System.Drawing.Size(114, 24);
            buttonX9.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            buttonX9.SubItems.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.buttonItem2});
            buttonX9.TabIndex = 574;
            buttonX9.Text = "Importar: Excel";
            buttonX9.TextAlignment = DevComponents.DotNetBar.eButtonTextAlignment.Right;
            buttonX9.Click += new System.EventHandler(this.buttonX9_Click);
            // 
            // buttonItem2
            // 
            this.buttonItem2.GlobalItem = false;
            this.buttonItem2.Image = global::Desarrollo.Properties.Resources.wizard;
            this.buttonItem2.Name = "buttonItem2";
            this.buttonItem2.Text = "Generar Plantilla";
            this.buttonItem2.Click += new System.EventHandler(this.buttonItem2_Click);
            // 
            // buttonX11
            // 
            buttonX11.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            buttonX11.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            buttonX11.Image = global::Desarrollo.Properties.Resources.Eliminar_18;
            buttonX11.Location = new System.Drawing.Point(438, 3);
            buttonX11.Name = "buttonX11";
            buttonX11.Size = new System.Drawing.Size(68, 24);
            buttonX11.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            buttonX11.TabIndex = 576;
            buttonX11.Text = "Eliminar";
            buttonX11.TextAlignment = DevComponents.DotNetBar.eButtonTextAlignment.Right;
            buttonX11.Click += new System.EventHandler(this.buttonX11_Click);
            // 
            // buttonX3
            // 
            buttonX3.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            buttonX3.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            buttonX3.Image = global::Desarrollo.Properties.Resources.Eliminar_18;
            buttonX3.Location = new System.Drawing.Point(141, 0);
            buttonX3.Name = "buttonX3";
            buttonX3.Size = new System.Drawing.Size(68, 24);
            buttonX3.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            buttonX3.TabIndex = 1;
            buttonX3.Text = "Eliminar";
            buttonX3.TextAlignment = DevComponents.DotNetBar.eButtonTextAlignment.Right;
            buttonX3.Click += new System.EventHandler(this.buttonX3_Click);
            // 
            // buttonX1
            // 
            buttonX1.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            buttonX1.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            buttonX1.Image = global::Desarrollo.Properties.Resources.Nueno_18;
            buttonX1.Location = new System.Drawing.Point(67, 0);
            buttonX1.Name = "buttonX1";
            buttonX1.Size = new System.Drawing.Size(68, 24);
            buttonX1.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            buttonX1.TabIndex = 1;
            buttonX1.Text = "Guardar";
            buttonX1.TextAlignment = DevComponents.DotNetBar.eButtonTextAlignment.Right;
            buttonX1.Click += new System.EventHandler(this.buttonX1_Click);
            // 
            // buttonX2
            // 
            buttonX2.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            buttonX2.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            buttonX2.Image = global::Desarrollo.Properties.Resources.Nuevo_18_19;
            buttonX2.Location = new System.Drawing.Point(0, 0);
            buttonX2.Name = "buttonX2";
            buttonX2.Size = new System.Drawing.Size(61, 24);
            buttonX2.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            buttonX2.TabIndex = 1;
            buttonX2.Text = "Nuevo";
            buttonX2.TextAlignment = DevComponents.DotNetBar.eButtonTextAlignment.Right;
            buttonX2.Click += new System.EventHandler(this.buttonX2_Click);
            // 
            // buttonX26
            // 
            buttonX26.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            buttonX26.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            buttonX26.Image = global::Desarrollo.Properties.Resources.excel;
            buttonX26.Location = new System.Drawing.Point(325, 3);
            buttonX26.Name = "buttonX26";
            buttonX26.Size = new System.Drawing.Size(107, 24);
            buttonX26.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            buttonX26.TabIndex = 580;
            buttonX26.Text = "Exportar: Excel";
            buttonX26.Click += new System.EventHandler(this.buttonX26_Click);
            // 
            // buttonX27
            // 
            buttonX27.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            buttonX27.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            buttonX27.Image = global::Desarrollo.Properties.Resources.excel;
            buttonX27.Location = new System.Drawing.Point(472, 3);
            buttonX27.Name = "buttonX27";
            buttonX27.Size = new System.Drawing.Size(98, 24);
            buttonX27.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            buttonX27.TabIndex = 581;
            buttonX27.Text = "Exportar: Excel";
            buttonX27.Click += new System.EventHandler(this.buttonX27_Click);
            // 
            // buttonX15
            // 
            buttonX15.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            buttonX15.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            buttonX15.Image = global::Desarrollo.Properties.Resources._1424086975_19_20;
            buttonX15.Location = new System.Drawing.Point(215, 0);
            buttonX15.Name = "buttonX15";
            buttonX15.Size = new System.Drawing.Size(121, 24);
            buttonX15.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            buttonX15.TabIndex = 579;
            buttonX15.Text = "Detalles de P�lizas";
            buttonX15.TextAlignment = DevComponents.DotNetBar.eButtonTextAlignment.Right;
            buttonX15.Click += new System.EventHandler(this.buttonX15_Click);
            // 
            // buttonX14
            // 
            buttonX14.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            buttonX14.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            buttonX14.Image = global::Desarrollo.Properties.Resources.Eliminar_18;
            buttonX14.Location = new System.Drawing.Point(686, 3);
            buttonX14.Name = "buttonX14";
            buttonX14.Size = new System.Drawing.Size(171, 24);
            buttonX14.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            buttonX14.TabIndex = 598;
            buttonX14.Text = "Eliminar todas sin agrupador";
            buttonX14.TextAlignment = DevComponents.DotNetBar.eButtonTextAlignment.Right;
            buttonX14.Visible = false;
            buttonX14.Click += new System.EventHandler(this.buttonX14_Click_2);
            // 
            // metroStatusBar1
            // 
            this.metroStatusBar1.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.metroStatusBar1.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.metroStatusBar1.ContainerControlProcessDialogKey = true;
            this.metroStatusBar1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.metroStatusBar1.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.metroStatusBar1.ForeColor = System.Drawing.Color.Black;
            this.metroStatusBar1.Items.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.labelItem1});
            this.metroStatusBar1.Location = new System.Drawing.Point(0, 397);
            this.metroStatusBar1.Name = "metroStatusBar1";
            this.metroStatusBar1.Size = new System.Drawing.Size(860, 21);
            this.metroStatusBar1.TabIndex = 8;
            // 
            // labelItem1
            // 
            this.labelItem1.Name = "labelItem1";
            // 
            // mesField
            // 
            this.mesField.AutoCompleteCustomSource.AddRange(new string[] {
            "01",
            "02",
            "03",
            "04",
            "05",
            "06",
            "07",
            "08",
            "09",
            "10",
            "11",
            "12"});
            this.mesField.DisplayMember = "Text";
            this.mesField.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.mesField.ForeColor = System.Drawing.Color.Black;
            this.mesField.FormattingEnabled = true;
            this.mesField.ItemHeight = 16;
            this.mesField.Items.AddRange(new object[] {
            this.comboItem1,
            this.comboItem2,
            this.comboItem5,
            this.comboItem6,
            this.comboItem7,
            this.comboItem8,
            this.comboItem9,
            this.comboItem10,
            this.comboItem11,
            this.comboItem12,
            this.comboItem13,
            this.comboItem14,
            this.comboItem15});
            this.mesField.Location = new System.Drawing.Point(282, 30);
            this.mesField.Name = "mesField";
            this.mesField.Size = new System.Drawing.Size(45, 22);
            this.mesField.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.mesField.TabIndex = 20;
            // 
            // comboItem1
            // 
            this.comboItem1.Text = "01";
            // 
            // comboItem2
            // 
            this.comboItem2.Text = "02";
            // 
            // comboItem5
            // 
            this.comboItem5.Text = "03";
            // 
            // comboItem6
            // 
            this.comboItem6.Text = "04";
            // 
            // comboItem7
            // 
            this.comboItem7.Text = "05";
            // 
            // comboItem8
            // 
            this.comboItem8.Text = "06";
            // 
            // comboItem9
            // 
            this.comboItem9.Text = "07";
            // 
            // comboItem10
            // 
            this.comboItem10.Text = "08";
            // 
            // comboItem11
            // 
            this.comboItem11.Text = "09";
            // 
            // comboItem12
            // 
            this.comboItem12.Text = "10";
            // 
            // comboItem13
            // 
            this.comboItem13.Text = "11";
            // 
            // comboItem14
            // 
            this.comboItem14.Text = "12";
            // 
            // comboItem15
            // 
            this.comboItem15.Text = "13";
            // 
            // labelX4
            // 
            this.labelX4.AutoSize = true;
            this.labelX4.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.labelX4.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX4.ForeColor = System.Drawing.Color.Black;
            this.labelX4.Location = new System.Drawing.Point(411, 33);
            this.labelX4.Name = "labelX4";
            this.labelX4.Size = new System.Drawing.Size(84, 17);
            this.labelX4.TabIndex = 18;
            this.labelX4.Text = "Total de Cuentas";
            // 
            // totalCtasField
            // 
            this.totalCtasField.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.totalCtasField.Border.Class = "TextBoxBorder";
            this.totalCtasField.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.totalCtasField.Enabled = false;
            this.totalCtasField.ForeColor = System.Drawing.Color.Black;
            this.totalCtasField.Location = new System.Drawing.Point(501, 30);
            this.totalCtasField.Name = "totalCtasField";
            this.totalCtasField.Size = new System.Drawing.Size(60, 22);
            this.totalCtasField.TabIndex = 19;
            // 
            // labelX3
            // 
            this.labelX3.AutoSize = true;
            this.labelX3.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.labelX3.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX3.ForeColor = System.Drawing.Color.Black;
            this.labelX3.Location = new System.Drawing.Point(333, 33);
            this.labelX3.Name = "labelX3";
            this.labelX3.Size = new System.Drawing.Size(22, 17);
            this.labelX3.TabIndex = 16;
            this.labelX3.Text = "A�o";
            // 
            // anoField
            // 
            this.anoField.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.anoField.Border.Class = "TextBoxBorder";
            this.anoField.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.anoField.ForeColor = System.Drawing.Color.Black;
            this.anoField.Location = new System.Drawing.Point(359, 30);
            this.anoField.Name = "anoField";
            this.anoField.Size = new System.Drawing.Size(48, 22);
            this.anoField.TabIndex = 17;
            // 
            // labelX2
            // 
            this.labelX2.AutoSize = true;
            this.labelX2.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.labelX2.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX2.ForeColor = System.Drawing.Color.Black;
            this.labelX2.Location = new System.Drawing.Point(254, 33);
            this.labelX2.Name = "labelX2";
            this.labelX2.Size = new System.Drawing.Size(23, 17);
            this.labelX2.TabIndex = 15;
            this.labelX2.Text = "Mes";
            // 
            // labelX1
            // 
            this.labelX1.AutoSize = true;
            this.labelX1.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.labelX1.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX1.ForeColor = System.Drawing.Color.Black;
            this.labelX1.Location = new System.Drawing.Point(93, 33);
            this.labelX1.Name = "labelX1";
            this.labelX1.Size = new System.Drawing.Size(21, 17);
            this.labelX1.TabIndex = 13;
            this.labelX1.Text = "RFC";
            // 
            // rFCField
            // 
            this.rFCField.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.rFCField.Border.Class = "TextBoxBorder";
            this.rFCField.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.rFCField.Enabled = false;
            this.rFCField.ForeColor = System.Drawing.Color.Black;
            this.rFCField.Location = new System.Drawing.Point(122, 30);
            this.rFCField.Name = "rFCField";
            this.rFCField.Size = new System.Drawing.Size(128, 22);
            this.rFCField.TabIndex = 14;
            // 
            // versionField
            // 
            this.versionField.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.versionField.Border.Class = "TextBoxBorder";
            this.versionField.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.versionField.ForeColor = System.Drawing.Color.Black;
            this.versionField.Location = new System.Drawing.Point(45, 30);
            this.versionField.Name = "versionField";
            this.versionField.ReadOnly = true;
            this.versionField.Size = new System.Drawing.Size(44, 22);
            this.versionField.TabIndex = 12;
            this.versionField.Text = "1.3";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.label1.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.label1.ForeColor = System.Drawing.Color.Black;
            this.label1.Location = new System.Drawing.Point(1, 33);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(39, 17);
            this.label1.TabIndex = 11;
            this.label1.Text = "Versi�n";
            // 
            // superTabControl1
            // 
            this.superTabControl1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.superTabControl1.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            // 
            // 
            // 
            this.superTabControl1.ControlBox.CloseBox.Name = "";
            // 
            // 
            // 
            this.superTabControl1.ControlBox.MenuBox.Name = "";
            this.superTabControl1.ControlBox.Name = "";
            this.superTabControl1.ControlBox.SubItems.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.superTabControl1.ControlBox.MenuBox,
            this.superTabControl1.ControlBox.CloseBox});
            this.superTabControl1.Controls.Add(this.superTabControlPanel2);
            this.superTabControl1.Controls.Add(this.superTabControlPanel1);
            this.superTabControl1.Controls.Add(this.superTabControlPanel4);
            this.superTabControl1.ForeColor = System.Drawing.Color.Black;
            this.superTabControl1.Location = new System.Drawing.Point(0, 80);
            this.superTabControl1.Name = "superTabControl1";
            this.superTabControl1.ReorderTabsEnabled = true;
            this.superTabControl1.SelectedTabFont = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Bold);
            this.superTabControl1.SelectedTabIndex = 0;
            this.superTabControl1.Size = new System.Drawing.Size(860, 311);
            this.superTabControl1.TabFont = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.superTabControl1.TabIndex = 21;
            this.superTabControl1.Tabs.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.superTabItem1,
            this.superTabItem2,
            this.superTabItem3});
            this.superTabControl1.TabStyle = DevComponents.DotNetBar.eSuperTabStyle.Office2010BackstageBlue;
            this.superTabControl1.Text = "superTabControl1";
            // 
            // superTabControlPanel2
            // 
            this.superTabControlPanel2.Controls.Add(this.labelX9);
            this.superTabControlPanel2.Controls.Add(this.SALDO_FINAL_TOTAL);
            this.superTabControlPanel2.Controls.Add(this.labelX8);
            this.superTabControlPanel2.Controls.Add(this.HABER_TOTAL);
            this.superTabControlPanel2.Controls.Add(this.labelX7);
            this.superTabControlPanel2.Controls.Add(this.DEBE_TOTAL);
            this.superTabControlPanel2.Controls.Add(this.labelX6);
            this.superTabControlPanel2.Controls.Add(this.SALDO_INICIAL_TOTAL);
            this.superTabControlPanel2.Controls.Add(buttonX26);
            this.superTabControlPanel2.Controls.Add(this.buttonX21);
            this.superTabControlPanel2.Controls.Add(this.circularProgress2);
            this.superTabControlPanel2.Controls.Add(this.buttonX10);
            this.superTabControlPanel2.Controls.Add(this.buttonX12);
            this.superTabControlPanel2.Controls.Add(this.dtgBalanza);
            this.superTabControlPanel2.Controls.Add(buttonX9);
            this.superTabControlPanel2.Controls.Add(buttonX11);
            this.superTabControlPanel2.Controls.Add(this.buttonX13);
            this.superTabControlPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.superTabControlPanel2.Location = new System.Drawing.Point(0, 25);
            this.superTabControlPanel2.Name = "superTabControlPanel2";
            this.superTabControlPanel2.Size = new System.Drawing.Size(860, 286);
            this.superTabControlPanel2.TabIndex = 0;
            this.superTabControlPanel2.TabItem = this.superTabItem2;
            // 
            // labelX9
            // 
            this.labelX9.AutoSize = true;
            this.labelX9.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.labelX9.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX9.ForeColor = System.Drawing.Color.Black;
            this.labelX9.Location = new System.Drawing.Point(505, 34);
            this.labelX9.Name = "labelX9";
            this.labelX9.Size = new System.Drawing.Size(55, 17);
            this.labelX9.TabIndex = 595;
            this.labelX9.Text = "Saldo Final";
            // 
            // SALDO_FINAL_TOTAL
            // 
            this.SALDO_FINAL_TOTAL.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.SALDO_FINAL_TOTAL.Border.Class = "TextBoxBorder";
            this.SALDO_FINAL_TOTAL.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.SALDO_FINAL_TOTAL.Enabled = false;
            this.SALDO_FINAL_TOTAL.ForeColor = System.Drawing.Color.Black;
            this.SALDO_FINAL_TOTAL.Location = new System.Drawing.Point(566, 31);
            this.SALDO_FINAL_TOTAL.Name = "SALDO_FINAL_TOTAL";
            this.SALDO_FINAL_TOTAL.Size = new System.Drawing.Size(140, 22);
            this.SALDO_FINAL_TOTAL.TabIndex = 596;
            this.SALDO_FINAL_TOTAL.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // labelX8
            // 
            this.labelX8.AutoSize = true;
            this.labelX8.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.labelX8.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX8.ForeColor = System.Drawing.Color.Black;
            this.labelX8.Location = new System.Drawing.Point(359, 34);
            this.labelX8.Name = "labelX8";
            this.labelX8.Size = new System.Drawing.Size(32, 17);
            this.labelX8.TabIndex = 593;
            this.labelX8.Text = "Haber";
            // 
            // HABER_TOTAL
            // 
            this.HABER_TOTAL.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.HABER_TOTAL.Border.Class = "TextBoxBorder";
            this.HABER_TOTAL.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.HABER_TOTAL.Enabled = false;
            this.HABER_TOTAL.ForeColor = System.Drawing.Color.Black;
            this.HABER_TOTAL.Location = new System.Drawing.Point(397, 31);
            this.HABER_TOTAL.Name = "HABER_TOTAL";
            this.HABER_TOTAL.Size = new System.Drawing.Size(102, 22);
            this.HABER_TOTAL.TabIndex = 594;
            this.HABER_TOTAL.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // labelX7
            // 
            this.labelX7.AutoSize = true;
            this.labelX7.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.labelX7.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX7.ForeColor = System.Drawing.Color.Black;
            this.labelX7.Location = new System.Drawing.Point(212, 34);
            this.labelX7.Name = "labelX7";
            this.labelX7.Size = new System.Drawing.Size(28, 17);
            this.labelX7.TabIndex = 591;
            this.labelX7.Text = "Debe";
            // 
            // DEBE_TOTAL
            // 
            this.DEBE_TOTAL.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.DEBE_TOTAL.Border.Class = "TextBoxBorder";
            this.DEBE_TOTAL.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.DEBE_TOTAL.Enabled = false;
            this.DEBE_TOTAL.ForeColor = System.Drawing.Color.Black;
            this.DEBE_TOTAL.Location = new System.Drawing.Point(245, 31);
            this.DEBE_TOTAL.Name = "DEBE_TOTAL";
            this.DEBE_TOTAL.Size = new System.Drawing.Size(102, 22);
            this.DEBE_TOTAL.TabIndex = 592;
            this.DEBE_TOTAL.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // labelX6
            // 
            this.labelX6.AutoSize = true;
            this.labelX6.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.labelX6.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX6.ForeColor = System.Drawing.Color.Black;
            this.labelX6.Location = new System.Drawing.Point(6, 34);
            this.labelX6.Name = "labelX6";
            this.labelX6.Size = new System.Drawing.Size(60, 17);
            this.labelX6.TabIndex = 589;
            this.labelX6.Text = "Saldo Inicial";
            // 
            // SALDO_INICIAL_TOTAL
            // 
            this.SALDO_INICIAL_TOTAL.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.SALDO_INICIAL_TOTAL.Border.Class = "TextBoxBorder";
            this.SALDO_INICIAL_TOTAL.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.SALDO_INICIAL_TOTAL.Enabled = false;
            this.SALDO_INICIAL_TOTAL.ForeColor = System.Drawing.Color.Black;
            this.SALDO_INICIAL_TOTAL.Location = new System.Drawing.Point(69, 31);
            this.SALDO_INICIAL_TOTAL.Name = "SALDO_INICIAL_TOTAL";
            this.SALDO_INICIAL_TOTAL.Size = new System.Drawing.Size(140, 22);
            this.SALDO_INICIAL_TOTAL.TabIndex = 590;
            this.SALDO_INICIAL_TOTAL.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // buttonX21
            // 
            this.buttonX21.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.buttonX21.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.buttonX21.Image = global::Desarrollo.Properties.Resources.sat_18;
            this.buttonX21.Location = new System.Drawing.Point(227, 3);
            this.buttonX21.Name = "buttonX21";
            this.buttonX21.Size = new System.Drawing.Size(92, 24);
            this.buttonX21.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.buttonX21.TabIndex = 579;
            this.buttonX21.Text = "Generar XML";
            this.buttonX21.Click += new System.EventHandler(this.buttonX21_Click);
            // 
            // circularProgress2
            // 
            this.circularProgress2.AnimationSpeed = 50;
            this.circularProgress2.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.circularProgress2.BackgroundStyle.BackgroundImageAlpha = ((byte)(64));
            this.circularProgress2.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.circularProgress2.FocusCuesEnabled = false;
            this.circularProgress2.Font = new System.Drawing.Font("Segoe UI", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.circularProgress2.Location = new System.Drawing.Point(196, 3);
            this.circularProgress2.Name = "circularProgress2";
            this.circularProgress2.ProgressBarType = DevComponents.DotNetBar.eCircularProgressType.Donut;
            this.circularProgress2.ProgressColor = System.Drawing.Color.Orange;
            this.circularProgress2.ProgressTextFormat = "{0}";
            this.circularProgress2.Size = new System.Drawing.Size(25, 24);
            this.circularProgress2.Style = DevComponents.DotNetBar.eDotNetBarStyle.OfficeXP;
            this.circularProgress2.TabIndex = 577;
            this.circularProgress2.Value = 100;
            this.circularProgress2.Visible = false;
            // 
            // buttonX10
            // 
            this.buttonX10.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.buttonX10.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.buttonX10.Image = global::Desarrollo.Properties.Resources.visual;
            this.buttonX10.Location = new System.Drawing.Point(87, 3);
            this.buttonX10.Name = "buttonX10";
            this.buttonX10.Size = new System.Drawing.Size(106, 24);
            this.buttonX10.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.buttonX10.TabIndex = 575;
            this.buttonX10.Text = "Importar: Visual";
            this.buttonX10.Click += new System.EventHandler(this.buttonX10_Click);
            // 
            // buttonX12
            // 
            this.buttonX12.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.buttonX12.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.buttonX12.Image = global::Desarrollo.Properties.Resources.actualizar_18;
            this.buttonX12.Location = new System.Drawing.Point(3, 3);
            this.buttonX12.Name = "buttonX12";
            this.buttonX12.Size = new System.Drawing.Size(78, 24);
            this.buttonX12.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.buttonX12.TabIndex = 572;
            this.buttonX12.Text = "Refrescar";
            this.buttonX12.Click += new System.EventHandler(this.buttonX12_Click);
            // 
            // dtgBalanza
            // 
            this.dtgBalanza.AllowUserToAddRows = false;
            this.dtgBalanza.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dtgBalanza.BackgroundColor = System.Drawing.Color.White;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dtgBalanza.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.dtgBalanza.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dtgBalanza.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.ROW_ID_b,
            this.numCtaField_b,
            this.saldoIniField_b,
            this.debeField_b,
            this.haberField_b,
            this.saldoFinField_b,
            this.SUMMARY_ACCOUNT});
            dataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle6.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle6.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle6.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle6.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle6.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle6.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dtgBalanza.DefaultCellStyle = dataGridViewCellStyle6;
            this.dtgBalanza.EnableHeadersVisualStyles = false;
            this.dtgBalanza.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(170)))), ((int)(((byte)(170)))), ((int)(((byte)(170)))));
            this.dtgBalanza.Location = new System.Drawing.Point(3, 59);
            this.dtgBalanza.Name = "dtgBalanza";
            dataGridViewCellStyle7.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle7.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle7.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle7.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle7.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle7.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle7.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dtgBalanza.RowHeadersDefaultCellStyle = dataGridViewCellStyle7;
            this.dtgBalanza.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dtgBalanza.Size = new System.Drawing.Size(854, 224);
            this.dtgBalanza.TabIndex = 571;
            this.dtgBalanza.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dtgBalanza_CellDoubleClick);
            // 
            // ROW_ID_b
            // 
            this.ROW_ID_b.HeaderText = "ROW_ID";
            this.ROW_ID_b.Name = "ROW_ID_b";
            this.ROW_ID_b.Visible = false;
            // 
            // numCtaField_b
            // 
            this.numCtaField_b.HeaderText = "Cuenta";
            this.numCtaField_b.Name = "numCtaField_b";
            this.numCtaField_b.ReadOnly = true;
            this.numCtaField_b.Width = 200;
            // 
            // saldoIniField_b
            // 
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.saldoIniField_b.DefaultCellStyle = dataGridViewCellStyle2;
            this.saldoIniField_b.HeaderText = "Saldo Inicial";
            this.saldoIniField_b.Name = "saldoIniField_b";
            this.saldoIniField_b.ReadOnly = true;
            // 
            // debeField_b
            // 
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.debeField_b.DefaultCellStyle = dataGridViewCellStyle3;
            this.debeField_b.HeaderText = "Debe";
            this.debeField_b.Name = "debeField_b";
            this.debeField_b.ReadOnly = true;
            // 
            // haberField_b
            // 
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.haberField_b.DefaultCellStyle = dataGridViewCellStyle4;
            this.haberField_b.HeaderText = "Haber";
            this.haberField_b.Name = "haberField_b";
            this.haberField_b.ReadOnly = true;
            // 
            // saldoFinField_b
            // 
            dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.saldoFinField_b.DefaultCellStyle = dataGridViewCellStyle5;
            this.saldoFinField_b.HeaderText = "Saldo Final";
            this.saldoFinField_b.Name = "saldoFinField_b";
            this.saldoFinField_b.ReadOnly = true;
            // 
            // SUMMARY_ACCOUNT
            // 
            this.SUMMARY_ACCOUNT.HeaderText = "SUMMARY_ACCOUNT";
            this.SUMMARY_ACCOUNT.Name = "SUMMARY_ACCOUNT";
            this.SUMMARY_ACCOUNT.Visible = false;
            // 
            // buttonX13
            // 
            this.buttonX13.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.buttonX13.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.buttonX13.Image = global::Desarrollo.Properties.Resources.Nueno_18;
            this.buttonX13.Location = new System.Drawing.Point(512, 3);
            this.buttonX13.Name = "buttonX13";
            this.buttonX13.Size = new System.Drawing.Size(74, 24);
            this.buttonX13.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.buttonX13.TabIndex = 573;
            this.buttonX13.Text = "Agregar";
            this.buttonX13.Visible = false;
            this.buttonX13.Click += new System.EventHandler(this.buttonX13_Click);
            // 
            // superTabItem2
            // 
            this.superTabItem2.AttachedControl = this.superTabControlPanel2;
            this.superTabItem2.GlobalItem = false;
            this.superTabItem2.Name = "superTabItem2";
            this.superTabItem2.Text = "Balanza de Comprobaci�n";
            // 
            // superTabControlPanel1
            // 
            this.superTabControlPanel1.Controls.Add(buttonX14);
            this.superTabControlPanel1.Controls.Add(this.chkPeriodo);
            this.superTabControlPanel1.Controls.Add(buttonX27);
            this.superTabControlPanel1.Controls.Add(this.buttonX24);
            this.superTabControlPanel1.Controls.Add(this.buttonX20);
            this.superTabControlPanel1.Controls.Add(this.circularProgress1);
            this.superTabControlPanel1.Controls.Add(this.dtgCuentas);
            this.superTabControlPanel1.Controls.Add(buttonX4);
            this.superTabControlPanel1.Controls.Add(this.buttonX5);
            this.superTabControlPanel1.Controls.Add(buttonX6);
            this.superTabControlPanel1.Controls.Add(this.buttonX8);
            this.superTabControlPanel1.Controls.Add(this.buttonX7);
            this.superTabControlPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.superTabControlPanel1.Location = new System.Drawing.Point(0, 25);
            this.superTabControlPanel1.Name = "superTabControlPanel1";
            this.superTabControlPanel1.Size = new System.Drawing.Size(860, 286);
            this.superTabControlPanel1.TabIndex = 1;
            this.superTabControlPanel1.TabItem = this.superTabItem1;
            // 
            // chkPeriodo
            // 
            this.chkPeriodo.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.chkPeriodo.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.chkPeriodo.ForeColor = System.Drawing.Color.Black;
            this.chkPeriodo.Location = new System.Drawing.Point(3, 33);
            this.chkPeriodo.Name = "chkPeriodo";
            this.chkPeriodo.Size = new System.Drawing.Size(235, 18);
            this.chkPeriodo.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.chkPeriodo.TabIndex = 582;
            this.chkPeriodo.Text = "Mostrar Cuentas con falta de Agrupador";
            this.chkPeriodo.CheckedChanged += new System.EventHandler(this.checkBoxX1_CheckedChanged);
            // 
            // buttonX24
            // 
            this.buttonX24.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.buttonX24.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.buttonX24.Image = global::Desarrollo.Properties.Resources.visual;
            this.buttonX24.Location = new System.Drawing.Point(342, 3);
            this.buttonX24.Name = "buttonX24";
            this.buttonX24.Size = new System.Drawing.Size(124, 24);
            this.buttonX24.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.buttonX24.TabIndex = 579;
            this.buttonX24.Text = "Asignar Agrupador";
            this.buttonX24.Tooltip = "Seleccione las cuentas contables y presione este boton para selecionar el c�digo " +
    "de agrupador el cual pertenece.";
            this.buttonX24.Click += new System.EventHandler(this.buttonX24_Click);
            // 
            // buttonX20
            // 
            this.buttonX20.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.buttonX20.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.buttonX20.Image = global::Desarrollo.Properties.Resources.sat_18;
            this.buttonX20.Location = new System.Drawing.Point(244, 3);
            this.buttonX20.Name = "buttonX20";
            this.buttonX20.Size = new System.Drawing.Size(92, 24);
            this.buttonX20.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.buttonX20.TabIndex = 578;
            this.buttonX20.Text = "Generar XML";
            this.buttonX20.Click += new System.EventHandler(this.buttonX20_Click);
            // 
            // circularProgress1
            // 
            this.circularProgress1.AnimationSpeed = 50;
            this.circularProgress1.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.circularProgress1.BackgroundStyle.BackgroundImageAlpha = ((byte)(64));
            this.circularProgress1.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.circularProgress1.FocusCuesEnabled = false;
            this.circularProgress1.Font = new System.Drawing.Font("Segoe UI", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.circularProgress1.Location = new System.Drawing.Point(213, 2);
            this.circularProgress1.Name = "circularProgress1";
            this.circularProgress1.ProgressBarType = DevComponents.DotNetBar.eCircularProgressType.Donut;
            this.circularProgress1.ProgressColor = System.Drawing.Color.Orange;
            this.circularProgress1.ProgressTextFormat = "{0}";
            this.circularProgress1.Size = new System.Drawing.Size(25, 24);
            this.circularProgress1.Style = DevComponents.DotNetBar.eDotNetBarStyle.OfficeXP;
            this.circularProgress1.TabIndex = 570;
            this.circularProgress1.Value = 100;
            this.circularProgress1.Visible = false;
            // 
            // dtgCuentas
            // 
            this.dtgCuentas.AllowUserToAddRows = false;
            this.dtgCuentas.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dtgCuentas.BackgroundColor = System.Drawing.Color.White;
            dataGridViewCellStyle8.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle8.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle8.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle8.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle8.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle8.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle8.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dtgCuentas.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle8;
            this.dtgCuentas.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dtgCuentas.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.ROW_ID,
            this.codAgrupField,
            this.numCtaField,
            this.descField,
            this.subCtaDeField,
            this.nivelField,
            this.naturField});
            dataGridViewCellStyle9.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle9.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle9.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle9.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle9.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle9.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle9.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dtgCuentas.DefaultCellStyle = dataGridViewCellStyle9;
            this.dtgCuentas.EnableHeadersVisualStyles = false;
            this.dtgCuentas.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(170)))), ((int)(((byte)(170)))), ((int)(((byte)(170)))));
            this.dtgCuentas.Location = new System.Drawing.Point(3, 58);
            this.dtgCuentas.Name = "dtgCuentas";
            dataGridViewCellStyle10.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle10.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle10.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle10.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle10.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle10.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle10.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dtgCuentas.RowHeadersDefaultCellStyle = dataGridViewCellStyle10;
            this.dtgCuentas.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dtgCuentas.Size = new System.Drawing.Size(854, 225);
            this.dtgCuentas.TabIndex = 6;
            this.dtgCuentas.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dtgCuentas_CellDoubleClick);
            // 
            // ROW_ID
            // 
            this.ROW_ID.HeaderText = "ROW_ID";
            this.ROW_ID.Name = "ROW_ID";
            this.ROW_ID.Visible = false;
            // 
            // codAgrupField
            // 
            this.codAgrupField.HeaderText = "Agrupador";
            this.codAgrupField.Name = "codAgrupField";
            this.codAgrupField.ReadOnly = true;
            this.codAgrupField.Width = 80;
            // 
            // numCtaField
            // 
            this.numCtaField.HeaderText = "Cuenta";
            this.numCtaField.Name = "numCtaField";
            this.numCtaField.ReadOnly = true;
            this.numCtaField.Width = 200;
            // 
            // descField
            // 
            this.descField.HeaderText = "Descripci�n";
            this.descField.Name = "descField";
            this.descField.ReadOnly = true;
            this.descField.Width = 300;
            // 
            // subCtaDeField
            // 
            this.subCtaDeField.HeaderText = "SubCuenta";
            this.subCtaDeField.Name = "subCtaDeField";
            this.subCtaDeField.ReadOnly = true;
            this.subCtaDeField.Width = 70;
            // 
            // nivelField
            // 
            this.nivelField.HeaderText = "Nivel";
            this.nivelField.Name = "nivelField";
            this.nivelField.ReadOnly = true;
            this.nivelField.Width = 40;
            // 
            // naturField
            // 
            this.naturField.HeaderText = "Naturaleza";
            this.naturField.Name = "naturField";
            this.naturField.ReadOnly = true;
            this.naturField.Width = 70;
            // 
            // buttonX5
            // 
            this.buttonX5.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.buttonX5.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.buttonX5.Image = global::Desarrollo.Properties.Resources.visual;
            this.buttonX5.Location = new System.Drawing.Point(87, 3);
            this.buttonX5.Name = "buttonX5";
            this.buttonX5.Size = new System.Drawing.Size(119, 24);
            this.buttonX5.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.buttonX5.SubItems.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.buttonItem5,
            this.buttonItem4});
            this.buttonX5.TabIndex = 11;
            this.buttonX5.Text = "Importar: Visual";
            this.buttonX5.Click += new System.EventHandler(this.buttonX5_Click);
            // 
            // buttonItem5
            // 
            this.buttonItem5.GlobalItem = false;
            this.buttonItem5.Image = global::Desarrollo.Properties.Resources.wizard;
            this.buttonItem5.Name = "buttonItem5";
            this.buttonItem5.Text = "Generar Plantilla";
            this.buttonItem5.Click += new System.EventHandler(this.buttonItem5_Click);
            // 
            // buttonItem4
            // 
            this.buttonItem4.GlobalItem = false;
            this.buttonItem4.Image = global::Desarrollo.Properties.Resources.excel;
            this.buttonItem4.ImageSmall = global::Desarrollo.Properties.Resources.excel;
            this.buttonItem4.Name = "buttonItem4";
            this.buttonItem4.Text = "Importar Excel";
            this.buttonItem4.Click += new System.EventHandler(this.buttonItem4_Click);
            // 
            // buttonX8
            // 
            this.buttonX8.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.buttonX8.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.buttonX8.Image = global::Desarrollo.Properties.Resources.actualizar_18;
            this.buttonX8.Location = new System.Drawing.Point(3, 3);
            this.buttonX8.Name = "buttonX8";
            this.buttonX8.Size = new System.Drawing.Size(78, 24);
            this.buttonX8.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.buttonX8.TabIndex = 9;
            this.buttonX8.Text = "Refrescar";
            this.buttonX8.Click += new System.EventHandler(this.buttonX8_Click);
            // 
            // buttonX7
            // 
            this.buttonX7.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.buttonX7.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.buttonX7.Image = global::Desarrollo.Properties.Resources.Nueno_18;
            this.buttonX7.Location = new System.Drawing.Point(342, 32);
            this.buttonX7.Name = "buttonX7";
            this.buttonX7.Size = new System.Drawing.Size(74, 24);
            this.buttonX7.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.buttonX7.TabIndex = 9;
            this.buttonX7.Text = "Agregar";
            this.buttonX7.Visible = false;
            this.buttonX7.Click += new System.EventHandler(this.buttonX7_Click);
            // 
            // superTabItem1
            // 
            this.superTabItem1.AttachedControl = this.superTabControlPanel1;
            this.superTabItem1.GlobalItem = false;
            this.superTabItem1.Name = "superTabItem1";
            this.superTabItem1.Text = "Cuentas Contables";
            // 
            // superTabControlPanel4
            // 
            this.superTabControlPanel4.Controls.Add(this.buttonX23);
            this.superTabControlPanel4.Controls.Add(this.dtgValidador);
            this.superTabControlPanel4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.superTabControlPanel4.Location = new System.Drawing.Point(0, 0);
            this.superTabControlPanel4.Name = "superTabControlPanel4";
            this.superTabControlPanel4.Size = new System.Drawing.Size(860, 311);
            this.superTabControlPanel4.TabIndex = 0;
            this.superTabControlPanel4.TabItem = this.superTabItem3;
            // 
            // buttonX23
            // 
            this.buttonX23.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.buttonX23.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.buttonX23.Image = global::Desarrollo.Properties.Resources.actualizar_18;
            this.buttonX23.Location = new System.Drawing.Point(3, 3);
            this.buttonX23.Name = "buttonX23";
            this.buttonX23.Size = new System.Drawing.Size(78, 24);
            this.buttonX23.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.buttonX23.TabIndex = 580;
            this.buttonX23.Text = "Refrescar";
            this.buttonX23.Click += new System.EventHandler(this.buttonX23_Click);
            // 
            // dtgValidador
            // 
            this.dtgValidador.AllowUserToAddRows = false;
            this.dtgValidador.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dtgValidador.BackgroundColor = System.Drawing.Color.White;
            dataGridViewCellStyle11.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle11.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle11.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle11.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle11.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle11.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle11.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dtgValidador.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle11;
            this.dtgValidador.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dtgValidador.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn1,
            this.TIPO,
            this.CUENTA,
            this.MENSAJE});
            dataGridViewCellStyle12.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle12.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle12.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle12.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle12.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle12.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle12.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dtgValidador.DefaultCellStyle = dataGridViewCellStyle12;
            this.dtgValidador.EnableHeadersVisualStyles = false;
            this.dtgValidador.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(170)))), ((int)(((byte)(170)))), ((int)(((byte)(170)))));
            this.dtgValidador.Location = new System.Drawing.Point(3, 33);
            this.dtgValidador.Name = "dtgValidador";
            dataGridViewCellStyle13.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle13.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle13.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle13.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle13.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle13.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle13.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dtgValidador.RowHeadersDefaultCellStyle = dataGridViewCellStyle13;
            this.dtgValidador.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dtgValidador.Size = new System.Drawing.Size(854, 275);
            this.dtgValidador.TabIndex = 7;
            // 
            // dataGridViewTextBoxColumn1
            // 
            this.dataGridViewTextBoxColumn1.HeaderText = "ROW_ID";
            this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
            this.dataGridViewTextBoxColumn1.Visible = false;
            // 
            // TIPO
            // 
            this.TIPO.HeaderText = "Tipo";
            this.TIPO.Name = "TIPO";
            this.TIPO.ReadOnly = true;
            // 
            // CUENTA
            // 
            this.CUENTA.HeaderText = "Cuenta";
            this.CUENTA.Name = "CUENTA";
            this.CUENTA.ReadOnly = true;
            this.CUENTA.Width = 200;
            // 
            // MENSAJE
            // 
            this.MENSAJE.HeaderText = "Descripci�n";
            this.MENSAJE.Name = "MENSAJE";
            this.MENSAJE.ReadOnly = true;
            this.MENSAJE.Width = 400;
            // 
            // superTabItem3
            // 
            this.superTabItem3.AttachedControl = this.superTabControlPanel4;
            this.superTabItem3.GlobalItem = false;
            this.superTabItem3.Name = "superTabItem3";
            this.superTabItem3.Text = "Punto de Control";
            // 
            // Polizas
            // 
            this.Polizas.GlobalItem = false;
            this.Polizas.Name = "Polizas";
            this.Polizas.Text = "P�lizas";
            // 
            // ESTADO
            // 
            this.ESTADO.AutoCompleteCustomSource.AddRange(new string[] {
            "Abierto",
            "Cerrado"});
            this.ESTADO.DisplayMember = "Text";
            this.ESTADO.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.ESTADO.ForeColor = System.Drawing.Color.Black;
            this.ESTADO.FormattingEnabled = true;
            this.ESTADO.ItemHeight = 16;
            this.ESTADO.Items.AddRange(new object[] {
            this.comboItem3,
            this.comboItem4});
            this.ESTADO.Location = new System.Drawing.Point(613, 30);
            this.ESTADO.Name = "ESTADO";
            this.ESTADO.Size = new System.Drawing.Size(123, 22);
            this.ESTADO.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.ESTADO.TabIndex = 23;
            // 
            // comboItem3
            // 
            this.comboItem3.Text = "Abierto";
            // 
            // comboItem4
            // 
            this.comboItem4.Text = "Cerrado";
            // 
            // labelX5
            // 
            this.labelX5.AutoSize = true;
            this.labelX5.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.labelX5.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX5.ForeColor = System.Drawing.Color.Black;
            this.labelX5.Location = new System.Drawing.Point(567, 33);
            this.labelX5.Name = "labelX5";
            this.labelX5.Size = new System.Drawing.Size(35, 17);
            this.labelX5.TabIndex = 22;
            this.labelX5.Text = "Estado";
            // 
            // backgroundWorker1
            // 
            this.backgroundWorker1.WorkerReportsProgress = true;
            this.backgroundWorker1.WorkerSupportsCancellation = true;
            this.backgroundWorker1.DoWork += new System.ComponentModel.DoWorkEventHandler(this.backgroundWorker1_DoWork);
            this.backgroundWorker1.ProgressChanged += new System.ComponentModel.ProgressChangedEventHandler(this.backgroundWorker1_ProgressChanged);
            this.backgroundWorker1.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.backgroundWorker1_RunWorkerCompleted);
            // 
            // backgroundWorker2
            // 
            this.backgroundWorker2.WorkerReportsProgress = true;
            this.backgroundWorker2.WorkerSupportsCancellation = true;
            this.backgroundWorker2.DoWork += new System.ComponentModel.DoWorkEventHandler(this.backgroundWorker2_DoWork);
            this.backgroundWorker2.ProgressChanged += new System.ComponentModel.ProgressChangedEventHandler(this.backgroundWorker2_ProgressChanged);
            this.backgroundWorker2.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.backgroundWorker2_RunWorkerCompleted);
            // 
            // usarPeriodo
            // 
            this.usarPeriodo.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.usarPeriodo.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.usarPeriodo.ForeColor = System.Drawing.Color.Black;
            this.usarPeriodo.Location = new System.Drawing.Point(0, 56);
            this.usarPeriodo.Name = "usarPeriodo";
            this.usarPeriodo.Size = new System.Drawing.Size(432, 18);
            this.usarPeriodo.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.usarPeriodo.TabIndex = 583;
            this.usarPeriodo.Text = "Usar per�odos contables de Visual (Los periodos de arriba quedan desahabilitados)" +
    "";
            this.usarPeriodo.CheckedChanged += new System.EventHandler(this.usarPeriodo_CheckedChanged);
            // 
            // periodoInicial
            // 
            this.periodoInicial.AutoCompleteCustomSource.AddRange(new string[] {
            "01",
            "02",
            "03",
            "04",
            "05",
            "06",
            "07",
            "08",
            "09",
            "10",
            "11",
            "12"});
            this.periodoInicial.DisplayMember = "Text";
            this.periodoInicial.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.periodoInicial.ForeColor = System.Drawing.Color.Black;
            this.periodoInicial.FormattingEnabled = true;
            this.periodoInicial.ItemHeight = 16;
            this.periodoInicial.Location = new System.Drawing.Point(538, 54);
            this.periodoInicial.Name = "periodoInicial";
            this.periodoInicial.Size = new System.Drawing.Size(60, 22);
            this.periodoInicial.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.periodoInicial.TabIndex = 585;
            // 
            // labelX10
            // 
            this.labelX10.AutoSize = true;
            this.labelX10.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.labelX10.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX10.ForeColor = System.Drawing.Color.Black;
            this.labelX10.Location = new System.Drawing.Point(461, 57);
            this.labelX10.Name = "labelX10";
            this.labelX10.Size = new System.Drawing.Size(71, 17);
            this.labelX10.TabIndex = 584;
            this.labelX10.Text = "Periodo Inicial";
            // 
            // labelX11
            // 
            this.labelX11.AutoSize = true;
            this.labelX11.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.labelX11.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX11.ForeColor = System.Drawing.Color.Black;
            this.labelX11.Location = new System.Drawing.Point(604, 57);
            this.labelX11.Name = "labelX11";
            this.labelX11.Size = new System.Drawing.Size(66, 17);
            this.labelX11.TabIndex = 586;
            this.labelX11.Text = "Periodo Final";
            // 
            // periodoFinal
            // 
            this.periodoFinal.AutoCompleteCustomSource.AddRange(new string[] {
            "01",
            "02",
            "03",
            "04",
            "05",
            "06",
            "07",
            "08",
            "09",
            "10",
            "11",
            "12"});
            this.periodoFinal.DisplayMember = "Text";
            this.periodoFinal.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.periodoFinal.ForeColor = System.Drawing.Color.Black;
            this.periodoFinal.FormattingEnabled = true;
            this.periodoFinal.ItemHeight = 16;
            this.periodoFinal.Location = new System.Drawing.Point(676, 54);
            this.periodoFinal.Name = "periodoFinal";
            this.periodoFinal.Size = new System.Drawing.Size(60, 22);
            this.periodoFinal.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.periodoFinal.TabIndex = 587;
            // 
            // frmDeclaracion
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.BottomLeftCornerSize = 0;
            this.ClientSize = new System.Drawing.Size(860, 418);
            this.Controls.Add(this.periodoFinal);
            this.Controls.Add(this.labelX11);
            this.Controls.Add(this.periodoInicial);
            this.Controls.Add(this.labelX10);
            this.Controls.Add(this.usarPeriodo);
            this.Controls.Add(buttonX15);
            this.Controls.Add(this.ESTADO);
            this.Controls.Add(this.labelX5);
            this.Controls.Add(this.superTabControl1);
            this.Controls.Add(this.mesField);
            this.Controls.Add(this.labelX4);
            this.Controls.Add(this.totalCtasField);
            this.Controls.Add(this.labelX3);
            this.Controls.Add(this.anoField);
            this.Controls.Add(this.labelX2);
            this.Controls.Add(this.labelX1);
            this.Controls.Add(this.rFCField);
            this.Controls.Add(this.versionField);
            this.Controls.Add(this.label1);
            this.Controls.Add(buttonX3);
            this.Controls.Add(buttonX1);
            this.Controls.Add(buttonX2);
            this.Controls.Add(this.metroStatusBar1);
            this.Cursor = System.Windows.Forms.Cursors.Default;
            this.DoubleBuffered = true;
            this.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "frmDeclaracion";
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Declaraci�n";
            ((System.ComponentModel.ISupportInitialize)(this.superTabControl1)).EndInit();
            this.superTabControl1.ResumeLayout(false);
            this.superTabControlPanel2.ResumeLayout(false);
            this.superTabControlPanel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dtgBalanza)).EndInit();
            this.superTabControlPanel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dtgCuentas)).EndInit();
            this.superTabControlPanel4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dtgValidador)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevComponents.DotNetBar.Metro.MetroStatusBar metroStatusBar1;
        private DevComponents.DotNetBar.LabelItem labelItem1;
        private DevComponents.DotNetBar.Controls.ComboBoxEx mesField;
        private DevComponents.Editors.ComboItem comboItem1;
        private DevComponents.Editors.ComboItem comboItem2;
        private DevComponents.DotNetBar.LabelX labelX4;
        private DevComponents.DotNetBar.Controls.TextBoxX totalCtasField;
        private DevComponents.DotNetBar.LabelX labelX3;
        private DevComponents.DotNetBar.Controls.TextBoxX anoField;
        private DevComponents.DotNetBar.LabelX labelX2;
        private DevComponents.DotNetBar.LabelX labelX1;
        private DevComponents.DotNetBar.Controls.TextBoxX rFCField;
        private DevComponents.DotNetBar.Controls.TextBoxX versionField;
        private DevComponents.DotNetBar.LabelX label1;
        private DevComponents.DotNetBar.SuperTabControl superTabControl1;
        private DevComponents.DotNetBar.SuperTabControlPanel superTabControlPanel1;
        private DevComponents.DotNetBar.SuperTabItem superTabItem1;
        private DevComponents.DotNetBar.SuperTabControlPanel superTabControlPanel2;
        private DevComponents.DotNetBar.SuperTabItem superTabItem2;
        private DevComponents.DotNetBar.SuperTabItem Polizas;
        private DevComponents.DotNetBar.Controls.ComboBoxEx ESTADO;
        private DevComponents.Editors.ComboItem comboItem3;
        private DevComponents.Editors.ComboItem comboItem4;
        private DevComponents.DotNetBar.LabelX labelX5;
        private DevComponents.DotNetBar.Controls.DataGridViewX dtgCuentas;
        private DevComponents.DotNetBar.ButtonItem buttonItem1;
        private DevComponents.DotNetBar.ButtonX buttonX7;
        private System.Windows.Forms.FolderBrowserDialog folderBrowserDialog1;
        private DevComponents.DotNetBar.Controls.CircularProgress circularProgress1;
        private System.ComponentModel.BackgroundWorker backgroundWorker1;
        private DevComponents.DotNetBar.ButtonX buttonX5;
        private DevComponents.DotNetBar.ButtonX buttonX8;
        private DevComponents.DotNetBar.Controls.CircularProgress circularProgress2;
        private DevComponents.DotNetBar.ButtonItem buttonItem2;
        private DevComponents.DotNetBar.ButtonX buttonX10;
        private DevComponents.DotNetBar.ButtonX buttonX12;
        private DevComponents.DotNetBar.ButtonX buttonX13;
        private DevComponents.DotNetBar.Controls.DataGridViewX dtgBalanza;
        private System.ComponentModel.BackgroundWorker backgroundWorker2;
        private System.Windows.Forms.DataGridViewTextBoxColumn ROW_ID;
        private System.Windows.Forms.DataGridViewTextBoxColumn codAgrupField;
        private System.Windows.Forms.DataGridViewTextBoxColumn numCtaField;
        private System.Windows.Forms.DataGridViewTextBoxColumn descField;
        private System.Windows.Forms.DataGridViewTextBoxColumn subCtaDeField;
        private System.Windows.Forms.DataGridViewTextBoxColumn nivelField;
        private System.Windows.Forms.DataGridViewTextBoxColumn naturField;
        private DevComponents.Editors.ComboItem comboItem5;
        private DevComponents.Editors.ComboItem comboItem6;
        private DevComponents.Editors.ComboItem comboItem7;
        private DevComponents.Editors.ComboItem comboItem8;
        private DevComponents.Editors.ComboItem comboItem9;
        private DevComponents.Editors.ComboItem comboItem10;
        private DevComponents.Editors.ComboItem comboItem11;
        private DevComponents.Editors.ComboItem comboItem12;
        private DevComponents.Editors.ComboItem comboItem13;
        private DevComponents.Editors.ComboItem comboItem14;
        private DevComponents.DotNetBar.ButtonX buttonX21;
        private DevComponents.DotNetBar.ButtonX buttonX20;
        private DevComponents.DotNetBar.SuperTabControlPanel superTabControlPanel4;
        private DevComponents.DotNetBar.SuperTabItem superTabItem3;
        private DevComponents.DotNetBar.ButtonX buttonX23;
        private DevComponents.DotNetBar.Controls.DataGridViewX dtgValidador;
        private DevComponents.DotNetBar.ButtonX buttonX24;
        private DevComponents.DotNetBar.LabelX labelX9;
        private DevComponents.DotNetBar.Controls.TextBoxX SALDO_FINAL_TOTAL;
        private DevComponents.DotNetBar.LabelX labelX8;
        private DevComponents.DotNetBar.Controls.TextBoxX HABER_TOTAL;
        private DevComponents.DotNetBar.LabelX labelX7;
        private DevComponents.DotNetBar.Controls.TextBoxX DEBE_TOTAL;
        private DevComponents.DotNetBar.LabelX labelX6;
        private DevComponents.DotNetBar.Controls.TextBoxX SALDO_INICIAL_TOTAL;
        private DevComponents.DotNetBar.ButtonItem buttonItem4;
        private DevComponents.DotNetBar.ButtonItem buttonItem5;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn TIPO;
        private System.Windows.Forms.DataGridViewTextBoxColumn CUENTA;
        private System.Windows.Forms.DataGridViewTextBoxColumn MENSAJE;
        private DevComponents.DotNetBar.Controls.CheckBoxX chkPeriodo;
        private System.Windows.Forms.DataGridViewTextBoxColumn ROW_ID_b;
        private System.Windows.Forms.DataGridViewTextBoxColumn numCtaField_b;
        private System.Windows.Forms.DataGridViewTextBoxColumn saldoIniField_b;
        private System.Windows.Forms.DataGridViewTextBoxColumn debeField_b;
        private System.Windows.Forms.DataGridViewTextBoxColumn haberField_b;
        private System.Windows.Forms.DataGridViewTextBoxColumn saldoFinField_b;
        private System.Windows.Forms.DataGridViewTextBoxColumn SUMMARY_ACCOUNT;
        private DevComponents.DotNetBar.ButtonItem buttonItem3;
        private DevComponents.DotNetBar.Controls.CheckBoxX usarPeriodo;
        private DevComponents.DotNetBar.Controls.ComboBoxEx periodoInicial;
        private DevComponents.DotNetBar.LabelX labelX10;
        private DevComponents.DotNetBar.Controls.ComboBoxEx periodoFinalperiodoFinal;
        private DevComponents.DotNetBar.LabelX labelX11;
        private DevComponents.DotNetBar.Controls.ComboBoxEx periodoFinal;
        private DevComponents.Editors.ComboItem comboItem15;

    }
}