﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.OleDb;
using System.Data.SqlClient;
using System.Data;
using System.Windows;
using System.IO;
using System.Windows.Forms;
using System.Configuration;
using DevComponents.DotNetBar;
using System.Text.RegularExpressions;

namespace Desarrollo
{
    class cCONEXCION
    {

        public string sConn;
        public OleDbDataAdapter oDataAdapter;
        public OleDbConnection oConn;
        public string sTipo;
        public string sServer;
        public string sDatabase;
        public string sUsername;
        public string sPassword;
        public string no_existe_BD = "";

        public cCONEXCION()
        {

            leer_datos();
            crear_sConn();
        }
        public void crear_sConn()
        {
            sTipo = "SQLSERVER";
            sConn = @"provider=sqloledb;server=" + sServer + ";database=" + sDatabase + ";uid=" + sUsername + ";pwd=" + sPassword + ";";
        }
        public cCONEXCION(string pConn)
        {
            this.sConn = pConn;

        }
        public bool verificar(string USUARIO, string PASSWORD)
        {
            try
            {
                oConn = new OleDbConnection();
                oConn.ConnectionString = System.Configuration.ConfigurationManager.AppSettings["conexcion"].ToString();
                OleDbConnectionStringBuilder oString = new OleDbConnectionStringBuilder(oConn.ConnectionString);
                string sConn_local = @"provider=sqloledb;server=" + System.Configuration.ConfigurationManager.AppSettings["SERVER"].ToString()
                    + ";database=" + System.Configuration.ConfigurationManager.AppSettings["DATA_BASE"].ToString()
                    + ";uid=" + USUARIO
                    + ";pwd=" + PASSWORD + ";";
                oConn.ConnectionString = sConn_local;
                oConn.Open();
                return true;
            }
            catch
            {
                return false;
            }
        }
        public cCONEXCION(string sServerp, string sDatabasep, string sUsernamep, string sPasswordp)
        {
            this.sServer = sServerp;
            this.sDatabase = sDatabasep;
            this.sUsername = sUsernamep;
            this.sPassword = sPasswordp;            
        }

        //Buscar Datos
        public void leer_datos()
        {
            sServer = ConfigurationManager.AppSettings.Get("SERVER").ToString();
            sDatabase = ConfigurationManager.AppSettings.Get("DATA_BASE").ToString();
            sUsername = ConfigurationManager.AppSettings.Get("USER").ToString();
            sPassword = ConfigurationManager.AppSettings.Get("PASSWORD").ToString();
        }

        public bool verificar_existencia(string sPath)
        {
            if (!File.Exists(sPath))
            {
                MessageBoxEx.Show("Error: La Base de datos no fue encontrada.", Application.ProductName + "-" + Application.ProductVersion, MessageBoxButtons.OK, MessageBoxIcon.Error);

            }
            return File.Exists(sPath);
        }
        public bool CrearConexion()
        {
            if (sConn == null)
            {
                return false;
            }
            oConn = new OleDbConnection(sConn);
            try
            {
                oConn.Open();
                return true;
            }
            catch (OleDbException e)
            {
                MessageBoxEx.Show(e.Message, "Exepción " + Application.ProductName + "-" + Application.ProductVersion, MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return false;
            }
        }

        public void DestruirConexion()
        {
            oConn = null;
        }

        public DataTable EjecutarConsulta(string pQuery)
        {
            try
            {
                DataTable oDT = new DataTable();
                //Ejecutar para SQL Server
                oDT = new DataTable();
                oConn = new OleDbConnection(sConn);
                oDataAdapter = new OleDbDataAdapter();
                oDataAdapter.SelectCommand = new OleDbCommand(pQuery, oConn);
                oDataAdapter.Fill(oDT);
                return oDT;
            }
            catch (OleDbException e)
            {
                MessageBoxEx.Show(e.Message + "\n" + pQuery, "Exepción " + Application.ProductName + "-" + Application.ProductVersion, MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return null;
            }

        }


        public bool EjecutarCommando(OleDbCommand pQuery)
        {
            try
            {
                DataTable oDT = new DataTable();
                //Ejecutar para SQL Server
                oDT = new DataTable();
                oConn = new OleDbConnection(sConn);
                oDataAdapter = new OleDbDataAdapter();
                pQuery.Connection = oConn;
                oDataAdapter.SelectCommand = pQuery;
                oDataAdapter.Fill(oDT);
                return true;
            }
            catch (OleDbException e)
            {
                MessageBoxEx.Show(e.Message + "\n" + pQuery, "Exepción " + Application.ProductName + "-" + Application.ProductVersion, MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return false;
            }
        }

        public string Trunca_y_formatea(decimal numero)
        {
            string resultado = "0.0000";
            decimal numero_f = Truncar_decimales(decimal.Parse(numero.ToString()));
            resultado = formato_decimal(numero_f.ToString(), "N");
            return (resultado);
        }

        public decimal Truncar_decimales(decimal numero)
        {
            int numero_truncar = 100;
            decimal parte_decimal = 0, pasar_a_decimal = 0, resultado = 0;
            int parte_entera, separar_2_primeros_decimales;

            parte_entera = (int)numero;
            if (parte_entera != 0)
            {
                parte_decimal = numero % parte_entera;
            }
            else
            {
                parte_decimal = numero;
            }
            separar_2_primeros_decimales = (int)(parte_decimal * numero_truncar);
            pasar_a_decimal = (decimal)separar_2_primeros_decimales / numero_truncar;
            resultado = (decimal)parte_entera + pasar_a_decimal;

            return (resultado);
        }

        public string formato_decimal(string valor, string con_coma, int cantidad_decimales = 2)
        {
            string conversion;
            double Plantilla;
            if (valor.Trim() == "")
            {
                valor = "0";
            }
            Plantilla = double.Parse(valor.ToString());
            if (con_coma == "")
            {
                if (cantidad_decimales == 2)
                {
                    conversion = Plantilla.ToString("#,###,###,##0.00");
                }
                else
                {
                    conversion = Plantilla.ToString("#,###,###,##0.0000");
                }
            }
            else
            {

                if (cantidad_decimales == 2)
                {
                    conversion = Plantilla.ToString("#########0.00");
                }
                else
                {
                    conversion = Plantilla.ToString("#########0.0000");
                }
            }
            return conversion;
        }

        public string IsNullNumero(object Expression)
        {
            string valor = Trunca_y_formatea(1);
            if (Expression != null)
            {
                if (Expression.ToString() == "")
                {
                    valor = Trunca_y_formatea(0);
                }
                else
                {
                    valor = Expression.ToString();
                }
            }
            else
            {
                if (Expression.ToString() == "")
                {
                    valor = Trunca_y_formatea(0);
                }
            }
            return valor;
        }

        public string IsNull(object Expression)
        {
            string valor = "";
            if (Expression != null)
            {
                if (Expression.ToString() == "01/01/1900 00:00:00")
                {
                    valor = "";
                }
                else
                {
                    valor = Expression.ToString();
                }
            }
            return valor;
        }

        public void ReplaceInFile(string filePath, string searchText, string replaceText)
        {
            StreamReader reader = new StreamReader(filePath);
            string content = reader.ReadToEnd();
            reader.Close();

            content = Regex.Replace(content, searchText, replaceText);

            StreamWriter writer = new StreamWriter(filePath);
            writer.Write(content);
            writer.Close();
        }

        public bool IsNumeric(object Expression)
        {
            bool isNum;
            double retNum;

            isNum = Double.TryParse(Convert.ToString(Expression), System.Globalization.NumberStyles.Any, System.Globalization.NumberFormatInfo.InvariantInfo, out retNum);
            return isNum;
        }

        public string convertir_fecha(DateTime pfecha, string como = "n")
        {
            string fecha = "";
            //SQLServer
            fecha = "CONVERT(DATETIME,'" + pfecha.Year.ToString() + "-" + pfecha.Month.ToString() + "-" + pfecha.Day.ToString() + " " + pfecha.Hour.ToString() + ":" + pfecha.Minute.ToString() + ":" + pfecha.Second.ToString() + "',102)";
            if (como == "i")
            {
                fecha = "CONVERT(DATETIME,'" + pfecha.Year.ToString() + "-" + pfecha.Month.ToString() + "-" + pfecha.Day.ToString() + " 00:00:00',102)";
            }
            if (como == "f")
            {
                fecha = "CONVERT(DATETIME,'" + pfecha.Year.ToString() + "-" + pfecha.Month.ToString() + "-" + pfecha.Day.ToString() + " 23:59:59',102)";
            }
            return fecha;
        }

        public string convertir_cadena_fecha(string fecha)
        {
            try
            {
                DateTime fecha_tr = DateTime.Parse(fecha);
                return convertir_fecha(fecha_tr, "");
            }
            catch
            {
                return "NULL";
            }
        }
    }

}
