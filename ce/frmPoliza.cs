using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevComponents.DotNetBar;
using System.IO;
using OfficeOpenXml;
using System.Xml.Serialization;
using DevComponents.DotNetBar.Controls;
using System.Linq;
using Generales;
namespace Desarrollo
{
    public partial class frmPoliza : DevComponents.DotNetBar.Metro.MetroForm
    {
        bool modificado = false;
        string ROW_ID_CATALOGO;
        string USUARIO;

        cPoliza oObjeto = new cPoliza();
        cCONEXCION oData;
        public frmPoliza(string ROW_ID_CATALOGOp, string USUARIOp)
        {
            oObjeto = new cPoliza();
            ROW_ID_CATALOGO = ROW_ID_CATALOGOp;
            USUARIO = USUARIOp;
            InitializeComponent();
            //cargar_seguridad();
            limpiar();
        }

        //private void cargar_seguridad()
        //{
        //    ASIGNADO.Items.Clear();
        //    cCE_SEGURIDAD ocVENDOR_BANCO_SEGURIDAD = new cCE_SEGURIDAD(oData);
        //    foreach (cCE_SEGURIDAD registro in ocVENDOR_BANCO_SEGURIDAD.todos(""))
        //    {
        //        ASIGNADO.Items.Add(registro.USUARIO);
        //    }
        //}

        public frmPoliza(string ROW_ID_CATALOGOp, string ROW_ID, string USUARIOp)
        {
            ROW_ID_CATALOGO = ROW_ID_CATALOGOp;
            USUARIO = USUARIOp;
            oObjeto = new cPoliza();
            InitializeComponent();
            //cargar_seguridad();
            cargar(ROW_ID);
        }

        #region Acciones
        private void Solo_Numero_KeyPress(object sender, KeyPressEventArgs e)
        {
            try
            {
                modificado = true;
                if (!char.IsDigit(e.KeyChar) && !char.IsControl(e.KeyChar) && !char.IsPunctuation(e.KeyChar)) e.Handled = true;
            }
            catch
            {
                e.Handled = false;
            }
        }
        private void COSTO_Leave(object sender, EventArgs e)
        {
            TextBox tb = (TextBox)sender;
            tb.Text = oData.formato_decimal(tb.Text, "");
        }
        private void cargar(string ROW_IDp)
        {
            if (oObjeto.cargar(ROW_IDp))
            {
                ROW_ID_CATALOGO = oObjeto.ROW_ID_CATALOGO;
                tipoField.Text = oObjeto.tipoField;
                numField.Text = oObjeto.numField;
                fechaField.Value = oObjeto.fechaField;
                conceptoField.Text=oObjeto.conceptoField;
                ASIGNADO.Text = oObjeto.ASIGNADO;
                cargar_transacciones();
            }

        }

        private void guardar()
        {            
            
            oObjeto.ROW_ID_CATALOGO = ROW_ID_CATALOGO;
            oObjeto.tipoField = tipoField.Text;
            oObjeto.numField = numField.Text;
            oObjeto.fechaField = fechaField.Value;
            oObjeto.ASIGNADO = ASIGNADO.Text;
            oObjeto.conceptoField = conceptoField.Text;
            if (oObjeto.guardar())
            {
                labelItem1.Text = "Datos Actualizados " + DateTime.Now.ToShortDateString() + " " + DateTime.Now.ToLongTimeString();
                mostrar_mensaje("Datos Actualizados ", false);
                modificado = false;
            }

        }
        
        private void mostrar_mensaje(string mensaje, bool mostrar_error)
        {
            ToastNotification.Show(this,
                mensaje,
                mostrar_error ? global::Desarrollo.Properties.Resources.error : global::Desarrollo.Properties.Resources.guardar,
                3000,
                mostrar_error ? eToastGlowColor.Red : eToastGlowColor.Green,
                eToastPosition.TopCenter);
        }
        public void limpiar()
        {
            if (modificado)
            {

                DialogResult Resultado = MessageBoxEx.Show("�Desea guardar los cambios?", Application.ProductName + "-" + Application.ProductVersion, MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                if (Resultado.ToString() == "Yes")
                {
                    guardar();
                }
            }
            modificado = false;
            oObjeto = new cPoliza();
            
            foreach (var c in this.Controls)
            {
                if (c is TextBox)
                {
                    ((TextBox)c).Text = "";
                }
            }
            dtgTransaccion.Rows.Clear();

        }
        

        private void eliminar()
        {
            DialogResult Resultado = MessageBoxEx.Show("�Esta seguro de eliminar?", Application.ProductName + "-" + Application.ProductVersion, MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            if (Resultado.ToString() != "Yes")
            {
                return;
            }
            if (oObjeto.eliminar(oObjeto.ROW_ID))
            {
                modificado = false;
                
                MessageBoxEx.Show("Eliminado.", Application.ProductName + "-" + Application.ProductVersion, MessageBoxButtons.OK, MessageBoxIcon.Information);
                limpiar();
                
            }
        }
        #endregion

        private void buttonX3_Click(object sender, EventArgs e)
        {
            eliminar();
        }

        private void buttonX2_Click(object sender, EventArgs e)
        {
            limpiar();
        }

        private void buttonX1_Click(object sender, EventArgs e)
        {
            guardar();
        }

        private void buttonX18_Click(object sender, EventArgs e)
        {
            cargar_transacciones();
        }

        private void cargar_transacciones()
        {
            if (oObjeto.ROW_ID != "")
            {
                dtgTransaccion.Rows.Clear();
                cTransaccion ocTransaccion = new cTransaccion();
                List<cTransaccion> lista = ocTransaccion.todos(oObjeto.ROW_ID);
                foreach (cTransaccion registro in lista)
                {
                    int n = dtgTransaccion.Rows.Add();
                    actualizar_linea_transaccion(registro, n);
                }
                cargar_total();
            }
        }
        private void cargar_total()
        {
            DEBE_TOTAL.Text = oData.formato_decimal("0", "S");
            HABER_TOTAL.Text = oData.formato_decimal("0", "S");
            double resultado = 0;
            resultado = dtgTransaccion.Rows.Cast<DataGridViewRow>().Sum(x => Convert.ToDouble(x.Cells["debeField"].Value));
            DEBE_TOTAL.Text = oData.formato_decimal(resultado.ToString(), "");
            resultado = dtgTransaccion.Rows.Cast<DataGridViewRow>().Sum(x => Convert.ToDouble(x.Cells["haberField"].Value));
            HABER_TOTAL.Text = oData.formato_decimal(resultado.ToString(), "");
        }
        private void actualizar_linea_transaccion(cTransaccion registro, int n)
        {
            for (int i = 0; i < dtgTransaccion.Columns.Count; i++)
            {
                dtgTransaccion.Rows[n].Cells[i].Value = "";
            }
            dtgTransaccion.Rows[n].Tag = registro;
            dtgTransaccion.Rows[n].Cells["ROW_ID"].Value = registro.ROW_ID;
            dtgTransaccion.Rows[n].Cells["numCtaField"].Value = registro.numCtaField;
            dtgTransaccion.Rows[n].Cells["conceptoField_t"].Value = registro.conceptoField;
            dtgTransaccion.Rows[n].Cells["debeField"].Value = oData.formato_decimal(registro.debeField.ToString(), "");
            dtgTransaccion.Rows[n].Cells["haberField"].Value = oData.formato_decimal(registro.haberField.ToString(), "");
            dtgTransaccion.Rows[n].Cells["monedaField"].Value = registro.monedaField;

        }

        private void buttonX17_Click(object sender, EventArgs e)
        {
            eliminar_transaccion();
        }
        private void eliminar_transaccion()
        {
            dtgTransaccion.EndEdit();
            if (dtgTransaccion.SelectedCells.Count > 0)
            {
                DialogResult Resultado = MessageBoxEx.Show("�Desea eliminar las seleccionada?", Application.ProductName + "-" + Application.ProductVersion, MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                if (Resultado.ToString() != "Yes")
                {
                    return;
                }
                foreach (DataGridViewRow drv in dtgTransaccion.SelectedRows)
                {
                    string ROW_ID = drv.Cells["ROW_ID"].Value.ToString();
                    cTransaccion ocTransaccion = (cTransaccion)drv.Tag;
                    ocTransaccion.eliminar(ocTransaccion.ROW_ID);
                    dtgTransaccion.Rows.Remove(drv);
                }
            }
            else
            {
                MessageBoxEx.Show("Seleccione alguna l�nea.", Application.ProductName + "-" + Application.ProductVersion, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }

        private void buttonX15_Click(object sender, EventArgs e)
        {
            importar_transaccion_excel();
        }

        private void importar_transaccion_excel()
        {
            if (oObjeto.ROW_ID == "")
            {
                MessageBoxEx.Show("Guarde primero.", Application.ProductName + "-" + Application.ProductVersion, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return;
            }

            OpenFileDialog openFileDialog1 = new OpenFileDialog();
            openFileDialog1.Filter = "Excel files (*.xlsx)|*.xlsx|All files (*.*)|*.*";
            openFileDialog1.FilterIndex = 1;
            openFileDialog1.RestoreDirectory = true;

            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                try
                {
                    procesar(dtgTransaccion, 1, openFileDialog1.FileName);
                }
                catch (Exception)
                {
                    MessageBoxEx.Show("No existe el Archivo seleccionado", Application.ProductName + "-" + Application.ProductVersion, MessageBoxButtons.OK);
                }
            }
        }

        private void procesar(DataGridView dtg, int hoja, string archivo)
        {
            try
            {
                dtg.Rows.Clear();
                int n;
                int rowIndex = 1;
                FileInfo existingFile = new FileInfo(archivo);
                using (ExcelPackage package = new ExcelPackage(existingFile))
                {
                    ExcelWorksheet worksheet = package.Workbook.Worksheets[hoja];
                    while (worksheet.Cells[rowIndex, 1].Value != null)
                    {
                        //Agregar las lineas de la cabecera
                        if (rowIndex != 1)
                        {
                            cTransaccion ocTransaccion = new cTransaccion();
                            ocTransaccion.ROW_ID_POLIZA = oObjeto.ROW_ID;
                            ocTransaccion.numCtaField = worksheet.Cells[rowIndex, 1].Text;
                            ocTransaccion.conceptoField = worksheet.Cells[rowIndex, 2].Text;
                            ocTransaccion.debeField = double.Parse(worksheet.Cells[rowIndex, 3].Text);
                            ocTransaccion.haberField = double.Parse(worksheet.Cells[rowIndex, 4].Text);
                            ocTransaccion.monedaField= worksheet.Cells[rowIndex, 5].Text;
                            if (ocTransaccion.guardar())
                            {
                                n = dtg.Rows.Add();
                                actualizar_linea_transaccion(ocTransaccion, n);
                            }
                        }
                        rowIndex++;

                    }

                }
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message.ToString());
            }

        }

        private void buttonItem3_Click(object sender, EventArgs e)
        {
            exportar_excel();
        }

        private void exportar_excel()
        {
            var fileName = " Plantilla de Transaccion " + DateTime.Now.ToString("yyyy-MM-dd-hh-mm-ss") + ".xlsx";
            if (folderBrowserDialog1.ShowDialog() == DialogResult.OK)
            {
                fileName = folderBrowserDialog1.SelectedPath + "\\" + fileName;
                System.IO.Stream myStream = new System.IO.FileStream(fileName, System.IO.FileMode.Create, System.IO.FileAccess.ReadWrite, System.IO.FileShare.None);

                using (ExcelPackage pck = new ExcelPackage())
                {

                    ExcelWorksheet ws1 = pck.Workbook.Worksheets.Add("Plantilla");
                    int columnas = 1;
                    for (int c = 1; c < dtgTransaccion.Columns.Count; c++)
                    {
                        if (dtgTransaccion.Columns[c].Visible)
                        {
                            ws1.Cells[1, columnas].Value = dtgTransaccion.Columns[c].HeaderText;
                            columnas++;
                        }
                    }
                    pck.SaveAs(myStream);
                }
                myStream.Close();
                MessageBoxEx.Show("Creaci�n de plantilla realizada " + Environment.NewLine + fileName + " finalizada.", Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private void buttonX19_Click(object sender, EventArgs e)
        {
            nueva_transaccion();
        }


        private void nueva_transaccion()
        {
            if (oObjeto.ROW_ID == "")
            {
                MessageBoxEx.Show("Guarde primero.", Application.ProductName + "-" + Application.ProductVersion, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return;
            }
            frmTransaccion ofrmTransaccion = new frmTransaccion(oObjeto.ROW_ID,USUARIO);
            ofrmTransaccion.ShowDialog();
            cargar_transacciones();
        }

        private void dtgTransaccion_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            editar_transaccion();
        }

        private void editar_transaccion()
        {
            

            dtgTransaccion.EndEdit();
            if (dtgTransaccion.SelectedRows.Count > 0)
            {
                foreach (DataGridViewRow drv in dtgTransaccion.SelectedRows)
                {
                    string ROW_ID = drv.Cells["ROW_ID"].Value.ToString();
                    string numCtaFieldt = drv.Cells["numCtaField"].Value.ToString();

                    
                    frmTransaccion ofrmCuenta = new frmTransaccion(oObjeto.ROW_ID, ROW_ID,USUARIO);
                    ofrmCuenta.ShowDialog();
                    cTransaccion ocTransaccion = new cTransaccion();
                    if (ocTransaccion.cargar(ROW_ID))
                    {
                        actualizar_linea_transaccion(ocTransaccion, drv.Index);
                    }
                    else
                    {
                        MessageBoxEx.Show("La l�nea de la p�liza no fue cargada.", Application.ProductName + "-" + Application.ProductVersion, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                    }
                    
                }
            }
            else
            {
                MessageBoxEx.Show("Seleccione alguna l�nea.", Application.ProductName + "-" + Application.ProductVersion, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }

        private void buttonItem1_Click(object sender, EventArgs e)
        {
            oObjeto.importar_comprobantes();
        }

        private void buttonX4_Click(object sender, EventArgs e)
        {
            oObjeto.importar_comprobantes();
        }

        private void frmPoliza_Load(object sender, EventArgs e)
        {

        }

        private void buttonItem2_Click(object sender, EventArgs e)
        {
            exportar_excel_comproantes();
        }


        private void exportar_excel_comproantes()
        {
            var fileName = " Plantilla de Importacion de Comprobantes " + DateTime.Now.ToString("yyyy-MM-dd-hh-mm-ss") + ".xlsx";
            if (folderBrowserDialog1.ShowDialog() == DialogResult.OK)
            {
                fileName = folderBrowserDialog1.SelectedPath + "\\" + fileName;
                System.IO.Stream myStream = new System.IO.FileStream(fileName, System.IO.FileMode.Create, System.IO.FileAccess.ReadWrite, System.IO.FileShare.None);

                using (ExcelPackage pck = new ExcelPackage())
                {

                    ExcelWorksheet ws1 = pck.Workbook.Worksheets.Add("Plantilla");
                                   
                    ws1.Cells[1, 1].Value = "RFC Emisor";
                    ws1.Cells[1, 2].Value = "RFC Receptor";
                    ws1.Cells[1, 3].Value = "UUID";
                    ws1.Cells[1, 4].Value = "Monto Total";
                    
                    pck.SaveAs(myStream);
                }
                myStream.Close();
                MessageBoxEx.Show("Creaci�n de plantilla realizada " + Environment.NewLine + fileName + " finalizada.", Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private void buttonX16_Click(object sender, EventArgs e)
        {

        }

        private void buttonItem1_Click_1(object sender, EventArgs e)
        {
            plantilla_excel_transaccion();
        }

        private void plantilla_excel_transaccion()
        {
            var fileName = " Plantilla de Transacciones " + DateTime.Now.ToString("yyyy-MM-dd-hh-mm-ss") + ".xlsx";
            if (folderBrowserDialog1.ShowDialog() == DialogResult.OK)
            {
                fileName = folderBrowserDialog1.SelectedPath + "\\" + fileName;
                System.IO.Stream myStream = new System.IO.FileStream(fileName, System.IO.FileMode.Create, System.IO.FileAccess.ReadWrite, System.IO.FileShare.None);

                using (ExcelPackage pck = new ExcelPackage())
                {

                    ExcelWorksheet ws1 = pck.Workbook.Worksheets.Add("Plantilla");

                    ws1.Cells[1, 1].Value = "Poliza";
                    ws1.Cells[1, 2].Value = "Transaccion";
                    ws1.Cells[1, 3].Value = "Cuenta Origen";
                    ws1.Cells[1, 4].Value = "Banco Origen";
                    ws1.Cells[1, 5].Value = "Monto";
                    ws1.Cells[1, 6].Value = "Fecha";
                    ws1.Cells[1, 7].Value = "Cuenta Destino";
                    ws1.Cells[1, 8].Value = "Banco Destino";
                    ws1.Cells[1, 9].Value = "Beneficiario";
                    ws1.Cells[1, 10].Value = "RFC";

                    pck.SaveAs(myStream);
                }
                myStream.Close();
                MessageBoxEx.Show("Creaci�n de plantilla realizada " + Environment.NewLine + fileName + " finalizada.", Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private void buttonX5_Click(object sender, EventArgs e)
        {
            oObjeto.importar_transferencia();
        }

        private void buttonX6_Click(object sender, EventArgs e)
        {
            oObjeto.importar_transacciones();
        }


    }
}