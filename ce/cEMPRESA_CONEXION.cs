﻿using DevComponents.DotNetBar;
using Generales;
using ModeloDocumentosFX;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Desarrollo
{
    class cEMPRESA_CONEXION
    {
        private string sSQL = "";
        public string ROW_ID { get; set; }
        public string ROW_ID_EMPRESA { get; set; }
        
        public string VISUAL_TIPO { get; set; }
        public string VISUAL_SERVIDOR { get; set; }
        public string VISUAL_BD { get; set; }
        public string VISUAL_USUARIO { get; set; }
        public string VISUAL_PASSWORD { get; set; }
        public string VISUAL_VERSION { get; set; }
        public string VISUAL_CURRENCY_ID { get; set; }
        public string VISUAL_BD_AUXILIAR { get; set; }
        public string VISUAL_ENTITY_ID { get; set; }
        public string VISUAL_SITE_ID { get; set; }

        private cCONEXCION oData = new cCONEXCION();
        public string auxiliarTipo { get; set; }
        public string auxiliarServidor { get; set; }
        public string auxiliarBaseDatos { get; set; }
        public string auxiliarUsuario { get; set; }
        public string auxiliarPassword { get; set; }

        public override string ToString() { return ROW_ID; }

        public cEMPRESA_CONEXION()
        {
            ModeloDocumentosFXContainer dbContext = new ModeloDocumentosFXContainer();
            try
            {
                string conection = dbContext.Database.Connection.ConnectionString;
                oData = new cCONEXCION(conection);
            }
            catch (Exception err)
            {
                ErrorFX.mostrar(err, false, true, "frmEmpresa - 96 - ", false);
            }
            limpiar();
        }


        public void limpiar()
        {
            ROW_ID = "";
            ROW_ID_EMPRESA = "";
            VISUAL_TIPO = "";
            VISUAL_SERVIDOR = "";
            VISUAL_BD = "";
            VISUAL_USUARIO = "";
            VISUAL_PASSWORD = "";
            VISUAL_VERSION = "";
            VISUAL_CURRENCY_ID = "";
            VISUAL_ENTITY_ID = "";
            VISUAL_SITE_ID = "";
            VISUAL_BD_AUXILIAR = "";

            auxiliarTipo = "";
            auxiliarServidor = "";
            auxiliarBaseDatos = "";
            auxiliarUsuario = "";
            auxiliarPassword = "";
    }

        public cCONEXCION conectar(string ROW_IDp="")
        {
            if (ROW_IDp != "")
            {
                cargar(ROW_IDp);
            }

            cCONEXCION ocCONEXCION = new cCONEXCION(VISUAL_SERVIDOR,VISUAL_BD
                ,VISUAL_USUARIO,VISUAL_PASSWORD);
            if (ocCONEXCION.CrearConexion())
            {
                return ocCONEXCION;
            }
            
            return null;
        }
        public cCONEXCION conectar_auxiliar(string ROW_IDp = "")
        {
            if (ROW_IDp != "")
            {
                cargar(ROW_IDp);
            }

            //cCONEXCION ocCONEXCION = new cCONEXCION(VISUAL_SERVIDOR, VISUAL_BD_AUXILIAR
            //    , VISUAL_USUARIO, VISUAL_PASSWORD);

            cCONEXCION ocCONEXCION = new cCONEXCION(auxiliarServidor, this.auxiliarBaseDatos
                , auxiliarUsuario, auxiliarUsuario);
            if (ocCONEXCION.CrearConexion())

                if (ocCONEXCION.CrearConexion())
            {
                return ocCONEXCION;
            }

            return null;
        }
        public List<cEMPRESA_CONEXION> todos(string ROW_ID_EMPRESAp = "", string RFCp = "")
        {

            List<cEMPRESA_CONEXION> lista = new List<cEMPRESA_CONEXION>();

            sSQL = " SELECT * ";
            sSQL += " FROM EMPRESA_CONEXION ";
            sSQL += " WHERE 1=1 ";
            if(RFCp!="")
            {
                sSQL += " AND '" + RFCp + "' IN (SELECT RFC FROM EMPRESA E WHERE E.ROW_ID=EMPRESA_CONEXION.ROW_ID_EMPRESA) ";
            }
            if (ROW_ID_EMPRESAp!="")
            {
                sSQL += " AND ROW_ID_EMPRESA=" + ROW_ID_EMPRESAp + " ";
            }
            

            sSQL += " ORDER BY ROW_ID";
            DataTable oDataTable = oData.EjecutarConsulta(sSQL);
            foreach (DataRow oDataRow in oDataTable.Rows)
            {
                cEMPRESA_CONEXION ocEMPRESA_CONEXION = new cEMPRESA_CONEXION();
                ocEMPRESA_CONEXION.cargar(oDataRow["ROW_ID"].ToString());
                lista.Add(ocEMPRESA_CONEXION);
            }
            return lista;

        }

        public bool cargar(string ROW_IDp)
        {
            sSQL = " SELECT * ";
            sSQL += " FROM EMPRESA_CONEXION ";
            sSQL += " WHERE ROW_ID=" + ROW_IDp + " ";
            DataTable oDataTable = oData.EjecutarConsulta(sSQL);
            if (oDataTable != null)
            {
                foreach (DataRow oDataRow in oDataTable.Rows)
                {
                    //Añadir el Registro
                    ROW_ID = oDataRow["ROW_ID"].ToString();
                    ROW_ID_EMPRESA = oDataRow["ROW_ID_EMPRESA"].ToString();
                    VISUAL_TIPO = oDataRow["VISUAL_TIPO"].ToString();
                    VISUAL_SERVIDOR = oDataRow["VISUAL_SERVIDOR"].ToString();
                    VISUAL_BD = oDataRow["VISUAL_BD"].ToString();
                    VISUAL_USUARIO = oDataRow["VISUAL_USUARIO"].ToString();
                    VISUAL_PASSWORD = oDataRow["VISUAL_PASSWORD"].ToString();
                    VISUAL_VERSION = oDataRow["VISUAL_VERSION"].ToString();
                    VISUAL_ENTITY_ID = oDataRow["VISUAL_ENTITY_ID"].ToString();
                    VISUAL_SITE_ID = oDataRow["VISUAL_SITE_ID"].ToString();
                    VISUAL_CURRENCY_ID = oDataRow["VISUAL_CURRENCY_ID"].ToString();
                    VISUAL_BD_AUXILIAR = oDataRow["VISUAL_BD_AUXILIAR"].ToString();

                    auxiliarTipo = oDataRow["auxiliarTipo"].ToString();
                    auxiliarServidor = oDataRow["auxiliarServidor"].ToString();
                    auxiliarBaseDatos = oDataRow["auxiliarBaseDatos"].ToString();
                    auxiliarUsuario = oDataRow["auxiliarUsuario"].ToString();
                    auxiliarPassword = oDataRow["auxiliarPassword"].ToString();

                    return true;
                }
            }
            return false;
        }

        public bool guardar()
        {
            if (ROW_ID_EMPRESA.Trim() == "")
            {
                MessageBoxEx.Show("El ROW_ID_EMPRESA es requerido.", Application.ProductName + "-" + Application.ProductVersion, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return false;
            }

            if (ROW_ID == "")
            {

                sSQL = " INSERT INTO EMPRESA_CONEXION ";
                sSQL += " ( ";
                sSQL += " [ROW_ID_EMPRESA] ";
                sSQL += " ,[VISUAL_TIPO],[VISUAL_SERVIDOR],[VISUAL_CURRENCY_ID] ";
                sSQL += " ,[VISUAL_BD],[VISUAL_USUARIO] ";
                sSQL += " ,[VISUAL_PASSWORD],[VISUAL_VERSION],[VISUAL_ENTITY_ID],[VISUAL_SITE_ID],[VISUAL_BD_AUXILIAR]";
                sSQL += " ,[auxiliarTipo],[auxiliarServidor],[auxiliarBaseDatos],[auxiliarUsuario],[auxiliarPassword]";

                sSQL += " )";
                sSQL += " VALUES ";
                sSQL += "(";
                sSQL += " '" + ROW_ID_EMPRESA+ "'";
                sSQL += " ,'" + VISUAL_TIPO+ "','" + VISUAL_SERVIDOR+ "','" + VISUAL_CURRENCY_ID+ "' ";
                sSQL += " ,'" + VISUAL_BD+ "','" + VISUAL_USUARIO+ "' ";
                sSQL += " ,'" + VISUAL_PASSWORD + "','" + VISUAL_VERSION + "','" + VISUAL_ENTITY_ID + "','" + VISUAL_SITE_ID + "','" + VISUAL_BD_AUXILIAR + "'";
                sSQL += " ,'" + auxiliarTipo + "','" + auxiliarServidor + "','" + auxiliarBaseDatos + "','" + auxiliarUsuario + "','" + auxiliarPassword + "'";
                sSQL += ")";
                DataTable oDataTable = oData.EjecutarConsulta(sSQL);
                if (oDataTable != null)
                {

                    sSQL = "SELECT TOP 1 MAX(ROW_ID) as ROW_ID FROM EMPRESA_CONEXION";
                    oDataTable = oData.EjecutarConsulta(sSQL);
                    if (oDataTable != null)
                    {
                        foreach (DataRow oDataRow in oDataTable.Rows)
                        {
                            ROW_ID = oDataRow["ROW_ID"].ToString();
                        }
                    }
                }

            }
            else
            {
                sSQL = " UPDATE EMPRESA_CONEXION ";
                sSQL += " SET ";
                sSQL += " VISUAL_TIPO='" + VISUAL_TIPO + "',VISUAL_SERVIDOR='" + VISUAL_SERVIDOR + "',VISUAL_CURRENCY_ID='" + VISUAL_CURRENCY_ID + "' ";
                sSQL += " ,VISUAL_BD='" + VISUAL_BD + "',VISUAL_USUARIO='" + VISUAL_USUARIO + "' ";
                sSQL += " ,VISUAL_PASSWORD='" + VISUAL_PASSWORD + "',VISUAL_VERSION='" + VISUAL_VERSION + "',VISUAL_ENTITY_ID='" + VISUAL_ENTITY_ID + "',VISUAL_SITE_ID='" + VISUAL_SITE_ID + "', VISUAL_BD_AUXILIAR='" + VISUAL_BD_AUXILIAR + "'";
                sSQL += " ,auxiliarTipo='" + auxiliarTipo + "',auxiliarServidor='" + auxiliarServidor + "',auxiliarBaseDatos='" + auxiliarBaseDatos + "',auxiliarUsuario='" + auxiliarUsuario + "',auxiliarPassword='" + auxiliarPassword + "'";
                sSQL += " WHERE ROW_ID=" + ROW_ID;
                oData.EjecutarConsulta(sSQL);

            }
            return true;

        }

        public bool eliminar(string ROW_IDp)
        {
            if (ROW_IDp != "")
            {
                sSQL = "DELETE FROM EMPRESA_CONEXION ";
                sSQL += " WHERE ROW_ID='" + ROW_IDp + "' ";
                oData.EjecutarConsulta(sSQL);
                return true;
            }
            return false;
        }

        internal cCONEXCION conectarAuxiliar(string ROW_IDp = "")
        {
            if (ROW_IDp != "")
            {
                cargar(ROW_IDp);
            }

            cCONEXCION ocCONEXCION = new cCONEXCION(auxiliarServidor, this.auxiliarBaseDatos
                , auxiliarUsuario, auxiliarUsuario);
            if (ocCONEXCION.CrearConexion())
            {
                return ocCONEXCION;
            }

            return null;
        }
    }
}
