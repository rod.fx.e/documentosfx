namespace Desarrollo
{
    partial class frmPoliza
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            DevComponents.DotNetBar.ButtonX buttonX3;
            DevComponents.DotNetBar.ButtonX buttonX1;
            DevComponents.DotNetBar.ButtonX buttonX2;
            DevComponents.DotNetBar.ButtonX buttonX15;
            DevComponents.DotNetBar.ButtonX buttonX17;
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmPoliza));
            this.buttonItem3 = new DevComponents.DotNetBar.ButtonItem();
            this.metroStatusBar1 = new DevComponents.DotNetBar.Metro.MetroStatusBar();
            this.labelItem1 = new DevComponents.DotNetBar.LabelItem();
            this.tipoField = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.labelX1 = new DevComponents.DotNetBar.LabelX();
            this.labelX2 = new DevComponents.DotNetBar.LabelX();
            this.labelX3 = new DevComponents.DotNetBar.LabelX();
            this.numField = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.fechaField = new System.Windows.Forms.DateTimePicker();
            this.labelX4 = new DevComponents.DotNetBar.LabelX();
            this.conceptoField = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.circularProgress3 = new DevComponents.DotNetBar.Controls.CircularProgress();
            this.buttonX16 = new DevComponents.DotNetBar.ButtonX();
            this.buttonX18 = new DevComponents.DotNetBar.ButtonX();
            this.buttonX19 = new DevComponents.DotNetBar.ButtonX();
            this.dtgTransaccion = new DevComponents.DotNetBar.Controls.DataGridViewX();
            this.ROW_ID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ROW_ID_POLIZA = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.numCtaField = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.conceptoField_t = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.debeField = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.haberField = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.monedaField = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.superTabItem1 = new DevComponents.DotNetBar.SuperTabItem();
            this.folderBrowserDialog1 = new System.Windows.Forms.FolderBrowserDialog();
            this.labelX8 = new DevComponents.DotNetBar.LabelX();
            this.HABER_TOTAL = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.labelX7 = new DevComponents.DotNetBar.LabelX();
            this.DEBE_TOTAL = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.labelX5 = new DevComponents.DotNetBar.LabelX();
            this.buttonX4 = new DevComponents.DotNetBar.ButtonX();
            this.buttonItem2 = new DevComponents.DotNetBar.ButtonItem();
            this.ASIGNADO = new DevComponents.DotNetBar.Controls.ComboBoxEx();
            this.buttonX5 = new DevComponents.DotNetBar.ButtonX();
            this.buttonItem1 = new DevComponents.DotNetBar.ButtonItem();
            this.buttonX6 = new DevComponents.DotNetBar.ButtonX();
            this.buttonItem4 = new DevComponents.DotNetBar.ButtonItem();
            buttonX3 = new DevComponents.DotNetBar.ButtonX();
            buttonX1 = new DevComponents.DotNetBar.ButtonX();
            buttonX2 = new DevComponents.DotNetBar.ButtonX();
            buttonX15 = new DevComponents.DotNetBar.ButtonX();
            buttonX17 = new DevComponents.DotNetBar.ButtonX();
            ((System.ComponentModel.ISupportInitialize)(this.dtgTransaccion)).BeginInit();
            this.SuspendLayout();
            // 
            // buttonX3
            // 
            buttonX3.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            buttonX3.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            buttonX3.Image = global::Desarrollo.Properties.Resources.Eliminar_18;
            buttonX3.Location = new System.Drawing.Point(141, 0);
            buttonX3.Name = "buttonX3";
            buttonX3.Size = new System.Drawing.Size(68, 24);
            buttonX3.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            buttonX3.TabIndex = 2;
            buttonX3.Text = "Eliminar";
            buttonX3.TextAlignment = DevComponents.DotNetBar.eButtonTextAlignment.Right;
            buttonX3.Click += new System.EventHandler(this.buttonX3_Click);
            // 
            // buttonX1
            // 
            buttonX1.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            buttonX1.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            buttonX1.Image = global::Desarrollo.Properties.Resources.Nueno_18;
            buttonX1.Location = new System.Drawing.Point(67, 0);
            buttonX1.Name = "buttonX1";
            buttonX1.Size = new System.Drawing.Size(68, 24);
            buttonX1.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            buttonX1.TabIndex = 1;
            buttonX1.Text = "Guardar";
            buttonX1.TextAlignment = DevComponents.DotNetBar.eButtonTextAlignment.Right;
            buttonX1.Click += new System.EventHandler(this.buttonX1_Click);
            // 
            // buttonX2
            // 
            buttonX2.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            buttonX2.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            buttonX2.Image = global::Desarrollo.Properties.Resources.Nuevo_18_19;
            buttonX2.Location = new System.Drawing.Point(0, 0);
            buttonX2.Name = "buttonX2";
            buttonX2.Size = new System.Drawing.Size(61, 24);
            buttonX2.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            buttonX2.TabIndex = 0;
            buttonX2.Text = "Nuevo";
            buttonX2.TextAlignment = DevComponents.DotNetBar.eButtonTextAlignment.Right;
            buttonX2.Click += new System.EventHandler(this.buttonX2_Click);
            // 
            // buttonX15
            // 
            buttonX15.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            buttonX15.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            buttonX15.Image = global::Desarrollo.Properties.Resources.excel;
            buttonX15.Location = new System.Drawing.Point(238, 88);
            buttonX15.Name = "buttonX15";
            buttonX15.Size = new System.Drawing.Size(114, 24);
            buttonX15.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            buttonX15.SubItems.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.buttonItem3});
            buttonX15.TabIndex = 581;
            buttonX15.Text = "Importar: Excel";
            buttonX15.TextAlignment = DevComponents.DotNetBar.eButtonTextAlignment.Right;
            buttonX15.Visible = false;
            buttonX15.Click += new System.EventHandler(this.buttonX15_Click);
            // 
            // buttonItem3
            // 
            this.buttonItem3.GlobalItem = false;
            this.buttonItem3.Image = global::Desarrollo.Properties.Resources.wizard;
            this.buttonItem3.Name = "buttonItem3";
            this.buttonItem3.Text = "Generar Plantilla";
            this.buttonItem3.Click += new System.EventHandler(this.buttonItem3_Click);
            // 
            // buttonX17
            // 
            buttonX17.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            buttonX17.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            buttonX17.Image = global::Desarrollo.Properties.Resources.Eliminar_18;
            buttonX17.Location = new System.Drawing.Point(164, 88);
            buttonX17.Name = "buttonX17";
            buttonX17.Size = new System.Drawing.Size(68, 24);
            buttonX17.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            buttonX17.TabIndex = 583;
            buttonX17.Text = "Eliminar";
            buttonX17.TextAlignment = DevComponents.DotNetBar.eButtonTextAlignment.Right;
            buttonX17.Click += new System.EventHandler(this.buttonX17_Click);
            // 
            // metroStatusBar1
            // 
            this.metroStatusBar1.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.metroStatusBar1.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.metroStatusBar1.ContainerControlProcessDialogKey = true;
            this.metroStatusBar1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.metroStatusBar1.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.metroStatusBar1.ForeColor = System.Drawing.Color.Black;
            this.metroStatusBar1.Items.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.labelItem1});
            this.metroStatusBar1.Location = new System.Drawing.Point(0, 358);
            this.metroStatusBar1.Name = "metroStatusBar1";
            this.metroStatusBar1.Size = new System.Drawing.Size(791, 21);
            this.metroStatusBar1.TabIndex = 8;
            // 
            // labelItem1
            // 
            this.labelItem1.Name = "labelItem1";
            // 
            // tipoField
            // 
            this.tipoField.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.tipoField.Border.Class = "TextBoxBorder";
            this.tipoField.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.tipoField.ForeColor = System.Drawing.Color.Black;
            this.tipoField.Location = new System.Drawing.Point(67, 32);
            this.tipoField.Name = "tipoField";
            this.tipoField.Size = new System.Drawing.Size(68, 22);
            this.tipoField.TabIndex = 4;
            this.tipoField.TabStop = false;
            this.tipoField.WatermarkBehavior = DevComponents.DotNetBar.eWatermarkBehavior.HideNonEmpty;
            // 
            // labelX1
            // 
            this.labelX1.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.labelX1.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX1.ForeColor = System.Drawing.Color.Black;
            this.labelX1.Location = new System.Drawing.Point(0, 32);
            this.labelX1.Name = "labelX1";
            this.labelX1.Size = new System.Drawing.Size(61, 22);
            this.labelX1.TabIndex = 3;
            this.labelX1.Text = "Tipo";
            this.labelX1.TextAlignment = System.Drawing.StringAlignment.Far;
            // 
            // labelX2
            // 
            this.labelX2.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.labelX2.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX2.ForeColor = System.Drawing.Color.Black;
            this.labelX2.Location = new System.Drawing.Point(141, 32);
            this.labelX2.Name = "labelX2";
            this.labelX2.Size = new System.Drawing.Size(49, 22);
            this.labelX2.TabIndex = 3;
            this.labelX2.Text = "N�mero";
            this.labelX2.TextAlignment = System.Drawing.StringAlignment.Far;
            // 
            // labelX3
            // 
            this.labelX3.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.labelX3.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX3.ForeColor = System.Drawing.Color.Black;
            this.labelX3.Location = new System.Drawing.Point(289, 32);
            this.labelX3.Name = "labelX3";
            this.labelX3.Size = new System.Drawing.Size(30, 22);
            this.labelX3.TabIndex = 3;
            this.labelX3.Text = "Fecha";
            this.labelX3.TextAlignment = System.Drawing.StringAlignment.Far;
            // 
            // numField
            // 
            this.numField.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.numField.Border.Class = "TextBoxBorder";
            this.numField.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.numField.ForeColor = System.Drawing.Color.Black;
            this.numField.Location = new System.Drawing.Point(196, 32);
            this.numField.Name = "numField";
            this.numField.Size = new System.Drawing.Size(87, 22);
            this.numField.TabIndex = 4;
            this.numField.TabStop = false;
            this.numField.WatermarkBehavior = DevComponents.DotNetBar.eWatermarkBehavior.HideNonEmpty;
            // 
            // fechaField
            // 
            this.fechaField.BackColor = System.Drawing.Color.White;
            this.fechaField.ForeColor = System.Drawing.Color.Black;
            this.fechaField.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.fechaField.Location = new System.Drawing.Point(325, 32);
            this.fechaField.Name = "fechaField";
            this.fechaField.Size = new System.Drawing.Size(99, 22);
            this.fechaField.TabIndex = 562;
            // 
            // labelX4
            // 
            this.labelX4.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.labelX4.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX4.ForeColor = System.Drawing.Color.Black;
            this.labelX4.Location = new System.Drawing.Point(0, 60);
            this.labelX4.Name = "labelX4";
            this.labelX4.Size = new System.Drawing.Size(61, 22);
            this.labelX4.TabIndex = 3;
            this.labelX4.Text = "Concepto";
            this.labelX4.TextAlignment = System.Drawing.StringAlignment.Far;
            // 
            // conceptoField
            // 
            this.conceptoField.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.conceptoField.Border.Class = "TextBoxBorder";
            this.conceptoField.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.conceptoField.ForeColor = System.Drawing.Color.Black;
            this.conceptoField.Location = new System.Drawing.Point(67, 60);
            this.conceptoField.Name = "conceptoField";
            this.conceptoField.Size = new System.Drawing.Size(357, 22);
            this.conceptoField.TabIndex = 4;
            this.conceptoField.TabStop = false;
            this.conceptoField.WatermarkBehavior = DevComponents.DotNetBar.eWatermarkBehavior.HideNonEmpty;
            // 
            // circularProgress3
            // 
            this.circularProgress3.AnimationSpeed = 50;
            this.circularProgress3.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.circularProgress3.BackgroundStyle.BackgroundImageAlpha = ((byte)(64));
            this.circularProgress3.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.circularProgress3.FocusCuesEnabled = false;
            this.circularProgress3.Font = new System.Drawing.Font("Segoe UI", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.circularProgress3.Location = new System.Drawing.Point(467, 88);
            this.circularProgress3.Name = "circularProgress3";
            this.circularProgress3.ProgressBarType = DevComponents.DotNetBar.eCircularProgressType.Donut;
            this.circularProgress3.ProgressColor = System.Drawing.Color.Orange;
            this.circularProgress3.ProgressTextFormat = "{0}";
            this.circularProgress3.Size = new System.Drawing.Size(25, 24);
            this.circularProgress3.Style = DevComponents.DotNetBar.eDotNetBarStyle.OfficeXP;
            this.circularProgress3.TabIndex = 584;
            this.circularProgress3.Value = 100;
            this.circularProgress3.Visible = false;
            // 
            // buttonX16
            // 
            this.buttonX16.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.buttonX16.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.buttonX16.Image = global::Desarrollo.Properties.Resources.visual;
            this.buttonX16.Location = new System.Drawing.Point(358, 88);
            this.buttonX16.Name = "buttonX16";
            this.buttonX16.Size = new System.Drawing.Size(106, 24);
            this.buttonX16.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.buttonX16.TabIndex = 582;
            this.buttonX16.Text = "Importar: Visual";
            this.buttonX16.Visible = false;
            this.buttonX16.Click += new System.EventHandler(this.buttonX16_Click);
            // 
            // buttonX18
            // 
            this.buttonX18.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.buttonX18.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.buttonX18.Image = global::Desarrollo.Properties.Resources.actualizar_18;
            this.buttonX18.Location = new System.Drawing.Point(0, 88);
            this.buttonX18.Name = "buttonX18";
            this.buttonX18.Size = new System.Drawing.Size(78, 24);
            this.buttonX18.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.buttonX18.TabIndex = 579;
            this.buttonX18.Text = "Refrescar";
            this.buttonX18.Click += new System.EventHandler(this.buttonX18_Click);
            // 
            // buttonX19
            // 
            this.buttonX19.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.buttonX19.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.buttonX19.Image = global::Desarrollo.Properties.Resources.Nueno_18;
            this.buttonX19.Location = new System.Drawing.Point(84, 88);
            this.buttonX19.Name = "buttonX19";
            this.buttonX19.Size = new System.Drawing.Size(74, 24);
            this.buttonX19.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.buttonX19.TabIndex = 580;
            this.buttonX19.Text = "Agregar";
            this.buttonX19.Click += new System.EventHandler(this.buttonX19_Click);
            // 
            // dtgTransaccion
            // 
            this.dtgTransaccion.AllowUserToAddRows = false;
            this.dtgTransaccion.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dtgTransaccion.BackgroundColor = System.Drawing.Color.White;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dtgTransaccion.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.dtgTransaccion.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dtgTransaccion.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.ROW_ID,
            this.ROW_ID_POLIZA,
            this.numCtaField,
            this.conceptoField_t,
            this.debeField,
            this.haberField,
            this.monedaField});
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle4.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle4.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle4.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle4.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle4.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dtgTransaccion.DefaultCellStyle = dataGridViewCellStyle4;
            this.dtgTransaccion.EnableHeadersVisualStyles = false;
            this.dtgTransaccion.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(170)))), ((int)(((byte)(170)))), ((int)(((byte)(170)))));
            this.dtgTransaccion.Location = new System.Drawing.Point(0, 118);
            this.dtgTransaccion.Name = "dtgTransaccion";
            dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle5.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle5.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle5.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle5.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle5.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle5.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dtgTransaccion.RowHeadersDefaultCellStyle = dataGridViewCellStyle5;
            this.dtgTransaccion.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dtgTransaccion.Size = new System.Drawing.Size(791, 234);
            this.dtgTransaccion.TabIndex = 578;
            this.dtgTransaccion.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dtgTransaccion_CellDoubleClick);
            // 
            // ROW_ID
            // 
            this.ROW_ID.HeaderText = "ROW_ID";
            this.ROW_ID.Name = "ROW_ID";
            this.ROW_ID.Visible = false;
            // 
            // ROW_ID_POLIZA
            // 
            this.ROW_ID_POLIZA.HeaderText = "ROW_ID_POLIZA";
            this.ROW_ID_POLIZA.Name = "ROW_ID_POLIZA";
            this.ROW_ID_POLIZA.ReadOnly = true;
            this.ROW_ID_POLIZA.Visible = false;
            // 
            // numCtaField
            // 
            this.numCtaField.HeaderText = "NumCta";
            this.numCtaField.Name = "numCtaField";
            this.numCtaField.ReadOnly = true;
            // 
            // conceptoField_t
            // 
            this.conceptoField_t.HeaderText = "Concepto";
            this.conceptoField_t.Name = "conceptoField_t";
            this.conceptoField_t.ReadOnly = true;
            this.conceptoField_t.Width = 250;
            // 
            // debeField
            // 
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.debeField.DefaultCellStyle = dataGridViewCellStyle2;
            this.debeField.HeaderText = "Debe";
            this.debeField.Name = "debeField";
            this.debeField.ReadOnly = true;
            // 
            // haberField
            // 
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.haberField.DefaultCellStyle = dataGridViewCellStyle3;
            this.haberField.HeaderText = "Haber";
            this.haberField.Name = "haberField";
            this.haberField.ReadOnly = true;
            // 
            // monedaField
            // 
            this.monedaField.HeaderText = "Moneda";
            this.monedaField.Name = "monedaField";
            this.monedaField.ReadOnly = true;
            // 
            // superTabItem1
            // 
            this.superTabItem1.GlobalItem = false;
            this.superTabItem1.Name = "superTabItem1";
            this.superTabItem1.Text = "Transacciones";
            // 
            // labelX8
            // 
            this.labelX8.AutoSize = true;
            this.labelX8.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.labelX8.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX8.ForeColor = System.Drawing.Color.Black;
            this.labelX8.Location = new System.Drawing.Point(595, 63);
            this.labelX8.Name = "labelX8";
            this.labelX8.Size = new System.Drawing.Size(32, 17);
            this.labelX8.TabIndex = 597;
            this.labelX8.Text = "Haber";
            // 
            // HABER_TOTAL
            // 
            this.HABER_TOTAL.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.HABER_TOTAL.Border.Class = "TextBoxBorder";
            this.HABER_TOTAL.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.HABER_TOTAL.Enabled = false;
            this.HABER_TOTAL.ForeColor = System.Drawing.Color.Black;
            this.HABER_TOTAL.Location = new System.Drawing.Point(633, 60);
            this.HABER_TOTAL.Name = "HABER_TOTAL";
            this.HABER_TOTAL.Size = new System.Drawing.Size(102, 22);
            this.HABER_TOTAL.TabIndex = 598;
            this.HABER_TOTAL.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // labelX7
            // 
            this.labelX7.AutoSize = true;
            this.labelX7.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.labelX7.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX7.ForeColor = System.Drawing.Color.Black;
            this.labelX7.Location = new System.Drawing.Point(449, 63);
            this.labelX7.Name = "labelX7";
            this.labelX7.Size = new System.Drawing.Size(28, 17);
            this.labelX7.TabIndex = 595;
            this.labelX7.Text = "Debe";
            // 
            // DEBE_TOTAL
            // 
            this.DEBE_TOTAL.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.DEBE_TOTAL.Border.Class = "TextBoxBorder";
            this.DEBE_TOTAL.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.DEBE_TOTAL.Enabled = false;
            this.DEBE_TOTAL.ForeColor = System.Drawing.Color.Black;
            this.DEBE_TOTAL.Location = new System.Drawing.Point(483, 60);
            this.DEBE_TOTAL.Name = "DEBE_TOTAL";
            this.DEBE_TOTAL.Size = new System.Drawing.Size(106, 22);
            this.DEBE_TOTAL.TabIndex = 596;
            this.DEBE_TOTAL.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // labelX5
            // 
            this.labelX5.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.labelX5.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX5.ForeColor = System.Drawing.Color.Black;
            this.labelX5.Location = new System.Drawing.Point(430, 32);
            this.labelX5.Name = "labelX5";
            this.labelX5.Size = new System.Drawing.Size(47, 22);
            this.labelX5.TabIndex = 600;
            this.labelX5.Text = "Asginado";
            this.labelX5.TextAlignment = System.Drawing.StringAlignment.Far;
            // 
            // buttonX4
            // 
            this.buttonX4.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.buttonX4.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.buttonX4.Image = global::Desarrollo.Properties.Resources.excel;
            this.buttonX4.Location = new System.Drawing.Point(498, 88);
            this.buttonX4.Name = "buttonX4";
            this.buttonX4.Size = new System.Drawing.Size(177, 24);
            this.buttonX4.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.buttonX4.SubItems.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.buttonItem2});
            this.buttonX4.TabIndex = 601;
            this.buttonX4.Text = "Importar: Comprobantes";
            this.buttonX4.Click += new System.EventHandler(this.buttonX4_Click);
            // 
            // buttonItem2
            // 
            this.buttonItem2.GlobalItem = false;
            this.buttonItem2.Image = global::Desarrollo.Properties.Resources.wizard;
            this.buttonItem2.Name = "buttonItem2";
            this.buttonItem2.Text = "Generar Plantilla";
            this.buttonItem2.Click += new System.EventHandler(this.buttonItem2_Click);
            // 
            // ASIGNADO
            // 
            this.ASIGNADO.DisplayMember = "Text";
            this.ASIGNADO.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.ASIGNADO.ForeColor = System.Drawing.Color.Black;
            this.ASIGNADO.FormattingEnabled = true;
            this.ASIGNADO.ItemHeight = 16;
            this.ASIGNADO.Location = new System.Drawing.Point(483, 32);
            this.ASIGNADO.Name = "ASIGNADO";
            this.ASIGNADO.Size = new System.Drawing.Size(106, 22);
            this.ASIGNADO.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.ASIGNADO.TabIndex = 602;
            // 
            // buttonX5
            // 
            this.buttonX5.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.buttonX5.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.buttonX5.Image = global::Desarrollo.Properties.Resources.excel;
            this.buttonX5.Location = new System.Drawing.Point(215, 0);
            this.buttonX5.Name = "buttonX5";
            this.buttonX5.Size = new System.Drawing.Size(170, 24);
            this.buttonX5.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.buttonX5.SubItems.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.buttonItem1});
            this.buttonX5.TabIndex = 603;
            this.buttonX5.Text = "Importar: Transferencias";
            this.buttonX5.Click += new System.EventHandler(this.buttonX5_Click);
            // 
            // buttonItem1
            // 
            this.buttonItem1.GlobalItem = false;
            this.buttonItem1.Image = global::Desarrollo.Properties.Resources.wizard;
            this.buttonItem1.Name = "buttonItem1";
            this.buttonItem1.Text = "Generar Plantilla";
            this.buttonItem1.Click += new System.EventHandler(this.buttonItem1_Click_1);
            // 
            // buttonX6
            // 
            this.buttonX6.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.buttonX6.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.buttonX6.Image = global::Desarrollo.Properties.Resources.excel;
            this.buttonX6.Location = new System.Drawing.Point(391, 0);
            this.buttonX6.Name = "buttonX6";
            this.buttonX6.Size = new System.Drawing.Size(170, 24);
            this.buttonX6.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.buttonX6.SubItems.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.buttonItem4});
            this.buttonX6.TabIndex = 604;
            this.buttonX6.Text = "Importar: Transacciones";
            this.buttonX6.Click += new System.EventHandler(this.buttonX6_Click);
            // 
            // buttonItem4
            // 
            this.buttonItem4.GlobalItem = false;
            this.buttonItem4.Image = global::Desarrollo.Properties.Resources.wizard;
            this.buttonItem4.Name = "buttonItem4";
            this.buttonItem4.Text = "Generar Plantilla";
            // 
            // frmPoliza
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.BottomLeftCornerSize = 0;
            this.ClientSize = new System.Drawing.Size(791, 379);
            this.Controls.Add(this.buttonX6);
            this.Controls.Add(this.buttonX5);
            this.Controls.Add(this.ASIGNADO);
            this.Controls.Add(this.buttonX4);
            this.Controls.Add(this.labelX5);
            this.Controls.Add(this.labelX8);
            this.Controls.Add(this.HABER_TOTAL);
            this.Controls.Add(this.labelX7);
            this.Controls.Add(this.DEBE_TOTAL);
            this.Controls.Add(this.circularProgress3);
            this.Controls.Add(buttonX15);
            this.Controls.Add(this.fechaField);
            this.Controls.Add(this.buttonX16);
            this.Controls.Add(this.labelX4);
            this.Controls.Add(buttonX17);
            this.Controls.Add(this.labelX3);
            this.Controls.Add(this.buttonX18);
            this.Controls.Add(this.buttonX19);
            this.Controls.Add(this.labelX2);
            this.Controls.Add(this.dtgTransaccion);
            this.Controls.Add(this.labelX1);
            this.Controls.Add(this.conceptoField);
            this.Controls.Add(this.numField);
            this.Controls.Add(this.tipoField);
            this.Controls.Add(buttonX3);
            this.Controls.Add(buttonX1);
            this.Controls.Add(buttonX2);
            this.Controls.Add(this.metroStatusBar1);
            this.Cursor = System.Windows.Forms.Cursors.Default;
            this.DoubleBuffered = true;
            this.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "frmPoliza";
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Poliza";
            this.Load += new System.EventHandler(this.frmPoliza_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dtgTransaccion)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevComponents.DotNetBar.Metro.MetroStatusBar metroStatusBar1;
        private DevComponents.DotNetBar.LabelItem labelItem1;
        private DevComponents.DotNetBar.Controls.TextBoxX tipoField;
        private DevComponents.DotNetBar.LabelX labelX1;
        private DevComponents.DotNetBar.LabelX labelX2;
        private DevComponents.DotNetBar.LabelX labelX3;
        private DevComponents.DotNetBar.Controls.TextBoxX numField;
        private System.Windows.Forms.DateTimePicker fechaField;
        private DevComponents.DotNetBar.LabelX labelX4;
        private DevComponents.DotNetBar.Controls.TextBoxX conceptoField;
        private DevComponents.DotNetBar.Controls.CircularProgress circularProgress3;
        private DevComponents.DotNetBar.ButtonItem buttonItem3;
        private DevComponents.DotNetBar.ButtonX buttonX16;
        private DevComponents.DotNetBar.ButtonX buttonX18;
        private DevComponents.DotNetBar.ButtonX buttonX19;
        private DevComponents.DotNetBar.Controls.DataGridViewX dtgTransaccion;
        private DevComponents.DotNetBar.SuperTabItem superTabItem1;
        private System.Windows.Forms.FolderBrowserDialog folderBrowserDialog1;
        private System.Windows.Forms.DataGridViewTextBoxColumn ROW_ID;
        private System.Windows.Forms.DataGridViewTextBoxColumn ROW_ID_POLIZA;
        private System.Windows.Forms.DataGridViewTextBoxColumn numCtaField;
        private System.Windows.Forms.DataGridViewTextBoxColumn conceptoField_t;
        private System.Windows.Forms.DataGridViewTextBoxColumn debeField;
        private System.Windows.Forms.DataGridViewTextBoxColumn haberField;
        private System.Windows.Forms.DataGridViewTextBoxColumn monedaField;
        private DevComponents.DotNetBar.LabelX labelX8;
        private DevComponents.DotNetBar.Controls.TextBoxX HABER_TOTAL;
        private DevComponents.DotNetBar.LabelX labelX7;
        private DevComponents.DotNetBar.Controls.TextBoxX DEBE_TOTAL;
        private DevComponents.DotNetBar.LabelX labelX5;
        private DevComponents.DotNetBar.ButtonX buttonX4;
        private DevComponents.DotNetBar.Controls.ComboBoxEx ASIGNADO;
        private DevComponents.DotNetBar.ButtonItem buttonItem2;
        private DevComponents.DotNetBar.ButtonX buttonX5;
        private DevComponents.DotNetBar.ButtonItem buttonItem1;
        private DevComponents.DotNetBar.ButtonX buttonX6;
        private DevComponents.DotNetBar.ButtonItem buttonItem4;

    }
}