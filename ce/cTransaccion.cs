﻿using DevComponents.DotNetBar;
using Generales;
using ModeloDocumentosFX;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Desarrollo
{
    class cTransaccion
    {
        private string sSQL = "";
        public string ROW_ID { get; set; }

        public string ROW_ID_POLIZA { get; set; }

        public string numCtaField { get; set; }
        public string conceptoField { get; set; }
        public string monedaField { get; set; }
        public double debeField { get; set; }
        public double haberField { get; set; }
        
        private cCONEXCION oData = new cCONEXCION();
        
        public string DOCUMENTO { get; set; }
        public string TIPO { get; set; }
        public string ENTE { get; set; }


        public override string ToString() { return ROW_ID; }

        public cTransaccion()
        {
            ModeloDocumentosFXContainer dbContext = new ModeloDocumentosFXContainer();
            try
            {
                string conection = dbContext.Database.Connection.ConnectionString;
                oData = new cCONEXCION(conection);
            }
            catch (Exception err)
            {
                ErrorFX.mostrar(err, false, true, "frmEmpresa - 96 - ", false);
            }
            limpiar();
        }


        public void limpiar()
        {
            ROW_ID = "";
            ROW_ID_POLIZA = "";
            numCtaField="";
            debeField = 0;
            haberField = 0;
            conceptoField="";
            monedaField="";
            
            DOCUMENTO="";
            TIPO="";
            ENTE="";

        }


        public List<cTransaccion> todos(string ROW_ID_POLIZAp="",string numCtaFieldp = "", string ASIGNADOp=""
            , string condicion_extra="")
        {

            List<cTransaccion> lista = new List<cTransaccion>();

            sSQL = " SELECT * ";
            sSQL += " FROM PolizasPolizaTransaccion ";
            sSQL += " WHERE 1=1 ";
            if (ROW_ID_POLIZAp!="")
            {
                sSQL += " AND ROW_ID_POLIZA=" + ROW_ID_POLIZAp + "";
            }
            sSQL += " AND numCtaField LIKE '%" + numCtaFieldp + "%' ";
            if(ASIGNADOp!="")
            {
                sSQL += " AND ASIGNADO='" + ASIGNADOp + "' ";
            }
            if (condicion_extra!="")
            {
                sSQL += " AND " + condicion_extra;
            }


            sSQL += " ORDER BY numCtaField";
            DataTable oDataTable = oData.EjecutarConsulta(sSQL);
            foreach (DataRow oDataRow in oDataTable.Rows)
            {
                cTransaccion ocCatalogo = new cTransaccion();
                ocCatalogo.cargar(oDataRow["ROW_ID"].ToString());
                lista.Add(ocCatalogo);
            }
            return lista;

        }

        public bool verificar(string numCtaFieldp, string ROW_ID_POLIZAp)
        {
            sSQL = " SELECT * ";
            sSQL += " FROM PolizasPolizaTransaccion ";
            sSQL += " WHERE numCtaField='" + numCtaFieldp + "' ";
            sSQL += " AND ROW_ID_POLIZA=" + ROW_ID_POLIZAp + "";
            DataTable oDataTable = oData.EjecutarConsulta(sSQL);
            if (oDataTable != null)
            {
                foreach (DataRow oDataRow in oDataTable.Rows)
                {                    
                    return true;
                }
            }
            return false;
        }

        public bool cargar(string ROW_IDp)
        {
            sSQL = " SELECT * ";
            sSQL += " FROM PolizasPolizaTransaccion ";
            sSQL += " WHERE ROW_ID=" + ROW_IDp + " ";
            DataTable oDataTable = oData.EjecutarConsulta(sSQL);
            if (oDataTable != null)
            {
                foreach (DataRow oDataRow in oDataTable.Rows)
                {
                    //Añadir el Registro
                    ROW_ID = oDataRow["ROW_ID"].ToString();
                    ROW_ID_POLIZA = oDataRow["ROW_ID_POLIZA"].ToString();
                    numCtaField = oDataRow["numCtaField"].ToString();
                    conceptoField = oDataRow["conceptoField"].ToString();
                    monedaField = oDataRow["monedaField"].ToString();
                    debeField = double.Parse(oDataRow["debeField"].ToString());
                    haberField = double.Parse(oDataRow["haberField"].ToString());
                    DOCUMENTO = oDataRow["DOCUMENTO"].ToString();
                    TIPO = oDataRow["TIPO"].ToString();
                    ENTE = oDataRow["ENTE"].ToString();
                    return true;
                }
            }
            return false;
        }

        public bool guardar(bool validar=true, bool eliminar=false)
        {
            DataTable oDataTable;
            if (validar)
            {

                if (numCtaField.Trim() == "")
                {
                    MessageBoxEx.Show("La cuenta es requerida.", Application.ProductName + "-" + Application.ProductVersion, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                    return false;
                }
                
            }

            if (eliminar)
            {
                sSQL = "SELECT TOP 1 ROW_ID "
                + " FROM PolizasPolizaTransaccion "
                + " WHERE ROW_ID_POLIZA=" + ROW_ID_POLIZA
                + " AND numCtaField='" + numCtaField + "' AND conceptoField='" + conceptoField.Replace("'", "") + "'"
                + " AND [haberField]=" + haberField.ToString("N4").Replace(",", "") + " AND [monedaField]='" + monedaField + "'"
                + " AND [debeField]=" + debeField.ToString("N4").Replace(",", "") + ""
                + " ORDER BY ROW_ID";
                oDataTable = oData.EjecutarConsulta(sSQL);
                if (oDataTable != null)
                {
                    foreach (DataRow oDataRow in oDataTable.Rows)
                    {
                        sSQL = " DELETE FROM PolizasPolizaTransaccion "
                        + " WHERE ROW_ID_POLIZA=" + ROW_ID_POLIZA
                        + " AND numCtaField='" + numCtaField + "' AND conceptoField='" + conceptoField.Replace("'", "") + "'"
                        + " AND [haberField]=" + haberField.ToString("N4").Replace(",", "") + " AND [monedaField]='" + monedaField + "'"
                        + " AND [debeField]=" + debeField.ToString("N4").Replace(",", "") + ""
                        + " AND ROW_ID=" + oDataRow["ROW_ID"].ToString();
                        oData.EjecutarConsulta(sSQL);
                    }
                }
            }

            if (ROW_ID == "")
            {
                sSQL = " INSERT INTO PolizasPolizaTransaccion ";
                sSQL += " ( ";
                sSQL += " [ROW_ID_POLIZA],[numCtaField],[conceptoField]";
                sSQL += ",[haberField],[monedaField],[debeField],ASIGNADO ";
                sSQL += ",DOCUMENTO,TIPO,ENTE ";
                sSQL += " )";
                sSQL += " VALUES ";
                sSQL += "(";
                sSQL += " " + ROW_ID_POLIZA + ",'" + numCtaField + "','" + conceptoField.Replace("'","") + "'";
                sSQL += "," + haberField.ToString("N4").Replace(",", "") + ",'" + monedaField  + "'";
                sSQL += "," + debeField.ToString("N4").Replace(",", "") + " ,'" + ASIGNADO + "'";
                sSQL += ",'" + DOCUMENTO + "','" + TIPO + "','" + ENTE + "'";
                sSQL += ")";

                

                oDataTable = oData.EjecutarConsulta(sSQL);
                if (oDataTable != null)
                {

                    sSQL = "SELECT TOP 1 MAX(ROW_ID) as ROW_ID FROM PolizasPolizaTransaccion";
                    oDataTable = oData.EjecutarConsulta(sSQL);
                    if (oDataTable != null)
                    {
                        foreach (DataRow oDataRow in oDataTable.Rows)
                        {
                            ROW_ID = oDataRow["ROW_ID"].ToString();
                        }
                    }
                }

            }
            else
            {
                sSQL = " UPDATE PolizasPolizaTransaccion ";
                sSQL += " SET ROW_ID_POLIZA=" + ROW_ID_POLIZA + ",numCtaField='" + numCtaField + "',conceptoField='" + conceptoField.Replace("'", "") + "',ASIGNADO='" + ASIGNADO + "'";
                sSQL += ",haberField=" + haberField.ToString("N4").Replace(",", "") + ",monedaField='" + monedaField + "',debeField=" + debeField.ToString("N4").Replace(",", "") + " ";
                sSQL += ",DOCUMENTO='" + DOCUMENTO + "',TIPO='" + TIPO + "',ENTE='" + ENTE + "'";
                sSQL += " WHERE ROW_ID=" + ROW_ID;
                oData.EjecutarConsulta(sSQL);

            }
            return true;

        }

        public bool eliminar(string ROW_IDp)
        {
            if (ROW_IDp != "")
            {
                sSQL = "DELETE FROM PolizasPolizaTransaccion ";
                sSQL += " WHERE ROW_ID='" + ROW_IDp + "' ";
                oData.EjecutarConsulta(sSQL);
                //Eliminar transacciones PolizasPolizaTransaccionCompExt
                sSQL = " DELETE FROM PolizasPolizaTransaccionCompExt "
                + " FROM PolizasPolizaTransaccion INNER JOIN PolizasPolizaTransaccionCompExt ON PolizasPolizaTransaccion.ROW_ID = PolizasPolizaTransaccionCompExt.ROW_ID_TRANSACCION "
                + " WHERE (PolizasPolizaTransaccion.ROW_ID = " + ROW_IDp + ") ";
                oData.EjecutarConsulta(sSQL);
                //Eliminar transacciones PolizasPolizaTransaccionCompNal
                sSQL = " DELETE FROM PolizasPolizaTransaccionCompNal "
                + " FROM PolizasPolizaTransaccion INNER JOIN PolizasPolizaTransaccionCompNal ON PolizasPolizaTransaccion.ROW_ID = PolizasPolizaTransaccionCompNal.ROW_ID_TRANSACCION "
                + " WHERE (PolizasPolizaTransaccion.ROW_ID = " + ROW_IDp + ") ";
                oData.EjecutarConsulta(sSQL);
                //Eliminar transacciones PolizasPolizaTransaccionCompNalOtr
                sSQL = " DELETE FROM PolizasPolizaTransaccionCompNalOtr "
                + " FROM PolizasPolizaTransaccion INNER JOIN PolizasPolizaTransaccionCompNalOtr ON PolizasPolizaTransaccion.ROW_ID = PolizasPolizaTransaccionCompNalOtr.ROW_ID_TRANSACCION "
                + " WHERE (PolizasPolizaTransaccion.ROW_ID = " + ROW_IDp + ") ";
                oData.EjecutarConsulta(sSQL);
                //Eliminar transacciones PolizasPolizaTransaccionOtrMetodoPago
                sSQL = " DELETE FROM PolizasPolizaTransaccionOtrMetodoPago "
                + " FROM PolizasPolizaTransaccion INNER JOIN PolizasPolizaTransaccionOtrMetodoPago ON PolizasPolizaTransaccion.ROW_ID = PolizasPolizaTransaccionOtrMetodoPago.ROW_ID_TRANSACCION "
                + " WHERE (PolizasPolizaTransaccion.ROW_ID = " + ROW_IDp + ") ";
                oData.EjecutarConsulta(sSQL);
                return true;
            }
            return false;
        }

        public string ASIGNADO { get; set; }

        internal cTransaccion cargar_id(string ROW_ID_CATALOGOp, string Poliza, string numCtaFieldp)
        {
            cTransaccion ocTransaccion = new cTransaccion();
            sSQL = "SELECT TOP 1 PolizasPolizaTransaccion.ROW_ID as ROW_ID "
                + " FROM PolizasPolizaTransaccion INNER JOIN Poliza ON PolizasPolizaTransaccion.ROW_ID_POLIZA = Poliza.ROW_ID "
                + " WHERE (Poliza.numField = '" + Poliza + "') "
                + " AND (Poliza.ROW_ID_CATALOGO = " + ROW_ID_CATALOGOp + ") "
                + "   AND (PolizasPolizaTransaccion.numCtaField = '" + numCtaFieldp  + "')";


            DataTable oDataTable = oData.EjecutarConsulta(sSQL);
            if (oDataTable != null)
            {
                foreach (DataRow oDataRow in oDataTable.Rows)
                {
                    ocTransaccion.cargar(oDataRow["ROW_ID"].ToString());
                    return ocTransaccion;
                }
            }
            return null;
        }


        internal void eliminar_duplicidad(string ROW_ID_POLIZAp)
        {
            sSQL = "SELECT TOP 1 ROW_ID "
            + " FROM PolizasPolizaTransaccion "
            + " WHERE ROW_ID_POLIZA=" + ROW_ID_POLIZAp
            + " AND numCtaField='" + numCtaField + "' AND conceptoField='" + conceptoField.Replace("'", "") + "'"
            + " AND [haberField]=" + haberField.ToString("N4").Replace(",", "") + " AND [monedaField]='" + monedaField + "'"
            + " AND [debeField]=" + debeField.ToString("N4").Replace(",", "") + ""
            + " ORDER BY ROW_ID";
            DataTable oDataTable = oData.EjecutarConsulta(sSQL);
            if (oDataTable != null)
            {
                foreach (DataRow oDataRow in oDataTable.Rows)
                {
                    sSQL = "SELECT TOP 1 ROW_ID "
                    + " WHERE ROW_ID_POLIZA=" + ROW_ID_POLIZAp
                    + " AND numCtaField='" + numCtaField + "' AND conceptoField='" + conceptoField.Replace("'", "") + "'"
                    + " AND [haberField]=" + haberField.ToString("N4").Replace(",", "") + " AND [monedaField]='" + monedaField + "'"
                    + " AND [debeField]=" + debeField.ToString("N4").Replace(",", "") + ""
                    + " AND ROW_ID<>" + oDataRow["ROW_ID"].ToString()
                    + " ORDER BY ROW_ID";
                    DataTable oDataTable2 = oData.EjecutarConsulta(sSQL);
                    if (oDataTable2 != null)
                    {
                        foreach (DataRow oDataRow2 in oDataTable2.Rows)
                        {
                            cTransaccion ocTransaccion = new cTransaccion();
                            ocTransaccion.eliminar(oDataRow2["ROW_ID"].ToString());
                        }
                    }
                }
            }
        }


        public bool eliminar_poliza(string ROW_ID_POLIZAp)
        {
            if (ROW_ID_POLIZAp != "")
            {
                sSQL = "DELETE FROM PolizasPolizaTransaccion ";
                sSQL += " WHERE ROW_ID_POLIZA='" + ROW_ID_POLIZAp + "' ";
                oData.EjecutarConsulta(sSQL);
                //Eliminar transacciones PolizasPolizaTransaccionCompExt
                sSQL = " DELETE FROM PolizasPolizaTransaccionCompExt "
                + " FROM PolizasPolizaTransaccion INNER JOIN PolizasPolizaTransaccionCompExt ON PolizasPolizaTransaccion.ROW_ID = PolizasPolizaTransaccionCompExt.ROW_ID_TRANSACCION "
                + " WHERE (PolizasPolizaTransaccion.ROW_ID_POLIZA = " + ROW_ID_POLIZAp + ") ";
                oData.EjecutarConsulta(sSQL);
                //Eliminar transacciones PolizasPolizaTransaccionCompNal
                sSQL = " DELETE FROM PolizasPolizaTransaccionCompNal "
                + " FROM PolizasPolizaTransaccion INNER JOIN PolizasPolizaTransaccionCompNal ON PolizasPolizaTransaccion.ROW_ID = PolizasPolizaTransaccionCompNal.ROW_ID_TRANSACCION "
                + " WHERE (PolizasPolizaTransaccion.ROW_ID_POLIZA = " + ROW_ID_POLIZAp + ") ";
                oData.EjecutarConsulta(sSQL);
                ////Eliminar transacciones PolizasPolizaTransaccionCompNalOtr
                ////sSQL = " DELETE FROM PolizasPolizaTransaccionCompNalOtr "
                ////+ " FROM PolizasPolizaTransaccion INNER JOIN PolizasPolizaTransaccionCompNalOtr ON PolizasPolizaTransaccion.ROW_ID = PolizasPolizaTransaccionCompNalOtr.ROW_ID_TRANSACCION "
                ////+ " WHERE (PolizasPolizaTransaccion.ROW_ID_POLIZA = " + ROW_ID_POLIZAp + ") ";
                //oData.EjecutarConsulta(sSQL);
                ////Eliminar transacciones PolizasPolizaTransaccionOtrMetodoPago
                ////sSQL = " DELETE FROM PolizasPolizaTransaccionOtrMetodoPago "
                ////+ " FROM PolizasPolizaTransaccion INNER JOIN PolizasPolizaTransaccionOtrMetodoPago ON PolizasPolizaTransaccion.ROW_ID = PolizasPolizaTransaccionOtrMetodoPago.ROW_ID_TRANSACCION "
                ////+ " WHERE (PolizasPolizaTransaccion.ROW_ID_POLIZA = " + ROW_ID_POLIZAp + ") ";
                //oData.EjecutarConsulta(sSQL);
                return true;
            }
            return false;
        }

    }
}
