using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevComponents.DotNetBar;
using DevComponents.DotNetBar.Controls;

namespace Desarrollo
{
    public partial class frmVISUAL_Excepcion : DevComponents.DotNetBar.Metro.MetroForm
    {
        private cCONEXCION oData = new cCONEXCION("");
        private cCONEXCION oData_ERP = new cCONEXCION("");
        //private cTRASPASO_EXCEPCION_CXP oObjeto;
        //private bool modificado;

        public frmVISUAL_Excepcion(string sConn, string sConn_ERP)
        {
            oData.sConn = sConn;
            oData_ERP.sConn = sConn_ERP;
            oObjeto = new cTRASPASO_EXCEPCION_CXP(oData);
            InitializeComponent();
            limpiar();
        }
        public frmVISUAL_Excepcion(string sConn, string sConn_ERP, string ROW_IDp)
        {
            modificado = false;
            oData.sConn = sConn;
            oData_ERP.sConn = sConn_ERP;
            oObjeto = new cTRASPASO_EXCEPCION_CXP(oData);

            InitializeComponent();

            limpiar();
            oObjeto.ROW_ID = ROW_IDp;
            cargar();
        }

        public void limpiar()
        {
            if (modificado)
            {

                DialogResult Resultado = MessageBoxEx.Show("�Desea guardar los cambios?", Application.ProductName + "-" + Application.ProductVersion, MessageBoxButtons.YesNo);
                if (Resultado.ToString() == "Yes")
                {
                    guardar();
                }


            }
            modificado = false;
            oObjeto = new cTRASPASO_EXCEPCION_CXP(oData);

            foreach (var c in this.Controls)
            {
                if (c is TextBox)
                {
                    ((TextBox)c).Text = "";
                }
            }
        }

        private void eliminar()
        {

            DialogResult Resultado = MessageBoxEx.Show("�Desea eliminar?", Application.ProductName + "-" + Application.ProductVersion, MessageBoxButtons.YesNo);
            if (Resultado.ToString() == "Yes")
            {
                if (oObjeto.eliminar(oObjeto.ROW_ID))
                {
                    modificado = false;
                    limpiar();
                }
            }
        }

        private void guardar()
        {

            oObjeto.ACCOUNT_ID = ACCOUNT_ID.Text;
            if (oObjeto.guardar())
            {
                toolStripStatusLabel1.Text = "Datos Actualizados " + DateTime.Now.ToShortDateString() + " " + DateTime.Now.ToLongTimeString();
                modificado = false;
            }
        }

        private void cargar()
        {
            if (oObjeto.cargar(oObjeto.ROW_ID))
            {
                ACCOUNT_ID.Text=oObjeto.ACCOUNT_ID;
                buscar_descripcion(ACCOUNT_ID);

                toolStripStatusLabel1.Text  = "Datos Cargados " + DateTime.Now.ToShortDateString() + " " + DateTime.Now.ToLongTimeString();
                modificado = false;

            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            guardar();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            limpiar();
        }

        private void frmUsuario_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (modificado)
            {
                DialogResult Resultado = MessageBoxEx.Show("�Desea guardar los cambios?", Application.ProductName + "-" + Application.ProductVersion, MessageBoxButtons.YesNo);
                if (Resultado.ToString() == "Yes")
                {
                    guardar();
                }
            }
        }

        private void frmUsuario_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)27)
            {
                this.Close();
            }
            else
            {
                modificado = true;
                if ((e.KeyChar.ToString() == "<")
                                | (e.KeyChar.ToString() == ">")
                                
                                | (e.KeyChar.ToString() == "'")
                                | (e.KeyChar.ToString() == "#")
                                )
                {
                    e.Handled = true;
                }
            }
        }

        private void buttonX2_Click(object sender, EventArgs e)
        {
            limpiar();
        }

        private void buttonX1_Click(object sender, EventArgs e)
        {
            guardar();
        }

        private void buttonX3_Click(object sender, EventArgs e)
        {
            eliminar();
        }

        private void buscar_descripcion(TextBoxX contenedorp)
        {
            string buscarp = contenedorp.Name + "_D";

            Control[] tbxs = this.Controls.Find(buscarp, true);
            if (tbxs != null && tbxs.Length > 0)
            {
                cACCOUNT ocACCOUNT = new cACCOUNT(oData_ERP);
                ocACCOUNT.cargar(contenedorp.Text);
                tbxs[0].Text = ocACCOUNT.DESCRIPCION;
            }
        }

        private void buscar_ACCOUNT(TextBoxX IDp)
        {
            frmACCOUNT oBuscador = new frmACCOUNT(oData_ERP.sConn, "S");
            if (oBuscador.ShowDialog() == DialogResult.OK)
            {
                IDp.Text = oBuscador.selecionados[0].Cells["ID"].Value.ToString();
                buscar_descripcion(IDp);
            }
        }

        private void buttonX11_Click(object sender, EventArgs e)
        {
            buscar_ACCOUNT(ACCOUNT_ID);
        }

        private void buttonX13_Click(object sender, EventArgs e)
        {
            limpiar();
        }
       
        private void buttonX12_Click(object sender, EventArgs e)
        {
            guardar();
        }

        private void buttonX2_Click_1(object sender, EventArgs e)
        {
            eliminar();
        }

        private void ACCOUNT_Leave(object sender, EventArgs e)
        {
            verificar_ACCOUNT_ID((TextBoxX)sender);
        }

        private void verificar_ACCOUNT_ID(TextBoxX IDp)
        {
            cACCOUNT ocACCOUNT = new cACCOUNT(oData_ERP);
            if (ocACCOUNT.cargar(IDp.Text))
            {
                buscar_descripcion(IDp);
            }
            else
            {
                buscar_ACCOUNT(IDp);
            }
            
        }
    }
}