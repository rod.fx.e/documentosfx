
GO

/****** Object:  Table [dbo].[Balanza]    Script Date: 01/13/2015 16:23:19 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[Balanza](
	[ROW_ID] [bigint] IDENTITY(1,1) NOT NULL,
	[ROW_ID_CATALOGO] [bigint] NOT NULL,
	[numCtaField] [varchar](max) NULL,
	[saldoIniField] [decimal](18, 4) NULL,
	[debeField] [decimal](18, 4) NULL,
	[haberField] [decimal](18, 4) NULL,
	[saldoFinField] [decimal](18, 4) NULL,
 CONSTRAINT [PK_Balanza] PRIMARY KEY CLUSTERED 
(
	[ROW_ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO


GO

/****** Object:  Table [dbo].[Catalogo]    Script Date: 01/13/2015 16:23:19 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[Catalogo](
	[ROW_ID] [bigint] IDENTITY(1,1) NOT NULL,
	[versionField] [varchar](max) NULL,
	[rFCField] [varchar](max) NULL,
	[mesField] [bigint] NULL,
	[anoField] [bigint] NULL,
	[totalCtasField] [bigint] NULL,
	[ESTADO] [varchar](max) NULL,
 CONSTRAINT [PK_Catalogo] PRIMARY KEY CLUSTERED 
(
	[ROW_ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO


GO

/****** Object:  Table [dbo].[CatalogoCtas]    Script Date: 01/13/2015 16:23:19 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[CatalogoCtas](
	[ROW_ID] [bigint] IDENTITY(1,1) NOT NULL,
	[codAgrupField] [varchar](max) NULL,
	[numCtaField] [varchar](max) NULL,
	[descField] [varchar](max) NULL,
	[subCtaDeField] [varchar](max) NULL,
	[nivelField] [bigint] NULL,
	[naturField] [varchar](max) NULL,
	[ROW_ID_CATALOGO] [bigint] NULL,
 CONSTRAINT [PK_CatalogoCtas] PRIMARY KEY CLUSTERED 
(
	[ROW_ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO


GO

/****** Object:  Table [dbo].[EMPRESA]    Script Date: 01/13/2015 16:23:19 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[EMPRESA](
	[ROW_ID] [bigint] IDENTITY(1,1) NOT NULL,
	[ID] [varchar](max) NULL,
	[RFC] [varchar](max) NULL,
	[DIRECTORIO_XML_CLIENTES] [varchar](max) NULL,
	[DIRECTORIO_XML_PROVEEDORES] [varchar](max) NULL,
	[VISUAL_TIPO] [varchar](max) NULL,
	[VISUAL_SERVIDOR] [varchar](max) NULL,
	[VISUAL_BD] [varchar](max) NULL,
	[VISUAL_USUARIO] [varchar](max) NULL,
	[VISUAL_PASSWORD] [varchar](max) NULL,
	[VISUAL_VERSION] [varchar](max) NULL,
	[VISUAL_ENTITY_ID] [varchar](max) NULL,
	[VISUAL_SITE_ID] [varchar](max) NULL,
	[NIVEL] [varchar](max) NULL,
 CONSTRAINT [PK_EMPRESA] PRIMARY KEY CLUSTERED 
(
	[ROW_ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE EMPRESA ADD  [NIVEL] [varchar](max) NULL
GO

SET ANSI_PADDING OFF
GO


GO

/****** Object:  Table [dbo].[EMPRESA_CONEXION]    Script Date: 01/13/2015 16:23:19 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[EMPRESA_CONEXION](
	[ROW_ID] [bigint] IDENTITY(1,1) NOT NULL,
	[ROW_ID_EMPRESA] [bigint] NULL,
	[VISUAL_TIPO] [varchar](max) NULL,
	[VISUAL_SERVIDOR] [varchar](max) NULL,
	[VISUAL_BD] [varchar](max) NULL,
	[VISUAL_USUARIO] [varchar](max) NULL,
	[VISUAL_PASSWORD] [varchar](max) NULL,
	[VISUAL_VERSION] [varchar](max) NULL,
	[VISUAL_ENTITY_ID] [varchar](max) NULL,
	[VISUAL_SITE_ID] [varchar](max) NULL,
	[VISUAL_CURRENCY_ID] [varchar](max) NULL,
 CONSTRAINT [PK_EMPRESA_CONEXION] PRIMARY KEY CLUSTERED 
(
	[ROW_ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO


CREATE TABLE [dbo].[EMPRESA_DESCRIPCION](
	[ROW_ID] [bigint] IDENTITY(1,1) NOT NULL,
	[ROW_ID_EMPRESA] [bigint] NULL,
	[VISUAL_DESCRIPCION] [varchar](max) NULL,
	[DESCRIPCION] [varchar](max) NULL,
 CONSTRAINT [PK_EMPRESA_DESCRIPCION] PRIMARY KEY CLUSTERED 
(
	[ROW_ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO


GO

/****** Object:  Table [dbo].[Poliza]    Script Date: 01/13/2015 16:23:19 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[Poliza](
	[ROW_ID] [bigint] IDENTITY(1,1) NOT NULL,
	[tipoField] [int] NULL,
	[ASIGNADO] [varchar](max) NULL,
	[numField] [varchar](max) NULL,
	[fechaField] [datetime] NULL,
	[conceptoField] [varchar](max) NULL,
	[ROW_ID_CATALOGO] [bigint] NULL,
 CONSTRAINT [PK_Poliza] PRIMARY KEY CLUSTERED 
(
	[ROW_ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO


GO

/****** Object:  Table [dbo].[Polizas]    Script Date: 01/13/2015 16:23:19 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[Polizas](
	[ROW_ID] [bigint] IDENTITY(1,1) NOT NULL,
	[versionField] [varchar](max) NULL,
	[rFCField] [varchar](max) NULL,
	[anoField] [bigint] NULL,
	[totalCtasField] [bigint] NULL,
	[tipoSolicitudField] [varchar](max) NULL,
	[numOrdenField] [varchar](max) NULL,
	[numTramiteField] [varchar](max) NULL,
	[CREATE_DATE] [datetime] NULL,
	[USUARIO] [varchar](max) NULL,
	[selloField] [varchar](max) NULL,
	[noCertificadoField] [varchar](max) NULL,
	[certificadoField] [varchar](max) NULL,
	[ROW_ID_CATALOGO] [bigint] NULL,
 CONSTRAINT [PK_Polizas] PRIMARY KEY CLUSTERED 
(
	[ROW_ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO


SET ANSI_PADDING OFF
GO


GO

/****** Object:  Table [dbo].[PolizasPolizaTransaccion]    Script Date: 01/13/2015 16:23:19 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[PolizasPolizaTransaccion](
	[ROW_ID] [bigint] IDENTITY(1,1) NOT NULL,
	[numCtaField] [varchar](max) NULL,
	[conceptoField] [varchar](max) NULL,
	[debeField] [decimal](15, 4) NOT NULL,
	[haberField] [decimal](15, 4) NOT NULL,
	[tipCambField] [decimal](15, 4) NULL,
	[tipCambFieldSpecified] [char](1) NULL,
	[ROW_ID_POLIZA] [bigint] NULL,
	[monedaField] [varchar](max) NULL,
 CONSTRAINT [PK_PolizasPolizaTransaccion] PRIMARY KEY CLUSTERED 
(
	[ROW_ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO


GO

/****** Object:  Table [dbo].[PolizasPolizaTransaccionCheque]    Script Date: 01/13/2015 16:23:19 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[PolizasPolizaTransaccionCheque](
	[ROW_ID] [bigint] IDENTITY(1,1) NOT NULL,
	[numCtaField] [varchar](max) NULL,
	[bancoField] [varchar](max) NULL,
	[ctaOriField] [varchar](max) NULL,
	[fechaField] [datetime] NULL,
	[montoField] [decimal](15, 4) NOT NULL,
	[benefField] [varchar](max) NULL,
	[rFCField] [varchar](max) NULL,
	[ROW_ID_TRANSACCION] [bigint] NULL,
 CONSTRAINT [PK_PolizasPolizaTransaccionCheque] PRIMARY KEY CLUSTERED 
(
	[ROW_ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO


GO

/****** Object:  Table [dbo].[PolizasPolizaTransaccionComprobantes]    Script Date: 01/13/2015 16:23:19 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[PolizasPolizaTransaccionComprobantes](
	[ROW_ID] [bigint] IDENTITY(1,1) NOT NULL,
	[uUID_CFDIField] [varchar](max) NULL,
	[rFCField] [varchar](max) NULL,
	[ROW_ID_TRANSACCION] [bigint] NULL,
 CONSTRAINT [PK_PolizasPolizaTransaccionComprobantes] PRIMARY KEY CLUSTERED 
(
	[ROW_ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO


GO

/****** Object:  Table [dbo].[PolizasPolizaTransaccionTransferencia]    Script Date: 01/13/2015 16:23:19 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[PolizasPolizaTransaccionTransferencia](
	[ROW_ID] [bigint] IDENTITY(1,1) NOT NULL,
	[ctaOriField] [varchar](max) NULL,
	[bancoOriField] [varchar](max) NULL,
	[montoField] [decimal](15, 4) NOT NULL,
	[ctaDestField] [varchar](max) NULL,
	[bancoDestField] [varchar](max) NULL,
	[fechaField] [datetime] NULL,
	[benefField] [varchar](max) NULL,
	[rFCField] [varchar](max) NULL,
	[ROW_ID_TRANSACCION] [bigint] NULL,
 CONSTRAINT [PK_PolizasPolizaTransaccionTransferencia] PRIMARY KEY CLUSTERED 
(
	[ROW_ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO


/****** Object:  Table [dbo].[PolizasPolizaTransaccionCompNal]    Script Date: 01/13/2015 16:23:19 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO
	
CREATE TABLE [dbo].[PolizasPolizaTransaccionCompNal](
	[ROW_ID] [bigint] IDENTITY(1,1) NOT NULL,
	[uUID_CFDIField] [varchar](max) NULL,
	[rFCField] [varchar](max) NULL,
	[RFC_RECEPTOR] [varchar](max) NULL,
	[montoTotalField] [decimal](15,4) NULL,
	[monedaField] [varchar](max) NULL,
	[tipCambField] [decimal](15,4) NULL,
	[tipCambFieldSpecified] [varchar](max) NULL,
	[TIPO] [varchar](max) NULL,
	[LLAVE] [varchar](max) NULL,
	[ROW_ID_TRANSACCION] [bigint] NULL,
 CONSTRAINT [PK_PolizasPolizaTransaccionCompNal] PRIMARY KEY CLUSTERED 
(
	[ROW_ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO


GO

DROP TABLE [dbo].[ENT_CE_SEGURIDAD]
GO
CREATE TABLE [dbo].[ENT_CE_SEGURIDAD](
	[ROW_ID] [bigint] IDENTITY(1,1) NOT NULL,
	USUARIO [varchar](max) NULL,
	ACCOUNT_ID [varchar](max) NULL,
	SOLO_CONTRATO [varchar](max) NULL,
 CONSTRAINT [PK_ENT_CE_SEGURIDAD] PRIMARY KEY CLUSTERED 
(
	[ROW_ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[PolizasPolizaTransaccionCompNal]    Script Date: 01/13/2015 16:23:19 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[PolizasPolizaTransaccionCompExt](
	[ROW_ID] [bigint] IDENTITY(1,1) NOT NULL,
	[numFactExtField] [varchar](max) NULL,
	[taxIDField] [varchar](max) NULL,
	[RFC_RECEPTOR] [varchar](max) NULL,
	[montoTotalField] [decimal](10,4) NULL,
	[monedaField] [varchar](max) NULL,
	[tipCambField] [decimal](10,4) NULL,
	[tipCambFieldSpecified] [varchar](max) NULL,
	[TIPO] [varchar](max) NULL,
	[LLAVE] [varchar](max) NULL,
	[ROW_ID_TRANSACCION] [bigint] NULL,
 CONSTRAINT [PK_PolizasPolizaTransaccionCompExt] PRIMARY KEY CLUSTERED 
(
	[ROW_ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

CREATE TABLE [dbo].[AuxiliarCtas](
	[ROW_ID] [bigint] IDENTITY(1,1) NOT NULL,
	[tipoSolicitudField] [varchar](max) NULL,
	[numOrdenField] [varchar](max) NULL,
	[numTramiteField] [varchar](max) NULL,
	CREATE_DATE [datetime] NULL,
	USUARIO [varchar](max) NULL,
	[selloField] [varchar](max) NULL,
	[noCertificadoField] [varchar](max) NULL,
	[certificadoField] [varchar](max) NULL,
	[ROW_ID_CATALOGO] [bigint] NULL,
 CONSTRAINT [PK_AuxiliarCtas] PRIMARY KEY CLUSTERED 
(
	[ROW_ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

CREATE TABLE [dbo].[AuxiliarFolios](
	[ROW_ID] [bigint] IDENTITY(1,1) NOT NULL,
	[tipoSolicitudField] [varchar](max) NULL,
	[numOrdenField] [varchar](max) NULL,
	[numTramiteField] [varchar](max) NULL,
	CREATE_DATE [datetime] NULL,
	USUARIO [varchar](max) NULL,
	[selloField] [varchar](max) NULL,
	[noCertificadoField] [varchar](max) NULL,
	[certificadoField] [varchar](max) NULL,
	[ROW_ID_CATALOGO] [bigint] NULL,
 CONSTRAINT [PK_AuxiliarFolios] PRIMARY KEY CLUSTERED 
(
	[ROW_ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

GO
ALTER TABLE EMPRESA ADD 	[CERTIFICADO] [varchar](max) NULL
GO
ALTER TABLE EMPRESA ADD 	[LLAVE] [varchar](max) NULL
GO
ALTER TABLE EMPRESA ADD 	[PASSWORD] [varchar](max) NULL
GO

/*
CREATE VIEW [dbo].[ENT_CE_POLIZA_BALANZA] as 
SELECT T.numCtaField, debe,haber,debeField,haberField,T.ROW_ID_CATALOGO, T.ROW_ID
FROM 
(
SELECT     ISNULL(SUM(PolizasPolizaTransaccion.debeField),0) AS debe, ISNULL(SUM(PolizasPolizaTransaccion.haberField),0) AS haber, PolizasPolizaTransaccion.numCtaField, 
                      Poliza.ROW_ID_CATALOGO, Poliza.ROW_ID
FROM         Poliza LEFT OUTER JOIN
                      PolizasPolizaTransaccion ON Poliza.ROW_ID = PolizasPolizaTransaccion.ROW_ID_POLIZA
GROUP BY PolizasPolizaTransaccion.numCtaField, Poliza.ROW_ID_CATALOGO, Poliza.ROW_ID
) T
, [dbo].[Balanza] B
WHERE B.ROW_ID_CATALOGO=T.ROW_ID_CATALOGO AND B.numCtaField=T.numCtaField
AND 
(B.debeField<>T.debe OR B.haberField<>T.haber)

GO
*/
CREATE VIEW [dbo].[ENT_CE_POLIZA_BALANZA] as 

SELECT numCtaField, debeField as 'debe_Balanza'
,(
SELECT SUM(ABS(PPT.debeField))
FROM PolizasPolizaTransaccion AS PPT INNER JOIN Poliza AS P ON PPT.ROW_ID_POLIZA = P.ROW_ID
WHERE (P.ROW_ID_CATALOGO = B.ROW_ID_CATALOGO) AND PPT.numCtaField=B.numCtaField)
as 'debe_Transacciones'
, haberField as 'haber_Balanza'
,(
SELECT SUM(ABS(PPT.haberField))
FROM PolizasPolizaTransaccion AS PPT INNER JOIN Poliza AS P ON PPT.ROW_ID_POLIZA = P.ROW_ID
WHERE (P.ROW_ID_CATALOGO = B.ROW_ID_CATALOGO) AND PPT.numCtaField=B.numCtaField)
as 'haber_Transacciones'
,B.ROW_ID_CATALOGO
FROM [dbo].[Balanza] B 



GO

ALTER TABLE PolizasPolizaTransaccion ADD 	[ASIGNADO] [varchar](max) NULL
GO
ALTER TABLE PolizasPolizaTransaccion ADD 	[DOCUMENTO] [varchar](max) NULL
GO
ALTER TABLE PolizasPolizaTransaccion ADD 	[ENTE] [varchar](max) NULL
GO
ALTER TABLE PolizasPolizaTransaccion ADD 	[TIPO] [varchar](max) NULL
GO


ALTER TABLE Poliza ADD 	[ASIGNADO] [varchar](max) NULL

GO

ALTER TABLE EMPRESA_CONEXION ADD 	[VISUAL_BD_AUXILIAR] [varchar](max) NULL
GO


ALTER TABLE PolizasPolizaTransaccionCompNal ALTER COLUMN montoTotalField decimal(14,4) NOT NULL
GO
ALTER TABLE PolizasPolizaTransaccionCompExt ALTER COLUMN montoTotalField decimal(14,4) NOT NULL
GO

ALTER TABLE CFDI_PROVEEDORES ALTER COLUMN [montoTotalField] decimal(14,4) NOT NULL
GO
ALTER TABLE CFDI_PROVEEDORES ALTER COLUMN [VENDOR_ID]  [varchar](15) NULL
GO

CREATE TABLE [dbo].[EMPRESA_DESCRIPCION](
	[ROW_ID] [bigint] IDENTITY(1,1) NOT NULL,
	[ROW_ID_EMPRESA] [bigint] NULL,
	[VISUAL_DESCRIPCION] [varchar](max) NULL,
	[DESCRIPCION] [varchar](max) NULL,
 CONSTRAINT [PK_EMPRESA_DESCRIPCION] PRIMARY KEY CLUSTERED 
(
	[ROW_ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO


INSERT INTO [EMPRESA_DESCRIPCION] ([ROW_ID_EMPRESA],[VISUAL_DESCRIPCION],[DESCRIPCION]) VALUES (1,        'Adjs Journal posted','Poliza de Ajuste' )
INSERT INTO [EMPRESA_DESCRIPCION] ([ROW_ID_EMPRESA],[VISUAL_DESCRIPCION],[DESCRIPCION]) VALUES (1,        'Bank adjustment subsidiary ledger','Poliza de Ajuste Bancario' )
INSERT INTO [EMPRESA_DESCRIPCION] ([ROW_ID_EMPRESA],[VISUAL_DESCRIPCION],[DESCRIPCION]) VALUES (1,        'Fin Goods Journal posted','Poliza de Producto Terminado' )
INSERT INTO [EMPRESA_DESCRIPCION] ([ROW_ID_EMPRESA],[VISUAL_DESCRIPCION],[DESCRIPCION]) VALUES (1,        'General Journal Entry','Poliza de Diario' )
INSERT INTO [EMPRESA_DESCRIPCION] ([ROW_ID_EMPRESA],[VISUAL_DESCRIPCION],[DESCRIPCION]) VALUES (1,        'Indirect Journal posted','Poliza de Indirectos' )
INSERT INTO [EMPRESA_DESCRIPCION] ([ROW_ID_EMPRESA],[VISUAL_DESCRIPCION],[DESCRIPCION]) VALUES (1,        'Payables subsidiary ledger','Poliza de CxP' )
INSERT INTO [EMPRESA_DESCRIPCION] ([ROW_ID_EMPRESA],[VISUAL_DESCRIPCION],[DESCRIPCION]) VALUES (1,        'Purchase Journal posted','Poliza de Compras' )
INSERT INTO [EMPRESA_DESCRIPCION] ([ROW_ID_EMPRESA],[VISUAL_DESCRIPCION],[DESCRIPCION]) VALUES (1,        'Receivables subsidiary ledger','Poliza de CxC' )
INSERT INTO [EMPRESA_DESCRIPCION] ([ROW_ID_EMPRESA],[VISUAL_DESCRIPCION],[DESCRIPCION]) VALUES (1,        'Shipments Journal posted','Poliza de Ventas' )
INSERT INTO [EMPRESA_DESCRIPCION] ([ROW_ID_EMPRESA],[VISUAL_DESCRIPCION],[DESCRIPCION]) VALUES (1,        'WIP Journal posted','Poliza de Producción en Proceso' )
GO

ALTER TABLE EMPRESA ADD 	[ACCOUNT_VENDOR] [varchar](max) NULL
GO
ALTER TABLE EMPRESA ADD 	[ACCOUNT_CUSTOMER] [varchar](max) NULL
GO

ALTER TABLE EMPRESA ADD 	[NO_CERTIFICADO] [varchar](max) NULL
GO

ALTER TABLE Poliza ADD  ROW_ID_CATALOGO int NULL
GO
ALTER TABLE Polizas ADD  ROW_ID_CATALOGO int NULL
GO
ALTER TABLE Polizas ADD  ROW_ID_CATALOGO int NULL
GO
ALTER TABLE CFDI_PROVEEDORES ADD  [VENDOR_ID] [varchar](15) NULL
GO


ALTER TABLE EMPRESA_CONEXION ADD auxiliarTipo [varchar](max) NULL
GO
ALTER TABLE EMPRESA_CONEXION ADD auxiliarServidor [varchar](max) NULL
GO
ALTER TABLE EMPRESA_CONEXION ADD auxiliarBaseDatos [varchar](max) NULL
GO
ALTER TABLE EMPRESA_CONEXION ADD auxiliarUsuario [varchar](max) NULL
GO
ALTER TABLE EMPRESA_CONEXION ADD auxiliarPassword [varchar](max) NULL
GO

