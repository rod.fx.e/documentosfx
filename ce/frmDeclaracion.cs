using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevComponents.DotNetBar;
using System.IO;
using OfficeOpenXml;
using System.Threading;
using System.Xml.Serialization;
using DevComponents.DotNetBar.Controls;
using System.Linq;
using Ionic.Zip;
using System.Xml;
using System.Xml.XPath;
using System.Xml.Xsl;
using System.Configuration;
using Generales;
using ModeloDocumentosFX;

namespace Desarrollo
{
    public partial class frmDeclaracion : DevComponents.DotNetBar.Metro.MetroForm
    {
        bool modificado = false;
        cCatalogo oObjeto = new cCatalogo();
        cCONEXCION oData;
        cCONEXCION oData_ERP;
        cEMPRESA_CE ocEMPRESA;
        cEMPRESA_CONEXION ocEMPRESA_CONEXION;
        List<BackgroundWorker> bgws;
        string USUARIO;

        public frmDeclaracion(Object ocEMPRESAp,string USUARIOp)
        {
            try
            {
                USUARIO = USUARIOp;
                oObjeto = new cCatalogo();
                InitializeComponent();
                ModeloDocumentosFXContainer dbContext = new ModeloDocumentosFXContainer();
                try
                {
                    string conection = dbContext.Database.Connection.ConnectionString;
                    oData = new cCONEXCION(conection);
                }
                catch (Exception err)
                {
                    ErrorFX.mostrar(err, false, true, "frmDeclaracion - 49 - ");
                }
                cargar_configuracion();
                ocEMPRESA = new cEMPRESA_CE();
                ocEMPRESA = (cEMPRESA_CE)ocEMPRESAp;

                bgws = new List<BackgroundWorker>();

                limpiar();
            }
            catch(Exception error)
            {
                string mensaje = "";
                mensaje = error.Message.ToString();
                if (error.InnerException != null)
                {
                    mensaje += " " + error.InnerException.Message.ToString();
                }
                MessageBoxEx.Show(mensaje, Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error);

            }

        }

        public frmDeclaracion(Object ocEMPRESAp, string ROW_IDp, string USUARIOp)
        {
            try
            {
                ModeloDocumentosFXContainer dbContext = new ModeloDocumentosFXContainer();
                try
                {
                    string conection = dbContext.Database.Connection.ConnectionString;
                    oData = new cCONEXCION(conection);
                }
                catch (Exception err)
                {
                    ErrorFX.mostrar(err, false, true, "frmDeclaracion - 72 - ", false);
                }
                USUARIO = USUARIOp;
                oObjeto = new cCatalogo();
                InitializeComponent();
                periodoInicial.Enabled = false;
                periodoFinal.Enabled = false;

                cargar_configuracion();
                ocEMPRESA = (cEMPRESA_CE)ocEMPRESAp;
                cargarPeriodo();
                cargar(ROW_IDp);
            }
            catch (Exception error)
            {
                string mensaje = "";
                mensaje = error.Message.ToString();
                if (error.InnerException != null)
                {
                    mensaje += " " + error.InnerException.Message.ToString();
                }
                MessageBoxEx.Show(mensaje, Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error);

            }
        }

        private void cargarPeriodo()
        {
            if (anoField.Text=="")
            {
                return;
            }
            cEMPRESA_CONEXION ocEMPRESA_CONEXION = new cEMPRESA_CONEXION();
            foreach (cEMPRESA_CONEXION registro in ocEMPRESA_CONEXION.todos(ocEMPRESA.ROW_ID))
            {
                cCONEXCION oData_ERP = registro.conectar(registro.ROW_ID);
                this.oData_ERP = registro.conectar(registro.ROW_ID);
                //periodoFinal.DataSource = null;
                //periodoInicial.DataSource = null;
                periodoInicial.Items.Clear();
                periodoFinal.Items.Clear();
                

                //Cargar periodo
                string sSQL = "SELECT ACCT_PERIOD as Value, ACCT_PERIOD as Name, ACCT_PERIOD"
                + " FROM [dbo].[ACCOUNT_PERIOD]"
                + " WHERE ACCT_YEAR=" + anoField.Text;
                DataTable oDataTable = oData_ERP.EjecutarConsulta(sSQL);

                //periodoFinal.DataSource = oDataTable;
                //periodoFinal.DisplayMember = "Name";
                //periodoFinal.ValueMember = "Value";

                //periodoInicial.DataSource = oDataTable;
                //periodoInicial.DisplayMember = "Name";
                //periodoInicial.ValueMember = "Value";


                if (oDataTable != null)
                {
                    foreach (DataRow oDataRow in oDataTable.Rows)
                    {
                        periodoInicial.Items.Add(oDataRow["ACCT_PERIOD"].ToString());
                        periodoFinal.Items.Add(oDataRow["ACCT_PERIOD"].ToString());
                    }
                }
                periodoInicial.Enabled = true;
                periodoFinal.Enabled = true;
                return;
            }


        }

        

        private void cargar_configuracion()
        {
            bool VISUAL = true;
            try
            {
                VISUAL = bool.Parse(ConfigurationManager.AppSettings.Get("VISUAL").ToString());

            }
            catch
            {
                //Cargar la configuarcion
            }
            if(!VISUAL)
            {
                buttonX5.Visible = false;
                buttonX10.Visible = false;
            }

        }


        #region Acciones
        private void cargar(string ROW_IDp)
        {
            if (oObjeto.cargar(ROW_IDp))
            {
                versionField.Text=oObjeto.versionField;
                rFCField.Text = oObjeto.rFCField;
                mesField.Text = oObjeto.mesField;
                anoField.Text = oObjeto.anoField;
                totalCtasField.Text = oObjeto.totalCtasField;
                ESTADO.Text = oObjeto.ESTADO;
                periodoInicial.Text = oObjeto.periodo;
                periodoFinal.Text = oObjeto.periodoFinal;
                usarPeriodo.Checked = oObjeto.usarPeriodo;

                cargar_cuentas();
                cargar_balanza();
            }

        }

        private bool guardar()
        {
            oObjeto.versionField = versionField.Text;
            oObjeto.rFCField = rFCField.Text;
            oObjeto.mesField = mesField.Text;
            oObjeto.anoField = anoField.Text;
            oObjeto.totalCtasField = totalCtasField.Text;
            oObjeto.ESTADO = ESTADO.Text;
            oObjeto.periodo = periodoInicial.Text;
            oObjeto.periodoFinal = periodoFinal.Text;
            oObjeto.usarPeriodo = usarPeriodo.Checked;
            
            if (usarPeriodo.Checked)
            {
                if (String.IsNullOrEmpty(periodoFinal.Text))
                {
                    MessageBoxEx.Show("Seleccione el periodo final", Application.ProductName + "-" + Application.ProductVersion, MessageBoxButtons.OK, MessageBoxIcon.Information);
                    return false;
                }

                if (String.IsNullOrEmpty(periodoInicial.Text))
                {
                    MessageBoxEx.Show("Seleccione el periodo inicial", Application.ProductName + "-" + Application.ProductVersion, MessageBoxButtons.OK, MessageBoxIcon.Information);
                    return false;
                }
                cCONEXCION oConex=new cCONEXCION();
                if (Globales.IsNumeric(periodoInicial.Text) & Globales.IsNumeric(periodoFinal.Text))
                {
                    if (int.Parse(periodoInicial.Text) > int.Parse(periodoFinal.Text))
                    {
                        MessageBoxEx.Show("El periodo inicial no puede ser mayor que el inicial", Application.ProductName + "-" + Application.ProductVersion, MessageBoxButtons.OK, MessageBoxIcon.Information);
                        return false;
                    }
                }
            }

            if (oObjeto.guardar())
            {
                labelItem1.Text = "Datos Actualizados " + DateTime.Now.ToShortDateString() + " " + DateTime.Now.ToLongTimeString();
                mostrar_mensaje("Datos Actualizados ", false);
                modificado = false;
                return true;
            }
            return false;
        }

        private void mostrar_mensaje(string mensaje, bool mostrar_error)
        {
            ToastNotification.Show(this,
                mensaje,
                mostrar_error ? global::Desarrollo.Properties.Resources.error : global::Desarrollo.Properties.Resources.guardar,
                3000,
                mostrar_error ? eToastGlowColor.Red : eToastGlowColor.Green,
                eToastPosition.TopCenter);
        }
        public void limpiar()
        {
            if (modificado)
            {

                DialogResult Resultado = MessageBoxEx.Show("�Desea guardar los cambios?", Application.ProductName + "-" + Application.ProductVersion, MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                if (Resultado.ToString() == "Yes")
                {
                    guardar();
                }
            }
            circularProgress1.Visible = false;
            modificado = false;
            rFCField.Text = ocEMPRESA.RFC;
            //Buscar la ultima versi�n
            //Cargar los meses
            int index_selecionado = 0;
            mesField.Items.Clear();
            for (int i = 1; i < 14; i++)
            {
                string tr = "00" + i.ToString();
                mesField.Items.Add((tr).Substring(tr.Length - 2, 2));
                if (DateTime.Now.Month == i)
                {
                    index_selecionado = i - 1;
                }
            }
            mesField.SelectedIndex = index_selecionado;
            anoField.Text = DateTime.Now.Year.ToString();
            ESTADO.SelectedIndex = 0;
            totalCtasField.Text = "0";
            chkPeriodo.Checked = false;
            usarPeriodo.Checked = false;
            periodoInicial.Enabled = false;
            periodoFinal.Enabled = false;

            periodoInicial.Text = "";
            periodoFinal.Text = "";
            cargarPeriodo();
            dtgCuentas.Rows.Clear();

        }
        private void eliminar()
        {
            DialogResult Resultado = MessageBoxEx.Show("�Esta seguro de eliminar?", Application.ProductName + "-" + Application.ProductVersion, MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            if (Resultado.ToString() != "Yes")
            {
                return;
            }
            if (oObjeto.eliminar(oObjeto.ROW_ID))
            {
                modificado = false;

                MessageBoxEx.Show("Eliminado.", Application.ProductName + "-" + Application.ProductVersion, MessageBoxButtons.OK, MessageBoxIcon.Information);
                limpiar();
            }
        }
        #endregion

        private void buttonX3_Click(object sender, EventArgs e)
        {
            eliminar();
        }

        private void buttonX2_Click(object sender, EventArgs e)
        {
            limpiar();
        }

        private void buttonX1_Click(object sender, EventArgs e)
        {
            guardar();
        }

        private void buttonX7_Click(object sender, EventArgs e)
        {
            nueva_cuenta();
        }

        private void nueva_cuenta()
        {
            if (oObjeto.ROW_ID == "")
            {
                MessageBoxEx.Show("Guarde primero.", Application.ProductName + "-" + Application.ProductVersion, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return;
            }
            frmCuenta ofrmCuenta = new frmCuenta(oObjeto.ROW_ID);
            ofrmCuenta.ShowDialog();
            cargar_cuentas();        
        }

        private void cargar_cuentas()
        {
            if (oObjeto.ROW_ID != "")
            {
                dtgCuentas.Rows.Clear();
                cCatalogoCtas ocCatalogoCtas = new cCatalogoCtas();
                List<cCatalogoCtas> lista=ocCatalogoCtas.todos(oObjeto.ROW_ID);
                oObjeto.totalCtasField=lista.Count.ToString();
                totalCtasField.Text=lista.Count.ToString();
                foreach (cCatalogoCtas registro in lista)
                {
                    int n = dtgCuentas.Rows.Add();
                    actualizar_linea_cuenta(registro, n);
                }
            }
        }

        private void actualizar_linea_cuenta(cCatalogoCtas registro, int n)
        {
            for (int i = 0; i < dtgCuentas.Columns.Count; i++)
            {
                dtgCuentas.Rows[n].Cells[i].Value = "";
            }
            dtgCuentas.Rows[n].Tag = registro;
            dtgCuentas.Rows[n].Cells["ROW_ID"].Value = registro.ROW_ID;
            dtgCuentas.Rows[n].Cells["naturField"].Value = registro.naturField;
            dtgCuentas.Rows[n].Cells["nivelField"].Value = registro.nivelField;
            dtgCuentas.Rows[n].Cells["numCtaField"].Value = registro.numCtaField;
            dtgCuentas.Rows[n].Cells["codAgrupField"].Value = registro.codAgrupField;
            dtgCuentas.Rows[n].Cells["descField"].Value = registro.descField;
            dtgCuentas.Rows[n].Cells["subCtaDeField"].Value = registro.subCtaDeField;
            

        }

        private void buttonX6_Click(object sender, EventArgs e)
        {
            eliminar_cuenta();
        }

        private void eliminar_cuenta()
        {
            dtgCuentas.EndEdit();
            if (dtgCuentas.SelectedCells.Count > 0)
            {
                DialogResult Resultado = MessageBoxEx.Show("�Desea eliminar las seleccionada?", Application.ProductName + "-" + Application.ProductVersion, MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                if (Resultado.ToString() != "Yes")
                {
                    return;
                }
                foreach (DataGridViewRow drv in dtgCuentas.SelectedRows)
                {
                    string ROW_ID = drv.Cells["ROW_ID"].Value.ToString();
                    cCatalogoCtas ocCatalogoCtas = (cCatalogoCtas)drv.Tag;
                    ocCatalogoCtas.eliminar(ocCatalogoCtas.ROW_ID);
                    dtgCuentas.Rows.Remove(drv);
                }
            }
            else
            {
                MessageBoxEx.Show("Seleccione alguna l�nea.", Application.ProductName + "-" + Application.ProductVersion, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }

        private void asignar_cuenta()
        {
            dtgCuentas.EndEdit();
            if (dtgCuentas.SelectedCells.Count > 0)
            {
                //Buscar el Codigo Agrupador
                frmAgrupador ofrmAgrupador = new frmAgrupador();
                if (ofrmAgrupador.ShowDialog() == DialogResult.OK)
                {
                    if (ofrmAgrupador.codAgrupFieldp != "")
                    {
                        DialogResult Resultado = MessageBoxEx.Show("�Desea asignar el c�digo agrupador a las cuentas seleccionadas?", Application.ProductName + "-" + Application.ProductVersion, MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                        if (Resultado.ToString() != "Yes")
                        {
                            return;
                        }
                        foreach (DataGridViewRow drv in dtgCuentas.SelectedRows)
                        {
                            string ROW_ID = drv.Cells["ROW_ID"].Value.ToString();
                            cCatalogoCtas ocCatalogoCtas = (cCatalogoCtas)drv.Tag;
                            ocCatalogoCtas.cargar(ROW_ID);
                            ocCatalogoCtas.codAgrupField = ofrmAgrupador.codAgrupFieldp;
                            ocCatalogoCtas.guardar();
                            actualizar_linea_cuenta(ocCatalogoCtas, drv.Index);
                        }
                    }
                }
            }
            else
            {
                MessageBoxEx.Show("Seleccione alguna l�nea.", Application.ProductName + "-" + Application.ProductVersion, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }

        private void buttonItem1_Click(object sender, EventArgs e)
        {
            exportar_excel();
        }

        private void exportar_excel()
        {

            MessageBoxEx.Show("La configuraci�n de  la Plantilla es la siguiente:" + Environment.NewLine
            + "Fila 1 - Encabezado" + Environment.NewLine
            + "Columna 1: Agrupador " + Environment.NewLine
            + "Columna 2: Cuenta " + Environment.NewLine
            + "Columna 3: Descripci�n " + Environment.NewLine
            + "Columna 4: Subcuenta " + Environment.NewLine
            + "Columna 5: Nivel " + Environment.NewLine
            + "Columna 6: Naturaleza " + Environment.NewLine
            + "Si no tiene esta configuraci�n no ser� cargada la informaci�n. "
            + "Ser� generada una plantilla de uso. "
            , Application.ProductName + "-" + Application.ProductVersion.ToString()
            , MessageBoxButtons.OK
            );

            var fileName = " Plantilla de Cuentas " + DateTime.Now.ToString("yyyy-MM-dd-hh-mm-ss") + ".xlsx";
            if (folderBrowserDialog1.ShowDialog() == DialogResult.OK)
            {
                fileName = folderBrowserDialog1.SelectedPath + "\\" + fileName;
                System.IO.Stream myStream = new System.IO.FileStream(fileName, System.IO.FileMode.Create, System.IO.FileAccess.ReadWrite, System.IO.FileShare.None);

                using (ExcelPackage pck = new ExcelPackage())
                {

                    ExcelWorksheet ws1 = pck.Workbook.Worksheets.Add("Plantilla");
                    int columna = 1;
                    for (int c = 1; c < dtgCuentas.Columns.Count; c++)
                    {
                        if (dtgCuentas.Columns[c].HeaderText != "")
                        {
                            ws1.Cells[1, columna].Value = dtgCuentas.Columns[c].HeaderText;
                            columna++;
                        }
                    }
                    
                    pck.SaveAs(myStream);
                }
                myStream.Close();
                MessageBoxEx.Show("Creaci�n de plantilla realizada " + Environment.NewLine + fileName + " finalizada.", Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private void dtgCuentas_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            editar_cuenta();
        }

        private void editar_cuenta()
        {
            dtgCuentas.EndEdit();
            if (dtgCuentas.SelectedRows.Count > 0)
            {
                foreach (DataGridViewRow drv in dtgCuentas.SelectedRows)
                {
                    string ROW_ID = drv.Cells["ROW_ID"].Value.ToString();
                    frmCuenta ofrmCuenta = new frmCuenta(oObjeto.ROW_ID,ROW_ID);
                    ofrmCuenta.ShowDialog();
                    cCatalogoCtas ocCatalogoCtas = new cCatalogoCtas();
                    ocCatalogoCtas.cargar(ROW_ID);
                    actualizar_linea_cuenta(ocCatalogoCtas, drv.Index);
                }
            }
            else
            {
                MessageBoxEx.Show("Seleccione alguna l�nea.", Application.ProductName + "-" + Application.ProductVersion, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }

        private void importar_cuentas_excel()
        {
            if (oObjeto.ROW_ID == "")
            {
                MessageBoxEx.Show("Guarde primero.", Application.ProductName + "-" + Application.ProductVersion, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return;
            }

            OpenFileDialog openFileDialog1 = new OpenFileDialog();
            openFileDialog1.Filter = "Excel files (*.xlsx)|*.xlsx|All files (*.*)|*.*";
            openFileDialog1.FilterIndex = 1;
            openFileDialog1.RestoreDirectory = true;

            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                try
                {
                    procesar(dtgCuentas, 1, openFileDialog1.FileName);
                }
                catch (Exception)
                {
                    MessageBoxEx.Show("No existe el Archivo seleccionado", Application.ProductName + "-" + Application.ProductVersion, MessageBoxButtons.OK);
                }
            }
        }

        private void buttonX4_Click(object sender, EventArgs e)
        {
            importar_cuentas_excel();
        }


        private void procesar(DataGridView dtg, int hoja, string archivo)
        {
            try
            {
                dtg.Rows.Clear();
                int n;
                int rowIndex = 1;
                FileInfo existingFile = new FileInfo(archivo);
                using (ExcelPackage package = new ExcelPackage(existingFile))
                {
                    ExcelWorksheet worksheet = package.Workbook.Worksheets[hoja];
                    while (worksheet.Cells[rowIndex, 2].Value != null)
                    {
                        labelItem1.Text = "Registro " + rowIndex.ToString();
                        //Agregar las lineas de la cabecera
                        if (rowIndex != 1)
                        {
                            

                            cCatalogoCtas ocCatalogoCtas = new cCatalogoCtas();

                            ocCatalogoCtas.numCtaField = worksheet.Cells[rowIndex, 2].Text;
                            ocCatalogoCtas.cargar_numCta(ocCatalogoCtas.numCtaField);

                            ocCatalogoCtas.ROW_ID_CATALOGO = oObjeto.ROW_ID;
                            ocCatalogoCtas.codAgrupField = worksheet.Cells[rowIndex, 1].Text;
                            
                            ocCatalogoCtas.descField = worksheet.Cells[rowIndex, 3].Text;
                            ocCatalogoCtas.subCtaDeField = worksheet.Cells[rowIndex, 4].Text;
                            ocCatalogoCtas.nivelField = worksheet.Cells[rowIndex, 5].Text;
                            ocCatalogoCtas.naturField = worksheet.Cells[rowIndex, 6].Text;
                            labelItem1.Text = "Cuenta " + ocCatalogoCtas.numCtaField;
                            if (ocCatalogoCtas.guardar(false))
                            {
                                n = dtg.Rows.Add();
                                actualizar_linea_cuenta(ocCatalogoCtas, n);
                            }                           
                        }
                        rowIndex++;
                        
                    }
                    MessageBoxEx.Show("Archivo Importado",Application.ProductName);
                }
            }
            catch (Exception e)
            {
                MessageBoxEx.Show(e.Message.ToString());
            }

        }

        private void buttonX5_Click(object sender, EventArgs e)
        {
            importar_cuentas_visual();
        }


        private void importar_cuentas_visual()
        {
            if (oObjeto.ROW_ID == "")
            {
                if (!guardar())
                {
                    return;
                }
            }

            if (buttonX5.Text == "Detener")
            {
                if (backgroundWorker1.IsBusy)
                {
                    backgroundWorker1.CancelAsync();
                }
                return;
            }

            circularProgress1.Visible = true;
            circularProgress1.IsRunning = true;
            cambiar_boton_visual();
            if (!backgroundWorker1.IsBusy)
            {
                List<object> arguments = new List<object>();
                backgroundWorker1.RunWorkerAsync(arguments);

            }
        }


        private void importar_balanza_visual()
        {
            //if (oObjeto.ROW_ID == "")
            //{
            //    if (!guardar())
            //    {
            //        return;
            //    }
            //}

            if(guardar())
            {
                if (buttonX10.Text == "Detener")
                {
                    if (backgroundWorker2.IsBusy)
                    {
                        backgroundWorker2.CancelAsync();
                    }
                    return;
                }
                circularProgress2.Visible = true;
                circularProgress2.IsRunning = true;
                cambiar_boton_balanza();
                if (!backgroundWorker2.IsBusy)
                {
                    //Eliminar todos los campos de la Balanza de Comprobaci�n
                    string sSQL = "DELETE FROM [dbo].[Balanza] WHERE ROW_ID_CATALOGO=" + oObjeto.ROW_ID;
                    oData.EjecutarConsulta(sSQL);

                    List<object> arguments = new List<object>();
                    backgroundWorker2.RunWorkerAsync(arguments);

                }
            }


        }

        private void cambiar_boton_visual()
        {

            if (buttonX5.Text == "Importar: Visual")
            {
                buttonX5.Image = global::Desarrollo.Properties.Resources.stop_16;
                buttonX5.Text = "Detener";
            }
            else
            {
                buttonX5.Image = global::Desarrollo.Properties.Resources.visual;
                
                buttonX5.Text = "Importar: Visual";
            }
        }
        private void cancelar()
        {
            if (backgroundWorker1.IsBusy)
            {
                backgroundWorker1.CancelAsync();
                cambiar_boton_visual();
            }
        }
        private void backgroundWorker1_DoWork(object sender, DoWorkEventArgs e)
        {
            try
            {
                int n = 0;
                cCatalogo ocCatalogo = new cCatalogo();
                
                try
                {

                    List<object> genericlist = e.Argument as List<object>;
                    //Navegar por cada empresa
                    cEMPRESA_CONEXION ocEMPRESA_CONEXION = new cEMPRESA_CONEXION();
                    labelItem1.Text = "Migrando de Infor Visual Base de datos: " + ocEMPRESA_CONEXION.VISUAL_ENTITY_ID;
                    foreach (cEMPRESA_CONEXION registro in ocEMPRESA_CONEXION.todos(ocEMPRESA.ROW_ID))
                    {
                        cCONEXCION oData_ERP = registro.conectar(registro.ROW_ID);
                        this.oData_ERP = registro.conectar(registro.ROW_ID);
                        string sSQL = "";
                        //if (registro.VISUAL_VERSION == "7.0.0")
                        //{
                        DateTime today = DateTime.Today;
                        DateTime endOfMonth = new DateTime(today.Year, today.Month, DateTime.DaysInMonth(today.Year, today.Month));

                        sSQL = "SELECT * FROM ACCOUNT WHERE [ACTIVE_FLAG]='Y' AND [STATUS_EFF_DATE]<=" + oData.convertir_fecha(endOfMonth,"f") ;
                        bool SUMMARY_ACCOUNT_tr = false;
                        try
                        {
                            string SUMMARY_ACCOUNT_acc=ConfigurationManager.AppSettings.Get("SUMMARY_ACCOUNT").ToString();
                            if (SUMMARY_ACCOUNT_acc=="N")
                            {
                                SUMMARY_ACCOUNT_tr = true;
                            }
                        }
                        catch
                        {

                        }

                        if (SUMMARY_ACCOUNT_tr)
                        {
                            sSQL += " AND [SUMMARY_ACCOUNT]='N'  ";
                        }

                        sSQL += " ORDER BY ID";

                        DataTable oDataTable = oData_ERP.EjecutarConsulta(sSQL);
                        if (oDataTable != null)
                        {
                            foreach (DataRow oDataRow in oDataTable.Rows)
                            {
                                n++;
                                Thread.Sleep(100);
                                List<object> arguments = new List<object>();
                                arguments.Add(registro);
                                arguments.Add(oDataRow);

                                backgroundWorker1.ReportProgress(n, arguments);
                                if (backgroundWorker1.CancellationPending)
                                {
                                    e.Cancel = true;
                                    backgroundWorker1.ReportProgress(0);
                                    return;
                                }
                            }
                        }

                    }   
                }
                catch
                {
                    backgroundWorker1.CancelAsync();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void backgroundWorker1_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            try
            {
                if (!backgroundWorker1.CancellationPending)
                {
                    List<object> parametros = (List<object>)e.UserState;
                    cEMPRESA_CONEXION ocEMPRESA_CONEXIONp = (cEMPRESA_CONEXION)parametros[0];
                    DataRow oDataRow = (DataRow)parametros[1];
                    if (oDataRow != null)
                    {

                        cCatalogoCtas ocCatalogoCtas = new cCatalogoCtas();
                        ocCatalogoCtas.ROW_ID_CATALOGO = oObjeto.ROW_ID;
                        ocCatalogoCtas.numCtaField = oDataRow["ID"].ToString().Trim();
                        ocCatalogoCtas.descField = oDataRow["DESCRIPTION"].ToString();
                        ocCatalogoCtas.subCtaDeField = oDataRow["PARENT_ACCT_ID"].ToString();
                        try
                        {
                            ocCatalogoCtas.nivelField = ocCatalogoCtas.buscar_nivel(oDataRow["ID"].ToString().Trim(), oData_ERP).ToString();
                        }
                        catch(Exception error)
                        {
                            string mensaje = "";
                            mensaje = error.Message.ToString();
                            if (error.InnerException != null)
                            {
                                mensaje += " " + error.InnerException.Message.ToString();
                            }

                            mensaje += "frmDeclaracion - nivelField -  815 ";
                        }

                        BitacoraFX.Log(" ROW_ID_CATALOGO:" + ocCatalogoCtas.ROW_ID_CATALOGO + " ID:" + oDataRow["ID"].ToString().Trim());


                        bool guardar = true;
                        if(!String.IsNullOrEmpty(ocEMPRESA.NIVEL))
                        {
                            try
                            {
                                guardar = false;
                                int nivelt = int.Parse(oData.IsNullNumero(ocCatalogoCtas.nivelField));
                                int nivel_empresa = int.Parse(oData.IsNullNumero(ocEMPRESA.NIVEL));
                                if (nivel_empresa >= nivelt)
                                {
                                    guardar = true;
                                }
                            }
                            catch (Exception error)
                            {
                                string mensaje = "";
                                mensaje = error.Message.ToString();
                                if (error.InnerException != null)
                                {
                                    mensaje += " " + error.InnerException.Message.ToString();
                                }

                                mensaje += "frmDeclaracion - nivelt -  843 ";
                            }

                            
                        }
                        //Definir el tipo de cuenta
                        //E
                        //R
                        //A
                        //L
                        //Q
                        switch (oDataRow["TYPE"].ToString())
                        {
                            
                            case "Q":
                            case "R":
                            case "L":
                                ocCatalogoCtas.naturField = "A";
                                break;
                            case "E":
                            case "A":
                                ocCatalogoCtas.naturField = "D";
                                break;
                        }
                        if(guardar)
                        {
                            try
                            {
                                ocCatalogoCtas.guardar(false);
                            }
                            catch (Exception error)
                            {
                                string mensaje = "";
                                mensaje = error.Message.ToString();
                                if (error.InnerException != null)
                                {
                                    mensaje += " " + error.InnerException.Message.ToString();
                                }

                                mensaje += "frmDeclaracion - guardar -  872 ";
                            }

                            
                        }
                        
                        circularProgress1.Value = e.ProgressPercentage;
                        labelItem1.Text = "Migrando Cuenta Contable: " + oDataRow["ID"].ToString();
                        circularProgress1.ProgressText = "Procesando.. " + e.ProgressPercentage.ToString();// + " de " + TotalRecords;
                    }
                }
            }
            catch (Exception error)
            {
                string mensaje = "";
                mensaje = error.Message.ToString();
                if (error.InnerException != null)
                {
                    mensaje += " " + error.InnerException.Message.ToString();
                }

                mensaje += "frmDeclaracion - 853 ";

                BitacoraFX.Log(mensaje);
                backgroundWorker1.CancelAsync();
                return;
            }
        }

        

        private void backgroundWorker1_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            if (e.Cancelled)
            {
                labelItem1.Text = "Cancelado por el usuario...";
                circularProgress1.Value = 0;
            }
            // Check to see if an error occurred in the background process.
            else if (e.Error != null)
            {
                labelItem1.Text = e.Error.Message;
            }
            else
            {
                //BackGround Task Completed with out Error
                labelItem1.Text = " Finalizado...";
            }
            cambiar_boton_visual();
            cargar_cuentas();
            circularProgress1.Visible = false;
        }

        private void buttonX8_Click(object sender, EventArgs e)
        {
            cargar_cuentas();
        }

        private void buttonX14_Click(object sender, EventArgs e)
        {
            importar_general();
        }
        private void importar_general()
        {
            if (folderBrowserDialog1.ShowDialog() == DialogResult.OK)
            {
                importar_catalogo_cuentas(folderBrowserDialog1.SelectedPath,false);
                importar_catalogo_balanza(folderBrowserDialog1.SelectedPath, false);
                //importar_catalogo_polizas(folderBrowserDialog1.SelectedPath, false);
                MessageBoxEx.Show("Exportaci�n de los archivos finalizada en " + folderBrowserDialog1.SelectedPath.ToString(), Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private void importar_catalogo_cuentas(string pathp, bool mostrar_mensaje_finalizacion)
        {
            
            if(mostrar_mensaje_finalizacion)
            {
                validar("C");
                if (dtgValidador.Rows.Count != 0)
                {
                    DialogResult Resultado = MessageBoxEx.Show("Existen registro invalidos. �Desea continuar?", Application.ProductName + "-" + Application.ProductVersion, MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                    if (Resultado.ToString() != "Yes")
                    {
                        return;
                    }
                }
            }

            cCatalogoCtas ocCatalogoCtas = new cCatalogoCtas();
            ocCatalogoCtas.generar_archivo(oObjeto, pathp, mostrar_mensaje_finalizacion);
            
        }


        private void importar_catalogo_balanza(string pathp, bool mostrar_mensaje_finalizacion)
        {
            //string path = pathp;
            //if (path.Trim() == "")
            //{
            //    if (folderBrowserDialog1.ShowDialog() == DialogResult.OK)
            //    {
            //        path = folderBrowserDialog1.SelectedPath;

            //    }
            //    else
            //    {
            //        MessageBoxEx.Show("Generaci�n de archivo cancelada.", Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
            //        return;
            //    }
            //}
            cBalanza ocBalanza = new cBalanza();
            ocBalanza.generar_archivo(oObjeto,pathp,mostrar_mensaje_finalizacion);

        }

        private void importar_catalogo_polizas(string pathp, bool mostrar_mensaje_finalizacion)
        {
            string path = pathp;
            if (path.Trim() == "")
            {
                if (folderBrowserDialog1.ShowDialog() == DialogResult.OK)
                {
                    path = folderBrowserDialog1.SelectedPath;

                }
                else
                {
                    MessageBoxEx.Show("Generaci�n de archivo cancelada.", Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
                    return;
                }
            }

            //string nombre_tmp = path + "\\" + "Polizas_" + DateTime.Now.ToString("ddMMyyyyhhmmss");
            string nombre_tmp = path + "\\" + "PL" + oObjeto.rFCField + oObjeto.anoField + oObjeto.mesField + ".xml";

            //Generar Poliza
            Polizas oPolizas = new Polizas();
            cPoliza ocPoliza = new cPoliza();
            oPolizas.Anio = int.Parse(oObjeto.anoField);

            switch (int.Parse(mesField.Text))
            {
                case 1:
                    oPolizas.Mes = PolizasMes.Item01;
                    break;
                case 2:
                    oPolizas.Mes = PolizasMes.Item02;
                    break;
                case 3:
                    oPolizas.Mes = PolizasMes.Item03;
                    break;
                case 4:
                    oPolizas.Mes = PolizasMes.Item04;
                    break;
                case 5:
                    oPolizas.Mes = PolizasMes.Item05;
                    break;
                case 6:
                    oPolizas.Mes = PolizasMes.Item06;
                    break;
                case 7:
                    oPolizas.Mes = PolizasMes.Item07;
                    break;
                case 8:
                    oPolizas.Mes = PolizasMes.Item08;
                    break;
                case 9:
                    oPolizas.Mes = PolizasMes.Item09;
                    break;
                case 10:
                    oPolizas.Mes = PolizasMes.Item10;
                    break;
                case 11:
                    oPolizas.Mes = PolizasMes.Item11;
                    break;
                case 12:
                    oPolizas.Mes = PolizasMes.Item12;
                    break;
            } 
            oPolizas.RFC = oObjeto.rFCField;
            oPolizas.Version = oObjeto.versionField;

            List<cPoliza> listacPolizas = new List<cPoliza>();
            listacPolizas = ocPoliza.todos(oObjeto.ROW_ID);
            PolizasPoliza[] oPolizas_p;
            oPolizas_p = new PolizasPoliza[listacPolizas.Count];
            int contador_p = 0;
            foreach (cPoliza registro in listacPolizas)
            {
                oPolizas_p[contador_p] = new PolizasPoliza();

                //oPolizas_p[contador_p].Tipo = int.Parse(registro.tipoField);
                //oPolizas_p[contador_p].Num = registro.numField;
                oPolizas_p[contador_p].NumUnIdenPol = registro.numField;

                oPolizas_p[contador_p].Concepto = registro.conceptoField.Replace("'","");
                oPolizas_p[contador_p].Fecha = registro.fechaField;
                //Buscar las transacciones 
                cTransaccion ocTransaccion = new cTransaccion();
                List<cTransaccion> lista_ocTransaccion = ocTransaccion.todos(registro.ROW_ID);
                PolizasPolizaTransaccion[] PolizasPolizaTransaccion_p;
                PolizasPolizaTransaccion_p = new PolizasPolizaTransaccion[lista_ocTransaccion.Count];
                int contador_t = 0;
                oPolizas_p[contador_p].Transaccion = new PolizasPolizaTransaccion[PolizasPolizaTransaccion_p.Length];
                foreach (cTransaccion registro_t in lista_ocTransaccion)
                {
                    PolizasPolizaTransaccion_p[contador_t] = new PolizasPolizaTransaccion();
                    //PolizasPolizaTransaccion_p[contador_t].numCtaField = registro_t.numCtaField;
                    //PolizasPolizaTransaccion_p[contador_t].debeField = decimal.Parse(registro_t.debeField.ToString());
                    //PolizasPolizaTransaccion_p[contador_t].haberField = decimal.Parse(registro_t.haberField.ToString());
                    //PolizasPolizaTransaccion_p[contador_t].monedaField = registro_t.monedaField;

                    PolizasPolizaTransaccion_p[contador_t].NumCta = registro_t.numCtaField;
                    PolizasPolizaTransaccion_p[contador_t].Debe = decimal.Parse(registro_t.debeField.ToString());
                    PolizasPolizaTransaccion_p[contador_t].Haber = decimal.Parse(registro_t.haberField.ToString());
                    
                    //Agregar Cheques
                    cCheques ocCheques = new cCheques();
                    List<cCheques> lista_cheques = ocCheques.todos(registro_t.ROW_ID);
                    PolizasPolizaTransaccionCheque[] PolizasPolizaTransaccionCheque_p;
                    PolizasPolizaTransaccionCheque_p = new PolizasPolizaTransaccionCheque[lista_cheques.Count];
                    int contador_ch = 0;
                    if (lista_cheques.Count>0)
                    {
                        PolizasPolizaTransaccion_p[contador_t].Cheque = new PolizasPolizaTransaccionCheque[lista_cheques.Count];

                        foreach (cCheques registro_ch in lista_cheques)
                        {
                            PolizasPolizaTransaccion_p[contador_t].Cheque[contador_ch] = new PolizasPolizaTransaccionCheque();
                            PolizasPolizaTransaccionCheque_p[contador_ch] = new PolizasPolizaTransaccionCheque();
                            PolizasPolizaTransaccionCheque_p[contador_ch].Num = registro_ch.numCtaField;
                            PolizasPolizaTransaccionCheque_p[contador_ch].Fecha = registro_ch.fechaField;
                            PolizasPolizaTransaccionCheque_p[contador_ch].Monto = decimal.Parse(registro_ch.montoField.ToString());
                            
                            PolizasPolizaTransaccionCheque_p[contador_ch].BanEmisNal = registro_ch.bancoField;

                            PolizasPolizaTransaccionCheque_p[contador_ch].CtaOri = registro_ch.ctaOriField;
                            PolizasPolizaTransaccionCheque_p[contador_ch].Benef = registro_ch.benefField;
                            PolizasPolizaTransaccionCheque_p[contador_ch].RFC = registro_ch.rFCField;
                            PolizasPolizaTransaccion_p[contador_t].Cheque[contador_ch] = PolizasPolizaTransaccionCheque_p[contador_ch];
                            contador_ch++;
                        }
                        PolizasPolizaTransaccion_p[contador_t].Cheque = PolizasPolizaTransaccionCheque_p;
                    }
                    //Agregar Transferencias
                    cTransferencia ocTransferencia = new cTransferencia();
                    List<cTransferencia> lista_transferencias = ocTransferencia.todos(registro_t.ROW_ID);
                    PolizasPolizaTransaccionTransferencia[] PolizasPolizaTransaccionTransferencia_p;
                    PolizasPolizaTransaccionTransferencia_p = new PolizasPolizaTransaccionTransferencia[lista_transferencias.Count];
                    int contador_tr = 0;
                    if (lista_transferencias.Count > 0)
                    {
                        PolizasPolizaTransaccion_p[contador_t].Transferencia = new PolizasPolizaTransaccionTransferencia[lista_transferencias.Count];

                        foreach (cTransferencia registro_tr in lista_transferencias)
                        {
                            PolizasPolizaTransaccionTransferencia_p[contador_tr] = new PolizasPolizaTransaccionTransferencia();
                            PolizasPolizaTransaccionTransferencia_p[contador_tr].CtaDest = registro_tr.ctaDestField;
                            PolizasPolizaTransaccionTransferencia_p[contador_tr].CtaOri = registro_tr.ctaOriField;
                            PolizasPolizaTransaccionTransferencia_p[contador_tr].BancoDestNal = registro_tr.bancoDestField;
                            PolizasPolizaTransaccionTransferencia_p[contador_tr].BancoOriNal = registro_tr.bancoOriField;
                            PolizasPolizaTransaccionTransferencia_p[contador_tr].Fecha = registro_tr.fechaField;
                            PolizasPolizaTransaccionTransferencia_p[contador_tr].Monto = decimal.Parse(registro_tr.montoField.ToString());
                            PolizasPolizaTransaccionTransferencia_p[contador_tr].Benef = registro_tr.benefField;
                            PolizasPolizaTransaccionTransferencia_p[contador_tr].RFC = registro_tr.rFCField;
                            PolizasPolizaTransaccion_p[contador_t].Transferencia[contador_tr] = PolizasPolizaTransaccionTransferencia_p[contador_tr];
                            contador_tr++;
                        }
                        PolizasPolizaTransaccion_p[contador_t].Transferencia = PolizasPolizaTransaccionTransferencia_p;
                        
                    }
                    //Agregar Comprobantes

                    PolizasPolizaTransaccionCompNalOtr a = new PolizasPolizaTransaccionCompNalOtr();

                    cPolizasPolizaTransaccionCompNal ocComprobantes = new cPolizasPolizaTransaccionCompNal();
                    List<cPolizasPolizaTransaccionCompNal> lista_comprobantes = ocComprobantes.todos(registro_t.ROW_ID);

                    cPolizasPolizaTransaccionCompExt ocPolizasPolizaTransaccionCompExt = new cPolizasPolizaTransaccionCompExt();
                    List<cPolizasPolizaTransaccionCompExt> lista_cPolizasPolizaTransaccionCompExt = ocPolizasPolizaTransaccionCompExt.todos(registro_t.ROW_ID);

                    PolizasPolizaTransaccionCompExt[] oPolizasPolizaTransaccionCompExt;
                    oPolizasPolizaTransaccionCompExt = new PolizasPolizaTransaccionCompExt[lista_comprobantes.Count];
                    int contador_co = 0;
                    if (lista_cPolizasPolizaTransaccionCompExt.Count > 0)
                    {
                        PolizasPolizaTransaccion_p[contador_t].CompExt = new PolizasPolizaTransaccionCompExt[lista_cPolizasPolizaTransaccionCompExt.Count];

                        foreach (cPolizasPolizaTransaccionCompExt registro_tr in lista_cPolizasPolizaTransaccionCompExt)
                        {

                            oPolizasPolizaTransaccionCompExt[contador_co] = new PolizasPolizaTransaccionCompExt();
                            oPolizasPolizaTransaccionCompExt[contador_co].NumFactExt = registro_tr.numFactExtField;
                            oPolizasPolizaTransaccionCompExt[contador_co].TaxID = registro_tr.taxIDField;
                            oPolizasPolizaTransaccionCompExt[contador_co].MontoTotal = registro_tr.montoTotalField;
                            oPolizasPolizaTransaccionCompExt[contador_co].Moneda = registro_tr.monedaField;
                            oPolizasPolizaTransaccionCompExt[contador_co].TipCamb = registro_tr.tipCambField;
                            oPolizasPolizaTransaccionCompExt[contador_co].TipCambSpecified = registro_tr.tipCambFieldSpecified;

                            PolizasPolizaTransaccion_p[contador_t].Transferencia[contador_tr] = PolizasPolizaTransaccionTransferencia_p[contador_co];
                            contador_tr++;
                        }
                        PolizasPolizaTransaccion_p[contador_t].Transferencia = PolizasPolizaTransaccionTransferencia_p;
                        
                    }


                    //PolizasPolizaTransaccionCompNal[] oPolizasPolizaTransaccionCompNal;
                    //oPolizasPolizaTransaccionCompNal = new PolizasPolizaTransaccionCompNal[lista_comprobantes.Count];
                    //contador_co = 0;
                    //if (lista_cPolizasPolizaTransaccionCompExt.Count > 0)
                    //{
                    //    PolizasPolizaTransaccion_p[contador_t].CompExt = new PolizasPolizaTransaccionCompNal[lista_cPolizasPolizaTransaccionCompNal.Count];

                    //    foreach (cPolizasPolizaTransaccionCompExt registro_tr in lista_cPolizasPolizaTransaccionCompExt)
                    //    {

                    //        oPolizasPolizaTransaccionCompExt[contador_co] = new PolizasPolizaTransaccionCompExt();
                    //        oPolizasPolizaTransaccionCompExt[contador_co].NumFactExt = registro_tr.numFactExtField;
                    //        oPolizasPolizaTransaccionCompExt[contador_co].TaxID = registro_tr.taxIDField;
                    //        oPolizasPolizaTransaccionCompExt[contador_co].MontoTotal = registro_tr.montoTotalField;
                    //        oPolizasPolizaTransaccionCompExt[contador_co].Moneda = registro_tr.monedaField;
                    //        oPolizasPolizaTransaccionCompExt[contador_co].TipCamb = registro_tr.tipCambField;
                    //        oPolizasPolizaTransaccionCompExt[contador_co].TipCambSpecified = registro_tr.tipCambFieldSpecified;

                    //        PolizasPolizaTransaccion_p[contador_t].Transferencia[contador_tr] = PolizasPolizaTransaccionTransferencia_p[contador_co];
                    //        contador_tr++;
                    //    }
                    //    PolizasPolizaTransaccion_p[contador_t].Transferencia = PolizasPolizaTransaccionTransferencia_p;

                    //}


                    PolizasPolizaTransaccionCompNalOtr[] oPolizasPolizaTransaccionCompNalOtr;

                    PolizasPolizaTransaccionOtrMetodoPago[] oPolizasPolizaTransaccionOtrMetodoPago;
                    

                    //PolizasPolizaTransaccionComprobantes[] PolizasPolizaTransaccionComprobantes_p;
                    //PolizasPolizaTransaccionComprobantes_p = new PolizasPolizaTransaccionComprobantes[lista_comprobantes.Count];
                    //int contador_co = 0;
                    //if (lista_comprobantes.Count > 0)
                    //{
                    //    PolizasPolizaTransaccion_p[contador_t].comprobantesField = new PolizasPolizaTransaccionComprobantes[lista_comprobantes.Count];

                    //    foreach (cComprobantes registro_co in lista_comprobantes)
                    //    {
                    //        PolizasPolizaTransaccionComprobantes_p[contador_co] = new PolizasPolizaTransaccionComprobantes();
                    //        PolizasPolizaTransaccionComprobantes_p[contador_co].UUID_CFDI = registro_co.uUID_CFDIField;
                    //        PolizasPolizaTransaccionComprobantes_p[contador_co].RFC = registro_co.rFCField;
                    //        PolizasPolizaTransaccion_p[contador_t].comprobantesField[contador_co] = PolizasPolizaTransaccionComprobantes_p[contador_co];
                    //        contador_co++;
                    //    }
                    //    PolizasPolizaTransaccion_p[contador_t].comprobantesField = PolizasPolizaTransaccionComprobantes_p;
                    //}


                    oPolizas_p[contador_p].Transaccion[contador_t] = PolizasPolizaTransaccion_p[contador_t];


                    contador_t++;
                }
                contador_p++;
            }
            oPolizas.Poliza = oPolizas_p;
            //Transformar al XML
            TextWriter writer_p = new StreamWriter(nombre_tmp + ".XML");
            XmlSerializer serializer_p = new XmlSerializer(typeof(Polizas));
            serializer_p.Serialize(writer_p, oPolizas);

            writer_p.Close();


            if (mostrar_mensaje_finalizacion)
            {
                MessageBoxEx.Show("Exportaci�n de los archivos finalizada.", Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Information);
            }

        }


        private void buttonX10_Click(object sender, EventArgs e)
        {
            importar_balanza_visual();
        }
        #region Balanza
        private void buttonX12_Click(object sender, EventArgs e)
        {
            cargar_balanza();
        }

        private void cargar_balanza()
        {
            if (oObjeto.ROW_ID != "")
            {
                dtgBalanza.Rows.Clear();
                cBalanza ocBalanza = new cBalanza();
                foreach (cBalanza registro in ocBalanza.todos(oObjeto.ROW_ID))
                {
                    int n = dtgBalanza.Rows.Add();
                    actualizar_linea_balanza(registro, n);
                }
                cargar_balanza_totales();
            }
        }

        private void cargar_balanza_totales()
        {
            SALDO_INICIAL_TOTAL.Text = oData.formato_decimal("0", "S");
            DEBE_TOTAL.Text = oData.formato_decimal("0", "S");
            HABER_TOTAL.Text = oData.formato_decimal("0", "S");
            SALDO_FINAL_TOTAL.Text = oData.formato_decimal("0", "S");

            double resultado = 0;
            double gran_total = 0;
            resultado = dtgBalanza.Rows.Cast<DataGridViewRow>()
                .Where(r => r.Cells["SUMMARY_ACCOUNT"].Value.ToString().Equals("N"))
                .Sum(x => Convert.ToDouble(x.Cells["saldoIniField_b"].Value));
            gran_total += Math.Abs(resultado);
            SALDO_INICIAL_TOTAL.Text = oData.formato_decimal(resultado.ToString(), "");

            resultado = dtgBalanza.Rows.Cast<DataGridViewRow>()
                .Where(r => r.Cells["SUMMARY_ACCOUNT"].Value.ToString().Equals("N"))
                .Sum(x => Convert.ToDouble(x.Cells["debeField_b"].Value));
            gran_total += Math.Abs(resultado);
            DEBE_TOTAL.Text = oData.formato_decimal(resultado.ToString(), "");

            resultado = dtgBalanza.Rows.Cast<DataGridViewRow>()
                .Where(r => r.Cells["SUMMARY_ACCOUNT"].Value.ToString().Equals("N"))
                .Sum(x => Convert.ToDouble(x.Cells["haberField_b"].Value));
            gran_total += Math.Abs(resultado);
            HABER_TOTAL.Text = oData.formato_decimal(resultado.ToString(), "");

            resultado = dtgBalanza.Rows.Cast<DataGridViewRow>()
                .Where(r => r.Cells["SUMMARY_ACCOUNT"].Value.ToString().Equals("N"))
                .Sum(x => Convert.ToDouble(x.Cells["saldoFinField_b"].Value));
            gran_total += Math.Abs(resultado);
            SALDO_FINAL_TOTAL.Text = oData.formato_decimal(resultado.ToString(), "");

        }

        private void actualizar_linea_balanza(cBalanza registro, int n)
        {
            for (int i = 0; i < dtgBalanza.Columns.Count; i++)
            {
                dtgBalanza.Rows[n].Cells[i].Value = "";
            }
            dtgBalanza.Rows[n].Tag = registro;
            dtgBalanza.Rows[n].Cells["ROW_ID_b"].Value = registro.ROW_ID;
            dtgBalanza.Rows[n].Cells["numCtaField_b"].Value = registro.numCtaField;
            dtgBalanza.Rows[n].Cells["saldoIniField_b"].Value = oData.formato_decimal(registro.saldoIniField.ToString(), "");
            dtgBalanza.Rows[n].Cells["debeField_b"].Value = oData.formato_decimal(registro.debeField.ToString(), "");
            dtgBalanza.Rows[n].Cells["haberField_b"].Value = oData.formato_decimal(registro.haberField.ToString(), "");
            dtgBalanza.Rows[n].Cells["saldoFinField_b"].Value = oData.formato_decimal(registro.saldoFinField.ToString(), "");
            dtgBalanza.Rows[n].Cells["SUMMARY_ACCOUNT"].Value = registro.SUMMARY_ACCOUNT;
            if (registro.SUMMARY_ACCOUNT.Contains("Y"))
            {
                 dtgBalanza.Rows[n].DefaultCellStyle.BackColor = Color.Gray;
                 
            }
            

        }

        private void buttonX11_Click(object sender, EventArgs e)
        {
            eliminar_balanza();
        }

        private void eliminar_balanza()
        {
            dtgBalanza.EndEdit();
            if (dtgBalanza.SelectedCells.Count > 0)
            {
                DialogResult Resultado = MessageBoxEx.Show("�Desea eliminar las seleccionada?", Application.ProductName + "-" + Application.ProductVersion, MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                if (Resultado.ToString() != "Yes")
                {
                    return;
                }
                foreach (DataGridViewRow drv in dtgBalanza.SelectedRows)
                {
                    string ROW_ID = drv.Cells["ROW_ID_b"].Value.ToString();
                    cBalanza ocBalanza = (cBalanza)drv.Tag;
                    ocBalanza.eliminar(ocBalanza.ROW_ID);
                    dtgBalanza.Rows.Remove(drv);
                }
                cargar_balanza_totales();
            }
            else
            {
                MessageBoxEx.Show("Seleccione alguna l�nea.", Application.ProductName + "-" + Application.ProductVersion, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }

        private void buttonX9_Click(object sender, EventArgs e)
        {
            importar_balanza_excel();
        }

        private void importar_balanza_excel()
        {
            if (oObjeto.ROW_ID == "")
            {
                MessageBoxEx.Show("Guarde primero.", Application.ProductName + "-" + Application.ProductVersion, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return;
            }

            OpenFileDialog openFileDialog1 = new OpenFileDialog();
            openFileDialog1.Filter = "Excel files (*.xlsx)|*.xlsx|All files (*.*)|*.*";
            openFileDialog1.FilterIndex = 1;
            openFileDialog1.RestoreDirectory = true;

            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                try
                {
                    procesar_balanza(dtgBalanza, 1, openFileDialog1.FileName);
                }
                catch (Exception)
                {
                    MessageBoxEx.Show("No existe el Archivo seleccionado", Application.ProductName + "-" + Application.ProductVersion, MessageBoxButtons.OK);
                }
            }
        }

        private void procesar_balanza(DataGridView dtg, int hoja, string archivo)
        {
            try
            {
                dtg.Rows.Clear();
                int n;
                int rowIndex = 1;
                FileInfo existingFile = new FileInfo(archivo);
                using (ExcelPackage package = new ExcelPackage(existingFile))
                {
                    ExcelWorksheet worksheet = package.Workbook.Worksheets[hoja];
                    while (worksheet.Cells[rowIndex, 1].Value != null)
                    {
                        //Agregar las lineas de la cabecera
                        if (rowIndex != 1)
                        {
                            cBalanza ocBalanza = new cBalanza();
                            ocBalanza.ROW_ID_CATALOGO = oObjeto.ROW_ID;
                            ocBalanza.numCtaField = worksheet.Cells[rowIndex, 2].Text;
                            ocBalanza.saldoIniField = double.Parse(worksheet.Cells[rowIndex, 3].Text);
                            ocBalanza.debeField = double.Parse(worksheet.Cells[rowIndex, 4].Text);
                            ocBalanza.haberField = double.Parse(worksheet.Cells[rowIndex, 5].Text);
                            ocBalanza.saldoFinField = double.Parse(worksheet.Cells[rowIndex, 6].Text);
                            if (ocBalanza.guardar(false))
                            {
                                n = dtg.Rows.Add();
                                actualizar_linea_balanza(ocBalanza, n);
                            }
                        }
                        rowIndex++;

                    }

                }
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message.ToString());
            }

        }

        private void buttonItem2_Click(object sender, EventArgs e)
        {
            exportar_excel_balanza();
        }

        private void exportar_excel_balanza()
        {
            exportar("Balanza de Comprobacion " + anoField.Text + " - " + mesField.Text + " ", this.dtgBalanza);
        }
        private void importar_excel_balanza()
        {


            var fileName = " Plantilla de Balanza " + DateTime.Now.ToString("yyyy-MM-dd-hh-mm-ss") + ".xlsx";
            if (folderBrowserDialog1.ShowDialog() == DialogResult.OK)
            {
                fileName = folderBrowserDialog1.SelectedPath + "\\" + fileName;
                System.IO.Stream myStream = new System.IO.FileStream(fileName, System.IO.FileMode.Create, System.IO.FileAccess.ReadWrite, System.IO.FileShare.None);

                using (ExcelPackage pck = new ExcelPackage())
                {

                    ExcelWorksheet ws1 = pck.Workbook.Worksheets.Add("Plantilla");

                    for (int c = 1; c < dtgBalanza.Columns.Count; c++)
                    {
                        ws1.Cells[1, c + 1].Value = dtgCuentas.Columns[c].HeaderText;
                    }
                    pck.SaveAs(myStream);
                }
                myStream.Close();
                MessageBoxEx.Show("Creaci�n de plantilla realizada " + Environment.NewLine + fileName + " finalizada.", Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }
        private void backgroundWorker2_DoWork(object sender, DoWorkEventArgs e)
        {
            try
            {
                int n = 0;
                cCatalogo ocCatalogo = new cCatalogo();
                labelItem1.Text = "Migrando Balanza de Infor Visual Base de datos: " + ocEMPRESA.VISUAL_BD;
                try
                {

                    List<object> genericlist = e.Argument as List<object>;
                    
                    //Cargar todas las cuentas contables
                    cEMPRESA_CONEXION ocEMPRESA_CONEXION_tr = new cEMPRESA_CONEXION();
                    labelItem1.Text = "Migrando de Infor Visual Base de datos: " + ocEMPRESA_CONEXION_tr.VISUAL_ENTITY_ID;
                    foreach (cEMPRESA_CONEXION registro in ocEMPRESA_CONEXION_tr.todos(ocEMPRESA.ROW_ID))
                    {
                        ocEMPRESA_CONEXION = registro;
                        oData_ERP = registro.conectar(registro.ROW_ID);
                        string sSQL = "";
                        //if (registro.VISUAL_VERSION == "7.0.0")
                        //{
                        sSQL = "SELECT * FROM ACCOUNT WHERE 1=1 ";//AND ID='3410-001' ";
                        //Se deben mostrar todas las Balanzas 
                        bool SUMMARY_ACCOUNT_tr = false;
                        try
                        {
                            string SUMMARY_ACCOUNT_acc=ConfigurationManager.AppSettings.Get("SUMMARY_ACCOUNT").ToString();
                            if (SUMMARY_ACCOUNT_acc=="N")
                            {
                                SUMMARY_ACCOUNT_tr = true;
                            }
                        }
                        catch
                        {

                        }

                        if (SUMMARY_ACCOUNT_tr)
                        {
                            sSQL += " AND [SUMMARY_ACCOUNT]='N'  ";
                        }
                        //7.1.2
                        //if ((registro.VISUAL_ENTITY_ID == "7.1.2")
                        //    | (registro.VISUAL_ENTITY_ID == "8.0.0"))
                        //{
                            sSQL += " AND ID IN (SELECT ACCOUNT_ID FROM ACCOUNT_SITE WHERE SITE_ID='" + registro.VISUAL_SITE_ID + "')  ";
                        //}
                        //Deben salir todas las cuentas
                        //Verificar que todas las cuentas se filtren porque la que se declararon
                        //cCatalogoCtas ocCatalogoCtas = new cCatalogoCtas();
                        //string cuentas_definidas = ocCatalogoCtas.todos_sSQL(oObjeto.ROW_ID);
                        //if (cuentas_definidas!="")
                        //{
                        //    sSQL += " AND ID IN (" + cuentas_definidas + ") ";
                        //    
                        //}
                        sSQL += " ORDER BY ID";

                        DataTable oDataTable = oData_ERP.EjecutarConsulta(sSQL);
                        if (oDataTable != null)
                        {
                            foreach (DataRow oDataRow in oDataTable.Rows)
                            {
                                n++;
                                Thread.Sleep(100);



                                backgroundWorker2.ReportProgress(n, oDataRow);
                                if (backgroundWorker2.CancellationPending)
                                {
                                    e.Cancel = true;
                                    backgroundWorker2.ReportProgress(0);
                                    return;
                                }
                            }
                        }
                    } 
                }
                catch
                {
                    backgroundWorker2.CancelAsync();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }


        private void backgroundWorker2_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            try
            {
                if (!backgroundWorker2.CancellationPending)
                {
                    DataRow oDataRow = (DataRow)e.UserState;
                    if (oDataRow != null)
                    {
                        bool guardar = true;
                        if (ocEMPRESA.NIVEL != "")
                        {
                            //
                            
                            //Buscar el nivel
                            cCatalogoCtas ocCatalogoCtas = new cCatalogoCtas();
                            try
                            {
                                int nivelt = ocCatalogoCtas.buscar_nivel(oDataRow["ID"].ToString(), oData_ERP);
                                guardar = false;
                                int nivel_empresa = int.Parse(oData.IsNullNumero(ocEMPRESA.NIVEL));
                                if (nivel_empresa >= nivelt)
                                {
                                    guardar = true;
                                }
                            }
                            catch (Exception excep)
                            {
                                MessageBoxEx.Show("frmDeclaracion - 1519 Error en la cuenta nivel" + oDataRow["ID"].ToString() + " " + excep.ToString());
                                backgroundWorker2.CancelAsync();
                                return;
                            }
                        }
                        if(guardar)
                        {

                            //Buscar los saldos iniciales y finales
                            double saldo_inicial = 0;
                            double saldo_final = 0;
                            //Calculo de Saldo Inicial y Saldo Final
                            //Saldo Inicial
                            string fecha_inicial = "";
                            string fecha_final = "";
                            try
                            {
                                int anio = int.Parse(anoField.Text);
                                int mes = int.Parse(mesField.Text);

                                DateTime primerdia = new DateTime(anio, mes, 1);
                                fecha_inicial = primerdia.ToShortDateString();
                                fecha_inicial = oData.convertir_cadena_fecha(primerdia.ToShortDateString());

                                //Saldo Final
                                int anio_final = int.Parse(anoField.Text);
                                int mes_final = int.Parse(mesField.Text);

                                DateTime ultimodia = new DateTime(anio_final, mes_final, DateTime.DaysInMonth(anio_final, mes_final));
                                fecha_final = ultimodia.ToShortDateString();
                                fecha_final = oData.convertir_cadena_fecha(ultimodia.ToShortDateString());
                            }
                            catch (Exception excep)
                            {
                                MessageBoxEx.Show("frmDeclaracion - 1153 Error en la cuenta nivel" + oDataRow["ID"].ToString() + " " + excep.ToString());
                                backgroundWorker2.CancelAsync();
                                return;
                            }

                            string sSQL = "";

                            if (oObjeto.usarPeriodo)
                            {
                                //Buscar la fecha del Periodo Inicial
                                sSQL = "SELECT * FROM [ACCOUNT_PERIOD] WHERE ACCT_YEAR=" + oObjeto.anoField
                                    + " AND ACCT_PERIOD=" + oObjeto.periodo;
                                DataTable oDataTable_ACCOUNT_PERIOD = oData_ERP.EjecutarConsulta(sSQL);
                                if (oDataTable_ACCOUNT_PERIOD != null)
                                {
                                    foreach (DataRow oDataRow_ACCOUNT_PERIOD in oDataTable_ACCOUNT_PERIOD.Rows)
                                    {
                                        fecha_inicial = oDataRow_ACCOUNT_PERIOD["BEGIN_DATE"].ToString();
                                        fecha_inicial = oData.convertir_cadena_fecha(fecha_inicial);

                                    }
                                }
                                //Buscar la fecha del Periodo Final
                                sSQL = "SELECT * FROM [ACCOUNT_PERIOD] WHERE ACCT_YEAR=" + oObjeto.anoField
                                    + " AND ACCT_PERIOD=" + oObjeto.periodoFinal;
                                oDataTable_ACCOUNT_PERIOD = oData_ERP.EjecutarConsulta(sSQL);
                                if (oDataTable_ACCOUNT_PERIOD != null)
                                {
                                    foreach (DataRow oDataRow_ACCOUNT_PERIOD in oDataTable_ACCOUNT_PERIOD.Rows)
                                    {
                                        fecha_final = oDataRow_ACCOUNT_PERIOD["END_DATE"].ToString();
                                        fecha_final = oData.convertir_cadena_fecha(fecha_final.Replace("00:00:00", "23:59:59"));

                                    }
                                }
                            }

                            
                            //Verificar si los periodos de las empresas
                            if (ocEMPRESA.PERIODO)
                            {
                                //Buscar la fecha inicial y final del Periodo
                                
                                if(!oObjeto.usarPeriodo)
                                {
                                    //Cargar los datos de Mes seleccionado
                                    sSQL = "SELECT * FROM [ACCOUNT_PERIOD] WHERE ACCT_YEAR=" + oObjeto.anoField + " AND ACCT_PERIOD=" + oObjeto.mesField;
                                    DataTable oDataTable_ACCOUNT_PERIOD = oData_ERP.EjecutarConsulta(sSQL);
                                    if (oDataTable_ACCOUNT_PERIOD != null)
                                    {
                                        foreach (DataRow oDataRow_ACCOUNT_PERIOD in oDataTable_ACCOUNT_PERIOD.Rows)
                                        {
                                            fecha_inicial = oDataRow_ACCOUNT_PERIOD["BEGIN_DATE"].ToString();
                                            fecha_inicial = oData.convertir_cadena_fecha(fecha_inicial);
                                            fecha_final = oDataRow_ACCOUNT_PERIOD["END_DATE"].ToString();
                                            fecha_final = oData.convertir_cadena_fecha(fecha_final.Replace("00:00:00", "23:59:59"));
                                        }
                                    }
                                }
                                
                            }

                            //Saldo Inicial
                            saldo_inicial = saldo(fecha_inicial, oDataRow["TYPE"].ToString()
                            , oDataRow["ID"].ToString()
                            , ocEMPRESA_CONEXION
                            , oData_ERP);

                            double debe = 0;
                            double haber = 0;
                            
                            sSQL = " SELECT ISNULL(ACCOUNT_BALANCE.DEBIT_AMOUNT,0) as DEBIT_AMOUNT";
                            sSQL += " ,ISNULL(ACCOUNT_BALANCE.CREDIT_AMOUNT,0) as CREDIT_AMOUNT ";
                            sSQL += " From ACCOUNT_BALANCE, ACCOUNT_PERIOD, ACCOUNT ";
                            sSQL += " Where ACCOUNT.ID=ACCOUNT_BALANCE.ACCOUNT_ID ";
                            sSQL += " AND ACCOUNT_BALANCE.ACCT_YEAR = ACCOUNT_PERIOD.ACCT_YEAR And ACCOUNT_BALANCE.ACCT_PERIOD = ACCOUNT_PERIOD.ACCT_PERIOD ";
                            if (ocEMPRESA_CONEXION.VISUAL_VERSION != "7.0.0")
                            {
                                sSQL += " AND ACCOUNT_BALANCE.SITE_ID = ACCOUNT_PERIOD.SITE_ID ";
                            }
                            
                            sSQL += " AND ACCOUNT_BALANCE.CURRENCY_ID = '" + ocEMPRESA_CONEXION.VISUAL_CURRENCY_ID + "' ";
                            sSQL += " AND ACCOUNT_BALANCE.ACCOUNT_ID = '" + oDataRow["ID"].ToString() + "' ";
                            if (ocEMPRESA_CONEXION.VISUAL_VERSION == "7.0.0")
                            {
                                sSQL += " AND ACCOUNT_BALANCE.ENTITY_ID = '" + ocEMPRESA_CONEXION.VISUAL_ENTITY_ID + "' ";
                            }
                            else //if ((ocEMPRESA_CONEXION.VISUAL_VERSION == "7.1.2")  | (ocEMPRESA_CONEXION.VISUAL_VERSION == "8.0.0"))
                            {
                                sSQL += " AND ACCOUNT_BALANCE.SITE_ID = '" + ocEMPRESA_CONEXION.VISUAL_SITE_ID + "' ";
                            }

                            
                            //Periodo por fecha
                            sSQL += " AND ACCOUNT_PERIOD.BEGIN_DATE>= " + fecha_inicial + " ";
                            sSQL += " AND ACCOUNT_PERIOD.END_DATE<= " + fecha_final + " ";
                            

                            sSQL += " ORDER BY ACCOUNT_PERIOD.END_DATE";

                            DataTable oDataTable1 = oData_ERP.EjecutarConsulta(sSQL);
                            foreach (DataRow oDataRow1 in oDataTable1.Rows)
                            {
                                debe += double.Parse(oData.Truncar_decimales(decimal.Parse(oDataRow1["DEBIT_AMOUNT"].ToString())).ToString());
                                haber += double.Parse(oData.Truncar_decimales(decimal.Parse(oDataRow1["CREDIT_AMOUNT"].ToString())).ToString());
                            }
                            saldo_final = (debe - haber) + saldo_inicial;
                            try
                            {
                                cBalanza ocBalanza = new cBalanza();
                                ocBalanza.ROW_ID_CATALOGO = oObjeto.ROW_ID;
                                ocBalanza.numCtaField = oDataRow["ID"].ToString();
                                ocBalanza.saldoIniField = saldo_inicial;
                                ocBalanza.debeField = debe;
                                ocBalanza.haberField = haber;
                                ocBalanza.saldoFinField = saldo_final;
                                ocBalanza.SUMMARY_ACCOUNT = oDataRow["SUMMARY_ACCOUNT"].ToString();
                                ocBalanza.guardar(false);
                                circularProgress2.Value = e.ProgressPercentage;
                                labelItem1.Text = "Migrando Balanza: " + oDataRow["ID"].ToString();
                                circularProgress2.ProgressText = "Procesando.. " + e.ProgressPercentage.ToString();// + " de " + TotalRecords;
                            }
                            catch (Exception excep)
                            {
                                string mensaje = excep.Message.ToString();
                                if (excep.InnerException!=null)
                                {
                                    mensaje += Environment.NewLine + excep.InnerException.Message.ToString();
                                }
                                MessageBoxEx.Show("frmDeclaracion - 1677 " + Environment.NewLine + mensaje);
                                backgroundWorker2.CancelAsync();
                                return;
                            }

                        }
                    }
                }
            }
            catch (Exception excep)
            {
                MessageBoxEx.Show("frmDeclaracion - 1683 " + excep.ToString());
                backgroundWorker2.CancelAsync();
                return;
            }
        }

        private double saldo(string fecha, string TIPO, string Cuenta
            , cEMPRESA_CONEXION ocEMPRESA_CONEXIONp
            , cCONEXCION oData_ERPp)
        {
            decimal CREDIT_AMOUNT = 0;
            decimal DEBIT_AMOUNT = 0;
            decimal CURR_BALANCE = 0;

            string sSQL = "";

            if (TIPO == "E" || TIPO == "R")
            {
                sSQL = " SELECT CAST (ACCOUNT_BALANCE.DEBIT_AMOUNT as decimal(18,4)) as DEBIT_AMOUNT";
                sSQL += " ,CAST(ACCOUNT_BALANCE.CREDIT_AMOUNT as decimal(18,4)) as CREDIT_AMOUNT ";
                sSQL += " FROM ACCOUNT_BALANCE INNER JOIN ACCOUNT_PERIOD ON ACCOUNT_BALANCE.ACCT_YEAR = ACCOUNT_PERIOD.ACCT_YEAR AND ACCOUNT_BALANCE.ACCT_PERIOD = ACCOUNT_PERIOD.ACCT_PERIOD  ";
                if ((ocEMPRESA_CONEXIONp.VISUAL_VERSION == "7.1.2")  | (ocEMPRESA_CONEXION.VISUAL_VERSION == "8.0.0"))
                {
                    sSQL += " AND ACCOUNT_BALANCE.SITE_ID = ACCOUNT_PERIOD.SITE_ID ";
                }
                sSQL += " WHERE 1=1 ";
                //if (MFG_ENTITY_ID != "")
                //{
                //    sSQL += " AND ACCOUNT_BALANCE.ENTITY_ID = '" + MFG_ENTITY_ID + "'";
                //}
                if (ocEMPRESA_CONEXIONp.VISUAL_VERSION == "7.0.0")
                {
                    sSQL += " AND ACCOUNT_BALANCE.ENTITY_ID = '" + ocEMPRESA_CONEXIONp.VISUAL_ENTITY_ID + "' ";
                }
                if((ocEMPRESA_CONEXIONp.VISUAL_VERSION == "7.1.2")  | (ocEMPRESA_CONEXION.VISUAL_VERSION == "8.0.0"))
                {
                    sSQL += " AND ACCOUNT_BALANCE.SITE_ID = '" + ocEMPRESA_CONEXIONp.VISUAL_SITE_ID + "' ";
                }

                sSQL += " AND ACCOUNT_BALANCE.CURRENCY_ID = '" + ocEMPRESA_CONEXIONp.VISUAL_CURRENCY_ID + "'";
                sSQL += " AND ACCOUNT_BALANCE.ACCOUNT_ID='" + Cuenta + "'";
                sSQL += " AND ACCOUNT_PERIOD.END_DATE<" + fecha + "";
                sSQL += " AND DATEPART(yyyy, ACCOUNT_PERIOD.BEGIN_DATE)=DATEPART(yyyy, " + fecha + ")";
                sSQL += " ORDER BY ACCOUNT_BALANCE.ACCOUNT_ID, ACCOUNT_PERIOD.BEGIN_DATE, ACCOUNT_PERIOD.END_DATE ";
            }
            else
            {
                sSQL = " SELECT CAST (ACCOUNT_BALANCE.DEBIT_AMOUNT as decimal(18,4)) as DEBIT_AMOUNT";
                sSQL += " ,CAST(ACCOUNT_BALANCE.CREDIT_AMOUNT as decimal(18,4)) as CREDIT_AMOUNT "; 
                sSQL += " FROM ACCOUNT_BALANCE INNER JOIN ACCOUNT_PERIOD ON ACCOUNT_BALANCE.ACCT_YEAR = ACCOUNT_PERIOD.ACCT_YEAR AND ACCOUNT_BALANCE.ACCT_PERIOD = ACCOUNT_PERIOD.ACCT_PERIOD ";
                if ((ocEMPRESA_CONEXIONp.VISUAL_VERSION == "7.1.2")  | (ocEMPRESA_CONEXION.VISUAL_VERSION == "8.0.0"))
                {
                    sSQL += " AND ACCOUNT_BALANCE.SITE_ID = ACCOUNT_PERIOD.SITE_ID ";
                }
                

                sSQL += " WHERE 1=1 ";
                //if (MFG_ENTITY_ID != "")
                //{
                //    sSQL += " AND ACCOUNT_BALANCE.ENTITY_ID = '" + MFG_ENTITY_ID + "'";
                //}
                if (ocEMPRESA_CONEXIONp.VISUAL_VERSION == "7.0.0")
                {
                    sSQL += " AND ACCOUNT_BALANCE.ENTITY_ID = '" + ocEMPRESA_CONEXIONp.VISUAL_ENTITY_ID + "' ";
                }
                if ((ocEMPRESA_CONEXIONp.VISUAL_VERSION == "7.1.2")  | (ocEMPRESA_CONEXION.VISUAL_VERSION == "8.0.0"))
                {
                    sSQL += " AND ACCOUNT_BALANCE.SITE_ID = '" + ocEMPRESA_CONEXIONp.VISUAL_SITE_ID + "' ";
                }

                sSQL += " AND ACCOUNT_BALANCE.CURRENCY_ID = '" + ocEMPRESA_CONEXIONp.VISUAL_CURRENCY_ID + "'";
                sSQL += " AND ACCOUNT_BALANCE.ACCOUNT_ID='" + Cuenta + "'";
                
                sSQL += " AND ACCOUNT_PERIOD.END_DATE<" + fecha + " ";
                
                
                sSQL += " ORDER BY ACCOUNT_BALANCE.ACCOUNT_ID , ACCOUNT_PERIOD.BEGIN_DATE, ACCOUNT_PERIOD.END_DATE ";

            }

            DataTable oDataTable1 = oData_ERPp.EjecutarConsulta(sSQL);
            foreach (DataRow oDataRow1 in oDataTable1.Rows)
            {
                DEBIT_AMOUNT = 0;
                if (oDataRow1["DEBIT_AMOUNT"].ToString() != null)
                {
                    DEBIT_AMOUNT = decimal.Parse(oDataRow1["DEBIT_AMOUNT"].ToString());
                }

                CREDIT_AMOUNT = 0;
                if(oDataRow1["CREDIT_AMOUNT"].ToString() != null)
                {
                    CREDIT_AMOUNT = decimal.Parse(oDataRow1["CREDIT_AMOUNT"].ToString());
                }

                CURR_BALANCE = (DEBIT_AMOUNT + CURR_BALANCE) - CREDIT_AMOUNT;
            }

            return double.Parse(oData.Truncar_decimales(CURR_BALANCE).ToString());

        }


        private void cambiar_boton_balanza()
        {
            if (buttonX10.Text == "Importar: Visual")
            {
                buttonX10.Image = global::Desarrollo.Properties.Resources.stop_16;
                buttonX10.Text = "Detener";
            }
            else
            {
                buttonX10.Image = global::Desarrollo.Properties.Resources.visual;

                buttonX10.Text = "Importar: Visual";
            }
        }

        private void backgroundWorker2_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            if (e.Cancelled)
            {
                labelItem1.Text = "Cancelado por el usuario...";
                circularProgress2.Value = 0;
            }
            // Check to see if an error occurred in the background process.
            else if (e.Error != null)
            {
                labelItem1.Text = e.Error.Message;
            }
            else
            {
                //BackGround Task Completed with out Error
                labelItem1.Text = " Finalizado...";
            }
            cambiar_boton_balanza();
            cargar_balanza();
            circularProgress2.Visible = false;
        }
        private void nueva_balanza()
        {
            if (oObjeto.ROW_ID == "")
            {
                MessageBoxEx.Show("Guarde primero.", Application.ProductName + "-" + Application.ProductVersion, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return;
            }
            frmBalanza ofrmBalanza = new frmBalanza(oObjeto.ROW_ID);
            ofrmBalanza.ShowDialog();
            cargar_balanza();
        }
#endregion Balanza

        private void buttonX13_Click(object sender, EventArgs e)
        {
            nueva_balanza();
        }

        private void dtgBalanza_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            editar_balanza();
        }

        private void editar_balanza()
        {
            dtgBalanza.EndEdit();
            if (dtgBalanza.SelectedRows.Count > 0)
            {
                foreach (DataGridViewRow drv in dtgBalanza.SelectedRows)
                {
                    string ROW_ID = drv.Cells["ROW_ID_b"].Value.ToString();
                    frmBalanza ofrmBalanza = new frmBalanza(oObjeto.ROW_ID, ROW_ID);
                    ofrmBalanza.ShowDialog();
                    cBalanza ocBalanza = new cBalanza();
                    ocBalanza.cargar(ROW_ID);
                    actualizar_linea_balanza(ocBalanza, drv.Index);
                }
            }
            else
            {
                MessageBoxEx.Show("Seleccione alguna l�nea.", Application.ProductName + "-" + Application.ProductVersion, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }
        private bool verificar_seguridad_poliza(string ROW_ID_POLIZAp,string USUARIOp)
        {
            //Verificar seguridad de ingreso
            cPoliza ocPoliza=new cPoliza();
            return ocPoliza.cargar_seguridad(ROW_ID_POLIZAp, USUARIOp);

        }

        private void buttonX22_Click(object sender, EventArgs e)
        {
            importar_catalogo_polizas("", true);
        }

        private void buttonX21_Click(object sender, EventArgs e)
        {
            importar_catalogo_balanza("", true);
        }

        private void buttonX20_Click(object sender, EventArgs e)
        {
            importar_catalogo_cuentas("", true);
        }

        private void buttonX24_Click(object sender, EventArgs e)
        {
            asignar_cuenta();
        }


        private void buttonX26_Click(object sender, EventArgs e)
        {
            exportar_excel_balanza();
        }

        private DataTable GetDataTableFromDGV(DataGridView dgv)
        {
            var dt = new DataTable();
            foreach (DataGridViewColumn column in dgv.Columns)
            {
                //if (column.Visible)
                //{
                // You could potentially name the column based on the DGV column name (beware of dupes)
                // or assign a type based on the data type of the data bound to this DGV column.

                //}
                if (column.DefaultCellStyle.Alignment == DataGridViewContentAlignment.MiddleRight)
                {
                    dt.Columns.Add(column.Name, typeof(decimal));
                }
                else
                {
                    dt.Columns.Add(column.Name);
                }
            }

            object[] cellValues = new object[dgv.Columns.Count];
            foreach (DataGridViewRow row in dgv.Rows)
            {
                for (int i = 0; i < row.Cells.Count; i++)
                {

                    cellValues[i] = row.Cells[i].Value;
                }
                dt.Rows.Add(cellValues);
            }

            return dt;
        }


        public void exportar(string nombre, DataGridView dtg)
        {

            var fileName = nombre + " " + DateTime.Now.ToString("yyyy-MM-dd-hh-mm-ss") + ".xlsx";
            if (folderBrowserDialog1.ShowDialog() == DialogResult.OK)
            {
                fileName = folderBrowserDialog1.SelectedPath + "\\" + fileName;
                System.IO.Stream myStream = new System.IO.FileStream(fileName, System.IO.FileMode.Create, System.IO.FileAccess.ReadWrite, System.IO.FileShare.None);

                using (ExcelPackage pck = new ExcelPackage())
                {

                    DataTable oDataTable = new DataTable();

                    oDataTable = GetDataTableFromDGV(dtg);
                    ExcelWorksheet ws1 = pck.Workbook.Worksheets.Add("Datos");
                    ws1.Cells["A1"].LoadFromDataTable(oDataTable, true);
                    pck.SaveAs(myStream);
                }
                myStream.Close();
                MessageBoxEx.Show("Exportaci�n del archivo " + Environment.NewLine + fileName + " finalizada.", Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }


        private void exportar_excel_cuentas()
        {
            exportar("Cuentas " + anoField.Text + "-" + mesField.Text + " ", this.dtgCuentas);
        }

        private void buttonX27_Click(object sender, EventArgs e)
        {
            exportar_excel_cuentas();
        }

        private void buttonItem4_Click(object sender, EventArgs e)
        {
            importar_visual();
        }

        private void importar_visual()
        {
            if (oObjeto.ROW_ID == "")
            {
                MessageBoxEx.Show("Guarde primero.", Application.ProductName + "-" + Application.ProductVersion, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return;
            }

            OpenFileDialog openFileDialog1 = new OpenFileDialog();
            openFileDialog1.Filter = "Excel files (*.xlsx)|*.xlsx|All files (*.*)|*.*";
            openFileDialog1.FilterIndex = 1;
            openFileDialog1.RestoreDirectory = true;

            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                try
                {
                    procesar_visual(dtgCuentas, 1, openFileDialog1.FileName);
                }
                catch (Exception)
                {
                    MessageBoxEx.Show("No existe el Archivo seleccionado", Application.ProductName + "-" + Application.ProductVersion, MessageBoxButtons.OK);
                }
            }
        }

        private void procesar_visual(DataGridViewX dtg, int hoja, string archivo)
        {
            try
            {
                dtg.Rows.Clear();
                int n;
                int rowIndex = 1;
                FileInfo existingFile = new FileInfo(archivo);
                using (ExcelPackage package = new ExcelPackage(existingFile))
                {
                    ExcelWorksheet worksheet = package.Workbook.Worksheets[hoja];
                    while (worksheet.Cells[rowIndex, 2].Value != null)
                    {
                        labelItem1.Text = "Registro " + rowIndex.ToString();
                        //Agregar las lineas de la cabecera
                        if (rowIndex != 1)
                        {
                            cCatalogoCtas ocCatalogoCtas = new cCatalogoCtas();
                            bool guardar_cuenta = true;

                            ocCatalogoCtas.numCtaField = worksheet.Cells[rowIndex, 2].Text;
                            if (!ocCatalogoCtas.cargar_numCta(ocCatalogoCtas.numCtaField))
                            {
                                //Cargar los datos del Visual
                                if(!ocCatalogoCtas.obtener_VISUAL_ACCOUNT(ocCatalogoCtas.numCtaField,ocEMPRESA))
                                {
                                    DialogResult resultado= MessageBoxEx.Show("La cuenta " + ocCatalogoCtas.numCtaField  + " no existe en visual.", Application.ProductName,MessageBoxButtons.AbortRetryIgnore);
                                    if(resultado== System.Windows.Forms.DialogResult.Abort)
                                    {
                                        return;
                                    }
                                    if(resultado==System.Windows.Forms.DialogResult.Ignore)
                                    {
                                        guardar_cuenta = false;
                                    }
                                    

                                }
                            }
                            if (guardar_cuenta)
                            {
                                ocCatalogoCtas.ROW_ID_CATALOGO = oObjeto.ROW_ID;
                                ocCatalogoCtas.codAgrupField = worksheet.Cells[rowIndex, 1].Text;
                                //Si la cuenta tiene .00 poner ""
                                ocCatalogoCtas.codAgrupField = ocCatalogoCtas.codAgrupField.Replace(".00", "");

                                labelItem1.Text = "Cuenta " + ocCatalogoCtas.numCtaField;

                                if (ocCatalogoCtas.guardar(false))
                                {
                                    n = dtg.Rows.Add();
                                    actualizar_linea_cuenta(ocCatalogoCtas, n);
                                }
                            }
                        }
                        rowIndex++;

                    }
                    MessageBoxEx.Show("Archivo Importado", Application.ProductName);
                }
            }
            catch (Exception e)
            {
                MessageBoxEx.Show(e.Message.ToString());
            }
        }

        private void buttonItem5_Click(object sender, EventArgs e)
        {
            generar_plantilla_visual();
        }

        private void generar_plantilla_visual()
        {
            MessageBoxEx.Show("La configuraci�n de  la Plantilla estilo Visual es la siguiente:" + Environment.NewLine
            + "Fila 1 - Encabezado" + Environment.NewLine
            + "Columna 1: Agrupador " + Environment.NewLine
            + "Columna 2: Cuenta " + Environment.NewLine            
            + "Si no tiene esta configuraci�n no ser� cargada la informaci�n. "
            + "Ser� generada la demas informaci�n desde Visual. "
            , Application.ProductName + "-" + Application.ProductVersion.ToString()
            , MessageBoxButtons.OK
            );

            var fileName = " Plantilla de Cuentas " + DateTime.Now.ToString("yyyy-MM-dd-hh-mm-ss") + ".xlsx";
            if (folderBrowserDialog1.ShowDialog() == DialogResult.OK)
            {
                fileName = folderBrowserDialog1.SelectedPath + "\\" + fileName;
                System.IO.Stream myStream = new System.IO.FileStream(fileName, System.IO.FileMode.Create, System.IO.FileAccess.ReadWrite, System.IO.FileShare.None);

                using (ExcelPackage pck = new ExcelPackage())
                {

                    ExcelWorksheet ws1 = pck.Workbook.Worksheets.Add("Plantilla");
                    ws1.Cells[1, 1].Value = dtgCuentas.Columns[1].HeaderText;
                    ws1.Cells[1, 2].Value = dtgCuentas.Columns[2].HeaderText;

                    int fila = 2;
                    
                    foreach (DataGridViewRow drv in dtgCuentas.Rows)
                    {
                        ws1.Cells[fila, 2].Value = drv.Cells["numCtaField"].Value.ToString();
                        fila++;
                    }

                    pck.SaveAs(myStream);
                }
                myStream.Close();
                MessageBoxEx.Show("Creaci�n de plantilla realizada " + Environment.NewLine + fileName + " finalizada.", Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private void buttonX23_Click(object sender, EventArgs e)
        {
            validar("B");
            validar("C");
        }

        private void validar(string tipo)
        {
            dtgValidador.Rows.Clear();
            if(tipo=="C")
            {                
                //Validar cuentas
                foreach (DataGridViewRow drv in dtgCuentas.Rows)
                {
                    cCatalogoCtas ocCatalogoCtas = (cCatalogoCtas)drv.Tag;
                    string mensaje = ocCatalogoCtas.validar();
                    if(mensaje!="")
                    {
                        drv.ErrorText=mensaje;
                        int n = dtgValidador.Rows.Add();
                        dtgValidador.Rows[n].Cells["TIPO"].Value="Cuenta Contables";
                        dtgValidador.Rows[n].Cells["CUENTA"].Value=ocCatalogoCtas.numCtaField;
                        dtgValidador.Rows[n].Cells["MENSAJE"].Value = mensaje;
                    }
                }
            }
            if (tipo == "B")
            {
                foreach (DataGridViewRow drv in dtgBalanza.Rows)
                {
                    cBalanza ocBalanza = (cBalanza)drv.Tag;
                    string mensaje = ocBalanza.validar();
                    if (mensaje != "")
                    {
                        drv.ErrorText = mensaje;
                        int n = dtgValidador.Rows.Add();
                        dtgValidador.Rows[n].Cells["TIPO"].Value = "Balanza Comprobaci�n";
                        dtgValidador.Rows[n].Cells["CUENTA"].Value = ocBalanza.numCtaField;
                        dtgValidador.Rows[n].Cells["MENSAJE"].Value = mensaje;
                    }
                }
            }
        }

        private void backgroundWorker4_DoWork(object sender, DoWorkEventArgs e)
        {

        }

        private void backgroundWorker4_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {

        }

        private void checkBoxX1_CheckedChanged(object sender, EventArgs e)
        {
            if(chkPeriodo.Checked)
            {
                filtrar_agrupador();
            }
            else
            {

                foreach (DataGridViewRow row in dtgCuentas.Rows)
                {
                    row.Visible = true;                    
                }

            }
        }

        public void filtrar_agrupador()
        {
            foreach (DataGridViewRow row in dtgCuentas.Rows)
            {
                if (row.Cells["codAgrupField"].Value.ToString()=="")
                {
                    row.Visible = true;
                }
                else
                {
                    row.Visible = false;
                }

            }

        }

        private void buttonX28_Click(object sender, EventArgs e)
        {
            exportar_excel_poliza();
        }

        private void exportar_excel_poliza()
        {
            var fileName = "Polizas " + DateTime.Now.ToString("yyyy-MM-dd-hh-mm-ss") + ".xlsx";
            if (folderBrowserDialog1.ShowDialog() == DialogResult.OK)
            {
                fileName = folderBrowserDialog1.SelectedPath + "\\" + fileName;
                System.IO.Stream myStream = new System.IO.FileStream(fileName, System.IO.FileMode.Create, System.IO.FileAccess.ReadWrite, System.IO.FileShare.None);

                using (ExcelPackage pck = new ExcelPackage())
                {
                    DataTable oDataTable = new DataTable();

                    string sSQL = " SELECT Poliza.ROW_ID_CATALOGO, Poliza.numField, Poliza.conceptoField AS Expr1, PolizasPolizaTransaccion.numCtaField, PolizasPolizaTransaccion.conceptoField, ";
                    sSQL+= " PolizasPolizaTransaccion.debeField, PolizasPolizaTransaccion.haberField, PolizasPolizaTransaccion.tipCambField,  ";
                    sSQL+= " PolizasPolizaTransaccion.tipCambFieldSpecified, PolizasPolizaTransaccion.ROW_ID_POLIZA, PolizasPolizaTransaccion.monedaField ";
                    sSQL += " FROM PolizasPolizaTransaccion INNER JOIN Poliza ON PolizasPolizaTransaccion.ROW_ID_POLIZA = Poliza.ROW_ID ";
                    sSQL += " WHERE Poliza.ROW_ID_CATALOGO=" + oObjeto.ROW_ID;
                    oDataTable = oData.EjecutarConsulta(sSQL);
                    ExcelWorksheet ws1 = pck.Workbook.Worksheets.Add("Exportar");
                    ws1.Cells["A1"].LoadFromDataTable(oDataTable, true);
                    pck.SaveAs(myStream);
                }
                myStream.Close();
                MessageBoxEx.Show("Exportaci�n del archivo " + Environment.NewLine + fileName + " finalizada.", Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Information);
            }

        }

        private void buttonItem7_Click(object sender, EventArgs e)
        {
            exportar_excel_cfdi();
        }

        private void exportar_excel_cfdi()
        {
            var fileName = "Polizas con CFDI" + DateTime.Now.ToString("yyyy-MM-dd-hh-mm-ss") + ".xlsx";
            if (folderBrowserDialog1.ShowDialog() == DialogResult.OK)
            {
                fileName = folderBrowserDialog1.SelectedPath + "\\" + fileName;
                System.IO.Stream myStream = new System.IO.FileStream(fileName, System.IO.FileMode.Create, System.IO.FileAccess.ReadWrite, System.IO.FileShare.None);

                using (ExcelPackage pck = new ExcelPackage())
                {
                    DataTable oDataTable = new DataTable();

                    string sSQL = " SELECT Poliza.numField, Poliza.conceptoField AS Expr1, PolizasPolizaTransaccion.numCtaField, PolizasPolizaTransaccion.conceptoField, ";
                    sSQL += " PolizasPolizaTransaccion.debeField, PolizasPolizaTransaccion.haberField, PolizasPolizaTransaccion.tipCambField,  ";
                    sSQL += " PolizasPolizaTransaccion.tipCambFieldSpecified, PolizasPolizaTransaccion.monedaField, PolizasPolizaTransaccionCompNal.uUID_CFDIField,  ";
                    sSQL += " PolizasPolizaTransaccionCompNal.rFCField, PolizasPolizaTransaccionCompNal.RFC_RECEPTOR, PolizasPolizaTransaccionCompNal.montoTotalField  ";
                    sSQL += " FROM PolizasPolizaTransaccion INNER JOIN Poliza ON PolizasPolizaTransaccion.ROW_ID_POLIZA = Poliza.ROW_ID INNER JOIN PolizasPolizaTransaccionCompNal ON PolizasPolizaTransaccion.ROW_ID = PolizasPolizaTransaccionCompNal.ROW_ID_TRANSACCION ";
                    sSQL += " WHERE Poliza.ROW_ID_CATALOGO=" + oObjeto.ROW_ID;
                    oDataTable = oData.EjecutarConsulta(sSQL);
                    ExcelWorksheet ws1 = pck.Workbook.Worksheets.Add("Exportar");
                    ws1.Cells["A1"].LoadFromDataTable(oDataTable, true);
                    pck.SaveAs(myStream);
                }
                myStream.Close();
                MessageBoxEx.Show("Exportaci�n del archivo " + Environment.NewLine + fileName + " finalizada.", Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private void buttonX15_Click(object sender, EventArgs e)
        {
            if (oObjeto.ROW_ID == "")
            {
                MessageBoxEx.Show("Guarde para generar la poliza.", Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
            frmDeclaracionPoliza ofrmDeclaracionPoliza = new frmDeclaracionPoliza(ocEMPRESA, oObjeto.ROW_ID, USUARIO);
            ofrmDeclaracionPoliza.ShowDialog();
        }

        private void buttonX14_Click_1(object sender, EventArgs e)
        {
            eliminar_sinagrupador();
        }

        private void eliminar_sinagrupador()
        {
            DialogResult Resultado = MessageBoxEx.Show("�Desea eliminar las cuentas sin agrupador?", Application.ProductName + "-" + Application.ProductVersion, MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            if (Resultado.ToString() != "Yes")
            {
                return;
            }
            cCatalogoCtas ocCatalogoCtas = new cCatalogoCtas();
            ocCatalogoCtas.eliminar_sin_agrupador();
            cargar_cuentas();

        }

        private void buttonX14_Click_2(object sender, EventArgs e)
        {
            eliminar_sinagrupador();
        }

        private void buttonItem3_Click(object sender, EventArgs e)
        {
            eliminar_cuentas_sin_balanza();
        }

        private void eliminar_cuentas_sin_balanza()
        {
            DialogResult Resultado = MessageBoxEx.Show("�Desea eliminar las cuentas que no existan en la Balanza de Comprobaci�n?", Application.ProductName + "-" + Application.ProductVersion, MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            if (Resultado.ToString() != "Yes")
            {
                return;
            }
            cCatalogoCtas ocCatalogoCtas = new cCatalogoCtas();
            ocCatalogoCtas.eliminar_sin_banlanza(oObjeto.ROW_ID);
            cargar_cuentas();
        }

        private void usarPeriodo_CheckedChanged(object sender, EventArgs e)
        {
            periodoInicial.Enabled = usarPeriodo.Checked;
            periodoFinal.Enabled = usarPeriodo.Checked;
            if (usarPeriodo.Enabled)
            {
                cargarPeriodo();
            }
            else
            {
                periodoInicial.Items.Clear();
                periodoFinal.Items.Clear();
            }

        }

        private void periodoFinal_SelectedIndexChanged(object sender, EventArgs e)
        {
           
        }
    }
}