using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevComponents.DotNetBar;
using System.IO;
using OfficeOpenXml;
using System.Collections;

namespace Desarrollo
{
    public partial class frmBalanza_Comprobacion : DevComponents.DotNetBar.Metro.MetroForm
    {
        public string tipo = "";
        public DateTime fechaFechaModBal;

        public frmBalanza_Comprobacion()
        {
            InitializeComponent();           
        }


        #region Acciones

        private void mostrar_mensaje(string mensaje, bool mostrar_error)
        {
            ToastNotification.Show(this,
                mensaje,
                mostrar_error ? global::Desarrollo.Properties.Resources.error : global::Desarrollo.Properties.Resources.guardar,
                3000,
                mostrar_error ? eToastGlowColor.Red : eToastGlowColor.Green,
                eToastPosition.TopCenter);
        }
        
        #endregion

        private void buttonX3_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Cancel;
        }

        private void buttonX1_Click(object sender, EventArgs e)
        {
            guardar();
        }

        private void guardar()
        {
            
        }

        private void TIPO_POLIZA_SelectedIndexChanged(object sender, EventArgs e)
        {
            verificar();
        }

        private void verificar()
        {
            DevComponents.Editors.ComboItem oComboItem = (DevComponents.Editors.ComboItem)TIPO_BALANZA.Items[TIPO_BALANZA.SelectedIndex];

            if(TIPO_BALANZA.SelectedItem!=null)
            {
                if(
                    (oComboItem.Value == "BN"))
                {
                    //dateTimePicker1.Enabled = false;
                }
                else
                {
                    //dateTimePicker1.Enabled = true;
                }


            }
        }

        private void buttonX22_Click(object sender, EventArgs e)
        {
            generar_poliza();
        }

        private void generar_poliza()
        {
            if (TIPO_BALANZA.SelectedIndex == -1)
            {
                MessageBoxEx.Show("Seleccione el tipo a declarar.", Application.ProductName + "-" + Application.ProductVersion, MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
            DevComponents.Editors.ComboItem oComboItem = (DevComponents.Editors.ComboItem)TIPO_BALANZA.Items[TIPO_BALANZA.SelectedIndex];

            tipo = oComboItem.Value.ToString();
            fechaFechaModBal = dateTimePicker1.Value;
            DialogResult = DialogResult.OK;
        }

    }
}