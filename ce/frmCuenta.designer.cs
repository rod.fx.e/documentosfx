namespace Desarrollo
{
    partial class frmCuenta
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            DevComponents.DotNetBar.ButtonX buttonX3;
            DevComponents.DotNetBar.ButtonX buttonX1;
            DevComponents.DotNetBar.ButtonX buttonX2;
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmCuenta));
            this.metroStatusBar1 = new DevComponents.DotNetBar.Metro.MetroStatusBar();
            this.labelItem1 = new DevComponents.DotNetBar.LabelItem();
            this.subCtaDeField = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.numCtaField = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.labelX1 = new DevComponents.DotNetBar.LabelX();
            this.labelX2 = new DevComponents.DotNetBar.LabelX();
            this.descField = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.labelX16 = new DevComponents.DotNetBar.LabelX();
            this.labelX17 = new DevComponents.DotNetBar.LabelX();
            this.nivelField = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.labelX18 = new DevComponents.DotNetBar.LabelX();
            this.labelX19 = new DevComponents.DotNetBar.LabelX();
            this.codAgrupField = new DevComponents.DotNetBar.Controls.ComboBoxEx();
            this.naturField = new DevComponents.DotNetBar.Controls.ComboBoxEx();
            this.comboItem1 = new DevComponents.Editors.ComboItem();
            this.A = new DevComponents.Editors.ComboItem();
            buttonX3 = new DevComponents.DotNetBar.ButtonX();
            buttonX1 = new DevComponents.DotNetBar.ButtonX();
            buttonX2 = new DevComponents.DotNetBar.ButtonX();
            this.SuspendLayout();
            // 
            // buttonX3
            // 
            buttonX3.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            buttonX3.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            buttonX3.Image = global::Desarrollo.Properties.Resources.Eliminar_18;
            buttonX3.Location = new System.Drawing.Point(141, 0);
            buttonX3.Name = "buttonX3";
            buttonX3.Size = new System.Drawing.Size(68, 24);
            buttonX3.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            buttonX3.TabIndex = 1;
            buttonX3.Text = "Eliminar";
            buttonX3.TextAlignment = DevComponents.DotNetBar.eButtonTextAlignment.Right;
            buttonX3.Click += new System.EventHandler(this.buttonX3_Click);
            // 
            // buttonX1
            // 
            buttonX1.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            buttonX1.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            buttonX1.Image = global::Desarrollo.Properties.Resources.Nueno_18;
            buttonX1.Location = new System.Drawing.Point(67, 0);
            buttonX1.Name = "buttonX1";
            buttonX1.Size = new System.Drawing.Size(68, 24);
            buttonX1.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            buttonX1.TabIndex = 1;
            buttonX1.Text = "Guardar";
            buttonX1.TextAlignment = DevComponents.DotNetBar.eButtonTextAlignment.Right;
            buttonX1.Click += new System.EventHandler(this.buttonX1_Click);
            // 
            // buttonX2
            // 
            buttonX2.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            buttonX2.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            buttonX2.Image = global::Desarrollo.Properties.Resources.Nuevo_18_19;
            buttonX2.Location = new System.Drawing.Point(0, 0);
            buttonX2.Name = "buttonX2";
            buttonX2.Size = new System.Drawing.Size(61, 24);
            buttonX2.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            buttonX2.TabIndex = 1;
            buttonX2.Text = "Nuevo";
            buttonX2.TextAlignment = DevComponents.DotNetBar.eButtonTextAlignment.Right;
            buttonX2.Click += new System.EventHandler(this.buttonX2_Click);
            // 
            // metroStatusBar1
            // 
            this.metroStatusBar1.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.metroStatusBar1.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.metroStatusBar1.ContainerControlProcessDialogKey = true;
            this.metroStatusBar1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.metroStatusBar1.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.metroStatusBar1.ForeColor = System.Drawing.Color.Black;
            this.metroStatusBar1.Items.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.labelItem1});
            this.metroStatusBar1.Location = new System.Drawing.Point(0, 115);
            this.metroStatusBar1.Name = "metroStatusBar1";
            this.metroStatusBar1.Size = new System.Drawing.Size(724, 21);
            this.metroStatusBar1.TabIndex = 8;
            // 
            // labelItem1
            // 
            this.labelItem1.Name = "labelItem1";
            // 
            // subCtaDeField
            // 
            this.subCtaDeField.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.subCtaDeField.Border.Class = "TextBoxBorder";
            this.subCtaDeField.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.subCtaDeField.ForeColor = System.Drawing.Color.Black;
            this.subCtaDeField.Location = new System.Drawing.Point(67, 88);
            this.subCtaDeField.Name = "subCtaDeField";
            this.subCtaDeField.Size = new System.Drawing.Size(290, 22);
            this.subCtaDeField.TabIndex = 546;
            this.subCtaDeField.TabStop = false;
            this.subCtaDeField.WatermarkBehavior = DevComponents.DotNetBar.eWatermarkBehavior.HideNonEmpty;
            // 
            // numCtaField
            // 
            this.numCtaField.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.numCtaField.Border.Class = "TextBoxBorder";
            this.numCtaField.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.numCtaField.ForeColor = System.Drawing.Color.Black;
            this.numCtaField.Location = new System.Drawing.Point(67, 32);
            this.numCtaField.Name = "numCtaField";
            this.numCtaField.Size = new System.Drawing.Size(290, 22);
            this.numCtaField.TabIndex = 548;
            this.numCtaField.TabStop = false;
            this.numCtaField.WatermarkBehavior = DevComponents.DotNetBar.eWatermarkBehavior.HideNonEmpty;
            // 
            // labelX1
            // 
            this.labelX1.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.labelX1.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX1.ForeColor = System.Drawing.Color.Black;
            this.labelX1.Location = new System.Drawing.Point(0, 30);
            this.labelX1.Name = "labelX1";
            this.labelX1.Size = new System.Drawing.Size(61, 22);
            this.labelX1.TabIndex = 579;
            this.labelX1.Text = "Cuenta";
            this.labelX1.TextAlignment = System.Drawing.StringAlignment.Far;
            // 
            // labelX2
            // 
            this.labelX2.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.labelX2.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX2.ForeColor = System.Drawing.Color.Black;
            this.labelX2.Location = new System.Drawing.Point(5, 88);
            this.labelX2.Name = "labelX2";
            this.labelX2.Size = new System.Drawing.Size(56, 22);
            this.labelX2.TabIndex = 580;
            this.labelX2.Text = "Subcuenta";
            this.labelX2.TextAlignment = System.Drawing.StringAlignment.Far;
            // 
            // descField
            // 
            this.descField.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.descField.Border.Class = "TextBoxBorder";
            this.descField.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.descField.ForeColor = System.Drawing.Color.Black;
            this.descField.Location = new System.Drawing.Point(67, 60);
            this.descField.Name = "descField";
            this.descField.Size = new System.Drawing.Size(657, 22);
            this.descField.TabIndex = 548;
            this.descField.TabStop = false;
            this.descField.WatermarkBehavior = DevComponents.DotNetBar.eWatermarkBehavior.HideNonEmpty;
            // 
            // labelX16
            // 
            this.labelX16.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.labelX16.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX16.ForeColor = System.Drawing.Color.Black;
            this.labelX16.Location = new System.Drawing.Point(0, 58);
            this.labelX16.Name = "labelX16";
            this.labelX16.Size = new System.Drawing.Size(61, 22);
            this.labelX16.TabIndex = 579;
            this.labelX16.Text = "Descripción";
            this.labelX16.TextAlignment = System.Drawing.StringAlignment.Far;
            // 
            // labelX17
            // 
            this.labelX17.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.labelX17.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX17.ForeColor = System.Drawing.Color.Black;
            this.labelX17.Location = new System.Drawing.Point(362, 32);
            this.labelX17.Name = "labelX17";
            this.labelX17.Size = new System.Drawing.Size(34, 22);
            this.labelX17.TabIndex = 583;
            this.labelX17.Text = "Nivel";
            this.labelX17.TextAlignment = System.Drawing.StringAlignment.Far;
            // 
            // nivelField
            // 
            this.nivelField.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.nivelField.Border.Class = "TextBoxBorder";
            this.nivelField.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.nivelField.ForeColor = System.Drawing.Color.Black;
            this.nivelField.Location = new System.Drawing.Point(401, 32);
            this.nivelField.Name = "nivelField";
            this.nivelField.Size = new System.Drawing.Size(45, 22);
            this.nivelField.TabIndex = 582;
            this.nivelField.TabStop = false;
            this.nivelField.WatermarkBehavior = DevComponents.DotNetBar.eWatermarkBehavior.HideNonEmpty;
            // 
            // labelX18
            // 
            this.labelX18.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.labelX18.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX18.ForeColor = System.Drawing.Color.Black;
            this.labelX18.Location = new System.Drawing.Point(452, 32);
            this.labelX18.Name = "labelX18";
            this.labelX18.Size = new System.Drawing.Size(60, 22);
            this.labelX18.TabIndex = 583;
            this.labelX18.Text = "Naturaleza";
            this.labelX18.TextAlignment = System.Drawing.StringAlignment.Far;
            // 
            // labelX19
            // 
            this.labelX19.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.labelX19.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX19.ForeColor = System.Drawing.Color.Black;
            this.labelX19.Location = new System.Drawing.Point(363, 88);
            this.labelX19.Name = "labelX19";
            this.labelX19.Size = new System.Drawing.Size(60, 22);
            this.labelX19.TabIndex = 583;
            this.labelX19.Text = "Agrupador";
            this.labelX19.TextAlignment = System.Drawing.StringAlignment.Far;
            // 
            // codAgrupField
            // 
            this.codAgrupField.DisplayMember = "Text";
            this.codAgrupField.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.codAgrupField.ForeColor = System.Drawing.Color.Black;
            this.codAgrupField.FormattingEnabled = true;
            this.codAgrupField.ItemHeight = 16;
            this.codAgrupField.Location = new System.Drawing.Point(430, 88);
            this.codAgrupField.Name = "codAgrupField";
            this.codAgrupField.Size = new System.Drawing.Size(294, 22);
            this.codAgrupField.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.codAgrupField.TabIndex = 584;
            // 
            // naturField
            // 
            this.naturField.DisplayMember = "Text";
            this.naturField.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.naturField.ForeColor = System.Drawing.Color.Black;
            this.naturField.FormattingEnabled = true;
            this.naturField.ItemHeight = 16;
            this.naturField.Items.AddRange(new object[] {
            this.comboItem1,
            this.A});
            this.naturField.Location = new System.Drawing.Point(518, 32);
            this.naturField.Name = "naturField";
            this.naturField.Size = new System.Drawing.Size(206, 22);
            this.naturField.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.naturField.TabIndex = 585;
            // 
            // comboItem1
            // 
            this.comboItem1.Text = "D - Deudora";
            this.comboItem1.Value = "D";
            // 
            // A
            // 
            this.A.Text = "A - Acreedora";
            this.A.Value = "A";
            // 
            // frmCuenta
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.BottomLeftCornerSize = 0;
            this.ClientSize = new System.Drawing.Size(724, 136);
            this.Controls.Add(this.naturField);
            this.Controls.Add(this.codAgrupField);
            this.Controls.Add(this.labelX19);
            this.Controls.Add(this.labelX18);
            this.Controls.Add(this.labelX17);
            this.Controls.Add(this.nivelField);
            this.Controls.Add(this.labelX2);
            this.Controls.Add(this.labelX16);
            this.Controls.Add(this.labelX1);
            this.Controls.Add(this.descField);
            this.Controls.Add(this.numCtaField);
            this.Controls.Add(this.subCtaDeField);
            this.Controls.Add(buttonX3);
            this.Controls.Add(buttonX1);
            this.Controls.Add(buttonX2);
            this.Controls.Add(this.metroStatusBar1);
            this.Cursor = System.Windows.Forms.Cursors.Default;
            this.DoubleBuffered = true;
            this.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "frmCuenta";
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Cuenta";
            this.ResumeLayout(false);

        }

        #endregion

        private DevComponents.DotNetBar.Metro.MetroStatusBar metroStatusBar1;
        private DevComponents.DotNetBar.LabelItem labelItem1;
        private DevComponents.DotNetBar.Controls.TextBoxX subCtaDeField;
        private DevComponents.DotNetBar.Controls.TextBoxX numCtaField;
        private DevComponents.DotNetBar.LabelX labelX1;
        private DevComponents.DotNetBar.LabelX labelX2;
        private DevComponents.DotNetBar.Controls.TextBoxX descField;
        private DevComponents.DotNetBar.LabelX labelX16;
        private DevComponents.DotNetBar.LabelX labelX17;
        private DevComponents.DotNetBar.Controls.TextBoxX nivelField;
        private DevComponents.DotNetBar.LabelX labelX18;
        private DevComponents.DotNetBar.LabelX labelX19;
        private DevComponents.DotNetBar.Controls.ComboBoxEx codAgrupField;
        private DevComponents.DotNetBar.Controls.ComboBoxEx naturField;
        private DevComponents.Editors.ComboItem comboItem1;
        private DevComponents.Editors.ComboItem A;

    }
}