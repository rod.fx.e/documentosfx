using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevComponents.DotNetBar;
using System.IO;
using OfficeOpenXml;

namespace Desarrollo
{
    public partial class frmComprobante_Filtro : DevComponents.DotNetBar.Metro.MetroForm
    {
        bool modificado = false;
        string ROW_ID_CATALOGO;
        public string BATCH_INICIALp="";
        public string BATCH_FINALp = "";
        public string APCp = "";
        public string APIp = "";
        public string ARIp = "";
        public string ARCp = "";


        public frmComprobante_Filtro()
        {
            InitializeComponent();
            
        }


        #region Acciones
        private void guardar()
        {
            BATCH_INICIALp = BATCH_INICIAL.Text;
            BATCH_FINALp = BATCH_FINAL.Text;
            if (APC.Checked)
            {
                APCp = "APC";
            }
            if (API.Checked)
            {
                APIp = "API";
            }
            
            if (ARI.Checked)
            {
                ARIp = "ARI";
            }
            if (ARC.Checked)
            {
                ARCp = "ARC";
            }

            DialogResult = DialogResult.OK;
        }

        private void mostrar_mensaje(string mensaje, bool mostrar_error)
        {
            ToastNotification.Show(this,
                mensaje,
                mostrar_error ? global::Desarrollo.Properties.Resources.error : global::Desarrollo.Properties.Resources.guardar,
                3000,
                mostrar_error ? eToastGlowColor.Red : eToastGlowColor.Green,
                eToastPosition.TopCenter);
        }

        #endregion

        private void buttonX3_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Cancel;
        }

        private void buttonX1_Click(object sender, EventArgs e)
        {
            guardar();
        }

        private void frmComprobante_Filtro_Load(object sender, EventArgs e)
        {

        }

        private void API_CheckedChanged(object sender, EventArgs e)
        {

        }

        private void checkBoxX1_CheckedChanged(object sender, EventArgs e)
        {

        }

    }
}