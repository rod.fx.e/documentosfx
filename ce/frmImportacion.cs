using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevComponents.DotNetBar;
using System.Drawing.Imaging;
using System.IO;
using System.Reflection;
using OfficeOpenXml;
using System.Threading;

namespace Desarrollo
{
    public partial class frmImportacion : DevComponents.DotNetBar.Metro.MetroForm
    {
        private cCONEXCION oData=new cCONEXCION();
        private cCONEXCION oData_ERP = new cCONEXCION();
        private bool modificado;
        string USUARIO = "";

        public frmImportacion(string sConn, string sConn_ERP, string USUARIOp)
        {
            oData.sConn = sConn;
            oData_ERP.sConn = sConn_ERP;
            USUARIO = USUARIOp;
            InitializeComponent();

            circularProgress1.Visible = false;
            circularProgress1.IsRunning = true;
            backgroundWorker1.WorkerReportsProgress = true;

            limpiar();            
        }


        private void saveButton_Click(object sender, EventArgs e)
        {
            guardar();
        }

        #region Acciones

        private DataTable GetDataTableFromDGV(DataGridView dgv)
        {
            var dt = new DataTable();
            int contador = 0;
            foreach (DataGridViewColumn column in dgv.Columns)
            {
                DataColumnCollection columns = dt.Columns;
                if (columns.Contains(column.HeaderText))
                {
                    dt.Columns.Add(column.HeaderText + contador.ToString());
                }
                else
                {
                    dt.Columns.Add(column.HeaderText);
                }
                contador++;
            }

            object[] cellValues = new object[dgv.Columns.Count];
            foreach (DataGridViewRow row in dgv.Rows)
            {
                for (int i = 0; i < row.Cells.Count; i++)
                {
                    cellValues[i] = row.Cells[i].Value;
                }
                dt.Rows.Add(cellValues);
            }

            return dt;
        }
        

        private void guardar()
        {
            try
            {
                //Verificar 
                if (true)
                {
                    //Navegar linea por linea
                    circularProgress1.Visible = true;
                    circularProgress1.IsRunning = true;
                    if (!backgroundWorker1.IsBusy)
                    {
                        backgroundWorker1.RunWorkerAsync(dtgConfiguracion);
                    }  
                }
            }
            catch (Exception e)
            {
                MessageBoxEx.Show(e.ToString());
            }
        }
        private void backgroundWorker1_DoWork(object sender, DoWorkEventArgs e)
        {
            try
            {
                int n = 0;
                circularProgress1.Minimum=0;
                circularProgress1.Maximum =dtgConfiguracion.Rows.Count;
                foreach (DataGridViewRow drv in dtgConfiguracion.Rows)
                {
                    n++;
                    Thread.Sleep(100);
                    // To Report progress.
                    backgroundWorker1.ReportProgress(n, drv);
                    if (backgroundWorker1.CancellationPending)
                    {
                        e.Cancel = true;
                        backgroundWorker1.ReportProgress(0);
                        return;
                    }
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        private void backgroundWorker1_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            if (!backgroundWorker1.CancellationPending)
            {
                DataGridViewRow drv = (DataGridViewRow)e.UserState;
                if (drv != null)
                {
                    try
                    {
                        
                        if (true)
                        {
                            circularProgress1.Value = e.ProgressPercentage;
                            circularProgress1.ProgressText = "Procesando.. " + e.ProgressPercentage.ToString();// + " de " + TotalRecords;
                        }
                    }
                    catch (Exception exception)
                    {
                        MessageBoxEx.Show(exception.ToString(), Application.ProductName + "-" + Application.ProductVersion, MessageBoxButtons.OK, MessageBoxIcon.Error);
                        return;
                    }
                }
            }
        }

        public static DataTable getDataTableFromExcel(string path, int hoja)
        {
            using (var pck = new OfficeOpenXml.ExcelPackage())
            {
                using (var stream = File.OpenRead(path))
                {
                    pck.Load(stream);
                }
                var ws = pck.Workbook.Worksheets[hoja];
                DataTable tbl = new DataTable();
                bool hasHeader = true; // adjust it accordingly( i've mentioned that this is a simple approach)
                foreach (var firstRowCell in ws.Cells[1, 1, 1, ws.Dimension.End.Column])
                {
                    tbl.Columns.Add(hasHeader ? firstRowCell.Text : string.Format("Column {0}", firstRowCell.Start.Column));
                }
                var startRow = hasHeader ? 2 : 1;
                for (var rowNum = startRow; rowNum <= ws.Dimension.End.Row; rowNum++)
                {
                    var wsRow = ws.Cells[rowNum, 1, rowNum, ws.Dimension.End.Column];
                    var row = tbl.NewRow();
                    foreach (var cell in wsRow)
                    {
                        row[cell.Start.Column - 1] = cell.Text;
                    }
                    tbl.Rows.Add(row);
                }
                return tbl;
            }
        }

        private bool IsDate(string fecha)
        {
            try
            {
                DateTime fecha_tmp = DateTime.Parse(fecha);
                return true;
            }
            catch
            {
                return false;
            }

        }

        public void limpiar()
        {
            if (modificado)
            {

                DialogResult Resultado = MessageBoxEx.Show("�Desea guardar los cambios?", Application.ProductName + "-" + Application.ProductVersion, MessageBoxButtons.YesNo,MessageBoxIcon.Question);
                if (Resultado.ToString() == "Yes")
                {
                    guardar();
                }


            }
            modificado = false;
            DIRECTORIO.Text = "";
            dtgConfiguracion.Rows.Clear();           
        }
        private void backgroundWorker1_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            completar(e);
        }
        private void completar(RunWorkerCompletedEventArgs e)
        {
            if (e.Cancelled)
            {
                labelItem1.Text = "Cancelado por el usuario...";
                circularProgress1.Value = 0;
            }
            // Check to see if an error occurred in the background process.
            else if (e.Error != null)
            {
                labelItem1.Text = e.Error.Message;
            }
            else
            {
                //BackGround Task Completed with out Error
                labelItem1.Text = " Finalizado...";
            }
            circularProgress1.Visible = false;
        }
        #endregion

        private void Solo_Numero_KeyPress(object sender, KeyPressEventArgs e)
        {
            modificado = true;
            if (!char.IsDigit(e.KeyChar) && !char.IsControl(e.KeyChar) && !char.IsPunctuation(e.KeyChar)) e.Handled = true;
        }

        private void buttonX1_Click(object sender, EventArgs e)
        {
            limpiar();
        }

        private void procesar(DataGridView dtg, int hoja)
        {
            try
            {
                dtg.Rows.Clear();
                int n;
                int rowIndex = 1;
                FileInfo existingFile = new FileInfo(DIRECTORIO.Text);
                using (ExcelPackage package = new ExcelPackage(existingFile))
                {
                    ExcelWorksheet worksheet = package.Workbook.Worksheets[hoja];
                    while (worksheet.Cells[rowIndex, 1].Value != null)
                    {
                        //Agregar las lineas de la cabecera
                        if (rowIndex == 1)
                        {
                            foreach (var firstRowCell in worksheet.Cells[1, 1, 1, worksheet.Dimension.End.Column])
                            {
                                //dtg.Columns.Add("C" + dtg.Columns.Count.ToString(), firstRowCell.Text);
                                dtg.Columns.Add(firstRowCell.Text, firstRowCell.Text);
                            }
                        }
                        else
                        {
                            n = dtg.Rows.Add();
                            for (var col = 1; col <= worksheet.Dimension.End.Column; col++)
                            {
                                dtg.Rows[n].Cells[col-1].Value = worksheet.Cells[rowIndex, col].Text;
                            }
                        }
                        rowIndex++;
                       
                    }
                    
                }                
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message.ToString());
            }
            
        }

        private void button1_Click(object sender, EventArgs e)
        {
            OpenFileDialog openFileDialog1 = new OpenFileDialog();
            openFileDialog1.Filter = "Excel files (*.xlsx)|*.xlsx|All files (*.*)|*.*";
            openFileDialog1.FilterIndex = 1;
            openFileDialog1.RestoreDirectory = true;

            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                try
                {
                    DIRECTORIO.Text = openFileDialog1.FileName;
                    procesar(dtgConfiguracion,1);
                    procesar(dataGridViewX1, 2);
                }
                catch (Exception)
                {
                    MessageBoxEx.Show("No existe el Archivo seleccionado", Application.ProductName + "-" + Application.ProductVersion, MessageBoxButtons.OK);
                }
            }
        }

        private void buttonX2_Click(object sender, EventArgs e)
        {
            limpiar();
        }

        private void buttonX5_Click(object sender, EventArgs e)
        {
            guardar();
        }

        private void buttonX4_Click(object sender, EventArgs e)
        {
            directorio_ver();
        }

        public void directorio_ver()
        {

            if (folderBrowserDialog1.ShowDialog() == DialogResult.OK)
            {
                DIRECTORIO.Text = folderBrowserDialog1.SelectedPath;
            }

        }

        private void buttonX6_Click(object sender, EventArgs e)
        {
            guardar();
        }

        private void buttonX7_Click(object sender, EventArgs e)
        {
            limpiar();
        }

    }
}