﻿using DevComponents.DotNetBar;
using Generales;
using ModeloDocumentosFX;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Desarrollo
{
    class cEMPRESA_DESCRIPCION
    {
        private string sSQL = "";
        public string ROW_ID { get; set; }
        public string ROW_ID_EMPRESA { get; set; }
        
        public string VISUAL_DESCRIPCION { get; set; }
        public string DESCRIPCION { get; set; }

        public cCONEXCION oData = new cCONEXCION();
        public override string ToString() { return ROW_ID; }

        public cEMPRESA_DESCRIPCION()
        {
            ModeloDocumentosFXContainer dbContext = new ModeloDocumentosFXContainer();
            try
            {
                string conection = dbContext.Database.Connection.ConnectionString;
                oData = new cCONEXCION(conection);
            }
            catch (Exception err)
            {
                ErrorFX.mostrar(err, false, true, "frmEmpresa - 96 - ",false);
            }
            limpiar();
        }

        //public cEMPRESA_DESCRIPCION(cCONEXCION pData)
        //{
        //    oData = pData;
        //    limpiar();
        //}

        public void limpiar()
        {
            ROW_ID = "";
            ROW_ID_EMPRESA = "";
            VISUAL_DESCRIPCION = "";
            DESCRIPCION = "";            
        }

        public List<cEMPRESA_DESCRIPCION> todos(string ROW_ID_EMPRESAp = "", string RFCp = "")
        {

            List<cEMPRESA_DESCRIPCION> lista = new List<cEMPRESA_DESCRIPCION>();

            sSQL = " SELECT * ";
            sSQL += " FROM EMPRESA_DESCRIPCION ";
            sSQL += " WHERE 1=1 ";
            if(RFCp!="")
            {
                sSQL += " AND '" + RFCp + "' IN (SELECT RFC FROM EMPRESA E WHERE E.ROW_ID=EMPRESA_DESCRIPCION.ROW_ID_EMPRESA) ";
            }
            if (ROW_ID_EMPRESAp!="")
            {
                sSQL += " AND ROW_ID_EMPRESA=" + ROW_ID_EMPRESAp + " ";
            }
            

            sSQL += " ORDER BY ROW_ID";
            DataTable oDataTable = oData.EjecutarConsulta(sSQL);
            foreach (DataRow oDataRow in oDataTable.Rows)
            {
                cEMPRESA_DESCRIPCION ocEMPRESA_DESCRIPCION = new cEMPRESA_DESCRIPCION();
                ocEMPRESA_DESCRIPCION.cargar(oDataRow["ROW_ID"].ToString());
                lista.Add(ocEMPRESA_DESCRIPCION);
            }
            return lista;

        }

        public bool cargar(string ROW_IDp)
        {
            sSQL = " SELECT * ";
            sSQL += " FROM EMPRESA_DESCRIPCION ";
            sSQL += " WHERE ROW_ID=" + ROW_IDp + " ";
            DataTable oDataTable = oData.EjecutarConsulta(sSQL);
            if (oDataTable != null)
            {
                foreach (DataRow oDataRow in oDataTable.Rows)
                {
                    //Añadir el Registro
                    ROW_ID = oDataRow["ROW_ID"].ToString();
                    ROW_ID_EMPRESA = oDataRow["ROW_ID_EMPRESA"].ToString();
                    VISUAL_DESCRIPCION = oDataRow["VISUAL_DESCRIPCION"].ToString();
                    DESCRIPCION = oDataRow["DESCRIPCION"].ToString();
                    return true;
                }
            }
            return false;
        }

        public bool guardar()
        {
            if (ROW_ID_EMPRESA.Trim() == "")
            {
                MessageBoxEx.Show("El ROW_ID_EMPRESA es requerido.", Application.ProductName + "-" + Application.ProductVersion, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return false;
            }

            if (ROW_ID == "")
            {
                sSQL = " INSERT INTO EMPRESA_DESCRIPCION ";
                sSQL += " ( ";
                sSQL += " [ROW_ID_EMPRESA] ";
                sSQL += " ,[VISUAL_DESCRIPCION],[DESCRIPCION]"; 
                sSQL += " )";
                sSQL += " VALUES ";
                sSQL += "(";
                sSQL += " '" + ROW_ID_EMPRESA+ "'";
                sSQL += " ,'" + VISUAL_DESCRIPCION + "','" + DESCRIPCION + "'";
                sSQL += ")";
                DataTable oDataTable = oData.EjecutarConsulta(sSQL);
                if (oDataTable != null)
                {

                    sSQL = "SELECT TOP 1 MAX(ROW_ID) as ROW_ID FROM EMPRESA_DESCRIPCION";
                    oDataTable = oData.EjecutarConsulta(sSQL);
                    if (oDataTable != null)
                    {
                        foreach (DataRow oDataRow in oDataTable.Rows)
                        {
                            ROW_ID = oDataRow["ROW_ID"].ToString();
                        }
                    }
                }

            }
            else
            {
                sSQL = " UPDATE EMPRESA_DESCRIPCION ";
                sSQL += " SET ";
                sSQL += " VISUAL_DESCRIPCION='" + VISUAL_DESCRIPCION + "',DESCRIPCION='" + DESCRIPCION + "'";
                sSQL += " WHERE ROW_ID=" + ROW_ID;
                oData.EjecutarConsulta(sSQL);

            }
            return true;

        }

        public bool eliminar(string ROW_IDp)
        {
            if (ROW_IDp != "")
            {
                sSQL = "DELETE FROM EMPRESA_DESCRIPCION ";
                sSQL += " WHERE ROW_ID='" + ROW_IDp + "' ";
                oData.EjecutarConsulta(sSQL);
                return true;
            }
            return false;
        }

        internal static string cambiar_descripcion(string descripcion)
        {
            cCONEXCION oData = new cCONEXCION();
            DataTable oDataTable;
            string sSQL = "";
            try
            {

                
                sSQL = " SELECT TOP 1 * ";
                sSQL += " FROM EMPRESA_DESCRIPCION ";
                sSQL += " WHERE VISUAL_DESCRIPCION like '%" + descripcion.Substring(0, 10).Replace("'","") + "%' ";
                oDataTable = oData.EjecutarConsulta(sSQL);
                if (oDataTable != null)
                {
                    foreach (DataRow oDataRow in oDataTable.Rows)
                    {
                        //Añadir el Registro
                        return oDataRow["DESCRIPCION"].ToString();
                    }
                }
                //
            }
            catch(Exception e)
            {

            }
            sSQL = " SELECT TOP 1 * ";
            sSQL += " FROM EMPRESA_DESCRIPCION ";
            sSQL += " WHERE VISUAL_DESCRIPCION like '%" + descripcion.Replace("'", "") + "%' ";
            oDataTable = oData.EjecutarConsulta(sSQL);
            if (oDataTable != null)
            {
                foreach (DataRow oDataRow in oDataTable.Rows)
                {
                    //Añadir el Registro
                    return oDataRow["DESCRIPCION"].ToString();
                }
            }


            return descripcion;
        }
    }
}
