namespace Desarrollo
{
    partial class frmXML
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            DevComponents.DotNetBar.ButtonX buttonX3;
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmXML));
            this.metroStatusBar1 = new DevComponents.DotNetBar.Metro.MetroStatusBar();
            this.labelItem1 = new DevComponents.DotNetBar.LabelItem();
            this.buttonX22 = new DevComponents.DotNetBar.ButtonX();
            this.NumTramite = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.labelX12 = new DevComponents.DotNetBar.LabelX();
            this.NumOrden = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.TIPO_POLIZA = new DevComponents.DotNetBar.Controls.ComboBoxEx();
            this.AF = new DevComponents.Editors.ComboItem();
            this.FC = new DevComponents.Editors.ComboItem();
            this.DE = new DevComponents.Editors.ComboItem();
            this.CO = new DevComponents.Editors.ComboItem();
            this.labelX11 = new DevComponents.DotNetBar.LabelX();
            this.labelX10 = new DevComponents.DotNetBar.LabelX();
            this.labelX1 = new DevComponents.DotNetBar.LabelX();
            this.labelX2 = new DevComponents.DotNetBar.LabelX();
            buttonX3 = new DevComponents.DotNetBar.ButtonX();
            this.SuspendLayout();
            // 
            // buttonX3
            // 
            buttonX3.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            buttonX3.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            buttonX3.Image = global::Desarrollo.Properties.Resources.Eliminar_18;
            buttonX3.Location = new System.Drawing.Point(110, 0);
            buttonX3.Name = "buttonX3";
            buttonX3.Size = new System.Drawing.Size(68, 24);
            buttonX3.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            buttonX3.TabIndex = 1;
            buttonX3.Text = "Cancelar";
            buttonX3.TextAlignment = DevComponents.DotNetBar.eButtonTextAlignment.Right;
            buttonX3.Click += new System.EventHandler(this.buttonX3_Click);
            // 
            // metroStatusBar1
            // 
            this.metroStatusBar1.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.metroStatusBar1.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.metroStatusBar1.ContainerControlProcessDialogKey = true;
            this.metroStatusBar1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.metroStatusBar1.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.metroStatusBar1.ForeColor = System.Drawing.Color.Black;
            this.metroStatusBar1.Items.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.labelItem1});
            this.metroStatusBar1.Location = new System.Drawing.Point(0, 160);
            this.metroStatusBar1.Name = "metroStatusBar1";
            this.metroStatusBar1.Size = new System.Drawing.Size(396, 21);
            this.metroStatusBar1.TabIndex = 8;
            // 
            // labelItem1
            // 
            this.labelItem1.Name = "labelItem1";
            // 
            // buttonX22
            // 
            this.buttonX22.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.buttonX22.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.buttonX22.Image = global::Desarrollo.Properties.Resources.sat_18;
            this.buttonX22.Location = new System.Drawing.Point(12, 0);
            this.buttonX22.Name = "buttonX22";
            this.buttonX22.Size = new System.Drawing.Size(92, 24);
            this.buttonX22.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.buttonX22.TabIndex = 587;
            this.buttonX22.Text = "Generar XML";
            this.buttonX22.Click += new System.EventHandler(this.buttonX22_Click);
            // 
            // NumTramite
            // 
            this.NumTramite.AcceptsTab = true;
            this.NumTramite.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.NumTramite.Border.Class = "TextBoxBorder";
            this.NumTramite.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.NumTramite.ForeColor = System.Drawing.Color.Black;
            this.NumTramite.Location = new System.Drawing.Point(125, 109);
            this.NumTramite.MaxLength = 10;
            this.NumTramite.Name = "NumTramite";
            this.NumTramite.Size = new System.Drawing.Size(268, 22);
            this.NumTramite.TabIndex = 602;
            // 
            // labelX12
            // 
            this.labelX12.AutoSize = true;
            this.labelX12.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.labelX12.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX12.ForeColor = System.Drawing.Color.Black;
            this.labelX12.Location = new System.Drawing.Point(1, 111);
            this.labelX12.Name = "labelX12";
            this.labelX12.Size = new System.Drawing.Size(118, 17);
            this.labelX12.TabIndex = 601;
            this.labelX12.Text = "N�mero de Transacci�n";
            // 
            // NumOrden
            // 
            this.NumOrden.AcceptsTab = true;
            this.NumOrden.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.NumOrden.Border.Class = "TextBoxBorder";
            this.NumOrden.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.NumOrden.ForeColor = System.Drawing.Color.Black;
            this.NumOrden.Location = new System.Drawing.Point(125, 58);
            this.NumOrden.MaxLength = 13;
            this.NumOrden.Name = "NumOrden";
            this.NumOrden.Size = new System.Drawing.Size(268, 22);
            this.NumOrden.TabIndex = 600;
            // 
            // TIPO_POLIZA
            // 
            this.TIPO_POLIZA.AutoCompleteCustomSource.AddRange(new string[] {
            "Abierto",
            "Cerrado"});
            this.TIPO_POLIZA.DisplayMember = "Text";
            this.TIPO_POLIZA.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.TIPO_POLIZA.ForeColor = System.Drawing.Color.Black;
            this.TIPO_POLIZA.FormattingEnabled = true;
            this.TIPO_POLIZA.ItemHeight = 16;
            this.TIPO_POLIZA.Items.AddRange(new object[] {
            this.AF,
            this.FC,
            this.DE,
            this.CO});
            this.TIPO_POLIZA.Location = new System.Drawing.Point(125, 30);
            this.TIPO_POLIZA.Name = "TIPO_POLIZA";
            this.TIPO_POLIZA.Size = new System.Drawing.Size(268, 22);
            this.TIPO_POLIZA.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.TIPO_POLIZA.TabIndex = 599;
            this.TIPO_POLIZA.SelectedIndexChanged += new System.EventHandler(this.TIPO_POLIZA_SelectedIndexChanged);
            // 
            // AF
            // 
            this.AF.Text = "Acto de Fiscalizaci�n";
            this.AF.Value = "AF";
            // 
            // FC
            // 
            this.FC.Text = "Fiscalizaci�n Compulsa";
            this.FC.Value = "FC";
            // 
            // DE
            // 
            this.DE.Text = "Devoluci�n";
            this.DE.Value = "DE";
            // 
            // CO
            // 
            this.CO.Text = "Compensaci�n";
            this.CO.Value = "CO";
            // 
            // labelX11
            // 
            this.labelX11.AutoSize = true;
            this.labelX11.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.labelX11.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX11.ForeColor = System.Drawing.Color.Black;
            this.labelX11.Location = new System.Drawing.Point(28, 60);
            this.labelX11.Name = "labelX11";
            this.labelX11.Size = new System.Drawing.Size(91, 17);
            this.labelX11.TabIndex = 597;
            this.labelX11.Text = "N�mero de Orden";
            // 
            // labelX10
            // 
            this.labelX10.AutoSize = true;
            this.labelX10.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.labelX10.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX10.ForeColor = System.Drawing.Color.Black;
            this.labelX10.Location = new System.Drawing.Point(48, 32);
            this.labelX10.Name = "labelX10";
            this.labelX10.Size = new System.Drawing.Size(71, 17);
            this.labelX10.TabIndex = 598;
            this.labelX10.Text = "Tipo de Poliza";
            // 
            // labelX1
            // 
            this.labelX1.AutoSize = true;
            this.labelX1.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.labelX1.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX1.ForeColor = System.Drawing.Color.Black;
            this.labelX1.Location = new System.Drawing.Point(125, 86);
            this.labelX1.Name = "labelX1";
            this.labelX1.Size = new System.Drawing.Size(277, 17);
            this.labelX1.TabIndex = 603;
            this.labelX1.Text = "13 car�cteres.  Patr�n: [A-Z]{3}[0-6][0-9][0-9]{5}(/)[0-9]{2}";
            // 
            // labelX2
            // 
            this.labelX2.AutoSize = true;
            this.labelX2.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.labelX2.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX2.ForeColor = System.Drawing.Color.Black;
            this.labelX2.Location = new System.Drawing.Point(125, 137);
            this.labelX2.Name = "labelX2";
            this.labelX2.Size = new System.Drawing.Size(150, 17);
            this.labelX2.TabIndex = 604;
            this.labelX2.Text = "10 car�cteres. Patr�n: [0-9]{10}";
            // 
            // frmXML
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.BottomLeftCornerSize = 0;
            this.ClientSize = new System.Drawing.Size(396, 181);
            this.Controls.Add(this.labelX2);
            this.Controls.Add(this.labelX1);
            this.Controls.Add(this.NumTramite);
            this.Controls.Add(this.labelX12);
            this.Controls.Add(this.NumOrden);
            this.Controls.Add(this.TIPO_POLIZA);
            this.Controls.Add(this.labelX11);
            this.Controls.Add(this.labelX10);
            this.Controls.Add(this.buttonX22);
            this.Controls.Add(buttonX3);
            this.Controls.Add(this.metroStatusBar1);
            this.Cursor = System.Windows.Forms.Cursors.Default;
            this.DoubleBuffered = true;
            this.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "frmXML";
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Informaci�n para el XML";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevComponents.DotNetBar.Metro.MetroStatusBar metroStatusBar1;
        private DevComponents.DotNetBar.LabelItem labelItem1;
        private DevComponents.DotNetBar.ButtonX buttonX22;
        private DevComponents.DotNetBar.Controls.TextBoxX NumTramite;
        private DevComponents.DotNetBar.LabelX labelX12;
        private DevComponents.DotNetBar.Controls.TextBoxX NumOrden;
        private DevComponents.DotNetBar.Controls.ComboBoxEx TIPO_POLIZA;
        private DevComponents.Editors.ComboItem AF;
        private DevComponents.Editors.ComboItem FC;
        private DevComponents.Editors.ComboItem DE;
        private DevComponents.Editors.ComboItem CO;
        private DevComponents.DotNetBar.LabelX labelX11;
        private DevComponents.DotNetBar.LabelX labelX10;
        private DevComponents.DotNetBar.LabelX labelX1;
        private DevComponents.DotNetBar.LabelX labelX2;

    }
}