namespace Desarrollo
{
    partial class frmEmpresa
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            DevComponents.DotNetBar.ButtonX buttonX3;
            DevComponents.DotNetBar.ButtonX buttonX1;
            DevComponents.DotNetBar.ButtonX buttonX2;
            DevComponents.DotNetBar.ButtonX buttonX17;
            DevComponents.DotNetBar.ButtonX buttonX4;
            DevComponents.DotNetBar.ButtonX buttonX7;
            DevComponents.DotNetBar.ButtonX buttonX8;
            DevComponents.DotNetBar.ButtonX buttonX5;
            DevComponents.DotNetBar.ButtonX buttonX6;
            DevComponents.DotNetBar.ButtonX buttonX11;
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle7 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle8 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle9 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmEmpresa));
            this.metroStatusBar1 = new DevComponents.DotNetBar.Metro.MetroStatusBar();
            this.labelItem1 = new DevComponents.DotNetBar.LabelItem();
            this.toolStripStatusLabel1 = new DevComponents.DotNetBar.LabelItem();
            this.ID = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.labelX1 = new DevComponents.DotNetBar.LabelX();
            this.labelX2 = new DevComponents.DotNetBar.LabelX();
            this.superTabControl1 = new DevComponents.DotNetBar.SuperTabControl();
            this.superTabControlPanel1 = new DevComponents.DotNetBar.SuperTabControlPanel();
            this.PERIODO = new DevComponents.DotNetBar.Controls.CheckBoxX();
            this.labelX8 = new DevComponents.DotNetBar.LabelX();
            this.NO_CERTIFICADO = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.ACCOUNT_CUSTOMER = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.labelX6 = new DevComponents.DotNetBar.LabelX();
            this.ACCOUNT_VENDOR = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.labelX5 = new DevComponents.DotNetBar.LabelX();
            this.labelX4 = new DevComponents.DotNetBar.LabelX();
            this.PASSWORD = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.LLAVE = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.CERTIFICADO = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.labelX3 = new DevComponents.DotNetBar.LabelX();
            this.NIVEL = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.DIRECTORIO_XML_PROVEEDORES = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.DIRECTORIO_XML_CLIENTES = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.labelX10 = new DevComponents.DotNetBar.LabelX();
            this.labelX7 = new DevComponents.DotNetBar.LabelX();
            this.superTabItem1 = new DevComponents.DotNetBar.SuperTabItem();
            this.superTabControlPanel2 = new DevComponents.DotNetBar.SuperTabControlPanel();
            this.buttonX18 = new DevComponents.DotNetBar.ButtonX();
            this.buttonX19 = new DevComponents.DotNetBar.ButtonX();
            this.dtgGeneral = new DevComponents.DotNetBar.Controls.DataGridViewX();
            this.ROW_ID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.VISUAL_BD = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.VISUAL_TIPO = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.VISUAL_ENTITY_ID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.VISUAL_SITE_ID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.superTabItem2 = new DevComponents.DotNetBar.SuperTabItem();
            this.superTabControlPanel4 = new DevComponents.DotNetBar.SuperTabControlPanel();
            this.buttonX12 = new DevComponents.DotNetBar.ButtonX();
            this.buttonX13 = new DevComponents.DotNetBar.ButtonX();
            this.dtgMonedas = new DevComponents.DotNetBar.Controls.DataGridViewX();
            this.ROW_ID_mnd = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.VISUAL_MONEDA = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.MONEDA = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.superTabItem4 = new DevComponents.DotNetBar.SuperTabItem();
            this.superTabControlPanel3 = new DevComponents.DotNetBar.SuperTabControlPanel();
            this.buttonX9 = new DevComponents.DotNetBar.ButtonX();
            this.buttonX10 = new DevComponents.DotNetBar.ButtonX();
            this.dtgDescripciones = new DevComponents.DotNetBar.Controls.DataGridViewX();
            this.ROW_ID_tr = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.VISUAL_DESCRIPCION = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.DESCRIPCION = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.superTabItem3 = new DevComponents.DotNetBar.SuperTabItem();
            this.RFC = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.backgroundWorker1 = new System.ComponentModel.BackgroundWorker();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            buttonX3 = new DevComponents.DotNetBar.ButtonX();
            buttonX1 = new DevComponents.DotNetBar.ButtonX();
            buttonX2 = new DevComponents.DotNetBar.ButtonX();
            buttonX17 = new DevComponents.DotNetBar.ButtonX();
            buttonX4 = new DevComponents.DotNetBar.ButtonX();
            buttonX7 = new DevComponents.DotNetBar.ButtonX();
            buttonX8 = new DevComponents.DotNetBar.ButtonX();
            buttonX5 = new DevComponents.DotNetBar.ButtonX();
            buttonX6 = new DevComponents.DotNetBar.ButtonX();
            buttonX11 = new DevComponents.DotNetBar.ButtonX();
            ((System.ComponentModel.ISupportInitialize)(this.superTabControl1)).BeginInit();
            this.superTabControl1.SuspendLayout();
            this.superTabControlPanel1.SuspendLayout();
            this.superTabControlPanel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dtgGeneral)).BeginInit();
            this.superTabControlPanel4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dtgMonedas)).BeginInit();
            this.superTabControlPanel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dtgDescripciones)).BeginInit();
            this.SuspendLayout();
            // 
            // buttonX3
            // 
            buttonX3.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            buttonX3.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            buttonX3.Image = global::Desarrollo.Properties.Resources.Eliminar_18;
            buttonX3.Location = new System.Drawing.Point(141, 0);
            buttonX3.Name = "buttonX3";
            buttonX3.Size = new System.Drawing.Size(68, 24);
            buttonX3.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            buttonX3.TabIndex = 2;
            buttonX3.Text = "Eliminar";
            buttonX3.TextAlignment = DevComponents.DotNetBar.eButtonTextAlignment.Right;
            buttonX3.Click += new System.EventHandler(this.buttonX3_Click);
            // 
            // buttonX1
            // 
            buttonX1.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            buttonX1.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            buttonX1.Image = global::Desarrollo.Properties.Resources.Nueno_18;
            buttonX1.Location = new System.Drawing.Point(67, 0);
            buttonX1.Name = "buttonX1";
            buttonX1.Size = new System.Drawing.Size(68, 24);
            buttonX1.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            buttonX1.TabIndex = 1;
            buttonX1.Text = "Guardar";
            buttonX1.TextAlignment = DevComponents.DotNetBar.eButtonTextAlignment.Right;
            buttonX1.Click += new System.EventHandler(this.buttonX1_Click);
            // 
            // buttonX2
            // 
            buttonX2.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            buttonX2.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            buttonX2.Image = global::Desarrollo.Properties.Resources.Nuevo_18_19;
            buttonX2.Location = new System.Drawing.Point(0, 0);
            buttonX2.Name = "buttonX2";
            buttonX2.Size = new System.Drawing.Size(61, 24);
            buttonX2.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            buttonX2.TabIndex = 0;
            buttonX2.Text = "Nuevo";
            buttonX2.TextAlignment = DevComponents.DotNetBar.eButtonTextAlignment.Right;
            buttonX2.Click += new System.EventHandler(this.buttonX2_Click);
            // 
            // buttonX17
            // 
            buttonX17.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            buttonX17.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            buttonX17.Image = global::Desarrollo.Properties.Resources.Eliminar_18;
            buttonX17.Location = new System.Drawing.Point(167, 2);
            buttonX17.Name = "buttonX17";
            buttonX17.Size = new System.Drawing.Size(68, 24);
            buttonX17.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            buttonX17.TabIndex = 587;
            buttonX17.Text = "Eliminar";
            buttonX17.TextAlignment = DevComponents.DotNetBar.eButtonTextAlignment.Right;
            buttonX17.Click += new System.EventHandler(this.buttonX17_Click);
            // 
            // buttonX4
            // 
            buttonX4.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            buttonX4.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            buttonX4.Image = global::Desarrollo.Properties.Resources.opened_folder;
            buttonX4.Location = new System.Drawing.Point(50, 31);
            buttonX4.Name = "buttonX4";
            buttonX4.Size = new System.Drawing.Size(95, 22);
            buttonX4.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            buttonX4.TabIndex = 0;
            buttonX4.Text = "Certificado";
            buttonX4.Click += new System.EventHandler(this.buttonX4_Click_1);
            // 
            // buttonX7
            // 
            buttonX7.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            buttonX7.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            buttonX7.Image = global::Desarrollo.Properties.Resources.opened_folder;
            buttonX7.Location = new System.Drawing.Point(50, 59);
            buttonX7.Name = "buttonX7";
            buttonX7.Size = new System.Drawing.Size(95, 22);
            buttonX7.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            buttonX7.TabIndex = 2;
            buttonX7.Text = "Llave";
            buttonX7.Click += new System.EventHandler(this.buttonX7_Click);
            // 
            // buttonX8
            // 
            buttonX8.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            buttonX8.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            buttonX8.Image = global::Desarrollo.Properties.Resources.Eliminar_18;
            buttonX8.Location = new System.Drawing.Point(167, 2);
            buttonX8.Name = "buttonX8";
            buttonX8.Size = new System.Drawing.Size(68, 24);
            buttonX8.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            buttonX8.TabIndex = 591;
            buttonX8.Text = "Eliminar";
            buttonX8.TextAlignment = DevComponents.DotNetBar.eButtonTextAlignment.Right;
            buttonX8.Click += new System.EventHandler(this.buttonX8_Click);
            // 
            // buttonX5
            // 
            buttonX5.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            buttonX5.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            buttonX5.Image = global::Desarrollo.Properties.Resources.opened_folder;
            buttonX5.Location = new System.Drawing.Point(5, 306);
            buttonX5.Name = "buttonX5";
            buttonX5.Size = new System.Drawing.Size(142, 22);
            buttonX5.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            buttonX5.TabIndex = 6;
            buttonX5.Text = "Directorio de Archivos";
            buttonX5.TextAlignment = DevComponents.DotNetBar.eButtonTextAlignment.Right;
            buttonX5.Click += new System.EventHandler(this.buttonX5_Click);
            // 
            // buttonX6
            // 
            buttonX6.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            buttonX6.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            buttonX6.Image = global::Desarrollo.Properties.Resources.opened_folder;
            buttonX6.Location = new System.Drawing.Point(5, 250);
            buttonX6.Name = "buttonX6";
            buttonX6.Size = new System.Drawing.Size(142, 22);
            buttonX6.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            buttonX6.TabIndex = 7;
            buttonX6.Text = "Directorio de Archivos";
            buttonX6.TextAlignment = DevComponents.DotNetBar.eButtonTextAlignment.Right;
            buttonX6.Click += new System.EventHandler(this.buttonX6_Click);
            // 
            // buttonX11
            // 
            buttonX11.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            buttonX11.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            buttonX11.Image = global::Desarrollo.Properties.Resources.Eliminar_18;
            buttonX11.Location = new System.Drawing.Point(167, 2);
            buttonX11.Name = "buttonX11";
            buttonX11.Size = new System.Drawing.Size(68, 24);
            buttonX11.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            buttonX11.TabIndex = 595;
            buttonX11.Text = "Eliminar";
            buttonX11.TextAlignment = DevComponents.DotNetBar.eButtonTextAlignment.Right;
            buttonX11.Click += new System.EventHandler(this.buttonX11_Click);
            // 
            // metroStatusBar1
            // 
            this.metroStatusBar1.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.metroStatusBar1.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.metroStatusBar1.ContainerControlProcessDialogKey = true;
            this.metroStatusBar1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.metroStatusBar1.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.metroStatusBar1.ForeColor = System.Drawing.Color.Black;
            this.metroStatusBar1.Items.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.labelItem1,
            this.toolStripStatusLabel1});
            this.metroStatusBar1.Location = new System.Drawing.Point(0, 539);
            this.metroStatusBar1.Name = "metroStatusBar1";
            this.metroStatusBar1.Size = new System.Drawing.Size(844, 21);
            this.metroStatusBar1.TabIndex = 8;
            // 
            // labelItem1
            // 
            this.labelItem1.Name = "labelItem1";
            // 
            // toolStripStatusLabel1
            // 
            this.toolStripStatusLabel1.Name = "toolStripStatusLabel1";
            // 
            // ID
            // 
            this.ID.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.ID.Border.Class = "TextBoxBorder";
            this.ID.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.ID.ForeColor = System.Drawing.Color.Black;
            this.ID.Location = new System.Drawing.Point(67, 32);
            this.ID.Name = "ID";
            this.ID.Size = new System.Drawing.Size(582, 22);
            this.ID.TabIndex = 4;
            this.ID.TabStop = false;
            this.ID.WatermarkBehavior = DevComponents.DotNetBar.eWatermarkBehavior.HideNonEmpty;
            // 
            // labelX1
            // 
            this.labelX1.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.labelX1.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX1.ForeColor = System.Drawing.Color.Black;
            this.labelX1.Location = new System.Drawing.Point(0, 30);
            this.labelX1.Name = "labelX1";
            this.labelX1.Size = new System.Drawing.Size(56, 22);
            this.labelX1.TabIndex = 3;
            this.labelX1.Text = "Empresa";
            this.labelX1.TextAlignment = System.Drawing.StringAlignment.Far;
            // 
            // labelX2
            // 
            this.labelX2.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.labelX2.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX2.ForeColor = System.Drawing.Color.Black;
            this.labelX2.Location = new System.Drawing.Point(0, 58);
            this.labelX2.Name = "labelX2";
            this.labelX2.Size = new System.Drawing.Size(56, 22);
            this.labelX2.TabIndex = 5;
            this.labelX2.Text = "RFC";
            this.labelX2.TextAlignment = System.Drawing.StringAlignment.Far;
            // 
            // superTabControl1
            // 
            this.superTabControl1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.superTabControl1.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            // 
            // 
            // 
            this.superTabControl1.ControlBox.CloseBox.Name = "";
            // 
            // 
            // 
            this.superTabControl1.ControlBox.MenuBox.Name = "";
            this.superTabControl1.ControlBox.Name = "";
            this.superTabControl1.ControlBox.SubItems.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.superTabControl1.ControlBox.MenuBox,
            this.superTabControl1.ControlBox.CloseBox});
            this.superTabControl1.Controls.Add(this.superTabControlPanel2);
            this.superTabControl1.Controls.Add(this.superTabControlPanel1);
            this.superTabControl1.Controls.Add(this.superTabControlPanel4);
            this.superTabControl1.Controls.Add(this.superTabControlPanel3);
            this.superTabControl1.ForeColor = System.Drawing.Color.Black;
            this.superTabControl1.Location = new System.Drawing.Point(0, 86);
            this.superTabControl1.Name = "superTabControl1";
            this.superTabControl1.ReorderTabsEnabled = true;
            this.superTabControl1.SelectedTabFont = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Bold);
            this.superTabControl1.SelectedTabIndex = 1;
            this.superTabControl1.Size = new System.Drawing.Size(844, 452);
            this.superTabControl1.TabFont = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.superTabControl1.TabIndex = 7;
            this.superTabControl1.Tabs.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.superTabItem1,
            this.superTabItem2,
            this.superTabItem3,
            this.superTabItem4});
            this.superTabControl1.TabStyle = DevComponents.DotNetBar.eSuperTabStyle.Office2010BackstageBlue;
            this.superTabControl1.Text = "Monedas";
            // 
            // superTabControlPanel1
            // 
            this.superTabControlPanel1.Controls.Add(this.PERIODO);
            this.superTabControlPanel1.Controls.Add(this.labelX8);
            this.superTabControlPanel1.Controls.Add(this.NO_CERTIFICADO);
            this.superTabControlPanel1.Controls.Add(this.ACCOUNT_CUSTOMER);
            this.superTabControlPanel1.Controls.Add(this.labelX6);
            this.superTabControlPanel1.Controls.Add(this.ACCOUNT_VENDOR);
            this.superTabControlPanel1.Controls.Add(this.labelX5);
            this.superTabControlPanel1.Controls.Add(this.labelX4);
            this.superTabControlPanel1.Controls.Add(this.PASSWORD);
            this.superTabControlPanel1.Controls.Add(buttonX7);
            this.superTabControlPanel1.Controls.Add(this.LLAVE);
            this.superTabControlPanel1.Controls.Add(buttonX4);
            this.superTabControlPanel1.Controls.Add(this.CERTIFICADO);
            this.superTabControlPanel1.Controls.Add(this.labelX3);
            this.superTabControlPanel1.Controls.Add(this.NIVEL);
            this.superTabControlPanel1.Controls.Add(buttonX6);
            this.superTabControlPanel1.Controls.Add(buttonX5);
            this.superTabControlPanel1.Controls.Add(this.DIRECTORIO_XML_PROVEEDORES);
            this.superTabControlPanel1.Controls.Add(this.DIRECTORIO_XML_CLIENTES);
            this.superTabControlPanel1.Controls.Add(this.labelX10);
            this.superTabControlPanel1.Controls.Add(this.labelX7);
            this.superTabControlPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.superTabControlPanel1.Location = new System.Drawing.Point(0, 25);
            this.superTabControlPanel1.Name = "superTabControlPanel1";
            this.superTabControlPanel1.Size = new System.Drawing.Size(844, 427);
            this.superTabControlPanel1.TabIndex = 1;
            this.superTabControlPanel1.TabItem = this.superTabItem1;
            // 
            // PERIODO
            // 
            this.PERIODO.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.PERIODO.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.PERIODO.ForeColor = System.Drawing.Color.Black;
            this.PERIODO.Location = new System.Drawing.Point(215, 6);
            this.PERIODO.Name = "PERIODO";
            this.PERIODO.Size = new System.Drawing.Size(434, 18);
            this.PERIODO.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.PERIODO.TabIndex = 583;
            this.PERIODO.Text = "Usar periodos de Visual configurados (Ej: Si el periodo es del 4-1-2015 al 28-1-2" +
    "015)";
            // 
            // labelX8
            // 
            this.labelX8.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.labelX8.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX8.ForeColor = System.Drawing.Color.Black;
            this.labelX8.Location = new System.Drawing.Point(285, 87);
            this.labelX8.Name = "labelX8";
            this.labelX8.Size = new System.Drawing.Size(91, 22);
            this.labelX8.TabIndex = 14;
            this.labelX8.Text = "No. Certificado";
            this.labelX8.TextAlignment = System.Drawing.StringAlignment.Far;
            // 
            // NO_CERTIFICADO
            // 
            this.NO_CERTIFICADO.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.NO_CERTIFICADO.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.NO_CERTIFICADO.Border.Class = "TextBoxBorder";
            this.NO_CERTIFICADO.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.NO_CERTIFICADO.Enabled = false;
            this.NO_CERTIFICADO.ForeColor = System.Drawing.Color.Black;
            this.NO_CERTIFICADO.Location = new System.Drawing.Point(382, 87);
            this.NO_CERTIFICADO.Name = "NO_CERTIFICADO";
            this.NO_CERTIFICADO.Size = new System.Drawing.Size(456, 22);
            this.NO_CERTIFICADO.TabIndex = 15;
            this.NO_CERTIFICADO.TabStop = false;
            this.NO_CERTIFICADO.WatermarkBehavior = DevComponents.DotNetBar.eWatermarkBehavior.HideNonEmpty;
            // 
            // ACCOUNT_CUSTOMER
            // 
            this.ACCOUNT_CUSTOMER.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.ACCOUNT_CUSTOMER.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.ACCOUNT_CUSTOMER.Border.Class = "TextBoxBorder";
            this.ACCOUNT_CUSTOMER.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.ACCOUNT_CUSTOMER.ForeColor = System.Drawing.Color.Black;
            this.ACCOUNT_CUSTOMER.Location = new System.Drawing.Point(3, 196);
            this.ACCOUNT_CUSTOMER.Name = "ACCOUNT_CUSTOMER";
            this.ACCOUNT_CUSTOMER.Size = new System.Drawing.Size(835, 22);
            this.ACCOUNT_CUSTOMER.TabIndex = 13;
            this.ACCOUNT_CUSTOMER.TabStop = false;
            this.ACCOUNT_CUSTOMER.UseSystemPasswordChar = true;
            this.ACCOUNT_CUSTOMER.WatermarkBehavior = DevComponents.DotNetBar.eWatermarkBehavior.HideNonEmpty;
            // 
            // labelX6
            // 
            this.labelX6.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.labelX6.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX6.ForeColor = System.Drawing.Color.Black;
            this.labelX6.Location = new System.Drawing.Point(5, 168);
            this.labelX6.Name = "labelX6";
            this.labelX6.Size = new System.Drawing.Size(455, 22);
            this.labelX6.TabIndex = 12;
            this.labelX6.Text = "Cuentas contables de Clientes (separadas por coma(,) Ejemplo 1000-1000,1000-1002)" +
    "";
            // 
            // ACCOUNT_VENDOR
            // 
            this.ACCOUNT_VENDOR.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.ACCOUNT_VENDOR.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.ACCOUNT_VENDOR.Border.Class = "TextBoxBorder";
            this.ACCOUNT_VENDOR.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.ACCOUNT_VENDOR.ForeColor = System.Drawing.Color.Black;
            this.ACCOUNT_VENDOR.Location = new System.Drawing.Point(3, 139);
            this.ACCOUNT_VENDOR.Name = "ACCOUNT_VENDOR";
            this.ACCOUNT_VENDOR.Size = new System.Drawing.Size(835, 22);
            this.ACCOUNT_VENDOR.TabIndex = 11;
            this.ACCOUNT_VENDOR.TabStop = false;
            this.ACCOUNT_VENDOR.UseSystemPasswordChar = true;
            this.ACCOUNT_VENDOR.WatermarkBehavior = DevComponents.DotNetBar.eWatermarkBehavior.HideNonEmpty;
            // 
            // labelX5
            // 
            this.labelX5.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.labelX5.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX5.ForeColor = System.Drawing.Color.Black;
            this.labelX5.Location = new System.Drawing.Point(5, 111);
            this.labelX5.Name = "labelX5";
            this.labelX5.Size = new System.Drawing.Size(455, 22);
            this.labelX5.TabIndex = 10;
            this.labelX5.Text = "Cuentas contables de Proveedores (separadas por coma(,) Ejemplo 1000-1000,1000-10" +
    "02)";
            // 
            // labelX4
            // 
            this.labelX4.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.labelX4.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX4.ForeColor = System.Drawing.Color.Black;
            this.labelX4.Location = new System.Drawing.Point(3, 87);
            this.labelX4.Name = "labelX4";
            this.labelX4.Size = new System.Drawing.Size(142, 22);
            this.labelX4.TabIndex = 4;
            this.labelX4.Text = "Contraseña";
            this.labelX4.TextAlignment = System.Drawing.StringAlignment.Far;
            // 
            // PASSWORD
            // 
            this.PASSWORD.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.PASSWORD.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.PASSWORD.Border.Class = "TextBoxBorder";
            this.PASSWORD.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.PASSWORD.ForeColor = System.Drawing.Color.Black;
            this.PASSWORD.Location = new System.Drawing.Point(151, 87);
            this.PASSWORD.Name = "PASSWORD";
            this.PASSWORD.Size = new System.Drawing.Size(128, 22);
            this.PASSWORD.TabIndex = 5;
            this.PASSWORD.TabStop = false;
            this.PASSWORD.UseSystemPasswordChar = true;
            this.PASSWORD.WatermarkBehavior = DevComponents.DotNetBar.eWatermarkBehavior.HideNonEmpty;
            // 
            // LLAVE
            // 
            this.LLAVE.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.LLAVE.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.LLAVE.Border.Class = "TextBoxBorder";
            this.LLAVE.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.LLAVE.Enabled = false;
            this.LLAVE.ForeColor = System.Drawing.Color.Black;
            this.LLAVE.Location = new System.Drawing.Point(151, 59);
            this.LLAVE.Name = "LLAVE";
            this.LLAVE.Size = new System.Drawing.Size(687, 22);
            this.LLAVE.TabIndex = 3;
            this.LLAVE.TabStop = false;
            this.LLAVE.WatermarkBehavior = DevComponents.DotNetBar.eWatermarkBehavior.HideNonEmpty;
            // 
            // CERTIFICADO
            // 
            this.CERTIFICADO.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.CERTIFICADO.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.CERTIFICADO.Border.Class = "TextBoxBorder";
            this.CERTIFICADO.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.CERTIFICADO.Enabled = false;
            this.CERTIFICADO.ForeColor = System.Drawing.Color.Black;
            this.CERTIFICADO.Location = new System.Drawing.Point(151, 31);
            this.CERTIFICADO.Name = "CERTIFICADO";
            this.CERTIFICADO.Size = new System.Drawing.Size(687, 22);
            this.CERTIFICADO.TabIndex = 1;
            this.CERTIFICADO.TabStop = false;
            this.CERTIFICADO.WatermarkBehavior = DevComponents.DotNetBar.eWatermarkBehavior.HideNonEmpty;
            // 
            // labelX3
            // 
            this.labelX3.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.labelX3.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX3.ForeColor = System.Drawing.Color.Black;
            this.labelX3.Location = new System.Drawing.Point(3, 3);
            this.labelX3.Name = "labelX3";
            this.labelX3.Size = new System.Drawing.Size(142, 22);
            this.labelX3.TabIndex = 9;
            this.labelX3.Text = "Nivel de cuenta a declarar";
            this.labelX3.TextAlignment = System.Drawing.StringAlignment.Far;
            // 
            // NIVEL
            // 
            this.NIVEL.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.NIVEL.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.NIVEL.Border.Class = "TextBoxBorder";
            this.NIVEL.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.NIVEL.ForeColor = System.Drawing.Color.Black;
            this.NIVEL.Location = new System.Drawing.Point(151, 3);
            this.NIVEL.Name = "NIVEL";
            this.NIVEL.Size = new System.Drawing.Size(58, 22);
            this.NIVEL.TabIndex = 8;
            this.NIVEL.TabStop = false;
            this.NIVEL.WatermarkBehavior = DevComponents.DotNetBar.eWatermarkBehavior.HideNonEmpty;
            // 
            // DIRECTORIO_XML_PROVEEDORES
            // 
            this.DIRECTORIO_XML_PROVEEDORES.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.DIRECTORIO_XML_PROVEEDORES.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.DIRECTORIO_XML_PROVEEDORES.Border.Class = "TextBoxBorder";
            this.DIRECTORIO_XML_PROVEEDORES.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.DIRECTORIO_XML_PROVEEDORES.ForeColor = System.Drawing.Color.Black;
            this.DIRECTORIO_XML_PROVEEDORES.Location = new System.Drawing.Point(153, 306);
            this.DIRECTORIO_XML_PROVEEDORES.Name = "DIRECTORIO_XML_PROVEEDORES";
            this.DIRECTORIO_XML_PROVEEDORES.Size = new System.Drawing.Size(685, 22);
            this.DIRECTORIO_XML_PROVEEDORES.TabIndex = 5;
            this.DIRECTORIO_XML_PROVEEDORES.TabStop = false;
            this.DIRECTORIO_XML_PROVEEDORES.WatermarkBehavior = DevComponents.DotNetBar.eWatermarkBehavior.HideNonEmpty;
            // 
            // DIRECTORIO_XML_CLIENTES
            // 
            this.DIRECTORIO_XML_CLIENTES.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.DIRECTORIO_XML_CLIENTES.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.DIRECTORIO_XML_CLIENTES.Border.Class = "TextBoxBorder";
            this.DIRECTORIO_XML_CLIENTES.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.DIRECTORIO_XML_CLIENTES.ForeColor = System.Drawing.Color.Black;
            this.DIRECTORIO_XML_CLIENTES.Location = new System.Drawing.Point(153, 250);
            this.DIRECTORIO_XML_CLIENTES.Name = "DIRECTORIO_XML_CLIENTES";
            this.DIRECTORIO_XML_CLIENTES.Size = new System.Drawing.Size(685, 22);
            this.DIRECTORIO_XML_CLIENTES.TabIndex = 2;
            this.DIRECTORIO_XML_CLIENTES.TabStop = false;
            this.DIRECTORIO_XML_CLIENTES.WatermarkBehavior = DevComponents.DotNetBar.eWatermarkBehavior.HideNonEmpty;
            // 
            // labelX10
            // 
            this.labelX10.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.labelX10.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX10.ForeColor = System.Drawing.Color.Black;
            this.labelX10.Location = new System.Drawing.Point(3, 278);
            this.labelX10.Name = "labelX10";
            this.labelX10.Size = new System.Drawing.Size(218, 22);
            this.labelX10.TabIndex = 3;
            this.labelX10.Text = "Proveedores Busqueda de los archivos XML";
            // 
            // labelX7
            // 
            this.labelX7.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.labelX7.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX7.ForeColor = System.Drawing.Color.Black;
            this.labelX7.Location = new System.Drawing.Point(5, 224);
            this.labelX7.Name = "labelX7";
            this.labelX7.Size = new System.Drawing.Size(218, 22);
            this.labelX7.TabIndex = 0;
            this.labelX7.Text = "Clientes Busqueda de los archivos XML";
            // 
            // superTabItem1
            // 
            this.superTabItem1.AttachedControl = this.superTabControlPanel1;
            this.superTabItem1.GlobalItem = false;
            this.superTabItem1.Name = "superTabItem1";
            this.superTabItem1.Text = "Configuración General";
            // 
            // superTabControlPanel2
            // 
            this.superTabControlPanel2.Controls.Add(buttonX17);
            this.superTabControlPanel2.Controls.Add(this.buttonX18);
            this.superTabControlPanel2.Controls.Add(this.buttonX19);
            this.superTabControlPanel2.Controls.Add(this.dtgGeneral);
            this.superTabControlPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.superTabControlPanel2.Location = new System.Drawing.Point(0, 25);
            this.superTabControlPanel2.Name = "superTabControlPanel2";
            this.superTabControlPanel2.Size = new System.Drawing.Size(844, 427);
            this.superTabControlPanel2.TabIndex = 0;
            this.superTabControlPanel2.TabItem = this.superTabItem2;
            // 
            // buttonX18
            // 
            this.buttonX18.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.buttonX18.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.buttonX18.Image = global::Desarrollo.Properties.Resources.actualizar_18;
            this.buttonX18.Location = new System.Drawing.Point(3, 2);
            this.buttonX18.Name = "buttonX18";
            this.buttonX18.Size = new System.Drawing.Size(78, 24);
            this.buttonX18.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.buttonX18.TabIndex = 585;
            this.buttonX18.Text = "Refrescar";
            this.buttonX18.Click += new System.EventHandler(this.buttonX18_Click);
            // 
            // buttonX19
            // 
            this.buttonX19.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.buttonX19.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.buttonX19.Image = global::Desarrollo.Properties.Resources.Nueno_18;
            this.buttonX19.Location = new System.Drawing.Point(87, 2);
            this.buttonX19.Name = "buttonX19";
            this.buttonX19.Size = new System.Drawing.Size(74, 24);
            this.buttonX19.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.buttonX19.TabIndex = 586;
            this.buttonX19.Text = "Agregar";
            this.buttonX19.Click += new System.EventHandler(this.buttonX19_Click);
            // 
            // dtgGeneral
            // 
            this.dtgGeneral.AllowUserToAddRows = false;
            this.dtgGeneral.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dtgGeneral.BackgroundColor = System.Drawing.Color.White;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dtgGeneral.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.dtgGeneral.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dtgGeneral.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.ROW_ID,
            this.VISUAL_BD,
            this.VISUAL_TIPO,
            this.VISUAL_ENTITY_ID,
            this.VISUAL_SITE_ID});
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dtgGeneral.DefaultCellStyle = dataGridViewCellStyle2;
            this.dtgGeneral.EnableHeadersVisualStyles = false;
            this.dtgGeneral.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(170)))), ((int)(((byte)(170)))), ((int)(((byte)(170)))));
            this.dtgGeneral.Location = new System.Drawing.Point(3, 28);
            this.dtgGeneral.Name = "dtgGeneral";
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle3.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dtgGeneral.RowHeadersDefaultCellStyle = dataGridViewCellStyle3;
            this.dtgGeneral.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dtgGeneral.Size = new System.Drawing.Size(838, 396);
            this.dtgGeneral.TabIndex = 584;
            this.dtgGeneral.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dtgGeneral_CellDoubleClick);
            // 
            // ROW_ID
            // 
            this.ROW_ID.HeaderText = "ROW_ID";
            this.ROW_ID.Name = "ROW_ID";
            this.ROW_ID.Visible = false;
            // 
            // VISUAL_BD
            // 
            this.VISUAL_BD.HeaderText = "Base de Datos";
            this.VISUAL_BD.Name = "VISUAL_BD";
            this.VISUAL_BD.ReadOnly = true;
            this.VISUAL_BD.Width = 150;
            // 
            // VISUAL_TIPO
            // 
            this.VISUAL_TIPO.HeaderText = "Tipo";
            this.VISUAL_TIPO.Name = "VISUAL_TIPO";
            this.VISUAL_TIPO.ReadOnly = true;
            // 
            // VISUAL_ENTITY_ID
            // 
            this.VISUAL_ENTITY_ID.HeaderText = "Entidad";
            this.VISUAL_ENTITY_ID.Name = "VISUAL_ENTITY_ID";
            this.VISUAL_ENTITY_ID.ReadOnly = true;
            // 
            // VISUAL_SITE_ID
            // 
            this.VISUAL_SITE_ID.HeaderText = "Site";
            this.VISUAL_SITE_ID.Name = "VISUAL_SITE_ID";
            this.VISUAL_SITE_ID.ReadOnly = true;
            // 
            // superTabItem2
            // 
            this.superTabItem2.AttachedControl = this.superTabControlPanel2;
            this.superTabItem2.GlobalItem = false;
            this.superTabItem2.Name = "superTabItem2";
            this.superTabItem2.Text = "Conexiones a Base de Datos ";
            // 
            // superTabControlPanel4
            // 
            this.superTabControlPanel4.Controls.Add(buttonX11);
            this.superTabControlPanel4.Controls.Add(this.buttonX12);
            this.superTabControlPanel4.Controls.Add(this.buttonX13);
            this.superTabControlPanel4.Controls.Add(this.dtgMonedas);
            this.superTabControlPanel4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.superTabControlPanel4.Location = new System.Drawing.Point(0, 0);
            this.superTabControlPanel4.Name = "superTabControlPanel4";
            this.superTabControlPanel4.Size = new System.Drawing.Size(844, 452);
            this.superTabControlPanel4.TabIndex = 0;
            this.superTabControlPanel4.TabItem = this.superTabItem4;
            // 
            // buttonX12
            // 
            this.buttonX12.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.buttonX12.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.buttonX12.Image = global::Desarrollo.Properties.Resources.actualizar_18;
            this.buttonX12.Location = new System.Drawing.Point(3, 2);
            this.buttonX12.Name = "buttonX12";
            this.buttonX12.Size = new System.Drawing.Size(78, 24);
            this.buttonX12.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.buttonX12.TabIndex = 593;
            this.buttonX12.Text = "Refrescar";
            this.buttonX12.Click += new System.EventHandler(this.buttonX12_Click);
            // 
            // buttonX13
            // 
            this.buttonX13.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.buttonX13.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.buttonX13.Image = global::Desarrollo.Properties.Resources.Nueno_18;
            this.buttonX13.Location = new System.Drawing.Point(87, 2);
            this.buttonX13.Name = "buttonX13";
            this.buttonX13.Size = new System.Drawing.Size(74, 24);
            this.buttonX13.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.buttonX13.TabIndex = 594;
            this.buttonX13.Text = "Agregar";
            this.buttonX13.Click += new System.EventHandler(this.buttonX13_Click);
            // 
            // dtgMonedas
            // 
            this.dtgMonedas.AllowUserToAddRows = false;
            this.dtgMonedas.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dtgMonedas.BackgroundColor = System.Drawing.Color.White;
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle4.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle4.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle4.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle4.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle4.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dtgMonedas.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle4;
            this.dtgMonedas.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dtgMonedas.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.ROW_ID_mnd,
            this.VISUAL_MONEDA,
            this.MONEDA});
            dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle5.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle5.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle5.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle5.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle5.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle5.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dtgMonedas.DefaultCellStyle = dataGridViewCellStyle5;
            this.dtgMonedas.EnableHeadersVisualStyles = false;
            this.dtgMonedas.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(170)))), ((int)(((byte)(170)))), ((int)(((byte)(170)))));
            this.dtgMonedas.Location = new System.Drawing.Point(3, 28);
            this.dtgMonedas.Name = "dtgMonedas";
            dataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle6.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle6.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle6.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle6.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle6.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle6.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dtgMonedas.RowHeadersDefaultCellStyle = dataGridViewCellStyle6;
            this.dtgMonedas.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dtgMonedas.Size = new System.Drawing.Size(838, 421);
            this.dtgMonedas.TabIndex = 592;
            // 
            // ROW_ID_mnd
            // 
            this.ROW_ID_mnd.HeaderText = "ROW_ID";
            this.ROW_ID_mnd.Name = "ROW_ID_mnd";
            this.ROW_ID_mnd.Visible = false;
            // 
            // VISUAL_MONEDA
            // 
            this.VISUAL_MONEDA.HeaderText = "Moneda de Visual";
            this.VISUAL_MONEDA.Name = "VISUAL_MONEDA";
            this.VISUAL_MONEDA.ReadOnly = true;
            this.VISUAL_MONEDA.Width = 150;
            // 
            // MONEDA
            // 
            this.MONEDA.HeaderText = "Moneda SAT";
            this.MONEDA.Name = "MONEDA";
            this.MONEDA.ReadOnly = true;
            // 
            // superTabItem4
            // 
            this.superTabItem4.AttachedControl = this.superTabControlPanel4;
            this.superTabItem4.GlobalItem = false;
            this.superTabItem4.Name = "superTabItem4";
            this.superTabItem4.Text = "Monedas";
            // 
            // superTabControlPanel3
            // 
            this.superTabControlPanel3.Controls.Add(buttonX8);
            this.superTabControlPanel3.Controls.Add(this.buttonX9);
            this.superTabControlPanel3.Controls.Add(this.buttonX10);
            this.superTabControlPanel3.Controls.Add(this.dtgDescripciones);
            this.superTabControlPanel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.superTabControlPanel3.Location = new System.Drawing.Point(0, 0);
            this.superTabControlPanel3.Name = "superTabControlPanel3";
            this.superTabControlPanel3.Size = new System.Drawing.Size(844, 452);
            this.superTabControlPanel3.TabIndex = 0;
            this.superTabControlPanel3.TabItem = this.superTabItem3;
            // 
            // buttonX9
            // 
            this.buttonX9.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.buttonX9.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.buttonX9.Image = global::Desarrollo.Properties.Resources.actualizar_18;
            this.buttonX9.Location = new System.Drawing.Point(3, 2);
            this.buttonX9.Name = "buttonX9";
            this.buttonX9.Size = new System.Drawing.Size(78, 24);
            this.buttonX9.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.buttonX9.TabIndex = 589;
            this.buttonX9.Text = "Refrescar";
            this.buttonX9.Click += new System.EventHandler(this.buttonX9_Click);
            // 
            // buttonX10
            // 
            this.buttonX10.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.buttonX10.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.buttonX10.Image = global::Desarrollo.Properties.Resources.Nueno_18;
            this.buttonX10.Location = new System.Drawing.Point(87, 2);
            this.buttonX10.Name = "buttonX10";
            this.buttonX10.Size = new System.Drawing.Size(74, 24);
            this.buttonX10.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.buttonX10.TabIndex = 590;
            this.buttonX10.Text = "Agregar";
            this.buttonX10.Click += new System.EventHandler(this.buttonX10_Click);
            // 
            // dtgDescripciones
            // 
            this.dtgDescripciones.AllowUserToAddRows = false;
            this.dtgDescripciones.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dtgDescripciones.BackgroundColor = System.Drawing.Color.White;
            dataGridViewCellStyle7.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle7.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle7.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle7.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle7.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle7.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle7.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dtgDescripciones.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle7;
            this.dtgDescripciones.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dtgDescripciones.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.ROW_ID_tr,
            this.VISUAL_DESCRIPCION,
            this.DESCRIPCION});
            dataGridViewCellStyle8.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle8.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle8.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle8.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle8.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle8.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle8.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dtgDescripciones.DefaultCellStyle = dataGridViewCellStyle8;
            this.dtgDescripciones.EnableHeadersVisualStyles = false;
            this.dtgDescripciones.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(170)))), ((int)(((byte)(170)))), ((int)(((byte)(170)))));
            this.dtgDescripciones.Location = new System.Drawing.Point(3, 28);
            this.dtgDescripciones.Name = "dtgDescripciones";
            dataGridViewCellStyle9.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle9.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle9.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle9.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle9.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle9.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle9.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dtgDescripciones.RowHeadersDefaultCellStyle = dataGridViewCellStyle9;
            this.dtgDescripciones.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dtgDescripciones.Size = new System.Drawing.Size(838, 421);
            this.dtgDescripciones.TabIndex = 588;
            // 
            // ROW_ID_tr
            // 
            this.ROW_ID_tr.HeaderText = "ROW_ID";
            this.ROW_ID_tr.Name = "ROW_ID_tr";
            this.ROW_ID_tr.Visible = false;
            // 
            // VISUAL_DESCRIPCION
            // 
            this.VISUAL_DESCRIPCION.HeaderText = "Descripción de Visual";
            this.VISUAL_DESCRIPCION.Name = "VISUAL_DESCRIPCION";
            this.VISUAL_DESCRIPCION.ReadOnly = true;
            this.VISUAL_DESCRIPCION.Width = 300;
            // 
            // DESCRIPCION
            // 
            this.DESCRIPCION.HeaderText = "Descripción";
            this.DESCRIPCION.Name = "DESCRIPCION";
            this.DESCRIPCION.ReadOnly = true;
            this.DESCRIPCION.Width = 300;
            // 
            // superTabItem3
            // 
            this.superTabItem3.AttachedControl = this.superTabControlPanel3;
            this.superTabItem3.GlobalItem = false;
            this.superTabItem3.Name = "superTabItem3";
            this.superTabItem3.Text = "Conversión de descripciones";
            // 
            // RFC
            // 
            this.RFC.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.RFC.Border.Class = "TextBoxBorder";
            this.RFC.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.RFC.ForeColor = System.Drawing.Color.Black;
            this.RFC.Location = new System.Drawing.Point(67, 58);
            this.RFC.Name = "RFC";
            this.RFC.Size = new System.Drawing.Size(142, 22);
            this.RFC.TabIndex = 6;
            this.RFC.TabStop = false;
            this.RFC.WatermarkBehavior = DevComponents.DotNetBar.eWatermarkBehavior.HideNonEmpty;
            // 
            // backgroundWorker1
            // 
            this.backgroundWorker1.WorkerReportsProgress = true;
            this.backgroundWorker1.WorkerSupportsCancellation = true;
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.FileName = "openFileDialog1";
            // 
            // frmEmpresa
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.BottomLeftCornerSize = 0;
            this.ClientSize = new System.Drawing.Size(844, 560);
            this.Controls.Add(this.RFC);
            this.Controls.Add(this.superTabControl1);
            this.Controls.Add(this.labelX2);
            this.Controls.Add(this.labelX1);
            this.Controls.Add(this.ID);
            this.Controls.Add(buttonX3);
            this.Controls.Add(buttonX1);
            this.Controls.Add(buttonX2);
            this.Controls.Add(this.metroStatusBar1);
            this.Cursor = System.Windows.Forms.Cursors.Default;
            this.DoubleBuffered = true;
            this.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "frmEmpresa";
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Empresa";
            this.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.frmEmpresa_KeyPress);
            ((System.ComponentModel.ISupportInitialize)(this.superTabControl1)).EndInit();
            this.superTabControl1.ResumeLayout(false);
            this.superTabControlPanel1.ResumeLayout(false);
            this.superTabControlPanel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dtgGeneral)).EndInit();
            this.superTabControlPanel4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dtgMonedas)).EndInit();
            this.superTabControlPanel3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dtgDescripciones)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevComponents.DotNetBar.Metro.MetroStatusBar metroStatusBar1;
        private DevComponents.DotNetBar.LabelItem labelItem1;
        private DevComponents.DotNetBar.Controls.TextBoxX ID;
        private DevComponents.DotNetBar.LabelX labelX1;
        private DevComponents.DotNetBar.LabelX labelX2;
        private DevComponents.DotNetBar.SuperTabControl superTabControl1;
        private DevComponents.DotNetBar.SuperTabControlPanel superTabControlPanel1;
        private DevComponents.DotNetBar.SuperTabItem superTabItem1;
        private DevComponents.DotNetBar.SuperTabControlPanel superTabControlPanel2;
        private DevComponents.DotNetBar.SuperTabItem superTabItem2;
        private DevComponents.DotNetBar.LabelItem toolStripStatusLabel1;
        private DevComponents.DotNetBar.Controls.TextBoxX RFC;
        private System.ComponentModel.BackgroundWorker backgroundWorker1;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private DevComponents.DotNetBar.ButtonX buttonX18;
        private DevComponents.DotNetBar.ButtonX buttonX19;
        private DevComponents.DotNetBar.Controls.DataGridViewX dtgGeneral;
        private DevComponents.DotNetBar.LabelX labelX3;
        private DevComponents.DotNetBar.Controls.TextBoxX NIVEL;
        private System.Windows.Forms.DataGridViewTextBoxColumn ROW_ID;
        private System.Windows.Forms.DataGridViewTextBoxColumn VISUAL_BD;
        private System.Windows.Forms.DataGridViewTextBoxColumn VISUAL_TIPO;
        private System.Windows.Forms.DataGridViewTextBoxColumn VISUAL_ENTITY_ID;
        private System.Windows.Forms.DataGridViewTextBoxColumn VISUAL_SITE_ID;
        private DevComponents.DotNetBar.LabelX labelX4;
        private DevComponents.DotNetBar.Controls.TextBoxX PASSWORD;
        private DevComponents.DotNetBar.Controls.TextBoxX LLAVE;
        private DevComponents.DotNetBar.Controls.TextBoxX CERTIFICADO;
        private DevComponents.DotNetBar.SuperTabControlPanel superTabControlPanel3;
        private DevComponents.DotNetBar.ButtonX buttonX9;
        private DevComponents.DotNetBar.ButtonX buttonX10;
        private DevComponents.DotNetBar.Controls.DataGridViewX dtgDescripciones;
        private DevComponents.DotNetBar.SuperTabItem superTabItem3;
        private System.Windows.Forms.DataGridViewTextBoxColumn ROW_ID_tr;
        private System.Windows.Forms.DataGridViewTextBoxColumn VISUAL_DESCRIPCION;
        private System.Windows.Forms.DataGridViewTextBoxColumn DESCRIPCION;
        private DevComponents.DotNetBar.Controls.TextBoxX ACCOUNT_CUSTOMER;
        private DevComponents.DotNetBar.LabelX labelX6;
        private DevComponents.DotNetBar.Controls.TextBoxX ACCOUNT_VENDOR;
        private DevComponents.DotNetBar.LabelX labelX5;
        private DevComponents.DotNetBar.Controls.TextBoxX DIRECTORIO_XML_PROVEEDORES;
        private DevComponents.DotNetBar.Controls.TextBoxX DIRECTORIO_XML_CLIENTES;
        private DevComponents.DotNetBar.LabelX labelX10;
        private DevComponents.DotNetBar.LabelX labelX7;
        private DevComponents.DotNetBar.LabelX labelX8;
        private DevComponents.DotNetBar.Controls.TextBoxX NO_CERTIFICADO;
        private DevComponents.DotNetBar.SuperTabControlPanel superTabControlPanel4;
        private DevComponents.DotNetBar.ButtonX buttonX12;
        private DevComponents.DotNetBar.ButtonX buttonX13;
        private DevComponents.DotNetBar.Controls.DataGridViewX dtgMonedas;
        private DevComponents.DotNetBar.SuperTabItem superTabItem4;
        private System.Windows.Forms.DataGridViewTextBoxColumn ROW_ID_mnd;
        private System.Windows.Forms.DataGridViewTextBoxColumn VISUAL_MONEDA;
        private System.Windows.Forms.DataGridViewTextBoxColumn MONEDA;
        private DevComponents.DotNetBar.Controls.CheckBoxX PERIODO;

    }
}