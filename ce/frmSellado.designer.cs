namespace Desarrollo
{
    partial class frmSellado
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            DevComponents.DotNetBar.ButtonX buttonX3;
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmSellado));
            this.metroStatusBar1 = new DevComponents.DotNetBar.Metro.MetroStatusBar();
            this.labelItem1 = new DevComponents.DotNetBar.LabelItem();
            this.buttonX22 = new DevComponents.DotNetBar.ButtonX();
            this.NumTramite = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.labelX12 = new DevComponents.DotNetBar.LabelX();
            this.labelX1 = new DevComponents.DotNetBar.LabelX();
            buttonX3 = new DevComponents.DotNetBar.ButtonX();
            this.SuspendLayout();
            // 
            // buttonX3
            // 
            buttonX3.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            buttonX3.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            buttonX3.Image = global::Desarrollo.Properties.Resources.Eliminar_18;
            buttonX3.Location = new System.Drawing.Point(114, 0);
            buttonX3.Name = "buttonX3";
            buttonX3.Size = new System.Drawing.Size(68, 24);
            buttonX3.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            buttonX3.TabIndex = 1;
            buttonX3.Text = "Cancelar";
            buttonX3.TextAlignment = DevComponents.DotNetBar.eButtonTextAlignment.Right;
            buttonX3.Click += new System.EventHandler(this.buttonX3_Click);
            // 
            // metroStatusBar1
            // 
            this.metroStatusBar1.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.metroStatusBar1.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.metroStatusBar1.ContainerControlProcessDialogKey = true;
            this.metroStatusBar1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.metroStatusBar1.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.metroStatusBar1.ForeColor = System.Drawing.Color.Black;
            this.metroStatusBar1.Items.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.labelItem1});
            this.metroStatusBar1.Location = new System.Drawing.Point(0, 80);
            this.metroStatusBar1.Name = "metroStatusBar1";
            this.metroStatusBar1.Size = new System.Drawing.Size(419, 21);
            this.metroStatusBar1.TabIndex = 8;
            // 
            // labelItem1
            // 
            this.labelItem1.Name = "labelItem1";
            // 
            // buttonX22
            // 
            this.buttonX22.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.buttonX22.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.buttonX22.Image = global::Desarrollo.Properties.Resources.sat_18;
            this.buttonX22.Location = new System.Drawing.Point(1, 0);
            this.buttonX22.Name = "buttonX22";
            this.buttonX22.Size = new System.Drawing.Size(107, 24);
            this.buttonX22.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.buttonX22.TabIndex = 587;
            this.buttonX22.Text = "Generar Sellado";
            this.buttonX22.Click += new System.EventHandler(this.buttonX22_Click);
            // 
            // NumTramite
            // 
            this.NumTramite.AcceptsTab = true;
            this.NumTramite.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.NumTramite.Border.Class = "TextBoxBorder";
            this.NumTramite.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.NumTramite.ForeColor = System.Drawing.Color.Black;
            this.NumTramite.Location = new System.Drawing.Point(133, 30);
            this.NumTramite.MaxLength = 22;
            this.NumTramite.Name = "NumTramite";
            this.NumTramite.Size = new System.Drawing.Size(286, 22);
            this.NumTramite.TabIndex = 602;
            // 
            // labelX12
            // 
            this.labelX12.AutoSize = true;
            this.labelX12.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.labelX12.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX12.ForeColor = System.Drawing.Color.Black;
            this.labelX12.Location = new System.Drawing.Point(1, 33);
            this.labelX12.Name = "labelX12";
            this.labelX12.Size = new System.Drawing.Size(126, 17);
            this.labelX12.TabIndex = 601;
            this.labelX12.Text = "Folio asignado por el SAT";
            // 
            // labelX1
            // 
            this.labelX1.AutoSize = true;
            this.labelX1.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.labelX1.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX1.ForeColor = System.Drawing.Color.Black;
            this.labelX1.Location = new System.Drawing.Point(0, 56);
            this.labelX1.Name = "labelX1";
            this.labelX1.Size = new System.Drawing.Size(409, 17);
            this.labelX1.TabIndex = 603;
            this.labelX1.Text = "Caracteres del folio asignado por el SAT en recepci�n de los archivos (22 car�cte" +
    "res)";
            // 
            // frmSellado
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.BottomLeftCornerSize = 0;
            this.ClientSize = new System.Drawing.Size(419, 101);
            this.Controls.Add(this.labelX1);
            this.Controls.Add(this.NumTramite);
            this.Controls.Add(this.labelX12);
            this.Controls.Add(this.buttonX22);
            this.Controls.Add(buttonX3);
            this.Controls.Add(this.metroStatusBar1);
            this.Cursor = System.Windows.Forms.Cursors.Default;
            this.DoubleBuffered = true;
            this.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "frmSellado";
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Informaci�n para realizar Sellado";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevComponents.DotNetBar.Metro.MetroStatusBar metroStatusBar1;
        private DevComponents.DotNetBar.LabelItem labelItem1;
        private DevComponents.DotNetBar.ButtonX buttonX22;
        private DevComponents.DotNetBar.Controls.TextBoxX NumTramite;
        private DevComponents.DotNetBar.LabelX labelX12;
        private DevComponents.DotNetBar.LabelX labelX1;

    }
}