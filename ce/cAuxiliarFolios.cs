﻿using CryptoSysPKI;
using DevComponents.DotNetBar;
using Generales;
using Ionic.Zip;
using ModeloDocumentosFX;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Windows.Forms;
using System.Xml;
using System.Xml.Serialization;
using System.Xml.XPath;
using System.Xml.Xsl;

namespace Desarrollo
{
    class cAuxiliarFolios
    {
        private string sSQL = "";
        public string ROW_ID { get; set; }

        public string ROW_ID_CATALOGO { get; set; }

        public string tipoSolicitudField { get; set; }

        public string numOrdenField { get; set; }

        public string numTramiteField { get; set; }

        public string selloField { get; set; }

        public string noCertificadoField { get; set; }

        public string certificadoField { get; set; }

        private cCONEXCION oData;
        public override string ToString() { return ROW_ID; }

        public cAuxiliarFolios()
        {
            ModeloDocumentosFXContainer dbContext = new ModeloDocumentosFXContainer();
            try
            {
                string conection = dbContext.Database.Connection.ConnectionString;
                oData = new cCONEXCION(conection);
            }
            catch (Exception err)
            {
                ErrorFX.mostrar(err, false, true, "frmEmpresa - 96 - ", false);
            }
            limpiar();
        }
        public void limpiar()
        {
            ROW_ID = "";
            ROW_ID_CATALOGO = "";
            tipoSolicitudField = "";
            numOrdenField = "";
            numTramiteField = "";
            selloField = "";
            noCertificadoField = "";
            certificadoField = "";
        }


        public List<cAuxiliarFolios> todos(string ROW_ID_CATALOGOp,string tipoFieldp = "",string ASIGNADOp="")
        {

            List<cAuxiliarFolios> lista = new List<cAuxiliarFolios>();

            sSQL = " SELECT * ";
            sSQL += " FROM AuxiliarFolios ";
            sSQL += " WHERE 1=1 ";
            if(ROW_ID_CATALOGOp!="")
            {
                sSQL += " AND ROW_ID_CATALOGO=" + ROW_ID_CATALOGOp + "";
            }
            
            sSQL += " ORDER BY ROW_ID";
            DataTable oDataTable = oData.EjecutarConsulta(sSQL);
            foreach (DataRow oDataRow in oDataTable.Rows)
            {
                cAuxiliarFolios ocCatalogo = new cAuxiliarFolios();
                ocCatalogo.cargar(oDataRow["ROW_ID"].ToString());
                lista.Add(ocCatalogo);
            }
            return lista;

        }

        public bool verificar(string numFieldp, string ROW_ID_CATALOGOp, bool cargarp=false)
        {
            sSQL = " SELECT * ";
            sSQL += " FROM AuxiliarFolios ";
            sSQL += " WHERE numField='" + numFieldp + "' ";
            sSQL += " AND ROW_ID_CATALOGO=" + ROW_ID_CATALOGOp + "";
            DataTable oDataTable = oData.EjecutarConsulta(sSQL);
            if (oDataTable != null)
            {
                foreach (DataRow oDataRow in oDataTable.Rows)
                {

                    if (!cargarp)
                    {
                        return true;
                    }
                    else
                    {
                        cargar(oDataRow["ROW_ID"].ToString());
                    }
                }
            }
            return false;
        }

        public bool cargar(string ROW_IDp)
        {
            sSQL = " SELECT * ";
            sSQL += " FROM AuxiliarFolios ";
            sSQL += " WHERE ROW_ID=" + ROW_IDp + " ";
            DataTable oDataTable = oData.EjecutarConsulta(sSQL);
            if (oDataTable != null)
            {
                foreach (DataRow oDataRow in oDataTable.Rows)
                {
                    //Añadir el Registro
                    ROW_ID = oDataRow["ROW_ID"].ToString();
                    ROW_ID_CATALOGO = oDataRow["ROW_ID_CATALOGO"].ToString();
                    tipoSolicitudField = oDataRow["tipoSolicitudField"].ToString();
                    numOrdenField = oDataRow["numOrdenField"].ToString();
                    numTramiteField = oDataRow["numTramiteField"].ToString();
                    selloField = oDataRow["selloField"].ToString();
                    noCertificadoField = oDataRow["noCertificadoField"].ToString();
                    certificadoField = oDataRow["certificadoField"].ToString();
                    return true;
                }
            }
            return false;
        }

        public List<cAuxiliarFolios> validar(string ROW_ID_CATALOGOp)
        {

            List<cAuxiliarFolios> lista = new List<cAuxiliarFolios>();

            return lista;

        }
        


        public bool guardar(bool validar=true)
        {

            if (ROW_ID == "")
            {
                sSQL = " INSERT INTO AuxiliarFolios ";
                sSQL += " ( ";
                sSQL += " [ROW_ID_CATALOGO],[tipoSolicitudField],[numOrdenField],[numTramiteField]";
                sSQL += ",[selloField],[noCertificadoField],[certificadoField] ";
                sSQL += " )";
                sSQL += " VALUES ";
                sSQL += "(";
                sSQL += " " + ROW_ID_CATALOGO + ",'" + tipoSolicitudField + "','" + numOrdenField.ToString() + "','" + numTramiteField.ToString() + "'";
                sSQL += ",'" + selloField.ToString() + "','" + noCertificadoField.ToString() + "','" + certificadoField + "' ";
                sSQL += ")";
                DataTable oDataTable = oData.EjecutarConsulta(sSQL);
                if (oDataTable != null)
                {

                    sSQL = "SELECT TOP 1 MAX(ROW_ID) as ROW_ID FROM AuxiliarFolios";
                    oDataTable = oData.EjecutarConsulta(sSQL);
                    if (oDataTable != null)
                    {
                        foreach (DataRow oDataRow in oDataTable.Rows)
                        {
                            ROW_ID = oDataRow["ROW_ID"].ToString();
                        }
                    }
                }

            }
            else
            {
                sSQL = " UPDATE AuxiliarFolios ";
                sSQL += " SET ROW_ID_CATALOGO=" + ROW_ID_CATALOGO + ",tipoSolicitudField='" + tipoSolicitudField + "',numOrdenField='" + numOrdenField.ToString() + "'";
                sSQL += ",numTramiteField='" + numTramiteField.ToString() + "',selloField='" + selloField + "',noCertificadoField='" + noCertificadoField + "' ";
                sSQL += " WHERE ROW_ID=" + ROW_ID;
                oData.EjecutarConsulta(sSQL);

            }
            return true;

        }

        public bool eliminar(string ROW_IDp)
        {
            if (ROW_IDp != "")
            {
                sSQL = "DELETE FROM AuxiliarFolios ";
                sSQL += " WHERE ROW_ID='" + ROW_IDp + "' ";
                oData.EjecutarConsulta(sSQL);
                return true;
            }
            return false;
        }

        public bool generar(string ROW_ID_CATALOGOp)
        {
            FolderBrowserDialog folderBrowserDialog1 = new FolderBrowserDialog();
            if (folderBrowserDialog1.ShowDialog() == DialogResult.OK)
            {
                importar(folderBrowserDialog1, false, ROW_ID_CATALOGOp);
                MessageBoxEx.Show("Exportación de los archivos finalizada en " + folderBrowserDialog1.SelectedPath.ToString(), Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Information);
                return true;
            }
            return false;
        }

        private void importar(FolderBrowserDialog folderBrowserDialog1, bool mostrar_mensaje_finalizacion, string ROW_ID_CATALOGOp)
        {

            if (folderBrowserDialog1.SelectedPath.Trim() == "")
            {
                if (folderBrowserDialog1.ShowDialog() != DialogResult.OK)
                {
                    MessageBoxEx.Show("Generación de archivo cancelada.", Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
                    return;
                }
            }
            cCatalogo ocCatalogo = new cCatalogo();
            ocCatalogo.cargar(ROW_ID_CATALOGOp);
            //string nombre_tmp = path + "\\" + "Polizas_" + DateTime.Now.ToString("ddMMyyyyhhmmss");
            //string nombre_tmp = folderBrowserDialog1.SelectedPath + "\\" + "XF" + ocCatalogo.rFCField + ocCatalogo.anoField + ocCatalogo.mesField + ".xml";
            string mes = ocCatalogo.mesField;
            if (mes.Length == 1)
            {
                mes = "0" + mes;
            }
            string nombre = ocCatalogo.rFCField + ocCatalogo.anoField + mes + "XF";
            string nombre_tmp = folderBrowserDialog1.SelectedPath + "\\" + nombre + ".xml";// +DateTime.Now.ToString("ddMMyyyyhhmmss");


            //Generar AuxliarCtas
            RepAuxFol oRepAuxFol = new RepAuxFol();
            cAuxiliarFolios ocAuxiliarFolios = new cAuxiliarFolios();

            oRepAuxFol.TipoSolicitud = tipoSolicitudField;
            oRepAuxFol.NumOrden = numOrdenField;
            oRepAuxFol.NumTramite = numTramiteField;
            if (oRepAuxFol.NumOrden=="")
            {
                oRepAuxFol.NumOrden = null;
            }
            if (oRepAuxFol.NumTramite == "")
            {
                oRepAuxFol.NumTramite = null;
            }

            oRepAuxFol.Anio = int.Parse(ocCatalogo.anoField);

            switch (int.Parse(ocCatalogo.mesField))
            {
                case 1:
                    oRepAuxFol.Mes = RepAuxFolMes.Item01;
                    break;
                case 2:
                    oRepAuxFol.Mes = RepAuxFolMes.Item02;
                    break;
                case 3:
                    oRepAuxFol.Mes = RepAuxFolMes.Item03;
                    break;
                case 4:
                    oRepAuxFol.Mes = RepAuxFolMes.Item04;
                    break;
                case 5:
                    oRepAuxFol.Mes = RepAuxFolMes.Item05;
                    break;
                case 6:
                    oRepAuxFol.Mes = RepAuxFolMes.Item06;
                    break;
                case 7:
                    oRepAuxFol.Mes = RepAuxFolMes.Item07;
                    break;
                case 8:
                    oRepAuxFol.Mes = RepAuxFolMes.Item08;
                    break;
                case 9:
                    oRepAuxFol.Mes = RepAuxFolMes.Item09;
                    break;
                case 10:
                    oRepAuxFol.Mes = RepAuxFolMes.Item10;
                    break;
                case 11:
                    oRepAuxFol.Mes = RepAuxFolMes.Item11;
                    break;
                case 12:
                    oRepAuxFol.Mes = RepAuxFolMes.Item12;
                    break;
            }
            oRepAuxFol.RFC = ocCatalogo.rFCField;
            oRepAuxFol.RFC = ocCatalogo.rFCField;
            cEMPRESA_CE ocEMPRESA = new cEMPRESA_CE();
            ocEMPRESA.cargar_rfc(oRepAuxFol.RFC);
            if (ocEMPRESA.NO_CERTIFICADO.Trim().Length == 0)
            {
                MessageBoxEx.Show("No se tiene configurado el Certificado"
                   , Application.ProductName + "-" + Application.ProductVersion, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);

                return;
            }
            oRepAuxFol.noCertificado = ocEMPRESA.NO_CERTIFICADO;
            if (!File.Exists(ocEMPRESA.CERTIFICADO))
            {
                MessageBoxEx.Show("No se tiene acceso al Certificado, archivo " + ocEMPRESA.CERTIFICADO + ""
                    , Application.ProductName + "-" + Application.ProductVersion, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);

                return;
            }
            X509Certificate cert = X509Certificate.CreateFromCertFile(ocEMPRESA.CERTIFICADO);
            string Certificado64 = System.Convert.ToBase64String(cert.GetRawCertData());
            oRepAuxFol.Certificado = Certificado64;


            //Cargar la Balanza de Comprobación
            cPoliza ocPoliza = new cPoliza();
            ocPoliza.cargar(ROW_ID_CATALOGOp);
            List<cPoliza> listaPoliza = new List<cPoliza>();
            listaPoliza = ocPoliza.todos(ROW_ID_CATALOGOp);

            oRepAuxFol.DetAuxFol = new RepAuxFolDetAuxFol[listaPoliza.Count];
            int contador=0;
            foreach (cPoliza registro in listaPoliza)
            {
                oRepAuxFol.DetAuxFol[contador] = new RepAuxFolDetAuxFol();
                oRepAuxFol.DetAuxFol[contador].NumUnIdenPol = registro.numField;
                oRepAuxFol.DetAuxFol[contador].Fecha = registro.fechaField;
                //Verificar Comprobantes Nacionales
                oRepAuxFol.DetAuxFol[contador].ComprNal = cargar_comprobantes_nacionales(ROW_ID_CATALOGO, registro.numField);

                //Verificar Comprobantes Extranjeros
                oRepAuxFol.DetAuxFol[contador].ComprExt = cargar_comprobantes_extrajeros(ROW_ID_CATALOGO, registro.numField);

                contador++;
            }

            crear_archivo(nombre,oRepAuxFol);
            
            oRepAuxFol.Sello = sellar(nombre + ".XML", ocEMPRESA);

            if (oRepAuxFol.Sello == "")
            {
                MessageBoxEx.Show("El sello tiene error de generación.", Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            crear_archivo(nombre, oRepAuxFol);

            //Comprimir
            string ARCHIVO_zip = nombre + ".ZIP";
            using (ZipFile zip = new ZipFile())
            {
                zip.AddFile(nombre + ".XML");
                zip.Save(ARCHIVO_zip);
            }
            File.Delete(nombre + ".XML");
            File.Delete(folderBrowserDialog1.SelectedPath + "\\" + ARCHIVO_zip);
            File.Copy(ARCHIVO_zip, folderBrowserDialog1.SelectedPath + "\\" + ARCHIVO_zip);
            File.Delete(ARCHIVO_zip);
            //Fin de Compresion de Archivo

            if (mostrar_mensaje_finalizacion)
            {
                MessageBoxEx.Show("Exportación de los archivos finalizada.", Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Information);
            }

        }

        private void crear_archivo(string nombre, RepAuxFol oRepAuxFol)
        {
            XmlSerializerNamespaces myNamespaces = new XmlSerializerNamespaces();
            myNamespaces.Add("schemaLocation", "http://www.sat.gob.mx/esquemas/ContabilidadE/1_1/AuxiliarFolios/AuxiliarFolios_1_2.xsd");
            myNamespaces.Add("xsi", "http://www.w3.org/2001/XMLSchema-instance");
            myNamespaces.Add("RepAux", "www.sat.gob.mx/esquemas/ContabilidadE/1_1/AuxiliarFolios");
            myNamespaces.Add("cambiar", "www.sat.gob.mx/esquemas/ContabilidadE/1_1/AuxiliarFolios http://www.sat.gob.mx/esquemas/ContabilidadE/1_1/AuxiliarFolios/AuxiliarFolios_1_2.xsd");

            //Transformar al XML
            TextWriter writer_p = new StreamWriter(nombre + ".XML");
            XmlSerializer serializer_p = new XmlSerializer(typeof(RepAuxFol));
            serializer_p.Serialize(writer_p, oRepAuxFol, myNamespaces);
            writer_p.Close();
            oData.ReplaceInFile(nombre + ".XML", "xmlns:cambiar", "xsi:schemaLocation");

        }
        private string sellar(string archivo, cEMPRESA_CE oEMPRESAp)
        {
            string sDirectory = AppDomain.CurrentDomain.BaseDirectory;
            XPathDocument myXPathDoc = new XPathDocument(archivo);
            //XslTransform myXslTrans = new XslTransform();
            XslCompiledTransform myXslTrans = new XslCompiledTransform();


            XslCompiledTransform trans = new XslCompiledTransform();
            XmlUrlResolver resolver = new XmlUrlResolver();

            resolver.Credentials = System.Net.CredentialCache.DefaultCredentials;
            XsltSettings settings = new XsltSettings(true, true);

            //Cargar xslt
            try
            {

                myXslTrans.Load(sDirectory + "/xslt/AuxiliarFolios_1_1.xslt");
            }
            catch (XmlException errores)
            {
                MessageBoxEx.Show("Error en " + errores.InnerException.ToString());
                return "";
            }
            //Crear Archivo Temporal
            string nombre_tmp = "TMP" + DateTime.Now.ToString("ddMMyyyyhhmmss");
            XmlTextWriter myWriter = new XmlTextWriter(nombre_tmp + ".TXT", null);

            //Transformar al XML
            myXslTrans.Transform(myXPathDoc, null, myWriter);

            myWriter.Close();

            //Abrir Archivo TXT
            string cadena_original = "";
            StreamReader re = File.OpenText(nombre_tmp + ".TXT");
            string input = null;
            while ((input = re.ReadLine()) != null)
            {
                cadena_original += input;
            }
            re.Close();
            //Buscar el &amp; en la Cadena y sustituirlo por &
            cadena_original = cadena_original.Replace("&amp;", "&");

            //Eliminar Archivos Temporales
            File.Delete(nombre_tmp + ".TXT");
            File.Delete(nombre_tmp + ".XML");


            StringBuilder sbPassword;
            StringBuilder sbPrivateKey;
            byte[] b;
            byte[] block;
            int keyBytes;
            sbPassword = new StringBuilder(oEMPRESAp.PASSWORD);
            sbPrivateKey = Rsa.ReadEncPrivateKey(oEMPRESAp.LLAVE, sbPassword.ToString());
            keyBytes = Rsa.KeyBytes(sbPrivateKey.ToString());
            b = System.Text.Encoding.UTF8.GetBytes(cadena_original);
            block = Rsa.EncodeMsgForSignature(keyBytes, b, HashAlgorithm.Sha1);
            block = Rsa.RawPrivate(block, sbPrivateKey.ToString());
            string resultado = System.Convert.ToBase64String(block);
            Wipe.String(sbPassword);
            Wipe.String(sbPrivateKey);
            Wipe.Data(block);

            return resultado;
        }


        private RepAuxFolDetAuxFolComprExt[] cargar_comprobantes_extrajeros(string ROW_ID_CATALOGOp, string poliza)
        {
            RepAuxFolDetAuxFolComprExt[] lista = new RepAuxFolDetAuxFolComprExt[0];
            sSQL = " SELECT     Poliza.numField, PolizasPolizaTransaccionCompExt.taxIDField, PolizasPolizaTransaccionCompExt.montoTotalField, ";
            sSQL+= " PolizasPolizaTransaccionCompExt.numFactExtField, PolizasPolizaTransaccionCompExt.monedaField, PolizasPolizaTransaccionCompExt.tipCambField, ";
            sSQL+= " PolizasPolizaTransaccionCompExt.tipCambFieldSpecified ";
            sSQL += " FROM PolizasPolizaTransaccion INNER JOIN Poliza ON PolizasPolizaTransaccion.ROW_ID_POLIZA = Poliza.ROW_ID INNER JOIN PolizasPolizaTransaccionCompExt ON PolizasPolizaTransaccion.ROW_ID = PolizasPolizaTransaccionCompExt.ROW_ID_TRANSACCION ";
            sSQL += " WHERE (Poliza.ROW_ID_CATALOGO = " + ROW_ID_CATALOGOp + ") AND (Poliza.numField = '" + poliza + "')";
            DataTable oDataTable = oData.EjecutarConsulta(sSQL);
            if (oDataTable != null)
            {
                lista = new RepAuxFolDetAuxFolComprExt[oDataTable.Rows.Count];
                int contador = 0;
                foreach (DataRow oDataRow in oDataTable.Rows)
                {
                    RepAuxFolDetAuxFolComprExt cRepAux = new RepAuxFolDetAuxFolComprExt();
                    cRepAux.TaxID = oDataRow["TaxID"].ToString();
                    cRepAux.NumFactExt = oDataRow["NumFactExt"].ToString();
                    cRepAux.MontoTotal = decimal.Parse(oDataRow["montoTotalField"].ToString());
                    lista[contador] = cRepAux;
                    contador++;
                }
            }
            return lista;
        }

        private RepAuxFolDetAuxFolComprNal[] cargar_comprobantes_nacionales(string ROW_ID_CATALOGOp, string poliza)
        {
            RepAuxFolDetAuxFolComprNal[] lista = new RepAuxFolDetAuxFolComprNal[0];
            sSQL =" SELECT DISTINCT PolizasPolizaTransaccionCompNal.uUID_CFDIField, PolizasPolizaTransaccionCompNal.rFCField, PolizasPolizaTransaccionCompNal.montoTotalField, ";
            sSQL+=" PolizasPolizaTransaccionCompNal.monedaField, PolizasPolizaTransaccionCompNal.tipCambField, PolizasPolizaTransaccionCompNal.tipCambFieldSpecified,Poliza.numField ";
            sSQL+=" FROM PolizasPolizaTransaccion INNER JOIN Poliza ON PolizasPolizaTransaccion.ROW_ID_POLIZA = Poliza.ROW_ID INNER JOIN PolizasPolizaTransaccionCompNal ON PolizasPolizaTransaccion.ROW_ID = PolizasPolizaTransaccionCompNal.ROW_ID_TRANSACCION ";
            sSQL += " WHERE (Poliza.ROW_ID_CATALOGO = " + ROW_ID_CATALOGOp + ") AND (Poliza.numField = '" + poliza + "')";
            DataTable oDataTable = oData.EjecutarConsulta(sSQL);
            if (oDataTable != null)
            {
                lista = new RepAuxFolDetAuxFolComprNal[oDataTable.Rows.Count];
                int contador = 0;
                foreach (DataRow oDataRow in oDataTable.Rows)
                {
                    RepAuxFolDetAuxFolComprNal cRepAux = new RepAuxFolDetAuxFolComprNal();
                    cRepAux.RFC = oDataRow["rFCField"].ToString();
                    cRepAux.UUID_CFDI = oDataRow["uUID_CFDIField"].ToString();
                    cRepAux.MontoTotal = decimal.Parse(oDataRow["montoTotalField"].ToString());
                    lista[contador] = cRepAux;
                    contador++;
                }
            }
            return lista;
        }

    }
}
