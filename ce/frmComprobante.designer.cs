namespace Desarrollo
{
    partial class frmComprobante
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            DevComponents.DotNetBar.ButtonX buttonX3;
            DevComponents.DotNetBar.ButtonX buttonX1;
            DevComponents.DotNetBar.ButtonX buttonX2;
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmComprobante));
            this.metroStatusBar1 = new DevComponents.DotNetBar.Metro.MetroStatusBar();
            this.labelItem1 = new DevComponents.DotNetBar.LabelItem();
            this.rFCField = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.labelX2 = new DevComponents.DotNetBar.LabelX();
            this.uUID_CFDIField = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.labelX4 = new DevComponents.DotNetBar.LabelX();
            buttonX3 = new DevComponents.DotNetBar.ButtonX();
            buttonX1 = new DevComponents.DotNetBar.ButtonX();
            buttonX2 = new DevComponents.DotNetBar.ButtonX();
            this.SuspendLayout();
            // 
            // buttonX3
            // 
            buttonX3.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            buttonX3.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            buttonX3.Image = global::Desarrollo.Properties.Resources.Eliminar_18;
            buttonX3.Location = new System.Drawing.Point(141, 0);
            buttonX3.Name = "buttonX3";
            buttonX3.Size = new System.Drawing.Size(68, 24);
            buttonX3.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            buttonX3.TabIndex = 1;
            buttonX3.Text = "Eliminar";
            buttonX3.TextAlignment = DevComponents.DotNetBar.eButtonTextAlignment.Right;
            buttonX3.Click += new System.EventHandler(this.buttonX3_Click);
            // 
            // buttonX1
            // 
            buttonX1.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            buttonX1.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            buttonX1.Image = global::Desarrollo.Properties.Resources.Nueno_18;
            buttonX1.Location = new System.Drawing.Point(67, 0);
            buttonX1.Name = "buttonX1";
            buttonX1.Size = new System.Drawing.Size(68, 24);
            buttonX1.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            buttonX1.TabIndex = 1;
            buttonX1.Text = "Guardar";
            buttonX1.TextAlignment = DevComponents.DotNetBar.eButtonTextAlignment.Right;
            buttonX1.Click += new System.EventHandler(this.buttonX1_Click);
            // 
            // buttonX2
            // 
            buttonX2.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            buttonX2.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            buttonX2.Image = global::Desarrollo.Properties.Resources.Nuevo_18_19;
            buttonX2.Location = new System.Drawing.Point(0, 0);
            buttonX2.Name = "buttonX2";
            buttonX2.Size = new System.Drawing.Size(61, 24);
            buttonX2.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            buttonX2.TabIndex = 1;
            buttonX2.Text = "Nuevo";
            buttonX2.TextAlignment = DevComponents.DotNetBar.eButtonTextAlignment.Right;
            buttonX2.Click += new System.EventHandler(this.buttonX2_Click);
            // 
            // metroStatusBar1
            // 
            this.metroStatusBar1.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.metroStatusBar1.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.metroStatusBar1.ContainerControlProcessDialogKey = true;
            this.metroStatusBar1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.metroStatusBar1.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.metroStatusBar1.ForeColor = System.Drawing.Color.Black;
            this.metroStatusBar1.Items.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.labelItem1});
            this.metroStatusBar1.Location = new System.Drawing.Point(0, 85);
            this.metroStatusBar1.Name = "metroStatusBar1";
            this.metroStatusBar1.Size = new System.Drawing.Size(357, 21);
            this.metroStatusBar1.TabIndex = 8;
            // 
            // labelItem1
            // 
            this.labelItem1.Name = "labelItem1";
            // 
            // rFCField
            // 
            this.rFCField.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.rFCField.Border.Class = "TextBoxBorder";
            this.rFCField.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.rFCField.ForeColor = System.Drawing.Color.Black;
            this.rFCField.Location = new System.Drawing.Point(83, 58);
            this.rFCField.Name = "rFCField";
            this.rFCField.Size = new System.Drawing.Size(274, 22);
            this.rFCField.TabIndex = 546;
            this.rFCField.TabStop = false;
            this.rFCField.WatermarkBehavior = DevComponents.DotNetBar.eWatermarkBehavior.HideNonEmpty;
            // 
            // labelX2
            // 
            this.labelX2.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.labelX2.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX2.ForeColor = System.Drawing.Color.Black;
            this.labelX2.Location = new System.Drawing.Point(21, 58);
            this.labelX2.Name = "labelX2";
            this.labelX2.Size = new System.Drawing.Size(56, 22);
            this.labelX2.TabIndex = 580;
            this.labelX2.Text = "RFC";
            this.labelX2.TextAlignment = System.Drawing.StringAlignment.Far;
            // 
            // uUID_CFDIField
            // 
            this.uUID_CFDIField.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.uUID_CFDIField.Border.Class = "TextBoxBorder";
            this.uUID_CFDIField.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.uUID_CFDIField.ForeColor = System.Drawing.Color.Black;
            this.uUID_CFDIField.Location = new System.Drawing.Point(83, 30);
            this.uUID_CFDIField.Name = "uUID_CFDIField";
            this.uUID_CFDIField.Size = new System.Drawing.Size(274, 22);
            this.uUID_CFDIField.TabIndex = 548;
            this.uUID_CFDIField.TabStop = false;
            this.uUID_CFDIField.WatermarkBehavior = DevComponents.DotNetBar.eWatermarkBehavior.HideNonEmpty;
            // 
            // labelX4
            // 
            this.labelX4.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.labelX4.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX4.ForeColor = System.Drawing.Color.Black;
            this.labelX4.Location = new System.Drawing.Point(0, 28);
            this.labelX4.Name = "labelX4";
            this.labelX4.Size = new System.Drawing.Size(77, 22);
            this.labelX4.TabIndex = 579;
            this.labelX4.Text = "UUID";
            this.labelX4.TextAlignment = System.Drawing.StringAlignment.Far;
            this.labelX4.Click += new System.EventHandler(this.labelX4_Click);
            // 
            // frmComprobante
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.BottomLeftCornerSize = 0;
            this.ClientSize = new System.Drawing.Size(357, 106);
            this.Controls.Add(this.labelX2);
            this.Controls.Add(this.labelX4);
            this.Controls.Add(this.uUID_CFDIField);
            this.Controls.Add(this.rFCField);
            this.Controls.Add(buttonX3);
            this.Controls.Add(buttonX1);
            this.Controls.Add(buttonX2);
            this.Controls.Add(this.metroStatusBar1);
            this.Cursor = System.Windows.Forms.Cursors.Default;
            this.DoubleBuffered = true;
            this.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "frmComprobante";
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Comprobante";
            this.ResumeLayout(false);

        }

        #endregion

        private DevComponents.DotNetBar.Metro.MetroStatusBar metroStatusBar1;
        private DevComponents.DotNetBar.LabelItem labelItem1;
        private DevComponents.DotNetBar.Controls.TextBoxX rFCField;
        private DevComponents.DotNetBar.LabelX labelX2;
        private DevComponents.DotNetBar.Controls.TextBoxX uUID_CFDIField;
        private DevComponents.DotNetBar.LabelX labelX4;

    }
}