namespace Desarrollo
{
    partial class frmBatchs
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            DevComponents.DotNetBar.ButtonX buttonX3;
            DevComponents.DotNetBar.ButtonX buttonX1;
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmBatchs));
            this.metroStatusBar1 = new DevComponents.DotNetBar.Metro.MetroStatusBar();
            this.labelItem1 = new DevComponents.DotNetBar.LabelItem();
            this.labelX19 = new DevComponents.DotNetBar.LabelX();
            this.BATCH_INICIAL = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.BATCH_FINAL = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.labelX1 = new DevComponents.DotNetBar.LabelX();
            this.ARI = new DevComponents.DotNetBar.Controls.CheckBoxX();
            this.API = new DevComponents.DotNetBar.Controls.CheckBoxX();
            this.ARC = new DevComponents.DotNetBar.Controls.CheckBoxX();
            this.APC = new DevComponents.DotNetBar.Controls.CheckBoxX();
            this.labelX2 = new DevComponents.DotNetBar.LabelX();
            this.SLS = new DevComponents.DotNetBar.Controls.CheckBoxX();
            this.PUR = new DevComponents.DotNetBar.Controls.CheckBoxX();
            this.GJ = new DevComponents.DotNetBar.Controls.CheckBoxX();
            this.ADJ = new DevComponents.DotNetBar.Controls.CheckBoxX();
            this.IND = new DevComponents.DotNetBar.Controls.CheckBoxX();
            this.FG = new DevComponents.DotNetBar.Controls.CheckBoxX();
            this.WIP = new DevComponents.DotNetBar.Controls.CheckBoxX();
            this.RVJ = new DevComponents.DotNetBar.Controls.CheckBoxX();
            this.BAJ = new DevComponents.DotNetBar.Controls.CheckBoxX();
            this.labelX3 = new DevComponents.DotNetBar.LabelX();
            this.labelX4 = new DevComponents.DotNetBar.LabelX();
            this.labelX5 = new DevComponents.DotNetBar.LabelX();
            buttonX3 = new DevComponents.DotNetBar.ButtonX();
            buttonX1 = new DevComponents.DotNetBar.ButtonX();
            this.SuspendLayout();
            // 
            // buttonX3
            // 
            buttonX3.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            buttonX3.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            buttonX3.Image = global::Desarrollo.Properties.Resources.Eliminar_18;
            buttonX3.Location = new System.Drawing.Point(74, 330);
            buttonX3.Name = "buttonX3";
            buttonX3.Size = new System.Drawing.Size(68, 24);
            buttonX3.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            buttonX3.TabIndex = 5;
            buttonX3.Text = "Cancelar";
            buttonX3.TextAlignment = DevComponents.DotNetBar.eButtonTextAlignment.Right;
            buttonX3.Click += new System.EventHandler(this.buttonX3_Click);
            // 
            // buttonX1
            // 
            buttonX1.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            buttonX1.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            buttonX1.Image = global::Desarrollo.Properties.Resources.Nueno_18;
            buttonX1.Location = new System.Drawing.Point(0, 330);
            buttonX1.Name = "buttonX1";
            buttonX1.Size = new System.Drawing.Size(68, 24);
            buttonX1.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            buttonX1.TabIndex = 4;
            buttonX1.Text = "Importar";
            buttonX1.TextAlignment = DevComponents.DotNetBar.eButtonTextAlignment.Right;
            buttonX1.Click += new System.EventHandler(this.buttonX1_Click);
            // 
            // metroStatusBar1
            // 
            this.metroStatusBar1.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.metroStatusBar1.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.metroStatusBar1.ContainerControlProcessDialogKey = true;
            this.metroStatusBar1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.metroStatusBar1.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.metroStatusBar1.ForeColor = System.Drawing.Color.Black;
            this.metroStatusBar1.Items.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.labelItem1});
            this.metroStatusBar1.Location = new System.Drawing.Point(0, 360);
            this.metroStatusBar1.Name = "metroStatusBar1";
            this.metroStatusBar1.Size = new System.Drawing.Size(425, 21);
            this.metroStatusBar1.TabIndex = 6;
            // 
            // labelItem1
            // 
            this.labelItem1.Name = "labelItem1";
            // 
            // labelX19
            // 
            this.labelX19.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.labelX19.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX19.ForeColor = System.Drawing.Color.Black;
            this.labelX19.Location = new System.Drawing.Point(0, 3);
            this.labelX19.Name = "labelX19";
            this.labelX19.Size = new System.Drawing.Size(60, 22);
            this.labelX19.TabIndex = 0;
            this.labelX19.Text = "Lote Inicial";
            this.labelX19.TextAlignment = System.Drawing.StringAlignment.Far;
            // 
            // BATCH_INICIAL
            // 
            this.BATCH_INICIAL.AcceptsTab = true;
            this.BATCH_INICIAL.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.BATCH_INICIAL.Border.Class = "TextBoxBorder";
            this.BATCH_INICIAL.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.BATCH_INICIAL.ForeColor = System.Drawing.Color.Black;
            this.BATCH_INICIAL.Location = new System.Drawing.Point(66, 3);
            this.BATCH_INICIAL.Name = "BATCH_INICIAL";
            this.BATCH_INICIAL.Size = new System.Drawing.Size(76, 22);
            this.BATCH_INICIAL.TabIndex = 1;
            this.BATCH_INICIAL.WatermarkBehavior = DevComponents.DotNetBar.eWatermarkBehavior.HideNonEmpty;
            // 
            // BATCH_FINAL
            // 
            this.BATCH_FINAL.AcceptsTab = true;
            this.BATCH_FINAL.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.BATCH_FINAL.Border.Class = "TextBoxBorder";
            this.BATCH_FINAL.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.BATCH_FINAL.ForeColor = System.Drawing.Color.Black;
            this.BATCH_FINAL.Location = new System.Drawing.Point(215, 3);
            this.BATCH_FINAL.Name = "BATCH_FINAL";
            this.BATCH_FINAL.Size = new System.Drawing.Size(76, 22);
            this.BATCH_FINAL.TabIndex = 3;
            this.BATCH_FINAL.WatermarkBehavior = DevComponents.DotNetBar.eWatermarkBehavior.HideNonEmpty;
            // 
            // labelX1
            // 
            this.labelX1.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.labelX1.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX1.ForeColor = System.Drawing.Color.Black;
            this.labelX1.Location = new System.Drawing.Point(149, 3);
            this.labelX1.Name = "labelX1";
            this.labelX1.Size = new System.Drawing.Size(60, 22);
            this.labelX1.TabIndex = 2;
            this.labelX1.Text = "Lote Final";
            this.labelX1.TextAlignment = System.Drawing.StringAlignment.Far;
            // 
            // ARI
            // 
            this.ARI.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.ARI.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.ARI.Checked = true;
            this.ARI.CheckState = System.Windows.Forms.CheckState.Checked;
            this.ARI.CheckValue = "Y";
            this.ARI.ForeColor = System.Drawing.Color.Black;
            this.ARI.Location = new System.Drawing.Point(88, 243);
            this.ARI.Name = "ARI";
            this.ARI.Size = new System.Drawing.Size(328, 23);
            this.ARI.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.ARI.TabIndex = 595;
            this.ARI.Text = "Cuentas por Cobrar (ARI). Requiere comprobantes fiscales.";
            // 
            // API
            // 
            this.API.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.API.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.API.Checked = true;
            this.API.CheckState = System.Windows.Forms.CheckState.Checked;
            this.API.CheckValue = "Y";
            this.API.ForeColor = System.Drawing.Color.Black;
            this.API.Location = new System.Drawing.Point(88, 272);
            this.API.Name = "API";
            this.API.Size = new System.Drawing.Size(328, 23);
            this.API.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.API.TabIndex = 596;
            this.API.Text = "Cuentas por  Pagar (API). Requiere comprobantes fiscales.";
            // 
            // ARC
            // 
            this.ARC.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.ARC.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.ARC.Checked = true;
            this.ARC.CheckState = System.Windows.Forms.CheckState.Checked;
            this.ARC.CheckValue = "Y";
            this.ARC.ForeColor = System.Drawing.Color.Black;
            this.ARC.Location = new System.Drawing.Point(88, 90);
            this.ARC.Name = "ARC";
            this.ARC.Size = new System.Drawing.Size(272, 23);
            this.ARC.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.ARC.TabIndex = 597;
            this.ARC.Text = "Cobranza (ARC). Requiere comprobantes fiscales.";
            // 
            // APC
            // 
            this.APC.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.APC.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.APC.Checked = true;
            this.APC.CheckState = System.Windows.Forms.CheckState.Checked;
            this.APC.CheckValue = "Y";
            this.APC.ForeColor = System.Drawing.Color.Black;
            this.APC.Location = new System.Drawing.Point(88, 61);
            this.APC.Name = "APC";
            this.APC.Size = new System.Drawing.Size(272, 23);
            this.APC.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.APC.TabIndex = 598;
            this.APC.Text = "Pagos (APC). Requiere comprobantes fiscales.";
            // 
            // labelX2
            // 
            this.labelX2.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.labelX2.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX2.ForeColor = System.Drawing.Color.Black;
            this.labelX2.Location = new System.Drawing.Point(9, 31);
            this.labelX2.Name = "labelX2";
            this.labelX2.Size = new System.Drawing.Size(94, 22);
            this.labelX2.TabIndex = 599;
            this.labelX2.Text = "Importar de tipo:";
            // 
            // SLS
            // 
            this.SLS.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.SLS.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.SLS.Checked = true;
            this.SLS.CheckState = System.Windows.Forms.CheckState.Checked;
            this.SLS.CheckValue = "Y";
            this.SLS.ForeColor = System.Drawing.Color.Black;
            this.SLS.Location = new System.Drawing.Point(260, 127);
            this.SLS.Name = "SLS";
            this.SLS.Size = new System.Drawing.Size(156, 23);
            this.SLS.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.SLS.TabIndex = 603;
            this.SLS.Text = "Embarques (SLS)";
            // 
            // PUR
            // 
            this.PUR.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.PUR.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.PUR.Checked = true;
            this.PUR.CheckState = System.Windows.Forms.CheckState.Checked;
            this.PUR.CheckValue = "Y";
            this.PUR.ForeColor = System.Drawing.Color.Black;
            this.PUR.Location = new System.Drawing.Point(88, 185);
            this.PUR.Name = "PUR";
            this.PUR.Size = new System.Drawing.Size(157, 23);
            this.PUR.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.PUR.TabIndex = 602;
            this.PUR.Text = "Compras (PUR)";
            // 
            // GJ
            // 
            this.GJ.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.GJ.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.GJ.Checked = true;
            this.GJ.CheckState = System.Windows.Forms.CheckState.Checked;
            this.GJ.CheckValue = "Y";
            this.GJ.ForeColor = System.Drawing.Color.Black;
            this.GJ.Location = new System.Drawing.Point(88, 156);
            this.GJ.Name = "GJ";
            this.GJ.Size = new System.Drawing.Size(157, 23);
            this.GJ.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.GJ.TabIndex = 601;
            this.GJ.Text = "P�lizas de diario (GJ)";
            // 
            // ADJ
            // 
            this.ADJ.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.ADJ.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.ADJ.Checked = true;
            this.ADJ.CheckState = System.Windows.Forms.CheckState.Checked;
            this.ADJ.CheckValue = "Y";
            this.ADJ.ForeColor = System.Drawing.Color.Black;
            this.ADJ.Location = new System.Drawing.Point(88, 127);
            this.ADJ.Name = "ADJ";
            this.ADJ.Size = new System.Drawing.Size(157, 23);
            this.ADJ.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.ADJ.TabIndex = 600;
            this.ADJ.Text = "Ajustes de inventario (ADJ)";
            // 
            // IND
            // 
            this.IND.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.IND.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.IND.Checked = true;
            this.IND.CheckState = System.Windows.Forms.CheckState.Checked;
            this.IND.CheckValue = "Y";
            this.IND.ForeColor = System.Drawing.Color.Black;
            this.IND.Location = new System.Drawing.Point(260, 156);
            this.IND.Name = "IND";
            this.IND.Size = new System.Drawing.Size(156, 23);
            this.IND.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.IND.TabIndex = 604;
            this.IND.Text = "Indirectos (IND)";
            // 
            // FG
            // 
            this.FG.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.FG.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.FG.Checked = true;
            this.FG.CheckState = System.Windows.Forms.CheckState.Checked;
            this.FG.CheckValue = "Y";
            this.FG.ForeColor = System.Drawing.Color.Black;
            this.FG.Location = new System.Drawing.Point(260, 185);
            this.FG.Name = "FG";
            this.FG.Size = new System.Drawing.Size(156, 23);
            this.FG.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.FG.TabIndex = 605;
            this.FG.Text = "Produto terminado (FG)";
            // 
            // WIP
            // 
            this.WIP.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.WIP.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.WIP.Checked = true;
            this.WIP.CheckState = System.Windows.Forms.CheckState.Checked;
            this.WIP.CheckValue = "Y";
            this.WIP.ForeColor = System.Drawing.Color.Black;
            this.WIP.Location = new System.Drawing.Point(260, 214);
            this.WIP.Name = "WIP";
            this.WIP.Size = new System.Drawing.Size(165, 23);
            this.WIP.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.WIP.TabIndex = 606;
            this.WIP.Text = "Producci�n en proceso (WIP)";
            // 
            // RVJ
            // 
            this.RVJ.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.RVJ.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.RVJ.Checked = true;
            this.RVJ.CheckState = System.Windows.Forms.CheckState.Checked;
            this.RVJ.CheckValue = "Y";
            this.RVJ.ForeColor = System.Drawing.Color.Black;
            this.RVJ.Location = new System.Drawing.Point(88, 214);
            this.RVJ.Name = "RVJ";
            this.RVJ.Size = new System.Drawing.Size(157, 23);
            this.RVJ.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.RVJ.TabIndex = 607;
            this.RVJ.Text = "Revaluaci�n (RVJ)";
            // 
            // BAJ
            // 
            this.BAJ.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.BAJ.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.BAJ.Checked = true;
            this.BAJ.CheckState = System.Windows.Forms.CheckState.Checked;
            this.BAJ.CheckValue = "Y";
            this.BAJ.ForeColor = System.Drawing.Color.Black;
            this.BAJ.Location = new System.Drawing.Point(9, 301);
            this.BAJ.Name = "BAJ";
            this.BAJ.Size = new System.Drawing.Size(351, 23);
            this.BAJ.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.BAJ.TabIndex = 608;
            this.BAJ.Text = "Ajustes bancarios (BAJ). ";
            // 
            // labelX3
            // 
            this.labelX3.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.labelX3.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX3.ForeColor = System.Drawing.Color.Black;
            this.labelX3.Location = new System.Drawing.Point(9, 59);
            this.labelX3.Name = "labelX3";
            this.labelX3.Size = new System.Drawing.Size(51, 22);
            this.labelX3.TabIndex = 609;
            this.labelX3.Text = "Egreso";
            // 
            // labelX4
            // 
            this.labelX4.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.labelX4.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX4.ForeColor = System.Drawing.Color.Black;
            this.labelX4.Location = new System.Drawing.Point(9, 88);
            this.labelX4.Name = "labelX4";
            this.labelX4.Size = new System.Drawing.Size(51, 22);
            this.labelX4.TabIndex = 610;
            this.labelX4.Text = "Ingresos";
            // 
            // labelX5
            // 
            this.labelX5.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.labelX5.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX5.ForeColor = System.Drawing.Color.Black;
            this.labelX5.Location = new System.Drawing.Point(9, 127);
            this.labelX5.Name = "labelX5";
            this.labelX5.Size = new System.Drawing.Size(73, 22);
            this.labelX5.TabIndex = 611;
            this.labelX5.Text = "Movimientos";
            // 
            // frmBatchs
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.BottomLeftCornerSize = 0;
            this.ClientSize = new System.Drawing.Size(425, 381);
            this.Controls.Add(this.labelX5);
            this.Controls.Add(this.labelX4);
            this.Controls.Add(this.labelX3);
            this.Controls.Add(this.BAJ);
            this.Controls.Add(this.RVJ);
            this.Controls.Add(this.WIP);
            this.Controls.Add(this.FG);
            this.Controls.Add(this.IND);
            this.Controls.Add(this.SLS);
            this.Controls.Add(this.PUR);
            this.Controls.Add(this.GJ);
            this.Controls.Add(this.ADJ);
            this.Controls.Add(this.labelX2);
            this.Controls.Add(this.APC);
            this.Controls.Add(this.ARC);
            this.Controls.Add(this.API);
            this.Controls.Add(this.ARI);
            this.Controls.Add(this.BATCH_FINAL);
            this.Controls.Add(this.labelX1);
            this.Controls.Add(this.BATCH_INICIAL);
            this.Controls.Add(this.labelX19);
            this.Controls.Add(buttonX3);
            this.Controls.Add(buttonX1);
            this.Controls.Add(this.metroStatusBar1);
            this.Cursor = System.Windows.Forms.Cursors.Default;
            this.DoubleBuffered = true;
            this.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "frmBatchs";
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Lotes";
            this.ResumeLayout(false);

        }

        #endregion

        private DevComponents.DotNetBar.Metro.MetroStatusBar metroStatusBar1;
        private DevComponents.DotNetBar.LabelItem labelItem1;
        private DevComponents.DotNetBar.LabelX labelX19;
        private DevComponents.DotNetBar.Controls.TextBoxX BATCH_INICIAL;
        private DevComponents.DotNetBar.Controls.TextBoxX BATCH_FINAL;
        private DevComponents.DotNetBar.LabelX labelX1;
        private DevComponents.DotNetBar.Controls.CheckBoxX ARI;
        private DevComponents.DotNetBar.Controls.CheckBoxX API;
        private DevComponents.DotNetBar.Controls.CheckBoxX ARC;
        private DevComponents.DotNetBar.Controls.CheckBoxX APC;
        private DevComponents.DotNetBar.LabelX labelX2;
        private DevComponents.DotNetBar.Controls.CheckBoxX SLS;
        private DevComponents.DotNetBar.Controls.CheckBoxX PUR;
        private DevComponents.DotNetBar.Controls.CheckBoxX GJ;
        private DevComponents.DotNetBar.Controls.CheckBoxX ADJ;
        private DevComponents.DotNetBar.Controls.CheckBoxX IND;
        private DevComponents.DotNetBar.Controls.CheckBoxX FG;
        private DevComponents.DotNetBar.Controls.CheckBoxX WIP;
        private DevComponents.DotNetBar.Controls.CheckBoxX RVJ;
        private DevComponents.DotNetBar.Controls.CheckBoxX BAJ;
        private DevComponents.DotNetBar.LabelX labelX3;
        private DevComponents.DotNetBar.LabelX labelX4;
        private DevComponents.DotNetBar.LabelX labelX5;

    }
}