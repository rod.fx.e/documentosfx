namespace Desarrollo
{
    partial class frmAgrupador
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            DevComponents.DotNetBar.ButtonX buttonX3;
            DevComponents.DotNetBar.ButtonX buttonX1;
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmAgrupador));
            this.metroStatusBar1 = new DevComponents.DotNetBar.Metro.MetroStatusBar();
            this.labelItem1 = new DevComponents.DotNetBar.LabelItem();
            this.labelX19 = new DevComponents.DotNetBar.LabelX();
            this.codAgrupField = new DevComponents.DotNetBar.Controls.ComboBoxEx();
            buttonX3 = new DevComponents.DotNetBar.ButtonX();
            buttonX1 = new DevComponents.DotNetBar.ButtonX();
            this.SuspendLayout();
            // 
            // buttonX3
            // 
            buttonX3.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            buttonX3.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            buttonX3.Image = global::Desarrollo.Properties.Resources.Eliminar_18;
            buttonX3.Location = new System.Drawing.Point(86, 0);
            buttonX3.Name = "buttonX3";
            buttonX3.Size = new System.Drawing.Size(68, 24);
            buttonX3.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            buttonX3.TabIndex = 1;
            buttonX3.Text = "Cancelar";
            buttonX3.TextAlignment = DevComponents.DotNetBar.eButtonTextAlignment.Right;
            buttonX3.Click += new System.EventHandler(this.buttonX3_Click);
            // 
            // buttonX1
            // 
            buttonX1.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            buttonX1.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            buttonX1.Image = global::Desarrollo.Properties.Resources.Nueno_18;
            buttonX1.Location = new System.Drawing.Point(12, 0);
            buttonX1.Name = "buttonX1";
            buttonX1.Size = new System.Drawing.Size(68, 24);
            buttonX1.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            buttonX1.TabIndex = 1;
            buttonX1.Text = "Guardar";
            buttonX1.TextAlignment = DevComponents.DotNetBar.eButtonTextAlignment.Right;
            buttonX1.Click += new System.EventHandler(this.buttonX1_Click);
            // 
            // metroStatusBar1
            // 
            this.metroStatusBar1.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.metroStatusBar1.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.metroStatusBar1.ContainerControlProcessDialogKey = true;
            this.metroStatusBar1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.metroStatusBar1.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.metroStatusBar1.ForeColor = System.Drawing.Color.Black;
            this.metroStatusBar1.Items.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.labelItem1});
            this.metroStatusBar1.Location = new System.Drawing.Point(0, 60);
            this.metroStatusBar1.Name = "metroStatusBar1";
            this.metroStatusBar1.Size = new System.Drawing.Size(578, 21);
            this.metroStatusBar1.TabIndex = 8;
            // 
            // labelItem1
            // 
            this.labelItem1.Name = "labelItem1";
            // 
            // labelX19
            // 
            this.labelX19.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.labelX19.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX19.ForeColor = System.Drawing.Color.Black;
            this.labelX19.Location = new System.Drawing.Point(0, 30);
            this.labelX19.Name = "labelX19";
            this.labelX19.Size = new System.Drawing.Size(60, 22);
            this.labelX19.TabIndex = 583;
            this.labelX19.Text = "Agrupador";
            this.labelX19.TextAlignment = System.Drawing.StringAlignment.Far;
            // 
            // codAgrupField
            // 
            this.codAgrupField.DisplayMember = "Text";
            this.codAgrupField.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.codAgrupField.ForeColor = System.Drawing.Color.Black;
            this.codAgrupField.FormattingEnabled = true;
            this.codAgrupField.ItemHeight = 16;
            this.codAgrupField.Location = new System.Drawing.Point(67, 30);
            this.codAgrupField.Name = "codAgrupField";
            this.codAgrupField.Size = new System.Drawing.Size(511, 22);
            this.codAgrupField.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.codAgrupField.TabIndex = 584;
            // 
            // frmAgrupador
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.BottomLeftCornerSize = 0;
            this.ClientSize = new System.Drawing.Size(578, 81);
            this.Controls.Add(this.codAgrupField);
            this.Controls.Add(this.labelX19);
            this.Controls.Add(buttonX3);
            this.Controls.Add(buttonX1);
            this.Controls.Add(this.metroStatusBar1);
            this.Cursor = System.Windows.Forms.Cursors.Default;
            this.DoubleBuffered = true;
            this.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "frmAgrupador";
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Cuenta";
            this.ResumeLayout(false);

        }

        #endregion

        private DevComponents.DotNetBar.Metro.MetroStatusBar metroStatusBar1;
        private DevComponents.DotNetBar.LabelItem labelItem1;
        private DevComponents.DotNetBar.LabelX labelX19;
        private DevComponents.DotNetBar.Controls.ComboBoxEx codAgrupField;

    }
}