﻿using CryptoSysPKI;
using DevComponents.DotNetBar;
using Generales;
using Ionic.Zip;
using ModeloDocumentosFX;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Windows.Forms;
using System.Xml;
using System.Xml.Serialization;
using System.Xml.XPath;
using System.Xml.Xsl;

namespace Desarrollo
{
    class cAuxiliarCtas
    {
        private string sSQL = "";
        public string ROW_ID { get; set; }

        public string ROW_ID_CATALOGO { get; set; }

        public string tipoSolicitudField { get; set; }

        public string numOrdenField { get; set; }

        public string numTramiteField { get; set; }

        public string selloField { get; set; }

        public string noCertificadoField { get; set; }

        public string certificadoField { get; set; }

        private cCONEXCION oData;
        public override string ToString() { return ROW_ID; }

        public cAuxiliarCtas()
        {
            ModeloDocumentosFXContainer dbContext = new ModeloDocumentosFXContainer();
            try
            {
                string conection = dbContext.Database.Connection.ConnectionString;
                oData = new cCONEXCION(conection);
            }
            catch (Exception err)
            {
                ErrorFX.mostrar(err, false, true, "frmEmpresa - 96 - ", false);
            }
            limpiar();
        }
        public void limpiar()
        {
            ROW_ID = "";
            ROW_ID_CATALOGO = "";
            tipoSolicitudField = "";
            numOrdenField = "";
            numTramiteField = "";
            selloField = "";
            noCertificadoField = "";
            certificadoField = "";
        }


        public List<cAuxiliarCtas> todos(string ROW_ID_CATALOGOp,string tipoFieldp = "",string ASIGNADOp="")
        {

            List<cAuxiliarCtas> lista = new List<cAuxiliarCtas>();

            sSQL = " SELECT * ";
            sSQL += " FROM AuxiliarCtas ";
            sSQL += " WHERE 1=1 ";
            if(ROW_ID_CATALOGOp!="")
            {
                sSQL += " AND ROW_ID_CATALOGO=" + ROW_ID_CATALOGOp + "";
            }
            
            sSQL += " ORDER BY ROW_ID";
            DataTable oDataTable = oData.EjecutarConsulta(sSQL);
            foreach (DataRow oDataRow in oDataTable.Rows)
            {
                cAuxiliarCtas ocCatalogo = new cAuxiliarCtas();
                ocCatalogo.cargar(oDataRow["ROW_ID"].ToString());
                lista.Add(ocCatalogo);
            }
            return lista;

        }

        public bool verificar(string numFieldp, string ROW_ID_CATALOGOp, bool cargarp=false)
        {
            sSQL = " SELECT * ";
            sSQL += " FROM AuxiliarCtas ";
            sSQL += " WHERE numField='" + numFieldp + "' ";
            sSQL += " AND ROW_ID_CATALOGO=" + ROW_ID_CATALOGOp + "";
            DataTable oDataTable = oData.EjecutarConsulta(sSQL);
            if (oDataTable != null)
            {
                foreach (DataRow oDataRow in oDataTable.Rows)
                {

                    if (!cargarp)
                    {
                        return true;
                    }
                    else
                    {
                        cargar(oDataRow["ROW_ID"].ToString());
                    }
                }
            }
            return false;
        }

        public bool cargar(string ROW_IDp)
        {
            sSQL = " SELECT * ";
            sSQL += " FROM AuxiliarCtas ";
            sSQL += " WHERE ROW_ID=" + ROW_IDp + " ";
            DataTable oDataTable = oData.EjecutarConsulta(sSQL);
            if (oDataTable != null)
            {
                foreach (DataRow oDataRow in oDataTable.Rows)
                {
                    //Añadir el Registro
                    ROW_ID = oDataRow["ROW_ID"].ToString();
                    ROW_ID_CATALOGO = oDataRow["ROW_ID_CATALOGO"].ToString();
                    tipoSolicitudField = oDataRow["tipoSolicitudField"].ToString();
                    numOrdenField = oDataRow["numOrdenField"].ToString();
                    numTramiteField = oDataRow["numTramiteField"].ToString();
                    selloField = oDataRow["selloField"].ToString();
                    noCertificadoField = oDataRow["noCertificadoField"].ToString();
                    certificadoField = oDataRow["certificadoField"].ToString();
                    return true;
                }
            }
            return false;
        }

        public List<cAuxiliarCtas> validar(string ROW_ID_CATALOGOp)
        {

            List<cAuxiliarCtas> lista = new List<cAuxiliarCtas>();

            return lista;

        }
        


        public bool guardar(bool validar=true)
        {

            if (ROW_ID == "")
            {
                sSQL = " INSERT INTO AuxiliarCtas ";
                sSQL += " ( ";
                sSQL += " [ROW_ID_CATALOGO],[tipoSolicitudField],[numOrdenField],[numTramiteField]";
                sSQL += ",[selloField],[noCertificadoField],[certificadoField] ";
                sSQL += " )";
                sSQL += " VALUES ";
                sSQL += "(";
                sSQL += " " + ROW_ID_CATALOGO + ",'" + tipoSolicitudField + "','" + numOrdenField.ToString() + "','" + numTramiteField.ToString() + "'";
                sSQL += ",'" + selloField.ToString() + "','" + noCertificadoField.ToString() + "','" + certificadoField + "' ";
                sSQL += ")";
                DataTable oDataTable = oData.EjecutarConsulta(sSQL);
                if (oDataTable != null)
                {

                    sSQL = "SELECT TOP 1 MAX(ROW_ID) as ROW_ID FROM AuxiliarCtas";
                    oDataTable = oData.EjecutarConsulta(sSQL);
                    if (oDataTable != null)
                    {
                        foreach (DataRow oDataRow in oDataTable.Rows)
                        {
                            ROW_ID = oDataRow["ROW_ID"].ToString();
                        }
                    }
                }

            }
            else
            {
                sSQL = " UPDATE AuxiliarCtas ";
                sSQL += " SET ROW_ID_CATALOGO=" + ROW_ID_CATALOGO + ",tipoSolicitudField='" + tipoSolicitudField + "',numOrdenField='" + numOrdenField.ToString() + "'";
                sSQL += ",numTramiteField='" + numTramiteField.ToString() + "',selloField='" + selloField + "',noCertificadoField='" + noCertificadoField + "' ";
                sSQL += " WHERE ROW_ID=" + ROW_ID;
                oData.EjecutarConsulta(sSQL);

            }
            return true;

        }

        public bool eliminar(string ROW_IDp)
        {
            if (ROW_IDp != "")
            {
                sSQL = "DELETE FROM AuxiliarCtas ";
                sSQL += " WHERE ROW_ID='" + ROW_IDp + "' ";
                oData.EjecutarConsulta(sSQL);
                return true;
            }
            return false;
        }

        public bool generar(string ROW_ID_CATALOGOp)
        {
            FolderBrowserDialog folderBrowserDialog1 = new FolderBrowserDialog();
            if (folderBrowserDialog1.ShowDialog() == DialogResult.OK)
            {
                importar(folderBrowserDialog1, false, ROW_ID_CATALOGOp);
                MessageBoxEx.Show("Exportación de los archivos finalizada en " + folderBrowserDialog1.SelectedPath.ToString(), Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Information);
                return true;
            }
            return false;
        }

        private void importar(FolderBrowserDialog folderBrowserDialog1, bool mostrar_mensaje_finalizacion, string ROW_ID_CATALOGOp)
        {

            if (folderBrowserDialog1.SelectedPath.Trim() == "")
            {
                if (folderBrowserDialog1.ShowDialog() != DialogResult.OK)
                {
                    MessageBoxEx.Show("Generación de archivo cancelada.", Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
                    return;
                }
            }
            cCatalogo ocCatalogo = new cCatalogo();
            ocCatalogo.cargar(ROW_ID_CATALOGOp);
            //string nombre_tmp = path + "\\" + "Polizas_" + DateTime.Now.ToString("ddMMyyyyhhmmss");
            //string nombre_tmp = folderBrowserDialog1.SelectedPath + "\\" + "XC" + ocCatalogo.rFCField + ocCatalogo.anoField + ocCatalogo.mesField + ".xml";

            string mes = ocCatalogo.mesField;
            if (mes.Length == 1)
            {
                mes = "0" + mes;
            }

            string nombre = ocCatalogo.rFCField + ocCatalogo.anoField + mes + "XC";
            string nombre_tmp = folderBrowserDialog1.SelectedPath + "\\" + nombre + ".xml";// +DateTime.Now.ToString("ddMMyyyyhhmmss");


            //Generar AuxliarCtas
            AuxiliarCtas oAuxiliarCtas = new AuxiliarCtas();
            cAuxiliarCtas ocAuxiliarCtas = new cAuxiliarCtas();
            
            oAuxiliarCtas.TipoSolicitud = tipoSolicitudField;
            oAuxiliarCtas.NumOrden = numOrdenField;
            oAuxiliarCtas.NumTramite= numTramiteField;
            if (oAuxiliarCtas.NumOrden == "")
            {
                oAuxiliarCtas.NumOrden = null;
            }
            if (oAuxiliarCtas.NumTramite == "")
            {
                oAuxiliarCtas.NumTramite = null;
            }

            oAuxiliarCtas.RFC = ocCatalogo.rFCField;
            cEMPRESA_CE ocEMPRESA = new cEMPRESA_CE();
            ocEMPRESA.cargar_rfc(oAuxiliarCtas.RFC);
            if (ocEMPRESA.NO_CERTIFICADO.Trim().Length == 0)
            {
                MessageBoxEx.Show("No se tiene configurado el Certificado"
                   , Application.ProductName + "-" + Application.ProductVersion, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);

                return;
            }
            oAuxiliarCtas.noCertificado = ocEMPRESA.NO_CERTIFICADO;
            if (!File.Exists(ocEMPRESA.CERTIFICADO))
            {
                MessageBoxEx.Show("No se tiene acceso al Certificado, archivo " + ocEMPRESA.CERTIFICADO + ""
                    , Application.ProductName + "-" + Application.ProductVersion, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);

                return;
            }
            X509Certificate cert = X509Certificate.CreateFromCertFile(ocEMPRESA.CERTIFICADO);
            string Certificado64 = System.Convert.ToBase64String(cert.GetRawCertData());
            oAuxiliarCtas.Certificado = Certificado64;


            oAuxiliarCtas.Anio = int.Parse(ocCatalogo.anoField);

            switch (int.Parse(ocCatalogo.mesField))
            {
                case 1:
                    oAuxiliarCtas.Mes = AuxiliarCtasMes.Item01;
                    break;
                case 2:
                    oAuxiliarCtas.Mes = AuxiliarCtasMes.Item02;
                    break;
                case 3:
                    oAuxiliarCtas.Mes = AuxiliarCtasMes.Item03;
                    break;
                case 4:
                    oAuxiliarCtas.Mes = AuxiliarCtasMes.Item04;
                    break;
                case 5:
                    oAuxiliarCtas.Mes = AuxiliarCtasMes.Item05;
                    break;
                case 6:
                    oAuxiliarCtas.Mes = AuxiliarCtasMes.Item06;
                    break;
                case 7:
                    oAuxiliarCtas.Mes = AuxiliarCtasMes.Item07;
                    break;
                case 8:
                    oAuxiliarCtas.Mes = AuxiliarCtasMes.Item08;
                    break;
                case 9:
                    oAuxiliarCtas.Mes = AuxiliarCtasMes.Item09;
                    break;
                case 10:
                    oAuxiliarCtas.Mes = AuxiliarCtasMes.Item10;
                    break;
                case 11:
                    oAuxiliarCtas.Mes = AuxiliarCtasMes.Item11;
                    break;
                case 12:
                    oAuxiliarCtas.Mes = AuxiliarCtasMes.Item12;
                    break;
            }
            oAuxiliarCtas.RFC = ocCatalogo.rFCField;

            //Cargar la Balanza de Comprobación
            cBalanza ocBalanza=new cBalanza();
            ocBalanza.cargar(ROW_ID_CATALOGOp);
            List<cBalanza> listaBalanza = new List<cBalanza>();
            listaBalanza=ocBalanza.todos(ROW_ID_CATALOGOp,"",true);
            oAuxiliarCtas.Cuenta = new AuxiliarCtasCuenta[listaBalanza.Count];
            int contador_AuxiliarCtas=0;
            foreach(cBalanza registro in listaBalanza)
            {
                oAuxiliarCtas.Cuenta[contador_AuxiliarCtas] = new AuxiliarCtasCuenta();
                oAuxiliarCtas.Cuenta[contador_AuxiliarCtas].NumCta = registro.numCtaField;
                cCatalogoCtas cCatalogoCtas = new Desarrollo.cCatalogoCtas();
                cCatalogoCtas.cargar_numCta(registro.numCtaField);
                oAuxiliarCtas.Cuenta[contador_AuxiliarCtas].DesCta = cCatalogoCtas.descField;

                oAuxiliarCtas.Cuenta[contador_AuxiliarCtas].SaldoFin = decimal.Parse(registro.saldoFinField.ToString());
                oAuxiliarCtas.Cuenta[contador_AuxiliarCtas].SaldoIni = decimal.Parse(registro.saldoIniField.ToString());
                //Cargar el Detalle
                oAuxiliarCtas.Cuenta[contador_AuxiliarCtas].DetalleAux = cargar_detalle(ROW_ID_CATALOGOp, registro.numCtaField);
                contador_AuxiliarCtas++;
            }

            crear_archivo(nombre,oAuxiliarCtas);

            oAuxiliarCtas.Sello = sellar(nombre + ".XML", ocEMPRESA);
            
            if (oAuxiliarCtas.Sello == "")
            {
                MessageBoxEx.Show("El sello tiene error de generación.", Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            crear_archivo(nombre, oAuxiliarCtas);

            //Comprimir
            string ARCHIVO_zip = nombre + ".ZIP";
            using (ZipFile zip = new ZipFile())
            {
                zip.AddFile(nombre + ".XML");
                zip.Save(ARCHIVO_zip);
            }
            File.Delete(nombre + ".XML");
            File.Delete(folderBrowserDialog1.SelectedPath + "\\" + ARCHIVO_zip);
            File.Copy(ARCHIVO_zip, folderBrowserDialog1.SelectedPath + "\\" + ARCHIVO_zip);
            File.Delete(ARCHIVO_zip);
            //Fin de Compresion de Archivo

            if (mostrar_mensaje_finalizacion)
            {
                MessageBoxEx.Show("Exportación de los archivos finalizada.", Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Information);
            }

        }

        private void crear_archivo(string nombre, AuxiliarCtas oAuxiliarCtas)
        {
            //Transformar al XML
            XmlSerializerNamespaces myNamespaces = new XmlSerializerNamespaces();
            //myNamespaces.Add("schemaLocation", "http://www.sat.gob.mx/esquemas/ContabilidadE/1_1/AuxiliarCtas/AuxiliarCtas_1_1.xsd");
            myNamespaces.Add("xsi", "http://www.w3.org/2001/XMLSchema-instance");
            myNamespaces.Add("AuxiliarCtas", "www.sat.gob.mx/esquemas/ContabilidadE/1_1/AuxiliarCtas");
            myNamespaces.Add("cambiar", "www.sat.gob.mx/esquemas/ContabilidadE/1_1/AuxiliarCtas http://www.sat.gob.mx/esquemas/ContabilidadE/1_1/AuxiliarCtas/AuxiliarCtas_1_1.xsd");


            //Transformar al XML
            TextWriter writer_p = new StreamWriter(nombre + ".XML");
            XmlSerializer serializer_p = new XmlSerializer(typeof(AuxiliarCtas));
            serializer_p.Serialize(writer_p, oAuxiliarCtas, myNamespaces);
            writer_p.Close();

            oData.ReplaceInFile(nombre + ".XML", "xmlns:cambiar", "xsi:schemaLocation");

        }
        private string sellar(string archivo, cEMPRESA_CE oEMPRESAp)
        {
            string sDirectory = AppDomain.CurrentDomain.BaseDirectory;
            XPathDocument myXPathDoc = new XPathDocument(archivo);
            //XslTransform myXslTrans = new XslTransform();
            XslCompiledTransform myXslTrans = new XslCompiledTransform();


            XslCompiledTransform trans = new XslCompiledTransform();
            XmlUrlResolver resolver = new XmlUrlResolver();

            resolver.Credentials = System.Net.CredentialCache.DefaultCredentials;
            XsltSettings settings = new XsltSettings(true, true);

            //Cargar xslt
            try
            {

                myXslTrans.Load(sDirectory + "/xslt/AuxiliarCtas_1_1.xslt");
            }
            catch (XmlException errores)
            {
                MessageBoxEx.Show("Error en " + errores.InnerException.ToString());
                return "";
            }
            //Crear Archivo Temporal
            string nombre_tmp = "TMP" + DateTime.Now.ToString("ddMMyyyyhhmmss");
            XmlTextWriter myWriter = new XmlTextWriter(nombre_tmp + ".TXT", null);

            //Transformar al XML
            myXslTrans.Transform(myXPathDoc, null, myWriter);

            myWriter.Close();

            //Abrir Archivo TXT
            string cadena_original = "";
            StreamReader re = File.OpenText(nombre_tmp + ".TXT");
            string input = null;
            while ((input = re.ReadLine()) != null)
            {
                cadena_original += input;
            }
            re.Close();
            //Buscar el &amp; en la Cadena y sustituirlo por &
            cadena_original = cadena_original.Replace("&amp;", "&");

            //Eliminar Archivos Temporales
            File.Delete(nombre_tmp + ".TXT");
            File.Delete(nombre_tmp + ".XML");


            StringBuilder sbPassword;
            StringBuilder sbPrivateKey;
            byte[] b;
            byte[] block;
            int keyBytes;
            sbPassword = new StringBuilder(oEMPRESAp.PASSWORD);
            sbPrivateKey = Rsa.ReadEncPrivateKey(oEMPRESAp.LLAVE, sbPassword.ToString());
            keyBytes = Rsa.KeyBytes(sbPrivateKey.ToString());
            b = System.Text.Encoding.UTF8.GetBytes(cadena_original);
            block = Rsa.EncodeMsgForSignature(keyBytes, b, HashAlgorithm.Sha1);
            block = Rsa.RawPrivate(block, sbPrivateKey.ToString());
            string resultado = System.Convert.ToBase64String(block);
            Wipe.String(sbPassword);
            Wipe.String(sbPrivateKey);
            Wipe.Data(block);

            return resultado;
        }

        private AuxiliarCtasCuentaDetalleAux[] cargar_detalle(string ROW_ID_CATALOGOp, string cuentap)
        {
            AuxiliarCtasCuentaDetalleAux[] lista=new AuxiliarCtasCuentaDetalleAux[0];
            sSQL = " SELECT Poliza.ROW_ID_CATALOGO, Poliza.numField, PolizasPolizaTransaccion.numCtaField, Poliza.conceptoField, PolizasPolizaTransaccion.debeField, PolizasPolizaTransaccion.haberField,Poliza.fechaField ";
            sSQL+=" FROM Poliza INNER JOIN PolizasPolizaTransaccion ON Poliza.ROW_ID = PolizasPolizaTransaccion.ROW_ID_POLIZA ";
            sSQL += " WHERE Poliza.ROW_ID_CATALOGO=" + ROW_ID_CATALOGOp + " and PolizasPolizaTransaccion.numCtaField='" + cuentap+ "'";
            DataTable oDataTable = oData.EjecutarConsulta(sSQL);
            if(oDataTable!=null)
            {
                lista = new AuxiliarCtasCuentaDetalleAux[oDataTable.Rows.Count];
                int contador=0;
                foreach (DataRow oDataRow in oDataTable.Rows)
                {
                    AuxiliarCtasCuentaDetalleAux cAuxiliarCtasCuentaDetalleAux = new AuxiliarCtasCuentaDetalleAux();
                    cAuxiliarCtasCuentaDetalleAux.NumUnIdenPol = oDataRow["numField"].ToString();
                    cAuxiliarCtasCuentaDetalleAux.Concepto = oDataRow["conceptoField"].ToString();
                    cAuxiliarCtasCuentaDetalleAux.Debe = decimal.Parse(oDataRow["debeField"].ToString());
                    cAuxiliarCtasCuentaDetalleAux.Haber = decimal.Parse(oDataRow["haberField"].ToString());
                    cAuxiliarCtasCuentaDetalleAux.Fecha = DateTime.Parse(oDataRow["fechaField"].ToString());
                    lista[contador] = cAuxiliarCtasCuentaDetalleAux;
                    contador++;
                }
            }
            return lista;
        }

    }
}
