﻿using DevComponents.DotNetBar;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Generales;
using ModeloDocumentosFX;

namespace Desarrollo
{

    class cPolizasPolizaTransaccionOtrMetodoPago
    {
        private string sSQL = "";
        public string ROW_ID { get; set; }

        public string ROW_ID_TRANSACCION { get; set; }
        public string metPagoPolField { get; set; }
        public DateTime fechaField { get; set; }
        public string benefField { get; set; }
        public string rFCField { get; set; }
        public decimal montoTotalField { get; set; }
        public string monedaField { get; set; }
        public decimal tipCambField { get; set; }
        public bool tipCambFieldSpecified { get; set; }

        private cCONEXCION oData = new cCONEXCION();
        public override string ToString() { return ROW_ID; }

        public cPolizasPolizaTransaccionOtrMetodoPago()
        {
            ModeloDocumentosFXContainer dbContext = new ModeloDocumentosFXContainer();
            try
            {
                string conection = dbContext.Database.Connection.ConnectionString;
                oData = new cCONEXCION(conection);
            }
            catch (Exception err)
            {
                ErrorFX.mostrar(err, false, true, "frmEmpresa - 96 - ", false);
            }
            limpiar();
        }

        public void limpiar()
        {
            ROW_ID = "";
            ROW_ID_TRANSACCION = "";
            metPagoPolField = "";
            fechaField = DateTime.Now;
            benefField = "";
            rFCField = "";
            montoTotalField =0;
            monedaField = "";
            tipCambField =0;
            tipCambFieldSpecified = false;

        }


        public List<cPolizasPolizaTransaccionOtrMetodoPago> todos(string ROW_ID_TRANSACCIONp,string numCtaFieldp = "")
        {

            List<cPolizasPolizaTransaccionOtrMetodoPago> lista = new List<cPolizasPolizaTransaccionOtrMetodoPago>();

            sSQL = " SELECT * ";
            sSQL += " FROM PolizasPolizaTransaccionOtrMetodoPago ";
            sSQL += " WHERE 1=1 AND ROW_ID_TRANSACCION=" + ROW_ID_TRANSACCIONp  + "";
            DataTable oDataTable = oData.EjecutarConsulta(sSQL);
            foreach (DataRow oDataRow in oDataTable.Rows)
            {
                cPolizasPolizaTransaccionOtrMetodoPago ocCatalogo = new cPolizasPolizaTransaccionOtrMetodoPago();
                ocCatalogo.cargar(oDataRow["ROW_ID"].ToString());
                lista.Add(ocCatalogo);
            }
            return lista;

        }

        public bool verificar(string numCtaFieldp, string ROW_ID_TRANSACCIONp)
        {
            sSQL = " SELECT * ";
            sSQL += " FROM PolizasPolizaTransaccionOtrMetodoPago ";
            sSQL += " WHERE numCtaField='" + numCtaFieldp + "' ";
            sSQL += " AND ROW_ID_TRANSACCION=" + ROW_ID_TRANSACCIONp + "";
            DataTable oDataTable = oData.EjecutarConsulta(sSQL);
            if (oDataTable != null)
            {
                foreach (DataRow oDataRow in oDataTable.Rows)
                {                    
                    return true;
                }
            }
            return false;
        }

        public bool cargar(string ROW_IDp)
        {
            sSQL = " SELECT * ";
            sSQL += " FROM PolizasPolizaTransaccionOtrMetodoPago ";
            sSQL += " WHERE ROW_ID=" + ROW_IDp + " ";
            DataTable oDataTable = oData.EjecutarConsulta(sSQL);
            if (oDataTable != null)
            {
                foreach (DataRow oDataRow in oDataTable.Rows)
                {
                    //Añadir el Registro
                    ROW_ID = oDataRow["ROW_ID"].ToString();
                    ROW_ID_TRANSACCION = oDataRow["ROW_ID_TRANSACCION"].ToString();

                    metPagoPolField = oDataRow["metPagoPolField"].ToString();
                    fechaField = DateTime.Parse(oDataRow["fechaField"].ToString());
                    benefField = oDataRow["benefField"].ToString();
                    rFCField = oDataRow["rFCField"].ToString();
                    montoTotalField = decimal.Parse(oDataRow["montoTotalField"].ToString());
                    monedaField = oDataRow["monedaField"].ToString();
                    tipCambField = decimal.Parse(oDataRow["tipCambField"].ToString());
                    tipCambFieldSpecified = bool.Parse(oDataRow["tipCambFieldSpecified"].ToString());

                    return true;
                }
            }
            return false;
        }

        public bool guardar(bool validar=true)
        {
            if (ROW_ID == "")
            {
                sSQL = " INSERT INTO PolizasPolizaTransaccionOtrMetodoPago ";
                sSQL += " ( ";
                sSQL += " [ROW_ID_TRANSACCION],[metPagoPolField],[fechaField],[rFCField]";
                sSQL += ",[montoTotalField],[monedaField],[tipCambField],[tipCambFieldSpecified]";
                sSQL += " )";
                sSQL += " VALUES ";
                sSQL += "(";
                sSQL += " " + ROW_ID_TRANSACCION + ",'" + metPagoPolField + "','" + fechaField.ToString() + "','" + benefField + "' ";
                sSQL += ", " + montoTotalField.ToString() + ",'" + monedaField + "','" + tipCambField + "','" + tipCambFieldSpecified + "' ";
                sSQL += ")";
                DataTable oDataTable = oData.EjecutarConsulta(sSQL);
                if (oDataTable != null)
                {
                    sSQL = "SELECT TOP 1 MAX(ROW_ID) as ROW_ID FROM PolizasPolizaTransaccionOtrMetodoPago";
                    oDataTable = oData.EjecutarConsulta(sSQL);
                    if (oDataTable != null)
                    {
                        foreach (DataRow oDataRow in oDataTable.Rows)
                        {
                            ROW_ID = oDataRow["ROW_ID"].ToString();
                        }
                    }
                }
            }
            else
            {
                sSQL = " UPDATE PolizasPolizaTransaccionOtrMetodoPago ";
                sSQL += " SET ROW_ID_TRANSACCION=" + ROW_ID_TRANSACCION + ",metPagoPolField='" + metPagoPolField + "',fechaField='" + fechaField.ToString() + "',rFCField='" + rFCField + "'";
                sSQL += " ,montoTotalField=" + montoTotalField.ToString() + ",monedaField='" + monedaField + "',tipCambField='" + tipCambField + "',tipCambFieldSpecified='" + tipCambFieldSpecified + "' ";
                sSQL += " WHERE ROW_ID=" + ROW_ID;
                oData.EjecutarConsulta(sSQL);

            }
            return true;

        }

        private bool buscar_UUID(string uUID_CFDIFieldp)
        {
            sSQL = " SELECT * ";
            sSQL += " FROM PolizasPolizaTransaccionOtrMetodoPago ";
            sSQL += " WHERE uUID_CFDIField='" + uUID_CFDIFieldp + "' ";
            DataTable oDataTable = oData.EjecutarConsulta(sSQL);
            if (oDataTable != null)
            {
                foreach (DataRow oDataRow in oDataTable.Rows)
                {
                    return true;
                }
            }
            return false;
        }

        public bool eliminar(string ROW_IDp)
        {
            if (ROW_IDp != "")
            {
                sSQL = "DELETE FROM PolizasPolizaTransaccionOtrMetodoPago ";
                sSQL += " WHERE ROW_ID='" + ROW_IDp + "' ";
                oData.EjecutarConsulta(sSQL);
                return true;
            }
            return false;
        }
    }
}
