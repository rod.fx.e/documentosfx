using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevComponents.DotNetBar;
using System.IO;
using OfficeOpenXml;

namespace Desarrollo
{
    public partial class frmBatchs : DevComponents.DotNetBar.Metro.MetroForm
    {
        bool modificado = false;
        string ROW_ID_CATALOGO;
        public string BATCH_INICIALp="";
        public string BATCH_FINALp = "";
        public string APCp="";
        public string APIp="";
        public string ADJp="";
        public string GJp="";
        public string PURp="";                
        public string SLSp="";
        public string INDp="";
        public string FGp="";
        public string WIPp="";
        public string RVJp="";
        public string ARIp="";
        public string ARCp="";
        public string BAJp = "";

        public frmBatchs()
        {
            InitializeComponent();
            
        }


        #region Acciones
        private void guardar()
        {
            BATCH_INICIALp = BATCH_INICIAL.Text;
            BATCH_FINALp = BATCH_FINAL.Text;
            if(APC.Checked)
            {
                APCp = "APC";
            }
            if (API.Checked)
            {
                APIp = "API";
            }
            if (ADJ.Checked)
            {
                ADJp = "ADJ";
            }
            if (GJ.Checked)
            {
                GJp = "GJ";
            }
            if (PUR.Checked)
            {
                PURp = "PUR";
            }
            if (SLS.Checked)
            {
                SLSp = "SLS";
            }
            if (IND.Checked)
            {
                INDp = "IND";
            }
            if (FG.Checked)
            {
                FGp = "FG";
            }
            if (WIP.Checked)
            {
                WIPp = "WIP";
            }
            if (RVJ.Checked)
            {
                RVJp = "RVJ";
            }
            if (ARI.Checked)
            {
                ARIp = "ARI";
            }
            if (ARC.Checked)
            {
                ARCp = "ARC";
            }
            if (BAJ.Checked)
            {
                BAJp = "BAJ";
            }

            DialogResult = DialogResult.OK;
        }

        private void mostrar_mensaje(string mensaje, bool mostrar_error)
        {
            ToastNotification.Show(this,
                mensaje,
                mostrar_error ? global::Desarrollo.Properties.Resources.error : global::Desarrollo.Properties.Resources.guardar,
                3000,
                mostrar_error ? eToastGlowColor.Red : eToastGlowColor.Green,
                eToastPosition.TopCenter);
        }

        #endregion

        private void buttonX3_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Cancel;
        }

        private void buttonX1_Click(object sender, EventArgs e)
        {
            guardar();
        }

    }
}