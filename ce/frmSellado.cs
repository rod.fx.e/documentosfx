using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevComponents.DotNetBar;
using System.IO;
using OfficeOpenXml;
using System.Collections;

namespace Desarrollo
{
    public partial class frmSellado : DevComponents.DotNetBar.Metro.MetroForm
    {
        string ROW_ID_CATALOGO = "";
        cEMPRESA_CE ocEMPRESA;
        string tipo = "";
        Catalogo oCatalogo;

        public frmSellado(string ROW_ID_CATALOGOp, Object ocEMPRESAp
            , string tipop, Catalogo cCatalogop)
        {
            ROW_ID_CATALOGO = ROW_ID_CATALOGOp;

            ocEMPRESA = new cEMPRESA_CE();
            ocEMPRESA = (cEMPRESA_CE)ocEMPRESAp;

            //AuxiliarCtas
            //AuxiliarFolios
            //Polizas
            tipo = tipop;
            oCatalogo = cCatalogop;
            InitializeComponent();
           
        }


        #region Acciones

        private void mostrar_mensaje(string mensaje, bool mostrar_error)
        {
            ToastNotification.Show(this,
                mensaje,
                mostrar_error ? global::Desarrollo.Properties.Resources.error : global::Desarrollo.Properties.Resources.guardar,
                3000,
                mostrar_error ? eToastGlowColor.Red : eToastGlowColor.Green,
                eToastPosition.TopCenter);
        }
        
        #endregion

        private void buttonX3_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Cancel;
        }

        private void buttonX1_Click(object sender, EventArgs e)
        {
            guardar();
        }

        private void guardar()
        {
            
        }

        private void TIPO_POLIZA_SelectedIndexChanged(object sender, EventArgs e)
        {
            verificar();
        }

        private void verificar()
        {

        }

        private void buttonX22_Click(object sender, EventArgs e)
        {
            generar_sellado();
        }

        private void generar_sellado()
        {

            SelloDigitalContElec oSelloDigitalContElec = new SelloDigitalContElec();
            oSelloDigitalContElec.FechadeSello = DateTime.Now;
            oSelloDigitalContElec.noCertificadoSAT = oCatalogo.noCertificado;
            oSelloDigitalContElec.sello = oCatalogo.Sello;
            
        }

    }
}