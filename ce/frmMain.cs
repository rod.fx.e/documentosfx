using DevComponents.DotNetBar;
using Generales;
using ModeloDocumentosFX;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Drawing;
using System.Text;
using System.Threading;
using System.Windows.Forms;

namespace Desarrollo
{
    public partial class frmMain : DevComponents.DotNetBar.Metro.MetroAppForm
    {
        private cCONEXCION oData = new cCONEXCION("");
        private cEMPRESA_CE ocEMPRESA;
        string USUARIO;
        BackgroundWorker bgwAsignado;

        public frmMain(string sConn, string USUARIOp)
        {
            //Cargar los datos del Entity Framwork 
            ModeloDocumentosFXContainer dbContext = new ModeloDocumentosFXContainer();
            try
            {
                string conection = dbContext.Database.Connection.ConnectionString;
                oData = new cCONEXCION(conection);
            }
            catch (Exception err)
            {
                ErrorFX.mostrar(err, false, true, "frmEmpresa - 68 - ", false);
            }

            USUARIO = USUARIOp;
            InitializeComponent();
            iniciar();
            this.Text = Application.ProductName + "-" + Application.ProductVersion.ToString() + " Usuario: " + USUARIO;
            

        }

        private void iniciar()
        {
            //Importar
            //ucImportar1 = new ucImportar(sConn);

            cargar_configuracion();
            cargar_seguridad();
            circularProgress1.Visible = false;
            circularProgress1.IsRunning = true;
            cargar_empresas();

        }

        private void cargar_configuracion()
        {
            bool IMPORTAR = true;
            bool COMPROBANTES_ASIGNADOS = true;
            try
            {
                IMPORTAR=bool.Parse(ConfigurationManager.AppSettings.Get("IMPORTAR").ToString());

            }
            catch
            {

            }
            try
            {
                COMPROBANTES_ASIGNADOS = bool.Parse(ConfigurationManager.AppSettings.Get("COMPROBANTES_ASIGNADOS").ToString());
                ribbonTabItem2.Visible = COMPROBANTES_ASIGNADOS;
                ribbonPanel2.Visible = COMPROBANTES_ASIGNADOS;

            }
            catch
            {

            }
            ribbonTabItem3.Visible = IMPORTAR;
            ribbonPanel3.Visible = IMPORTAR;


            
        }

        private void cargar_seguridad()
        {
            btnSeguridad.Visible = false;
            if (USUARIO == "SYSADM")
            {
                btnSeguridad.Visible = true;
            }
            //cCE_SEGURIDAD ocCONTRATO_SEGURIDAD = new cCE_SEGURIDAD();
            //if (ocCONTRATO_SEGURIDAD.cargar_usuario(USUARIO))
            //{
            //    if (bool.Parse(ocCONTRATO_SEGURIDAD.SOLO_CONTRATO))
            //    {
            //        ribbonTabItem1.Visible = false;
            //        ribbonPanel1.Visible = false;
            //        ribbonTabItem2.Select();
            //        ribbonPanel2.Select();
            //    }

            //}
        }

        private void buttonNew_Click(object sender, EventArgs e)
        {
            nueva_empresa();
        }

        private void cargar_asignado()
        {
            ribbonTabItem1.Focus();
            ribbonPanel1.Focus();
            if (EMPRESA.SelectedItem != null)
            {
                dtgTransaccion.Rows.Clear();
                bgwAsignado = new BackgroundWorker();
                bgwAsignado.WorkerSupportsCancellation = true;
                bgwAsignado.WorkerReportsProgress = true;
                bgwAsignado.DoWork += new DoWorkEventHandler(bgwAsignado_DoWork);
                bgwAsignado.ProgressChanged += new ProgressChangedEventHandler(bgwAsignado_ProgressChanged);
                bgwAsignado.RunWorkerCompleted += new RunWorkerCompletedEventHandler(bgwAsignado_RunWorkerCompleted);
                bgwAsignado.RunWorkerAsync();
            }
        }

        private void bgwAsignado_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            //throw new NotImplementedException();
        }

        private void bgwAsignado_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            //throw new NotImplementedException();
        }

        private void bgwAsignado_DoWork(object sender, DoWorkEventArgs e)
        {
            //ribbonTabItem1.Focus();
            //ribbonPanel1.Focus();
            //if (EMPRESA.SelectedItem != null)
            //{
                //Cargar las asignaciones 
                
                cPoliza ocPoliza = new cPoliza();
                List<cPoliza> lista = ocPoliza.todos("", "", USUARIO);
                foreach (cPoliza registro in lista)
                {
                    actualizar_linea_poliza(registro);
                }
                //if (lista.Count > 0)
                //{
                //    ribbonTabItem2.Focus();
                //    ribbonPanel2.Focus();
                //}
                //else
                //{

                //}
            //}
        }
        private void actualizar_linea_poliza(cPoliza registro)
        {
            if (dtgTransaccion.InvokeRequired)
            {
                MethodInvoker invoker = new MethodInvoker(delegate
                {
                    int n = dtgTransaccion.Rows.Add();
                    insertar_lina_poliza(registro, n);
                });
                dtgTransaccion.Invoke(invoker);
            }
            else
            {
                int n = dtgTransaccion.Rows.Add();
                insertar_lina_poliza(registro, n);
            } 
        }
        private void insertar_lina_poliza(cPoliza registro, int n)
        {
            for (int i = 0; i < dtgTransaccion.Columns.Count; i++)
            {
                dtgTransaccion.Rows[n].Cells[i].Value = "";
            }
            dtgTransaccion.Rows[n].Tag = registro;
            dtgTransaccion.Rows[n].Cells["ROW_ID_tr"].Value = registro.ROW_ID;
            dtgTransaccion.Rows[n].Cells["ROW_ID_CATALOGO"].Value = registro.ROW_ID_CATALOGO;
            dtgTransaccion.Rows[n].Cells["tipoField"].Value = registro.tipoField;
            dtgTransaccion.Rows[n].Cells["numField"].Value = registro.numField;

            dtgTransaccion.Rows[n].Cells["conceptoField"].Value = registro.conceptoField;
        }


        private void nueva_empresa()
        {
            frmEmpresa ofrmEmpresa = new frmEmpresa();
            ofrmEmpresa.ShowDialog();
            cargar_empresas();
            
        }

        private void cargar_empresas()
        {
            //Limpiar el treeview
            cEMPRESA_CE ocEMPRESA = new cEMPRESA_CE();
            EMPRESA.Items.Clear();
            foreach (cEMPRESA_CE registro in ocEMPRESA.todos())
            {
                
                DevComponents.DotNetBar.ItemContainer oItem = new DevComponents.DotNetBar.ItemContainer();
                oItem.Text = registro.ID;
                oItem.Tag = registro;
                EMPRESA.Items.Add(oItem);
            }
            if(EMPRESA.Items.Count>0)
            {
                EMPRESA.SelectedIndex = 0;
                
            }
        }

        private void buttonItem3_Click(object sender, EventArgs e)
        {
            nueva_declaracion();
        }

        private void nueva_declaracion()
        {
            if (EMPRESA.SelectedItem == null)
            {
                MessageBoxEx.Show("Selecione una empresa para continuar.", Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            DevComponents.DotNetBar.ItemContainer oItem = (ItemContainer)EMPRESA.SelectedItem;
            ocEMPRESA = (cEMPRESA_CE)oItem.Tag;
            frmDeclaracion ofrmDeclaracion = new frmDeclaracion(ocEMPRESA,USUARIO);
            ofrmDeclaracion.FormClosed+= frmDeclaracion_FormClosed;
            ofrmDeclaracion.Show();
            
        }
        private void frmDeclaracion_FormClosed(object sender, FormClosedEventArgs e)
        {
            cargar();
        }
        private void frmMain_FormClosed(object sender, FormClosedEventArgs e)
        {
            //Application.Exit();
        }

        private void buttonX3_Click(object sender, EventArgs e)
        {
            cargar();
        }

        private void cargar()
        {
            if (EMPRESA.SelectedItem == null)
            {
                MessageBoxEx.Show("Selecione una empresa para continuar.", Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            if (btnBuscar.Text == "Detener")
            {
                if (backgroundWorker1.IsBusy)
                {
                    backgroundWorker1.CancelAsync();
                    cambiar_boton();

                }
                return;
            }

            DevComponents.DotNetBar.ItemContainer oItem = (ItemContainer)EMPRESA.SelectedItem;
            ocEMPRESA = (cEMPRESA_CE)oItem.Tag;
            circularProgress1.Visible = true;
            circularProgress1.IsRunning = true;
            cambiar_boton();
            if (!backgroundWorker1.IsBusy)
            {
                dtgrdGeneral.Rows.Clear();
                List<object> arguments = new List<object>();
                arguments.Add(ocEMPRESA.RFC);
                arguments.Add(A�O.Text);
                arguments.Add(MES.Text);
                arguments.Add(ESTADO.Text);
                AnioTr = A�O.Text;
                MesTr = MES.Text;
                EstadoTr = ESTADO.Text;

                backgroundWorker1.RunWorkerAsync(arguments);

            }
        }
        string AnioTr = "";
        string MesTr = "";
        string EstadoTr = "";

        private void cancelar()
        {
            if (backgroundWorker1.IsBusy)
            {
                backgroundWorker1.CancelAsync();
                cambiar_boton();
            }
        }
        private void backgroundWorker1_DoWork(object sender, DoWorkEventArgs e)
        {
            try
            {
                int n = 0;
                cCatalogo ocCatalogo = new cCatalogo();
                labelItem1.Text = "Cargando ";
                dtgrdGeneral.Rows.Clear();
                try
                {

                    List<object> genericlist = e.Argument as List<object>;

                    foreach (cCatalogo registro in ocCatalogo.todos(ocEMPRESA.RFC,AnioTr,MesTr
                        ,EstadoTr))
                    {
                        n++;
                        //Thread.Sleep(100);

                        backgroundWorker1.ReportProgress(n, registro);
                        if (backgroundWorker1.CancellationPending)
                        {
                            e.Cancel = true;
                            backgroundWorker1.ReportProgress(0);
                            return;
                        }
                    }

                }
                catch
                {
                    backgroundWorker1.CancelAsync();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void backgroundWorker1_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            try
            {
                if (!backgroundWorker1.CancellationPending)
                {
                    cCatalogo registro = (cCatalogo)e.UserState;
                    if (registro != null)
                    {
                        
                        int n = dtgrdGeneral.Rows.Add();
                        actualizar_linea(registro, n);
                        circularProgress1.Value = e.ProgressPercentage;
                        circularProgress1.ProgressText = "Procesando.. " + e.ProgressPercentage.ToString();// + " de " + TotalRecords;
                    }
                }
            }
            catch (Exception excep)
            {
                MessageBoxEx.Show(excep.ToString());
                backgroundWorker1.CancelAsync();
                cambiar_boton();
                return;
            }
        }

        private void backgroundWorker1_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            if (e.Cancelled)
            {
                labelItem1.Text = "Cancelado por el usuario...";
                circularProgress1.Value = 0;
                cambiar_boton();
            }
            // Check to see if an error occurred in the background process.
            else if (e.Error != null)
            {
                labelItem1.Text = e.Error.Message;
            }
            else
            {
                //BackGround Task Completed with out Error
                labelItem1.Text = " Finalizado...";
            }
            cargar_asignado();
            cambiar_boton();
            
            circularProgress1.Visible = false;
        }



        private void cambiar_boton()
        {
            if (btnBuscar.Text == "Buscar")
            {
                btnBuscar.Image = global::Desarrollo.Properties.Resources.stop_32;
                btnBuscar.Text = "Detener";
            }
            else
            {
                btnBuscar.Image = global::Desarrollo.Properties.Resources.buscar_32;
                btnBuscar.Text = "Buscar";
            }
        }
    
        private void buttonX2_Click(object sender, EventArgs e)
        {
            //nueva_empresa();
            frmEmpresas ofrmEmpresas = new frmEmpresas(oData.sConn);
            ofrmEmpresas.ShowDialog();
            cargar_empresas();
        }

        private void btnNuevo_Click(object sender, EventArgs e)
        {
            nueva_declaracion();
        }

        private void dtgrdGeneral_DoubleClick(object sender, EventArgs e)
        {

        }

        private void dtgrdGeneral_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            editar();
        }

        private void editar()
        {
            dtgrdGeneral.EndEdit();
            if (dtgrdGeneral.SelectedCells.Count > 0)
            {
                DataGridViewRow drv = dtgrdGeneral.Rows[dtgrdGeneral.SelectedCells[0].RowIndex];

                string ROW_ID = drv.Cells["ROW_ID"].Value.ToString();
                object registro = (object)drv.Tag;
                frmDeclaracion ofrmDeclaracion = new frmDeclaracion(ocEMPRESA, ROW_ID,USUARIO);
                ofrmDeclaracion.ShowDialog();
                cCatalogo ocCatalogo = new cCatalogo();
                if (ocCatalogo.cargar(ROW_ID))
                {
                    actualizar_linea(ocCatalogo, drv.Index);
                }
                else
                {
                    //Eliminar el registro
                    dtgrdGeneral.Rows.Remove(drv);
                }

            }
            else
            {
                MessageBoxEx.Show("Seleccione alguna l�nea.", Application.ProductName + "-" + Application.ProductVersion, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }

        private void actualizar_linea(cCatalogo registro, int n)
        {
            for (int i = 0; i < dtgrdGeneral.Columns.Count; i++)
            {
                dtgrdGeneral.Rows[n].Cells[i].Value = "";
            }
            dtgrdGeneral.Rows[n].Tag = registro;
            dtgrdGeneral.Rows[n].Cells["ROW_ID"].Value = registro.ROW_ID;
            dtgrdGeneral.Rows[n].Cells["MES_col"].Value = registro.mesField;
            dtgrdGeneral.Rows[n].Cells["A�O_col"].Value = registro.anoField;
            dtgrdGeneral.Rows[n].Cells["VERSION_col"].Value = registro.versionField;
            dtgrdGeneral.Rows[n].Cells["ESTADO_col"].Value = registro.ESTADO;

        }

        private void buttonItem20_Click(object sender, EventArgs e)
        {
            frmEmpresas ofrmEmpresas = new frmEmpresas(oData.sConn);
            ofrmEmpresas.ShowDialog();
            cargar_empresas();
        }

        private void buttonItem2_Click(object sender, EventArgs e)
        {
            frmPrincipal ofrmPrincipal = new frmPrincipal(oData.sConn);
            ofrmPrincipal.ShowDialog();
        }

        private void EMPRESA_SelectedIndexChanged(object sender, EventArgs e)
        {
            cargar();
        }

        private void ribbonControl1_Click(object sender, EventArgs e)
        {

        }

        private void buttonX18_Click(object sender, EventArgs e)
        {
            cargar_asignado();
        }

        private void dtgTransaccion_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            editar_poliza();
        }

        private void editar_poliza()
        {
            dtgTransaccion.EndEdit();
            if (dtgTransaccion.SelectedRows.Count > 0)
            {
                foreach (DataGridViewRow drv in dtgTransaccion.SelectedRows)
                {
                    string ROW_IDt = drv.Cells["ROW_ID_tr"].Value.ToString();
                    string ROW_ID_CATALOGOt = drv.Cells["ROW_ID_CATALOGO"].Value.ToString();
                    frmPoliza ofrm = new frmPoliza(ROW_ID_CATALOGOt, ROW_IDt, USUARIO);
                    ofrm.ShowDialog();
                }
            }
            else
            {
                MessageBoxEx.Show("Seleccione alguna l�nea.", Application.ProductName + "-" + Application.ProductVersion, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }

        //private void buttonItem3_Click_1(object sender, EventArgs e)
        //{
        //    frmSEGURIDAD ofrmSEGURIDAD = new frmSEGURIDAD(oData.sConn);
        //    ofrmSEGURIDAD.ShowDialog();
        //}

        private void ucImportar1_Load(object sender, EventArgs e)
        {

        }

        private void ribbonTabItem1_Click(object sender, EventArgs e)
        {
            iniciar();
        }



    }
}