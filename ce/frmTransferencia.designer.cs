namespace Desarrollo
{
    partial class frmTransferencia
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            DevComponents.DotNetBar.ButtonX buttonX3;
            DevComponents.DotNetBar.ButtonX buttonX1;
            DevComponents.DotNetBar.ButtonX buttonX2;
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmTransferencia));
            this.metroStatusBar1 = new DevComponents.DotNetBar.Metro.MetroStatusBar();
            this.labelItem1 = new DevComponents.DotNetBar.LabelItem();
            this.rFCField = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.labelX2 = new DevComponents.DotNetBar.LabelX();
            this.benefField = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.labelX16 = new DevComponents.DotNetBar.LabelX();
            this.bancoOriField = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.labelX3 = new DevComponents.DotNetBar.LabelX();
            this.ctaOriField = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.labelX4 = new DevComponents.DotNetBar.LabelX();
            this.labelX5 = new DevComponents.DotNetBar.LabelX();
            this.fechaField = new System.Windows.Forms.DateTimePicker();
            this.labelX6 = new DevComponents.DotNetBar.LabelX();
            this.montoField = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.bancoDestField = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.ctaDestField = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.labelX7 = new DevComponents.DotNetBar.LabelX();
            this.labelX8 = new DevComponents.DotNetBar.LabelX();
            buttonX3 = new DevComponents.DotNetBar.ButtonX();
            buttonX1 = new DevComponents.DotNetBar.ButtonX();
            buttonX2 = new DevComponents.DotNetBar.ButtonX();
            this.SuspendLayout();
            // 
            // buttonX3
            // 
            buttonX3.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            buttonX3.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            buttonX3.Image = global::Desarrollo.Properties.Resources.Eliminar_18;
            buttonX3.Location = new System.Drawing.Point(141, 0);
            buttonX3.Name = "buttonX3";
            buttonX3.Size = new System.Drawing.Size(68, 24);
            buttonX3.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            buttonX3.TabIndex = 1;
            buttonX3.Text = "Eliminar";
            buttonX3.TextAlignment = DevComponents.DotNetBar.eButtonTextAlignment.Right;
            buttonX3.Click += new System.EventHandler(this.buttonX3_Click);
            // 
            // buttonX1
            // 
            buttonX1.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            buttonX1.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            buttonX1.Image = global::Desarrollo.Properties.Resources.Nueno_18;
            buttonX1.Location = new System.Drawing.Point(67, 0);
            buttonX1.Name = "buttonX1";
            buttonX1.Size = new System.Drawing.Size(68, 24);
            buttonX1.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            buttonX1.TabIndex = 1;
            buttonX1.Text = "Guardar";
            buttonX1.TextAlignment = DevComponents.DotNetBar.eButtonTextAlignment.Right;
            buttonX1.Click += new System.EventHandler(this.buttonX1_Click);
            // 
            // buttonX2
            // 
            buttonX2.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            buttonX2.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            buttonX2.Image = global::Desarrollo.Properties.Resources.Nuevo_18_19;
            buttonX2.Location = new System.Drawing.Point(0, 0);
            buttonX2.Name = "buttonX2";
            buttonX2.Size = new System.Drawing.Size(61, 24);
            buttonX2.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            buttonX2.TabIndex = 1;
            buttonX2.Text = "Nuevo";
            buttonX2.TextAlignment = DevComponents.DotNetBar.eButtonTextAlignment.Right;
            buttonX2.Click += new System.EventHandler(this.buttonX2_Click);
            // 
            // metroStatusBar1
            // 
            this.metroStatusBar1.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.metroStatusBar1.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.metroStatusBar1.ContainerControlProcessDialogKey = true;
            this.metroStatusBar1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.metroStatusBar1.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.metroStatusBar1.ForeColor = System.Drawing.Color.Black;
            this.metroStatusBar1.Items.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.labelItem1});
            this.metroStatusBar1.Location = new System.Drawing.Point(0, 256);
            this.metroStatusBar1.Name = "metroStatusBar1";
            this.metroStatusBar1.Size = new System.Drawing.Size(357, 21);
            this.metroStatusBar1.TabIndex = 8;
            // 
            // labelItem1
            // 
            this.labelItem1.Name = "labelItem1";
            // 
            // rFCField
            // 
            this.rFCField.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.rFCField.Border.Class = "TextBoxBorder";
            this.rFCField.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.rFCField.ForeColor = System.Drawing.Color.Black;
            this.rFCField.Location = new System.Drawing.Point(83, 227);
            this.rFCField.Name = "rFCField";
            this.rFCField.Size = new System.Drawing.Size(274, 22);
            this.rFCField.TabIndex = 546;
            this.rFCField.TabStop = false;
            this.rFCField.WatermarkBehavior = DevComponents.DotNetBar.eWatermarkBehavior.HideNonEmpty;
            // 
            // labelX2
            // 
            this.labelX2.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.labelX2.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX2.ForeColor = System.Drawing.Color.Black;
            this.labelX2.Location = new System.Drawing.Point(21, 227);
            this.labelX2.Name = "labelX2";
            this.labelX2.Size = new System.Drawing.Size(56, 22);
            this.labelX2.TabIndex = 580;
            this.labelX2.Text = "RFC";
            this.labelX2.TextAlignment = System.Drawing.StringAlignment.Far;
            // 
            // benefField
            // 
            this.benefField.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.benefField.Border.Class = "TextBoxBorder";
            this.benefField.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.benefField.ForeColor = System.Drawing.Color.Black;
            this.benefField.Location = new System.Drawing.Point(83, 199);
            this.benefField.Name = "benefField";
            this.benefField.Size = new System.Drawing.Size(274, 22);
            this.benefField.TabIndex = 548;
            this.benefField.TabStop = false;
            this.benefField.WatermarkBehavior = DevComponents.DotNetBar.eWatermarkBehavior.HideNonEmpty;
            // 
            // labelX16
            // 
            this.labelX16.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.labelX16.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX16.ForeColor = System.Drawing.Color.Black;
            this.labelX16.Location = new System.Drawing.Point(16, 197);
            this.labelX16.Name = "labelX16";
            this.labelX16.Size = new System.Drawing.Size(61, 22);
            this.labelX16.TabIndex = 579;
            this.labelX16.Text = "Beneficiario";
            this.labelX16.TextAlignment = System.Drawing.StringAlignment.Far;
            // 
            // bancoOriField
            // 
            this.bancoOriField.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.bancoOriField.Border.Class = "TextBoxBorder";
            this.bancoOriField.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.bancoOriField.ForeColor = System.Drawing.Color.Black;
            this.bancoOriField.Location = new System.Drawing.Point(83, 57);
            this.bancoOriField.Name = "bancoOriField";
            this.bancoOriField.Size = new System.Drawing.Size(274, 22);
            this.bancoOriField.TabIndex = 548;
            this.bancoOriField.TabStop = false;
            this.bancoOriField.WatermarkBehavior = DevComponents.DotNetBar.eWatermarkBehavior.HideNonEmpty;
            // 
            // labelX3
            // 
            this.labelX3.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.labelX3.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX3.ForeColor = System.Drawing.Color.Black;
            this.labelX3.Location = new System.Drawing.Point(0, 55);
            this.labelX3.Name = "labelX3";
            this.labelX3.Size = new System.Drawing.Size(77, 22);
            this.labelX3.TabIndex = 579;
            this.labelX3.Text = "Banco Origen";
            this.labelX3.TextAlignment = System.Drawing.StringAlignment.Far;
            // 
            // ctaOriField
            // 
            this.ctaOriField.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.ctaOriField.Border.Class = "TextBoxBorder";
            this.ctaOriField.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.ctaOriField.ForeColor = System.Drawing.Color.Black;
            this.ctaOriField.Location = new System.Drawing.Point(83, 30);
            this.ctaOriField.Name = "ctaOriField";
            this.ctaOriField.Size = new System.Drawing.Size(274, 22);
            this.ctaOriField.TabIndex = 548;
            this.ctaOriField.TabStop = false;
            this.ctaOriField.WatermarkBehavior = DevComponents.DotNetBar.eWatermarkBehavior.HideNonEmpty;
            // 
            // labelX4
            // 
            this.labelX4.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.labelX4.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX4.ForeColor = System.Drawing.Color.Black;
            this.labelX4.Location = new System.Drawing.Point(0, 28);
            this.labelX4.Name = "labelX4";
            this.labelX4.Size = new System.Drawing.Size(77, 22);
            this.labelX4.TabIndex = 579;
            this.labelX4.Text = "Cuenta Origen";
            this.labelX4.TextAlignment = System.Drawing.StringAlignment.Far;
            // 
            // labelX5
            // 
            this.labelX5.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.labelX5.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX5.ForeColor = System.Drawing.Color.Black;
            this.labelX5.Location = new System.Drawing.Point(24, 116);
            this.labelX5.Name = "labelX5";
            this.labelX5.Size = new System.Drawing.Size(53, 22);
            this.labelX5.TabIndex = 579;
            this.labelX5.Text = "Fecha";
            this.labelX5.TextAlignment = System.Drawing.StringAlignment.Far;
            // 
            // fechaField
            // 
            this.fechaField.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.fechaField.Location = new System.Drawing.Point(83, 116);
            this.fechaField.Name = "fechaField";
            this.fechaField.Size = new System.Drawing.Size(93, 22);
            this.fechaField.TabIndex = 581;
            // 
            // labelX6
            // 
            this.labelX6.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            // 
            // 
            // 
            this.labelX6.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX6.ForeColor = System.Drawing.Color.Black;
            this.labelX6.Location = new System.Drawing.Point(13, 82);
            this.labelX6.Name = "labelX6";
            this.labelX6.Size = new System.Drawing.Size(61, 22);
            this.labelX6.TabIndex = 588;
            this.labelX6.Text = "Monto";
            this.labelX6.TextAlignment = System.Drawing.StringAlignment.Far;
            // 
            // montoField
            // 
            this.montoField.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            // 
            // 
            // 
            this.montoField.Border.Class = "TextBoxBorder";
            this.montoField.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.montoField.ForeColor = System.Drawing.Color.Black;
            this.montoField.Location = new System.Drawing.Point(83, 84);
            this.montoField.Name = "montoField";
            this.montoField.Size = new System.Drawing.Size(126, 22);
            this.montoField.TabIndex = 589;
            this.montoField.TabStop = false;
            this.montoField.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.montoField.WatermarkBehavior = DevComponents.DotNetBar.eWatermarkBehavior.HideNonEmpty;
            this.montoField.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.Solo_Numero_KeyPress);
            // 
            // bancoDestField
            // 
            this.bancoDestField.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.bancoDestField.Border.Class = "TextBoxBorder";
            this.bancoDestField.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.bancoDestField.ForeColor = System.Drawing.Color.Black;
            this.bancoDestField.Location = new System.Drawing.Point(83, 171);
            this.bancoDestField.Name = "bancoDestField";
            this.bancoDestField.Size = new System.Drawing.Size(274, 22);
            this.bancoDestField.TabIndex = 548;
            this.bancoDestField.TabStop = false;
            this.bancoDestField.WatermarkBehavior = DevComponents.DotNetBar.eWatermarkBehavior.HideNonEmpty;
            // 
            // ctaDestField
            // 
            this.ctaDestField.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.ctaDestField.Border.Class = "TextBoxBorder";
            this.ctaDestField.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.ctaDestField.ForeColor = System.Drawing.Color.Black;
            this.ctaDestField.Location = new System.Drawing.Point(83, 144);
            this.ctaDestField.Name = "ctaDestField";
            this.ctaDestField.Size = new System.Drawing.Size(274, 22);
            this.ctaDestField.TabIndex = 548;
            this.ctaDestField.TabStop = false;
            this.ctaDestField.WatermarkBehavior = DevComponents.DotNetBar.eWatermarkBehavior.HideNonEmpty;
            // 
            // labelX7
            // 
            this.labelX7.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.labelX7.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX7.ForeColor = System.Drawing.Color.Black;
            this.labelX7.Location = new System.Drawing.Point(0, 169);
            this.labelX7.Name = "labelX7";
            this.labelX7.Size = new System.Drawing.Size(77, 22);
            this.labelX7.TabIndex = 579;
            this.labelX7.Text = "Banco Destino";
            this.labelX7.TextAlignment = System.Drawing.StringAlignment.Far;
            // 
            // labelX8
            // 
            this.labelX8.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.labelX8.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX8.ForeColor = System.Drawing.Color.Black;
            this.labelX8.Location = new System.Drawing.Point(0, 142);
            this.labelX8.Name = "labelX8";
            this.labelX8.Size = new System.Drawing.Size(77, 22);
            this.labelX8.TabIndex = 579;
            this.labelX8.Text = "Cuenta Destino";
            this.labelX8.TextAlignment = System.Drawing.StringAlignment.Far;
            // 
            // frmTransferencia
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.BottomLeftCornerSize = 0;
            this.ClientSize = new System.Drawing.Size(357, 277);
            this.Controls.Add(this.labelX6);
            this.Controls.Add(this.montoField);
            this.Controls.Add(this.fechaField);
            this.Controls.Add(this.labelX2);
            this.Controls.Add(this.labelX16);
            this.Controls.Add(this.labelX5);
            this.Controls.Add(this.labelX8);
            this.Controls.Add(this.labelX4);
            this.Controls.Add(this.labelX7);
            this.Controls.Add(this.labelX3);
            this.Controls.Add(this.benefField);
            this.Controls.Add(this.ctaDestField);
            this.Controls.Add(this.bancoDestField);
            this.Controls.Add(this.ctaOriField);
            this.Controls.Add(this.bancoOriField);
            this.Controls.Add(this.rFCField);
            this.Controls.Add(buttonX3);
            this.Controls.Add(buttonX1);
            this.Controls.Add(buttonX2);
            this.Controls.Add(this.metroStatusBar1);
            this.Cursor = System.Windows.Forms.Cursors.Default;
            this.DoubleBuffered = true;
            this.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "frmTransferencia";
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Transferencia";
            this.ResumeLayout(false);

        }

        #endregion

        private DevComponents.DotNetBar.Metro.MetroStatusBar metroStatusBar1;
        private DevComponents.DotNetBar.LabelItem labelItem1;
        private DevComponents.DotNetBar.Controls.TextBoxX rFCField;
        private DevComponents.DotNetBar.LabelX labelX2;
        private DevComponents.DotNetBar.Controls.TextBoxX benefField;
        private DevComponents.DotNetBar.LabelX labelX16;
        private DevComponents.DotNetBar.Controls.TextBoxX bancoOriField;
        private DevComponents.DotNetBar.LabelX labelX3;
        private DevComponents.DotNetBar.Controls.TextBoxX ctaOriField;
        private DevComponents.DotNetBar.LabelX labelX4;
        private DevComponents.DotNetBar.LabelX labelX5;
        private System.Windows.Forms.DateTimePicker fechaField;
        private DevComponents.DotNetBar.LabelX labelX6;
        private DevComponents.DotNetBar.Controls.TextBoxX montoField;
        private DevComponents.DotNetBar.Controls.TextBoxX bancoDestField;
        private DevComponents.DotNetBar.Controls.TextBoxX ctaDestField;
        private DevComponents.DotNetBar.LabelX labelX7;
        private DevComponents.DotNetBar.LabelX labelX8;

    }
}