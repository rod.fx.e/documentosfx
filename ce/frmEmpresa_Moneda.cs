using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevComponents.DotNetBar;
using Generales;

namespace Desarrollo
{
    public partial class frmEmpresa_Moneda : DevComponents.DotNetBar.Metro.MetroForm
    {
        bool modificado = false;
        private cCONEXCION oData = new cCONEXCION();
        cEMPRESA_MONEDA oObjeto = new cEMPRESA_MONEDA();
        string ROW_ID_EMPRESA = "";

        public frmEmpresa_Moneda(string ROW_ID_EMPRESAp)
        {
            ROW_ID_EMPRESA = ROW_ID_EMPRESAp;
            oObjeto = new cEMPRESA_MONEDA();
            InitializeComponent();
            
        }
        public frmEmpresa_Moneda(string ROW_IDp, string ROW_ID_EMPRESAp)
        {
            ROW_ID_EMPRESA = ROW_ID_EMPRESAp;
            oObjeto = new cEMPRESA_MONEDA();
            oObjeto.ROW_ID = ROW_IDp;
            InitializeComponent();
            cargar(ROW_IDp);
        }

        private void cargar(string ROW_IDp)
        {
            if (oObjeto.cargar(ROW_IDp))
            {

                VISUAL_MONEDA.Text = oObjeto.VISUAL_MONEDA;
                MONEDA.Text = oObjeto.MONEDA;
               
                toolStripStatusLabel1.Text = "Datos Actualizados " + DateTime.Now.ToShortDateString() + " " + DateTime.Now.ToLongTimeString();
                modificado = false;
            }
        }
        private void buttonX2_Click(object sender, EventArgs e)
        {
            limpiar();
        }

        private void limpiar()
        {
            if (modificado)
            {

                DialogResult Resultado = MessageBoxEx.Show("�Desea guardar los cambios?", Application.ProductName + "-" + Application.ProductVersion, MessageBoxButtons.YesNo);
                if (Resultado.ToString() == "Yes")
                {
                    guardar();
                }


            }
            modificado = false;
            oObjeto = new cEMPRESA_MONEDA();

            foreach (var c in this.Controls)
            {
                if (c is TextBox)
                {
                    ((TextBox)c).Text = "";
                }
            }
        }

        private void buttonX1_Click(object sender, EventArgs e)
        {
            guardar();
        }

        private void guardar()
        {
            oObjeto.ROW_ID_EMPRESA = ROW_ID_EMPRESA;
            oObjeto.VISUAL_MONEDA = VISUAL_MONEDA.Text;
            oObjeto.MONEDA = MONEDA.Text;
            if (oObjeto.guardar())
            {
                toolStripStatusLabel1.Text = "Datos Actualizados " + DateTime.Now.ToShortDateString() + " " + DateTime.Now.ToLongTimeString();
                modificado = false;
            }
        }

        private void buttonX3_Click(object sender, EventArgs e)
        {
            eliminar();
        }

        private void eliminar()
        {

            DialogResult Resultado = MessageBoxEx.Show("�Desea eliminar?", Application.ProductName + "-" + Application.ProductVersion, MessageBoxButtons.YesNo);
            if (Resultado.ToString() == "Yes")
            {
                if (oObjeto.eliminar(oObjeto.ROW_ID))
                {
                    modificado = false;
                    limpiar();
                }
            }
        }

        private void frmEmpresa_KeyPress(object sender, KeyPressEventArgs e)
        {
            modificado = true;
        }
    }
}