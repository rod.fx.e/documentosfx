﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using DevComponents.DotNetBar;

namespace Desarrollo
{
    public partial class ucDeclaracion : UserControl
    {
        bool modificado = false;
        cCatalogo oObjeto;
        public ucDeclaracion()
        {
            oObjeto=new cCatalogo();
            InitializeComponent();
        }

        public bool guardar()
        {
            oObjeto.versionField = versionField.Text;
            oObjeto.rFCField = rFCField.Text;
            oObjeto.mesField = mesField.Text;
            oObjeto.anoField = anoField.Text;
            oObjeto.totalCtasField = totalCtasField.Text;
            oObjeto.ESTADO = ESTADO.Text;
            if (oObjeto.guardar())
            {
                mostrar_mensaje("Datos Actualizados ", false);
                return true;
            }
            return false;
        }

        private void mostrar_mensaje(string mensaje, bool mostrar_error)
        {
            ToastNotification.Show(this,
                mensaje,
                mostrar_error ? global::Desarrollo.Properties.Resources.error : global::Desarrollo.Properties.Resources.guardar,
                3000,
                mostrar_error ? eToastGlowColor.Red : eToastGlowColor.Green,
                eToastPosition.TopCenter);
        }

        public void cargar(string ROW_IDp)
        {
            if (oObjeto.cargar(ROW_IDp))
            {
                versionField.Text = oObjeto.versionField;
                rFCField.Text = oObjeto.rFCField;
                mesField.Text = oObjeto.mesField;
                anoField.Text = oObjeto.anoField;
                totalCtasField.Text = oObjeto.totalCtasField;
                ESTADO.Text = oObjeto.ESTADO;
            }

        }
    }
}
