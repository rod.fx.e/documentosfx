﻿using DevComponents.DotNetBar;
using Generales;
using Ionic.Zip;
using ModeloDocumentosFX;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Xml.Serialization;

namespace Desarrollo
{
    class cPolizas
    {
        private string sSQL = "";
        public string ROW_ID { get; set; }

        public string ROW_ID_CATALOGO { get; set; }

        public string tipoSolicitudField { get; set; }

        public string numOrdenField { get; set; }

        public string numTramiteField { get; set; }

        public string selloField { get; set; }

        public string noCertificadoField { get; set; }

        public string certificadoField { get; set; }

        private cCONEXCION oData = new cCONEXCION();
        public override string ToString() { return ROW_ID; }

        public cPolizas()
        {
            ModeloDocumentosFXContainer dbContext = new ModeloDocumentosFXContainer();
            try
            {
                string conection = dbContext.Database.Connection.ConnectionString;
                oData = new cCONEXCION(conection);
            }
            catch (Exception err)
            {
                ErrorFX.mostrar(err, false, true, "frmEmpresa - 96 - ", false);
            }
            limpiar();
        }

        public void limpiar()
        {
            ROW_ID = "";
            ROW_ID_CATALOGO = "";
            tipoSolicitudField = "";
            numOrdenField = "";
            numTramiteField = "";
            selloField = "";
            noCertificadoField = "";
            certificadoField = "";
        }


        public List<cPolizas> todos(string ROW_ID_CATALOGOp,string tipoFieldp = "",string ASIGNADOp="")
        {

            List<cPolizas> lista = new List<cPolizas>();

            sSQL = " SELECT * ";
            sSQL += " FROM Polizas ";
            sSQL += " WHERE 1=1 ";
            if(ROW_ID_CATALOGOp!="")
            {
                sSQL += " AND ROW_ID_CATALOGO=" + ROW_ID_CATALOGOp + "";
            }
            
            sSQL += " ORDER BY ROW_ID";
            DataTable oDataTable = oData.EjecutarConsulta(sSQL);
            foreach (DataRow oDataRow in oDataTable.Rows)
            {
                cPolizas ocCatalogo = new cPolizas();
                ocCatalogo.cargar(oDataRow["ROW_ID"].ToString());
                lista.Add(ocCatalogo);
            }
            return lista;

        }

        public bool verificar(string numFieldp, string ROW_ID_CATALOGOp, bool cargarp=false)
        {
            sSQL = " SELECT * ";
            sSQL += " FROM Polizas ";
            sSQL += " WHERE numField='" + numFieldp + "' ";
            sSQL += " AND ROW_ID_CATALOGO=" + ROW_ID_CATALOGOp + "";
            DataTable oDataTable = oData.EjecutarConsulta(sSQL);
            if (oDataTable != null)
            {
                foreach (DataRow oDataRow in oDataTable.Rows)
                {

                    if (!cargarp)
                    {
                        return true;
                    }
                    else
                    {
                        cargar(oDataRow["ROW_ID"].ToString());
                    }
                }
            }
            return false;
        }

        public bool cargar(string ROW_IDp)
        {
            sSQL = " SELECT * ";
            sSQL += " FROM Polizas ";
            sSQL += " WHERE ROW_ID=" + ROW_IDp + " ";
            DataTable oDataTable = oData.EjecutarConsulta(sSQL);
            if (oDataTable != null)
            {
                foreach (DataRow oDataRow in oDataTable.Rows)
                {
                    //Añadir el Registro
                    ROW_ID = oDataRow["ROW_ID"].ToString();
                    ROW_ID_CATALOGO = oDataRow["ROW_ID_CATALOGO"].ToString();
                    tipoSolicitudField = oDataRow["tipoSolicitudField"].ToString();
                    numOrdenField = oDataRow["numOrdenField"].ToString();
                    numTramiteField = oDataRow["numTramiteField"].ToString();
                    selloField = oDataRow["selloField"].ToString();
                    noCertificadoField = oDataRow["noCertificadoField"].ToString();
                    certificadoField = oDataRow["certificadoField"].ToString();
                    return true;
                }
            }
            return false;
        }

        public List<cPolizas> validar(string ROW_ID_CATALOGOp)
        {

            List<cPolizas> lista = new List<cPolizas>();

            return lista;

        }
        


        public bool guardar(bool validar=true)
        {

            if (ROW_ID == "")
            {
                sSQL = " INSERT INTO Polizas ";
                sSQL += " ( ";
                sSQL += " [ROW_ID_CATALOGO],[tipoSolicitudField],[numOrdenField],[numTramiteField]";
                sSQL += ",[selloField],[noCertificadoField],[certificadoField] ";
                sSQL += " )";
                sSQL += " VALUES ";
                sSQL += "(";
                sSQL += " " + ROW_ID_CATALOGO + ",'" + tipoSolicitudField + "','" + numOrdenField.ToString() + "','" + numTramiteField.ToString() + "'";
                sSQL += ",'" + selloField.ToString() + "','" + noCertificadoField.ToString() + "','" + certificadoField + "' ";
                sSQL += ")";
                DataTable oDataTable = oData.EjecutarConsulta(sSQL);
                if (oDataTable != null)
                {

                    sSQL = "SELECT TOP 1 MAX(ROW_ID) as ROW_ID FROM Polizas";
                    oDataTable = oData.EjecutarConsulta(sSQL);
                    if (oDataTable != null)
                    {
                        foreach (DataRow oDataRow in oDataTable.Rows)
                        {
                            ROW_ID = oDataRow["ROW_ID"].ToString();
                        }
                    }
                }

            }
            else
            {
                sSQL = " UPDATE Polizas ";
                sSQL += " SET ROW_ID_CATALOGO=" + ROW_ID_CATALOGO + ",tipoSolicitudField='" + tipoSolicitudField + "',numOrdenField='" + numOrdenField.ToString() + "'";
                sSQL += ",numTramiteField='" + numTramiteField.ToString() + "',selloField='" + selloField + "',noCertificadoField='" + noCertificadoField + "' ";
                sSQL += " WHERE ROW_ID=" + ROW_ID;
                oData.EjecutarConsulta(sSQL);

            }
            return true;

        }

        public bool eliminar(string ROW_IDp)
        {
            if (ROW_IDp != "")
            {
                sSQL = "DELETE FROM Polizas ";
                sSQL += " WHERE ROW_ID='" + ROW_IDp + "' ";
                oData.EjecutarConsulta(sSQL);
                return true;
            }
            return false;
        }
    }
}
