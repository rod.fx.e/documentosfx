namespace Desarrollo
{
    partial class frmDeclaracionPoliza
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            DevComponents.DotNetBar.ButtonX buttonX15;
            DevComponents.DotNetBar.ButtonX buttonX17;
            DevComponents.DotNetBar.ButtonX buttonX3;
            DevComponents.DotNetBar.ButtonX buttonX1;
            DevComponents.DotNetBar.ButtonX buttonX2;
            DevComponents.DotNetBar.ButtonX buttonX28;
            DevComponents.DotNetBar.ButtonX buttonX8;
            DevComponents.DotNetBar.ButtonX buttonX9;
            DevComponents.DotNetBar.ButtonX buttonX13;
            DevComponents.DotNetBar.ButtonX buttonX12;
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle8 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle9 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle7 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle10 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle11 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle12 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle13 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle14 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle15 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmDeclaracionPoliza));
            this.buttonItem3 = new DevComponents.DotNetBar.ButtonItem();
            this.buttonItem7 = new DevComponents.DotNetBar.ButtonItem();
            this.metroStatusBar1 = new DevComponents.DotNetBar.Metro.MetroStatusBar();
            this.labelItem1 = new DevComponents.DotNetBar.LabelItem();
            this.labelItem2 = new DevComponents.DotNetBar.LabelItem();
            this.mesField = new DevComponents.DotNetBar.Controls.ComboBoxEx();
            this.comboItem1 = new DevComponents.Editors.ComboItem();
            this.comboItem2 = new DevComponents.Editors.ComboItem();
            this.comboItem5 = new DevComponents.Editors.ComboItem();
            this.comboItem6 = new DevComponents.Editors.ComboItem();
            this.comboItem7 = new DevComponents.Editors.ComboItem();
            this.comboItem8 = new DevComponents.Editors.ComboItem();
            this.comboItem9 = new DevComponents.Editors.ComboItem();
            this.comboItem10 = new DevComponents.Editors.ComboItem();
            this.comboItem11 = new DevComponents.Editors.ComboItem();
            this.comboItem12 = new DevComponents.Editors.ComboItem();
            this.comboItem13 = new DevComponents.Editors.ComboItem();
            this.comboItem14 = new DevComponents.Editors.ComboItem();
            this.labelX4 = new DevComponents.DotNetBar.LabelX();
            this.totalCtasField = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.labelX3 = new DevComponents.DotNetBar.LabelX();
            this.anoField = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.labelX2 = new DevComponents.DotNetBar.LabelX();
            this.labelX1 = new DevComponents.DotNetBar.LabelX();
            this.rFCField = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.versionField = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.label1 = new DevComponents.DotNetBar.LabelX();
            this.superTabControl1 = new DevComponents.DotNetBar.SuperTabControl();
            this.superTabControlPanel3 = new DevComponents.DotNetBar.SuperTabControlPanel();
            this.filtrarComprobantes = new DevComponents.DotNetBar.Controls.CheckBoxX();
            this.buttonX25 = new DevComponents.DotNetBar.ButtonX();
            this.BUSCAR_BATCH_ID = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.circularProgress3 = new DevComponents.DotNetBar.Controls.CircularProgress();
            this.buttonX16 = new DevComponents.DotNetBar.ButtonX();
            this.buttonX18 = new DevComponents.DotNetBar.ButtonX();
            this.buttonX19 = new DevComponents.DotNetBar.ButtonX();
            this.dtgPoliza = new DevComponents.DotNetBar.Controls.DataGridViewX();
            this.ROW_ID_p = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tipoField = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.numField = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.fechaField = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.conceptoField = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.DEBE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.HABER = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Comprobantes = new DevComponents.DotNetBar.Controls.DataGridViewCheckBoxXColumn();
            this.Polizas = new DevComponents.DotNetBar.SuperTabItem();
            this.superTabControlPanel2 = new DevComponents.DotNetBar.SuperTabControlPanel();
            this.circularProgress1 = new DevComponents.DotNetBar.Controls.CircularProgress();
            this.BUSCAR_RFC = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.BUSCAR_CFDI_POLIZA = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.buttonX7 = new DevComponents.DotNetBar.ButtonX();
            this.BUSCAR_UUID = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.buttonX11 = new DevComponents.DotNetBar.ButtonX();
            this.dtgComprobantes = new DevComponents.DotNetBar.Controls.DataGridViewX();
            this.ROW_ID_t = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.numFieldc = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.TIPO_CFDI = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.INVOICE_ID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.TRANSACCIONES = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.INVOICE_DATE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.UUID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.RFC = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.monto = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.buttonX10 = new DevComponents.DotNetBar.ButtonX();
            this.buttonItem5 = new DevComponents.DotNetBar.ButtonItem();
            this.buttonItem1 = new DevComponents.DotNetBar.ButtonItem();
            this.superTabItem5 = new DevComponents.DotNetBar.SuperTabItem();
            this.superTabControlPanel4 = new DevComponents.DotNetBar.SuperTabControlPanel();
            this.labelX6 = new DevComponents.DotNetBar.LabelX();
            this.buttonX23 = new DevComponents.DotNetBar.ButtonX();
            this.dtgValidador = new DevComponents.DotNetBar.Controls.DataGridViewX();
            this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.TIPO = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CUENTA = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.MENSAJE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.superTabItem3 = new DevComponents.DotNetBar.SuperTabItem();
            this.superTabControlPanel1 = new DevComponents.DotNetBar.SuperTabControlPanel();
            this.buttonX6 = new DevComponents.DotNetBar.ButtonX();
            this.buttonX5 = new DevComponents.DotNetBar.ButtonX();
            this.buttonX4 = new DevComponents.DotNetBar.ButtonX();
            this.buttonItem2 = new DevComponents.DotNetBar.ButtonItem();
            this.dtgTipos = new DevComponents.DotNetBar.Controls.DataGridViewX();
            this.ROW_ID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.TIPO_tr = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.TIPO_SOLICITUD = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.numero = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn6 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.buttonX22 = new DevComponents.DotNetBar.ButtonX();
            this.superTabItem4 = new DevComponents.DotNetBar.SuperTabItem();
            this.superTabItem1 = new DevComponents.DotNetBar.SuperTabItem();
            this.superTabItem2 = new DevComponents.DotNetBar.SuperTabItem();
            this.ESTADO = new DevComponents.DotNetBar.Controls.ComboBoxEx();
            this.comboItem3 = new DevComponents.Editors.ComboItem();
            this.comboItem4 = new DevComponents.Editors.ComboItem();
            this.labelX5 = new DevComponents.DotNetBar.LabelX();
            this.folderBrowserDialog1 = new System.Windows.Forms.FolderBrowserDialog();
            this.backgroundWorker3 = new System.ComponentModel.BackgroundWorker();
            this.bW1 = new System.ComponentModel.BackgroundWorker();
            this.backgroundWorker1 = new System.ComponentModel.BackgroundWorker();
            buttonX15 = new DevComponents.DotNetBar.ButtonX();
            buttonX17 = new DevComponents.DotNetBar.ButtonX();
            buttonX3 = new DevComponents.DotNetBar.ButtonX();
            buttonX1 = new DevComponents.DotNetBar.ButtonX();
            buttonX2 = new DevComponents.DotNetBar.ButtonX();
            buttonX28 = new DevComponents.DotNetBar.ButtonX();
            buttonX8 = new DevComponents.DotNetBar.ButtonX();
            buttonX9 = new DevComponents.DotNetBar.ButtonX();
            buttonX13 = new DevComponents.DotNetBar.ButtonX();
            buttonX12 = new DevComponents.DotNetBar.ButtonX();
            ((System.ComponentModel.ISupportInitialize)(this.superTabControl1)).BeginInit();
            this.superTabControl1.SuspendLayout();
            this.superTabControlPanel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dtgPoliza)).BeginInit();
            this.superTabControlPanel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dtgComprobantes)).BeginInit();
            this.superTabControlPanel4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dtgValidador)).BeginInit();
            this.superTabControlPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dtgTipos)).BeginInit();
            this.SuspendLayout();
            // 
            // buttonX15
            // 
            buttonX15.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            buttonX15.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            buttonX15.Image = global::Desarrollo.Properties.Resources.excel;
            buttonX15.Location = new System.Drawing.Point(247, 3);
            buttonX15.Name = "buttonX15";
            buttonX15.Size = new System.Drawing.Size(114, 24);
            buttonX15.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            buttonX15.SubItems.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.buttonItem3});
            buttonX15.TabIndex = 581;
            buttonX15.Text = "Importar: Excel";
            buttonX15.TextAlignment = DevComponents.DotNetBar.eButtonTextAlignment.Right;
            buttonX15.Click += new System.EventHandler(this.buttonX15_Click);
            // 
            // buttonItem3
            // 
            this.buttonItem3.GlobalItem = false;
            this.buttonItem3.Image = global::Desarrollo.Properties.Resources.wizard;
            this.buttonItem3.Name = "buttonItem3";
            this.buttonItem3.Text = "Generar Plantilla";
            this.buttonItem3.Click += new System.EventHandler(this.buttonItem3_Click);
            // 
            // buttonX17
            // 
            buttonX17.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            buttonX17.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            buttonX17.Image = global::Desarrollo.Properties.Resources.Eliminar_18;
            buttonX17.Location = new System.Drawing.Point(480, 3);
            buttonX17.Name = "buttonX17";
            buttonX17.Size = new System.Drawing.Size(68, 24);
            buttonX17.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            buttonX17.TabIndex = 583;
            buttonX17.Text = "Eliminar";
            buttonX17.TextAlignment = DevComponents.DotNetBar.eButtonTextAlignment.Right;
            buttonX17.Click += new System.EventHandler(this.buttonX17_Click);
            // 
            // buttonX3
            // 
            buttonX3.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            buttonX3.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            buttonX3.Image = global::Desarrollo.Properties.Resources.Eliminar_18;
            buttonX3.Location = new System.Drawing.Point(577, 33);
            buttonX3.Name = "buttonX3";
            buttonX3.Size = new System.Drawing.Size(68, 24);
            buttonX3.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            buttonX3.TabIndex = 1;
            buttonX3.Text = "Eliminar";
            buttonX3.TextAlignment = DevComponents.DotNetBar.eButtonTextAlignment.Right;
            buttonX3.Visible = false;
            buttonX3.Click += new System.EventHandler(this.buttonX3_Click);
            // 
            // buttonX1
            // 
            buttonX1.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            buttonX1.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            buttonX1.Image = global::Desarrollo.Properties.Resources.Nueno_18;
            buttonX1.Location = new System.Drawing.Point(503, 33);
            buttonX1.Name = "buttonX1";
            buttonX1.Size = new System.Drawing.Size(68, 24);
            buttonX1.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            buttonX1.TabIndex = 1;
            buttonX1.Text = "Guardar";
            buttonX1.TextAlignment = DevComponents.DotNetBar.eButtonTextAlignment.Right;
            buttonX1.Visible = false;
            buttonX1.Click += new System.EventHandler(this.buttonX1_Click);
            // 
            // buttonX2
            // 
            buttonX2.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            buttonX2.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            buttonX2.Image = global::Desarrollo.Properties.Resources.Nuevo_18_19;
            buttonX2.Location = new System.Drawing.Point(660, 33);
            buttonX2.Name = "buttonX2";
            buttonX2.Size = new System.Drawing.Size(61, 24);
            buttonX2.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            buttonX2.TabIndex = 1;
            buttonX2.Text = "Nuevo";
            buttonX2.TextAlignment = DevComponents.DotNetBar.eButtonTextAlignment.Right;
            buttonX2.Visible = false;
            buttonX2.Click += new System.EventHandler(this.buttonX2_Click);
            // 
            // buttonX28
            // 
            buttonX28.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            buttonX28.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            buttonX28.Image = global::Desarrollo.Properties.Resources.excel;
            buttonX28.Location = new System.Drawing.Point(367, 3);
            buttonX28.Name = "buttonX28";
            buttonX28.Size = new System.Drawing.Size(107, 24);
            buttonX28.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            buttonX28.SubItems.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.buttonItem7});
            buttonX28.TabIndex = 593;
            buttonX28.Text = "Exportar: Excel";
            buttonX28.Click += new System.EventHandler(this.buttonX28_Click);
            // 
            // buttonItem7
            // 
            this.buttonItem7.GlobalItem = false;
            this.buttonItem7.Name = "buttonItem7";
            this.buttonItem7.Text = "Exportar CFDI";
            this.buttonItem7.Click += new System.EventHandler(this.buttonItem7_Click);
            // 
            // buttonX8
            // 
            buttonX8.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            buttonX8.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            buttonX8.Image = global::Desarrollo.Properties.Resources.Eliminar_18;
            buttonX8.Location = new System.Drawing.Point(634, 3);
            buttonX8.Name = "buttonX8";
            buttonX8.Size = new System.Drawing.Size(68, 24);
            buttonX8.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            buttonX8.TabIndex = 603;
            buttonX8.Text = "Eliminar";
            buttonX8.TextAlignment = DevComponents.DotNetBar.eButtonTextAlignment.Right;
            buttonX8.Click += new System.EventHandler(this.buttonX8_Click);
            // 
            // buttonX9
            // 
            buttonX9.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            buttonX9.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            buttonX9.Image = global::Desarrollo.Properties.Resources.excel;
            buttonX9.Location = new System.Drawing.Point(87, 3);
            buttonX9.Name = "buttonX9";
            buttonX9.Size = new System.Drawing.Size(163, 24);
            buttonX9.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            buttonX9.TabIndex = 594;
            buttonX9.Text = "Exportar: Excel Validaci�n";
            buttonX9.Click += new System.EventHandler(this.buttonX9_Click);
            // 
            // buttonX13
            // 
            buttonX13.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            buttonX13.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            buttonX13.Image = global::Desarrollo.Properties.Resources.envio_mail_18;
            buttonX13.Location = new System.Drawing.Point(259, 3);
            buttonX13.Name = "buttonX13";
            buttonX13.Size = new System.Drawing.Size(135, 24);
            buttonX13.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            buttonX13.TabIndex = 587;
            buttonX13.Text = "Buscar Comprobantes";
            buttonX13.TextAlignment = DevComponents.DotNetBar.eButtonTextAlignment.Right;
            buttonX13.Click += new System.EventHandler(this.buttonX13_Click);
            // 
            // buttonX12
            // 
            buttonX12.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            buttonX12.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            buttonX12.Image = global::Desarrollo.Properties.Resources.envio_mail_18;
            buttonX12.Location = new System.Drawing.Point(744, 4);
            buttonX12.Name = "buttonX12";
            buttonX12.Size = new System.Drawing.Size(99, 24);
            buttonX12.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            buttonX12.TabIndex = 600;
            buttonX12.Text = "Validar CFDI";
            buttonX12.Visible = false;
            buttonX12.Click += new System.EventHandler(this.buttonX12_Click);
            // 
            // metroStatusBar1
            // 
            this.metroStatusBar1.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.metroStatusBar1.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.metroStatusBar1.ContainerControlProcessDialogKey = true;
            this.metroStatusBar1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.metroStatusBar1.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.metroStatusBar1.ForeColor = System.Drawing.Color.Black;
            this.metroStatusBar1.Items.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.labelItem1,
            this.labelItem2});
            this.metroStatusBar1.Location = new System.Drawing.Point(0, 397);
            this.metroStatusBar1.Name = "metroStatusBar1";
            this.metroStatusBar1.Size = new System.Drawing.Size(846, 21);
            this.metroStatusBar1.TabIndex = 8;
            this.metroStatusBar1.ItemClick += new System.EventHandler(this.metroStatusBar1_ItemClick);
            // 
            // labelItem1
            // 
            this.labelItem1.Name = "labelItem1";
            // 
            // labelItem2
            // 
            this.labelItem2.Name = "labelItem2";
            // 
            // mesField
            // 
            this.mesField.AutoCompleteCustomSource.AddRange(new string[] {
            "01",
            "02",
            "03",
            "04",
            "05",
            "06",
            "07",
            "08",
            "09",
            "10",
            "11",
            "12"});
            this.mesField.DisplayMember = "Text";
            this.mesField.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.mesField.Enabled = false;
            this.mesField.ForeColor = System.Drawing.Color.Black;
            this.mesField.FormattingEnabled = true;
            this.mesField.ItemHeight = 16;
            this.mesField.Items.AddRange(new object[] {
            this.comboItem1,
            this.comboItem2,
            this.comboItem5,
            this.comboItem6,
            this.comboItem7,
            this.comboItem8,
            this.comboItem9,
            this.comboItem10,
            this.comboItem11,
            this.comboItem12,
            this.comboItem13,
            this.comboItem14});
            this.mesField.Location = new System.Drawing.Point(283, 12);
            this.mesField.Name = "mesField";
            this.mesField.Size = new System.Drawing.Size(45, 22);
            this.mesField.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.mesField.TabIndex = 20;
            // 
            // comboItem1
            // 
            this.comboItem1.Text = "01";
            // 
            // comboItem2
            // 
            this.comboItem2.Text = "02";
            // 
            // comboItem5
            // 
            this.comboItem5.Text = "03";
            // 
            // comboItem6
            // 
            this.comboItem6.Text = "04";
            // 
            // comboItem7
            // 
            this.comboItem7.Text = "05";
            // 
            // comboItem8
            // 
            this.comboItem8.Text = "06";
            // 
            // comboItem9
            // 
            this.comboItem9.Text = "07";
            // 
            // comboItem10
            // 
            this.comboItem10.Text = "08";
            // 
            // comboItem11
            // 
            this.comboItem11.Text = "09";
            // 
            // comboItem12
            // 
            this.comboItem12.Text = "10";
            // 
            // comboItem13
            // 
            this.comboItem13.Text = "11";
            // 
            // comboItem14
            // 
            this.comboItem14.Text = "12";
            // 
            // labelX4
            // 
            this.labelX4.AutoSize = true;
            this.labelX4.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.labelX4.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX4.ForeColor = System.Drawing.Color.Black;
            this.labelX4.Location = new System.Drawing.Point(412, 15);
            this.labelX4.Name = "labelX4";
            this.labelX4.Size = new System.Drawing.Size(84, 17);
            this.labelX4.TabIndex = 18;
            this.labelX4.Text = "Total de Cuentas";
            // 
            // totalCtasField
            // 
            this.totalCtasField.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.totalCtasField.Border.Class = "TextBoxBorder";
            this.totalCtasField.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.totalCtasField.Enabled = false;
            this.totalCtasField.ForeColor = System.Drawing.Color.Black;
            this.totalCtasField.Location = new System.Drawing.Point(502, 12);
            this.totalCtasField.Name = "totalCtasField";
            this.totalCtasField.Size = new System.Drawing.Size(60, 22);
            this.totalCtasField.TabIndex = 19;
            // 
            // labelX3
            // 
            this.labelX3.AutoSize = true;
            this.labelX3.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.labelX3.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX3.ForeColor = System.Drawing.Color.Black;
            this.labelX3.Location = new System.Drawing.Point(334, 15);
            this.labelX3.Name = "labelX3";
            this.labelX3.Size = new System.Drawing.Size(22, 17);
            this.labelX3.TabIndex = 16;
            this.labelX3.Text = "A�o";
            // 
            // anoField
            // 
            this.anoField.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.anoField.Border.Class = "TextBoxBorder";
            this.anoField.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.anoField.Enabled = false;
            this.anoField.ForeColor = System.Drawing.Color.Black;
            this.anoField.Location = new System.Drawing.Point(360, 12);
            this.anoField.Name = "anoField";
            this.anoField.Size = new System.Drawing.Size(48, 22);
            this.anoField.TabIndex = 17;
            // 
            // labelX2
            // 
            this.labelX2.AutoSize = true;
            this.labelX2.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.labelX2.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX2.ForeColor = System.Drawing.Color.Black;
            this.labelX2.Location = new System.Drawing.Point(255, 15);
            this.labelX2.Name = "labelX2";
            this.labelX2.Size = new System.Drawing.Size(23, 17);
            this.labelX2.TabIndex = 15;
            this.labelX2.Text = "Mes";
            // 
            // labelX1
            // 
            this.labelX1.AutoSize = true;
            this.labelX1.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.labelX1.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX1.ForeColor = System.Drawing.Color.Black;
            this.labelX1.Location = new System.Drawing.Point(94, 15);
            this.labelX1.Name = "labelX1";
            this.labelX1.Size = new System.Drawing.Size(21, 17);
            this.labelX1.TabIndex = 13;
            this.labelX1.Text = "RFC";
            // 
            // rFCField
            // 
            this.rFCField.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.rFCField.Border.Class = "TextBoxBorder";
            this.rFCField.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.rFCField.Enabled = false;
            this.rFCField.ForeColor = System.Drawing.Color.Black;
            this.rFCField.Location = new System.Drawing.Point(123, 12);
            this.rFCField.Name = "rFCField";
            this.rFCField.Size = new System.Drawing.Size(128, 22);
            this.rFCField.TabIndex = 14;
            // 
            // versionField
            // 
            this.versionField.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.versionField.Border.Class = "TextBoxBorder";
            this.versionField.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.versionField.ForeColor = System.Drawing.Color.Black;
            this.versionField.Location = new System.Drawing.Point(46, 12);
            this.versionField.Name = "versionField";
            this.versionField.Size = new System.Drawing.Size(44, 22);
            this.versionField.TabIndex = 12;
            this.versionField.Text = "1.1";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.label1.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.label1.ForeColor = System.Drawing.Color.Black;
            this.label1.Location = new System.Drawing.Point(2, 15);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(39, 17);
            this.label1.TabIndex = 11;
            this.label1.Text = "Versi�n";
            // 
            // superTabControl1
            // 
            this.superTabControl1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.superTabControl1.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            // 
            // 
            // 
            this.superTabControl1.ControlBox.CloseBox.Name = "";
            // 
            // 
            // 
            this.superTabControl1.ControlBox.MenuBox.Name = "";
            this.superTabControl1.ControlBox.Name = "";
            this.superTabControl1.ControlBox.SubItems.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.superTabControl1.ControlBox.MenuBox,
            this.superTabControl1.ControlBox.CloseBox});
            this.superTabControl1.Controls.Add(this.superTabControlPanel3);
            this.superTabControl1.Controls.Add(this.superTabControlPanel2);
            this.superTabControl1.Controls.Add(this.superTabControlPanel4);
            this.superTabControl1.Controls.Add(this.superTabControlPanel1);
            this.superTabControl1.ForeColor = System.Drawing.Color.Black;
            this.superTabControl1.Location = new System.Drawing.Point(0, 40);
            this.superTabControl1.Name = "superTabControl1";
            this.superTabControl1.ReorderTabsEnabled = true;
            this.superTabControl1.SelectedTabFont = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Bold);
            this.superTabControl1.SelectedTabIndex = 0;
            this.superTabControl1.Size = new System.Drawing.Size(846, 351);
            this.superTabControl1.TabFont = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.superTabControl1.TabIndex = 21;
            this.superTabControl1.Tabs.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.Polizas,
            this.superTabItem3,
            this.superTabItem4,
            this.superTabItem5});
            this.superTabControl1.TabStyle = DevComponents.DotNetBar.eSuperTabStyle.Office2010BackstageBlue;
            this.superTabControl1.Text = "Generar XML";
            // 
            // superTabControlPanel3
            // 
            this.superTabControlPanel3.Controls.Add(this.filtrarComprobantes);
            this.superTabControlPanel3.Controls.Add(buttonX28);
            this.superTabControlPanel3.Controls.Add(this.buttonX25);
            this.superTabControlPanel3.Controls.Add(this.BUSCAR_BATCH_ID);
            this.superTabControlPanel3.Controls.Add(this.circularProgress3);
            this.superTabControlPanel3.Controls.Add(buttonX15);
            this.superTabControlPanel3.Controls.Add(this.buttonX16);
            this.superTabControlPanel3.Controls.Add(buttonX17);
            this.superTabControlPanel3.Controls.Add(this.buttonX18);
            this.superTabControlPanel3.Controls.Add(this.buttonX19);
            this.superTabControlPanel3.Controls.Add(this.dtgPoliza);
            this.superTabControlPanel3.Controls.Add(buttonX2);
            this.superTabControlPanel3.Controls.Add(buttonX1);
            this.superTabControlPanel3.Controls.Add(buttonX3);
            this.superTabControlPanel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.superTabControlPanel3.Location = new System.Drawing.Point(0, 25);
            this.superTabControlPanel3.Name = "superTabControlPanel3";
            this.superTabControlPanel3.Size = new System.Drawing.Size(846, 326);
            this.superTabControlPanel3.TabIndex = 0;
            this.superTabControlPanel3.TabItem = this.Polizas;
            // 
            // filtrarComprobantes
            // 
            this.filtrarComprobantes.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.filtrarComprobantes.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.filtrarComprobantes.ForeColor = System.Drawing.Color.Black;
            this.filtrarComprobantes.Location = new System.Drawing.Point(167, 35);
            this.filtrarComprobantes.Name = "filtrarComprobantes";
            this.filtrarComprobantes.Size = new System.Drawing.Size(327, 18);
            this.filtrarComprobantes.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.filtrarComprobantes.TabIndex = 594;
            this.filtrarComprobantes.Text = "Mostrar P�liza con falta de Comprobantes (API, ARI,APC,ARC)";
            this.filtrarComprobantes.CheckedChanged += new System.EventHandler(this.filtrarComprobantes_CheckedChanged);
            // 
            // buttonX25
            // 
            this.buttonX25.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.buttonX25.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.buttonX25.Image = global::Desarrollo.Properties.Resources._1424086708_filter_16;
            this.buttonX25.Location = new System.Drawing.Point(106, 33);
            this.buttonX25.Name = "buttonX25";
            this.buttonX25.Size = new System.Drawing.Size(58, 22);
            this.buttonX25.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.buttonX25.TabIndex = 592;
            this.buttonX25.Text = "Filtrar";
            this.buttonX25.Click += new System.EventHandler(this.buttonX25_Click);
            // 
            // BUSCAR_BATCH_ID
            // 
            this.BUSCAR_BATCH_ID.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.BUSCAR_BATCH_ID.Border.Class = "TextBoxBorder";
            this.BUSCAR_BATCH_ID.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.BUSCAR_BATCH_ID.ForeColor = System.Drawing.Color.Black;
            this.BUSCAR_BATCH_ID.Location = new System.Drawing.Point(3, 33);
            this.BUSCAR_BATCH_ID.Name = "BUSCAR_BATCH_ID";
            this.BUSCAR_BATCH_ID.Size = new System.Drawing.Size(97, 22);
            this.BUSCAR_BATCH_ID.TabIndex = 591;
            this.BUSCAR_BATCH_ID.WatermarkText = "Buscar por Lote...";
            this.BUSCAR_BATCH_ID.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.BUSCAR_BATCH_ID_KeyPress);
            // 
            // circularProgress3
            // 
            this.circularProgress3.AnimationSpeed = 50;
            this.circularProgress3.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.circularProgress3.BackgroundStyle.BackgroundImageAlpha = ((byte)(64));
            this.circularProgress3.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.circularProgress3.FocusCuesEnabled = false;
            this.circularProgress3.Font = new System.Drawing.Font("Segoe UI", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.circularProgress3.Location = new System.Drawing.Point(216, 3);
            this.circularProgress3.Name = "circularProgress3";
            this.circularProgress3.ProgressBarType = DevComponents.DotNetBar.eCircularProgressType.Donut;
            this.circularProgress3.ProgressColor = System.Drawing.Color.Orange;
            this.circularProgress3.ProgressTextFormat = "{0}";
            this.circularProgress3.Size = new System.Drawing.Size(25, 24);
            this.circularProgress3.Style = DevComponents.DotNetBar.eDotNetBarStyle.OfficeXP;
            this.circularProgress3.TabIndex = 584;
            this.circularProgress3.Value = 100;
            this.circularProgress3.Visible = false;
            // 
            // buttonX16
            // 
            this.buttonX16.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.buttonX16.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.buttonX16.Image = global::Desarrollo.Properties.Resources.visual;
            this.buttonX16.Location = new System.Drawing.Point(87, 3);
            this.buttonX16.Name = "buttonX16";
            this.buttonX16.Size = new System.Drawing.Size(123, 24);
            this.buttonX16.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.buttonX16.TabIndex = 582;
            this.buttonX16.Text = "Importar: Visual";
            this.buttonX16.Click += new System.EventHandler(this.buttonX16_Click);
            // 
            // buttonX18
            // 
            this.buttonX18.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.buttonX18.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.buttonX18.Image = global::Desarrollo.Properties.Resources.actualizar_18;
            this.buttonX18.Location = new System.Drawing.Point(3, 3);
            this.buttonX18.Name = "buttonX18";
            this.buttonX18.Size = new System.Drawing.Size(78, 24);
            this.buttonX18.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.buttonX18.TabIndex = 579;
            this.buttonX18.Text = "Refrescar";
            this.buttonX18.Click += new System.EventHandler(this.buttonX18_Click);
            // 
            // buttonX19
            // 
            this.buttonX19.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.buttonX19.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.buttonX19.Image = global::Desarrollo.Properties.Resources.Nueno_18;
            this.buttonX19.Location = new System.Drawing.Point(554, 3);
            this.buttonX19.Name = "buttonX19";
            this.buttonX19.Size = new System.Drawing.Size(74, 24);
            this.buttonX19.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.buttonX19.TabIndex = 580;
            this.buttonX19.Text = "Agregar";
            this.buttonX19.Visible = false;
            this.buttonX19.Click += new System.EventHandler(this.buttonX19_Click);
            // 
            // dtgPoliza
            // 
            this.dtgPoliza.AllowUserToAddRows = false;
            this.dtgPoliza.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dtgPoliza.BackgroundColor = System.Drawing.Color.White;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dtgPoliza.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.dtgPoliza.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dtgPoliza.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.ROW_ID_p,
            this.tipoField,
            this.numField,
            this.fechaField,
            this.conceptoField,
            this.DEBE,
            this.HABER,
            this.Comprobantes});
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle4.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle4.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle4.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle4.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle4.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dtgPoliza.DefaultCellStyle = dataGridViewCellStyle4;
            this.dtgPoliza.EnableHeadersVisualStyles = false;
            this.dtgPoliza.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(170)))), ((int)(((byte)(170)))), ((int)(((byte)(170)))));
            this.dtgPoliza.Location = new System.Drawing.Point(3, 61);
            this.dtgPoliza.Name = "dtgPoliza";
            dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle5.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle5.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle5.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle5.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle5.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle5.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dtgPoliza.RowHeadersDefaultCellStyle = dataGridViewCellStyle5;
            this.dtgPoliza.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dtgPoliza.Size = new System.Drawing.Size(840, 262);
            this.dtgPoliza.TabIndex = 578;
            this.dtgPoliza.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dtgPoliza_CellDoubleClick);
            // 
            // ROW_ID_p
            // 
            this.ROW_ID_p.HeaderText = "ROW_ID";
            this.ROW_ID_p.Name = "ROW_ID_p";
            this.ROW_ID_p.Visible = false;
            // 
            // tipoField
            // 
            this.tipoField.HeaderText = "Tipo";
            this.tipoField.Name = "tipoField";
            this.tipoField.ReadOnly = true;
            this.tipoField.Width = 50;
            // 
            // numField
            // 
            this.numField.HeaderText = "N�mero";
            this.numField.Name = "numField";
            this.numField.ReadOnly = true;
            this.numField.Width = 70;
            // 
            // fechaField
            // 
            this.fechaField.HeaderText = "Fecha";
            this.fechaField.Name = "fechaField";
            this.fechaField.ReadOnly = true;
            this.fechaField.Width = 75;
            // 
            // conceptoField
            // 
            this.conceptoField.HeaderText = "Concepto";
            this.conceptoField.Name = "conceptoField";
            this.conceptoField.ReadOnly = true;
            this.conceptoField.Width = 300;
            // 
            // DEBE
            // 
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.DEBE.DefaultCellStyle = dataGridViewCellStyle2;
            this.DEBE.HeaderText = "Debe";
            this.DEBE.Name = "DEBE";
            this.DEBE.ReadOnly = true;
            // 
            // HABER
            // 
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.HABER.DefaultCellStyle = dataGridViewCellStyle3;
            this.HABER.HeaderText = "Haber";
            this.HABER.Name = "HABER";
            this.HABER.ReadOnly = true;
            // 
            // Comprobantes
            // 
            this.Comprobantes.Checked = true;
            this.Comprobantes.CheckState = System.Windows.Forms.CheckState.Indeterminate;
            this.Comprobantes.CheckValue = "N";
            this.Comprobantes.HeaderText = "Comprobantes";
            this.Comprobantes.Name = "Comprobantes";
            this.Comprobantes.ReadOnly = true;
            this.Comprobantes.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.Comprobantes.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            // 
            // Polizas
            // 
            this.Polizas.AttachedControl = this.superTabControlPanel3;
            this.Polizas.GlobalItem = false;
            this.Polizas.Name = "Polizas";
            this.Polizas.Text = "P�lizas";
            // 
            // superTabControlPanel2
            // 
            this.superTabControlPanel2.Controls.Add(buttonX12);
            this.superTabControlPanel2.Controls.Add(this.circularProgress1);
            this.superTabControlPanel2.Controls.Add(this.BUSCAR_RFC);
            this.superTabControlPanel2.Controls.Add(this.BUSCAR_CFDI_POLIZA);
            this.superTabControlPanel2.Controls.Add(this.buttonX7);
            this.superTabControlPanel2.Controls.Add(this.BUSCAR_UUID);
            this.superTabControlPanel2.Controls.Add(buttonX13);
            this.superTabControlPanel2.Controls.Add(this.buttonX11);
            this.superTabControlPanel2.Controls.Add(this.dtgComprobantes);
            this.superTabControlPanel2.Controls.Add(this.buttonX10);
            this.superTabControlPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.superTabControlPanel2.Location = new System.Drawing.Point(0, 0);
            this.superTabControlPanel2.Name = "superTabControlPanel2";
            this.superTabControlPanel2.Size = new System.Drawing.Size(846, 351);
            this.superTabControlPanel2.TabIndex = 0;
            this.superTabControlPanel2.TabItem = this.superTabItem5;
            this.superTabControlPanel2.Visible = false;
            // 
            // circularProgress1
            // 
            this.circularProgress1.AnimationSpeed = 50;
            this.circularProgress1.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.circularProgress1.BackgroundStyle.BackgroundImageAlpha = ((byte)(64));
            this.circularProgress1.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.circularProgress1.FocusCuesEnabled = false;
            this.circularProgress1.Font = new System.Drawing.Font("Segoe UI", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.circularProgress1.Location = new System.Drawing.Point(400, 3);
            this.circularProgress1.Name = "circularProgress1";
            this.circularProgress1.ProgressBarType = DevComponents.DotNetBar.eCircularProgressType.Donut;
            this.circularProgress1.ProgressColor = System.Drawing.Color.Orange;
            this.circularProgress1.ProgressTextFormat = "{0}";
            this.circularProgress1.Size = new System.Drawing.Size(25, 24);
            this.circularProgress1.Style = DevComponents.DotNetBar.eDotNetBarStyle.OfficeXP;
            this.circularProgress1.TabIndex = 597;
            this.circularProgress1.Value = 100;
            this.circularProgress1.Visible = false;
            // 
            // BUSCAR_RFC
            // 
            this.BUSCAR_RFC.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.BUSCAR_RFC.Border.Class = "TextBoxBorder";
            this.BUSCAR_RFC.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.BUSCAR_RFC.ForeColor = System.Drawing.Color.Black;
            this.BUSCAR_RFC.Location = new System.Drawing.Point(298, 33);
            this.BUSCAR_RFC.Name = "BUSCAR_RFC";
            this.BUSCAR_RFC.Size = new System.Drawing.Size(107, 22);
            this.BUSCAR_RFC.TabIndex = 596;
            this.BUSCAR_RFC.WatermarkText = "Buscar por RFC...";
            // 
            // BUSCAR_CFDI_POLIZA
            // 
            this.BUSCAR_CFDI_POLIZA.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.BUSCAR_CFDI_POLIZA.Border.Class = "TextBoxBorder";
            this.BUSCAR_CFDI_POLIZA.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.BUSCAR_CFDI_POLIZA.ForeColor = System.Drawing.Color.Black;
            this.BUSCAR_CFDI_POLIZA.Location = new System.Drawing.Point(185, 33);
            this.BUSCAR_CFDI_POLIZA.Name = "BUSCAR_CFDI_POLIZA";
            this.BUSCAR_CFDI_POLIZA.Size = new System.Drawing.Size(107, 22);
            this.BUSCAR_CFDI_POLIZA.TabIndex = 595;
            this.BUSCAR_CFDI_POLIZA.WatermarkText = "Buscar por P�liza...";
            // 
            // buttonX7
            // 
            this.buttonX7.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.buttonX7.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.buttonX7.Image = global::Desarrollo.Properties.Resources._1424086708_filter_16;
            this.buttonX7.Location = new System.Drawing.Point(411, 33);
            this.buttonX7.Name = "buttonX7";
            this.buttonX7.Size = new System.Drawing.Size(56, 22);
            this.buttonX7.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.buttonX7.TabIndex = 594;
            this.buttonX7.Text = "Filtrar";
            this.buttonX7.Click += new System.EventHandler(this.buttonX7_Click_1);
            // 
            // BUSCAR_UUID
            // 
            this.BUSCAR_UUID.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.BUSCAR_UUID.Border.Class = "TextBoxBorder";
            this.BUSCAR_UUID.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.BUSCAR_UUID.ForeColor = System.Drawing.Color.Black;
            this.BUSCAR_UUID.Location = new System.Drawing.Point(3, 33);
            this.BUSCAR_UUID.Name = "BUSCAR_UUID";
            this.BUSCAR_UUID.Size = new System.Drawing.Size(176, 22);
            this.BUSCAR_UUID.TabIndex = 593;
            this.BUSCAR_UUID.WatermarkText = "Buscar por Folio Fiscal...";
            this.BUSCAR_UUID.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.BUSCAR_UUID_KeyPress);
            // 
            // buttonX11
            // 
            this.buttonX11.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.buttonX11.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.buttonX11.Image = global::Desarrollo.Properties.Resources.actualizar_18;
            this.buttonX11.Location = new System.Drawing.Point(3, 3);
            this.buttonX11.Name = "buttonX11";
            this.buttonX11.Size = new System.Drawing.Size(78, 24);
            this.buttonX11.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.buttonX11.TabIndex = 585;
            this.buttonX11.Text = "Refrescar";
            this.buttonX11.Click += new System.EventHandler(this.buttonX11_Click);
            // 
            // dtgComprobantes
            // 
            this.dtgComprobantes.AllowUserToAddRows = false;
            this.dtgComprobantes.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dtgComprobantes.BackgroundColor = System.Drawing.Color.White;
            dataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle6.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle6.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle6.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle6.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle6.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle6.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dtgComprobantes.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle6;
            this.dtgComprobantes.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dtgComprobantes.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.ROW_ID_t,
            this.numFieldc,
            this.TIPO_CFDI,
            this.INVOICE_ID,
            this.TRANSACCIONES,
            this.INVOICE_DATE,
            this.UUID,
            this.RFC,
            this.monto});
            dataGridViewCellStyle8.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle8.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle8.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle8.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle8.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle8.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle8.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dtgComprobantes.DefaultCellStyle = dataGridViewCellStyle8;
            this.dtgComprobantes.EnableHeadersVisualStyles = false;
            this.dtgComprobantes.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(170)))), ((int)(((byte)(170)))), ((int)(((byte)(170)))));
            this.dtgComprobantes.Location = new System.Drawing.Point(3, 61);
            this.dtgComprobantes.Name = "dtgComprobantes";
            dataGridViewCellStyle9.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle9.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle9.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle9.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle9.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle9.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle9.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dtgComprobantes.RowHeadersDefaultCellStyle = dataGridViewCellStyle9;
            this.dtgComprobantes.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dtgComprobantes.Size = new System.Drawing.Size(840, 287);
            this.dtgComprobantes.TabIndex = 584;
            // 
            // ROW_ID_t
            // 
            this.ROW_ID_t.HeaderText = "ROW_ID";
            this.ROW_ID_t.Name = "ROW_ID_t";
            this.ROW_ID_t.Visible = false;
            // 
            // numFieldc
            // 
            this.numFieldc.HeaderText = "P�liza";
            this.numFieldc.Name = "numFieldc";
            this.numFieldc.ReadOnly = true;
            this.numFieldc.Width = 70;
            // 
            // TIPO_CFDI
            // 
            this.TIPO_CFDI.HeaderText = "Tipo";
            this.TIPO_CFDI.Name = "TIPO_CFDI";
            this.TIPO_CFDI.ReadOnly = true;
            this.TIPO_CFDI.Width = 80;
            // 
            // INVOICE_ID
            // 
            this.INVOICE_ID.HeaderText = "Folio";
            this.INVOICE_ID.Name = "INVOICE_ID";
            this.INVOICE_ID.ReadOnly = true;
            // 
            // TRANSACCIONES
            // 
            this.TRANSACCIONES.HeaderText = "Transacci�n";
            this.TRANSACCIONES.Name = "TRANSACCIONES";
            this.TRANSACCIONES.ReadOnly = true;
            this.TRANSACCIONES.Width = 120;
            // 
            // INVOICE_DATE
            // 
            this.INVOICE_DATE.HeaderText = "Fecha";
            this.INVOICE_DATE.Name = "INVOICE_DATE";
            this.INVOICE_DATE.ReadOnly = true;
            this.INVOICE_DATE.Visible = false;
            // 
            // UUID
            // 
            this.UUID.HeaderText = "Folio Fiscal";
            this.UUID.Name = "UUID";
            this.UUID.ReadOnly = true;
            this.UUID.Width = 200;
            // 
            // RFC
            // 
            this.RFC.HeaderText = "RFC";
            this.RFC.Name = "RFC";
            this.RFC.ReadOnly = true;
            // 
            // monto
            // 
            dataGridViewCellStyle7.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.monto.DefaultCellStyle = dataGridViewCellStyle7;
            this.monto.HeaderText = "Monto";
            this.monto.Name = "monto";
            this.monto.ReadOnly = true;
            // 
            // buttonX10
            // 
            this.buttonX10.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.buttonX10.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.buttonX10.Image = global::Desarrollo.Properties.Resources.visual;
            this.buttonX10.Location = new System.Drawing.Point(87, 3);
            this.buttonX10.Name = "buttonX10";
            this.buttonX10.Size = new System.Drawing.Size(166, 24);
            this.buttonX10.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.buttonX10.SubItems.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.buttonItem5,
            this.buttonItem1});
            this.buttonX10.TabIndex = 583;
            this.buttonX10.Text = "Importar: Comprobantes";
            this.buttonX10.Click += new System.EventHandler(this.buttonX10_Click);
            // 
            // buttonItem5
            // 
            this.buttonItem5.GlobalItem = false;
            this.buttonItem5.Image = global::Desarrollo.Properties.Resources.excel;
            this.buttonItem5.Name = "buttonItem5";
            this.buttonItem5.Text = "Importar desde Excel";
            this.buttonItem5.Click += new System.EventHandler(this.buttonItem5_Click);
            // 
            // buttonItem1
            // 
            this.buttonItem1.GlobalItem = false;
            this.buttonItem1.Image = global::Desarrollo.Properties.Resources.wizard;
            this.buttonItem1.Name = "buttonItem1";
            this.buttonItem1.Text = "Generar Plantilla";
            this.buttonItem1.Click += new System.EventHandler(this.buttonItem1_Click_1);
            // 
            // superTabItem5
            // 
            this.superTabItem5.AttachedControl = this.superTabControlPanel2;
            this.superTabItem5.GlobalItem = false;
            this.superTabItem5.Name = "superTabItem5";
            this.superTabItem5.Text = "Comprobantes";
            // 
            // superTabControlPanel4
            // 
            this.superTabControlPanel4.Controls.Add(this.labelX6);
            this.superTabControlPanel4.Controls.Add(buttonX9);
            this.superTabControlPanel4.Controls.Add(this.buttonX23);
            this.superTabControlPanel4.Controls.Add(this.dtgValidador);
            this.superTabControlPanel4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.superTabControlPanel4.Location = new System.Drawing.Point(0, 0);
            this.superTabControlPanel4.Name = "superTabControlPanel4";
            this.superTabControlPanel4.Size = new System.Drawing.Size(846, 351);
            this.superTabControlPanel4.TabIndex = 0;
            this.superTabControlPanel4.TabItem = this.superTabItem3;
            // 
            // labelX6
            // 
            this.labelX6.AutoSize = true;
            this.labelX6.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.labelX6.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX6.ForeColor = System.Drawing.Color.Black;
            this.labelX6.Location = new System.Drawing.Point(255, 7);
            this.labelX6.Name = "labelX6";
            this.labelX6.Size = new System.Drawing.Size(383, 17);
            this.labelX6.TabIndex = 595;
            this.labelX6.Text = "Verifica si las p�lizas exportadas concuerdan con la Balanza  de Comprobaci�n";
            // 
            // buttonX23
            // 
            this.buttonX23.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.buttonX23.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.buttonX23.Image = global::Desarrollo.Properties.Resources.actualizar_18;
            this.buttonX23.Location = new System.Drawing.Point(3, 3);
            this.buttonX23.Name = "buttonX23";
            this.buttonX23.Size = new System.Drawing.Size(78, 24);
            this.buttonX23.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.buttonX23.TabIndex = 580;
            this.buttonX23.Text = "Refrescar";
            this.buttonX23.Click += new System.EventHandler(this.buttonX23_Click);
            // 
            // dtgValidador
            // 
            this.dtgValidador.AllowUserToAddRows = false;
            this.dtgValidador.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dtgValidador.BackgroundColor = System.Drawing.Color.White;
            dataGridViewCellStyle10.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle10.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle10.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle10.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle10.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle10.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle10.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dtgValidador.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle10;
            this.dtgValidador.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dtgValidador.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn1,
            this.TIPO,
            this.CUENTA,
            this.MENSAJE});
            dataGridViewCellStyle11.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle11.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle11.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle11.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle11.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle11.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle11.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dtgValidador.DefaultCellStyle = dataGridViewCellStyle11;
            this.dtgValidador.EnableHeadersVisualStyles = false;
            this.dtgValidador.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(170)))), ((int)(((byte)(170)))), ((int)(((byte)(170)))));
            this.dtgValidador.Location = new System.Drawing.Point(3, 33);
            this.dtgValidador.Name = "dtgValidador";
            dataGridViewCellStyle12.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle12.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle12.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle12.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle12.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle12.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle12.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dtgValidador.RowHeadersDefaultCellStyle = dataGridViewCellStyle12;
            this.dtgValidador.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dtgValidador.Size = new System.Drawing.Size(840, 315);
            this.dtgValidador.TabIndex = 7;
            // 
            // dataGridViewTextBoxColumn1
            // 
            this.dataGridViewTextBoxColumn1.HeaderText = "ROW_ID";
            this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
            this.dataGridViewTextBoxColumn1.Visible = false;
            // 
            // TIPO
            // 
            this.TIPO.HeaderText = "Tipo";
            this.TIPO.Name = "TIPO";
            this.TIPO.ReadOnly = true;
            // 
            // CUENTA
            // 
            this.CUENTA.HeaderText = "Cuenta";
            this.CUENTA.Name = "CUENTA";
            this.CUENTA.ReadOnly = true;
            this.CUENTA.Width = 200;
            // 
            // MENSAJE
            // 
            this.MENSAJE.HeaderText = "Descripci�n";
            this.MENSAJE.Name = "MENSAJE";
            this.MENSAJE.ReadOnly = true;
            this.MENSAJE.Width = 400;
            // 
            // superTabItem3
            // 
            this.superTabItem3.AttachedControl = this.superTabControlPanel4;
            this.superTabItem3.GlobalItem = false;
            this.superTabItem3.Name = "superTabItem3";
            this.superTabItem3.Text = "Punto de Control";
            // 
            // superTabControlPanel1
            // 
            this.superTabControlPanel1.Controls.Add(buttonX8);
            this.superTabControlPanel1.Controls.Add(this.buttonX6);
            this.superTabControlPanel1.Controls.Add(this.buttonX5);
            this.superTabControlPanel1.Controls.Add(this.buttonX4);
            this.superTabControlPanel1.Controls.Add(this.dtgTipos);
            this.superTabControlPanel1.Controls.Add(this.buttonX22);
            this.superTabControlPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.superTabControlPanel1.Location = new System.Drawing.Point(0, 0);
            this.superTabControlPanel1.Name = "superTabControlPanel1";
            this.superTabControlPanel1.Size = new System.Drawing.Size(846, 351);
            this.superTabControlPanel1.TabIndex = 0;
            this.superTabControlPanel1.TabItem = this.superTabItem4;
            // 
            // buttonX6
            // 
            this.buttonX6.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.buttonX6.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.buttonX6.Image = global::Desarrollo.Properties.Resources.actualizar_18;
            this.buttonX6.Location = new System.Drawing.Point(3, 3);
            this.buttonX6.Name = "buttonX6";
            this.buttonX6.Size = new System.Drawing.Size(78, 24);
            this.buttonX6.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.buttonX6.TabIndex = 602;
            this.buttonX6.Text = "Refrescar";
            this.buttonX6.Click += new System.EventHandler(this.buttonX6_Click);
            // 
            // buttonX5
            // 
            this.buttonX5.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.buttonX5.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.buttonX5.Image = global::Desarrollo.Properties.Resources.sat_18;
            this.buttonX5.Location = new System.Drawing.Point(390, 3);
            this.buttonX5.Name = "buttonX5";
            this.buttonX5.Size = new System.Drawing.Size(238, 24);
            this.buttonX5.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.buttonX5.TabIndex = 601;
            this.buttonX5.Text = "Auxiliar de folios de comprobantes fiscales";
            this.buttonX5.Click += new System.EventHandler(this.buttonX5_Click);
            // 
            // buttonX4
            // 
            this.buttonX4.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.buttonX4.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.buttonX4.Image = global::Desarrollo.Properties.Resources.sat_18;
            this.buttonX4.Location = new System.Drawing.Point(196, 3);
            this.buttonX4.Name = "buttonX4";
            this.buttonX4.Size = new System.Drawing.Size(188, 24);
            this.buttonX4.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.buttonX4.SubItems.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.buttonItem2});
            this.buttonX4.TabIndex = 600;
            this.buttonX4.Text = "Auxiliar de cuenta y subcuenta";
            this.buttonX4.Click += new System.EventHandler(this.buttonX4_Click);
            // 
            // buttonItem2
            // 
            this.buttonItem2.GlobalItem = false;
            this.buttonItem2.Image = global::Desarrollo.Properties.Resources.excel;
            this.buttonItem2.Name = "buttonItem2";
            this.buttonItem2.Text = "Exportar a Excel";
            this.buttonItem2.Click += new System.EventHandler(this.buttonItem2_Click);
            // 
            // dtgTipos
            // 
            this.dtgTipos.AllowUserToAddRows = false;
            this.dtgTipos.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dtgTipos.BackgroundColor = System.Drawing.Color.White;
            dataGridViewCellStyle13.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle13.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle13.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle13.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle13.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle13.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle13.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dtgTipos.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle13;
            this.dtgTipos.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dtgTipos.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.ROW_ID,
            this.TIPO_tr,
            this.TIPO_SOLICITUD,
            this.numero,
            this.dataGridViewTextBoxColumn5,
            this.dataGridViewTextBoxColumn6});
            dataGridViewCellStyle14.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle14.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle14.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle14.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle14.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle14.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle14.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dtgTipos.DefaultCellStyle = dataGridViewCellStyle14;
            this.dtgTipos.EnableHeadersVisualStyles = false;
            this.dtgTipos.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(170)))), ((int)(((byte)(170)))), ((int)(((byte)(170)))));
            this.dtgTipos.Location = new System.Drawing.Point(3, 33);
            this.dtgTipos.Name = "dtgTipos";
            dataGridViewCellStyle15.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle15.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle15.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle15.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle15.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle15.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle15.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dtgTipos.RowHeadersDefaultCellStyle = dataGridViewCellStyle15;
            this.dtgTipos.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dtgTipos.Size = new System.Drawing.Size(840, 315);
            this.dtgTipos.TabIndex = 597;
            // 
            // ROW_ID
            // 
            this.ROW_ID.HeaderText = "ROW_ID";
            this.ROW_ID.Name = "ROW_ID";
            this.ROW_ID.Visible = false;
            // 
            // TIPO_tr
            // 
            this.TIPO_tr.HeaderText = "Tipo";
            this.TIPO_tr.Name = "TIPO_tr";
            this.TIPO_tr.ReadOnly = true;
            // 
            // TIPO_SOLICITUD
            // 
            this.TIPO_SOLICITUD.HeaderText = "Tipo Solicitud";
            this.TIPO_SOLICITUD.Name = "TIPO_SOLICITUD";
            this.TIPO_SOLICITUD.ReadOnly = true;
            this.TIPO_SOLICITUD.Width = 200;
            // 
            // numero
            // 
            this.numero.HeaderText = "N�mero";
            this.numero.Name = "numero";
            this.numero.ReadOnly = true;
            this.numero.Width = 150;
            // 
            // dataGridViewTextBoxColumn5
            // 
            this.dataGridViewTextBoxColumn5.HeaderText = "Fecha";
            this.dataGridViewTextBoxColumn5.Name = "dataGridViewTextBoxColumn5";
            this.dataGridViewTextBoxColumn5.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn6
            // 
            this.dataGridViewTextBoxColumn6.HeaderText = "Usuario";
            this.dataGridViewTextBoxColumn6.Name = "dataGridViewTextBoxColumn6";
            this.dataGridViewTextBoxColumn6.ReadOnly = true;
            // 
            // buttonX22
            // 
            this.buttonX22.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.buttonX22.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.buttonX22.Image = global::Desarrollo.Properties.Resources.sat_18;
            this.buttonX22.Location = new System.Drawing.Point(87, 3);
            this.buttonX22.Name = "buttonX22";
            this.buttonX22.Size = new System.Drawing.Size(103, 24);
            this.buttonX22.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.buttonX22.TabIndex = 586;
            this.buttonX22.Text = "Generar P�liza";
            this.buttonX22.Click += new System.EventHandler(this.buttonX22_Click_1);
            // 
            // superTabItem4
            // 
            this.superTabItem4.AttachedControl = this.superTabControlPanel1;
            this.superTabItem4.GlobalItem = false;
            this.superTabItem4.Name = "superTabItem4";
            this.superTabItem4.Text = "Generar XML";
            // 
            // superTabItem1
            // 
            this.superTabItem1.GlobalItem = false;
            this.superTabItem1.Name = "superTabItem1";
            this.superTabItem1.Text = "Cuentas Contables";
            // 
            // superTabItem2
            // 
            this.superTabItem2.GlobalItem = false;
            this.superTabItem2.Name = "superTabItem2";
            this.superTabItem2.Text = "Balanza de Comprobaci�n";
            // 
            // ESTADO
            // 
            this.ESTADO.AutoCompleteCustomSource.AddRange(new string[] {
            "Abierto",
            "Cerrado"});
            this.ESTADO.DisplayMember = "Text";
            this.ESTADO.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.ESTADO.ForeColor = System.Drawing.Color.Black;
            this.ESTADO.FormattingEnabled = true;
            this.ESTADO.ItemHeight = 16;
            this.ESTADO.Items.AddRange(new object[] {
            this.comboItem3,
            this.comboItem4});
            this.ESTADO.Location = new System.Drawing.Point(606, 12);
            this.ESTADO.Name = "ESTADO";
            this.ESTADO.Size = new System.Drawing.Size(109, 22);
            this.ESTADO.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.ESTADO.TabIndex = 23;
            // 
            // comboItem3
            // 
            this.comboItem3.Text = "Abierto";
            // 
            // comboItem4
            // 
            this.comboItem4.Text = "Cerrado";
            // 
            // labelX5
            // 
            this.labelX5.AutoSize = true;
            this.labelX5.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.labelX5.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX5.ForeColor = System.Drawing.Color.Black;
            this.labelX5.Location = new System.Drawing.Point(568, 15);
            this.labelX5.Name = "labelX5";
            this.labelX5.Size = new System.Drawing.Size(35, 17);
            this.labelX5.TabIndex = 22;
            this.labelX5.Text = "Estado";
            // 
            // backgroundWorker3
            // 
            this.backgroundWorker3.WorkerReportsProgress = true;
            this.backgroundWorker3.WorkerSupportsCancellation = true;
            this.backgroundWorker3.DoWork += new System.ComponentModel.DoWorkEventHandler(this.backgroundWorker3_DoWork);
            this.backgroundWorker3.ProgressChanged += new System.ComponentModel.ProgressChangedEventHandler(this.backgroundWorker3_ProgressChanged);
            this.backgroundWorker3.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.backgroundWorker3_RunWorkerCompleted);
            // 
            // bW1
            // 
            this.bW1.WorkerReportsProgress = true;
            this.bW1.WorkerSupportsCancellation = true;
            // 
            // backgroundWorker1
            // 
            this.backgroundWorker1.WorkerReportsProgress = true;
            this.backgroundWorker1.WorkerSupportsCancellation = true;
            this.backgroundWorker1.DoWork += new System.ComponentModel.DoWorkEventHandler(this.backgroundWorker1_DoWork);
            this.backgroundWorker1.ProgressChanged += new System.ComponentModel.ProgressChangedEventHandler(this.backgroundWorker1_ProgressChanged);
            this.backgroundWorker1.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.backgroundWorker1_RunWorkerCompleted);
            // 
            // frmDeclaracionPoliza
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.BottomLeftCornerSize = 0;
            this.ClientSize = new System.Drawing.Size(846, 418);
            this.Controls.Add(this.ESTADO);
            this.Controls.Add(this.labelX5);
            this.Controls.Add(this.superTabControl1);
            this.Controls.Add(this.mesField);
            this.Controls.Add(this.labelX4);
            this.Controls.Add(this.totalCtasField);
            this.Controls.Add(this.labelX3);
            this.Controls.Add(this.anoField);
            this.Controls.Add(this.labelX2);
            this.Controls.Add(this.labelX1);
            this.Controls.Add(this.rFCField);
            this.Controls.Add(this.versionField);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.metroStatusBar1);
            this.Cursor = System.Windows.Forms.Cursors.Default;
            this.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "frmDeclaracionPoliza";
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Declaraci�n de P�liza";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frmDeclaracionPoliza_FormClosing);
            ((System.ComponentModel.ISupportInitialize)(this.superTabControl1)).EndInit();
            this.superTabControl1.ResumeLayout(false);
            this.superTabControlPanel3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dtgPoliza)).EndInit();
            this.superTabControlPanel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dtgComprobantes)).EndInit();
            this.superTabControlPanel4.ResumeLayout(false);
            this.superTabControlPanel4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dtgValidador)).EndInit();
            this.superTabControlPanel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dtgTipos)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevComponents.DotNetBar.Metro.MetroStatusBar metroStatusBar1;
        private DevComponents.DotNetBar.LabelItem labelItem1;
        private DevComponents.DotNetBar.Controls.ComboBoxEx mesField;
        private DevComponents.Editors.ComboItem comboItem1;
        private DevComponents.Editors.ComboItem comboItem2;
        private DevComponents.DotNetBar.LabelX labelX4;
        private DevComponents.DotNetBar.Controls.TextBoxX totalCtasField;
        private DevComponents.DotNetBar.LabelX labelX3;
        private DevComponents.DotNetBar.Controls.TextBoxX anoField;
        private DevComponents.DotNetBar.LabelX labelX2;
        private DevComponents.DotNetBar.LabelX labelX1;
        private DevComponents.DotNetBar.Controls.TextBoxX rFCField;
        private DevComponents.DotNetBar.Controls.TextBoxX versionField;
        private DevComponents.DotNetBar.LabelX label1;
        private DevComponents.DotNetBar.SuperTabControl superTabControl1;
        private DevComponents.DotNetBar.SuperTabItem superTabItem1;
        private DevComponents.DotNetBar.SuperTabItem superTabItem2;
        private DevComponents.DotNetBar.SuperTabControlPanel superTabControlPanel3;
        private DevComponents.DotNetBar.SuperTabItem Polizas;
        private DevComponents.DotNetBar.Controls.ComboBoxEx ESTADO;
        private DevComponents.Editors.ComboItem comboItem3;
        private DevComponents.Editors.ComboItem comboItem4;
        private DevComponents.DotNetBar.LabelX labelX5;
        private System.Windows.Forms.FolderBrowserDialog folderBrowserDialog1;
        private DevComponents.DotNetBar.Controls.CircularProgress circularProgress3;
        private DevComponents.DotNetBar.ButtonItem buttonItem3;
        private DevComponents.DotNetBar.ButtonX buttonX16;
        private DevComponents.DotNetBar.ButtonX buttonX18;
        private DevComponents.DotNetBar.ButtonX buttonX19;
        private DevComponents.DotNetBar.Controls.DataGridViewX dtgPoliza;
        private System.ComponentModel.BackgroundWorker backgroundWorker3;
        private DevComponents.Editors.ComboItem comboItem5;
        private DevComponents.Editors.ComboItem comboItem6;
        private DevComponents.Editors.ComboItem comboItem7;
        private DevComponents.Editors.ComboItem comboItem8;
        private DevComponents.Editors.ComboItem comboItem9;
        private DevComponents.Editors.ComboItem comboItem10;
        private DevComponents.Editors.ComboItem comboItem11;
        private DevComponents.Editors.ComboItem comboItem12;
        private DevComponents.Editors.ComboItem comboItem13;
        private DevComponents.Editors.ComboItem comboItem14;
        private DevComponents.DotNetBar.SuperTabControlPanel superTabControlPanel4;
        private DevComponents.DotNetBar.SuperTabItem superTabItem3;
        private DevComponents.DotNetBar.ButtonX buttonX23;
        private DevComponents.DotNetBar.Controls.DataGridViewX dtgValidador;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn TIPO;
        private System.Windows.Forms.DataGridViewTextBoxColumn CUENTA;
        private System.Windows.Forms.DataGridViewTextBoxColumn MENSAJE;
        private DevComponents.DotNetBar.ButtonX buttonX25;
        private DevComponents.DotNetBar.Controls.TextBoxX BUSCAR_BATCH_ID;
        private DevComponents.DotNetBar.ButtonItem buttonItem7;
        private DevComponents.DotNetBar.SuperTabControlPanel superTabControlPanel1;
        private DevComponents.DotNetBar.ButtonX buttonX22;
        private DevComponents.DotNetBar.SuperTabItem superTabItem4;
        private DevComponents.DotNetBar.LabelItem labelItem2;
        private DevComponents.DotNetBar.Controls.DataGridViewX dtgTipos;
        private DevComponents.DotNetBar.ButtonX buttonX5;
        private DevComponents.DotNetBar.ButtonX buttonX4;
        private DevComponents.DotNetBar.ButtonX buttonX6;
        private System.ComponentModel.BackgroundWorker bW1;
        private System.Windows.Forms.DataGridViewTextBoxColumn ROW_ID;
        private System.Windows.Forms.DataGridViewTextBoxColumn TIPO_tr;
        private System.Windows.Forms.DataGridViewTextBoxColumn TIPO_SOLICITUD;
        private System.Windows.Forms.DataGridViewTextBoxColumn numero;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn5;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn6;
        private DevComponents.DotNetBar.ButtonItem buttonItem2;
        private DevComponents.DotNetBar.SuperTabControlPanel superTabControlPanel2;
        private DevComponents.DotNetBar.SuperTabItem superTabItem5;
        private DevComponents.DotNetBar.Controls.DataGridViewX dtgComprobantes;
        private DevComponents.DotNetBar.ButtonX buttonX10;
        private System.Windows.Forms.DataGridViewTextBoxColumn ROW_ID_p;
        private System.Windows.Forms.DataGridViewTextBoxColumn tipoField;
        private System.Windows.Forms.DataGridViewTextBoxColumn numField;
        private System.Windows.Forms.DataGridViewTextBoxColumn fechaField;
        private System.Windows.Forms.DataGridViewTextBoxColumn conceptoField;
        private System.Windows.Forms.DataGridViewTextBoxColumn DEBE;
        private System.Windows.Forms.DataGridViewTextBoxColumn HABER;
        private DevComponents.DotNetBar.Controls.DataGridViewCheckBoxXColumn Comprobantes;
        private DevComponents.DotNetBar.ButtonItem buttonItem5;
        private DevComponents.DotNetBar.ButtonX buttonX11;
        private DevComponents.DotNetBar.ButtonItem buttonItem1;
        private DevComponents.DotNetBar.ButtonX buttonX7;
        private DevComponents.DotNetBar.Controls.TextBoxX BUSCAR_UUID;
        private DevComponents.DotNetBar.Controls.TextBoxX BUSCAR_RFC;
        private DevComponents.DotNetBar.Controls.TextBoxX BUSCAR_CFDI_POLIZA;
        private DevComponents.DotNetBar.Controls.CircularProgress circularProgress1;
        private System.ComponentModel.BackgroundWorker backgroundWorker1;
        private System.Windows.Forms.DataGridViewTextBoxColumn ROW_ID_t;
        private System.Windows.Forms.DataGridViewTextBoxColumn numFieldc;
        private System.Windows.Forms.DataGridViewTextBoxColumn TIPO_CFDI;
        private System.Windows.Forms.DataGridViewTextBoxColumn INVOICE_ID;
        private System.Windows.Forms.DataGridViewTextBoxColumn TRANSACCIONES;
        private System.Windows.Forms.DataGridViewTextBoxColumn INVOICE_DATE;
        private System.Windows.Forms.DataGridViewTextBoxColumn UUID;
        private System.Windows.Forms.DataGridViewTextBoxColumn RFC;
        private System.Windows.Forms.DataGridViewTextBoxColumn monto;
        private DevComponents.DotNetBar.LabelX labelX6;
        private DevComponents.DotNetBar.Controls.CheckBoxX filtrarComprobantes;

    }
}