﻿using DevComponents.DotNetBar;
using Generales;
using ModeloDocumentosFX;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Desarrollo
{
    class cEMPRESA_MONEDA
    {
        private string sSQL = "";
        public string ROW_ID { get; set; }
        public string ROW_ID_EMPRESA { get; set; }
        
        public string VISUAL_MONEDA { get; set; }
        public string MONEDA { get; set; }

        public cCONEXCION oData = new cCONEXCION();
        public override string ToString() { return ROW_ID; }

        public cEMPRESA_MONEDA()
        {
            ModeloDocumentosFXContainer dbContext = new ModeloDocumentosFXContainer();
            try
            {
                string conection = dbContext.Database.Connection.ConnectionString;
                oData = new cCONEXCION(conection);
            }
            catch (Exception err)
            {
                ErrorFX.mostrar(err, false, true, "frmEmpresa - 96 - ", false);
            }
            limpiar();
        }
        public void limpiar()
        {
            ROW_ID = "";
            ROW_ID_EMPRESA = "";
            VISUAL_MONEDA = "";
            MONEDA = "";            
        }

        public List<cEMPRESA_MONEDA> todos(string ROW_ID_EMPRESAp = "", string RFCp = "")
        {

            List<cEMPRESA_MONEDA> lista = new List<cEMPRESA_MONEDA>();

            sSQL = " SELECT * ";
            sSQL += " FROM EMPRESA_MONEDA ";
            sSQL += " WHERE 1=1 ";
            if(RFCp!="")
            {
                sSQL += " AND '" + RFCp + "' IN (SELECT RFC FROM EMPRESA E WHERE E.ROW_ID=EMPRESA_MONEDA.ROW_ID_EMPRESA) ";
            }
            if (ROW_ID_EMPRESAp!="")
            {
                sSQL += " AND ROW_ID_EMPRESA=" + ROW_ID_EMPRESAp + " ";
            }
            

            sSQL += " ORDER BY ROW_ID";
            DataTable oDataTable = oData.EjecutarConsulta(sSQL);
            foreach (DataRow oDataRow in oDataTable.Rows)
            {
                cEMPRESA_MONEDA ocEMPRESA_MONEDA = new cEMPRESA_MONEDA();
                ocEMPRESA_MONEDA.cargar(oDataRow["ROW_ID"].ToString());
                lista.Add(ocEMPRESA_MONEDA);
            }
            return lista;

        }

        public bool cargar(string ROW_IDp)
        {
            sSQL = " SELECT * ";
            sSQL += " FROM EMPRESA_MONEDA ";
            sSQL += " WHERE ROW_ID=" + ROW_IDp + " ";
            DataTable oDataTable = oData.EjecutarConsulta(sSQL);
            if (oDataTable != null)
            {
                foreach (DataRow oDataRow in oDataTable.Rows)
                {
                    //Añadir el Registro
                    ROW_ID = oDataRow["ROW_ID"].ToString();
                    ROW_ID_EMPRESA = oDataRow["ROW_ID_EMPRESA"].ToString();
                    VISUAL_MONEDA = oDataRow["VISUAL_MONEDA"].ToString();
                    MONEDA = oDataRow["MONEDA"].ToString();
                    return true;
                }
            }
            return false;
        }

        public bool guardar()
        {
            if (ROW_ID_EMPRESA.Trim() == "")
            {
                MessageBoxEx.Show("El ROW_ID_EMPRESA es requerido.", Application.ProductName + "-" + Application.ProductVersion, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return false;
            }

            if (ROW_ID == "")
            {
                sSQL = " INSERT INTO EMPRESA_MONEDA ";
                sSQL += " ( ";
                sSQL += " [ROW_ID_EMPRESA] ";
                sSQL += " ,[VISUAL_MONEDA],[MONEDA]"; 
                sSQL += " )";
                sSQL += " VALUES ";
                sSQL += "(";
                sSQL += " '" + ROW_ID_EMPRESA+ "'";
                sSQL += " ,'" + VISUAL_MONEDA + "','" + MONEDA + "'";
                sSQL += ")";
                DataTable oDataTable = oData.EjecutarConsulta(sSQL);
                if (oDataTable != null)
                {

                    sSQL = "SELECT TOP 1 MAX(ROW_ID) as ROW_ID FROM EMPRESA_MONEDA";
                    oDataTable = oData.EjecutarConsulta(sSQL);
                    if (oDataTable != null)
                    {
                        foreach (DataRow oDataRow in oDataTable.Rows)
                        {
                            ROW_ID = oDataRow["ROW_ID"].ToString();
                        }
                    }
                }

            }
            else
            {
                sSQL = " UPDATE EMPRESA_MONEDA ";
                sSQL += " SET ";
                sSQL += " VISUAL_MONEDA='" + VISUAL_MONEDA + "',MONEDA='" + MONEDA + "'";
                sSQL += " WHERE ROW_ID=" + ROW_ID;
                oData.EjecutarConsulta(sSQL);

            }
            return true;

        }

        public bool eliminar(string ROW_IDp)
        {
            if (ROW_IDp != "")
            {
                sSQL = "DELETE FROM EMPRESA_MONEDA ";
                sSQL += " WHERE ROW_ID='" + ROW_IDp + "' ";
                oData.EjecutarConsulta(sSQL);
                return true;
            }
            return false;
        }

        internal string cambiar_MONEDA(string MONEDAp, string ROW_ID_EMPRESAp="")
        {
            cCONEXCION oData = new cCONEXCION();
            DataTable oDataTable;
            string sSQL = "";
            try
            {

                
                sSQL = " SELECT TOP 1 * ";
                sSQL += " FROM EMPRESA_MONEDA ";
                sSQL += " WHERE VISUAL_MONEDA like '" + MONEDAp + "' ";
                if(ROW_ID_EMPRESAp!="")
                {
                    sSQL += " AND ROW_ID_EMPRESA=" + ROW_ID_EMPRESAp;
                }
                
                oDataTable = oData.EjecutarConsulta(sSQL);
                if (oDataTable != null)
                {
                    foreach (DataRow oDataRow in oDataTable.Rows)
                    {
                        //Añadir el Registro
                        return oDataRow["MONEDA"].ToString();
                    }
                }
                //Devolver moneda por defecto MXN Pesos
                return "MXN";
            }
            catch(Exception e)
            {

            }
            return "MXN";
        }
    }
}
