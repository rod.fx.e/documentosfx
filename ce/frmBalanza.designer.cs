namespace Desarrollo
{
    partial class frmBalanza
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            DevComponents.DotNetBar.ButtonX buttonX3;
            DevComponents.DotNetBar.ButtonX buttonX1;
            DevComponents.DotNetBar.ButtonX buttonX2;
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmBalanza));
            this.metroStatusBar1 = new DevComponents.DotNetBar.Metro.MetroStatusBar();
            this.labelItem1 = new DevComponents.DotNetBar.LabelItem();
            this.numCtaField = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.labelX1 = new DevComponents.DotNetBar.LabelX();
            this.labelX2 = new DevComponents.DotNetBar.LabelX();
            this.saldoFinField = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.labelX5 = new DevComponents.DotNetBar.LabelX();
            this.haberField = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.labelX4 = new DevComponents.DotNetBar.LabelX();
            this.debeField = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.labelX3 = new DevComponents.DotNetBar.LabelX();
            this.saldoIniField = new DevComponents.DotNetBar.Controls.TextBoxX();
            buttonX3 = new DevComponents.DotNetBar.ButtonX();
            buttonX1 = new DevComponents.DotNetBar.ButtonX();
            buttonX2 = new DevComponents.DotNetBar.ButtonX();
            this.SuspendLayout();
            // 
            // buttonX3
            // 
            buttonX3.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            buttonX3.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            buttonX3.Image = global::Desarrollo.Properties.Resources.Eliminar_18;
            buttonX3.Location = new System.Drawing.Point(141, 0);
            buttonX3.Name = "buttonX3";
            buttonX3.Size = new System.Drawing.Size(68, 24);
            buttonX3.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            buttonX3.TabIndex = 2;
            buttonX3.Text = "Eliminar";
            buttonX3.TextAlignment = DevComponents.DotNetBar.eButtonTextAlignment.Right;
            buttonX3.Click += new System.EventHandler(this.buttonX3_Click);
            // 
            // buttonX1
            // 
            buttonX1.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            buttonX1.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            buttonX1.Image = global::Desarrollo.Properties.Resources.Nueno_18;
            buttonX1.Location = new System.Drawing.Point(67, 0);
            buttonX1.Name = "buttonX1";
            buttonX1.Size = new System.Drawing.Size(68, 24);
            buttonX1.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            buttonX1.TabIndex = 1;
            buttonX1.Text = "Guardar";
            buttonX1.TextAlignment = DevComponents.DotNetBar.eButtonTextAlignment.Right;
            buttonX1.Click += new System.EventHandler(this.buttonX1_Click);
            // 
            // buttonX2
            // 
            buttonX2.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            buttonX2.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            buttonX2.Image = global::Desarrollo.Properties.Resources.Nuevo_18_19;
            buttonX2.Location = new System.Drawing.Point(0, 0);
            buttonX2.Name = "buttonX2";
            buttonX2.Size = new System.Drawing.Size(61, 24);
            buttonX2.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            buttonX2.TabIndex = 0;
            buttonX2.Text = "Nuevo";
            buttonX2.TextAlignment = DevComponents.DotNetBar.eButtonTextAlignment.Right;
            buttonX2.Click += new System.EventHandler(this.buttonX2_Click);
            // 
            // metroStatusBar1
            // 
            this.metroStatusBar1.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.metroStatusBar1.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.metroStatusBar1.ContainerControlProcessDialogKey = true;
            this.metroStatusBar1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.metroStatusBar1.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.metroStatusBar1.ForeColor = System.Drawing.Color.Black;
            this.metroStatusBar1.Items.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.labelItem1});
            this.metroStatusBar1.Location = new System.Drawing.Point(0, 166);
            this.metroStatusBar1.Name = "metroStatusBar1";
            this.metroStatusBar1.Size = new System.Drawing.Size(362, 21);
            this.metroStatusBar1.TabIndex = 8;
            // 
            // labelItem1
            // 
            this.labelItem1.Name = "labelItem1";
            // 
            // numCtaField
            // 
            this.numCtaField.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.numCtaField.Border.Class = "TextBoxBorder";
            this.numCtaField.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.numCtaField.ForeColor = System.Drawing.Color.Black;
            this.numCtaField.Location = new System.Drawing.Point(67, 32);
            this.numCtaField.Name = "numCtaField";
            this.numCtaField.Size = new System.Drawing.Size(290, 22);
            this.numCtaField.TabIndex = 4;
            this.numCtaField.TabStop = false;
            this.numCtaField.WatermarkBehavior = DevComponents.DotNetBar.eWatermarkBehavior.HideNonEmpty;
            // 
            // labelX1
            // 
            this.labelX1.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.labelX1.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX1.ForeColor = System.Drawing.Color.Black;
            this.labelX1.Location = new System.Drawing.Point(0, 30);
            this.labelX1.Name = "labelX1";
            this.labelX1.Size = new System.Drawing.Size(61, 22);
            this.labelX1.TabIndex = 3;
            this.labelX1.Text = "Cuenta";
            this.labelX1.TextAlignment = System.Drawing.StringAlignment.Far;
            // 
            // labelX2
            // 
            this.labelX2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            // 
            // 
            // 
            this.labelX2.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX2.ForeColor = System.Drawing.Color.Black;
            this.labelX2.Location = new System.Drawing.Point(-3, 136);
            this.labelX2.Name = "labelX2";
            this.labelX2.Size = new System.Drawing.Size(61, 22);
            this.labelX2.TabIndex = 11;
            this.labelX2.Text = "Saldo Final";
            this.labelX2.TextAlignment = System.Drawing.StringAlignment.Far;
            // 
            // saldoFinField
            // 
            this.saldoFinField.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            // 
            // 
            // 
            this.saldoFinField.Border.Class = "TextBoxBorder";
            this.saldoFinField.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.saldoFinField.ForeColor = System.Drawing.Color.Black;
            this.saldoFinField.Location = new System.Drawing.Point(67, 138);
            this.saldoFinField.Name = "saldoFinField";
            this.saldoFinField.Size = new System.Drawing.Size(143, 22);
            this.saldoFinField.TabIndex = 12;
            this.saldoFinField.TabStop = false;
            this.saldoFinField.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.saldoFinField.WatermarkBehavior = DevComponents.DotNetBar.eWatermarkBehavior.HideNonEmpty;
            this.saldoFinField.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.Solo_Numero_KeyPress);
            this.saldoFinField.Leave += new System.EventHandler(this.COSTO_Leave);
            // 
            // labelX5
            // 
            this.labelX5.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            // 
            // 
            // 
            this.labelX5.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX5.ForeColor = System.Drawing.Color.Black;
            this.labelX5.Location = new System.Drawing.Point(-3, 110);
            this.labelX5.Name = "labelX5";
            this.labelX5.Size = new System.Drawing.Size(61, 22);
            this.labelX5.TabIndex = 9;
            this.labelX5.Text = "Haber";
            this.labelX5.TextAlignment = System.Drawing.StringAlignment.Far;
            // 
            // haberField
            // 
            this.haberField.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            // 
            // 
            // 
            this.haberField.Border.Class = "TextBoxBorder";
            this.haberField.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.haberField.ForeColor = System.Drawing.Color.Black;
            this.haberField.Location = new System.Drawing.Point(67, 112);
            this.haberField.Name = "haberField";
            this.haberField.Size = new System.Drawing.Size(143, 22);
            this.haberField.TabIndex = 10;
            this.haberField.TabStop = false;
            this.haberField.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.haberField.WatermarkBehavior = DevComponents.DotNetBar.eWatermarkBehavior.HideNonEmpty;
            this.haberField.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.Solo_Numero_KeyPress);
            this.haberField.Leave += new System.EventHandler(this.COSTO_Leave);
            // 
            // labelX4
            // 
            this.labelX4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            // 
            // 
            // 
            this.labelX4.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX4.ForeColor = System.Drawing.Color.Black;
            this.labelX4.Location = new System.Drawing.Point(-3, 84);
            this.labelX4.Name = "labelX4";
            this.labelX4.Size = new System.Drawing.Size(61, 22);
            this.labelX4.TabIndex = 7;
            this.labelX4.Text = "Debe";
            this.labelX4.TextAlignment = System.Drawing.StringAlignment.Far;
            // 
            // debeField
            // 
            this.debeField.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            // 
            // 
            // 
            this.debeField.Border.Class = "TextBoxBorder";
            this.debeField.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.debeField.ForeColor = System.Drawing.Color.Black;
            this.debeField.Location = new System.Drawing.Point(67, 86);
            this.debeField.Name = "debeField";
            this.debeField.Size = new System.Drawing.Size(143, 22);
            this.debeField.TabIndex = 8;
            this.debeField.TabStop = false;
            this.debeField.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.debeField.WatermarkBehavior = DevComponents.DotNetBar.eWatermarkBehavior.HideNonEmpty;
            this.debeField.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.Solo_Numero_KeyPress);
            this.debeField.Leave += new System.EventHandler(this.COSTO_Leave);
            // 
            // labelX3
            // 
            this.labelX3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            // 
            // 
            // 
            this.labelX3.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX3.ForeColor = System.Drawing.Color.Black;
            this.labelX3.Location = new System.Drawing.Point(-3, 58);
            this.labelX3.Name = "labelX3";
            this.labelX3.Size = new System.Drawing.Size(61, 22);
            this.labelX3.TabIndex = 5;
            this.labelX3.Text = "Saldo Inicial";
            this.labelX3.TextAlignment = System.Drawing.StringAlignment.Far;
            // 
            // saldoIniField
            // 
            this.saldoIniField.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            // 
            // 
            // 
            this.saldoIniField.Border.Class = "TextBoxBorder";
            this.saldoIniField.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.saldoIniField.ForeColor = System.Drawing.Color.Black;
            this.saldoIniField.Location = new System.Drawing.Point(67, 60);
            this.saldoIniField.Name = "saldoIniField";
            this.saldoIniField.Size = new System.Drawing.Size(143, 22);
            this.saldoIniField.TabIndex = 6;
            this.saldoIniField.TabStop = false;
            this.saldoIniField.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.saldoIniField.WatermarkBehavior = DevComponents.DotNetBar.eWatermarkBehavior.HideNonEmpty;
            this.saldoIniField.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.Solo_Numero_KeyPress);
            this.saldoIniField.Leave += new System.EventHandler(this.COSTO_Leave);
            // 
            // frmBalanza
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.BottomLeftCornerSize = 0;
            this.ClientSize = new System.Drawing.Size(362, 187);
            this.Controls.Add(this.labelX2);
            this.Controls.Add(this.saldoFinField);
            this.Controls.Add(this.labelX5);
            this.Controls.Add(this.haberField);
            this.Controls.Add(this.labelX4);
            this.Controls.Add(this.debeField);
            this.Controls.Add(this.labelX3);
            this.Controls.Add(this.saldoIniField);
            this.Controls.Add(this.labelX1);
            this.Controls.Add(this.numCtaField);
            this.Controls.Add(buttonX3);
            this.Controls.Add(buttonX1);
            this.Controls.Add(buttonX2);
            this.Controls.Add(this.metroStatusBar1);
            this.Cursor = System.Windows.Forms.Cursors.Default;
            this.DoubleBuffered = true;
            this.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "frmBalanza";
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Balanza";
            this.ResumeLayout(false);

        }

        #endregion

        private DevComponents.DotNetBar.Metro.MetroStatusBar metroStatusBar1;
        private DevComponents.DotNetBar.LabelItem labelItem1;
        private DevComponents.DotNetBar.Controls.TextBoxX numCtaField;
        private DevComponents.DotNetBar.LabelX labelX1;
        private DevComponents.DotNetBar.LabelX labelX2;
        private DevComponents.DotNetBar.Controls.TextBoxX saldoFinField;
        private DevComponents.DotNetBar.LabelX labelX5;
        private DevComponents.DotNetBar.Controls.TextBoxX haberField;
        private DevComponents.DotNetBar.LabelX labelX4;
        private DevComponents.DotNetBar.Controls.TextBoxX debeField;
        private DevComponents.DotNetBar.LabelX labelX3;
        private DevComponents.DotNetBar.Controls.TextBoxX saldoIniField;

    }
}