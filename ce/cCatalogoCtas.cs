﻿using CryptoSysPKI;
using DevComponents.DotNetBar;
using Generales;
using Ionic.Zip;
using ModeloDocumentosFX;
using OfficeOpenXml;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Windows.Forms;
using System.Xml;
using System.Xml.Serialization;
using System.Xml.XPath;
using System.Xml.Xsl;

namespace Desarrollo
{
    class cCatalogoCtas
    {
        private string sSQL = "";
        public string ROW_ID { get; set; }

        public string ROW_ID_CATALOGO { get; set; }

        public string codAgrupField { get; set; }
        public string numCtaField { get; set; }
        public string descField { get; set; }
        public string subCtaDeField { get; set; }
        public string nivelField { get; set; }
        public string naturField { get; set; }

        private cCONEXCION oData = new cCONEXCION();
        public override string ToString() { return ROW_ID; }

        public cCatalogoCtas()
        {
            ModeloDocumentosFXContainer dbContext = new ModeloDocumentosFXContainer();
            try
            {
                string conection = dbContext.Database.Connection.ConnectionString;
                oData = new cCONEXCION(conection);
            }
            catch (Exception err)
            {
                ErrorFX.mostrar(err, false, true, "frmEmpresa - 96 - ", false);
            }
            limpiar();
        }
        public void limpiar()
        {
            ROW_ID = "";
            ROW_ID_CATALOGO = "";
            codAgrupField="";
            numCtaField="";
            descField="";
            subCtaDeField="";
            nivelField="";
            naturField="";
        }
        public bool validar_codigo_agrupador(string codAgrupFieldp)
        {
            DataTable oDataTable = this.obtener_agrupador();
            if (oDataTable==null)
            {
                return false;
            }
            var query = from resultado in oDataTable.AsEnumerable()
                        where resultado["VALUE"].ToString() == codAgrupFieldp.ToString()
                        select resultado;
            foreach (var item in query)
            {
                return true;
            }
            return false;

        }
        public bool generar_archivo(cCatalogo oCatalogop, string pathp, bool mostrar_mensaje_finalizacion=true)
        {
            try
            {
                FolderBrowserDialog folderBrowserDialog1 = new FolderBrowserDialog();

                string path = pathp;
                if (path.Trim() == "")
                {
                    if (folderBrowserDialog1.ShowDialog() == DialogResult.OK)
                    {
                        path = folderBrowserDialog1.SelectedPath;

                    }
                    else
                    {
                        MessageBoxEx.Show("Generación de archivo cancelada.", Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
                        return false;
                    }
                }

                //El RFC (Personas Morales 12 posiciones y Personas Físicas 13 posiciones)
                //Año (Año al que corresponde la información)  (4 caracteres)
                //Mes (Mes al que corresponde la información) (2 caracteres)
                //Identificador (CT= CATÁLOGO DE CUENTAS, PL= PÓLIZAS, BN=BALANZA DE COMPROBACIÓN NORMAL, BC= BALANZA DE COMPROBACIÓN COMPLEMENTARIA, XC=AUXILIAR DE CUENTAS, XF=AUXILIAR DE FOLIOS)
                string mes = oCatalogop.mesField;
                if (mes.Length == 1)
                {
                    mes = "0" + mes;
                }
                string nombre = oCatalogop.rFCField + oCatalogop.anoField + mes + "CT";
                string nombre_tmp = path + "\\" + nombre + ".xml";// +DateTime.Now.ToString("ddMMyyyyhhmmss");

                cCatalogoCtas ocCatalogoCtas = new cCatalogoCtas();

                Catalogo oCatalogo = new Catalogo();
                oCatalogo.Anio = int.Parse(oCatalogop.anoField);

                switch (int.Parse(oCatalogop.mesField))
                {
                    case 1:
                        oCatalogo.Mes = CatalogoMes.Item01;
                        break;
                    case 2:
                        oCatalogo.Mes = CatalogoMes.Item02;
                        break;
                    case 3:
                        oCatalogo.Mes = CatalogoMes.Item03;
                        break;
                    case 4:
                        oCatalogo.Mes = CatalogoMes.Item04;
                        break;
                    case 5:
                        oCatalogo.Mes = CatalogoMes.Item05;
                        break;
                    case 6:
                        oCatalogo.Mes = CatalogoMes.Item06;
                        break;
                    case 7:
                        oCatalogo.Mes = CatalogoMes.Item07;
                        break;
                    case 8:
                        oCatalogo.Mes = CatalogoMes.Item08;
                        break;
                    case 9:
                        oCatalogo.Mes = CatalogoMes.Item09;
                        break;
                    case 10:
                        oCatalogo.Mes = CatalogoMes.Item10;
                        break;
                    case 11:
                        oCatalogo.Mes = CatalogoMes.Item11;
                        break;
                    case 12:
                        oCatalogo.Mes = CatalogoMes.Item12;
                        break;
                }



                oCatalogo.RFC = oCatalogop.rFCField;
                cEMPRESA_CE ocEMPRESA = new cEMPRESA_CE();
                ocEMPRESA.cargar_rfc(oCatalogo.RFC);
                if (ocEMPRESA.NO_CERTIFICADO.Trim().Length==0)
                {
                    MessageBoxEx.Show("No se tiene configurado el Certificado"
                       , Application.ProductName + "-" + Application.ProductVersion, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);

                    return false;
                }
                oCatalogo.noCertificado = ocEMPRESA.NO_CERTIFICADO;
                if (!File.Exists(ocEMPRESA.CERTIFICADO))
                {
                    MessageBoxEx.Show("No se tiene acceso al Certificado, archivo " + ocEMPRESA.CERTIFICADO + ""
                        , Application.ProductName + "-" + Application.ProductVersion, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);

                    return false;
                }
                X509Certificate cert = X509Certificate.CreateFromCertFile(ocEMPRESA.CERTIFICADO);
                string Certificado64 = System.Convert.ToBase64String(cert.GetRawCertData());
                oCatalogo.Certificado = Certificado64;
                
                List<cCatalogoCtas> listaCatalogoCtas = new List<cCatalogoCtas>();
                listaCatalogoCtas = ocCatalogoCtas.todos(oCatalogop.ROW_ID);
                //oCatalogo.TotalCtas = listaCatalogoCtas.Count;
                CatalogoCtas[] oCtas;
                oCtas = new CatalogoCtas[listaCatalogoCtas.Count];
                int contador = 0;
                foreach (cCatalogoCtas registro in listaCatalogoCtas)
                {
                    oCtas[contador] = new CatalogoCtas();
                    oCtas[contador].CodAgrup = registro.codAgrupField;
                    oCtas[contador].NumCta = registro.numCtaField;
                    oCtas[contador].Desc = registro.descField;
                    oCtas[contador].SubCtaDe = null;
                    if (registro.subCtaDeField != "")
                    {
                        oCtas[contador].SubCtaDe = registro.subCtaDeField;
                    }


                    oCtas[contador].Nivel = int.Parse(registro.nivelField);
                    oCtas[contador].Natur = registro.naturField;
                    contador++;
                }
                oCatalogo.Ctas = oCtas;
                crear_archivo(nombre,oCatalogo);
                //Sellar
                oCatalogo.Sello=sellar(nombre + ".XML",ocEMPRESA);
                
                if(oCatalogo.Sello=="")
                {
                    MessageBoxEx.Show("El sello tiene error de generación.", Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return false;
                }

                crear_archivo(nombre, oCatalogo, true);


                //Comprimir
                string ARCHIVO_zip = nombre + ".ZIP";
                using (ZipFile zip = new ZipFile())
                {
                    zip.AddFile(nombre + ".XML");
                    zip.Save(ARCHIVO_zip);
                }
                File.Delete(nombre + ".XML");
                File.Delete(path + "\\" + ARCHIVO_zip);

                File.Copy(ARCHIVO_zip, path + "\\" + ARCHIVO_zip);
                File.Delete(ARCHIVO_zip);
                //Fin de Compresion de Archivo

                //Generar el archivo de sellado
                //frmSellado ofrmSellado = new frmSellado(ROW_ID_CATALOGO
                //    , ocEMPRESA, "CT", oCatalogo);
                //ofrmSellado.ShowDialog();

                if (mostrar_mensaje_finalizacion)
                {
                    MessageBoxEx.Show("Exportación de los archivos finalizada.", Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                return true;
            }
            catch (Exception e)
            {
                MessageBoxEx.Show(e.Message.ToString(), Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
            return false;


        }

        private void crear_archivo(string nombre, Catalogo oCatalogo, bool prefijos=false)
        {
            //Transformar al XML                
            XmlSerializerNamespaces myNamespaces = new XmlSerializerNamespaces();

            //<catalogocuentas:Catalogo
            //xsi:schemaLocation="http://www.sat.gob.mx/esquemas/ContabilidadE/1_1/CatalogoCuentas/CatalogoCuentas_1_1.xsd "
            //xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
            //xmlns: catalogocuentas=" http://www.sat.gob.mx/esquemas/ContabilidadE/1_1/CatalogoCuentas">

            //myNamespaces.Add("schemaLocation", "http://www.sat.gob.mx/esquemas/ContabilidadE/1_1/CatalogoCuentas/CatalogoCuentas_1_1.xsd");
            //myNamespaces.Add("xsi", "http://www.w3.org/2001/XMLSchema-instance");
            //myNamespaces.Add("catalogocuentas", "http://www.sat.gob.mx/esquemas/ContabilidadE/1_1/CatalogoCuentas");
            //myNamespaces.Add("cambiar", "www.sat.gob.mx/esquemas/ContabilidadE/1_1/CatalogoCuentas http://www.sat.gob.mx/esquemas/ContabilidadE/1_1/CatalogoCuentas/CatalogoCuentas_1_1.xsd");

            //xsi:schemaLocation='www.sat.gob.mx/esquemas/ContabilidadE/1_1/CatalogoCuentas http://www.sat.gob.mx/esquemas/ContabilidadE/1_1/CatalogoCuentas/CatalogoCuentas_1_1.xsd'

            myNamespaces.Add("schemaLocation", "http://www.sat.gob.mx/esquemas/ContabilidadE/1_3/CatalogoCuentas http://www.sat.gob.mx/esquemas/ContabilidadE/1_3/CatalogoCuentas/CatalogoCuentas_1_3.xsd");
            myNamespaces.Add("xsi", "http://www.w3.org/2001/XMLSchema-instance");
            myNamespaces.Add("catalogocuentas", "http://www.sat.gob.mx/esquemas/ContabilidadE/1_3/CatalogoCuentas");
            //myNamespaces.Add("cambiar", "www.sat.gob.mx/esquemas/ContabilidadE/1_3/CatalogoCuentas http://www.sat.gob.mx/esquemas/ContabilidadE/1_3/CatalogoCuentas/CatalogoCuentas_1_3.xsd");
            
            TextWriter writer = new StreamWriter(nombre + ".XML");
            XmlSerializer serializer = new XmlSerializer(typeof(Catalogo));

            serializer.Serialize(writer, oCatalogo, myNamespaces);
            writer.Close();
            if (prefijos)
            {
                oData.ReplaceInFile(nombre + ".XML", "xmlns:schemaLocation", "xsi:schemaLocation");
                //oData.ReplaceInFile(nombre + ".XML", "xmlns:cambiar", "xsi:schemaLocation");
                oData.ReplaceInFile(nombre + ".XML", "<Catalogo", "<catalogocuentas:Catalogo");
                oData.ReplaceInFile(nombre + ".XML", "</Catalogo", "</catalogocuentas:Catalogo");
                oData.ReplaceInFile(nombre + ".XML", "<Ctas", "<catalogocuentas:Ctas");

            }
        }

        private string sellar(string archivo, cEMPRESA_CE oEMPRESAp)
        {
            string sDirectory = AppDomain.CurrentDomain.BaseDirectory;
            XPathDocument myXPathDoc = new XPathDocument(archivo);
            //XslTransform myXslTrans = new XslTransform();
            XslCompiledTransform myXslTrans = new XslCompiledTransform();


            XslCompiledTransform trans = new XslCompiledTransform();
            XmlUrlResolver resolver = new XmlUrlResolver();

            resolver.Credentials = System.Net.CredentialCache.DefaultCredentials;
            XsltSettings settings = new XsltSettings(true, true);

            //Cargar xslt
            try
            {

                myXslTrans.Load(sDirectory + "/xslt/CatalogoCuentas_1_2.xslt");
            }
            catch (XmlException errores)
            {
                MessageBoxEx.Show("Error en " + errores.InnerException.ToString());
                return "";
            }
            //Crear Archivo Temporal
            string nombre_tmp = "TMP" + DateTime.Now.ToString("ddMMyyyyhhmmss");
            XmlTextWriter myWriter = new XmlTextWriter(nombre_tmp + ".TXT", null);

            //Transformar al XML
            myXslTrans.Transform(myXPathDoc, null, myWriter);

            myWriter.Close();

            //Abrir Archivo TXT
            string cadena_original = "";
            StreamReader re = File.OpenText(nombre_tmp + ".TXT");
            string input = null;
            while ((input = re.ReadLine()) != null)
            {
                cadena_original += input;
            }
            re.Close();
            //Buscar el &amp; en la Cadena y sustituirlo por &
            cadena_original = cadena_original.Replace("&amp;", "&");

            //Eliminar Archivos Temporales
            File.Delete(nombre_tmp + ".TXT");
            File.Delete(nombre_tmp + ".XML");


            StringBuilder sbPassword;
            StringBuilder sbPrivateKey;
            byte[] b;
            byte[] block;
            int keyBytes;
            sbPassword = new StringBuilder(oEMPRESAp.PASSWORD);
            sbPrivateKey = Rsa.ReadEncPrivateKey(oEMPRESAp.LLAVE, sbPassword.ToString());
            keyBytes = Rsa.KeyBytes(sbPrivateKey.ToString());
            b = System.Text.Encoding.UTF8.GetBytes(cadena_original);
            block = Rsa.EncodeMsgForSignature(keyBytes, b, HashAlgorithm.Sha256);
            block = Rsa.RawPrivate(block, sbPrivateKey.ToString());
            string resultado = System.Convert.ToBase64String(block);
            Wipe.String(sbPassword);
            Wipe.String(sbPrivateKey);
            Wipe.Data(block);

            return resultado;
        }


        public string validar()
        {
            if(codAgrupField.Trim()=="")
            {
                return "C1-Agregar el código agrupador.";
            }
            if(codAgrupField.Trim()!="")
            {
                //Validar codigo agrupador
                if(!this.validar_codigo_agrupador(codAgrupField))
                {
                    return "C2-El código agrupador " + codAgrupField+ " para la cuenta " + numCtaField + " no es correcto.";
                }
                
            }

            if (descField.Trim() == "")
            {
                return "C3-Agregar la descripción de la cuenta " + numCtaField + " ";
            }
            if (naturField.Trim() == "")
            {
                return "C4-Agregar la naturaleza de la cuenta " + numCtaField + " ";
            }

            return "";
        }
        public DataTable obtener_agrupador()
        {
            string Archivo = Path.GetDirectoryName(Application.ExecutablePath) + @"\configuracion\Configuracion.xlsx";
            string ArchivoTmp = System.IO.Path.GetTempPath() + Guid.NewGuid().ToString() + ".xlsx";

            try
            {
                DataTable dt = new DataTable();
                dt.Columns.Add(new DataColumn("ID"));
                dt.Columns.Add(new DataColumn("VALUE"));

                if (!File.Exists(Archivo))
                {
                    MessageBoxEx.Show("Falta el archivo de configuración " + Archivo, Application.ProductName + "-" + Application.ProductVersion, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                    return null;
                }
                if (File.Exists(ArchivoTmp))
                {
                    File.Delete(ArchivoTmp);
                }
                File.Copy(Archivo, ArchivoTmp);

                FileInfo existingFile = new FileInfo(ArchivoTmp);
                ExcelPackage package = new ExcelPackage(existingFile);
                ExcelWorksheet worksheet = package.Workbook.Worksheets[1];
                int rowIndex = 2;

                while (worksheet.Cells[rowIndex, 1].Value != null)
                {
                    ////DevComponents.DotNetBar.Item oItems=new Items();
                    //DevComponents.DotNetBar.ComboBoxItem oComboBoxItem = new ComboBoxItem();

                    //string valor = worksheet.Cells[rowIndex, 1].Text;
                    //string descripcion = worksheet.Cells[rowIndex, 2].Text;
                    //oComboBoxItem.Items.Add(valor);
                    //oComboBoxItem.Tag = valor;
                    //oComboBoxItem.Text = valor + " - " + descripcion;
                    //codAgrupField.Items.Add(oComboBoxItem);
                    DataRow registro = dt.NewRow();
                    registro["ID"] = worksheet.Cells[rowIndex, 1].Text + " - " + worksheet.Cells[rowIndex, 2].Text;
                    registro["VALUE"] = worksheet.Cells[rowIndex, 1].Text;
                    dt.Rows.Add(registro);

                    rowIndex++;
                }
                File.Delete(ArchivoTmp);
                return dt;
            }
            catch (Exception error)
            {
                string Mensaje = "cTalogoCtas - 432 + Archivo Origen: " + Archivo
                    + " Archivo Destino: " + ArchivoTmp;
                Mensaje += error.Message.ToString();
                if (error.InnerException != null)
                {
                    Mensaje += " " + error.InnerException.Message.ToString();
                }
                MessageBoxEx.Show(Mensaje, Application.ProductName + "-" + Application.ProductVersion, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return null;
            }

        }
        public string todos_sSQL(string ROW_ID_CATALOGOp)
        {
            string resultado = "";
            sSQL = " SELECT numCtaField ";
            sSQL += " FROM CatalogoCtas ";
            sSQL += " WHERE 1=1 AND ROW_ID_CATALOGO=" + ROW_ID_CATALOGOp + "";
            sSQL += " ORDER BY numCtaField";
            DataTable oDataTable = oData.EjecutarConsulta(sSQL);
            foreach (DataRow oDataRow in oDataTable.Rows)
            {
                if(resultado!="")
                {
                    resultado+=",";
                }
                resultado += "'" + oDataRow["numCtaField"].ToString() + "'";
                
            }
            return resultado;
        }
        public List<cCatalogoCtas> todos(string ROW_ID_CATALOGOp,string numCtaFieldp = "")
        {

            List<cCatalogoCtas> lista = new List<cCatalogoCtas>();

            sSQL = " SELECT * ";
            sSQL += " FROM CatalogoCtas ";
            sSQL += " WHERE 1=1 AND ROW_ID_CATALOGO=" + ROW_ID_CATALOGOp  + "";
            sSQL += " AND numCtaField LIKE '%" + numCtaFieldp + "%' ";
            sSQL += " ORDER BY numCtaField";
            DataTable oDataTable = oData.EjecutarConsulta(sSQL);
            foreach (DataRow oDataRow in oDataTable.Rows)
            {
                cCatalogoCtas ocCatalogo = new cCatalogoCtas();
                ocCatalogo.cargar(oDataRow["ROW_ID"].ToString());
                lista.Add(ocCatalogo);
            }
            return lista;

        }

        public bool verificar(string numCtaFieldp, string ROW_ID_CATALOGOp)
        {
            sSQL = " SELECT * ";
            sSQL += " FROM CatalogoCtas ";
            sSQL += " WHERE numCtaField='" + numCtaFieldp + "' ";
            sSQL += " AND ROW_ID_CATALOGO=" + ROW_ID_CATALOGOp + "";
            DataTable oDataTable = oData.EjecutarConsulta(sSQL);
            if (oDataTable != null)
            {
                foreach (DataRow oDataRow in oDataTable.Rows)
                {                    
                    return true;
                }
            }
            return false;
        }

        public bool cargar(string ROW_IDp)
        {
            sSQL = " SELECT * ";
            sSQL += " FROM CatalogoCtas ";
            sSQL += " WHERE ROW_ID=" + ROW_IDp + " ";
            DataTable oDataTable = oData.EjecutarConsulta(sSQL);
            if (oDataTable != null)
            {
                foreach (DataRow oDataRow in oDataTable.Rows)
                {
                    //Añadir el Registro
                    ROW_ID = oDataRow["ROW_ID"].ToString();
                    ROW_ID_CATALOGO = oDataRow["ROW_ID_CATALOGO"].ToString();
                    codAgrupField = oDataRow["codAgrupField"].ToString();
                    //Verificar .1 o 10
                    //


                    numCtaField = oDataRow["numCtaField"].ToString();
                    descField = oDataRow["descField"].ToString();
                    subCtaDeField = oDataRow["subCtaDeField"].ToString();
                    nivelField = oDataRow["nivelField"].ToString();
                    naturField = oDataRow["naturField"].ToString();
                    return true;
                }
            }
            return false;
        }

        public bool guardar(bool validar=true)
        {
            try
            {
                if (validar)
                {
                    if (codAgrupField.Trim() == "")
                    {
                        MessageBoxEx.Show("El grupo es requerido.", Application.ProductName + "-" + Application.ProductVersion, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                        return false;
                    }

                    if (numCtaField.Trim() == "")
                    {
                        MessageBoxEx.Show("La cuenta es requerida.", Application.ProductName + "-" + Application.ProductVersion, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                        return false;
                    }
                    if (nivelField.Trim() == "")
                    {
                        MessageBoxEx.Show("Nivel es requerido.", Application.ProductName + "-" + Application.ProductVersion, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                        return false;
                    }
                    if (naturField.Trim() == "")
                    {
                        MessageBoxEx.Show("La Naturaleza es requerida.", Application.ProductName + "-" + Application.ProductVersion, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                        return false;
                    }
                    if (numCtaField.Trim() == subCtaDeField.Trim())
                    {
                        MessageBoxEx.Show("La cuenta no puede ser subordinada asi misma.", Application.ProductName + "-" + Application.ProductVersion, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                        return false;
                    }
                    if (subCtaDeField != "")
                    {
                        if (!verificar(subCtaDeField, ROW_ID_CATALOGO))
                        {
                            //MessageBoxEx.Show("La cuenta subordinada " + subCtaDeField + "  no existe.", Application.ProductName + "-" + Application.ProductVersion, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                            //return false;
                            //subCtaDeField = "";
                        }
                    }
                }
                descField = descField.Replace("'", "");

                if (codAgrupField != "")
                {
                    string mensaje = this.validar();
                    if (mensaje != "")
                    {
                        MessageBoxEx.Show(mensaje);
                        return false;
                    }
                }

                if (ROW_ID == "")
                {

                    if (verificar(numCtaField, ROW_ID_CATALOGO))
                    {
                        if (validar)
                        {
                            MessageBoxEx.Show("La cuenta " + numCtaField + " ya existe.", Application.ProductName + "-" + Application.ProductVersion, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                        }
                        return false;
                    }
                    //Buscar el codAgrupField si esta en blanco
                    if (codAgrupField == "")
                    {
                        cCatalogoCtas ocCatalogoCtas = new cCatalogoCtas();

                        cCatalogo ocCatalogo = new cCatalogo();
                        ocCatalogo.cargar(ROW_ID_CATALOGO);


                        if (ocCatalogoCtas.cargar_numCta(numCtaField, true, ocCatalogo))
                        {
                            if (ocCatalogoCtas.codAgrupField != "")
                            {
                                codAgrupField = ocCatalogoCtas.codAgrupField;
                            }

                        }

                    }

                    sSQL = " INSERT INTO CatalogoCtas ";
                    sSQL += " ( ";
                    sSQL += " [ROW_ID_CATALOGO],[codAgrupField],[numCtaField],[descField]";
                    sSQL += ",[subCtaDeField],[nivelField],[naturField] ";
                    sSQL += " )";
                    sSQL += " VALUES ";
                    sSQL += "(";
                    sSQL += " " + ROW_ID_CATALOGO + ",'" + codAgrupField + "','" + numCtaField + "','" + descField + "'";
                    sSQL += ",'" + subCtaDeField + "','" + nivelField + "','" + naturField + "' ";
                    sSQL += ")";
                    DataTable oDataTable = oData.EjecutarConsulta(sSQL);
                    if (oDataTable != null)
                    {

                        sSQL = "SELECT TOP 1 MAX(ROW_ID) as ROW_ID FROM CatalogoCtas";
                        oDataTable = oData.EjecutarConsulta(sSQL);
                        if (oDataTable != null)
                        {
                            foreach (DataRow oDataRow in oDataTable.Rows)
                            {
                                ROW_ID = oDataRow["ROW_ID"].ToString();
                            }
                        }
                    }

                }
                else
                {
                    sSQL = " UPDATE CatalogoCtas ";
                    sSQL += " SET ROW_ID_CATALOGO=" + ROW_ID_CATALOGO + ",codAgrupField='" + codAgrupField + "',numCtaField='" + numCtaField + "',descField='" + descField + "'";
                    sSQL += ",subCtaDeField='" + subCtaDeField + "',nivelField='" + nivelField + "',naturField='" + naturField + "' ";
                    sSQL += " WHERE ROW_ID=" + ROW_ID;
                    oData.EjecutarConsulta(sSQL);

                }
                return true;

            }
            catch (Exception error)
            {
                string mensaje = "";
                mensaje = error.Message.ToString();
                if (error.InnerException != null)
                {
                    mensaje += " " + error.InnerException.Message.ToString();
                }

                mensaje += "cCatalogoCtas - 640 ";

                MessageBoxEx.Show(mensaje);
                return false;
            }

        }


        public bool eliminar(string ROW_IDp)
        {
            if (ROW_IDp != "")
            {
                sSQL = "DELETE FROM CatalogoCtas ";
                sSQL += " WHERE ROW_ID='" + ROW_IDp + "' ";
                oData.EjecutarConsulta(sSQL);
                return true;
            }
            return false;
        }

        internal bool cargar_numCta(string numCtaFieldp, bool solo_codAgrupField = false, cCatalogo ocCatalogop=null)
        {
            
            sSQL = " SELECT * ";
            sSQL += " FROM CatalogoCtas ";
            sSQL += " WHERE numCtaField='" + numCtaFieldp + "'";
            if (solo_codAgrupField)
            {
                sSQL += " and codAgrupField<>''";
            }
            //Filtrar por la Empresa
            if (ocCatalogop!=null)
            {
                sSQL += " and ISNULL((SELECT TOP 1 C.rFCField FROM Catalogo C WHERE C.ROW_ID=ROW_ID_CATALOGO),'')='" + ocCatalogop.rFCField + "'";
            }

            sSQL += " ORDER BY ROW_ID DESC";
            DataTable oDataTable = oData.EjecutarConsulta(sSQL);
            if (oDataTable != null)
            {
                foreach (DataRow oDataRow in oDataTable.Rows)
                {
                    //Añadir el Registro
                    return cargar(oDataRow["ROW_ID"].ToString());
                }
            }
            return false;
        }

        internal bool existe(string numCtaFieldp, string ROW_ID_CATALOGOp)
        {
            sSQL = " SELECT * ";
            sSQL += " FROM CatalogoCtas ";
            sSQL += " WHERE numCtaField='" + numCtaFieldp + "' AND ROW_ID_CATALOGO=" + ROW_ID_CATALOGOp + " ";
            DataTable oDataTable = oData.EjecutarConsulta(sSQL);
            if (oDataTable != null)
            {
                foreach (DataRow oDataRow in oDataTable.Rows)
                {
                    //Añadir el Registro
                    codAgrupField = oDataRow["codAgrupField"].ToString();
                    return true;
                }
            }
            return false;
        }

        public bool obtener_VISUAL_ACCOUNT(string numCtaFieldp, cEMPRESA_CE ocEMPRESA)
        {

            cEMPRESA_CONEXION ocEMPRESA_CONEXION = new cEMPRESA_CONEXION();

            foreach (cEMPRESA_CONEXION registro in ocEMPRESA_CONEXION.todos(ocEMPRESA.ROW_ID))
            {
                cCONEXCION oData_ERP = registro.conectar(registro.ROW_ID);
                oData_ERP = registro.conectar(registro.ROW_ID);
                string sSQL = "";
                sSQL = "SELECT * FROM ACCOUNT WHERE [ACTIVE_FLAG]='Y' AND ID='" + numCtaFieldp + "'";
                DataTable oDataTable = oData_ERP.EjecutarConsulta(sSQL);
                if (oDataTable != null)
                {
                    foreach (DataRow oDataRow in oDataTable.Rows)
                    {
                        numCtaField = oDataRow["ID"].ToString().Trim();
                        descField = oDataRow["DESCRIPTION"].ToString();
                        subCtaDeField = oDataRow["PARENT_ACCT_ID"].ToString();
                        nivelField = buscar_nivel(oDataRow["ID"].ToString().Trim(),oData_ERP).ToString();
                        //Definir el tipo de cuenta
                        //E
                        //R
                        //A
                        //L
                        //Q
                        switch (oDataRow["TYPE"].ToString())
                        {

                            case "Q":
                            case "R":
                            case "L":
                                naturField = "A";
                                break;
                            case "E":
                            case "A":
                                naturField = "D";
                                break;
                        }
                        return true;
                    }
                }

            }
            return false;
        }

        internal int buscar_nivel(string p, cCONEXCION oData_ERP)
        {
            int resultado = 1;
            try
            {
                string sSQL = "";
                
                sSQL = "SELECT TOP 1 * FROM ACCOUNT WHERE ID='" + p + "'";
                DataTable oDataTable = oData_ERP.EjecutarConsulta(sSQL);
                foreach (DataRow oDataRow in oDataTable.Rows)
                {
                    if (oDataRow["PARENT_ACCT_ID"].ToString() != "")
                    {
                        resultado += buscar_nivel(oDataRow["PARENT_ACCT_ID"].ToString(), oData_ERP);
                    }

                }
            }
            catch (Exception excep)
            {
                MessageBoxEx.Show("frmDeclaracion - 1153 Error en la cuenta nivel" + p + " " + excep.ToString());
                return -1;
            }
            
            return resultado;
        }


        internal void eliminar_sin_agrupador()
        {

            sSQL = "DELETE FROM CatalogoCtas ";
            sSQL += " WHERE codAgrupField='' ";
            oData.EjecutarConsulta(sSQL);

        }

        internal void eliminar_sin_banlanza(string ROW_ID_CATALOGOp)
        {
            sSQL = " DELETE FROM CatalogoCtas ";
            sSQL += " WHERE [ROW_ID_CATALOGO]='" + ROW_ID_CATALOGOp + "' ";
            sSQL += " AND NOT([numCtaField] IN (SELECT [numCtaField] ";
            sSQL += " FROM [Balanza] WHERE [ROW_ID_CATALOGO]='" + ROW_ID_CATALOGOp + "')) ";
            oData.EjecutarConsulta(sSQL);
        }
    }
}
