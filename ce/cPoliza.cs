﻿using CryptoSysPKI;
using DevComponents.DotNetBar;
using Generales;
using Ionic.Zip;
using ModeloDocumentosFX;
using OfficeOpenXml;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Windows.Forms;
using System.Xml;
using System.Xml.Serialization;
using System.Xml.XPath;
using System.Xml.Xsl;

namespace Desarrollo
{
    class cPoliza
    {
        private string sSQL = "";
        public string ROW_ID { get; set; }

        public string ROW_ID_CATALOGO { get; set; }

        public string tipoField { get; set; }

        public string TYPE { get; set; }

        public string numField { get; set; }
        public DateTime fechaField { get; set; }
        public string conceptoField { get; set; }
        public string error { get; set; }

        public int comprobantes { get; set; }
        private cCONEXCION oData = new cCONEXCION();
        public decimal DEBE { get; set; }
        public decimal HABER { get; set; }

        public override string ToString() { return ROW_ID; }

        public cPoliza()
        {
            ModeloDocumentosFXContainer dbContext = new ModeloDocumentosFXContainer();
            try
            {
                string conection = dbContext.Database.Connection.ConnectionString;
                oData = new cCONEXCION(conection);
            }
            catch (Exception err)
            {
                ErrorFX.mostrar(err, false, true, "frmEmpresa - 96 - ", false);
            }
            limpiar();
        }

        public void limpiar()
        {
            ROW_ID = "";
            ROW_ID_CATALOGO = "";
            tipoField="";
            numField = "";
            conceptoField = "";
            comprobantes = 0;
            TYPE = "";
        }
        public bool generar_archivo(cCatalogo oCatalogop, string pathp, bool mostrar_mensaje_finalizacion = true, cPolizas ocPolizasp=null)
        {
            try
            {
                //Validar polizas vs trnascciones
                cPoliza ocPolizat = new cPoliza();
                List<cPoliza> lista=ocPolizat.todos(oCatalogop.ROW_ID, "", "", false, true);




                FolderBrowserDialog folderBrowserDialog1 = new FolderBrowserDialog();

                string path = pathp;
                if (path.Trim() == "")
                {
                    if (folderBrowserDialog1.ShowDialog() == DialogResult.OK)
                    {
                        path = folderBrowserDialog1.SelectedPath;

                    }
                    else
                    {
                        MessageBoxEx.Show("Generación de archivo cancelada.", Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
                        return false;
                    }
                }


                string mes = oCatalogop.mesField;
                if (mes.Length == 1)
                {
                    mes = "0" + mes;
                }

                string nombre = oCatalogop.rFCField + oCatalogop.anoField + mes + "PL";

                string nombre_tmp = path + "\\" + "" + nombre + ".xml";

                //Generar Poliza
                Polizas oPolizas = new Polizas();
                cPoliza ocPoliza = new cPoliza();
                oPolizas.Anio = int.Parse(oCatalogop.anoField);

                switch (int.Parse(oCatalogop.mesField))
                {
                    case 1:
                        oPolizas.Mes = PolizasMes.Item01;
                        break;
                    case 2:
                        oPolizas.Mes = PolizasMes.Item02;
                        break;
                    case 3:
                        oPolizas.Mes = PolizasMes.Item03;
                        break;
                    case 4:
                        oPolizas.Mes = PolizasMes.Item04;
                        break;
                    case 5:
                        oPolizas.Mes = PolizasMes.Item05;
                        break;
                    case 6:
                        oPolizas.Mes = PolizasMes.Item06;
                        break;
                    case 7:
                        oPolizas.Mes = PolizasMes.Item07;
                        break;
                    case 8:
                        oPolizas.Mes = PolizasMes.Item08;
                        break;
                    case 9:
                        oPolizas.Mes = PolizasMes.Item09;
                        break;
                    case 10:
                        oPolizas.Mes = PolizasMes.Item10;
                        break;
                    case 11:
                        oPolizas.Mes = PolizasMes.Item11;
                        break;
                    case 12:
                        oPolizas.Mes = PolizasMes.Item12;
                        break;
                }
                //Cargar el tipo de Poliza
                if(ocPolizasp !=null)
                {
                    oPolizas.TipoSolicitud = ocPolizasp.tipoSolicitudField;
                    if(ocPolizasp.numOrdenField!="")
                    {
                        oPolizas.NumOrden=ocPolizasp.numOrdenField;
                    }
                    if (ocPolizasp.numTramiteField != "")
                    {
                        oPolizas.NumTramite=ocPolizasp.numTramiteField;
                    }
                }
                


                oPolizas.RFC = oCatalogop.rFCField;
                cEMPRESA_CE ocEMPRESA = new cEMPRESA_CE();
                ocEMPRESA.cargar_rfc(oPolizas.RFC);
                if (ocEMPRESA.NO_CERTIFICADO.Trim().Length == 0)
                {
                    MessageBoxEx.Show("No se tiene configurado el Certificado"
                       , Application.ProductName + "-" + Application.ProductVersion, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);

                    return false;
                }
                oPolizas.noCertificado = ocEMPRESA.NO_CERTIFICADO;
                if (!File.Exists(ocEMPRESA.CERTIFICADO))
                {
                    MessageBoxEx.Show("No se tiene acceso al Certificado, archivo " + ocEMPRESA.CERTIFICADO + ""
                        , Application.ProductName + "-" + Application.ProductVersion, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);

                    return false;
                }
                X509Certificate cert = X509Certificate.CreateFromCertFile(ocEMPRESA.CERTIFICADO);
                string Certificado64 = System.Convert.ToBase64String(cert.GetRawCertData());
                oPolizas.Certificado = Certificado64;

                List<cPoliza> listacPolizas = new List<cPoliza>();
                listacPolizas = ocPoliza.todos(oCatalogop.ROW_ID, "","",true);
                PolizasPoliza[] oPolizas_p;
                oPolizas_p = new PolizasPoliza[listacPolizas.Count];
                int contador_p = 0;
                foreach (cPoliza registro in listacPolizas)
                {
                    oPolizas_p[contador_p] = new PolizasPoliza();

                    //oPolizas_p[contador_p].Tipo = int.Parse(registro.tipoField);
                    //oPolizas_p[contador_p].Num = registro.numField;
                    oPolizas_p[contador_p].NumUnIdenPol = registro.numField;

                    oPolizas_p[contador_p].Concepto = registro.conceptoField;
                    oPolizas_p[contador_p].Fecha = registro.fechaField;
                    //Buscar las transacciones 
                    cTransaccion ocTransaccion = new cTransaccion();
                    List<cTransaccion> lista_ocTransaccion = ocTransaccion.todos(registro.ROW_ID);
                    PolizasPolizaTransaccion[] PolizasPolizaTransaccion_p;
                    PolizasPolizaTransaccion_p = new PolizasPolizaTransaccion[lista_ocTransaccion.Count];
                    int contador_t = 0;
                    oPolizas_p[contador_p].Transaccion = new PolizasPolizaTransaccion[PolizasPolizaTransaccion_p.Length];
                    foreach (cTransaccion registro_t in lista_ocTransaccion)
                    {
                        PolizasPolizaTransaccion_p[contador_t] = new PolizasPolizaTransaccion();
                        //PolizasPolizaTransaccion_p[contador_t].numCtaField = registro_t.numCtaField;
                        //PolizasPolizaTransaccion_p[contador_t].debeField = decimal.Parse(registro_t.debeField.ToString());
                        //PolizasPolizaTransaccion_p[contador_t].haberField = decimal.Parse(registro_t.haberField.ToString());
                        //PolizasPolizaTransaccion_p[contador_t].monedaField = registro_t.monedaField;

                        PolizasPolizaTransaccion_p[contador_t].NumCta = registro_t.numCtaField;
                        cCatalogoCtas ocCatalogoCtas=new cCatalogoCtas();
                        ocCatalogoCtas.cargar_numCta(registro_t.numCtaField);
                        PolizasPolizaTransaccion_p[contador_t].DesCta = ocCatalogoCtas.descField;
                        if (PolizasPolizaTransaccion_p[contador_t].DesCta=="")
                        {
                            PolizasPolizaTransaccion_p[contador_t].DesCta = registro_t.numCtaField;
                        }


                        PolizasPolizaTransaccion_p[contador_t].Concepto = registro_t.conceptoField;
                        PolizasPolizaTransaccion_p[contador_t].Debe = decimal.Parse(registro_t.debeField.ToString());
                        PolizasPolizaTransaccion_p[contador_t].Haber = decimal.Parse(registro_t.haberField.ToString());

                        //Agregar Cheques
                        cCheques ocCheques = new cCheques();
                        List<cCheques> lista_cheques = ocCheques.todos(registro_t.ROW_ID);
                        PolizasPolizaTransaccionCheque[] PolizasPolizaTransaccionCheque_p;
                        PolizasPolizaTransaccionCheque_p = new PolizasPolizaTransaccionCheque[lista_cheques.Count];
                        int contador_ch = 0;
                        if (lista_cheques.Count > 0)
                        {
                            PolizasPolizaTransaccion_p[contador_t].Cheque = new PolizasPolizaTransaccionCheque[lista_cheques.Count];

                            foreach (cCheques registro_ch in lista_cheques)
                            {
                                PolizasPolizaTransaccion_p[contador_t].Cheque[contador_ch] = new PolizasPolizaTransaccionCheque();
                                PolizasPolizaTransaccionCheque_p[contador_ch] = new PolizasPolizaTransaccionCheque();
                                PolizasPolizaTransaccionCheque_p[contador_ch].Num = registro_ch.numCtaField;
                                PolizasPolizaTransaccionCheque_p[contador_ch].Fecha = registro_ch.fechaField;
                                PolizasPolizaTransaccionCheque_p[contador_ch].Monto = decimal.Parse(registro_ch.montoField.ToString());

                                PolizasPolizaTransaccionCheque_p[contador_ch].BanEmisNal = registro_ch.bancoField;

                                PolizasPolizaTransaccionCheque_p[contador_ch].CtaOri = registro_ch.ctaOriField;
                                PolizasPolizaTransaccionCheque_p[contador_ch].Benef = registro_ch.benefField;
                                PolizasPolizaTransaccionCheque_p[contador_ch].RFC = registro_ch.rFCField;
                                PolizasPolizaTransaccion_p[contador_t].Cheque[contador_ch] = PolizasPolizaTransaccionCheque_p[contador_ch];
                                contador_ch++;
                            }
                            PolizasPolizaTransaccion_p[contador_t].Cheque = PolizasPolizaTransaccionCheque_p;
                        }
                        //Agregar Transferencias
                        cTransferencia ocTransferencia = new cTransferencia();
                        List<cTransferencia> lista_transferencias = ocTransferencia.todos(registro_t.ROW_ID);
                        PolizasPolizaTransaccionTransferencia[] PolizasPolizaTransaccionTransferencia_p;
                        PolizasPolizaTransaccionTransferencia_p = new PolizasPolizaTransaccionTransferencia[lista_transferencias.Count];
                        int contador_tr = 0;
                        if (lista_transferencias.Count > 0)
                        {
                            PolizasPolizaTransaccion_p[contador_t].Transferencia = new PolizasPolizaTransaccionTransferencia[lista_transferencias.Count];

                            foreach (cTransferencia registro_tr in lista_transferencias)
                            {
                                PolizasPolizaTransaccionTransferencia_p[contador_tr] = new PolizasPolizaTransaccionTransferencia();
                                PolizasPolizaTransaccionTransferencia_p[contador_tr].CtaDest = registro_tr.ctaDestField;
                                PolizasPolizaTransaccionTransferencia_p[contador_tr].CtaOri = registro_tr.ctaOriField;
                                PolizasPolizaTransaccionTransferencia_p[contador_tr].BancoDestNal = registro_tr.bancoDestField;
                                PolizasPolizaTransaccionTransferencia_p[contador_tr].BancoOriNal = registro_tr.bancoOriField;
                                PolizasPolizaTransaccionTransferencia_p[contador_tr].Fecha = registro_tr.fechaField;
                                PolizasPolizaTransaccionTransferencia_p[contador_tr].Monto = decimal.Parse(registro_tr.montoField.ToString());
                                PolizasPolizaTransaccionTransferencia_p[contador_tr].Benef = registro_tr.benefField;
                                PolizasPolizaTransaccionTransferencia_p[contador_tr].RFC = registro_tr.rFCField;
                                PolizasPolizaTransaccion_p[contador_t].Transferencia[contador_tr] = PolizasPolizaTransaccionTransferencia_p[contador_tr];
                                contador_tr++;
                            }
                            PolizasPolizaTransaccion_p[contador_t].Transferencia = PolizasPolizaTransaccionTransferencia_p;

                        }
                        //Agregar Comprobantes

                        //PolizasPolizaTransaccionCompNalOtr a = new PolizasPolizaTransaccionCompNalOtr();

                        cPolizasPolizaTransaccionCompNal ocComprobantes = new cPolizasPolizaTransaccionCompNal();
                        List<cPolizasPolizaTransaccionCompNal> lista_comprobantes = ocComprobantes.todos(registro_t.ROW_ID);
                        int contador_co = 0;
                        if (lista_comprobantes.Count > 0)
                        {
                            PolizasPolizaTransaccion_p[contador_t].CompNal = new PolizasPolizaTransaccionCompNal[lista_comprobantes.Count];

                            foreach (cPolizasPolizaTransaccionCompNal registro_tr in lista_comprobantes)
                            {

                                PolizasPolizaTransaccion_p[contador_t].CompNal[contador_co] = new PolizasPolizaTransaccionCompNal();
                                PolizasPolizaTransaccion_p[contador_t].CompNal[contador_co].UUID_CFDI = registro_tr.uUID_CFDIField;
                                PolizasPolizaTransaccion_p[contador_t].CompNal[contador_co].RFC = registro_tr.rFCField;
                                PolizasPolizaTransaccion_p[contador_t].CompNal[contador_co].MontoTotal = registro_tr.montoTotalField;
                                PolizasPolizaTransaccion_p[contador_t].CompNal[contador_co].Moneda = validar_moneda(registro_tr.monedaField,ocEMPRESA.ROW_ID);                                
                                PolizasPolizaTransaccion_p[contador_t].CompNal[contador_co].TipCamb = registro_tr.tipCambField;
                                PolizasPolizaTransaccion_p[contador_t].CompNal[contador_co].TipCambSpecified = registro_tr.tipCambFieldSpecified;
                                contador_tr++;
                            }
                            PolizasPolizaTransaccion_p[contador_t].Transferencia = PolizasPolizaTransaccionTransferencia_p;

                        }


                        cPolizasPolizaTransaccionCompExt ocPolizasPolizaTransaccionCompExt = new cPolizasPolizaTransaccionCompExt();
                        List<cPolizasPolizaTransaccionCompExt> lista_cPolizasPolizaTransaccionCompExt = ocPolizasPolizaTransaccionCompExt.todos(registro_t.ROW_ID);

                        PolizasPolizaTransaccionCompExt[] oPolizasPolizaTransaccionCompExt;
                        oPolizasPolizaTransaccionCompExt = new PolizasPolizaTransaccionCompExt[lista_comprobantes.Count];
                        contador_co = 0;
                        if (lista_cPolizasPolizaTransaccionCompExt.Count > 0)
                        {
                            PolizasPolizaTransaccion_p[contador_t].CompExt = new PolizasPolizaTransaccionCompExt[lista_cPolizasPolizaTransaccionCompExt.Count];

                            foreach (cPolizasPolizaTransaccionCompExt registro_tr in lista_cPolizasPolizaTransaccionCompExt)
                            {

                                oPolizasPolizaTransaccionCompExt[contador_co] = new PolizasPolizaTransaccionCompExt();
                                oPolizasPolizaTransaccionCompExt[contador_co].NumFactExt = registro_tr.numFactExtField;
                                oPolizasPolizaTransaccionCompExt[contador_co].TaxID = registro_tr.taxIDField;
                                oPolizasPolizaTransaccionCompExt[contador_co].MontoTotal = registro_tr.montoTotalField;
                                oPolizasPolizaTransaccionCompExt[contador_co].Moneda = validar_moneda(registro_tr.monedaField, ocEMPRESA.ROW_ID);
                                oPolizasPolizaTransaccionCompExt[contador_co].TipCamb = registro_tr.tipCambField;
                                oPolizasPolizaTransaccionCompExt[contador_co].TipCambSpecified = registro_tr.tipCambFieldSpecified;

                                PolizasPolizaTransaccion_p[contador_t].Transferencia[contador_tr] = PolizasPolizaTransaccionTransferencia_p[contador_co];
                                contador_tr++;
                            }
                            PolizasPolizaTransaccion_p[contador_t].Transferencia = PolizasPolizaTransaccionTransferencia_p;

                        }

                        PolizasPolizaTransaccionCompNalOtr[] oPolizasPolizaTransaccionCompNalOtr;

                        PolizasPolizaTransaccionOtrMetodoPago[] oPolizasPolizaTransaccionOtrMetodoPago;

                        oPolizas_p[contador_p].Transaccion[contador_t] = PolizasPolizaTransaccion_p[contador_t];


                        contador_t++;
                    }
                    contador_p++;
                }
                oPolizas.Poliza = oPolizas_p;
                crear_archivo(nombre, oPolizas);

                oPolizas.Sello = sellar(nombre + ".XML", ocEMPRESA);

                if (oPolizas.Sello == "")
                {
                    MessageBoxEx.Show("El sello tiene error de generación.", Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return false;
                }

                crear_archivo(nombre, oPolizas);

                //Comprimir
                string ARCHIVO_zip = nombre + ".ZIP";
                using (ZipFile zip = new ZipFile())
                {
                    zip.AddFile(nombre + ".XML");
                    zip.Save(ARCHIVO_zip);
                }
                File.Delete(nombre + ".XML");
                File.Delete(path + "\\" + ARCHIVO_zip);
                File.Copy(ARCHIVO_zip, path + "\\" + ARCHIVO_zip);
                File.Delete(ARCHIVO_zip);
                //Fin de Compresion de Archivo



                if (mostrar_mensaje_finalizacion)
                {
                    MessageBoxEx.Show("Exportación de los archivos finalizada.", Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Information);
                }

            }
            catch (Exception e)
            {
                MessageBoxEx.Show(e.Message.ToString(), Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
            return false;

        }

        private void crear_archivo(string nombre, Polizas oPolizas)
        {
            //Transformar al XML
            XmlSerializerNamespaces myNamespaces = new XmlSerializerNamespaces();
            myNamespaces.Add("schemaLocation", "http://www.sat.gob.mx/esquemas/ContabilidadE/1_1/PolizasPeriodo/PolizasPeriodo_1_1.xsd");
            myNamespaces.Add("xsi", "http://www.w3.org/2001/XMLSchema-instance");
            myNamespaces.Add("PLZ", "www.sat.gob.mx/esquemas/ContabilidadE/1_1/PolizasPeriodo");
            myNamespaces.Add("cambiar", "www.sat.gob.mx/esquemas/ContabilidadE/1_1/PolizasPeriodo http://www.sat.gob.mx/esquemas/ContabilidadE/1_1/PolizasPeriodo/PolizasPeriodo_1_1.xsd");

            //Transformar al XML
            TextWriter writer_p = new StreamWriter(nombre + ".XML");
            XmlSerializer serializer_p = new XmlSerializer(typeof(Polizas));
            serializer_p.Serialize(writer_p, oPolizas, myNamespaces);
            writer_p.Close();

            oData.ReplaceInFile(nombre + ".XML", "xmlns:cambiar", "xsi:schemaLocation");
        }

        private string validar_moneda(string monedap,string ROW_ID_EMPRESAp)
        {
            cEMPRESA_MONEDA ocEMPRESA_MONEDA = new cEMPRESA_MONEDA();
            return ocEMPRESA_MONEDA.cambiar_MONEDA(monedap, ROW_ID_EMPRESAp);
        }
        private string sellar(string archivo, cEMPRESA_CE oEMPRESAp)
        {
            string sDirectory = AppDomain.CurrentDomain.BaseDirectory;
            XPathDocument myXPathDoc = new XPathDocument(archivo);
            //XslTransform myXslTrans = new XslTransform();
            XslCompiledTransform myXslTrans = new XslCompiledTransform();


            XslCompiledTransform trans = new XslCompiledTransform();
            XmlUrlResolver resolver = new XmlUrlResolver();

            resolver.Credentials = System.Net.CredentialCache.DefaultCredentials;
            XsltSettings settings = new XsltSettings(true, true);

            //Cargar xslt
            try
            {

                myXslTrans.Load(sDirectory + "/xslt/PolizasPeriodo_1_1.xslt");
            }
            catch (XmlException errores)
            {
                MessageBoxEx.Show("Error en " + errores.InnerException.ToString());
                return "";
            }
            //Crear Archivo Temporal
            string nombre_tmp = "TMP" + DateTime.Now.ToString("ddMMyyyyhhmmss");
            XmlTextWriter myWriter = new XmlTextWriter(nombre_tmp + ".TXT", null);

            //Transformar al XML
            myXslTrans.Transform(myXPathDoc, null, myWriter);

            myWriter.Close();

            //Abrir Archivo TXT
            string cadena_original = "";
            StreamReader re = File.OpenText(nombre_tmp + ".TXT");
            string input = null;
            while ((input = re.ReadLine()) != null)
            {
                cadena_original += input;
            }
            re.Close();
            //Buscar el &amp; en la Cadena y sustituirlo por &
            cadena_original = cadena_original.Replace("&amp;", "&");

            //Eliminar Archivos Temporales
            File.Delete(nombre_tmp + ".TXT");
            File.Delete(nombre_tmp + ".XML");


            StringBuilder sbPassword;
            StringBuilder sbPrivateKey;
            byte[] b;
            byte[] block;
            int keyBytes;
            sbPassword = new StringBuilder(oEMPRESAp.PASSWORD);
            sbPrivateKey = Rsa.ReadEncPrivateKey(oEMPRESAp.LLAVE, sbPassword.ToString());
            keyBytes = Rsa.KeyBytes(sbPrivateKey.ToString());
            b = System.Text.Encoding.UTF8.GetBytes(cadena_original);
            block = Rsa.EncodeMsgForSignature(keyBytes, b, HashAlgorithm.Sha1);
            block = Rsa.RawPrivate(block, sbPrivateKey.ToString());
            string resultado = System.Convert.ToBase64String(block);
            Wipe.String(sbPassword);
            Wipe.String(sbPrivateKey);
            Wipe.Data(block);

            return resultado;
        }


        public List<cPoliza> todos(string ROW_ID_CATALOGOp,string tipoFieldp = "",string ASIGNADOp=""
            , bool solo_con_transacciones = false, bool solo_sin_transacciones = false)
        {

            List<cPoliza> lista = new List<cPoliza>();

            sSQL = " SELECT * ";
            sSQL += " FROM Poliza ";
            sSQL += " WHERE 1=1 ";
            if(ROW_ID_CATALOGOp!="")
            {
                sSQL += " AND ROW_ID_CATALOGO=" + ROW_ID_CATALOGOp + "";
            }
            
            sSQL += " AND tipoField LIKE '%" + tipoFieldp + "%' ";
            if (ASIGNADOp!="")
            {
                sSQL += " AND ASIGNADO LIKE '" + ASIGNADOp + "' ";

            }
            if(solo_con_transacciones)
            {
                sSQL += " AND (select COUNT(*) from PolizasPolizaTransaccion ppt where ppt.ROW_ID_POLIZA=Poliza.ROW_ID)<>0";
            }
            if (solo_sin_transacciones)
            {
                sSQL += " AND (select COUNT(*) from PolizasPolizaTransaccion ppt where ppt.ROW_ID_POLIZA=Poliza.ROW_ID)=0";
            }
            sSQL += " ORDER BY numField";
            DataTable oDataTable = oData.EjecutarConsulta(sSQL);
            foreach (DataRow oDataRow in oDataTable.Rows)
            {
                cPoliza ocCatalogo = new cPoliza();
                ocCatalogo.cargar(oDataRow["ROW_ID"].ToString());
                lista.Add(ocCatalogo);
            }
            return lista;

        }

        public bool verificar(string numFieldp, string ROW_ID_CATALOGOp, bool cargarp=false)
        {
            sSQL = " SELECT * ";
            sSQL += " FROM Poliza ";
            sSQL += " WHERE numField='" + numFieldp + "' ";
            sSQL += " AND ROW_ID_CATALOGO=" + ROW_ID_CATALOGOp + "";
            DataTable oDataTable = oData.EjecutarConsulta(sSQL);
            if (oDataTable != null)
            {
                foreach (DataRow oDataRow in oDataTable.Rows)
                {

                    if (!cargarp)
                    {
                        return true;
                    }
                    else
                    {
                        cargar(oDataRow["ROW_ID"].ToString());
                    }
                }
            }
            return false;
        }
        public bool cargar_seguridad(string ROW_IDp, string USUARIOp)
        {
            cargar(ROW_IDp);
            if(ASIGNADO!="")
            {
                if(ASIGNADO==USUARIOp)
                {
                    return true;
                }
                return false;
            }
            return true;
        }
        public bool cargar(string ROW_IDp)
        {
            sSQL = " SELECT *";
            sSQL += " , ISNULL((SELECT SUM(PPT.debeField) FROM PolizasPolizaTransaccion PPT WHERE PPT.ROW_ID_POLIZA=Poliza.ROW_ID),0) as debe ";
            sSQL += " , ISNULL((SELECT SUM(PPT.haberField) FROM PolizasPolizaTransaccion PPT WHERE PPT.ROW_ID_POLIZA=Poliza.ROW_ID),0) as haber ";
            sSQL += " , ISNULL((SELECT TOP 1 PPT.TIPO FROM PolizasPolizaTransaccion PPT WHERE PPT.ROW_ID_POLIZA=Poliza.ROW_ID),'') as TIPO ";
            sSQL += " FROM Poliza ";
            sSQL += " WHERE ROW_ID=" + ROW_IDp + " ";
            DataTable oDataTable = oData.EjecutarConsulta(sSQL);
            if (oDataTable != null)
            {
                foreach (DataRow oDataRow in oDataTable.Rows)
                {
                    //Añadir el Registro
                    ROW_ID = oDataRow["ROW_ID"].ToString();
                    ROW_ID_CATALOGO = oDataRow["ROW_ID_CATALOGO"].ToString();
                    tipoField = oDataRow["tipoField"].ToString();
                    ASIGNADO = oDataRow["ASIGNADO"].ToString();
                    numField = oDataRow["numField"].ToString();
                    fechaField = DateTime.Parse(oDataRow["fechaField"].ToString());
                    conceptoField = oDataRow["conceptoField"].ToString();
                    DEBE = decimal.Parse(oDataRow["debe"].ToString());
                    HABER = decimal.Parse(oDataRow["haber"].ToString());
                    TYPE = oDataRow["TIPO"].ToString();
                    cargar_comprobantes(ROW_ID);
                    //validar_montos_comprobantes(ROW_ID);
                    return true;
                }
            }
            return false;
        }

        private void cargar_comprobantes(string ROW_ID_POLIZAp)
        {
            comprobantes = 0;            
            sSQL = " SELECT COUNT(*) as Cuenta ";
            sSQL += " FROM PolizasPolizaTransaccion INNER JOIN PolizasPolizaTransaccionCompNal ON PolizasPolizaTransaccion.ROW_ID = PolizasPolizaTransaccionCompNal.ROW_ID_TRANSACCION ";
            sSQL += " WHERE PolizasPolizaTransaccion.ROW_ID_POLIZA=" + ROW_ID_POLIZAp + "";
            DataTable oDataTable = oData.EjecutarConsulta(sSQL);
            if (oDataTable != null)
            {
                foreach (DataRow oDataRow in oDataTable.Rows)
                {
                    comprobantes=int.Parse(oDataRow["Cuenta"].ToString());
                }
            }
            if ((tipoField == "1") | (tipoField == "2"))
            {
                if(TYPE != "BAJ")
                {
                    if (comprobantes == 0)
                    {
                        error += "| P2-Póliza " + numField + " debe tener Comprobantes Físcales.";
                    }
                }
            }
            if (tipoField == "3")
            {
                if ((TYPE == "API") | (TYPE == "ARI"))
                {
                    if (comprobantes == 0)
                    {
                        error += "| P2-Póliza " + numField + " debe tener Comprobantes Físcales.";
                    }
                }
            }

        }


        public string validar_montos_comprobantes(string ROW_ID_POLIZAp)
        {
            decimal montoTotalField_tr = 0;

            sSQL = " SELECT ISNULL(SUM(PolizasPolizaTransaccionCompNal.montoTotalField),0) as Total ";
            sSQL += " FROM PolizasPolizaTransaccion INNER JOIN PolizasPolizaTransaccionCompNal ON PolizasPolizaTransaccion.ROW_ID = PolizasPolizaTransaccionCompNal.ROW_ID_TRANSACCION ";
            sSQL += " WHERE PolizasPolizaTransaccion.ROW_ID_POLIZA=" + ROW_ID_POLIZAp + "";
            DataTable oDataTable = oData.EjecutarConsulta(sSQL);
            if (oDataTable != null)
            {
                foreach (DataRow oDataRow in oDataTable.Rows)
                {
                    montoTotalField_tr = decimal.Parse(oDataRow["Total"].ToString());
                }
            }
            cCatalogo ocCatalogo=new cCatalogo();
            ocCatalogo.cargar(ROW_ID_CATALOGO);

            //Sumar las cuentas de Bancos 
            //Buscar las cuentas de bancos
            string cuentas_bancos = "";
            sSQL = "SELECT GL_ACCOUNT_ID FROM BANK_ACCOUNT";
            cEMPRESA_CONEXION ocEMPRESA_CONEXION = new cEMPRESA_CONEXION();
            foreach (cEMPRESA_CONEXION registro in ocEMPRESA_CONEXION.todos("",ocCatalogo.rFCField))
            {
                cCONEXCION oData_ERPp = registro.conectar(registro.ROW_ID);
                oDataTable = oData_ERPp.EjecutarConsulta(sSQL);
                if (oDataTable != null)
                {
                    foreach (DataRow oDataRow in oDataTable.Rows)
                    {
                        if(cuentas_bancos!="")
                        {
                            cuentas_bancos+=",";
                        }
                        cuentas_bancos+= "'"+oDataRow["GL_ACCOUNT_ID"].ToString()+"'";
                    }
                }
            }
            
            
            //
            decimal polizaTotalField_tr = 0;
            if (cuentas_bancos!="")
            {
                sSQL = " SELECT ISNULL(SUM(PolizasPolizaTransaccion.debeField-PolizasPolizaTransaccion.haberField),0) as Total ";
                sSQL += " FROM PolizasPolizaTransaccion ";
                sSQL += " WHERE PolizasPolizaTransaccion.ROW_ID_POLIZA=" + ROW_ID_POLIZAp + " AND numCtaField in (" + cuentas_bancos + ")";
                oDataTable = oData.EjecutarConsulta(sSQL);
                if (oDataTable != null)
                {
                    foreach (DataRow oDataRow in oDataTable.Rows)
                    {
                        polizaTotalField_tr = decimal.Parse(oDataRow["Total"].ToString());
                    }
                }
                if (montoTotalField_tr != polizaTotalField_tr)
                {
                    return "P4 - El monto de comprobantes (" + montoTotalField_tr.ToString() + ") es diferente al monto de la póliza (" + polizaTotalField_tr.ToString() + "). ";
                }
            }


            return "";
        }

        public List<cPoliza> validar(string ROW_ID_CATALOGOp)
        {

            List<cPoliza> lista = new List<cPoliza>();
            sSQL = " SELECT * ";
            sSQL += " ,(debe_Balanza-haber_Balanza) as 'Movimientos Balanza'";
            sSQL += " ,(debe_Transacciones-haber_Transacciones) as 'Movimientos Transacciones' ";
            sSQL += " ,(debe_Balanza-haber_Balanza)-(debe_Transacciones-haber_Transacciones) as 'Diferencia' ";
            sSQL += " FROM [dbo].[ENT_CE_POLIZA_BALANZA] ";
            sSQL += " WHERE 1=1 ";
            sSQL += " AND ROW_ID_CATALOGO=" + ROW_ID_CATALOGOp;
            sSQL += " AND ((debe_Balanza-haber_Balanza)-(debe_Transacciones-haber_Transacciones))<-1";
            sSQL += " AND ((debe_Balanza-haber_Balanza)-(debe_Transacciones-haber_Transacciones))>1";
            sSQL += " ORDER BY numCtaField ";

            DataTable oDataTable = oData.EjecutarConsulta(sSQL);
            foreach (DataRow oDataRow in oDataTable.Rows)
            {
                cPoliza ocCatalogo = new cPoliza();
                //ocCatalogo.cargar(oDataRow["ROW_ID"].ToString());



                ocCatalogo.error = "P1-Póliza " + oDataRow["numCtaField"].ToString() + " la sumatoría del debe y haber con la Balanza de Comprobación no coinciden.";
                ocCatalogo.numField = oDataRow["numCtaField"].ToString();
                lista.Add(ocCatalogo);
            }
            
            return lista;

        }

        public void comprobantes_API(string ROW_ID_CATALOGOp, cEMPRESA_CE ocEMPRESA)
        {
            //Buscar aquellas comprobantes que no tienen Documentos
            sSQL = " select PPT.ROW_ID,Poliza.conceptoField, DOCUMENTO, ENTE, TIPO,Poliza.ROW_ID_CATALOGO"; 
            sSQL += " FROM PolizasPolizaTransaccion AS PPT INNER JOIN Poliza ON PPT.ROW_ID_POLIZA = Poliza.ROW_ID ";
            sSQL += " WHERE TIPO='API' AND Poliza.ROW_ID_CATALOGO=" + ROW_ID_CATALOGOp;
            sSQL += " AND (";
            sSQL += " SELECT COUNT(*) as Cuenta "; 
            sSQL += " FROM PolizasPolizaTransaccion INNER JOIN PolizasPolizaTransaccionCompNal ON PolizasPolizaTransaccion.ROW_ID = PolizasPolizaTransaccionCompNal.ROW_ID_TRANSACCION ";
            sSQL += " WHERE PolizasPolizaTransaccion.ROW_ID=PPT.ROW_ID ";
            sSQL += " )=0 AND DOCUMENTO='MP45999'";

            DataTable oDataTable = oData.EjecutarConsulta(sSQL);
            foreach (DataRow oDataRow in oDataTable.Rows)
            {
                cEMPRESA_CONEXION ocEMPRESA_CONEXION_tr = new cEMPRESA_CONEXION();
                foreach (cEMPRESA_CONEXION registro_tr in ocEMPRESA_CONEXION_tr.todos(ocEMPRESA.ROW_ID))
                {
                    string INVOICE_IDt = "";
                    string CURRENCY_ID = "";
                    cEMPRESA_CONEXION ocEMPRESA_CONEXION = registro_tr;
                    cCONEXCION oData_ERPp = registro_tr.conectar(registro_tr.ROW_ID);
                    sSQL = "SELECT INVOICE_ID,CURRENCY_ID, SELL_RATE FROM PAYABLE WHERE VOUCHER_ID='" + oDataRow["DOCUMENTO"].ToString() + "'";
                    DataTable oDataTable2 = oData_ERPp.EjecutarConsulta(sSQL);                    
                    foreach (DataRow oDataRow2 in oDataTable2.Rows)
                    {
                        INVOICE_IDt = oDataRow2["INVOICE_ID"].ToString();
                        CURRENCY_ID= oDataRow2["CURRENCY_ID"].ToString();
                    }
                    //Buscar en CFDI_PROVEEDORES y Agregar a las transacciones
                    //Buscar Voucher
                    cEMPRESA_MONEDA ocEMPRESA_MONEDA=new cEMPRESA_MONEDA();
                    cPolizasPolizaTransaccionCompNal ocPolizasPolizaTransaccionCompNal=new cPolizasPolizaTransaccionCompNal();
                    if(ocPolizasPolizaTransaccionCompNal.generar_CFDI_PROVEEDORES(oDataRow["ROW_ID"].ToString()
                        , oDataRow["DOCUMENTO"].ToString(), oDataRow["ENTE"].ToString(), ocEMPRESA_CONEXION))
                    {
                        ocPolizasPolizaTransaccionCompNal.monedaField = ocEMPRESA_MONEDA.cambiar_MONEDA(CURRENCY_ID, ocEMPRESA.ROW_ID);
                        ocPolizasPolizaTransaccionCompNal.guardar();
                    }
                    //Buscar Invoice_D
                    if(ocPolizasPolizaTransaccionCompNal.generar_CFDI_PROVEEDORES(oDataRow["ROW_ID"].ToString()
                        , INVOICE_IDt, oDataRow["ENTE"].ToString(), ocEMPRESA_CONEXION))
                    {
                        ocPolizasPolizaTransaccionCompNal.monedaField = ocEMPRESA_MONEDA.cambiar_MONEDA(CURRENCY_ID, ocEMPRESA.ROW_ID);
                        ocPolizasPolizaTransaccionCompNal.guardar();
                    }

                }
            }
            

        }



        public bool guardar(bool validar=true)
        {
            if (validar)
            {

                if (tipoField.Trim() == "")
                {
                    MessageBoxEx.Show("El tipo es requerido de la póliza " + numField, Application.ProductName + "-" + Application.ProductVersion, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                    return false;
                }
                
            }
            string ASIGNADOtr = "";
            ASIGNADOtr = ASIGNADO;
            verificar(numField, ROW_ID_CATALOGO, true);
            if (ASIGNADO != ASIGNADOtr)
            {
                ASIGNADO = ASIGNADOtr;
            }
            if (ROW_ID == "")
            {
                if (verificar(numField, ROW_ID_CATALOGO))
                {
                    if (validar)
                    {
                       // MessageBoxEx.Show("La Poliza " + numField + " ya existe.", Application.ProductName + "-" + Application.ProductVersion, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                        
                    }
                    return false;
                }
                sSQL = " DELETE FROM Poliza ";
                sSQL += " WHERE numField='" + numField.ToString() + "' ";
               oData.EjecutarConsulta(sSQL);

                sSQL = " INSERT INTO Poliza ";
                sSQL += " ( ";
                sSQL += " [ROW_ID_CATALOGO],[tipoField],[numField]";
                sSQL += ",[conceptoField],[fechaField],[ASIGNADO] ";
                sSQL += " )";
                sSQL += " VALUES ";
                sSQL += "(";
                sSQL += " " + ROW_ID_CATALOGO + ",'" + tipoField + "','" + numField.ToString() + "'";
                sSQL += ",'" + conceptoField.ToString() + "'," + oData.convertir_cadena_fecha(fechaField .ToString())+ ",'"+ASIGNADO+"' ";
                sSQL += ")";
                DataTable oDataTable = oData.EjecutarConsulta(sSQL);
                if (oDataTable != null)
                {

                    sSQL = "SELECT TOP 1 MAX(ROW_ID) as ROW_ID FROM Poliza";
                    oDataTable = oData.EjecutarConsulta(sSQL);
                    if (oDataTable != null)
                    {
                        foreach (DataRow oDataRow in oDataTable.Rows)
                        {
                            ROW_ID = oDataRow["ROW_ID"].ToString();
                        }
                    }
                }

            }
            else
            {
                sSQL = " UPDATE Poliza ";
                sSQL += " SET ROW_ID_CATALOGO=" + ROW_ID_CATALOGO + ",tipoField='" + tipoField + "',numField='" + numField.ToString() + "'";
                sSQL += ",conceptoField='" + conceptoField.ToString() + "',fechaField=" + oData.convertir_cadena_fecha(fechaField.ToString()) + " ";
                sSQL += ",ASIGNADO='" + ASIGNADO + "' WHERE ROW_ID=" + ROW_ID;
                oData.EjecutarConsulta(sSQL);

            }
            return true;

        }

        public bool eliminar(string ROW_IDp)
        {
            if (ROW_IDp != "")
            {
                sSQL = "DELETE FROM Poliza ";
                sSQL += " WHERE ROW_ID='" + ROW_IDp + "' ";
                oData.EjecutarConsulta(sSQL);
                //Eliminar las transacciones
                cTransaccion ocTransaccion = new cTransaccion();
                ocTransaccion.eliminar_poliza(ROW_IDp);
                return true;
            }
            return false;
        }

        public string ASIGNADO { get; set; }



        internal void importar_comprobantes()
        {
            if (ROW_ID == "")
            {
                MessageBoxEx.Show("Guarde primero.", Application.ProductName + "-" + Application.ProductVersion, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return;
            }

            OpenFileDialog openFileDialog1 = new OpenFileDialog();
            openFileDialog1.Filter = "Excel files (*.xlsx)|*.xlsx|All files (*.*)|*.*";
            openFileDialog1.FilterIndex = 1;
            openFileDialog1.RestoreDirectory = true;

            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                try
                {
                    procesar(1, openFileDialog1.FileName);
                }
                catch (Exception)
                {
                    MessageBoxEx.Show("No existe el Archivo seleccionado", Application.ProductName + "-" + Application.ProductVersion, MessageBoxButtons.OK);
                }
            }
        }

        private void procesar(int hoja, string archivo)
        {
                int n;
                int rowIndex = 1;
                FileInfo existingFile = new FileInfo(archivo);
                using (ExcelPackage package = new ExcelPackage(existingFile))
                {
                    ExcelWorksheet worksheet = package.Workbook.Worksheets[hoja];
                    while (worksheet.Cells[rowIndex, 2].Value != null)
                    {
                        
                        //Agregar las lineas de la cabecera
                        if (rowIndex != 1)
                        {
                            string RFC_EMISOR_t = worksheet.Cells[rowIndex, 1].Text;
                            string RFC_RECEPTOR_t = worksheet.Cells[rowIndex, 2].Text;
                            string UUID_t = worksheet.Cells[rowIndex, 3].Text;
                            string montoTotalField_t = worksheet.Cells[rowIndex, 4].Text;
                            //Buscar todas las transacciones de la poliza
                            cTransaccion ocTransaccion = new cTransaccion();
                            foreach(cTransaccion registro in ocTransaccion.todos(ROW_ID))
                            {
                                cPolizasPolizaTransaccionCompNal ocPolizasPolizaTransaccionCompNal = new cPolizasPolizaTransaccionCompNal();
                                ocPolizasPolizaTransaccionCompNal.ROW_ID_TRANSACCION = registro.ROW_ID;
                                ocPolizasPolizaTransaccionCompNal.TIPO = registro.TIPO;
                                ocPolizasPolizaTransaccionCompNal.rFCField = RFC_EMISOR_t;
                                ocPolizasPolizaTransaccionCompNal.uUID_CFDIField = UUID_t;
                                ocPolizasPolizaTransaccionCompNal.montoTotalField = decimal.Parse(montoTotalField_t);
                                ocPolizasPolizaTransaccionCompNal.guardar();
                            }
                        }
                        rowIndex++;
                    }
                    MessageBoxEx.Show("Archivo migrado.", Application.ProductName + "-" + Application.ProductVersion, MessageBoxButtons.OK);
                }
        }

        internal void importar_transferencia()
        {
            if (ROW_ID == "")
            {
                MessageBoxEx.Show("Guarde primero.", Application.ProductName + "-" + Application.ProductVersion, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return;
            }

            OpenFileDialog openFileDialog1 = new OpenFileDialog();
            openFileDialog1.Filter = "Excel files (*.xlsx)|*.xlsx|All files (*.*)|*.*";
            openFileDialog1.FilterIndex = 1;
            openFileDialog1.RestoreDirectory = true;

            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                try
                {
                    procesar_transferencias(1, openFileDialog1.FileName);
                }
                catch (Exception)
                {
                    MessageBoxEx.Show("No existe el Archivo seleccionado", Application.ProductName + "-" + Application.ProductVersion, MessageBoxButtons.OK);
                }
            }
        }

        private void procesar_transferencias(int hoja, string archivo)
        {
            int n;
            int rowIndex = 1;
            FileInfo existingFile = new FileInfo(archivo);
            using (ExcelPackage package = new ExcelPackage(existingFile))
            {
                if (package.Workbook.Worksheets.Count <= hoja)
                {
                    return;
                }
                ExcelWorksheet worksheet = package.Workbook.Worksheets[hoja];
                while (worksheet.Cells[rowIndex, 2].Value != null)
                {

                    //Agregar las lineas de la cabecera
                    if (rowIndex != 1)
                    {
                        string poliza_t = worksheet.Cells[rowIndex, 1].Text;
                        string transaccion_t = worksheet.Cells[rowIndex, 2].Text;
                        string cuenta_origen_t = worksheet.Cells[rowIndex, 3].Text;
                        string banco_origen_t = worksheet.Cells[rowIndex, 4].Text;
                        string monto_t = worksheet.Cells[rowIndex, 1].Text;
                        string fecha_t = worksheet.Cells[rowIndex, 2].Text;
                        string cuenta_destino_t = worksheet.Cells[rowIndex, 3].Text;
                        string banco_destino_t = worksheet.Cells[rowIndex, 4].Text;
                        string beneficiario_t = worksheet.Cells[rowIndex, 4].Text;
                        string rfc_t = worksheet.Cells[rowIndex, 4].Text;


                        //Buscar todas las transacciones de la poliza
                        cTransaccion ocTransaccion = new cTransaccion();
                        foreach (cTransaccion registro in ocTransaccion.todos(ROW_ID))
                        {

                            cTransferencia ocTransferencia = new cTransferencia();
                            ocTransferencia.ROW_ID_TRANSACCION = registro.ROW_ID;
                            ocTransferencia.ctaDestField =cuenta_destino_t;
                            ocTransferencia.bancoOriField = banco_origen_t;
                            ocTransferencia.montoField =double.Parse(monto_t);
                            ocTransferencia.ctaOriField = cuenta_origen_t;
                            ocTransferencia.fechaField = DateTime.Parse(fecha_t);
                            ocTransferencia.bancoDestField = banco_destino_t;
                            ocTransferencia.rFCField = rfc_t;
                            ocTransferencia.benefField = beneficiario_t;
                            ocTransferencia.guardar();
                        }
                    }
                    rowIndex++;
                }
                MessageBoxEx.Show("Archivo migrado.", Application.ProductName + "-" + Application.ProductVersion, MessageBoxButtons.OK);
            }
        }

        internal void importar_cheques()
        {
            if (ROW_ID == "")
            {
                MessageBoxEx.Show("Guarde primero.", Application.ProductName + "-" + Application.ProductVersion, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return;
            }

            OpenFileDialog openFileDialog1 = new OpenFileDialog();
            openFileDialog1.Filter = "Excel files (*.xlsx)|*.xlsx|All files (*.*)|*.*";
            openFileDialog1.FilterIndex = 1;
            openFileDialog1.RestoreDirectory = true;

            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                try
                {
                    procesar_cheques(1, openFileDialog1.FileName);
                }
                catch (Exception)
                {
                    MessageBoxEx.Show("No existe el Archivo seleccionado", Application.ProductName + "-" + Application.ProductVersion, MessageBoxButtons.OK);
                }
            }
        }

        private void procesar_cheques(int hoja, string archivo)
        {
            int n;
            int rowIndex = 1;
            FileInfo existingFile = new FileInfo(archivo);
            using (ExcelPackage package = new ExcelPackage(existingFile))
            {
                ExcelWorksheet worksheet = package.Workbook.Worksheets[hoja];
                while (worksheet.Cells[rowIndex, 2].Value != null)
                {

                    //Agregar las lineas de la cabecera
                    if (rowIndex != 1)
                    {
                        string benefField = worksheet.Cells[rowIndex, 1].Text;
                        string rFCField = worksheet.Cells[rowIndex, 2].Text;
                        string bancoField = worksheet.Cells[rowIndex, 3].Text;
                        string ctaOriField = worksheet.Cells[rowIndex, 4].Text;
                        DateTime fechaField = DateTime.Parse(worksheet.Cells[rowIndex, 5].Text);
                        double montoField = double.Parse(worksheet.Cells[rowIndex, 6].Text);
                        
                        //Buscar todas las transacciones de la poliza
                        cTransaccion ocTransaccion = new cTransaccion();
                        foreach (cTransaccion registro in ocTransaccion.todos(ROW_ID))
                        {
                            cCheques ocCheques = new cCheques();
                            ocCheques.ROW_ID_TRANSACCION = registro.ROW_ID;
                            ocCheques.benefField = benefField;
                            ocCheques.rFCField = rFCField;
                            ocCheques.bancoField = bancoField;
                            ocCheques.ctaOriField = ctaOriField;
                            ocCheques.fechaField = fechaField;
                            ocCheques.montoField = montoField;
                            ocCheques.guardar();
                        }
                    }
                    rowIndex++;
                }
                MessageBoxEx.Show("Archivo migrado.", Application.ProductName + "-" + Application.ProductVersion, MessageBoxButtons.OK);
            }
        }

        public void procesar_comprobantes(int hoja, string archivo,cCatalogo ocCatalogo)
        {
            int n;
            int rowIndex = 1;
            FileInfo existingFile = new FileInfo(archivo);
            using (ExcelPackage package = new ExcelPackage(existingFile))
            {
                ExcelWorksheet worksheet = package.Workbook.Worksheets[hoja];
                while (worksheet.Cells[rowIndex, 2].Value != null)
                {

                    //Agregar las lineas de la cabecera
                    if (rowIndex != 1)
                    {
                        string Transaccion = worksheet.Cells[rowIndex, 1].Text;
                        string Poliza = worksheet.Cells[rowIndex, 2].Text;
                        string NumCta = worksheet.Cells[rowIndex, 3].Text;

                        string UUID_CFDI = worksheet.Cells[rowIndex, 4].Text;
                        string RFC = worksheet.Cells[rowIndex, 5].Text;
                        double montoField = double.Parse(worksheet.Cells[rowIndex, 6].Text);
                        string monedaField = worksheet.Cells[rowIndex, 7].Text;
                        decimal tipCambField = decimal.Parse(worksheet.Cells[rowIndex, 8].Text);

                        //Buscar todas las transacciones de la poliza
                        cPolizasPolizaTransaccionCompNal cCompNal= new cPolizasPolizaTransaccionCompNal();

                        cTransaccion ocTransaccion=new cTransaccion();
                        ocTransaccion=ocTransaccion.cargar_id(ocCatalogo.ROW_ID,Poliza,NumCta);
                        if (ocTransaccion != null)
                        {
                            cPolizasPolizaTransaccionCompNal ocCompNal = new cPolizasPolizaTransaccionCompNal();
                            ocCompNal.ROW_ID_TRANSACCION = ocTransaccion.ROW_ID;
                            ocCompNal.rFCField = RFC;
                            ocCompNal.uUID_CFDIField = UUID_CFDI;
                            ocCompNal.montoTotalField = decimal.Parse(montoField.ToString());
                            ocCompNal.monedaField = monedaField;
                            ocCompNal.tipCambField = tipCambField;
                            if (ocCompNal.guardar())
                            {

                            }
                        }

                    }
                    rowIndex++;
                }
                //MessageBoxEx.Show("Archivo migrado.", Application.ProductName + "-" + Application.ProductVersion, MessageBoxButtons.OK);
            }
        }


        internal void importar_transacciones()
        {
            if (ROW_ID == "")
            {
                MessageBoxEx.Show("Guarde primero.", Application.ProductName + "-" + Application.ProductVersion, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return;
            }

            OpenFileDialog openFileDialog1 = new OpenFileDialog();
            openFileDialog1.Filter = "Excel files (*.xlsx)|*.xlsx|All files (*.*)|*.*";
            openFileDialog1.FilterIndex = 1;
            openFileDialog1.RestoreDirectory = true;

            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                try
                {
                    procesar_transacciones(1, openFileDialog1.FileName);
                }
                catch (Exception)
                {
                    MessageBoxEx.Show("No existe el Archivo seleccionado", Application.ProductName + "-" + Application.ProductVersion, MessageBoxButtons.OK);
                }
            }
        }

        public void procesar_transacciones(int hoja, string archivo, bool buscar_polizas=false)
        {
            int n;
            int rowIndex = 1;
            FileInfo existingFile = new FileInfo(archivo);
            using (ExcelPackage package = new ExcelPackage(existingFile))
            {
                if (package.Workbook.Worksheets.Count <= hoja)
                {
                    return;
                }
                ExcelWorksheet worksheet = package.Workbook.Worksheets[hoja];
                while (worksheet.Cells[rowIndex, 2].Value != null)
                {

                    //Agregar las lineas de la cabecera
                    if (rowIndex != 1)
                    {
                        //Layout
                        string numCtaField = "";
                        string conceptoField = "";
                        double debeField = 0;
                        double haberField = 0;
                        string monedaField = "";

                        if(buscar_polizas)
                        {
                            string Transaccion = worksheet.Cells[rowIndex, 1].Text;
                            string Poliza = worksheet.Cells[rowIndex, 2].Text;
                            numCtaField = worksheet.Cells[rowIndex, 3].Text;
                            conceptoField = worksheet.Cells[rowIndex, 4].Text;
                            debeField = double.Parse(oData.IsNullNumero(worksheet.Cells[rowIndex, 5].Text));
                            haberField = double.Parse(oData.IsNullNumero(worksheet.Cells[rowIndex, 6].Text));
                            monedaField = worksheet.Cells[rowIndex, 7].Text;
                            
                            cPoliza ocPoliza = new cPoliza();
                            ocPoliza = ocPoliza.cargar_id(Poliza,ROW_ID_CATALOGO);
                            if (ocPoliza != null)
                            {
                                cTransaccion ocTransaccion = new cTransaccion();
                                ocTransaccion.ROW_ID_POLIZA = ocPoliza.ROW_ID;
                                ocTransaccion.numCtaField = numCtaField;
                                ocTransaccion.conceptoField=conceptoField;
                                ocTransaccion.monedaField=monedaField;
                                ocTransaccion.debeField=debeField;
                                ocTransaccion.haberField=haberField;
                                if(ocTransaccion.guardar(true))
                                {

                                }
                            }
                        }
                    }
                    rowIndex++;
                }
                //MessageBoxEx.Show("Archivo migrado.", Application.ProductName + "-" + Application.ProductVersion, MessageBoxButtons.OK);
            }
        }

        private cPoliza cargar_id(string Polizap, string ROW_ID_CATALOGOp)
        {
            sSQL = "SELECT TOP 1 * FROM Poliza WHERE [numField]='" + Polizap + "' AND ROW_ID_CATALOGO=" + ROW_ID_CATALOGOp;
            DataTable oDataTable = oData.EjecutarConsulta(sSQL);
            if (oDataTable != null)
            {
                foreach (DataRow oDataRow in oDataTable.Rows)
                {
                    cPoliza oPoliza=new cPoliza();
                    if(oPoliza.cargar(oDataRow["ROW_ID"].ToString()))
                    {
                        return oPoliza;
                    }
                    return null;
                }
            }
            return null;
        }

        internal void importar_poliza()
        {
            if (ROW_ID == "")
            {
                MessageBoxEx.Show("Guarde primero.", Application.ProductName + "-" + Application.ProductVersion, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return;
            }

            OpenFileDialog openFileDialog1 = new OpenFileDialog();
            openFileDialog1.Filter = "Excel files (*.xlsx)|*.xlsx|All files (*.*)|*.*";
            openFileDialog1.FilterIndex = 1;
            openFileDialog1.RestoreDirectory = true;

            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                try
                {
                    procesar_poliza(1, openFileDialog1.FileName);
                }
                catch (Exception)
                {
                    MessageBoxEx.Show("No existe el Archivo seleccionado", Application.ProductName + "-" + Application.ProductVersion, MessageBoxButtons.OK);
                }
            }
        }

        public void procesar_poliza(int hoja, string archivo)
        {
            int n;
            int rowIndex = 1;
            FileInfo existingFile = new FileInfo(archivo);
            
            using (ExcelPackage package = new ExcelPackage(existingFile))
            {
                if(package.Workbook.Worksheets.Count<=hoja)
                {
                    return;
                }
                ExcelWorksheet worksheet = package.Workbook.Worksheets[hoja];
                while (worksheet.Cells[rowIndex, 2].Value != null)
                {

                    //Agregar las lineas de la cabecera
                    if (rowIndex != 1)
                    {
                        string tipoField = worksheet.Cells[rowIndex, 1].Text;
                        string numField = worksheet.Cells[rowIndex, 2].Text;
                        DateTime fechaField = DateTime.Parse(worksheet.Cells[rowIndex, 3].Text);
                        string conceptoField = worksheet.Cells[rowIndex, 4].Text;

                        //Buscar todas las transacciones de la poliza
                        cPoliza ocPoliza = new cPoliza();
                        if(ROW_ID_CATALOGO!="")
                        {
                            ocPoliza.ROW_ID_CATALOGO = ROW_ID_CATALOGO;
                            ocPoliza.tipoField = tipoField;
                            ocPoliza.numField = numField;
                            ocPoliza.fechaField=fechaField;
                            ocPoliza.conceptoField=conceptoField;
                            ocPoliza.guardar();
                        }
                        
                    }
                    rowIndex++;
                }
                //MessageBoxEx.Show("Archivo migrado.", Application.ProductName + "-" + Application.ProductVersion, MessageBoxButtons.OK);
            }
        }


        internal void eliminar_diferentes(string ROW_ID_CATALOGOp, string numFieldp)
        {
            string sSQL = " SELECT * ";
            sSQL += " FROM Poliza ";
            sSQL += " WHERE numField='" + numFieldp + "' ";
            sSQL += " AND ROW_ID_CATALOGO<>" + ROW_ID_CATALOGOp + "";
            DataTable oDataTable = oData.EjecutarConsulta(sSQL);
            if (oDataTable != null)
            {
                foreach (DataRow oDataRow in oDataTable.Rows)
                {
                    eliminar(oDataRow["ROW_ID"].ToString());
                }
            }
        }
    }
}
