using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevComponents.DotNetBar;
using System.Globalization;
using System.Reflection;
using System.IO;
using OfficeOpenXml;
using System.Drawing.Imaging;
using System.Threading;
using System.Xml;
using System.Xml.XPath;
using System.Xml.Xsl;
using System.Xml.Serialization;
using Generales;

namespace Desarrollo
{
    public partial class frmPrincipal : DevComponents.DotNetBar.Metro.MetroAppForm
    {
        private cCONEXCION oData = new cCONEXCION("");
        string USUARIO;

        public frmPrincipal(string sConn)
        {
            oData.sConn = sConn;
            InitializeComponent();
        }

        private void frmPrincipal_FormClosed(object sender, FormClosedEventArgs e)
        {
            //Application.Exit();
        }

        private void buttonItem2_Click(object sender, EventArgs e)
        {
            //Application.Exit();
        }
        private void procesar(DataGridView dtg, int hoja)
        {
            try
            {
                dtg.Rows.Clear();
                int n;
                int rowIndex = 1;
                FileInfo existingFile = new FileInfo(DIRECTORIO.Text);
                using (ExcelPackage package = new ExcelPackage(existingFile))
                {
                    ExcelWorksheet worksheet = package.Workbook.Worksheets[hoja];
                    while (worksheet.Cells[rowIndex, 1].Value != null)
                    {
                        //Agregar las lineas de la cabecera
                        if (rowIndex == 1)
                        {
                            foreach (var firstRowCell in worksheet.Cells[1, 1, 1, worksheet.Dimension.End.Column])
                            {
                                //dtg.Columns.Add("C" + dtg.Columns.Count.ToString(), firstRowCell.Text);
                                dtg.Columns.Add(firstRowCell.Text, firstRowCell.Text);
                            }
                        }
                        else
                        {
                            n = dtg.Rows.Add();
                            for (var col = 1; col <= worksheet.Dimension.End.Column; col++)
                            {
                                dtg.Rows[n].Cells[col - 1].Value = worksheet.Cells[rowIndex, col].Text;
                            }
                        }
                        rowIndex++;

                    }

                }
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message.ToString());
            }

        }

        private void button1_Click(object sender, EventArgs e)
        {
            OpenFileDialog openFileDialog1 = new OpenFileDialog();
            openFileDialog1.Filter = "Excel files (*.xlsx)|*.xlsx|All files (*.*)|*.*";
            openFileDialog1.FilterIndex = 1;
            openFileDialog1.RestoreDirectory = true;

            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                try
                {
                    DIRECTORIO.Text = openFileDialog1.FileName;
                    procesar(dtgConfiguracion, 1);
                    procesar(dataGridViewX1, 2);
                }
                catch (Exception)
                {
                    MessageBoxEx.Show("No existe el Archivo seleccionado", Application.ProductName + "-" + Application.ProductVersion, MessageBoxButtons.OK);
                }
            }
        }

        private void buttonX6_Click(object sender, EventArgs e)
        {
            importar_catalogo_cuentas();
            importar_balanza_comprobacion();
            MessageBoxEx.Show("Registros Procesados.", Application.ProductName + "-" + Application.ProductVersion, MessageBoxButtons.OK);
        }

        private void importar_catalogo_cuentas()
        {
            Catalogo oCatalogo = new Catalogo();
            oCatalogo.Anio = int.Parse(A�O.Text);
            oCatalogo.Mes = CatalogoMes.Item01;
            oCatalogo.RFC = RFC.Text;
            CatalogoCtas[] oCtas;
            oCtas = new CatalogoCtas[dtgConfiguracion.Rows.Count];
            int contador = 0;
            foreach(DataGridViewRow registro in dtgConfiguracion.Rows)
            {
                oCtas[contador] = new CatalogoCtas();
                oCtas[contador].CodAgrup = registro.Cells[0].Value.ToString();
                oCtas[contador].NumCta = registro.Cells[1].Value.ToString();
                oCtas[contador].Desc = registro.Cells[2].Value.ToString();
                oCtas[contador].SubCtaDe = registro.Cells[3].Value.ToString();
                oCtas[contador].Nivel = int.Parse(registro.Cells[4].Value.ToString());

                contador++;
            }
            oCatalogo.Ctas = oCtas;
            //Transformar al XML
            string nombre_tmp = "Catalogo_Cuentas_" + DateTime.Now.ToString("ddMMyyyyhhmmss");
            TextWriter writer = new StreamWriter(nombre_tmp + ".XML");
            XmlSerializer serializer = new XmlSerializer(typeof(Catalogo));
            serializer.Serialize(writer, oCatalogo);

            writer.Close();
        }

        private void importar_balanza_comprobacion()
        {
            Balanza oBalanza=new Balanza();
            oBalanza.Anio = int.Parse(A�O.Text);
            oBalanza.Mes = BalanzaMes.Item01;
            oBalanza.RFC = RFC.Text;
            oBalanza.Version = VERSION.Text;
            BalanzaCtas[] oCtas;
            oCtas = new BalanzaCtas[dataGridViewX1.Rows.Count];
            int contador = 0;
            foreach (DataGridViewRow registro in dataGridViewX1.Rows)
            {
                oCtas[contador] = new BalanzaCtas();
                oCtas[contador].NumCta = registro.Cells[0].Value.ToString();
                oCtas[contador].SaldoIni = decimal.Parse(registro.Cells[1].Value.ToString());
                oCtas[contador].Debe = decimal.Parse(registro.Cells[2].Value.ToString());
                oCtas[contador].Haber = decimal.Parse(registro.Cells[3].Value.ToString());
                oCtas[contador].SaldoFin = decimal.Parse(registro.Cells[4].Value.ToString());
                contador++;
            }
            oBalanza.Ctas = oCtas;
            //Transformar al XML
            string nombre_tmp = "Balanza_Comprobacion_" + DateTime.Now.ToString("ddMMyyyyhhmmss");
            TextWriter writer = new StreamWriter(nombre_tmp + ".XML");
            XmlSerializer serializer = new XmlSerializer(typeof(Balanza));
            serializer.Serialize(writer, oBalanza);

            writer.Close();
        }

        private void metroTabPanel1_Click(object sender, EventArgs e)
        {

        }

        private void buttonX7_Click(object sender, EventArgs e)
        {

        }

    }
}
