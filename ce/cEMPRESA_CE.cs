﻿using CryptoSysPKI;
using DevComponents.DotNetBar;
using Generales;
using ModeloDocumentosFX;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Security.Cryptography;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Windows.Forms;

namespace Desarrollo
{
    class cEMPRESA_CE
    {
        private string sSQL = "";
        public string ROW_ID { get; set; }
        public string ID { get; set; }
        public string RFC { get; set; }

        public string NIVEL { get; set; }
        public string DIRECTORIO_XML_CLIENTES { get; set; }
        public string DIRECTORIO_XML_PROVEEDORES { get; set; }
        public string VISUAL_TIPO { get; set; }
        public string VISUAL_SERVIDOR { get; set; }
        public string VISUAL_BD { get; set; }
        public string VISUAL_USUARIO { get; set; }
        public string VISUAL_PASSWORD { get; set; }
        public string VISUAL_VERSION { get; set; }

        public string VISUAL_ENTITY_ID { get; set; }
        public string VISUAL_SITE_ID { get; set; }

        public string NO_CERTIFICADO { get; set; }

        public string CERTIFICADO { get; set; }
        public string LLAVE { get; set; }
        public string PASSWORD { get; set; }
        public bool PERIODO { get; set; }

        private cCONEXCION oData = new cCONEXCION();
        public override string ToString() { return ROW_ID; }

        public cEMPRESA_CE()
        {
            ModeloDocumentosFXContainer dbContext = new ModeloDocumentosFXContainer();
            try
            {
                string conection = dbContext.Database.Connection.ConnectionString;
                oData = new cCONEXCION(conection);
            }
            catch (Exception err)
            {
                ErrorFX.mostrar(err, false, true, "frmEmpresa - 96 - ", false);
            }
            limpiar();
        }

        //public cEMPRESA(cCONEXCION pData)
        //{
        //    oData = pData;
        //    limpiar();
        //}

        public void limpiar()
        {
            ROW_ID = "";
            ID = "";
            RFC = "";
            NIVEL = "";
            DIRECTORIO_XML_CLIENTES = "";
            DIRECTORIO_XML_PROVEEDORES = "";
            VISUAL_TIPO = "";
            VISUAL_SERVIDOR = "";
            VISUAL_BD = "";
            VISUAL_USUARIO = "";
            VISUAL_PASSWORD = "";
            VISUAL_VERSION = "";
            CERTIFICADO = "";
            LLAVE = "";
            PASSWORD = "";
            PERIODO = false;
            VISUAL_ENTITY_ID = "";
            VISUAL_SITE_ID = "";
        }

        public List<cEMPRESA_CE> todos(string IDp="", string RFCp="")
        {

            List<cEMPRESA_CE> lista = new List<cEMPRESA_CE>();

            sSQL = " SELECT * ";
            sSQL += " FROM EMPRESA_CE ";
            sSQL += " WHERE ID LIKE '%" + IDp + "%' ";
            sSQL += " AND RFC LIKE '%" + RFCp + "%' ";
            sSQL += " ORDER BY ID";
            DataTable oDataTable = oData.EjecutarConsulta(sSQL);
            foreach (DataRow oDataRow in oDataTable.Rows)
            {
                cEMPRESA_CE ocEMPRESA = new cEMPRESA_CE();
                ocEMPRESA.cargar(oDataRow["ROW_ID"].ToString());
                lista.Add(ocEMPRESA);
            }
            return lista;

        }

        public bool cargar(string ROW_IDp)
        {
            sSQL = " SELECT * ";
            sSQL += " FROM EMPRESA_CE ";
            sSQL += " WHERE ROW_ID=" + ROW_IDp + " ";
            DataTable oDataTable = oData.EjecutarConsulta(sSQL);
            if (oDataTable != null)
            {
                foreach (DataRow oDataRow in oDataTable.Rows)
                {
                    //Añadir el Registro
                    ROW_ID = oDataRow["ROW_ID"].ToString();
                    ID = oDataRow["ID"].ToString();
                    RFC = oDataRow["RFC"].ToString();
                    NIVEL = oDataRow["NIVEL"].ToString();
                    DIRECTORIO_XML_CLIENTES = oDataRow["DIRECTORIO_XML_CLIENTES"].ToString();
                    DIRECTORIO_XML_PROVEEDORES = oDataRow["DIRECTORIO_XML_PROVEEDORES"].ToString();
                    VISUAL_TIPO = oDataRow["VISUAL_TIPO"].ToString();
                    VISUAL_SERVIDOR = oDataRow["VISUAL_SERVIDOR"].ToString();
                    VISUAL_BD = oDataRow["VISUAL_BD"].ToString();
                    VISUAL_USUARIO = oDataRow["VISUAL_USUARIO"].ToString();
                    VISUAL_PASSWORD = oDataRow["VISUAL_PASSWORD"].ToString();
                    VISUAL_VERSION = oDataRow["VISUAL_VERSION"].ToString();
                    VISUAL_ENTITY_ID = oDataRow["VISUAL_ENTITY_ID"].ToString();
                    VISUAL_SITE_ID = oDataRow["VISUAL_SITE_ID"].ToString();

                    CERTIFICADO = oDataRow["CERTIFICADO"].ToString();
                    LLAVE = oDataRow["LLAVE"].ToString();
                    PASSWORD = oDataRow["PASSWORD"].ToString();
                    NO_CERTIFICADO = oDataRow["NO_CERTIFICADO"].ToString();
                    PERIODO = false;
                    try
                    {
                        PERIODO = bool.Parse(oDataRow["PERIODO"].ToString());
                    }
                    catch
                    {
                    }

                    return true;
                }
            }
            return false;
        }

        public bool guardar()
        {
            if(!validaCertificado())
            {
                MessageBoxEx.Show("Existe un error en el certificado.", Application.ProductName + "-" + Application.ProductVersion, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return false;
            }
            if (!verficar_password())
            {
                MessageBoxEx.Show("La Contraseña es incorrecta, no es posible generar el Sello.", Application.ProductName + "-" + Application.ProductVersion, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return false;
            }
            if (ID.Trim() == "")
            {
                MessageBoxEx.Show("La cuenta es requerida.", Application.ProductName + "-" + Application.ProductVersion, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return false;
            }

            if (RFC.Trim() == "")
            {
                MessageBoxEx.Show("Escriba un RFC.", Application.ProductName + "-" + Application.ProductVersion, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return false;
            }

            if (ROW_ID == "")
            {
                sSQL = " INSERT INTO EMPRESA_CE ";
                sSQL += " ( ";
                sSQL += " [ID],[RFC],[NIVEL] ";
                sSQL += " ,[DIRECTORIO_XML_CLIENTES],[DIRECTORIO_XML_PROVEEDORES] ";
                sSQL += " ,[VISUAL_TIPO],[VISUAL_SERVIDOR] ";
                sSQL += " ,[VISUAL_BD],[VISUAL_USUARIO] ";
                sSQL += " ,[VISUAL_PASSWORD],[VISUAL_VERSION],[VISUAL_ENTITY_ID],[VISUAL_SITE_ID]";
                sSQL += " ,CERTIFICADO,LLAVE,[PASSWORD]";
                sSQL += " ,ACCOUNT_VENDOR,ACCOUNT_CUSTOMER";
                sSQL += " ,NO_CERTIFICADO, PERIODO";
                sSQL += " )";
                sSQL += " VALUES ";
                sSQL += "(";
                sSQL += " '" + ID + "','" + RFC + "','" + NIVEL + "' ";
                sSQL += " ,'" + DIRECTORIO_XML_CLIENTES+ "','" + DIRECTORIO_XML_PROVEEDORES+ "' ";
                sSQL += " ,'" + VISUAL_TIPO+ "','" + VISUAL_SERVIDOR+ "' ";
                sSQL += " ,'" + VISUAL_BD+ "','" + VISUAL_USUARIO+ "' ";
                sSQL += " ,'" + VISUAL_PASSWORD+ "','" + VISUAL_VERSION+ "','" + VISUAL_ENTITY_ID+ "','" + VISUAL_SITE_ID+ "'";
                sSQL += " ,'" + CERTIFICADO + "','" + LLAVE + "','" + PASSWORD + "'";
                sSQL += " ,'" + ACCOUNT_VENDOR + "','" + ACCOUNT_CUSTOMER + "'";
                sSQL += " ,'" + NO_CERTIFICADO + "','" + PERIODO.ToString() + "'";
                sSQL += ")";
                DataTable oDataTable = oData.EjecutarConsulta(sSQL);
                if (oDataTable != null)
                {

                    sSQL = "SELECT TOP 1 MAX(ROW_ID) as ROW_ID FROM EMPRESA";
                    oDataTable = oData.EjecutarConsulta(sSQL);
                    if (oDataTable != null)
                    {
                        foreach (DataRow oDataRow in oDataTable.Rows)
                        {
                            ROW_ID = oDataRow["ROW_ID"].ToString();
                        }
                    }
                }

            }
            else
            {
                sSQL = " UPDATE EMPRESA_CE ";
                sSQL += " SET ID='" + ID + "',RFC='" + RFC + "',NIVEL='" + NIVEL + "' ";
                sSQL += " ,DIRECTORIO_XML_CLIENTES='" + DIRECTORIO_XML_CLIENTES + "',DIRECTORIO_XML_PROVEEDORES='" + DIRECTORIO_XML_PROVEEDORES + "' ";
                sSQL += " ,VISUAL_TIPO='" + VISUAL_TIPO + "',VISUAL_SERVIDOR='" + VISUAL_SERVIDOR + "' ";
                sSQL += " ,VISUAL_BD='" + VISUAL_BD + "',VISUAL_USUARIO='" + VISUAL_USUARIO + "' ";
                sSQL += " ,VISUAL_PASSWORD='" + VISUAL_PASSWORD + "',VISUAL_VERSION='" + VISUAL_VERSION + "',VISUAL_ENTITY_ID='" + VISUAL_ENTITY_ID + "',VISUAL_SITE_ID='" + VISUAL_SITE_ID + "'"; ;
                sSQL += " ,CERTIFICADO='" + CERTIFICADO + "',LLAVE='" + LLAVE + "',PASSWORD='" + PASSWORD + "'";
                sSQL += " ,ACCOUNT_VENDOR='" + ACCOUNT_VENDOR + "', ACCOUNT_CUSTOMER='" + ACCOUNT_CUSTOMER  + "'";
                sSQL += " ,NO_CERTIFICADO='" + NO_CERTIFICADO + "',PERIODO='" + PERIODO.ToString() + "'";
                sSQL += " WHERE ROW_ID=" + ROW_ID;
                oData.EjecutarConsulta(sSQL);

            }
            return true;

        }

        public bool eliminar(string ROW_IDp)
        {
            if (ROW_IDp != "")
            {
                sSQL = "DELETE FROM EMPRESA_CE ";
                sSQL += " WHERE ROW_ID='" + ROW_IDp + "' ";
                oData.EjecutarConsulta(sSQL);
                return true;
            }
            return false;
        }

        internal void cargar_rfc(string RFCp)
        {
            sSQL = " SELECT * ";
            sSQL += " FROM EMPRESA_CE ";
            sSQL += " WHERE RFC='" + RFCp + "' ";
            DataTable oDataTable = oData.EjecutarConsulta(sSQL);
            if (oDataTable != null)
            {
                foreach (DataRow oDataRow in oDataTable.Rows)
                {
                    cargar(oDataRow["ROW_ID"].ToString());
                }
            }
        }

        public string ACCOUNT_VENDOR { get; set; }

        public string ACCOUNT_CUSTOMER { get; set; }

        public string sellar(string LLAVEp, string PASSWORDp, string cadenap)
        {

            StringBuilder sbPassword;
            StringBuilder sbPrivateKey;
            byte[] b;
            byte[] block;
            int keyBytes;



            string strKeyFile = LLAVEp;

            sbPassword = new StringBuilder(PASSWORDp);

            sbPrivateKey = Rsa.ReadEncPrivateKey(strKeyFile, sbPassword.ToString());

            keyBytes = Rsa.KeyBytes(sbPrivateKey.ToString());

            b = System.Text.Encoding.UTF8.GetBytes(cadenap);


            block = Rsa.EncodeMsgForSignature(keyBytes, b, CryptoSysPKI.HashAlgorithm.Sha1);

            block = Rsa.RawPrivate(block, sbPrivateKey.ToString());

            string resultado = System.Convert.ToBase64String(block);

            Wipe.String(sbPassword);
            Wipe.String(sbPrivateKey);
            Wipe.Data(block);

            return resultado;

        }
        public bool verficar_password()
        {

            StringBuilder sbPassword;
            StringBuilder sbPrivateKey;

            string strKeyFile = LLAVE;

            sbPassword = new StringBuilder(PASSWORD);

            sbPrivateKey = Rsa.ReadEncPrivateKey(strKeyFile, sbPassword.ToString());


            if (sbPrivateKey.Length == 0)
            {
                return false;
            }
            else
            {
                return true;
            }


        }
        

        private bool validaCertificado()
        {
            string Certificate = CERTIFICADO.Trim();
            char[] delimiterChars = { '.' };
            string Text = CERTIFICADO.ToString();
            string[] TipoCer = Text.Split(delimiterChars);

            if (TipoCer[TipoCer.Length - 1].ToString() != "cer")
            {
                MessageBoxEx.Show("El archivo para el certificado debe tener la extensión .cer", Application.ProductName + "-" + Application.ProductVersion, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                CERTIFICADO = "";
                return false;
            }
            else if (System.IO.File.Exists(CERTIFICADO.Trim()) == false)
            {
                MessageBoxEx.Show("No existe el archivo:" + CERTIFICADO.Trim(), Application.ProductName + "-" + Application.ProductVersion, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                CERTIFICADO = "";
                return false;
            }
            else
            {
                try
                {
                    X509Certificate cert = X509Certificate.CreateFromCertFile(Certificate);
                    string NoSerie = cert.GetSerialNumberString();
                   
                    StringBuilder SerieHex = new StringBuilder();
                    for (int i = 0; i <= NoSerie.Length - 2; i += 2)
                    {
                        SerieHex.Append(Convert.ToString(Convert.ToChar(Int32.Parse(NoSerie.Substring(i, 2), System.Globalization.NumberStyles.HexNumber))));
                    }

                    NO_CERTIFICADO = SerieHex.ToString();

                    return true;
                }

                catch (CryptographicException)
                {
                    MessageBoxEx.Show("No es un certificado valido", Application.ProductName + "-" + Application.ProductVersion, MessageBoxButtons.OK, MessageBoxIcon.Information);
                    CERTIFICADO = "";
                    return false;
                }
            }
        }


    }
}
