using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevComponents.DotNetBar;
using System.IO;
using OfficeOpenXml;

namespace Desarrollo
{
    public partial class frmComprobante : DevComponents.DotNetBar.Metro.MetroForm
    {
        bool modificado = false;
        string ROW_ID_TRANSACCION;
        cPolizasPolizaTransaccionCompNal oObjeto = new cPolizasPolizaTransaccionCompNal();
        
        public frmComprobante(string ROW_ID_TRANSACCIONp)
        {
            oObjeto = new cPolizasPolizaTransaccionCompNal();
            ROW_ID_TRANSACCION = ROW_ID_TRANSACCIONp;
            InitializeComponent();
            limpiar();
        }

        public frmComprobante(string ROW_ID_TRANSACCIONp,string ROW_ID)
        {
            ROW_ID_TRANSACCION = ROW_ID_TRANSACCIONp;
            oObjeto = new cPolizasPolizaTransaccionCompNal();
            InitializeComponent();
            cargar(ROW_ID);
        }

        #region Acciones
        private void cargar(string ROW_IDp)
        {
            if (oObjeto.cargar(ROW_IDp))
            {
                ROW_ID_TRANSACCION = oObjeto.ROW_ID_TRANSACCION;
                rFCField.Text = oObjeto.rFCField;
                uUID_CFDIField.Text = oObjeto.uUID_CFDIField;
            }

        }

        private void guardar()
        {
            oObjeto.ROW_ID_TRANSACCION = ROW_ID_TRANSACCION;
            oObjeto.rFCField = rFCField.Text;
            oObjeto.uUID_CFDIField = uUID_CFDIField.Text;            
            if (oObjeto.guardar())
            {
                labelItem1.Text = "Datos Actualizados " + DateTime.Now.ToShortDateString() + " " + DateTime.Now.ToLongTimeString();
                mostrar_mensaje("Datos Actualizados ", false);
                modificado = false;
            }

        }

        private void mostrar_mensaje(string mensaje, bool mostrar_error)
        {
            ToastNotification.Show(this,
                mensaje,
                mostrar_error ? global::Desarrollo.Properties.Resources.error : global::Desarrollo.Properties.Resources.guardar,
                3000,
                mostrar_error ? eToastGlowColor.Red : eToastGlowColor.Green,
                eToastPosition.TopCenter);
        }
        public void limpiar()
        {
            if (modificado)
            {

                DialogResult Resultado = MessageBoxEx.Show("�Desea guardar los cambios?", Application.ProductName + "-" + Application.ProductVersion, MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                if (Resultado.ToString() == "Yes")
                {
                    guardar();
                }
            }
            modificado = false;
            oObjeto = new cPolizasPolizaTransaccionCompNal();
            foreach (var c in this.Controls)
            {
                if (c is TextBox)
                {
                    ((TextBox)c).Text = "";
                }
            }
            

        }
        
        private void eliminar()
        {
            DialogResult Resultado = MessageBoxEx.Show("�Esta seguro de eliminar?", Application.ProductName + "-" + Application.ProductVersion, MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            if (Resultado.ToString() != "Yes")
            {
                return;
            }
            if (oObjeto.eliminar(oObjeto.ROW_ID))
            {
                modificado = false;
                
                MessageBoxEx.Show("Eliminado.", Application.ProductName + "-" + Application.ProductVersion, MessageBoxButtons.OK, MessageBoxIcon.Information);
                limpiar();
                
            }
        }
        #endregion

        private void buttonX3_Click(object sender, EventArgs e)
        {
            eliminar();
        }

        private void buttonX2_Click(object sender, EventArgs e)
        {
            limpiar();
        }

        private void buttonX1_Click(object sender, EventArgs e)
        {
            guardar();
        }

        private void labelX4_Click(object sender, EventArgs e)
        {

        }

    }
}