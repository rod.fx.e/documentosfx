﻿namespace Desarrollo
{
    partial class ucDeclaracion
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.ESTADO = new DevComponents.DotNetBar.Controls.ComboBoxEx();
            this.comboItem3 = new DevComponents.Editors.ComboItem();
            this.comboItem4 = new DevComponents.Editors.ComboItem();
            this.labelX5 = new DevComponents.DotNetBar.LabelX();
            this.mesField = new DevComponents.DotNetBar.Controls.ComboBoxEx();
            this.comboItem1 = new DevComponents.Editors.ComboItem();
            this.comboItem2 = new DevComponents.Editors.ComboItem();
            this.comboItem5 = new DevComponents.Editors.ComboItem();
            this.comboItem6 = new DevComponents.Editors.ComboItem();
            this.comboItem7 = new DevComponents.Editors.ComboItem();
            this.comboItem8 = new DevComponents.Editors.ComboItem();
            this.comboItem9 = new DevComponents.Editors.ComboItem();
            this.comboItem10 = new DevComponents.Editors.ComboItem();
            this.comboItem11 = new DevComponents.Editors.ComboItem();
            this.comboItem12 = new DevComponents.Editors.ComboItem();
            this.comboItem13 = new DevComponents.Editors.ComboItem();
            this.comboItem14 = new DevComponents.Editors.ComboItem();
            this.labelX4 = new DevComponents.DotNetBar.LabelX();
            this.totalCtasField = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.labelX3 = new DevComponents.DotNetBar.LabelX();
            this.anoField = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.labelX2 = new DevComponents.DotNetBar.LabelX();
            this.labelX1 = new DevComponents.DotNetBar.LabelX();
            this.rFCField = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.versionField = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.label1 = new DevComponents.DotNetBar.LabelX();
            this.SuspendLayout();
            // 
            // ESTADO
            // 
            this.ESTADO.AutoCompleteCustomSource.AddRange(new string[] {
            "Abierto",
            "Cerrado"});
            this.ESTADO.DisplayMember = "Text";
            this.ESTADO.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.ESTADO.ForeColor = System.Drawing.Color.Black;
            this.ESTADO.FormattingEnabled = true;
            this.ESTADO.ItemHeight = 16;
            this.ESTADO.Items.AddRange(new object[] {
            this.comboItem3,
            this.comboItem4});
            this.ESTADO.Location = new System.Drawing.Point(560, 3);
            this.ESTADO.Name = "ESTADO";
            this.ESTADO.Size = new System.Drawing.Size(109, 22);
            this.ESTADO.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.ESTADO.TabIndex = 35;
            // 
            // comboItem3
            // 
            this.comboItem3.Text = "Abierto";
            // 
            // comboItem4
            // 
            this.comboItem4.Text = "Cerrado";
            // 
            // labelX5
            // 
            this.labelX5.AutoSize = true;
            this.labelX5.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.labelX5.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX5.ForeColor = System.Drawing.Color.Black;
            this.labelX5.Location = new System.Drawing.Point(522, 6);
            this.labelX5.Name = "labelX5";
            this.labelX5.Size = new System.Drawing.Size(37, 15);
            this.labelX5.TabIndex = 34;
            this.labelX5.Text = "Estado";
            // 
            // mesField
            // 
            this.mesField.AutoCompleteCustomSource.AddRange(new string[] {
            "01",
            "02",
            "03",
            "04",
            "05",
            "06",
            "07",
            "08",
            "09",
            "10",
            "11",
            "12"});
            this.mesField.DisplayMember = "Text";
            this.mesField.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.mesField.ForeColor = System.Drawing.Color.Black;
            this.mesField.FormattingEnabled = true;
            this.mesField.ItemHeight = 16;
            this.mesField.Items.AddRange(new object[] {
            this.comboItem1,
            this.comboItem2,
            this.comboItem5,
            this.comboItem6,
            this.comboItem7,
            this.comboItem8,
            this.comboItem9,
            this.comboItem10,
            this.comboItem11,
            this.comboItem12,
            this.comboItem13,
            this.comboItem14});
            this.mesField.Location = new System.Drawing.Point(237, 3);
            this.mesField.Name = "mesField";
            this.mesField.Size = new System.Drawing.Size(45, 22);
            this.mesField.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.mesField.TabIndex = 33;
            // 
            // comboItem1
            // 
            this.comboItem1.Text = "01";
            // 
            // comboItem2
            // 
            this.comboItem2.Text = "02";
            // 
            // comboItem5
            // 
            this.comboItem5.Text = "03";
            // 
            // comboItem6
            // 
            this.comboItem6.Text = "04";
            // 
            // comboItem7
            // 
            this.comboItem7.Text = "05";
            // 
            // comboItem8
            // 
            this.comboItem8.Text = "06";
            // 
            // comboItem9
            // 
            this.comboItem9.Text = "07";
            // 
            // comboItem10
            // 
            this.comboItem10.Text = "08";
            // 
            // comboItem11
            // 
            this.comboItem11.Text = "09";
            // 
            // comboItem12
            // 
            this.comboItem12.Text = "10";
            // 
            // comboItem13
            // 
            this.comboItem13.Text = "11";
            // 
            // comboItem14
            // 
            this.comboItem14.Text = "12";
            // 
            // labelX4
            // 
            this.labelX4.AutoSize = true;
            this.labelX4.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.labelX4.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX4.ForeColor = System.Drawing.Color.Black;
            this.labelX4.Location = new System.Drawing.Point(366, 6);
            this.labelX4.Name = "labelX4";
            this.labelX4.Size = new System.Drawing.Size(86, 15);
            this.labelX4.TabIndex = 31;
            this.labelX4.Text = "Total de Cuentas";
            // 
            // totalCtasField
            // 
            this.totalCtasField.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.totalCtasField.Border.Class = "TextBoxBorder";
            this.totalCtasField.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.totalCtasField.Enabled = false;
            this.totalCtasField.ForeColor = System.Drawing.Color.Black;
            this.totalCtasField.Location = new System.Drawing.Point(456, 3);
            this.totalCtasField.Name = "totalCtasField";
            this.totalCtasField.Size = new System.Drawing.Size(60, 22);
            this.totalCtasField.TabIndex = 32;
            // 
            // labelX3
            // 
            this.labelX3.AutoSize = true;
            this.labelX3.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.labelX3.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX3.ForeColor = System.Drawing.Color.Black;
            this.labelX3.Location = new System.Drawing.Point(288, 6);
            this.labelX3.Name = "labelX3";
            this.labelX3.Size = new System.Drawing.Size(22, 15);
            this.labelX3.TabIndex = 29;
            this.labelX3.Text = "Año";
            // 
            // anoField
            // 
            this.anoField.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.anoField.Border.Class = "TextBoxBorder";
            this.anoField.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.anoField.ForeColor = System.Drawing.Color.Black;
            this.anoField.Location = new System.Drawing.Point(314, 3);
            this.anoField.Name = "anoField";
            this.anoField.Size = new System.Drawing.Size(48, 22);
            this.anoField.TabIndex = 30;
            // 
            // labelX2
            // 
            this.labelX2.AutoSize = true;
            this.labelX2.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.labelX2.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX2.ForeColor = System.Drawing.Color.Black;
            this.labelX2.Location = new System.Drawing.Point(209, 6);
            this.labelX2.Name = "labelX2";
            this.labelX2.Size = new System.Drawing.Size(23, 15);
            this.labelX2.TabIndex = 28;
            this.labelX2.Text = "Mes";
            // 
            // labelX1
            // 
            this.labelX1.AutoSize = true;
            this.labelX1.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.labelX1.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX1.ForeColor = System.Drawing.Color.Black;
            this.labelX1.Location = new System.Drawing.Point(48, 6);
            this.labelX1.Name = "labelX1";
            this.labelX1.Size = new System.Drawing.Size(25, 15);
            this.labelX1.TabIndex = 26;
            this.labelX1.Text = "RFC";
            // 
            // rFCField
            // 
            this.rFCField.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.rFCField.Border.Class = "TextBoxBorder";
            this.rFCField.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.rFCField.Enabled = false;
            this.rFCField.ForeColor = System.Drawing.Color.Black;
            this.rFCField.Location = new System.Drawing.Point(77, 3);
            this.rFCField.Name = "rFCField";
            this.rFCField.Size = new System.Drawing.Size(128, 22);
            this.rFCField.TabIndex = 27;
            // 
            // versionField
            // 
            this.versionField.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.versionField.Border.Class = "TextBoxBorder";
            this.versionField.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.versionField.ForeColor = System.Drawing.Color.Black;
            this.versionField.Location = new System.Drawing.Point(0, 3);
            this.versionField.Name = "versionField";
            this.versionField.ReadOnly = true;
            this.versionField.Size = new System.Drawing.Size(44, 22);
            this.versionField.TabIndex = 25;
            this.versionField.Text = "1.1";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.label1.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.label1.ForeColor = System.Drawing.Color.Black;
            this.label1.Location = new System.Drawing.Point(-42, 70);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(40, 15);
            this.label1.TabIndex = 24;
            this.label1.Text = "Versión";
            // 
            // ucDeclaracion
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.ESTADO);
            this.Controls.Add(this.labelX5);
            this.Controls.Add(this.mesField);
            this.Controls.Add(this.labelX4);
            this.Controls.Add(this.totalCtasField);
            this.Controls.Add(this.labelX3);
            this.Controls.Add(this.anoField);
            this.Controls.Add(this.labelX2);
            this.Controls.Add(this.labelX1);
            this.Controls.Add(this.rFCField);
            this.Controls.Add(this.versionField);
            this.Controls.Add(this.label1);
            this.Name = "ucDeclaracion";
            this.Size = new System.Drawing.Size(682, 28);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevComponents.DotNetBar.Controls.ComboBoxEx ESTADO;
        private DevComponents.Editors.ComboItem comboItem3;
        private DevComponents.Editors.ComboItem comboItem4;
        private DevComponents.DotNetBar.LabelX labelX5;
        private DevComponents.DotNetBar.Controls.ComboBoxEx mesField;
        private DevComponents.Editors.ComboItem comboItem1;
        private DevComponents.Editors.ComboItem comboItem2;
        private DevComponents.Editors.ComboItem comboItem5;
        private DevComponents.Editors.ComboItem comboItem6;
        private DevComponents.Editors.ComboItem comboItem7;
        private DevComponents.Editors.ComboItem comboItem8;
        private DevComponents.Editors.ComboItem comboItem9;
        private DevComponents.Editors.ComboItem comboItem10;
        private DevComponents.Editors.ComboItem comboItem11;
        private DevComponents.Editors.ComboItem comboItem12;
        private DevComponents.Editors.ComboItem comboItem13;
        private DevComponents.Editors.ComboItem comboItem14;
        private DevComponents.DotNetBar.LabelX labelX4;
        private DevComponents.DotNetBar.Controls.TextBoxX totalCtasField;
        private DevComponents.DotNetBar.LabelX labelX3;
        private DevComponents.DotNetBar.Controls.TextBoxX anoField;
        private DevComponents.DotNetBar.LabelX labelX2;
        private DevComponents.DotNetBar.LabelX labelX1;
        private DevComponents.DotNetBar.Controls.TextBoxX rFCField;
        private DevComponents.DotNetBar.Controls.TextBoxX versionField;
        private DevComponents.DotNetBar.LabelX label1;
    }
}
