﻿//namespace Desarrollo
//{

//    //------------------------------------------------------------------------------
//    // <auto-generated>
//    //     Este código fue generado por una herramienta.
//    //     Versión del motor en tiempo de ejecución:2.0.50727.5477
//    //
//    //     Los cambios en este archivo podrían causar un comportamiento incorrecto y se perderán si
//    //     se vuelve a generar el código.
//    // </auto-generated>
//    //------------------------------------------------------------------------------

//    using System.Xml.Serialization;

//    // 
//    // This source code was auto-generated by xsd, Version=2.0.50727.3038.
//    // 


//    /// <comentarios/>
//    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.3038")]
//    [System.SerializableAttribute()]
//    [System.Diagnostics.DebuggerStepThroughAttribute()]
//    [System.ComponentModel.DesignerCategoryAttribute("code")]
//    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.sat.gob.mx/polizas")]
//    [System.Xml.Serialization.XmlRootAttribute(Namespace = "http://www.sat.gob.mx/polizas", IsNullable = false)]
//    public partial class Polizas
//    {

//        private PolizasPoliza[] polizaField;

//        private string versionField;

//        private string rFCField;

//        private PolizasMes mesField;

//        private int anoField;

//        public Polizas()
//        {
//            this.versionField = "1.0";
//        }

//        /// <comentarios/>
//        [System.Xml.Serialization.XmlElementAttribute("Poliza")]
//        public PolizasPoliza[] Poliza
//        {
//            get
//            {
//                return this.polizaField;
//            }
//            set
//            {
//                this.polizaField = value;
//            }
//        }

//        /// <comentarios/>
//        [System.Xml.Serialization.XmlAttributeAttribute()]
//        public string Version
//        {
//            get
//            {
//                return this.versionField;
//            }
//            set
//            {
//                this.versionField = value;
//            }
//        }

//        /// <comentarios/>
//        [System.Xml.Serialization.XmlAttributeAttribute()]
//        public string RFC
//        {
//            get
//            {
//                return this.rFCField;
//            }
//            set
//            {
//                this.rFCField = value;
//            }
//        }

//        /// <comentarios/>
//        [System.Xml.Serialization.XmlAttributeAttribute()]
//        public PolizasMes Mes
//        {
//            get
//            {
//                return this.mesField;
//            }
//            set
//            {
//                this.mesField = value;
//            }
//        }

//        /// <comentarios/>
//        [System.Xml.Serialization.XmlAttributeAttribute()]
//        public int Ano
//        {
//            get
//            {
//                return this.anoField;
//            }
//            set
//            {
//                this.anoField = value;
//            }
//        }
//    }

//    /// <comentarios/>
//    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.3038")]
//    [System.SerializableAttribute()]
//    [System.Diagnostics.DebuggerStepThroughAttribute()]
//    [System.ComponentModel.DesignerCategoryAttribute("code")]
//    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.sat.gob.mx/polizas")]
//    public partial class PolizasPoliza
//    {

//        private PolizasPolizaTransaccion[] transaccionField;

//        private int tipoField;

//        private string numField;

//        private System.DateTime fechaField;

//        private string conceptoField;

//        /// <comentarios/>
//        [System.Xml.Serialization.XmlElementAttribute("Transaccion")]
//        public PolizasPolizaTransaccion[] Transaccion
//        {
//            get
//            {
//                return this.transaccionField;
//            }
//            set
//            {
//                this.transaccionField = value;
//            }
//        }

//        /// <comentarios/>
//        [System.Xml.Serialization.XmlAttributeAttribute()]
//        public int Tipo
//        {
//            get
//            {
//                return this.tipoField;
//            }
//            set
//            {
//                this.tipoField = value;
//            }
//        }

//        /// <comentarios/>
//        [System.Xml.Serialization.XmlAttributeAttribute()]
//        public string Num
//        {
//            get
//            {
//                return this.numField;
//            }
//            set
//            {
//                this.numField = value;
//            }
//        }

//        /// <comentarios/>
//        [System.Xml.Serialization.XmlAttributeAttribute(DataType = "date")]
//        public System.DateTime Fecha
//        {
//            get
//            {
//                return this.fechaField;
//            }
//            set
//            {
//                this.fechaField = value;
//            }
//        }

//        /// <comentarios/>
//        [System.Xml.Serialization.XmlAttributeAttribute()]
//        public string Concepto
//        {
//            get
//            {
//                return this.conceptoField;
//            }
//            set
//            {
//                this.conceptoField = value;
//            }
//        }
//    }

//    /// <comentarios/>
//    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.3038")]
//    [System.SerializableAttribute()]
//    [System.Diagnostics.DebuggerStepThroughAttribute()]
//    [System.ComponentModel.DesignerCategoryAttribute("code")]
//    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.sat.gob.mx/polizas")]
//    public partial class PolizasPolizaTransaccion
//    {

//        public PolizasPolizaTransaccionCheque[] chequeField;

//        public PolizasPolizaTransaccionTransferencia[] transferenciaField;

//        public PolizasPolizaTransaccionComprobantes[] comprobantesField;

//        public string numCtaField;

//        public string conceptoField;

//        public decimal debeField;

//        public decimal haberField;

//        public string monedaField;

//        public decimal tipCambField;

//        public bool tipCambFieldSpecified;

//        /// <comentarios/>
//        [System.Xml.Serialization.XmlElementAttribute("Cheque")]
//        public PolizasPolizaTransaccionCheque[] Cheque
//        {
//            get
//            {
//                return this.chequeField;
//            }
//            set
//            {
//                this.chequeField = value;
//            }
//        }

//        /// <comentarios/>
//        [System.Xml.Serialization.XmlElementAttribute("Transferencia")]
//        public PolizasPolizaTransaccionTransferencia[] Transferencia
//        {
//            get
//            {
//                return this.transferenciaField;
//            }
//            set
//            {
//                this.transferenciaField = value;
//            }
//        }

//        /// <comentarios/>
//        [System.Xml.Serialization.XmlElementAttribute("Comprobantes")]
//        public PolizasPolizaTransaccionComprobantes[] Comprobantes
//        {
//            get
//            {
//                return this.comprobantesField;
//            }
//            set
//            {
//                this.comprobantesField = value;
//            }
//        }

//        /// <comentarios/>
//        [System.Xml.Serialization.XmlAttributeAttribute()]
//        public string NumCta
//        {
//            get
//            {
//                return this.numCtaField;
//            }
//            set
//            {
//                this.numCtaField = value;
//            }
//        }

//        /// <comentarios/>
//        [System.Xml.Serialization.XmlAttributeAttribute()]
//        public string Concepto
//        {
//            get
//            {
//                return this.conceptoField;
//            }
//            set
//            {
//                this.conceptoField = value;
//            }
//        }

//        /// <comentarios/>
//        [System.Xml.Serialization.XmlAttributeAttribute()]
//        public decimal Debe
//        {
//            get
//            {
//                return this.debeField;
//            }
//            set
//            {
//                this.debeField = value;
//            }
//        }

//        /// <comentarios/>
//        [System.Xml.Serialization.XmlAttributeAttribute()]
//        public decimal Haber
//        {
//            get
//            {
//                return this.haberField;
//            }
//            set
//            {
//                this.haberField = value;
//            }
//        }

//        /// <comentarios/>
//        [System.Xml.Serialization.XmlAttributeAttribute()]
//        public string Moneda
//        {
//            get
//            {
//                return this.monedaField;
//            }
//            set
//            {
//                this.monedaField = value;
//            }
//        }

//        /// <comentarios/>
//        [System.Xml.Serialization.XmlAttributeAttribute()]
//        public decimal TipCamb
//        {
//            get
//            {
//                return this.tipCambField;
//            }
//            set
//            {
//                this.tipCambField = value;
//            }
//        }

//        /// <comentarios/>
//        [System.Xml.Serialization.XmlIgnoreAttribute()]
//        public bool TipCambSpecified
//        {
//            get
//            {
//                return this.tipCambFieldSpecified;
//            }
//            set
//            {
//                this.tipCambFieldSpecified = value;
//            }
//        }
//    }

//    /// <comentarios/>
//    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.3038")]
//    [System.SerializableAttribute()]
//    [System.Diagnostics.DebuggerStepThroughAttribute()]
//    [System.ComponentModel.DesignerCategoryAttribute("code")]
//    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.sat.gob.mx/polizas")]
//    public partial class PolizasPolizaTransaccionCheque
//    {

//        private string numField;

//        private string bancoField;

//        private string ctaOriField;

//        private System.DateTime fechaField;

//        private decimal montoField;

//        private string benefField;

//        private string rFCField;

//        /// <comentarios/>
//        [System.Xml.Serialization.XmlAttributeAttribute()]
//        public string Num
//        {
//            get
//            {
//                return this.numField;
//            }
//            set
//            {
//                this.numField = value;
//            }
//        }

//        /// <comentarios/>
//        [System.Xml.Serialization.XmlAttributeAttribute()]
//        public string Banco
//        {
//            get
//            {
//                return this.bancoField;
//            }
//            set
//            {
//                this.bancoField = value;
//            }
//        }

//        /// <comentarios/>
//        [System.Xml.Serialization.XmlAttributeAttribute()]
//        public string CtaOri
//        {
//            get
//            {
//                return this.ctaOriField;
//            }
//            set
//            {
//                this.ctaOriField = value;
//            }
//        }

//        /// <comentarios/>
//        [System.Xml.Serialization.XmlAttributeAttribute(DataType = "date")]
//        public System.DateTime Fecha
//        {
//            get
//            {
//                return this.fechaField;
//            }
//            set
//            {
//                this.fechaField = value;
//            }
//        }

//        /// <comentarios/>
//        [System.Xml.Serialization.XmlAttributeAttribute()]
//        public decimal Monto
//        {
//            get
//            {
//                return this.montoField;
//            }
//            set
//            {
//                this.montoField = value;
//            }
//        }

//        /// <comentarios/>
//        [System.Xml.Serialization.XmlAttributeAttribute()]
//        public string Benef
//        {
//            get
//            {
//                return this.benefField;
//            }
//            set
//            {
//                this.benefField = value;
//            }
//        }

//        /// <comentarios/>
//        [System.Xml.Serialization.XmlAttributeAttribute()]
//        public string RFC
//        {
//            get
//            {
//                return this.rFCField;
//            }
//            set
//            {
//                this.rFCField = value;
//            }
//        }
//    }

//    /// <comentarios/>
//    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.3038")]
//    [System.SerializableAttribute()]
//    [System.Diagnostics.DebuggerStepThroughAttribute()]
//    [System.ComponentModel.DesignerCategoryAttribute("code")]
//    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.sat.gob.mx/polizas")]
//    public partial class PolizasPolizaTransaccionTransferencia
//    {

//        private string ctaOriField;

//        private string bancoOriField;

//        private decimal montoField;

//        private string ctaDestField;

//        private string bancoDestField;

//        private System.DateTime fechaField;

//        private string benefField;

//        private string rFCField;

//        /// <comentarios/>
//        [System.Xml.Serialization.XmlAttributeAttribute()]
//        public string CtaOri
//        {
//            get
//            {
//                return this.ctaOriField;
//            }
//            set
//            {
//                this.ctaOriField = value;
//            }
//        }

//        /// <comentarios/>
//        [System.Xml.Serialization.XmlAttributeAttribute()]
//        public string BancoOri
//        {
//            get
//            {
//                return this.bancoOriField;
//            }
//            set
//            {
//                this.bancoOriField = value;
//            }
//        }

//        /// <comentarios/>
//        [System.Xml.Serialization.XmlAttributeAttribute()]
//        public decimal Monto
//        {
//            get
//            {
//                return this.montoField;
//            }
//            set
//            {
//                this.montoField = value;
//            }
//        }

//        /// <comentarios/>
//        [System.Xml.Serialization.XmlAttributeAttribute()]
//        public string CtaDest
//        {
//            get
//            {
//                return this.ctaDestField;
//            }
//            set
//            {
//                this.ctaDestField = value;
//            }
//        }

//        /// <comentarios/>
//        [System.Xml.Serialization.XmlAttributeAttribute()]
//        public string BancoDest
//        {
//            get
//            {
//                return this.bancoDestField;
//            }
//            set
//            {
//                this.bancoDestField = value;
//            }
//        }

//        /// <comentarios/>
//        [System.Xml.Serialization.XmlAttributeAttribute(DataType = "date")]
//        public System.DateTime Fecha
//        {
//            get
//            {
//                return this.fechaField;
//            }
//            set
//            {
//                this.fechaField = value;
//            }
//        }

//        /// <comentarios/>
//        [System.Xml.Serialization.XmlAttributeAttribute()]
//        public string Benef
//        {
//            get
//            {
//                return this.benefField;
//            }
//            set
//            {
//                this.benefField = value;
//            }
//        }

//        /// <comentarios/>
//        [System.Xml.Serialization.XmlAttributeAttribute()]
//        public string RFC
//        {
//            get
//            {
//                return this.rFCField;
//            }
//            set
//            {
//                this.rFCField = value;
//            }
//        }
//    }

//    /// <comentarios/>
//    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.3038")]
//    [System.SerializableAttribute()]
//    [System.Diagnostics.DebuggerStepThroughAttribute()]
//    [System.ComponentModel.DesignerCategoryAttribute("code")]
//    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.sat.gob.mx/polizas")]
//    public partial class PolizasPolizaTransaccionComprobantes
//    {

//        private string uUID_CFDIField;

//        private decimal montoField;

//        private string rFCField;

//        /// <comentarios/>
//        [System.Xml.Serialization.XmlAttributeAttribute()]
//        public string UUID_CFDI
//        {
//            get
//            {
//                return this.uUID_CFDIField;
//            }
//            set
//            {
//                this.uUID_CFDIField = value;
//            }
//        }

//        /// <comentarios/>
//        [System.Xml.Serialization.XmlAttributeAttribute()]
//        public decimal Monto
//        {
//            get
//            {
//                return this.montoField;
//            }
//            set
//            {
//                this.montoField = value;
//            }
//        }

//        /// <comentarios/>
//        [System.Xml.Serialization.XmlAttributeAttribute()]
//        public string RFC
//        {
//            get
//            {
//                return this.rFCField;
//            }
//            set
//            {
//                this.rFCField = value;
//            }
//        }
//    }

//    /// <comentarios/>
//    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.3038")]
//    [System.SerializableAttribute()]
//    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.sat.gob.mx/polizas")]
//    public enum PolizasMes
//    {

//        /// <comentarios/>
//        [System.Xml.Serialization.XmlEnumAttribute("01")]
//        Item01,

//        /// <comentarios/>
//        [System.Xml.Serialization.XmlEnumAttribute("02")]
//        Item02,

//        /// <comentarios/>
//        [System.Xml.Serialization.XmlEnumAttribute("03")]
//        Item03,

//        /// <comentarios/>
//        [System.Xml.Serialization.XmlEnumAttribute("04")]
//        Item04,

//        /// <comentarios/>
//        [System.Xml.Serialization.XmlEnumAttribute("05")]
//        Item05,

//        /// <comentarios/>
//        [System.Xml.Serialization.XmlEnumAttribute("06")]
//        Item06,

//        /// <comentarios/>
//        [System.Xml.Serialization.XmlEnumAttribute("07")]
//        Item07,

//        /// <comentarios/>
//        [System.Xml.Serialization.XmlEnumAttribute("08")]
//        Item08,

//        /// <comentarios/>
//        [System.Xml.Serialization.XmlEnumAttribute("09")]
//        Item09,

//        /// <comentarios/>
//        [System.Xml.Serialization.XmlEnumAttribute("10")]
//        Item10,

//        /// <comentarios/>
//        [System.Xml.Serialization.XmlEnumAttribute("11")]
//        Item11,

//        /// <comentarios/>
//        [System.Xml.Serialization.XmlEnumAttribute("12")]
//        Item12,
//    }
//}
