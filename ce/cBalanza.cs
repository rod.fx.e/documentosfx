﻿using CryptoSysPKI;
using DevComponents.DotNetBar;
using Ionic.Zip;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Windows.Forms;
using System.Xml;
using System.Xml.Serialization;
using System.Xml.XPath;
using System.Xml.Xsl;
using Generales;
using ModeloDocumentosFX;

namespace Desarrollo
{
    class cBalanza
    {
        private string sSQL = "";
        public string ROW_ID { get; set; }

        public string ROW_ID_CATALOGO { get; set; }

        public string numCtaField { get; set; }
        public double saldoIniField { get; set; }
        public double debeField { get; set; }
        public double haberField { get; set; }
        public double saldoFinField { get; set; }

        private cCONEXCION oData = new cCONEXCION();
        public override string ToString() { return ROW_ID; }

        public cBalanza()
        {
            ModeloDocumentosFXContainer dbContext = new ModeloDocumentosFXContainer();
            try
            {
                string conection = dbContext.Database.Connection.ConnectionString;
                oData = new cCONEXCION(conection);
            }
            catch (Exception err)
            {
                ErrorFX.mostrar(err, false, true, "frmEmpresa - 96 - ", false);
            }
            limpiar();
        }

        public void limpiar()
        {
            ROW_ID = "";
            ROW_ID_CATALOGO = "";
            numCtaField="";
            saldoIniField=0;
            debeField = 0;
            haberField = 0;
            saldoFinField = 0;
        }

        public string validar()
        {
            //Existe la cuenta en el Catalogo
            if(numCtaField!="")
            {
                //Validar cuenta
                cCatalogoCtas ocCatalogoCtas = new cCatalogoCtas();
                if (!ocCatalogoCtas.existe(numCtaField, ROW_ID_CATALOGO))
                {
                    return "B1-La cuenta " + numCtaField + " no existe en el catalogo de cuentas.";
                }
                else
                {
                    //Verificar que no tiene código agrupador
                    if (ocCatalogoCtas.codAgrupField == "")
                    {
                        return "B2-La cuenta " + numCtaField + " existe en el catalogo de cuentas pero no tiene agrupador.";
                    }
                }
            }
            double resultado=(saldoIniField+debeField-haberField)-saldoFinField;

            if (Math.Truncate(resultado)!=0)
            {
                return "B2-La balanza de la cuenta " + numCtaField + " esta incorrecta.";
            }
            
            return "";
        }

        public List<cBalanza> todos(string ROW_ID_CATALOGOp,string numCtaFieldp = "",bool solo_con_movimientos=false)
        {

            List<cBalanza> lista = new List<cBalanza>();

            sSQL = " SELECT * ";
            sSQL += " FROM Balanza ";
            sSQL += " WHERE 1=1 AND ROW_ID_CATALOGO=" + ROW_ID_CATALOGOp  + "";
            sSQL += " AND numCtaField LIKE '%" + numCtaFieldp + "%' ";
            if (solo_con_movimientos)
            {
                sSQL += " AND debeField<>0 AND haberField<>0  ";
            }
            sSQL += " ORDER BY numCtaField";
            DataTable oDataTable = oData.EjecutarConsulta(sSQL);
            foreach (DataRow oDataRow in oDataTable.Rows)
            {
                cBalanza ocCatalogo = new cBalanza();
                ocCatalogo.cargar(oDataRow["ROW_ID"].ToString());
                lista.Add(ocCatalogo);
            }
            return lista;

        }

        public bool verificar(string numCtaFieldp, string ROW_ID_CATALOGOp, bool cargar_ROW_ID)
        {
            sSQL = " SELECT * ";
            sSQL += " FROM Balanza ";
            sSQL += " WHERE numCtaField='" + numCtaFieldp + "' ";
            sSQL += " AND ROW_ID_CATALOGO=" + ROW_ID_CATALOGOp + "";
            DataTable oDataTable = oData.EjecutarConsulta(sSQL);
            if (oDataTable != null)
            {
                foreach (DataRow oDataRow in oDataTable.Rows)
                {
                    if (cargar_ROW_ID)
                    {
                        ROW_ID = oDataRow["ROW_ID"].ToString();
                    }
                    return true;
                }
            }
            return false;
        }

        public bool cargar(string ROW_IDp)
        {
            sSQL = " SELECT * ";
            sSQL += " FROM Balanza ";
            sSQL += " WHERE ROW_ID=" + ROW_IDp + " ";
            DataTable oDataTable = oData.EjecutarConsulta(sSQL);
            if (oDataTable != null)
            {
                foreach (DataRow oDataRow in oDataTable.Rows)
                {
                    //Añadir el Registro
                    ROW_ID = oDataRow["ROW_ID"].ToString();
                    ROW_ID_CATALOGO = oDataRow["ROW_ID_CATALOGO"].ToString();
                    numCtaField = oDataRow["numCtaField"].ToString();
                    SUMMARY_ACCOUNT = oDataRow["SUMMARY_ACCOUNT"].ToString();
                    saldoIniField = double.Parse(oDataRow["saldoIniField"].ToString());
                    debeField = double.Parse(oDataRow["debeField"].ToString());
                    haberField = double.Parse(oDataRow["haberField"].ToString());
                    saldoFinField = double.Parse(oDataRow["saldoFinField"].ToString());
                    
                    return true;
                }
            }
            return false;
        }

        public bool guardar(bool validar=true)
        {
            if (validar)
            {

                if (numCtaField.Trim() == "")
                {
                    MessageBoxEx.Show("La cuenta es requerida.", Application.ProductName + "-" + Application.ProductVersion, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                    return false;
                }
                
            }
            if ((Math.Abs(saldoIniField)
                + Math.Abs(haberField)
                + Math.Abs(saldoFinField)
                + Math.Abs(debeField))==0)
            {
                return false;
            }
 

            if (verificar(numCtaField, ROW_ID_CATALOGO,true))
            {
                
            }



            if (ROW_ID == "")
            {
                sSQL = " INSERT INTO Balanza ";
                sSQL += " ( ";
                sSQL += " [ROW_ID_CATALOGO],[numCtaField],[saldoIniField]";
                sSQL += ",[haberField],[saldoFinField],[debeField] ";
                sSQL += ",SUMMARY_ACCOUNT";
                sSQL += " )";
                sSQL += " VALUES ";
                sSQL += "(";
                sSQL += " " + ROW_ID_CATALOGO + ",'" + numCtaField + "'," + saldoIniField.ToString() + "";
                sSQL += "," + haberField.ToString() + "," + saldoFinField.ToString() + "," + debeField.ToString() + " ";
                sSQL += ",'" + SUMMARY_ACCOUNT + "'";
                sSQL += ")";
                DataTable oDataTable = oData.EjecutarConsulta(sSQL);
                if (oDataTable != null)
                {

                    sSQL = "SELECT TOP 1 MAX(ROW_ID) as ROW_ID FROM Balanza";
                    oDataTable = oData.EjecutarConsulta(sSQL);
                    if (oDataTable != null)
                    {
                        foreach (DataRow oDataRow in oDataTable.Rows)
                        {
                            ROW_ID = oDataRow["ROW_ID"].ToString();
                        }
                    }
                }

            }
            else
            {
                sSQL = " UPDATE Balanza ";
                sSQL += " SET ROW_ID_CATALOGO=" + ROW_ID_CATALOGO + ",numCtaField='" + numCtaField + "',saldoIniField=saldoIniField+" + saldoIniField.ToString() + "";
                sSQL += ",haberField=haberField+" + haberField.ToString() + ",saldoFinField=saldoFinField+" + saldoFinField.ToString() + ",debeField=debeField+" + debeField.ToString() + " ";
                sSQL += ",SUMMARY_ACCOUNT='" + SUMMARY_ACCOUNT + "'";
                sSQL += " WHERE ROW_ID=" + ROW_ID;
                oData.EjecutarConsulta(sSQL);

            }
            return true;

        }
        public bool generar_archivo(cCatalogo oCatalogop, string pathp, bool mostrar_mensaje_finalizacion = true)
        {
            try
            {
                FolderBrowserDialog folderBrowserDialog1 = new FolderBrowserDialog();

                string path = pathp;
                if (path.Trim() == "")
                {
                    if (folderBrowserDialog1.ShowDialog() == DialogResult.OK)
                    {
                        path = folderBrowserDialog1.SelectedPath;

                    }
                    else
                    {
                        MessageBoxEx.Show("Generación de archivo cancelada.", Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
                        return false;
                    }
                }

                string mes = oCatalogop.mesField;
                if (mes.Length == 1)
                {
                    mes = "0" + mes;
                }
                frmBalanza_Comprobacion ofrmBalanza_Comprobacion = new frmBalanza_Comprobacion();
                if (ofrmBalanza_Comprobacion.ShowDialog() == DialogResult.Cancel)
                {
                    MessageBoxEx.Show("Generación cancelada.", Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
                    return false;
                }


                string nombre = oCatalogop.rFCField + oCatalogop.anoField + mes + ofrmBalanza_Comprobacion.tipo;

                string nombre_tmp = path + "\\" + "" + nombre + ".xml";



                Balanza oBalanza = new Balanza();
                cBalanza ocBalanza = new cBalanza();
                oBalanza.Anio = int.Parse(oCatalogop.anoField);
                oBalanza.TipoEnvio = "N";
                if (ofrmBalanza_Comprobacion.tipo == "BC")
                {
                    oBalanza.FechaModBalSpecified = true;
                    oBalanza.FechaModBal = ofrmBalanza_Comprobacion.fechaFechaModBal;
                    oBalanza.TipoEnvio = "C";
                }


                switch (int.Parse(oCatalogop.mesField))
                {
                    case 1:
                        oBalanza.Mes = BalanzaMes.Item01;
                        break;
                    case 2:
                        oBalanza.Mes = BalanzaMes.Item02;
                        break;
                    case 3:
                        oBalanza.Mes = BalanzaMes.Item03;
                        break;
                    case 4:
                        oBalanza.Mes = BalanzaMes.Item04;
                        break;
                    case 5:
                        oBalanza.Mes = BalanzaMes.Item05;
                        break;
                    case 6:
                        oBalanza.Mes = BalanzaMes.Item06;
                        break;
                    case 7:
                        oBalanza.Mes = BalanzaMes.Item07;
                        break;
                    case 8:
                        oBalanza.Mes = BalanzaMes.Item08;
                        break;
                    case 9:
                        oBalanza.Mes = BalanzaMes.Item09;
                        break;
                    case 10:
                        oBalanza.Mes = BalanzaMes.Item10;
                        break;
                    case 11:
                        oBalanza.Mes = BalanzaMes.Item11;
                        break;
                    case 12:
                        oBalanza.Mes = BalanzaMes.Item12;
                        break;
                    case 13:
                        oBalanza.Mes = BalanzaMes.Item13;
                        break;
                }
                oBalanza.RFC = oCatalogop.rFCField;
                cEMPRESA_CE ocEMPRESA = new cEMPRESA_CE();
                ocEMPRESA.cargar_rfc(oBalanza.RFC);
                if (ocEMPRESA.NO_CERTIFICADO.Trim().Length == 0)
                {
                    MessageBoxEx.Show("No se tiene configurado el Certificado"
                       , Application.ProductName + "-" + Application.ProductVersion, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);

                    return false;
                }
                oBalanza.noCertificado = ocEMPRESA.NO_CERTIFICADO;
                if (!File.Exists(ocEMPRESA.CERTIFICADO))
                {
                    MessageBoxEx.Show("No se tiene acceso al Certificado, archivo " + ocEMPRESA.CERTIFICADO + ""
                        , Application.ProductName + "-" + Application.ProductVersion, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);

                    return false;
                }

                X509Certificate cert = X509Certificate.CreateFromCertFile(ocEMPRESA.CERTIFICADO);
                string Certificado64 = System.Convert.ToBase64String(cert.GetRawCertData());
                oBalanza.Certificado = Certificado64;

                BalanzaCtas[] oCtas_b;
                List<cBalanza> listacBalanza = new List<cBalanza>();
                listacBalanza = ocBalanza.todos(oCatalogop.ROW_ID);
                oCtas_b = new BalanzaCtas[listacBalanza.Count];
                int contador_b = 0;
                foreach (cBalanza registro in listacBalanza)
                {
                    oCtas_b[contador_b] = new BalanzaCtas();
                    oCtas_b[contador_b].NumCta = registro.numCtaField;
                    oCtas_b[contador_b].SaldoIni = decimal.Parse(registro.saldoIniField.ToString());
                    oCtas_b[contador_b].Debe = decimal.Parse(registro.debeField.ToString());
                    oCtas_b[contador_b].Haber = decimal.Parse(registro.haberField.ToString());
                    oCtas_b[contador_b].SaldoFin = decimal.Parse(registro.saldoFinField.ToString());
                    contador_b++;
                }
                oBalanza.Ctas = oCtas_b;

                crear_archivo(nombre, oBalanza);

                oBalanza.Sello = sellar(nombre + ".XML", ocEMPRESA);

                if (oBalanza.Sello == "")
                {
                    MessageBoxEx.Show("El sello tiene error de generación.", Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return false;
                }
                crear_archivo(nombre, oBalanza, true);

                //Comprimir
                string ARCHIVO_zip = nombre + ".ZIP";
                using (ZipFile zip = new ZipFile())
                {
                    zip.AddFile(nombre + ".XML");
                    zip.Save(ARCHIVO_zip);
                }
                File.Delete(nombre + ".XML");
                File.Delete(path + "\\" + ARCHIVO_zip);
                File.Copy(ARCHIVO_zip, path + "\\" + ARCHIVO_zip);
                File.Delete(ARCHIVO_zip);
                //Fin de Compresion de Archivo

                if (mostrar_mensaje_finalizacion)
                {
                    MessageBoxEx.Show("Exportación de los archivos finalizada.", Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Information);
                    return true;
                }
            }
            catch (Exception e)
            {
                MessageBoxEx.Show(e.Message.ToString(), Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
            return false;

        }

        private void crear_archivo(string nombre, Balanza oBalanza, bool prefijos = false)
        {
            //Transformar al XML
            XmlSerializerNamespaces myNamespaces = new XmlSerializerNamespaces();

            myNamespaces.Add("schemaLocation", "http://www.sat.gob.mx/esquemas/ContabilidadE/1_3/BalanzaComprobacion http://www.sat.gob.mx/esquemas/ContabilidadE/1_3/BalanzaComprobacion/BalanzaComprobacion_1_3.xsd");
            myNamespaces.Add("xsi", "http://www.w3.org/2001/XMLSchema-instance");
            myNamespaces.Add("BCE", "http://www.sat.gob.mx/esquemas/ContabilidadE/1_3/BalanzaComprobacion");
            //myNamespaces.Add("cambiar", "www.sat.gob.mx/esquemas/ContabilidadE/1_3/BalanzaComprobacion http://www.sat.gob.mx/esquemas/ContabilidadE/1_3/BalanzaComprobacion/BalanzaComprobacion_1_3.xsd");
            
            TextWriter writer_b = new StreamWriter(nombre + ".XML");
            XmlSerializer serializer_b = new XmlSerializer(typeof(Balanza));
            serializer_b.Serialize(writer_b, oBalanza, myNamespaces);
            writer_b.Close();
            if (prefijos)
            {
                oData.ReplaceInFile(nombre + ".XML", "xmlns:cambiar", "xsi:schemaLocation");
                oData.ReplaceInFile(nombre + ".XML", "xmlns:schemaLocation", "xsi:schemaLocation");
                oData.ReplaceInFile(nombre + ".XML", "<Balanza", "<BCE:Balanza");
                oData.ReplaceInFile(nombre + ".XML", "</Balanza", "</BCE:Balanza");
                oData.ReplaceInFile(nombre + ".XML", "<Ctas", "<BCE:Ctas");
            }
        }
        private string sellar(string archivo, cEMPRESA_CE oEMPRESAp)
        {
            string sDirectory = AppDomain.CurrentDomain.BaseDirectory;
            XPathDocument myXPathDoc = new XPathDocument(archivo);
            //XslTransform myXslTrans = new XslTransform();
            XslCompiledTransform myXslTrans = new XslCompiledTransform();


            XslCompiledTransform trans = new XslCompiledTransform();
            XmlUrlResolver resolver = new XmlUrlResolver();

            resolver.Credentials = System.Net.CredentialCache.DefaultCredentials;
            XsltSettings settings = new XsltSettings(true, true);

            //Cargar xslt
            try
            {

                myXslTrans.Load(sDirectory + "/xslt/BalanzaComprobacion_1_2.xslt");
            }
            catch (XmlException errores)
            {
                MessageBoxEx.Show("Error en " + errores.InnerException.ToString());
                return "";
            }
            //Crear Archivo Temporal
            string nombre_tmp = "TMP" + DateTime.Now.ToString("ddMMyyyyhhmmss");
            XmlTextWriter myWriter = new XmlTextWriter(nombre_tmp + ".TXT", null);

            //Transformar al XML
            myXslTrans.Transform(myXPathDoc, null, myWriter);

            myWriter.Close();

            //Abrir Archivo TXT
            string cadena_original = "";
            StreamReader re = File.OpenText(nombre_tmp + ".TXT");
            string input = null;
            while ((input = re.ReadLine()) != null)
            {
                cadena_original += input;
            }
            re.Close();
            //Buscar el &amp; en la Cadena y sustituirlo por &
            cadena_original = cadena_original.Replace("&amp;", "&");

            //Eliminar Archivos Temporales
            File.Delete(nombre_tmp + ".TXT");
            File.Delete(nombre_tmp + ".XML");


            StringBuilder sbPassword;
            StringBuilder sbPrivateKey;
            byte[] b;
            byte[] block;
            int keyBytes;
            sbPassword = new StringBuilder(oEMPRESAp.PASSWORD);
            sbPrivateKey = Rsa.ReadEncPrivateKey(oEMPRESAp.LLAVE, sbPassword.ToString());
            keyBytes = Rsa.KeyBytes(sbPrivateKey.ToString());
            b = System.Text.Encoding.UTF8.GetBytes(cadena_original);
            block = Rsa.EncodeMsgForSignature(keyBytes, b, HashAlgorithm.Sha256);
            block = Rsa.RawPrivate(block, sbPrivateKey.ToString());
            string resultado = System.Convert.ToBase64String(block);
            Wipe.String(sbPassword);
            Wipe.String(sbPrivateKey);
            Wipe.Data(block);

            return resultado;
        }

        public bool eliminar(string ROW_IDp)
        {
            if (ROW_IDp != "")
            {
                sSQL = "DELETE FROM Balanza ";
                sSQL += " WHERE ROW_ID='" + ROW_IDp + "' ";
                oData.EjecutarConsulta(sSQL);
                return true;
            }
            return false;
        }

        public string SUMMARY_ACCOUNT { get; set; }
    }
}
