﻿using DevComponents.DotNetBar;
using FE_FX;
using Generales;
using ModeloDocumentosFX;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Xml;
using System.Xml.Serialization;

namespace Desarrollo
{

    class cPolizasPolizaTransaccionCompNal
    {
        private string sSQL = "";
        public string ROW_ID { get; set; }

        public string ROW_ID_TRANSACCION { get; set; }
        public string uUID_CFDIField { get; set; }
        public string rFCField { get; set; }
        public string RFC_RECEPTOR { get; set; }
        public decimal montoTotalField { get; set; }
        public string monedaField { get; set; }
        public decimal tipCambField { get; set; }
        public bool tipCambFieldSpecified { get; set; }

        private cCONEXCION oData = new cCONEXCION();
        public override string ToString() { return ROW_ID; }


        public string TIPO { get; set; }

        public string LLAVE { get; set; }
        public cPolizasPolizaTransaccionCompNal()
        {
            ModeloDocumentosFXContainer dbContext = new ModeloDocumentosFXContainer();
            try
            {
                string conection = dbContext.Database.Connection.ConnectionString;
                oData = new cCONEXCION(conection);
            }
            catch (Exception err)
            {
                ErrorFX.mostrar(err, false, true, "frmEmpresa - 96 - ", false);
            }
            limpiar();
        }

        public void limpiar()
        {
            ROW_ID = "";
            ROW_ID_TRANSACCION = "";
            uUID_CFDIField = "";
            rFCField = "";
            montoTotalField = 0;
            monedaField = "";
            tipCambField = 0;
            tipCambFieldSpecified = false;
            RFC_RECEPTOR = "";
            TIPO = "";
            LLAVE = "";
        }


        public List<cPolizasPolizaTransaccionCompNal> todos(string ROW_ID_TRANSACCIONp, string numCtaFieldp = "")
        {

            List<cPolizasPolizaTransaccionCompNal> lista = new List<cPolizasPolizaTransaccionCompNal>();

            sSQL = " SELECT * ";
            sSQL += " FROM PolizasPolizaTransaccionCompNal ";
            sSQL += " WHERE 1=1 AND ROW_ID_TRANSACCION=" + ROW_ID_TRANSACCIONp + "";
            DataTable oDataTable = oData.EjecutarConsulta(sSQL);
            foreach (DataRow oDataRow in oDataTable.Rows)
            {
                cPolizasPolizaTransaccionCompNal ocCatalogo = new cPolizasPolizaTransaccionCompNal();
                ocCatalogo.cargar(oDataRow["ROW_ID"].ToString());
                lista.Add(ocCatalogo);
            }
            return lista;

        }

        public bool verificar(string numCtaFieldp, string ROW_ID_TRANSACCIONp)
        {
            sSQL = " SELECT * ";
            sSQL += " FROM PolizasPolizaTransaccionCompNal ";
            sSQL += " WHERE numCtaField='" + numCtaFieldp + "' ";
            sSQL += " AND ROW_ID_TRANSACCION=" + ROW_ID_TRANSACCIONp + "";
            DataTable oDataTable = oData.EjecutarConsulta(sSQL);
            if (oDataTable != null)
            {
                foreach (DataRow oDataRow in oDataTable.Rows)
                {
                    return true;
                }
            }
            return false;
        }

        public bool cargar(string ROW_IDp)
        {
            sSQL = " SELECT * ";
            sSQL += " FROM PolizasPolizaTransaccionCompNal ";
            sSQL += " WHERE ROW_ID=" + ROW_IDp + " ";
            DataTable oDataTable = oData.EjecutarConsulta(sSQL);
            if (oDataTable != null)
            {
                foreach (DataRow oDataRow in oDataTable.Rows)
                {
                    //Añadir el Registro
                    ROW_ID = oDataRow["ROW_ID"].ToString();
                    ROW_ID_TRANSACCION = oDataRow["ROW_ID_TRANSACCION"].ToString();
                    RFC_RECEPTOR = oDataRow["RFC_RECEPTOR"].ToString();
                    uUID_CFDIField = oDataRow["uUID_CFDIField"].ToString();
                    rFCField = oDataRow["rFCField"].ToString();
                    montoTotalField = decimal.Parse(oDataRow["montoTotalField"].ToString());
                    monedaField = oDataRow["monedaField"].ToString();
                    tipCambField = decimal.Parse(oDataRow["tipCambField"].ToString());
                    tipCambFieldSpecified = bool.Parse(oDataRow["tipCambFieldSpecified"].ToString());


                    return true;
                }
            }
            return false;
        }

        public bool guardar(bool validar = true, bool mostrar_mensaje=false)
        {
            if (ROW_ID == "")
            {
                //Verificar que el Folio fiscal no se encuentre asignado.
                if (buscar_UUID(uUID_CFDIField, ROW_ID_TRANSACCION))
                {
                    if (mostrar_mensaje)
                    {
                        MessageBoxEx.Show("El folio fiscal: " + uUID_CFDIField + " ya se encuentra en la transacción " + ROW_ID_TRANSACCION
                            , Application.ProductName + "-" + Application.ProductVersion, MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                    return false;
                }
                if (uUID_CFDIField.Trim() == "")
                {
                    return false;
                }
                if (rFCField.Trim() == "")
                {
                    return false;
                }


                sSQL = " INSERT INTO PolizasPolizaTransaccionCompNal ";
                sSQL += " ( ";
                sSQL += " [ROW_ID_TRANSACCION],[uUID_CFDIField],[rFCField],[RFC_RECEPTOR]";
                sSQL += ",[montoTotalField],[monedaField],[tipCambField],[tipCambFieldSpecified]";
                sSQL += ",TIPO,LLAVE,USUARIO";
                sSQL += " )";
                sSQL += " VALUES ";
                sSQL += "(";
                sSQL += " " + ROW_ID_TRANSACCION + ",'" + uUID_CFDIField + "','" + rFCField + "','" + RFC_RECEPTOR + "' ";
                sSQL += ", " + montoTotalField.ToString() + ",'" + monedaField + "','" + tipCambField + "','" + tipCambFieldSpecified + "' ";
                sSQL += ",'" + TIPO + "','" + LLAVE + "','" + USUARIO + "'";
                sSQL += ")";
                DataTable oDataTable = oData.EjecutarConsulta(sSQL);
                if (oDataTable != null)
                {

                    sSQL = "SELECT TOP 1 MAX(ROW_ID) as ROW_ID FROM PolizasPolizaTransaccionCompNal";
                    oDataTable = oData.EjecutarConsulta(sSQL);
                    if (oDataTable != null)
                    {
                        foreach (DataRow oDataRow in oDataTable.Rows)
                        {
                            ROW_ID = oDataRow["ROW_ID"].ToString();
                        }
                    }
                }
                else
                {
                    return false;
                }

            }
            else
            {
                sSQL = " UPDATE PolizasPolizaTransaccionCompNal ";
                sSQL += " SET ROW_ID_TRANSACCION=" + ROW_ID_TRANSACCION + ",RFC_RECEPTOR='" + RFC_RECEPTOR + "',uUID_CFDIField='" + uUID_CFDIField + "',rFCField='" + rFCField + "'";
                sSQL += " ,montoTotalField=" + montoTotalField.ToString() + ",monedaField='" + monedaField + "',tipCambField='" + tipCambField + "',tipCambFieldSpecified='" + tipCambFieldSpecified + "' ";
                sSQL += ",TIPO='" + TIPO + "',LLAVE='" + LLAVE + "'";
                sSQL += " WHERE ROW_ID=" + ROW_ID;
                oData.EjecutarConsulta(sSQL);
            }
            return true;

        }

        private bool buscar_UUID(string uUID_CFDIFieldp, string ROW_ID_TRANSACCIONp)
        {
            sSQL = " SELECT * ";
            sSQL += " FROM PolizasPolizaTransaccionCompNal ";
            sSQL += " WHERE uUID_CFDIField='" + uUID_CFDIFieldp + "' AND ROW_ID_TRANSACCION=" + ROW_ID_TRANSACCIONp + " ";
            DataTable oDataTable = oData.EjecutarConsulta(sSQL);
            if (oDataTable != null)
            {
                foreach (DataRow oDataRow in oDataTable.Rows)
                {
                    return true;
                }
            }
            return false;
        }
        private bool buscar(string TIPOp, string LLAVEp)
        {
            sSQL = " SELECT * ";
            sSQL += " FROM PolizasPolizaTransaccionCompNal ";
            sSQL += " WHERE TIPO='" + TIPOp + "' AND LLAVE='" + LLAVEp + "' ";
            DataTable oDataTable = oData.EjecutarConsulta(sSQL);
            if (oDataTable != null)
            {
                foreach (DataRow oDataRow in oDataTable.Rows)
                {
                    return cargar(oDataRow["ROW_ID"].ToString());
                }
            }
            return false;
        }
        public bool actualizar(string TIPOp, string LLAVEp, string ROW_ID_TRANSACCIONp)
        {
            if (ROW_ID_TRANSACCIONp != "")
            {
                sSQL = " UPDATE  PolizasPolizaTransaccionCompNal ";
                sSQL += " SET ROW_ID_TRANSACCION=" + ROW_ID_TRANSACCIONp + "  ";
                sSQL += " WHERE TIPO LiKE '%" + TIPOp + "%' AND LLAVE LIKE '%" + LLAVEp.Replace(" ", "_") + "%' AND ROW_ID_TRANSACCION=-1";
                oData.EjecutarConsulta(sSQL);
                return true;
            }
            return false;
        }

        public bool eliminar(string ROW_IDp)
        {
            if (ROW_IDp != "")
            {
                sSQL = "DELETE FROM PolizasPolizaTransaccionCompNal ";
                sSQL += " WHERE ROW_ID='" + ROW_IDp + "' ";
                oData.EjecutarConsulta(sSQL);
                return true;
            }
            return false;
        }
        internal bool generar_CFDI(string ROW_ID_TRANSACCIONp, string INVOICE_IDp, cCONEXCION oData_ERPp
            , cEMPRESA_CONEXION ocEMPRESA_CONEXIONp)
        {
            string sSQL;
            if (INVOICE_IDp == "")
            {
                return false;
            }
            //Cargar las conexiones por empresa
            cCONEXCION oData_tmp = ocEMPRESA_CONEXIONp.conectar_auxiliar(ocEMPRESA_CONEXIONp.ROW_ID);
            sSQL = " select * from information_schema.tables where table_name = 'VMX_FE'";
            DataTable oDataTable = oData_tmp.EjecutarConsulta(sSQL);
            if (oDataTable.Rows.Count != 0)
            {
                //Cargar los datos de la bae de datos 
                return cargar_VMX_FE(oData_tmp, INVOICE_IDp, ROW_ID_TRANSACCIONp, oData_ERPp);
            }
            else
            {
                //Cargar desde la base de datos normal
                return cargar_VMX_FE(oData_ERPp, INVOICE_IDp, ROW_ID_TRANSACCIONp, oData_ERPp);
            }
        }

        private bool cargar_VMX_FE(cCONEXCION oData_tmp, string INVOICE_IDp, string ROW_ID_TRANSACCIONp, cCONEXCION oData_ERP)
        {

            sSQL = " SELECT * ";
            sSQL += " FROM VMX_FE ";
            sSQL += " WHERE 1=1 AND INVOICE_ID='" + INVOICE_IDp + "'";

            DataTable oDataTable3 = oData_tmp.EjecutarConsulta(sSQL);
            foreach (DataRow oDataRow3 in oDataTable3.Rows)
            {
                string RFC = "";
                string CURRENCY_ID = "";
                decimal TOTAL_AMOUNT = 0;
                sSQL = " SELECT CUSTOMER.VAT_REGISTRATION, RECEIVABLE.TOTAL_AMOUNT, RECEIVABLE.CURRENCY_ID ";
                sSQL += " FROM  RECEIVABLE INNER JOIN  CUSTOMER ON RECEIVABLE.CUSTOMER_ID = CUSTOMER.ID";
                sSQL += " WHERE RECEIVABLE.INVOICE_ID LIKE '" + oDataRow3["INVOICE_ID"].ToString() + "' ";
                DataTable oDataTable2 = oData_ERP.EjecutarConsulta(sSQL);
                foreach (DataRow oDataRow2 in oDataTable2.Rows)
                {
                    RFC = oDataRow2["VAT_REGISTRATION"].ToString();
                    CURRENCY_ID = oDataRow2["CURRENCY_ID"].ToString();
                    TOTAL_AMOUNT = decimal.Parse(oDataRow2["TOTAL_AMOUNT"].ToString());
                }

                ROW_ID_TRANSACCION = ROW_ID_TRANSACCIONp;
                uUID_CFDIField = oDataRow3["UUID"].ToString();
                montoTotalField = TOTAL_AMOUNT;
                rFCField = RFC;
                cEMPRESA_MONEDA ocEMPRESA_MONEDA = new cEMPRESA_MONEDA();
                monedaField = ocEMPRESA_MONEDA.cambiar_MONEDA(CURRENCY_ID);
                if (guardar(true, false))
                {
                    return true;
                }
            }
            return false;

        }

        internal void vincular(string INVOICE_ID_t, string RFC_EMISOR_t, string UUID_t, decimal montoTotalField_t
            , string TIPOp)
        {
            //Buscar las transacciones que sean de Clientes, Proveedores y Bancos
            //Buscar el API o ARI
            sSQL = " SELECT * FROM PolizasPolizaTransaccion WHERE DOCUMENTO='" + INVOICE_ID_t + "'";
            DataTable oDataTable = oData.EjecutarConsulta(sSQL);
            if (oDataTable != null)
            {
                foreach (DataRow oDataRow in oDataTable.Rows)
                {
                    ROW_ID_TRANSACCION = oDataRow["ROW_ID"].ToString();
                    rFCField = RFC_EMISOR_t;
                    uUID_CFDIField = UUID_t;
                    montoTotalField = montoTotalField_t;
                    TIPO = oDataRow["TIPO"].ToString();
                    LLAVE = oDataRow["DOCUMENTO"].ToString();
                    guardar();
                }
            }
        }


        internal bool generar_CFDI_PROVEEDORES(string ROW_ID_TRANSACCIONp, string INVOICE_IDp, string RFC_EMISORp
            , cEMPRESA_CONEXION ocEMPRESA_CONEXIONp)
        {
            string sSQL;
            //Cargar las conexiones por empresa
            cCONEXCION oData_tmp = ocEMPRESA_CONEXIONp.conectar_auxiliar(ocEMPRESA_CONEXIONp.ROW_ID);
            sSQL = " select * from information_schema.tables where table_name = 'CFDI_PROVEEDORES'";
            DataTable oDataTable = oData_tmp.EjecutarConsulta(sSQL);
            foreach (DataRow oDataRow in oDataTable.Rows)
            {

                sSQL = " SELECT * ";
                sSQL += " FROM CFDI_PROVEEDORES ";
                sSQL += " WHERE 1=1 AND INVOICE_ID='" + INVOICE_IDp + "' AND RFC_EMISOR='" + RFC_EMISORp + "'";

                oDataTable = oData_tmp.EjecutarConsulta(sSQL);
                foreach (DataRow oDataRow2 in oDataTable.Rows)
                {
                    ROW_ID_TRANSACCION = ROW_ID_TRANSACCIONp;
                    uUID_CFDIField = oDataRow2["UUID"].ToString();
                    rFCField = oDataRow2["RFC_EMISOR"].ToString();
                    try
                    {
                        montoTotalField = decimal.Parse(oDataRow2["montoTotalField"].ToString());
                    }
                    catch
                    {
                        montoTotalField = 0;
                    }
                    if (guardar(true,false))
                    {
                        return true;
                    }
                }

                //Traer del validador



            }
            return false;
        }




        internal bool ingresar_comprobante(string file)
        {
            if (File.Exists(file))
            {
                Comprobante oComprobante;
                //Pasear el comprobante
                try
                {
                    XmlSerializer serializer_CFD_3 = new XmlSerializer(typeof(Comprobante));
                    TextReader reader = new StreamReader(file);
                    oComprobante = (Comprobante)serializer_CFD_3.Deserialize(reader);
                    reader.Dispose();
                    reader.Close();                    
                }
                catch
                {
                    MessageBoxEx.Show("Cargar Comprobante Automatica: El archivo " + file + " no es un CFDI válido.",Application.ProductVersion,MessageBoxButtons.OK,MessageBoxIcon.Error);
                    return false;
                }
                XmlDocument doc = new XmlDocument();
                doc.Load(file);
                XmlNodeList TimbreFiscalDigital = doc.GetElementsByTagName("tfd:TimbreFiscalDigital");
                cTransaccion ocTransaccion = new cTransaccion();
                if(ROW_ID_TRANSACCION!="")
                {
                    if (!ocTransaccion.cargar(ROW_ID_TRANSACCION))
                    {
                        //Buscar por el tipo de llave la transaccion
                        sSQL = "SELECT TOP 1 [ROW_ID],[DOCUMENTO] FROM PolizasPolizaTransaccion WHERE DOCUMENTO like '%" + LLAVE.Replace("-", "") + "%'";
                        DataTable oDataTable = oData.EjecutarConsulta(sSQL);
                        foreach (DataRow oDataRow in oDataTable.Rows)
                        {
                            ROW_ID_TRANSACCION = oDataRow["ROW_ID"].ToString();
                            ocTransaccion.cargar(ROW_ID_TRANSACCION);
                        }
                    }
                    else
                    {
                        ROW_ID_TRANSACCION = ocTransaccion.ROW_ID;
                    }

                    if (ROW_ID_TRANSACCION == "-1")
                    {
                        if (TIPO.Contains("PAGO"))
                        {
                            rFCField = oComprobante.Emisor.rfc;
                            RFC_RECEPTOR = oComprobante.Receptor.rfc;
                        }
                        else
                        {
                            rFCField = oComprobante.Receptor.rfc;
                            RFC_RECEPTOR = oComprobante.Emisor.rfc;
                        }
                        montoTotalField = oComprobante.total;
                        
                        uUID_CFDIField = TimbreFiscalDigital[0].Attributes["UUID"].Value;
                        return guardar(true, false);
                    }
                    else
                    {
                        if ((ocTransaccion.TIPO == "API") | (ocTransaccion.TIPO == "APC"))
                        {
                            rFCField = oComprobante.Emisor.rfc;

                            RFC_RECEPTOR = oComprobante.Receptor.rfc;
                        }
                        if ((ocTransaccion.TIPO == "ARI") | (ocTransaccion.TIPO == "ARC"))
                        {
                            rFCField = oComprobante.Receptor.rfc;
                            RFC_RECEPTOR = oComprobante.Emisor.rfc;
                        }
                        TIPO = ocTransaccion.TIPO;
                        if (TIPO=="")
                        {

                        }
                        montoTotalField = oComprobante.total;
                        try
                        {
                            uUID_CFDIField = TimbreFiscalDigital[0].Attributes["UUID"].Value;
                            return guardar(true, false);
                        }
                        catch { }
                        
                    }               
                }
                MessageBoxEx.Show("Error cargando: " + uUID_CFDIField + " no se encontro referencia a " + LLAVE, Application.ProductName + "-" + Application.ProductVersion, MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
            MessageBoxEx.Show("La transacción esta en blanco", Application.ProductName + "-" + Application.ProductVersion, MessageBoxButtons.OK, MessageBoxIcon.Error);
            return false;
        }

        internal bool generar_PAYABLE_BINARY(string ROW_ID_TRANSACCIONp, string INVOICE_IDp, string VENDOR_IDp
            , cEMPRESA_CONEXION ocEMPRESA_CONEXIONp)
        {
            string sSQL;
            //Cargar las conexiones por empresa
            cCONEXCION oData_tmp = ocEMPRESA_CONEXIONp.conectar(ocEMPRESA_CONEXIONp.ROW_ID);
            sSQL = " select * from information_schema.tables where table_name = 'PAYABLE_BINARY'";
            DataTable oDataTable = oData_tmp.EjecutarConsulta(sSQL);
            foreach (DataRow oDataRow in oDataTable.Rows)
            {

                sSQL = " SELECT CAST(CAST(PAYABLE_BINARY.BITS AS VARBINARY(MAX)) AS VARCHAR(MAX)) as resultado, VENDOR.VAT_REGISTRATION as rFCField, PAYABLE.TOTAL_AMOUNT ";
                sSQL += " FROM PAYABLE INNER JOIN PAYABLE_BINARY ON PAYABLE.VOUCHER_ID = PAYABLE_BINARY.VOUCHER_ID INNER JOIN VENDOR ON PAYABLE.VENDOR_ID = VENDOR.ID";
                sSQL += " WHERE 1=1 AND PAYABLE.INVOICE_ID='" + INVOICE_IDp + "' AND PAYABLE.VENDOR_ID='" + VENDOR_IDp + "'";

                oDataTable = oData_tmp.EjecutarConsulta(sSQL);
                foreach (DataRow oDataRow2 in oDataTable.Rows)
                {
                    //Validar si solo tiene el UUID
                    //_re¿ATH930204947/rr¿IMO730403RCA/tt¿3087.800000/id¿be20c9db'3b7e'4e33'8fdb'a142bc79d794
                    string resultado = oDataRow2["resultado"].ToString().Replace("¿", "=").Replace("'", "-").Replace("/", "&");
                    //Es el UUID 37
                    if (resultado.Length == 37)
                    {
                        uUID_CFDIField = resultado;
                    }
                    else
                    {
                        //?re=GYR880101TL1&rr=IMO730403RCA&tt=0000032828.000000&id=43005233-0718-4A5C-A772-E8D73BABC924
                        string[] secciones = resultado.Split('&');
                        int contador = 0;
                        if (secciones.Length == 4)
                        {

                            foreach (string seccion in secciones)
                            {
                                switch (contador)
                                {
                                    case 3:
                                        //RFC del emisor
                                        string[] valores1 = seccion.Split('=');
                                        uUID_CFDIField = valores1[1];
                                        break;
                                }
                                contador++;
                            }

                        }

                    }

                    rFCField = oDataRow2["rFCField"].ToString();
                    montoTotalField = decimal.Parse(oDataRow2["TOTAL_AMOUNT"].ToString());

                    ROW_ID_TRANSACCION = ROW_ID_TRANSACCIONp;



                    if (guardar())
                    {
                        return true;
                    }
                }
            }
            return false;
        }

        public string USUARIO { get; set; }
    }
}
