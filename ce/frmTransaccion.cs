using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevComponents.DotNetBar;
using System.IO;
using OfficeOpenXml;
using Generales;

namespace Desarrollo
{
    public partial class frmTransaccion : DevComponents.DotNetBar.Metro.MetroForm
    {
        bool modificado = false;
        string ROW_ID_POLIZA;
        cTransaccion oObjeto = new cTransaccion();
        cCONEXCION oData = new cCONEXCION();
        string USUARIO;
        public frmTransaccion(string ROW_ID_CATALOGOp, string USUARIOp)
        {
            USUARIO = USUARIOp;
            oObjeto = new cTransaccion();
            ROW_ID_POLIZA = ROW_ID_CATALOGOp;
            InitializeComponent();
            limpiar();
        }

        public frmTransaccion(string ROW_ID_CATALOGOp,string ROW_ID, string USUARIOp)
        {
            USUARIO = USUARIOp;
            ROW_ID_POLIZA = ROW_ID_CATALOGOp;
            oObjeto = new cTransaccion();
            InitializeComponent();
            limpiar();
            cargar(ROW_ID);
        }

        #region Acciones
        private void cargar(string ROW_IDp)
        {
            if (oObjeto.cargar(ROW_IDp))
            {
                ROW_ID_POLIZA = oObjeto.ROW_ID_POLIZA;
                numCtaField.Text=oObjeto.numCtaField;
                conceptoField.Text = oObjeto.conceptoField;
                debeField.Text = oData.formato_decimal(oObjeto.debeField.ToString(), "");
                haberField.Text = oData.formato_decimal(oObjeto.haberField.ToString(), "");
                monedaField.SelectedValue = oObjeto.monedaField;
                ASIGNADO.Text = oObjeto.ASIGNADO;
                cargar_cheques();
                cargar_tranferencias();
                cargar_comprobantes();
                //cargar_seguridad();
            }

        }

        private void guardar()
        {
            oObjeto.numCtaField=numCtaField.Text;
            oObjeto.ROW_ID_POLIZA = ROW_ID_POLIZA;
            oObjeto.conceptoField = conceptoField.Text;
            oObjeto.ASIGNADO = ASIGNADO.Text;
            oObjeto.debeField = double.Parse(debeField.Text);
            oObjeto.haberField = double.Parse(haberField.Text);
            oObjeto.monedaField = monedaField.Text;

            if (oObjeto.guardar())
            {
                labelItem1.Text = "Datos Actualizados " + DateTime.Now.ToShortDateString() + " " + DateTime.Now.ToLongTimeString();
                mostrar_mensaje("Datos Actualizados ", false);
                modificado = false;
            }

        }

        private void mostrar_mensaje(string mensaje, bool mostrar_error)
        {
            ToastNotification.Show(this,
                mensaje,
                mostrar_error ? global::Desarrollo.Properties.Resources.error : global::Desarrollo.Properties.Resources.guardar,
                3000,
                mostrar_error ? eToastGlowColor.Red : eToastGlowColor.Green,
                eToastPosition.TopCenter);
        }
        public void limpiar()
        {
            if (modificado)
            {

                DialogResult Resultado = MessageBoxEx.Show("�Desea guardar los cambios?", Application.ProductName + "-" + Application.ProductVersion, MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                if (Resultado.ToString() == "Yes")
                {
                    guardar();
                }
            }
            modificado = false;
            oObjeto = new cTransaccion();
            monedaField.SelectedIndex = 0;
            foreach (var c in this.Controls)
            {
                if (c is TextBox)
                {
                    ((TextBox)c).Text = "";
                }
            }


        }
        //private void cargar_seguridad()
        //{
        //    //Verificar seguridad de ingreso
        //    cCE_SEGURIDAD ocCE_SEGURIDAD = new cCE_SEGURIDAD();
        //    ocCE_SEGURIDAD.cargar_cuenta(numCtaField.Text);
        //    if (ocCE_SEGURIDAD.USUARIO != "")
        //    {
        //        if (ocCE_SEGURIDAD.USUARIO == USUARIO)
        //        {
        //            superTabItem3.Visible = true;
        //            superTabControlPanel4.Visible = true;

        //        }
        //        else
        //        {
        //            superTabItem3.Visible = false;
        //            superTabControlPanel4.Visible = false;
        //        }
        //    }

        //}
        private void eliminar()
        {
            DialogResult Resultado = MessageBoxEx.Show("�Esta seguro de eliminar?", Application.ProductName + "-" + Application.ProductVersion, MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            if (Resultado.ToString() != "Yes")
            {
                return;
            }
            if (oObjeto.eliminar(oObjeto.ROW_ID))
            {
                modificado = false;
                
                MessageBoxEx.Show("Eliminado.", Application.ProductName + "-" + Application.ProductVersion, MessageBoxButtons.OK, MessageBoxIcon.Information);
                limpiar();
                
            }
        }
        #endregion

        private void buttonX3_Click(object sender, EventArgs e)
        {
            eliminar();
        }

        private void buttonX2_Click(object sender, EventArgs e)
        {
            limpiar();
        }

        private void buttonX1_Click(object sender, EventArgs e)
        {
            guardar();
        }

        private void buttonX12_Click(object sender, EventArgs e)
        {
            cargar_cheques();
        }

        private void cargar_cheques()
        {
            if (oObjeto.ROW_ID != "")
            {
                dtgCheque.Rows.Clear();
                cCheques ocCheques = new cCheques();
                List<cCheques> lista = ocCheques.todos(oObjeto.ROW_ID);
                foreach (cCheques registro in lista)
                {
                    int n = dtgCheque.Rows.Add();
                    actualizar_linea_cheque(registro, n);
                }
            }
        }

        private void actualizar_linea_cheque(cCheques registro, int n)
        {
            for (int i = 0; i < dtgCheque.Columns.Count; i++)
            {
                dtgCheque.Rows[n].Cells[i].Value = "";
            }
            dtgCheque.Rows[n].Tag = registro;
            dtgCheque.Rows[n].Cells["ROW_ID_b"].Value = registro.ROW_ID;
            dtgCheque.Rows[n].Cells["numCtaField_c"].Value = registro.numCtaField;
            dtgCheque.Rows[n].Cells["bancoField"].Value = registro.bancoField;
            dtgCheque.Rows[n].Cells["ctaOriField"].Value = registro.ctaOriField;
            dtgCheque.Rows[n].Cells["fechaField"].Value = registro.fechaField;
            dtgCheque.Rows[n].Cells["bancoField"].Value = registro.bancoField;
            dtgCheque.Rows[n].Cells["benefField"].Value = registro.benefField;
            dtgCheque.Rows[n].Cells["montoField"].Value = oData.formato_decimal(registro.montoField.ToString(), "");
            dtgCheque.Rows[n].Cells["rFCField"].Value = registro.rFCField;
        }

        private void buttonX13_Click(object sender, EventArgs e)
        {
            nueva_cheque();
        }

        private void nueva_cheque()
        {
            if (oObjeto.ROW_ID == "")
            {
                MessageBoxEx.Show("Guarde primero.", Application.ProductName + "-" + Application.ProductVersion, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return;
            }
            frmCheques ofrmCheques = new frmCheques(oObjeto.ROW_ID);
            ofrmCheques.ShowDialog();
            cargar_cheques();
        }
        private void eliminar_cheques()
        {
            dtgCheque.EndEdit();
            if (dtgCheque.SelectedCells.Count > 0)
            {
                DialogResult Resultado = MessageBoxEx.Show("�Desea eliminar las seleccionada?", Application.ProductName + "-" + Application.ProductVersion, MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                if (Resultado.ToString() != "Yes")
                {
                    return;
                }
                foreach (DataGridViewRow drv in dtgCheque.SelectedRows)
                {
                    string ROW_ID = drv.Cells["ROW_ID_b"].Value.ToString();
                    cCheques ocCheques = (cCheques)drv.Tag;
                    ocCheques.eliminar(ocCheques.ROW_ID);
                    dtgCheque.Rows.Remove(drv);
                }
            }
            else
            {
                MessageBoxEx.Show("Seleccione alguna l�nea.", Application.ProductName + "-" + Application.ProductVersion, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }
        private void buttonX11_Click(object sender, EventArgs e)
        {
            eliminar_cheques();
        }

        private void buttonX22_Click(object sender, EventArgs e)
        {
            cargar_tranferencias();
        }

        private void cargar_tranferencias()
        {
            if (oObjeto.ROW_ID != "")
            {
                dtgTranferencia.Rows.Clear();
                cTransferencia ocTransferencia = new cTransferencia();
                List<cTransferencia> lista = ocTransferencia.todos(oObjeto.ROW_ID);
                foreach (cTransferencia registro in lista)
                {
                    int n = dtgTranferencia.Rows.Add();
                    actualizar_linea_tranferencias(registro, n);
                }
            }
        }

        private void actualizar_linea_tranferencias(cTransferencia registro, int n)
        {
            for (int i = 0; i < dtgTranferencia.Columns.Count; i++)
            {
                dtgTranferencia.Rows[n].Cells[i].Value = "";
            }
            dtgTranferencia.Rows[n].Tag = registro;
            dtgTranferencia.Rows[n].Cells["ROW_ID_t"].Value = registro.ROW_ID;
            dtgTranferencia.Rows[n].Cells["ctaOriField_t"].Value = registro.ctaOriField;
            dtgTranferencia.Rows[n].Cells["bancoOriField_t"].Value = registro.bancoOriField;
            dtgTranferencia.Rows[n].Cells["montoField_t"].Value = oData.formato_decimal(registro.montoField.ToString(), "");
            dtgTranferencia.Rows[n].Cells["ctaDestField_t"].Value = registro.ctaDestField;
            dtgTranferencia.Rows[n].Cells["bancoDestField_t"].Value = registro.bancoDestField;
            dtgTranferencia.Rows[n].Cells["fechaField_t"].Value = registro.fechaField;
            dtgTranferencia.Rows[n].Cells["benefField_t"].Value = registro.benefField;
            dtgTranferencia.Rows[n].Cells["rFCField_t"].Value = registro.rFCField;
        }
        private void nueva_transferencia()
        {
            if (oObjeto.ROW_ID == "")
            {
                MessageBoxEx.Show("Guarde primero.", Application.ProductName + "-" + Application.ProductVersion, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return;
            }
            frmTransferencia ofrmTransferencia = new frmTransferencia(oObjeto.ROW_ID);
            ofrmTransferencia.ShowDialog();
            cargar_tranferencias();
        }
        private void buttonX21_Click(object sender, EventArgs e)
        {
            eliminar_transferencia();
        }

        private void eliminar_transferencia()
        {
            dtgTranferencia.EndEdit();
            if (dtgTranferencia.SelectedCells.Count > 0)
            {
                DialogResult Resultado = MessageBoxEx.Show("�Desea eliminar las seleccionada?", Application.ProductName + "-" + Application.ProductVersion, MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                if (Resultado.ToString() != "Yes")
                {
                    return;
                }
                foreach (DataGridViewRow drv in dtgTranferencia.SelectedRows)
                {
                    string ROW_ID = drv.Cells["ROW_ID_t"].Value.ToString();
                    cTransferencia ocTransferencia = (cTransferencia)drv.Tag;
                    ocTransferencia.eliminar(ocTransferencia.ROW_ID);
                    dtgTranferencia.Rows.Remove(drv);
                }
            }
            else
            {
                MessageBoxEx.Show("Seleccione alguna l�nea.", Application.ProductName + "-" + Application.ProductVersion, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }

        private void buttonX7_Click(object sender, EventArgs e)
        {
            cargar_comprobantes();
        }

        private void cargar_comprobantes()
        {
            if (oObjeto.ROW_ID != "")
            {
                dtgComprobantes.Rows.Clear();
                cPolizasPolizaTransaccionCompNal ocComprobantes = new cPolizasPolizaTransaccionCompNal();
                List<cPolizasPolizaTransaccionCompNal> lista = ocComprobantes.todos(oObjeto.ROW_ID);
                foreach (cPolizasPolizaTransaccionCompNal registro in lista)
                {
                    int n = dtgComprobantes.Rows.Add();
                    actualizar_linea_comprobantes(registro, n);
                }
            }
        }

        private void actualizar_linea_comprobantes(cPolizasPolizaTransaccionCompNal registro, int n)
        {
            for (int i = 0; i < dtgComprobantes.Columns.Count; i++)
            {
                dtgComprobantes.Rows[n].Cells[i].Value = "";
            }
            dtgComprobantes.Rows[n].Tag = registro;
            dtgComprobantes.Rows[n].Cells["ROW_ID_c"].Value = registro.ROW_ID;
            dtgComprobantes.Rows[n].Cells["rFCField_c"].Value = registro.rFCField;
            dtgComprobantes.Rows[n].Cells["uUID_CFDIField"].Value = registro.uUID_CFDIField;
        }

        private void buttonX8_Click(object sender, EventArgs e)
        {
            nueva_comprobante();
        }

        private void nueva_comprobante()
        {
            if (oObjeto.ROW_ID == "")
            {
                MessageBoxEx.Show("Guarde primero.", Application.ProductName + "-" + Application.ProductVersion, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return;
            }
            frmComprobante ofrmComprobante = new frmComprobante(oObjeto.ROW_ID);
            ofrmComprobante.ShowDialog();
            cargar_comprobantes();
        }

        private void buttonX6_Click(object sender, EventArgs e)
        {
            eliminar_comprobante();
        }
        private void eliminar_comprobante()
        {
            dtgComprobantes.EndEdit();
            if (dtgComprobantes.SelectedCells.Count > 0)
            {
                DialogResult Resultado = MessageBoxEx.Show("�Desea eliminar las seleccionada?", Application.ProductName + "-" + Application.ProductVersion, MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                if (Resultado.ToString() != "Yes")
                {
                    return;
                }
                foreach (DataGridViewRow drv in dtgComprobantes.SelectedRows)
                {
                    string ROW_ID = drv.Cells["ROW_ID_c"].Value.ToString();
                    cPolizasPolizaTransaccionCompNal ocComprobantes = (cPolizasPolizaTransaccionCompNal)drv.Tag;
                    ocComprobantes.eliminar(ocComprobantes.ROW_ID);
                    dtgComprobantes.Rows.Remove(drv);
                }
            }
            else
            {
                MessageBoxEx.Show("Seleccione alguna l�nea.", Application.ProductName + "-" + Application.ProductVersion, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }

        private void buttonX23_Click(object sender, EventArgs e)
        {
            nueva_transferencia();
        }

        private void dtgCheque_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            editar_cheque();
        }

        private void editar_cheque()
        {
            dtgCheque.EndEdit();
            if (dtgCheque.SelectedRows.Count > 0)
            {
                foreach (DataGridViewRow drv in dtgCheque.SelectedRows)
                {
                    string ROW_ID = drv.Cells["ROW_ID_b"].Value.ToString();
                    frmCheques ofrmCheques = new frmCheques(oObjeto.ROW_ID, ROW_ID);
                    ofrmCheques.ShowDialog();
                    cCheques ocCheques = new cCheques();
                    ocCheques.cargar(ROW_ID);
                    actualizar_linea_cheque(ocCheques, drv.Index);
                }
            }
            else
            {
                MessageBoxEx.Show("Seleccione alguna l�nea.", Application.ProductName + "-" + Application.ProductVersion, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }

        private void editar_comprobante()
        {
            dtgComprobantes.EndEdit();
            if (dtgComprobantes.SelectedRows.Count > 0)
            {
                foreach (DataGridViewRow drv in dtgComprobantes.SelectedRows)
                {
                    string ROW_ID = drv.Cells["ROW_ID_c"].Value.ToString();
                    frmComprobante ofrmComprobante = new frmComprobante(oObjeto.ROW_ID, ROW_ID);
                    ofrmComprobante.ShowDialog();
                    cPolizasPolizaTransaccionCompNal ocComprobantes = new cPolizasPolizaTransaccionCompNal();
                    ocComprobantes.cargar(ROW_ID);
                    actualizar_linea_comprobantes(ocComprobantes, drv.Index);
                }
            }
            else
            {
                MessageBoxEx.Show("Seleccione alguna l�nea.", Application.ProductName + "-" + Application.ProductVersion, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }

        private void dtgComprobantes_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            editar_comprobante();
        }

        private void dtgTranferencia_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            editar_transferencia();
        }

        private void editar_transferencia()
        {
            dtgTranferencia.EndEdit();
            if (dtgTranferencia.SelectedRows.Count > 0)
            {
                foreach (DataGridViewRow drv in dtgTranferencia.SelectedRows)
                {
                    string ROW_ID = drv.Cells["ROW_ID_t"].Value.ToString();
                    frmTransferencia ofrmTransferencia = new frmTransferencia(oObjeto.ROW_ID, ROW_ID);
                    ofrmTransferencia.ShowDialog();
                    cTransferencia ocTransferencia = new cTransferencia();
                    ocTransferencia.cargar(ROW_ID);
                    actualizar_linea_tranferencias(ocTransferencia, drv.Index);
                }
            }
            else
            {
                MessageBoxEx.Show("Seleccione alguna l�nea.", Application.ProductName + "-" + Application.ProductVersion, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }

        private void buttonX16_Click(object sender, EventArgs e)
        {
            frmComprobantes ofrmComprobantes = new frmComprobantes(oObjeto.ROW_ID);
            ofrmComprobantes.ShowDialog();
            cargar_comprobantes();
        }

        private void buttonItem4_Click(object sender, EventArgs e)
        {
            plantilla_excel_transaccion();
        }

        private void buttonX14_Click(object sender, EventArgs e)
        {
            cPoliza ocPoliza = new cPoliza();
            ocPoliza.importar_transferencia();
        }

        private void buttonItem1_Click(object sender, EventArgs e)
        {

        }

        private void buttonX4_Click(object sender, EventArgs e)
        {
            cPoliza ocPoliza = new cPoliza();
            ocPoliza.importar_comprobantes();
        }

        private void plantilla_excel_transaccion()
        {
            var fileName = " Plantilla de Transacciones " + DateTime.Now.ToString("yyyy-MM-dd-hh-mm-ss") + ".xlsx";
            if (folderBrowserDialog1.ShowDialog() == DialogResult.OK)
            {
                fileName = folderBrowserDialog1.SelectedPath + "\\" + fileName;
                System.IO.Stream myStream = new System.IO.FileStream(fileName, System.IO.FileMode.Create, System.IO.FileAccess.ReadWrite, System.IO.FileShare.None);

                using (ExcelPackage pck = new ExcelPackage())
                {

                    ExcelWorksheet ws1 = pck.Workbook.Worksheets.Add("Plantilla");

                    ws1.Cells[1, 1].Value = "Poliza";
                    ws1.Cells[1, 2].Value = "Transaccion";
                    ws1.Cells[1, 3].Value = "Cuenta Origen";
                    ws1.Cells[1, 4].Value = "Banco Origen";
                    ws1.Cells[1, 5].Value = "Monto";
                    ws1.Cells[1, 6].Value = "Fecha";
                    ws1.Cells[1, 7].Value = "Cuenta Destino";
                    ws1.Cells[1, 8].Value = "Banco Destino";
                    ws1.Cells[1, 9].Value = "Beneficiario";
                    ws1.Cells[1, 10].Value = "RFC";

                    pck.SaveAs(myStream);
                }
                myStream.Close();
                MessageBoxEx.Show("Creaci�n de plantilla realizada " + Environment.NewLine + fileName + " finalizada.", Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private void buttonItem2_Click(object sender, EventArgs e)
        {
            
        }

        private void buttonX9_Click(object sender, EventArgs e)
        {
            cPoliza ocPoliza = new cPoliza();
            ocPoliza.importar_cheques();
        }

    }
}