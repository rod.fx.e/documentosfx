using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevComponents.DotNetBar;
using System.IO;
using OfficeOpenXml;

namespace Desarrollo
{
    public partial class frmCheques : DevComponents.DotNetBar.Metro.MetroForm
    {
        bool modificado = false;
        string ROW_ID_TRANSACCION;
        cCheques oObjeto = new cCheques();
        
        public frmCheques(string ROW_ID_TRANSACCIONp)
        {
            oObjeto = new cCheques();
            ROW_ID_TRANSACCION = ROW_ID_TRANSACCIONp;
            InitializeComponent();
            limpiar();
        }

        public frmCheques(string ROW_ID_TRANSACCIONp,string ROW_ID)
        {
            ROW_ID_TRANSACCION = ROW_ID_TRANSACCIONp;
            oObjeto = new cCheques();
            InitializeComponent();
            cargar(ROW_ID);
        }

        #region Acciones
        private void cargar(string ROW_IDp)
        {
            if (oObjeto.cargar(ROW_IDp))
            {
                ROW_ID_TRANSACCION = oObjeto.ROW_ID_TRANSACCION;
                numCtaField.Text=oObjeto.numCtaField;
                benefField.Text = oObjeto.benefField;
                rFCField.Text = oObjeto.rFCField;
                bancoField.Text = oObjeto.bancoField;
                ctaOriField.Text = oObjeto.ctaOriField;
                fechaField.Value = oObjeto.fechaField;
                montoField.Text = oObjeto.montoField.ToString();
                bancoField.Text = oObjeto.bancoField;
                bancoField.Text = oObjeto.bancoField;
            }

        }

        private void guardar()
        {
            oObjeto.numCtaField=numCtaField.Text;
            oObjeto.ROW_ID_TRANSACCION = ROW_ID_TRANSACCION;
            oObjeto.benefField = benefField.Text;
            oObjeto.rFCField = rFCField.Text;
            oObjeto.bancoField=bancoField.Text;
            oObjeto.ctaOriField = ctaOriField.Text;
            oObjeto.fechaField=fechaField.Value;
            oObjeto.montoField = double.Parse(montoField.Text);
            oObjeto.bancoField=bancoField.Text;
            if (oObjeto.guardar())
            {
                labelItem1.Text = "Datos Actualizados " + DateTime.Now.ToShortDateString() + " " + DateTime.Now.ToLongTimeString();
                mostrar_mensaje("Datos Actualizados ", false);
                modificado = false;
            }

        }

        private void mostrar_mensaje(string mensaje, bool mostrar_error)
        {
            ToastNotification.Show(this,
                mensaje,
                mostrar_error ? global::Desarrollo.Properties.Resources.error : global::Desarrollo.Properties.Resources.guardar,
                3000,
                mostrar_error ? eToastGlowColor.Red : eToastGlowColor.Green,
                eToastPosition.TopCenter);
        }
        public void limpiar()
        {
            if (modificado)
            {
                DialogResult Resultado = MessageBoxEx.Show("�Desea guardar los cambios?", Application.ProductName + "-" + Application.ProductVersion, MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                if (Resultado.ToString() == "Yes")
                {
                    guardar();
                }
            }
            modificado = false;
            oObjeto = new cCheques();
            foreach (var c in this.Controls)
            {
                if (c is TextBox)
                {
                    ((TextBox)c).Text = "";
                }
            }
            

        }
        
        private void eliminar()
        {
            DialogResult Resultado = MessageBoxEx.Show("�Esta seguro de eliminar?", Application.ProductName + "-" + Application.ProductVersion, MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            if (Resultado.ToString() != "Yes")
            {
                return;
            }
            if (oObjeto.eliminar(oObjeto.ROW_ID))
            {
                modificado = false;
                
                MessageBoxEx.Show("Eliminado.", Application.ProductName + "-" + Application.ProductVersion, MessageBoxButtons.OK, MessageBoxIcon.Information);
                limpiar();
                
            }
        }
        #endregion

        private void buttonX3_Click(object sender, EventArgs e)
        {
            eliminar();
        }

        private void buttonX2_Click(object sender, EventArgs e)
        {
            limpiar();
        }

        private void buttonX1_Click(object sender, EventArgs e)
        {
            guardar();
        }

    }
}