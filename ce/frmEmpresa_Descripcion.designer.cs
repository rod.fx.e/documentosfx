namespace Desarrollo
{
    partial class frmEmpresa_Descripcion
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            DevComponents.DotNetBar.ButtonX buttonX3;
            DevComponents.DotNetBar.ButtonX buttonX1;
            DevComponents.DotNetBar.ButtonX buttonX2;
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmEmpresa_Descripcion));
            this.metroStatusBar1 = new DevComponents.DotNetBar.Metro.MetroStatusBar();
            this.labelItem1 = new DevComponents.DotNetBar.LabelItem();
            this.toolStripStatusLabel1 = new DevComponents.DotNetBar.LabelItem();
            this.labelX5 = new DevComponents.DotNetBar.LabelX();
            this.DESCRIPCION = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.labelX3 = new DevComponents.DotNetBar.LabelX();
            this.VISUAL_DESCRIPCION = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.backgroundWorker1 = new System.ComponentModel.BackgroundWorker();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            buttonX3 = new DevComponents.DotNetBar.ButtonX();
            buttonX1 = new DevComponents.DotNetBar.ButtonX();
            buttonX2 = new DevComponents.DotNetBar.ButtonX();
            this.SuspendLayout();
            // 
            // buttonX3
            // 
            buttonX3.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            buttonX3.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            buttonX3.Image = global::Desarrollo.Properties.Resources.Eliminar_18;
            buttonX3.Location = new System.Drawing.Point(141, 0);
            buttonX3.Name = "buttonX3";
            buttonX3.Size = new System.Drawing.Size(68, 24);
            buttonX3.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            buttonX3.TabIndex = 2;
            buttonX3.Text = "Eliminar";
            buttonX3.TextAlignment = DevComponents.DotNetBar.eButtonTextAlignment.Right;
            buttonX3.Click += new System.EventHandler(this.buttonX3_Click);
            // 
            // buttonX1
            // 
            buttonX1.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            buttonX1.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            buttonX1.Image = global::Desarrollo.Properties.Resources.Nueno_18;
            buttonX1.Location = new System.Drawing.Point(67, 0);
            buttonX1.Name = "buttonX1";
            buttonX1.Size = new System.Drawing.Size(68, 24);
            buttonX1.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            buttonX1.TabIndex = 1;
            buttonX1.Text = "Guardar";
            buttonX1.TextAlignment = DevComponents.DotNetBar.eButtonTextAlignment.Right;
            buttonX1.Click += new System.EventHandler(this.buttonX1_Click);
            // 
            // buttonX2
            // 
            buttonX2.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            buttonX2.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            buttonX2.Image = global::Desarrollo.Properties.Resources.Nuevo_18_19;
            buttonX2.Location = new System.Drawing.Point(0, 0);
            buttonX2.Name = "buttonX2";
            buttonX2.Size = new System.Drawing.Size(61, 24);
            buttonX2.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            buttonX2.TabIndex = 0;
            buttonX2.Text = "Nuevo";
            buttonX2.TextAlignment = DevComponents.DotNetBar.eButtonTextAlignment.Right;
            buttonX2.Click += new System.EventHandler(this.buttonX2_Click);
            // 
            // metroStatusBar1
            // 
            this.metroStatusBar1.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.metroStatusBar1.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.metroStatusBar1.ContainerControlProcessDialogKey = true;
            this.metroStatusBar1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.metroStatusBar1.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.metroStatusBar1.ForeColor = System.Drawing.Color.Black;
            this.metroStatusBar1.Items.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.labelItem1,
            this.toolStripStatusLabel1});
            this.metroStatusBar1.Location = new System.Drawing.Point(0, 83);
            this.metroStatusBar1.Name = "metroStatusBar1";
            this.metroStatusBar1.Size = new System.Drawing.Size(644, 21);
            this.metroStatusBar1.TabIndex = 7;
            // 
            // labelItem1
            // 
            this.labelItem1.Name = "labelItem1";
            // 
            // toolStripStatusLabel1
            // 
            this.toolStripStatusLabel1.Name = "toolStripStatusLabel1";
            // 
            // labelX5
            // 
            this.labelX5.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.labelX5.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX5.ForeColor = System.Drawing.Color.Black;
            this.labelX5.Location = new System.Drawing.Point(32, 56);
            this.labelX5.Name = "labelX5";
            this.labelX5.Size = new System.Drawing.Size(79, 22);
            this.labelX5.TabIndex = 5;
            this.labelX5.Text = "Descripción";
            this.labelX5.TextAlignment = System.Drawing.StringAlignment.Far;
            // 
            // DESCRIPCION
            // 
            this.DESCRIPCION.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.DESCRIPCION.Border.Class = "TextBoxBorder";
            this.DESCRIPCION.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.DESCRIPCION.ForeColor = System.Drawing.Color.Black;
            this.DESCRIPCION.Location = new System.Drawing.Point(117, 56);
            this.DESCRIPCION.Name = "DESCRIPCION";
            this.DESCRIPCION.Size = new System.Drawing.Size(526, 22);
            this.DESCRIPCION.TabIndex = 6;
            this.DESCRIPCION.WatermarkBehavior = DevComponents.DotNetBar.eWatermarkBehavior.HideNonEmpty;
            // 
            // labelX3
            // 
            this.labelX3.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.labelX3.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX3.ForeColor = System.Drawing.Color.Black;
            this.labelX3.Location = new System.Drawing.Point(0, 30);
            this.labelX3.Name = "labelX3";
            this.labelX3.Size = new System.Drawing.Size(111, 22);
            this.labelX3.TabIndex = 3;
            this.labelX3.Text = "Descripción Visual";
            this.labelX3.TextAlignment = System.Drawing.StringAlignment.Far;
            // 
            // VISUAL_DESCRIPCION
            // 
            this.VISUAL_DESCRIPCION.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.VISUAL_DESCRIPCION.Border.Class = "TextBoxBorder";
            this.VISUAL_DESCRIPCION.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.VISUAL_DESCRIPCION.ForeColor = System.Drawing.Color.Black;
            this.VISUAL_DESCRIPCION.Location = new System.Drawing.Point(117, 30);
            this.VISUAL_DESCRIPCION.Name = "VISUAL_DESCRIPCION";
            this.VISUAL_DESCRIPCION.Size = new System.Drawing.Size(526, 22);
            this.VISUAL_DESCRIPCION.TabIndex = 4;
            this.VISUAL_DESCRIPCION.WatermarkBehavior = DevComponents.DotNetBar.eWatermarkBehavior.HideNonEmpty;
            // 
            // backgroundWorker1
            // 
            this.backgroundWorker1.WorkerReportsProgress = true;
            this.backgroundWorker1.WorkerSupportsCancellation = true;
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.FileName = "openFileDialog1";
            // 
            // frmEmpresa_Descripcion
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.BottomLeftCornerSize = 0;
            this.ClientSize = new System.Drawing.Size(644, 104);
            this.Controls.Add(buttonX3);
            this.Controls.Add(buttonX1);
            this.Controls.Add(buttonX2);
            this.Controls.Add(this.metroStatusBar1);
            this.Controls.Add(this.VISUAL_DESCRIPCION);
            this.Controls.Add(this.labelX3);
            this.Controls.Add(this.DESCRIPCION);
            this.Controls.Add(this.labelX5);
            this.Cursor = System.Windows.Forms.Cursors.Default;
            this.DoubleBuffered = true;
            this.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "frmEmpresa_Descripcion";
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Descripción por concepto";
            this.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.frmEmpresa_KeyPress);
            this.ResumeLayout(false);

        }

        #endregion

        private DevComponents.DotNetBar.Metro.MetroStatusBar metroStatusBar1;
        private DevComponents.DotNetBar.LabelItem labelItem1;
        private DevComponents.DotNetBar.LabelX labelX5;
        private DevComponents.DotNetBar.Controls.TextBoxX DESCRIPCION;
        private DevComponents.DotNetBar.LabelX labelX3;
        private DevComponents.DotNetBar.Controls.TextBoxX VISUAL_DESCRIPCION;
        private DevComponents.DotNetBar.LabelItem toolStripStatusLabel1;
        private System.ComponentModel.BackgroundWorker backgroundWorker1;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;

    }
}