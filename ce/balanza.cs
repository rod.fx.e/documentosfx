﻿//namespace Desarrollo
//{
//    //------------------------------------------------------------------------------
//    // <auto-generated>
//    //     Este código fue generado por una herramienta.
//    //     Versión del motor en tiempo de ejecución:2.0.50727.5477
//    //
//    //     Los cambios en este archivo podrían causar un comportamiento incorrecto y se perderán si
//    //     se vuelve a generar el código.
//    // </auto-generated>
//    //------------------------------------------------------------------------------

//    using System.Xml.Serialization;

//    // 
//    // This source code was auto-generated by xsd, Version=2.0.50727.3038.
//    // 


//    /// <comentarios/>
//    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.3038")]
//    [System.SerializableAttribute()]
//    [System.Diagnostics.DebuggerStepThroughAttribute()]
//    [System.ComponentModel.DesignerCategoryAttribute("code")]
//    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.sat.gob.mx/balanza")]
//    [System.Xml.Serialization.XmlRootAttribute(Namespace = "http://www.sat.gob.mx/balanza", IsNullable = false)]
//    public partial class Balanza
//    {

//        private BalanzaCtas[] ctasField;

//        private string versionField;

//        private string rFCField;

//        private int totalCtasField;

//        private BalanzaMes mesField;

//        private int anoField;

//        public Balanza()
//        {
//            this.versionField = "1.0";
//        }

//        /// <comentarios/>
//        [System.Xml.Serialization.XmlElementAttribute("Ctas")]
//        public BalanzaCtas[] Ctas
//        {
//            get
//            {
//                return this.ctasField;
//            }
//            set
//            {
//                this.ctasField = value;
//            }
//        }

//        /// <comentarios/>
//        [System.Xml.Serialization.XmlAttributeAttribute()]
//        public string Version
//        {
//            get
//            {
//                return this.versionField;
//            }
//            set
//            {
//                this.versionField = value;
//            }
//        }

//        /// <comentarios/>
//        [System.Xml.Serialization.XmlAttributeAttribute()]
//        public string RFC
//        {
//            get
//            {
//                return this.rFCField;
//            }
//            set
//            {
//                this.rFCField = value;
//            }
//        }

//        /// <comentarios/>
//        [System.Xml.Serialization.XmlAttributeAttribute()]
//        public int TotalCtas
//        {
//            get
//            {
//                return this.totalCtasField;
//            }
//            set
//            {
//                this.totalCtasField = value;
//            }
//        }

//        /// <comentarios/>
//        [System.Xml.Serialization.XmlAttributeAttribute()]
//        public BalanzaMes Mes
//        {
//            get
//            {
//                return this.mesField;
//            }
//            set
//            {
//                this.mesField = value;
//            }
//        }

//        /// <comentarios/>
//        [System.Xml.Serialization.XmlAttributeAttribute()]
//        public int Ano
//        {
//            get
//            {
//                return this.anoField;
//            }
//            set
//            {
//                this.anoField = value;
//            }
//        }
//    }

//    /// <comentarios/>
//    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.3038")]
//    [System.SerializableAttribute()]
//    [System.Diagnostics.DebuggerStepThroughAttribute()]
//    [System.ComponentModel.DesignerCategoryAttribute("code")]
//    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.sat.gob.mx/balanza")]
//    public partial class BalanzaCtas
//    {

//        private string numCtaField;

//        private decimal saldoIniField;

//        private decimal debeField;

//        private decimal haberField;

//        private decimal saldoFinField;

//        /// <comentarios/>
//        [System.Xml.Serialization.XmlAttributeAttribute()]
//        public string NumCta
//        {
//            get
//            {
//                return this.numCtaField;
//            }
//            set
//            {
//                this.numCtaField = value;
//            }
//        }

//        /// <comentarios/>
//        [System.Xml.Serialization.XmlAttributeAttribute()]
//        public decimal SaldoIni
//        {
//            get
//            {
//                return this.saldoIniField;
//            }
//            set
//            {
//                this.saldoIniField = value;
//            }
//        }

//        /// <comentarios/>
//        [System.Xml.Serialization.XmlAttributeAttribute()]
//        public decimal Debe
//        {
//            get
//            {
//                return this.debeField;
//            }
//            set
//            {
//                this.debeField = value;
//            }
//        }

//        /// <comentarios/>
//        [System.Xml.Serialization.XmlAttributeAttribute()]
//        public decimal Haber
//        {
//            get
//            {
//                return this.haberField;
//            }
//            set
//            {
//                this.haberField = value;
//            }
//        }

//        /// <comentarios/>
//        [System.Xml.Serialization.XmlAttributeAttribute()]
//        public decimal SaldoFin
//        {
//            get
//            {
//                return this.saldoFinField;
//            }
//            set
//            {
//                this.saldoFinField = value;
//            }
//        }
//    }

//    /// <comentarios/>
//    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.3038")]
//    [System.SerializableAttribute()]
//    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.sat.gob.mx/balanza")]
//    public enum BalanzaMes
//    {

//        /// <comentarios/>
//        [System.Xml.Serialization.XmlEnumAttribute("01")]
//        Item01,

//        /// <comentarios/>
//        [System.Xml.Serialization.XmlEnumAttribute("02")]
//        Item02,

//        /// <comentarios/>
//        [System.Xml.Serialization.XmlEnumAttribute("03")]
//        Item03,

//        /// <comentarios/>
//        [System.Xml.Serialization.XmlEnumAttribute("04")]
//        Item04,

//        /// <comentarios/>
//        [System.Xml.Serialization.XmlEnumAttribute("05")]
//        Item05,

//        /// <comentarios/>
//        [System.Xml.Serialization.XmlEnumAttribute("06")]
//        Item06,

//        /// <comentarios/>
//        [System.Xml.Serialization.XmlEnumAttribute("07")]
//        Item07,

//        /// <comentarios/>
//        [System.Xml.Serialization.XmlEnumAttribute("08")]
//        Item08,

//        /// <comentarios/>
//        [System.Xml.Serialization.XmlEnumAttribute("09")]
//        Item09,

//        /// <comentarios/>
//        [System.Xml.Serialization.XmlEnumAttribute("10")]
//        Item10,

//        /// <comentarios/>
//        [System.Xml.Serialization.XmlEnumAttribute("11")]
//        Item11,

//        /// <comentarios/>
//        [System.Xml.Serialization.XmlEnumAttribute("12")]
//        Item12,
//    }


//}
