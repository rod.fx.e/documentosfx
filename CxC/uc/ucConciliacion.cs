﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using Generales;
using CFDI33;
using System.Xml.Serialization;
using System.Xml;
using System.Xml.Linq;
using ModeloComprobantePago;
using Pago10;
using FE_FX;

namespace CxC.uc
{
    public partial class ucConciliacion : UserControl
    {
        public ucConciliacion()
        {
            InitializeComponent();
            cargarEmisor();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            buscar();
        }

        private void cargarEmisor()
        {
            emisor.Items.Clear();
            cEMPRESA ocEMPRESA = new cEMPRESA();
            foreach (cEMPRESA registro in ocEMPRESA.todos("", true))
            {
                emisor.Items.Add(registro);
            }
            if (emisor.Items.Count > 0)
            {
                emisor.SelectedIndex = 0;
            }
        }

        private void buscar()
        {
            try
            {
                OpenFileDialog folderBrowser = new OpenFileDialog();
                // Set validate names and check file exists to false otherwise windows will
                // not let you select "Folder Selection."
                folderBrowser.ValidateNames = false;
                folderBrowser.CheckFileExists = false;
                folderBrowser.CheckPathExists = true;
                if (Directory.Exists(directorio.Text))
                {
                    folderBrowser.InitialDirectory = directorio.Text;
                }

                // Always default to Folder Selection.
                folderBrowser.FileName = "Directorio.";
                if (folderBrowser.ShowDialog() == DialogResult.OK)
                {
                    dtg.Rows.Clear();
                    string folderPath = Path.GetDirectoryName(folderBrowser.FileName);
                    directorio.Text = folderPath;
                    if (Directory.Exists(directorio.Text))
                    {
                        procesar();
                    }
                }
            }
            catch (Exception error)
            {
                ErrorFX.mostrar(error, true, true, true);
            }
        }
        private void procesar()
        {
            string[] allfiles = System.IO.Directory.GetFiles(directorio.Text, "*.xml", System.IO.SearchOption.AllDirectories);
            foreach (var file in allfiles)
            {
                FileInfo info = new FileInfo(file);
                //Cargar la información del pago en el grid
                informacionPago(file);
            }
            dtg.AutoResizeColumns(DataGridViewAutoSizeColumnsMode.AllCells);
        }

        private void informacionPago(string filename)
        {
            try
            {
                Comprobante oComprobante;
                //Pasear el comprobante

                try
                {
                    XmlSerializer serializer_CFD_3 = new XmlSerializer(typeof(Comprobante));
                    TextReader reader = new StreamReader(filename);
                    oComprobante = (Comprobante)serializer_CFD_3.Deserialize(reader);
                    reader.Dispose();
                    reader.Close();

                    if (!oComprobante.TipoDeComprobante.Equals("P"))
                    {
                        return;
                    }


                    int n = dtg.Rows.Add();

                    dtg.Rows[n].Tag = filename;
                    dtg.Rows[n].Cells["rfcEmisor"].Value = oComprobante.Emisor.Rfc;
                    dtg.Rows[n].Cells["nombreEmisor"].Value = oComprobante.Emisor.Nombre;
                    dtg.Rows[n].Cells["rfcReceptor"].Value = oComprobante.Receptor.Rfc;
                    dtg.Rows[n].Cells["nombreReceptor"].Value = oComprobante.Receptor.Nombre;
                    dtg.Rows[n].Cells["Serie"].Value = oComprobante.Serie;
                    dtg.Rows[n].Cells["Folio"].Value = oComprobante.Folio;
                    bool duplicar = false;
                    //Determinar si es Timbrado
                    foreach (ComprobanteComplemento complemento in oComprobante.Complemento)
                    {
                        foreach (XmlElement elemento in complemento.Any)
                        {
                            //Timbrado
                            if (elemento.OuterXml.Contains("tfd:TimbreFiscalDigital"))
                            {
                                var doc = XDocument.Parse(elemento.OuterXml);
                                string uuidtr = doc.Root.Attributes("UUID").First().Value.ToString();
                                dtg.Rows[n].Cells["uuid"].Value = uuidtr;
                                string FechaTimbradotr = doc.Root.Attributes("FechaTimbrado").First().Value.ToString();
                                dtg.Rows[n].Cells["timbrado"].Value = FechaTimbradotr;
                                ModeloComprobantePago.documentosfxEntities dbContext = new documentosfxEntities();
                                //Cargar el Check y el Customer
                                pago oRegistroTr = dbContext.pagoSet.Where(a => a.uuid.Equals(uuidtr)).FirstOrDefault();
                                if (oRegistroTr != null)
                                {
                                    dtg.Rows[n].Cells["CHECK_ID"].Value = oRegistroTr.checkId;
                                    dtg.Rows[n].Cells["CUSTOMER_ID"].Value = oRegistroTr.customerId;
                                }
                            }
                        }
                    }

                    //Buscar el Comprobante de Pago
                    foreach (ComprobanteComplemento complemento in oComprobante.Complemento)
                    {
                        foreach (XmlElement elemento in complemento.Any)
                        {
                            //Si es nomina buscar los elementos con Linq
                            if (elemento.OuterXml.Contains("pago10:Pagos"))
                            {
                                using (StringReader readerPagos = new StringReader(elemento.OuterXml))
                                {
                                    XmlSerializer valueSerializer = new XmlSerializer(typeof(Pagos));
                                    Pagos oPagos = (Pagos)valueSerializer.Deserialize(readerPagos);
                                    foreach (Pago10.PagosPago pago in oPagos.Pago)
                                    {
                                        dtg.Rows[n].Cells["Monto"].Value = pago.Monto;//pago.Monto.ToString("C");
                                        dtg.Rows[n].Cells["Moneda"].Value = pago.MonedaP;
                                        
                                        foreach (Pago10.PagosPagoDoctoRelacionado cfdi in pago.DoctoRelacionado)
                                        {
                                            if (duplicar)
                                            {
                                                int nAnterior = n;
                                                n = dtg.Rows.Add();
                                                dtg.Rows[n].Tag = dtg.Rows[nAnterior].Tag;

                                                foreach (DataGridViewColumn column in dtg.Columns)
                                                {
                                                    if (dtg.Rows[nAnterior].Cells[column.Index].Value != null)
                                                    {
                                                        dtg.Rows[n].Cells[column.Index].Value = dtg.Rows[nAnterior].Cells[column.Index].Value;
                                                    }
                                                }

                                            }
                                            dtg.Rows[n].Cells["cfdiSerie"].Value = cfdi.Serie;
                                            dtg.Rows[n].Cells["cfdiFolio"].Value = cfdi.Folio;
                                            dtg.Rows[n].Cells["NumParcialidad"].Value = cfdi.NumParcialidad;
                                            dtg.Rows[n].Cells["ImpPagado"].Value = cfdi.ImpPagado.ToString("C");
                                            dtg.Rows[n].Cells["cfdiUUID"].Value = cfdi.IdDocumento;
                                            duplicar = true;
                                        }
                                    }
                                }
                            }
                        }
                    }


                }
                catch (Exception error)
                {
                    string mensaje = "Archivo: " + filename + " " + error.Message.ToString();
                    if (error.InnerException != null)
                    {
                        mensaje += Environment.NewLine + error.InnerException.Message.ToString();
                        if (error.InnerException.InnerException != null)
                        {
                            mensaje += Environment.NewLine + error.InnerException.InnerException.ToString();
                        }
                    }
                    MostrarErrores.Text += mensaje + Environment.NewLine;
                }

            }
            catch (Exception error)
            {
                string mensaje = "Archivo: " + filename + " " + error.Message.ToString();
                if (error.InnerException != null)
                {
                    mensaje += Environment.NewLine + error.InnerException.Message.ToString();
                    if (error.InnerException.InnerException != null)
                    {
                        mensaje += Environment.NewLine + error.InnerException.InnerException.ToString();
                    }
                }
                MostrarErrores.Text += mensaje + Environment.NewLine;
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            ExcelFX.exportar(dtg, "Comprobantes de Pago " + DateTime.Now.ToShortDateString());
        }

        private void button5_Click(object sender, EventArgs e)
        {
            cargarSerie();
        }
        cEMPRESA ocEMPRESA;
        private void cargarSerie()
        {
            try
            {
                serieT.Text = "";
                ocEMPRESA = new cEMPRESA();
                cSERIE ocSERIE = new cSERIE();
                ocEMPRESA = (cEMPRESA)emisor.SelectedItem;
                cCONEXCION oData_ERP = new cCONEXCION(ocEMPRESA.TIPO, ocEMPRESA.SERVIDOR, ocEMPRESA.BD, ocEMPRESA.USUARIO_BD, ocEMPRESA.PASSWORD_BD);
                foreach (cSERIE registro in ocSERIE.todos_empresa("", ocEMPRESA.ROW_ID, true, false))
                {
                    serieT.Text = registro.ID;
                    directorio.Text = registro.DIRECTORIO;
                }
            }
            catch (Exception e)
            {
                ErrorFX.mostrar(e, true, true, "frmComprobantePago - 113 ");
            }
        }

        private void emisor_SelectedIndexChanged(object sender, EventArgs e)
        {
            cargarSerie();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            procesar();
        }
    }
}
