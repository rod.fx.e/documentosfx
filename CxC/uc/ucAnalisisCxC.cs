﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using CFDI33;
using System.Xml.Serialization;
using System.IO;
using System.Xml;
using System.Xml.Linq;
using Generales;

namespace CxC.uc
{
    public partial class ucAnalisisCxC : UserControl
    {
        public ucAnalisisCxC()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            cargar_archivos();
        }

        private void cargar_archivos()
        {
            try
            {
                using (var fbd = new FolderBrowserDialog())
                {
                    DialogResult result = fbd.ShowDialog();
                    directorio.Text = fbd.SelectedPath;
                    if (result == DialogResult.OK && !string.IsNullOrWhiteSpace(fbd.SelectedPath))
                    {
                        directorio.Text = fbd.SelectedPath;
                    }
                }

            }
            catch
            {

            }
        }

        private void informacionCFDI(string filename)
        {
            try
            {
                Comprobante oComprobante;
                //Pasear el comprobante
                int n = dtg.Rows.Add();
                try
                {
                    XmlSerializer serializer_CFD_3 = new XmlSerializer(typeof(Comprobante));
                    TextReader reader = new StreamReader(filename);
                    oComprobante = (Comprobante)serializer_CFD_3.Deserialize(reader);
                    reader.Dispose();
                    reader.Close();
                    
                    dtg.Rows[n].Tag = filename;


                    dtg.Rows[n].Cells["rfcReceptor"].Value = oComprobante.Receptor.Rfc;
                    dtg.Rows[n].Cells["nombreReceptor"].Value = oComprobante.Receptor.Nombre;

                    dtg.Rows[n].Cells["MetodoPago"].Value = oComprobante.MetodoPago;
                    dtg.Rows[n].Cells["Serie"].Value = oComprobante.Serie;
                    dtg.Rows[n].Cells["Folio"].Value = oComprobante.Folio;
                    dtg.Rows[n].Cells["FormaPago"].Value = oComprobante.FormaPago;
                    dtg.Rows[n].Cells["Moneda"].Value = oComprobante.Moneda;

                    dtg.Rows[n].Cells["tipoCambio"].Value = "1";
                    try
                    {
                        dtg.Rows[n].Cells["tipoCambio"].Value = oComprobante.TipoCambio.ToString();
                    }
                    catch
                    {

                    }


                    dtg.Rows[n].Cells["usoCFDI"].Value = oComprobante.Receptor.UsoCFDI;

                    dtg.Rows[n].Cells["SubTotal"].Value = oComprobante.SubTotal.ToString();
                    dtg.Rows[n].Cells["Total"].Value = oComprobante.Total.ToString();

                    if (oComprobante.Impuestos != null)
                    {
                        if (oComprobante.Impuestos.TotalImpuestosTrasladadosSpecified)
                        {
                            dtg.Rows[n].Cells["IVA"].Value = oComprobante.Impuestos.TotalImpuestosTrasladados;
                        }

                        if (oComprobante.Impuestos.TotalImpuestosRetenidosSpecified)
                        {
                            dtg.Rows[n].Cells["Retenido"].Value = oComprobante.Impuestos.TotalImpuestosRetenidos;
                        }

                    }



                    //Determinar si es Timbrado o Nomina12
                    foreach (ComprobanteComplemento complemento in oComprobante.Complemento)
                    {
                        XmlElement elemento = (XmlElement)complemento.Any[0];

                        //Si es nomina buscar los elementos con Linq
                        if (elemento.OuterXml.Contains("nomina12:Nomina"))
                        {


                        }
                        //Timbrado
                        if (elemento.OuterXml.Contains("tfd:TimbreFiscalDigital"))
                        {
                            var doc = XDocument.Parse(elemento.OuterXml);
                            string uuidtr = doc.Root.Attributes("UUID").First().Value.ToString();
                            dtg.Rows[n].Cells["uuid"].Value = uuidtr;
                            string FechaTimbradotr = doc.Root.Attributes("FechaTimbrado").First().Value.ToString();
                            dtg.Rows[n].Cells["timbrado"].Value = FechaTimbradotr;
                        }

                    }



                    dtg.Rows[n].Cells["estado"].Value = "Valido";



                    //Cargar información de las lineas
                    if (procesarLineaCFDI.Checked)
                    {
                        bool agregarLinea = false;
                        foreach (ComprobanteConcepto linea in oComprobante.Conceptos)
                        {
                            if (agregarLinea)
                            {
                                //Cambiar el stilo de la primera linea
                                dtg.Rows[n].DefaultCellStyle.BackColor = Color.LightGray;

                                n = dtg.Rows.Add();
                                dtg.Rows[n].Tag = dtg.Rows[n - 1].Tag;
                                //Copiar la información de la linea
                                for (int i = 0; i < dtg.Columns.Count; i++)
                                {
                                    dtg.Rows[n].Cells[i].Value = dtg.Rows[n - 1].Cells[i].Value;
                                }

                            }
                            dtg.Rows[n].Cells["ClaveProdServ"].Value = linea.ClaveProdServ;
                            dtg.Rows[n].Cells["ClaveUnidad"].Value = linea.ClaveUnidad;
                            dtg.Rows[n].Cells["Descripcion"].Value = linea.Descripcion;
                            dtg.Rows[n].Cells["ValorUnitario"].Value = linea.ValorUnitario;
                            dtg.Rows[n].Cells["Importe"].Value = linea.Importe;
                            decimal totalLineatrasladado = 0;
                            if (linea.Impuestos != null)
                            {
                                if (linea.Impuestos.Traslados != null)
                                {
                                    foreach (ComprobanteConceptoImpuestosTraslado lineaTraslados in linea.Impuestos.Traslados)
                                    {
                                        if (lineaTraslados.ImporteSpecified)
                                        {
                                            totalLineatrasladado += decimal.Parse(lineaTraslados.Importe.ToString());
                                        }

                                    }
                                }
                            }
                            //else
                            //{
                            //    string value = File.ReadAllText(filename);
                            //    XmlDocument xml = new XmlDocument();
                            //    xml.LoadXml(value);


                            //    XmlNodeList elemList = xml.GetElementsByTagName("cfdi:Impuestos");
                            //    for (int i = 0; i < elemList.Count; i++)
                            //    {
                            //        if (elemList[i].Attributes["TotalImpuestosTrasladados"] != null)
                            //        {
                            //            totalLineatrasladado += decimal.Parse(elemList[i].Attributes["TotalImpuestosTrasladados"].Value);
                            //        }

                            //    }


                            //}

                            dtg.Rows[n].Cells["trasladoLinea"].Value = totalLineatrasladado;

                            decimal totalLineaRetencion = 0;
                            if (linea.Impuestos != null)
                            {
                                if (linea.Impuestos.Retenciones != null)
                                {
                                    foreach (ComprobanteConceptoImpuestosRetencion lineaRetencion in linea.Impuestos.Retenciones)
                                    {
                                        totalLineaRetencion += decimal.Parse(lineaRetencion.Importe.ToString());
                                    }
                                }
                            }
                            //else
                            //{
                            //    //Buscar el nodo de Impuestos
                            //    string value = File.ReadAllText(filename);
                            //    XmlDocument xml = new XmlDocument();
                            //    xml.LoadXml(value);


                            //    XmlNodeList elemList = xml.GetElementsByTagName("cfdi:Impuestos");
                            //    for (int i = 0; i < elemList.Count; i++)
                            //    {
                            //        if (elemList[i].Attributes["TotalImpuestosRetenidos"] != null)
                            //        {
                            //            totalLineaRetencion += decimal.Parse(elemList[i].Attributes["TotalImpuestosRetenidos"].Value);
                            //        }

                            //    }

                            //}
                            dtg.Rows[n].Cells["retencionLinea"].Value = totalLineaRetencion;

                            agregarLinea = true;
                        }
                    }

                }
                catch (Exception e)
                {
                    dtg.Rows[n].ErrorText = "Cargar Comprobante Automatica: El archivo " + filename + " no es un CFDI válido.";
                    dtg.Rows[n].Cells["estado"].Value = "No valido";
                }

                
                dtg.AutoResizeColumns(DataGridViewAutoSizeColumnsMode.Fill);
            }
            catch
            {

            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            ExcelFX.exportar(dtg, "CFDI" + DateTime.Now.ToShortDateString());
        }

        private void button3_Click(object sender, EventArgs e)
        {
            procesar();
        }

        private void procesar()
        {
            dtg.Rows.Clear();
            string[] allfiles = System.IO.Directory.GetFiles(directorio.Text, "*.xml", System.IO.SearchOption.AllDirectories);
            foreach (var file in allfiles)
            {
                FileInfo info = new FileInfo(file);
                informacionCFDI(file);
            }
            dtg.AutoResizeColumns(DataGridViewAutoSizeColumnsMode.AllCells);
        }
    }
}
