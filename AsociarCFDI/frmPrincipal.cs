#region Copyright Syncfusion Inc. 2001 - 2017
// Copyright Syncfusion Inc. 2001 - 2017. All rights reserved.
// Use of this code is subject to the terms of our license.
// A copy of the current license can be obtained at any time by e-mailing
// licensing@syncfusion.com. Any infringement will be prosecuted under
// applicable laws. 
#endregion
using CFDI33;
using Generales;
using ModeloDocumentosFX;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.Entity.Infrastructure;
using System.Data.Entity.Validation;
using System.Drawing;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Xml;
using System.Xml.Linq;
using System.Xml.Serialization;

namespace AsociarCFDI
{
    public partial class frmPrincipal : Syncfusion.Windows.Forms.MetroForm
    {
        private string tipo;
        private string documento;
        private string aplicacion;
        private string usuario;

        public frmPrincipal()
        {
            InitializeComponent();
            if (System.Diagnostics.Debugger.IsAttached)
            {
                this.TopMost = false;
            }
        }

        public frmPrincipal(string tipo, string documento, string aplicacion, string usuario)
        {
            this.tipo = tipo;
            this.documento = documento;
            this.aplicacion = aplicacion;
            this.usuario = usuario;
            InitializeComponent();

            if (System.Diagnostics.Debugger.IsAttached)
            {
                this.TopMost = false;
            }

            tipoDocumento.Text = tipo;
            voucherId.Text = documento;
            usuariot.Text = usuario;
            if (tipo.Equals("BAJ"))
            {
                voucherId.Enabled = true;
                voucherId.Text = "";
            }
            cargar();
        }

        private void cargar()
        {
            try
            {
                string voucherIdTr = voucherId.Text;
                ModeloDocumentosFX.ModeloDocumentosFXContainer dbContext = new ModeloDocumentosFXContainer();
                var result = from b in dbContext.cxpConciliacionSet
                            .Where(
                            a =>
                            a.voucherId.Equals(voucherIdTr)
                            )
                            .OrderBy(a => a.Id)
                             select b;


                List<cxpConciliacion> dt = result.ToList();
                dtgrdGeneral.Rows.Clear();
                decimal totaltr = 0;
                foreach (cxpConciliacion registroTr in dt)
                {
                    if (registroTr != null)
                    {
                        int n = dtgrdGeneral.Rows.Add();
                        dtgrdGeneral.Rows[n].Tag = registroTr;
                        

                        dtgrdGeneral.Rows[n].Cells["rfcEmisor"].Value = registroTr.rfcEmisor;
                        dtgrdGeneral.Rows[n].Cells["UUDI"].Value = registroTr.uuid;

                        
                    }

                }
                dbContext.Dispose();
                this.dtgrdGeneral.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.DisplayedCells;

            }
            catch(Exception error)
            {
                ErrorFX.mostrar(error, true, true, true);
            }
        }

        private void buttonAdv2_Click(object sender, EventArgs e)
        {
            cargar_archivos();
        }

        private void cargar_archivos()
        {
            OpenFileDialog openFileDialog = new OpenFileDialog();
            openFileDialog.Multiselect = true;
            openFileDialog.Filter = "Archivos CFDI  (*.xml)|*.xml";
            openFileDialog.InitialDirectory = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments);
            if (openFileDialog.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                decimal totaltr = 0;
                //dtgrdGeneral.Rows.Clear();
                foreach (string filename in openFileDialog.FileNames)
                {
                    

                    Comprobante oComprobante;
                    //Pasear el comprobante
                    try
                    {
                        XmlSerializer serializer_CFD_3 = new XmlSerializer(typeof(Comprobante));
                        TextReader reader = new StreamReader(filename);
                        oComprobante = (Comprobante)serializer_CFD_3.Deserialize(reader);
                        reader.Dispose();
                        reader.Close();

                        XmlDocument doc = new XmlDocument();
                        doc.Load(filename);


                        XmlNodeList TimbreFiscalDigital = doc.GetElementsByTagName("tfd:TimbreFiscalDigital");
                        XmlNodeList comprobante = doc.GetElementsByTagName("cfdi:Comprobante");

                        string total = "";
                        try
                        {
                            total = comprobante[0].Attributes["Total"].Value;
                        }
                        catch
                        {

                        }

                        string moneda = "";
                        try
                        {
                            moneda = comprobante[0].Attributes["Moneda"].Value;
                        }
                        catch
                        {

                        }

                        string UUID = "";

                        try
                        {
                            UUID = TimbreFiscalDigital[0].Attributes["UUID"].Value;
                        }
                        catch
                        {

                        }
                        int n = dtgrdGeneral.Rows.Add();
                        dtgrdGeneral.Rows[n].Tag = filename;

                        dtgrdGeneral.Rows[n].Cells["rfcEmisor"].Value = oComprobante.Emisor.Rfc;
                        dtgrdGeneral.Rows[n].Cells["UUDI"].Value = UUID;
                        dtgrdGeneral.Rows[n].Cells["moneda"].Value = moneda;
                        dtgrdGeneral.Rows[n].Cells["total"].Value = total;
                        totaltr += decimal.Parse(total);

                        this.dtgrdGeneral.Columns["total"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
                        this.dtgrdGeneral.Columns["total"].DefaultCellStyle.Format = "c2";
                        this.dtgrdGeneral.Columns["total"].DefaultCellStyle.FormatProvider = CultureInfo.GetCultureInfo("es-MX");

                    }
                    catch (Exception e)
                    {
                        ErrorFX.mostrar(e, true, true);
                    }

                }

                //Total

                lblTotal.Text = totaltr.ToString();

            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            cargar_archivos();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            guardar();
        }

        private void guardar()
        {

            ModeloDocumentosFX.ModeloDocumentosFXContainer dbContext = new ModeloDocumentosFXContainer();
            try
            {
                if (voucherId.Text.Trim().Length == 0)
                {
                    MessageBox.Show("Debes ingresar el ajuste");
                    voucherId.Focus();
                    return;
                }
                //Preguntar si existe
                string existetr = "";
                foreach (DataGridViewRow r in dtgrdGeneral.Rows)
                {
                    string uuidtr=r.Cells[2].Value.ToString();
                    var result = from b in dbContext.cxpConciliacionSet
                                             .Where(a => a.uuid.Contains(uuidtr))
                                 select b;
                    if (result!=null)
                    {
                        List<cxpConciliacion> dt = result.ToList();
                        if (dt.Count>0)
                        {

                            existetr+=Environment.NewLine + 
                                " El folio fiscal existen en: " + dt[0].tipoDocumento.ToString() + " Documento: " + dt[0].voucherId.ToString();

                        }
                    }

                }

                if (existetr!="")
                {
                    MessageBox.Show(existetr
                        + Environment.NewLine +
                                " Por favor validar de cada uno de los XML ");
                    return;
                }
                else
                {
                    foreach (DataGridViewRow r in dtgrdGeneral.Rows)
                    {
                        
                        cxpConciliacion registro = new cxpConciliacion();
                        registro.uuid = r.Cells[2].Value.ToString();
                        registro.tipoDocumento = tipoDocumento.Text;
                        registro.voucherId = voucherId.Text;
                        registro.rfcEmisor= r.Cells["rfcEmisor"].Value.ToString(); ;
                        registro.usuario = usuario;
                        registro.fechaCreacion = DateTime.Now;
                        dbContext.cxpConciliacionSet.Add(registro);
                        dbContext.SaveChanges();
                    }

                    MessageBox.Show(this, "Datos guardados", Application.ProductName + "-" + Application.ProductVersion, MessageBoxButtons.OK, MessageBoxIcon.Information);
                    //Application.Exit();
                }
               
            }
            catch (DbUpdateConcurrencyException e)
            {
                ErrorFX.mostrar(e, true, true, true);
            }
            catch (Exception e)
            {
                ErrorFX.mostrar(e, true, true, true);
            }
        }

        private void button4_Click(object sender, EventArgs e)
        {
            eliminarSeleccionados();
        }

        private void eliminarSeleccionados()
        {
            dtgrdGeneral.EndEdit();
            if (dtgrdGeneral.SelectedCells.Count > 0)
            {
                DialogResult Resultado = MessageBox.Show("�Desea eliminar las seleccionada?", Application.ProductName + "-" + Application.ProductVersion, MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                if (Resultado.ToString() != "Yes")
                {
                    return;
                }
                foreach (DataGridViewRow drv in dtgrdGeneral.SelectedRows)
                {
                    dtgrdGeneral.Rows.Remove(drv);                    
                }
            }
            else
            {
                MessageBox.Show("Seleccione alguna l�nea.", Application.ProductName + "-" + Application.ProductVersion, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {

        }
    }
}
