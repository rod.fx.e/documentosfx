﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AsociarCFDI
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main(string[] args)
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);

            if (args.Length == 4)
            {
                //Adjuntar el CFDI para Pagos CxP
                string tipo = args[0];
                string documento = args[1];
                string aplicacion = args[3];
                string usuario = args[2]; 
                Application.Run(new frmPrincipal(tipo, documento,aplicacion,usuario));


            }
            else
            {


            }
            Application.Exit();
        }
    }
}
