#region Copyright Syncfusion Inc. 2001 - 2017
// Copyright Syncfusion Inc. 2001 - 2017. All rights reserved.
// Use of this code is subject to the terms of our license.
// A copy of the current license can be obtained at any time by e-mailing
// licensing@syncfusion.com. Any infringement will be prosecuted under
// applicable laws. 
#endregion
using DocumentosFX.clases;
using ModeloDocumentosFX;
using OfficeOpenXml;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.Entity.Validation;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace ControlGastos.formularios
{
    public partial class frmConexion : Syncfusion.Windows.Forms.MetroForm
    {
        bool modificado = false;
        configuracionConexion oRegistro;
        public frmConexion()
        {
            InitializeComponent();
            limpiar();
        }
        public frmConexion(configuracionConexion oRegistrop)
        {
            InitializeComponent();
            limpiar();
            oRegistro = oRegistrop;
            cargar();
        }
        private void buttonAdv2_Click(object sender, EventArgs e)
        {
            probar();
        }

        #region funciones
        private void probar()
        {

        }

        public void limpiar()
        {
            if (modificado)
            {

                DialogResult Resultado = MessageBox.Show(this, "�Desea guardar los cambios?", Application.ProductName + "-" + Application.ProductVersion, MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                if (Resultado.ToString() == "Yes")
                {
                    guardar();
                }
            }
            oRegistro = new configuracionConexion();
            modificado = false;
            foreach (var c in this.Controls)
            {
                if (c is TextBox)
                {
                    ((TextBox)c).Text = "";
                }
            }
        }
        private void guardar()
        {

            ModeloDocumentosFXContainer dbContext = new ModeloDocumentosFXContainer();
            try
            {
                configuracionConexion registro = new configuracionConexion();
                modificado = false;
                if (oRegistro.Id != 0)
                {
                    //Cargar registro
                    int valor = oRegistro.Id;
                    registro = (from r in dbContext.configuracionConexionSet.Where
                                            (a => a.Id == valor)
                                select r).FirstOrDefault();
                    modificado = true;
                }
                registro.tipo = tipo.Text;
                registro.servidor = servidor.Text;
                registro.baseDatos = baseDatos.Text;
                registro.usuario = usuario.Text;
                registro.password = password.Text;

                registro.visualEntity = visualEntity.Text;
                registro.visualSite = visualSite.Text;
                registro.visualBDAuxiliar = visualBDAuxiliar.Text;

                if (!modificado)
                {
                    dbContext.configuracionConexionSet.Add(registro);
                }
                else
                {
                    dbContext.configuracionConexionSet.Attach(registro);
                    dbContext.Entry(registro).State = System.Data.Entity.EntityState.Modified;
                }

                dbContext.SaveChanges();
                oRegistro = registro;
                modificado = false;
                MessageBox.Show(this, "Datos guardados", Application.ProductName + "-" + Application.ProductVersion, MessageBoxButtons.OK, MessageBoxIcon.Information);
                DialogResult = DialogResult.OK;
            }
            catch (DbEntityValidationException e)
            {
                foreach (var eve in e.EntityValidationErrors)
                {
                    foreach (var ve in eve.ValidationErrors)
                    {
                        MessageBox.Show(this, ve.PropertyName + " Error: " + ve.ErrorMessage, Application.ProductName
                            , MessageBoxButtons.OK, MessageBoxIcon.Error);
                        return;
                    }
                }
            }
        }
        private void cargar()
        {
            if (oRegistro.Id != 0)
            {
                tipo.Text = oRegistro.tipo;
                servidor.Text = oRegistro.servidor;
                baseDatos.Text = oRegistro.baseDatos;
                usuario.Text = oRegistro.usuario;
                password.Text = oRegistro.password;

                visualEntity.Text = oRegistro.visualEntity;
                visualSite.Text = oRegistro.visualSite;
                visualBDAuxiliar.Text = oRegistro.visualBDAuxiliar;
            }
        }
        private void eliminar()
        {
            DialogResult Resultado = MessageBox.Show(this, "�Esta seguro de eliminar?", Application.ProductName + "-" + Application.ProductVersion, MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            if (Resultado.ToString() != "Yes")
            {
                return;
            }
            ModeloDocumentosFXContainer dbContext = new ModeloDocumentosFXContainer();
            dbContext.configuracionConexionSet.Attach(oRegistro);
            dbContext.configuracionConexionSet.Remove(oRegistro);
            dbContext.SaveChanges();
            MessageBox.Show(this, "Eliminado.", Application.ProductName + "-" + Application.ProductVersion, MessageBoxButtons.OK, MessageBoxIcon.Information);
            limpiar();
        }
        #endregion

        private void btnAddItem_Click(object sender, EventArgs e)
        {
            guardar();
        }

        private void btnDeleteItem_Click(object sender, EventArgs e)
        {
            eliminar();
        }

        private void buttonAdv1_Click(object sender, EventArgs e)
        {
            limpiar();
        }
    }
}
