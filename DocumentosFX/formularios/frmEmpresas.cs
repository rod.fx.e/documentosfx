#region Copyright Syncfusion Inc. 2001 - 2017
// Copyright Syncfusion Inc. 2001 - 2017. All rights reserved.
// Use of this code is subject to the terms of our license.
// A copy of the current license can be obtained at any time by e-mailing
// licensing@syncfusion.com. Any infringement will be prosecuted under
// applicable laws. 
#endregion
using DocumentosFX.clases;
using Generales;
using ModeloDocumentosFX;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.Entity.Validation;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows.Forms;


namespace PlantillaFormulario
{
    public partial class frmEmpresas : Syncfusion.Windows.Forms.MetroForm
    {
        public frmEmpresas()
        {
            InitializeComponent();
            cargar();
        }

        private void btnAddItem_Click(object sender, EventArgs e)
        {
            frmEmpresa o = new frmEmpresa();
            o.Show();
        }
        private void buscar_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)13)
            {
                cargar();
            }
        }

        #region funciones
        private void eliminar()
        {
            dtg.EndEdit();
            if (dtg.SelectedCells.Count > 0)
            {
                DialogResult Resultado = MessageBox.Show("�Desea eliminar las seleccionada?", Application.ProductName + "-" + Application.ProductVersion, MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                if (Resultado.ToString() != "Yes")
                {
                    return;
                }
                foreach (DataGridViewRow drv in dtg.SelectedRows)
                {
                    try
                    {
                        ModeloDocumentosFX.empresa oRegistroTMP = (empresa)drv.Tag;

                        ModeloDocumentosFX.ModeloDocumentosFXContainer dbContext = new ModeloDocumentosFXContainer();

                        empresa oRegistro = dbContext.empresaSet
                            .Where(a => a.Id == oRegistroTMP.Id)
                            .FirstOrDefault();

                        dbContext.empresaSet.Attach(oRegistro);
                        dbContext.empresaSet.Remove(oRegistro);
                        dbContext.SaveChanges();
                        dtg.Rows.Remove(drv);
                    }
                    catch (Exception e)
                    {
                        ErrorFX.mostrar(e, true, true, "frmEmpresa - 76 - ");
                    }
                }
            }
            else
            {
                MessageBox.Show("Seleccione alguna l�nea.", Application.ProductName + "-" + Application.ProductVersion, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }
        private void cargar()
        {
            dtg.Rows.Clear();
            ModeloDocumentosFX.ModeloDocumentosFXContainer dbContext = new ModeloDocumentosFXContainer();
            try
            {
                var result = from b in dbContext.empresaSet
                             .Where(a => a.razon.Contains(buscar.Text))
                             select b;

                List<empresa> dt = result.ToList();
                dtg.Rows.Clear();
                foreach (empresa registro in dt)
                {
                    int n = dtg.Rows.Add();
                    dtg.Rows[n].Tag = registro;
                    dtg.Rows[n].Cells["razon"].Value = registro.razon;
                    dtg.Rows[n].Cells["rfc"].Value = registro.rfc;
                }
                dbContext.Dispose();
                this.dtg.Columns[0].AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;
                this.dtg.Columns[1].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
            }
            catch (DbEntityValidationException e)
            {
                foreach (var eve in e.EntityValidationErrors)
                {
                    foreach (var ve in eve.ValidationErrors)
                    {
                        MessageBox.Show(this, ve.PropertyName + " Error: " + ve.ErrorMessage, Application.ProductName
                            , MessageBoxButtons.OK, MessageBoxIcon.Error);
                        return;
                    }
                }
            }


        }

        private void cargar_registro(DataGridViewCellEventArgs e)
        {
            if (e.RowIndex > -1)
            {
                empresa registro = (empresa)dtg.Rows[e.RowIndex].Tag;
                frmEmpresa oObjeto = new frmEmpresa(registro);
                oObjeto.ShowDialog();
                cargar();
            }
        }
        #endregion


        private void btnDeleteItem_Click(object sender, EventArgs e)
        {
            eliminar();
        }

        private void buttonAdv1_Click(object sender, EventArgs e)
        {
            cargar();
        }

        private void dtg_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            editar();
        }

        private void editar()
        {
            dtg.EndEdit();
            if (dtg.SelectedRows.Count > 0)
            {
                foreach (DataGridViewRow drv in dtg.SelectedRows)
                {
                    empresa registrotmp = (empresa)drv.Tag;
                    
                    frmEmpresa oObjeto = new frmEmpresa(registrotmp);
                    oObjeto.ShowDialog();
                    cargar();
                }
            }
            else
            {
                MessageBox.Show(this,
                    "Seleccione alguna l�nea.", Application.ProductName + "-" + Application.ProductVersion, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }
    }
}
