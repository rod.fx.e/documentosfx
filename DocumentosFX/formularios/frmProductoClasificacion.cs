#region Copyright Syncfusion Inc. 2001 - 2017
// Copyright Syncfusion Inc. 2001 - 2017. All rights reserved.
// Use of this code is subject to the terms of our license.
// A copy of the current license can be obtained at any time by e-mailing
// licensing@syncfusion.com. Any infringement will be prosecuted under
// applicable laws. 
#endregion
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using ModeloDocumentosFX;
using System.Text;
using System.Windows.Forms;
using System.Linq;
using System.Data.Entity.Validation;

namespace PlantillaFormulario
{
    public partial class frmProductoClasificacion : Syncfusion.Windows.Forms.MetroForm
    {
        bool modificado = false;
        configuracionProducto oRegistro;
        public frmProductoClasificacion()
        {
            InitializeComponent();
            limpiar();
        }
        public frmProductoClasificacion(configuracionProducto oRegistrop)
        {
            InitializeComponent();
            limpiar();
            oRegistro = oRegistrop;
            cargar();
        }

        private void buttonAdv1_Click(object sender, EventArgs e)
        {
            limpiar();
        }

        private void btnAddItem_Click(object sender, EventArgs e)
        {
            guardar();
        }

        private void btnDeleteItem_Click(object sender, EventArgs e)
        {
            eliminar();
        }

        #region Metodos
        private void cargar()
        {
            clasificacionSAT.Text = oRegistro.clasificacionSAT;
            producto.Text = oRegistro.producto;
        }

        public void limpiar()
        {
            if (modificado)
            {

                DialogResult Resultado = MessageBox.Show(this, "�Desea guardar los cambios?", Application.ProductName + "-" + Application.ProductVersion, MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                if (Resultado.ToString() == "Yes")
                {
                    guardar();
                }
            }
            oRegistro = new configuracionProducto();
            modificado = false;
            foreach (var c in this.Controls)
            {
                if (c is TextBox)
                {
                    ((TextBox)c).Text = "";
                }
            }
        }
        private void guardar()
        {

            ModeloDocumentosFX.ModeloDocumentosFXContainer dbContext = new ModeloDocumentosFXContainer();
            try
            {
                configuracionProducto registro = new configuracionProducto();
                modificado = false;
                if (oRegistro.Id != 0)
                {
                    //Cargar registro
                    int valor = oRegistro.Id;
                    registro = (from r in dbContext.configuracionProductoSet.Where
                                            (a => a.Id == valor)
                                select r).FirstOrDefault();
                    modificado = true;
                }
                registro.producto = producto.Text;
                registro.clasificacionSAT = clasificacionSAT.Text;
                registro.fechaCreacion = DateTime.Now;
                if (!modificado)
                {
                    dbContext.configuracionProductoSet.Add(registro);
                }
                else
                {
                    dbContext.configuracionProductoSet.Attach(registro);
                    dbContext.Entry(registro).State = System.Data.Entity.EntityState.Modified;
                }

                dbContext.SaveChanges();
                oRegistro = registro;
                modificado = false;
                MessageBox.Show(this, "Datos guardados", Application.ProductName + "-" + Application.ProductVersion, MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            catch (DbEntityValidationException e)
            {
                foreach (var eve in e.EntityValidationErrors)
                {
                    foreach (var ve in eve.ValidationErrors)
                    {
                        MessageBox.Show(this, ve.PropertyName + " Error: " + ve.ErrorMessage, Application.ProductName
                            , MessageBoxButtons.OK, MessageBoxIcon.Error);
                        return;
                    }
                }
            }
        }

        private void eliminar()
        {

            DialogResult Resultado = MessageBox.Show(this, "�Esta seguro de eliminar?", Application.ProductName + "-" + Application.ProductVersion, MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            if (Resultado.ToString() != "Yes")
            {
                return;
            }
            ModeloDocumentosFX.ModeloDocumentosFXContainer dbContext = new ModeloDocumentosFXContainer();
            dbContext.configuracionProductoSet.Attach(oRegistro);
            dbContext.configuracionProductoSet.Remove(oRegistro);
            dbContext.SaveChanges();
            MessageBox.Show(this, "Eliminado.", Application.ProductName + "-" + Application.ProductVersion, MessageBoxButtons.OK, MessageBoxIcon.Information);
            limpiar();
        }

        #endregion


    }
}
