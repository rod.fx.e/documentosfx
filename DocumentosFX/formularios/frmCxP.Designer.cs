#region Copyright Syncfusion Inc. 2001 - 2017
// Copyright Syncfusion Inc. 2001 - 2017. All rights reserved.
// Use of this code is subject to the terms of our license.
// A copy of the current license can be obtained at any time by e-mailing
// licensing@syncfusion.com. Any infringement will be prosecuted under
// applicable laws. 
#endregion
namespace DocumentosFX.formularios
{
    partial class frmCxP
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmCxP));
            Syncfusion.Windows.Forms.CaptionImage captionImage1 = new Syncfusion.Windows.Forms.CaptionImage();
            this.notifyIcon = new System.Windows.Forms.NotifyIcon(this.components);
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.ucListaPagos2 = new CxP.ComplementoPago.ucListaPagos();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.ucCxP1 = new DocumentosFX.uc.ucCxP();
            this.dsCxP1 = new DocumentosFX.dsCxP();
            this.tabControl1.SuspendLayout();
            this.tabPage3.SuspendLayout();
            this.tabPage1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dsCxP1)).BeginInit();
            this.SuspendLayout();
            // 
            // notifyIcon
            // 
            this.notifyIcon.Icon = ((System.Drawing.Icon)(resources.GetObject("notifyIcon.Icon")));
            this.notifyIcon.Text = "Sincronización CxP";
            this.notifyIcon.Visible = true;
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage3);
            this.tabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl1.Location = new System.Drawing.Point(0, 0);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(1023, 478);
            this.tabControl1.TabIndex = 0;
            // 
            // tabPage3
            // 
            this.tabPage3.Controls.Add(this.ucListaPagos2);
            this.tabPage3.Location = new System.Drawing.Point(4, 22);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage3.Size = new System.Drawing.Size(1015, 452);
            this.tabPage3.TabIndex = 2;
            this.tabPage3.Text = "Lista de Pagos";
            this.tabPage3.UseVisualStyleBackColor = true;
            // 
            // ucListaPagos2
            // 
            this.ucListaPagos2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ucListaPagos2.Location = new System.Drawing.Point(3, 3);
            this.ucListaPagos2.Name = "ucListaPagos2";
            this.ucListaPagos2.Size = new System.Drawing.Size(1009, 446);
            this.ucListaPagos2.TabIndex = 0;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.ucCxP1);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(1015, 452);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Conciliación con el Portal";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // ucCxP1
            // 
            this.ucCxP1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ucCxP1.Location = new System.Drawing.Point(3, 3);
            this.ucCxP1.Margin = new System.Windows.Forms.Padding(2);
            this.ucCxP1.Name = "ucCxP1";
            this.ucCxP1.Size = new System.Drawing.Size(1009, 446);
            this.ucCxP1.TabIndex = 0;
            // 
            // dsCxP1
            // 
            this.dsCxP1.DataSetName = "dsCxP";
            this.dsCxP1.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // frmCxP
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CaptionBarColor = System.Drawing.Color.FromArgb(((int)(((byte)(77)))), ((int)(((byte)(168)))), ((int)(((byte)(29)))));
            this.CaptionBarHeight = 80;
            this.CaptionButtonColor = System.Drawing.Color.White;
            captionImage1.BackColor = System.Drawing.Color.Transparent;
            captionImage1.Image = global::DocumentosFX.Properties.Resources.cxpAsset_2_2x;
            captionImage1.Location = new System.Drawing.Point(30, 5);
            captionImage1.Name = "CaptionImage1";
            captionImage1.Size = new System.Drawing.Size(176, 70);
            this.CaptionImages.Add(captionImage1);
            this.ClientSize = new System.Drawing.Size(1023, 478);
            this.Controls.Add(this.tabControl1);
            this.Margin = new System.Windows.Forms.Padding(2);
            this.Name = "frmCxP";
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.tabControl1.ResumeLayout(false);
            this.tabPage3.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dsCxP1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.NotifyIcon notifyIcon;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private uc.ucCxP ucCxP1;
        private System.Windows.Forms.TabPage tabPage3;
        private dsCxP dsCxP1;
        private CxP.ComplementoPago.ucListaPagos ucListaPagos2;
    }
}