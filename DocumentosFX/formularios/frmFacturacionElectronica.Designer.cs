#region Copyright Syncfusion Inc. 2001 - 2017
// Copyright Syncfusion Inc. 2001 - 2017. All rights reserved.
// Use of this code is subject to the terms of our license.
// A copy of the current license can be obtained at any time by e-mailing
// licensing@syncfusion.com. Any infringement will be prosecuted under
// applicable laws. 
#endregion
namespace DocumentosFX.formularios
{
    partial class frmFacturacionElectronica
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Syncfusion.Windows.Forms.CaptionImage captionImage1 = new Syncfusion.Windows.Forms.CaptionImage();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.ucPlantillaCOVE1 = new DocumentosFX.uc.ucPlantillaCOVE();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.tabPage2.SuspendLayout();
            this.tabPage3.SuspendLayout();
            this.SuspendLayout();
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Controls.Add(this.tabPage3);
            this.tabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl1.Location = new System.Drawing.Point(0, 0);
            this.tabControl1.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(766, 362);
            this.tabControl1.TabIndex = 0;

           // 
            // tabPage3
            // 
            this.tabPage3.Controls.Add(this.ucPlantillaCOVE1);
            this.tabPage3.Location = new System.Drawing.Point(4, 22);
            this.tabPage3.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Padding = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.tabPage3.Size = new System.Drawing.Size(758, 336);
            this.tabPage3.TabIndex = 2;
            this.tabPage3.Text = "Formatos para COVE\'s";
            this.tabPage3.UseVisualStyleBackColor = true;
            // 
            // ucPlantillaCOVE1
            // 
            this.ucPlantillaCOVE1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ucPlantillaCOVE1.Location = new System.Drawing.Point(2, 2);
            this.ucPlantillaCOVE1.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.ucPlantillaCOVE1.Name = "ucPlantillaCOVE1";
            this.ucPlantillaCOVE1.Size = new System.Drawing.Size(754, 332);
            this.ucPlantillaCOVE1.TabIndex = 0;
            // 
            // frmFacturacionElectronica
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CaptionBarColor = System.Drawing.Color.FromArgb(((int)(((byte)(124)))), ((int)(((byte)(54)))), ((int)(((byte)(145)))));
            this.CaptionBarHeight = 80;
            captionImage1.BackColor = System.Drawing.Color.Transparent;
            captionImage1.Image = global::DocumentosFX.Properties.Resources.facturacionelectronicaAsset_5_2x;
            captionImage1.Location = new System.Drawing.Point(30, 5);
            captionImage1.Name = "CaptionImage1";
            captionImage1.Size = new System.Drawing.Size(176, 70);
            this.CaptionImages.Add(captionImage1);
            this.ClientSize = new System.Drawing.Size(766, 362);
            this.Controls.Add(this.tabControl1);
            this.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.Name = "frmFacturacionElectronica";
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage2.ResumeLayout(false);
            this.tabPage3.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.TabPage tabPage3;
        private uc.ucPlantillaCOVE ucPlantillaCOVE1;
    }
}