#region Copyright Syncfusion Inc. 2001 - 2017
// Copyright Syncfusion Inc. 2001 - 2017. All rights reserved.
// Use of this code is subject to the terms of our license.
// A copy of the current license can be obtained at any time by e-mailing
// licensing@syncfusion.com. Any infringement will be prosecuted under
// applicable laws. 
#endregion
using ModeloDocumentosFX;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Linq;
using System.Drawing;

using System.Text;
using System.Windows.Forms;
using System.Data.Entity.Validation;
using ControlGastos.formularios;
using DocumentosFX.clases;
using Generales;

namespace PlantillaFormulario
{
    public partial class frmEmpresa : Syncfusion.Windows.Forms.MetroForm
    {
        bool modificado = false;
        empresa oRegistro;
        private object frmConexcion;

        public frmEmpresa()
        {
            InitializeComponent();
            oRegistro = new ModeloDocumentosFX.empresa();
            limpiar();

        }
        public frmEmpresa(empresa oRegistrop)
        {
            InitializeComponent();
            limpiar();
            oRegistro = oRegistrop;
            cargar();
        }
        #region funciones

        
        public void limpiar()
        {
            if (modificado)
            {

                DialogResult Resultado = MessageBox.Show(this, "�Desea guardar los cambios?", Application.ProductName + "-" + Application.ProductVersion, MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                if (Resultado.ToString() == "Yes")
                {
                    guardar();
                }
            }
            cargarRegimenFiscal();
            oRegistro = new empresa();
            modificado = false;
            foreach (var c in this.Controls)
            {
                if (c is TextBox)
                {
                    ((TextBox)c).Text = "";
                }
            }
            empresa.Enabled = true;
        }

        private void cargarRegimenFiscal()
        {
            try
            {
                //using (ModeloDocumentosFXContainer c = new ModeloDocumentosFXContainer(Properties.Settings.Default.Connection))
                //{
                //    //List<Customer> usepurposes = c.Customers.ToList();
                //    //DataTable dt = new DataTable();
                //    //dt.Columns.Add("id");
                //    //dt.Columns.Add("name");
                //    //foreach (Customer usepurpose in usepurposes)
                //    //{
                //    //    dt.Rows.Add(usepurpose.id, usepurpose.name);
                //    //}
                //    //comboBox1.ValueMember = dt.Columns[0].ColumnName;
                //    //comboBox1.DisplayMember = dt.Columns[1].ColumnName;
                //    //comboBox1.DataSource = dt;

                //}
            }
            catch (Exception e)
            {
                ErrorFX.mostrar(e, false, true, "frmEmpresa - 100 - ");
            }
        }

        private void guardar()
        {

            ModeloDocumentosFXContainer dbContext = new ModeloDocumentosFXContainer();
            try
            {
                empresa registro = new empresa();
                modificado = false;
                if (oRegistro.Id != 0)
                {
                    //Cargar registro
                    int valor = oRegistro.Id;
                    registro = (from r in dbContext.empresaSet.Where
                                            (a => a.Id == valor)
                                select r).FirstOrDefault();
                    modificado = true;
                }
                registro.razon = empresa.Text;
                registro.rfc = rfc.Text;

                registro.regimenFiscal = regimenFiscal.Text;

                registro.estado = "INACTIVO";
                if (activo.Checked)
                {
                    registro.estado = "ACTIVO";
                }
                //registro.entidad = entidad.Text;
                
                //registro.activo = activo.Text;
                //registro.site = site.Text;
                //registro.bdauxiliar = bdauxiliar.Text;
                //registro.calle = calle.Text;
                //registro.exterior = exterior.Text;
                //registro.interior = interior.Text;
                //registro.colonia = colonia.Text;
                //registro.cp = cp.Text;
                //registro.localidad = localidad.Text;
                //registro.referencia = referencia.Text;
                //registro.pais = pais.Text;
                //registro.estado = estado.Text;
                //registro.municipio = municipio.Text;
                //registro.smtp = smtp.Text;
                //registro.usuario = usuario.Text;
                //registro.contrace�a = contrace�a.Text;
                //registro.puertasalida = puertasalida.Text;
                //registro.conexionS = conexionS.Text;
                //registro.autentificacion = autentificacion.Text;
                //registro.tipoconexion = tipoconexion.Text;
                //registro.servidorconexion = servidorconexion.Text;
                //registro.bd = bd.Text;
                //registro.usuarioconexion = usuarioconexion.Text;
                //registro.password = password.Text;
                //registro.tiempoaproximacion = tiempoaproximacion.Text;
                //registro.valoresdecimales = valoresdecimales.Text;
                //registro.clientes = clientes.Text;
                //registro.aplicardescuento = aplicardescuento.Text;
                //registro.lineafactura = lineafactura.Text;


                if (!modificado)
                {
                    dbContext.empresaSet.Add(registro);
                }
                else
                {
                    dbContext.empresaSet.Attach(registro);
                    dbContext.Entry(registro).State = System.Data.Entity.EntityState.Modified;
                }

                dbContext.SaveChanges();
                oRegistro = registro;
                modificado = false;
                MessageBox.Show(this, "Datos guardados", Application.ProductName + "-" + Application.ProductVersion, MessageBoxButtons.OK, MessageBoxIcon.Information);
                DialogResult = DialogResult.OK;
            }
            catch (DbEntityValidationException e)
            {
                foreach (var eve in e.EntityValidationErrors)
                {
                    foreach (var ve in eve.ValidationErrors)
                    {
                        MessageBox.Show(this, ve.PropertyName + " Error: " + ve.ErrorMessage, Application.ProductName
                            , MessageBoxButtons.OK, MessageBoxIcon.Error);
                        return;
                    }
                }
            }
        }
        private void cargar()
        {
            if (oRegistro.Id != 0)
            {
                empresa.Enabled = false;
            }

            empresa.Text = oRegistro.razon;
            rfc.Text = oRegistro.rfc;

            //entidad.Text = oRegistro.entidad;
            //empresa.Text = oRegistro.empresa;
            //rfc.Text = oRegistro.rfc;
            //activo.Text = oRegistro.activo;
            //site.Text = oRegistro.site;
            //bdauxiliar.Text = oRegistro.bdauxiliar;
            //regimenfiscal.Text = oRegistro.regimenfiscal;
            //calle.Text = oRegistro.calle;
            //exterior.Text = oRegistro.exterior;
            //interior.Text = oRegistro.interior;
            //colonia.Text = oRegistro.colonia;
            //cp.Text = oRegistro.cp;
            //localidad.Text = oRegistro.localidad;
            //referencia.Text = oRegistro.referencia;
            //pais.Text = oRegistro.pais;
            //estado.Text = oRegistro.regimenfiscal;
            //municipio.Text = oRegistro.municipio;
            //smtp.Text = oRegistro.smtp;
            //usuario.Text = oRegistro.usuario;
            //contrace�a.Text = oRegistro.contrace�a;
            //puertasalida.Text = oRegistro.puertasalida;
            //conexionS.Text = oRegistro.conexionS;
            //autentificacion.Text = oRegistro.autentificacion;
            //tipoconexion.Text = oRegistro.tipoconexion;
            //servidorconexion.Text = oRegistro.servidorconexion;
            //bd.Text = oRegistro.bd;
            //usuarioconexion.Text = oRegistro.usuarioconexion;
            //password.Text = oRegistro.password;
            //tiempoaproximacion.Text = oRegistro.tiempoaproximacion;
            //valoresdecimales.Text = oRegistro.valoresdecimales;
            //clientes.Text = oRegistro.clientes;
            //aplicardescuento.Text = oRegistro.aplicardescuento;
            //lineafactura.Text = oRegistro.lineafactura;

        }
        private void eliminar()
        {
            DialogResult Resultado = MessageBox.Show(this, "�Esta seguro de eliminar?", Application.ProductName + "-" + Application.ProductVersion, MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            if (Resultado.ToString() != "Yes")
            {
                return;
            }
            ModeloDocumentosFXContainer dbContext = new ModeloDocumentosFXContainer();
            dbContext.empresaSet.Attach(oRegistro);
            dbContext.empresaSet.Remove(oRegistro);
            dbContext.SaveChanges();
            MessageBox.Show(this, "Eliminado.", Application.ProductName + "-" + Application.ProductVersion, MessageBoxButtons.OK, MessageBoxIcon.Information);
            limpiar();
        }
        #endregion
        private void btnAddItem_Click(object sender, EventArgs e)
        {
            guardar();
        }

        private void btnDeleteItem_Click(object sender, EventArgs e)
        {
            eliminar();
        }

        private void buttonAdv4_Click(object sender, EventArgs e)
        {
            frmConexion o = new frmConexion();
            o.Show();
        }

        private void buttonAdv5_Click(object sender, EventArgs e)
        {
            
        }
    }
}
