#region Copyright Syncfusion Inc. 2001 - 2017
// Copyright Syncfusion Inc. 2001 - 2017. All rights reserved.
// Use of this code is subject to the terms of our license.
// A copy of the current license can be obtained at any time by e-mailing
// licensing@syncfusion.com. Any infringement will be prosecuted under
// applicable laws. 
#endregion
using DocumentosFX.clases;
using FE_FX;
using Generales;
using ModeloDocumentosFX;
using PlantillaFormulario;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;

using System.Text;
using System.Windows.Forms;

namespace DocumentosFX.formularios
{
    public partial class frmPrincipal : Syncfusion.Windows.Forms.MetroForm
    {
        public frmPrincipal()
        {
            InitializeComponent();
            bienvenido.Text = "Bienvenido, " + Globales.usuario.nombreCompleto;
            cargarSeguridad();
        }

        private void cargarSeguridad()
        {
            btnCE.Enabled = Globales.usuario.contabilidadElectronica;
            btnFE.Enabled = Globales.usuario.facturacionElectronica;
            btnCxC.Enabled = Globales.usuario.cuentasCobrar;
            btnCxP.Enabled = Globales.usuario.cuentasPagar;
            btnConfiguracion.Enabled = Globales.usuario.configuracion;

        }

        private void button3_Click(object sender, EventArgs e)
        {

        }

        private void buttonAdv1_MouseHover(object sender, EventArgs e)
        {
            cargarExplicacion();
        }

        private void cargarExplicacion()
        {
            
        }

        private void buttonAdv1_Click(object sender, EventArgs e)
        {
            ModeloDocumentosFXContainer dbContext = new ModeloDocumentosFXContainer();
            try
            {
                string conection = dbContext.Database.Connection.ConnectionString;
                FE_FX.frmPrincipalFX o = new FE_FX.frmPrincipalFX(conection,Globales.usuario.usuario);
                o.Show();
            }
            catch (Exception err)
            {
                ErrorFX.mostrar(err, false, true, "frmEmpresa - 68 - ", false);
            }

        }

        private void buttonAdv2_Click(object sender, EventArgs e)
        {
            frmCxP o = new frmCxP();
            o.Show();
        }

        private void buttonAdv3_Click(object sender, EventArgs e)
        {
            CxC.frmCxC o = new CxC.frmCxC();
            o.Show();
        }

        private void buttonAdv4_Click(object sender, EventArgs e)
        {
            ModeloDocumentosFXContainer dbContext = new ModeloDocumentosFXContainer();
            try
            {
                string conection = dbContext.Database.Connection.ConnectionString;
                Desarrollo.frmMain o = new Desarrollo.frmMain(conection, Globales.usuario.nombreCompleto);
                o.Show();
            }
            catch(Exception err)
            {
                ErrorFX.mostrar(err, false, true, "frmEmpresa - 96 - ", false);
            }
        }

        private void buttonAdv5_Click_1(object sender, EventArgs e)
        {
            frmConfiguracionGeneral o = new frmConfiguracionGeneral();
            o.Show();
        }

        private void frmPrincipal_FormClosed(object sender, FormClosedEventArgs e)
        {
            Application.Exit();
        }

        private void buttonAdv1_Click_1(object sender, EventArgs e)
        {
            ModeloDocumentosFXContainer dbContext = new ModeloDocumentosFXContainer();
            try
            {
                string conection = dbContext.Database.Connection.ConnectionString;
                FE_FX.frmBandejaComprobantePago o = new FE_FX.frmBandejaComprobantePago(conection, Globales.usuario.usuario);
                o.Show();
            }
            catch (Exception err)
            {
                ErrorFX.mostrar(err, false, true, "frmEmpresa - 122 - ", false);
            }
        }

        private void buttonAdv2_Click_1(object sender, EventArgs e)
        {
            frmTraslados o = new frmTraslados();
            o.Show();
            //DescargaFX.frmPrincipalNuevo2 O = new DescargaFX.frmPrincipalNuevo2(Globales.usuario.usuario);
            //O.ShowDialog();
        }

        private void buttonAdv2_Click_2(object sender, EventArgs e)
        {
            DescargaFX.frmPrincipalNuevo2 O = new DescargaFX.frmPrincipalNuevo2(Globales.usuario.usuario);
            O.ShowDialog();
        }

        private void buttonAdv4_Click_1(object sender, EventArgs e)
        {
            FE_FX.CFDI.frmTraslados o = new FE_FX.CFDI.frmTraslados();
            o.Show();
        }

        private void buttonAdv5_Click(object sender, EventArgs e)
        {
            CxP.Formularios.GenerarCxP o = new CxP.Formularios.GenerarCxP();
            o.Show();
        }

        private void buttonAdv6_Click(object sender, EventArgs e)
        {
            CxP.Formularios.frmImpuestos o = new CxP.Formularios.frmImpuestos();
            o.Show();
        }

        private void buttonAdv7_Click(object sender, EventArgs e)
        {
            CxP.Formularios.FlujoEfectivo o = new CxP.Formularios.FlujoEfectivo();
            o.Show();
        }
    }
}
