#region Copyright Syncfusion Inc. 2001 - 2017
// Copyright Syncfusion Inc. 2001 - 2017. All rights reserved.
// Use of this code is subject to the terms of our license.
// A copy of the current license can be obtained at any time by e-mailing
// licensing@syncfusion.com. Any infringement will be prosecuted under
// applicable laws. 
#endregion
namespace ControlGastos.formularios
{
    partial class frmUsuario
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.btnDeleteItem = new Syncfusion.Windows.Forms.ButtonAdv();
            this.btnAddItem = new Syncfusion.Windows.Forms.ButtonAdv();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.password = new System.Windows.Forms.TextBox();
            this.usuario = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.nombreCompleto = new System.Windows.Forms.TextBox();
            this.correoElectronico = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.buttonAdv1 = new Syncfusion.Windows.Forms.ButtonAdv();
            this.usuarioVISUAL = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.configuracion = new System.Windows.Forms.CheckBox();
            this.contabilidadElectronica = new System.Windows.Forms.CheckBox();
            this.cuentasCobrar = new System.Windows.Forms.CheckBox();
            this.cuentasPagar = new System.Windows.Forms.CheckBox();
            this.facturacionElectronica = new System.Windows.Forms.CheckBox();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.AccesoNomina = new System.Windows.Forms.CheckBox();
            this.AccesoEgreso = new System.Windows.Forms.CheckBox();
            this.AccesoIngreso = new System.Windows.Forms.CheckBox();
            this.AccesoTraslado = new System.Windows.Forms.CheckBox();
            this.AccesoPago = new System.Windows.Forms.CheckBox();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.tabPage2.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnDeleteItem
            // 
            this.btnDeleteItem.Appearance = Syncfusion.Windows.Forms.ButtonAppearance.Metro;
            this.btnDeleteItem.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(219)))), ((int)(((byte)(94)))), ((int)(((byte)(94)))));
            this.btnDeleteItem.BeforeTouchSize = new System.Drawing.Size(94, 28);
            this.btnDeleteItem.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnDeleteItem.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnDeleteItem.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnDeleteItem.IsBackStageButton = false;
            this.btnDeleteItem.Location = new System.Drawing.Point(201, 9);
            this.btnDeleteItem.Name = "btnDeleteItem";
            this.btnDeleteItem.Size = new System.Drawing.Size(94, 28);
            this.btnDeleteItem.TabIndex = 2;
            this.btnDeleteItem.Text = "Eliminar";
            this.btnDeleteItem.Click += new System.EventHandler(this.btnDeleteItem_Click);
            // 
            // btnAddItem
            // 
            this.btnAddItem.Appearance = Syncfusion.Windows.Forms.ButtonAppearance.Metro;
            this.btnAddItem.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(153)))), ((int)(((byte)(181)))), ((int)(((byte)(105)))));
            this.btnAddItem.BeforeTouchSize = new System.Drawing.Size(91, 28);
            this.btnAddItem.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnAddItem.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAddItem.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnAddItem.IsBackStageButton = false;
            this.btnAddItem.Location = new System.Drawing.Point(104, 9);
            this.btnAddItem.Name = "btnAddItem";
            this.btnAddItem.Size = new System.Drawing.Size(91, 28);
            this.btnAddItem.TabIndex = 1;
            this.btnAddItem.Text = "Guardar";
            this.btnAddItem.Click += new System.EventHandler(this.btnAddItem_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(41, 119);
            this.label2.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(61, 13);
            this.label2.TabIndex = 9;
            this.label2.Text = "Contraseña";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(56, 94);
            this.label1.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(43, 13);
            this.label1.TabIndex = 7;
            this.label1.Text = "Usuario";
            // 
            // password
            // 
            this.password.Location = new System.Drawing.Point(104, 116);
            this.password.Margin = new System.Windows.Forms.Padding(2);
            this.password.Name = "password";
            this.password.PasswordChar = '*';
            this.password.Size = new System.Drawing.Size(114, 20);
            this.password.TabIndex = 10;
            // 
            // usuario
            // 
            this.usuario.Location = new System.Drawing.Point(104, 91);
            this.usuario.Margin = new System.Windows.Forms.Padding(2);
            this.usuario.Name = "usuario";
            this.usuario.Size = new System.Drawing.Size(114, 20);
            this.usuario.TabIndex = 8;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(9, 45);
            this.label5.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(90, 13);
            this.label5.TabIndex = 3;
            this.label5.Text = "Nombre completo";
            // 
            // nombreCompleto
            // 
            this.nombreCompleto.Location = new System.Drawing.Point(104, 42);
            this.nombreCompleto.Margin = new System.Windows.Forms.Padding(2);
            this.nombreCompleto.Name = "nombreCompleto";
            this.nombreCompleto.Size = new System.Drawing.Size(365, 20);
            this.nombreCompleto.TabIndex = 4;
            // 
            // correoElectronico
            // 
            this.correoElectronico.Location = new System.Drawing.Point(104, 67);
            this.correoElectronico.Margin = new System.Windows.Forms.Padding(2);
            this.correoElectronico.Name = "correoElectronico";
            this.correoElectronico.Size = new System.Drawing.Size(365, 20);
            this.correoElectronico.TabIndex = 6;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(9, 70);
            this.label3.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(93, 13);
            this.label3.TabIndex = 5;
            this.label3.Text = "Correo electrónico";
            // 
            // buttonAdv1
            // 
            this.buttonAdv1.Appearance = Syncfusion.Windows.Forms.ButtonAppearance.Metro;
            this.buttonAdv1.BackColor = System.Drawing.Color.SteelBlue;
            this.buttonAdv1.BeforeTouchSize = new System.Drawing.Size(91, 28);
            this.buttonAdv1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonAdv1.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonAdv1.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.buttonAdv1.IsBackStageButton = false;
            this.buttonAdv1.Location = new System.Drawing.Point(8, 9);
            this.buttonAdv1.Name = "buttonAdv1";
            this.buttonAdv1.Size = new System.Drawing.Size(91, 28);
            this.buttonAdv1.TabIndex = 0;
            this.buttonAdv1.Text = "Limpiar";
            this.buttonAdv1.Click += new System.EventHandler(this.buttonAdv1_Click);
            // 
            // usuarioVISUAL
            // 
            this.usuarioVISUAL.Location = new System.Drawing.Point(104, 141);
            this.usuarioVISUAL.Margin = new System.Windows.Forms.Padding(2);
            this.usuarioVISUAL.Name = "usuarioVISUAL";
            this.usuarioVISUAL.Size = new System.Drawing.Size(114, 20);
            this.usuarioVISUAL.TabIndex = 12;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(3, 144);
            this.label4.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(99, 13);
            this.label4.TabIndex = 11;
            this.label4.Text = "Usuario de VISUAL";
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Location = new System.Drawing.Point(8, 166);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(461, 178);
            this.tabControl1.TabIndex = 13;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.configuracion);
            this.tabPage1.Controls.Add(this.contabilidadElectronica);
            this.tabPage1.Controls.Add(this.cuentasCobrar);
            this.tabPage1.Controls.Add(this.cuentasPagar);
            this.tabPage1.Controls.Add(this.facturacionElectronica);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(453, 152);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Permisos por módulos";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // configuracion
            // 
            this.configuracion.AutoSize = true;
            this.configuracion.Location = new System.Drawing.Point(10, 98);
            this.configuracion.Name = "configuracion";
            this.configuracion.Size = new System.Drawing.Size(91, 17);
            this.configuracion.TabIndex = 4;
            this.configuracion.Text = "Configuración";
            this.configuracion.UseVisualStyleBackColor = true;
            // 
            // contabilidadElectronica
            // 
            this.contabilidadElectronica.AutoSize = true;
            this.contabilidadElectronica.Location = new System.Drawing.Point(10, 75);
            this.contabilidadElectronica.Name = "contabilidadElectronica";
            this.contabilidadElectronica.Size = new System.Drawing.Size(139, 17);
            this.contabilidadElectronica.TabIndex = 3;
            this.contabilidadElectronica.Text = "Contabilidad electrónica";
            this.contabilidadElectronica.UseVisualStyleBackColor = true;
            // 
            // cuentasCobrar
            // 
            this.cuentasCobrar.AutoSize = true;
            this.cuentasCobrar.Location = new System.Drawing.Point(10, 52);
            this.cuentasCobrar.Name = "cuentasCobrar";
            this.cuentasCobrar.Size = new System.Drawing.Size(116, 17);
            this.cuentasCobrar.TabIndex = 2;
            this.cuentasCobrar.Text = "Cuentas por cobrar";
            this.cuentasCobrar.UseVisualStyleBackColor = true;
            // 
            // cuentasPagar
            // 
            this.cuentasPagar.AutoSize = true;
            this.cuentasPagar.Location = new System.Drawing.Point(10, 29);
            this.cuentasPagar.Name = "cuentasPagar";
            this.cuentasPagar.Size = new System.Drawing.Size(113, 17);
            this.cuentasPagar.TabIndex = 1;
            this.cuentasPagar.Text = "Cuentas por pagar";
            this.cuentasPagar.UseVisualStyleBackColor = true;
            // 
            // facturacionElectronica
            // 
            this.facturacionElectronica.AutoSize = true;
            this.facturacionElectronica.Location = new System.Drawing.Point(10, 6);
            this.facturacionElectronica.Name = "facturacionElectronica";
            this.facturacionElectronica.Size = new System.Drawing.Size(137, 17);
            this.facturacionElectronica.TabIndex = 0;
            this.facturacionElectronica.Text = "Facturación electrónica";
            this.facturacionElectronica.UseVisualStyleBackColor = true;
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.AccesoNomina);
            this.tabPage2.Controls.Add(this.AccesoPago);
            this.tabPage2.Controls.Add(this.AccesoTraslado);
            this.tabPage2.Controls.Add(this.AccesoEgreso);
            this.tabPage2.Controls.Add(this.AccesoIngreso);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(453, 152);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Descarga";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // AccesoNomina
            // 
            this.AccesoNomina.AutoSize = true;
            this.AccesoNomina.Location = new System.Drawing.Point(6, 98);
            this.AccesoNomina.Name = "AccesoNomina";
            this.AccesoNomina.Size = new System.Drawing.Size(62, 17);
            this.AccesoNomina.TabIndex = 4;
            this.AccesoNomina.Text = "Nomina";
            this.AccesoNomina.UseVisualStyleBackColor = true;
            // 
            // AccesoEgreso
            // 
            this.AccesoEgreso.AutoSize = true;
            this.AccesoEgreso.Location = new System.Drawing.Point(6, 29);
            this.AccesoEgreso.Name = "AccesoEgreso";
            this.AccesoEgreso.Size = new System.Drawing.Size(59, 17);
            this.AccesoEgreso.TabIndex = 1;
            this.AccesoEgreso.Text = "Egreso";
            this.AccesoEgreso.UseVisualStyleBackColor = true;
            // 
            // AccesoIngreso
            // 
            this.AccesoIngreso.AutoSize = true;
            this.AccesoIngreso.Location = new System.Drawing.Point(6, 6);
            this.AccesoIngreso.Name = "AccesoIngreso";
            this.AccesoIngreso.Size = new System.Drawing.Size(61, 17);
            this.AccesoIngreso.TabIndex = 0;
            this.AccesoIngreso.Text = "Ingreso";
            this.AccesoIngreso.UseVisualStyleBackColor = true;
            // 
            // AccesoTraslado
            // 
            this.AccesoTraslado.AutoSize = true;
            this.AccesoTraslado.Location = new System.Drawing.Point(6, 52);
            this.AccesoTraslado.Name = "AccesoTraslado";
            this.AccesoTraslado.Size = new System.Drawing.Size(67, 17);
            this.AccesoTraslado.TabIndex = 2;
            this.AccesoTraslado.Text = "Traslado";
            this.AccesoTraslado.UseVisualStyleBackColor = true;
            // 
            // AccesoPago
            // 
            this.AccesoPago.AutoSize = true;
            this.AccesoPago.Location = new System.Drawing.Point(6, 75);
            this.AccesoPago.Name = "AccesoPago";
            this.AccesoPago.Size = new System.Drawing.Size(51, 17);
            this.AccesoPago.TabIndex = 3;
            this.AccesoPago.Text = "Pago";
            this.AccesoPago.UseVisualStyleBackColor = true;
            // 
            // frmUsuario
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(179)))), ((int)(((byte)(207)))), ((int)(((byte)(96)))));
            this.BorderThickness = 0;
            this.CaptionAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.CaptionBarColor = System.Drawing.Color.FromArgb(((int)(((byte)(179)))), ((int)(((byte)(207)))), ((int)(((byte)(96)))));
            this.CaptionBarHeight = 30;
            this.CaptionButtonColor = System.Drawing.Color.Beige;
            this.CaptionForeColor = System.Drawing.Color.Black;
            this.ClientSize = new System.Drawing.Size(478, 355);
            this.Controls.Add(this.tabControl1);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.password);
            this.Controls.Add(this.usuarioVISUAL);
            this.Controls.Add(this.usuario);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.correoElectronico);
            this.Controls.Add(this.nombreCompleto);
            this.Controls.Add(this.btnDeleteItem);
            this.Controls.Add(this.buttonAdv1);
            this.Controls.Add(this.btnAddItem);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label4);
            this.HelpButton = true;
            this.Margin = new System.Windows.Forms.Padding(2);
            this.MetroColor = System.Drawing.Color.FromArgb(((int)(((byte)(179)))), ((int)(((byte)(207)))), ((int)(((byte)(96)))));
            this.Name = "frmUsuario";
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Usuario";
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage1.PerformLayout();
            this.tabPage2.ResumeLayout(false);
            this.tabPage2.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private Syncfusion.Windows.Forms.ButtonAdv btnDeleteItem;
        private Syncfusion.Windows.Forms.ButtonAdv btnAddItem;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox password;
        private System.Windows.Forms.TextBox usuario;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox nombreCompleto;
        private System.Windows.Forms.TextBox correoElectronico;
        private System.Windows.Forms.Label label3;
        private Syncfusion.Windows.Forms.ButtonAdv buttonAdv1;
        private System.Windows.Forms.TextBox usuarioVISUAL;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.CheckBox configuracion;
        private System.Windows.Forms.CheckBox contabilidadElectronica;
        private System.Windows.Forms.CheckBox cuentasCobrar;
        private System.Windows.Forms.CheckBox cuentasPagar;
        private System.Windows.Forms.CheckBox facturacionElectronica;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.CheckBox AccesoNomina;
        private System.Windows.Forms.CheckBox AccesoEgreso;
        private System.Windows.Forms.CheckBox AccesoIngreso;
        private System.Windows.Forms.CheckBox AccesoPago;
        private System.Windows.Forms.CheckBox AccesoTraslado;
    }
}