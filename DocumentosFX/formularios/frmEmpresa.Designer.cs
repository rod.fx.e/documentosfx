#region Copyright Syncfusion Inc. 2001 - 2017
// Copyright Syncfusion Inc. 2001 - 2017. All rights reserved.
// Use of this code is subject to the terms of our license.
// A copy of the current license can be obtained at any time by e-mailing
// licensing@syncfusion.com. Any infringement will be prosecuted under
// applicable laws. 
#endregion
namespace PlantillaFormulario
{
    partial class frmEmpresa
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.label5 = new System.Windows.Forms.Label();
            this.rfc = new System.Windows.Forms.TextBox();
            this.btnDeleteItem = new Syncfusion.Windows.Forms.ButtonAdv();
            this.buttonAdv1 = new Syncfusion.Windows.Forms.ButtonAdv();
            this.btnAddItem = new Syncfusion.Windows.Forms.ButtonAdv();
            this.label3 = new System.Windows.Forms.Label();
            this.buttonAdv2 = new Syncfusion.Windows.Forms.ButtonAdv();
            this.empresa = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.activo = new System.Windows.Forms.CheckBox();
            this.ROW_ID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.VISUAL_BD = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.VISUAL_TIPO = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.VISUAL_ENTITY_ID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.VISUAL_SITE_ID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tabPage7 = new System.Windows.Forms.TabPage();
            this.buttonAdv5 = new Syncfusion.Windows.Forms.ButtonAdv();
            this.buttonAdv4 = new Syncfusion.Windows.Forms.ButtonAdv();
            this.dtgConexion = new System.Windows.Forms.DataGridView();
            this.tabPage6 = new System.Windows.Forms.TabPage();
            this.tabPage5 = new System.Windows.Forms.TabPage();
            this.tabPage4 = new System.Windows.Forms.TabPage();
            this.label24 = new System.Windows.Forms.Label();
            this.aplicardescuento = new System.Windows.Forms.CheckBox();
            this.asuntosclientes = new System.Windows.Forms.Button();
            this.lineafactura = new System.Windows.Forms.TextBox();
            this.clientes = new System.Windows.Forms.TextBox();
            this.label22 = new System.Windows.Forms.Label();
            this.label34 = new System.Windows.Forms.Label();
            this.label37 = new System.Windows.Forms.Label();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.button3 = new System.Windows.Forms.Button();
            this.checkBox3 = new System.Windows.Forms.CheckBox();
            this.conexionS = new System.Windows.Forms.CheckBox();
            this.contraceña = new System.Windows.Forms.TextBox();
            this.usuario = new System.Windows.Forms.TextBox();
            this.puertasalida = new System.Windows.Forms.TextBox();
            this.smtp = new System.Windows.Forms.TextBox();
            this.label21 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.municipio = new System.Windows.Forms.TextBox();
            this.estado = new System.Windows.Forms.TextBox();
            this.pais = new System.Windows.Forms.TextBox();
            this.referencia = new System.Windows.Forms.TextBox();
            this.localidad = new System.Windows.Forms.TextBox();
            this.cp = new System.Windows.Forms.TextBox();
            this.colonia = new System.Windows.Forms.TextBox();
            this.interior = new System.Windows.Forms.TextBox();
            this.exterior = new System.Windows.Forms.TextBox();
            this.calle = new System.Windows.Forms.TextBox();
            this.label17 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.tipo = new System.Windows.Forms.TabControl();
            this.regimenFiscal = new System.Windows.Forms.ComboBox();
            this.tabPage7.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dtgConexion)).BeginInit();
            this.tabPage4.SuspendLayout();
            this.tabPage2.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.tipo.SuspendLayout();
            this.SuspendLayout();
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(0, 66);
            this.label5.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(28, 13);
            this.label5.TabIndex = 8;
            this.label5.Text = "RFC";
            // 
            // rfc
            // 
            this.rfc.Location = new System.Drawing.Point(92, 63);
            this.rfc.Margin = new System.Windows.Forms.Padding(2);
            this.rfc.Name = "rfc";
            this.rfc.Size = new System.Drawing.Size(130, 20);
            this.rfc.TabIndex = 9;
            // 
            // btnDeleteItem
            // 
            this.btnDeleteItem.Appearance = Syncfusion.Windows.Forms.ButtonAppearance.Metro;
            this.btnDeleteItem.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(219)))), ((int)(((byte)(94)))), ((int)(((byte)(94)))));
            this.btnDeleteItem.BeforeTouchSize = new System.Drawing.Size(94, 28);
            this.btnDeleteItem.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnDeleteItem.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnDeleteItem.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnDeleteItem.IsBackStageButton = false;
            this.btnDeleteItem.Location = new System.Drawing.Point(195, 8);
            this.btnDeleteItem.Name = "btnDeleteItem";
            this.btnDeleteItem.Size = new System.Drawing.Size(94, 28);
            this.btnDeleteItem.TabIndex = 7;
            this.btnDeleteItem.Text = "Eliminar";
            this.btnDeleteItem.Click += new System.EventHandler(this.btnDeleteItem_Click);
            // 
            // buttonAdv1
            // 
            this.buttonAdv1.Appearance = Syncfusion.Windows.Forms.ButtonAppearance.Metro;
            this.buttonAdv1.BackColor = System.Drawing.Color.SteelBlue;
            this.buttonAdv1.BeforeTouchSize = new System.Drawing.Size(91, 28);
            this.buttonAdv1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonAdv1.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonAdv1.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.buttonAdv1.IsBackStageButton = false;
            this.buttonAdv1.Location = new System.Drawing.Point(2, 8);
            this.buttonAdv1.Name = "buttonAdv1";
            this.buttonAdv1.Size = new System.Drawing.Size(91, 28);
            this.buttonAdv1.TabIndex = 5;
            this.buttonAdv1.Text = "Nuevo";
            // 
            // btnAddItem
            // 
            this.btnAddItem.Appearance = Syncfusion.Windows.Forms.ButtonAppearance.Metro;
            this.btnAddItem.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(153)))), ((int)(((byte)(181)))), ((int)(((byte)(105)))));
            this.btnAddItem.BeforeTouchSize = new System.Drawing.Size(91, 28);
            this.btnAddItem.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnAddItem.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAddItem.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnAddItem.IsBackStageButton = false;
            this.btnAddItem.Location = new System.Drawing.Point(98, 8);
            this.btnAddItem.Name = "btnAddItem";
            this.btnAddItem.Size = new System.Drawing.Size(91, 28);
            this.btnAddItem.TabIndex = 6;
            this.btnAddItem.Text = "Guardar";
            this.btnAddItem.Click += new System.EventHandler(this.btnAddItem_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(0, 90);
            this.label3.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(79, 13);
            this.label3.TabIndex = 13;
            this.label3.Text = "Regimen Fiscal";
            // 
            // buttonAdv2
            // 
            this.buttonAdv2.Appearance = Syncfusion.Windows.Forms.ButtonAppearance.Metro;
            this.buttonAdv2.BackColor = System.Drawing.Color.SteelBlue;
            this.buttonAdv2.BeforeTouchSize = new System.Drawing.Size(91, 28);
            this.buttonAdv2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonAdv2.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonAdv2.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.buttonAdv2.IsBackStageButton = false;
            this.buttonAdv2.Location = new System.Drawing.Point(295, 8);
            this.buttonAdv2.Name = "buttonAdv2";
            this.buttonAdv2.Size = new System.Drawing.Size(91, 28);
            this.buttonAdv2.TabIndex = 21;
            this.buttonAdv2.Text = "Duplicar";
            // 
            // empresa
            // 
            this.empresa.Location = new System.Drawing.Point(92, 39);
            this.empresa.Margin = new System.Windows.Forms.Padding(2);
            this.empresa.Name = "empresa";
            this.empresa.Size = new System.Drawing.Size(608, 20);
            this.empresa.TabIndex = 25;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(0, 42);
            this.label7.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(48, 13);
            this.label7.TabIndex = 26;
            this.label7.Text = "Empresa";
            // 
            // activo
            // 
            this.activo.AutoSize = true;
            this.activo.Location = new System.Drawing.Point(233, 65);
            this.activo.Name = "activo";
            this.activo.Size = new System.Drawing.Size(56, 17);
            this.activo.TabIndex = 41;
            this.activo.Text = "Activo";
            this.activo.UseVisualStyleBackColor = true;
            // 
            // ROW_ID
            // 
            this.ROW_ID.HeaderText = "ROW_ID";
            this.ROW_ID.Name = "ROW_ID";
            this.ROW_ID.Visible = false;
            // 
            // VISUAL_BD
            // 
            this.VISUAL_BD.HeaderText = "Base de Datos";
            this.VISUAL_BD.Name = "VISUAL_BD";
            this.VISUAL_BD.ReadOnly = true;
            this.VISUAL_BD.Width = 150;
            // 
            // VISUAL_TIPO
            // 
            this.VISUAL_TIPO.HeaderText = "Tipo";
            this.VISUAL_TIPO.Name = "VISUAL_TIPO";
            this.VISUAL_TIPO.ReadOnly = true;
            // 
            // VISUAL_ENTITY_ID
            // 
            this.VISUAL_ENTITY_ID.HeaderText = "Entidad";
            this.VISUAL_ENTITY_ID.Name = "VISUAL_ENTITY_ID";
            this.VISUAL_ENTITY_ID.ReadOnly = true;
            // 
            // VISUAL_SITE_ID
            // 
            this.VISUAL_SITE_ID.HeaderText = "Site";
            this.VISUAL_SITE_ID.Name = "VISUAL_SITE_ID";
            this.VISUAL_SITE_ID.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn1
            // 
            this.dataGridViewTextBoxColumn1.HeaderText = "ROW_ID";
            this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
            this.dataGridViewTextBoxColumn1.Visible = false;
            // 
            // dataGridViewTextBoxColumn2
            // 
            this.dataGridViewTextBoxColumn2.HeaderText = "Base de Datos";
            this.dataGridViewTextBoxColumn2.Name = "dataGridViewTextBoxColumn2";
            this.dataGridViewTextBoxColumn2.ReadOnly = true;
            this.dataGridViewTextBoxColumn2.Width = 150;
            // 
            // dataGridViewTextBoxColumn3
            // 
            this.dataGridViewTextBoxColumn3.HeaderText = "Tipo";
            this.dataGridViewTextBoxColumn3.Name = "dataGridViewTextBoxColumn3";
            this.dataGridViewTextBoxColumn3.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn4
            // 
            this.dataGridViewTextBoxColumn4.HeaderText = "Entidad";
            this.dataGridViewTextBoxColumn4.Name = "dataGridViewTextBoxColumn4";
            this.dataGridViewTextBoxColumn4.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn5
            // 
            this.dataGridViewTextBoxColumn5.HeaderText = "Site";
            this.dataGridViewTextBoxColumn5.Name = "dataGridViewTextBoxColumn5";
            this.dataGridViewTextBoxColumn5.ReadOnly = true;
            // 
            // tabPage7
            // 
            this.tabPage7.Controls.Add(this.buttonAdv5);
            this.tabPage7.Controls.Add(this.buttonAdv4);
            this.tabPage7.Controls.Add(this.dtgConexion);
            this.tabPage7.Location = new System.Drawing.Point(4, 22);
            this.tabPage7.Name = "tabPage7";
            this.tabPage7.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage7.Size = new System.Drawing.Size(909, 355);
            this.tabPage7.TabIndex = 6;
            this.tabPage7.Text = "Conexión";
            this.tabPage7.UseVisualStyleBackColor = true;
            // 
            // buttonAdv5
            // 
            this.buttonAdv5.Appearance = Syncfusion.Windows.Forms.ButtonAppearance.Metro;
            this.buttonAdv5.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(219)))), ((int)(((byte)(94)))), ((int)(((byte)(94)))));
            this.buttonAdv5.BeforeTouchSize = new System.Drawing.Size(94, 28);
            this.buttonAdv5.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonAdv5.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonAdv5.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.buttonAdv5.IsBackStageButton = false;
            this.buttonAdv5.Location = new System.Drawing.Point(103, 6);
            this.buttonAdv5.Name = "buttonAdv5";
            this.buttonAdv5.Size = new System.Drawing.Size(94, 28);
            this.buttonAdv5.TabIndex = 8;
            this.buttonAdv5.Text = "Eliminar";
            this.buttonAdv5.Click += new System.EventHandler(this.buttonAdv5_Click);
            // 
            // buttonAdv4
            // 
            this.buttonAdv4.Appearance = Syncfusion.Windows.Forms.ButtonAppearance.Metro;
            this.buttonAdv4.BackColor = System.Drawing.Color.SteelBlue;
            this.buttonAdv4.BeforeTouchSize = new System.Drawing.Size(91, 28);
            this.buttonAdv4.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonAdv4.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonAdv4.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.buttonAdv4.IsBackStageButton = false;
            this.buttonAdv4.Location = new System.Drawing.Point(6, 6);
            this.buttonAdv4.Name = "buttonAdv4";
            this.buttonAdv4.Size = new System.Drawing.Size(91, 28);
            this.buttonAdv4.TabIndex = 6;
            this.buttonAdv4.Text = "Nuevo";
            this.buttonAdv4.Click += new System.EventHandler(this.buttonAdv4_Click);
            // 
            // dtgConexion
            // 
            this.dtgConexion.AllowUserToAddRows = false;
            this.dtgConexion.AllowUserToDeleteRows = false;
            this.dtgConexion.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dtgConexion.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dtgConexion.Location = new System.Drawing.Point(6, 40);
            this.dtgConexion.Name = "dtgConexion";
            this.dtgConexion.ReadOnly = true;
            this.dtgConexion.Size = new System.Drawing.Size(897, 309);
            this.dtgConexion.TabIndex = 0;
            // 
            // tabPage6
            // 
            this.tabPage6.Location = new System.Drawing.Point(4, 22);
            this.tabPage6.Name = "tabPage6";
            this.tabPage6.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage6.Size = new System.Drawing.Size(909, 355);
            this.tabPage6.TabIndex = 5;
            this.tabPage6.Text = "Complementos";
            this.tabPage6.UseVisualStyleBackColor = true;
            // 
            // tabPage5
            // 
            this.tabPage5.Location = new System.Drawing.Point(4, 22);
            this.tabPage5.Name = "tabPage5";
            this.tabPage5.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage5.Size = new System.Drawing.Size(909, 355);
            this.tabPage5.TabIndex = 4;
            this.tabPage5.Text = "Impresión";
            this.tabPage5.UseVisualStyleBackColor = true;
            // 
            // tabPage4
            // 
            this.tabPage4.Controls.Add(this.label24);
            this.tabPage4.Controls.Add(this.aplicardescuento);
            this.tabPage4.Controls.Add(this.asuntosclientes);
            this.tabPage4.Controls.Add(this.lineafactura);
            this.tabPage4.Controls.Add(this.clientes);
            this.tabPage4.Controls.Add(this.label22);
            this.tabPage4.Controls.Add(this.label34);
            this.tabPage4.Controls.Add(this.label37);
            this.tabPage4.Location = new System.Drawing.Point(4, 22);
            this.tabPage4.Name = "tabPage4";
            this.tabPage4.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage4.Size = new System.Drawing.Size(909, 355);
            this.tabPage4.TabIndex = 3;
            this.tabPage4.Text = "Otros";
            this.tabPage4.UseVisualStyleBackColor = true;
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Location = new System.Drawing.Point(72, 154);
            this.label24.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(266, 13);
            this.label24.TabIndex = 72;
            this.label24.Text = "Clientes separados por (,) para aplicar la especificación";
            // 
            // aplicardescuento
            // 
            this.aplicardescuento.AutoSize = true;
            this.aplicardescuento.Location = new System.Drawing.Point(78, 105);
            this.aplicardescuento.Name = "aplicardescuento";
            this.aplicardescuento.Size = new System.Drawing.Size(451, 17);
            this.aplicardescuento.TabIndex = 71;
            this.aplicardescuento.Text = "Aplicar el descuento sobre las lineas, esto implica que el descuento no vendrá de" +
    "sglozado";
            this.aplicardescuento.UseVisualStyleBackColor = true;
            // 
            // asuntosclientes
            // 
            this.asuntosclientes.Location = new System.Drawing.Point(78, 72);
            this.asuntosclientes.Name = "asuntosclientes";
            this.asuntosclientes.Size = new System.Drawing.Size(195, 22);
            this.asuntosclientes.TabIndex = 70;
            this.asuntosclientes.Text = "Asuntos por Clientes";
            this.asuntosclientes.UseVisualStyleBackColor = true;
            // 
            // lineafactura
            // 
            this.lineafactura.Location = new System.Drawing.Point(297, 129);
            this.lineafactura.Margin = new System.Windows.Forms.Padding(2);
            this.lineafactura.Name = "lineafactura";
            this.lineafactura.Size = new System.Drawing.Size(373, 20);
            this.lineafactura.TabIndex = 69;
            // 
            // clientes
            // 
            this.clientes.Location = new System.Drawing.Point(80, 20);
            this.clientes.Margin = new System.Windows.Forms.Padding(2);
            this.clientes.Name = "clientes";
            this.clientes.Size = new System.Drawing.Size(590, 20);
            this.clientes.TabIndex = 51;
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Location = new System.Drawing.Point(18, 132);
            this.label22.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(281, 13);
            this.label22.TabIndex = 68;
            this.label22.Text = "Clientes para agregar espesificación de la línea de factura";
            // 
            // label34
            // 
            this.label34.AutoSize = true;
            this.label34.Location = new System.Drawing.Point(77, 52);
            this.label34.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(379, 13);
            this.label34.TabIndex = 56;
            this.label34.Text = "Clientes separados por (,) para buscar las  Direcciónes  de Embarque y Factura";
            // 
            // label37
            // 
            this.label37.AutoSize = true;
            this.label37.Location = new System.Drawing.Point(18, 23);
            this.label37.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label37.Name = "label37";
            this.label37.Size = new System.Drawing.Size(44, 13);
            this.label37.TabIndex = 50;
            this.label37.Text = "Clientes";
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.button3);
            this.tabPage2.Controls.Add(this.checkBox3);
            this.tabPage2.Controls.Add(this.conexionS);
            this.tabPage2.Controls.Add(this.contraceña);
            this.tabPage2.Controls.Add(this.usuario);
            this.tabPage2.Controls.Add(this.puertasalida);
            this.tabPage2.Controls.Add(this.smtp);
            this.tabPage2.Controls.Add(this.label21);
            this.tabPage2.Controls.Add(this.label20);
            this.tabPage2.Controls.Add(this.label19);
            this.tabPage2.Controls.Add(this.label18);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(909, 355);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Configuración de Envio Correo Electronico";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(104, 130);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(169, 23);
            this.button3.TabIndex = 41;
            this.button3.Text = "Realizar Prueba";
            this.button3.UseVisualStyleBackColor = true;
            // 
            // checkBox3
            // 
            this.checkBox3.AutoSize = true;
            this.checkBox3.Location = new System.Drawing.Point(104, 84);
            this.checkBox3.Name = "checkBox3";
            this.checkBox3.Size = new System.Drawing.Size(191, 17);
            this.checkBox3.TabIndex = 40;
            this.checkBox3.Text = "El Servidor requiere Autentificación";
            this.checkBox3.UseVisualStyleBackColor = true;
            // 
            // conexionS
            // 
            this.conexionS.AutoSize = true;
            this.conexionS.Location = new System.Drawing.Point(104, 107);
            this.conexionS.Name = "conexionS";
            this.conexionS.Size = new System.Drawing.Size(220, 17);
            this.conexionS.TabIndex = 38;
            this.conexionS.Text = "El Servidor requiere una conexion segura";
            this.conexionS.UseVisualStyleBackColor = true;
            // 
            // contraceña
            // 
            this.contraceña.Location = new System.Drawing.Point(104, 59);
            this.contraceña.Margin = new System.Windows.Forms.Padding(2);
            this.contraceña.Name = "contraceña";
            this.contraceña.Size = new System.Drawing.Size(438, 20);
            this.contraceña.TabIndex = 37;
            // 
            // usuario
            // 
            this.usuario.Location = new System.Drawing.Point(104, 35);
            this.usuario.Margin = new System.Windows.Forms.Padding(2);
            this.usuario.Name = "usuario";
            this.usuario.Size = new System.Drawing.Size(438, 20);
            this.usuario.TabIndex = 35;
            // 
            // puertasalida
            // 
            this.puertasalida.Location = new System.Drawing.Point(400, 84);
            this.puertasalida.Margin = new System.Windows.Forms.Padding(2);
            this.puertasalida.Name = "puertasalida";
            this.puertasalida.Size = new System.Drawing.Size(142, 20);
            this.puertasalida.TabIndex = 33;
            // 
            // smtp
            // 
            this.smtp.Location = new System.Drawing.Point(104, 11);
            this.smtp.Margin = new System.Windows.Forms.Padding(2);
            this.smtp.Name = "smtp";
            this.smtp.Size = new System.Drawing.Size(438, 20);
            this.smtp.TabIndex = 15;
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(38, 62);
            this.label21.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(61, 13);
            this.label21.TabIndex = 36;
            this.label21.Text = "Contraseña";
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(57, 38);
            this.label20.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(43, 13);
            this.label20.TabIndex = 34;
            this.label20.Text = "Usuario";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(311, 87);
            this.label19.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(85, 13);
            this.label19.TabIndex = 16;
            this.label19.Text = "Puerto de Salida";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(15, 14);
            this.label18.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(85, 13);
            this.label18.TabIndex = 14;
            this.label18.Text = "Servidor (SMTP)";
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.municipio);
            this.tabPage1.Controls.Add(this.estado);
            this.tabPage1.Controls.Add(this.pais);
            this.tabPage1.Controls.Add(this.referencia);
            this.tabPage1.Controls.Add(this.localidad);
            this.tabPage1.Controls.Add(this.cp);
            this.tabPage1.Controls.Add(this.colonia);
            this.tabPage1.Controls.Add(this.interior);
            this.tabPage1.Controls.Add(this.exterior);
            this.tabPage1.Controls.Add(this.calle);
            this.tabPage1.Controls.Add(this.label17);
            this.tabPage1.Controls.Add(this.label16);
            this.tabPage1.Controls.Add(this.label15);
            this.tabPage1.Controls.Add(this.label14);
            this.tabPage1.Controls.Add(this.label13);
            this.tabPage1.Controls.Add(this.label12);
            this.tabPage1.Controls.Add(this.label11);
            this.tabPage1.Controls.Add(this.label4);
            this.tabPage1.Controls.Add(this.label2);
            this.tabPage1.Controls.Add(this.label1);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(909, 355);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Dirección";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // municipio
            // 
            this.municipio.Location = new System.Drawing.Point(75, 131);
            this.municipio.Margin = new System.Windows.Forms.Padding(2);
            this.municipio.Name = "municipio";
            this.municipio.Size = new System.Drawing.Size(590, 20);
            this.municipio.TabIndex = 49;
            // 
            // estado
            // 
            this.estado.Location = new System.Drawing.Point(403, 101);
            this.estado.Margin = new System.Windows.Forms.Padding(2);
            this.estado.Name = "estado";
            this.estado.Size = new System.Drawing.Size(262, 20);
            this.estado.TabIndex = 46;
            // 
            // pais
            // 
            this.pais.Location = new System.Drawing.Point(75, 101);
            this.pais.Margin = new System.Windows.Forms.Padding(2);
            this.pais.Name = "pais";
            this.pais.Size = new System.Drawing.Size(258, 20);
            this.pais.TabIndex = 45;
            // 
            // referencia
            // 
            this.referencia.Location = new System.Drawing.Point(403, 67);
            this.referencia.Margin = new System.Windows.Forms.Padding(2);
            this.referencia.Name = "referencia";
            this.referencia.Size = new System.Drawing.Size(262, 20);
            this.referencia.TabIndex = 43;
            // 
            // localidad
            // 
            this.localidad.Location = new System.Drawing.Point(75, 67);
            this.localidad.Margin = new System.Windows.Forms.Padding(2);
            this.localidad.Name = "localidad";
            this.localidad.Size = new System.Drawing.Size(258, 20);
            this.localidad.TabIndex = 41;
            // 
            // cp
            // 
            this.cp.Location = new System.Drawing.Point(545, 40);
            this.cp.Margin = new System.Windows.Forms.Padding(2);
            this.cp.Name = "cp";
            this.cp.Size = new System.Drawing.Size(120, 20);
            this.cp.TabIndex = 39;
            // 
            // colonia
            // 
            this.colonia.Location = new System.Drawing.Point(75, 40);
            this.colonia.Margin = new System.Windows.Forms.Padding(2);
            this.colonia.Name = "colonia";
            this.colonia.Size = new System.Drawing.Size(423, 20);
            this.colonia.TabIndex = 37;
            // 
            // interior
            // 
            this.interior.Location = new System.Drawing.Point(545, 10);
            this.interior.Margin = new System.Windows.Forms.Padding(2);
            this.interior.Name = "interior";
            this.interior.Size = new System.Drawing.Size(120, 20);
            this.interior.TabIndex = 35;
            // 
            // exterior
            // 
            this.exterior.Location = new System.Drawing.Point(386, 10);
            this.exterior.Margin = new System.Windows.Forms.Padding(2);
            this.exterior.Name = "exterior";
            this.exterior.Size = new System.Drawing.Size(112, 20);
            this.exterior.TabIndex = 33;
            // 
            // calle
            // 
            this.calle.Location = new System.Drawing.Point(75, 10);
            this.calle.Margin = new System.Windows.Forms.Padding(2);
            this.calle.Name = "calle";
            this.calle.Size = new System.Drawing.Size(261, 20);
            this.calle.TabIndex = 15;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(5, 134);
            this.label17.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(52, 13);
            this.label17.TabIndex = 48;
            this.label17.Text = "Municipio";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(359, 104);
            this.label16.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(40, 13);
            this.label16.TabIndex = 47;
            this.label16.Text = "Estado";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(5, 101);
            this.label15.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(27, 13);
            this.label15.TabIndex = 44;
            this.label15.Text = "Pais";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(340, 74);
            this.label14.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(59, 13);
            this.label14.TabIndex = 42;
            this.label14.Text = "Referencia";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(5, 70);
            this.label13.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(53, 13);
            this.label13.TabIndex = 40;
            this.label13.Text = "Localidad";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(514, 43);
            this.label12.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(27, 13);
            this.label12.TabIndex = 38;
            this.label12.Text = "C.P.";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(5, 40);
            this.label11.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(42, 13);
            this.label11.TabIndex = 36;
            this.label11.Text = "Colonia";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(502, 13);
            this.label4.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(39, 13);
            this.label4.TabIndex = 34;
            this.label4.Text = "Interior";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(340, 13);
            this.label2.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(42, 13);
            this.label2.TabIndex = 29;
            this.label2.Text = "Exterior";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(5, 13);
            this.label1.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(30, 13);
            this.label1.TabIndex = 14;
            this.label1.Text = "Calle";
            // 
            // tipo
            // 
            this.tipo.Controls.Add(this.tabPage1);
            this.tipo.Controls.Add(this.tabPage2);
            this.tipo.Controls.Add(this.tabPage4);
            this.tipo.Controls.Add(this.tabPage5);
            this.tipo.Controls.Add(this.tabPage6);
            this.tipo.Controls.Add(this.tabPage7);
            this.tipo.Location = new System.Drawing.Point(3, 112);
            this.tipo.Name = "tipo";
            this.tipo.SelectedIndex = 0;
            this.tipo.Size = new System.Drawing.Size(917, 381);
            this.tipo.TabIndex = 33;
            // 
            // regimenFiscal
            // 
            this.regimenFiscal.FormattingEnabled = true;
            this.regimenFiscal.Location = new System.Drawing.Point(92, 87);
            this.regimenFiscal.Name = "regimenFiscal";
            this.regimenFiscal.Size = new System.Drawing.Size(608, 21);
            this.regimenFiscal.TabIndex = 42;
            // 
            // frmEmpresa
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(179)))), ((int)(((byte)(207)))), ((int)(((byte)(96)))));
            this.BorderThickness = 0;
            this.CaptionAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.CaptionBarColor = System.Drawing.Color.FromArgb(((int)(((byte)(179)))), ((int)(((byte)(207)))), ((int)(((byte)(96)))));
            this.CaptionBarHeight = 30;
            this.CaptionButtonColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(932, 505);
            this.Controls.Add(this.regimenFiscal);
            this.Controls.Add(this.activo);
            this.Controls.Add(this.tipo);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.empresa);
            this.Controls.Add(this.buttonAdv2);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.rfc);
            this.Controls.Add(this.btnDeleteItem);
            this.Controls.Add(this.buttonAdv1);
            this.Controls.Add(this.btnAddItem);
            this.MetroColor = System.Drawing.Color.FromArgb(((int)(((byte)(179)))), ((int)(((byte)(207)))), ((int)(((byte)(96)))));
            this.Name = "frmEmpresa";
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Empresa";
            this.tabPage7.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dtgConexion)).EndInit();
            this.tabPage4.ResumeLayout(false);
            this.tabPage4.PerformLayout();
            this.tabPage2.ResumeLayout(false);
            this.tabPage2.PerformLayout();
            this.tabPage1.ResumeLayout(false);
            this.tabPage1.PerformLayout();
            this.tipo.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox rfc;
        private Syncfusion.Windows.Forms.ButtonAdv btnDeleteItem;
        private Syncfusion.Windows.Forms.ButtonAdv buttonAdv1;
        private Syncfusion.Windows.Forms.ButtonAdv btnAddItem;
        private System.Windows.Forms.Label label3;
        private Syncfusion.Windows.Forms.ButtonAdv buttonAdv2;
        private System.Windows.Forms.TextBox empresa;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.CheckBox activo;
        private System.Windows.Forms.DataGridViewTextBoxColumn ROW_ID;
        private System.Windows.Forms.DataGridViewTextBoxColumn VISUAL_BD;
        private System.Windows.Forms.DataGridViewTextBoxColumn VISUAL_TIPO;
        private System.Windows.Forms.DataGridViewTextBoxColumn VISUAL_ENTITY_ID;
        private System.Windows.Forms.DataGridViewTextBoxColumn VISUAL_SITE_ID;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn2;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn3;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn4;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn5;
        private System.Windows.Forms.TabPage tabPage7;
        private Syncfusion.Windows.Forms.ButtonAdv buttonAdv5;
        private Syncfusion.Windows.Forms.ButtonAdv buttonAdv4;
        private System.Windows.Forms.DataGridView dtgConexion;
        private System.Windows.Forms.TabPage tabPage6;
        private System.Windows.Forms.TabPage tabPage5;
        private System.Windows.Forms.TabPage tabPage4;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.CheckBox aplicardescuento;
        private System.Windows.Forms.Button asuntosclientes;
        private System.Windows.Forms.TextBox lineafactura;
        private System.Windows.Forms.TextBox clientes;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label label34;
        private System.Windows.Forms.Label label37;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.CheckBox checkBox3;
        private System.Windows.Forms.CheckBox conexionS;
        private System.Windows.Forms.TextBox contraceña;
        private System.Windows.Forms.TextBox usuario;
        private System.Windows.Forms.TextBox puertasalida;
        private System.Windows.Forms.TextBox smtp;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TextBox municipio;
        private System.Windows.Forms.TextBox estado;
        private System.Windows.Forms.TextBox pais;
        private System.Windows.Forms.TextBox referencia;
        private System.Windows.Forms.TextBox localidad;
        private System.Windows.Forms.TextBox cp;
        private System.Windows.Forms.TextBox colonia;
        private System.Windows.Forms.TextBox interior;
        private System.Windows.Forms.TextBox exterior;
        private System.Windows.Forms.TextBox calle;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TabControl tipo;
        private System.Windows.Forms.ComboBox regimenFiscal;
    }
}