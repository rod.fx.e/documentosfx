#region Copyright Syncfusion Inc. 2001 - 2017
// Copyright Syncfusion Inc. 2001 - 2017. All rights reserved.
// Use of this code is subject to the terms of our license.
// A copy of the current license can be obtained at any time by e-mailing
// licensing@syncfusion.com. Any infringement will be prosecuted under
// applicable laws. 
#endregion
namespace DocumentosFX.formularios
{
    partial class frmCxPServicio
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmCxPServicio));
            Syncfusion.Windows.Forms.CaptionImage captionImage1 = new Syncfusion.Windows.Forms.CaptionImage();
            this.notifyIcon = new System.Windows.Forms.NotifyIcon(this.components);
            this.ucCxPsubirNueva1 = new DocumentosFX.uc.ucCxPsubirNueva();
            this.SuspendLayout();
            // 
            // notifyIcon
            // 
            this.notifyIcon.Icon = ((System.Drawing.Icon)(resources.GetObject("notifyIcon.Icon")));
            this.notifyIcon.Text = "Sincronización CxP";
            this.notifyIcon.Visible = true;
            this.notifyIcon.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.notifyIcon_MouseDoubleClick);
            // 
            // ucCxPsubirNueva1
            // 
            this.ucCxPsubirNueva1.Location = new System.Drawing.Point(11, 11);
            this.ucCxPsubirNueva1.Margin = new System.Windows.Forms.Padding(2);
            this.ucCxPsubirNueva1.Name = "ucCxPsubirNueva1";
            this.ucCxPsubirNueva1.Size = new System.Drawing.Size(619, 131);
            this.ucCxPsubirNueva1.TabIndex = 0;
            // 
            // frmCxPServicio
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CaptionBarColor = System.Drawing.Color.FromArgb(((int)(((byte)(77)))), ((int)(((byte)(168)))), ((int)(((byte)(29)))));
            this.CaptionBarHeight = 80;
            this.CaptionButtonColor = System.Drawing.Color.White;
            captionImage1.BackColor = System.Drawing.Color.Transparent;
            captionImage1.Image = global::DocumentosFX.Properties.Resources.cxpAsset_2_2x;
            captionImage1.Location = new System.Drawing.Point(30, 5);
            captionImage1.Name = "CaptionImage1";
            captionImage1.Size = new System.Drawing.Size(176, 70);
            this.CaptionImages.Add(captionImage1);
            this.ClientSize = new System.Drawing.Size(641, 150);
            this.Controls.Add(this.ucCxPsubirNueva1);
            this.Margin = new System.Windows.Forms.Padding(2);
            this.Name = "frmCxPServicio";
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Resize += new System.EventHandler(this.frmCxPServicio_Resize);
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.NotifyIcon notifyIcon;
        private uc.ucCxPsubirNueva ucCxPsubirNueva1;
    }
}