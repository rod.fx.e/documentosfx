#region Copyright Syncfusion Inc. 2001 - 2017
// Copyright Syncfusion Inc. 2001 - 2017. All rights reserved.
// Use of this code is subject to the terms of our license.
// A copy of the current license can be obtained at any time by e-mailing
// licensing@syncfusion.com. Any infringement will be prosecuted under
// applicable laws. 
#endregion
using FE_FX.Clases;
using Generales;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;

using System.Text;
using System.Windows.Forms;

namespace DocumentosFX.formularios
{
    public partial class frmPayableCambio : Syncfusion.Windows.Forms.MetroForm
    {
        Encapsular oEncapsular;
        string factura, voucher, proveedor;
        public frmPayableCambio(Encapsular oEncapsularp, string facturap, string voucherp, string proveedorp)
        {
            oEncapsular = oEncapsularp;
            factura = facturap;
            voucher = voucherp;
            proveedor = proveedorp;
            InitializeComponent();
        }

        private void buttonAdv4_Click(object sender, EventArgs e)
        {
            actualizarVisual();
        }

        private void actualizarVisual()
        {
            try
            {
                string sSQL = @"
                UPDATE PAYABLE
                SET INVOICE_ID='" + invoiceId.Text + @"'
                WHERE VENDOR_ID='" + proveedor + @"' AND VOUCHER_ID='" + voucher + @"'
                ";
                oEncapsular.oData_ERP.EjecutarConsulta(sSQL);

                MessageBox.Show(this,"Datos actualizados");
                this.Close();
            }
            catch (Exception e)
            {
                ErrorFX.mostrar(e, true, true, true);
            }
        }
    }
}
