#region Copyright Syncfusion Inc. 2001 - 2017
// Copyright Syncfusion Inc. 2001 - 2017. All rights reserved.
// Use of this code is subject to the terms of our license.
// A copy of the current license can be obtained at any time by e-mailing
// licensing@syncfusion.com. Any infringement will be prosecuted under
// applicable laws. 
#endregion
using ModeloDocumentosFX;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Linq;
using System.Drawing;

using System.Text;
using System.Windows.Forms;
using System.Data.Entity.Validation;
using ControlGastos.formularios;
using DocumentosFX.clases;
using Generales;

namespace PlantillaFormulario
{
    public partial class frmConfiguracionGeneral : Syncfusion.Windows.Forms.MetroForm
    {
      
        public frmConfiguracionGeneral()
        {
            InitializeComponent();
        }
        
        private void buttonX2_Click(object sender, EventArgs e)
        {
            ModeloDocumentosFXContainer dbContext = new ModeloDocumentosFXContainer();
            try
            {
                string conection = dbContext.Database.Connection.ConnectionString;
                FE_FX.frmEmpresas o = new FE_FX.frmEmpresas(conection);
                o.Show();
            }
            catch (Exception err)
            {
                ErrorFX.mostrar(err, false, true
                    ,"frmConfiguracionGeneral - 45 - ", false
                    );
            }
        }

        private void buttonX1_Click(object sender, EventArgs e)
        {
            ModeloDocumentosFXContainer dbContext = new ModeloDocumentosFXContainer();
            try
            {
                string conection = dbContext.Database.Connection.ConnectionString;
                FE_FX.frmCertificados o = new FE_FX.frmCertificados(conection);
                o.Show();
            }
            catch (Exception err)
            {
                ErrorFX.mostrar(err, false, true,"frmConfiguracionGeneral - 61 - ", false);
            }
        }

        private void buttonX10_Click(object sender, EventArgs e)
        {
            ModeloDocumentosFXContainer dbContext = new ModeloDocumentosFXContainer();
            try
            {
                string conection = dbContext.Database.Connection.ConnectionString;
                FE_FX.frmFormatos o = new FE_FX.frmFormatos(conection);
                o.Show();
            }
            catch (Exception err)
            {
                ErrorFX.mostrar(err, false, true, "frmConfiguracionGeneral - 76 - ", false);
            }
        }

        private void buttonX3_Click(object sender, EventArgs e)
        {
            ModeloDocumentosFXContainer dbContext = new ModeloDocumentosFXContainer();
            try
            {
                string conection = dbContext.Database.Connection.ConnectionString;
                FE_FX.frmSeries o = new FE_FX.frmSeries(conection);
                o.Show();
            }
            catch (Exception err)
            {
                ErrorFX.mostrar(err, false, true, "frmConfiguracionGeneral - 92 - ", false);
            }
        }

        private void buttonX4_Click(object sender, EventArgs e)
        {
            
        }

        private void buttonX14_Click(object sender, EventArgs e)
        {
            ClasificadorProductos.formularios.frmProductos o = new ClasificadorProductos.formularios.frmProductos();
            o.Show();
        }

        private void buttonX14_Click_1(object sender, EventArgs e)
        {
            ModeloDocumentosFXContainer dbContext = new ModeloDocumentosFXContainer();
            try
            {
                string conection = dbContext.Database.Connection.ConnectionString;
                FE_FX.frmComercioExteriorConfiguracion o = new FE_FX.frmComercioExteriorConfiguracion(conection);
                o.Show();
            }
            catch (Exception err)
            {
                ErrorFX.mostrar(err, false, true, "frmConfiguracionGeneral - 117 - ");
            }
        }

        private void buttonX5_Click(object sender, EventArgs e)
        {
            frmUsuarios o = new frmUsuarios();
            o.Show();
        }

        private void buttonX6_Click(object sender, EventArgs e)
        {
            ClasificadorProductos.formularios.frmConversionUM o = 
                new ClasificadorProductos.formularios.frmConversionUM();
            o.Show();
        }

        private void buttonX7_Click(object sender, EventArgs e)
        {
            ErrorFX.mostrar("Mensaje de prueba", false, true, true);
        }
    }
}
