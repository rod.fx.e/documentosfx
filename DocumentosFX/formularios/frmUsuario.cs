#region Copyright Syncfusion Inc. 2001 - 2017
// Copyright Syncfusion Inc. 2001 - 2017. All rights reserved.
// Use of this code is subject to the terms of our license.
// A copy of the current license can be obtained at any time by e-mailing
// licensing@syncfusion.com. Any infringement will be prosecuted under
// applicable laws. 
#endregion
using ModeloDocumentosFX;
using OfficeOpenXml;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.Entity.Validation;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace ControlGastos.formularios
{
    public partial class frmUsuario : Syncfusion.Windows.Forms.MetroForm
    {
        bool modificado = false;
        usuarios oRegistro;
        public frmUsuario()
        {
            InitializeComponent();
            oRegistro = new usuarios();
        }
        public frmUsuario(usuarios oRegistrop)
        {
            InitializeComponent();
            limpiar();
            oRegistro = oRegistrop;
            cargar();
        }
        private void buttonAdv1_Click(object sender, EventArgs e)
        {
            limpiar();
        }

        private void btnAddItem_Click(object sender, EventArgs e)
        {
            guardar();
        }

        private void btnDeleteItem_Click(object sender, EventArgs e)
        {
            eliminar();
        }
        #region funciones
        private void cargar()
        {
            usuario.Text = oRegistro.usuario;
            password.Text = oRegistro.password;
            nombreCompleto.Text = oRegistro.nombreCompleto;
            correoElectronico.Text = oRegistro.correoElectronico;
            usuarioVISUAL.Text = oRegistro.usuarioVISUAL;


            facturacionElectronica.Checked = oRegistro.facturacionElectronica;
            contabilidadElectronica.Checked = oRegistro.contabilidadElectronica;
            cuentasCobrar.Checked = oRegistro.cuentasCobrar;
            cuentasPagar.Checked = oRegistro.cuentasPagar;
            configuracion.Checked = oRegistro.configuracion;

            AccesoIngreso.Checked = oRegistro.AccesoIngreso;
            AccesoEgreso.Checked = oRegistro.AccesoEgreso;
            AccesoNomina.Checked = oRegistro.AccesoNomina;
        }

        public void limpiar()
        {
            if (modificado)
            {

                DialogResult Resultado = MessageBox.Show(this, "�Desea guardar los cambios?", Application.ProductName + "-" + Application.ProductVersion, MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                if (Resultado.ToString() == "Yes")
                {
                    guardar();
                }
            }
            oRegistro = new usuarios();
            modificado = false;
            foreach (var c in this.Controls)
            {
                if (c is TextBox)
                {
                    ((TextBox)c).Text = "";
                }
            }
        }
        private void guardar()
        {

            ModeloDocumentosFX.ModeloDocumentosFXContainer dbContext = new ModeloDocumentosFXContainer();
            try
            {
                usuarios registro = new usuarios();
                modificado = false;
                if (oRegistro.Id != 0)
                {
                    //Cargar registro
                    int valor = oRegistro.Id;
                    registro = (from r in dbContext.usuariosSet.Where
                                            (a => a.Id == valor)
                                select r).FirstOrDefault();
                    modificado = true;
                }

                registro.usuario=usuario.Text;
                registro.password=password.Text;
                registro.nombreCompleto=nombreCompleto.Text;
                registro.correoElectronico=correoElectronico.Text;
                registro.usuarioVISUAL=usuarioVISUAL.Text;

                registro.facturacionElectronica = facturacionElectronica.Checked;
                registro.contabilidadElectronica = contabilidadElectronica.Checked;
                registro.cuentasCobrar = cuentasCobrar.Checked;
                registro.cuentasPagar = cuentasPagar.Checked;
                registro.configuracion = configuracion.Checked;

                registro.AccesoIngreso = AccesoIngreso.Checked;
                registro.AccesoEgreso = AccesoEgreso.Checked;
                registro.AccesoNomina = AccesoNomina.Checked;
                registro.AccesoIngreso = AccesoIngreso.Checked;


                if (!modificado)
                {
                    dbContext.usuariosSet.Add(registro);
                }
                else
                {
                    dbContext.usuariosSet.Attach(registro);
                    dbContext.Entry(registro).State = System.Data.Entity.EntityState.Modified;
                }

                dbContext.SaveChanges();
                oRegistro = registro;
                modificado = false;
                MessageBox.Show(this, "Datos guardados", Application.ProductName + "-" + Application.ProductVersion, MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            catch (DbEntityValidationException e)
            {
                foreach (var eve in e.EntityValidationErrors)
                {
                    foreach (var ve in eve.ValidationErrors)
                    {
                        MessageBox.Show(this, ve.PropertyName + " Error: " + ve.ErrorMessage, Application.ProductName
                            , MessageBoxButtons.OK, MessageBoxIcon.Error);
                        return;
                    }
                }
            }
        }

        private void eliminar()
        {

            DialogResult Resultado = MessageBox.Show(this, "�Esta seguro de eliminar?", Application.ProductName + "-" + Application.ProductVersion, MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            if (Resultado.ToString() != "Yes")
            {
                return;
            }
            ModeloDocumentosFXContainer dbContext = new ModeloDocumentosFXContainer();
            dbContext.usuariosSet.Attach(oRegistro);
            dbContext.usuariosSet.Remove(oRegistro);
            dbContext.SaveChanges();
            MessageBox.Show(this, "Eliminado.", Application.ProductName + "-" + Application.ProductVersion, MessageBoxButtons.OK, MessageBoxIcon.Information);
            limpiar();
        }

        #endregion
    }
}
