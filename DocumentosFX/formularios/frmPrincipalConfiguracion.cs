using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevComponents.DotNetBar;

namespace FE_FX
{
    public partial class frmPrincipalConfiguracion : DevComponents.DotNetBar.Metro.MetroForm
    {
        private cCONEXCION oData = new cCONEXCION("");
        
        public frmPrincipalConfiguracion(string sConn)
        {
            oData.sConn = sConn;
            InitializeComponent();
        }


        private void buttonX1_Click(object sender, EventArgs e)
        {
            frmCertificados oObjeto = new frmCertificados(oData.sConn);
            oObjeto.ShowDialog();
        }

        private void buttonX2_Click(object sender, EventArgs e)
        {
            frmEmpresas oObjeto = new frmEmpresas(oData.sConn);
            oObjeto.ShowDialog();

        }

        private void buttonX3_Click(object sender, EventArgs e)
        {
            frmSeries oObjeto = new frmSeries(oData.sConn);
            oObjeto.ShowDialog();
        }

        private void buttonX4_Click(object sender, EventArgs e)
        {
            frmUsuarios oObjeto = new frmUsuarios(oData.sConn);
            oObjeto.ShowDialog();
        }

        private void buttonX5_Click(object sender, EventArgs e)
        {
            frmClientes oObjeto = new frmClientes(oData.sConn,"");
            oObjeto.ShowDialog();
        }

        private void buttonX6_Click(object sender, EventArgs e)
        {
            frmConceptos oObjeto = new frmConceptos(oData.sConn);
            oObjeto.ShowDialog();
        }

        private void buttonX8_Click(object sender, EventArgs e)
        {
            frmImpuestos oObjeto = new frmImpuestos(oData.sConn);
            oObjeto.ShowDialog();
        }

        private void buttonX7_Click(object sender, EventArgs e)
        {
            frmMonedas oObjeto = new frmMonedas(oData.sConn);
            oObjeto.ShowDialog();
        }

        private void buttonX9_Click(object sender, EventArgs e)
        {
            frmRespaldo oObjeto = new frmRespaldo(oData.sConn,"FE");
            oObjeto.ShowDialog();
        }

        private void frmPrincipalConfiguracion_Load(object sender, EventArgs e)
        {

        }

        private void buttonX10_Click(object sender, EventArgs e)
        {
            frmFormatos oObjeto = new frmFormatos(oData.sConn);
            oObjeto.ShowDialog();
        }

        private void buttonX11_Click(object sender, EventArgs e)
        {
            frmUMs oObjeto = new frmUMs(oData.sConn);
            oObjeto.ShowDialog();
        }

        private void buttonX12_Click(object sender, EventArgs e)
        {
            frmClientes oObjeto = new frmClientes(oData.sConn, "TRABAJADOR");
            oObjeto.ShowDialog();
        }

        private void buttonX13_Click(object sender, EventArgs e)
        {
            frmEstados oObjeto = new frmEstados(oData.sConn);
            oObjeto.ShowDialog();
        }
    }
}