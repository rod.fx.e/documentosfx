#region Copyright Syncfusion Inc. 2001 - 2017
// Copyright Syncfusion Inc. 2001 - 2017. All rights reserved.
// Use of this code is subject to the terms of our license.
// A copy of the current license can be obtained at any time by e-mailing
// licensing@syncfusion.com. Any infringement will be prosecuted under
// applicable laws. 
#endregion
namespace DocumentosFX.formularios
{
    partial class frmPrincipal
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            Syncfusion.Windows.Forms.CaptionImage captionImage1 = new Syncfusion.Windows.Forms.CaptionImage();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmPrincipal));
            this.bienvenido = new System.Windows.Forms.Label();
            this.buttonAdv7 = new Syncfusion.Windows.Forms.ButtonAdv();
            this.buttonAdv6 = new Syncfusion.Windows.Forms.ButtonAdv();
            this.buttonAdv5 = new Syncfusion.Windows.Forms.ButtonAdv();
            this.buttonAdv4 = new Syncfusion.Windows.Forms.ButtonAdv();
            this.buttonAdv3 = new Syncfusion.Windows.Forms.ButtonAdv();
            this.buttonAdv2 = new Syncfusion.Windows.Forms.ButtonAdv();
            this.btnConfiguracion = new Syncfusion.Windows.Forms.ButtonAdv();
            this.btnCE = new Syncfusion.Windows.Forms.ButtonAdv();
            this.btnCxC = new Syncfusion.Windows.Forms.ButtonAdv();
            this.buttonAdv1 = new Syncfusion.Windows.Forms.ButtonAdv();
            this.btnCxP = new Syncfusion.Windows.Forms.ButtonAdv();
            this.btnFE = new Syncfusion.Windows.Forms.ButtonAdv();
            this.SuspendLayout();
            // 
            // bienvenido
            // 
            this.bienvenido.Font = new System.Drawing.Font("Segoe UI Semibold", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bienvenido.Location = new System.Drawing.Point(9, 7);
            this.bienvenido.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.bienvenido.Name = "bienvenido";
            this.bienvenido.Size = new System.Drawing.Size(724, 26);
            this.bienvenido.TabIndex = 2;
            this.bienvenido.Text = "Bienvenido, Rodrigo Escalona";
            this.bienvenido.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // buttonAdv7
            // 
            this.buttonAdv7.Appearance = Syncfusion.Windows.Forms.ButtonAppearance.Metro;
            this.buttonAdv7.BackColor = System.Drawing.Color.Transparent;
            this.buttonAdv7.BackgroundImage = global::DocumentosFX.Properties.Resources.botonesPrincipalFlujoEfectivo;
            this.buttonAdv7.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.buttonAdv7.BeforeTouchSize = new System.Drawing.Size(117, 124);
            this.buttonAdv7.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonAdv7.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonAdv7.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.buttonAdv7.IsBackStageButton = false;
            this.buttonAdv7.Location = new System.Drawing.Point(618, 195);
            this.buttonAdv7.MetroColor = System.Drawing.Color.Transparent;
            this.buttonAdv7.Name = "buttonAdv7";
            this.buttonAdv7.Size = new System.Drawing.Size(117, 124);
            this.buttonAdv7.TabIndex = 8;
            this.buttonAdv7.Click += new System.EventHandler(this.buttonAdv7_Click);
            // 
            // buttonAdv6
            // 
            this.buttonAdv6.Appearance = Syncfusion.Windows.Forms.ButtonAppearance.Metro;
            this.buttonAdv6.BackColor = System.Drawing.Color.Transparent;
            this.buttonAdv6.BackgroundImage = global::DocumentosFX.Properties.Resources.botonesPrincipalIVA;
            this.buttonAdv6.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.buttonAdv6.BeforeTouchSize = new System.Drawing.Size(117, 124);
            this.buttonAdv6.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonAdv6.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonAdv6.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.buttonAdv6.IsBackStageButton = false;
            this.buttonAdv6.Location = new System.Drawing.Point(495, 195);
            this.buttonAdv6.MetroColor = System.Drawing.Color.Transparent;
            this.buttonAdv6.Name = "buttonAdv6";
            this.buttonAdv6.Size = new System.Drawing.Size(117, 124);
            this.buttonAdv6.TabIndex = 7;
            this.buttonAdv6.Click += new System.EventHandler(this.buttonAdv6_Click);
            // 
            // buttonAdv5
            // 
            this.buttonAdv5.Appearance = Syncfusion.Windows.Forms.ButtonAppearance.Metro;
            this.buttonAdv5.BackColor = System.Drawing.Color.Transparent;
            this.buttonAdv5.BackgroundImage = global::DocumentosFX.Properties.Resources.botonesPrincipalCxPGeneracion;
            this.buttonAdv5.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.buttonAdv5.BeforeTouchSize = new System.Drawing.Size(117, 124);
            this.buttonAdv5.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonAdv5.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonAdv5.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.buttonAdv5.IsBackStageButton = false;
            this.buttonAdv5.Location = new System.Drawing.Point(372, 195);
            this.buttonAdv5.MetroColor = System.Drawing.Color.Transparent;
            this.buttonAdv5.Name = "buttonAdv5";
            this.buttonAdv5.Size = new System.Drawing.Size(117, 124);
            this.buttonAdv5.TabIndex = 6;
            this.buttonAdv5.Click += new System.EventHandler(this.buttonAdv5_Click);
            // 
            // buttonAdv4
            // 
            this.buttonAdv4.Appearance = Syncfusion.Windows.Forms.ButtonAppearance.Metro;
            this.buttonAdv4.BackColor = System.Drawing.Color.Transparent;
            this.buttonAdv4.BackgroundImage = global::DocumentosFX.Properties.Resources.botonesPrincipalTrasladoNuevo2;
            this.buttonAdv4.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.buttonAdv4.BeforeTouchSize = new System.Drawing.Size(117, 124);
            this.buttonAdv4.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonAdv4.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonAdv4.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.buttonAdv4.IsBackStageButton = false;
            this.buttonAdv4.Location = new System.Drawing.Point(249, 195);
            this.buttonAdv4.MetroColor = System.Drawing.Color.Transparent;
            this.buttonAdv4.Name = "buttonAdv4";
            this.buttonAdv4.Size = new System.Drawing.Size(117, 124);
            this.buttonAdv4.TabIndex = 5;
            this.buttonAdv4.Click += new System.EventHandler(this.buttonAdv4_Click_1);
            // 
            // buttonAdv3
            // 
            this.buttonAdv3.Appearance = Syncfusion.Windows.Forms.ButtonAppearance.Metro;
            this.buttonAdv3.BackColor = System.Drawing.Color.Transparent;
            this.buttonAdv3.BackgroundImage = global::DocumentosFX.Properties.Resources.botonesPrincipalTrasladoCEENuevo;
            this.buttonAdv3.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.buttonAdv3.BeforeTouchSize = new System.Drawing.Size(117, 124);
            this.buttonAdv3.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonAdv3.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonAdv3.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.buttonAdv3.IsBackStageButton = false;
            this.buttonAdv3.Location = new System.Drawing.Point(126, 195);
            this.buttonAdv3.MetroColor = System.Drawing.Color.Transparent;
            this.buttonAdv3.Name = "buttonAdv3";
            this.buttonAdv3.Size = new System.Drawing.Size(117, 124);
            this.buttonAdv3.TabIndex = 4;
            this.buttonAdv3.Click += new System.EventHandler(this.buttonAdv2_Click_1);
            // 
            // buttonAdv2
            // 
            this.buttonAdv2.Appearance = Syncfusion.Windows.Forms.ButtonAppearance.Metro;
            this.buttonAdv2.BackColor = System.Drawing.Color.Transparent;
            this.buttonAdv2.BackgroundImage = global::DocumentosFX.Properties.Resources.descargabotonesPrincipal;
            this.buttonAdv2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.buttonAdv2.BeforeTouchSize = new System.Drawing.Size(117, 124);
            this.buttonAdv2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonAdv2.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonAdv2.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.buttonAdv2.IsBackStageButton = false;
            this.buttonAdv2.Location = new System.Drawing.Point(3, 195);
            this.buttonAdv2.MetroColor = System.Drawing.Color.Transparent;
            this.buttonAdv2.Name = "buttonAdv2";
            this.buttonAdv2.Size = new System.Drawing.Size(117, 124);
            this.buttonAdv2.TabIndex = 4;
            this.buttonAdv2.Click += new System.EventHandler(this.buttonAdv2_Click_2);
            // 
            // btnConfiguracion
            // 
            this.btnConfiguracion.Appearance = Syncfusion.Windows.Forms.ButtonAppearance.Metro;
            this.btnConfiguracion.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.btnConfiguracion.BackColor = System.Drawing.Color.Transparent;
            this.btnConfiguracion.BackgroundImage = global::DocumentosFX.Properties.Resources.config_white;
            this.btnConfiguracion.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.btnConfiguracion.BeforeTouchSize = new System.Drawing.Size(83, 94);
            this.btnConfiguracion.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnConfiguracion.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnConfiguracion.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnConfiguracion.IsBackStageButton = false;
            this.btnConfiguracion.Location = new System.Drawing.Point(639, 43);
            this.btnConfiguracion.MetroColor = System.Drawing.Color.Transparent;
            this.btnConfiguracion.Name = "btnConfiguracion";
            this.btnConfiguracion.Size = new System.Drawing.Size(83, 94);
            this.btnConfiguracion.TabIndex = 3;
            this.btnConfiguracion.Click += new System.EventHandler(this.buttonAdv5_Click_1);
            // 
            // btnCE
            // 
            this.btnCE.Appearance = Syncfusion.Windows.Forms.ButtonAppearance.Metro;
            this.btnCE.BackColor = System.Drawing.Color.Transparent;
            this.btnCE.BackgroundImage = global::DocumentosFX.Properties.Resources.facturacionelectronica_04;
            this.btnCE.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.btnCE.BeforeTouchSize = new System.Drawing.Size(117, 124);
            this.btnCE.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnCE.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCE.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnCE.IsBackStageButton = false;
            this.btnCE.Location = new System.Drawing.Point(494, 36);
            this.btnCE.MetroColor = System.Drawing.Color.Transparent;
            this.btnCE.Name = "btnCE";
            this.btnCE.Size = new System.Drawing.Size(117, 124);
            this.btnCE.TabIndex = 1;
            this.btnCE.Click += new System.EventHandler(this.buttonAdv4_Click);
            // 
            // btnCxC
            // 
            this.btnCxC.Appearance = Syncfusion.Windows.Forms.ButtonAppearance.Metro;
            this.btnCxC.BackColor = System.Drawing.Color.Transparent;
            this.btnCxC.BackgroundImage = global::DocumentosFX.Properties.Resources.facturacionelectronica_02;
            this.btnCxC.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.btnCxC.BeforeTouchSize = new System.Drawing.Size(117, 124);
            this.btnCxC.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnCxC.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCxC.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnCxC.IsBackStageButton = false;
            this.btnCxC.Location = new System.Drawing.Point(249, 37);
            this.btnCxC.MetroColor = System.Drawing.Color.Transparent;
            this.btnCxC.Name = "btnCxC";
            this.btnCxC.Size = new System.Drawing.Size(117, 124);
            this.btnCxC.TabIndex = 1;
            this.btnCxC.Click += new System.EventHandler(this.buttonAdv3_Click);
            // 
            // buttonAdv1
            // 
            this.buttonAdv1.Appearance = Syncfusion.Windows.Forms.ButtonAppearance.Metro;
            this.buttonAdv1.BackColor = System.Drawing.Color.Transparent;
            this.buttonAdv1.BackgroundImage = global::DocumentosFX.Properties.Resources.icono_16;
            this.buttonAdv1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.buttonAdv1.BeforeTouchSize = new System.Drawing.Size(117, 124);
            this.buttonAdv1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonAdv1.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonAdv1.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.buttonAdv1.IsBackStageButton = false;
            this.buttonAdv1.Location = new System.Drawing.Point(126, 37);
            this.buttonAdv1.MetroColor = System.Drawing.Color.Transparent;
            this.buttonAdv1.Name = "buttonAdv1";
            this.buttonAdv1.Size = new System.Drawing.Size(117, 124);
            this.buttonAdv1.TabIndex = 1;
            this.buttonAdv1.Click += new System.EventHandler(this.buttonAdv1_Click_1);
            // 
            // btnCxP
            // 
            this.btnCxP.Appearance = Syncfusion.Windows.Forms.ButtonAppearance.Metro;
            this.btnCxP.BackColor = System.Drawing.Color.Transparent;
            this.btnCxP.BackgroundImage = global::DocumentosFX.Properties.Resources.facturacionelectronica2;
            this.btnCxP.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.btnCxP.BeforeTouchSize = new System.Drawing.Size(117, 124);
            this.btnCxP.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnCxP.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCxP.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnCxP.IsBackStageButton = false;
            this.btnCxP.Location = new System.Drawing.Point(372, 36);
            this.btnCxP.MetroColor = System.Drawing.Color.Transparent;
            this.btnCxP.Name = "btnCxP";
            this.btnCxP.Size = new System.Drawing.Size(117, 124);
            this.btnCxP.TabIndex = 1;
            this.btnCxP.Click += new System.EventHandler(this.buttonAdv2_Click);
            // 
            // btnFE
            // 
            this.btnFE.Appearance = Syncfusion.Windows.Forms.ButtonAppearance.Metro;
            this.btnFE.BackColor = System.Drawing.Color.Transparent;
            this.btnFE.BackgroundImage = global::DocumentosFX.Properties.Resources.facturacionelectronica_03;
            this.btnFE.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.btnFE.BeforeTouchSize = new System.Drawing.Size(117, 124);
            this.btnFE.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnFE.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnFE.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnFE.IsBackStageButton = false;
            this.btnFE.Location = new System.Drawing.Point(3, 37);
            this.btnFE.MetroColor = System.Drawing.Color.Transparent;
            this.btnFE.Name = "btnFE";
            this.btnFE.Size = new System.Drawing.Size(117, 124);
            this.btnFE.TabIndex = 1;
            this.btnFE.Click += new System.EventHandler(this.buttonAdv1_Click);
            this.btnFE.MouseHover += new System.EventHandler(this.buttonAdv1_MouseHover);
            // 
            // frmPrincipal
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CaptionBarColor = System.Drawing.Color.Indigo;
            this.CaptionBarHeight = 70;
            captionImage1.Image = global::DocumentosFX.Properties.Resources.fondo1Asset_principal;
            captionImage1.Location = new System.Drawing.Point(170, 5);
            captionImage1.Name = "CaptionImage1";
            captionImage1.Size = new System.Drawing.Size(175, 60);
            this.CaptionImages.Add(captionImage1);
            this.ClientSize = new System.Drawing.Size(740, 339);
            this.Controls.Add(this.buttonAdv7);
            this.Controls.Add(this.buttonAdv6);
            this.Controls.Add(this.buttonAdv5);
            this.Controls.Add(this.buttonAdv4);
            this.Controls.Add(this.buttonAdv3);
            this.Controls.Add(this.buttonAdv2);
            this.Controls.Add(this.btnConfiguracion);
            this.Controls.Add(this.bienvenido);
            this.Controls.Add(this.btnCE);
            this.Controls.Add(this.btnCxC);
            this.Controls.Add(this.buttonAdv1);
            this.Controls.Add(this.btnCxP);
            this.Controls.Add(this.btnFE);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(2);
            this.MaximizeBox = false;
            this.MetroColor = System.Drawing.Color.Indigo;
            this.Name = "frmPrincipal";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.frmPrincipal_FormClosed);
            this.ResumeLayout(false);

        }

        #endregion

        private Syncfusion.Windows.Forms.ButtonAdv btnFE;
        private Syncfusion.Windows.Forms.ButtonAdv btnCxP;
        private Syncfusion.Windows.Forms.ButtonAdv btnCxC;
        private Syncfusion.Windows.Forms.ButtonAdv btnCE;
        private System.Windows.Forms.Label bienvenido;
        private Syncfusion.Windows.Forms.ButtonAdv btnConfiguracion;
        private Syncfusion.Windows.Forms.ButtonAdv buttonAdv1;
        private Syncfusion.Windows.Forms.ButtonAdv buttonAdv2;
        private Syncfusion.Windows.Forms.ButtonAdv buttonAdv3;
        private Syncfusion.Windows.Forms.ButtonAdv buttonAdv4;
        private Syncfusion.Windows.Forms.ButtonAdv buttonAdv5;
        private Syncfusion.Windows.Forms.ButtonAdv buttonAdv6;
        private Syncfusion.Windows.Forms.ButtonAdv buttonAdv7;
    }
}