#region Copyright Syncfusion Inc. 2001 - 2017
// Copyright Syncfusion Inc. 2001 - 2017. All rights reserved.
// Use of this code is subject to the terms of our license.
// A copy of the current license can be obtained at any time by e-mailing
// licensing@syncfusion.com. Any infringement will be prosecuted under
// applicable laws. 
#endregion
using Generales;
using ModeloDocumentosFX;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Drawing;
using System.IO;
using System.Net;
using System.Text;
using System.Windows.Forms;
using System.Linq;
using System.Globalization;
using DocumentosFX.clases;

namespace DocumentosFX.formularios
{
    public partial class frmPendientesSyncTighitco : Syncfusion.Windows.Forms.MetroForm
    {

        public frmPendientesSyncTighitco()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            cargar("78",dtg,false);
            cargar("31", dtgCUU,false);
            cargar("31", dtgOtros, true);
        }

        private void cargar(string lugarexpedicion, DataGridView dtgP, bool diferente)
        {
            string url = ConfigurationManager.AppSettings["portal"].ToString() + "CxP/documentos/";
            try
            {
                // You need to post the data as key value pairs:
                string postData = "inicio=" + inicio.Value.ToString("yyyy-MM-dd")
                    + "&fin=" + fin.Value.ToString("yyyy-MM-dd")
                    + "&rfc=" + rfc.Text
                    + "&estado=" + estado.Text
                    + "&uuid="
                    + "&folio="
                    + "&lugarexpedicion=" + lugarexpedicion
                    + "&cliente=TIGHITCO"
                    + "&diferente=";
                if (diferente)
                {
                    postData += "&diferente=true";
                }
                byte[] byteArray = Encoding.UTF8.GetBytes(postData);

                // Post the data to the right place.
                Uri target = new Uri(url);
                WebRequest request = WebRequest.Create(target);

                request.Method = "POST";
                request.ContentType = "application/x-www-form-urlencoded";
                request.ContentLength = byteArray.Length;

                using (var dataStream = request.GetRequestStream())
                {
                    dataStream.Write(byteArray, 0, byteArray.Length);
                }

                using (var response = (HttpWebResponse)request.GetResponse())
                {
                    Console.WriteLine("Error code: {0}", response.StatusCode);
                    using (Stream data = response.GetResponseStream())
                    using (var reader = new StreamReader(data))
                    {
                        string text = reader.ReadToEnd();
                        DataTable table = JsonConvert.DeserializeObject<DataTable>(text);
                        vaciarDatos(table, dtgP);
                    }
                }

            }
            catch (Exception error)
            {
                ErrorFX.mostrar(error, true, true);
            }
        }

        private void vaciarDatos(DataTable table, DataGridView dtgP)
        {
            try
            {
                ModeloDocumentosFXContainer dbContext = new ModeloDocumentosFXContainer();

                dsCxP.dtConciliacionDataTable odtConciliacionDataTable = new dsCxP.dtConciliacionDataTable();

                foreach (DataRow registro in table.Rows)
                {
                    string uuidtr = registro["UUID"].ToString();
                    cxpConciliacion registroConciliacion = (from r in dbContext.cxpConciliacionSet.Where
                                              (a => a.uuid == uuidtr)
                                                            select r).FirstOrDefault();

                    string pdftr = "";
                    if (!String.IsNullOrEmpty(registro["pdf"].ToString()))
                    {
                        pdftr = registro["pdf"].ToString();
                    }
                    string xmltr = "";
                    if (!String.IsNullOrEmpty(registro["xml"].ToString()))
                    {
                        xmltr = registro["xml"].ToString();
                    }

                    string foliotr = "";
                    if (String.IsNullOrEmpty(registro["folio"].ToString()))
                    {
                        foliotr = uuidtr.Substring(uuidtr.Length - 5, 5);
                    }
                    else
                    {
                        foliotr = registro["folio"].ToString();
                    }


                    string SolicitudServicioTr = "";
                    if (!String.IsNullOrEmpty(registro["solicitudServicio"].ToString()))
                    {
                        SolicitudServicioTr = registro["solicitudServicio"].ToString();
                    }
                    string SolicitudServicioNotasTr = "";
                    if (!String.IsNullOrEmpty(registro["solicitudServicioNotas"].ToString()))
                    {
                        SolicitudServicioNotasTr = registro["solicitudServicioNotas"].ToString();
                    }
                    string SolicitudServicioTipoTr = "";
                    if (!String.IsNullOrEmpty(registro["SolicitudServicioTipo"].ToString()))
                    {
                        SolicitudServicioTipoTr = registro["SolicitudServicioTipo"].ToString();
                    }


                    if (registroConciliacion != null)
                    {
                        if (!faltaSincronizar.Checked)
                        {
                            odtConciliacionDataTable.AdddtConciliacionRow(
                                registroConciliacion.uuid
                                , foliotr
                                , decimal.Parse(registro["total"].ToString())
                                , registroConciliacion.voucherId
                                , registroConciliacion.rfcEmisor
                                , registro["emisor"].ToString()
                                , pdftr
                                , xmltr
                                , registro["usoCFDI"].ToString()
                                , registro["metodoPago"].ToString()
                                , registro["formaPago"].ToString()
                                , registro["oc"].ToString()
                                ,"","","",""
                                ,"","",""
                                );
                        }
                    }
                    else
                    {
                        odtConciliacionDataTable.AdddtConciliacionRow(
                        uuidtr
                        , foliotr
                        , decimal.Parse(registro["total"].ToString())
                        , ""
                        , registro["rfcEmisor"].ToString()
                        , registro["emisor"].ToString()
                        , pdftr
                        , xmltr
                        , registro["usoCFDI"].ToString()
                        , registro["metodoPago"].ToString()
                        , registro["formaPago"].ToString()
                        , registro["oc"].ToString()
                        , "", "","",""
                        , SolicitudServicioTr
                        , SolicitudServicioNotasTr
                        , SolicitudServicioTipoTr
                        );
                    }

                }

                dtgP.DataSource = odtConciliacionDataTable;

                dtgP.Columns["xml"].Visible = this.dtg.Columns["pdf"].Visible = false;

                this.dtg.Columns["xml"].Visible = this.dtg.Columns["pdf"].Visible = false;

                this.dtg.Columns["folio"].HeaderText = "Folio CFDI";

                this.dtg.Columns["payableId"].HeaderText = "Voucher ID";
                this.dtg.Columns["rfc"].HeaderText = "RFC";
                this.dtg.Columns["razon"].HeaderText = "Razón";
                this.dtg.Columns["total"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
                this.dtg.Columns["total"].HeaderText = "Total";
                this.dtg.Columns["uuid"].HeaderText = "UUID";
                this.dtg.Columns["total"].DefaultCellStyle.Format = "c2";
                this.dtg.Columns["total"].DefaultCellStyle.FormatProvider = CultureInfo.GetCultureInfo("es-MX");


                dtgP.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.DisplayedCells;

                dbContext.Dispose();
            }
            catch (Exception error)
            {
                ErrorFX.mostrar(error, true, true);
            }
        }

        private void dtgOtros_MouseClick(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Right)
            {
                contextMenuStrip1.Show(dtg, new Point(e.X, e.Y));
            }
        }

        private void tabControl1_MouseClick(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Right)
            {
                contextMenuStrip1.Show(dtg, new Point(e.X, e.Y));
            }
        }

        private void dtg_MouseClick(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Right)
            {
                contextMenuStrip1.Show(dtg, new Point(e.X, e.Y));
            }
        }

        private void dtg_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            //Copiar registros en el CLipboard
            if (e.RowIndex != -1)
            {
                string foliotr = dtg.Rows[e.RowIndex].Cells["folio"].Value.ToString();
                System.Windows.Forms.Clipboard.SetText(foliotr);
                AutoClosingMessageBox.Show("Folio: " + foliotr + " copiado para su uso", ProductName + " " + ProductVersion);
            }
        }

        private void dtgCUU_CellContentDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            //Copiar registros en el CLipboard
            if (e.RowIndex != -1)
            {
                string foliotr = dtgCUU.Rows[e.RowIndex].Cells["folio"].Value.ToString();
                System.Windows.Forms.Clipboard.SetText(foliotr);
                AutoClosingMessageBox.Show("Folio: " + foliotr + " copiado para su uso", ProductName + " " + ProductVersion);
            }
        }

        private void dtgOtros_CellContentDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            //Copiar registros en el CLipboard
            if (e.RowIndex != -1)
            {
                string foliotr = dtgOtros.Rows[e.RowIndex].Cells["folio"].Value.ToString();
                System.Windows.Forms.Clipboard.SetText(foliotr);
                AutoClosingMessageBox.Show("Folio: " + foliotr + " copiado para su uso", ProductName + " " + ProductVersion);
            }
        }

        private void subirPDFToolStripMenuItem_Click(object sender, EventArgs e)
        {
            subirPDF();
        }

        private void subirPDF()
        {
            try
            {
                foreach (DataGridViewRow registro in dtg.SelectedRows)
                {
                    string uuid = registro.Cells["uuid"].Value.ToString();
                    string rfc = registro.Cells["rfc"].Value.ToString();

                    OpenFileDialog openFileDialog1 = new OpenFileDialog();
                    openFileDialog1.CheckFileExists = true;
                    openFileDialog1.CheckPathExists = true;
                    openFileDialog1.DefaultExt = "pdf";
                    openFileDialog1.Filter = "PDF archivos (*.pdf)|*.pdf";
                    if (openFileDialog1.ShowDialog() == DialogResult.OK)
                    {
                        string pdftr = openFileDialog1.FileName;
                        string ruta = subirPDF(pdftr, uuid, rfc);
                        if (!String.IsNullOrEmpty(ruta))
                        {
                            dtg.Rows[registro.Index].Cells["pdf"].Value = ruta;
                        }
                    }
                }
            }
            catch (Exception error)
            {
                ErrorFX.mostrar(error, true, true, "ucCxP - 353 ");
            }
        }

        private string subirPDF(string pdf, string uuid, string rfc)
        {
            using (WebClient client = new WebClient())
            {
                string url = ConfigurationManager.AppSettings["portal"].ToString() + "Conector/subirPDF/";

                HttpWebRequest requestToServerEndpoint =
                (HttpWebRequest)WebRequest.Create(url);

                string boundaryString = "----SomeRandomText";
                string fileUrl = pdf;

                // Set the http request header \\
                requestToServerEndpoint.Method = WebRequestMethods.Http.Post;
                requestToServerEndpoint.ContentType = "multipart/form-data; boundary=" + boundaryString;
                requestToServerEndpoint.KeepAlive = true;
                requestToServerEndpoint.Credentials = System.Net.CredentialCache.DefaultCredentials;

                // Use a MemoryStream to form the post data request,
                // so that we can get the content-length attribute.
                MemoryStream postDataStream = new MemoryStream();
                StreamWriter postDataWriter = new StreamWriter(postDataStream);

                // Include value from the myFileDescription text area in the post data
                postDataWriter.Write("\r\n--" + boundaryString + "\r\n");
                postDataWriter.Write("Content-Disposition: form-data; name=\"{0}\"\r\n\r\n{1}", "uuid", uuid);

                postDataWriter.Write("\r\n--" + boundaryString + "\r\n");
                postDataWriter.Write("Content-Disposition: form-data; name=\"{0}\"\r\n\r\n{1}", "rfc", rfc);

                // Include the file in the post data
                postDataWriter.Write("\r\n--" + boundaryString + "\r\n");
                postDataWriter.Write("Content-Disposition: form-data;"
                + "name=\"{0}\";"
                + "filename=\"{1}\""
                + "\r\nContent-Type: {2}\r\n\r\n",
                "PDF_FILE",
                Path.GetFileName(fileUrl),
                Path.GetExtension(fileUrl));
                postDataWriter.Flush();
                // Read the file
                FileStream fileStream = new FileStream(fileUrl, FileMode.Open, FileAccess.Read);
                byte[] buffer = new byte[1024];
                int bytesRead = 0;
                while ((bytesRead = fileStream.Read(buffer, 0, buffer.Length)) != 0)
                {
                    postDataStream.Write(buffer, 0, bytesRead);
                }
                fileStream.Close();
                postDataWriter.Write("\r\n--" + boundaryString + "--\r\n");
                postDataWriter.Flush();

                // Set the http request body content length
                requestToServerEndpoint.ContentLength = postDataStream.Length;

                // Dump the post data from the memory stream to the request stream
                using (Stream s = requestToServerEndpoint.GetRequestStream())
                {
                    postDataStream.WriteTo(s);
                }
                postDataStream.Close();

                WebResponse response = requestToServerEndpoint.GetResponse();
                StreamReader responseReader = new StreamReader(response.GetResponseStream());
                string replyFromServer = responseReader.ReadToEnd();

                //Convertir en Json ver los errores
                try
                {
                    cResultadoSubir resultado = JsonConvert.DeserializeObject<cResultadoSubir>(replyFromServer);
                    if (resultado.status)
                    {
                        string rutatr = resultado.mensaje.Replace("Archivo creado exitosamente: ", "");
                        return rutatr;
                    }
                }
                catch (Exception error)
                {
                    ErrorFX.mostrar(error, true, true, true);
                }

                return null;
            }
        }

        private void button4_Click(object sender, EventArgs e)
        {
            if(this.tabControl1.SelectedTab.Name.Equals("tabPage1"))
            {
                ExcelFX.exportar(dtg, "Conciliación CxP SLP");
            }

            if (this.tabControl1.SelectedTab.Name.Equals("tabPage2"))
            {
                ExcelFX.exportar(dtgCUU, "Conciliación CxP CUU");
            }

            if (this.tabControl1.SelectedTab.Name.Equals("tabPage3"))
            {
                ExcelFX.exportar(dtgOtros, "Conciliación CxP Otro");
            }
        }
    }
}
