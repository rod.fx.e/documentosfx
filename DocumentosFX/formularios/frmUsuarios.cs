#region Copyright Syncfusion Inc. 2001 - 2017
// Copyright Syncfusion Inc. 2001 - 2017. All rights reserved.
// Use of this code is subject to the terms of our license.
// A copy of the current license can be obtained at any time by e-mailing
// licensing@syncfusion.com. Any infringement will be prosecuted under
// applicable laws. 
#endregion
using OfficeOpenXml;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.Entity.Validation;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using ModeloDocumentosFX;

namespace ControlGastos.formularios
{
    public partial class frmUsuarios : Syncfusion.Windows.Forms.MetroForm
    {
        public frmUsuarios()
        {
            InitializeComponent();
            cargar();
        }

        private void btnAddItem_Click(object sender, EventArgs e)
        {
            frmUsuario o = new frmUsuario();
            o.ShowDialog();
            cargar();
        }
        private void btnDeleteItem_Click(object sender, EventArgs e)
        {
            eliminar();
        }

        private void eliminar()
        {
            dtg.EndEdit();
            if (dtg.SelectedCells.Count > 0)
            {
                DialogResult Resultado = MessageBox.Show("�Desea eliminar las seleccionada?", Application.ProductName + "-" + Application.ProductVersion, MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                if (Resultado.ToString() != "Yes")
                {
                    return;
                }
                foreach (DataGridViewRow drv in dtg.SelectedRows)
                {
                    usuarios oRegistroTMP = (usuarios)drv.Tag;
                    ModeloDocumentosFX.ModeloDocumentosFXContainer dbContext = new ModeloDocumentosFXContainer();

                    usuarios oRegistro = dbContext.usuariosSet
                        .Where(a => a.Id == oRegistroTMP.Id)
                        .FirstOrDefault();


                    dbContext.usuariosSet.Attach(oRegistro);
                    dbContext.usuariosSet.Remove(oRegistro);
                    dbContext.SaveChanges();
                    dtg.Rows.Remove(drv);
                }
            }
            else
            {
                MessageBox.Show("Seleccione alguna l�nea.", Application.ProductName + "-" + Application.ProductVersion, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }

        private void cargar()
        {
            dtg.Rows.Clear();
            ModeloDocumentosFX.ModeloDocumentosFXContainer dbContext = new ModeloDocumentosFXContainer();
            try
            {
                var result = from b in dbContext.usuariosSet
                             .Where(a => a.usuario.Contains(buscar.Text))
                             select b;

                List<usuarios> dt = result.ToList();
                dtg.Rows.Clear();
                foreach (usuarios registro in dt)
                {
                    int n = dtg.Rows.Add();
                    dtg.Rows[n].Tag = registro;
                    dtg.Rows[n].Cells["usuario"].Value = registro.usuario;
                    dtg.Rows[n].Cells["nombreCompleto"].Value = registro.nombreCompleto;
                }
                dbContext.Dispose();
                this.dtg.Columns[0].AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;
                this.dtg.Columns[1].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
            }
            catch (DbEntityValidationException e)
            {
                foreach (var eve in e.EntityValidationErrors)
                {
                    foreach (var ve in eve.ValidationErrors)
                    {
                        MessageBox.Show(this, ve.PropertyName + " Error: " + ve.ErrorMessage, Application.ProductName
                            , MessageBoxButtons.OK, MessageBoxIcon.Error);
                        return;
                    }
                }
            }


        }

        private void buscar_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)13)
            {
                cargar();
            }
        }

        private void dtg_CellDoubleClick_1(object sender, DataGridViewCellEventArgs e)
        {
            cargar_registro(e);
        }

        private void cargar_registro(DataGridViewCellEventArgs e)
        {
            if (e.RowIndex > -1)
            {
                usuarios registro = (usuarios)dtg.Rows[e.RowIndex].Tag;
                formularios.frmUsuario oObjeto = new formularios.frmUsuario(registro);
                oObjeto.ShowDialog();
                cargar();
            }
        }

        private void btnDeleteItem_Click_1(object sender, EventArgs e)
        {
            eliminar();
        }
    }
}
