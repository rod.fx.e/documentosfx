﻿using CxC;
using DocumentosFX.clases;
using DocumentosFX.formularios;
using Generales;
using ModeloDocumentosFX;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DocumentosFX
{
    static class Program
    {

        //TODO: Aplicacion de pagos para finalizar 

        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main(string[] args)
        {
            //Application.Run(new formularios.frmDatos());
            //Application.Exit();
            //FormUtils.SetDefaultIcon();
            //Cargar configuración
            bool automatico = false;
            try
            {
                Globales.automatico = bool.Parse(ConfigurationManager.AppSettings["ActualizarAutomatico"].ToString());
            }
            catch
            {

            }

            
            //Console.WriteLine(cENCRIPTACION.Decrypt("iRamtJq5TdpY9Yk//IIBCiQFk3r5pgu3"));
            //Console.WriteLine(cENCRIPTACION.Decrypt("HqxkZeNLOqWeDN0JQm/GAuFBpNKVx2ny"));
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            //Application.Run(new formularios.frmLogin());
            if(args.Count()==0)
            {
                ModeloDocumentosFXContainer dbContext = new ModeloDocumentosFXContainer();
                formularios.frmLogin login = new formularios.frmLogin();
                if (login.ShowDialog() == DialogResult.OK)
                {
                    if (Globales.usuario != null)
                    {
                        if (Globales.usuario.usuario.ToLower() == "logistica")
                        {
                            string conection = dbContext.Database.Connection.ConnectionString;
                            Application.Run(new FE_FX.frmPrincipalFX(conection, Globales.usuario.usuario));
                            Application.Exit();
                        }
                        

                        if (Globales.usuario.usuario.ToLower() == "traslado")
                        {
                            Application.Run(new FE_FX.CFDI.frmTraslados());
                            Application.Exit();
                        }
                        else
                        {
                            if (Globales.usuario.usuario.ToUpper() == "FE")
                            {
                                
                                try
                                {
                                    string conection = dbContext.Database.Connection.ConnectionString;
                                    Application.Run(new FE_FX.frmPrincipalFX(conection, Globales.usuario.usuario));
                                    Application.Exit();
                                }
                                catch (Exception err)
                                {
                                    ErrorFX.mostrar(err, false, true, "Program - 42 - ");
                                }
                            }
                            else
                            {
                                //Validar permisos del usuario si solo tiene 1 llamar la ventana donde tiene acceso
                                if ((!Globales.usuario.contabilidadElectronica)
                                    & (!Globales.usuario.facturacionElectronica)
                                    & (!Globales.usuario.cuentasCobrar)
                                    & (!Globales.usuario.configuracion)
                                    )
                                {
                                    if (Globales.usuario.cuentasPagar)
                                    {
                                        Application.Run(new formularios.frmCxP());
                                    }
                                }
                                else
                                {
                                    if ((!Globales.usuario.contabilidadElectronica)
                                                                    & (!Globales.usuario.facturacionElectronica)
                                                                    & (!Globales.usuario.cuentasPagar)
                                                                    & (!Globales.usuario.configuracion)
                                                                    )
                                    {
                                        if (Globales.usuario.cuentasCobrar)
                                        {
                                            Application.Run(new formularios.frmCxC());
                                        }
                                    }
                                    else
                                    {
                                        Application.Run(new formularios.frmPrincipal());
                                    }
                                }
                            }
                        }
                    }
                    else
                    {
                        Application.Exit();
                    }
                }
            }
            else
            {
                //Validar que sea automatico
                if (args.Length == 1)
                {
                    //Automatico
                    if (args[0].ToString().Equals("CFDIPROVEEDORES"))
                    {
                        Globales.automatico = true;
                        Application.Run(new frmCxPServicio());
                        Application.Exit();
                    }
                    else
                    {

                        Globales.automatico = true;
                        ModeloDocumentosFXContainer dbContext = new ModeloDocumentosFXContainer();
                        try
                        {
                            string conection = dbContext.Database.Connection.ConnectionString;
                            Application.Run(new FE_FX.frmPrincipalFX(conection, "automatico", true));
                            Application.Exit();
                        }
                        catch (Exception err)
                        {
                            ErrorFX.mostrar(err, false, true, "Program - 42 - ");
                        }
                    }
                }
                else
                {
                    //Cancelar
                    if (args.Length == 2)
                    {
                        //Cancelacion es C y Factura
                        if (args[0].ToUpper() == "C")
                        {
                            string factura = args[1];
                            //Cancelar la factura 
                            if (factura != "")
                            {
                                ModeloDocumentosFXContainer dbContext = new ModeloDocumentosFXContainer();
                                try
                                {
                                    string conection = dbContext.Database.Connection.ConnectionString;
                                    FE_FX.frmPrincipalFX o = new FE_FX.frmPrincipalFX(conection);
                                    if (o.cancelar(factura))
                                    {
                                        MessageBox.Show("La factura " + factura + " fue cancelada en el SAT.");
                                        Application.Exit();
                                    }
                                }
                                catch (Exception err)
                                {
                                    ErrorFX.mostrar(err, false, true, "frmEmpresa - 68 - ", false);
                                }
                            }

                            Application.Exit();
                        }

                        Application.Exit();
                    }
                    if (args.Length == 3)
                    {
                        //Clasificar de productos
                        string entity_id = args[0];
                        string invoice_id = args[1];
                        string customer_id = args[2];
                        MessageBox.Show(entity_id + " " + invoice_id + " " + customer_id);
                        ModeloDocumentosFXContainer dbContext = new ModeloDocumentosFXContainer();
                        try
                        {
                            string conection = dbContext.Database.Connection.ConnectionString;
                            Application.Run(new ClasificadorProductos.formularios.frmTipoRelacionNDC(entity_id, invoice_id, customer_id));
                            Application.Exit();
                        }
                        catch (Exception err)
                        {
                            ErrorFX.mostrar(err, false, true, "Program - 195 - ", false);
                        }
                    }
                    if (args.Length == 4)
                    {
                        //Adjuntar el CFDI para Pagos CxP
                        string tipo = args[0];
                        string documento = args[1];
                        string aplicacion = args[2];
                        string usuario = args[3];
                        Globales.automatico = true;
                        ModeloDocumentosFXContainer dbContext = new ModeloDocumentosFXContainer();
                        try
                        {
                            string conection = dbContext.Database.Connection.ConnectionString;
                            Application.Run(new FE_FX.frmBandejaComprobantePago(conection, "automatico", true));
                            Application.Exit();
                        }
                        catch (Exception err)
                        {
                            ErrorFX.mostrar(err, false, true, "Program - 216 - ");
                        }
                    }
                }
            }
        }
    }
}
