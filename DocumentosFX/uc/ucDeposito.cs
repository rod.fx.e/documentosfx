﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using DocumentosFX.clases;
using System.Data.Entity.Validation;
using ModeloDocumentosFX;
using Generales;

namespace DocumentosFX.uc
{
    public partial class ucDeposito : UserControl
    {
        public ucDeposito()
        {
            InitializeComponent();
        }

        private void ucDeposito_Load(object sender, EventArgs e)
        {

        }

        private void ucDeposito_Resize(object sender, EventArgs e)
        {
            this.dtg.Height = this.Height - this.groupBox1.Height-5;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            cargar();
        }

        private void cargar()
        {
            dtg.Rows.Clear();
            ModeloDocumentosFX.ModeloDocumentosFXContainer dbContext = new ModeloDocumentosFXContainer();
            try
            {
                string conection = dbContext.Database.Connection.ConnectionString;
                cCONEXCION oData = new cCONEXCION(conection);
                string sSQL = @"
                select
                from depositoSet d
                where 
                CAST(d.Id as varchar(10)) like '%" + buscar.Text + @"%'
                OR d.VISUALid like '%" + buscar.Text + @"%'
                ";
                DataTable oDataTable = oData.EjecutarConsulta(sSQL);
                if (oDataTable != null)
                {
                    foreach (DataRow registro in oDataTable.Rows)
                    {
                        
                    }

                }
                this.dtg.Columns[0].AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;
                this.dtg.Columns[1].AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;
                this.dtg.Columns[2].AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;
                this.dtg.Columns[3].AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;
                this.dtg.Columns[4].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;

            }
            catch (DbEntityValidationException e)
            {
                foreach (var eve in e.EntityValidationErrors)
                {
                    foreach (var ve in eve.ValidationErrors)
                    {
                        MessageBox.Show(this, ve.PropertyName + " Error: " + ve.ErrorMessage, Application.ProductName
                            , MessageBoxButtons.OK, MessageBoxIcon.Error);
                        return;
                    }
                }
            }
            finally
            {
                dbContext.Dispose();
            }
        }

        private void button7_Click(object sender, EventArgs e)
        {
            plantilla();
        }

        private void plantilla()
        {
            throw new NotImplementedException();
        }
    }
}
