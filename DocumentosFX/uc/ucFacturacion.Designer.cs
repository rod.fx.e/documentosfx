﻿namespace DocumentosFX.uc
{
    partial class ucFacturacion
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.dtg = new System.Windows.Forms.DataGridView();
            this.contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.procesarToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.modoRealToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.modoPruebaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.selecionarFormatoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.procesarModoPruebaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem2 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem3 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem4 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem5 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem6 = new System.Windows.Forms.ToolStripMenuItem();
            this.verToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.pDFToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.xMLToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.procesarSinComplementosToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.formatoPorClienteToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.enviarPorCorreoPDFYXMLToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.regenerarPDFToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.cancelarToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.generarAdendaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.exportarToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.conciliarToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.clasificarProductoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.validarEn69Y69BToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.configuraciónGeneralToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.empresasToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.seriesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.certificadosToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.usuariosToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.formatosToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.label1 = new System.Windows.Forms.Label();
            this.busqueda = new System.Windows.Forms.TextBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.baseDatos = new System.Windows.Forms.ComboBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.fin = new System.Windows.Forms.DateTimePicker();
            this.inicio = new System.Windows.Forms.DateTimePicker();
            this.label3 = new System.Windows.Forms.Label();
            this.mostrar = new System.Windows.Forms.ComboBox();
            this.button2 = new System.Windows.Forms.Button();
            this.button5 = new System.Windows.Forms.Button();
            this.button4 = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.top = new System.Windows.Forms.TextBox();
            this.button6 = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.dtg)).BeginInit();
            this.contextMenuStrip1.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // dtg
            // 
            this.dtg.AllowUserToAddRows = false;
            this.dtg.AllowUserToDeleteRows = false;
            this.dtg.AllowUserToResizeRows = false;
            this.dtg.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dtg.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.dtg.Location = new System.Drawing.Point(0, 118);
            this.dtg.Margin = new System.Windows.Forms.Padding(2);
            this.dtg.Name = "dtg";
            this.dtg.ReadOnly = true;
            this.dtg.RowTemplate.Height = 24;
            this.dtg.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dtg.Size = new System.Drawing.Size(917, 370);
            this.dtg.TabIndex = 0;
            this.dtg.MouseClick += new System.Windows.Forms.MouseEventHandler(this.dtg_MouseClick);
            // 
            // contextMenuStrip1
            // 
            this.contextMenuStrip1.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.contextMenuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.procesarToolStripMenuItem,
            this.procesarModoPruebaToolStripMenuItem,
            this.toolStripMenuItem4,
            this.verToolStripMenuItem,
            this.procesarSinComplementosToolStripMenuItem,
            this.formatoPorClienteToolStripMenuItem,
            this.enviarPorCorreoPDFYXMLToolStripMenuItem,
            this.regenerarPDFToolStripMenuItem,
            this.cancelarToolStripMenuItem,
            this.generarAdendaToolStripMenuItem,
            this.exportarToolStripMenuItem,
            this.conciliarToolStripMenuItem,
            this.clasificarProductoToolStripMenuItem,
            this.validarEn69Y69BToolStripMenuItem,
            this.configuraciónGeneralToolStripMenuItem});
            this.contextMenuStrip1.Name = "contextMenuStrip1";
            this.contextMenuStrip1.Size = new System.Drawing.Size(239, 334);
            // 
            // procesarToolStripMenuItem
            // 
            this.procesarToolStripMenuItem.BackColor = System.Drawing.Color.LightSalmon;
            this.procesarToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.modoRealToolStripMenuItem,
            this.modoPruebaToolStripMenuItem,
            this.selecionarFormatoToolStripMenuItem});
            this.procesarToolStripMenuItem.Name = "procesarToolStripMenuItem";
            this.procesarToolStripMenuItem.Size = new System.Drawing.Size(238, 22);
            this.procesarToolStripMenuItem.Text = "CFDI 3.3";
            // 
            // modoRealToolStripMenuItem
            // 
            this.modoRealToolStripMenuItem.BackColor = System.Drawing.Color.LightCoral;
            this.modoRealToolStripMenuItem.Name = "modoRealToolStripMenuItem";
            this.modoRealToolStripMenuItem.Size = new System.Drawing.Size(174, 22);
            this.modoRealToolStripMenuItem.Text = "Modo real";
            // 
            // modoPruebaToolStripMenuItem
            // 
            this.modoPruebaToolStripMenuItem.BackColor = System.Drawing.Color.MediumSeaGreen;
            this.modoPruebaToolStripMenuItem.Name = "modoPruebaToolStripMenuItem";
            this.modoPruebaToolStripMenuItem.Size = new System.Drawing.Size(174, 22);
            this.modoPruebaToolStripMenuItem.Text = "Modo prueba";
            // 
            // selecionarFormatoToolStripMenuItem
            // 
            this.selecionarFormatoToolStripMenuItem.Name = "selecionarFormatoToolStripMenuItem";
            this.selecionarFormatoToolStripMenuItem.Size = new System.Drawing.Size(174, 22);
            this.selecionarFormatoToolStripMenuItem.Text = "Selecionar formato";
            // 
            // procesarModoPruebaToolStripMenuItem
            // 
            this.procesarModoPruebaToolStripMenuItem.BackColor = System.Drawing.Color.LightSeaGreen;
            this.procesarModoPruebaToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuItem1,
            this.toolStripMenuItem2,
            this.toolStripMenuItem3});
            this.procesarModoPruebaToolStripMenuItem.Name = "procesarModoPruebaToolStripMenuItem";
            this.procesarModoPruebaToolStripMenuItem.Size = new System.Drawing.Size(238, 22);
            this.procesarModoPruebaToolStripMenuItem.Text = "CFDI 3.2";
            // 
            // toolStripMenuItem1
            // 
            this.toolStripMenuItem1.BackColor = System.Drawing.Color.LightCoral;
            this.toolStripMenuItem1.Name = "toolStripMenuItem1";
            this.toolStripMenuItem1.Size = new System.Drawing.Size(174, 22);
            this.toolStripMenuItem1.Text = "Modo real";
            // 
            // toolStripMenuItem2
            // 
            this.toolStripMenuItem2.BackColor = System.Drawing.Color.MediumSeaGreen;
            this.toolStripMenuItem2.Name = "toolStripMenuItem2";
            this.toolStripMenuItem2.Size = new System.Drawing.Size(174, 22);
            this.toolStripMenuItem2.Text = "Modo prueba";
            // 
            // toolStripMenuItem3
            // 
            this.toolStripMenuItem3.Name = "toolStripMenuItem3";
            this.toolStripMenuItem3.Size = new System.Drawing.Size(174, 22);
            this.toolStripMenuItem3.Text = "Selecionar formato";
            // 
            // toolStripMenuItem4
            // 
            this.toolStripMenuItem4.BackColor = System.Drawing.Color.LightGreen;
            this.toolStripMenuItem4.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuItem5,
            this.toolStripMenuItem6});
            this.toolStripMenuItem4.Name = "toolStripMenuItem4";
            this.toolStripMenuItem4.Size = new System.Drawing.Size(238, 22);
            this.toolStripMenuItem4.Text = "Generar Comprobante de Pago";
            // 
            // toolStripMenuItem5
            // 
            this.toolStripMenuItem5.BackColor = System.Drawing.Color.LightCoral;
            this.toolStripMenuItem5.Name = "toolStripMenuItem5";
            this.toolStripMenuItem5.Size = new System.Drawing.Size(146, 22);
            this.toolStripMenuItem5.Text = "Modo real";
            // 
            // toolStripMenuItem6
            // 
            this.toolStripMenuItem6.BackColor = System.Drawing.Color.MediumSeaGreen;
            this.toolStripMenuItem6.Name = "toolStripMenuItem6";
            this.toolStripMenuItem6.Size = new System.Drawing.Size(146, 22);
            this.toolStripMenuItem6.Text = "Modo prueba";
            // 
            // verToolStripMenuItem
            // 
            this.verToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.pDFToolStripMenuItem,
            this.xMLToolStripMenuItem});
            this.verToolStripMenuItem.Name = "verToolStripMenuItem";
            this.verToolStripMenuItem.Size = new System.Drawing.Size(238, 22);
            this.verToolStripMenuItem.Text = "Ver";
            // 
            // pDFToolStripMenuItem
            // 
            this.pDFToolStripMenuItem.Name = "pDFToolStripMenuItem";
            this.pDFToolStripMenuItem.Size = new System.Drawing.Size(98, 22);
            this.pDFToolStripMenuItem.Text = "PDF";
            this.pDFToolStripMenuItem.Click += new System.EventHandler(this.pDFToolStripMenuItem_Click);
            // 
            // xMLToolStripMenuItem
            // 
            this.xMLToolStripMenuItem.Name = "xMLToolStripMenuItem";
            this.xMLToolStripMenuItem.Size = new System.Drawing.Size(98, 22);
            this.xMLToolStripMenuItem.Text = "XML";
            this.xMLToolStripMenuItem.Click += new System.EventHandler(this.xMLToolStripMenuItem_Click);
            // 
            // procesarSinComplementosToolStripMenuItem
            // 
            this.procesarSinComplementosToolStripMenuItem.Name = "procesarSinComplementosToolStripMenuItem";
            this.procesarSinComplementosToolStripMenuItem.Size = new System.Drawing.Size(238, 22);
            this.procesarSinComplementosToolStripMenuItem.Text = "Procesar sin complementos";
            // 
            // formatoPorClienteToolStripMenuItem
            // 
            this.formatoPorClienteToolStripMenuItem.Name = "formatoPorClienteToolStripMenuItem";
            this.formatoPorClienteToolStripMenuItem.Size = new System.Drawing.Size(238, 22);
            this.formatoPorClienteToolStripMenuItem.Text = "Formato por cliente";
            // 
            // enviarPorCorreoPDFYXMLToolStripMenuItem
            // 
            this.enviarPorCorreoPDFYXMLToolStripMenuItem.Name = "enviarPorCorreoPDFYXMLToolStripMenuItem";
            this.enviarPorCorreoPDFYXMLToolStripMenuItem.Size = new System.Drawing.Size(238, 22);
            this.enviarPorCorreoPDFYXMLToolStripMenuItem.Text = "Enviar por correo PDF y XML";
            // 
            // regenerarPDFToolStripMenuItem
            // 
            this.regenerarPDFToolStripMenuItem.Name = "regenerarPDFToolStripMenuItem";
            this.regenerarPDFToolStripMenuItem.Size = new System.Drawing.Size(238, 22);
            this.regenerarPDFToolStripMenuItem.Text = "Regenerar PDF";
            // 
            // cancelarToolStripMenuItem
            // 
            this.cancelarToolStripMenuItem.Name = "cancelarToolStripMenuItem";
            this.cancelarToolStripMenuItem.Size = new System.Drawing.Size(238, 22);
            this.cancelarToolStripMenuItem.Text = "Cancelar";
            // 
            // generarAdendaToolStripMenuItem
            // 
            this.generarAdendaToolStripMenuItem.Name = "generarAdendaToolStripMenuItem";
            this.generarAdendaToolStripMenuItem.Size = new System.Drawing.Size(238, 22);
            this.generarAdendaToolStripMenuItem.Text = "Generar Adenda";
            // 
            // exportarToolStripMenuItem
            // 
            this.exportarToolStripMenuItem.Name = "exportarToolStripMenuItem";
            this.exportarToolStripMenuItem.Size = new System.Drawing.Size(238, 22);
            this.exportarToolStripMenuItem.Text = "Exportar";
            // 
            // conciliarToolStripMenuItem
            // 
            this.conciliarToolStripMenuItem.Name = "conciliarToolStripMenuItem";
            this.conciliarToolStripMenuItem.Size = new System.Drawing.Size(238, 22);
            this.conciliarToolStripMenuItem.Text = "Conciliar";
            // 
            // clasificarProductoToolStripMenuItem
            // 
            this.clasificarProductoToolStripMenuItem.Name = "clasificarProductoToolStripMenuItem";
            this.clasificarProductoToolStripMenuItem.Size = new System.Drawing.Size(238, 22);
            this.clasificarProductoToolStripMenuItem.Text = "Clasificar producto";
            // 
            // validarEn69Y69BToolStripMenuItem
            // 
            this.validarEn69Y69BToolStripMenuItem.Name = "validarEn69Y69BToolStripMenuItem";
            this.validarEn69Y69BToolStripMenuItem.Size = new System.Drawing.Size(238, 22);
            this.validarEn69Y69BToolStripMenuItem.Text = "Validar en 69 y 69-B";
            // 
            // configuraciónGeneralToolStripMenuItem
            // 
            this.configuraciónGeneralToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.empresasToolStripMenuItem,
            this.seriesToolStripMenuItem,
            this.certificadosToolStripMenuItem,
            this.usuariosToolStripMenuItem,
            this.formatosToolStripMenuItem});
            this.configuraciónGeneralToolStripMenuItem.Name = "configuraciónGeneralToolStripMenuItem";
            this.configuraciónGeneralToolStripMenuItem.Size = new System.Drawing.Size(238, 22);
            this.configuraciónGeneralToolStripMenuItem.Text = "Configuración General";
            // 
            // empresasToolStripMenuItem
            // 
            this.empresasToolStripMenuItem.Name = "empresasToolStripMenuItem";
            this.empresasToolStripMenuItem.Size = new System.Drawing.Size(137, 22);
            this.empresasToolStripMenuItem.Text = "Empresas";
            // 
            // seriesToolStripMenuItem
            // 
            this.seriesToolStripMenuItem.Name = "seriesToolStripMenuItem";
            this.seriesToolStripMenuItem.Size = new System.Drawing.Size(137, 22);
            this.seriesToolStripMenuItem.Text = "Series";
            // 
            // certificadosToolStripMenuItem
            // 
            this.certificadosToolStripMenuItem.Name = "certificadosToolStripMenuItem";
            this.certificadosToolStripMenuItem.Size = new System.Drawing.Size(137, 22);
            this.certificadosToolStripMenuItem.Text = "Certificados";
            // 
            // usuariosToolStripMenuItem
            // 
            this.usuariosToolStripMenuItem.Name = "usuariosToolStripMenuItem";
            this.usuariosToolStripMenuItem.Size = new System.Drawing.Size(137, 22);
            this.usuariosToolStripMenuItem.Text = "Usuarios";
            // 
            // formatosToolStripMenuItem
            // 
            this.formatosToolStripMenuItem.Name = "formatosToolStripMenuItem";
            this.formatosToolStripMenuItem.Size = new System.Drawing.Size(137, 22);
            this.formatosToolStripMenuItem.Text = "Formatos";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(123, 76);
            this.label1.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(55, 13);
            this.label1.TabIndex = 2;
            this.label1.Text = "Busqueda";
            // 
            // busqueda
            // 
            this.busqueda.Location = new System.Drawing.Point(202, 73);
            this.busqueda.Margin = new System.Windows.Forms.Padding(2);
            this.busqueda.Name = "busqueda";
            this.busqueda.Size = new System.Drawing.Size(169, 20);
            this.busqueda.TabIndex = 3;
            // 
            // groupBox1
            // 
            this.groupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox1.Controls.Add(this.button6);
            this.groupBox1.Controls.Add(this.baseDatos);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.fin);
            this.groupBox1.Controls.Add(this.inicio);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.mostrar);
            this.groupBox1.Controls.Add(this.button2);
            this.groupBox1.Controls.Add(this.button5);
            this.groupBox1.Controls.Add(this.button4);
            this.groupBox1.Controls.Add(this.button3);
            this.groupBox1.Controls.Add(this.button1);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.top);
            this.groupBox1.Controls.Add(this.busqueda);
            this.groupBox1.Location = new System.Drawing.Point(0, 2);
            this.groupBox1.Margin = new System.Windows.Forms.Padding(2);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Padding = new System.Windows.Forms.Padding(2);
            this.groupBox1.Size = new System.Drawing.Size(915, 112);
            this.groupBox1.TabIndex = 9;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Listado de facturas";
            // 
            // baseDatos
            // 
            this.baseDatos.FormattingEnabled = true;
            this.baseDatos.Items.AddRange(new object[] {
            "FIMA"});
            this.baseDatos.Location = new System.Drawing.Point(202, 18);
            this.baseDatos.Margin = new System.Windows.Forms.Padding(2);
            this.baseDatos.Name = "baseDatos";
            this.baseDatos.Size = new System.Drawing.Size(169, 21);
            this.baseDatos.TabIndex = 37;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(123, 22);
            this.label6.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(75, 13);
            this.label6.TabIndex = 36;
            this.label6.Text = "Base de datos";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(123, 50);
            this.label2.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(32, 13);
            this.label2.TabIndex = 32;
            this.label2.Text = "Inicio";
            // 
            // fin
            // 
            this.fin.CustomFormat = "dd/MM/yyyy HH:mm:ss";
            this.fin.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.fin.Location = new System.Drawing.Point(397, 46);
            this.fin.Margin = new System.Windows.Forms.Padding(2);
            this.fin.Name = "fin";
            this.fin.Size = new System.Drawing.Size(166, 20);
            this.fin.TabIndex = 34;
            // 
            // inicio
            // 
            this.inicio.CustomFormat = "dd/MM/yyyy HH:mm:ss";
            this.inicio.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.inicio.Location = new System.Drawing.Point(202, 46);
            this.inicio.Margin = new System.Windows.Forms.Padding(2);
            this.inicio.Name = "inicio";
            this.inicio.Size = new System.Drawing.Size(169, 20);
            this.inicio.TabIndex = 35;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(372, 50);
            this.label3.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(21, 13);
            this.label3.TabIndex = 33;
            this.label3.Text = "Fin";
            // 
            // mostrar
            // 
            this.mostrar.FormattingEnabled = true;
            this.mostrar.Items.AddRange(new object[] {
            "Pendientes",
            "Timbrados",
            "Todos"});
            this.mostrar.Location = new System.Drawing.Point(471, 20);
            this.mostrar.Margin = new System.Windows.Forms.Padding(2);
            this.mostrar.Name = "mostrar";
            this.mostrar.Size = new System.Drawing.Size(92, 21);
            this.mostrar.TabIndex = 31;
            // 
            // button2
            // 
            this.button2.BackColor = System.Drawing.Color.OrangeRed;
            this.button2.FlatAppearance.BorderSize = 0;
            this.button2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button2.ForeColor = System.Drawing.Color.White;
            this.button2.Image = global::DocumentosFX.Properties.Resources.if_24_171492;
            this.button2.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.button2.Location = new System.Drawing.Point(5, 46);
            this.button2.Margin = new System.Windows.Forms.Padding(2);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(107, 24);
            this.button2.TabIndex = 30;
            this.button2.Text = "Procesar";
            this.button2.UseVisualStyleBackColor = false;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // button5
            // 
            this.button5.BackColor = System.Drawing.Color.SeaGreen;
            this.button5.FlatAppearance.BorderSize = 0;
            this.button5.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button5.ForeColor = System.Drawing.Color.White;
            this.button5.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.button5.Location = new System.Drawing.Point(770, 12);
            this.button5.Margin = new System.Windows.Forms.Padding(2);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(90, 24);
            this.button5.TabIndex = 30;
            this.button5.Text = "Soporte";
            this.button5.UseVisualStyleBackColor = false;
            this.button5.Click += new System.EventHandler(this.button5_Click);
            // 
            // button4
            // 
            this.button4.BackColor = System.Drawing.Color.CadetBlue;
            this.button4.FlatAppearance.BorderSize = 0;
            this.button4.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button4.ForeColor = System.Drawing.Color.White;
            this.button4.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.button4.Location = new System.Drawing.Point(676, 12);
            this.button4.Margin = new System.Windows.Forms.Padding(2);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(90, 24);
            this.button4.TabIndex = 30;
            this.button4.Text = "Ayuda";
            this.button4.UseVisualStyleBackColor = false;
            this.button4.Click += new System.EventHandler(this.button4_Click);
            // 
            // button3
            // 
            this.button3.BackColor = System.Drawing.Color.DarkSeaGreen;
            this.button3.FlatAppearance.BorderSize = 0;
            this.button3.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button3.ForeColor = System.Drawing.Color.White;
            this.button3.Image = global::DocumentosFX.Properties.Resources.favicon;
            this.button3.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.button3.Location = new System.Drawing.Point(582, 12);
            this.button3.Margin = new System.Windows.Forms.Padding(2);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(90, 24);
            this.button3.TabIndex = 30;
            this.button3.Text = "Base legal";
            this.button3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button3.UseVisualStyleBackColor = false;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // button1
            // 
            this.button1.BackColor = System.Drawing.Color.Goldenrod;
            this.button1.FlatAppearance.BorderSize = 0;
            this.button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button1.ForeColor = System.Drawing.Color.White;
            this.button1.Image = global::DocumentosFX.Properties.Resources.if_68_171427;
            this.button1.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.button1.Location = new System.Drawing.Point(5, 16);
            this.button1.Margin = new System.Windows.Forms.Padding(2);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(107, 24);
            this.button1.TabIndex = 30;
            this.button1.Text = "Buscar";
            this.button1.UseVisualStyleBackColor = false;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(418, 76);
            this.label4.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(51, 13);
            this.label4.TabIndex = 29;
            this.label4.Text = "Registros";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(427, 23);
            this.label5.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(42, 13);
            this.label5.TabIndex = 2;
            this.label5.Text = "Mostrar";
            // 
            // top
            // 
            this.top.Location = new System.Drawing.Point(471, 73);
            this.top.Margin = new System.Windows.Forms.Padding(2);
            this.top.Name = "top";
            this.top.Size = new System.Drawing.Size(92, 20);
            this.top.TabIndex = 3;
            this.top.Text = "100";
            // 
            // button6
            // 
            this.button6.BackColor = System.Drawing.Color.OrangeRed;
            this.button6.FlatAppearance.BorderSize = 0;
            this.button6.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button6.ForeColor = System.Drawing.Color.White;
            this.button6.Image = global::DocumentosFX.Properties.Resources.if_24_171492;
            this.button6.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.button6.Location = new System.Drawing.Point(5, 78);
            this.button6.Margin = new System.Windows.Forms.Padding(2);
            this.button6.Name = "button6";
            this.button6.Size = new System.Drawing.Size(107, 24);
            this.button6.TabIndex = 38;
            this.button6.Text = "Automatico";
            this.button6.UseVisualStyleBackColor = false;
            // 
            // ucFacturacion
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.dtg);
            this.Margin = new System.Windows.Forms.Padding(2);
            this.Name = "ucFacturacion";
            this.Size = new System.Drawing.Size(917, 488);
            this.Load += new System.EventHandler(this.ucFacturacion_Load);
            this.Resize += new System.EventHandler(this.ucFacturacion_Resize);
            ((System.ComponentModel.ISupportInitialize)(this.dtg)).EndInit();
            this.contextMenuStrip1.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataGridView dtg;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip1;
        private System.Windows.Forms.ToolStripMenuItem formatoPorClienteToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem procesarToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem enviarPorCorreoPDFYXMLToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem regenerarPDFToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem cancelarToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem generarAdendaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem exportarToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem conciliarToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem clasificarProductoToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem validarEn69Y69BToolStripMenuItem;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox busqueda;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.ToolStripMenuItem configuraciónGeneralToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem empresasToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem seriesToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem certificadosToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem usuariosToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem selecionarFormatoToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem formatosToolStripMenuItem;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox top;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button5;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.ComboBox mostrar;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.DateTimePicker fin;
        private System.Windows.Forms.DateTimePicker inicio;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ComboBox baseDatos;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.ToolStripMenuItem procesarSinComplementosToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem procesarModoPruebaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem verToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem pDFToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem xMLToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem2;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem3;
        private System.Windows.Forms.ToolStripMenuItem modoRealToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem modoPruebaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem4;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem5;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem6;
        private System.Windows.Forms.Button button6;
    }
}
