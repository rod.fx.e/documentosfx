﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using DocumentosFX.clases;
using System.Data.Entity.Validation;
using ModeloDocumentosFX;
using System.IO;
using OfficeOpenXml;
using PlantillaFormulario;

namespace DocumentosFX.uc
{
    public partial class ucClasificarProductos : UserControl
    {
        public ucClasificarProductos()
        {
            InitializeComponent();
            cargar();
        }

        private void ucClasificarProductos_Load(object sender, EventArgs e)
        {

        }

        private void ucClasificarProductos_Resize(object sender, EventArgs e)
        {
            this.dtg.Height = this.Height - this.groupBox1.Height-5;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            cargar();
        }

        private void cargar()
        {
            try
            {
                ModeloDocumentosFX.ModeloDocumentosFXContainer dbContext = new ModeloDocumentosFXContainer();
                var result = from b in dbContext.configuracionProductoSet
                                             .Where(a => a.producto.Contains(buscar.Text))
                                             .OrderBy(a => a.producto)
                             select b;

                List<configuracionProducto> dt = result.ToList();
                dtg.Rows.Clear();
                foreach (configuracionProducto registro in dt)
                {
                    if (registro != null)
                    {
                        int n = dtg.Rows.Add();
                        dtg.Rows[n].Tag = registro;
                        dtg.Rows[n].Cells["producto"].Value = registro.producto;
                        dtg.Rows[n].Cells["clasificacionSAT"].Value = registro.clasificacionSAT;
                        

                    }

                }
                dbContext.Dispose();
                this.dtg.Columns[0].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
            }
            catch (DbEntityValidationException error)
            {
                foreach (var eve in error.EntityValidationErrors)
                {
                    foreach (var ve in eve.ValidationErrors)
                    {
                        MessageBox.Show(this, ve.PropertyName + " Error: " + ve.ErrorMessage, Application.ProductName
                            , MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
            }
        }

        private void button7_Click(object sender, EventArgs e)
        {
            plantilla();
        }

        private void plantilla()
        {
            importar();
        }
        private void importar()
        {
            Application.DoEvents();
            OpenFileDialog openFileDialog1 = new OpenFileDialog();
            openFileDialog1.Filter = "Excel files (*.xlsx)|*.xlsx|All files (*.*)|*.*";
            openFileDialog1.FilterIndex = 1;
            openFileDialog1.RestoreDirectory = true;

            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                try
                {
                    procesar(dtg, 1, openFileDialog1.FileName);
                }
                catch (Exception)
                {
                    MessageBox.Show("No existe el Archivo seleccionado", Application.ProductName + "-" + Application.ProductVersion, MessageBoxButtons.OK);
                }
                finally
                {
                    Application.DoEvents();
                }
            }
        }

        private void procesar(DataGridView dtg, int hoja, string archivo)
        {
            string tmpFile = Path.GetTempFileName();
            try
            {
                if (!File.Exists(archivo))
                {
                    MessageBox.Show(this, "El archivo " + archivo + " no existe.", Application.ProductName
                        , MessageBoxButtons.OK, MessageBoxIcon.Error);
                }

                File.Delete(tmpFile);
                File.Copy(archivo, tmpFile);

                dtg.Rows.Clear();
                int rowIndex = 1;
                FileInfo existingFile = new FileInfo(tmpFile);
                using (ExcelPackage package = new ExcelPackage(existingFile))
                {
                    ExcelWorksheet worksheet = package.Workbook.Worksheets[hoja];
                    while (worksheet.Cells[rowIndex, 2].Value != null)
                    {
                        try
                        {
                            string productotr = worksheet.Cells[rowIndex, 1].Text;
                            string clasificacion = worksheet.Cells[rowIndex, 2].Text;

                            guardar(productotr,clasificacion);
                        }
                        catch (DbEntityValidationException e)
                        {
                            foreach (var eve in e.EntityValidationErrors)
                            {
                                foreach (var ve in eve.ValidationErrors)
                                {
                                    MessageBox.Show(this, ve.PropertyName + " Error: " + ve.ErrorMessage, Application.ProductName
                                        , MessageBoxButtons.OK, MessageBoxIcon.Error);
                                }
                            }
                        }
                        finally
                        {
                            File.Delete(tmpFile);
                        }
                        rowIndex++;
                    }
                }
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message.ToString());
            }
            finally
            {
                File.Delete(tmpFile);
                cargar();
            }

        }

        private void guardar(string productotr, string clasificacion)
        {
            ModeloDocumentosFX.ModeloDocumentosFXContainer dbContext = new ModeloDocumentosFXContainer();
            try
            {
                configuracionProducto registro = new configuracionProducto();

                registro = (from r in dbContext.configuracionProductoSet.Where
                                        (a => a.producto == productotr 
                                        & a.clasificacionSAT==clasificacion)
                            select r).FirstOrDefault();

                registro.producto = productotr;
                registro.clasificacionSAT = clasificacion;

                if (registro==null)
                {
                    dbContext.configuracionProductoSet.Add(registro);
                }
                else
                {
                    dbContext.configuracionProductoSet.Attach(registro);
                    dbContext.Entry(registro).State = System.Data.Entity.EntityState.Modified;
                }

                dbContext.SaveChanges();
            }
            catch (DbEntityValidationException e)
            {
                foreach (var eve in e.EntityValidationErrors)
                {
                    foreach (var ve in eve.ValidationErrors)
                    {
                        MessageBox.Show(this, ve.PropertyName + " Error: " + ve.ErrorMessage, Application.ProductName
                            , MessageBoxButtons.OK, MessageBoxIcon.Error);
                        return;
                    }
                }
            }
        }

    private void button2_Click(object sender, EventArgs e)
    {
        frmProductoClasificacion o = new frmProductoClasificacion();
        o.Show();
    }

    private void dtg_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
    {
            cargar_registro(e);
    }

        private void cargar_registro(DataGridViewCellEventArgs e)
        {
            if (e.RowIndex > -1)
            {
                configuracionProducto registro = (configuracionProducto)dtg.Rows[e.RowIndex].Tag;
                frmProductoClasificacion o = new frmProductoClasificacion(registro);
                o.ShowDialog();
                cargar();
            }
        }
    }
}
