﻿namespace DocumentosFX.uc
{
    partial class ucDeposito
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.dtg = new System.Windows.Forms.DataGridView();
            this.contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.formatoPorClienteToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.procesarToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.enviarPorCorreoPDFYXMLToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.regenerarPDFToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.cancelarToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.generarComprobanteDePagoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.generarAdendaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.exportarToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.conciliarToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.clasificarProductoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.validarEn69Y69BToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.configuraciónGeneralToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.empresasToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.seriesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.certificadosToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.usuariosToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.label1 = new System.Windows.Forms.Label();
            this.buscar = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.dateTimePicker1 = new System.Windows.Forms.DateTimePicker();
            this.label3 = new System.Windows.Forms.Label();
            this.dateTimePicker2 = new System.Windows.Forms.DateTimePicker();
            this.checkBox1 = new System.Windows.Forms.CheckBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.button7 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.button5 = new System.Windows.Forms.Button();
            this.button4 = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.dtg)).BeginInit();
            this.contextMenuStrip1.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // dtg
            // 
            this.dtg.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dtg.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.dtg.Location = new System.Drawing.Point(0, 137);
            this.dtg.Name = "dtg";
            this.dtg.RowTemplate.Height = 24;
            this.dtg.Size = new System.Drawing.Size(1042, 449);
            this.dtg.TabIndex = 0;
            // 
            // contextMenuStrip1
            // 
            this.contextMenuStrip1.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.contextMenuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.formatoPorClienteToolStripMenuItem,
            this.procesarToolStripMenuItem,
            this.enviarPorCorreoPDFYXMLToolStripMenuItem,
            this.regenerarPDFToolStripMenuItem,
            this.cancelarToolStripMenuItem,
            this.generarComprobanteDePagoToolStripMenuItem,
            this.generarAdendaToolStripMenuItem,
            this.exportarToolStripMenuItem,
            this.conciliarToolStripMenuItem,
            this.clasificarProductoToolStripMenuItem,
            this.validarEn69Y69BToolStripMenuItem,
            this.configuraciónGeneralToolStripMenuItem});
            this.contextMenuStrip1.Name = "contextMenuStrip1";
            this.contextMenuStrip1.Size = new System.Drawing.Size(285, 292);
            // 
            // formatoPorClienteToolStripMenuItem
            // 
            this.formatoPorClienteToolStripMenuItem.Name = "formatoPorClienteToolStripMenuItem";
            this.formatoPorClienteToolStripMenuItem.Size = new System.Drawing.Size(284, 24);
            this.formatoPorClienteToolStripMenuItem.Text = "Formato por cliente";
            // 
            // procesarToolStripMenuItem
            // 
            this.procesarToolStripMenuItem.Name = "procesarToolStripMenuItem";
            this.procesarToolStripMenuItem.Size = new System.Drawing.Size(284, 24);
            this.procesarToolStripMenuItem.Text = "Procesar";
            // 
            // enviarPorCorreoPDFYXMLToolStripMenuItem
            // 
            this.enviarPorCorreoPDFYXMLToolStripMenuItem.Name = "enviarPorCorreoPDFYXMLToolStripMenuItem";
            this.enviarPorCorreoPDFYXMLToolStripMenuItem.Size = new System.Drawing.Size(284, 24);
            this.enviarPorCorreoPDFYXMLToolStripMenuItem.Text = "Enviar por correo PDF y XML";
            // 
            // regenerarPDFToolStripMenuItem
            // 
            this.regenerarPDFToolStripMenuItem.Name = "regenerarPDFToolStripMenuItem";
            this.regenerarPDFToolStripMenuItem.Size = new System.Drawing.Size(284, 24);
            this.regenerarPDFToolStripMenuItem.Text = "Regenerar PDF";
            // 
            // cancelarToolStripMenuItem
            // 
            this.cancelarToolStripMenuItem.Name = "cancelarToolStripMenuItem";
            this.cancelarToolStripMenuItem.Size = new System.Drawing.Size(284, 24);
            this.cancelarToolStripMenuItem.Text = "Cancelar";
            // 
            // generarComprobanteDePagoToolStripMenuItem
            // 
            this.generarComprobanteDePagoToolStripMenuItem.Name = "generarComprobanteDePagoToolStripMenuItem";
            this.generarComprobanteDePagoToolStripMenuItem.Size = new System.Drawing.Size(284, 24);
            this.generarComprobanteDePagoToolStripMenuItem.Text = "Generar Comprobante de Pago";
            // 
            // generarAdendaToolStripMenuItem
            // 
            this.generarAdendaToolStripMenuItem.Name = "generarAdendaToolStripMenuItem";
            this.generarAdendaToolStripMenuItem.Size = new System.Drawing.Size(284, 24);
            this.generarAdendaToolStripMenuItem.Text = "Generar Adenda";
            // 
            // exportarToolStripMenuItem
            // 
            this.exportarToolStripMenuItem.Name = "exportarToolStripMenuItem";
            this.exportarToolStripMenuItem.Size = new System.Drawing.Size(284, 24);
            this.exportarToolStripMenuItem.Text = "Exportar";
            // 
            // conciliarToolStripMenuItem
            // 
            this.conciliarToolStripMenuItem.Name = "conciliarToolStripMenuItem";
            this.conciliarToolStripMenuItem.Size = new System.Drawing.Size(284, 24);
            this.conciliarToolStripMenuItem.Text = "Conciliar";
            // 
            // clasificarProductoToolStripMenuItem
            // 
            this.clasificarProductoToolStripMenuItem.Name = "clasificarProductoToolStripMenuItem";
            this.clasificarProductoToolStripMenuItem.Size = new System.Drawing.Size(284, 24);
            this.clasificarProductoToolStripMenuItem.Text = "Clasificar producto";
            // 
            // validarEn69Y69BToolStripMenuItem
            // 
            this.validarEn69Y69BToolStripMenuItem.Name = "validarEn69Y69BToolStripMenuItem";
            this.validarEn69Y69BToolStripMenuItem.Size = new System.Drawing.Size(284, 24);
            this.validarEn69Y69BToolStripMenuItem.Text = "Validar en 69 y 69-B";
            // 
            // configuraciónGeneralToolStripMenuItem
            // 
            this.configuraciónGeneralToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.empresasToolStripMenuItem,
            this.seriesToolStripMenuItem,
            this.certificadosToolStripMenuItem,
            this.usuariosToolStripMenuItem});
            this.configuraciónGeneralToolStripMenuItem.Name = "configuraciónGeneralToolStripMenuItem";
            this.configuraciónGeneralToolStripMenuItem.Size = new System.Drawing.Size(284, 24);
            this.configuraciónGeneralToolStripMenuItem.Text = "Configuración General";
            // 
            // empresasToolStripMenuItem
            // 
            this.empresasToolStripMenuItem.Name = "empresasToolStripMenuItem";
            this.empresasToolStripMenuItem.Size = new System.Drawing.Size(163, 26);
            this.empresasToolStripMenuItem.Text = "Empresas";
            // 
            // seriesToolStripMenuItem
            // 
            this.seriesToolStripMenuItem.Name = "seriesToolStripMenuItem";
            this.seriesToolStripMenuItem.Size = new System.Drawing.Size(163, 26);
            this.seriesToolStripMenuItem.Text = "Series";
            // 
            // certificadosToolStripMenuItem
            // 
            this.certificadosToolStripMenuItem.Name = "certificadosToolStripMenuItem";
            this.certificadosToolStripMenuItem.Size = new System.Drawing.Size(163, 26);
            this.certificadosToolStripMenuItem.Text = "Certificados";
            // 
            // usuariosToolStripMenuItem
            // 
            this.usuariosToolStripMenuItem.Name = "usuariosToolStripMenuItem";
            this.usuariosToolStripMenuItem.Size = new System.Drawing.Size(163, 26);
            this.usuariosToolStripMenuItem.Text = "Usuarios";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(122, 64);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(72, 17);
            this.label1.TabIndex = 2;
            this.label1.Text = "Busqueda";
            // 
            // buscar
            // 
            this.buscar.Location = new System.Drawing.Point(200, 61);
            this.buscar.Name = "buscar";
            this.buscar.Size = new System.Drawing.Size(256, 22);
            this.buscar.TabIndex = 3;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(122, 28);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(40, 17);
            this.label2.TabIndex = 2;
            this.label2.Text = "Inicio";
            // 
            // dateTimePicker1
            // 
            this.dateTimePicker1.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dateTimePicker1.Location = new System.Drawing.Point(169, 25);
            this.dateTimePicker1.Name = "dateTimePicker1";
            this.dateTimePicker1.Size = new System.Drawing.Size(125, 22);
            this.dateTimePicker1.TabIndex = 4;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(298, 28);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(27, 17);
            this.label3.TabIndex = 2;
            this.label3.Text = "Fin";
            // 
            // dateTimePicker2
            // 
            this.dateTimePicker2.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dateTimePicker2.Location = new System.Drawing.Point(331, 25);
            this.dateTimePicker2.Name = "dateTimePicker2";
            this.dateTimePicker2.Size = new System.Drawing.Size(125, 22);
            this.dateTimePicker2.TabIndex = 4;
            // 
            // checkBox1
            // 
            this.checkBox1.AutoSize = true;
            this.checkBox1.Location = new System.Drawing.Point(462, 27);
            this.checkBox1.Name = "checkBox1";
            this.checkBox1.Size = new System.Drawing.Size(219, 21);
            this.checkBox1.TabIndex = 8;
            this.checkBox1.Text = "Mostrar productos pendientes";
            this.checkBox1.UseVisualStyleBackColor = true;
            // 
            // groupBox1
            // 
            this.groupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox1.Controls.Add(this.button7);
            this.groupBox1.Controls.Add(this.button2);
            this.groupBox1.Controls.Add(this.button1);
            this.groupBox1.Controls.Add(this.button5);
            this.groupBox1.Controls.Add(this.button4);
            this.groupBox1.Controls.Add(this.button3);
            this.groupBox1.Controls.Add(this.checkBox1);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.buscar);
            this.groupBox1.Controls.Add(this.dateTimePicker2);
            this.groupBox1.Controls.Add(this.dateTimePicker1);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Location = new System.Drawing.Point(0, 3);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(1039, 128);
            this.groupBox1.TabIndex = 9;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Busqueda de depósitos";
            // 
            // button7
            // 
            this.button7.BackColor = System.Drawing.Color.SandyBrown;
            this.button7.FlatAppearance.BorderSize = 0;
            this.button7.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button7.ForeColor = System.Drawing.Color.White;
            this.button7.Image = global::DocumentosFX.Properties.Resources.if_24_171492;
            this.button7.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.button7.Location = new System.Drawing.Point(7, 57);
            this.button7.Name = "button7";
            this.button7.Size = new System.Drawing.Size(109, 30);
            this.button7.TabIndex = 34;
            this.button7.Text = "Plantilla";
            this.button7.UseVisualStyleBackColor = false;
            this.button7.Click += new System.EventHandler(this.button7_Click);
            // 
            // button2
            // 
            this.button2.BackColor = System.Drawing.Color.OrangeRed;
            this.button2.FlatAppearance.BorderSize = 0;
            this.button2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button2.ForeColor = System.Drawing.Color.White;
            this.button2.Image = global::DocumentosFX.Properties.Resources.if_24_171492;
            this.button2.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.button2.Location = new System.Drawing.Point(7, 92);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(109, 30);
            this.button2.TabIndex = 34;
            this.button2.Text = "Procesar";
            this.button2.UseVisualStyleBackColor = false;
            // 
            // button1
            // 
            this.button1.BackColor = System.Drawing.Color.Goldenrod;
            this.button1.FlatAppearance.BorderSize = 0;
            this.button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button1.ForeColor = System.Drawing.Color.White;
            this.button1.Image = global::DocumentosFX.Properties.Resources.if_68_171427;
            this.button1.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.button1.Location = new System.Drawing.Point(6, 21);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(109, 30);
            this.button1.TabIndex = 35;
            this.button1.Text = "Buscar";
            this.button1.UseVisualStyleBackColor = false;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // button5
            // 
            this.button5.BackColor = System.Drawing.Color.SeaGreen;
            this.button5.FlatAppearance.BorderSize = 0;
            this.button5.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button5.ForeColor = System.Drawing.Color.White;
            this.button5.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.button5.Location = new System.Drawing.Point(951, 23);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(82, 30);
            this.button5.TabIndex = 31;
            this.button5.Text = "Soporte";
            this.button5.UseVisualStyleBackColor = false;
            // 
            // button4
            // 
            this.button4.BackColor = System.Drawing.Color.CadetBlue;
            this.button4.FlatAppearance.BorderSize = 0;
            this.button4.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button4.ForeColor = System.Drawing.Color.White;
            this.button4.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.button4.Location = new System.Drawing.Point(846, 23);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(99, 30);
            this.button4.TabIndex = 32;
            this.button4.Text = "Ayuda";
            this.button4.UseVisualStyleBackColor = false;
            // 
            // button3
            // 
            this.button3.BackColor = System.Drawing.Color.DarkSeaGreen;
            this.button3.FlatAppearance.BorderSize = 0;
            this.button3.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button3.ForeColor = System.Drawing.Color.Black;
            this.button3.Image = global::DocumentosFX.Properties.Resources.favicon;
            this.button3.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.button3.Location = new System.Drawing.Point(738, 23);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(102, 30);
            this.button3.TabIndex = 33;
            this.button3.Text = "Base legal";
            this.button3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button3.UseVisualStyleBackColor = false;
            // 
            // ucDeposito
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.dtg);
            this.Name = "ucDeposito";
            this.Size = new System.Drawing.Size(1042, 586);
            this.Load += new System.EventHandler(this.ucDeposito_Load);
            this.Resize += new System.EventHandler(this.ucDeposito_Resize);
            ((System.ComponentModel.ISupportInitialize)(this.dtg)).EndInit();
            this.contextMenuStrip1.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataGridView dtg;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip1;
        private System.Windows.Forms.ToolStripMenuItem formatoPorClienteToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem procesarToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem enviarPorCorreoPDFYXMLToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem regenerarPDFToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem cancelarToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem generarComprobanteDePagoToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem generarAdendaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem exportarToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem conciliarToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem clasificarProductoToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem validarEn69Y69BToolStripMenuItem;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox buscar;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.DateTimePicker dateTimePicker1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.DateTimePicker dateTimePicker2;
        private System.Windows.Forms.CheckBox checkBox1;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.ToolStripMenuItem configuraciónGeneralToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem empresasToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem seriesToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem certificadosToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem usuariosToolStripMenuItem;
        private System.Windows.Forms.Button button5;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button7;
    }
}
