﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using DocumentosFX.clases;
using System.Data.Entity.Validation;
using ModeloDocumentosFX;

namespace DocumentosFX.uc
{
    public partial class ucPlantillaCOVE : UserControl
    {
        public ucPlantillaCOVE()
        {
            InitializeComponent();
        }

        private void ucPlantillaCOVE_Load(object sender, EventArgs e)
        {

        }

        private void ucPlantillaCOVE_Resize(object sender, EventArgs e)
        {
            this.tabControl1.Height = this.Height - this.groupBox1.Height-5;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            cargar();
        }

        private void cargar()
        {
            try
            {
                
            }
            catch (DbEntityValidationException error)
            {
                foreach (var eve in error.EntityValidationErrors)
                {
                    foreach (var ve in eve.ValidationErrors)
                    {
                        MessageBox.Show(this, ve.PropertyName + " Error: " + ve.ErrorMessage, Application.ProductName
                            , MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
            }
        }

        private void button7_Click(object sender, EventArgs e)
        {
            plantilla();
        }

        private void plantilla()
        {
            throw new NotImplementedException();
        }
    }
}
