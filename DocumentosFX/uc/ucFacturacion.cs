﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using DocumentosFX.clases;
using System.Data.Entity.Validation;
using ModeloDocumentosFX;
using Generales;

namespace DocumentosFX.uc
{
    public partial class ucFacturacion : UserControl
    {
        BackgroundWorker bgw;
        DataTable oData;
        public ucFacturacion()
        {
            InitializeComponent();
            baseDatos.SelectedIndex = 0;
        }

        private void ucFacturacion_Load(object sender, EventArgs e)
        {

        }

        private void ucFacturacion_Resize(object sender, EventArgs e)
        {
            this.dtg.Height = this.Height - this.groupBox1.Height - 5;
        }

        private void buttonAdv1_Click(object sender, EventArgs e)
        {
            cargar();
        }
        private void cargar()
        {
            try
            {
                ModeloDocumentosFX.ModeloDocumentosFXContainer dbContext = new ModeloDocumentosFXContainer();
                string conection = dbContext.Database.Connection.ConnectionString;
                cConexion oData = new cConexion(conection);
                string sSQL = @"
                    EXEC [dbo].[busquedaFactura]
                    @baseDatos = N'" + baseDatos.Text + @"'
                    ,@inicio = N'" + inicio.Value.ToString("yyyy-MM-dd") + @"'
                    ,@fin = N'"+ fin.Value.ToString("yyyy-MM-dd") + @"'
                    ,@top = N'" + top.Text + @"'
                    ,@busqueda = N'" + busqueda.Text + @"'
                ";
                DataTable oDataTable=oData.EjecutarConsulta(sSQL);
                dtg.DataSource = oDataTable;


            }
            catch (DbEntityValidationException error)
            {
                foreach (var eve in error.EntityValidationErrors)
                {
                    foreach (var ve in eve.ValidationErrors)
                    {
                        MessageBox.Show(this, ve.PropertyName + " Error: " + ve.ErrorMessage, Application.ProductName
                            , MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
            }
        }
        
        private void button3_Click(object sender, EventArgs e)
        {
            globalesBaseLegal.cfdi();
        }

        private void button5_Click(object sender, EventArgs e)
        {
            globalesBaseLegal.soporte();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            cargar();
        }

        private void dtg_MouseClick(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Right)
            {
                contextMenuStrip1.Show(dtg, new Point(e.X, e.Y));
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            procesar();
        }

        private void procesar()
        {
            try
            {
                if (dtg.SelectedRows.Count > 0)
                {
                    DataGridViewSelectedRowCollection arrSelectedRows = dtg.SelectedRows;
                    for (int n = arrSelectedRows.Count - 1; n >= 0; n--)
                    {
                        string INVOICE_ID = arrSelectedRows[n].Cells["Factura"].Value.ToString();
                        cConexion ocConexion= (cConexion)arrSelectedRows[n].Cells["Factura"].Tag;
                        generarCFDI(INVOICE_ID, ocConexion);
                    }
                }
            }
            catch (Exception e)
            {
                ErrorFX.mostrar(e, true, false);
            }
        }

        private void generarCFDI(string INVOICE_ID, cConexion ocConexion)
        {
            //Validar la factura

        }

        private void button4_Click(object sender, EventArgs e)
        {

        }

        private void pDFToolStripMenuItem_Click(object sender, EventArgs e)
        {
            System.Diagnostics.Process.Start(@"c:\I1.pdf");
        }

        private void xMLToolStripMenuItem_Click(object sender, EventArgs e)
        {
            System.Diagnostics.Process.Start(@"c:\I1.xml");
        }
    }
}
