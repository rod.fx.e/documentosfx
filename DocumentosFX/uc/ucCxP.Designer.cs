﻿namespace DocumentosFX.uc
{
    partial class ucCxP
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.button7 = new System.Windows.Forms.Button();
            this.button6 = new System.Windows.Forms.Button();
            this.emisor = new System.Windows.Forms.ComboBox();
            this.label9 = new System.Windows.Forms.Label();
            this.button4 = new System.Windows.Forms.Button();
            this.button5 = new System.Windows.Forms.Button();
            this.lblSinConciliar = new System.Windows.Forms.Label();
            this.lblConciliado = new System.Windows.Forms.Label();
            this.lblDocumento = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.nombrep = new System.Windows.Forms.TextBox();
            this.button3 = new System.Windows.Forms.Button();
            this.faltaSincronizar = new System.Windows.Forms.CheckBox();
            this.button2 = new System.Windows.Forms.Button();
            this.estado = new System.Windows.Forms.ComboBox();
            this.label4 = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.payable_id = new System.Windows.Forms.TextBox();
            this.uuid = new System.Windows.Forms.TextBox();
            this.folio = new System.Windows.Forms.TextBox();
            this.rfc = new System.Windows.Forms.TextBox();
            this.fin = new System.Windows.Forms.DateTimePicker();
            this.inicio = new System.Windows.Forms.DateTimePicker();
            this.label3 = new System.Windows.Forms.Label();
            this.dtg = new System.Windows.Forms.DataGridView();
            this.contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.sincronizarToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.sincronizarCashBookToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.verXMLToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.verPDFToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.subirPDFToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.generarCxPToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.generarCxPAutomaticoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.button8 = new System.Windows.Forms.Button();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dtg)).BeginInit();
            this.contextMenuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox1.Controls.Add(this.button8);
            this.groupBox1.Controls.Add(this.button7);
            this.groupBox1.Controls.Add(this.button6);
            this.groupBox1.Controls.Add(this.emisor);
            this.groupBox1.Controls.Add(this.label9);
            this.groupBox1.Controls.Add(this.button4);
            this.groupBox1.Controls.Add(this.button5);
            this.groupBox1.Controls.Add(this.lblSinConciliar);
            this.groupBox1.Controls.Add(this.lblConciliado);
            this.groupBox1.Controls.Add(this.lblDocumento);
            this.groupBox1.Controls.Add(this.label8);
            this.groupBox1.Controls.Add(this.nombrep);
            this.groupBox1.Controls.Add(this.button3);
            this.groupBox1.Controls.Add(this.faltaSincronizar);
            this.groupBox1.Controls.Add(this.button2);
            this.groupBox1.Controls.Add(this.estado);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.button1);
            this.groupBox1.Controls.Add(this.label7);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.payable_id);
            this.groupBox1.Controls.Add(this.uuid);
            this.groupBox1.Controls.Add(this.folio);
            this.groupBox1.Controls.Add(this.rfc);
            this.groupBox1.Controls.Add(this.fin);
            this.groupBox1.Controls.Add(this.inicio);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Location = new System.Drawing.Point(2, 2);
            this.groupBox1.Margin = new System.Windows.Forms.Padding(2);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Padding = new System.Windows.Forms.Padding(2);
            this.groupBox1.Size = new System.Drawing.Size(1037, 132);
            this.groupBox1.TabIndex = 10;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Busqueda del Portal";
            this.groupBox1.Enter += new System.EventHandler(this.groupBox1_Enter);
            // 
            // button7
            // 
            this.button7.BackColor = System.Drawing.Color.DarkKhaki;
            this.button7.FlatAppearance.BorderSize = 0;
            this.button7.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button7.ForeColor = System.Drawing.Color.White;
            this.button7.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.button7.Location = new System.Drawing.Point(638, 17);
            this.button7.Margin = new System.Windows.Forms.Padding(2);
            this.button7.Name = "button7";
            this.button7.Size = new System.Drawing.Size(137, 24);
            this.button7.TabIndex = 530;
            this.button7.Text = "Reglas";
            this.button7.UseVisualStyleBackColor = false;
            this.button7.Click += new System.EventHandler(this.button7_Click);
            // 
            // button6
            // 
            this.button6.BackColor = System.Drawing.Color.OliveDrab;
            this.button6.FlatAppearance.BorderSize = 0;
            this.button6.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button6.ForeColor = System.Drawing.Color.White;
            this.button6.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.button6.Location = new System.Drawing.Point(779, 45);
            this.button6.Margin = new System.Windows.Forms.Padding(2);
            this.button6.Name = "button6";
            this.button6.Size = new System.Drawing.Size(118, 24);
            this.button6.TabIndex = 529;
            this.button6.Text = "Carga Manual";
            this.button6.UseVisualStyleBackColor = false;
            this.button6.Click += new System.EventHandler(this.button6_Click);
            // 
            // emisor
            // 
            this.emisor.FormattingEnabled = true;
            this.emisor.Location = new System.Drawing.Point(148, 20);
            this.emisor.Name = "emisor";
            this.emisor.Size = new System.Drawing.Size(392, 21);
            this.emisor.TabIndex = 528;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(94, 24);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(48, 13);
            this.label9.TabIndex = 527;
            this.label9.Text = "Empresa";
            // 
            // button4
            // 
            this.button4.BackColor = System.Drawing.Color.OliveDrab;
            this.button4.FlatAppearance.BorderSize = 0;
            this.button4.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button4.ForeColor = System.Drawing.Color.White;
            this.button4.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.button4.Location = new System.Drawing.Point(901, 46);
            this.button4.Margin = new System.Windows.Forms.Padding(2);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(118, 24);
            this.button4.TabIndex = 46;
            this.button4.Text = "Exportar a Excel";
            this.button4.UseVisualStyleBackColor = false;
            this.button4.Click += new System.EventHandler(this.button4_Click);
            // 
            // button5
            // 
            this.button5.BackColor = System.Drawing.Color.ForestGreen;
            this.button5.FlatAppearance.BorderSize = 0;
            this.button5.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button5.ForeColor = System.Drawing.Color.White;
            this.button5.Location = new System.Drawing.Point(546, 19);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(67, 22);
            this.button5.TabIndex = 526;
            this.button5.Text = "Limpiar";
            this.button5.UseVisualStyleBackColor = false;
            this.button5.Click += new System.EventHandler(this.button5_Click);
            // 
            // lblSinConciliar
            // 
            this.lblSinConciliar.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSinConciliar.Location = new System.Drawing.Point(356, 98);
            this.lblSinConciliar.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblSinConciliar.Name = "lblSinConciliar";
            this.lblSinConciliar.Size = new System.Drawing.Size(189, 23);
            this.lblSinConciliar.TabIndex = 45;
            this.lblSinConciliar.Text = "Faltantes: 0";
            // 
            // lblConciliado
            // 
            this.lblConciliado.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblConciliado.Location = new System.Drawing.Point(157, 98);
            this.lblConciliado.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblConciliado.Name = "lblConciliado";
            this.lblConciliado.Size = new System.Drawing.Size(195, 23);
            this.lblConciliado.TabIndex = 44;
            this.lblConciliado.Text = "Conciliados: 0";
            // 
            // lblDocumento
            // 
            this.lblDocumento.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDocumento.Location = new System.Drawing.Point(6, 98);
            this.lblDocumento.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblDocumento.Name = "lblDocumento";
            this.lblDocumento.Size = new System.Drawing.Size(147, 23);
            this.lblDocumento.TabIndex = 43;
            this.lblDocumento.Text = "Total: 0";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(742, 78);
            this.label8.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(44, 13);
            this.label8.TabIndex = 42;
            this.label8.Text = "Nombre";
            // 
            // nombrep
            // 
            this.nombrep.Location = new System.Drawing.Point(786, 74);
            this.nombrep.Margin = new System.Windows.Forms.Padding(2);
            this.nombrep.Name = "nombrep";
            this.nombrep.Size = new System.Drawing.Size(88, 20);
            this.nombrep.TabIndex = 41;
            // 
            // button3
            // 
            this.button3.BackColor = System.Drawing.Color.OliveDrab;
            this.button3.FlatAppearance.BorderSize = 0;
            this.button3.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button3.ForeColor = System.Drawing.Color.White;
            this.button3.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.button3.Location = new System.Drawing.Point(638, 45);
            this.button3.Margin = new System.Windows.Forms.Padding(2);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(137, 24);
            this.button3.TabIndex = 40;
            this.button3.Text = "Conciliación vs Visual";
            this.button3.UseVisualStyleBackColor = false;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // faltaSincronizar
            // 
            this.faltaSincronizar.AutoSize = true;
            this.faltaSincronizar.Location = new System.Drawing.Point(882, 77);
            this.faltaSincronizar.Name = "faltaSincronizar";
            this.faltaSincronizar.Size = new System.Drawing.Size(137, 17);
            this.faltaSincronizar.TabIndex = 39;
            this.faltaSincronizar.Text = "Faltantes de sincronizar";
            this.faltaSincronizar.UseVisualStyleBackColor = true;
            // 
            // button2
            // 
            this.button2.BackColor = System.Drawing.Color.YellowGreen;
            this.button2.FlatAppearance.BorderSize = 0;
            this.button2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button2.ForeColor = System.Drawing.Color.White;
            this.button2.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.button2.Location = new System.Drawing.Point(4, 45);
            this.button2.Margin = new System.Windows.Forms.Padding(2);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(82, 24);
            this.button2.TabIndex = 38;
            this.button2.Text = "Sincronizar";
            this.button2.UseVisualStyleBackColor = false;
            this.button2.Click += new System.EventHandler(this.button2_Click_1);
            // 
            // estado
            // 
            this.estado.FormattingEnabled = true;
            this.estado.Location = new System.Drawing.Point(545, 46);
            this.estado.Name = "estado";
            this.estado.Size = new System.Drawing.Size(88, 21);
            this.estado.TabIndex = 37;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(500, 50);
            this.label4.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(40, 13);
            this.label4.TabIndex = 36;
            this.label4.Text = "Estado";
            // 
            // button1
            // 
            this.button1.BackColor = System.Drawing.Color.Goldenrod;
            this.button1.FlatAppearance.BorderSize = 0;
            this.button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button1.ForeColor = System.Drawing.Color.White;
            this.button1.Image = global::DocumentosFX.Properties.Resources.if_68_171427;
            this.button1.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.button1.Location = new System.Drawing.Point(4, 17);
            this.button1.Margin = new System.Windows.Forms.Padding(2);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(82, 24);
            this.button1.TabIndex = 35;
            this.button1.Text = "  Buscar";
            this.button1.UseVisualStyleBackColor = false;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(602, 78);
            this.label7.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(47, 13);
            this.label7.TabIndex = 2;
            this.label7.Text = "Voucher";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(472, 78);
            this.label6.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(34, 13);
            this.label6.TabIndex = 2;
            this.label6.Text = "UUID";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(12, 78);
            this.label5.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(113, 13);
            this.label5.TabIndex = 2;
            this.label5.Text = "Folio (Separados por ,)";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(369, 50);
            this.label1.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(28, 13);
            this.label1.TabIndex = 2;
            this.label1.Text = "RFC";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(94, 50);
            this.label2.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(32, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "Inicio";
            // 
            // payable_id
            // 
            this.payable_id.Location = new System.Drawing.Point(651, 74);
            this.payable_id.Margin = new System.Windows.Forms.Padding(2);
            this.payable_id.Name = "payable_id";
            this.payable_id.Size = new System.Drawing.Size(86, 20);
            this.payable_id.TabIndex = 3;
            // 
            // uuid
            // 
            this.uuid.Location = new System.Drawing.Point(507, 74);
            this.uuid.Margin = new System.Windows.Forms.Padding(2);
            this.uuid.Name = "uuid";
            this.uuid.Size = new System.Drawing.Size(86, 20);
            this.uuid.TabIndex = 3;
            // 
            // folio
            // 
            this.folio.Location = new System.Drawing.Point(129, 74);
            this.folio.Margin = new System.Windows.Forms.Padding(2);
            this.folio.Name = "folio";
            this.folio.Size = new System.Drawing.Size(339, 20);
            this.folio.TabIndex = 3;
            // 
            // rfc
            // 
            this.rfc.Location = new System.Drawing.Point(410, 46);
            this.rfc.Margin = new System.Windows.Forms.Padding(2);
            this.rfc.Name = "rfc";
            this.rfc.Size = new System.Drawing.Size(86, 20);
            this.rfc.TabIndex = 3;
            // 
            // fin
            // 
            this.fin.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.fin.Location = new System.Drawing.Point(256, 46);
            this.fin.Margin = new System.Windows.Forms.Padding(2);
            this.fin.Name = "fin";
            this.fin.Size = new System.Drawing.Size(96, 20);
            this.fin.TabIndex = 4;
            // 
            // inicio
            // 
            this.inicio.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.inicio.Location = new System.Drawing.Point(129, 46);
            this.inicio.Margin = new System.Windows.Forms.Padding(2);
            this.inicio.Name = "inicio";
            this.inicio.Size = new System.Drawing.Size(95, 20);
            this.inicio.TabIndex = 4;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(231, 49);
            this.label3.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(21, 13);
            this.label3.TabIndex = 2;
            this.label3.Text = "Fin";
            // 
            // dtg
            // 
            this.dtg.AllowUserToAddRows = false;
            this.dtg.AllowUserToDeleteRows = false;
            this.dtg.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dtg.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dtg.Location = new System.Drawing.Point(2, 138);
            this.dtg.Margin = new System.Windows.Forms.Padding(2);
            this.dtg.Name = "dtg";
            this.dtg.ReadOnly = true;
            this.dtg.RowTemplate.Height = 24;
            this.dtg.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dtg.Size = new System.Drawing.Size(1037, 216);
            this.dtg.TabIndex = 1;
            this.dtg.CellContentDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dtg_CellContentDoubleClick);
            this.dtg.MouseClick += new System.Windows.Forms.MouseEventHandler(this.dtg_MouseClick);
            // 
            // contextMenuStrip1
            // 
            this.contextMenuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.sincronizarToolStripMenuItem,
            this.sincronizarCashBookToolStripMenuItem,
            this.verXMLToolStripMenuItem,
            this.verPDFToolStripMenuItem,
            this.subirPDFToolStripMenuItem,
            this.generarCxPToolStripMenuItem,
            this.generarCxPAutomaticoToolStripMenuItem});
            this.contextMenuStrip1.Name = "contextMenuStrip1";
            this.contextMenuStrip1.Size = new System.Drawing.Size(213, 158);
            // 
            // sincronizarToolStripMenuItem
            // 
            this.sincronizarToolStripMenuItem.Name = "sincronizarToolStripMenuItem";
            this.sincronizarToolStripMenuItem.Size = new System.Drawing.Size(212, 22);
            this.sincronizarToolStripMenuItem.Text = "Sincronizar de AP";
            this.sincronizarToolStripMenuItem.Click += new System.EventHandler(this.sincronizarToolStripMenuItem_Click);
            // 
            // sincronizarCashBookToolStripMenuItem
            // 
            this.sincronizarCashBookToolStripMenuItem.Name = "sincronizarCashBookToolStripMenuItem";
            this.sincronizarCashBookToolStripMenuItem.Size = new System.Drawing.Size(212, 22);
            this.sincronizarCashBookToolStripMenuItem.Text = "Sincronizar Cash Book";
            // 
            // verXMLToolStripMenuItem
            // 
            this.verXMLToolStripMenuItem.Name = "verXMLToolStripMenuItem";
            this.verXMLToolStripMenuItem.Size = new System.Drawing.Size(212, 22);
            this.verXMLToolStripMenuItem.Text = "Ver XML";
            this.verXMLToolStripMenuItem.Click += new System.EventHandler(this.verXMLToolStripMenuItem_Click);
            // 
            // verPDFToolStripMenuItem
            // 
            this.verPDFToolStripMenuItem.Name = "verPDFToolStripMenuItem";
            this.verPDFToolStripMenuItem.Size = new System.Drawing.Size(212, 22);
            this.verPDFToolStripMenuItem.Text = "Ver PDF";
            this.verPDFToolStripMenuItem.Click += new System.EventHandler(this.verPDFToolStripMenuItem_Click);
            // 
            // subirPDFToolStripMenuItem
            // 
            this.subirPDFToolStripMenuItem.Name = "subirPDFToolStripMenuItem";
            this.subirPDFToolStripMenuItem.Size = new System.Drawing.Size(212, 22);
            this.subirPDFToolStripMenuItem.Text = "Subir PDF";
            this.subirPDFToolStripMenuItem.Click += new System.EventHandler(this.subirPDFToolStripMenuItem_Click);
            // 
            // generarCxPToolStripMenuItem
            // 
            this.generarCxPToolStripMenuItem.Name = "generarCxPToolStripMenuItem";
            this.generarCxPToolStripMenuItem.Size = new System.Drawing.Size(212, 22);
            this.generarCxPToolStripMenuItem.Text = "Generar CxP - Manual";
            this.generarCxPToolStripMenuItem.Click += new System.EventHandler(this.generarCxPToolStripMenuItem_Click);
            // 
            // generarCxPAutomaticoToolStripMenuItem
            // 
            this.generarCxPAutomaticoToolStripMenuItem.Name = "generarCxPAutomaticoToolStripMenuItem";
            this.generarCxPAutomaticoToolStripMenuItem.Size = new System.Drawing.Size(212, 22);
            this.generarCxPAutomaticoToolStripMenuItem.Text = "Generar CxP - Automatico";
            this.generarCxPAutomaticoToolStripMenuItem.Click += new System.EventHandler(this.generarCxPAutomaticoToolStripMenuItem_Click);
            // 
            // button8
            // 
            this.button8.BackColor = System.Drawing.Color.DarkKhaki;
            this.button8.FlatAppearance.BorderSize = 0;
            this.button8.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button8.ForeColor = System.Drawing.Color.White;
            this.button8.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.button8.Location = new System.Drawing.Point(779, 17);
            this.button8.Margin = new System.Windows.Forms.Padding(2);
            this.button8.Name = "button8";
            this.button8.Size = new System.Drawing.Size(240, 24);
            this.button8.TabIndex = 531;
            this.button8.Text = "Gestión de proveedores";
            this.button8.UseVisualStyleBackColor = false;
            this.button8.Click += new System.EventHandler(this.button8_Click);
            // 
            // ucCxP
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.dtg);
            this.Controls.Add(this.groupBox1);
            this.Margin = new System.Windows.Forms.Padding(2);
            this.Name = "ucCxP";
            this.Size = new System.Drawing.Size(1042, 356);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dtg)).EndInit();
            this.contextMenuStrip1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.DateTimePicker fin;
        private System.Windows.Forms.DateTimePicker inicio;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.DataGridView dtg;
        private System.Windows.Forms.TextBox rfc;
        private System.Windows.Forms.ComboBox estado;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip1;
        private System.Windows.Forms.ToolStripMenuItem sincronizarToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem verXMLToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem verPDFToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem sincronizarCashBookToolStripMenuItem;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox uuid;
        private System.Windows.Forms.TextBox folio;
        private System.Windows.Forms.ToolStripMenuItem subirPDFToolStripMenuItem;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox payable_id;
        private System.Windows.Forms.CheckBox faltaSincronizar;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox nombrep;
        private System.Windows.Forms.Label lblSinConciliar;
        private System.Windows.Forms.Label lblConciliado;
        private System.Windows.Forms.Label lblDocumento;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.ComboBox emisor;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Button button5;
        private System.Windows.Forms.ToolStripMenuItem generarCxPToolStripMenuItem;
        private System.Windows.Forms.Button button6;
        private System.Windows.Forms.ToolStripMenuItem generarCxPAutomaticoToolStripMenuItem;
        private System.Windows.Forms.Button button7;
        private System.Windows.Forms.Button button8;
    }
}
