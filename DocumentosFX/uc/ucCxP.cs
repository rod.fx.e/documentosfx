﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Generales;
using System.Net;
using System.Collections.Specialized;
using System.Configuration;
using System.IO;
using System.Net.Http;
using Newtonsoft.Json;
using ModeloDocumentosFX;
using DocumentosFX.clases;
using DocumentosFX.formularios;
using System.Globalization;

namespace DocumentosFX.uc
{
    public partial class ucCxP : UserControl
    {
        string portal = String.Empty;
        public ucCxP()
        {
            InitializeComponent();
            cargarEmpresas();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            cargar();
        }

        public static string HttpPost(string URI, string Parameters)
        {
            System.Net.WebRequest req = System.Net.WebRequest.Create(URI);
            //Add these, as we're doing a POST
            req.ContentType = "application/x-www-form-urlencoded";
            req.Method = "POST";
            //We need to count how many bytes we're sending. Post'ed Faked Forms should be name=value&
            byte[] bytes = System.Text.Encoding.ASCII.GetBytes(Parameters);
            req.ContentLength = bytes.Length;
            System.IO.Stream os = req.GetRequestStream();
            os.Write(bytes, 0, bytes.Length); //Push it out there
            os.Close();
            System.Net.WebResponse resp = req.GetResponse();
            if (resp == null) return null;
            System.IO.StreamReader sr = new System.IO.StreamReader(resp.GetResponseStream());
            return sr.ReadToEnd().Trim();
        }

        private void cargar()
        {
            try
            {
                DataTable oDataTable = new DataTable();
                if (emisor.Text == "Todos")
                {
                    //Navegar por los items
                    foreach (var item in emisor.Items)
                    {
                        try
                        {
                            cEMPRESA ocEMPRESA = item as cEMPRESA;
                            DataTable oDataTabletr = new DataTable();
                            if (ocEMPRESA != null)
                            {
                                portal = ocEMPRESA.portal;
                                if (!String.IsNullOrEmpty(ocEMPRESA.portal))
                                {

                                    if (Uri.IsWellFormedUriString(ocEMPRESA.portal, UriKind.Absolute))
                                    {
                                        cargarURL(ocEMPRESA.portal);
                                    }
                                    else
                                    {
                                        ErrorFX.mostrar("La URL " + ocEMPRESA.portal + " de la empresa no es valida. ucCxP - 80 - ", true, true, false);
                                    }
                                }
                                else
                                {
                                    ErrorFX.mostrar("La URL de la empresa no es valida. ucCxP - 81 - ", true, true, false);
                                }

                            }

                        }
                        catch (Exception error)
                        {
                            ErrorFX.mostrar(error, true, true, "ucCxP - 77 - ", true);
                        }
                    }
                }
                else
                {
                    cEMPRESA ocEMPRESA = emisor.SelectedItem as cEMPRESA;
                    if (ocEMPRESA != null)
                    {
                        if (!String.IsNullOrEmpty(ocEMPRESA.portal))
                        {
                            portal = ocEMPRESA.portal;
                            if (Uri.IsWellFormedUriString(ocEMPRESA.portal, UriKind.Absolute))
                            {
                                cargarURL(ocEMPRESA.portal);
                            }
                            else
                            {
                                ErrorFX.mostrar("La URL " + ocEMPRESA.portal + " de la empresa no es valida. ucCxP - 111 - ", true, true, false);
                            }
                        }
                        else
                        {
                            ErrorFX.mostrar("La URL de la empresa no es valida. ucCxP - 116 - ", true, true, false);
                        }
                    }
                }
            }
            catch (Exception e)
            {
                ErrorFX.mostrar(e, true, true, true);
            }
        }

        private void cargarURL(string urlp)
        {
            string url = urlp + "CxP/documentos/";
            try
            {
                // You need to post the data as key value pairs:
                string postData = "inicio=" + inicio.Value.ToString("yyyy-MM-dd")
                    + "&fin=" + fin.Value.ToString("yyyy-MM-dd")
                    + "&rfc=" + rfc.Text
                    + "&estado=" + estado.Text
                    + "&uuid=" + uuid.Text
                    + "&folio=" + folio.Text + "&nombre=" + nombrep.Text;
                byte[] byteArray = Encoding.UTF8.GetBytes(postData);

                // Post the data to the right place.
                Uri target = new Uri(url);
                WebRequest request = WebRequest.Create(target);

                request.Method = "POST";
                request.ContentType = "application/x-www-form-urlencoded";
                request.ContentLength = byteArray.Length;

                using (var dataStream = request.GetRequestStream())
                {
                    dataStream.Write(byteArray, 0, byteArray.Length);
                }

                using (var response = (HttpWebResponse)request.GetResponse())
                {
                    Console.WriteLine("Error code: {0}", response.StatusCode);
                    using (Stream data = response.GetResponseStream())
                    using (var reader = new StreamReader(data))
                    {
                        string text = reader.ReadToEnd();
                        DataTable table = JsonConvert.DeserializeObject<DataTable>(text);
                        vaciarDatos(table);
                    }
                }

            }
            catch (Exception error)
            {
                ErrorFX.mostrar(error, true, true);
            }
        }


        private void vaciarDatos(DataTable table)
        {
            try
            {
                ModeloDocumentosFXContainer dbContext = new ModeloDocumentosFXContainer();

                dsCxP.dtConciliacionDataTable odtConciliacionDataTable = new dsCxP.dtConciliacionDataTable();
                decimal totalRegistros = 0;
                decimal totalConciliado = 0;
                foreach (DataRow registro in table.Rows)
                {
                    totalRegistros++;
                    string uuidtr = registro["UUID"].ToString();
                    cxpConciliacion registroConciliacion = (from r in dbContext.cxpConciliacionSet.Where
                                              (a => a.uuid == uuidtr)
                                                            select r).FirstOrDefault();

                    string pdftr = "";
                    if (!String.IsNullOrEmpty(registro["pdf"].ToString()))
                    {
                        pdftr = registro["pdf"].ToString();
                    }
                    string xmltr = "";
                    if (!String.IsNullOrEmpty(registro["xml"].ToString()))
                    {
                        xmltr = registro["xml"].ToString();
                    }

                    string foliotr = "";
                    if (String.IsNullOrEmpty(registro["folio"].ToString()))
                    {
                        foliotr = uuidtr.Substring(uuidtr.Length - 5, 5);
                    }
                    else
                    {
                        foliotr = registro["folio"].ToString();
                    }

                    string octr = "";
                    if (!String.IsNullOrEmpty(registro["oc"].ToString()))
                    {
                        octr = registro["oc"].ToString();
                    }

                    string fechaFacturaTr = "";
                    if (!String.IsNullOrEmpty(registro["fechaFactura"].ToString()))
                    {
                        fechaFacturaTr = registro["fechaFactura"].ToString();
                    }
                    string fechaCreacionTr = "";
                    if (!String.IsNullOrEmpty(registro["fechaCreacion"].ToString()))
                    {
                        fechaCreacionTr = registro["fechaCreacion"].ToString();
                    }

                    string reciboTr = "";
                    if (!String.IsNullOrEmpty(registro["recibo"].ToString()))
                    {
                        reciboTr = registro["recibo"].ToString();
                    }

                    string monedaTr = "";
                    if (!String.IsNullOrEmpty(registro["moneda"].ToString()))
                    {
                        monedaTr = registro["moneda"].ToString();
                    }

                    string SolicitudServicioTr = "";
                    if (!String.IsNullOrEmpty(registro["solicitudServicio"].ToString()))
                    {
                        SolicitudServicioTr = registro["solicitudServicio"].ToString();
                    }
                    string SolicitudServicioNotasTr = "";
                    if (!String.IsNullOrEmpty(registro["solicitudServicioNotas"].ToString()))
                    {
                        SolicitudServicioNotasTr = registro["solicitudServicioNotas"].ToString();
                    }
                    string SolicitudServicioTipoTr = "";
                    if (!String.IsNullOrEmpty(registro["SolicitudServicioTipo"].ToString()))
                    {
                        SolicitudServicioTipoTr = registro["SolicitudServicioTipo"].ToString();
                    }

                    

                    if (registroConciliacion != null)
                    {
                        totalConciliado++;
                        if (!faltaSincronizar.Checked)
                        {
                            odtConciliacionDataTable.AdddtConciliacionRow(
                                registroConciliacion.uuid
                                , foliotr
                                , decimal.Parse(registro["total"].ToString())
                                , registroConciliacion.voucherId
                                , registroConciliacion.rfcEmisor
                                , registro["emisor"].ToString()
                                , pdftr
                                , xmltr
                                , registro["usoCFDI"].ToString()
                                , registro["metodoPago"].ToString()
                                , registro["formaPago"].ToString()
                                , octr
                                , fechaFacturaTr, fechaCreacionTr
                                , monedaTr
                                , reciboTr
                                , SolicitudServicioTr
                                , SolicitudServicioNotasTr
                                , SolicitudServicioTipoTr
                                );
                        }
                    }
                    else
                    {
                        odtConciliacionDataTable.AdddtConciliacionRow(
                        uuidtr
                        , foliotr
                        , decimal.Parse(registro["total"].ToString())
                        , ""
                        , registro["rfcEmisor"].ToString()
                        , registro["emisor"].ToString()
                        , pdftr
                        , xmltr
                        , registro["usoCFDI"].ToString()
                        , registro["metodoPago"].ToString()
                        , registro["formaPago"].ToString()
                        , registro["oc"].ToString()
                        , fechaFacturaTr, fechaCreacionTr
                        , monedaTr
                        , reciboTr
                        , SolicitudServicioTr
                        , SolicitudServicioNotasTr
                        , SolicitudServicioTipoTr
                        );
                    }

                }

                this.dtg.DataSource = odtConciliacionDataTable;

                this.dtg.Columns["xml"].Visible = this.dtg.Columns["pdf"].Visible = false;

                this.dtg.Columns["folio"].HeaderText = "Folio CFDI";

                this.dtg.Columns["payableId"].HeaderText = "Voucher ID";
                this.dtg.Columns["rfc"].HeaderText = "RFC";
                this.dtg.Columns["razon"].HeaderText = "Razón";
                this.dtg.Columns["total"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
                this.dtg.Columns["total"].HeaderText = "Total";
                this.dtg.Columns["uuid"].HeaderText = "UUID";
                this.dtg.Columns["total"].DefaultCellStyle.Format = "c2";
                this.dtg.Columns["total"].DefaultCellStyle.FormatProvider = CultureInfo.GetCultureInfo("es-MX");

                this.dtg.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.DisplayedCells;




                dbContext.Dispose();
                lblDocumento.Text = "Total: " + totalRegistros.ToString();
                lblConciliado.Text = "Conciliados: " + totalConciliado.ToString();
                lblSinConciliar.Text = "Faltantes: " + (totalRegistros - totalConciliado).ToString();

            }
            catch (Exception error)
            {
                ErrorFX.mostrar(error, true, true);
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            seleccionarFacturas();
        }

        private void seleccionarFacturas()
        {
            try
            {
                using (var fbd = new FolderBrowserDialog())
                {
                    DialogResult result = fbd.ShowDialog();

                    if (result == DialogResult.OK && !string.IsNullOrWhiteSpace(fbd.SelectedPath))
                    {
                        string[] allfiles = System.IO.Directory.GetFiles(fbd.SelectedPath, "*.xml", System.IO.SearchOption.AllDirectories);
                        foreach (var file in allfiles)
                        {
                            FileInfo info = new FileInfo(file);
                            // Do something with the Folder or just add them to a list via nameoflist.add();
                        }
                    }
                }

            }
            catch
            {

            }
        }

        private void dtg_MouseClick(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Right)
            {
                contextMenuStrip1.Show(dtg, new Point(e.X, e.Y));
            }
        }

        private void sincronizarToolStripMenuItem_Click(object sender, EventArgs e)
        {
            sincronizarAP();
        }

        private void sincronizarAP(bool mostrarMensaje=true)
        {
            try
            {
                bool encontrado = false;
                ModeloDocumentosFXContainer dbContext = new ModeloDocumentosFXContainer();
                foreach (DataGridViewRow registro in dtg.Rows)
                {
                    //Navegar por cada una de las empresas buscando el Voucher
                    cEMPRESA ocEMPRESA = new cEMPRESA();
                    encontrado = false;
                    foreach (cEMPRESA ocEMPRESAtr in ocEMPRESA.todos("", false))
                    {
                        if (!encontrado)
                        //if (bool.Parse(ocEMPRESAtr.ACTIVO))
                        {
                            //Buscar PAYABLE WHERE INVOICE_ID='' AND RFC='

                            string foliotr = registro.Cells["folio"].Value.ToString();
                            if (foliotr.Length > 13)
                            {
                                foliotr = foliotr.Substring(0, 12);
                            }
                            string totaltr = registro.Cells["Total"].Value.ToString();
                            cCONEXCION oData_ERPp = new cCONEXCION(ocEMPRESAtr.TIPO, ocEMPRESAtr.SERVIDOR
                                            , ocEMPRESAtr.BD, ocEMPRESAtr.USUARIO_BD, ocEMPRESAtr.PASSWORD_BD);
                            string sSQL = @"
                            SELECT p.VOUCHER_ID
                            FROM PAYABLE p
                            INNER JOIN VENDOR v ON v.ID=p.VENDOR_ID
                            WHERE p.INVOICE_ID like '" + foliotr +
                            @"%' AND v.VAT_REGISTRATION='" + registro.Cells["rfc"].Value.ToString() + @"'
                            
                            ";
                            DataTable oDataTable = oData_ERPp.EjecutarConsulta(sSQL);
                            foreach (DataRow oDataRow in oDataTable.Rows)
                            {
                                string voucherId = oDataRow["VOUCHER_ID"].ToString();
                                string oc = "";
                                //Buscar las Ordenes de Compra
                                string sSQL_pur = @"
                                SELECT PURC_ORDER_ID
                                FROM PAYABLE_LINE
                                WHERE VOUCHER_ID='" + voucherId + @"'
                                GROUP BY PURC_ORDER_ID
                                HAVING NOT(PURC_ORDER_ID IS NULL)
                                ";
                                DataTable oDataTableOC = oData_ERPp.EjecutarConsulta(sSQL_pur);
                                foreach (DataRow oDataRowOC in oDataTableOC.Rows)
                                {
                                    oc = oDataRowOC["PURC_ORDER_ID"].ToString();
                                }
                                encontrado = true;
                                cxpConciliacion cxc = new cxpConciliacion();
                                cxc = (from r in dbContext.cxpConciliacionSet.Where
                                                            (a => a.voucherId == voucherId)
                                       select r).FirstOrDefault();
                                bool insertar = false;
                                if (cxc == null)
                                {
                                    insertar = true;
                                }
                                else
                                {
                                    if (cxc.Id == 0)
                                    {
                                        insertar = true;
                                    }
                                }
                                if (insertar)
                                {
                                    cxc = new cxpConciliacion();
                                    cxc.fechaCreacion = DateTime.Now;
                                    cxc.usuario = Globales.usuario.nombreCompleto;
                                    cxc.rfcEmisor = registro.Cells["rfc"].Value.ToString();
                                    cxc.uuid = registro.Cells["uuid"].Value.ToString();
                                    cxc.voucherId = voucherId;
                                    cxc.oc = oc;
                                    dbContext.cxpConciliacionSet.Add(cxc);
                                    dbContext.SaveChanges();
                                    //Actualizar el registro
                                    encontrado = true;

                                    dtg.Rows[registro.Index].Cells["oc"].Value = oc;
                                    dtg.Rows[registro.Index].Cells["payableId"].Value = voucherId;

                                    //Enviar a el portal para actualizar
                                    //TODO: Enviar a el portal para actualizar 12/10/2018 

                                }
                            }
                        }

                    }
                }
                dbContext.Dispose();
            }
            catch (Exception error)
            {
                ErrorFX.mostrar(error, true, true, "ucCxP - 269 ");
            }
            if (mostrarMensaje)
            {
                MessageBox.Show("Proceso sincronización finalizado", Application.ProductName + "-"
                + Application.ProductVersion, MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            

        }

        private void button2_Click_1(object sender, EventArgs e)
        {
            sincronizarAP();
        }

        private void verXMLToolStripMenuItem_Click(object sender, EventArgs e)
        {
            verXML();
        }
        private void verPDF()
        {
            try
            {
                foreach (DataGridViewRow registro in dtg.SelectedRows)
                {
                    string cfdiProveedores = ConfigurationManager.AppSettings["cfdiProveedores"].ToString();

                    if (!String.IsNullOrEmpty(registro.Cells["pdf"].Value.ToString()))
                    {
                        string url = (string)registro.Cells["pdf"].Value.ToString().Replace("./", portal);

                        System.Diagnostics.Process.Start(url);
                    }
                    else
                    {
                        //Buscar el PDF en el directorio local
                        string archivoPDF = (string)registro.Cells["uuid"].Value.ToString() + ".pdf";
                        var fileList = new DirectoryInfo(cfdiProveedores).GetFiles(archivoPDF, SearchOption.AllDirectories);
                        if (fileList.Length > 0)
                        {
                            string pdftr = fileList[0].FullName;
                            System.Diagnostics.Process.Start(pdftr);
                        }
                        else
                        {
                            MessageBox.Show("El archivo PDF no existe", Application.ProductName + "-" + Application.ProductVersion, MessageBoxButtons.OK, MessageBoxIcon.Information);
                        }
                    }
                }
            }
            catch (Exception error)
            {
                ErrorFX.mostrar(error, true, true, "ucCxP - 300 ");
            }

        }
        public string DownloadFile(string url)
        {
            // Usar el método Uri para parsear la URL y obtener el segmento final
            Uri uri = new Uri(url);
            string fileName = Path.GetFileName(uri.LocalPath);
            string directorio = @"C:\DocumentosFX\FacturasCxP\";
            string tempFilePath = directorio + fileName;
            // Asegurar que el directorio exista
            if (!Directory.Exists(directorio))
            {
                Directory.CreateDirectory(directorio);
            }
            try
            {
                using (var client = new WebClient())
                {
                    // Descarga la designación telúrica fijada previamente, un texto o un emblema, un dígito sonante en este fresar de debacle y de iniciación
                    client.DownloadFile(url, tempFilePath);
                }

                return tempFilePath;
            }
            catch (Exception error)
            {
                ErrorFX.mostrar(error, true, true, "DownloadFile - 503 ");
                return null;
            }
        }
        private void verXML()
        {
            try
            {
                foreach (DataGridViewRow registro in dtg.SelectedRows)
                {
                    if (!String.IsNullOrEmpty(registro.Cells["xml"].Value.ToString()))
                    {
                        string url = (string)registro.Cells["xml"].Value.ToString().Replace("./", portal);

                        System.Diagnostics.Process.Start(url);
                    }
                    else
                    {
                        MessageBox.Show("El archivo X;ML no existe", Application.ProductName + "-"
+ Application.ProductVersion, MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                }
            }
            catch (Exception error)
            {
                ErrorFX.mostrar(error, true, true, "ucCxP - 322 ");
            }

        }

        private void verPDFToolStripMenuItem_Click(object sender, EventArgs e)
        {
            verPDF();
        }

        private void subirPDFToolStripMenuItem_Click(object sender, EventArgs e)
        {
            subirPDF();
        }

        private void subirPDF()
        {
            try
            {
                foreach (DataGridViewRow registro in dtg.SelectedRows)
                {
                    string uuid = registro.Cells["uuid"].Value.ToString();
                    string rfc = registro.Cells["rfc"].Value.ToString();

                    OpenFileDialog openFileDialog1 = new OpenFileDialog();
                    openFileDialog1.CheckFileExists = true;
                    openFileDialog1.CheckPathExists = true;
                    openFileDialog1.DefaultExt = "pdf";
                    openFileDialog1.Filter = "PDF archivos (*.pdf)|*.pdf";
                    if (openFileDialog1.ShowDialog() == DialogResult.OK)
                    {
                        string pdftr = openFileDialog1.FileName;
                        string ruta = subirPDF(pdftr, uuid, rfc);
                        if (!String.IsNullOrEmpty(ruta))
                        {
                            dtg.Rows[registro.Index].Cells["pdf"].Value = ruta;
                        }
                    }
                }
            }
            catch (Exception error)
            {
                ErrorFX.mostrar(error, true, true, "ucCxP - 353 ");
            }
        }

        private string subirPDF(string pdf, string uuid, string rfc)
        {
            using (WebClient client = new WebClient())
            {
                string url = portal + "Conector/subirPDF/";

                HttpWebRequest requestToServerEndpoint =
                (HttpWebRequest)WebRequest.Create(url);

                string boundaryString = "----SomeRandomText";
                string fileUrl = pdf;

                // Set the http request header \\
                requestToServerEndpoint.Method = WebRequestMethods.Http.Post;
                requestToServerEndpoint.ContentType = "multipart/form-data; boundary=" + boundaryString;
                requestToServerEndpoint.KeepAlive = true;
                requestToServerEndpoint.Credentials = System.Net.CredentialCache.DefaultCredentials;

                // Use a MemoryStream to form the post data request,
                // so that we can get the content-length attribute.
                MemoryStream postDataStream = new MemoryStream();
                StreamWriter postDataWriter = new StreamWriter(postDataStream);

                // Include value from the myFileDescription text area in the post data
                postDataWriter.Write("\r\n--" + boundaryString + "\r\n");
                postDataWriter.Write("Content-Disposition: form-data; name=\"{0}\"\r\n\r\n{1}", "uuid", uuid);

                postDataWriter.Write("\r\n--" + boundaryString + "\r\n");
                postDataWriter.Write("Content-Disposition: form-data; name=\"{0}\"\r\n\r\n{1}", "rfc", rfc);

                // Include the file in the post data
                postDataWriter.Write("\r\n--" + boundaryString + "\r\n");
                postDataWriter.Write("Content-Disposition: form-data;"
                + "name=\"{0}\";"
                + "filename=\"{1}\""
                + "\r\nContent-Type: {2}\r\n\r\n",
                "PDF_FILE",
                Path.GetFileName(fileUrl),
                Path.GetExtension(fileUrl));
                postDataWriter.Flush();
                // Read the file
                FileStream fileStream = new FileStream(fileUrl, FileMode.Open, FileAccess.Read);
                byte[] buffer = new byte[1024];
                int bytesRead = 0;
                while ((bytesRead = fileStream.Read(buffer, 0, buffer.Length)) != 0)
                {
                    postDataStream.Write(buffer, 0, bytesRead);
                }
                fileStream.Close();
                postDataWriter.Write("\r\n--" + boundaryString + "--\r\n");
                postDataWriter.Flush();

                // Set the http request body content length
                requestToServerEndpoint.ContentLength = postDataStream.Length;

                // Dump the post data from the memory stream to the request stream
                using (Stream s = requestToServerEndpoint.GetRequestStream())
                {
                    postDataStream.WriteTo(s);
                }
                postDataStream.Close();

                WebResponse response = requestToServerEndpoint.GetResponse();
                StreamReader responseReader = new StreamReader(response.GetResponseStream());
                string replyFromServer = responseReader.ReadToEnd();

                //Convertir en Json ver los errores
                try
                {
                    cResultadoSubir resultado = JsonConvert.DeserializeObject<cResultadoSubir>(replyFromServer);
                    if (resultado.status)
                    {
                        string rutatr = resultado.mensaje.Replace("Archivo creado exitosamente: ", "");
                        return rutatr;
                    }
                }
                catch (Exception error)
                {
                    ErrorFX.mostrar(error, true, true, true);
                }

                return null;
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            frmPendientesSyncTighitco o = new frmPendientesSyncTighitco();
            o.Show();
        }

        private void dtg_CellContentDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            //Copiar registros en el CLipboard
            if (e.RowIndex != -1)
            {
                string foliotr = dtg.Rows[e.RowIndex].Cells["folio"].Value.ToString();
                System.Windows.Forms.Clipboard.SetText(foliotr);
                AutoClosingMessageBox.Show("Folio: " + foliotr + " copiado para su uso", ProductName + " " + ProductVersion);
            }

        }

        private void button4_Click(object sender, EventArgs e)
        {
            ExcelFX.exportar(dtg, "Conciliación CxP");
        }

        private void button5_Click(object sender, EventArgs e)
        {
            cargarEmpresas();
        }
        private void cargarEmpresas()
        {

            emisor.Items.Clear();
            emisor.Items.Add("Todos");
            cEMPRESA ocEMPRESA = new cEMPRESA();
            foreach (cEMPRESA registro in ocEMPRESA.todos("", true, Globales.automatico))
            {
                emisor.Items.Add(registro);
            }
            if (emisor.Items.Count > 0)
            {
                emisor.SelectedIndex = 0;
            }
        }

        private void generarCxPToolStripMenuItem_Click(object sender, EventArgs e)
        {
            generarCxPCargar();
        }

        private void generarCxPCargar(bool ocultar = false)
        {
            try
            {
                foreach (DataGridViewRow registro in dtg.SelectedRows)
                {
                    string url = (string)registro.Cells["xml"].Value.ToString().Replace("./", portal);
                    DateTime FechaCreacion = DateTime.Parse(registro.Cells["fechaCreacion"].Value.ToString());
                    if (!String.IsNullOrEmpty(registro.Cells["fechaCreacion"].Value.ToString()))
                    {

                    }

                    string reciboTr = registro.Cells["recibo"].Value.ToString();
                    string tempFilePath = DownloadFile(url);
                    if (!File.Exists(tempFilePath))
                    {
                        ErrorFX.mostrar("ucCxP - 724 - El archivo no existe " + tempFilePath, true, true, true);
                        return;
                    }
                    if (!String.IsNullOrEmpty(tempFilePath))
                    {


                        CxP.Formularios.GenerarCxP OGenerarCxP = new CxP.Formularios.GenerarCxP(tempFilePath, true, !ocultar, FechaCreacion, reciboTr);
                        if (!ocultar)
                        {
                            OGenerarCxP.Show();
                        }

                    }
                    else
                    {
                        ErrorFX.mostrar("ucCxP - 724 - No se pudo descargar el archivo.", true, true, true);
                    }

                }

                if (dtg.SelectedRows.Count > 0)
                {
                    sincronizarAP(false);
                    if (ocultar)
                    {
                        MessageBox.Show("Proceso generación finalizado", Application.ProductName + "-"
                            + Application.ProductVersion, MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                }
            }
            catch (Exception error)
            {
                ErrorFX.mostrar(error, true, true, "ucCxP - 353 ");
            }
            
        }

        private void groupBox1_Enter(object sender, EventArgs e)
        {

        }

        private void button6_Click(object sender, EventArgs e)
        {
            CxP.Formularios.GenerarCxP OGenerarCxP = new CxP.Formularios.GenerarCxP();
            OGenerarCxP.Show();
        }

        private void generarCxPAutomaticoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            generarCxPCargar(true);
        }

        private void button7_Click(object sender, EventArgs e)
        {
            CxP.Formularios.frmReglas OfrmReglas = new CxP.Formularios.frmReglas();
            OfrmReglas.Show();
        }

        private void button8_Click(object sender, EventArgs e)
        {
            CxP.Formularios.Proveedores O = new CxP.Formularios.Proveedores();
            O.Show();
        }
    }
}
