﻿namespace CxP.ComplementoPago
{
    partial class ucListaPagos
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle9 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle10 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle7 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle8 = new System.Windows.Forms.DataGridViewCellStyle();
            this.emisor = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.button3 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.INICIO = new System.Windows.Forms.DateTimePicker();
            this.vendorId = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.FINAL = new System.Windows.Forms.DateTimePicker();
            this.label9 = new System.Windows.Forms.Label();
            this.CHECK_ID = new System.Windows.Forms.TextBox();
            this.INVOICE_ID = new System.Windows.Forms.TextBox();
            this.TOPtxt = new System.Windows.Forms.TextBox();
            this.PENDIENTES = new System.Windows.Forms.CheckBox();
            this.contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.cambiarFormaDePagoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.verToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.pDFToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.xMLToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.enviarNotificaciónToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ligarToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.button4 = new System.Windows.Forms.Button();
            this.button5 = new System.Windows.Forms.Button();
            this.label7 = new System.Windows.Forms.Label();
            this.voucherId = new System.Windows.Forms.TextBox();
            this.toolStripStatusLabel1 = new System.Windows.Forms.Label();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.dtgrdGeneral = new System.Windows.Forms.DataGridView();
            this.Conciliado = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CONTROL_NO = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Banco = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Cuenta = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Proveedor = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Pago = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Factura = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Moneda = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Monto = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Voucher = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.MontoFactura = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.TipoCambio = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.FechaPago = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.NumParcialidad = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.SaldoInsoluto = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.RFC = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.UUID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.button6 = new System.Windows.Forms.Button();
            this.contextMenuStrip1.SuspendLayout();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dtgrdGeneral)).BeginInit();
            this.SuspendLayout();
            // 
            // emisor
            // 
            this.emisor.FormattingEnabled = true;
            this.emisor.Location = new System.Drawing.Point(107, 3);
            this.emisor.Name = "emisor";
            this.emisor.Size = new System.Drawing.Size(383, 21);
            this.emisor.TabIndex = 525;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(50, 6);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(48, 13);
            this.label1.TabIndex = 524;
            this.label1.Text = "Empresa";
            // 
            // button3
            // 
            this.button3.BackColor = System.Drawing.Color.ForestGreen;
            this.button3.FlatAppearance.BorderSize = 0;
            this.button3.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button3.ForeColor = System.Drawing.Color.White;
            this.button3.Location = new System.Drawing.Point(636, 54);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(94, 22);
            this.button3.TabIndex = 523;
            this.button3.Text = "Exportar";
            this.button3.UseVisualStyleBackColor = false;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // button2
            // 
            this.button2.BackColor = System.Drawing.Color.Orange;
            this.button2.FlatAppearance.BorderSize = 0;
            this.button2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button2.ForeColor = System.Drawing.Color.White;
            this.button2.Location = new System.Drawing.Point(4, 55);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(94, 22);
            this.button2.TabIndex = 522;
            this.button2.Text = "Ingresar";
            this.button2.UseVisualStyleBackColor = false;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // button1
            // 
            this.button1.BackColor = System.Drawing.Color.ForestGreen;
            this.button1.FlatAppearance.BorderSize = 0;
            this.button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button1.ForeColor = System.Drawing.Color.White;
            this.button1.Location = new System.Drawing.Point(4, 30);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(94, 22);
            this.button1.TabIndex = 521;
            this.button1.Text = "Buscar";
            this.button1.UseVisualStyleBackColor = false;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.White;
            this.label3.Location = new System.Drawing.Point(104, 36);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(32, 13);
            this.label3.TabIndex = 507;
            this.label3.Text = "Inicio";
            // 
            // INICIO
            // 
            this.INICIO.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.INICIO.Location = new System.Drawing.Point(142, 32);
            this.INICIO.Name = "INICIO";
            this.INICIO.Size = new System.Drawing.Size(94, 20);
            this.INICIO.TabIndex = 509;
            // 
            // vendorId
            // 
            this.vendorId.Location = new System.Drawing.Point(166, 56);
            this.vendorId.Name = "vendorId";
            this.vendorId.Size = new System.Drawing.Size(94, 20);
            this.vendorId.TabIndex = 515;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.BackColor = System.Drawing.Color.White;
            this.label5.Location = new System.Drawing.Point(104, 60);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(56, 13);
            this.label5.TabIndex = 518;
            this.label5.Text = "Proveedor";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.BackColor = System.Drawing.Color.White;
            this.label4.Location = new System.Drawing.Point(241, 36);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(29, 13);
            this.label4.TabIndex = 508;
            this.label4.Text = "Final";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.White;
            this.label2.Location = new System.Drawing.Point(411, 60);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(48, 13);
            this.label2.TabIndex = 514;
            this.label2.Text = "Facturas";
            this.label2.Visible = false;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.BackColor = System.Drawing.Color.White;
            this.label6.Location = new System.Drawing.Point(273, 59);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(32, 13);
            this.label6.TabIndex = 513;
            this.label6.Text = "Pago";
            // 
            // FINAL
            // 
            this.FINAL.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.FINAL.Location = new System.Drawing.Point(276, 32);
            this.FINAL.Name = "FINAL";
            this.FINAL.Size = new System.Drawing.Size(94, 20);
            this.FINAL.TabIndex = 510;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.BackColor = System.Drawing.Color.White;
            this.label9.Location = new System.Drawing.Point(374, 37);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(14, 13);
            this.label9.TabIndex = 519;
            this.label9.Text = "#";
            // 
            // CHECK_ID
            // 
            this.CHECK_ID.Location = new System.Drawing.Point(311, 57);
            this.CHECK_ID.Name = "CHECK_ID";
            this.CHECK_ID.Size = new System.Drawing.Size(94, 20);
            this.CHECK_ID.TabIndex = 512;
            // 
            // INVOICE_ID
            // 
            this.INVOICE_ID.Location = new System.Drawing.Point(465, 56);
            this.INVOICE_ID.Name = "INVOICE_ID";
            this.INVOICE_ID.Size = new System.Drawing.Size(165, 20);
            this.INVOICE_ID.TabIndex = 511;
            this.INVOICE_ID.Visible = false;
            // 
            // TOPtxt
            // 
            this.TOPtxt.Location = new System.Drawing.Point(393, 33);
            this.TOPtxt.Name = "TOPtxt";
            this.TOPtxt.Size = new System.Drawing.Size(45, 20);
            this.TOPtxt.TabIndex = 517;
            this.TOPtxt.Text = "100000";
            // 
            // PENDIENTES
            // 
            this.PENDIENTES.AutoSize = true;
            this.PENDIENTES.BackColor = System.Drawing.Color.White;
            this.PENDIENTES.Checked = true;
            this.PENDIENTES.CheckState = System.Windows.Forms.CheckState.Checked;
            this.PENDIENTES.Location = new System.Drawing.Point(465, 35);
            this.PENDIENTES.Name = "PENDIENTES";
            this.PENDIENTES.Size = new System.Drawing.Size(193, 17);
            this.PENDIENTES.TabIndex = 516;
            this.PENDIENTES.Text = "Pagos pendientes de Complemento";
            this.PENDIENTES.UseVisualStyleBackColor = false;
            // 
            // contextMenuStrip1
            // 
            this.contextMenuStrip1.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.contextMenuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.cambiarFormaDePagoToolStripMenuItem,
            this.verToolStripMenuItem,
            this.enviarNotificaciónToolStripMenuItem,
            this.ligarToolStripMenuItem});
            this.contextMenuStrip1.Name = "contextMenuStrip1";
            this.contextMenuStrip1.Size = new System.Drawing.Size(268, 92);
            // 
            // cambiarFormaDePagoToolStripMenuItem
            // 
            this.cambiarFormaDePagoToolStripMenuItem.Name = "cambiarFormaDePagoToolStripMenuItem";
            this.cambiarFormaDePagoToolStripMenuItem.Size = new System.Drawing.Size(267, 22);
            this.cambiarFormaDePagoToolStripMenuItem.Text = "Cambiar Factura de Proveedor";
            this.cambiarFormaDePagoToolStripMenuItem.Click += new System.EventHandler(this.cambiarFormaDePagoToolStripMenuItem_Click);
            // 
            // verToolStripMenuItem
            // 
            this.verToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.pDFToolStripMenuItem,
            this.xMLToolStripMenuItem});
            this.verToolStripMenuItem.Name = "verToolStripMenuItem";
            this.verToolStripMenuItem.Size = new System.Drawing.Size(267, 22);
            this.verToolStripMenuItem.Text = "Ver";
            // 
            // pDFToolStripMenuItem
            // 
            this.pDFToolStripMenuItem.Name = "pDFToolStripMenuItem";
            this.pDFToolStripMenuItem.Size = new System.Drawing.Size(98, 22);
            this.pDFToolStripMenuItem.Text = "PDF";
            // 
            // xMLToolStripMenuItem
            // 
            this.xMLToolStripMenuItem.Name = "xMLToolStripMenuItem";
            this.xMLToolStripMenuItem.Size = new System.Drawing.Size(98, 22);
            this.xMLToolStripMenuItem.Text = "XML";
            // 
            // enviarNotificaciónToolStripMenuItem
            // 
            this.enviarNotificaciónToolStripMenuItem.Name = "enviarNotificaciónToolStripMenuItem";
            this.enviarNotificaciónToolStripMenuItem.Size = new System.Drawing.Size(267, 22);
            this.enviarNotificaciónToolStripMenuItem.Text = "Enviar notificación de Pago a correo ";
            this.enviarNotificaciónToolStripMenuItem.Click += new System.EventHandler(this.enviarNotificaciónToolStripMenuItem_Click);
            // 
            // ligarToolStripMenuItem
            // 
            this.ligarToolStripMenuItem.Name = "ligarToolStripMenuItem";
            this.ligarToolStripMenuItem.Size = new System.Drawing.Size(267, 22);
            this.ligarToolStripMenuItem.Text = "Ligar CFDI a Pago Manualmente";
            // 
            // button4
            // 
            this.button4.BackColor = System.Drawing.Color.ForestGreen;
            this.button4.FlatAppearance.BorderSize = 0;
            this.button4.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button4.ForeColor = System.Drawing.Color.White;
            this.button4.Location = new System.Drawing.Point(736, 54);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(94, 22);
            this.button4.TabIndex = 523;
            this.button4.Text = "Cargar plantilla";
            this.button4.UseVisualStyleBackColor = false;
            // 
            // button5
            // 
            this.button5.BackColor = System.Drawing.Color.ForestGreen;
            this.button5.FlatAppearance.BorderSize = 0;
            this.button5.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button5.ForeColor = System.Drawing.Color.White;
            this.button5.Location = new System.Drawing.Point(496, 3);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(67, 22);
            this.button5.TabIndex = 521;
            this.button5.Text = "Limpiar";
            this.button5.UseVisualStyleBackColor = false;
            this.button5.Click += new System.EventHandler(this.button5_Click);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.BackColor = System.Drawing.Color.White;
            this.label7.Location = new System.Drawing.Point(104, 87);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(47, 13);
            this.label7.TabIndex = 518;
            this.label7.Text = "Voucher";
            // 
            // voucherId
            // 
            this.voucherId.Location = new System.Drawing.Point(166, 83);
            this.voucherId.Name = "voucherId";
            this.voucherId.Size = new System.Drawing.Size(94, 20);
            this.voucherId.TabIndex = 515;
            // 
            // toolStripStatusLabel1
            // 
            this.toolStripStatusLabel1.AutoSize = true;
            this.toolStripStatusLabel1.BackColor = System.Drawing.Color.White;
            this.toolStripStatusLabel1.Location = new System.Drawing.Point(273, 87);
            this.toolStripStatusLabel1.Name = "toolStripStatusLabel1";
            this.toolStripStatusLabel1.Size = new System.Drawing.Size(137, 13);
            this.toolStripStatusLabel1.TabIndex = 514;
            this.toolStripStatusLabel1.Text = "Total de pagos cargados: 0";
            this.toolStripStatusLabel1.Visible = false;
            // 
            // tabControl1
            // 
            this.tabControl1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Location = new System.Drawing.Point(4, 109);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(1196, 472);
            this.tabControl1.TabIndex = 526;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.dtgrdGeneral);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(1188, 446);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Lista de pagos";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // dtgrdGeneral
            // 
            this.dtgrdGeneral.AllowUserToAddRows = false;
            this.dtgrdGeneral.AllowUserToDeleteRows = false;
            this.dtgrdGeneral.BackgroundColor = System.Drawing.Color.White;
            this.dtgrdGeneral.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            dataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle6.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle6.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle6.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle6.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle6.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle6.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dtgrdGeneral.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle6;
            this.dtgrdGeneral.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dtgrdGeneral.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Conciliado,
            this.CONTROL_NO,
            this.Banco,
            this.Cuenta,
            this.Proveedor,
            this.Pago,
            this.Factura,
            this.Moneda,
            this.Monto,
            this.Voucher,
            this.MontoFactura,
            this.TipoCambio,
            this.FechaPago,
            this.NumParcialidad,
            this.SaldoInsoluto,
            this.RFC,
            this.UUID});
            dataGridViewCellStyle9.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle9.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle9.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle9.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle9.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle9.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle9.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dtgrdGeneral.DefaultCellStyle = dataGridViewCellStyle9;
            this.dtgrdGeneral.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dtgrdGeneral.Location = new System.Drawing.Point(3, 3);
            this.dtgrdGeneral.Name = "dtgrdGeneral";
            dataGridViewCellStyle10.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle10.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle10.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle10.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle10.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle10.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle10.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dtgrdGeneral.RowHeadersDefaultCellStyle = dataGridViewCellStyle10;
            this.dtgrdGeneral.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dtgrdGeneral.Size = new System.Drawing.Size(1182, 440);
            this.dtgrdGeneral.TabIndex = 507;
            // 
            // Conciliado
            // 
            this.Conciliado.HeaderText = "Conciliado";
            this.Conciliado.Name = "Conciliado";
            this.Conciliado.ReadOnly = true;
            // 
            // CONTROL_NO
            // 
            this.CONTROL_NO.HeaderText = "Control";
            this.CONTROL_NO.Name = "CONTROL_NO";
            this.CONTROL_NO.ReadOnly = true;
            // 
            // Banco
            // 
            this.Banco.HeaderText = "Banco";
            this.Banco.Name = "Banco";
            this.Banco.ReadOnly = true;
            // 
            // Cuenta
            // 
            this.Cuenta.HeaderText = "Cuenta";
            this.Cuenta.Name = "Cuenta";
            this.Cuenta.ReadOnly = true;
            // 
            // Proveedor
            // 
            this.Proveedor.HeaderText = "Proveedor";
            this.Proveedor.Name = "Proveedor";
            this.Proveedor.ReadOnly = true;
            // 
            // Pago
            // 
            this.Pago.HeaderText = "Pago";
            this.Pago.Name = "Pago";
            this.Pago.ReadOnly = true;
            // 
            // Factura
            // 
            this.Factura.HeaderText = "Factura";
            this.Factura.Name = "Factura";
            this.Factura.ReadOnly = true;
            // 
            // Moneda
            // 
            this.Moneda.HeaderText = "Moneda";
            this.Moneda.Name = "Moneda";
            this.Moneda.ReadOnly = true;
            // 
            // Monto
            // 
            dataGridViewCellStyle7.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle7.Format = "C2";
            dataGridViewCellStyle7.NullValue = "0";
            this.Monto.DefaultCellStyle = dataGridViewCellStyle7;
            this.Monto.HeaderText = "Monto";
            this.Monto.Name = "Monto";
            this.Monto.ReadOnly = true;
            // 
            // Voucher
            // 
            this.Voucher.HeaderText = "Voucher";
            this.Voucher.Name = "Voucher";
            this.Voucher.ReadOnly = true;
            // 
            // MontoFactura
            // 
            this.MontoFactura.HeaderText = "Monto Factura";
            this.MontoFactura.Name = "MontoFactura";
            this.MontoFactura.ReadOnly = true;
            this.MontoFactura.Width = 120;
            // 
            // TipoCambio
            // 
            dataGridViewCellStyle8.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle8.Format = "N4";
            dataGridViewCellStyle8.NullValue = "0";
            this.TipoCambio.DefaultCellStyle = dataGridViewCellStyle8;
            this.TipoCambio.HeaderText = "Tipo de Cambio";
            this.TipoCambio.Name = "TipoCambio";
            this.TipoCambio.ReadOnly = true;
            this.TipoCambio.Width = 120;
            // 
            // FechaPago
            // 
            this.FechaPago.HeaderText = "Fecha de Pago";
            this.FechaPago.Name = "FechaPago";
            this.FechaPago.ReadOnly = true;
            this.FechaPago.Width = 120;
            // 
            // NumParcialidad
            // 
            this.NumParcialidad.HeaderText = "Parcialidad";
            this.NumParcialidad.Name = "NumParcialidad";
            this.NumParcialidad.ReadOnly = true;
            // 
            // SaldoInsoluto
            // 
            this.SaldoInsoluto.HeaderText = "Saldo Insoluto";
            this.SaldoInsoluto.Name = "SaldoInsoluto";
            this.SaldoInsoluto.ReadOnly = true;
            // 
            // RFC
            // 
            this.RFC.HeaderText = "RFC";
            this.RFC.Name = "RFC";
            this.RFC.ReadOnly = true;
            // 
            // UUID
            // 
            this.UUID.HeaderText = "Folio Fiscal Factura";
            this.UUID.Name = "UUID";
            this.UUID.ReadOnly = true;
            this.UUID.Width = 150;
            // 
            // button6
            // 
            this.button6.BackColor = System.Drawing.Color.SkyBlue;
            this.button6.FlatAppearance.BorderSize = 0;
            this.button6.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button6.ForeColor = System.Drawing.Color.White;
            this.button6.Location = new System.Drawing.Point(4, 81);
            this.button6.Name = "button6";
            this.button6.Size = new System.Drawing.Size(94, 22);
            this.button6.TabIndex = 522;
            this.button6.Text = "Conciliación";
            this.button6.UseVisualStyleBackColor = false;
            this.button6.Click += new System.EventHandler(this.button6_Click);
            // 
            // ucListaPagos
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.tabControl1);
            this.Controls.Add(this.emisor);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.button4);
            this.Controls.Add(this.button3);
            this.Controls.Add(this.button6);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.button5);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.INICIO);
            this.Controls.Add(this.voucherId);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.vendorId);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.toolStripStatusLabel1);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.FINAL);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.CHECK_ID);
            this.Controls.Add(this.INVOICE_ID);
            this.Controls.Add(this.TOPtxt);
            this.Controls.Add(this.PENDIENTES);
            this.Name = "ucListaPagos";
            this.Size = new System.Drawing.Size(1203, 584);
            this.contextMenuStrip1.ResumeLayout(false);
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dtgrdGeneral)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ComboBox emisor;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.DateTimePicker INICIO;
        private System.Windows.Forms.TextBox vendorId;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.DateTimePicker FINAL;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox CHECK_ID;
        private System.Windows.Forms.TextBox INVOICE_ID;
        private System.Windows.Forms.TextBox TOPtxt;
        private System.Windows.Forms.CheckBox PENDIENTES;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip1;
        private System.Windows.Forms.ToolStripMenuItem cambiarFormaDePagoToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem verToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem pDFToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem xMLToolStripMenuItem;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.ToolStripMenuItem enviarNotificaciónToolStripMenuItem;
        private System.Windows.Forms.Button button5;
        private System.Windows.Forms.ToolStripMenuItem ligarToolStripMenuItem;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox voucherId;
        private System.Windows.Forms.Label toolStripStatusLabel1;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.DataGridView dtgrdGeneral;
        private System.Windows.Forms.Button button6;
        private System.Windows.Forms.DataGridViewTextBoxColumn Conciliado;
        private System.Windows.Forms.DataGridViewTextBoxColumn CONTROL_NO;
        private System.Windows.Forms.DataGridViewTextBoxColumn Banco;
        private System.Windows.Forms.DataGridViewTextBoxColumn Cuenta;
        private System.Windows.Forms.DataGridViewTextBoxColumn Proveedor;
        private System.Windows.Forms.DataGridViewTextBoxColumn Pago;
        private System.Windows.Forms.DataGridViewTextBoxColumn Factura;
        private System.Windows.Forms.DataGridViewTextBoxColumn Moneda;
        private System.Windows.Forms.DataGridViewTextBoxColumn Monto;
        private System.Windows.Forms.DataGridViewTextBoxColumn Voucher;
        private System.Windows.Forms.DataGridViewTextBoxColumn MontoFactura;
        private System.Windows.Forms.DataGridViewTextBoxColumn TipoCambio;
        private System.Windows.Forms.DataGridViewTextBoxColumn FechaPago;
        private System.Windows.Forms.DataGridViewTextBoxColumn NumParcialidad;
        private System.Windows.Forms.DataGridViewTextBoxColumn SaldoInsoluto;
        private System.Windows.Forms.DataGridViewTextBoxColumn RFC;
        private System.Windows.Forms.DataGridViewTextBoxColumn UUID;
    }
}
