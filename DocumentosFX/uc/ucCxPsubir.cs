﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Generales;
using System.Net;
using System.Collections.Specialized;
using System.Configuration;
using System.IO;
using System.Net.Http;
using System.Xml.Serialization;
using System.Xml.Linq;
using System.Xml;
using System.Web.Script.Serialization;
using DocumentosFX.clases;
using Newtonsoft.Json;
using CFDI33;
using System.Runtime.InteropServices;

namespace DocumentosFX.uc
{
    //TODO: Cargar el Check verde 
    //https://www.lawebdelprogramador.com/foros/Visual-Basic.NET/1478397-Descarga-Masiva-XML-SAT.html
    //Descargar de XML
    public partial class ucCxPsubir : UserControl
    {
        string cfdiProveedores = "";
        public bool servicio = false;
        string uuidstr = "";
        public ucCxPsubir()
        {
            InitializeComponent();
            
            try
            {
                cfdiProveedores = ConfigurationManager.AppSettings["cfdiProveedores"].ToString();
                ruta.Text = cfdiProveedores;
                MonitorDirectory(ruta.Text);
            }
            catch
            {

            }

        }

        private void button1_Click(object sender, EventArgs e)
        {
            cargar();
        }

        public static string HttpPost(string URI, string Parameters)
        {
            System.Net.WebRequest req = System.Net.WebRequest.Create(URI);
            //Add these, as we're doing a POST
            req.ContentType = "application/x-www-form-urlencoded";
            req.Method = "POST";
            //We need to count how many bytes we're sending. Post'ed Faked Forms should be name=value&
            byte[] bytes = System.Text.Encoding.ASCII.GetBytes(Parameters);
            req.ContentLength = bytes.Length;
            System.IO.Stream os = req.GetRequestStream();
            os.Write(bytes, 0, bytes.Length); //Push it out there
            os.Close();
            System.Net.WebResponse resp = req.GetResponse();
            if (resp == null) return null;
            System.IO.StreamReader sr = new System.IO.StreamReader(resp.GetResponseStream());
            return sr.ReadToEnd().Trim();
        }

        private void cargar()
        {
            string url = ConfigurationManager.AppSettings["portal"].ToString() + "CxP/documentos/";
            try {
                // You need to post the data as key value pairs:
                string postData = "ver=1&cmd=abf";
                byte[] byteArray = Encoding.UTF8.GetBytes(postData);

                // Post the data to the right place.
                Uri target = new Uri(url);
                WebRequest request = WebRequest.Create(target);
                
                request.Method = "POST";
                request.ContentType = "application/x-www-form-urlencoded";
                request.ContentLength = byteArray.Length;

                using (var dataStream = request.GetRequestStream())
                {
                    dataStream.Write(byteArray, 0, byteArray.Length);
                }

                using (var response = (HttpWebResponse)request.GetResponse())
                {
                    Console.WriteLine("Error code: {0}", response.StatusCode);
                    using (Stream data = response.GetResponseStream())
                    using (var reader = new StreamReader(data))
                    {
                        string text = reader.ReadToEnd();
                        
                    }
                }

            }
            catch (Exception error)
            {
                ErrorFX.mostrar(error, !servicio, true);
            }
        }

        private void MonitorDirectory(string path)
        {

            FileSystemWatcher watcher = new FileSystemWatcher();
            watcher.Path = path;
            watcher.IncludeSubdirectories = true;
            watcher.NotifyFilter = NotifyFilters.LastWrite;
            watcher.Filter = "*.xml";
            watcher.Changed += new FileSystemEventHandler(FileSystemWatcher_Created);
            watcher.EnableRaisingEvents = true;
        }

        private void FileSystemWatcher_Created(object sender, FileSystemEventArgs e)
        {
            timer1.Stop();
            Console.WriteLine("File created: {0}", e.Name);
            FileInfo info = new FileInfo(e.FullPath);
            validarXML(e.FullPath);
            if (lblEjecutar.InvokeRequired)
            {
                MethodInvoker invoker = new MethodInvoker(delegate
                {
                    lblEjecutar.Text = "0";
                    timer1.Start();
                });
                lblEjecutar.Invoke(invoker);
            }
            
        }

        private void button2_Click(object sender, EventArgs e)
        {
            seleccionarFacturas();
        }

        private void seleccionarFacturas()
        {
            try
            {
                using (var fbd = new FolderBrowserDialog())
                {
                    DialogResult result = fbd.ShowDialog();

                    if (result == DialogResult.OK && !string.IsNullOrWhiteSpace(fbd.SelectedPath))
                    {
                        string direccion = fbd.SelectedPath;
                        ruta.Text = direccion;
                        MonitorDirectory(ruta.Text);
                        cargarArchivos(direccion);
                    }
                }

            }
            catch (Exception error)
            {
                ErrorFX.mostrar(error, !servicio, true, true);
            }
        }

        private void cargarArchivos(string direccion)
        {
            try
            {
                dtg.Rows.Clear();
                string[] allfiles = System.IO.Directory.GetFiles(direccion, "*.xml", System.IO.SearchOption.AllDirectories);
                foreach (var file in allfiles)
                {
                    FileInfo info = new FileInfo(file);
                    validarXML(file);
                }
                dtg.AutoResizeColumns(DataGridViewAutoSizeColumnsMode.AllCells);
            }
            catch (Exception error)
            {
                ErrorFX.mostrar(error, !servicio, true, true);
            }
        }

        private void validarXML(string archivo)
        {
            //Validar el tipo de comprobante
            validar33(archivo);
            
        }
        private void agregarRegistro(Comprobante oComprobante, string archivo)
        {
            XmlDocument docXML = new XmlDocument();
            docXML.Load(archivo);
            XmlNodeList TimbreFiscalDigital = docXML.GetElementsByTagName("tfd:TimbreFiscalDigital");
            if (TimbreFiscalDigital == null)
            {
                return;
            }
            //Validar el folio fiscal
            string uuidtr = "";
            if (TimbreFiscalDigital[0] != null)
            {
                uuidtr = TimbreFiscalDigital[0].Attributes["UUID"].Value;
            }


            bool encontrado = false;
            foreach(DataGridViewRow r in dtg.Rows)
            {
                if (r.Cells["UUID"].Equals(uuidtr))
                {
                    encontrado = true;
                }
            }
            //if (uuidstr.Contains(uuidtr))
            //{
            //    encontrado = true;
            //}
            //else
            //{
            //    if (uuidstr!="")
            //    {
            //        uuidstr += ",";
            //    }
            //    uuidstr += uuidtr;
            //}
            if (!encontrado)
            {
                if (dtg.InvokeRequired)
                {
                    MethodInvoker invoker = new MethodInvoker(delegate
                    {
                        int n = dtg.Rows.Add();
                        agregarComprobante(n, oComprobante, archivo);
                        actualizarTotal();
                    });
                    dtg.Invoke(invoker);
                }
                //else
                //{
                //    int n = dtg.Rows.Add();
                //    agregarComprobante(n, oComprobante, archivo);
                //}
            }

        }

        private void actualizarTotal()
        {
            lblTotal.Text = "Total: " + dtg.Rows.Count.ToString();
        }

        private async void agregarComprobante(int n, Comprobante oComprobante, string archivo)
        {
            dtg.Rows[n].Tag = archivo;
            dtg.Rows[n].Cells["procesado"].Value = "";
            dtg.Rows[n].Cells["version"].Value = oComprobante.Version;
            dtg.Rows[n].Cells["TipoDeComprobante"].Value = oComprobante.TipoDeComprobante;

            dtg.Rows[n].Cells["subTotal"].Value = oComprobante.SubTotal;
            dtg.Rows[n].Cells["total"].Value = oComprobante.Total;
            dtg.Rows[n].Cells["emisor"].Value = oComprobante.Emisor.Rfc;
            dtg.Rows[n].Cells["nombre"].Value = oComprobante.Emisor.Nombre;
            dtg.Rows[n].Cells["serie"].Value = "";

            if (!String.IsNullOrEmpty(oComprobante.Serie))
            {
                dtg.Rows[n].Cells["serie"].Value = oComprobante.Serie;
            }
            oComprobante.Folio = "";
            


            XmlDocument docXML = new XmlDocument();
            docXML.Load(archivo);
            XmlNodeList TimbreFiscalDigital = docXML.GetElementsByTagName("tfd:TimbreFiscalDigital");

            //XDocument doc = XDocument.Load(archivo);
            //XNamespace df = doc.Root.Name.Namespace;

            //XElement timbreFiscalDigital = (from rateElement in doc.Elements("tfdTimbreFiscalDigital") 
            //                 select rateElement).Single();

            //string UUIDtr = doc.Root.Attribute("UUID").Value;
            //string receptorName = doc.Root.Element(df + "Receptor").Attribute("nombre").Value;
            string uuidtr = "";
            if (TimbreFiscalDigital != null)
            {
                if (TimbreFiscalDigital[0] != null)
                {
                    uuidtr = TimbreFiscalDigital[0].Attributes["UUID"].Value;
                    dtg.Rows[n].Cells["UUID"].Value = TimbreFiscalDigital[0].Attributes["UUID"].Value;
                }

            }

            if (!String.IsNullOrEmpty(oComprobante.Folio))
            {
                dtg.Rows[n].Cells["folio"].Value = oComprobante.Folio;
            }
            else
            {
                if (uuidtr.Length>5)
                {
                    dtg.Rows[n].Cells["folio"].Value = uuidtr.Substring(uuidtr.Length - 5, 5);
                }
            }

        }

        private bool validar33(string archivo)
        {
            //Pasear el comprobante
            try
            {
                if (!IsFileLocked(archivo))
                {
                    XmlSerializer serializer_CFD_3 = new XmlSerializer(typeof(CFDI33.Comprobante));
                    TextReader reader = new StreamReader(archivo);
                    CFDI33.Comprobante oComprobante = (CFDI33.Comprobante)serializer_CFD_3.Deserialize(reader);
                    reader.Dispose();
                    reader.Close();

                    //Agregar fila
                    agregarRegistro(oComprobante, archivo);
                    return true;
                }
                return false;

            }
            catch(Exception error)
            {
                ErrorFX.mostrar(error, !servicio, true,true);
                return false;
            }
        }

        const int ERROR_SHARING_VIOLATION = 32;
        const int ERROR_LOCK_VIOLATION = 33;
        private bool IsFileLocked(string file)
        {
            //check that problem is not in destination file
            if (File.Exists(file) == true)
            {
                FileStream stream = null;
                try
                {
                    stream = File.Open(file, FileMode.Open, FileAccess.ReadWrite, FileShare.None);
                }
                catch (Exception ex2)
                {
                    //_log.WriteLog(ex2, "Error in checking whether file is locked " + file);
                    int errorCode = Marshal.GetHRForException(ex2) & ((1 << 16) - 1);
                    if ((ex2 is IOException) && (errorCode == ERROR_SHARING_VIOLATION || errorCode == ERROR_LOCK_VIOLATION))
                    {
                        return true;
                    }
                }
                finally
                {
                    if (stream != null)
                        stream.Close();
                }
            }
            return false;
        }
        private void button1_Click_1(object sender, EventArgs e)
        {
            subirArchivos();
        }

        private void subirArchivos()
        {
            try
            {
                lblEjecutar.Text = "0";
                timer1.Stop();
                if (dtg.SelectedRows.Count > 0)
                {
                    foreach (DataGridViewRow drv in dtg.SelectedRows)
                    {
                        try
                        {
                            subirArchivo(drv);
                        }
                        catch (Exception e)
                        {
                            Bitacora.Log(e.Message.ToString());
                        }

                    }
                }

                if (!servicio)
                {
                    //MessageBox.Show("Proceso finalizado", Application.ProductName + "-" 
                    //    + Application.ProductVersion, MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                lblEjecutar.Text = "0";
                timer1.Start();
            }
            catch (Exception error)
            {
                ErrorFX.mostrar(error, !servicio, true, true);
                return;
            }
        }
        //TODO: Subir todos en Gatwaey 
        //TODO: Cargar local Validar
        //TODO: Lista de pagos y postearlas
        //TODO: Agregar un plantilla para el formato de pago

        private bool validarUUID(string uuid)
        {
            using (WebClient client = new WebClient())
            {
                string url = ConfigurationManager.AppSettings["portal"].ToString() + "Conector/existeUUID/";

                HttpWebRequest requestToServerEndpoint =
                (HttpWebRequest)WebRequest.Create(url);
                string boundaryString = "----SomeRandomText";
                // Set the http request header \\
                requestToServerEndpoint.Method = WebRequestMethods.Http.Post;
                requestToServerEndpoint.ContentType = "multipart/form-data; boundary=" + boundaryString;
                requestToServerEndpoint.KeepAlive = true;
                requestToServerEndpoint.Credentials = System.Net.CredentialCache.DefaultCredentials;

                

                MemoryStream postDataStream = new MemoryStream();
                StreamWriter postDataWriter = new StreamWriter(postDataStream);

                // Include value from the myFileDescription text area in the post data
                postDataWriter.Write("\r\n--" + boundaryString + "\r\n");
                postDataWriter.Write("Content-Disposition: form-data; name=\"{0}\"\r\n\r\n{1}", "uuid", uuid);
                postDataWriter.Flush();
                
                // Set the http request body content length
                requestToServerEndpoint.ContentLength = postDataStream.Length;

                // Dump the post data from the memory stream to the request stream
                using (Stream s = requestToServerEndpoint.GetRequestStream())
                {
                    postDataStream.WriteTo(s);
                }
                postDataStream.Close();

                WebResponse response = requestToServerEndpoint.GetResponse();
                StreamReader responseReader = new StreamReader(response.GetResponseStream());
                string replyFromServer = responseReader.ReadToEnd();

                //Convertir en Json ver los errores
                try
                {
                    cResultadoSubir resultado = JsonConvert.DeserializeObject<cResultadoSubir>(replyFromServer);
                    if (resultado.status)
                    {
                        return false;
                    }
                    return true;
                }
                catch (Exception error)
                {
                    ErrorFX.mostrar(error, false, true, true);
                    System.Threading.Thread.Sleep(10000);
                    validarUUID(uuid);
                }

                return true;
            }
        }

        private void subirArchivo(DataGridViewRow drv)
        {
            //Validar existencia en el portal
            //Existe el UUID
            //string uuditr = drv.Cells["UUID"].Value.ToString();
            //System.Threading.Thread.Sleep(10000);
            //if (this.validarUUID(uuditr))
            //{
            //    return;
            //}
            System.Threading.Thread.Sleep(5000);
            using (WebClient client = new WebClient())
            {



                string archivo = (string)drv.Tag;
                string url = ConfigurationManager.AppSettings["portal"].ToString() + "Documento/guardar/";
                dtg.Rows[drv.Index].ErrorText = "";

                HttpWebRequest requestToServerEndpoint =
                (HttpWebRequest)WebRequest.Create(url);

                string boundaryString = "----SomeRandomText";
                string fileUrl = archivo;

                // Set the http request header \\
                requestToServerEndpoint.Method = WebRequestMethods.Http.Post;
                requestToServerEndpoint.ContentType = "multipart/form-data; boundary=" + boundaryString;
                requestToServerEndpoint.KeepAlive = true;
                requestToServerEndpoint.Credentials = System.Net.CredentialCache.DefaultCredentials;

                // Use a MemoryStream to form the post data request,
                // so that we can get the content-length attribute.
                MemoryStream postDataStream = new MemoryStream();
                StreamWriter postDataWriter = new StreamWriter(postDataStream);

                // Include value from the myFileDescription text area in the post data
                postDataWriter.Write("\r\n--" + boundaryString + "\r\n");
                postDataWriter.Write("Content-Disposition: form-data; name=\"{0}\"\r\n\r\n{1}", "identificacion", "validadorXML");

                postDataWriter.Write("\r\n--" + boundaryString + "\r\n");
                postDataWriter.Write("Content-Disposition: form-data; name=\"{0}\"\r\n\r\n{1}", "no_notificar", "no_notificar");

                // Include the file in the post data
                postDataWriter.Write("\r\n--" + boundaryString + "\r\n");
                postDataWriter.Write("Content-Disposition: form-data;"
                + "name=\"{0}\";"
                + "filename=\"{1}\""
                + "\r\nContent-Type: {2}\r\n\r\n",
                "XML_FILE",
                Path.GetFileName(fileUrl),
                Path.GetExtension(fileUrl));
                postDataWriter.Flush();
                // Read the file
                FileStream fileStream = new FileStream(fileUrl, FileMode.Open, FileAccess.Read);
                byte[] buffer = new byte[1024];
                int bytesRead = 0;
                while ((bytesRead = fileStream.Read(buffer, 0, buffer.Length)) != 0)
                {
                    postDataStream.Write(buffer, 0, bytesRead);
                }
                fileStream.Close();
                postDataWriter.Write("\r\n--" + boundaryString + "--\r\n");
                postDataWriter.Flush();




                // Set the http request body content length
                requestToServerEndpoint.ContentLength = postDataStream.Length;

                // Dump the post data from the memory stream to the request stream
                using (Stream s = requestToServerEndpoint.GetRequestStream())
                {
                    postDataStream.WriteTo(s);
                }
                postDataStream.Close();

                WebResponse response = requestToServerEndpoint.GetResponse();
                StreamReader responseReader = new StreamReader(response.GetResponseStream());
                string replyFromServer = responseReader.ReadToEnd();

                //Convertir en Json ver los errores
                try
                {
                    cResultadoSubir resultado = JsonConvert.DeserializeObject<cResultadoSubir>(replyFromServer);
                    if (!resultado.status)
                    {
                        dtg.Rows[drv.Index].ErrorText = "Error " + resultado.mensaje;
                    }
                    else
                    {
                        dtg.Rows[drv.Index].Cells["procesado"].Value = "Procesado " + DateTime.Now.ToShortDateString() +  " "
                            + DateTime.Now.ToShortTimeString();
                    }
                }
                catch(WebException errorWeb)
                {
                    System.Threading.Thread.Sleep(10000);
                    subirArchivo(drv);
                }
                catch(Exception error)
                {
                    ErrorFX.mostrar(error, false, true, true);
                    System.Threading.Thread.Sleep(10000);
                    subirArchivo(drv);
                }

            }
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            procesarRegistro();
        }

        private void procesarRegistro()
        {
            if (lblEjecutar.Text=="30")
            {
                lblEjecutar.Text = "0";
                timer1.Stop();
                ////Procesar el primer registro que no este procesado
                //foreach (DataGridViewRow r in dtg.Rows)
                //{
                //    if (String.IsNullOrEmpty(r.Cells["procesado"].Value.ToString()))
                //    {
                //        subirArchivo(r);
                //    }
                //    break;
                //}
                dtg.SelectAll();
                subirArchivos();
                timer1.Start();
            }
            else
            {
                int contador = int.Parse(lblEjecutar.Text);
                contador++;
                lblEjecutar.Text = contador.ToString();
            }
        }

        private void lblEjecutar_Click(object sender, EventArgs e)
        {

        }

        private void timer1_Tick_1(object sender, EventArgs e)
        {
            procesarRegistro();
        }
    }
}
