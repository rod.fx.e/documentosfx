﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Generales;
using DocumentosFX.formularios;
using FE_FX.Clases;
using System.Configuration;
using Newtonsoft.Json;
using System.IO;
using ModeloSincronizacion;

namespace CxP.ComplementoPago
{
    public partial class ucListaPagos : UserControl
    {

        public ucListaPagos()
        {
            InitializeComponent();
        }
        private void limpiar()
        {
            this.Text = Application.ProductName + " " + Application.ProductVersion;
            DateTime firstDayOfCurrentMonth = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1);
            INICIO.Value = firstDayOfCurrentMonth;
            cargarEmpresas();
        }
        private void cargarEmpresas()
        {

            emisor.Items.Clear();
            emisor.Items.Add("Todos");
            cEMPRESA ocEMPRESA = new cEMPRESA();
            foreach (cEMPRESA registro in ocEMPRESA.todos("", true, Globales.automatico))
            {
                emisor.Items.Add(registro);
            }
            if (emisor.Items.Count > 0)
            {
                emisor.SelectedIndex = 0;
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            cargar();
        }

        private DataTable cargarPagos(cCONEXCION oData_ERPp, cEMPRESA ocEMPRESA)
        {
            string BD_Auxiliar = "";
            if (ocEMPRESA.BD_AUXILIAR != "")
            {
                BD_Auxiliar = ocEMPRESA.BD_AUXILIAR + ".dbo.";
            }
            //Que esten posteados
            string sSQL_Where = @" WHERE 1=1 
            AND ISNULL((SELECT TOP 1 1 FROM CASH_DISBURSE_DIST cdd WHERE cdd.BANK_ACCOUNT_ID=cd.BANK_ACCOUNT_ID AND cdd.CONTROL_NO=cd.CONTROL_NO 
            AND cdd.POSTING_STATUS <> 'U'),0)= 1";
            string FECHA_INICIO = oData_ERPp.convertir_fecha(new DateTime(INICIO.Value.Year, INICIO.Value.Month, INICIO.Value.Day, 0, 0, 0), oData_ERPp.sTipo);
            sSQL_Where += " AND cd.CHECK_DATE>=" + FECHA_INICIO + "";
            string FECHA_FINAL = oData_ERPp.convertir_fecha(new DateTime(FINAL.Value.Year, FINAL.Value.Month, FINAL.Value.Day, 0, 0, 0), oData_ERPp.sTipo);
            sSQL_Where += " AND cd.CHECK_DATE<=" + FECHA_FINAL + "";

            //, ISNULL((SELECT TOP 1 a.UUID FROM documentosfx.dbo.archivoSyncSet a
            //WHERE a.folio COLLATE SQL_Latin1_General_CP1_CI_AS = p.INVOICE_ID COLLATE SQL_Latin1_General_CP1_CI_AS
            //AND a.rfcEmisor COLLATE SQL_Latin1_General_CP1_CI_AS = v.VAT_REGISTRATION COLLATE SQL_Latin1_General_CP1_CI_AS),'') AS 'UUID'

            if (!String.IsNullOrEmpty(vendorId.Text))
            {
                sSQL_Where += " AND cd.VENDOR_ID like '" + vendorId.Text + "'";
            }

            if (!String.IsNullOrEmpty(voucherId.Text))
            {
                sSQL_Where += " AND cdl.VOUCHER_ID like '" + voucherId.Text + "'";
            }

            if (!String.IsNullOrEmpty(this.CHECK_ID.Text))
            {
                sSQL_Where += " AND cd.CHECK_NO like '" + CHECK_ID.Text + "'";
            }

            //Validar el SITE
            if (!String.IsNullOrEmpty(ocEMPRESA.SITE_ID))
            {
                sSQL_Where += " AND cd.SITE_ID like '" + ocEMPRESA.SITE_ID + "'";
            }
            else
            {
                sSQL_Where += " AND cd.ENTITY_ID like '" + ocEMPRESA.ENTITY_ID + "'";
            }

            //Tighitco 
            sSQL_Where += " AND (NOT([DEF_EXP_GL_ACCT_ID] like '2100-1003')) ";

            string sSQL = @"
            SELECT cd.BANK_ACCOUNT_ID AS Banco, BANK_ACCOUNT.ACCOUNT_NO AS Cuenta, cd.VENDOR_ID AS Proveedor, cd.CONTROL_NO AS 'Control', cd.CHECK_NO AS 'Pago'
            , cd.AMOUNT AS Monto, cdl.VOUCHER_ID AS Voucher, cdl.AMOUNT AS 'Monto Factura', 
                                     cdl.SELL_RATE AS [Tipo de Cambio], cd.CURRENCY_ID AS Moneda, p.INVOICE_ID AS 'Factura'
            ,'' AS 'UUID'
            , cd.CHECK_DATE AS 'Fecha de Pago', ISNULL
            ((SELECT        COUNT(*) + 1 AS Expr1
                FROM            CASH_DISBURSEMENT AS cd2 INNER JOIN
                                        CASH_DISBURSE_LINE AS cdl2 ON cd2.BANK_ACCOUNT_ID = cdl2.BANK_ACCOUNT_ID AND cd2.CONTROL_NO = cdl2.CONTROL_NO INNER JOIN
                                        PAYABLE AS p2 ON cdl2.VOUCHER_ID = p2.VOUCHER_ID
                WHERE        (cd2.VENDOR_ID = cd.VENDOR_ID) AND (cdl2.VOUCHER_ID = cdl.VOUCHER_ID) AND (cd2.CHECK_DATE < cd.CHECK_DATE)), 1) AS NumParcialidad, ISNULL
            ((SELECT        SUM(cdl2.AMOUNT) AS Expr1
                FROM            CASH_DISBURSEMENT AS cd2 INNER JOIN
                                        CASH_DISBURSE_LINE AS cdl2 ON cd2.BANK_ACCOUNT_ID = cdl2.BANK_ACCOUNT_ID AND cd2.CONTROL_NO = cdl2.CONTROL_NO INNER JOIN
                                        PAYABLE AS p2 ON cdl2.VOUCHER_ID = p2.VOUCHER_ID
                WHERE        (cd2.VENDOR_ID = cd.VENDOR_ID) AND (cdl2.VOUCHER_ID = cdl.VOUCHER_ID) AND (cd2.CHECK_DATE < cd.CHECK_DATE)), 0) AS SaldoInsoluto
				, v.VAT_REGISTRATION as 'RFC'
            FROM            CASH_DISBURSEMENT AS cd INNER JOIN
                                     CASH_DISBURSE_LINE AS cdl ON cd.BANK_ACCOUNT_ID = cdl.BANK_ACCOUNT_ID AND cd.CONTROL_NO = cdl.CONTROL_NO INNER JOIN
                                     PAYABLE AS p ON cdl.VOUCHER_ID = p.VOUCHER_ID INNER JOIN
                                     BANK_ACCOUNT ON cd.BANK_ACCOUNT_ID = BANK_ACCOUNT.ID INNER JOIN
                                     VENDOR as v ON cd.VENDOR_ID = v.ID 
            " + sSQL_Where;
            //Ordenar por fecha de pago
            sSQL += " ORDER BY cd.CHECK_DATE";


            DataTable oDataTable = oData_ERPp.EjecutarConsulta(sSQL);


            return oDataTable;
        }


        private void cargar()
        {
            try
            {
                dtgrdGeneral.Rows.Clear();
                DataTable oDataTable = new DataTable();
                if (emisor.Text == "Todos")
                {
                    //Navegar por los items
                    foreach (var item in emisor.Items)
                    {
                        try
                        {
                            cEMPRESA ocEMPRESA = item as cEMPRESA;
                            DataTable oDataTabletr = new DataTable();
                            if (ocEMPRESA != null)
                            {
                                cCONEXCION oData_ERPp = new cCONEXCION(ocEMPRESA.TIPO, ocEMPRESA.SERVIDOR
                                    , ocEMPRESA.BD, ocEMPRESA.USUARIO_BD, ocEMPRESA.PASSWORD_BD);
                                cargarDataTable(cargarPagos(oData_ERPp, ocEMPRESA), dtgrdGeneral, oData_ERPp, ocEMPRESA,false);
                            }

                        }
                        catch (Exception error)
                        {
                            ErrorFX.mostrar(error, true, true, "ucListaPagos - 76 - ", true);
                        }
                    }
                }
                else
                {
                    cEMPRESA ocEMPRESA = emisor.SelectedItem as cEMPRESA;
                    if (ocEMPRESA != null)
                    {
                        cCONEXCION oData_ERPp = new cCONEXCION(ocEMPRESA.TIPO, ocEMPRESA.SERVIDOR, ocEMPRESA.BD, ocEMPRESA.USUARIO_BD, ocEMPRESA.PASSWORD_BD);
                        cargarDataTable(cargarPagos(oData_ERPp, ocEMPRESA), dtgrdGeneral, oData_ERPp, ocEMPRESA);
                    }
                }

                this.toolStripStatusLabel1.Text = "Total de pagos cargados: " + this.dtgrdGeneral.Rows.Count.ToString();

            }
            catch (Exception e)
            {
                ErrorFX.mostrar(e, true, true, true);
            }
        }

        private void cargarDataTable(DataTable oDataTable, DataGridView dtg, cCONEXCION oData_ERPp
            , cEMPRESA ocEMPRESA, bool limpiar = true)
        {
            if (oDataTable != null)
            {
                Application.DoEvents();
                if (limpiar)
                {
                    dtg.Rows.Clear();
                }
                toolStripStatusLabel1.Text = "Cargando ";

                int contador = 0;
                foreach (DataRow oDataRow in oDataTable.Rows)
                {
                    contador++;
                    //Banco	Cuenta	Proveedor	Pago	Monto	Voucher	Monto Factura	Tipo de Cambio	Moneda	Factura	UUID	Fecha de Pago	
                    //NumParcialidad	SaldoInsoluto	RFC

                    string uuid=buscarComplemento(oDataRow["Proveedor"].ToString(),
                        oDataRow["Factura"].ToString(), oDataRow["RFC"].ToString()
                        , oData_ERPp, ocEMPRESA);
                    bool mostrar = true;

                    //if (PENDIENTES.Checked)
                    //{
                    //    if (!String.IsNullOrEmpty(uuid))
                    //    {
                    //        mostrar = false;
                    //    }
                    //}
                        

                    if (mostrar)
                    {
                        int n = dtg.Rows.Add();

                        for (int i = 0; i < dtg.Columns.Count; i++)
                        {
                            dtg.Rows[n].Cells[i].Value = "";
                        }
                        Encapsular oEncapsular = new Encapsular();
                        oEncapsular.oData_ERP = oData_ERPp;
                        oEncapsular.ocEMPRESA = ocEMPRESA;
                        dtg.Rows[n].Tag = oEncapsular;
                        dtg.Rows[n].Cells[0].Tag = oDataRow;
                        dtg.Rows[n].Cells["CONTROL_NO"].Value = oDataRow["Control"].ToString();
                        dtg.Rows[n].Cells["Banco"].Value = oDataRow["Banco"].ToString();
                        dtg.Rows[n].Cells["Cuenta"].Value = oDataRow["Cuenta"].ToString();
                        dtg.Rows[n].Cells["Proveedor"].Value = oDataRow["Proveedor"].ToString();
                        dtg.Rows[n].Cells["Pago"].Value = oDataRow["Pago"].ToString();
                        dtg.Rows[n].Cells["Monto"].Value = decimal.Parse(oDataRow["Monto"].ToString()).ToString("C2");
                        dtg.Rows[n].Cells["Factura"].Value = oDataRow["Factura"].ToString();
                        dtg.Rows[n].Cells["Moneda"].Value = oDataRow["Moneda"].ToString();
                        dtg.Rows[n].Cells["Voucher"].Value = oDataRow["Voucher"].ToString();
                        dtg.Rows[n].Cells["MontoFactura"].Value = decimal.Parse(oDataRow["Monto Factura"].ToString()).ToString("C2");
                        dtg.Rows[n].Cells["TipoCambio"].Value = decimal.Parse(oDataRow["Tipo de Cambio"].ToString()).ToString("N4");
                        dtg.Rows[n].Cells["FechaPago"].Value = DateTime.Parse(oDataRow["Fecha de Pago"].ToString()).ToShortDateString();
                        dtg.Rows[n].Cells["NumParcialidad"].Value = oDataRow["NumParcialidad"].ToString();
                        dtg.Rows[n].Cells["SaldoInsoluto"].Value = oDataRow["SaldoInsoluto"].ToString();
                        dtg.Rows[n].Cells["RFC"].Value = oDataRow["RFC"].ToString();
                        dtg.Rows[n].Cells["UUID"].Value = uuid;

                        //Buscar la conciliacion

                    }
                    
                    
                }
                dtg.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.DisplayedCells;
            }
            Application.DoEvents();
            toolStripStatusLabel1.Text = "Fin ";
        }

        private string buscarComplemento(string proveedor, string factura, string rfc, cCONEXCION oData_ERPp
            , cEMPRESA ocEMPRESA)
        {
            try
            {
                bool validacionLocal = false;
                try
                {
                    validacionLocal = bool.Parse(ConfigurationManager.AppSettings["validacionComplementoPagoLocal"].ToString());
                }
                catch
                {

                }
                if (validacionLocal)
                {
                    //Validar en las tablas de archivoSync dependiendo de la empresa
                    ModeloSincronizacion.ModeloSincronizacionContainer dbContext = new ModeloSincronizacionContainer();
                    archivoSync result = (from b in dbContext.archivoSyncSet
                                                 .Where(a => a.rfcEmisor.Equals(rfc)
                                                 &
                                                 a.tipoComprobante.Equals("I")
                                                 &
                                                 a.folio.Equals(factura)
                                                 )
                                 select b).FirstOrDefault();
                    dbContext.Dispose();
                    if (result!=null)
                    {
                        return result.UUID;
                    }
                }
                else
                {
                    //Enviar al Portal en el listado de pago
                    return buscarUUIDportal(proveedor, factura, rfc, ocEMPRESA);
                }
            }
            catch (Exception e)
            {
                ErrorFX.mostrar(e, true, true, true);
            }
            return "";
        }

        private string buscarUUIDportal(string proveedor, string factura, string rfc, cEMPRESA ocEMPRESA)
        {
            try
            {
                
            }
            catch (Exception e)
            {
                ErrorFX.mostrar(e, true, true, true);
            }
            return "";
        }

        private void button3_Click(object sender, EventArgs e)
        {
            ExcelFX.exportar(dtgrdGeneral, "Lista de pagos ");
        }

        private void button5_Click(object sender, EventArgs e)
        {
            limpiar();
        }

        private void cambiarFormaDePagoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            cambiarFactura();
        }

        private void cambiarFactura()
        {
            try
            {
                if (dtgrdGeneral.SelectedRows.Count > 0)
                {
                    DataGridViewSelectedRowCollection arrSelectedRows = dtgrdGeneral.SelectedRows;
                    for (int n = arrSelectedRows.Count - 1; n >= 0; n--)
                    {
                        Encapsular oEncapsular = (Encapsular)arrSelectedRows[n].Tag;
                        string factura=arrSelectedRows[n].Cells["Factura"].Value.ToString();
                        string voucher=arrSelectedRows[n].Cells["Voucher"].Value.ToString();
                        string proveedor = arrSelectedRows[n].Cells["Proveedor"].Value.ToString();
                        frmPayableCambio ofrmPayableCambio = new frmPayableCambio(oEncapsular, factura, voucher,proveedor);
                        ofrmPayableCambio.Show();
                    }
                }

            }
            catch (Exception e)
            {
                ErrorFX.mostrar(e, true, true, true);
            }
        }

        private void dtgrdGeneral_MouseClick(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Right)
            {
                contextMenuStrip1.Show(dtgrdGeneral, new Point(e.X, e.Y));
            }
        }

        private void enviarNotificaciónToolStripMenuItem_Click(object sender, EventArgs e)
        {
            enviarCorreos();
        }

        private void enviarCorreos()
        {
            try
            {
                if (dtgrdGeneral.SelectedRows.Count > 0)
                {
                    DataGridViewSelectedRowCollection arrSelectedRows = dtgrdGeneral.SelectedRows;
                    for (int n = arrSelectedRows.Count - 1; n >= 0; n--)
                    {
                        enviarCorreo(arrSelectedRows[n]);
                    }
                }
            }
            catch (Exception e)
            {
                ErrorFX.mostrar(e, true, true, true);
            }
        }

        private void enviarCorreo(DataGridViewRow r)
        {
            try
            {
                bool envioLocal = false;
                try
                {
                    envioLocal = bool.Parse(ConfigurationManager.AppSettings["envioRecordatorioComplementoPagoLocal"].ToString());
                }
                catch
                {

                }
                if (envioLocal)
                {
                    //Enviar Correo local con la configuración

                }
                else
                {
                    Encapsular oEncapsular = (Encapsular)r.Tag;
                    //Enviar al Portal en el listado de pago
                    string archivo = Path.GetTempFileName();
                    string json = JsonConvert.SerializeObject((DataRow)r.Cells[0].Tag, Newtonsoft.Json.Formatting.None);
                    System.IO.File.WriteAllText(archivo, json);
                    string uri = oEncapsular.ocEMPRESA.portal + "/Conector/listapagoMasivo";
                    System.Net.WebClient Client = new System.Net.WebClient();
                    Client.Headers.Add("Content-Type", "binary/octet-stream");
                    byte[] result = Client.UploadFile(uri, "POST", archivo);
                    string resultado = System.Text.Encoding.UTF8.GetString(result, 0, result.Length);

                }
            }
            catch (Exception e)
            {
                ErrorFX.mostrar(e, true, true, true);
            }
            return;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            
        }

        private void button6_Click(object sender, EventArgs e)
        {
            concliacion();
        }

        private void concliacion()
        {
            try
            {
                //Cargar la concialición

            }
            catch (Exception e)
            {
                ErrorFX.mostrar(e, true, true, true);
            }
            return;
        }
    }
}
