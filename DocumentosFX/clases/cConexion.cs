﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DocumentosFX.clases
{
    class cConexion
    {

        public string sConn;
        public SqlDataAdapter oDataAdapter;
        public SqlConnection oConn;
        public string no_existe_BD = "";

        public cConexion()
        {

        }

        public string formato_double(string valor, string con_coma)
        {
            string conversion;
            double Plantilla;
            if (valor.Trim() == "")
            {
                valor = "0";
            }
            Plantilla = double.Parse(valor.ToString());
            if (con_coma == "")
            {
                conversion = Plantilla.ToString("#,###,###,##0.00");
            }
            else
            {
                conversion = Plantilla.ToString("#########0.00");
            }
            return conversion;
        }
        public bool IsBool(string Expression)
        {
            try
            {

                bool resultado = bool.Parse(Expression);
                return true;
            }
            catch
            {
                return false;
            }
        }


        public cConexion(string pConn)
        {
            this.sConn = pConn;
        }
        public bool IsDate(string inputDate)
        {
            bool isDate = true;
            try
            {
                DateTime dt = DateTime.Parse(inputDate);
            }
            catch
            {
                isDate = false;
            }
            return isDate;
        }


        public bool CrearConexion(bool mostrarError=true)
        {
            oConn = new SqlConnection(sConn);
            try
            {
                oConn.Open();
                return true;
            }
            catch (SqlException e)
            {
                ErrorFX.mostrar(e, mostrarError, false);
            }
            return false;
        }

        public void DestruirConexion()
        {
            oConn = null;
        }

        public DataTable EjecutarConsulta(string pQuery)
        {
            try
            {
                DataTable oDT = new DataTable();
                //Ejecutar para SQL Server
                oDT = new DataTable();
                oConn = new SqlConnection(sConn);
                oDataAdapter = new SqlDataAdapter();
                SqlCommand oSqlCommand = new SqlCommand(pQuery, oConn);
                oSqlCommand.CommandTimeout = 10000000;
                oDataAdapter.SelectCommand = oSqlCommand;
                oDataAdapter.Fill(oDT);
                return oDT;
            }
            catch (SqlException e)
            {
                ErrorFX.mostrar(e, true, false);
                return null;
            }

        }

        public string Trunca_y_formatea(decimal numero)
        {
            string resultado = "0.0000";
            decimal numero_f = Truncar_decimales(decimal.Parse(numero.ToString()));
            resultado = formato_decimal(numero_f.ToString(), "N");
            return (resultado);
        }

        public decimal Truncar_decimales(decimal numero)
        {
            int numero_truncar = 100;
            decimal parte_decimal = 0, pasar_a_decimal = 0, resultado = 0;
            int parte_entera, separar_2_primeros_decimales;

            parte_entera = (int)numero;
            if (parte_entera != 0)
            {
                parte_decimal = numero % parte_entera;
            }
            else
            {
                parte_decimal = numero;
            }
            separar_2_primeros_decimales = (int)(parte_decimal * numero_truncar);
            pasar_a_decimal = (decimal)separar_2_primeros_decimales / numero_truncar;
            resultado = (decimal)parte_entera + pasar_a_decimal;

            return (resultado);
        }
        public DataTable EjecutarCommando(SqlCommand pQuery)
        {
            try
            {
                DataTable oDT = new DataTable();
                //Ejecutar para SQL Server
                oDT = new DataTable();
                oConn = new SqlConnection(sConn);
                oDataAdapter = new SqlDataAdapter();
                pQuery.Connection = oConn;
                pQuery.CommandTimeout = 1000000;
                oDataAdapter.SelectCommand = pQuery;
                oDataAdapter.Fill(oDT);
                return oDT;
            }
            catch (SqlException e)
            {
                ErrorFX.mostrar(e, true, false);
                return null;
            }
        }
        public string formato_decimal(string valor, string con_coma, int cantidad_decimales = 2)
        {
            string conversion;
            double Plantilla;
            if (valor.Trim() == "")
            {
                valor = "0";
            }
            Plantilla = double.Parse(valor.ToString());
            if (con_coma == "")
            {
                if (cantidad_decimales == 2)
                {
                    conversion = Plantilla.ToString("#,###,###,##0.00");
                }
                else
                {
                    conversion = Plantilla.ToString("#,###,###,##0.0000");
                }
            }
            else
            {

                if (cantidad_decimales == 2)
                {
                    conversion = Plantilla.ToString("#########0.00");
                }
                else
                {
                    conversion = Plantilla.ToString("#########0.0000");
                }
            }
            return conversion;
        }

        public string IsNullNumero(object Expression)
        {
            string valor = Trunca_y_formatea(1);
            if (Expression != null)
            {
                valor = Expression.ToString();
            }
            else
            {
                if (Expression.ToString() == "")
                {
                    valor = Trunca_y_formatea(1);
                }
            }
            return valor;
        }

        public string IsNull(object Expression)
        {
            string valor = "";
            if (Expression != null)
            {
                if (Expression.ToString() == "01/01/1900 00:00:00")
                {
                    valor = "";
                }
                else
                {
                    valor = Expression.ToString();
                }
            }
            return valor;
        }

        public bool IsNumeric(object Expression)
        {
            bool isNum;
            double retNum;

            isNum = Double.TryParse(Convert.ToString(Expression), System.Globalization.NumberStyles.Any, System.Globalization.NumberFormatInfo.InvariantInfo, out retNum);
            return isNum;
        }

        public string convertir_fecha(DateTime pfecha, string como = "n")
        {
            string fecha = "";
            //SQLServer
            fecha = "CONVERT(DATETIME,'" + pfecha.Year.ToString() + "-" + pfecha.Month.ToString() + "-" + pfecha.Day.ToString() + " " + pfecha.Hour.ToString() + ":" + pfecha.Minute.ToString() + ":" + pfecha.Second.ToString() + "',102)";
            if (como == "i")
            {
                fecha = "CONVERT(DATETIME,'" + pfecha.Year.ToString() + "-" + pfecha.Month.ToString() + "-" + pfecha.Day.ToString() + " 00:00:00',102)";
            }
            if (como == "f")
            {
                fecha = "CONVERT(DATETIME,'" + pfecha.Year.ToString() + "-" + pfecha.Month.ToString() + "-" + pfecha.Day.ToString() + " 23:59:59',102)";
            }
            return fecha;
        }

        public string convertir_cadena_fecha(string fecha)
        {
            try
            {
                DateTime fecha_tr = DateTime.Parse(fecha);
                return convertir_fecha(fecha_tr, "");
            }
            catch
            {
                return "NULL";
            }
        }
    }

}
