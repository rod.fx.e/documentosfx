﻿using System;
using System.IO;

namespace DocumentosFX.clases
{
    public sealed class BitacoraFX
    {

        private static object syncRoot = new Object();
        public static string salidaConsola = "N";

        public static void Log(string msg)
        {
            DateTime Hoy = DateTime.Today;

            string fechaActual = Hoy.ToString("yyyy-MM-dd");

            using (StreamWriter w = File.AppendText(fechaActual + "_log.txt"))
            {
                Log(msg, w);

                w.Close();
            }
            // Open and read the file.
            //using (StreamReader r = File.OpenText("log.txt"))
            //{
            //    DumpLog(r);
            //}

        }

        private static void Log(String logMessage, TextWriter w)
        {
            string msg = "\r\n";
            msg += "\r\n " + DateTime.Now.ToLongTimeString() + " " + DateTime.Now.ToLongDateString();
            msg += "\r\n:: " + logMessage;
            msg += "\r\n" + "-------------------------------";

            w.Write(msg);
            w.Flush();

            if (salidaConsola.ToUpper().Equals("Y"))
                Console.Write(msg);

            //w.Write("\r\nMsg: ");
            //w.WriteLine("{0} {1}", DateTime.Now.ToLongTimeString(), DateTime.Now.ToLongDateString());
            //w.WriteLine("  :");
            //w.WriteLine("  :{0}", logMessage);
            //w.WriteLine("-------------------------------");
            // Actualiza el archivo.
        }
        private static void DumpLog(StreamReader r)
        {
            // Mientras no sea el final del archivo, lee las lineas y escribe en la consola.
            String line;
            while ((line = r.ReadLine()) != null)
            {
                Console.WriteLine(line);
            }
            r.Close();
        }

    }

}
