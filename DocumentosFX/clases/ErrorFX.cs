﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DocumentosFX.clases
{
    public sealed class ErrorFX
    {
        public static void mostrar(Exception error, bool mostrar, bool enviarCorreo)
        {
            string mensaje = error.Message.ToString();
            if (error.InnerException != null)
            {
                mensaje += Environment.NewLine + error.InnerException.Message.ToString();
            }

            if (mostrar)
            {
                MessageBox.Show(mensaje, Application.ProductName + "-" + Application.ProductVersion, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            if (enviarCorreo)
            {
                sendSMTPMail("Documentos FX: Versión " + Application.ProductName + "-" + Application.ProductVersion
                , mensaje);
            }

            BitacoraFX.Log(mensaje);

        }

        public static void sendSMTPMail(string asunto, string mensaje)
        {
            SmtpClient client = new SmtpClient(Properties.Settings.Default.errorHost,587);
            MailMessage mm = new MailMessage();
            mm.IsBodyHtml = true;
            mm.From = new MailAddress(Properties.Settings.Default.errorFrom, Properties.Settings.Default.errorFromName, Encoding.UTF8);
            mm.Subject = asunto;
            mm.Body = mensaje;
            mm.SubjectEncoding = System.Text.Encoding.UTF8;
            mm.IsBodyHtml = true;
            mm.Priority = MailPriority.High;

            string[] rEmailCC = Properties.Settings.Default.errorCorreos.Split(';');
            for (int i = 0; i < rEmailCC.Length; i++)
            {
                if (!String.IsNullOrEmpty(rEmailCC[i].ToString().Trim()))
                {
                    if (Globales.isEmail(rEmailCC[i].ToString().Trim()))
                    {
                        mm.CC.Add(rEmailCC[i].ToString().Trim());
                    }
                }
            }
            try
            {
                client.DeliveryMethod = SmtpDeliveryMethod.Network;
                client.EnableSsl = false;
                client.UseDefaultCredentials = true;
                client.Timeout = 2000000;
                System.Net.NetworkCredential credenciales = new System.Net.NetworkCredential(Properties.Settings.Default.errorUser, Properties.Settings.Default.errorPassword);
                client.Credentials = credenciales;
                client.SendAsync(mm, "userToken");
            }
            catch (Exception error)
            {
                string mensajetr = error.Message.ToString();
                if (error.InnerException != null)
                {
                    mensajetr += Environment.NewLine + error.InnerException.Message.ToString();
                }
                BitacoraFX.Log("Envio de correo ErrorFX: " + mensajetr);
            }

        }
    }

}
