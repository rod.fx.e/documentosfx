﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DocumentosFX.clases
{
    class cCFDI
    {
        public string version { get; set; }
        public string folio { get; set; }
        public string uuid { get; set; }
        public string tipoDeComprobante { get; set; }
        public DateTime fechaFactura { get; set; }
        public bool status { get; set; }
        public string archivo { get; set; }
        public string moneda  { get; set; }
        public decimal subTotal  { get; set; }
        public decimal total  { get; set; }
        public string rfcEmisor  { get; set; }
        public string nombre  { get; set; }
        public string rfcReceptor { get; set; }
        public decimal tipoCambio { get; set; }
        public string serie  { get; set; }

        public string tipo { get; set; }
        public string usoCFDI { get; set; }
        public string formaPago { get; set; }
        public string metodoPago { get; set; }
        public string lugarexpedicion { get; set; }
        public string razon { get; set; }
    }
}
