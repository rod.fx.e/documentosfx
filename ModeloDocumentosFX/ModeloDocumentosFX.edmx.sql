
-- --------------------------------------------------
-- Entity Designer DDL Script for SQL Server 2005, 2008, 2012 and Azure
-- --------------------------------------------------
-- Date Created: 07/11/2023 11:01:58
-- Generated from EDMX file: D:\Proyectos\DocumentosFX\SAT\ImportarCatalogos\ModeloDocumentosFX\ModeloDocumentosFX.edmx
-- --------------------------------------------------

SET QUOTED_IDENTIFIER OFF;
GO
USE [documentosfx];
GO
IF SCHEMA_ID(N'dbo') IS NULL EXECUTE(N'CREATE SCHEMA [dbo]');
GO

-- --------------------------------------------------
-- Dropping existing FOREIGN KEY constraints
-- --------------------------------------------------


-- --------------------------------------------------
-- Dropping existing tables
-- --------------------------------------------------

IF OBJECT_ID(N'[dbo].[VMX_FE]', 'U') IS NOT NULL
    DROP TABLE [dbo].[VMX_FE];
GO
IF OBJECT_ID(N'[dbo].[CLIENTE_CONFIGURACION]', 'U') IS NOT NULL
    DROP TABLE [dbo].[CLIENTE_CONFIGURACION];
GO
IF OBJECT_ID(N'[dbo].[EMPRESA]', 'U') IS NOT NULL
    DROP TABLE [dbo].[EMPRESA];
GO
IF OBJECT_ID(N'[dbo].[FACTURA]', 'U') IS NOT NULL
    DROP TABLE [dbo].[FACTURA];
GO
IF OBJECT_ID(N'[dbo].[cxpConciliacionSet]', 'U') IS NOT NULL
    DROP TABLE [dbo].[cxpConciliacionSet];
GO
IF OBJECT_ID(N'[dbo].[CFDI_USUARIO]', 'U') IS NOT NULL
    DROP TABLE [dbo].[CFDI_USUARIO];
GO
IF OBJECT_ID(N'[dbo].[ndcFacturaLineaSet]', 'U') IS NOT NULL
    DROP TABLE [dbo].[ndcFacturaLineaSet];
GO
IF OBJECT_ID(N'[dbo].[ndcFacturaSet]', 'U') IS NOT NULL
    DROP TABLE [dbo].[ndcFacturaSet];
GO
IF OBJECT_ID(N'[dbo].[usuariosSet]', 'U') IS NOT NULL
    DROP TABLE [dbo].[usuariosSet];
GO
IF OBJECT_ID(N'[dbo].[comprobantePagoSet]', 'U') IS NOT NULL
    DROP TABLE [dbo].[comprobantePagoSet];
GO
IF OBJECT_ID(N'[dbo].[configuracionConexionSet]', 'U') IS NOT NULL
    DROP TABLE [dbo].[configuracionConexionSet];
GO
IF OBJECT_ID(N'[dbo].[configuracionProductoSet]', 'U') IS NOT NULL
    DROP TABLE [dbo].[configuracionProductoSet];
GO
IF OBJECT_ID(N'[dbo].[configuracionUMSet]', 'U') IS NOT NULL
    DROP TABLE [dbo].[configuracionUMSet];
GO
IF OBJECT_ID(N'[dbo].[configuracionUMproductoSet]', 'U') IS NOT NULL
    DROP TABLE [dbo].[configuracionUMproductoSet];
GO
IF OBJECT_ID(N'[dbo].[configuracionPartesSet]', 'U') IS NOT NULL
    DROP TABLE [dbo].[configuracionPartesSet];
GO
IF OBJECT_ID(N'[dbo].[LogFXSet]', 'U') IS NOT NULL
    DROP TABLE [dbo].[LogFXSet];
GO
IF OBJECT_ID(N'[dbo].[InformacionGlobalSet]', 'U') IS NOT NULL
    DROP TABLE [dbo].[InformacionGlobalSet];
GO

-- --------------------------------------------------
-- Creating all tables
-- --------------------------------------------------

-- Creating table 'VMX_FE'
CREATE TABLE [dbo].[VMX_FE] (
    [ROW_ID] bigint IDENTITY(1,1) NOT NULL,
    [INVOICE_ID] varchar(30)  NULL,
    [SELLO] varchar(max)  NULL,
    [PDF] varchar(250)  NULL,
    [XML] varchar(250)  NULL,
    [SERIE] varchar(250)  NULL,
    [FORMATO] varchar(250)  NULL,
    [PEDIMENTO] varchar(50)  NULL,
    [FECHA_PEDIMENTO] datetime  NULL,
    [ADUANA] varchar(50)  NULL,
    [CREATE_DATE] datetime  NULL,
    [UPDATE_DATE] datetime  NULL,
    [FORMA_DE_PAGO] nvarchar(max)  NULL,
    [METODO_DE_PAGO] nvarchar(max)  NULL,
    [CTA_BANCO] nvarchar(max)  NULL,
    [UUID] varchar(max)  NULL,
    [FechaTimbrado] varchar(max)  NULL,
    [noCertificadoSAT] varchar(max)  NULL,
    [selloSAT] varchar(max)  NULL,
    [Cadena_TFD] varchar(max)  NULL,
    [QR_Code] varchar(max)  NULL,
    [IMAGEN_QR_Code] varbinary(max)  NULL,
    [CADENA] varchar(max)  NULL,
    [ESTADO] varchar(max)  NULL,
    [CANCELADO] varchar(max)  NULL,
    [noCertificado] varchar(max)  NULL,
    [ADDR_NO] varchar(max)  NULL,
    [PEDIMENTOS] varchar(max)  NULL,
    [PDF_ESTADO] varchar(max)  NULL,
    [USO_CFDI] varchar(150)  NULL,
    [leyendaFiscal] varchar(max)  NULL,
    [VERSION] varchar(10)  NULL,
    [timbrado_prueba] varchar(50)  NOT NULL
);
GO

-- Creating table 'CLIENTE_CONFIGURACION'
CREATE TABLE [dbo].[CLIENTE_CONFIGURACION] (
    [ROW_ID] int  NOT NULL,
    [CUSTOMER_ID] nvarchar(max)  NULL,
    [ASUNTO] nvarchar(max)  NULL,
    [MENSAJE] nvarchar(max)  NULL,
    [ARCHIVO] nvarchar(max)  NULL,
    [LEYENDA_FISCAL] nvarchar(max)  NULL,
    [FORMATO] nvarchar(max)  NULL,
    [CORREO] nvarchar(max)  NULL,
    [paisEntregar] varchar(10)  NULL
);
GO

-- Creating table 'EMPRESA'
CREATE TABLE [dbo].[EMPRESA] (
    [ROW_ID] int  NOT NULL,
    [ENTITY_ID] nvarchar(255)  NULL,
    [ID] nvarchar(255)  NULL,
    [RFC] nvarchar(255)  NULL,
    [CALLE] nvarchar(255)  NULL,
    [EXTERIOR] nvarchar(255)  NULL,
    [INTERIOR] nvarchar(255)  NULL,
    [COLONIA] nvarchar(255)  NULL,
    [CP] nvarchar(255)  NULL,
    [LOCALIDAD] nvarchar(255)  NULL,
    [REFERENCIA] nvarchar(255)  NULL,
    [PAIS] nvarchar(255)  NULL,
    [ESTADO] nvarchar(255)  NULL,
    [MUNICIPIO] nvarchar(255)  NULL,
    [SMTP] nvarchar(255)  NULL,
    [USUARIO] nvarchar(255)  NULL,
    [PASSWORD_EMAIL] nvarchar(255)  NULL,
    [PUERTO] nvarchar(255)  NULL,
    [SSL] nvarchar(255)  NULL,
    [CRENDENCIALES] nvarchar(255)  NULL,
    [ACTIVO] nvarchar(max)  NULL,
    [REGIMEN_FISCAL] nvarchar(max)  NULL,
    [TIPO] nvarchar(255)  NULL,
    [SERVIDOR] nvarchar(255)  NULL,
    [BD] nvarchar(255)  NULL,
    [USUARIO_BD] nvarchar(255)  NULL,
    [PASSWORD_BD] nvarchar(255)  NULL,
    [CUSTOMER_ID] nvarchar(max)  NULL,
    [ENVIO_CORREO] nvarchar(max)  NULL,
    [SITE_ID] nvarchar(max)  NULL,
    [BD_AUXILIAR] nvarchar(max)  NULL,
    [DESCUENTO_LINEA] nvarchar(max)  NULL,
    [CUSTOMER_ESPECIFICACION_LINEA] nvarchar(max)  NULL,
    [IMPRESION_AUTOMATICA] nvarchar(max)  NULL,
    [IMPRESORA] nvarchar(max)  NULL,
    [IMPRESION_AUTOMATICA_NDC] nvarchar(max)  NULL,
    [ACTIVAR_COMPLEMENTO_CCE] nvarchar(max)  NULL,
    [APROXIMACION] nvarchar(max)  NULL,
    [VALORES_DECIMALES] nvarchar(max)  NULL,
    [relacionLinea] nvarchar(10)  NOT NULL
);
GO

-- Creating table 'FACTURA'
CREATE TABLE [dbo].[FACTURA] (
    [ROW_ID] int  NOT NULL,
    [ID] nvarchar(255)  NULL,
    [ROW_ID_EMPRESA] int  NULL,
    [FECHA_CREACION] nvarchar(255)  NULL,
    [TIPO] nvarchar(255)  NULL,
    [IMPUESTO_DATO] nvarchar(255)  NULL,
    [ROW_ID_CLIENTE] int  NULL,
    [ROW_ID_SERIE] int  NULL,
    [APROBACION] nvarchar(255)  NULL,
    [ROW_ID_FORMATO] int  NULL,
    [SUBTOTAL] decimal(15,2)  NULL,
    [IMPUESTO] decimal(15,2)  NULL,
    [RETENCION] decimal(15,2)  NULL,
    [TOTAL] decimal(15,2)  NULL,
    [ROW_ID_MONEDA] int  NULL,
    [XML] nvarchar(255)  NULL,
    [PDF] nvarchar(255)  NULL,
    [SELLO] nvarchar(max)  NULL,
    [CADENA] nvarchar(max)  NULL,
    [TIPO_CAMBIO] decimal(15,2)  NULL,
    [OBSERVACIONES] nvarchar(max)  NULL,
    [CANCELADO] nvarchar(255)  NULL,
    [USER_1] nvarchar(max)  NULL,
    [USER_2] nvarchar(max)  NULL,
    [USER_3] nvarchar(max)  NULL,
    [USER_4] nvarchar(max)  NULL,
    [USER_5] nvarchar(max)  NULL,
    [USER_6] nvarchar(max)  NULL,
    [FECHA_CANCELADO] nvarchar(max)  NULL,
    [UUID] nvarchar(max)  NULL,
    [FechaTimbrado] nvarchar(max)  NULL,
    [noCertificadoSAT] nvarchar(max)  NULL,
    [selloSAT] nvarchar(max)  NULL,
    [Cadena_TFD] nvarchar(max)  NULL,
    [QR_Code] nvarchar(max)  NULL,
    [IMAGEN] varbinary(max)  NULL,
    [METODO_PAGO] nvarchar(max)  NULL,
    [CUENTA_BANCARIA] nvarchar(max)  NULL,
    [ESTADO_PAGO] nvarchar(max)  NULL,
    [DEPOSITO] nvarchar(max)  NULL,
    [FECHA_PAGO] nvarchar(max)  NULL,
    [BANCO] nvarchar(max)  NULL,
    [FORMA_PAGO] nvarchar(max)  NULL
);
GO

-- Creating table 'cxpConciliacionSet'
CREATE TABLE [dbo].[cxpConciliacionSet] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [uuid] nvarchar(max)  NOT NULL,
    [fechaCreacion] datetime  NOT NULL,
    [oc] nvarchar(50)  NULL,
    [usuario] nvarchar(max)  NULL,
    [voucherId] nvarchar(100)  NULL,
    [rfcEmisor] nvarchar(100)  NULL,
    [tipoDocumento] nvarchar(max)  NULL
);
GO

-- Creating table 'CFDI_USUARIO'
CREATE TABLE [dbo].[CFDI_USUARIO] (
    [ROW_ID] int IDENTITY(1,1) NOT NULL,
    [USUARIO] nvarchar(max)  NULL,
    [PASSWORD] nvarchar(max)  NULL,
    [TIPO] nvarchar(max)  NULL,
    [ROW_ID_EMPRESA] float  NULL,
    [USUARIO_PRUEBA] nvarchar(max)  NULL,
    [PASSWORD_PRUEBA] nvarchar(max)  NULL
);
GO

-- Creating table 'ndcFacturaLineaSet'
CREATE TABLE [dbo].[ndcFacturaLineaSet] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [INVOICE_ID] nvarchar(50)  NULL,
    [LINE_NO] nvarchar(50)  NULL,
    [invoice_id_relacionado] nvarchar(50)  NULL,
    [invoice_id_relacionado_linea] nvarchar(50)  NULL,
    [PART_ID] nvarchar(50)  NULL,
    [clasificacion] nvarchar(50)  NULL
);
GO

-- Creating table 'ndcFacturaSet'
CREATE TABLE [dbo].[ndcFacturaSet] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [tipoRelacion] nvarchar(50)  NULL,
    [INVOICE_ID] nvarchar(50)  NULL,
    [UUID] nvarchar(100)  NULL,
    [INVOICE_ID_relacionada] nvarchar(100)  NULL
);
GO

-- Creating table 'usuariosSet'
CREATE TABLE [dbo].[usuariosSet] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [nombreCompleto] nvarchar(250)  NOT NULL,
    [usuario] nvarchar(20)  NOT NULL,
    [usuarioVISUAL] nvarchar(20)  NOT NULL,
    [password] nvarchar(20)  NOT NULL,
    [permisoDocumentosSolo] bit  NOT NULL,
    [permisoCuentas] bit  NOT NULL,
    [notificacionCorreo] bit  NOT NULL,
    [notificacionREQ] bit  NOT NULL,
    [notificacionOC] bit  NOT NULL,
    [notificacionREC] bit  NOT NULL,
    [notificacionCXP] bit  NOT NULL,
    [notificacionPAG] bit  NOT NULL,
    [notificacionBAN] bit  NOT NULL,
    [notificacionGJ] bit  NOT NULL,
    [permisoAcceso] bit  NOT NULL,
    [permisoUsuarios] bit  NOT NULL,
    [correoElectronico] nvarchar(150)  NOT NULL,
    [facturacionElectronica] bit  NOT NULL,
    [contabilidadElectronica] bit  NOT NULL,
    [cuentasCobrar] bit  NOT NULL,
    [cuentasPagar] bit  NOT NULL,
    [configuracion] bit  NOT NULL,
    [AccesoIngreso] bit  NOT NULL,
    [AccesoEgreso] bit  NOT NULL,
    [AccesoNomina] bit  NOT NULL
);
GO

-- Creating table 'comprobantePagoSet'
CREATE TABLE [dbo].[comprobantePagoSet] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [rfcEmisor] nvarchar(15)  NOT NULL,
    [rfcReceptor] nvarchar(15)  NOT NULL
);
GO

-- Creating table 'configuracionConexionSet'
CREATE TABLE [dbo].[configuracionConexionSet] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [tipo] nvarchar(100)  NOT NULL,
    [servidor] nvarchar(100)  NOT NULL,
    [baseDatos] nvarchar(100)  NOT NULL,
    [usuario] nvarchar(100)  NOT NULL,
    [password] nvarchar(100)  NOT NULL,
    [visualSite] nvarchar(100)  NULL,
    [visualEntity] nvarchar(100)  NULL,
    [visualBDAuxiliar] nvarchar(100)  NULL,
    [socioNegocio_Id] int  NOT NULL
);
GO

-- Creating table 'configuracionProductoSet'
CREATE TABLE [dbo].[configuracionProductoSet] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [fechaCreacion] datetime  NOT NULL,
    [producto] nvarchar(max)  NOT NULL,
    [clasificacionSAT] nvarchar(max)  NOT NULL,
    [unidadSAT] nvarchar(50)  NULL
);
GO

-- Creating table 'configuracionUMSet'
CREATE TABLE [dbo].[configuracionUMSet] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [UnidadMedida] nvarchar(50)  NOT NULL,
    [SAT] nvarchar(50)  NULL,
    [CE] nvarchar(50)  NULL,
    [FactorCE] decimal(16,8)  NOT NULL
);
GO

-- Creating table 'configuracionUMproductoSet'
CREATE TABLE [dbo].[configuracionUMproductoSet] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [producto] nvarchar(50)  NOT NULL,
    [UnidadMedida] nvarchar(50)  NOT NULL,
    [SAT] nvarchar(50)  NULL,
    [CE] nvarchar(50)  NULL,
    [FactorCE] decimal(16,8)  NOT NULL
);
GO

-- Creating table 'configuracionPartesSet'
CREATE TABLE [dbo].[configuracionPartesSet] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [fechaCreacion] datetime  NOT NULL,
    [producto] nvarchar(max)  NOT NULL,
    [parte] nvarchar(max)  NOT NULL,
    [NoIdentificacion] nvarchar(max)  NOT NULL,
    [Cantidad] decimal(10,2)  NOT NULL,
    [ValorUnitario] decimal(16,6)  NOT NULL,
    [Importe] decimal(10,4)  NOT NULL,
    [ClaveUnidad] nvarchar(max)  NOT NULL,
    [Unidad] nvarchar(max)  NOT NULL,
    [Descripcion] nvarchar(max)  NOT NULL,
    [ClaveProdServ] nvarchar(max)  NOT NULL
);
GO

-- Creating table 'LogFXSet'
CREATE TABLE [dbo].[LogFXSet] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [INVOICE_ID] nvarchar(50)  NULL,
    [UUID] nvarchar(100)  NULL,
    [fechaCreacion] datetime  NOT NULL,
    [Usuario] nvarchar(max)  NOT NULL,
    [Mensaje] nvarchar(max)  NOT NULL
);
GO

-- Creating table 'InformacionGlobalSet'
CREATE TABLE [dbo].[InformacionGlobalSet] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [INVOICE_ID] nvarchar(50)  NULL,
    [InformacionGlobalAnio] nvarchar(max)  NOT NULL,
    [InformacionGlobalMes] nvarchar(max)  NOT NULL,
    [InformacionGlobalPeriodicidad] nvarchar(max)  NOT NULL
);
GO

-- Creating table 'cceClienteSet'
CREATE TABLE [dbo].[cceClienteSet] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [CustomerId] nvarchar(max)  NOT NULL,
    [Exportacion] nvarchar(max)  NOT NULL
);
GO

-- --------------------------------------------------
-- Creating all PRIMARY KEY constraints
-- --------------------------------------------------

-- Creating primary key on [ROW_ID] in table 'VMX_FE'
ALTER TABLE [dbo].[VMX_FE]
ADD CONSTRAINT [PK_VMX_FE]
    PRIMARY KEY CLUSTERED ([ROW_ID] ASC);
GO

-- Creating primary key on [ROW_ID] in table 'CLIENTE_CONFIGURACION'
ALTER TABLE [dbo].[CLIENTE_CONFIGURACION]
ADD CONSTRAINT [PK_CLIENTE_CONFIGURACION]
    PRIMARY KEY CLUSTERED ([ROW_ID] ASC);
GO

-- Creating primary key on [ROW_ID], [relacionLinea] in table 'EMPRESA'
ALTER TABLE [dbo].[EMPRESA]
ADD CONSTRAINT [PK_EMPRESA]
    PRIMARY KEY CLUSTERED ([ROW_ID], [relacionLinea] ASC);
GO

-- Creating primary key on [ROW_ID] in table 'FACTURA'
ALTER TABLE [dbo].[FACTURA]
ADD CONSTRAINT [PK_FACTURA]
    PRIMARY KEY CLUSTERED ([ROW_ID] ASC);
GO

-- Creating primary key on [Id] in table 'cxpConciliacionSet'
ALTER TABLE [dbo].[cxpConciliacionSet]
ADD CONSTRAINT [PK_cxpConciliacionSet]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [ROW_ID] in table 'CFDI_USUARIO'
ALTER TABLE [dbo].[CFDI_USUARIO]
ADD CONSTRAINT [PK_CFDI_USUARIO]
    PRIMARY KEY CLUSTERED ([ROW_ID] ASC);
GO

-- Creating primary key on [Id] in table 'ndcFacturaLineaSet'
ALTER TABLE [dbo].[ndcFacturaLineaSet]
ADD CONSTRAINT [PK_ndcFacturaLineaSet]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'ndcFacturaSet'
ALTER TABLE [dbo].[ndcFacturaSet]
ADD CONSTRAINT [PK_ndcFacturaSet]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'usuariosSet'
ALTER TABLE [dbo].[usuariosSet]
ADD CONSTRAINT [PK_usuariosSet]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'comprobantePagoSet'
ALTER TABLE [dbo].[comprobantePagoSet]
ADD CONSTRAINT [PK_comprobantePagoSet]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'configuracionConexionSet'
ALTER TABLE [dbo].[configuracionConexionSet]
ADD CONSTRAINT [PK_configuracionConexionSet]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'configuracionProductoSet'
ALTER TABLE [dbo].[configuracionProductoSet]
ADD CONSTRAINT [PK_configuracionProductoSet]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'configuracionUMSet'
ALTER TABLE [dbo].[configuracionUMSet]
ADD CONSTRAINT [PK_configuracionUMSet]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'configuracionUMproductoSet'
ALTER TABLE [dbo].[configuracionUMproductoSet]
ADD CONSTRAINT [PK_configuracionUMproductoSet]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'configuracionPartesSet'
ALTER TABLE [dbo].[configuracionPartesSet]
ADD CONSTRAINT [PK_configuracionPartesSet]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'LogFXSet'
ALTER TABLE [dbo].[LogFXSet]
ADD CONSTRAINT [PK_LogFXSet]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'InformacionGlobalSet'
ALTER TABLE [dbo].[InformacionGlobalSet]
ADD CONSTRAINT [PK_InformacionGlobalSet]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'cceClienteSet'
ALTER TABLE [dbo].[cceClienteSet]
ADD CONSTRAINT [PK_cceClienteSet]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- --------------------------------------------------
-- Creating all FOREIGN KEY constraints
-- --------------------------------------------------

-- --------------------------------------------------
-- Script has ended
-- --------------------------------------------------