#region Copyright Syncfusion Inc. 2001 - 2017
// Copyright Syncfusion Inc. 2001 - 2017. All rights reserved.
// Use of this code is subject to the terms of our license.
// A copy of the current license can be obtained at any time by e-mailing
// licensing@syncfusion.com. Any infringement will be prosecuted under
// applicable laws. 
#endregion
namespace Retencion
{
    partial class frmPrincipal
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmPrincipal));
            this.dtgrdGeneral = new System.Windows.Forms.DataGridView();
            this.archivo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.folio = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.fecha = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.rfcEmisor = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Ejerc = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.MesIni = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.MesFin = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.UUID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.NumCert = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.button2 = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.label5 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.folioFiltro = new System.Windows.Forms.TextBox();
            this.fin = new System.Windows.Forms.DateTimePicker();
            this.inicio = new System.Windows.Forms.DateTimePicker();
            this.label3 = new System.Windows.Forms.Label();
            this.button3 = new System.Windows.Forms.Button();
            this.directorio = new System.Windows.Forms.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this.dtgrdGeneral)).BeginInit();
            this.SuspendLayout();
            // 
            // dtgrdGeneral
            // 
            this.dtgrdGeneral.AllowUserToAddRows = false;
            this.dtgrdGeneral.AllowUserToDeleteRows = false;
            this.dtgrdGeneral.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dtgrdGeneral.BackgroundColor = System.Drawing.SystemColors.ControlLight;
            this.dtgrdGeneral.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.dtgrdGeneral.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dtgrdGeneral.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.archivo,
            this.folio,
            this.fecha,
            this.rfcEmisor,
            this.Ejerc,
            this.MesIni,
            this.MesFin,
            this.UUID,
            this.NumCert});
            this.dtgrdGeneral.Location = new System.Drawing.Point(12, 68);
            this.dtgrdGeneral.Name = "dtgrdGeneral";
            this.dtgrdGeneral.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dtgrdGeneral.Size = new System.Drawing.Size(887, 278);
            this.dtgrdGeneral.TabIndex = 15;
            // 
            // archivo
            // 
            this.archivo.HeaderText = "Archivo";
            this.archivo.Name = "archivo";
            this.archivo.ReadOnly = true;
            this.archivo.Visible = false;
            // 
            // folio
            // 
            this.folio.HeaderText = "Folio";
            this.folio.Name = "folio";
            this.folio.ReadOnly = true;
            // 
            // fecha
            // 
            this.fecha.HeaderText = "Fecha";
            this.fecha.Name = "fecha";
            this.fecha.ReadOnly = true;
            // 
            // rfcEmisor
            // 
            this.rfcEmisor.HeaderText = "RFC Emisor";
            this.rfcEmisor.Name = "rfcEmisor";
            this.rfcEmisor.ReadOnly = true;
            // 
            // Ejerc
            // 
            this.Ejerc.HeaderText = "Ejercicio";
            this.Ejerc.Name = "Ejerc";
            this.Ejerc.ReadOnly = true;
            // 
            // MesIni
            // 
            this.MesIni.HeaderText = "Inicio";
            this.MesIni.Name = "MesIni";
            this.MesIni.ReadOnly = true;
            // 
            // MesFin
            // 
            this.MesFin.HeaderText = "Fin";
            this.MesFin.Name = "MesFin";
            this.MesFin.ReadOnly = true;
            // 
            // UUID
            // 
            this.UUID.HeaderText = "Folio Fiscal";
            this.UUID.Name = "UUID";
            this.UUID.ReadOnly = true;
            // 
            // NumCert
            // 
            this.NumCert.HeaderText = "Certificado";
            this.NumCert.Name = "NumCert";
            this.NumCert.ReadOnly = true;
            // 
            // button2
            // 
            this.button2.BackColor = System.Drawing.Color.OliveDrab;
            this.button2.FlatAppearance.BorderSize = 0;
            this.button2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button2.ForeColor = System.Drawing.Color.White;
            this.button2.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.button2.Location = new System.Drawing.Point(12, 11);
            this.button2.Margin = new System.Windows.Forms.Padding(2);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(82, 24);
            this.button2.TabIndex = 38;
            this.button2.Text = "Crear";
            this.button2.UseVisualStyleBackColor = false;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // button1
            // 
            this.button1.BackColor = System.Drawing.Color.Goldenrod;
            this.button1.FlatAppearance.BorderSize = 0;
            this.button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button1.ForeColor = System.Drawing.Color.White;
            this.button1.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.button1.Location = new System.Drawing.Point(12, 39);
            this.button1.Margin = new System.Windows.Forms.Padding(2);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(82, 24);
            this.button1.TabIndex = 35;
            this.button1.Text = "Buscar";
            this.button1.UseVisualStyleBackColor = false;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(370, 45);
            this.label5.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(56, 13);
            this.label5.TabIndex = 2;
            this.label5.Text = "Folio fiscal";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(103, 45);
            this.label2.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(32, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "Inicio";
            // 
            // folioFiltro
            // 
            this.folioFiltro.Location = new System.Drawing.Point(430, 41);
            this.folioFiltro.Margin = new System.Windows.Forms.Padding(2);
            this.folioFiltro.Name = "folioFiltro";
            this.folioFiltro.Size = new System.Drawing.Size(175, 20);
            this.folioFiltro.TabIndex = 3;
            // 
            // fin
            // 
            this.fin.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.fin.Location = new System.Drawing.Point(265, 41);
            this.fin.Margin = new System.Windows.Forms.Padding(2);
            this.fin.Name = "fin";
            this.fin.Size = new System.Drawing.Size(96, 20);
            this.fin.TabIndex = 4;
            // 
            // inicio
            // 
            this.inicio.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.inicio.Location = new System.Drawing.Point(138, 41);
            this.inicio.Margin = new System.Windows.Forms.Padding(2);
            this.inicio.Name = "inicio";
            this.inicio.Size = new System.Drawing.Size(95, 20);
            this.inicio.TabIndex = 4;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(240, 45);
            this.label3.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(21, 13);
            this.label3.TabIndex = 2;
            this.label3.Text = "Fin";
            // 
            // button3
            // 
            this.button3.BackColor = System.Drawing.Color.OliveDrab;
            this.button3.FlatAppearance.BorderSize = 0;
            this.button3.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button3.ForeColor = System.Drawing.Color.White;
            this.button3.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.button3.Location = new System.Drawing.Point(106, 11);
            this.button3.Margin = new System.Windows.Forms.Padding(2);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(127, 24);
            this.button3.TabIndex = 39;
            this.button3.Text = "Directorio de CFDI";
            this.button3.UseVisualStyleBackColor = false;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // directorio
            // 
            this.directorio.Location = new System.Drawing.Point(237, 14);
            this.directorio.Margin = new System.Windows.Forms.Padding(2);
            this.directorio.Name = "directorio";
            this.directorio.Size = new System.Drawing.Size(368, 20);
            this.directorio.TabIndex = 40;
            // 
            // frmPrincipal
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(911, 358);
            this.Controls.Add(this.directorio);
            this.Controls.Add(this.button3);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.dtgrdGeneral);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.inicio);
            this.Controls.Add(this.fin);
            this.Controls.Add(this.folioFiltro);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MetroColor = System.Drawing.Color.Transparent;
            this.Name = "frmPrincipal";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Retenciones";
            ((System.ComponentModel.ISupportInitialize)(this.dtgrdGeneral)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView dtgrdGeneral;
        private System.Windows.Forms.DataGridViewTextBoxColumn archivo;
        private System.Windows.Forms.DataGridViewTextBoxColumn folio;
        private System.Windows.Forms.DataGridViewTextBoxColumn fecha;
        private System.Windows.Forms.DataGridViewTextBoxColumn rfcEmisor;
        private System.Windows.Forms.DataGridViewTextBoxColumn Ejerc;
        private System.Windows.Forms.DataGridViewTextBoxColumn MesIni;
        private System.Windows.Forms.DataGridViewTextBoxColumn MesFin;
        private System.Windows.Forms.DataGridViewTextBoxColumn UUID;
        private System.Windows.Forms.DataGridViewTextBoxColumn NumCert;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox folioFiltro;
        private System.Windows.Forms.DateTimePicker fin;
        private System.Windows.Forms.DateTimePicker inicio;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.TextBox directorio;
    }
}