﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Retencion.clases
{
    public static class cGeneralRetencion
    {
        public static string pathCertificado = System.AppDomain.CurrentDomain.BaseDirectory + "\\certificado.fxc";
        public static string pathEmisor = System.AppDomain.CurrentDomain.BaseDirectory + "\\emisor.fxc";
        public static string pathSerie = System.AppDomain.CurrentDomain.BaseDirectory + "\\serie.fxc";
        public static string pathConfiguracion = System.AppDomain.CurrentDomain.BaseDirectory + "\\configuracion.fxc";
        public static string pathGeneral = System.AppDomain.CurrentDomain.BaseDirectory;
    }
}
