﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Retencion.clases
{
    [Serializable]
    class cEmisor
    {
        public string rfc;
        public string nombre;
        public string curp;
    }
}
