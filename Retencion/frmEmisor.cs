#region Copyright Syncfusion Inc. 2001 - 2017
// Copyright Syncfusion Inc. 2001 - 2017. All rights reserved.
// Use of this code is subject to the terms of our license.
// A copy of the current license can be obtained at any time by e-mailing
// licensing@syncfusion.com. Any infringement will be prosecuted under
// applicable laws. 
#endregion
using Retencion.clases;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Security.Cryptography;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Windows.Forms;

namespace Retencion
{
    public partial class frmEmisor : Syncfusion.Windows.Forms.MetroForm
    {
        public frmEmisor()
        {
            InitializeComponent();
            cargar();
        }

        private void btnAddItem_Click(object sender, EventArgs e)
        {
            guardar();
        }

        private void guardar()
        {
            cEmisor objeto = new cEmisor();
            objeto.nombre = nombre.Text;
            objeto.rfc = rfc.Text;
            objeto.curp = curp.Text;

            using (Stream stream = System.IO.File.Open(cGeneralRetencion.pathEmisor, FileMode.Create))
            {
                var binaryFormatter = new System.Runtime.Serialization.Formatters.Binary.BinaryFormatter();
                binaryFormatter.Serialize(stream, objeto);
            }
            DialogResult = DialogResult.OK;
        }

        private void cargar()
        {
            if (System.IO.File.Exists(cGeneralRetencion.pathEmisor))
            {
                using (Stream stream = System.IO.File.Open(cGeneralRetencion.pathEmisor, FileMode.Open))
                {
                    var binaryFormatter = new System.Runtime.Serialization.Formatters.Binary.BinaryFormatter();
                    try
                    {
                        cEmisor objeto = (cEmisor)binaryFormatter.Deserialize(stream);
                        nombre.Text = objeto.nombre;
                        rfc.Text = objeto.rfc;
                        curp.Text = objeto.curp;
                    }
                    catch
                    {

                    }
                }
            }

        }
    }
}
