#region Copyright Syncfusion Inc. 2001 - 2017
// Copyright Syncfusion Inc. 2001 - 2017. All rights reserved.
// Use of this code is subject to the terms of our license.
// A copy of the current license can be obtained at any time by e-mailing
// licensing@syncfusion.com. Any infringement will be prosecuted under
// applicable laws. 
#endregion
namespace Retencion
{
    partial class frmRetencion
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmRetencion));
            this.buttonAdv1 = new System.Windows.Forms.Button();
            this.btnAddItem = new System.Windows.Forms.Button();
            this.button15 = new System.Windows.Forms.Button();
            this.nombre = new System.Windows.Forms.TextBox();
            this.rfc = new System.Windows.Forms.TextBox();
            this.button1 = new System.Windows.Forms.Button();
            this.certificado = new System.Windows.Forms.TextBox();
            this.button2 = new System.Windows.Forms.Button();
            this.serie = new System.Windows.Forms.TextBox();
            this.emision = new System.Windows.Forms.DateTimePicker();
            this.label3 = new System.Windows.Forms.Label();
            this.button3 = new System.Windows.Forms.Button();
            this.TipoPagoRet = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.montoRet = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.BaseRet = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Impuesto = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.button4 = new System.Windows.Forms.Button();
            this.button5 = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.nacionalidad = new System.Windows.Forms.ComboBox();
            this.curpReceptor = new System.Windows.Forms.TextBox();
            this.rfcReceptor = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.nombreReceptor = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label7 = new System.Windows.Forms.Label();
            this.inicio = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.fin = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.ejercicio = new System.Windows.Forms.TextBox();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.montoTotRet = new System.Windows.Forms.TextBox();
            this.label13 = new System.Windows.Forms.Label();
            this.montoTotExent = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.montoTotGrav = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.montoTotOperacion = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.dtgTotales = new System.Windows.Forms.DataGridView();
            this.tipoImpuesto = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.baseRetenida = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.montoRetenido = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tipoPagoRetenido = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.textBox5 = new System.Windows.Forms.TextBox();
            this.label14 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.tipoRetencion = new System.Windows.Forms.ComboBox();
            this.groupBox1.SuspendLayout();
            this.groupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dtgTotales)).BeginInit();
            this.SuspendLayout();
            // 
            // buttonAdv1
            // 
            this.buttonAdv1.BackColor = System.Drawing.Color.SteelBlue;
            this.buttonAdv1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonAdv1.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonAdv1.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.buttonAdv1.Location = new System.Drawing.Point(12, 12);
            this.buttonAdv1.Name = "buttonAdv1";
            this.buttonAdv1.Size = new System.Drawing.Size(81, 28);
            this.buttonAdv1.TabIndex = 0;
            this.buttonAdv1.Text = "Nuevo";
            this.buttonAdv1.UseVisualStyleBackColor = false;
            this.buttonAdv1.Click += new System.EventHandler(this.buttonAdv1_Click);
            // 
            // btnAddItem
            // 
            this.btnAddItem.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(153)))), ((int)(((byte)(181)))), ((int)(((byte)(105)))));
            this.btnAddItem.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnAddItem.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAddItem.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnAddItem.Location = new System.Drawing.Point(99, 12);
            this.btnAddItem.Name = "btnAddItem";
            this.btnAddItem.Size = new System.Drawing.Size(80, 28);
            this.btnAddItem.TabIndex = 1;
            this.btnAddItem.Text = "Timbrar";
            this.btnAddItem.UseVisualStyleBackColor = false;
            this.btnAddItem.Click += new System.EventHandler(this.btnAddItem_Click);
            // 
            // button15
            // 
            this.button15.BackColor = System.Drawing.Color.Goldenrod;
            this.button15.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button15.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button15.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.button15.Location = new System.Drawing.Point(12, 80);
            this.button15.Name = "button15";
            this.button15.Size = new System.Drawing.Size(81, 28);
            this.button15.TabIndex = 16;
            this.button15.Text = "Emisor";
            this.button15.UseVisualStyleBackColor = false;
            this.button15.Click += new System.EventHandler(this.button15_Click);
            // 
            // nombre
            // 
            this.nombre.Location = new System.Drawing.Point(204, 85);
            this.nombre.Margin = new System.Windows.Forms.Padding(2);
            this.nombre.Name = "nombre";
            this.nombre.Size = new System.Drawing.Size(586, 20);
            this.nombre.TabIndex = 18;
            this.nombre.TextChanged += new System.EventHandler(this.nombre_TextChanged);
            // 
            // rfc
            // 
            this.rfc.Location = new System.Drawing.Point(99, 85);
            this.rfc.Margin = new System.Windows.Forms.Padding(2);
            this.rfc.Name = "rfc";
            this.rfc.Size = new System.Drawing.Size(101, 20);
            this.rfc.TabIndex = 17;
            // 
            // button1
            // 
            this.button1.BackColor = System.Drawing.Color.Goldenrod;
            this.button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button1.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button1.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.button1.Location = new System.Drawing.Point(12, 46);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(81, 28);
            this.button1.TabIndex = 4;
            this.button1.Text = "Certificado";
            this.button1.UseVisualStyleBackColor = false;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // certificado
            // 
            this.certificado.Location = new System.Drawing.Point(99, 51);
            this.certificado.Margin = new System.Windows.Forms.Padding(2);
            this.certificado.Name = "certificado";
            this.certificado.Size = new System.Drawing.Size(101, 20);
            this.certificado.TabIndex = 5;
            this.certificado.TextChanged += new System.EventHandler(this.textBox2_TextChanged);
            // 
            // button2
            // 
            this.button2.BackColor = System.Drawing.Color.Goldenrod;
            this.button2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button2.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button2.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.button2.Location = new System.Drawing.Point(205, 46);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(81, 28);
            this.button2.TabIndex = 6;
            this.button2.Text = "Serie";
            this.button2.UseVisualStyleBackColor = false;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // serie
            // 
            this.serie.Enabled = false;
            this.serie.Location = new System.Drawing.Point(293, 51);
            this.serie.Margin = new System.Windows.Forms.Padding(2);
            this.serie.Name = "serie";
            this.serie.Size = new System.Drawing.Size(110, 20);
            this.serie.TabIndex = 7;
            // 
            // emision
            // 
            this.emision.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.emision.Location = new System.Drawing.Point(459, 51);
            this.emision.Margin = new System.Windows.Forms.Padding(2);
            this.emision.Name = "emision";
            this.emision.Size = new System.Drawing.Size(96, 20);
            this.emision.TabIndex = 9;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(410, 54);
            this.label3.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(43, 13);
            this.label3.TabIndex = 8;
            this.label3.Text = "Emisi�n";
            // 
            // button3
            // 
            this.button3.BackColor = System.Drawing.Color.SteelBlue;
            this.button3.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button3.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button3.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.button3.Location = new System.Drawing.Point(12, 413);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(188, 28);
            this.button3.TabIndex = 26;
            this.button3.Text = "Agregar complemento";
            this.button3.UseVisualStyleBackColor = false;
            // 
            // TipoPagoRet
            // 
            this.TipoPagoRet.HeaderText = "Tipo de Pago Retenido";
            this.TipoPagoRet.Items.AddRange(new object[] {
            "Pago definitivo",
            "Pago provisional"});
            this.TipoPagoRet.Name = "TipoPagoRet";
            this.TipoPagoRet.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.TipoPagoRet.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.TipoPagoRet.Width = 200;
            // 
            // montoRet
            // 
            this.montoRet.HeaderText = "Monto Retenido";
            this.montoRet.Name = "montoRet";
            this.montoRet.Width = 150;
            // 
            // BaseRet
            // 
            this.BaseRet.HeaderText = "Base Ret";
            this.BaseRet.Name = "BaseRet";
            // 
            // Impuesto
            // 
            this.Impuesto.HeaderText = "Impuesto";
            this.Impuesto.Items.AddRange(new object[] {
            "01-ISR",
            "02-IVA",
            "03-IEPS"});
            this.Impuesto.Name = "Impuesto";
            this.Impuesto.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.Impuesto.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            // 
            // button4
            // 
            this.button4.BackColor = System.Drawing.Color.ForestGreen;
            this.button4.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button4.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button4.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.button4.Location = new System.Drawing.Point(12, 197);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(37, 28);
            this.button4.TabIndex = 20;
            this.button4.Text = "+";
            this.button4.UseVisualStyleBackColor = false;
            // 
            // button5
            // 
            this.button5.BackColor = System.Drawing.Color.IndianRed;
            this.button5.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button5.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button5.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.button5.Location = new System.Drawing.Point(55, 197);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(37, 28);
            this.button5.TabIndex = 21;
            this.button5.Text = "-";
            this.button5.UseVisualStyleBackColor = false;
            this.button5.Click += new System.EventHandler(this.button5_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(11, 22);
            this.label2.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(69, 13);
            this.label2.TabIndex = 0;
            this.label2.Text = "Nacionalidad";
            // 
            // nacionalidad
            // 
            this.nacionalidad.FormattingEnabled = true;
            this.nacionalidad.Items.AddRange(new object[] {
            "Nacional",
            "Extranjero"});
            this.nacionalidad.Location = new System.Drawing.Point(85, 19);
            this.nacionalidad.Name = "nacionalidad";
            this.nacionalidad.Size = new System.Drawing.Size(103, 21);
            this.nacionalidad.TabIndex = 1;
            this.nacionalidad.SelectedIndexChanged += new System.EventHandler(this.nacionalidad_SelectedIndexChanged);
            // 
            // curpReceptor
            // 
            this.curpReceptor.Location = new System.Drawing.Point(85, 46);
            this.curpReceptor.Name = "curpReceptor";
            this.curpReceptor.Size = new System.Drawing.Size(157, 20);
            this.curpReceptor.TabIndex = 7;
            // 
            // rfcReceptor
            // 
            this.rfcReceptor.Location = new System.Drawing.Point(248, 19);
            this.rfcReceptor.Name = "rfcReceptor";
            this.rfcReceptor.Size = new System.Drawing.Size(110, 20);
            this.rfcReceptor.TabIndex = 3;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(42, 49);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(37, 13);
            this.label4.TabIndex = 6;
            this.label4.Text = "CURP";
            // 
            // nombreReceptor
            // 
            this.nombreReceptor.Location = new System.Drawing.Point(414, 19);
            this.nombreReceptor.Name = "nombreReceptor";
            this.nombreReceptor.Size = new System.Drawing.Size(358, 20);
            this.nombreReceptor.TabIndex = 5;
            // 
            // label5
            // 
            this.label5.Location = new System.Drawing.Point(190, 22);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(52, 13);
            this.label5.TabIndex = 2;
            this.label5.Text = "RFC";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(364, 22);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(44, 13);
            this.label6.TabIndex = 4;
            this.label6.Text = "Nombre";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.nacionalidad);
            this.groupBox1.Controls.Add(this.curpReceptor);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.rfcReceptor);
            this.groupBox1.Controls.Add(this.nombreReceptor);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Location = new System.Drawing.Point(12, 114);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(778, 77);
            this.groupBox1.TabIndex = 19;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Receptor";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(560, 54);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(32, 13);
            this.label7.TabIndex = 10;
            this.label7.Text = "Inicio";
            // 
            // inicio
            // 
            this.inicio.Location = new System.Drawing.Point(598, 51);
            this.inicio.Name = "inicio";
            this.inicio.Size = new System.Drawing.Size(26, 20);
            this.inicio.TabIndex = 11;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(630, 54);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(21, 13);
            this.label8.TabIndex = 12;
            this.label8.Text = "Fin";
            // 
            // fin
            // 
            this.fin.Location = new System.Drawing.Point(657, 51);
            this.fin.Name = "fin";
            this.fin.Size = new System.Drawing.Size(34, 20);
            this.fin.TabIndex = 13;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(697, 54);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(47, 13);
            this.label9.TabIndex = 14;
            this.label9.Text = "Ejercicio";
            // 
            // ejercicio
            // 
            this.ejercicio.Location = new System.Drawing.Point(750, 51);
            this.ejercicio.Name = "ejercicio";
            this.ejercicio.Size = new System.Drawing.Size(40, 20);
            this.ejercicio.TabIndex = 15;
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.montoTotRet);
            this.groupBox3.Controls.Add(this.label13);
            this.groupBox3.Controls.Add(this.montoTotExent);
            this.groupBox3.Controls.Add(this.label10);
            this.groupBox3.Controls.Add(this.montoTotGrav);
            this.groupBox3.Controls.Add(this.label11);
            this.groupBox3.Controls.Add(this.montoTotOperacion);
            this.groupBox3.Controls.Add(this.label12);
            this.groupBox3.Location = new System.Drawing.Point(12, 327);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(778, 54);
            this.groupBox3.TabIndex = 23;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Totales";
            // 
            // montoTotRet
            // 
            this.montoTotRet.Location = new System.Drawing.Point(515, 20);
            this.montoTotRet.Name = "montoTotRet";
            this.montoTotRet.Size = new System.Drawing.Size(89, 20);
            this.montoTotRet.TabIndex = 7;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(459, 23);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(50, 13);
            this.label13.TabIndex = 6;
            this.label13.Text = "Retenido";
            // 
            // montoTotExent
            // 
            this.montoTotExent.Location = new System.Drawing.Point(364, 20);
            this.montoTotExent.Name = "montoTotExent";
            this.montoTotExent.Size = new System.Drawing.Size(89, 20);
            this.montoTotExent.TabIndex = 5;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(312, 23);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(46, 13);
            this.label10.TabIndex = 4;
            this.label10.Text = "Excento";
            // 
            // montoTotGrav
            // 
            this.montoTotGrav.Location = new System.Drawing.Point(217, 20);
            this.montoTotGrav.Name = "montoTotGrav";
            this.montoTotGrav.Size = new System.Drawing.Size(89, 20);
            this.montoTotGrav.TabIndex = 3;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(163, 23);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(48, 13);
            this.label11.TabIndex = 2;
            this.label11.Text = "Gravado";
            // 
            // montoTotOperacion
            // 
            this.montoTotOperacion.Location = new System.Drawing.Point(68, 20);
            this.montoTotOperacion.Name = "montoTotOperacion";
            this.montoTotOperacion.Size = new System.Drawing.Size(89, 20);
            this.montoTotOperacion.TabIndex = 1;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(6, 23);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(56, 13);
            this.label12.TabIndex = 0;
            this.label12.Text = "Operaci�n";
            // 
            // dtgTotales
            // 
            this.dtgTotales.AllowUserToAddRows = false;
            this.dtgTotales.AllowUserToDeleteRows = false;
            this.dtgTotales.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dtgTotales.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.tipoImpuesto,
            this.baseRetenida,
            this.montoRetenido,
            this.tipoPagoRetenido});
            this.dtgTotales.Location = new System.Drawing.Point(12, 231);
            this.dtgTotales.Name = "dtgTotales";
            this.dtgTotales.ReadOnly = true;
            this.dtgTotales.Size = new System.Drawing.Size(778, 90);
            this.dtgTotales.TabIndex = 22;
            // 
            // tipoImpuesto
            // 
            this.tipoImpuesto.HeaderText = "Tipo de Impuesto";
            this.tipoImpuesto.Items.AddRange(new object[] {
            "Pago definitivo",
            "Pago provisional"});
            this.tipoImpuesto.Name = "tipoImpuesto";
            this.tipoImpuesto.ReadOnly = true;
            this.tipoImpuesto.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.tipoImpuesto.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.tipoImpuesto.Width = 150;
            // 
            // baseRetenida
            // 
            this.baseRetenida.HeaderText = "Base retenida";
            this.baseRetenida.Name = "baseRetenida";
            this.baseRetenida.ReadOnly = true;
            // 
            // montoRetenido
            // 
            this.montoRetenido.HeaderText = "Monto Retenido";
            this.montoRetenido.Name = "montoRetenido";
            this.montoRetenido.ReadOnly = true;
            this.montoRetenido.Width = 120;
            // 
            // tipoPagoRetenido
            // 
            this.tipoPagoRetenido.HeaderText = "Tipo de Pago";
            this.tipoPagoRetenido.Name = "tipoPagoRetenido";
            this.tipoPagoRetenido.ReadOnly = true;
            // 
            // textBox5
            // 
            this.textBox5.Location = new System.Drawing.Point(80, 387);
            this.textBox5.Name = "textBox5";
            this.textBox5.Size = new System.Drawing.Size(542, 20);
            this.textBox5.TabIndex = 25;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(12, 390);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(63, 13);
            this.label14.TabIndex = 24;
            this.label14.Text = "Descripci�n";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(185, 20);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(28, 13);
            this.label15.TabIndex = 2;
            this.label15.Text = "Tipo";
            // 
            // tipoRetencion
            // 
            this.tipoRetencion.AutoCompleteCustomSource.AddRange(new string[] {
            "01-Servicios profesionales",
            "02-Regal�as por derechos de autor",
            "03-Autotransporte terrestre de carga",
            "04-Servicios prestados por comisionistas",
            "05-Arrendamiento",
            "06-Enajenaci�n de acciones.",
            "07-Enajenaci�n de bienes objeto de la LIEPS, a trav�s de mediadores, agentes, rep" +
                "resentantes, corredores, consignatarios o distribuidores",
            "08-Enajenaci�n de bienes inmuebles consignada en escritura p�blica",
            "09-Enajenaci�n de otros bienes, no consignada en escritura p�blica",
            "10-Adquisici�n de desperdicios industriales",
            "11-Adquisici�n de bienes consignada en escritura p�blica",
            "12-Adquisici�n de otros bienes, no consignada en escritura p�blica",
            "13-Otros retiros de AFORE.",
            "14-Dividendos o utilidades distribuidas.",
            "15-Remanente distribuible.",
            "16-Intereses.",
            "17-Arrendamiento en fideicomiso.",
            "18-Pagos realizados a favor de residentes en el extranjero.",
            "19-Enajenaci�n de acciones u operaciones en bolsa de valores.",
            "20-Obtenci�n de premios.",
            "21-Fideicomisos que no realizan actividades empresariales.",
            "22-Planes personales de retiro.",
            "23-Intereses reales deducibles por cr�ditos hipotecarios.",
            "24-Operaciones Financieras Derivadas de Capital",
            "25-Otro tipo de retenciones"});
            this.tipoRetencion.FormattingEnabled = true;
            this.tipoRetencion.Items.AddRange(new object[] {
            "01-Servicios profesionales",
            "02-Regal�as por derechos de autor",
            "03-Autotransporte terrestre de carga",
            "04-Servicios prestados por comisionistas",
            "05-Arrendamiento",
            "06-Enajenaci�n de acciones.",
            "07-Enajenaci�n de bienes objeto de la LIEPS, a trav�s de mediadores, agentes, rep" +
                "resentantes, corredores, consignatarios o distribuidores",
            "08-Enajenaci�n de bienes inmuebles consignada en escritura p�blica",
            "09-Enajenaci�n de otros bienes, no consignada en escritura p�blica",
            "10-Adquisici�n de desperdicios industriales",
            "11-Adquisici�n de bienes consignada en escritura p�blica",
            "12-Adquisici�n de otros bienes, no consignada en escritura p�blica",
            "13-Otros retiros de AFORE.",
            "14-Dividendos o utilidades distribuidas.",
            "15-Remanente distribuible.",
            "16-Intereses.",
            "17-Arrendamiento en fideicomiso.",
            "18-Pagos realizados a favor de residentes en el extranjero.",
            "19-Enajenaci�n de acciones u operaciones en bolsa de valores.",
            "20-Obtenci�n de premios.",
            "21-Fideicomisos que no realizan actividades empresariales.",
            "22-Planes personales de retiro.",
            "23-Intereses reales deducibles por cr�ditos hipotecarios.",
            "24-Operaciones Financieras Derivadas de Capital",
            "25-Otro tipo de retenciones"});
            this.tipoRetencion.Location = new System.Drawing.Point(219, 17);
            this.tipoRetencion.Name = "tipoRetencion";
            this.tipoRetencion.Size = new System.Drawing.Size(571, 21);
            this.tipoRetencion.TabIndex = 3;
            // 
            // frmRetencion
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(804, 447);
            this.Controls.Add(this.ejercicio);
            this.Controls.Add(this.tipoRetencion);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.label15);
            this.Controls.Add(this.fin);
            this.Controls.Add(this.label14);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.textBox5);
            this.Controls.Add(this.inicio);
            this.Controls.Add(this.dtgTotales);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.button5);
            this.Controls.Add(this.button4);
            this.Controls.Add(this.button3);
            this.Controls.Add(this.emision);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.serie);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.certificado);
            this.Controls.Add(this.button15);
            this.Controls.Add(this.nombre);
            this.Controls.Add(this.rfc);
            this.Controls.Add(this.buttonAdv1);
            this.Controls.Add(this.btnAddItem);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MetroColor = System.Drawing.Color.Transparent;
            this.Name = "frmRetencion";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Retenci�n";
            this.Load += new System.EventHandler(this.frmRetencion_Load);
            this.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.frmRetencion_KeyPress);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dtgTotales)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Button buttonAdv1;
        private System.Windows.Forms.Button btnAddItem;
        private System.Windows.Forms.Button button15;
        private System.Windows.Forms.TextBox nombre;
        private System.Windows.Forms.TextBox rfc;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.TextBox certificado;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.TextBox serie;
        private System.Windows.Forms.DateTimePicker emision;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.DataGridViewComboBoxColumn TipoPagoRet;
        private System.Windows.Forms.DataGridViewTextBoxColumn montoRet;
        private System.Windows.Forms.DataGridViewTextBoxColumn BaseRet;
        private System.Windows.Forms.DataGridViewComboBoxColumn Impuesto;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.Button button5;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ComboBox nacionalidad;
        private System.Windows.Forms.TextBox curpReceptor;
        private System.Windows.Forms.TextBox rfcReceptor;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox nombreReceptor;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.TextBox ejercicio;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox fin;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox inicio;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.TextBox montoTotGrav;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TextBox montoTotOperacion;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.TextBox montoTotRet;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.TextBox montoTotExent;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.DataGridView dtgTotales;
        private System.Windows.Forms.TextBox textBox5;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.DataGridViewComboBoxColumn tipoImpuesto;
        private System.Windows.Forms.DataGridViewTextBoxColumn baseRetenida;
        private System.Windows.Forms.DataGridViewTextBoxColumn montoRetenido;
        private System.Windows.Forms.DataGridViewTextBoxColumn tipoPagoRetenido;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.ComboBox tipoRetencion;
    }
}