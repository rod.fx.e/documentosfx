#region Copyright Syncfusion Inc. 2001 - 2017
// Copyright Syncfusion Inc. 2001 - 2017. All rights reserved.
// Use of this code is subject to the terms of our license.
// A copy of the current license can be obtained at any time by e-mailing
// licensing@syncfusion.com. Any infringement will be prosecuted under
// applicable laws. 
#endregion
using Retencion.clases;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Security.Cryptography;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Windows.Forms;

namespace Retencion
{
    public partial class frmCertificado : Syncfusion.Windows.Forms.MetroForm
    {
        public frmCertificado()
        {
            InitializeComponent();
            cargar();
        }



        private void button3_Click(object sender, EventArgs e)
        {
            OpenFileDialog openFileDialog1 = new OpenFileDialog();

            //            openFileDialog1.InitialDirectory = "c:\\";
            openFileDialog1.Filter = "cer files (*.cer)|*.cer|All files (*.*)|*.*";
            openFileDialog1.FilterIndex = 1;
            openFileDialog1.RestoreDirectory = true;

            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                try
                {
                    certificado.Text = openFileDialog1.FileName;
                    if (ValidaCertificado() == false)
                    {
                        certificado.Focus();
                    }
                }
                catch (Exception)
                {
                    MessageBox.Show("No existe el Archivo seleccionado", Application.ProductName + "-" + Application.ProductVersion, MessageBoxButtons.OK);
                }
            }
        }

        private bool ValidaCertificado()
        {
            string Certificate = certificado.Text.Trim();
            char[] delimiterChars = { '.' };
            string Text = certificado.Text.ToString();
            string[] TipoCer = Text.Split(delimiterChars);

            if (TipoCer[TipoCer.Length - 1].ToString() != "cer")
            {
                MessageBox.Show("El archivo para el certificado debe tener la extensi�n .cer", Application.ProductName + "-" + Application.ProductVersion, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                certificado.Text = "";
                return false;
            }
            else if (System.IO.File.Exists(certificado.Text.Trim()) == false)
            {
                MessageBox.Show("No existe el archivo:" + certificado.Text.Trim(), Application.ProductName + "-" + Application.ProductVersion, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                certificado.Text = "";
                return false;
            }
            else
            {
                try
                {
                    X509Certificate cert = X509Certificate.CreateFromCertFile(Certificate);
                    string NoSerie = cert.GetSerialNumberString();

                    string Dato = cert.GetName();

                    FECHA_DESDE.Text = cert.GetEffectiveDateString();

                    FECHA_HASTA.Text = cert.GetExpirationDateString();

                    StringBuilder SerieHex = new StringBuilder();
                    for (int i = 0; i <= NoSerie.Length - 2; i += 2)
                    {
                        SerieHex.Append(Convert.ToString(Convert.ToChar(Int32.Parse(NoSerie.Substring(i, 2), System.Globalization.NumberStyles.HexNumber))));
                    }

                    NO_CERTIFICADO.Text = SerieHex.ToString();

                    return true;
                }

                catch (CryptographicException)
                {
                    MessageBox.Show("No es un certificado valido", Application.ProductName + "-" + Application.ProductVersion, MessageBoxButtons.OK, MessageBoxIcon.Information);
                    certificado.Text = "";
                    certificado.Focus();
                    return false;
                }
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            OpenFileDialog openFileDialog1 = new OpenFileDialog();
            openFileDialog1.Filter = "key files (*.key)|*.key|All files (*.*)|*.*";
            openFileDialog1.FilterIndex = 1;
            openFileDialog1.RestoreDirectory = true;
            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                try
                {
                    llave.Text = openFileDialog1.FileName;
                }
                catch (Exception)
                {
                    MessageBox.Show("No existe el archivo seleccionado", Application.ProductName + "-" + Application.ProductVersion, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                }
            }
        }

        private void btnAddItem_Click(object sender, EventArgs e)
        {
            guardar();
        }

        private void guardar()
        {
            cCertificado objeto = new cCertificado();
            objeto.certificado = certificado.Text;
            objeto.llave = llave.Text;
            objeto.password = password.Text;

            using (Stream stream = System.IO.File.Open(cGeneralRetencion.pathCertificado, FileMode.Create))
            {
                var binaryFormatter = new System.Runtime.Serialization.Formatters.Binary.BinaryFormatter();
                binaryFormatter.Serialize(stream, objeto);
            }
            DialogResult = DialogResult.OK;
        }

        private void cargar()
        {
            if (System.IO.File.Exists(cGeneralRetencion.pathCertificado))
            {
                using (Stream stream = System.IO.File.Open(cGeneralRetencion.pathCertificado, FileMode.Open))
                {
                    var binaryFormatter = new System.Runtime.Serialization.Formatters.Binary.BinaryFormatter();
                    try
                    {
                        cCertificado objeto = (cCertificado)binaryFormatter.Deserialize(stream);
                        certificado.Text = objeto.certificado;
                        password.Text = objeto.password;
                        llave.Text = objeto.llave;
                        ValidaCertificado();
                    }
                    catch
                    {

                    }
                }
            }

        }
    }
}
