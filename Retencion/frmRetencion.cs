#region Copyright Syncfusion Inc. 2001 - 2017
// Copyright Syncfusion Inc. 2001 - 2017. All rights reserved.
// Use of this code is subject to the terms of our license.
// A copy of the current license can be obtained at any time by e-mailing
// licensing@syncfusion.com. Any infringement will be prosecuted under
// applicable laws. 
#endregion
using FE_FX;
using Generales;
using Retencion.clases;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Globalization;
using System.IO;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Windows.Forms;

namespace Retencion
{
    public partial class frmRetencion : Syncfusion.Windows.Forms.MetroForm
    {
        bool modificado = false;
        Retenciones oRetenciones = new Retenciones();
        public frmRetencion()
        {
            InitializeComponent();
            limpiar();
            
        }

        private void cargarCertificado()
        {
            
            if (System.IO.File.Exists(cGeneralRetencion.pathCertificado))
            {
                using (Stream stream = System.IO.File.Open(cGeneralRetencion.pathCertificado, FileMode.Open))
                {
                    var binaryFormatter = new System.Runtime.Serialization.Formatters.Binary.BinaryFormatter();
                    try
                    {
                        cCertificado objeto = (cCertificado)binaryFormatter.Deserialize(stream);
                        certificado.Text = objeto.certificado;
                    }
                    catch(Exception error)
                    {
                        ErrorFX.mostrar(error, true, true, "frmRetencion - 48 ", true);
                    }
                }
            }

        }
        private void cargarSerie()
        {

            if (System.IO.File.Exists(cGeneralRetencion.pathSerie))
            {
                using (Stream stream = System.IO.File.Open(cGeneralRetencion.pathSerie, FileMode.Open))
                {
                    var binaryFormatter = new System.Runtime.Serialization.Formatters.Binary.BinaryFormatter();
                    try
                    {
                        cSerie objeto = (cSerie)binaryFormatter.Deserialize(stream);
                        serie.Text = objeto.serie+""+objeto.consecutivo;
                    }
                    catch (Exception error)
                    {
                        ErrorFX.mostrar(error, true, true, "frmRetencion - 48 ", true);
                    }
                }
            }

        }

        private void cargarEmisor()
        {

            if (System.IO.File.Exists(cGeneralRetencion.pathEmisor))
            {
                using (Stream stream = System.IO.File.Open(cGeneralRetencion.pathEmisor, FileMode.Open))
                {
                    var binaryFormatter = new System.Runtime.Serialization.Formatters.Binary.BinaryFormatter();
                    try
                    {
                        cEmisor objeto = (cEmisor)binaryFormatter.Deserialize(stream);
                        nombre.Text = objeto.nombre;
                        rfc.Text = objeto.rfc;
                        //curp.Text = objeto.curp;
                    }
                    catch (Exception error)
                    {
                        ErrorFX.mostrar(error, true, true, "frmRetencion - 72 ", true);
                    }
                }
            }

        }

        private void frmRetencion_Load(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            frmCertificado o = new frmCertificado();
            o.ShowDialog();
            cargarCertificado();
        }

        private void textBox2_TextChanged(object sender, EventArgs e)
        {

        }

        private void buttonAdv1_Click(object sender, EventArgs e)
        {
            limpiar();
        }

        private void limpiar()
        {
            if (modificado)
            {

                DialogResult Resultado = MessageBox.Show(this, "�Desea eliminar los cambios?", Application.ProductName + "-" + Application.ProductVersion, MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                if (Resultado== DialogResult.No)
                {
                    return;
                }
            }

            foreach (var c in this.Controls)
            {
                if (c is TextBox)
                {
                    ((TextBox)c).Text = "";
                }
            }
            cargarCertificado();
            cargarEmisor();
            cargarSerie();
            inicio.Text = DateTime.Now.Month.ToString();
            fin.Text = DateTime.Now.Month.ToString();
            ejercicio.Text= DateTime.Now.Year.ToString();
            nacionalidad.SelectedIndex = 0;
            calcular();
        }

        private void frmRetencion_KeyPress(object sender, KeyPressEventArgs e)
        {
            modificado = true;
        }

        private void btnAddItem_Click(object sender, EventArgs e)
        {
            guardar();
        }

        private void guardar()
        {
            //if (generar())
            //{
            //    if (timbrar())
            //    {
            //        if (ESTADO.Text != "BORRADOR")
            //        {
            //            ocSERIE.incrementar(ocSERIE.ROW_ID);
            //            cargarTimbre(PATH.Text);
            //        }
            //        MessageBox.Show("Retenci�n realizada correctamente.", Application.ProductName + "-" + Application.ProductVersion, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            //    }
            //}
        }

        //public bool generar()
        //{
        //    try
        //    {
        //        string sDirectory = AppDomain.CurrentDomain.BaseDirectory;

        //        if (String.IsNullOrEmpty(rfc.Text))
        //        {
        //            cargarEmisor();
        //        }

        //        //MessageBox.Show("XLT");
        //        if (!File.Exists(sDirectory + "/XSLT/retenciones.xslt"))
        //        {
        //            //Descargar archivo XSLT del SAT 
        //            cargarXSLT();
        //        }

        //        if (!File.Exists(ocCERTIFICADO.CERTIFICADO))
        //        {
        //            MessageBox.Show("No se tiene acceso al archivo " + ocCERTIFICADO.CERTIFICADO + "", Application.ProductName + "-" + Application.ProductVersion, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
        //            return false;
        //        }
        //        X509Certificate cert = X509Certificate.CreateFromCertFile(ocCERTIFICADO.CERTIFICADO);
        //        string NoSerie = cert.GetSerialNumberString();
        //        StringBuilder SerieHex = new StringBuilder();
        //        for (int i = 0; i <= NoSerie.Length - 2; i += 2)
        //        {
        //            SerieHex.Append(Convert.ToString(Convert.ToChar(Int32.Parse(NoSerie.Substring(i, 2), System.Globalization.NumberStyles.HexNumber))));
        //        }
        //        //MessageBox.Show("Comprobante");
        //        string NO_CERTIFICADO = SerieHex.ToString();

        //        DateTime INVOICE_DATE = DateTime.Now;
        //        IFormatProvider culture = new CultureInfo("es-MX", true);
        //        DateTime CREATE_DATE = DateTime.Now;

        //        INVOICE_DATE = DateTime.Parse(INVOICE_DATE.Day.ToString() + "/" + INVOICE_DATE.Month.ToString() + "/" + INVOICE_DATE.Year.ToString() + " " + CREATE_DATE.ToString("HH:mm:ss"), culture);


        //        oRetenciones = new Retenciones();
        //        cert = X509Certificate.CreateFromCertFile(ocCERTIFICADO.CERTIFICADO);
        //        string Certificado64 = System.Convert.ToBase64String(cert.GetRawCertData());
        //        oRetenciones.Cert = Certificado64;


        //        var date = DateTime.Now;

        //        date = new DateTime(date.Year, date.Month, date.Day, date.Hour, date.Minute, date.Second, date.Kind);


        //        oRetenciones.FechaExp = date;
        //        oRetenciones.DescRetenc = DescRetenc.Text;
        //        oRetenciones.CveRetenc = c_Retenciones.Item18;

        //        oRetenciones.Periodo = new RetencionesPeriodo();

        //        oRetenciones.Periodo.MesFin = int.Parse(fin.Text);
        //        oRetenciones.Periodo.MesIni = int.Parse(inicio.Text);
        //        oRetenciones.Periodo.Ejerc = int.Parse(ejercicio.Text);

        //        oRetenciones.NumCert = ocCERTIFICADO.NO_CERTIFICADO;
        //        oRetenciones.FolioInt = SERIE.Text + ID.Text;
        //        oRetenciones.Totales = new RetencionesTotales();
        //        oRetenciones.Totales.montoTotOperacion = decimal.Parse(montoTotOperacion.Text);

        //        //Emisor
        //        oRetenciones.Emisor = new RetencionesEmisor();
        //        oRetenciones.Emisor.RFCEmisor = ocEMPRESA.RFC;
        //        oRetenciones.Emisor.NomDenRazSocE = ocEMPRESA.ID;
        //        //oRetenciones.Emisor.CURPE = CURPE.Text;

        //        //Receptor
        //        oRetenciones.Receptor = new RetencionesReceptor();
        //        oRetenciones.Receptor.Nacionalidad = new RetencionesReceptorNacionalidad();
        //        oRetenciones.Receptor.Nacionalidad = RetencionesReceptorNacionalidad.Extranjero;
        //        if (oRetenciones.Receptor.Nacionalidad == RetencionesReceptorNacionalidad.Extranjero)
        //        {
        //            //<retenciones:Extranjero NomDenRazSocR="WALLACE &amp; SMITH CO. LTD" NumRegIdTrib="XEXX010101000"/>
        //            RetencionesReceptorExtranjero oRetencionesReceptorExtranjero = new RetencionesReceptorExtranjero();
        //            oRetencionesReceptorExtranjero.NomDenRazSocR = receptortxt.Text;
        //            oRetencionesReceptorExtranjero.NumRegIdTrib = rfc.Text;

        //            oRetenciones.Receptor.Item = new RetencionesReceptorExtranjero();
        //            oRetenciones.Receptor.Item = oRetencionesReceptorExtranjero;
        //        }


        //        //Totales
        //        oRetenciones.Totales = new RetencionesTotales();
        //        oRetenciones.Totales.montoTotRet = decimal.Parse(montoTotRet.Text);
        //        oRetenciones.Totales.montoTotOperacion = decimal.Parse(montoTotOperacion.Text);
        //        oRetenciones.Totales.montoTotGrav = decimal.Parse(montoTotGrav.Text);
        //        oRetenciones.Totales.montoTotExent = decimal.Parse(montoTotExent.Text);

        //        //Agregar lineas de impuestos
        //        oRetenciones.Totales = new RetencionesTotales();

        //        RetencionesTotalesImpRetenidos[] oRetencionesTotalesImpRetenidos;
        //        oRetencionesTotalesImpRetenidos = new RetencionesTotalesImpRetenidos[this.dtgTotales.Rows.Count];
        //        int contadorRegistro = 0;

        //        foreach (DataGridViewRow registro in this.dtgTotales.Rows)
        //        {
        //            RetencionesTotalesImpRetenidos oLinea = new RetencionesTotalesImpRetenidos();

        //            oLinea.BaseRetSpecified = true;
        //            oLinea.BaseRet = decimal.Parse(registro.Cells["BaseRet"].Value.ToString());
        //            oLinea.montoRet = decimal.Parse(registro.Cells["montoRet"].Value.ToString());
        //            oLinea.ImpuestoSpecified = true;
        //            oLinea.TipoPagoRet = RetencionesTotalesImpRetenidosTipoPagoRet.Pagodefinitivo;
        //            if (registro.Cells["TipoPagoRet"].Value.ToString().Contains("Pago provisional"))
        //            {
        //                oLinea.TipoPagoRet = RetencionesTotalesImpRetenidosTipoPagoRet.Pagoprovisional;
        //            }
        //            oLinea.Impuesto = RetencionesTotalesImpRetenidosImpuesto.Item01;
        //            if (registro.Cells["Impuesto"].Value.ToString().Contains("02"))
        //            {
        //                oLinea.Impuesto = RetencionesTotalesImpRetenidosImpuesto.Item02;
        //            }
        //            if (registro.Cells["Impuesto"].Value.ToString().Contains("03"))
        //            {
        //                oLinea.Impuesto = RetencionesTotalesImpRetenidosImpuesto.Item03;
        //            }
        //            oRetencionesTotalesImpRetenidos[contadorRegistro] = new RetencionesTotalesImpRetenidos();
        //            oRetencionesTotalesImpRetenidos[contadorRegistro] = oLinea;
        //            contadorRegistro++;
        //        }
        //        oRetenciones.Totales = new RetencionesTotales();
        //        oRetenciones.Totales.ImpRetenidos = new RetencionesTotalesImpRetenidos[this.dtgTotales.Rows.Count];
        //        oRetenciones.Totales.ImpRetenidos = oRetencionesTotalesImpRetenidos;
        //        oRetenciones.Totales.montoTotExent = decimal.Parse(montoTotExent.Text);
        //        oRetenciones.Totales.montoTotGrav = decimal.Parse(montoTotGrav.Text);
        //        oRetenciones.Totales.montoTotOperacion = decimal.Parse(montoTotOperacion.Text);
        //        oRetenciones.Totales.montoTotRet = decimal.Parse(montoTotRet.Text);

                
        //        string DIRECTORIO_ARCHIVOS = "";
        //        DIRECTORIO_ARCHIVOS += ocSERIE.DIRECTORIO + @"\" + ocSERIE.ID + @"\" + ocGeneracion.Fechammmyy(oRetenciones.FechaExp.ToString());
        //        string XML = DIRECTORIO_ARCHIVOS;

        //        string PDF = XML + @"\" + "pdf";
        //        if (!Directory.Exists(PDF))
        //        {
        //            try
        //            {

        //                Directory.CreateDirectory(PDF);
        //            }
        //            catch (IOException excep)
        //            {
        //                MessageBox.Show(excep.Message.ToString() + Environment.NewLine + "Creando directorio de PDF"
        //                    , Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error);
        //                return false;
        //            }
        //        }

        //        XML += @"\" + "xml";
        //        if (Directory.Exists(XML) == false)
        //            Directory.CreateDirectory(XML);

        //        string XML_Archivo = "";

        //        string nombre_archivo = ocSERIE.ID + INVOICE_ID;

        //        if (Directory.Exists(DIRECTORIO_ARCHIVOS))
        //        {
        //            XML_Archivo = XML + @"\" + nombre_archivo + ".xml";
        //        }
        //        else
        //        {
        //            XML_Archivo = @"\" + nombre_archivo + ".xml"; ;
        //        }

        //        XML_Archivo = XML + @"\" + nombre_archivo + ".xml";

        //        generarArchivoXML(XML_Archivo);
                
        //        //Generar Cadena
        //        string cadena = cadenaRetencion(ARCHIVO);
        //        oRetenciones.Sello = selloRSA(ocCERTIFICADO.LLAVE, ocCERTIFICADO.PASSWORD, cadena, decimal.Parse(ejercicio.Text));
        //        generarArchivoXML(XML_Archivo);

        //        return true;
        //    }
        //    catch (Exception errores)
        //    {
        //        MessageBox.Show("Error en generaci�n. " + errores.Message.ToString() + Environment.NewLine + errores.InnerException.Message.ToString());
        //        return false;
        //    }
        //}

        private void nombre_TextChanged(object sender, EventArgs e)
        {

        }

        private void button15_Click(object sender, EventArgs e)
        {
            frmEmisor o = new frmEmisor();
            o.ShowDialog();
            cargarEmisor();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            frmSerie o = new frmSerie();
            o.ShowDialog();
            cargarSerie();
        }

        private void button5_Click(object sender, EventArgs e)
        {
            eliminarLinea();
        }

        private void calcular()
        {
            double montoRet_tr = 0;
            double BaseRet_tr = 0;

            for (int i = 0; i < dtgTotales.Rows.Count; i++)
            {
                if (Globales.IsNumeric(dtgTotales.Rows[i].Cells["montoRet"].Value.ToString()))
                {
                    montoRet_tr += double.Parse(dtgTotales.Rows[i].Cells["montoRet"].Value.ToString().Replace("$", ""));
                }
                if (Globales.IsNumeric(dtgTotales.Rows[i].Cells["BaseRet"].Value.ToString()))
                {
                    BaseRet_tr += double.Parse(dtgTotales.Rows[i].Cells["BaseRet"].Value.ToString().Replace("$", ""));

                }

            }

            montoTotOperacion.Text = BaseRet_tr.ToString();
            montoTotGrav.Text = BaseRet_tr.ToString();
            montoTotRet.Text = montoRet_tr.ToString();
        }

        private void eliminarLinea()
        {

            dtgTotales.EndEdit();
            if (dtgTotales.SelectedRows.Count > 0)
            {
                DialogResult Resultado = MessageBox.Show("�Esta seguro de eliminar las l�neas seleccionadas?", Application.ProductName + "-" + Application.ProductVersion, MessageBoxButtons.YesNo);
                if (Resultado.ToString() != "Yes")
                {
                    return;
                }
                foreach (DataGridViewRow drv in dtgTotales.SelectedRows)
                {
                    dtgTotales.Rows.Remove(drv);
                }
                calcular();
            }
        }

        private void nacionalidad_SelectedIndexChanged(object sender, EventArgs e)
        {
            label4.Visible = true;
            label4.Text = "CURP";
            curpReceptor.Visible = true;
            label5.Text = "RFC";
            if (nacionalidad.Text.Equals("Extranjero"))
            {
                label4.Visible = curpReceptor.Visible = false;
                label5.Text = "NumReg";
            }
        }
    }
}
