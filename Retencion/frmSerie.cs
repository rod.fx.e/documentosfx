#region Copyright Syncfusion Inc. 2001 - 2017
// Copyright Syncfusion Inc. 2001 - 2017. All rights reserved.
// Use of this code is subject to the terms of our license.
// A copy of the current license can be obtained at any time by e-mailing
// licensing@syncfusion.com. Any infringement will be prosecuted under
// applicable laws. 
#endregion
using Retencion.clases;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Security.Cryptography;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Windows.Forms;

namespace Retencion
{
    public partial class frmSerie : Syncfusion.Windows.Forms.MetroForm
    {
        public frmSerie()
        {
            InitializeComponent();
            cargar();
        }

        private void btnAddItem_Click(object sender, EventArgs e)
        {
            guardar();
        }

        private void guardar()
        {
            cSerie objeto = new cSerie();
            objeto.serie = serie.Text;
            objeto.consecutivo = consecutivo.Text;

            using (Stream stream = System.IO.File.Open(cGeneralRetencion.pathSerie, FileMode.Create))
            {
                var binaryFormatter = new System.Runtime.Serialization.Formatters.Binary.BinaryFormatter();
                binaryFormatter.Serialize(stream, objeto);
            }
            DialogResult = DialogResult.OK;
        }

        private void cargar()
        {
            if (System.IO.File.Exists(cGeneralRetencion.pathSerie))
            {
                using (Stream stream = System.IO.File.Open(cGeneralRetencion.pathSerie, FileMode.Open))
                {
                    var binaryFormatter = new System.Runtime.Serialization.Formatters.Binary.BinaryFormatter();
                    try
                    {
                        cEmisor objeto = (cEmisor)binaryFormatter.Deserialize(stream);
                        serie.Text = objeto.nombre;
                        consecutivo.Text = objeto.rfc;
                    }
                    catch
                    {

                    }
                }
            }

        }
    }
}
