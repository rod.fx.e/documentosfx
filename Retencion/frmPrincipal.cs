#region Copyright Syncfusion Inc. 2001 - 2017
// Copyright Syncfusion Inc. 2001 - 2017. All rights reserved.
// Use of this code is subject to the terms of our license.
// A copy of the current license can be obtained at any time by e-mailing
// licensing@syncfusion.com. Any infringement will be prosecuted under
// applicable laws. 
#endregion
using FE_FX;
using FE_FX.CFDI;
using Generales;
using Retencion.clases;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Text;
using System.Windows.Forms;
using System.Xml;
using System.Xml.Serialization;

namespace Retencion
{
    public partial class frmPrincipal : Syncfusion.Windows.Forms.MetroForm
    {
        public frmPrincipal()
        {
            InitializeComponent();
            cargarConfiguracion();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            cargarDirectorio();
        }

        private void cargarDirectorio()
        {
            FolderBrowserDialog oFolderBrowserDialog = new FolderBrowserDialog();
            if (oFolderBrowserDialog.ShowDialog() == DialogResult.OK)
            {
                directorio.Text = oFolderBrowserDialog.SelectedPath;
                guardarConfiguracion();
                cargar_archivos();
            }
        }
        private void guardarConfiguracion()
        {
            clases.cConfiguracion objeto = new clases.cConfiguracion();
            objeto.directorio = directorio.Text;
            using (Stream stream = System.IO.File.Open(cGeneralRetencion.pathConfiguracion, FileMode.Create))
            {
                var binaryFormatter = new System.Runtime.Serialization.Formatters.Binary.BinaryFormatter();
                binaryFormatter.Serialize(stream, objeto);
            }
            DialogResult = DialogResult.OK;
        }
        private void cargarConfiguracion()
        {

            if (System.IO.File.Exists(cGeneralRetencion.pathConfiguracion))
            {
                using (Stream stream = System.IO.File.Open(cGeneralRetencion.pathConfiguracion, FileMode.Open))
                {
                    var binaryFormatter = new System.Runtime.Serialization.Formatters.Binary.BinaryFormatter();
                    try
                    {
                        clases.cConfiguracion objeto = (clases.cConfiguracion)binaryFormatter.Deserialize(stream);
                        directorio.Text = objeto.directorio;
                    }
                    catch (Exception error)
                    {
                        ErrorFX.mostrar(error, true, true, "frmPrincipal - 74 ", true);
                    }
                }
            }

        }

        private void cargar_archivos()
        {
            dtgrdGeneral.Rows.Clear();
            if (!Directory.Exists(directorio.Text))
            {
                return;
            }
            DataSetRetencion.dtRetencionDataTable tabla = new DataSetRetencion.dtRetencionDataTable();
            //Cargar archivos
            foreach (string archivo in Directory.GetFiles(directorio.Text, "*.xml", SearchOption.AllDirectories))
            {
                try
                {
                    XmlSerializer serializer_CFD_3 = new XmlSerializer(typeof(Retenciones));
                    TextReader reader = new StreamReader(archivo);
                    Retenciones oComprobante;
                    oComprobante = (Retenciones)serializer_CFD_3.Deserialize(reader);

                    XmlDocument docXML = new XmlDocument();
                    docXML.Load(archivo);
                    XmlNodeList TimbreFiscalDigital = docXML.GetElementsByTagName("tfd:TimbreFiscalDigital");
                    string TimbreFiscalDigitaltr = "";
                    if (TimbreFiscalDigital != null)
                    {
                        if (TimbreFiscalDigital[0] != null)
                        {
                            TimbreFiscalDigitaltr = TimbreFiscalDigital[0].Attributes["UUID"].Value;
                        }
                    }

                    tabla.AdddtRetencionRow(
                        archivo
                        , oComprobante.FolioInt
                        , oComprobante.FechaExp
                        , oComprobante.Emisor.RFCEmisor
                        , oComprobante.Periodo.MesIni.ToString()
                        , oComprobante.Periodo.MesFin.ToString()
                        , oComprobante.Periodo.Ejerc.ToString()
                        , oComprobante.NumCert
                        , TimbreFiscalDigitaltr);

                }
                catch
                {

                }
            }

            //Cargar tabla
            var dataQuery =
                        from tr in tabla
                        where tr.fecha >= inicio.Value & tr.fecha <= fin.Value
                        select tr;
            foreach (DataSetRetencion.dtRetencionRow r in dataQuery)
            {
                int n = dtgrdGeneral.Rows.Add();
                dtgrdGeneral.Rows[n].Cells["archivo"].Value = r.archivo;
                dtgrdGeneral.Rows[n].Cells["folio"].Value = r.folio;
                dtgrdGeneral.Rows[n].Cells["fecha"].Value = r.fecha;
                dtgrdGeneral.Rows[n].Cells["rfcEmisor"].Value = r.rfcEmisor;
                dtgrdGeneral.Rows[n].Cells["MesIni"].Value = r.MesIni;
                dtgrdGeneral.Rows[n].Cells["MesFin"].Value = r.MesFin;
                dtgrdGeneral.Rows[n].Cells["Ejerc"].Value = r.Ejerc;
                dtgrdGeneral.Rows[n].Cells["NumCert"].Value = r.NumCert;
                dtgrdGeneral.Rows[n].Cells["UUID"].Value = r.UUID;
            }

            dtgrdGeneral.AutoResizeColumns(DataGridViewAutoSizeColumnsMode.AllCells);
        }

        private void button2_Click(object sender, EventArgs e)
        {
            frmRetencion o = new frmRetencion();
            o.ShowDialog();
        }
    }
}
