#region Copyright Syncfusion Inc. 2001 - 2017
// Copyright Syncfusion Inc. 2001 - 2017. All rights reserved.
// Use of this code is subject to the terms of our license.
// A copy of the current license can be obtained at any time by e-mailing
// licensing@syncfusion.com. Any infringement will be prosecuted under
// applicable laws. 
#endregion
namespace Retencion
{
    partial class frmCertificado
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmCertificado));
            this.password = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.llave = new System.Windows.Forms.TextBox();
            this.FECHA_HASTA = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.FECHA_DESDE = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.NO_CERTIFICADO = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.certificado = new System.Windows.Forms.TextBox();
            this.button3 = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.btnAddItem = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // password
            // 
            this.password.Location = new System.Drawing.Point(107, 126);
            this.password.Name = "password";
            this.password.PasswordChar = '*';
            this.password.Size = new System.Drawing.Size(185, 20);
            this.password.TabIndex = 30;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(29, 129);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(73, 13);
            this.label5.TabIndex = 29;
            this.label5.Text = "Clave Privada";
            // 
            // llave
            // 
            this.llave.Enabled = false;
            this.llave.Location = new System.Drawing.Point(107, 100);
            this.llave.Name = "llave";
            this.llave.Size = new System.Drawing.Size(662, 20);
            this.llave.TabIndex = 28;
            // 
            // FECHA_HASTA
            // 
            this.FECHA_HASTA.Enabled = false;
            this.FECHA_HASTA.Location = new System.Drawing.Point(584, 74);
            this.FECHA_HASTA.Name = "FECHA_HASTA";
            this.FECHA_HASTA.Size = new System.Drawing.Size(185, 20);
            this.FECHA_HASTA.TabIndex = 26;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(543, 77);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(35, 13);
            this.label4.TabIndex = 25;
            this.label4.Text = "Hasta";
            // 
            // FECHA_DESDE
            // 
            this.FECHA_DESDE.Enabled = false;
            this.FECHA_DESDE.Location = new System.Drawing.Point(347, 74);
            this.FECHA_DESDE.Name = "FECHA_DESDE";
            this.FECHA_DESDE.Size = new System.Drawing.Size(185, 20);
            this.FECHA_DESDE.TabIndex = 24;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(303, 77);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(38, 13);
            this.label3.TabIndex = 23;
            this.label3.Text = "Desde";
            // 
            // NO_CERTIFICADO
            // 
            this.NO_CERTIFICADO.Enabled = false;
            this.NO_CERTIFICADO.Location = new System.Drawing.Point(107, 74);
            this.NO_CERTIFICADO.Name = "NO_CERTIFICADO";
            this.NO_CERTIFICADO.Size = new System.Drawing.Size(185, 20);
            this.NO_CERTIFICADO.TabIndex = 22;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(77, 77);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(24, 13);
            this.label2.TabIndex = 21;
            this.label2.Text = "No.";
            // 
            // certificado
            // 
            this.certificado.Enabled = false;
            this.certificado.Location = new System.Drawing.Point(107, 48);
            this.certificado.Name = "certificado";
            this.certificado.Size = new System.Drawing.Size(662, 20);
            this.certificado.TabIndex = 20;
            // 
            // button3
            // 
            this.button3.BackColor = System.Drawing.Color.OliveDrab;
            this.button3.FlatAppearance.BorderSize = 0;
            this.button3.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button3.ForeColor = System.Drawing.Color.White;
            this.button3.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.button3.Location = new System.Drawing.Point(11, 45);
            this.button3.Margin = new System.Windows.Forms.Padding(2);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(91, 24);
            this.button3.TabIndex = 40;
            this.button3.Text = "Certificado";
            this.button3.UseVisualStyleBackColor = false;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // button1
            // 
            this.button1.BackColor = System.Drawing.Color.OliveDrab;
            this.button1.FlatAppearance.BorderSize = 0;
            this.button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button1.ForeColor = System.Drawing.Color.White;
            this.button1.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.button1.Location = new System.Drawing.Point(10, 97);
            this.button1.Margin = new System.Windows.Forms.Padding(2);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(92, 24);
            this.button1.TabIndex = 41;
            this.button1.Text = "Llave";
            this.button1.UseVisualStyleBackColor = false;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // btnAddItem
            // 
            this.btnAddItem.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(153)))), ((int)(((byte)(181)))), ((int)(((byte)(105)))));
            this.btnAddItem.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnAddItem.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAddItem.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnAddItem.Location = new System.Drawing.Point(11, 12);
            this.btnAddItem.Name = "btnAddItem";
            this.btnAddItem.Size = new System.Drawing.Size(91, 28);
            this.btnAddItem.TabIndex = 42;
            this.btnAddItem.Text = "Guardar";
            this.btnAddItem.UseVisualStyleBackColor = false;
            this.btnAddItem.Click += new System.EventHandler(this.btnAddItem_Click);
            // 
            // frmCertificado
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(784, 159);
            this.Controls.Add(this.btnAddItem);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.button3);
            this.Controls.Add(this.password);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.llave);
            this.Controls.Add(this.FECHA_HASTA);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.FECHA_DESDE);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.NO_CERTIFICADO);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.certificado);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MetroColor = System.Drawing.Color.Transparent;
            this.Name = "frmCertificado";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Certificado";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox password;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox llave;
        private System.Windows.Forms.TextBox FECHA_HASTA;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox FECHA_DESDE;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox NO_CERTIFICADO;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox certificado;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button btnAddItem;
    }
}