﻿using Newtonsoft.Json;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net.Http;
using Generales;

namespace TipoCambio.Clases
{
    public class Banxico
    {
 
        #region /* Constantes */
        const string BANXICO_MI_TOKEN = "0e7ec00053526fd20e42eed08bc390d5a0fad89671a2fe03267902351bacc84d";
        const string BANXICO_URL = "https://www.banxico.org.mx/SieAPIRest/service/v1/series/{0}/datos/{1}/{1}";
        const string BANXICO_FORMATO_FECHA = "yyyy-MM-dd";
        const string BANXICO_HEADER_ITEMTOKEN = "Bmx-Token";
        const string BANXICO_HEADER_FORMATACCEPTED = "application/json";
        const string BANXICO_SERIE_TIPOCAMBIOFIX = "SF60653"; //DOF
        //"SF43718";// FIX FIX
        #endregion

        public decimal TipoCambio(DateTime fecha)
        {
            try
            {
                string _fmtFecha = fecha.ToString(BANXICO_FORMATO_FECHA);
                string _url = string.Format(BANXICO_URL, BANXICO_SERIE_TIPOCAMBIOFIX, _fmtFecha);

                var client = new RestClient(_url);
                var request = new RestRequest(_url, Method.Get);
                request.AddHeader("Accept", BANXICO_HEADER_FORMATACCEPTED);
                request.AddHeader(BANXICO_HEADER_ITEMTOKEN, BANXICO_MI_TOKEN);
                RestResponse response = client.Execute(request);
                Console.WriteLine(response.Content);
                BitacoraFX.Log(response.Content);
                Root myDeserializedClass = JsonConvert.DeserializeObject<Root>(response.Content);
                if (myDeserializedClass != null)
                {
                    if (myDeserializedClass.bmx.series.Count > 0)
                    {
                        if (myDeserializedClass.bmx.series[0].datos == null)
                        {
                            EmailService.Send("Documentos FX: Error - Banxico - 50 - Sin Datos: Fecha: " + fecha.ToString("yyyy-MM-dd"));
                            return 0;
                        }
                        if (myDeserializedClass.bmx.series[0].datos.Count > 0)
                        {
                            return decimal.Parse(myDeserializedClass.bmx.series[0].datos[0].dato);
                        }
                        return 0;
                    }
                }
                else
                {
                    return 0;
                }
                EmailService.Send("Documentos FX: Error - Banxico - 59 - Sin Datos: Fecha: " + fecha.ToString("yyyy-MM-dd"));
                return 0;
            }
            catch (Exception error)
            {
                BitacoraFX.Log(error.Message);
                if (error.InnerException!=null)
                {
                    BitacoraFX.Log(error.InnerException.Message);
                }
                EmailService.Send("Documentos FX: Error - Banxico - 64 - Fecha: " + fecha.ToString("yyyy-MM-dd"), error);
            }
            return 0;
        }

        // Root myDeserializedClass = JsonConvert.DeserializeObject<Root>(myJsonResponse); 
        public class Dato
        {
            public string fecha { get; set; }
            public string dato { get; set; }
        }

        public class Series
        {
            public string idSerie { get; set; }
            public string titulo { get; set; }
            public List<Dato> datos { get; set; }
        }

        public class Bmx
        {
            public List<Series> series { get; set; }
        }

        public class Root
        {
            public Bmx bmx { get; set; }
        }

    }

}
