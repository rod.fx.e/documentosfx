#region Copyright Syncfusion Inc. 2001 - 2017
// Copyright Syncfusion Inc. 2001 - 2017. All rights reserved.
// Use of this code is subject to the terms of our license.
// A copy of the current license can be obtained at any time by e-mailing
// licensing@syncfusion.com. Any infringement will be prosecuted under
// applicable laws. 
#endregion
using ConsultarCFDI.ConsultaCFDIService;
using Generales;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;

using System.Text;
using System.Windows.Forms;

namespace ConsultarCFDI
{
    public partial class frmConsultarCFDI : Syncfusion.Windows.Forms.MetroForm
    {
        public frmConsultarCFDI()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            validar();
        }

        private void validar()
        {
            try
            {
                ConsultaCFDIService.ConsultaCFDIServiceClient oConsulta = new ConsultaCFDIServiceClient();
                ConsultaCFDIService.Acuse oAcuse = new Acuse();
                oAcuse = oConsulta.Consulta("?re=BEN9501023I0&rr=SARM8209281F1&tt=440.000000&id=EC609EC1-5F63-4333-A2B8-2EDC10B68075");
                MessageBox.Show("Estatus " + oAcuse.CodigoEstatus + " Estado: " + oAcuse.Estado);
            }
            catch(Exception error)
            {
                ErrorFX.mostrar(error, true, true, "ConsultarCFDI - 40 ");
            }
        }
    }
}
