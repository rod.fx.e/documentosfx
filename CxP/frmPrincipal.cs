#region Copyright Syncfusion Inc. 2001 - 2017
// Copyright Syncfusion Inc. 2001 - 2017. All rights reserved.
// Use of this code is subject to the terms of our license.
// A copy of the current license can be obtained at any time by e-mailing
// licensing@syncfusion.com. Any infringement will be prosecuted under
// applicable laws. 
#endregion
using CFDI40;
using Generales;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Drawing;
using System.IO;
using System.Net;
using System.Runtime.InteropServices;
using System.Text;
using System.Windows.Forms;
using System.Xml;
using System.Xml.Serialization;

namespace CxP
{
    public partial class frmPrincipal : Syncfusion.Windows.Forms.MetroForm
    {
        string cfdiProveedores = "";
        List<cCFDI> lista;
        public frmPrincipal()
        {
            InitializeComponent();
            actualizarFacturas();

            if (System.ComponentModel.LicenseManager.UsageMode == System.ComponentModel.LicenseUsageMode.Designtime)
            {

            }

            try
            {
                cfdiProveedores = ConfigurationManager.AppSettings["cfdiProveedores"].ToString();
                ruta.Text = cfdiProveedores;
                MonitorDirectory(ruta.Text);
                lista = new List<cCFDI>();

            }
            catch
            {

            }
            this.notifyIcon.BalloonTipIcon = System.Windows.Forms.ToolTipIcon.Info;
            this.notifyIcon.BalloonTipText = Application.ProductName;
            this.notifyIcon.BalloonTipTitle = "Sincronizador CxP minimizado";
            this.notifyIcon.Text = "CxP FX - minimizado";
        }

        private void frmCxPServicio_Resize(object sender, EventArgs e)
        {
            if (this.WindowState == FormWindowState.Minimized)
            {
                notifyIcon.Visible = true;
                notifyIcon.ShowBalloonTip(3000);
                this.ShowInTaskbar = false;
            }
        }

        private void notifyIcon_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            this.WindowState = FormWindowState.Normal;
            this.ShowInTaskbar = true;
            notifyIcon.Visible = false;
        }

        private void MonitorDirectory(string path)
        {

            FileSystemWatcher watcher = new FileSystemWatcher();
            watcher.Path = path;
            watcher.IncludeSubdirectories = true;
            watcher.NotifyFilter = NotifyFilters.LastWrite;
            watcher.Filter = "*.xml";
            watcher.Changed += new FileSystemEventHandler(FileSystemWatcher_Created);
            watcher.EnableRaisingEvents = true;
        }

        private void FileSystemWatcher_Created(object sender, FileSystemEventArgs e)
        {
            Console.WriteLine("File created: {0}", e.Name);
            FileInfo info = new FileInfo(e.FullPath);
            validar33(e.FullPath);
        }
        private bool validar33(string archivo)
        {
            //Pasear el comprobante
            try
            {
                if (!IsFileLocked(archivo))
                {
                    XmlSerializer serializer_CFD_3 = new XmlSerializer(typeof(Comprobante));
                    TextReader reader = new StreamReader(archivo);
                    Comprobante oComprobante = (Comprobante)serializer_CFD_3.Deserialize(reader);
                    reader.Dispose();
                    reader.Close();

                    //Agregar fila
                    agregarRegistro(oComprobante, archivo);
                    return true;
                }
                return false;

            }
            catch (Exception error)
            {
                ErrorFX.mostrar(error, false, true, true);
                return false;
            }
        }
        private void agregarRegistro(Comprobante oComprobante, string archivo)
        {
            XmlDocument docXML = new XmlDocument();
            docXML.Load(archivo);
            XmlNodeList TimbreFiscalDigital = docXML.GetElementsByTagName("tfd:TimbreFiscalDigital");
            if (TimbreFiscalDigital == null)
            {
                return;
            }
            //Validar el folio fiscal
            string uuidtr = "";
            if (TimbreFiscalDigital[0] != null)
            {
                uuidtr = TimbreFiscalDigital[0].Attributes["UUID"].Value;
            }


            bool encontrado = false;
            foreach (cCFDI c in lista)
            {
                if (c.uuid.Equals(uuidtr))
                {
                    encontrado = true;
                }
            }

            if (!encontrado)
            {
                //Agregar a la lista
                agregarComprobante(oComprobante, archivo);
            }

        }

        private void agregarComprobante(Comprobante oComprobante, string archivo)
        {
            cCFDI ocCFDI = new cCFDI();
            ocCFDI.archivo = archivo;
            ocCFDI.version = oComprobante.Version;
            ocCFDI.tipoDeComprobante = oComprobante.TipoDeComprobante.ToString();
            ocCFDI.version = oComprobante.Version;
            ocCFDI.subTotal = oComprobante.SubTotal;
            ocCFDI.total = oComprobante.Total;
            ocCFDI.rfcEmisor = oComprobante.Emisor.Rfc;
            ocCFDI.nombre = oComprobante.Emisor.Nombre;
            ocCFDI.moneda = oComprobante.Moneda;
            ocCFDI.rfcReceptor = oComprobante.Receptor.Rfc;
            ocCFDI.fechaFactura = oComprobante.Fecha;
            ocCFDI.tipoCambio = oComprobante.TipoCambio;
            ocCFDI.formaPago = oComprobante.FormaPago.ToString();
            ocCFDI.metodoPago = oComprobante.MetodoPago;
            ocCFDI.lugarexpedicion = oComprobante.LugarExpedicion;
            ocCFDI.usoCFDI = oComprobante.Receptor.UsoCFDI.ToString();
            ocCFDI.status = false;
            ocCFDI.tipoCambio = oComprobante.TipoCambio;

            ocCFDI.serie = "";
            if (!String.IsNullOrEmpty(oComprobante.Serie))
            {
                ocCFDI.serie = oComprobante.Serie;
            }
            ocCFDI.folio = "";
            if (!String.IsNullOrEmpty(oComprobante.Folio))
            {
                ocCFDI.folio = oComprobante.Folio;
            }

            ocCFDI.folio = ocCFDI.serie + "" + ocCFDI.folio;

            XmlDocument docXML = new XmlDocument();
            docXML.Load(archivo);
            XmlNodeList TimbreFiscalDigital = docXML.GetElementsByTagName("tfd:TimbreFiscalDigital");
            if (TimbreFiscalDigital != null)
            {
                if (TimbreFiscalDigital[0] != null)
                {
                    ocCFDI.uuid = TimbreFiscalDigital[0].Attributes["UUID"].Value;
                }

            }
            lista.Add(ocCFDI);
        }


        const int ERROR_SHARING_VIOLATION = 32;
        const int ERROR_LOCK_VIOLATION = 33;
        private bool IsFileLocked(string file)
        {
            //check that problem is not in destination file
            if (File.Exists(file) == true)
            {
                FileStream stream = null;
                try
                {
                    stream = File.Open(file, FileMode.Open, FileAccess.ReadWrite, FileShare.None);
                }
                catch (Exception ex2)
                {
                    //_log.WriteLog(ex2, "Error in checking whether file is locked " + file);
                    int errorCode = Marshal.GetHRForException(ex2) & ((1 << 16) - 1);
                    if ((ex2 is IOException) && (errorCode == ERROR_SHARING_VIOLATION || errorCode == ERROR_LOCK_VIOLATION))
                    {
                        return true;
                    }
                }
                finally
                {
                    if (stream != null)
                        stream.Close();
                }
            }
            return false;
        }

        BackgroundWorker bgwFacturas;
        private void actualizarFacturas()
        {
            bgwFacturas = new BackgroundWorker();
            bgwFacturas.WorkerSupportsCancellation = true;
            bgwFacturas.WorkerReportsProgress = true;
            bgwFacturas.DoWork += new DoWorkEventHandler(bgwFacturas_DoWork);
            bgwFacturas.ProgressChanged += new ProgressChangedEventHandler(bgwFacturas_ProgressChanged);
            bgwFacturas.RunWorkerCompleted += new RunWorkerCompletedEventHandler(bgwFacturas_RunWorkerCompleted);
            bgwFacturas.RunWorkerAsync();
        }

        private void bgwFacturas_DoWork(object sender, DoWorkEventArgs e)
        {
            //Navegar por todas las empresas configuradas
            foreach (cCFDI c in lista)
            {
                if (!c.status)
                {
                    asigar_texto_actualizacion("CFDI Folio: " + c.folio + " RFC: " + c.rfcEmisor +  "  UUID: " + c.uuid + " "
                        + DateTime.Now.ToShortDateString()
                        + " " + DateTime.Now.ToShortTimeString());
                    subirArchivo(c);
                }
            }
        }
        private void bgwFacturas_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            asigar_texto_actualizacion("Facturas finalizado:" + DateTime.Now.ToShortDateString() + " " + DateTime.Now.ToShortTimeString());
            bgwFacturas.RunWorkerAsync();
        }
        private void bgwFacturas_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            asigar_texto_actualizacion(e.UserState.ToString());
        }
        private void asigar_texto_actualizacion(string texto)
        {
            if (textoHora.InvokeRequired)
            {
                MethodInvoker invoker = new MethodInvoker(delegate
                {
                    textoHora.Text = texto + " " + DateTime.Now.ToShortDateString() + " " + DateTime.Now.ToShortTimeString();
                });
                textoHora.Invoke(invoker);
            }
            else
            {
                textoHora.Text = texto;
            }
        }

        private void asigar_total_actualizacion(string texto)
        {
            if (lblTotal.InvokeRequired)
            {
                MethodInvoker invoker = new MethodInvoker(delegate
                {
                    lblTotal.Text = "Total: " + lista.Count.ToString() ;
                });
                lblTotal.Invoke(invoker);
            }
            else
            {
                lblTotal.Text = texto;
            }
        }

        //TODO: Subir todos en Gatwaey 
        //TODO: Cargar local Validar
        //TODO: Lista de pagos y postearlas
        //TODO: Agregar un plantilla para el formato de pago

        private bool validarUUID(string uuid)
        {
            using (WebClient client = new WebClient())
            {
                string url = ConfigurationManager.AppSettings["portal"].ToString() + "Conector/existeUUID/";

                HttpWebRequest requestToServerEndpoint =
                (HttpWebRequest)WebRequest.Create(url);
                string boundaryString = "----SomeRandomText";
                // Set the http request header \\
                requestToServerEndpoint.Method = WebRequestMethods.Http.Post;
                requestToServerEndpoint.ContentType = "multipart/form-data; boundary=" + boundaryString;
                requestToServerEndpoint.KeepAlive = true;
                requestToServerEndpoint.Credentials = System.Net.CredentialCache.DefaultCredentials;



                MemoryStream postDataStream = new MemoryStream();
                StreamWriter postDataWriter = new StreamWriter(postDataStream);

                // Include value from the myFileDescription text area in the post data
                postDataWriter.Write("\r\n--" + boundaryString + "\r\n");
                postDataWriter.Write("Content-Disposition: form-data; name=\"{0}\"\r\n\r\n{1}", "uuid", uuid);
                postDataWriter.Flush();

                // Set the http request body content length
                requestToServerEndpoint.ContentLength = postDataStream.Length;

                // Dump the post data from the memory stream to the request stream
                using (Stream s = requestToServerEndpoint.GetRequestStream())
                {
                    postDataStream.WriteTo(s);
                }
                postDataStream.Close();

                WebResponse response = requestToServerEndpoint.GetResponse();
                StreamReader responseReader = new StreamReader(response.GetResponseStream());
                string replyFromServer = responseReader.ReadToEnd();

                //Convertir en Json ver los errores
                try
                {
                    cResultadoSubir resultado = JsonConvert.DeserializeObject<cResultadoSubir>(replyFromServer);
                    return resultado.status;
                }
                catch (Exception error)
                {
                    ErrorFX.mostrar(error + Environment.NewLine
                        + replyFromServer, false, true, true);
                }

                return true;
            }
        }

        private void subirArchivo(cCFDI c)
        {
            //Validar existencia en el portal
            //Existe el UUID
            //string uuditr = drv.Cells["UUID"].Value.ToString();
            //System.Threading.Thread.Sleep(10000);
            if (!this.validarUUID(c.uuid))
            {
                c.status = true;
                return;
            }
            //System.Threading.Thread.Sleep(5000);
            using (WebClient client = new WebClient())
            {

                string url = ConfigurationManager.AppSettings["portal"].ToString() + "Conector/guardarCxP/";


                HttpWebRequest requestToServerEndpoint =
                (HttpWebRequest)WebRequest.Create(url);

                string boundaryString = "----SomeRandomText";
                string fileUrl = c.archivo;

                // Set the http request header \\
                requestToServerEndpoint.Method = WebRequestMethods.Http.Post;
                requestToServerEndpoint.ContentType = "multipart/form-data; boundary=" + boundaryString;
                requestToServerEndpoint.KeepAlive = true;
                requestToServerEndpoint.Credentials = System.Net.CredentialCache.DefaultCredentials;

                // Use a MemoryStream to form the post data request,
                // so that we can get the content-length attribute.
                MemoryStream postDataStream = new MemoryStream();
                StreamWriter postDataWriter = new StreamWriter(postDataStream);

                // Include value from the myFileDescription text area in the post data
                postDataWriter.Write("\r\n--" + boundaryString + "\r\n");
                postDataWriter.Write("Content-Disposition: form-data; name=\"{0}\"\r\n\r\n{1}", "total", c.total);

                postDataWriter.Write("\r\n--" + boundaryString + "\r\n");
                postDataWriter.Write("Content-Disposition: form-data; name=\"{0}\"\r\n\r\n{1}", "uuid", c.uuid);

                postDataWriter.Write("\r\n--" + boundaryString + "\r\n");
                postDataWriter.Write("Content-Disposition: form-data; name=\"{0}\"\r\n\r\n{1}", "version", c.version);

                postDataWriter.Write("\r\n--" + boundaryString + "\r\n");
                postDataWriter.Write("Content-Disposition: form-data; name=\"{0}\"\r\n\r\n{1}", "folio", c.folio);

                postDataWriter.Write("\r\n--" + boundaryString + "\r\n");
                postDataWriter.Write("Content-Disposition: form-data; name=\"{0}\"\r\n\r\n{1}", "moneda", c.moneda);

                postDataWriter.Write("\r\n--" + boundaryString + "\r\n");
                postDataWriter.Write("Content-Disposition: form-data; name=\"{0}\"\r\n\r\n{1}", "fecha", c.fechaFactura.ToString("yyyy-MM-dd"));

                postDataWriter.Write("\r\n--" + boundaryString + "\r\n");
                postDataWriter.Write("Content-Disposition: form-data; name=\"{0}\"\r\n\r\n{1}", "tipoComprobante", c.tipoDeComprobante);

                postDataWriter.Write("\r\n--" + boundaryString + "\r\n");
                postDataWriter.Write("Content-Disposition: form-data; name=\"{0}\"\r\n\r\n{1}", "rfcReceptor", c.rfcReceptor);

                postDataWriter.Write("\r\n--" + boundaryString + "\r\n");
                postDataWriter.Write("Content-Disposition: form-data; name=\"{0}\"\r\n\r\n{1}", "rfcEmisor", c.rfcEmisor);

                postDataWriter.Write("\r\n--" + boundaryString + "\r\n");
                postDataWriter.Write("Content-Disposition: form-data; name=\"{0}\"\r\n\r\n{1}", "metodoDePago", c.metodoPago);

                postDataWriter.Write("\r\n--" + boundaryString + "\r\n");
                postDataWriter.Write("Content-Disposition: form-data; name=\"{0}\"\r\n\r\n{1}", "formaDePago", c.formaPago);

                postDataWriter.Write("\r\n--" + boundaryString + "\r\n");
                postDataWriter.Write("Content-Disposition: form-data; name=\"{0}\"\r\n\r\n{1}", "UsoCFDI", c.usoCFDI);

                postDataWriter.Write("\r\n--" + boundaryString + "\r\n");
                postDataWriter.Write("Content-Disposition: form-data; name=\"{0}\"\r\n\r\n{1}", "name", c.nombre);

                postDataWriter.Write("\r\n--" + boundaryString + "\r\n");
                postDataWriter.Write("Content-Disposition: form-data; name=\"{0}\"\r\n\r\n{1}", "lugarexpedicion", c.lugarexpedicion);

                postDataWriter.Write("\r\n--" + boundaryString + "\r\n");
                postDataWriter.Write("Content-Disposition: form-data; name=\"{0}\"\r\n\r\n{1}", "tipoCambio", c.tipoCambio);

                // Include the file in the post data
                postDataWriter.Write("\r\n--" + boundaryString + "\r\n");
                postDataWriter.Write("Content-Disposition: form-data;"
                + "name=\"{0}\";"
                + "filename=\"{1}\""
                + "\r\nContent-Type: {2}\r\n\r\n",
                "XML_FILE",
                Path.GetFileName(fileUrl),
                Path.GetExtension(fileUrl));
                postDataWriter.Flush();
                // Read the file
                FileStream fileStream = new FileStream(fileUrl, FileMode.Open, FileAccess.Read);
                byte[] buffer = new byte[1024];
                int bytesRead = 0;
                while ((bytesRead = fileStream.Read(buffer, 0, buffer.Length)) != 0)
                {
                    postDataStream.Write(buffer, 0, bytesRead);
                }
                fileStream.Close();
                postDataWriter.Write("\r\n--" + boundaryString + "--\r\n");
                postDataWriter.Flush();




                // Set the http request body content length
                requestToServerEndpoint.ContentLength = postDataStream.Length;

                // Dump the post data from the memory stream to the request stream
                using (Stream s = requestToServerEndpoint.GetRequestStream())
                {
                    postDataStream.WriteTo(s);
                }
                postDataStream.Close();

                WebResponse response = requestToServerEndpoint.GetResponse();
                StreamReader responseReader = new StreamReader(response.GetResponseStream());
                string replyFromServer = responseReader.ReadToEnd();

                //Convertir en Json ver los errores
                try
                {
                    cResultadoSubir resultado = JsonConvert.DeserializeObject<cResultadoSubir>(replyFromServer);
                    c.status = resultado.status;
                }
                catch (Exception error)
                {
                    ErrorFX.mostrar(error + Environment.NewLine
                        + replyFromServer, false, true, true);
                    //System.Threading.Thread.Sleep(10000);
                    //subirArchivo(c);
                }

            }
        }

        private void button2_Click(object sender, EventArgs e)
        {

        }
    }
}
