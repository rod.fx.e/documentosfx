﻿using CxP.Clases;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CxP.Formularios
{
    public partial class GenerarCxP : Form
    {
        string reciboGeneral = "";
        public GenerarCxP()
        {
            InitializeComponent();
        }
        public GenerarCxP(string archivo, bool detalle, bool autoprocesar
            , DateTime? fechaCreacion=null, string recibo="", string solicitudServicioTr = ""
            , string solicitudServicioNotasTr = ""
            , string solicitudServicioTipoTr = "")
        {
            reciboGeneral = recibo;
            InitializeComponent();
            Detallado.Checked = detalle;
            CargarCFDIFromFile(archivo, detalle, fechaCreacion, recibo
                , solicitudServicioTr 
                , solicitudServicioNotasTr 
                , solicitudServicioTipoTr 
                );
            if (!autoprocesar)
            {
                Crear();
            }

        }
        private void button2_Click(object sender, EventArgs e)
        {
            CargarCFDI();
        }
        #region 

        cPAYABLE OcPAYABLE;
        private void CargarCFDI()
        {
            try
            {
                OpenFileDialog ofd = new OpenFileDialog();
                ofd.Filter = "XML Files (*.xml)|*.xml";
                ofd.FilterIndex = 0;
                ofd.DefaultExt = "xml";
                if (ofd.ShowDialog() == DialogResult.OK)
                {
                    if (!String.Equals(Path.GetExtension(ofd.FileName),
                                       ".xml",
                                       StringComparison.OrdinalIgnoreCase))
                    {
                        // Invalid file type selected; display an error.
                        MessageBox.Show("El archivo no es un CFDI.",
                                        "Invalid File Type",
                                        MessageBoxButtons.OK,
                                        MessageBoxIcon.Error);
                        return;
                    }
                    else
                    {
                        CargarCFDIFromFile(ofd.FileName, Detallado.Checked);
                    }
                }
            }
            catch (Exception error)
            {
                string mensaje = "";
                mensaje += error.Message.ToString();
                if (error.InnerException != null)
                {
                    mensaje += " " + error.InnerException.Message.ToString();
                }
                MessageBox.Show(this, mensaje, Application.ProductName
                            , MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void CargarCFDIFromFile(string archivo, bool detallado, DateTime? fechaCreacion = null
            , string recibo=""
            , string solicitudServicioTr = ""
            , string solicitudServicioNotasTr = ""
            , string solicitudServicioTipoTr = ""
            )
        {
            Limpiar();
            if (OcPAYABLE.ProcesarCFDI(archivo, detallado, fechaCreacion, reciboGeneral
                , solicitudServicioTr
                , solicitudServicioNotasTr
                , solicitudServicioTipoTr
                ))
            {
                this.CFDIPath.Text = archivo;
                INVOICE_ID.Text = OcPAYABLE.INVOICE_ID;
                INVOICE_DATE.Value = OcPAYABLE.INVOICE_DATE;
                VENDOR_ID.Text = OcPAYABLE.OVENDOR.ID;
                NAME.Text = OcPAYABLE.OVENDOR.NAME;
                Total.Text = OcPAYABLE.TOTAL_AMOUNT.ToString("C4");
                Subtotal.Text = OcPAYABLE.oComprobante.SubTotal.ToString("C4");
                Retencion.Text = (0).ToString("C4");
                Impuesto.Text = (0).ToString("C4");
                if (OcPAYABLE.oComprobante.Impuestos != null)
                {
                    Impuesto.Text = (OcPAYABLE.oComprobante.Impuestos.TotalImpuestosTrasladados).ToString("C4");
                    Retencion.Text = (OcPAYABLE.oComprobante.Impuestos.TotalImpuestosRetenidos).ToString("C4");
                }
                CURRENCY_ID.Text = OcPAYABLE.CURRENCY_ID;


                //Cargar las Lineas
                if (OcPAYABLE.lineas != null)
                {
                    foreach (cPAYABLE_LINE linea in OcPAYABLE.lineas)
                    {
                        int n = Dtg.Rows.Add();
                        Dtg.Rows[n].Tag = linea;
                        Dtg.Rows[n].Cells["QTY"].Value = linea.QTY.ToString();
                        Dtg.Rows[n].Cells["DESCRIPTION"].Value = linea.REFERENCE.ToString();
                        Dtg.Rows[n].Cells["Importe"].Value = linea.AMOUNT.ToString();
                        Dtg.Rows[n].Cells["GL_ACCOUNT_ID"].Value = linea.GL_ACCOUNT_ID;
                        Dtg.Rows[n].Cells["VAT_GL_ACCT_ID"].Value = linea.VAT_GL_ACCT_ID;
                        Dtg.Rows[n].Cells["VAT_AMOUNT"].Value = linea.VAT_AMOUNT.ToString();

                        Dtg.Rows[n].Cells["PURC_ORDER_ID"].Value = linea.PURC_ORDER_ID;
                        Dtg.Rows[n].Cells["PURC_ORDER_LINE_NO"].Value = linea.PURC_ORDER_LINE_NO;
                        Dtg.Rows[n].Cells["RECEIVER_ID"].Value = linea.RECEIVER_ID;
                        Dtg.Rows[n].Cells["Linea"].Value = linea.RECEIVER_LINE_NO;
                    }
                }
            }

        }
        #endregion

        private void button4_Click(object sender, EventArgs e)
        {
            Crear();
        }

        private void Crear()
        {
            if (!String.IsNullOrEmpty(VOUCHER_ID.Text))
            {
                MessageBox.Show("Ya fue procesado",
                            "Generación de CxP",
                            MessageBoxButtons.OK,
                            MessageBoxIcon.Error);
                return;
            }

            if (OcPAYABLE.Generar())
            {
                VOUCHER_ID.Text = OcPAYABLE.VOUCHER_ID;
            }
            OcPAYABLE.AutoAjustar();
        }

        private void button5_Click(object sender, EventArgs e)
        {
            Limpiar();
        }

        private void Limpiar()
        {
            OcPAYABLE = new cPAYABLE();
            Dtg.Rows.Clear();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            AbrirXML();
        }

        private void AbrirXML()
        {
            if (File.Exists(CFDIPath.Text))
            {
                string path = System.IO.Path.Combine(
                     System.IO.Directory.GetCurrentDirectory(),
                     CFDIPath.Text);
                System.Diagnostics.Process.Start("iexplore.exe", path);
            }
            
        }

        private void button6_Click(object sender, EventArgs e)
        {
            frmReglas OfrmReglas = new frmReglas();
            OfrmReglas.Show();
        }

        private void Dtg_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            //
            if( (e.ColumnIndex==3) | (e.ColumnIndex == 5))
            {

            }

            if (e.ColumnIndex == 6)
            {

            }
        }

        private void Dtg_MouseClick(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Right)
            {
                contextMenuStrip1.Show(this.Dtg, new Point(e.X, e.Y));
            }
        }


        private void CargarRecibo()
        {
            try
            {
                foreach (DataGridViewRow registro in Dtg.SelectedRows)
                {
                    frmRECEIVER OfrmRECEIVER = new frmRECEIVER(OcPAYABLE.OVENDOR.ID);
                    if (OfrmRECEIVER.ShowDialog() == DialogResult.OK)
                    {

                        cPAYABLE_LINE OcPAYABLE_LINE = (cPAYABLE_LINE)registro.Tag;


                        OcPAYABLE.ActualizarRecibo(OcPAYABLE_LINE.LINE_NO,
                            OfrmRECEIVER.selecionados[0].Cells["RECEIVER_ID"].Value.ToString()
                            , OfrmRECEIVER.selecionados[0].Cells["RECEIVER_LINE_NO"].Value.ToString()
                            , OfrmRECEIVER.selecionados[0].Cells["PURC_ORDER_ID"].Value.ToString(),
                            OfrmRECEIVER.selecionados[0].Cells["PURC_ORDER_LINE_NO"].Value.ToString()

                            );

                        registro.Cells["RECEIVER_ID"].Value=OfrmRECEIVER.selecionados[0].Cells["RECEIVER_ID"].Value.ToString();
                        registro.Cells["Linea"].Value = OfrmRECEIVER.selecionados[0].Cells["RECEIVER_LINE_NO"].Value.ToString();
                        registro.Cells["PURC_ORDER_ID"].Value = OfrmRECEIVER.selecionados[0].Cells["PURC_ORDER_ID"].Value.ToString();
                        registro.Cells["PURC_ORDER_LINE_NO"].Value = OfrmRECEIVER.selecionados[0].Cells["PURC_ORDER_LINE_NO"].Value.ToString();
                    }
                    break;
                }
            }
            catch (Exception error)
            {
                Generales.ErrorFX.mostrar(error, true, true, "GenerarCxP - 197 ");
            }
        }

        private void seleccionarReciboToolStripMenuItem_Click(object sender, EventArgs e)
        {
            CargarRecibo();
        }

        private void seleccionarCuentaContableToolStripMenuItem_Click(object sender, EventArgs e)
        {
            CargarCuentaContable();
        }

        private void CargarCuentaContable()
        {
            try
            {
                foreach (DataGridViewRow registro in Dtg.SelectedRows)
                {
                    frmACCOUNT OfrmACCOUNT = new frmACCOUNT();
                    if (OfrmACCOUNT.ShowDialog() == DialogResult.OK)
                    {
                        registro.Cells["GL_ACCOUNT_ID"].Value = OfrmACCOUNT.selecionados[0].Cells["ID"].Value.ToString();
                    }
                    break;
                }
            }
            catch (Exception error)
            {
                Generales.ErrorFX.mostrar(error, true, true, "GenerarCxP - 197 ");
            }
        }

        private void button7_Click(object sender, EventArgs e)
        {
            Eliminar();
        }

        private void Eliminar()
        {
            try
            {
                if (!String.IsNullOrEmpty(VOUCHER_ID.Text)) {
                    // Mostrar cuadro de diálogo de confirmación
                    DialogResult dialogResult = MessageBox.Show("¿Desea eliminar?", "Confirmar eliminación", MessageBoxButtons.YesNo);
                    if (dialogResult == DialogResult.Yes)
                    {
                        // Si el usuario responde que Sí, proceder con la eliminación
                        OcPAYABLE = new cPAYABLE();
                        OcPAYABLE.Eliminar(VOUCHER_ID.Text); // Asumiendo que VOUCHER_ID.Text contiene el valor de Voucher
                        Limpiar();
                    }
                   
                }
            }
            catch (Exception error)
            {
                Generales.ErrorFX.mostrar(error, true, true, "GenerarCxP - 197 ");
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {

        }
    }
}
