﻿using CxP.Clases;
using Generales;
using ModeloCxP;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.Entity.Validation;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CxP.Formularios
{
    public partial class FlujoEfectivo : Form
    {
        public FlujoEfectivo()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Cargar();
        }

        private void dtg_DataBindingComplete(object sender, DataGridViewBindingCompleteEventArgs e)
        {
            foreach (DataGridViewColumn col in dtg.Columns)
            {
                if (Detallado.Checked)
                {
                    if ((col.Index == 8) || (col.Index == 9) || (col.Index == 10))
                    {
                        // Alinear a la derecha
                        col.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;

                        // Formato de moneda
                        col.DefaultCellStyle.Format = "C2";  // C2 es un formato de moneda con dos decimales
                    }
                }
                else
                {
                    if (col.Index == 3)
                    {
                        // Alinear a la derecha
                        col.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;

                        // Formato de moneda
                        col.DefaultCellStyle.Format = "C2";  // C2 es un formato de moneda con dos decimales

                    }

                }

                // Aplicar formato específico si la columna se llama "TipoCambio"
                if (col.Name == "TipoCambio")
                {
                    col.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
                    col.DefaultCellStyle.Format = "N4";  // N4 es un formato numérico con cuatro decimales
                }

            }




        }
        private bool IsNumeric(object value)
        {
            double result;
            return double.TryParse(Convert.ToString(value), out result);
        }
        private void dtg_CellFormatting(object sender, DataGridViewCellFormattingEventArgs e)
        {

            if (this.dtg.Columns[e.ColumnIndex].Name != "TipoCambio" && this.dtg.Columns[e.ColumnIndex].Name != "Id" 
                && this.dtg.Columns[e.ColumnIndex].Name != "Documento"
                && this.dtg.Columns[e.ColumnIndex].Name != "Factura"
                && e.Value != null && IsNumeric(e.Value))
            {
                // Convert the value to a double
                double value = Convert.ToDouble(e.Value);

                // Apply color based on the value
                if (value < 0)
                {
                    e.CellStyle.BackColor = Color.Red;
                    e.CellStyle.ForeColor = Color.White; // Change text color for better readability
                }
                else if (value > 0)
                {
                    e.CellStyle.BackColor = Color.Green;
                    e.CellStyle.ForeColor = Color.White; // Change text color for better readability
                }
                // You can decide what to do if the value is exactly 0
                e.CellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
                e.CellStyle.Format = "C2";
            }

            // This event gets called for each cell being formatted. If the cell is in the "Tipo" column, we can check its value.
            if (dtg.Columns[e.ColumnIndex].Name == "Tipo")
            {
                if (e.Value != null && e.Value.ToString().Trim() == "Gasto Fijo")
                {
                    // Apply blue color to the entire row.
                    dtg.Rows[e.RowIndex].DefaultCellStyle.BackColor = Color.LightBlue;
                }
            }
        }

        private void dtg_CellFormattingTotales(object sender, DataGridViewCellFormattingEventArgs e)
        {

            if (e.Value != null && IsNumeric(e.Value))
            {
                // Convert the value to a double
                double value = Convert.ToDouble(e.Value);

                // Apply color based on the value
                if (value < 0)
                {
                    e.CellStyle.BackColor = Color.Red;
                    e.CellStyle.ForeColor = Color.White; // Change text color for better readability
                }
                else if (value > 0)
                {
                    e.CellStyle.BackColor = Color.Green;
                    e.CellStyle.ForeColor = Color.White; // Change text color for better readability
                }
                // You can decide what to do if the value is exactly 0
                e.CellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
                e.CellStyle.Format = "C2";
            }

            // This event gets called for each cell being formatted. If the cell is in the "Tipo" column, we can check its value.
            if (dtg.Columns[e.ColumnIndex].Name == "Tipo")
            {
                if (e.Value != null && e.Value.ToString().Trim() == "Gasto Fijo")
                {
                    // Apply blue color to the entire row.
                    dtg.Rows[e.RowIndex].DefaultCellStyle.BackColor = Color.LightBlue;
                }
            }
        }

        private void Cargar()
        {
            try
            {
                DateTime inicio = new DateTime(1999, 1, 1);
                System.Data.DataTable ODataTable = CFlujoEfectivo.CxC(inicio, fin.Value, Buscar.Text, BuscarDocumento.Text, Detallado.Checked);
                this.dtg.Columns.Clear();
                this.dtg.DataSource = ODataTable;

                //Agregar Columnas al final con Martes y Jueves hasta la fecha de solicitud
                // Fecha de inicio (puede ajustarse según necesites)
                DateTime startDate = DateTime.Now;

                // Fecha final seleccionada
                DateTime endDate = fin.Value;

                // Agregar columnas para los martes y jueves
                while (startDate <= endDate)
                {
                    if (startDate.DayOfWeek == DayOfWeek.Tuesday || startDate.DayOfWeek == DayOfWeek.Thursday)
                    {
                        int IndiceTr = dtg.Columns.Add(startDate.ToShortDateString(), startDate.ToShortDateString());
                        dtg.Columns[IndiceTr].ValueType = typeof(decimal);
                        dtg.Columns[IndiceTr].Tag = startDate.ToShortDateString();
                        dtg.Columns[IndiceTr].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
                        dtg.Columns[IndiceTr].DefaultCellStyle.Format = "C2";
                    }
                    startDate = startDate.AddDays(1);
                }

                CargarTotales();

                DataTable ODataTableTotales = ConvertDataGridViewToDataTable(dtg);
                DataTable ODataTableTotalesAgrupados = GroupAndSumDataTableByTipoAndNacionalidad(ODataTableTotales);
                DtgTotales.DataSource = ODataTableTotalesAgrupados;
            }
            catch (DbEntityValidationException error)
            {
                foreach (var eve in error.EntityValidationErrors)
                {
                    foreach (var ve in eve.ValidationErrors)
                    {
                        MessageBox.Show(this, ve.PropertyName + " Error: " + ve.ErrorMessage, Application.ProductName
                            , MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
            }
            catch (Exception error)
            {
                string mensaje = "";
                mensaje += error.Message.ToString();
                if (error.InnerException != null)
                {
                    mensaje += " " + error.InnerException.Message.ToString();
                }
                MessageBox.Show(this, mensaje, Application.ProductName
                            , MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        public DataTable GroupAndSumDataTableByTipoAndNacionalidad(DataTable dt)
        {
            // Crear un nuevo DataTable para almacenar el resultado
            DataTable groupedDt = new DataTable();

            // Agregar las columnas "Tipo" y "Nacionalidad" al DataTable de resultado
            groupedDt.Columns.Add("Tipo", typeof(string));
            groupedDt.Columns.Add("Nacionalidad", typeof(string));

            // Agregar las columnas numéricas al DataTable de resultado
            foreach (DataColumn column in dt.Columns)
            {
                if (column.ColumnName != "Tipo" && column.ColumnName != "Nacionalidad" &&
                    (column.DataType == typeof(int) || column.DataType == typeof(double) ||
                     column.DataType == typeof(decimal) || column.DataType == typeof(float)))
                {
                    groupedDt.Columns.Add(column.ColumnName, column.DataType);
                }
            }

            // Agrupar y sumar las columnas numéricas
            var groupedData = dt.AsEnumerable()
                .GroupBy(row => new { Tipo = row.Field<string>("Tipo"), Nacionalidad = row.Field<string>("Nacionalidad") })
                .Select(g =>
                {
                    var newRow = groupedDt.NewRow();
                    newRow["Tipo"] = g.Key.Tipo;
                    newRow["Nacionalidad"] = g.Key.Nacionalidad;

                    foreach (DataColumn column in groupedDt.Columns)
                    {
                        if (column.ColumnName != "Tipo" && column.ColumnName != "Nacionalidad")
                        {
                            newRow[column.ColumnName] = g.Sum(row => Convert.ToDouble(row[column.ColumnName]));
                        }
                    }

                    return newRow;
                });

            // Agregar las filas agrupadas al DataTable de resultado
            foreach (var row in groupedData)
            {
                groupedDt.Rows.Add(row);
            }

            return groupedDt;
        }

        public DataTable ConvertDataGridViewToDataTable(DataGridView dtg)
        {
            // Crear un nuevo DataTable
            DataTable dt = new DataTable();

            // Agregar columnas al DataTable, manteniendo el tipo de datos original
            foreach (DataGridViewColumn column in dtg.Columns)
            {
                Type columnType = column.ValueType ?? typeof(string);
                dt.Columns.Add(column.Name, columnType);
            }

            // Agregar filas al DataTable
            foreach (DataGridViewRow row in dtg.Rows)
            {
                if (row.IsNewRow) continue; // Ignorar la fila nueva (vacía)

                DataRow dataRow = dt.NewRow();

                foreach (DataGridViewColumn column in dtg.Columns)
                {
                    dataRow[column.Name] = row.Cells[column.Name].Value ?? DBNull.Value;
                }

                dt.Rows.Add(dataRow);
            }

            return dt;
        }
        private void CargarTotales()
        {
            foreach (DataGridViewRow Registro in this.dtg.Rows)
            {
                string TipoTr = Registro.Cells["Tipo"].Value.ToString();
                string IdTr=Registro.Cells["Id"].Value.ToString();
                DateTime inicio = new DateTime(1999,1,1);
                foreach (DataGridViewColumn ColumnaTr in this.dtg.Columns)
                {
                    if (ColumnaTr.Tag != null)
                    {
                        DateTime finTr = DateTime.Parse(ColumnaTr.Tag.ToString());

                        if (Detallado.Checked)
                        {
                            string DocumentoTr = Registro.Cells["Documento"].Value.ToString();
                            Registro.Cells[ColumnaTr.Index].Value = CFlujoEfectivo.ObtenerTotal(IdTr, TipoTr, inicio, finTr, DocumentoTr);
                        }
                        else
                        {
                            Registro.Cells[ColumnaTr.Index].Value = CFlujoEfectivo.ObtenerTotal(IdTr, TipoTr,inicio, finTr);
                        }
                        

                        inicio = finTr.AddDays(1);
                    }                    
                }
            }
        }

        private void dtg_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            
        }

        public DataGridViewSelectedRowCollection selecionados;
        public void Devolver()
        {
            if (dtg.SelectedRows.Count > 0)
            {
                DataGridViewSelectedRowCollection arrSelectedRows = dtg.SelectedRows;
                selecionados = arrSelectedRows;
                this.DialogResult = DialogResult.OK;
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            frmImpuesto O = new frmImpuesto();
            O.FormClosed += new FormClosedEventHandler(f_FormClosed);
            O.Show();
        }
        void f_FormClosed(object sender, FormClosedEventArgs e)
        {
            Cargar();
        }

        private void button2_Click_1(object sender, EventArgs e)
        {
            ExcelFX.exportar(dtg, "FlujoEfectivo" + DateTime.Now.ToShortDateString());
        }

        private void Detallado_CheckedChanged(object sender, EventArgs e)
        {

        }

        private void button3_Click(object sender, EventArgs e)
        {
            frmGastosFijo O = new frmGastosFijo();
            O.Show();
        }

        private void button4_Click(object sender, EventArgs e)
        {
            FlujoEfectivoCxC O = new FlujoEfectivoCxC();
            O.Show();
        }
    }
}
