﻿using Generales;
using ModeloCxP;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.Entity.Validation;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CxP.Formularios
{
    public partial class frmImpuestos : Form
    {
        public frmImpuestos()
        {
            InitializeComponent();
            Cargar();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Cargar();
        }

        private void Cargar()
        {
            try
            {
                ModeloCxP.CxPEntities dbContext = new ModeloCxP.CxPEntities();
                var result = from b in dbContext.DeclaracionImpuestoSet
                                             .Where(a => a.Inicio.ToString().Contains(Buscar.Text))
                                             .OrderBy(a => a.Inicio)
                             select b;

                List<DeclaracionImpuesto> dt = result.ToList();
                dtg.Rows.Clear();
                foreach (DeclaracionImpuesto registro in dt)
                {
                    if (registro != null)
                    {
                        int n = dtg.Rows.Add();
                        dtg.Rows[n].Tag = registro;
                        dtg.Rows[n].Cells["Inicio"].Value = registro.Inicio.ToShortDateString();
                        dtg.Rows[n].Cells["Fin"].Value = registro.Fin.ToShortDateString();
                    }

                }
                dbContext.Dispose();
                this.dtg.Columns[0].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
            }
            catch (DbEntityValidationException error)
            {
                foreach (var eve in error.EntityValidationErrors)
                {
                    foreach (var ve in eve.ValidationErrors)
                    {
                        MessageBox.Show(this, ve.PropertyName + " Error: " + ve.ErrorMessage, Application.ProductName
                            , MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
            }
            catch (Exception error)
            {
                string mensaje = "";
                mensaje += error.Message.ToString();
                if (error.InnerException != null)
                {
                    mensaje += " " + error.InnerException.Message.ToString();
                }
                MessageBox.Show(this, mensaje, Application.ProductName
                            , MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void dtg_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            CargarRegistro(e);
        }
        private void CargarRegistro(DataGridViewCellEventArgs e)
        {
            if (e.RowIndex > -1)
            {
                DeclaracionImpuesto registro = (DeclaracionImpuesto)dtg.Rows[e.RowIndex].Tag;
                frmImpuesto o = new frmImpuesto(registro.Id);
                o.Show();
            }
        }

        public DataGridViewSelectedRowCollection selecionados;
        public void Devolver()
        {
            if (dtg.SelectedRows.Count > 0)
            {
                DataGridViewSelectedRowCollection arrSelectedRows = dtg.SelectedRows;
                selecionados = arrSelectedRows;
                this.DialogResult = DialogResult.OK;
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            frmImpuesto O = new frmImpuesto();
            O.FormClosed += new FormClosedEventHandler(f_FormClosed);
            O.Show();
        }
        void f_FormClosed(object sender, FormClosedEventArgs e)
        {
            Cargar();
        }
    }
}
