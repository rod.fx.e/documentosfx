﻿using CxP.Clases;
using ModeloCxP;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.Entity.Validation;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CxP.Formularios
{
    public partial class FlujoEfectivoCxCComentario : Form
    {
        bool Modificado = false;
        public CxCCobranza oRegistro;
        Conexion OData = new Conexion();

        public FlujoEfectivoCxCComentario()
        {
            oRegistro=new CxCCobranza();
            InitializeComponent();
        }

        public FlujoEfectivoCxCComentario(string id)
        {
            oRegistro = new CxCCobranza();
            InitializeComponent();
            Cargar(id);
        }
        private void Cargar(string id)
        {
            try
            {
                ModeloCxP.CxPEntities dbContext = new ModeloCxP.CxPEntities();
                Modificado = false;
                CxCCobranza RegistroTmp = dbContext.CxCCobranzaSet
                                             .Where(a => a.Cliente.Equals(id)).FirstOrDefault()
                             ;
                if (RegistroTmp != null)
                {
                    oRegistro = RegistroTmp;
                    Comentario.Text = RegistroTmp.Comentario;                 
                }
                dbContext.Dispose();
                Cliente.Text = id;
            }
            catch (DbEntityValidationException error)
            {
                foreach (var eve in error.EntityValidationErrors)
                {
                    foreach (var ve in eve.ValidationErrors)
                    {
                        MessageBox.Show(this, ve.PropertyName + " Error: " + ve.ErrorMessage, Application.ProductName
                            , MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
            }
            catch (Exception error)
            {
                string mensaje = "";
                mensaje += error.Message.ToString();
                if (error.InnerException != null)
                {
                    mensaje += " " + error.InnerException.Message.ToString();
                }
                MessageBox.Show(this, mensaje, Application.ProductName
                            , MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        private void button6_Click(object sender, EventArgs e)
        {
            Limpiar();
        }
        #region Logica
        private void Limpiar()
        {
            if (Modificado)
            {

                DialogResult Resultado = MessageBox.Show(this, "¿Desea guardar los cambios?", Application.ProductName + "-" + Application.ProductVersion, MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                if (Resultado.ToString() == "Yes")
                {
                    Guardar();
                }
            }
            oRegistro = new CxCCobranza();
            Modificado = false;
            foreach (var c in this.Controls)
            {
                if (c is TextBox)
                {
                    ((TextBox)c).Text = "";

                }
            }

        }

        private void Guardar()
        {
            try
            {
                if (this.Cliente.Text.Trim() == "")
                {
                    MessageBox.Show(this, "Seleccione el proveedor", Application.ProductName
                            , MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }

                if (this.Comentario.Text.Trim() == "")
                {
                    MessageBox.Show(this, "Escriba el comentario", Application.ProductName
                            , MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }

                ModeloCxP.CxPEntities dbContext = new ModeloCxP.CxPEntities();
                Modificado = false;
                CxCCobranza Registro = new CxCCobranza();
                
                if (oRegistro.Id != 0)
                {
                    //Cargar registro
                    int valor = oRegistro.Id;
                    Registro = (from r in dbContext.CxCCobranzaSet.Where
                                            (a => a.Id == valor)
                                select r).FirstOrDefault();
                    Modificado = true;
                }


                if (!Modificado)
                {
                    Registro.Cliente = this.Cliente.Text;
                    Registro.Comentario = this.Comentario.Text;
                    Registro.FechaCreacion = DateTime.Now;
                    Registro.UsuarioCreacion = DateTime.Now;
                    dbContext.CxCCobranzaSet.Add(Registro);
                }
                else
                {
                    Registro.Cliente = this.Cliente.Text;
                    Registro.Comentario = this.Comentario.Text;
                    Registro.FechaCreacion = DateTime.Now;
                    Registro.UsuarioCreacion = DateTime.Now;
                    dbContext.CxCCobranzaSet.Attach(Registro);
                    dbContext.Entry(Registro).State = System.Data.Entity.EntityState.Modified;
                }

                dbContext.SaveChanges();
                oRegistro = Registro;
                Modificado = false;
                toolStripStatusLabel1.Text = "Datos guardados";
                //MessageBox.Show(this, "Datos guardados", Application.ProductName + "-" + Application.ProductVersion, MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            catch (DbEntityValidationException e)
            {
                foreach (var eve in e.EntityValidationErrors)
                {
                    foreach (var ve in eve.ValidationErrors)
                    {
                        MessageBox.Show(this, ve.PropertyName + " Error: " + ve.ErrorMessage, Application.ProductName
                            , MessageBoxButtons.OK, MessageBoxIcon.Error);
                        return;
                    }
                }
            }
            catch (Exception error)
            {
                string mensaje = "";
                mensaje += error.Message.ToString();
                if (error.InnerException != null)
                {
                    mensaje += " " + error.InnerException.Message.ToString();
                }
                MessageBox.Show(this, mensaje, Application.ProductName
                            , MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void Eliminar()
        {
            try
            {
                DialogResult Resultado = MessageBox.Show(this, "¿Esta seguro de eliminar?", Application.ProductName + "-" + Application.ProductVersion, MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                if (Resultado.ToString() != "Yes")
                {
                    return;
                }
                ModeloCxP.CxPEntities dbContext = new ModeloCxP.CxPEntities();
                dbContext.CxCCobranzaSet.Attach(oRegistro);
                dbContext.CxCCobranzaSet.Remove(oRegistro);
                dbContext.SaveChanges();
                MessageBox.Show(this, "Eliminado.", Application.ProductName + "-" + Application.ProductVersion, MessageBoxButtons.OK, MessageBoxIcon.Information);
                Limpiar();
            }
            catch (Exception error)
            {
                string mensaje = "";
                mensaje += error.Message.ToString();
                if (error.InnerException != null)
                {
                    mensaje += " " + error.InnerException.Message.ToString();
                }
                MessageBox.Show(this, mensaje, Application.ProductName
                            , MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        #endregion

        private void button4_Click(object sender, EventArgs e)
        {
            Guardar();
        }

        private void button5_Click(object sender, EventArgs e)
        {
            Eliminar();
        }

    }
}
