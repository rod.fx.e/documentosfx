﻿
namespace CxP.Formularios
{
    partial class Proveedor
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Proveedor));
            this.button1 = new System.Windows.Forms.Button();
            this.Id = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.Razon = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.Moneda = new System.Windows.Forms.ComboBox();
            this.Rfc = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.CuentaContable = new System.Windows.Forms.ComboBox();
            this.label6 = new System.Windows.Forms.Label();
            this.VatCode = new System.Windows.Forms.ComboBox();
            this.label17 = new System.Windows.Forms.Label();
            this.BUYER = new System.Windows.Forms.ComboBox();
            this.button2 = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // button1
            // 
            this.button1.BackColor = System.Drawing.Color.Goldenrod;
            this.button1.FlatAppearance.BorderSize = 0;
            this.button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button1.ForeColor = System.Drawing.Color.White;
            this.button1.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.button1.Location = new System.Drawing.Point(11, 176);
            this.button1.Margin = new System.Windows.Forms.Padding(2);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(82, 24);
            this.button1.TabIndex = 36;
            this.button1.Text = "Guardar";
            this.button1.UseVisualStyleBackColor = false;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // Id
            // 
            this.Id.Location = new System.Drawing.Point(69, 20);
            this.Id.Margin = new System.Windows.Forms.Padding(2);
            this.Id.Name = "Id";
            this.Id.Size = new System.Drawing.Size(76, 20);
            this.Id.TabIndex = 1;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Location = new System.Drawing.Point(8, 23);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(16, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Id";
            // 
            // Razon
            // 
            this.Razon.Location = new System.Drawing.Point(69, 44);
            this.Razon.Margin = new System.Windows.Forms.Padding(2);
            this.Razon.Name = "Razon";
            this.Razon.Size = new System.Drawing.Size(414, 20);
            this.Razon.TabIndex = 3;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Location = new System.Drawing.Point(8, 47);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(38, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "Razón";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.Transparent;
            this.label3.Location = new System.Drawing.Point(8, 72);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(46, 13);
            this.label3.TabIndex = 6;
            this.label3.Text = "Moneda";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.BackColor = System.Drawing.Color.Transparent;
            this.label4.Location = new System.Drawing.Point(8, 126);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(87, 13);
            this.label4.TabIndex = 32;
            this.label4.Text = "Cuenta para CxP";
            // 
            // Moneda
            // 
            this.Moneda.FormattingEnabled = true;
            this.Moneda.Items.AddRange(new object[] {
            "EUR",
            "MXN",
            "USD"});
            this.Moneda.Location = new System.Drawing.Point(69, 69);
            this.Moneda.Name = "Moneda";
            this.Moneda.Size = new System.Drawing.Size(76, 21);
            this.Moneda.TabIndex = 7;
            this.Moneda.Text = "MXN";
            // 
            // Rfc
            // 
            this.Rfc.Location = new System.Drawing.Point(518, 44);
            this.Rfc.Margin = new System.Windows.Forms.Padding(2);
            this.Rfc.Name = "Rfc";
            this.Rfc.Size = new System.Drawing.Size(123, 20);
            this.Rfc.TabIndex = 5;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.BackColor = System.Drawing.Color.Transparent;
            this.label5.Location = new System.Drawing.Point(488, 47);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(24, 13);
            this.label5.TabIndex = 4;
            this.label5.Text = "Rfc";
            // 
            // CuentaContable
            // 
            this.CuentaContable.FormattingEnabled = true;
            this.CuentaContable.Items.AddRange(new object[] {
            "2-11-01 / Proveedor MXN No Relacionado",
            "2-11-02 / Proveedor MXN Relacionado",
            "2-11-03 / Proveedor Moneda Ext No Relacionado",
            "2-11-04 / Proveedor Moneda Ext Relacionado"});
            this.CuentaContable.Location = new System.Drawing.Point(106, 123);
            this.CuentaContable.Name = "CuentaContable";
            this.CuentaContable.Size = new System.Drawing.Size(377, 21);
            this.CuentaContable.TabIndex = 33;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.BackColor = System.Drawing.Color.Transparent;
            this.label6.Location = new System.Drawing.Point(8, 101);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(55, 13);
            this.label6.TabIndex = 30;
            this.label6.Text = "Impuestos";
            // 
            // VatCode
            // 
            this.VatCode.FormattingEnabled = true;
            this.VatCode.Items.AddRange(new object[] {
            "IVA 0% Proveed",
            "IVA 16% Proveed"});
            this.VatCode.Location = new System.Drawing.Point(69, 96);
            this.VatCode.Name = "VatCode";
            this.VatCode.Size = new System.Drawing.Size(414, 21);
            this.VatCode.TabIndex = 31;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.BackColor = System.Drawing.Color.Transparent;
            this.label17.Location = new System.Drawing.Point(8, 153);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(34, 13);
            this.label17.TabIndex = 34;
            this.label17.Text = "Buyer";
            // 
            // BUYER
            // 
            this.BUYER.FormattingEnabled = true;
            this.BUYER.Items.AddRange(new object[] {
            "DRAMIREZ",
            "DALTAMIRANO",
            "MPADILLA",
            "OFERNANDEZ",
            "MRESENDIZ"});
            this.BUYER.Location = new System.Drawing.Point(69, 150);
            this.BUYER.Name = "BUYER";
            this.BUYER.Size = new System.Drawing.Size(414, 21);
            this.BUYER.TabIndex = 35;
            this.BUYER.Text = "DRAMIREZ";
            // 
            // button2
            // 
            this.button2.BackColor = System.Drawing.Color.ForestGreen;
            this.button2.FlatAppearance.BorderSize = 0;
            this.button2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button2.ForeColor = System.Drawing.Color.White;
            this.button2.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.button2.Location = new System.Drawing.Point(106, 176);
            this.button2.Margin = new System.Windows.Forms.Padding(2);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(82, 24);
            this.button2.TabIndex = 37;
            this.button2.Text = "Ver";
            this.button2.UseVisualStyleBackColor = false;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // Proveedor
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(653, 211);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.BUYER);
            this.Controls.Add(this.CuentaContable);
            this.Controls.Add(this.VatCode);
            this.Controls.Add(this.Moneda);
            this.Controls.Add(this.label17);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.Rfc);
            this.Controls.Add(this.Razon);
            this.Controls.Add(this.Id);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "Proveedor";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Proveedor";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.TextBox Id;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox Razon;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.ComboBox Moneda;
        private System.Windows.Forms.TextBox Rfc;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.ComboBox CuentaContable;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.ComboBox VatCode;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.ComboBox BUYER;
        private System.Windows.Forms.Button button2;
    }
}