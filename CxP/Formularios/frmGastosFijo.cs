﻿using Generales;
using ModeloCxP;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.Entity.Validation;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CxP.Formularios
{
    public partial class frmGastosFijo : Form
    {
        public frmGastosFijo()
        {
            InitializeComponent();
            Cargar();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Cargar();
        }

        private void Cargar()
        {
            try
            {
                ModeloCxP.CxPEntities dbContext = new ModeloCxP.CxPEntities();
                var result = from b in dbContext.GastoFijoSet
                                             .Where(a => a.Nombre.ToString().Contains(Buscar.Text))
                                             .OrderBy(a => a.Nombre)
                             select b;

                List<GastoFijo> dt = result.ToList();
                dtg.Rows.Clear();
                foreach (GastoFijo registro in dt)
                {
                    if (registro != null)
                    {
                        int n = dtg.Rows.Add();
                        dtg.Rows[n].Tag = registro;
                        dtg.Rows[n].Cells["Proveedor"].Value = registro.Nombre;
                        dtg.Rows[n].Cells["Monto"].Value = registro.Monto.ToString();
                    }

                }
                dbContext.Dispose();
                this.dtg.Columns[0].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
            }
            catch (DbEntityValidationException error)
            {
                foreach (var eve in error.EntityValidationErrors)
                {
                    foreach (var ve in eve.ValidationErrors)
                    {
                        MessageBox.Show(this, ve.PropertyName + " Error: " + ve.ErrorMessage, Application.ProductName
                            , MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
            }
            catch (Exception error)
            {
                string mensaje = "";
                mensaje += error.Message.ToString();
                if (error.InnerException != null)
                {
                    mensaje += " " + error.InnerException.Message.ToString();
                }
                MessageBox.Show(this, mensaje, Application.ProductName
                            , MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void dtg_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            CargarRegistro(e);
        }
        private void CargarRegistro(DataGridViewCellEventArgs e)
        {
            if (e.RowIndex > -1)
            {
                GastoFijo registro = (GastoFijo)dtg.Rows[e.RowIndex].Tag;
                frmGastoFijo o = new frmGastoFijo(registro.Id);
                o.Show();
            }
        }

        public DataGridViewSelectedRowCollection selecionados;
        public void Devolver()
        {
            if (dtg.SelectedRows.Count > 0)
            {
                DataGridViewSelectedRowCollection arrSelectedRows = dtg.SelectedRows;
                selecionados = arrSelectedRows;
                this.DialogResult = DialogResult.OK;
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            frmGastoFijo O = new frmGastoFijo();
            O.FormClosed += new FormClosedEventHandler(f_FormClosed);
            O.Show();
        }
        void f_FormClosed(object sender, FormClosedEventArgs e)
        {
            Cargar();
        }
    }
}
