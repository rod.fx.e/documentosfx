﻿using Generales;
using ModeloCxP;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.Entity.Validation;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CxP.Formularios
{
    public partial class frmRECEIVER : Form
    {
        public frmRECEIVER()
        {
            InitializeComponent();
            Cargar();
        }

        public frmRECEIVER(string vendor)
        {
            InitializeComponent();
            ProveedorBuscar.Text = vendor;
            Cargar();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Cargar();
        }

        private void Cargar()
        {
            try
            {
                ModeloERP.ERPEntities dbContext = new ModeloERP.ERPEntities();
                string sConn = dbContext.Database.Connection.ConnectionString;
                cCONEXCION oData = new cCONEXCION(sConn);

                //         string sSQL = @"SELECT rl.*
                //         FROM RECEIVER_LINE rl
                //         INNER JOIN PURC_ORDER_LINE pol ON pol.PURC_ORDER_ID=rl.PURC_ORDER_ID AND pol.LINE_NO=rl.PURC_ORDER_LINE_NO
                //         INNER JOIN PURCHASE_ORDER po ON pol.PURC_ORDER_ID=po.ID
                //         WHERE po.VENDOR_ID like '" + ProveedorBuscar.Text + @"'
                //OR rl.RECEIVER_ID like '%" + ReciboBuscar.Text + @"%'";
                string Sql = @"
                        SELECT rl.PURC_ORDER_ID, rl.PURC_ORDER_LINE_NO, rl.RECEIVER_ID, rl.LINE_NO, pol.VAT_CODE, v.TAX_PERCENT
                        FROM RECEIVER_LINE rl
                        INNER JOIN PURC_ORDER_LINE pol ON pol.PURC_ORDER_ID = rl.PURC_ORDER_ID AND pol.LINE_NO = rl.PURC_ORDER_LINE_NO
                        INNER JOIN PURCHASE_ORDER po ON pol.PURC_ORDER_ID = po.ID
						LEFT JOIN VAT v ON v.CODE = pol.VAT_CODE
                        WHERE NOT EXISTS (
                            SELECT 1
                            FROM PAYABLE_LINE pl
                            WHERE pl.PURC_ORDER_ID = rl.PURC_ORDER_ID
                            AND pl.PURC_ORDER_LINE_NO = rl.PURC_ORDER_LINE_NO
                            AND pl.RECEIVER_ID = rl.RECEIVER_ID
                            AND pl.RECEIVER_LINE_NO = rl.LINE_NO
                        )
                  AND      po.VENDOR_ID like '" + ProveedorBuscar.Text + @"'
                 AND rl.RECEIVER_ID like '%" + ReciboBuscar.Text + @"%'";
                dtg.Rows.Clear();
                DataTable oDataTable = oData.EjecutarConsulta(Sql);
                if (oDataTable != null)
                {
                    foreach (DataRow oDataRow in oDataTable.Rows)
                    {
                        int n = dtg.Rows.Add();
                        dtg.Rows[n].Cells["PURC_ORDER_ID"].Value = oDataRow["PURC_ORDER_ID"].ToString();
                        dtg.Rows[n].Cells["PURC_ORDER_LINE_NO"].Value = oDataRow["PURC_ORDER_LINE_NO"].ToString();
                        dtg.Rows[n].Cells["RECEIVER_ID"].Value = oDataRow["RECEIVER_ID"].ToString();
                        dtg.Rows[n].Cells["RECEIVER_LINE_NO"].Value = oDataRow["LINE_NO"].ToString();
                        dtg.Rows[n].Cells["VAT_CODE"].Value = oDataRow["VAT_CODE"].ToString();
                        dtg.Rows[n].Cells["TAX_PERCENT"].Value = oDataRow["TAX_PERCENT"].ToString();
                    }
                }
                dbContext.Dispose();
                this.dtg.Columns[0].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
            }
            catch (DbEntityValidationException error)
            {
                foreach (var eve in error.EntityValidationErrors)
                {
                    foreach (var ve in eve.ValidationErrors)
                    {
                        MessageBox.Show(this, ve.PropertyName + " Error: " + ve.ErrorMessage, Application.ProductName
                            , MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
            }
            catch (Exception error)
            {
                string mensaje = "";
                mensaje += error.Message.ToString();
                if (error.InnerException != null)
                {
                    mensaje += " " + error.InnerException.Message.ToString();
                }
                MessageBox.Show(this, mensaje, Application.ProductName
                            , MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void dtg_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            Devolver();
        }
        public DataGridViewSelectedRowCollection selecionados;
        public void Devolver()
        {
            if (dtg.SelectedRows.Count > 0)
            {
                DataGridViewSelectedRowCollection arrSelectedRows = dtg.SelectedRows;
                selecionados = arrSelectedRows;
                this.DialogResult = DialogResult.OK;
            }
        }
    }
}
