﻿using ModeloCxP;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.Entity.Validation;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CxP.Formularios
{
    public partial class frmReglas : Form
    {
        public frmReglas()
        {
            InitializeComponent();
            Cargar();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Cargar();
        }

        private void Cargar()
        {
            try
            {
                ModeloCxP.CxPEntities dbContext = new ModeloCxP.CxPEntities();
                var result = from b in dbContext.CxPReglasSet
                                             .Where(a => a.CuentaContable.Contains(Buscar.Text))
                                             .OrderBy(a => a.CuentaContable)
                             select b;

                List<CxPReglas> dt = result.ToList();
                dtg.Rows.Clear();
                foreach (CxPReglas registro in dt)
                {
                    if (registro != null)
                    {
                        int n = dtg.Rows.Add();
                        dtg.Rows[n].Tag = registro;
                        dtg.Rows[n].Cells["Tipo"].Value = registro.Tipo;
                        dtg.Rows[n].Cells["Valor"].Value = registro.Valor;
                        dtg.Rows[n].Cells["CuentaContable"].Value = registro.CuentaContable;
                        dtg.Rows[n].Cells["CondicionTipo"].Value = registro.CondicionTipo;
                        dtg.Rows[n].Cells["CondicionValor"].Value = registro.CondicionValor;
                    }

                }
                dbContext.Dispose();
                this.dtg.Columns[0].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
            }
            catch (DbEntityValidationException error)
            {
                foreach (var eve in error.EntityValidationErrors)
                {
                    foreach (var ve in eve.ValidationErrors)
                    {
                        MessageBox.Show(this, ve.PropertyName + " Error: " + ve.ErrorMessage, Application.ProductName
                            , MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
            }
            catch (Exception error)
            {
                string mensaje = "";
                mensaje += error.Message.ToString();
                if (error.InnerException != null)
                {
                    mensaje += " " + error.InnerException.Message.ToString();
                }
                MessageBox.Show(this, mensaje, Application.ProductName
                            , MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            frmRegla OfrmRegla = new frmRegla();
            OfrmRegla.FormClosed += new FormClosedEventHandler(f_FormClosed);
            OfrmRegla.Show();
        }
        void f_FormClosed(object sender, FormClosedEventArgs e)
        {
            Cargar();
        }

        private void dtg_DoubleClick(object sender, EventArgs e)
        {

        }

        private void dtg_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {

            CargarRegistro(e);
        }
        private void CargarRegistro(DataGridViewCellEventArgs e)
        {
            if (e.RowIndex > -1)
            {
                CxPReglas registro = (CxPReglas)dtg.Rows[e.RowIndex].Tag;
                frmRegla o = new frmRegla(registro.Id);
                o.Show();
            }
        }
    }
}
