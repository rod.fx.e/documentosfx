﻿using CxP.Clases;
using Generales;
using ModeloCxP;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.Entity.Validation;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CxP.Formularios
{
    public partial class FlujoEfectivoCxC : Form
    {
        public FlujoEfectivoCxC()
        {
            InitializeComponent();
            // Conectar el evento CellValueChanged
            dtg.CellValueChanged += dtg_CellValueChanged;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Cargar();
        }

        private void dtg_DataBindingComplete(object sender, DataGridViewBindingCompleteEventArgs e)
        {
            foreach (DataGridViewColumn col in dtg.Columns)
            {
                if (Detallado.Checked)
                {
                    if ((col.Index == 8) || (col.Index == 9) || (col.Index == 10))
                    {
                        // Alinear a la derecha
                        col.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;

                        // Formato de moneda
                        col.DefaultCellStyle.Format = "C2";  // C2 es un formato de moneda con dos decimales
                    }
                }
                else
                {
                    if (col.Index == 3)
                    {
                        // Alinear a la derecha
                        col.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;

                        // Formato de moneda
                        col.DefaultCellStyle.Format = "C2";  // C2 es un formato de moneda con dos decimales

                    }

                }

                // Aplicar formato específico si la columna se llama "TipoCambio"
                if (col.Name == "TipoCambio")
                {
                    col.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
                    col.DefaultCellStyle.Format = "N4";  // N4 es un formato numérico con cuatro decimales
                }

            }

        }
        private bool IsNumeric(object value)
        {
            double result;
            return double.TryParse(Convert.ToString(value), out result);
        }
        private void dtg_CellFormatting(object sender, DataGridViewCellFormattingEventArgs e)
        {

            if (this.dtg.Columns[e.ColumnIndex].Name != "TipoCambio" && this.dtg.Columns[e.ColumnIndex].Name != "Id"
                && this.dtg.Columns[e.ColumnIndex].Name != "Documento"
                && this.dtg.Columns[e.ColumnIndex].Name != "Factura"
                && e.Value != null && IsNumeric(e.Value))
            {
                // Convert the value to a double
                double value = Convert.ToDouble(e.Value);

                // Apply color based on the value
                if (value < 0)
                {
                    e.CellStyle.BackColor = Color.Red;
                    e.CellStyle.ForeColor = Color.White; // Change text color for better readability
                }
                else if (value > 0)
                {
                    e.CellStyle.BackColor = Color.Green;
                    e.CellStyle.ForeColor = Color.White; // Change text color for better readability
                }
                // You can decide what to do if the value is exactly 0
                e.CellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
                e.CellStyle.Format = "C2";
            }

            // This event gets called for each cell being formatted. If the cell is in the "Tipo" column, we can check its value.
            if (dtg.Columns[e.ColumnIndex].Name == "Tipo")
            {
                if (e.Value != null && e.Value.ToString().Trim() == "Gasto Fijo")
                {
                    // Apply blue color to the entire row.
                    dtg.Rows[e.RowIndex].DefaultCellStyle.BackColor = Color.LightBlue;
                }
            }
        }

        private void dtg_CellFormattingTotales(object sender, DataGridViewCellFormattingEventArgs e)
        {

            if (e.Value != null && IsNumeric(e.Value))
            {
                // Convert the value to a double
                double value = Convert.ToDouble(e.Value);

                // Apply color based on the value
                if (value < 0)
                {
                    e.CellStyle.BackColor = Color.Red;
                    e.CellStyle.ForeColor = Color.White; // Change text color for better readability
                }
                else if (value > 0)
                {
                    e.CellStyle.BackColor = Color.Green;
                    e.CellStyle.ForeColor = Color.White; // Change text color for better readability
                }
                // You can decide what to do if the value is exactly 0
                e.CellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
                e.CellStyle.Format = "C2";
            }

            // This event gets called for each cell being formatted. If the cell is in the "Tipo" column, we can check its value.
            if (dtg.Columns[e.ColumnIndex].Name == "Tipo")
            {
                if (e.Value != null && e.Value.ToString().Trim() == "Gasto Fijo")
                {
                    // Apply blue color to the entire row.
                    dtg.Rows[e.RowIndex].DefaultCellStyle.BackColor = Color.LightBlue;
                }
            }
        }

        private void Cargar()
        {
            try
            {
                DateTime inicio = new DateTime(1999, 1, 1);
                System.Data.DataTable ODataTable = CFlujoEfectivoCxC.CxC(inicio, fin.Value, Buscar.Text
                    , BuscarDocumento.Text, Detallado.Checked);
                this.dtg.Columns.Clear();
                this.dtg.DataSource = ODataTable;

                CargarTotales();

                DataTable ODataTableTotales = ConvertDataGridViewToDataTable(dtg);
                DataTable ODataTableTotalesAgrupados = GroupAndSumDataTableByTipoAndNacionalidad(ODataTableTotales);
                DtgTotales.DataSource = ODataTableTotalesAgrupados;



                try
                {
                    ModeloCxP.CxPEntities dbContext = new ModeloCxP.CxPEntities();
                    foreach (DataGridViewRow row in dtg.Rows)
                    {
                        string ClienteTr = row.Cells["Id"].Value.ToString();


                        CxCCobranza RegistroTmp = dbContext.CxCCobranzaSet
                                                     .Where(a => a.Cliente.Equals(ClienteTr)).FirstOrDefault()
                                     ;
                        if (RegistroTmp != null)
                        {
                            //Todo el Row en Amarillo
                            row.DefaultCellStyle.BackColor = Color.Yellow;
                        }
                    }
                    dbContext.Dispose();
                }
                catch (Exception error)
                {
                    string mensaje = "";
                    mensaje += error.Message.ToString();
                    if (error.InnerException != null)
                    {
                        mensaje += " " + error.InnerException.Message.ToString();
                    }
                    MessageBox.Show(this, mensaje, Application.ProductName
                                , MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                // Fecha final seleccionada más 4 semanas
                DateTime endDate = fin.Value.AddDays(28);

                // Ajusta la fecha de inicio al día después de 'fin.Value'
                DateTime startDate = fin.Value.AddDays(1);

                // Agregar columnas para los martes y jueves
                while (startDate <= endDate)
                {
                    if (startDate.DayOfWeek == DayOfWeek.Tuesday || startDate.DayOfWeek == DayOfWeek.Thursday)
                    {
                        int indiceTr = dtg.Columns.Add(startDate.ToShortDateString(), startDate.ToString("dd/MMM/yy"));
                        dtg.Columns[indiceTr].ValueType = typeof(decimal);
                        dtg.Columns[indiceTr].Tag = startDate.ToString("yyyyMMdd"); // Formato de fecha modificado
                        dtg.Columns[indiceTr].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
                        dtg.Columns[indiceTr].DefaultCellStyle.Format = "C2";
                    }
                    startDate = startDate.AddDays(1);
                }

                // Cargar valores existentes en las columnas de fechas agregadas
                LoadExistingValues();

                //Cargar los comentarios en una columna nueva
                int indiceTrN = dtg.Columns.Add("Comentario", "Comentario");
                dtg.Columns[indiceTrN].ValueType = typeof(string);
                dtg.Columns[indiceTrN].Visible = false;
                dtg.Columns[indiceTrN].Tag = "Comentario"; // Formato de fecha modificado
                dtg.Columns[indiceTrN].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;

                LoadComentarios();

                // Llamada al método para agregar la fila de totales
                //AgregarFilaSumatoria();
            }
            catch (DbEntityValidationException error)
            {
                foreach (var eve in error.EntityValidationErrors)
                {
                    foreach (var ve in eve.ValidationErrors)
                    {
                        MessageBox.Show(this, ve.PropertyName + " Error: " + ve.ErrorMessage, Application.ProductName
                            , MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
            }
            catch (Exception error)
            {
                string mensaje = "";
                mensaje += error.Message.ToString();
                if (error.InnerException != null)
                {
                    mensaje += " " + error.InnerException.Message.ToString();
                }
                MessageBox.Show(this, mensaje, Application.ProductName
                            , MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void AgregarFilaSumatoria()
        {
            if (dtg.Rows.Count == 0) return;

            // Crear una nueva fila para los totales
            DataGridViewRow totalRow = new DataGridViewRow();
            totalRow.CreateCells(dtg);

            foreach (DataGridViewColumn col in dtg.Columns)
            {
                if (col.Tag != null)
                {
                    decimal sum = 0;
                    foreach (DataGridViewRow row in dtg.Rows)
                    {
                        if (row.Cells[col.Index].Value != null && row.Cells[col.Index].Value != DBNull.Value)
                        {
                            // Validar si el valor es numérico
                            if (decimal.TryParse(row.Cells[col.Index].Value.ToString(), out decimal value))
                            {
                                sum += value;
                            }
                        }
                    }
                    totalRow.Cells[col.Index].Value = sum;
                    totalRow.Cells[col.Index].Style.Font = new Font(dtg.Font, FontStyle.Bold);
                    totalRow.Cells[col.Index].Style.BackColor = Color.LightGray;
                }
            }

            // Establecer un valor para la primera celda (opcional, solo por identificación)
            totalRow.Cells[0].Value = "Total";

            // Agregar la fila al DataGridView
            dtg.Rows.Add(totalRow);
        }

        private void LoadComentarios()
        {
            try
            {
                using (ModeloCxP.CxPEntities dbContext = new ModeloCxP.CxPEntities())
                {
                    foreach (DataGridViewRow row in dtg.Rows)
                    {
                        string customerId = row.Cells["Id"].Value.ToString();
                        var item = dbContext.CxCCobranzaSet.FirstOrDefault(a => a.Cliente == customerId);

                        if (item != null)
                        {
                            row.Cells["Comentario"].Value = item.Comentario;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error al cargar comentarios: " + ex.Message);
            }
        }

        private void LoadExistingValues()
        {
            try
            {
                using (ModeloCxP.CxPEntities dbContext = new ModeloCxP.CxPEntities())
                {
                    foreach (DataGridViewRow row in dtg.Rows)
                    {
                        string customerId = row.Cells["Id"].Value.ToString();
                        string FacturaTr = string.Empty;
                        if (Detallado.Checked)
                        {
                            FacturaTr = row.Cells["Documento"].Value.ToString();
                        }


                        foreach (DataGridViewColumn column in dtg.Columns)
                        {
                            if (column.Tag != null)
                            {
                                string columnName = column.Name;

                                if (Detallado.Checked)
                                {
                                    var item = dbContext.CxCCobranzaDiaSet.FirstOrDefault(
                                    a => a.Dia == columnName
                                    && a.Cliente == customerId
                                    && a.Factura == FacturaTr
                                    );

                                    if (item != null)
                                    {
                                        row.Cells[column.Index].Value = item.Comentario; // Ajusta esto según el campo que necesitas cargar
                                    }
                                }
                                else
                                {
                                    var items = dbContext.CxCCobranzaDiaSet.Where(
                                    a => a.Dia == columnName
                                    && a.Cliente == customerId
                                    );

                                    if (items != null)
                                    {
                                        decimal TotalColumnaTr = 0;
                                        foreach (CxCCobranzaDia item in items)
                                        {
                                            try
                                            {
                                                TotalColumnaTr += decimal.Parse(item.Comentario);
                                            }
                                            catch
                                            {

                                            }
                                        }
                                        //row.Cells[column.Index].Value = item.Comentario; // Ajusta esto según el campo que necesitas cargar
                                        row.Cells[column.Index].Value = TotalColumnaTr;
                                    }
                                }

                            }
                        }


                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error al cargar valores existentes: " + ex.Message);
            }
        }
        public DataTable GroupAndSumDataTableByTipoAndNacionalidad(DataTable dt)
        {
            // Crear un nuevo DataTable para almacenar el resultado
            DataTable groupedDt = new DataTable();

            // Agregar las columnas "Tipo" y "Nacionalidad" al DataTable de resultado
            //groupedDt.Columns.Add("Tipo", typeof(string));
            groupedDt.Columns.Add("Nacionalidad", typeof(string));

            // Agregar las columnas numéricas al DataTable de resultado
            foreach (DataColumn column in dt.Columns)
            {
                if (column.ColumnName != "Nacionalidad" &&
                    (column.DataType == typeof(int) || column.DataType == typeof(double) ||
                     column.DataType == typeof(decimal) || column.DataType == typeof(float)))
                {
                    groupedDt.Columns.Add(column.ColumnName, column.DataType);
                }
            }

            // Agrupar y sumar las columnas numéricas
            var groupedData = dt.AsEnumerable()
                .GroupBy(row => new { Nacionalidad = row.Field<string>("Nacionalidad") })
                .Select(g =>
                {
                    var newRow = groupedDt.NewRow();
                    newRow["Nacionalidad"] = g.Key.Nacionalidad;

                    foreach (DataColumn column in groupedDt.Columns)
                    {
                        if (column.ColumnName != "Nacionalidad")
                        {
                            newRow[column.ColumnName] = g.Sum(row => Convert.ToDouble(row[column.ColumnName]));
                        }
                    }

                    return newRow;
                });

            // Agregar las filas agrupadas al DataTable de resultado
            foreach (var row in groupedData)
            {
                groupedDt.Rows.Add(row);
            }

            return groupedDt;
        }

        public DataTable ConvertDataGridViewToDataTable(DataGridView dtg)
        {
            // Crear un nuevo DataTable
            DataTable dt = new DataTable();

            // Agregar columnas al DataTable, manteniendo el tipo de datos original
            foreach (DataGridViewColumn column in dtg.Columns)
            {
                Type columnType = column.ValueType ?? typeof(string);
                dt.Columns.Add(column.Name, columnType);
            }

            // Agregar filas al DataTable
            foreach (DataGridViewRow row in dtg.Rows)
            {
                if (row.IsNewRow) continue; // Ignorar la fila nueva (vacía)

                DataRow dataRow = dt.NewRow();

                foreach (DataGridViewColumn column in dtg.Columns)
                {
                    dataRow[column.Name] = row.Cells[column.Name].Value ?? DBNull.Value;
                }

                dt.Rows.Add(dataRow);
            }

            return dt;
        }
        private void CargarTotales()
        {
            foreach (DataGridViewRow Registro in this.dtg.Rows)
            {
                string IdTr = Registro.Cells["Id"].Value.ToString();
                DateTime inicio = new DateTime(1999, 1, 1);
                foreach (DataGridViewColumn ColumnaTr in this.dtg.Columns)
                {
                    if (ColumnaTr.Tag != null)
                    {
                        DateTime finTr = DateTime.Parse(ColumnaTr.Tag.ToString());

                        if (Detallado.Checked)
                        {
                            string DocumentoTr = Registro.Cells["Documento"].Value.ToString();
                            Registro.Cells[ColumnaTr.Index].Value = CFlujoEfectivoCxC.ObtenerTotal(IdTr, inicio, finTr, DocumentoTr);
                        }
                        else
                        {
                            Registro.Cells[ColumnaTr.Index].Value = CFlujoEfectivoCxC.ObtenerTotal(IdTr, inicio, finTr);
                        }


                        inicio = finTr.AddDays(1);
                    }
                }
            }
        }

        private void dtg_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            if (dtg.SelectedRows.Count > 0)
            {
                string Cliente = dtg.SelectedRows[0].Cells["Id"].Value.ToString();
                FlujoEfectivoCxCComentario OFlujoEfectivoCxCComentario = new FlujoEfectivoCxCComentario(Cliente);
                OFlujoEfectivoCxCComentario.ShowDialog();
            }
        }

        public DataGridViewSelectedRowCollection selecionados;
        public void Devolver()
        {
            if (dtg.SelectedRows.Count > 0)
            {
                DataGridViewSelectedRowCollection arrSelectedRows = dtg.SelectedRows;
                selecionados = arrSelectedRows;
                this.DialogResult = DialogResult.OK;
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            frmImpuesto O = new frmImpuesto();
            O.FormClosed += new FormClosedEventHandler(f_FormClosed);
            O.Show();
        }
        void f_FormClosed(object sender, FormClosedEventArgs e)
        {
            Cargar();
        }

        private void button2_Click_1(object sender, EventArgs e)
        {
            ExcelFX.exportar(dtg, "FlujoEfectivoCxC" + DateTime.Now.ToShortDateString());
        }

        private void Detallado_CheckedChanged(object sender, EventArgs e)
        {

        }

        private void button3_Click(object sender, EventArgs e)
        {
            frmGastosFijo O = new frmGastosFijo();
            O.Show();
        }

        private object previousValue; // Variable para almacenar el valor anterior

        private void dtg_CellBeginEdit(object sender, DataGridViewCellCancelEventArgs e)
        {
            //DataGridView dgv = sender as DataGridView;
            //if (dgv != null)
            //{
            //    // Guarda el valor anterior de la celda antes de que se edite
            //    previousValue = dgv.Rows[e.RowIndex].Cells[e.ColumnIndex].Value;
            //}
        }

        private void dtg_CellValueChanged(object sender, DataGridViewCellEventArgs e)
        {

            DataGridView dgv = sender as DataGridView;
            if (dgv != null)
            {
                if (Detallado.Checked)
                {
                    try
                    {
                        decimal TotalPesosTr = decimal.Parse(dgv.Rows[e.RowIndex].Cells["TotalPesos"].Value.ToString());
                        var updatedValue = dgv.Rows[e.RowIndex].Cells[e.ColumnIndex].Value;
                        if (IsNumeric(updatedValue))
                        {
                            decimal UpdateValueTr = decimal.Parse(updatedValue.ToString());
                            if (0 > UpdateValueTr)
                            {
                                // Si la condición no se cumple, restaurar el valor anterior
                                dgv.Rows[e.RowIndex].Cells[e.ColumnIndex].Value = 0;

                                return;
                            }
                            //Buscar la sumatoria de la columnas que estan con fecha
                            decimal TotalFila = ObtnerSumatoriaFila(e.RowIndex);
                            if (TotalPesosTr<TotalFila)
                            {
                                // Si la condición no se cumple, restaurar el valor anterior
                                dgv.Rows[e.RowIndex].Cells[e.ColumnIndex].Value = 0;
                                return;
                            }
                        }
                        // Aquí podrías llamar a un método para actualizar la base de datos
                        UpdateDatabase(e.RowIndex, e.ColumnIndex, updatedValue);
                    }
                    catch
                    {

                    }
                    
                }
                else
                {
                    var updatedValue = dgv.Rows[e.RowIndex].Cells[e.ColumnIndex].Value;
                    // Aquí podrías llamar a un método para actualizar la base de datos
                    decimal TotalPesosTr = decimal.Parse(dgv.Rows[e.RowIndex].Cells["TotalPesos"].Value.ToString());

                    if (IsNumeric(updatedValue))
                    {
                        decimal UpdateValueTr = decimal.Parse(updatedValue.ToString());
                        if (0 > UpdateValueTr)
                        {
                            // Si la condición no se cumple, restaurar el valor anterior
                            dgv.Rows[e.RowIndex].Cells[e.ColumnIndex].Value = 0;

                            return;
                        }
                        //Buscar la sumatoria de la columnas que estan con fecha
                        decimal TotalFila = ObtnerSumatoriaFila(e.RowIndex);
                        if (TotalPesosTr < (UpdateValueTr + TotalFila))
                        {
                            // Si la condición no se cumple, restaurar el valor anterior
                            dgv.Rows[e.RowIndex].Cells[e.ColumnIndex].Value = 0;
                            return;
                        }
                    }
                    UpdateDatabase(e.RowIndex, e.ColumnIndex, updatedValue);
                }

                
            }

        }

        private decimal ObtnerSumatoriaFila(int rowIndex)
        {
            decimal totalFila = 0;

            // Recorrer todas las columnas del DataGridView (dtg)
            foreach (DataGridViewColumn column in dtg.Columns)
            {
                // Validar si la columna tiene un Tag en el formato "yyyyMMdd"
                if (column.Tag != null && DateTime.TryParseExact(column.Tag.ToString(), "yyyyMMdd", null, System.Globalization.DateTimeStyles.None, out DateTime _))
                {
                    // Obtener el valor de la celda correspondiente a la fila y columna actuales
                    object cellValue = dtg.Rows[rowIndex].Cells[column.Index].Value;

                    // Validar si el valor es numérico y sumarlo
                    if (cellValue != null && decimal.TryParse(cellValue.ToString(), out decimal numericValue))
                    {
                        totalFila += numericValue;
                    }
                }
            }

            return totalFila;
        }


        private void UpdateDatabase(int rowIndex, int columnIndex, object updatedValue)
        {
            // Obtén el nombre de la columna que se está actualizando
            string columnName = dtg.Columns[columnIndex].Name;
            string CustomerId = dtg.Rows[rowIndex].Cells["Id"].Value.ToString(); // Asume que tienes una columna 'Id'

            string FacturaTr = string.Empty;
            if (Detallado.Checked)
            {
                FacturaTr = dtg.Rows[rowIndex].Cells["Documento"].Value.ToString();
            }

            try
            {
                using (ModeloCxP.CxPEntities dbContext = new ModeloCxP.CxPEntities())
                {
                    if (Detallado.Checked)
                    {
                        // Encuentra el registro en la base de datos basado en el Id
                        var item = dbContext.CxCCobranzaDiaSet
                        .Where(a => a.Dia == columnName && a.Cliente == CustomerId
                         && a.Factura == FacturaTr
                        ).FirstOrDefault();
                        if (item != null)
                        {
                            //Actualizar
                            item.Comentario = updatedValue.ToString();
                            dbContext.CxCCobranzaDiaSet.Attach(item);
                            dbContext.Entry(item).State = System.Data.Entity.EntityState.Modified;
                            dbContext.SaveChanges();
                        }
                        else
                        {
                            //Insertar
                            // Insertar un nuevo registro
                            CxCCobranzaDia newItem = new CxCCobranzaDia
                            {
                                Cliente = CustomerId,
                                Factura = FacturaTr,
                                Dia = columnName,
                                // Agrega otros campos necesarios aquí
                                Comentario = updatedValue.ToString(), // Si tienes un campo de comentario u otros campos adicionales, agrégalos aquí
                                FechaCreacion = DateTime.Now,
                                UsuarioCreacion = DateTime.Now, // Asegúrate de ajustar esto si tienes un tipo de datos diferente
                            };

                            dbContext.CxCCobranzaDiaSet.Add(newItem);
                            dbContext.SaveChanges();
                        }
                    }

                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error al actualizar la base de datos: " + ex.Message);
            }
        }
    }
}
