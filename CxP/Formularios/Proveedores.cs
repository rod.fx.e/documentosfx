﻿using CxP.Clases;
using Generales;
using ModeloCxP;
using ModeloDocumentosFX;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.Entity.Validation;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CxP.Formularios
{
    public partial class Proveedores : Form
    {
        public Proveedores()
        {
            InitializeComponent();
            CargarEmpresas();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Cargar();
        }
        string portal = string.Empty;
        private void Cargar()
        {
            try
            {
                DataTable oDataTable = new DataTable();
                if (emisor.Text == "Todos")
                {
                    //Navegar por los items
                    foreach (var item in emisor.Items)
                    {
                        try
                        {
                            cEMPRESA ocEMPRESA = item as cEMPRESA;
                            DataTable oDataTabletr = new DataTable();
                            if (ocEMPRESA != null)
                            {
                                portal = ocEMPRESA.portal;
                                if (!String.IsNullOrEmpty(ocEMPRESA.portal))
                                {

                                    if (Uri.IsWellFormedUriString(ocEMPRESA.portal, UriKind.Absolute))
                                    {
                                        CargarURL(ocEMPRESA.portal);
                                    }
                                    else
                                    {
                                        ErrorFX.mostrar("La URL " + ocEMPRESA.portal + " de la empresa no es valida. ucCxP - 80 - ", true, true, false);
                                    }
                                }
                                else
                                {
                                    ErrorFX.mostrar("La URL de la empresa no es valida. ucCxP - 81 - ", true, true, false);
                                }

                            }

                        }
                        catch (Exception error)
                        {
                            ErrorFX.mostrar(error, true, true, "ucCxP - 77 - ", true);
                        }
                    }
                }
                else
                {
                    cEMPRESA ocEMPRESA = emisor.SelectedItem as cEMPRESA;
                    if (ocEMPRESA != null)
                    {
                        if (!String.IsNullOrEmpty(ocEMPRESA.portal))
                        {
                            portal = ocEMPRESA.portal;
                            if (Uri.IsWellFormedUriString(ocEMPRESA.portal, UriKind.Absolute))
                            {
                                CargarURL(ocEMPRESA.portal);
                            }
                            else
                            {
                                ErrorFX.mostrar("La URL " + ocEMPRESA.portal + " de la empresa no es valida. ucCxP - 111 - ", true, true, false);
                            }
                        }
                        else
                        {
                            ErrorFX.mostrar("La URL de la empresa no es valida. ucCxP - 116 - ", true, true, false);
                        }
                    }
                }
            }
            catch (Exception e)
            {
                ErrorFX.mostrar(e, true, true, true);
            }
        }

        private void CargarEmpresas()
        {

            emisor.Items.Clear();
            emisor.Items.Add("Todos");
            cEMPRESA ocEMPRESA = new cEMPRESA();
            foreach (cEMPRESA registro in ocEMPRESA.todos("", true, Globales.automatico))
            {
                emisor.Items.Add(registro);
            }
            if (emisor.Items.Count > 0)
            {
                emisor.SelectedIndex = 0;
            }
        }

        private void CargarURL(string urlp)
        {
            try
            {
                string url = urlp + "SocioNegocio/listadosocios/";

                // You need to post the data as key value pairs:
                string postData = "nombre=" + Buscar.Text
               + "&faltaSincronizar="+faltaSincronizar.Checked.ToString();
                byte[] byteArray = Encoding.UTF8.GetBytes(postData);

                // Post the data to the right place.
                Uri target = new Uri(url);
                WebRequest request = WebRequest.Create(target);

                request.Method = "POST";
                request.ContentType = "application/x-www-form-urlencoded";
                request.ContentLength = byteArray.Length;

                using (var dataStream = request.GetRequestStream())
                {
                    dataStream.Write(byteArray, 0, byteArray.Length);
                }

                using (var response = (HttpWebResponse)request.GetResponse())
                {
                    Console.WriteLine("Error code: {0}", response.StatusCode);
                    using (Stream data = response.GetResponseStream())
                    using (var reader = new StreamReader(data))
                    {
                        string text = reader.ReadToEnd();
                        VaciarDatos(text);
                    }
                }



            }
            catch (DbEntityValidationException error)
            {
                foreach (var eve in error.EntityValidationErrors)
                {
                    foreach (var ve in eve.ValidationErrors)
                    {
                        MessageBox.Show(this, ve.PropertyName + " Error: " + ve.ErrorMessage, Application.ProductName
                            , MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
            }
            catch (Exception error)
            {
                string mensaje = "";
                mensaje += error.Message.ToString();
                if (error.InnerException != null)
                {
                    mensaje += " " + error.InnerException.Message.ToString();
                }
                MessageBox.Show(this, mensaje, Application.ProductName
                            , MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        private void VaciarDatos(string jsonInput)
        {
            try
            {
                // Deserializar el JSON
                var jsonData = JsonConvert.DeserializeObject<List<Proveedor>>(jsonInput);

                // Crear un nuevo DataTable
                DataTable table = new DataTable();

                // Verificar que las columnas existan, de lo contrario, agregarlas
                table.Columns.Add("id", typeof(string));
                table.Columns.Add("rfc", typeof(string));
                table.Columns.Add("razon", typeof(string));
                table.Columns.Add("alias", typeof(string));
                table.Columns.Add("password", typeof(string));
                table.Columns.Add("email", typeof(string));
                table.Columns.Add("oc_requerida", typeof(string));
                table.Columns.Add("xml_requerido", typeof(string));
                table.Columns.Add("rfc_validar", typeof(string));
                table.Columns.Add("pdf_requerido", typeof(string));
                table.Columns.Add("formatos_adicionales_requerido", typeof(string));
                table.Columns.Add("notificar_pago", typeof(string));
                table.Columns.Add("autorizacion", typeof(string));
                table.Columns.Add("metodoPago", typeof(string));
                table.Columns.Add("formaPago", typeof(string));
                table.Columns.Add("usoCFDI", typeof(string));
                table.Columns.Add("usoCFDIdos", typeof(string));
                table.Columns.Add("erpid", typeof(string));
                table.Columns.Add("credito_dias", typeof(string));
                table.Columns.Add("credito_limite", typeof(string));
                table.Columns.Add("swift", typeof(string));
                table.Columns.Add("routing", typeof(string));
                table.Columns.Add("banco", typeof(string));
                table.Columns.Add("clabe", typeof(string));
                table.Columns.Add("cuenta", typeof(string));
                table.Columns.Add("cif_archivo", typeof(string));
                table.Columns.Add("caratula_bancaria_archivo", typeof(string));
                table.Columns.Add("opinion_cumplimiento_archivo", typeof(string));
                table.Columns.Add("validarCfdi", typeof(string));
                table.Columns.Add("taxId", typeof(string));
                table.Columns.Add("estado", typeof(string));
                table.Columns.Add("opinion_cumplimineto_fecha", typeof(string));
                table.Columns.Add("usoCFDINDC", typeof(string));
                table.Columns.Add("autorizacionSegunda", typeof(string));
                table.Columns.Add("tipoDefecto", typeof(string));
                table.Columns.Add("cedula_identificacion_archivo", typeof(string));
                table.Columns.Add("cedula_identificacion_fecha", typeof(string));
                table.Columns.Add("moneda_cuenta", typeof(string));
                table.Columns.Add("tipoCompra", typeof(string));
                table.Columns.Add("correoContacto", typeof(string));
                table.Columns.Add("contactoAlterno", typeof(string));
                table.Columns.Add("contactoPuesto", typeof(string));
                table.Columns.Add("descripcionBienServicio", typeof(string));
                table.Columns.Add("referenciaBancaria", typeof(string));
                table.Columns.Add("moneda", typeof(string));
                table.Columns.Add("direccion", typeof(string));
                table.Columns.Add("colonia", typeof(string));
                table.Columns.Add("estadoPais", typeof(string));
                table.Columns.Add("cp", typeof(string));
                table.Columns.Add("ciudad", typeof(string));
                table.Columns.Add("pais", typeof(string));
                table.Columns.Add("contactoComercial", typeof(string));
                table.Columns.Add("puesto", typeof(string));
                table.Columns.Add("telefono", typeof(string));
                table.Columns.Add("telefonoMovil", typeof(string));
                table.Columns.Add("ClaveProdServ", typeof(string));
                table.Columns.Add("SolicitarRecibo", typeof(string));
                table.Columns.Add("agenteaduanal", typeof(string));
                table.Columns.Add("activo", typeof(string));
                table.Columns.Add("hashinvitacion", typeof(string));
                table.Columns.Add("validado69B", typeof(string));
                table.Columns.Add("porcentaje_completado_expediente", typeof(string));
                table.Columns.Add("direccion2", typeof(string));
                table.Columns.Add("direccion3", typeof(string));
                table.Columns.Add("operationTime", typeof(string));
                table.Columns.Add("nombreVendedor", typeof(string));
                table.Columns.Add("telefonoVendedor", typeof(string));
                table.Columns.Add("emailVendedor", typeof(string));
                table.Columns.Add("paymentConditions", typeof(string));
                table.Columns.Add("delivery", typeof(string));
                table.Columns.Add("branchNumber", typeof(string));
                table.Columns.Add("branchName", typeof(string));
                table.Columns.Add("plaza", typeof(string));
                table.Columns.Add("abba", typeof(string));
                table.Columns.Add("emailManager", typeof(string));
                table.Columns.Add("estadoDireccion", typeof(string));

                // Llenar el DataTable con los datos del JSON
                foreach (var registro in jsonData)
                {
                    DataRow row = table.NewRow();
                    row["id"] = registro.id;
                    row["rfc"] = registro.rfc;
                    row["razon"] = registro.razon;
                    row["alias"] = registro.alias;
                    row["password"] = registro.password;
                    row["email"] = registro.email;
                    row["oc_requerida"] = registro.oc_requerida;
                    row["xml_requerido"] = registro.xml_requerido;
                    row["rfc_validar"] = registro.rfc_validar;
                    row["pdf_requerido"] = registro.pdf_requerido;
                    row["formatos_adicionales_requerido"] = registro.formatos_adicionales_requerido;
                    row["notificar_pago"] = registro.notificar_pago;
                    row["autorizacion"] = registro.autorizacion;
                    row["metodoPago"] = registro.metodoPago;
                    row["formaPago"] = registro.formaPago;
                    row["usoCFDI"] = registro.usoCFDI;
                    row["usoCFDIdos"] = registro.usoCFDIdos;
                    row["erpid"] = registro.erpid;
                    row["credito_dias"] = registro.credito_dias;
                    row["credito_limite"] = registro.credito_limite;
                    row["swift"] = registro.swift;
                    row["routing"] = registro.routing;
                    row["banco"] = registro.banco;
                    row["clabe"] = registro.clabe;
                    row["cuenta"] = registro.cuenta;
                    row["cif_archivo"] = registro.cif_archivo;
                    row["caratula_bancaria_archivo"] = registro.caratula_bancaria_archivo;
                    row["opinion_cumplimiento_archivo"] = registro.opinion_cumplimiento_archivo;
                    row["validarCfdi"] = registro.validarCfdi;
                    row["taxId"] = registro.taxId;
                    row["estado"] = registro.estado;
                    row["opinion_cumplimineto_fecha"] = registro.opinion_cumplimineto_fecha;
                    row["usoCFDINDC"] = registro.usoCFDINDC;
                    row["autorizacionSegunda"] = registro.autorizacionSegunda;
                    row["tipoDefecto"] = registro.tipoDefecto;
                    row["cedula_identificacion_archivo"] = registro.cedula_identificacion_archivo;
                    row["cedula_identificacion_fecha"] = registro.cedula_identificacion_fecha;
                    row["moneda_cuenta"] = registro.moneda_cuenta;
                    row["tipoCompra"] = registro.tipoCompra;
                    row["correoContacto"] = registro.correoContacto;
                    row["contactoAlterno"] = registro.contactoAlterno;
                    row["contactoPuesto"] = registro.contactoPuesto;
                    row["descripcionBienServicio"] = registro.descripcionBienServicio;
                    row["referenciaBancaria"] = registro.referenciaBancaria;
                    row["moneda"] = registro.moneda;
                    row["direccion"] = registro.direccion;
                    row["colonia"] = registro.colonia;
                    row["estadoPais"] = registro.estadoPais;
                    row["cp"] = registro.cp;
                    row["ciudad"] = registro.ciudad;
                    row["pais"] = registro.pais;
                    row["contactoComercial"] = registro.contactoComercial;
                    row["puesto"] = registro.puesto;
                    row["telefono"] = registro.telefono;
                    row["telefonoMovil"] = registro.telefonoMovil;
                    row["ClaveProdServ"] = registro.ClaveProdServ;
                    row["SolicitarRecibo"] = registro.SolicitarRecibo;
                    row["agenteaduanal"] = registro.agenteaduanal;
                    row["activo"] = registro.activo;
                    row["hashinvitacion"] = registro.hashinvitacion;
                    row["validado69B"] = registro.validado69B;
                    row["porcentaje_completado_expediente"] = registro.porcentaje_completado_expediente;
                    row["direccion2"] = registro.direccion2;
                    row["direccion3"] = registro.direccion3;
                    row["operationTime"] = registro.operationTime;
                    row["nombreVendedor"] = registro.nombreVendedor;
                    row["telefonoVendedor"] = registro.telefonoVendedor;
                    row["emailVendedor"] = registro.emailVendedor;
                    row["paymentConditions"] = registro.paymentConditions;
                    row["delivery"] = registro.delivery;
                    row["branchNumber"] = registro.branchNumber;
                    row["branchName"] = registro.branchName;
                    row["plaza"] = registro.plaza;
                    row["abba"] = registro.abba;
                    row["emailManager"] = registro.emailManager;
                    row["estadoDireccion"] = registro.estadoDireccion;

                    table.Rows.Add(row);
                }

                // Asignar el DataTable a una fuente de datos, si es necesario
                this.dtg.DataSource = table;

                // Opcional: Configurar la visualización en la cuadrícula
                this.dtg.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.DisplayedCells;

                // Ocultar la columna "id"
                this.dtg.Columns["id"].Visible = false;
                // Ocultar las columnas especificadas
                this.dtg.Columns["id"].Visible = false;
                this.dtg.Columns["password"].Visible = false;
                this.dtg.Columns["oc_requerida"].Visible = false;
                this.dtg.Columns["xml_requerido"].Visible = false;
                this.dtg.Columns["rfc_validar"].Visible = false;
                this.dtg.Columns["pdf_requerido"].Visible = false;
                this.dtg.Columns["formatos_adicionales_requerido"].Visible = false;
                this.dtg.Columns["notificar_pago"].Visible = false;
                this.dtg.Columns["autorizacion"].Visible = false;
                this.dtg.Columns["metodoPago"].Visible = false;
                this.dtg.Columns["formaPago"].Visible = false;
                this.dtg.Columns["usoCFDI"].Visible = false;
                this.dtg.Columns["usoCFDIdos"].Visible = false;
                this.dtg.Columns["usoCFDINDC"].Visible = false;
                this.dtg.Columns["autorizacionSegunda"].Visible = false;
                this.dtg.Columns["tipoDefecto"].Visible = false;
                this.dtg.Columns["cedula_identificacion_archivo"].Visible = false;
                this.dtg.Columns["cedula_identificacion_fecha"].Visible = false;
                this.dtg.Columns["telefonoMovil"].Visible = false;
                this.dtg.Columns["ClaveProdServ"].Visible = false;
                this.dtg.Columns["SolicitarRecibo"].Visible = false;
                this.dtg.Columns["agenteaduanal"].Visible = false;
                this.dtg.Columns["activo"].Visible = false;
                this.dtg.Columns["hashinvitacion"].Visible = false;
                this.dtg.Columns["validado69B"].Visible = false;
                this.dtg.Columns["porcentaje_completado_expediente"].Visible = false;
            }
            catch (Exception error)
            {
                ErrorFX.mostrar(error, true, true);
            }
        }


        public class Proveedor
        {
            public string id { get; set; }
            public string rfc { get; set; }
            public string razon { get; set; }
            public string alias { get; set; }
            public string password { get; set; }
            public string email { get; set; }
            public string oc_requerida { get; set; }
            public string xml_requerido { get; set; }
            public string rfc_validar { get; set; }
            public string pdf_requerido { get; set; }
            public string formatos_adicionales_requerido { get; set; }
            public string notificar_pago { get; set; }
            public string autorizacion { get; set; }
            public string metodoPago { get; set; }
            public string formaPago { get; set; }
            public string usoCFDI { get; set; }
            public string usoCFDIdos { get; set; }
            public string erpid { get; set; }
            public string credito_dias { get; set; }
            public string credito_limite { get; set; }
            public string swift { get; set; }
            public string routing { get; set; }
            public string banco { get; set; }
            public string clabe { get; set; }
            public string cuenta { get; set; }
            public string cif_archivo { get; set; }
            public string caratula_bancaria_archivo { get; set; }
            public string opinion_cumplimiento_archivo { get; set; }
            public string validarCfdi { get; set; }
            public string taxId { get; set; }
            public string estado { get; set; }
            public string opinion_cumplimineto_fecha { get; set; }
            public string usoCFDINDC { get; set; }
            public string autorizacionSegunda { get; set; }
            public string tipoDefecto { get; set; }
            public string cedula_identificacion_archivo { get; set; }
            public string cedula_identificacion_fecha { get; set; }
            public string moneda_cuenta { get; set; }
            public string tipoCompra { get; set; }
            public string correoContacto { get; set; }
            public string contactoAlterno { get; set; }
            public string contactoPuesto { get; set; }
            public string descripcionBienServicio { get; set; }
            public string referenciaBancaria { get; set; }
            public string moneda { get; set; }
            public string direccion { get; set; }
            public string colonia { get; set; }
            public string estadoPais { get; set; }
            public string cp { get; set; }
            public string ciudad { get; set; }
            public string pais { get; set; }
            public string contactoComercial { get; set; }
            public string puesto { get; set; }
            public string telefono { get; set; }
            public string telefonoMovil { get; set; }
            public string ClaveProdServ { get; set; }
            public string SolicitarRecibo { get; set; }
            public string agenteaduanal { get; set; }
            public string activo { get; set; }
            public string hashinvitacion { get; set; }
            public string validado69B { get; set; }
            public string porcentaje_completado_expediente { get; set; }
            public string direccion2 { get; set; }
            public string direccion3 { get; set; }
            public string operationTime { get; set; }
            public string nombreVendedor { get; set; }
            public string telefonoVendedor { get; set; }
            public string emailVendedor { get; set; }
            public string paymentConditions { get; set; }
            public string delivery { get; set; }
            public string branchNumber { get; set; }
            public string branchName { get; set; }
            public string plaza { get; set; }
            public string abba { get; set; }
            public string emailManager { get; set; }
            public string estadoDireccion { get; set; }
        }


        private void button2_Click_1(object sender, EventArgs e)
        {
            ExcelFX.exportar(dtg, "Proveedores" + DateTime.Now.ToShortDateString());
        }

        private void dtg_CellContentDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            //Copiar registros en el CLipboard
            //if (e.RowIndex != -1)
            //{
            //    string foliotr = dtg.Rows[e.RowIndex].Cells["folio"].Value.ToString();
            //    System.Windows.Forms.Clipboard.SetText(foliotr);
            //}

        }

        private void generarCxPToolStripMenuItem_Click(object sender, EventArgs e)
        {
            GenerarProveedor();
        }

        //private void GenerarProveedor(bool ocultar = false)
        //{
        //    try
        //    {
        //        foreach (DataGridViewRow registro in dtg.SelectedRows)
        //        {
        //            string VAT_REGISTRATION = (string)registro.Cells["rfc"].Value.ToString().Trim();
        //            string Razon = (string)registro.Cells["razon"].Value.ToString().Trim();
        //            CxP.Formularios.Proveedor OGenerarProveedor 
        //                = new CxP.Formularios.Proveedor(VAT_REGISTRATION, Razon,!ocultar);
        //            if (!ocultar)
        //            {
        //                OGenerarProveedor.Show();
        //            }
        //        }
        //    }
        //    catch (Exception error)
        //    {
        //        ErrorFX.mostrar(error, true, true, "ucCxP - 353 ");
        //    }

        //}
        private void GenerarProveedor(bool ocultar = false)
        {
            try
            {
                foreach (DataGridViewRow registro in dtg.SelectedRows)
                {
                    // Pasar todo el registro al constructor de Proveedor
                    CxP.Formularios.Proveedor OGenerarProveedor = new CxP.Formularios.Proveedor(registro, !ocultar);

                    if (!ocultar)
                    {
                        OGenerarProveedor.Show();
                    }
                }
            }
            catch (Exception error)
            {
                ErrorFX.mostrar(error, true, true, "ucCxP - 353 ");
            }
        }


        private void generarCxPAutomaticoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            GenerarProveedor(true);
        }

        private void dtg_MouseClick(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Right)
            {
                contextMenuStrip1.Show(dtg, new Point(e.X, e.Y));
            }
        }
    }
}
