﻿using ModeloCxP;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.Entity.Validation;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CxP.Formularios
{
    public partial class frmRegla : Form
    {
        bool Modificado = false;
        public CxPReglas oRegistro;

        public frmRegla()
        {
            oRegistro=new CxPReglas();
            InitializeComponent();
        }
        public frmRegla(int id)
        {
            oRegistro = new CxPReglas();
            InitializeComponent();
            Cargar(id);
        }
        private void button6_Click(object sender, EventArgs e)
        {
            Limpiar();
        }
        #region Logica
        private void Limpiar()
        {
            if (Modificado)
            {

                DialogResult Resultado = MessageBox.Show(this, "¿Desea guardar los cambios?", Application.ProductName + "-" + Application.ProductVersion, MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                if (Resultado.ToString() == "Yes")
                {
                    Guardar();
                }
            }
            oRegistro = new CxPReglas();
            Modificado = false;
            foreach (var c in this.Controls)
            {
                if (c is TextBox)
                {
                    ((TextBox)c).Text = "";

                }
            }
            Negativo.Checked = false;
        }

        private void Guardar()
        {
            try
            {
                ModeloCxP.CxPEntities dbContext = new ModeloCxP.CxPEntities();
                Modificado = false;
                CxPReglas Registro = new CxPReglas();
                
                if (oRegistro.Id != 0)
                {
                    //Cargar registro
                    int valor = oRegistro.Id;
                    Registro = (from r in dbContext.CxPReglasSet.Where
                                            (a => a.Id == valor)
                                select r).FirstOrDefault();
                    Modificado = true;
                }
                Registro.Negativo = Negativo.Checked;
                Registro.Tipo = Tipo.Text;
                Registro.Valor = Valor.Text;
                Registro.CuentaContable = CuentaContable.Text;
                Registro.CondicionTipo = CondicionTipo.Text;
                Registro.CondicionValor = CondicionValor.Text;

                if (!Modificado)
                {
                    dbContext.CxPReglasSet.Add(Registro);
                }
                else
                {
                    dbContext.CxPReglasSet.Attach(Registro);
                    dbContext.Entry(Registro).State = System.Data.Entity.EntityState.Modified;
                }

                dbContext.SaveChanges();
                oRegistro = Registro;
                Modificado = false;
                MessageBox.Show(this, "Datos guardados", Application.ProductName + "-" + Application.ProductVersion, MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            catch (DbEntityValidationException e)
            {
                foreach (var eve in e.EntityValidationErrors)
                {
                    foreach (var ve in eve.ValidationErrors)
                    {
                        MessageBox.Show(this, ve.PropertyName + " Error: " + ve.ErrorMessage, Application.ProductName
                            , MessageBoxButtons.OK, MessageBoxIcon.Error);
                        return;
                    }
                }
            }
            catch (Exception error)
            {
                string mensaje = "";
                mensaje += error.Message.ToString();
                if (error.InnerException != null)
                {
                    mensaje += " " + error.InnerException.Message.ToString();
                }
                MessageBox.Show(this, mensaje, Application.ProductName
                            , MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void Eliminar()
        {
            try
            {
                DialogResult Resultado = MessageBox.Show(this, "¿Esta seguro de eliminar?", Application.ProductName + "-" + Application.ProductVersion, MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                if (Resultado.ToString() != "Yes")
                {
                    return;
                }
                ModeloCxP.CxPEntities dbContext = new ModeloCxP.CxPEntities();
                dbContext.CxPReglasSet.Attach(oRegistro);
                dbContext.CxPReglasSet.Remove(oRegistro);
                dbContext.SaveChanges();
                MessageBox.Show(this, "Eliminado.", Application.ProductName + "-" + Application.ProductVersion, MessageBoxButtons.OK, MessageBoxIcon.Information);
                Limpiar();
            }
            catch (Exception error)
            {
                string mensaje = "";
                mensaje += error.Message.ToString();
                if (error.InnerException != null)
                {
                    mensaje += " " + error.InnerException.Message.ToString();
                }
                MessageBox.Show(this, mensaje, Application.ProductName
                            , MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void Cargar(int id)
        {
            try
            {
                ModeloCxP.CxPEntities dbContext = new ModeloCxP.CxPEntities();
                Modificado = false;
                CxPReglas RegistroTmp = (from r in dbContext.CxPReglasSet.Where
                                            (a => a.Id == id)
                            select r).FirstOrDefault();
                ;
                if (RegistroTmp != null)
                {
                    oRegistro = RegistroTmp;
                    
                    Negativo.Checked=RegistroTmp.Negativo;
                    Tipo.Text= RegistroTmp.Tipo;
                    Valor.Text = RegistroTmp.Valor;
                    CuentaContable.Text = RegistroTmp.CuentaContable;

                    CondicionTipo.Text = RegistroTmp.CondicionTipo;

                    CondicionValor.Text = RegistroTmp.CondicionValor;
                }
                dbContext.Dispose();

            }
            catch (DbEntityValidationException error)
            {
                foreach (var eve in error.EntityValidationErrors)
                {
                    foreach (var ve in eve.ValidationErrors)
                    {
                        MessageBox.Show(this, ve.PropertyName + " Error: " + ve.ErrorMessage, Application.ProductName
                            , MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
            }
            catch (Exception error)
            {
                string mensaje = "";
                mensaje += error.Message.ToString();
                if (error.InnerException != null)
                {
                    mensaje += " " + error.InnerException.Message.ToString();
                }
                MessageBox.Show(this, mensaje, Application.ProductName
                            , MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        #endregion

        private void button4_Click(object sender, EventArgs e)
        {
            Guardar();
        }

        private void button5_Click(object sender, EventArgs e)
        {
            Eliminar();
        }
    }
}
