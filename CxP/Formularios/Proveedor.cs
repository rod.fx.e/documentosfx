﻿using CxP.Clases;
using Generales;
using ModeloCxP;
using ModeloDocumentosFX;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.Entity.Validation;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CxP.Formularios
{
    public partial class Proveedor : Form
    {
        string IdTr = "";
        DataGridViewRow registro = new DataGridViewRow();
        public Proveedor()
        {
            InitializeComponent();
            CargarBuyer();
        }

        private void CargarBuyer()
        {
            try
            {
                string Sql = @"
                SELECT po.BUYER
                FROM PURCHASE_ORDER po
                GROUP BY po.BUYER
                HAVING NOT(po.BUYER IS NULL)
                ";
                Conexion oData = new Conexion();
                System.Data.DataTable oDataTableR = oData.EjecutarConsulta(Sql);

                // Clear the ComboBox before loading new items
                this.BUYER.Items.Clear();

                if (oDataTableR != null)
                {
                    foreach (System.Data.DataRow oDataRowR in oDataTableR.Rows)
                    {
                        // Assuming "BuyerName" is a column in the BUYER table
                        string buyerName = oDataRowR["BUYER"].ToString();
                        this.BUYER.Items.Add(buyerName);
                    }
                }
            }
            catch (Exception ex)
            {
                // Optional: Add error handling here
                MessageBox.Show("Error loading buyers: " + ex.Message);
            }
        }


        public Proveedor(string vatRegistration,string name, bool ocultar)
        {
            InitializeComponent();
            Rfc.Text = vatRegistration;
            Razon.Text = name;
            CargarBuyer();
        }

        public Proveedor(DataGridViewRow registroTr, bool ocultar)
        {
            InitializeComponent();
            registro = registroTr;
            IdTr = registro.Cells["id"].Value?.ToString().Trim() ?? string.Empty;
            // Asignar valores a los campos del formulario usando el DataGridViewRow
            Rfc.Text = registro.Cells["rfc"].Value?.ToString().Trim() ?? string.Empty;
            Razon.Text = registro.Cells["razon"].Value?.ToString().Trim() ?? string.Empty;
            // Añade cualquier otro campo que necesites asignar aquí
            try
            {
                //ADDR_1.Text = registro.Cells["direccion"].Value?.ToString().Trim() ?? string.Empty;
                //ADDR_2.Text = registro.Cells["direccion2"].Value?.ToString().Trim() ?? string.Empty;
                //ADDR_3.Text = registro.Cells["direccion3"].Value?.ToString().Trim() ?? string.Empty;
                //COUNTRY.Text = registro.Cells["pais"].Value?.ToString().Trim() ?? string.Empty;
                //CITY.Text = registro.Cells["ciudad"].Value?.ToString().Trim() ?? string.Empty;
                //STATE.Text = registro.Cells["estado"].Value?.ToString().Trim() ?? string.Empty;
                //ZIPCODE.Text = registro.Cells["cp"].Value?.ToString().Trim() ?? string.Empty;
                //USER_1.Text = registro.Cells["giro_empresarial"].Value?.ToString().Trim() ?? string.Empty;
                //BUYER.Text = registro.Cells["buyer"].Value?.ToString().Trim() ?? string.Empty;

                //textBox3.Text = registro.Cells["contacto1"].Value?.ToString().Trim() ?? string.Empty;
                //textBox4.Text = registro.Cells["email1"].Value?.ToString().Trim() ?? string.Empty;
                //textBox5.Text = registro.Cells["contacto2"].Value?.ToString().Trim() ?? string.Empty;
                //textBox6.Text = registro.Cells["email2"].Value?.ToString().Trim() ?? string.Empty;
                //textBox7.Text = registro.Cells["contacto3"].Value?.ToString().Trim() ?? string.Empty;
                //textBox8.Text = registro.Cells["email3"].Value?.ToString().Trim() ?? string.Empty;
            }
            catch
            {

            }


            if (!ocultar)
            {
                this.Show();
            }
        }


        private void button1_Click(object sender, EventArgs e)
        {
            Guardar();
        }

        private void Guardar()
        {
            try
            {
                Vendor OVendor = new Vendor();
                if (String.IsNullOrEmpty(Id.Text))
                {
                    Id.Text = OVendor.ObtenerIdNuevo();
                }
                OVendor.ID = Id.Text;
                OVendor.NAME = Razon.Text;
                OVendor.BUYER = BUYER.Text;
                
                OVendor.USER_1 = registro.Cells["descripcionBienServicio"].Value?.ToString().Trim() ?? string.Empty;
                OVendor.ADDR_1 = registro.Cells["direccion"].Value?.ToString().Trim() ?? string.Empty;
                OVendor.ADDR_2 = registro.Cells["colonia"].Value?.ToString().Trim() ?? string.Empty;
                //OVendor.ADDR_3 = registro.Cells["estado"].Value?.ToString().Trim() ?? string.Empty;
                OVendor.CITY = registro.Cells["ciudad"].Value?.ToString().Trim() ?? string.Empty;
                OVendor.STATE = registro.Cells["estadoPais"].Value?.ToString().Trim() ?? string.Empty;
                OVendor.ZIPCODE = registro.Cells["cp"].Value?.ToString().Trim() ?? string.Empty;
                OVendor.COUNTRY = registro.Cells["pais"].Value?.ToString().Trim() ?? string.Empty;

                OVendor.CONTACT_FIRST_NAME = registro.Cells["nombreVendedor"].Value?.ToString().Trim() ?? string.Empty;
                OVendor.CONTACT_EMAIL = registro.Cells["emailVendedor"].Value?.ToString().Trim() ?? string.Empty;
                OVendor.CONTACT_PHONE = registro.Cells["telefonoVendedor"].Value?.ToString().Trim() ?? string.Empty;
                /*
                OVendor.CONTACT_FIRST_NAME_1 = registro.Cells["managerName"].Value?.ToString().Trim() ?? string.Empty;
                OVendor.CONTACT_EMAIL_1 = registro.Cells["emailManager"].Value?.ToString().Trim() ?? string.Empty;
                OVendor.CONTACT_PHONE_1 = registro.Cells["telefono"].Value?.ToString().Trim() ?? string.Empty;

                OVendor.CONTACT_FIRST_NAME_2 = registro.Cells["contactoAlterno"].Value?.ToString().Trim() ?? string.Empty;
                OVendor.CONTACT_EMAIL_2 = registro.Cells["correoContacto"].Value?.ToString().Trim() ?? string.Empty;
                OVendor.CONTACT_PHONE_2 = registro.Cells["telefonoMovil"].Value?.ToString().Trim() ?? string.Empty;
                */

                OVendor.VAT_REGISTRATION = Rfc.Text;
                OVendor.CURRENCY_ID = Moneda.Text;
                OVendor.VAT_CODE = VatCode.Text;
                string selectedValue = CuentaContable.Text;

                // Divide el texto seleccionado por el espacio
                string[] parts = selectedValue.Split('/');

                // Toma la primera parte antes del espacio
                if (parts.Length > 0)
                {
                    OVendor.DEF_PAYB_ACCT_ID = parts[0].Trim();
                }

                OVendor.Procesar();

                // Mensaje de confirmación
                MessageBox.Show("Proveedor creado satisfactoriamente.", "Éxito", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            catch (Exception ex)
            {
                // Mensaje de error
                MessageBox.Show($"Ocurrió un error al crear el proveedor: {ex.Message}", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            AbrirURL(Rfc.Text);
        }

        private void AbrirURL(string id)
        {
            // Construcción de la URL dinámica
            string url = $"https://www.documentosfx.com/web/losifra/invitacionProveedor/"+id;

            // Llamar a la URL y abrirla en el navegador predeterminado
            Process.Start(new ProcessStartInfo
            {
                FileName = url,
                UseShellExecute = true
            });
        }
    }
}
