﻿using CxP.Clases;
using Generales;
using ModeloCxP;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.Entity.Validation;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CxP.Formularios
{
    public partial class frmImpuestoDetalles : Form
    {
        TipoDetalle tipo;
        DateTime inicio;
        DateTime fin;
        public frmImpuestoDetalles(DateTime inicio, DateTime fin, TipoDetalle tipoDetalle)
        {
            this.inicio = inicio;
            this.fin = fin;
            tipo = tipoDetalle;
            InitializeComponent();
            Cargar();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Cargar();
        }

        private void Cargar()
        {
            try
            {
                Conexion oData = new Conexion();
                string Sql = "";
                if (tipo == TipoDetalle.TotalFacturasAsentadoCobrado)
                {
                    Sql = CImpuesto.ObtenerTotalAsentadoCobrado(inicio, fin, true);
                    System.Data.DataTable oDataTableR = oData.EjecutarConsulta(Sql);
                    this.dtg.DataSource = oDataTableR;
                }
                if (tipo == TipoDetalle.TotalFacturasTimbradoMonto)
                {
                    Sql = CImpuesto.ObtenerTotalAsentadoCobrado(inicio, fin, true);
                    System.Data.DataTable oDataTableR = oData.EjecutarConsulta(Sql);
                    this.dtg.DataSource = oDataTableR;
                }
                if (tipo == TipoDetalle.TotalFacturasPEsos)
                {
                    Sql = CImpuesto.ObtenerFacturasPesos(inicio, fin, true);
                    System.Data.DataTable oDataTableR = oData.EjecutarConsulta(Sql);
                    this.dtg.DataSource = oDataTableR;
                }
                if (tipo == TipoDetalle.TotalFacturasAsentado)
                {
                    Sql = CImpuesto.ObtenerTotalFacturasAsentado(inicio, fin, true);
                    System.Data.DataTable oDataTableR = oData.EjecutarConsulta(Sql);
                    this.dtg.DataSource = oDataTableR;
                }

                if (tipo == TipoDetalle.DiferenciaFacturasAsentado)
                {
                    Sql = CImpuesto.ObtenerDiferenciaFacturasAsentado(inicio, fin, true);
                    System.Data.DataTable oDataTableR = oData.EjecutarConsulta(Sql);
                    this.dtg.DataSource = oDataTableR;
                }

                if (tipo == TipoDetalle.TotalFacturasTimbradoMonto)
                {
                    System.Data.DataTable oDataTableR = CImpuesto.ObtenerTotalFacturasTimbradoMonto(inicio, fin);
                    this.dtg.DataSource = oDataTableR;
                }

                if (tipo == TipoDetalle.TotalFacturasTimbradoMonto)
                {
                    System.Data.DataTable oDataTableR = CImpuesto.ObtenerTotalFacturasTimbradoMonto(inicio, fin);
                    this.dtg.DataSource = oDataTableR;
                }
            }
            catch (DbEntityValidationException error)
            {
                foreach (var eve in error.EntityValidationErrors)
                {
                    foreach (var ve in eve.ValidationErrors)
                    {
                        MessageBox.Show(this, ve.PropertyName + " Error: " + ve.ErrorMessage, Application.ProductName
                            , MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
            }
            catch (Exception error)
            {
                string mensaje = "";
                mensaje += error.Message.ToString();
                if (error.InnerException != null)
                {
                    mensaje += " " + error.InnerException.Message.ToString();
                }
                MessageBox.Show(this, mensaje, Application.ProductName
                            , MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void dtg_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            Devolver();
        }
        public DataGridViewSelectedRowCollection selecionados;
        public void Devolver()
        {
            if (dtg.SelectedRows.Count > 0)
            {
                DataGridViewSelectedRowCollection arrSelectedRows = dtg.SelectedRows;
                selecionados = arrSelectedRows;
                this.DialogResult = DialogResult.OK;
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            frmImpuesto O = new frmImpuesto();
            O.FormClosed += new FormClosedEventHandler(f_FormClosed);
            O.Show();
        }
        void f_FormClosed(object sender, FormClosedEventArgs e)
        {
            Cargar();
        }
    }
}
