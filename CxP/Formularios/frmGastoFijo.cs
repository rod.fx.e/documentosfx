﻿using CxP.Clases;
using ModeloCxP;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.Entity.Validation;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CxP.Formularios
{
    public partial class frmGastoFijo : Form
    {
        bool Modificado = false;
        public GastoFijo oRegistro;
        Conexion OData = new Conexion();

        public frmGastoFijo()
        {
            oRegistro=new GastoFijo();
            InitializeComponent();
        }

        public frmGastoFijo(int id)
        {
            oRegistro = new GastoFijo();
            InitializeComponent();
            Cargar(id);
        }
        private void Cargar(int id)
        {
            try
            {
                ModeloCxP.CxPEntities dbContext = new ModeloCxP.CxPEntities();
                Modificado = false;
                GastoFijo RegistroTmp = dbContext.GastoFijoSet
                                             .Where(a => a.Id.Equals(id)).FirstOrDefault()
                             ;
                if (RegistroTmp != null)
                {
                    oRegistro = RegistroTmp;
                    Nombre.Text = RegistroTmp.Nombre;
                    Proveedor.Text = RegistroTmp.Proveedor;
                    Monto.Text = RegistroTmp.Monto.ToString();                    
                }
                dbContext.Dispose();

            }
            catch (DbEntityValidationException error)
            {
                foreach (var eve in error.EntityValidationErrors)
                {
                    foreach (var ve in eve.ValidationErrors)
                    {
                        MessageBox.Show(this, ve.PropertyName + " Error: " + ve.ErrorMessage, Application.ProductName
                            , MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
            }
            catch (Exception error)
            {
                string mensaje = "";
                mensaje += error.Message.ToString();
                if (error.InnerException != null)
                {
                    mensaje += " " + error.InnerException.Message.ToString();
                }
                MessageBox.Show(this, mensaje, Application.ProductName
                            , MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        private void button6_Click(object sender, EventArgs e)
        {
            Limpiar();
        }
        #region Logica
        private void Limpiar()
        {
            if (Modificado)
            {

                DialogResult Resultado = MessageBox.Show(this, "¿Desea guardar los cambios?", Application.ProductName + "-" + Application.ProductVersion, MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                if (Resultado.ToString() == "Yes")
                {
                    Guardar();
                }
            }
            oRegistro = new GastoFijo();
            Modificado = false;
            foreach (var c in this.Controls)
            {
                if (c is TextBox)
                {
                    ((TextBox)c).Text = "";

                }
            }

        }

        private void Guardar()
        {
            try
            {
                if (this.Proveedor.Text.Trim() == "")
                {
                    MessageBox.Show(this, "Seleccione el proveedor", Application.ProductName
                            , MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }

                if (this.Monto.Text.Trim() == "")
                {
                    MessageBox.Show(this, "Escriba el monto", Application.ProductName
                            , MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }

                ModeloCxP.CxPEntities dbContext = new ModeloCxP.CxPEntities();
                Modificado = false;
                GastoFijo Registro = new GastoFijo();
                
                if (oRegistro.Id != 0)
                {
                    //Cargar registro
                    int valor = oRegistro.Id;
                    Registro = (from r in dbContext.GastoFijoSet.Where
                                            (a => a.Id == valor)
                                select r).FirstOrDefault();
                    Modificado = true;
                }


                if (!Modificado)
                {
                    Registro.Proveedor = this.Proveedor.Text;
                    Registro.Nombre = this.Nombre.Text;
                    Registro.Monto = decimal.Parse(this.Monto.Text);
                    dbContext.GastoFijoSet.Add(Registro);
                }
                else
                {
                    Registro.Proveedor = this.Proveedor.Text;
                    Registro.Nombre = this.Nombre.Text;
                    Registro.Monto = decimal.Parse(this.Monto.Text);
                    dbContext.GastoFijoSet.Attach(Registro);
                    dbContext.Entry(Registro).State = System.Data.Entity.EntityState.Modified;
                }

                dbContext.SaveChanges();
                oRegistro = Registro;
                Modificado = false;
                toolStripStatusLabel1.Text = "Datos guardados";
                //MessageBox.Show(this, "Datos guardados", Application.ProductName + "-" + Application.ProductVersion, MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            catch (DbEntityValidationException e)
            {
                foreach (var eve in e.EntityValidationErrors)
                {
                    foreach (var ve in eve.ValidationErrors)
                    {
                        MessageBox.Show(this, ve.PropertyName + " Error: " + ve.ErrorMessage, Application.ProductName
                            , MessageBoxButtons.OK, MessageBoxIcon.Error);
                        return;
                    }
                }
            }
            catch (Exception error)
            {
                string mensaje = "";
                mensaje += error.Message.ToString();
                if (error.InnerException != null)
                {
                    mensaje += " " + error.InnerException.Message.ToString();
                }
                MessageBox.Show(this, mensaje, Application.ProductName
                            , MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void Eliminar()
        {
            try
            {
                DialogResult Resultado = MessageBox.Show(this, "¿Esta seguro de eliminar?", Application.ProductName + "-" + Application.ProductVersion, MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                if (Resultado.ToString() != "Yes")
                {
                    return;
                }
                ModeloCxP.CxPEntities dbContext = new ModeloCxP.CxPEntities();
                dbContext.GastoFijoSet.Attach(oRegistro);
                dbContext.GastoFijoSet.Remove(oRegistro);
                dbContext.SaveChanges();
                MessageBox.Show(this, "Eliminado.", Application.ProductName + "-" + Application.ProductVersion, MessageBoxButtons.OK, MessageBoxIcon.Information);
                Limpiar();
            }
            catch (Exception error)
            {
                string mensaje = "";
                mensaje += error.Message.ToString();
                if (error.InnerException != null)
                {
                    mensaje += " " + error.InnerException.Message.ToString();
                }
                MessageBox.Show(this, mensaje, Application.ProductName
                            , MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        #endregion

        private void button4_Click(object sender, EventArgs e)
        {
            Guardar();
        }

        private void button5_Click(object sender, EventArgs e)
        {
            Eliminar();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Conexion oData = new Conexion();

            FE_FX.frmVENDOR ofrmVENDOR = new FE_FX.frmVENDOR(oData.sConn);
            if (ofrmVENDOR.ShowDialog() == DialogResult.OK)
            {
                Proveedor.Text = (string)ofrmVENDOR.selecionados[0].Cells[0].Value;
                Nombre.Text = (string)ofrmVENDOR.selecionados[0].Cells[1].Value;
            }
        }
    }
}
