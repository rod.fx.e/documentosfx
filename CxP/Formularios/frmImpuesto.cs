﻿using CxP.Clases;
using ModeloCxP;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.Entity.Validation;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CxP.Formularios
{
    public partial class frmImpuesto : Form
    {
        bool Modificado = false;
        public DeclaracionImpuesto oRegistro;
        Conexion OData = new Conexion();

        public frmImpuesto()
        {
            oRegistro=new DeclaracionImpuesto();
            InitializeComponent();
        }

        public frmImpuesto(int id)
        {
            oRegistro = new DeclaracionImpuesto();
            InitializeComponent();
            Cargar(id);
        }
        private void Cargar(int id)
        {
            try
            {
                ModeloCxP.CxPEntities dbContext = new ModeloCxP.CxPEntities();
                Modificado = false;
                DeclaracionImpuesto Registro = new DeclaracionImpuesto();
                DeclaracionImpuesto RegistroTmp = dbContext.DeclaracionImpuestoSet
                                             .Where(a => a.Id.Equals(id)).FirstOrDefault()
                             ;
                if (RegistroTmp != null)
                {
                    Registro = RegistroTmp;
                    inicio.Value = RegistroTmp.Inicio;
                    fin.Value = RegistroTmp.Fin;                    
                }
                dbContext.Dispose();

            }
            catch (DbEntityValidationException error)
            {
                foreach (var eve in error.EntityValidationErrors)
                {
                    foreach (var ve in eve.ValidationErrors)
                    {
                        MessageBox.Show(this, ve.PropertyName + " Error: " + ve.ErrorMessage, Application.ProductName
                            , MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
            }
            catch (Exception error)
            {
                string mensaje = "";
                mensaje += error.Message.ToString();
                if (error.InnerException != null)
                {
                    mensaje += " " + error.InnerException.Message.ToString();
                }
                MessageBox.Show(this, mensaje, Application.ProductName
                            , MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        private void button6_Click(object sender, EventArgs e)
        {
            Limpiar();
        }
        #region Logica
        private void Limpiar()
        {
            if (Modificado)
            {

                DialogResult Resultado = MessageBox.Show(this, "¿Desea guardar los cambios?", Application.ProductName + "-" + Application.ProductVersion, MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                if (Resultado.ToString() == "Yes")
                {
                    Guardar();
                }
            }
            oRegistro = new DeclaracionImpuesto();
            Modificado = false;
            foreach (var c in this.Controls)
            {
                if (c is TextBox)
                {
                    ((TextBox)c).Text = "";

                }
            }

        }

        private void Guardar()
        {
            try
            {
                ModeloCxP.CxPEntities dbContext = new ModeloCxP.CxPEntities();
                Modificado = false;
                DeclaracionImpuesto Registro = new DeclaracionImpuesto();
                
                if (oRegistro.Id != 0)
                {
                    //Cargar registro
                    int valor = oRegistro.Id;
                    Registro = (from r in dbContext.DeclaracionImpuestoSet.Where
                                            (a => a.Id == valor)
                                select r).FirstOrDefault();
                    Modificado = true;
                }


                if (!Modificado)
                {
                    Registro.Inicio = this.inicio.Value;
                    Registro.Fin = this.fin.Value;
                    Registro.FechaCreacion = DateTime.Now;
                    Registro.UsuarioCreacion = DateTime.Now;
                    dbContext.DeclaracionImpuestoSet.Add(Registro);
                }
                else
                {
                    Registro.Inicio = this.inicio.Value;
                    Registro.Fin = this.fin.Value;

                    dbContext.DeclaracionImpuestoSet.Attach(Registro);
                    dbContext.Entry(Registro).State = System.Data.Entity.EntityState.Modified;
                }

                dbContext.SaveChanges();
                oRegistro = Registro;
                Modificado = false;
                toolStripStatusLabel1.Text = "Datos guardados";
                //MessageBox.Show(this, "Datos guardados", Application.ProductName + "-" + Application.ProductVersion, MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            catch (DbEntityValidationException e)
            {
                foreach (var eve in e.EntityValidationErrors)
                {
                    foreach (var ve in eve.ValidationErrors)
                    {
                        MessageBox.Show(this, ve.PropertyName + " Error: " + ve.ErrorMessage, Application.ProductName
                            , MessageBoxButtons.OK, MessageBoxIcon.Error);
                        return;
                    }
                }
            }
            catch (Exception error)
            {
                string mensaje = "";
                mensaje += error.Message.ToString();
                if (error.InnerException != null)
                {
                    mensaje += " " + error.InnerException.Message.ToString();
                }
                MessageBox.Show(this, mensaje, Application.ProductName
                            , MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void Eliminar()
        {
            try
            {
                DialogResult Resultado = MessageBox.Show(this, "¿Esta seguro de eliminar?", Application.ProductName + "-" + Application.ProductVersion, MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                if (Resultado.ToString() != "Yes")
                {
                    return;
                }
                ModeloCxP.CxPEntities dbContext = new ModeloCxP.CxPEntities();
                dbContext.DeclaracionImpuestoSet.Attach(oRegistro);
                dbContext.DeclaracionImpuestoSet.Remove(oRegistro);
                dbContext.SaveChanges();
                MessageBox.Show(this, "Eliminado.", Application.ProductName + "-" + Application.ProductVersion, MessageBoxButtons.OK, MessageBoxIcon.Information);
                Limpiar();
            }
            catch (Exception error)
            {
                string mensaje = "";
                mensaje += error.Message.ToString();
                if (error.InnerException != null)
                {
                    mensaje += " " + error.InnerException.Message.ToString();
                }
                MessageBox.Show(this, mensaje, Application.ProductName
                            , MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        #endregion

        private void button4_Click(object sender, EventArgs e)
        {
            Guardar();
        }

        private void button5_Click(object sender, EventArgs e)
        {
            Eliminar();
        }

        private void button15_Click(object sender, EventArgs e)
        {
            Calcular();
        }

        private void Calcular()
        {
            totalFactura.Text= CImpuesto.TotalFacturas(inicio.Value, fin.Value).ToString("N0");
            decimal totalFacturaPesosTr=CImpuesto.TotalFacturasPesos(inicio.Value, fin.Value);
            totalFacturaPesos.Text = totalFacturaPesosTr.ToString("C4");
            decimal totalFacturaAsentadoTr = CImpuesto.TotalFacturasAsentado(inicio.Value, fin.Value);
            totalFacturaAsentado.Text = totalFacturaAsentadoTr.ToString("C4");

            decimal DiferenciaFacturasAsentadoTr = (totalFacturaPesosTr - totalFacturaAsentadoTr);
            // Ajustar el valor de DiferenciaFacturasAsentadoTr si está dentro del rango especificado
            if (DiferenciaFacturasAsentadoTr >= -1 && DiferenciaFacturasAsentadoTr <= 1)
            {
                DiferenciaFacturasAsentadoTr = 0;
            }
            // Cambia el color de fondo según el valor
            if (DiferenciaFacturasAsentadoTr == 0)
            {
                DiferenciaFacturasAsentado.BackColor = Color.LightGreen;
            }
            else
            {
                DiferenciaFacturasAsentado.BackColor = Color.LightCoral;
            }

            DiferenciaFacturasAsentado.Text= DiferenciaFacturasAsentadoTr.ToString("C4");


            totalAsentadoCobrado.Text = CImpuesto.TotalAsentadoCobrado(inicio.Value, fin.Value).ToString("C4");
            totalFacturaTimbradas.Text = CImpuesto.TotalFacturasTimbradas(inicio.Value, fin.Value).ToString("N0");
            totalFacturasTimbradasMonto.Text = CImpuesto.TotalFacturasTimbradasMonto(inicio.Value, fin.Value).ToString("C4");

            totalVoucher.Text = CImpuesto.TotalVoucher(inicio.Value, fin.Value).ToString("N0");
            totalVoucherPesos.Text = CImpuesto.TotalVoucherPesos(inicio.Value, fin.Value).ToString("C4");
            totalVoucherAsentado.Text = CImpuesto.TotalVoucherAsentado(inicio.Value, fin.Value).ToString("C4");
            totalAsentadoPagado.Text = CImpuesto.TotalAsentadoPagado(inicio.Value, fin.Value).ToString("C4");
        }

        private void button2_Click(object sender, EventArgs e)
        {
            frmImpuestoDetalles OfrmImpuestoDetalles = new frmImpuestoDetalles(inicio.Value, fin.Value, TipoDetalle.TotalFacturas);
            OfrmImpuestoDetalles.Show();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            frmImpuestoDetalles OfrmImpuestoDetalles = new frmImpuestoDetalles(inicio.Value, fin.Value, TipoDetalle.TotalFacturasPEsos);
            OfrmImpuestoDetalles.Show();
        }

        private void button7_Click(object sender, EventArgs e)
        {
            frmImpuestoDetalles OfrmImpuestoDetalles = new frmImpuestoDetalles(inicio.Value, fin.Value, TipoDetalle.TotalFacturasAsentado);
            OfrmImpuestoDetalles.Show();
        }

        private void button8_Click(object sender, EventArgs e)
        {
            frmImpuestoDetalles OfrmImpuestoDetalles = new frmImpuestoDetalles(inicio.Value, fin.Value, TipoDetalle.TotalFacturasTimbradoMonto);
            OfrmImpuestoDetalles.Show();
        }

        private void button16_Click(object sender, EventArgs e)
        {
            frmImpuestoDetalles OfrmImpuestoDetalles = new frmImpuestoDetalles(inicio.Value, fin.Value, TipoDetalle.TotalFacturasCobrado);
            OfrmImpuestoDetalles.Show();
        }

        private void button17_Click(object sender, EventArgs e)
        {
            frmImpuestoDetalles OfrmImpuestoDetalles = new frmImpuestoDetalles(inicio.Value, fin.Value, TipoDetalle.TotalFacturasAsentadoCobrado);
            OfrmImpuestoDetalles.Show();
        }

        private void button9_Click(object sender, EventArgs e)
        {
            frmImpuestoDetalles OfrmImpuestoDetalles = new frmImpuestoDetalles(inicio.Value, fin.Value, TipoDetalle.TotalFacturasCP);
            OfrmImpuestoDetalles.Show();
        }

        private void button14_Click(object sender, EventArgs e)
        {

        }

        private void button21_Click(object sender, EventArgs e)
        {
            frmImpuestoDetalles OfrmImpuestoDetalles = new frmImpuestoDetalles(inicio.Value, fin.Value, TipoDetalle.DiferenciaFacturasAsentado);
            OfrmImpuestoDetalles.Show();
        }

        private void button20_Click(object sender, EventArgs e)
        {
            frmImpuestoDetalles OfrmImpuestoDetalles = new frmImpuestoDetalles(inicio.Value, fin.Value, TipoDetalle.TotalFacturasTimbradoMonto);
            OfrmImpuestoDetalles.Show();
        }

        private void button24_Click(object sender, EventArgs e)
        {
            frmImpuestoDetalles OfrmImpuestoDetalles = new frmImpuestoDetalles(inicio.Value, fin.Value, TipoDetalle.DiferenciaAsentadoTimbrado);
            OfrmImpuestoDetalles.Show();

            
        }
    }
}
