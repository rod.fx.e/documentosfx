﻿
namespace CxP.Formularios
{
    partial class frmRECEIVER
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmRECEIVER));
            this.button1 = new System.Windows.Forms.Button();
            this.ProveedorBuscar = new System.Windows.Forms.TextBox();
            this.dtg = new System.Windows.Forms.DataGridView();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.ReciboBuscar = new System.Windows.Forms.TextBox();
            this.PURC_ORDER_ID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PURC_ORDER_LINE_NO = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.RECEIVER_ID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.RECEIVER_LINE_NO = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.VAT_CODE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.TAX_PERCENT = new System.Windows.Forms.DataGridViewTextBoxColumn();
            ((System.ComponentModel.ISupportInitialize)(this.dtg)).BeginInit();
            this.SuspendLayout();
            // 
            // button1
            // 
            this.button1.BackColor = System.Drawing.Color.Goldenrod;
            this.button1.FlatAppearance.BorderSize = 0;
            this.button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button1.ForeColor = System.Drawing.Color.White;
            this.button1.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.button1.Location = new System.Drawing.Point(11, 11);
            this.button1.Margin = new System.Windows.Forms.Padding(2);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(82, 24);
            this.button1.TabIndex = 40;
            this.button1.Text = "Buscar";
            this.button1.UseVisualStyleBackColor = false;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // ProveedorBuscar
            // 
            this.ProveedorBuscar.Location = new System.Drawing.Point(73, 39);
            this.ProveedorBuscar.Margin = new System.Windows.Forms.Padding(2);
            this.ProveedorBuscar.Name = "ProveedorBuscar";
            this.ProveedorBuscar.Size = new System.Drawing.Size(108, 20);
            this.ProveedorBuscar.TabIndex = 37;
            // 
            // dtg
            // 
            this.dtg.AllowUserToAddRows = false;
            this.dtg.AllowUserToDeleteRows = false;
            this.dtg.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dtg.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dtg.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.PURC_ORDER_ID,
            this.PURC_ORDER_LINE_NO,
            this.RECEIVER_ID,
            this.RECEIVER_LINE_NO,
            this.VAT_CODE,
            this.TAX_PERCENT});
            this.dtg.Location = new System.Drawing.Point(11, 63);
            this.dtg.Margin = new System.Windows.Forms.Padding(2);
            this.dtg.MultiSelect = false;
            this.dtg.Name = "dtg";
            this.dtg.RowTemplate.Height = 24;
            this.dtg.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dtg.Size = new System.Drawing.Size(778, 376);
            this.dtg.TabIndex = 38;
            this.dtg.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dtg_CellDoubleClick);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 42);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(56, 13);
            this.label1.TabIndex = 41;
            this.label1.Text = "Proveedor";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(186, 42);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(41, 13);
            this.label2.TabIndex = 43;
            this.label2.Text = "Recibo";
            // 
            // ReciboBuscar
            // 
            this.ReciboBuscar.Location = new System.Drawing.Point(232, 39);
            this.ReciboBuscar.Margin = new System.Windows.Forms.Padding(2);
            this.ReciboBuscar.Name = "ReciboBuscar";
            this.ReciboBuscar.Size = new System.Drawing.Size(126, 20);
            this.ReciboBuscar.TabIndex = 42;
            // 
            // PURC_ORDER_ID
            // 
            this.PURC_ORDER_ID.HeaderText = "Compra";
            this.PURC_ORDER_ID.Name = "PURC_ORDER_ID";
            this.PURC_ORDER_ID.ReadOnly = true;
            // 
            // PURC_ORDER_LINE_NO
            // 
            this.PURC_ORDER_LINE_NO.HeaderText = "Linea";
            this.PURC_ORDER_LINE_NO.Name = "PURC_ORDER_LINE_NO";
            this.PURC_ORDER_LINE_NO.ReadOnly = true;
            // 
            // RECEIVER_ID
            // 
            this.RECEIVER_ID.HeaderText = "Recibo";
            this.RECEIVER_ID.Name = "RECEIVER_ID";
            this.RECEIVER_ID.ReadOnly = true;
            // 
            // RECEIVER_LINE_NO
            // 
            this.RECEIVER_LINE_NO.HeaderText = "Linea";
            this.RECEIVER_LINE_NO.Name = "RECEIVER_LINE_NO";
            this.RECEIVER_LINE_NO.ReadOnly = true;
            // 
            // VAT_CODE
            // 
            this.VAT_CODE.HeaderText = "VAT_CODE";
            this.VAT_CODE.Name = "VAT_CODE";
            this.VAT_CODE.Visible = false;
            // 
            // TAX_PERCENT
            // 
            this.TAX_PERCENT.HeaderText = "TAX_PERCENT";
            this.TAX_PERCENT.Name = "TAX_PERCENT";
            this.TAX_PERCENT.Visible = false;
            // 
            // frmRECEIVER
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.ReciboBuscar);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.ProveedorBuscar);
            this.Controls.Add(this.dtg);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "frmRECEIVER";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Recibos";
            ((System.ComponentModel.ISupportInitialize)(this.dtg)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.TextBox ProveedorBuscar;
        private System.Windows.Forms.DataGridView dtg;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox ReciboBuscar;
        private System.Windows.Forms.DataGridViewTextBoxColumn PURC_ORDER_ID;
        private System.Windows.Forms.DataGridViewTextBoxColumn PURC_ORDER_LINE_NO;
        private System.Windows.Forms.DataGridViewTextBoxColumn RECEIVER_ID;
        private System.Windows.Forms.DataGridViewTextBoxColumn RECEIVER_LINE_NO;
        private System.Windows.Forms.DataGridViewTextBoxColumn VAT_CODE;
        private System.Windows.Forms.DataGridViewTextBoxColumn TAX_PERCENT;
    }
}