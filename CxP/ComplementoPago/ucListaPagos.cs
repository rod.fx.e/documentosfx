﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Generales;

namespace CxP.ComplementoPago
{
    public partial class ucListaPagos : UserControl
    {
        private cCONEXCION oData = new cCONEXCION();
        public ucListaPagos()
        {
            InitializeComponent();
            limpiar();
        }
        private void limpiar()
        {
            this.Text = Application.ProductName + " " + Application.ProductVersion;
            DateTime firstDayOfCurrentMonth = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1);
            INICIO.Value = firstDayOfCurrentMonth;
            cargarEmpresas();
        }
        private void cargarEmpresas()
        {

            emisor.Items.Clear();
            emisor.Items.Add("Todos");
            cEMPRESA ocEMPRESA = new cEMPRESA(oData);
            foreach (cEMPRESA registro in ocEMPRESA.todos("", true, Globales.automatico))
            {
                emisor.Items.Add(registro);
            }
            if (emisor.Items.Count > 0)
            {
                emisor.SelectedIndex = 0;
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            cargar();
        }

        private DataTable cargarPagos(cCONEXCION oData_ERPp, cEMPRESA ocEMPRESA)
        {
            string BD_Auxiliar = "";
            if (ocEMPRESA.BD_AUXILIAR != "")
            {
                BD_Auxiliar = ocEMPRESA.BD_AUXILIAR + ".dbo.";
            }
            //Que esten posteados
            string sSQL_Where = " WHERE 1=1 AND cd.AMOUNT<>0 AND cd.POSTING_CADIDATE='Y'";
            string FECHA_INICIO = oData.convertir_fecha(new DateTime(INICIO.Value.Year, INICIO.Value.Month, INICIO.Value.Day, 0, 0, 0), oData_ERPp.sTipo);
            sSQL_Where += " AND cd.CHECK_DATE>=" + FECHA_INICIO + "";
            string FECHA_FINAL = oData.convertir_fecha(new DateTime(FINAL.Value.Year, FINAL.Value.Month, FINAL.Value.Day, 0, 0, 0), oData_ERPp.sTipo);
            sSQL_Where += " AND cd.CHECK_DATE<=" + FECHA_FINAL + "";

            string sSQL = @"
            SELECT cd.BANK_ACCOUNT_ID AS Banco, BANK_ACCOUNT.ACCOUNT_NO AS Cuenta, cd.VENDOR_ID AS Proveedor, cd.CHECK_NO AS 'Pago'
            , cd.AMOUNT AS Monto, cdl.VOUCHER_ID AS Voucher, cdl.AMOUNT AS 'Monto Factura', 
                                     cdl.SELL_RATE AS [Tipo de Cambio], cd.CURRENCY_ID AS Moneda, p.INVOICE_ID AS 'Factura'
            , ISNULL((SELECT TOP 1 a.UUID FROM documentosfx.dbo.archivoSyncSet a
            WHERE a.folio COLLATE SQL_Latin1_General_CP1_CI_AS =p.INVOICE_ID COLLATE SQL_Latin1_General_CP1_CI_AS 
			AND a.rfcEmisor COLLATE SQL_Latin1_General_CP1_CI_AS = v.VAT_REGISTRATION COLLATE SQL_Latin1_General_CP1_CI_AS),'') AS 'UUID'
        , cd.CHECK_DATE AS 'Fecha de Pago', ISNULL
                                         ((SELECT        COUNT(*) + 1 AS Expr1
                                             FROM            CASH_DISBURSEMENT AS cd2 INNER JOIN
                                                                      CASH_DISBURSE_LINE AS cdl2 ON cd2.BANK_ACCOUNT_ID = cdl2.BANK_ACCOUNT_ID AND cd2.CONTROL_NO = cdl2.CONTROL_NO INNER JOIN
                                                                      PAYABLE AS p2 ON cdl2.VOUCHER_ID = p2.VOUCHER_ID
                                             WHERE        (cd2.VENDOR_ID = cd.VENDOR_ID) AND (cdl2.VOUCHER_ID = cdl.VOUCHER_ID) AND (cd2.CHECK_DATE < cd.CHECK_DATE)), 1) AS NumParcialidad, ISNULL
                                         ((SELECT        SUM(cdl2.AMOUNT) AS Expr1
                                             FROM            CASH_DISBURSEMENT AS cd2 INNER JOIN
                                                                      CASH_DISBURSE_LINE AS cdl2 ON cd2.BANK_ACCOUNT_ID = cdl2.BANK_ACCOUNT_ID AND cd2.CONTROL_NO = cdl2.CONTROL_NO INNER JOIN
                                                                      PAYABLE AS p2 ON cdl2.VOUCHER_ID = p2.VOUCHER_ID
                                             WHERE        (cd2.VENDOR_ID = cd.VENDOR_ID) AND (cdl2.VOUCHER_ID = cdl.VOUCHER_ID) AND (cd2.CHECK_DATE < cd.CHECK_DATE)), 0) AS SaldoInsoluto
								             , v.VAT_REGISTRATION as 'RFC'
            FROM            CASH_DISBURSEMENT AS cd INNER JOIN
                                     CASH_DISBURSE_LINE AS cdl ON cd.BANK_ACCOUNT_ID = cdl.BANK_ACCOUNT_ID AND cd.CONTROL_NO = cdl.CONTROL_NO INNER JOIN
                                     PAYABLE AS p ON cdl.VOUCHER_ID = p.VOUCHER_ID INNER JOIN
                                     BANK_ACCOUNT ON cd.BANK_ACCOUNT_ID = BANK_ACCOUNT.ID INNER JOIN
                                     VENDOR as v ON cd.VENDOR_ID = v.ID 
            " + sSQL_Where;
            //Ordenar por fecha de pago
            sSQL += " ORDER BY cd.CHECK_DATE";


            DataTable oDataTable = oData_ERPp.EjecutarConsulta(sSQL);


            return oDataTable;
        }

        private void cargarDataTable(DataTable oDataTable, DataGridView dtg, cCONEXCION oData_ERPp
            , cEMPRESA ocEMPRESA)
        {
            if (oDataTable != null)
            {

            }
        }


        private void cargar()
        {
            try
            {
                dtgrdGeneral.Rows.Clear();
                DataTable oDataTable = new DataTable();
                if (emisor.Text == "Todos")
                {
                    //Navegar por los items
                    foreach (var item in emisor.Items)
                    {
                        try
                        {
                            cEMPRESA ocEMPRESA = item as cEMPRESA;
                            DataTable oDataTabletr = new DataTable();
                            if (ocEMPRESA != null)
                            {
                                cCONEXCION oData_ERPp = new cCONEXCION(ocEMPRESA.TIPO, ocEMPRESA.SERVIDOR
                                    , ocEMPRESA.BD, ocEMPRESA.USUARIO_BD, ocEMPRESA.PASSWORD_BD);
                                if (oDataTable!=null)
                                {
                                    oDataTable.Merge(cargarPagos(oData_ERPp, ocEMPRESA));
                                }
                                else
                                {
                                    oDataTable = cargarPagos(oData_ERPp, ocEMPRESA);
                                }
                                
                            }

                        }
                        catch (Exception error)
                        {
                            ErrorFX.mostrar(error, true, true, "ucListaPagos - 76 - ", true);
                        }
                    }
                }
                else
                {
                    cEMPRESA ocEMPRESA = emisor.SelectedItem as cEMPRESA;
                    if (ocEMPRESA != null)
                    {
                        emisor.Text = ocEMPRESA.ID;
                        cCONEXCION oData_ERPp = new cCONEXCION(ocEMPRESA.TIPO, ocEMPRESA.SERVIDOR, ocEMPRESA.BD, ocEMPRESA.USUARIO_BD, ocEMPRESA.PASSWORD_BD);
                        //cargar_formatos();
                        oDataTable = cargarPagos(oData_ERPp, ocEMPRESA);
                        if (oDataTable != null)
                        {
                            oDataTable.Merge(cargarPagos(oData_ERPp, ocEMPRESA));
                        }
                        else
                        {
                            oDataTable = cargarPagos(oData_ERPp, ocEMPRESA);
                        }
                    }
                }



            }
            catch (Exception e)
            {
                ErrorFX.mostrar(e, true, true, true);
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {

        }
    }
}
