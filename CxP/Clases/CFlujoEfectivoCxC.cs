﻿using System;       
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml;

namespace CxP.Clases
{
    static class CFlujoEfectivoCxC
    {

        static public System.Data.DataTable CxC(DateTime inicio, DateTime fin, string buscar = ""
            , string buscardocumento = ""
            , bool detallado=false)
        {
            try
            {
                Conexion oData = new Conexion();

                string SqlPrincipal = ObtenerPrincipal(fin);

                string SqlDocumentos = @"";

                if (detallado)
                {
                    //Detallado
                    SqlDocumentos = @"
                    SELECT Documento, Fecha, Id,Factura, Nombre, Nacionalidad, FechaPago, [Total Nativo]
                    , TipoCambio, TotalPesos, EstadoPago
                    FROM
                    (
                        " + SqlPrincipal + @"
                    ) as tabla1
                    WHERE 1=1
                    AND (Nombre like '%" + buscar + @"%')
                    AND (Documento like '%" + buscardocumento + @"%')
                    ";
                }
                else
                {
                    //Resumen
                    SqlDocumentos = @"
                    SELECT Id, Nombre, Nacionalidad, SUM(TotalPesos) as TotalPesos,
                    SUM(CASE WHEN DATEDIFF(day, FechaPago, GETDATE()) <= 0 THEN TotalPesos ELSE 0 END) as [Corriente],
                    SUM(CASE WHEN DATEDIFF(day, FechaPago, GETDATE()) BETWEEN 1 AND 30 THEN TotalPesos ELSE 0 END) as [Mas de 30 dias vencidos],
                    SUM(CASE WHEN DATEDIFF(day, FechaPago, GETDATE()) BETWEEN 31 AND 60 THEN TotalPesos ELSE 0 END) as [Mas de 60 dias vencidos],
                    SUM(CASE WHEN DATEDIFF(day, FechaPago, GETDATE()) BETWEEN 61 AND 90 THEN TotalPesos ELSE 0 END) as [Mas de 90 dias vencidos],
                    SUM(CASE WHEN DATEDIFF(day, FechaPago, GETDATE()) > 90 THEN TotalPesos ELSE 0 END) as [Mas de 120 dias vencidos]
                    FROM
                    (
                        " + SqlPrincipal + @"
                    ) as tabla1 
                    WHERE (Documento like '%" + buscardocumento + @"%')
                    GROUP BY Tipo, Id, Nombre, Nacionalidad
                    HAVING 1=1
                    AND (Nombre like '%" + buscar + @"%')
                    
                    ";
                }
                


                System.Data.DataTable oDataTable = oData.EjecutarConsulta(SqlDocumentos);
                return oDataTable;
            }
            catch (Exception error)
            {
                string mensaje = "";
                mensaje += error.Message.ToString();
                if (error.InnerException != null)
                {
                    mensaje += " " + error.InnerException.Message.ToString();
                }
                mensaje += "\n CFlujoEfectivo - 36 - CxC";
                // Invalid file type selected; display an error.
                MessageBox.Show(mensaje,
                                "TotalFacturas",
                                MessageBoxButtons.OK,
                                MessageBoxIcon.Error);
                return null;
            }
        }

        private static string ObtenerConsulta()
        {
            string SqlPrincipal = @"

                SELECT *
                FROM
                (
                    SELECT 'Factura' as Tipo, r.INVOICE_ID as Documento, r.INVOICE_ID as Factura, r.INVOICE_DATE as Fecha, c.ID as 'Id', c.NAME as 'Nombre'
                    ,
                    CASE 
                        WHEN LEFT(c.VAT_REGISTRATION, 4) = 'XEXX' THEN 'Extranjero'
                        ELSE 'Nacional'
                    END AS Nacionalidad
                    , DATEADD(d, r.TERMS_NET_DAYS, r.INVOICE_DATE) as FechaPago
                    , 
                        (ABS(r.TOTAL_AMOUNT) - ABS(PAID_AMOUNT))
                        * 
                        (
                            CASE 
                                WHEN r.TYPE = 'M' THEN -1
                                ELSE 1
                            END
                        )
                        as [Total Nativo]
                    , r.CURRENCY_ID as Moneda, r.SELL_RATE as TipoCambio
                    , 
                        (ABS(r.TOTAL_AMOUNT) - ABS(r.PAID_AMOUNT))
                        * 
                        (
                            CASE 
                                WHEN r.TYPE = 'M' THEN -1
                                ELSE 1
                            END
                        )
                        * 
                        r.SELL_RATE 
                        as TotalPesos
                    ,  
                    CASE 
                        WHEN r.PAID_AMOUNT <> 0 THEN 'Parcial'
                        ELSE 'Completa'
                    END AS EstadoPago

                    FROM RECEIVABLE r
                    INNER JOIN CUSTOMER c ON c.ID = r.CUSTOMER_ID
                    AND ABS(r.TOTAL_AMOUNT) > ABS(r.PAID_AMOUNT)
                    AND r.STATUS <> 'X'
                    AND (ABS(r.TOTAL_AMOUNT) - ABS(r.PAID_AMOUNT))>0.1

                ) as TablaPrincipal
                ";

            return SqlPrincipal;
        }


        private static string ObtenerPrincipal(DateTime fin)
        {
            Conexion oData = new Conexion();

            string SqlPrincipal = @"
            SELECT *
            FROM
            (
                " + ObtenerConsulta() + @"
            ) as TablaPrincipal            
            WHERE TablaPrincipal.Fecha <=" + oData.convertir_fecha(fin, "f") + @"            
            ";

            return SqlPrincipal;
        }

        internal static decimal ObtenerTotal(string idTr, DateTime inicio, DateTime fin, string documento="")
        {
            try
            {
                Conexion oData = new Conexion();

                string SqlDocumentos = @"
                SELECT ISNULL(SUM(TotalPesos),0) as Total
                FROM
                (
                    " + ObtenerConsulta() + @"
                ) as Tabla2
                WHERE Id='" + idTr  + @"' 
                AND Tabla2.FechaPago >=" + oData.convertir_fecha(inicio, "i") + @"
                AND Tabla2.FechaPago <=" + oData.convertir_fecha(fin, "f") + @"
                ";

                if (!String.IsNullOrEmpty(documento))
                {
                    SqlDocumentos += " AND Tabla2.Documento='" + documento + "'";
                }

                DataTable oDataTable = oData.EjecutarConsulta(SqlDocumentos);
                if (oDataTable != null)
                {
                    foreach (DataRow oDataRow in oDataTable.Rows)
                    {
                        //Añadir el Registro
                        return Math.Round(decimal.Parse(oDataRow["Total"].ToString()), 4);
                    }
                }
                return 0;
            }
            catch (Exception error)
            {
                string mensaje = "";
                mensaje += error.Message.ToString();
                if (error.InnerException != null)
                {
                    mensaje += " " + error.InnerException.Message.ToString();
                }
                mensaje += "\n Cobranza - 36 - ObtenerTotal";
                // Invalid file type selected; display an error.
                MessageBox.Show(mensaje,
                                "ObtenerTotal",
                                MessageBoxButtons.OK,
                                MessageBoxIcon.Error);
                return 0;
            }
        }
    }
}
