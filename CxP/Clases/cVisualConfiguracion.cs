﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;

namespace CxP.Clases
{
    public sealed class cVisualConfiguracion
    {
        public static string visualInstancia { get; set; }
        public static string visualUsuario { get; set; }
        public static string visualPassword { get; set; }


        public static void obtener()
        {
            try
            {
                visualInstancia = ConfigurationManager.AppSettings["visualInstancia"].ToString();
                visualUsuario = ConfigurationManager.AppSettings["visualUsuario"].ToString();
                visualPassword = ConfigurationManager.AppSettings["visualPassword"].ToString();
            }
            catch (Exception error)
            {
                string mensaje = "cVisualConfiguracion - 16 - obtener ";
                mensaje += error.Message.ToString();
                if (error.InnerException != null)
                {
                    mensaje += " " + error.InnerException.Message.ToString();
                }
                throw new Exception(mensaje);
            }
        }

    }


}
