﻿using System;       
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml;

namespace CxP.Clases
{
    static class CFlujoEfectivo
    {

        static public System.Data.DataTable CxC(DateTime inicio, DateTime fin, string buscar = ""
            , string buscardocumento = ""
            , bool detallado=false)
        {
            try
            {
                Conexion oData = new Conexion();

                string SqlPrincipal = ObtenerPrincipal(fin);

                string SqlDocumentos = @"";

                if (detallado)
                {
                    //Detallado
                    SqlDocumentos = @"
                    SELECT Tipo, Documento, Fecha, Id,Factura, Nombre, Nacionalidad, FechaPago, [Total Nativo], TipoCambio, TotalPesos, EstadoPago
                    FROM
                    (
                        " + SqlPrincipal + @"
                    ) as tabla1
                    WHERE 1=1
                    AND (Nombre like '%" + buscar + @"%')
                    AND (Documento like '%" + buscardocumento + @"%')
                    ";
                }
                else
                {
                    //Resumen
                    SqlDocumentos = @"
                    SELECT Tipo, Id, Nombre, Nacionalidad, SUM(TotalPesos) as TotalPesos
                    FROM
                    (
                        " + SqlPrincipal + @"
                    ) as tabla1 
                    WHERE (Documento like '%" + buscardocumento + @"%')
                    GROUP BY Tipo, Id, Nombre, Nacionalidad
                    HAVING 1=1
                    AND (Nombre like '%" + buscar + @"%')
                    
                    ";
                }
                


                System.Data.DataTable oDataTable = oData.EjecutarConsulta(SqlDocumentos);
                return oDataTable;
            }
            catch (Exception error)
            {
                string mensaje = "";
                mensaje += error.Message.ToString();
                if (error.InnerException != null)
                {
                    mensaje += " " + error.InnerException.Message.ToString();
                }
                mensaje += "\n CFlujoEfectivo - 36 - CxC";
                // Invalid file type selected; display an error.
                MessageBox.Show(mensaje,
                                "TotalFacturas",
                                MessageBoxButtons.OK,
                                MessageBoxIcon.Error);
                return null;
            }
        }

        private static string ObtenerConsulta()
        {
            string SQLGastosFijo = ObtenerConsultaGastosFijo();
            string SqlPrincipal = @"

                SELECT *
                FROM
                (
                    SELECT 'Factura' as Tipo, r.INVOICE_ID as Documento, r.INVOICE_ID as Factura, r.INVOICE_DATE as Fecha, c.ID as 'Id', c.NAME as 'Nombre'
                    ,
                    CASE 
                        WHEN LEFT(c.VAT_REGISTRATION, 4) = 'XEXX' THEN 'Extranjero'
                        ELSE 'Nacional'
                    END AS Nacionalidad
                    , DATEADD(d, r.TERMS_NET_DAYS, r.INVOICE_DATE) as FechaPago
                    , r.TOTAL_AMOUNT - PAID_AMOUNT as [Total Nativo]
                    , r.CURRENCY_ID as Moneda, r.SELL_RATE as TipoCambio
                    , (r.TOTAL_AMOUNT - r.PAID_AMOUNT) * r.SELL_RATE as TotalPesos
                    ,  
                    CASE 
                        WHEN r.PAID_AMOUNT <> 0 THEN 'Parcial'
                        ELSE 'Completa'
                    END AS EstadoPago

                    FROM RECEIVABLE r
                    INNER JOIN CUSTOMER c ON c.ID = r.CUSTOMER_ID
                    AND r.TOTAL_AMOUNT > r.PAID_AMOUNT
                    AND r.STATUS <> 'X'
                    AND ABS(r.TOTAL_AMOUNT - r.PAID_AMOUNT)>0.1


                    UNION ALL

                    SELECT 'Voucher' as Tipo, p.VOUCHER_ID as Documento, p.INVOICE_ID as Factura, p.INVOICE_DATE as Fecha, v.ID as 'Id', v.NAME as 'Nombre'
                    ,CASE 
                        WHEN LEFT(v.VAT_REGISTRATION, 4) = 'XEXX' THEN 'Extranjero'
                        ELSE 'Nacional'
                    END AS Nacionalidad
                    , ISNULL(p.PAYMENT_DATE,DATEADD(d, p.TERMS_NET_DAYS, p.INVOICE_DATE)) as FechaPago
                    , ABS(p.TOTAL_AMOUNT - PAID_AMOUNT)*-1 as [Total Nativo]
                    , p.CURRENCY_ID as Moneda, p.SELL_RATE as TipoCambio
                    , ABS((p.TOTAL_AMOUNT - p.PAID_AMOUNT) * p.SELL_RATE)*-1 as TotalPesos
                    ,  
                    CASE 
                        WHEN p.PAID_AMOUNT <> 0 THEN 'Parcial'
                        ELSE 'Completa'
                    END AS EstadoPago

                    FROM PAYABLE p
                    INNER JOIN VENDOR v ON v.ID = p.VENDOR_ID
                    AND p.TOTAL_AMOUNT > PAID_AMOUNT
                    AND p.PAY_STATUS <> 'X'

                    AND ABS(p.TOTAL_AMOUNT - p.PAID_AMOUNT)>0.1

                    UNION ALL

                    " + SQLGastosFijo +  @"
                ) as TablaPrincipal
                ";

            return SqlPrincipal;
        }

        private static string ObtenerConsultaGastosFijo()
        {
            

            ModeloDocumentosFX.ModeloDocumentosFXContainer db = new ModeloDocumentosFX.ModeloDocumentosFXContainer();
            string BaseDatos = db.Database.Connection.Database;

            string Sql = @"

                    SELECT 'Gasto Fijo' as Tipo, 'Fijo' as Documento, '' as Factura, DATEADD(month, DATEDIFF(month, 0, GETDATE()), 0) as Fecha, gf.Proveedor as 'Id', gf.Nombre as 'Nombre'
                    , 'Nacional' as 'Nacionalidad'

                    , DATEADD(month, DATEDIFF(month, 0, GETDATE()), 0) as FechaPago
                    , ABS(gf.Monto) *-1 as [Total Nativo]
                    , 'MXN' as Moneda, 1 as TipoCambio
                    , ABS(gf.Monto) *-1 as TotalPesos
                    , 'Completa' AS EstadoPago
                    FROM " + BaseDatos + @"..[GastoFijoSet] gf                
                ";

            return Sql;
        }

        private static string ObtenerPrincipal(DateTime fin)
        {
            Conexion oData = new Conexion();

            string SqlPrincipal = @"
            SELECT *
            FROM
            (
                " + ObtenerConsulta() + @"
            ) as TablaPrincipal            
            WHERE TablaPrincipal.Fecha <=" + oData.convertir_fecha(fin, "f") + @"            
            ";

            return SqlPrincipal;
        }

        internal static decimal ObtenerTotal(string idTr, string tipoTr, DateTime inicio, DateTime fin, string documento="")
        {
            try
            {
                Conexion oData = new Conexion();

                string SqlDocumentos = @"
                SELECT ISNULL(SUM(TotalPesos),0) as Total
                FROM
                (
                    " + ObtenerConsulta() + @"
                ) as Tabla2
                WHERE Id='" + idTr  + @"' AND Tipo='" + tipoTr + @"'
                AND Tabla2.FechaPago >=" + oData.convertir_fecha(inicio, "i") + @"
                AND Tabla2.FechaPago <=" + oData.convertir_fecha(fin, "f") + @"
                ";

                if (!String.IsNullOrEmpty(documento))
                {
                    SqlDocumentos += " AND Tabla2.Documento='" + documento + "'";
                }

                DataTable oDataTable = oData.EjecutarConsulta(SqlDocumentos);
                if (oDataTable != null)
                {
                    foreach (DataRow oDataRow in oDataTable.Rows)
                    {
                        //Añadir el Registro
                        return Math.Round(decimal.Parse(oDataRow["Total"].ToString()), 4);
                    }
                }
                return 0;
            }
            catch (Exception error)
            {
                string mensaje = "";
                mensaje += error.Message.ToString();
                if (error.InnerException != null)
                {
                    mensaje += " " + error.InnerException.Message.ToString();
                }
                mensaje += "\n CFlujoEfectivo - 36 - ObtenerTotal";
                // Invalid file type selected; display an error.
                MessageBox.Show(mensaje,
                                "ObtenerTotal",
                                MessageBoxButtons.OK,
                                MessageBoxIcon.Error);
                return 0;
            }
        }
    }
}
