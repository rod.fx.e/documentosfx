﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CxP.Clases
{
    public enum TipoDetalle
    {
        TotalFacturas,
        TotalFacturasPEsos,
        TotalFacturasAsentado,
        TotalFacturasTimbrado,
        TotalFacturasTimbradoMonto,
        TotalFacturasCobrado,
        TotalFacturasAsentadoCobrado,
        TotalFacturasCP,
        DiferenciaFacturasAsentado,
        DiferenciaAsentadoTimbrado
    }

}
