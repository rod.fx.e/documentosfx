﻿using ModeloERP;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CxP.Clases
{
    class Vendor
    {
        private ModeloERP.ERPEntities db = new ModeloERP.ERPEntities();
        string SITE = "";
        public string ID { get; set; }
        public string NAME { get; set; }
        public string USER_1 { get; set; }
        public string BUYER { get; set; }

        public string ADDR_1 { get; set; }
        public string ADDR_2 { get; set; }
        public string ADDR_3 { get; set; }
        public string CITY { get; set; }
        public string STATE { get; set; }
        public string ZIPCODE { get; set; }
        public string COUNTRY { get; set; }

        public string VAT_REGISTRATION { get; set; }
        public string VAT_CODE { get; set; }
    public string CURRENCY_ID { get; set; }
        public string DEF_PAYB_ACCT_ID { get; set; }
        public string CONTACT_FIRST_NAME { get; internal set; }
        public string CONTACT_EMAIL { get; internal set; }
        public string CONTACT_PHONE { get; internal set; }

        public string CONTACT_FIRST_NAME_1 { get; internal set; }
        public string CONTACT_EMAIL_1 { get; internal set; }
        public string CONTACT_PHONE_1 { get; internal set; }

        public string CONTACT_FIRST_NAME_2 { get; internal set; }
        public string CONTACT_EMAIL_2 { get; internal set; }
        public string CONTACT_PHONE_2 { get; internal set; }

        Conexion oData = new Conexion();
        DateTime FechaCreacion;

        public Vendor()
        {
            oData = new Conexion();
            FechaCreacion = DateTime.Now;
            //Obtener el SITE
            ModeloERP.SITE oSITE = db.SITE.FirstOrDefault();
            if (oSITE != null)
            {
                SITE = oSITE.ID;
            }
        }

        public bool Procesar()
        {
            try
            {
                cVisualConfiguracion.obtener();

                Lsa.Data.Dbms.OpenLocal(cVisualConfiguracion.visualInstancia
                    , cVisualConfiguracion.visualUsuario
                    , cVisualConfiguracion.visualPassword);

                Lsa.Vmfg.Purchasing.Vendor OVendor = new Lsa.Vmfg.Purchasing.Vendor(cVisualConfiguracion.visualInstancia);
                Lsa.Data.DataRow dr = OVendor.NewVendorRow(ID);
                dr["ENTITY_ID"] = "E-MMC";
                dr["SITE_ID"] = SITE;
                dr["CURRENCY_ID"] = CURRENCY_ID;
                dr["NAME"] = NAME;
                dr["VAT_REGISTRATION"] = VAT_REGISTRATION;
                dr["VAT_CODE"] = VAT_CODE;
                dr["USER_1"] = USER_1;
                dr["BUYER"] = BUYER;
                dr["ADDR_1"] = ADDR_1;
                dr["ADDR_2"] = ADDR_2;
                dr["ADDR_3"] = ADDR_3;
                dr["CITY"] = CITY;
                dr["STATE"] = STATE;
                dr["ZIPCODE"] = ZIPCODE;
                dr["COUNTRY"] = COUNTRY;

                dr["DEF_PAYB_ACCT_ID"] = DEF_PAYB_ACCT_ID;
                //dr["ADDR_1"] = "1234 East 5th Street";
                //dr["ADDR_2"] = "Dock #2";
                //dr["CITY"] = "Boston";
                //dr["STATE"] = "MA";
                //dr["ZIPCODE"] = "01945-911";
                //dr["COUNTRY"] = "USA";
                dr["CONTACT_FIRST_NAME"] = CONTACT_FIRST_NAME;
                dr["CONTACT_EMAIL"] = CONTACT_EMAIL;
                //dr["CONTACT_LAST_NAME"] = "Brown";
                //dr["CONTACT_INITIAL"] = "J.";
                //dr["CONTACT_POSITION"] = "General Manager";
                //dr["CONTACT_HONORIFIC"] = "Mr.";
                //dr["CONTACT_SALUTATION"] = "Dear Mr. Brown:";
                dr["CONTACT_PHONE"] = CONTACT_PHONE;
                //dr["BILL_TO_NAME"] = "Able Manufacturing";
                //dr["USER_1"] = "ABLMAN First Customer User Defined Field";
                //dr["TAX_EXEMPT"] = "Y";
                dr["REPORT_1099_MISC"] = "N";
                dr["TERMS_NET_TYPE"] = "N";
                dr["TERMS_DISC_TYPE"] = "D";
                dr["PAYMENT_METHOD"] = "C";
                dr["MATCH_TYPE"] = "N";
                dr["VAT_DISCOUNTED"] = "N";
                dr["VAT_ALWAYS_DISC"] = "N";
                dr["VAT_OVERRIDE_SEQ"] = "P";
                dr["VAT_BOOK_CODE_I"] = "AP";
                dr["VAT_BOOK_CODE_M"] = "AP";


                //(([REPORT_1099_MISC] = 'Y' OR[REPORT_1099_MISC] = 'N') 
                //    AND([TERMS_NET_TYPE] = 'A' OR[TERMS_NET_TYPE] = 'M' OR[TERMS_NET_TYPE] = 'D'
                //    OR[TERMS_NET_TYPE] = 'N' OR[TERMS_NET_TYPE] = 'E' OR[TERMS_NET_TYPE] = 'I') 
                //    AND([TERMS_DISC_TYPE] = 'A' OR[TERMS_DISC_TYPE] = 'M' OR[TERMS_DISC_TYPE] = 'D'
                //    OR[TERMS_DISC_TYPE] = 'N' OR[TERMS_DISC_TYPE] = 'E') 
                //    AND([MATCH_TYPE] = 'R' OR[MATCH_TYPE] = 'E' OR[MATCH_TYPE] = 'N') 
                //    AND([PAYMENT_METHOD] = 'C' OR[PAYMENT_METHOD] = 'F' OR[PAYMENT_METHOD] = 'B') 
                //    AND([VAT_DISCOUNTED] = 'Y' OR[VAT_DISCOUNTED] = 'N') 
                //    AND([VAT_ALWAYS_DISC] = 'Y' OR[VAT_ALWAYS_DISC] = 'N') 
                //    AND([VAT_OVERRIDE_SEQ] = 'V' OR[VAT_OVERRIDE_SEQ] = 'P'))

                OVendor.Save();

                if (!String.IsNullOrEmpty(CONTACT_FIRST_NAME))
                {
                    //Insertar el Vendor Contact
                    string Sql = $@"
                    INSERT INTO VENDOR_CONTACT ([VENDOR_ID]
                      ,[CONTACT_NO]
                      ,[CONTACT_FIRST_NAME]
                      ,[CONTACT_PHONE]
                      ,[CONTACT_EMAIL])
                    VALUES
                    ('{ID}',1,'{CONTACT_FIRST_NAME}','{CONTACT_PHONE}','{CONTACT_EMAIL}')
                        ";
                    oData = new Conexion();
                    oData.EjecutarConsulta(Sql);

                    string CONTACT_ID = ObtenerContactId();
                    //Insertar el Vendor Contact
                    Sql = $@"
                    INSERT INTO CONTACT ([ID],[FIRST_NAME]
                      ,[PHONE]
                      ,[EMAIL])
                    VALUES
                    ('{CONTACT_ID}','{CONTACT_FIRST_NAME}','{CONTACT_PHONE}','{CONTACT_EMAIL}')
                        ";
                    oData.EjecutarConsulta(Sql);

                    //CONTACT_ID = ObtenerContactId(CONTACT_EMAIL);
                    //Insertar el Vendor Contact
                    Sql = $@"
                    INSERT INTO VEND_CONTACT (
                       [VENDOR_ID]
                      ,[CONTACT_NO]
                      ,[CONTACT_ID]
                      ,[PRIMARY_CONTACT])
                    VALUES
                    ('{ID}',1,'{CONTACT_ID}','Y')
                        ";
                    oData.EjecutarConsulta(Sql);

                }
/*
                if (!String.IsNullOrEmpty(CONTACT_FIRST_NAME_1) 
                    && !String.IsNullOrEmpty(CONTACT_PHONE_1)
                    && !String.IsNullOrEmpty(CONTACT_EMAIL_1))
                {
                    //Insertar el Vendor Contact
                    string Sql = $@"
                    INSERT INTO VENDOR_CONTACT ([VENDOR_ID]
                      ,[CONTACT_NO]
                      ,[CONTACT_FIRST_NAME]
                      ,[CONTACT_PHONE]
                      ,[CONTACT_EMAIL])
                    VALUES
                    ('{ID}',2,'{CONTACT_FIRST_NAME_1}','{CONTACT_PHONE_1}','{CONTACT_EMAIL_1}')
                        ";
                    oData = new Conexion();
                    oData.EjecutarConsulta(Sql);
                }

                if (!String.IsNullOrEmpty(CONTACT_FIRST_NAME_2)
                    && !String.IsNullOrEmpty(CONTACT_PHONE_2)
                    && !String.IsNullOrEmpty(CONTACT_EMAIL_2))
                {
                    //Insertar el Vendor Contact
                    string Sql = $@"
                    INSERT INTO VENDOR_CONTACT ([VENDOR_ID]
                      ,[CONTACT_NO]
                      ,[CONTACT_FIRST_NAME]
                      ,[CONTACT_PHONE]
                      ,[CONTACT_EMAIL])
                    VALUES
                    ('{ID}',2,'{CONTACT_FIRST_NAME_2}','{CONTACT_PHONE_2}','{CONTACT_EMAIL_2}')
                        ";
                    oData = new Conexion();
                    oData.EjecutarConsulta(Sql);
                }
*/
                return true;
            }
            catch (Exception e)
            {
                throw e;
            }
            return false;
        }

        private string ObtenerContactId()
        {
            // Consulta para obtener el último ID
            string Sql = "SELECT TOP 1 ID FROM CONTACT ORDER BY ROWID DESC";

            // Ejecutar la consulta y obtener el resultado
            System.Data.DataTable oDataTableR = oData.EjecutarConsulta(Sql);

            if (oDataTableR != null && oDataTableR.Rows.Count > 0)
            {
                // Obtener el ID actual y convertirlo en entero
                int currentId = int.Parse(oDataTableR.Rows[0]["ID"].ToString());

                // Incrementar el ID en 1
                int newId = currentId + 1;

                // Formatear el nuevo ID a 5 dígitos con ceros a la izquierda
                return newId.ToString("D5"); // Esto garantiza que el ID tenga un formato de cinco dígitos, como 00823
            }

            return "00001"; // Valor inicial en caso de que no haya un ID existente en la tabla
        }


        public string ObtenerIdNuevo()
        {
            string Sql = "SELECT TOP 1  dbo.ObtenerNuevoId() as NuevoId FROM PART"; // Asegúrate de que la función esté en el esquema dbo

            // Ejecutar la consulta y obtener el resultado
            System.Data.DataTable oDataTableR = oData.EjecutarConsulta(Sql);

            if (oDataTableR != null && oDataTableR.Rows.Count > 0)
            {
                return oDataTableR.Rows[0]["NuevoId"].ToString(); // Utiliza el nombre de la columna alias "NuevoId"
            }

            return string.Empty;
        }
        public bool ObtenerVatRegistration(string rfc)
        {
            // Obtener todos los IDs que comienzan con "P" y extraer solo la parte numérica
            var proveedor = db.VENDOR
                                .Where(v => v.ID.Equals(rfc))
                                .FirstOrDefault();
            if (proveedor==null)
            {
                return false;
            }


            return true;
        }
            public string ObtenerId()
            {
            // Obtener todos los IDs que comienzan con "P" y extraer solo la parte numérica
            var idsConNumeros = db.VENDOR
                                .Where(v => v.ID.StartsWith("P"))
                                .Select(v => new
                                {
                                    ID = v.ID,
                                    Numero = ObtenerNumeroDeId(v.ID)
                                })
                                .OrderBy(v => v.Numero)
                                .ToList();

            int nuevoNumero = 1; // Iniciar en 1 si no se encuentra ningún ID existente

            if (idsConNumeros.Any())
            {
                var ultimoId = idsConNumeros.LastOrDefault();

                // Si el último ID tiene un número válido, se incrementa en 1
                if (ultimoId != null && ultimoId.Numero.HasValue)
                {
                    nuevoNumero = ultimoId.Numero.Value + 1;
                }
            }

            string nuevoId = $"P{nuevoNumero}";
            MessageBox.Show(nuevoId);
            // Validar si el nuevo ID existe en la tabla con prefijo "PD"
            while (db.VENDOR.Any(v => v.ID == nuevoId || v.ID == $"PD{nuevoNumero}"))
            {
                nuevoNumero++;
                nuevoId = $"P{nuevoNumero}";
            }

            // Retornar el ID final asegurado que no exista
            return nuevoId;
        }

        // Método para extraer el número de un ID
        private int? ObtenerNumeroDeId(string id)
        {
            string numeroStr = new string(id.Where(char.IsDigit).ToArray());

            if (int.TryParse(numeroStr, out int numero))
            {
                return numero;
            }

            return null; // Retorna null si no se pudo parsear el número
        }


    }
}
