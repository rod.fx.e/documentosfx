﻿using CFDI40;
using Generales;
using ModeloERP;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml;
using System.Xml.Serialization;

namespace CxP.Clases
{
    class cPAYABLE_LINE
    {
        public int LINE_NO { get; set; }
        public string REFERENCE { get; set; }
        public decimal QTY { get; set; }
        public decimal AMOUNT { get; set; }

        public string VAT_CODE { get; set; }
        public decimal VAT_PERCENT { get; set; }
        public decimal VAT_AMOUNT { get; set; }
        public string VAT_GL_ACCT_ID { get; set; }
        public string GL_ACCOUNT_ID { get; set; }
        public int FIXED_CHARGE { get; internal set; }
        public string DISC_ALLOWED { get; internal set; }
        public string COST_CATEGORY { get; internal set; }
        public int DUTY_PERCENT { get; internal set; }
        public int DUTY_AMOUNT { get; internal set; }
        public string DUTY_GL_ACCT_ID { get; internal set; }
        public string RECEIVER_ID { get; internal set; }

        public string RECEIVER_LINE_NO { get; internal set; }
        public string PURC_ORDER_ID { get; internal set; }
        public string PURC_ORDER_LINE_NO { get; internal set; }
        public object PO_RECV_QTY { get; internal set; }
        public object PO_DISC_PERCENT { get; internal set; }
        public object PO_GL_ACCOUNT_ID { get; internal set; }
        public object PO_UNIT_PRICE { get; internal set; }
        public object ACT_PO_UNIT_PRICE { get; internal set; }
        public object PRODUCT_CODE { get; internal set; }
        public object VAT_IE_PERCENT { get; internal set; }
        public object VAT_IE_AMOUNT { get; internal set; }
    }
    class cPAYABLE
    {
        private ModeloERP.ERPEntities db = new ModeloERP.ERPEntities();
        string SITE = "";
        private Conexion oData;
        public string VOUCHER_ID { get; set; }
        public string CURRENCY_ID { get; set; }
        public string INVOICE_ID { get; set; }
        public DateTime INVOICE_DATE {get; set; }
        public DateTime FechaCreacion { get; set; }
        public decimal TOTAL_AMOUNT { get; private set; }
        public List<cPAYABLE_LINE> lineas { get; set; }
        public cPAYABLE()
        {
            oData = new Conexion();
            FechaCreacion = DateTime.Now;
            //Obtener el SITE
            ModeloERP.SITE oSITE = db.SITE.FirstOrDefault();
            if (oSITE != null)
            {
                SITE = oSITE.ID;
            }
        }

        public CFDI40.Comprobante oComprobante;
        public VENDOR OVENDOR;

        public bool Eliminar(string voucher)
        {
            try
            {
                cVisualConfiguracion.obtener();

                Lsa.Data.Dbms.OpenLocal(cVisualConfiguracion.visualInstancia
                    , cVisualConfiguracion.visualUsuario
                    , cVisualConfiguracion.visualPassword);

                Lsa.Vmfg.Financials.SFPayables OSFPayables = new Lsa.Vmfg.Financials.SFPayables(cVisualConfiguracion.visualInstancia);
                OSFPayables.Find(voucher);
                
                return true;
            }
            catch (Exception error)
            {
                string mensaje = "";
                mensaje += error.Message.ToString();
                if (error.InnerException != null)
                {
                    mensaje += " " + error.InnerException.Message.ToString();
                }
                mensaje += "\n cPAYABLE - 847 - Eliminar";
                // Invalid file type selected; display an error.
                MessageBox.Show(mensaje,
                                "Generación de CxP",
                                MessageBoxButtons.OK,
                                MessageBoxIcon.Error);
                return false;
            }
        }
            public bool ProcesarCFDI(string filename, bool detallado, DateTime? fechaCreacion = null
                , string recibo=""
                , string solicitudServicioTr = ""
                , string solicitudServicioNotasTr = ""
                , string solicitudServicioTipoTr = "")
        {
            OVENDOR = new VENDOR();
            //Pasear el comprobante
            try
            {
                if (fechaCreacion != null)
                {
                    FechaCreacion = DateTime.Parse(fechaCreacion.ToString());
                }
                
                XmlSerializer serializer_CFD_3 = new XmlSerializer(typeof(CFDI40.Comprobante));
                TextReader reader = new StreamReader(filename);
                oComprobante = (CFDI40.Comprobante)serializer_CFD_3.Deserialize(reader);
                reader.Dispose();
                reader.Close();
                if (CargarProveedor(oComprobante.Emisor.Rfc, oComprobante.Moneda))
                {
                    INVOICE_ID = string.Empty;
                    INVOICE_ID = ObtenerFactura(oComprobante, filename);

                    INVOICE_DATE = oComprobante.Fecha;
                    TOTAL_AMOUNT = Math.Round(oComprobante.Total,2, MidpointRounding.AwayFromZero);
                    CURRENCY_ID = ObtenerMoneda(oComprobante.Moneda);
                    if (detallado)
                    {
                        CargarConceptos(oComprobante, fechaCreacion, recibo);
                    }
                    else
                    {
                        CargarLineas(oComprobante, fechaCreacion, recibo);
                    }

                    return true;
                }
                return false;
            }
            catch (Exception e)
            {
                throw e;
            }

        }

        private string ObtenerFactura(Comprobante oComprobante, string xml)
        {
            string InvoiceId=string.Empty;
            if (oComprobante.Serie != null)
            {
                InvoiceId += oComprobante.Serie;
            }
            if (oComprobante.Folio != null)
            {
                InvoiceId += oComprobante.Folio;
            }
            if (String.IsNullOrEmpty(InvoiceId))
            {
                //Cargar el Folio Fiscal
                XmlDocument doc = new XmlDocument();
                doc.Load(xml);
                XmlNodeList TimbreFiscalDigital = doc.GetElementsByTagName("tfd:TimbreFiscalDigital");
                string UUID = "";
                try
                {
                    UUID = TimbreFiscalDigital[0].Attributes["UUID"].Value;
                }
                catch
                {

                }
                if (UUID.Length>8)
                {
                    InvoiceId = UUID.Substring(UUID.Length-8,8);
                }

            }
            return InvoiceId;
        }

        private string ObtenerMoneda(string moneda)
        {
            //Cargar el ultimo recibo sin CxP
            string Sql = @"
                SELECT TOP 1 c.ID
                FROM CURRENCY c 
                WHERE c.ISO_CODE='" + moneda + @"'
                ";
            System.Data.DataTable oDataTableR = oData.EjecutarConsulta(Sql);
            if (oDataTableR != null)
            {
                foreach (System.Data.DataRow oDataRowR in oDataTableR.Rows)
                {
                    return  oDataRowR["ID"].ToString();
                }
            }
            return string.Empty;
        }

        private void Constantes(cPAYABLE_LINE OcPAYABLE_LINE)
        {
            OcPAYABLE_LINE.FIXED_CHARGE = 0;
            OcPAYABLE_LINE.DISC_ALLOWED = "Y";
            OcPAYABLE_LINE.COST_CATEGORY = "M";
            OcPAYABLE_LINE.DUTY_PERCENT = 0;
            OcPAYABLE_LINE.DUTY_AMOUNT = 0;
            OcPAYABLE_LINE.DUTY_GL_ACCT_ID = "M";
        }

        private void CargarImpuesto(cPAYABLE_LINE OcPAYABLE_LINE, CFDI40.ComprobanteImpuestos impuestos)
        {
            if (impuestos!=null)
            {
                if (impuestos.TotalImpuestosTrasladados != 0)
                {
                    foreach (CFDI40.ComprobanteImpuestosTraslado impuesto in impuestos.Traslados)
                    {
                        OcPAYABLE_LINE.VAT_PERCENT = Math.Round(impuesto.TasaOCuota * 100, 3);
                        OcPAYABLE_LINE.VAT_AMOUNT = impuesto.Importe;

                        if (impuesto.TasaOCuota == (decimal)0.16)
                        {
                            //Agregar regla                            
                            OcPAYABLE_LINE.VAT_GL_ACCT_ID = "10400002";
                        }
                    }
                }                
            }
        }

        internal void ActualizarRecibo(int linea, string recibo, string lineaRecibo, string oc, string lineaOC)
        {
            // Buscar la línea que coincida con los parámetros recibidos.
            /*
            var lineaParaActualizar = lineas.FirstOrDefault(l => l.RECEIVER_ID == recibo && l.RECEIVER_LINE_NO == lineaRecibo);

            // Si se encuentra la línea, actualizar los valores.
            if (lineaParaActualizar != null)
            {
                lineaParaActualizar.RECEIVER_ID = reciboNuevo;
                lineaParaActualizar.RECEIVER_LINE_NO = lineaNuevo;
            }
            */

            var lineaParaActualizar = lineas.FirstOrDefault(l => l.LINE_NO == linea);

            if (lineaParaActualizar != null)
            {
                lineaParaActualizar.RECEIVER_ID = recibo;
                lineaParaActualizar.RECEIVER_LINE_NO = lineaRecibo;
                lineaParaActualizar.PURC_ORDER_ID = oc;
                lineaParaActualizar.PURC_ORDER_LINE_NO = lineaOC;

                string Sql = @"
                SELECT TOP 1 rl.PURC_ORDER_ID, rl.PURC_ORDER_LINE_NO, rl.RECEIVER_ID, rl.LINE_NO, v.TAX_GL_ACCT_ID
                ,COALESCE(pol.GL_EXPENSE_ACCT_ID, p.INV_MAT_GL_ACCT_ID) as Cuenta
                , rl.UNIT_PRICE*rl.RECEIVED_QTY as TotalLinea
                ,rl.RECEIVED_QTY as PO_RECV_QTY
                ,pol.PRODUCT_CODE
                ,pol.UNIT_PRICE
                ,pol.VAT_CODE
                FROM RECEIVER_LINE rl
                INNER JOIN PURC_ORDER_LINE pol ON pol.PURC_ORDER_ID = rl.PURC_ORDER_ID AND pol.LINE_NO = rl.PURC_ORDER_LINE_NO
                INNER JOIN PURCHASE_ORDER po ON pol.PURC_ORDER_ID = po.ID
                left join PRODUCT p ON p.CODE = pol.PRODUCT_CODE
                left join VAT v ON v.CODE=pol.VAT_CODE
                WHERE NOT EXISTS (
                    SELECT 1
                    FROM PAYABLE_LINE pl
                    WHERE pl.PURC_ORDER_ID = rl.PURC_ORDER_ID
                    AND pl.PURC_ORDER_LINE_NO = rl.PURC_ORDER_LINE_NO
                    AND pl.RECEIVER_ID = rl.RECEIVER_ID
                    AND pl.RECEIVER_LINE_NO = rl.LINE_NO
                )
                AND po.VENDOR_ID like '" + OVENDOR.ID + @"'
                AND rl.RECEIVER_ID='" + lineaParaActualizar.RECEIVER_ID  + "' AND rl.LINE_NO=" + lineaParaActualizar.RECEIVER_LINE_NO + @"
                ORDER BY rl.RECEIVER_ID
                ";
                System.Data.DataTable oDataTableR = oData.EjecutarConsulta(Sql);
                if (oDataTableR != null)
                {
                    foreach (System.Data.DataRow oDataRowR in oDataTableR.Rows)
                    {
                        lineaParaActualizar.GL_ACCOUNT_ID = oDataRowR["Cuenta"].ToString();
                        lineaParaActualizar.VAT_GL_ACCT_ID = oDataRowR["TAX_GL_ACCT_ID"].ToString();
                        lineaParaActualizar.VAT_CODE = oDataRowR["VAT_CODE"].ToString();

                        lineaParaActualizar.PO_RECV_QTY = oDataRowR["PO_RECV_QTY"].ToString();
                        lineaParaActualizar.PO_DISC_PERCENT = 0;//oDataRowR["TAX_GL_ACCT_ID"].ToString();
                        lineaParaActualizar.PO_GL_ACCOUNT_ID = "20101998";//oDataRowR["PO_GL_ACCOUNT_ID"].ToString();
                        lineaParaActualizar.PRODUCT_CODE = oDataRowR["PRODUCT_CODE"].ToString();
                        lineaParaActualizar.PO_UNIT_PRICE = oDataRowR["UNIT_PRICE"].ToString();

                        lineaParaActualizar.DUTY_PERCENT = 0;
                        lineaParaActualizar.DUTY_AMOUNT = 0;
                        lineaParaActualizar.DUTY_GL_ACCT_ID = "20101999";

                        lineaParaActualizar.ACT_PO_UNIT_PRICE = oDataRowR["UNIT_PRICE"].ToString();

                        lineaParaActualizar.VAT_IE_AMOUNT = 0.0;
                        lineaParaActualizar.VAT_IE_PERCENT = 0.0;

                    }
                }

            }

        }

        internal bool  ExisteFactura(string vendorId, string invoiceId)
        {
            string Sql = @"
                SELECT TOP 1 p.VOUCHER_ID
                FROM PAYABLE p
                WHERE p.VENDOR_ID = '" + vendorId + @"'
                AND p.INVOICE_ID='" + invoiceId + @"'
                ";
                System.Data.DataTable oDataTableR = oData.EjecutarConsulta(Sql);
                if (oDataTableR != null)
                {
                    foreach (System.Data.DataRow oDataRowR in oDataTableR.Rows)
                    {
                        return true;
                    }
                }

            return false;

        }

        private void CargarLineas(CFDI40.Comprobante oComprobante, DateTime? fechaCreacion = null, string recibo="")
        {

            lineas = new List<cPAYABLE_LINE>();
            //Agregar una linea
            cPAYABLE_LINE OcPAYABLE_LINE = new cPAYABLE_LINE();
            Constantes(OcPAYABLE_LINE);
            OcPAYABLE_LINE.LINE_NO = 1;
            OcPAYABLE_LINE.QTY = 1;
            OcPAYABLE_LINE.REFERENCE = "CFDI";
            OcPAYABLE_LINE.AMOUNT = oComprobante.SubTotal;
            OcPAYABLE_LINE.GL_ACCOUNT_ID = "54000007";

            //Cargar el ültimo recibo que tiene el proveedor
            //OcPAYABLE_LINE.RECEIVER_ID = CargarUltimoRecibo(OVENDOR.ID, oComprobante.SubTotal);

            CargarImpuesto(OcPAYABLE_LINE, oComprobante.Impuestos);
            lineas.Add(OcPAYABLE_LINE);
            //Validar las retenciones
            if (oComprobante.Impuestos!=null)
            {
                if (oComprobante.Impuestos.TotalImpuestosRetenidos!=0)
                {
                    AgregarRetencionGeneral(oComprobante.Impuestos.TotalImpuestosRetenidos);
                }
            }
        }

        private void AgregarRetencionGeneral(decimal retencion)
        {
            cPAYABLE_LINE OcPAYABLE_LINE = new cPAYABLE_LINE();
            Constantes(OcPAYABLE_LINE);
            OcPAYABLE_LINE.QTY = 1;
            OcPAYABLE_LINE.REFERENCE = "Retencion";
            OcPAYABLE_LINE.AMOUNT = Math.Abs(retencion)*-1;
            OcPAYABLE_LINE.GL_ACCOUNT_ID = "20103007";
            OcPAYABLE_LINE.VAT_PERCENT = 0;
            OcPAYABLE_LINE.VAT_AMOUNT = 0;
            OcPAYABLE_LINE.VAT_GL_ACCT_ID = "10400006";
            lineas.Add(OcPAYABLE_LINE);
        }

        private void CargarConceptos(CFDI40.Comprobante oComprobante,DateTime? fechaCreacion=null
            , string recibo=""
            , string solicitudServicioTr = ""
            , string solicitudServicioNotasTr = ""
            , string solicitudServicioTipoTr = ""
            )
        {
            lineas = new List<cPAYABLE_LINE>();
            int Linea = 1;
            foreach (CFDI40.ComprobanteConcepto Concepto in oComprobante.Conceptos)
            {
                cPAYABLE_LINE OcPAYABLE_LINE = new cPAYABLE_LINE();
                OcPAYABLE_LINE.LINE_NO = Linea;
                OcPAYABLE_LINE.QTY = Concepto.Cantidad;
                OcPAYABLE_LINE.REFERENCE = Concepto.Descripcion;
                OcPAYABLE_LINE.AMOUNT = Concepto.Importe;
                OcPAYABLE_LINE.VAT_PERCENT = 0;
                OcPAYABLE_LINE.VAT_AMOUNT = 0;
                //OcPAYABLE_LINE.VAT_GL_ACCT_ID = "10400014";
                OcPAYABLE_LINE.FIXED_CHARGE = 0; 
                OcPAYABLE_LINE.DISC_ALLOWED = "Y";
                OcPAYABLE_LINE.COST_CATEGORY = "M";
                OcPAYABLE_LINE.DUTY_PERCENT = 0;
                OcPAYABLE_LINE.DUTY_AMOUNT = 0;
                OcPAYABLE_LINE.DUTY_GL_ACCT_ID = "M";
                OcPAYABLE_LINE.GL_ACCOUNT_ID = "";
                //Validar las reglas de las cuenta contable basado por proveedor, Clave 
                //Validar los Impuestos
                cPAYABLE_LINE OcPAYABLE_LINERetencion = new cPAYABLE_LINE();

                if (Concepto.Impuestos!=null)
                {
                    if (Concepto.Impuestos.Traslados!=null)
                    {
                        foreach (CFDI40.ComprobanteConceptoImpuestosTraslado impuesto in Concepto.Impuestos.Traslados)
                        {
                            OcPAYABLE_LINE.VAT_PERCENT = impuesto.TasaOCuota;
                            OcPAYABLE_LINE.VAT_AMOUNT = impuesto.Importe;
                            //OcPAYABLE_LINE.VAT_GL_ACCT_ID = "10400014";
                        }
                    }

                    string RegimenFiscalTr = oComprobante.Emisor.RegimenFiscal.ToString().Replace("Item", "");
                    string RfcTr = oComprobante.Emisor.Rfc.ToString();


                    ModeloCxP.CxPEntities dbContext = new ModeloCxP.CxPEntities();
                    var resultImpuesto = from b in dbContext.CxPReglasSet
                                                 .Where(a => a.Tipo.Contains("Impuesto")
                                                 && a.Valor.Contains("TotalImpuestosRetenidos")
                                                 && 
                                                 (a.CondicionTipo.Contains("RegimenFiscal") && a.CondicionValor.Contains(RegimenFiscalTr)
                                                 ||
                                                 a.CondicionTipo.Contains("Rfc") && a.CondicionValor.Contains(RfcTr)
                                                 )
                                                 )                                                 
                                 select b;

                    List<ModeloCxP.CxPReglas> dtImpuesto = resultImpuesto.ToList();
                    foreach (ModeloCxP.CxPReglas registroImpuesto in dtImpuesto)
                    {
                        if (registroImpuesto != null)
                        {
                            if (Concepto.Impuestos.Retenciones != null)
                            {
                                foreach (CFDI40.ComprobanteConceptoImpuestosRetencion impuesto in Concepto.Impuestos.Retenciones)
                                {
                                    OcPAYABLE_LINERetencion = new cPAYABLE_LINE();
                                    OcPAYABLE_LINERetencion.QTY = 1;
                                    OcPAYABLE_LINERetencion.AMOUNT = Math.Abs(impuesto.Importe);
                                    OcPAYABLE_LINERetencion.REFERENCE = "Tasa " + impuesto.TasaOCuota.ToString() + " " +
                                       INVOICE_ID + " " + oComprobante.Emisor.Rfc;

                                    //Cargar la regla de los impuestos de retencion 
                                    OcPAYABLE_LINERetencion.GL_ACCOUNT_ID = registroImpuesto.CuentaContable;

                                    if (registroImpuesto.Negativo)
                                    {
                                        OcPAYABLE_LINERetencion.AMOUNT = OcPAYABLE_LINERetencion.AMOUNT * -1;
                                    }

                                    OcPAYABLE_LINERetencion.VAT_CODE = "0% ACRED";
                                    OcPAYABLE_LINERetencion.VAT_PERCENT=0;
                                    OcPAYABLE_LINERetencion.VAT_AMOUNT = 0;
                                    OcPAYABLE_LINERetencion.VAT_GL_ACCT_ID = "10400006";
                                    OcPAYABLE_LINERetencion.DISC_ALLOWED = "Y";
                                    OcPAYABLE_LINERetencion.COST_CATEGORY = "M";
                                }
                            }
                        }

                    }

                    
                }

                decimal ImporteTr = Concepto.Importe;
                if (oComprobante.Moneda.Equals("USD"))
                {
                    ImporteTr = Concepto.Importe;// * oComprobante.TipoCambio;
                }
                //Cargar el ultimo recibo sin CxP
                string SqlReciboWHERE = @"";
                if (!String.IsNullOrEmpty(recibo))
                {
                    //if (!oComprobante.Moneda.Equals("USD"))
                    //{
                    SqlReciboWHERE = @" AND rl.RECEIVER_ID like '" + recibo + @"' 
                    AND rl.LINE_NO=" + Linea.ToString() + @" ";
                    //Rodrigo Escalona: 12/082024 Omitir validacion de monto interno
                    //SqlReciboWHERE = @" AND ABS((rl.UNIT_PRICE*rl.RECEIVED_QTY) - CAST(" + ImporteTr.ToString() + @" AS DECIMAL(18,2))) <= 1 ";
                    SqlReciboWHERE+= @"
                    AND 
                    (
                            (ABS(rl.UNIT_PRICE * rl.RECEIVED_QTY) / 
                            ISNULL(
                                (SELECT TOP 1 ce.SELL_RATE 
                                 FROM CURRENCY_EXCHANGE ce 
                                 WHERE ce.CURRENCY_ID = 'DLLS' 
                                 AND DATEDIFF(DAY, ce.EFFECTIVE_DATE, r.RECEIVED_DATE) = 0), 
                                1
                            )) 
                    - CAST(" + ImporteTr.ToString() + @" AS DECIMAL(18,2))
                    ) <= 1 
                    ";
                    //";
                    //}
                }
                //AND ABS((rl.UNIT_PRICE*rl.RECEIVED_QTY) - CAST(" + ImporteTr.ToString() + @" AS DECIMAL(18,2))) <= 0.1


                string Sql = @"
                SELECT TOP 1 rl.PURC_ORDER_ID, rl.PURC_ORDER_LINE_NO, rl.RECEIVER_ID, rl.LINE_NO, v.TAX_GL_ACCT_ID
                ,COALESCE(pol.GL_EXPENSE_ACCT_ID, p.INV_MAT_GL_ACCT_ID) as Cuenta
                , rl.UNIT_PRICE*rl.RECEIVED_QTY as TotalLinea
                ,rl.RECEIVED_QTY as PO_RECV_QTY
                ,pol.PRODUCT_CODE
                ,pol.UNIT_PRICE
                , pol.VAT_CODE
                FROM RECEIVER_LINE rl
                INNER JOIN PURC_ORDER_LINE pol ON pol.PURC_ORDER_ID = rl.PURC_ORDER_ID AND pol.LINE_NO = rl.PURC_ORDER_LINE_NO
                INNER JOIN PURCHASE_ORDER po ON pol.PURC_ORDER_ID = po.ID
                INNER JOIN RECEIVER r ON r.ID=rl.RECEIVER_ID 
                left join PRODUCT p ON p.CODE = pol.PRODUCT_CODE
                left join VAT v ON v.CODE=pol.VAT_CODE
                WHERE NOT EXISTS (
                    SELECT 1
                    FROM PAYABLE_LINE pl
                    WHERE pl.PURC_ORDER_ID = rl.PURC_ORDER_ID
                    AND pl.PURC_ORDER_LINE_NO = rl.PURC_ORDER_LINE_NO
                    AND pl.RECEIVER_ID = rl.RECEIVER_ID
                    AND pl.RECEIVER_LINE_NO = rl.LINE_NO
                )
                AND po.VENDOR_ID like '" + OVENDOR.ID + @"'
                " + SqlReciboWHERE + @"
                ORDER BY rl.RECEIVER_ID
                ";
                BitacoraFX.Log("CargarConceptos-511 " + Sql);
                System.Data.DataTable oDataTableR = oData.EjecutarConsulta(Sql);
                if (oDataTableR != null)
                {
                    foreach (System.Data.DataRow oDataRowR in oDataTableR.Rows)
                    {
                        OcPAYABLE_LINE.PURC_ORDER_ID = oDataRowR["PURC_ORDER_ID"].ToString();
                        OcPAYABLE_LINE.PURC_ORDER_LINE_NO = oDataRowR["PURC_ORDER_LINE_NO"].ToString();
                        OcPAYABLE_LINE.RECEIVER_ID = oDataRowR["RECEIVER_ID"].ToString();
                        OcPAYABLE_LINE.RECEIVER_LINE_NO = oDataRowR["LINE_NO"].ToString();
                        OcPAYABLE_LINE.GL_ACCOUNT_ID = oDataRowR["Cuenta"].ToString();
                        OcPAYABLE_LINE.GL_ACCOUNT_ID = "20101998"; //

                        OcPAYABLE_LINE.VAT_GL_ACCT_ID = oDataRowR["TAX_GL_ACCT_ID"].ToString();
                        OcPAYABLE_LINE.VAT_CODE = oDataRowR["VAT_CODE"].ToString();

                        OcPAYABLE_LINE.PO_RECV_QTY = oDataRowR["PO_RECV_QTY"].ToString();
                        OcPAYABLE_LINE.PO_DISC_PERCENT = 0;//oDataRowR["TAX_GL_ACCT_ID"].ToString();
                        OcPAYABLE_LINE.PO_GL_ACCOUNT_ID = "20101998";//oDataRowR["PO_GL_ACCOUNT_ID"].ToString();
                        OcPAYABLE_LINE.PRODUCT_CODE = oDataRowR["PRODUCT_CODE"].ToString();
                        OcPAYABLE_LINE.PO_UNIT_PRICE = oDataRowR["UNIT_PRICE"].ToString();

                        OcPAYABLE_LINE.DUTY_PERCENT = 0;
                        OcPAYABLE_LINE.DUTY_AMOUNT = 0;
                        OcPAYABLE_LINE.DUTY_GL_ACCT_ID = "20101999";

                        OcPAYABLE_LINE.ACT_PO_UNIT_PRICE = oDataRowR["UNIT_PRICE"].ToString();
                        
                        OcPAYABLE_LINE.VAT_IE_AMOUNT = 0.0;
                        OcPAYABLE_LINE.VAT_IE_PERCENT = 0.0;

                    }
                }

                //Rodrigo Escalona: 22/08/2024
                if (!String.IsNullOrEmpty(solicitudServicioTr))
                {
                    
                }



                Linea++;

                lineas.Add(OcPAYABLE_LINE);

                if (OcPAYABLE_LINERetencion.AMOUNT != 0)
                {
                    lineas.Add(OcPAYABLE_LINERetencion);
                }
            }

        }

        private bool CargarProveedor(string rfc, string moneda)
        {
            string MonedaTr = "MN";
            if (moneda.ToUpper().Equals("MXN"))
            {
                MonedaTr = "MN";
            }
            if (moneda.ToUpper().Equals("USD"))
            {
                MonedaTr = "DLLS";
            }

            OVENDOR = db.VENDOR.Where(a => a.VAT_REGISTRATION.Equals(rfc) 
            && a.CURRENCY_ID.Equals(MonedaTr)
            && a.ACTIVE_FLAG.Equals("Y")).FirstOrDefault();
            if (OVENDOR != null)
            {
                return true;
            }
            return false;
        }

        public bool Generar()
        {
            try
            {
                if (!String.IsNullOrEmpty(VOUCHER_ID))
                {
                    MessageBox.Show("Ya fue procesado",
                                "Generación de CxP",
                                MessageBoxButtons.OK,
                                MessageBoxIcon.Error);
                    return false;
                }

                //Rodrigo Escalona: Valida que  no exista Voucher y Vendor
                if (ExisteFactura(OVENDOR.ID, INVOICE_ID))
                {
                    MessageBox.Show("Ya fue procesado",
                                "Generación de CxP",
                                MessageBoxButtons.OK,
                                MessageBoxIcon.Error);
                    return false;
                }
                
                cVisualConfiguracion.obtener();

                Lsa.Data.Dbms.OpenLocal(cVisualConfiguracion.visualInstancia
                    , cVisualConfiguracion.visualUsuario
                    , cVisualConfiguracion.visualPassword);

                Lsa.Vmfg.Financials.SFPayables OSFPayables = new Lsa.Vmfg.Financials.SFPayables(cVisualConfiguracion.visualInstancia);
                OSFPayables.BeginInit();

                string Consecutivo = "<1>";
                if (!String.IsNullOrEmpty(OVENDOR.VAT_BOOK_CODE_I))
                {
                    Consecutivo=ObtenerConsecutivo(OVENDOR.VAT_BOOK_CODE_I);
                }

                Lsa.Data.DataRow oDataRow = OSFPayables.NewPayableRow(Consecutivo);
                

                if (String.IsNullOrEmpty(CURRENCY_ID))
                {
                    MessageBox.Show("La moneda esta en blanco",
                                "Generación de CxP",
                                MessageBoxButtons.OK,
                                MessageBoxIcon.Error);
                    return false;
                }

                if (!String.IsNullOrEmpty(OVENDOR.VAT_BOOK_CODE_I))
                {
                    oDataRow["VAT_BOOK_CODE"] = OVENDOR.VAT_BOOK_CODE_I;
                }
                oDataRow["CURRENCY_ID"] = "MN";//CURRENCY_ID;
                oDataRow["VENDOR_ID"] = OVENDOR.ID;
                oDataRow["INVOICE_ID"] = INVOICE_ID;
                oDataRow["INVOICE_DATE"] = INVOICE_DATE.Date;

                oDataRow["SITE_ID"] = SITE;


                oDataRow["TYPE"] = "I";
                oDataRow["CREATE_DATE"] = DateTime.Now;
                oDataRow["PAY_STATUS"] = "P";
                oDataRow["PAID_AMOUNT"] = 0;
                oDataRow["DISCOUNT_TAKEN"] = 0;
                oDataRow["REPORT_1099_MISC"] = "N";
                oDataRow["USER_ID"] = "FXCFDI";
                oDataRow["SELL_RATE"] = 1;
                oDataRow["BUY_RATE"] = 1;
                //if (oDataRow["CURRENCY_ID"].ToString().Equals("DLLS"))
                //{
                //    string Sql = @" SELECT TOP 1 * 
                //    FROM CURRENCY_EXCHANGE 
                //    WHERE (CURRENCY_ID = '" + oDataRow["CURRENCY_ID"] + @"')
                //    AND EFFECTIVE_DATE<=" + oData.convertir_fecha(DateTime.Now, "i") + @"
                //    ORDER BY ROWID DESC";
                //    System.Data.DataTable oDataTableC = oData.EjecutarConsulta(Sql);
                //    //Agueda se agrego la declaracion de la tabla temporal para almacenar los datos de las lineas
                //    //sSQL += " declare @PedidoMigradoTemporal table (Id int,CUSTOMER_ID nvarchar(30), CUSTOMER_PO_REF nvarchar(30), SHIP_TO_ADDR_NO int ";
                //    foreach (System.Data.DataRow oDataRowC in oDataTableC.Rows)
                //    {
                //        //Añadir el Registro
                //        oDataRow["SELL_RATE"] = Math.Round(decimal.Parse(oDataRowC["SELL_RATE"].ToString()), 4, MidpointRounding.AwayFromZero);
                //        oDataRow["BUY_RATE"] = Math.Round(decimal.Parse(oDataRowC["BUY_RATE"].ToString()), 4, MidpointRounding.AwayFromZero);
                //    }
                //}
                oDataRow["POSTING_DATE"] = DateTime.Now.Date;
                oDataRow["MARKED_FOR_PURGE"] = "N";
                oDataRow["WITHHELD_AMOUNT"] = 0;
                oDataRow["WH_ALLOWED_AMOUNT"] = 0;
                oDataRow["USER_EXCH_RATE"] = "N";
                oDataRow["LANDED_COST_INV"] = "N";
                oDataRow["POSTING_CANDIDATE"] = "Y";

                oDataRow["TERMS_NET_TYPE"] = OVENDOR.TERMS_NET_TYPE;
                oDataRow["TERMS_NET_DAYS"] = OVENDOR.TERMS_NET_DAYS;
                oDataRow["TERMS_DISC_TYPE"] = OVENDOR.TERMS_DISC_TYPE;
                oDataRow["TERMS_DISC_DAYS"] = OVENDOR.TERMS_DISC_DAYS;
                oDataRow["TERMS_DISC_PERCENT"] = OVENDOR.TERMS_DISC_PERCENT;
                oDataRow["TERMS_DESCRIPTION"] = OVENDOR.TERMS_DESCRIPTION;

                oDataRow["TERMS_ID"] = OVENDOR.DEF_TERMS_ID;
                oDataRow["TOTAL_AMOUNT"] = TOTAL_AMOUNT;
                oDataRow["PAYMENT_DATE"] = CalcularFechaPago(FechaCreacion, int.Parse(OVENDOR.TERMS_NET_DAYS.ToString()), DayOfWeek.Monday);

                oDataRow["PAYB_GL_ACCT_ID"] = "20101001";
                if (!String.IsNullOrEmpty(OVENDOR.DEF_PAYB_ACCT_ID))
                {
                    oDataRow["PAYB_GL_ACCT_ID"] = OVENDOR.DEF_PAYB_ACCT_ID;
                }
                

                //oDataRow["CREATE_DATE"] = "2024-03-05 12:39:29.163";
                //oDataRow["USER_ID"] = "SYSADM";
                //oDataRow["PAY_STATUS"] = "P";
                //oDataRow["TOTAL_AMOUNT"] = 38280.00000000;
                //oDataRow["PAID_AMOUNT"] = 0.00000000;
                //oDataRow["DISCOUNT_TAKEN"] = 0.00000000;

                //oDataRow["REPORT_1099_MISC"] = "N";
                //oDataRow["SELL_RATE"] = 1.00000000;
                //oDataRow["BUY_RATE"] = 1.00000000;
                //oDataRow["POSTING_DATE"] = "2024-03-05 00:00:00.000";
                //oDataRow["SITE_ID"] = "FIMA";
                //oDataRow["CUST_ORDER_ID"] = null;
                //oDataRow["PAYB_GL_ACCT_ID"] = "20101001";
                //oDataRow["MARKED_FOR_PURGE"] = "N";
                //oDataRow["VAT_BOOK_CODE"] = "P";
                //oDataRow["WITHHOLDING_CODE"] = null;
                //oDataRow["WITHHELD_AMOUNT"] = 0.00000000;
                //oDataRow["WH_ALLOWED_AMOUNT"] = 0.00000000;
                //oDataRow["CURRENCY_ID"] = "MN";
                //oDataRow["USER_EXCH_RATE"] = null;
                //oDataRow["COMM_INVOICE_ID"] = null;
                //oDataRow["LANDED_COST_INV"] = "N";
                //oDataRow["ACT_RATE_ID"] = null;
                //oDataRow["ACT_PERIOD_ID"] = null;
                //oDataRow["RETRO_RATE_ID"] = null;
                //oDataRow["RETRO_PERIOD_ID"] = null;
                //oDataRow["RETRO_RATE_CODE"] = null;
                //oDataRow["POSTING_CANDIDATE"] = "Y";
                //oDataRow["STATUS_EFF_DATE"] = "2024-03-05 12:39:29.163";
                //oDataRow["TERMS_ID"] = "100%PRIOR TO";
                //oDataRow["USER_DIMENSION_1"] = null;
                //oDataRow["USER_DIMENSION_2"] = null;

                //Guardar las líneas
                int LineaContador =1;
                if (lineas!=null)
                {
                    TOTAL_AMOUNT = 0;
                    foreach (cPAYABLE_LINE linea in lineas)
                    {
                        Lsa.Data.DataRow oDataLinea = OSFPayables.NewPayableLineRow(Consecutivo, LineaContador);
                        
                        oDataLinea["REFERENCE"] = linea.REFERENCE;
                        oDataLinea["QTY"] = linea.QTY;
                        oDataLinea["AMOUNT"] = Math.Round(linea.AMOUNT,2,MidpointRounding.AwayFromZero);
                        if (!String.IsNullOrEmpty(linea.VAT_CODE))
                        {
                            oDataLinea["VAT_CODE"] = linea.VAT_CODE;
                            oDataLinea["VAT_PERCENT"] = linea.VAT_PERCENT;
                            oDataLinea["VAT_AMOUNT"] = Math.Round(linea.VAT_AMOUNT, 2, MidpointRounding.AwayFromZero);
                            oDataLinea["VAT_CATEGORY"] = null;
                            oDataLinea["VAT_GL_ACCT_ID"] = linea.VAT_GL_ACCT_ID;
                        }

                        TOTAL_AMOUNT += Math.Round(linea.AMOUNT, 2, MidpointRounding.AwayFromZero)+
                            Math.Round(linea.VAT_AMOUNT, 2, MidpointRounding.AwayFromZero);

                        
                        oDataLinea["FIXED_CHARGE"] = linea.FIXED_CHARGE;
                        oDataLinea["DISC_ALLOWED"] = linea.DISC_ALLOWED;
                        oDataLinea["COST_CATEGORY"] = linea.COST_CATEGORY;
                        oDataLinea["DUTY_PERCENT"] = linea.DUTY_PERCENT;
                        oDataLinea["DUTY_AMOUNT"] = linea.DUTY_AMOUNT;
                        oDataLinea["DUTY_GL_ACCT_ID"] = linea.DUTY_GL_ACCT_ID;
                        
                        oDataLinea["GL_ACCOUNT_ID"] = linea.GL_ACCOUNT_ID;

                        oDataLinea["DUTY_PERCENT"] = linea.DUTY_PERCENT;
                        oDataLinea["DUTY_AMOUNT"] = linea.DUTY_AMOUNT;
                        oDataLinea["DUTY_GL_ACCT_ID"] = linea.DUTY_GL_ACCT_ID;

                        oDataLinea["VAT_IE_PERCENT"] = 0.000;
                        oDataLinea["VAT_IE_AMOUNT"] = 0.00000000;

                        if (!String.IsNullOrEmpty(linea.PURC_ORDER_ID))
                        {
                            //oDataLinea["CURRENCY_ID"] = CURRENCY_ID;
                            //oDataLinea["PURC_ORDER_ID"] = linea.PURC_ORDER_ID;
                            //oDataLinea["PURC_ORDER_LINE_NO"] = linea.PURC_ORDER_LINE_NO;
                            oDataLinea["RECEIVER_ID"] = linea.RECEIVER_ID;
                            oDataLinea["RECEIVER_LINE_NO"] = linea.RECEIVER_LINE_NO;
                            oDataLinea["PO_RECV_QTY"] = linea.PO_RECV_QTY;
                            oDataLinea["PO_DISC_PERCENT"] = linea.PO_DISC_PERCENT;
                            //oDataLinea["PO_GL_ACCOUNT_ID"] = "20101998";// linea.PO_GL_ACCOUNT_ID;
                            oDataLinea["PRODUCT_CODE"] = linea.PRODUCT_CODE;
                            oDataLinea["PO_UNIT_PRICE"] = linea.PO_UNIT_PRICE;
                            oDataLinea["ACT_PO_UNIT_PRICE"] = linea.PO_UNIT_PRICE;

                        }

                        //oDataLinea["REFERENCE"] = "0404014";
                        //oDataLinea["PURC_ORDER_ID"] = "OC-51694";
                        //oDataLinea["PURC_ORDER_LINE_NO"] = 1;
                        //oDataLinea["RECEIVER_ID"] = 87824;
                        //oDataLinea["RECEIVER_LINE_NO"] = 1;
                        //oDataLinea["QTY"] = 200.00000000;
                        //oDataLinea["AMOUNT"] = 33000.00000000;
                        //oDataLinea["VAT_PERCENT"] = 16.000;
                        //oDataLinea["VAT_AMOUNT"] = 5280.00000000;
                        //oDataLinea["VAT_GL_ACCT_ID"] = 10400001;
                        //oDataLinea["FIXED_CHARGE"] = 0.00000000;
                        //oDataLinea["DISC_ALLOWED"] = "Y";
                        //oDataLinea["GL_ACCOUNT_ID"] = 20101998;
                        ////oDataLinea["PO_ACT_FREIGHT"] = null;
                        //oDataLinea["PO_RECV_QTY"] = 200.00000000;
                        //oDataLinea["PO_DISC_PERCENT"] = 0.000;
                        //oDataLinea["PO_GL_ACCOUNT_ID"] = 20101998;
                        //oDataLinea["WORKORDER_TYPE"] = null;
                        //oDataLinea["WORKORDER_BASE_ID"] = null;
                        //oDataLinea["WORKORDER_LOT_ID"] = null;
                        //oDataLinea["WORKORDER_SPLIT_ID"] = null;
                        //oDataLinea["WORKORDER_SUB_ID"] = null;
                        //oDataLinea["COST_CATEGORY"] = null;
                        //oDataLinea["VAT_CODE"] = "M";
                        ////oDataLinea["VAT_RCV_PERCENT"] = "16% ACRED";
                        //oDataLinea["VAT_RCV_AMOUNT"] = 0.000;
                        //oDataLinea["VAT_RCV_GL_ACCT_ID"] = "10400007";
                        //oDataLinea["WITHHOLD_TYPE"] = null;
                        //oDataLinea["PO_UNIT_PRICE"] = 165.00000000;
                        //oDataLinea["HTS_CODE"] = null;
                        //oDataLinea["ORIG_COUNTRY_ID"] = null;
                        //oDataLinea["DUTY_PERCENT"] = 0.000;
                        //oDataLinea["DUTY_AMOUNT"] = 0.00000000;
                        //oDataLinea["DUTY_GL_ACCT_ID"] = "20101999";
                        //oDataLinea["VAT_CATEGORY"] = null;
                        //oDataLinea["WBS_CODE"] = null;
                        //oDataLinea["PROJECT_ID"] = null;
                        //oDataLinea["GL_OVERRIDE_ACCT"] = null;
                        //oDataLinea["COST_CATEGORY_ID"] = null;
                        //oDataLinea["DEPARTMENT_ID"] = null;
                        ////oDataLinea["PROJ_REF_SEQ_NO"] = null;
                        //oDataLinea["PROJ_REF_SUB_ID"] = null;
                        ////oDataLinea["ACT_BURDEN_COST"] = null;
                        //oDataLinea["ACT_PO_UNIT_PRICE"] = 165.00000000;
                        //oDataLinea["FT_TYPE"] = null;
                        //oDataLinea["USER_DIMENSION_1"] = null;
                        //oDataLinea["USER_DIMENSION_2"] = null;
                        //oDataLinea["CUST_ORDER_ID"] = "MPTARIMAS";
                        //oDataLinea["PRODUCT_CODE"] = null;
                        //oDataLinea["VAT_IE_PERCENT"] = 0.000;
                        //oDataLinea["VAT_IE_AMOUNT"] = 0.00000000;
                        //oDataLinea["VAT_IE_DR_GL_ACCT_ID"] = null;
                        //oDataLinea["VAT_IE_CR_GL_ACCT_ID"] = null;

                        LineaContador++;
                    }
                }

                oDataRow["TOTAL_AMOUNT"] = TOTAL_AMOUNT;


                OSFPayables.Save();
                this.VOUCHER_ID = oDataRow["VOUCHER_ID"].ToString();


                //Si es Dlls convertirlo
                if (CURRENCY_ID.Equals("DLLS"))
                {
                    ConvertirDlls(oDataRow["VOUCHER_ID"].ToString());
                }
                


                LineaContador = 1;
                //Actualizar las lineas de CxP Linea
                foreach (cPAYABLE_LINE linea in lineas)
                {
                    if (!String.IsNullOrEmpty(linea.PURC_ORDER_ID))
                    {
                        string Sql = @"
                        UPDATE PAYABLE_LINE
                        SET PURC_ORDER_ID='" + linea.PURC_ORDER_ID + @"' 
                        , PURC_ORDER_LINE_NO=" + linea.PURC_ORDER_LINE_NO + @"
                        , RECEIVER_ID='" + linea.RECEIVER_ID + @"' 
                        , RECEIVER_LINE_NO=" + linea.RECEIVER_LINE_NO + @"
                        , QTY=" + linea.QTY + @"
                        WHERE VOUCHER_ID='" + this.VOUCHER_ID + @"'
                        AND LINE_NO=" + LineaContador.ToString() + @"
                        ";
                        oData.EjecutarConsulta(Sql);
                    }
                    LineaContador++;
                }

                //Actualizar las lienas con los recibos
                string SqlCorreccion = @"
                WITH MismatchedRecords AS (
                    SELECT PL.RECEIVER_ID, PL.RECEIVER_LINE_NO, RL.USER_RECEIVED_QTY
                    FROM RECEIVER_LINE RL
                    INNER JOIN PAYABLE_LINE PL 
                        ON PL.RECEIVER_ID = RL.RECEIVER_ID 
                        AND PL.RECEIVER_LINE_NO = RL.LINE_NO
                    WHERE ABS(RL.USER_RECEIVED_QTY) <> ABS(PL.QTY)
                )
                -- Now, perform the update based on the records identified
                UPDATE PAYABLE_LINE
                SET QTY = MR.USER_RECEIVED_QTY
                FROM PAYABLE_LINE PL
                INNER JOIN MismatchedRecords MR 
                    ON MR.RECEIVER_ID = PL.RECEIVER_ID 
                    AND MR.RECEIVER_LINE_NO = PL.RECEIVER_LINE_NO;

                ";
                oData.EjecutarConsulta(SqlCorreccion);
                return true;
            }
            catch (Exception error)
            {
                string mensaje = "";
                mensaje += error.Message.ToString();
                if (error.InnerException != null)
                {
                    mensaje += " " + error.InnerException.Message.ToString();
                }
                mensaje += "\n cPAYABLE - 847 - GenerarVisual Voucher  ";
                // Invalid file type selected; display an error.
                MessageBox.Show(mensaje,
                                "Generación de CxP ",
                                MessageBoxButtons.OK,
                                MessageBoxIcon.Error);
                return false;
            }
        }

        private void ConvertirDlls(string voucher)
        {
            decimal SellRate = 1;
            decimal BuyRate = 1;
            decimal TotalAmount = 0;
            string CurrencyId = "MN";

            string Sql = $@" SELECT TOP 1 * 
                FROM CURRENCY_EXCHANGE 
                WHERE (CURRENCY_ID = 'DLLS')
                AND EFFECTIVE_DATE<={oData.convertir_fecha(DateTime.Now, "i")}
                ORDER BY ROWID DESC";
            System.Data.DataTable oDataTableC = oData.EjecutarConsulta(Sql);
            foreach (System.Data.DataRow oDataRowC in oDataTableC.Rows)
            {
                //Añadir el Registro
                SellRate = Math.Round(decimal.Parse(oDataRowC["SELL_RATE"].ToString()), 4, MidpointRounding.AwayFromZero);
                BuyRate = Math.Round(decimal.Parse(oDataRowC["BUY_RATE"].ToString()), 4, MidpointRounding.AwayFromZero);
            }

            Sql = $@" SELECT TOP 1 * 
                FROM PAYABLE 
                WHERE (CURRENCY_ID = 'DLLS')
                AND VOUCHER_ID='{voucher}'
                ";
            System.Data.DataTable oDataTablePayable = oData.EjecutarConsulta(Sql);
            foreach (System.Data.DataRow oDataRowPayable in oDataTablePayable.Rows)
            {
                //Añadir el Registro
                TotalAmount = Math.Round(decimal.Parse(oDataRowPayable["TOTAL_AMOUNT"].ToString()), 2, MidpointRounding.AwayFromZero);
            }

            Sql = $@"
            UPDATE PAYABLE 
            SET CURRENCY_ID='DLLS', SELL_RATE={SellRate.ToString()}, BUY_RATE={BuyRate.ToString()}
            WHERE VOUCHER_ID='{voucher}'
            ";
            oData.EjecutarConsulta(Sql);

            decimal TotalAmountMN = Math.Round(TotalAmount * SellRate, 2, MidpointRounding.AwayFromZero);

            //Tabla: PAYABLE_CURR
            Sql = $@"
            UPDATE PAYABLE_CURR 
            SET CURRENCY_ID='DLLS', SELL_RATE={SellRate.ToString()}, BUY_RATE={BuyRate.ToString()}
            WHERE VOUCHER_ID='{voucher}' AND CURRENCY_ID='MN'
            ";
            oData.EjecutarConsulta(Sql);
            //Moneda MN
            Sql = $@"
            INSERT INTO PAYABLE_CURR 
            (VOUCHER_ID,CURRENCY_ID,AMOUNT,SELL_RATE,BUY_RATE)
            VALUES
            ('{voucher}','MN',{TotalAmountMN.ToString()},1,1)
            ";
            oData.EjecutarConsulta(Sql);



            //Tabla: PAYABLE_DIST
            Sql = $@"
            UPDATE PAYABLE_DIST 
            SET CURRENCY_ID='DLLS'
            WHERE VOUCHER_ID='{voucher}' AND CURRENCY_ID='MN'
            ";
            oData.EjecutarConsulta(Sql);


            int EntryNo = 0;
            //Insertar PAYABLE_DIST MN
            Sql = $@" SELECT pd.* 
                , (SELECT COUNT(*) as Cuenta FROM PAYABLE_DIST pd1 WHERE pd1.VOUCHER_ID='{voucher}') as Cuenta
                FROM PAYABLE_DIST pd
                WHERE (pd.CURRENCY_ID = 'DLLS') 
                AND pd.VOUCHER_ID='{voucher}' 
                ORDER BY pd.ROWID";
            System.Data.DataTable oDataTablePayableDist = oData.EjecutarConsulta(Sql);
            foreach (System.Data.DataRow oDataRowPayableDist in oDataTablePayableDist.Rows)
            {
                if (EntryNo == 0)
                {
                    EntryNo = int.Parse(oDataRowPayableDist["Cuenta"].ToString());
                }
                EntryNo++;

                //Añadir el Registro
                string RowId = oDataRowPayableDist["ROWID"].ToString();

                decimal AmountDist = Math.Round(decimal.Parse(oDataRowPayableDist["AMOUNT"].ToString()), 2, MidpointRounding.AwayFromZero);
                AmountDist = Math.Round(AmountDist * SellRate, 2, MidpointRounding.AwayFromZero);
                Sql = $@"
                INSERT INTO PAYABLE_DIST 
                (VOUCHER_ID,DIST_NO,ENTRY_NO,AMOUNT,AMOUNT_TYPE,GL_ACCOUNT_ID,NATIVE_AMOUNT
                ,POSTING_DATE,POSTING_STATUS,BATCH_ID,CREATE_DATE,SITE_ID,CURRENCY_ID,NATIVE)
                SELECT 
                '{voucher}',1,{EntryNo.ToString()},{AmountDist},AMOUNT_TYPE,GL_ACCOUNT_ID,NATIVE_AMOUNT
                ,POSTING_DATE,POSTING_STATUS,BATCH_ID,CREATE_DATE,SITE_ID,'{CurrencyId}','N'
                FROM PAYABLE_DIST
                WHERE ROWID={RowId}
                ";
                oData.EjecutarConsulta(Sql);
            }


            //Validar la Sumatori de DR debe ser igual a CR
            Sql = $@" SELECT SUM(pd.AMOUNT) as Monto
                FROM PAYABLE_DIST pd
                WHERE (pd.CURRENCY_ID = 'MN') AND pd.AMOUNT_TYPE='CR'
                AND pd.VOUCHER_ID='{voucher}'";
            System.Data.DataTable oDataTablePayableDistCorrecion = oData.EjecutarConsulta(Sql);
            foreach (System.Data.DataRow oDataRowPayableDistCorrecion in oDataTablePayableDistCorrecion.Rows)
            {
                decimal AmountDist = decimal.Parse(oDataRowPayableDistCorrecion["Monto"].ToString());
                //Sql = $@"
                //UPDATE PAYABLE_DIST 
                //SET AMOUNT={AmountDist}
                //WHERE VOUCHER_ID='{voucher}' AND CURRENCY_ID='MN' AND AMOUNT_TYPE='DR' AND NATIVE='N'
                //";
                //oData.EjecutarConsulta(Sql);

                Sql = $@"
                UPDATE PAYABLE_CURR 
                SET AMOUNT={AmountDist}
                WHERE VOUCHER_ID='{voucher}' AND CURRENCY_ID='MN'
                ";
                oData.EjecutarConsulta(Sql);
            }

            Sql = $@"

            -- Primero, obtenemos las sumas de CR y DR
            DECLARE @MontoCR DECIMAL(18, 2)
            DECLARE @MontoDR DECIMAL(18, 2)
            DECLARE @VoucherID NVARCHAR(10) = '{voucher}'

            -- Obtenemos la suma de CR
            SELECT @MontoCR = SUM(pd.AMOUNT)
            FROM PAYABLE_DIST pd
            WHERE pd.CURRENCY_ID = 'MN' AND pd.AMOUNT_TYPE = 'CR' AND pd.VOUCHER_ID = @VoucherID

            -- Obtenemos la suma de DR
            SELECT @MontoDR = SUM(pd.AMOUNT)
            FROM PAYABLE_DIST pd
            WHERE pd.CURRENCY_ID = 'MN' AND pd.AMOUNT_TYPE = 'DR' AND pd.VOUCHER_ID = @VoucherID

            -- Calculamos la diferencia
            DECLARE @Diferencia DECIMAL(18, 2)

            -- Si CR es mayor que DR, actualizamos DR
            IF @MontoCR > @MontoDR
            BEGIN
                SET @Diferencia = @MontoCR - @MontoDR
                UPDATE PAYABLE_DIST
                SET AMOUNT = AMOUNT + @Diferencia
                WHERE CURRENCY_ID = 'MN' AND AMOUNT_TYPE = 'DR' AND VOUCHER_ID = @VoucherID
            END
            -- Si DR es mayor que CR, actualizamos CR
            ELSE IF @MontoDR > @MontoCR
            BEGIN
                SET @Diferencia = @MontoDR - @MontoCR
                UPDATE PAYABLE_DIST
                SET AMOUNT = AMOUNT + @Diferencia
                WHERE CURRENCY_ID = 'MN' AND AMOUNT_TYPE = 'CR' AND VOUCHER_ID = @VoucherID
            END

            ";
            oData.EjecutarConsulta(Sql);



        }

        public void AutoAjustar()
        {
            string Sql = $@"
            -- Crear una tabla temporal para almacenar las diferencias
            CREATE TABLE #Diferencias (
                VOUCHER_ID NVARCHAR(10),
                MontoCR DECIMAL(18, 2),
                MontoDR DECIMAL(18, 2),
                Diferencia DECIMAL(18, 2),
                TipoAjuste NVARCHAR(2)
            )

            -- Insertar los registros con sus diferencias
            INSERT INTO #Diferencias (VOUCHER_ID, MontoCR, MontoDR, Diferencia, TipoAjuste)
            SELECT
                cr.VOUCHER_ID,
                cr.MontoCR,
                dr.MontoDR,
                CASE 
                    WHEN cr.MontoCR > dr.MontoDR THEN cr.MontoCR - dr.MontoDR
                    WHEN dr.MontoDR > cr.MontoCR THEN dr.MontoDR - cr.MontoCR
                    ELSE 0
                END AS Diferencia,
                CASE 
                    WHEN cr.MontoCR > dr.MontoDR THEN 'DR'
                    WHEN dr.MontoDR > cr.MontoCR THEN 'CR'
                    ELSE ''
                END AS TipoAjuste
            FROM
                (SELECT VOUCHER_ID, SUM(AMOUNT) AS MontoCR
                 FROM PAYABLE_DIST
                 WHERE CURRENCY_ID = 'MN' AND AMOUNT_TYPE = 'CR'
                 GROUP BY VOUCHER_ID) cr
            JOIN
                (SELECT VOUCHER_ID, SUM(AMOUNT) AS MontoDR
                 FROM PAYABLE_DIST
                 WHERE CURRENCY_ID = 'MN' AND AMOUNT_TYPE = 'DR'
                 GROUP BY VOUCHER_ID) dr
            ON cr.VOUCHER_ID = dr.VOUCHER_ID
            WHERE cr.MontoCR <> dr.MontoDR

            -- Actualizar los montos según la diferencia calculada
            UPDATE pd
            SET pd.AMOUNT = pd.AMOUNT + d.Diferencia
            FROM PAYABLE_DIST pd
            JOIN #Diferencias d
            ON pd.VOUCHER_ID = d.VOUCHER_ID
            AND pd.CURRENCY_ID = 'MN'
            AND ((pd.AMOUNT_TYPE = 'CR' AND d.TipoAjuste = 'CR') OR (pd.AMOUNT_TYPE = 'DR' AND d.TipoAjuste = 'DR'))

            -- Eliminar la tabla temporal
            DROP TABLE #Diferencias

            ";

            oData.EjecutarConsulta(Sql);
        }

        public string ObtenerConsecutivo(string vAT_BOOK_CODE_I)
        {
            string InvoiceID = "";

            VAT_BOOK oVAT_BOOK = db.VAT_BOOK.Where(a => a.AP_INVOICES.Equals("Y") && a.YEAR.Equals(DateTime.Now.Year)).FirstOrDefault();
            if (oVAT_BOOK == null)
            {
                InvoiceID = "1";
            }
            string NextNumber = "0000";
            if (oVAT_BOOK.NEXT_NUMBER != null)
            {
                NextNumber = oVAT_BOOK.NEXT_NUMBER.ToString();
                //Aumentar
                oVAT_BOOK.NEXT_NUMBER++;
                // Guardar los cambios en la base de datos
                db.SaveChanges();
            }
            if (oVAT_BOOK.LEADING_ZEROS == "Y")
            {
                string NexNumberTr = string.Empty;
                short DecimalPlace = 0;
                if (oVAT_BOOK.DECIMAL_PLACES != null)
                {
                    DecimalPlace = short.Parse(oVAT_BOOK.DECIMAL_PLACES.ToString());
                }
                if (DecimalPlace > 0)
                {
                    string CantidadCeros = "";
                    for (int i = 0; i < DecimalPlace - NextNumber.Length; i++)
                    {
                        CantidadCeros += "0";


                    }
                    NextNumber = CantidadCeros + NextNumber;
                }

            }
            if (oVAT_BOOK.ALPHA_PREFIX != null)
            {
                NextNumber = oVAT_BOOK.ALPHA_PREFIX.ToString() + NextNumber;
            }
            InvoiceID = NextNumber;
            return InvoiceID;
        }
        private DateTime CalcularFechaPago(DateTime FechaCreacion, int Dias, DayOfWeek DiaProximoPago)
        {
            // Sumar días a la fecha de creación
            DateTime fechaPreliminarPago = FechaCreacion.AddDays(Dias);

            // Si Dias es 0, entonces ya estamos en la fecha de creación, así que la retornamos directamente
            if (Dias == 0)
            {
                return FechaCreacion;
            }

            // Calcular la diferencia de días hasta el próximo día de pago deseado
            int diasHastaProximoPago = (int)DiaProximoPago - (int)fechaPreliminarPago.DayOfWeek;

            // Si el día de pago deseado ya pasó en la semana, calcula para la siguiente semana
            if (diasHastaProximoPago < 0)
            {
                diasHastaProximoPago += 7;
            }

            // Ajustar la fecha preliminar al próximo día de pago
            DateTime fechaDePago = fechaPreliminarPago.AddDays(diasHastaProximoPago);

            return fechaDePago.Date;
        }
    }


}
