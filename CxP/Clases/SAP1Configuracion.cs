﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CxP.Clases
{
    internal sealed class SAP1Configuracion
    {
        public static string Server { get; set; }
        public static string CompanyDB { get; set; }
        public static bool UseTrusted { get; set; }
        public static string UserName { get; set; }
        public static string Password { get; set; }


        public static void Obtener()
        {
            try
            {
                Server = ConfigurationManager.AppSettings["SAPServer"].ToString();
                UseTrusted = false;
                CompanyDB = ConfigurationManager.AppSettings["SAPCompanyDB"].ToString();
                UserName = ConfigurationManager.AppSettings["SAPUserName"].ToString();
                Password = ConfigurationManager.AppSettings["SAPPassword"].ToString();
            }
            catch (Exception error)
            {
                string mensaje = "SAP1Configuracion - 33 - obtener ";
                mensaje += error.Message.ToString();
                if (error.InnerException != null)
                {
                    mensaje += " " + error.InnerException.Message.ToString();
                }
                throw new Exception(mensaje);
            }
        }

    }
}
