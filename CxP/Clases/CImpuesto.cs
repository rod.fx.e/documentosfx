﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml;

namespace CxP.Clases
{
    static class CImpuesto
    {
        static public decimal TotalFacturasPesos(DateTime inicio, DateTime fin)
        {
            try
            {
                Conexion oData = new Conexion();
                string Sql = ObtenerFacturasPesos(inicio, fin);
                System.Data.DataTable oDataTableR = oData.EjecutarConsulta(Sql);
                if (oDataTableR != null)
                {
                    foreach (System.Data.DataRow oDataRowR in oDataTableR.Rows)
                    {
                        return decimal.Parse(oDataRowR["Total"].ToString());                        
                    }
                }
                return 0;
            }
            catch (Exception error)
            {
                string mensaje = "";
                mensaje += error.Message.ToString();
                if (error.InnerException != null)
                {
                    mensaje += " " + error.InnerException.Message.ToString();
                }
                mensaje += "\n CImpuesto - 36 - TotalFacturas";
                // Invalid file type selected; display an error.
                MessageBox.Show(mensaje,
                                "TotalFacturas",
                                MessageBoxButtons.OK,
                                MessageBoxIcon.Error);
                return 0;
            }
        }
        static public string ObtenerFacturasPesos(DateTime inicio, DateTime fin, bool detallado = false)
        {
            Conexion oData = new Conexion();
            //String detalladoString = "SUM(r.TOTAL_AMOUNT*r.SELL_RATE) as Total";
            String detalladoString = "SUM(rl.AMOUNT*r.SELL_RATE) as Total";
            if (detallado)
            {
                detalladoString = "*";
            }

            string Sql = @"
            SELECT " + detalladoString + @" 
            FROM RECEIVABLE r
            INNER JOIN RECEIVABLE_LINE rl ON rl.INVOICE_ID=r.INVOICE_ID
            WHERE r.POSTING_DATE>=" + oData.convertir_fecha(inicio, "i") + @" AND r.POSTING_DATE<=" + oData.convertir_fecha(fin, "f") + @"
            AND r.STATUS<>'X'
            AND r.TYPE='I'
            ";
            return Sql;
        }
        static public decimal TotalFacturas(DateTime inicio, DateTime fin)
        {
            try
            {
                Conexion oData = new Conexion();
                string Sql = @"
                SELECT COUNT(*) as Total
                FROM RECEIVABLE r
                WHERE r.POSTING_DATE>=" + oData.convertir_fecha(inicio, "i") + @" AND r.POSTING_DATE<=" + oData.convertir_fecha(fin, "f") + @"
                AND r.STATUS<>'X'
                ";
                System.Data.DataTable oDataTableR = oData.EjecutarConsulta(Sql);
                if (oDataTableR != null)
                {
                    foreach (System.Data.DataRow oDataRowR in oDataTableR.Rows)
                    {
                        return decimal.Parse(oDataRowR["Total"].ToString());
                    }
                }
                return 0;
            }
            catch (Exception error)
            {
                string mensaje = "";
                mensaje += error.Message.ToString();
                if (error.InnerException != null)
                {
                    mensaje += " " + error.InnerException.Message.ToString();
                }
                mensaje += "\n CImpuesto - 36 - TotalFacturas";
                // Invalid file type selected; display an error.
                MessageBox.Show(mensaje,
                                "TotalFacturas",
                                MessageBoxButtons.OK,
                                MessageBoxIcon.Error);
                return 0;
            }
        }

        internal static string ObtenerDiferenciaFacturasAsentado(DateTime inicio, DateTime fin, bool v)
        {
            Conexion oData = new Conexion();
            string SqlC1 = @"
            SELECT r.INVOICE_ID as Factura, SUM(r.TOTAL_AMOUNT*r.SELL_RATE) as Total 
            FROM RECEIVABLE r
            WHERE r.POSTING_DATE>=" + oData.convertir_fecha(inicio, "i") + @" AND r.POSTING_DATE<=" + oData.convertir_fecha(fin, "f") + @"
            AND r.STATUS<>'X'
            AND r.TYPE='I'
            GROUP BY r.INVOICE_ID
            ";

            string SqlC2 = @"
            SELECT  r.INVOICE_ID as Factura, SUM(rd.AMOUNT) as Total
            FROM RECEIVABLE r
            INNER JOIN RECEIVABLE_DIST rd ON rd.INVOICE_ID=r.INVOICE_ID
            WHERE r.POSTING_DATE>=" + oData.convertir_fecha(inicio, "i") + @" AND r.POSTING_DATE<=" + oData.convertir_fecha(fin, "f") + @"
            AND rd.CURRENCY_ID='MN'
            AND rd.GL_ACCOUNT_ID NOT IN (SELECT TAX_GL_ACCT_ID FROM VAT)
            AND rd.AMOUNT_TYPE='CR'
            AND rd.POSTING_STATUS='P'
            AND r.STATUS<>'X'
            AND r.TYPE='I'
            GROUP BY r.INVOICE_ID
            ";

            string Sql = @"
            WITH C1 AS (
                " + SqlC1 + @" 
            ),
            C2 AS (
                " + SqlC2 + @" 
            )
            SELECT C1.Factura, C1.Total AS Total_Factura, C2.Total AS Total_Asentado, (C1.Total - C2.Total) AS Diferencia
            FROM C1
            FULL OUTER JOIN C2 ON C1.Factura = C2.Factura
            WHERE C1.Total <> C2.Total OR C1.Factura IS NULL OR C2.Factura IS NULL
            ";

            return Sql;

        }

        static public decimal TotalFacturasTimbradas(DateTime inicio, DateTime fin)
        {
            try
            {
                Conexion oData = new Conexion();
                string Sql = @"
                SELECT COUNT(*) as Total
                FROM [APP].[dbo].[VMX_FE] v
                INNER JOIN FIMA..RECEIVABLE r ON r.INVOICE_ID=v.INVOICE_ID 
                WHERE CONVERT(datetime, v.FechaTimbrado, 126) BETWEEN '" + inicio.ToString("yyyy-MM-dd") + @"' AND '" + fin.ToString("yyyy-MM-dd") + @"'
                AND r.STATUS<>'X' 
                AND r.TYPE='I' 
                ";
                System.Data.DataTable oDataTableR = oData.EjecutarConsulta(Sql);
                if (oDataTableR != null)
                {
                    foreach (System.Data.DataRow oDataRowR in oDataTableR.Rows)
                    {
                        return decimal.Parse(oDataRowR["Total"].ToString());
                    }
                }
                return 0;
            }
            catch (Exception error)
            {
                string mensaje = "";
                mensaje += error.Message.ToString();
                if (error.InnerException != null)
                {
                    mensaje += " " + error.InnerException.Message.ToString();
                }
                mensaje += "\n CImpuesto - 36 - TotalFacturas";
                // Invalid file type selected; display an error.
                MessageBox.Show(mensaje,
                                "TotalFacturas",
                                MessageBoxButtons.OK,
                                MessageBoxIcon.Error);
                return 0;
            }
        }
        static public decimal TotalFacturasTimbradasMonto(DateTime inicio, DateTime fin)
        {
            try
            {
                decimal Total = 0;
                Conexion oData = new Conexion();
                string Sql = @"
                SELECT v.XML
                FROM [APP].[dbo].[VMX_FE] v
                INNER JOIN FIMA..RECEIVABLE r ON r.INVOICE_ID=v.INVOICE_ID 
                WHERE CONVERT(datetime, v.FechaTimbrado, 126) BETWEEN '" + inicio.ToString("yyyy-MM-dd") + @"' AND '" + fin.ToString("yyyy-MM-dd") + @"'
                AND r.STATUS<>'X' 
                AND r.TYPE='I' 
                ";
                System.Data.DataTable oDataTableR = oData.EjecutarConsulta(Sql);
                if (oDataTableR != null)
                {
                    foreach (System.Data.DataRow oDataRowR in oDataTableR.Rows)
                    {
                        Total += ObtenerCFDITotal(oDataRowR["XML"].ToString());
                    }
                }
                return Total;
            }
            catch (Exception error)
            {
                string mensaje = "";
                mensaje += error.Message.ToString();
                if (error.InnerException != null)
                {
                    mensaje += " " + error.InnerException.Message.ToString();
                }
                mensaje += "\n CImpuesto - 36 - TotalFacturas";
                // Invalid file type selected; display an error.
                MessageBox.Show(mensaje,
                                "TotalFacturas",
                                MessageBoxButtons.OK,
                                MessageBoxIcon.Error);
                return 0;
            }
        }

        static public System.Data.DataTable ObtenerTotalFacturasTimbradoMonto(DateTime inicio, DateTime fin)
        {
            try
            {
                Conexion oData = new Conexion();
                string Sql = @"
                SELECT v.INVOICE_ID ,v.PDF,v.XML, v.UUID, c.VAT_REGISTRATION, c.NAME, 0 as Total
                FROM [APP].[dbo].[VMX_FE] v
                INNER JOIN FIMA..RECEIVABLE r ON r.INVOICE_ID=v.INVOICE_ID 
                INNER JOIN FIMA..CUSTOMER c ON c.ID=r.CUSTOMER_ID  
                WHERE CONVERT(datetime, v.FechaTimbrado, 126) BETWEEN '" + inicio.ToString("yyyy-MM-dd") + @"' AND '" + fin.ToString("yyyy-MM-dd") + @"'
                AND r.STATUS<>'X' 
                AND r.TYPE='I' 
                ";
                System.Data.DataTable oDataTableR = oData.EjecutarConsulta(Sql);

                // Recorrer cada DataRow en el DataTable.
                foreach (System.Data.DataRow row in oDataTableR.Rows)
                {
                    // Leer el valor XML de la fila actual.
                    string xmlData = row["XML"].ToString();

                    // Llamar al método ObtenerSubtotal y pasar el XML.
                    decimal subtotal = ObtenerCFDITotal(xmlData);

                    // Guardar el subtotal en el campo 'Total' de la fila actual.
                    row["Total"] = subtotal;
                }


                return oDataTableR;
            }
            catch (Exception error)
            {
                string mensaje = "";
                mensaje += error.Message.ToString();
                if (error.InnerException != null)
                {
                    mensaje += " " + error.InnerException.Message.ToString();
                }
                mensaje += "\n CImpuesto - 36 - ObtenerTotalFacturasTimbradoMonto";
                // Invalid file type selected; display an error.
                MessageBox.Show(mensaje,
                                "ObtenerTotalFacturasTimbradoMonto",
                                MessageBoxButtons.OK,
                                MessageBoxIcon.Error);
                return null;
            }
        }


        private static decimal ObtenerCFDITotal(string xml)
        {
            try
            {
                if (!File.Exists(xml))
                {
                   return 0;
                }
                //Abrir el archivo y copiar los sellos
                XmlDocument doc = new XmlDocument();
                doc.Load(xml);
                XmlNodeList comprobante = doc.GetElementsByTagName("cfdi:Comprobante");

                string total = "0";

                try
                {
                    total = comprobante[0].Attributes["SubTotal"].Value;
                }
                catch
                {

                }

                string tipoCambio = "1";

                try
                {
                    tipoCambio = comprobante[0].Attributes["TipoCambio"].Value;
                }
                catch
                {

                }

                return decimal.Parse(total)* decimal.Parse(tipoCambio);
            }
            catch (Exception error)
            {
                string mensaje = "";
                mensaje += error.Message.ToString();
                if (error.InnerException != null)
                {
                    mensaje += " " + error.InnerException.Message.ToString();
                }
                mensaje += "\n CImpuesto - 36 - ObtenerCFDITotal";
                // Invalid file type selected; display an error.
                MessageBox.Show(mensaje,
                                "TotalFacturas",
                                MessageBoxButtons.OK,
                                MessageBoxIcon.Error);
                return 0;
            }
        }
        static public decimal TotalAsentadoCobrado(DateTime inicio, DateTime fin)
        {
            try
            {
                Conexion oData = new Conexion();
                
                string Sql = ObtenerTotalAsentadoCobrado(inicio, fin);
                System.Data.DataTable oDataTableR = oData.EjecutarConsulta(Sql);
                if (oDataTableR != null)
                {
                    foreach (System.Data.DataRow oDataRowR in oDataTableR.Rows)
                    {
                        return decimal.Parse(oDataRowR["Total"].ToString());
                    }
                }
                return 0;
            }
            catch (Exception error)
            {
                string mensaje = "";
                mensaje += error.Message.ToString();
                if (error.InnerException != null)
                {
                    mensaje += " " + error.InnerException.Message.ToString();
                }
                mensaje += "\n CImpuesto - 36 - TotalFacturas";
                // Invalid file type selected; display an error.
                MessageBox.Show(mensaje,
                                "TotalFacturas",
                                MessageBoxButtons.OK,
                                MessageBoxIcon.Error);
                return 0;
            }
        }
        public static string ObtenerTotalAsentadoCobrado(DateTime inicio, DateTime fin, bool detallado=false)
        {
            Conexion oData = new Conexion();
            String detalladoString = "SUM(crd.AMOUNT) as Total";
            if (detallado)
            {
                detalladoString = "*";
            }
            string Sql = @"
                SELECT  " + detalladoString + @" 
                FROM CASH_RECEIPT_DIST crd
                WHERE crd.POSTING_DATE>=" + oData.convertir_fecha(inicio, "i") + @" AND crd.POSTING_DATE<=" + oData.convertir_fecha(fin, "f") + @"
                AND crd.GL_ACCOUNT_ID IN ('20103003')
                AND crd.CURRENCY_ID='MN'
                AND crd.AMOUNT_TYPE='CR'
                AND crd.POSTING_STATUS='P'
                ";
            return Sql;
        }

        static public decimal TotalAsentadoPagado(DateTime inicio, DateTime fin)
        {
            try
            {
                Conexion oData = new Conexion();
                string Sql = @"
                SELECT SUM(crd.AMOUNT) as Total 
                FROM CASH_DISBURSE_DIST crd
                WHERE crd.POSTING_DATE>=" + oData.convertir_fecha(inicio, "i") + @" AND crd.POSTING_DATE<=" + oData.convertir_fecha(fin, "f") + @"
                AND crd.GL_ACCOUNT_ID IN (SELECT TAX_GL_ACCT_ID FROM VAT)
                AND crd.CURRENCY_ID='MN'
                AND crd.AMOUNT_TYPE='CR'
                AND crd.POSTING_STATUS='P'
                ";
                System.Data.DataTable oDataTableR = oData.EjecutarConsulta(Sql);
                if (oDataTableR != null)
                {
                    foreach (System.Data.DataRow oDataRowR in oDataTableR.Rows)
                    {
                        return decimal.Parse(oDataRowR["Total"].ToString());
                    }
                }
                return 0;
            }
            catch (Exception error)
            {
                string mensaje = "";
                mensaje += error.Message.ToString();
                if (error.InnerException != null)
                {
                    mensaje += " " + error.InnerException.Message.ToString();
                }
                mensaje += "\n CImpuesto - 36 - TotalFacturas";
                // Invalid file type selected; display an error.
                MessageBox.Show(mensaje,
                                "TotalFacturas",
                                MessageBoxButtons.OK,
                                MessageBoxIcon.Error);
                return 0;
            }
        }

        static public decimal TotalFacturasAsentado(DateTime inicio, DateTime fin)
        {
            try
            {
                Conexion oData = new Conexion();
                string Sql = ObtenerTotalFacturasAsentado(inicio, fin);
                System.Data.DataTable oDataTableR = oData.EjecutarConsulta(Sql);
                if (oDataTableR != null)
                {
                    foreach (System.Data.DataRow oDataRowR in oDataTableR.Rows)
                    {
                        return decimal.Parse(oDataRowR["Total"].ToString());
                    }
                }
                return 0;
            }
            catch (Exception error)
            {
                string mensaje = "";
                mensaje += error.Message.ToString();
                if (error.InnerException != null)
                {
                    mensaje += " " + error.InnerException.Message.ToString();
                }
                mensaje += "\n CImpuesto - 36 - TotalFacturas";
                // Invalid file type selected; display an error.
                MessageBox.Show(mensaje,
                                "TotalFacturas",
                                MessageBoxButtons.OK,
                                MessageBoxIcon.Error);
                return 0;
            }
        }
        static public string ObtenerTotalFacturasAsentado(DateTime inicio, DateTime fin, bool detallado = false)
        {
            Conexion oData = new Conexion();
            String detalladoString = "SUM(rd.AMOUNT) as Total";
            if (detallado)
            {
                detalladoString = "*";
            }
            string Sql = @"
                SELECT  " + detalladoString + @" 
                FROM RECEIVABLE r
                INNER JOIN RECEIVABLE_DIST rd ON rd.INVOICE_ID=r.INVOICE_ID
                WHERE r.POSTING_DATE>=" + oData.convertir_fecha(inicio, "i") + @" AND r.POSTING_DATE<=" + oData.convertir_fecha(fin, "f") + @"
                AND rd.CURRENCY_ID='MN'
                AND rd.GL_ACCOUNT_ID NOT IN (SELECT TAX_GL_ACCT_ID FROM VAT)
                AND rd.AMOUNT_TYPE='CR'
                AND rd.POSTING_STATUS='P'
                AND r.STATUS<>'X'
                AND r.TYPE='I'
                ";
            return Sql;
        }



        static public decimal TotalVoucherPesos(DateTime inicio, DateTime fin)
        {
            try
            {
                Conexion oData = new Conexion();
                string Sql = @"
                SELECT SUM(pl.VAT_AMOUNT*p.SELL_RATE) as Total
                FROM PAYABLE p
                INNER JOIN PAYABLE_LINE pl ON pl.VOUCHER_ID=p.VOUCHER_ID
                WHERE p.POSTING_DATE>=" + oData.convertir_fecha(inicio, "i") + @" AND p.POSTING_DATE<=" + oData.convertir_fecha(fin, "f") + @"
                AND p.PAY_STATUS<>'X'
                ";
                System.Data.DataTable oDataTableR = oData.EjecutarConsulta(Sql);
                if (oDataTableR != null)
                {
                    foreach (System.Data.DataRow oDataRowR in oDataTableR.Rows)
                    {
                        return decimal.Parse(oDataRowR["Total"].ToString());
                    }
                }
                return 0;
            }
            catch (Exception error)
            {
                string mensaje = "";
                mensaje += error.Message.ToString();
                if (error.InnerException != null)
                {
                    mensaje += " " + error.InnerException.Message.ToString();
                }
                mensaje += "\n CImpuesto - 36 - TotalFacturas";
                // Invalid file type selected; display an error.
                MessageBox.Show(mensaje,
                                "TotalFacturas",
                                MessageBoxButtons.OK,
                                MessageBoxIcon.Error);
                return 0;
            }
        }

        static public decimal TotalVoucher(DateTime inicio, DateTime fin)
        {
            try
            {
                Conexion oData = new Conexion();
                string Sql = @"
                SELECT COUNT(*) as Total
                FROM PAYABLE p
                WHERE p.POSTING_DATE>=" + oData.convertir_fecha(inicio, "i") + @" AND p.POSTING_DATE<=" + oData.convertir_fecha(fin, "f") + @"
                AND p.PAY_STATUS<>'X'
                ";
                System.Data.DataTable oDataTableR = oData.EjecutarConsulta(Sql);
                if (oDataTableR != null)
                {
                    foreach (System.Data.DataRow oDataRowR in oDataTableR.Rows)
                    {
                        return decimal.Parse(oDataRowR["Total"].ToString());
                    }
                }
                return 0;
            }
            catch (Exception error)
            {
                string mensaje = "";
                mensaje += error.Message.ToString();
                if (error.InnerException != null)
                {
                    mensaje += " " + error.InnerException.Message.ToString();
                }
                mensaje += "\n CImpuesto - 36 - TotalFacturas";
                // Invalid file type selected; display an error.
                MessageBox.Show(mensaje,
                                "TotalFacturas",
                                MessageBoxButtons.OK,
                                MessageBoxIcon.Error);
                return 0;
            }
        }

        static public decimal TotalVoucherAsentado(DateTime inicio, DateTime fin)
        {
            try
            {
                Conexion oData = new Conexion();
                string Sql = @"
                SELECT SUM(rd.AMOUNT) as Total
                FROM PAYABLE r
                INNER JOIN PAYABLE_DIST rd ON rd.VOUCHER_ID=r.VOUCHER_ID
                WHERE r.POSTING_DATE>=" + oData.convertir_fecha(inicio, "i") + @" AND r.POSTING_DATE<=" + oData.convertir_fecha(fin, "f") + @"
                AND rd.GL_ACCOUNT_ID IN (SELECT TAX_GL_ACCT_ID FROM VAT)
                AND rd.CURRENCY_ID='MN'
                AND rd.AMOUNT_TYPE='DR'
                AND rd.POSTING_STATUS='P'
                ";
                System.Data.DataTable oDataTableR = oData.EjecutarConsulta(Sql);
                if (oDataTableR != null)
                {
                    foreach (System.Data.DataRow oDataRowR in oDataTableR.Rows)
                    {
                        return decimal.Parse(oDataRowR["Total"].ToString());
                    }
                }
                return 0;
            }
            catch (Exception error)
            {
                string mensaje = "";
                mensaje += error.Message.ToString();
                if (error.InnerException != null)
                {
                    mensaje += " " + error.InnerException.Message.ToString();
                }
                mensaje += "\n CImpuesto - 36 - TotalFacturas";
                // Invalid file type selected; display an error.
                MessageBox.Show(mensaje,
                                "TotalFacturas",
                                MessageBoxButtons.OK,
                                MessageBoxIcon.Error);
                return 0;
            }
        }
    }
}
