#region Copyright Syncfusion Inc. 2001 - 2017
// Copyright Syncfusion Inc. 2001 - 2017. All rights reserved.
// Use of this code is subject to the terms of our license.
// A copy of the current license can be obtained at any time by e-mailing
// licensing@syncfusion.com. Any infringement will be prosecuted under
// applicable laws. 
#endregion
using CFDI40;
using Generales;
using ModeloSincronizacion;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Drawing;
using System.IO;
using System.Net;
using System.Runtime.InteropServices;
using System.Text;
using System.Windows.Forms;
using System.Xml;
using System.Xml.Serialization;
using System.Linq;
using Pago10;
using ModeloDocumentosFX;
using ModeloComprobantePago;

namespace CxP
{
    public partial class frmPrincipalNuevo : Syncfusion.Windows.Forms.MetroForm
    {
        string cfdiProveedores = "";
        List<cCFDI> lista;
        ModeloSincronizacionContainer dbContext;
        public frmPrincipalNuevo()
        {
            dbContext = new ModeloSincronizacionContainer();
            bgwFacturas = new BackgroundWorker();
            InitializeComponent();
            
            if (System.ComponentModel.LicenseManager.UsageMode == System.ComponentModel.LicenseUsageMode.Designtime)
            {

            }

            try
            {
                cfdiProveedores = ConfigurationManager.AppSettings["cfdiProveedores"].ToString();
                ruta.Text = cfdiProveedores;
                lista = new List<cCFDI>();
             }
            catch
            {

            }
            this.notifyIcon.BalloonTipIcon = System.Windows.Forms.ToolTipIcon.Info;
            this.notifyIcon.BalloonTipText = Application.ProductName;
            this.notifyIcon.BalloonTipTitle = "Sincronizador CxP minimizado";
            this.notifyIcon.Text = "CxP FX - minimizado";
        }

        private void frmCxPServicio_Resize(object sender, EventArgs e)
        {
            if (this.WindowState == FormWindowState.Minimized)
            {
                notifyIcon.Visible = true;
                notifyIcon.ShowBalloonTip(3000);
                this.ShowInTaskbar = false;
            }
        }

        private void notifyIcon_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            this.WindowState = FormWindowState.Normal;
            this.ShowInTaskbar = true;
            notifyIcon.Visible = false;
        }

        

        private bool validar33(string archivo)
        {
            //Pasear el comprobante
            try
            {
                if (!IsFileLocked(archivo))
                {
                    XmlSerializer serializer_CFD_3 = new XmlSerializer(typeof(Comprobante));
                    TextReader reader = new StreamReader(archivo);
                    Comprobante oComprobante = (Comprobante)serializer_CFD_3.Deserialize(reader);
                    reader.Dispose();
                    reader.Close();

                    //Agregar fila
                    agregarRegistro(oComprobante, archivo);
                    return true;
                }
                return false;

            }
            catch (Exception error)
            {
                ErrorFX.mostrar(error, false, true, "CxP frmPrincipalNuevo 102 ",true);
                return false;
            }
        }
        private void agregarRegistro(Comprobante oComprobante, string archivo)
        {
            XmlDocument docXML = new XmlDocument();
            docXML.Load(archivo);
            XmlNodeList TimbreFiscalDigital = docXML.GetElementsByTagName("tfd:TimbreFiscalDigital");
            if (TimbreFiscalDigital == null)
            {
                return;
            }
            //Validar el folio fiscal
            string uuidtr = "";
            if (TimbreFiscalDigital[0] != null)
            {
                uuidtr = TimbreFiscalDigital[0].Attributes["UUID"].Value;
            }
            //Validar si el folio fiscal ya esta sincronizado
            archivoSync oarchivoSync = (from a in dbContext.archivoSyncSet
                                        where a.UUID.Equals(uuidtr)
                                        select a).FirstOrDefault();
                                       ;

            if (oarchivoSync != null)
            {
                return;
            }

            bool encontrado = false;
            foreach (cCFDI c in lista)
            {
                if (c.uuid.Equals(uuidtr))
                {
                    encontrado = true;
                }
            }

            if (!encontrado)
            {
                //Agregar a la lista
                agregarComprobante(oComprobante, archivo);
            }

        }

        private void agregarComprobante(Comprobante oComprobante, string archivo)
        {
            cCFDI ocCFDI = new cCFDI();
            ocCFDI.archivo = archivo;
            ocCFDI.version = oComprobante.Version;
            ocCFDI.tipoDeComprobante = oComprobante.TipoDeComprobante.ToString();
            ocCFDI.version = oComprobante.Version;
            ocCFDI.subTotal = oComprobante.SubTotal;
            ocCFDI.total = oComprobante.Total;
            ocCFDI.rfcEmisor = oComprobante.Emisor.Rfc;
            ocCFDI.nombre = oComprobante.Emisor.Nombre;
            ocCFDI.moneda = oComprobante.Moneda;
            ocCFDI.rfcReceptor = oComprobante.Receptor.Rfc;
            ocCFDI.fechaFactura = oComprobante.Fecha;
            ocCFDI.tipoCambio = oComprobante.TipoCambio;
            ocCFDI.formaPago = oComprobante.FormaPago.ToString();
            ocCFDI.metodoPago = oComprobante.MetodoPago;
            ocCFDI.lugarexpedicion = oComprobante.LugarExpedicion;
            ocCFDI.usoCFDI = oComprobante.Receptor.UsoCFDI.ToString();
            ocCFDI.status = false;
            ocCFDI.tipoCambio = oComprobante.TipoCambio;

            ocCFDI.serie = "";
            if (!String.IsNullOrEmpty(oComprobante.Serie))
            {
                ocCFDI.serie = oComprobante.Serie;
            }
            ocCFDI.folio = "";
            if (!String.IsNullOrEmpty(oComprobante.Folio))
            {
                ocCFDI.folio = oComprobante.Folio;
            }

            ocCFDI.folio = ocCFDI.serie + "" + ocCFDI.folio;

            XmlDocument docXML = new XmlDocument();
            docXML.Load(archivo);
            XmlNodeList TimbreFiscalDigital = docXML.GetElementsByTagName("tfd:TimbreFiscalDigital");
            if (TimbreFiscalDigital != null)
            {
                if (TimbreFiscalDigital[0] != null)
                {
                    ocCFDI.uuid = TimbreFiscalDigital[0].Attributes["UUID"].Value;
                }

            }
            lista.Add(ocCFDI);
        }


        const int ERROR_SHARING_VIOLATION = 32;
        const int ERROR_LOCK_VIOLATION = 33;
        private bool IsFileLocked(string file)
        {
            //check that problem is not in destination file
            if (File.Exists(file) == true)
            {
                FileStream stream = null;
                try
                {
                    stream = File.Open(file, FileMode.Open, FileAccess.ReadWrite, FileShare.None);
                }
                catch (Exception ex2)
                {
                    //_log.WriteLog(ex2, "Error in checking whether file is locked " + file);
                    int errorCode = Marshal.GetHRForException(ex2) & ((1 << 16) - 1);
                    if ((ex2 is IOException) && (errorCode == ERROR_SHARING_VIOLATION || errorCode == ERROR_LOCK_VIOLATION))
                    {
                        return true;
                    }
                }
                finally
                {
                    if (stream != null)
                        stream.Close();
                }
            }
            return false;
        }

        BackgroundWorker bgwFacturas;
        private void actualizarFacturas()
        {
            //Asincronado
            bgwFacturas = new BackgroundWorker();
            bgwFacturas.WorkerSupportsCancellation = true;
            bgwFacturas.WorkerReportsProgress = true;
            bgwFacturas.DoWork += new DoWorkEventHandler(bgwFacturas_DoWork);
            bgwFacturas.ProgressChanged += new ProgressChangedEventHandler(bgwFacturas_ProgressChanged);
            bgwFacturas.RunWorkerCompleted += new RunWorkerCompletedEventHandler(bgwFacturas_RunWorkerCompleted);
            bgwFacturas.RunWorkerAsync();
        }

        private void bgwFacturas_DoWork(object sender, DoWorkEventArgs e)
        {
            enviarArchivosIngresos();
            validarPagos();
        }

        private void validarPagos()
        {
            foreach (cCFDI c in lista)
            {
                if (!c.status)
                {
                    if (c.tipoDeComprobante.Equals("P"))
                    {
                        asigar_texto_actualizacion("Validando CFDI Pago Folio: " + c.folio + " RFC: " + c.rfcEmisor + "  UUID: " + c.uuid + " "
                        + DateTime.Now.ToShortDateString()
                        + " " + DateTime.Now.ToShortTimeString());
                        validarArchivoPago(c);
                    }

                }
            }
        }
        private bool validarArchivoPago(cCFDI c)
        {
            try
            {
                ModeloComprobantePago.documentosfxEntities dbContextPago = new documentosfxEntities();

                //Buscar que todos los Voucher esten aplicados en el pago
                //Obtener los pagos y los documentos relacionados y validar que si esten pagados
                XmlDocument docXML = new XmlDocument();
                docXML.Load(c.archivo);
                Comprobante oComprobante;
                if (!IsFileLocked(c.archivo))
                {
                    XmlSerializer serializer_CFD_3 = new XmlSerializer(typeof(Comprobante));
                    TextReader reader = new StreamReader(c.archivo);
                    oComprobante = (Comprobante)serializer_CFD_3.Deserialize(reader);
                    reader.Dispose();
                    reader.Close();
                }


                XmlNodeList doctoRelacionados = docXML.GetElementsByTagName("pago10:DoctoRelacionado");
                if (doctoRelacionados == null)
                {
                    return false;
                }

                //Guardar en el archivoSync
                archivoSync registroNuevo = new archivoSync();
                registroNuevo.idDocumentoSet = "";
                registroNuevo.folio = c.folio;
                registroNuevo.xml = c.archivo;
                registroNuevo.pdf = c.archivo.Replace(".xml", ".pdf");
                registroNuevo.rfcEmisor = c.rfcEmisor;
                registroNuevo.rfcReceptor = c.rfcReceptor;
                registroNuevo.UUID = c.uuid;
                registroNuevo.tipoComprobante = c.tipoDeComprobante;
                registroNuevo.proceso = 0;
                dbContext.archivoSyncSet.Add(registroNuevo);
                dbContext.SaveChanges();


                //Guardar la cabecera de pagos
                XmlNodeList pago = docXML.GetElementsByTagName("pago10:Pago");
                if (pago == null)
                {
                    return false;
                }

                XmlDocument docXMLPago = new XmlDocument();
                docXMLPago.Load(c.archivo);
                Pago10.Pagos oPagos;
                if (!IsFileLocked(c.archivo))
                {
                    XmlSerializer serializer_CFD_3 = new XmlSerializer(typeof(Pago10.Pagos));
                    TextReader reader = new StreamReader(c.archivo);
                    oPagos = (Pago10.Pagos)serializer_CFD_3.Deserialize(reader);
                    reader.Dispose();
                    reader.Close();
                }

                //Guardar el pago
                //<pago10:Pago CtaBeneficiario="0188306966" FechaPago="2018-10-08T12:00:00" FormaDePagoP="03" 
                //MonedaP="USD" Monto="232.58" RfcEmisorCtaBen="BBA830831LJ2" TipoCambioP="19.9444">

                for (int i = 0; i < pago.Count; i++)
                {
                    pago oPago = new pago();
                    oPago.CtaBeneficiario = pago[i].Attributes["CtaBeneficiario"].Value.ToString();
                    oPago.fechaPago = DateTime.Parse(pago[i].Attributes["FechaPago"].Value.ToString());
                    oPago.formaDePagoP = pago[i].Attributes["FormaDePagoP"].Value.ToString();
                    oPago.monedaP = pago[i].Attributes["MonedaP"].Value.ToString();
                    oPago.monto = decimal.Parse(pago[i].Attributes["Monto"].Value.ToString());
                    oPago.RfcEmisorCtaBen = pago[i].Attributes["RfcEmisorCtaBen"].Value.ToString();
                    oPago.tipoCambioP = decimal.Parse(pago[i].Attributes["IdDocumento"].Value.ToString());
                    dbContextPago.pagoSet.Add(oPago);
                    dbContext.SaveChanges();

                    for (int y = 0; y < doctoRelacionados.Count; y++)
                    {
                        //<pago10:DoctoRelacionado IdDocumento="24D8F5F5-8564-42E1-B705-0776235F1642" Serie="SL" Folio="78853" 
                        //MonedaDR ="USD" MetodoDePagoDR="PPD" NumParcialidad="1" ImpSaldoAnt="2837.40" ImpPagado="2837.40" ImpSaldoInsoluto="0.00" />
                        string IdDocumento = doctoRelacionados[i].Attributes["IdDocumento"].Value.ToString();
                        string Serie = doctoRelacionados[i].Attributes["Serie"].Value.ToString();
                        string Folio = doctoRelacionados[i].Attributes["Folio"].Value.ToString();
                        string MonedaDR = doctoRelacionados[i].Attributes["MonedaDR"].Value.ToString();
                        string MetodoDePagoDR = doctoRelacionados[i].Attributes["MetodoDePagoDR"].Value.ToString();
                        string NumParcialidad = doctoRelacionados[i].Attributes["NumParcialidad"].Value.ToString();
                        string ImpSaldoAnt = doctoRelacionados[i].Attributes["ImpSaldoAnt"].Value.ToString();
                        decimal ImpPagado = decimal.Parse(doctoRelacionados[i].Attributes["ImpPagado"].Value.ToString());
                        string ImpSaldoInsoluto = doctoRelacionados[i].Attributes["ImpSaldoInsoluto"].Value.ToString();

                        if (!MetodoDePagoDR.Equals("PPD"))
                        {
                            //El metodo de pago DR no es PPD
                            return false;
                        }



                        //Validar si es PPD MetodoDePagoDR
                        //
                        //<pago10:Pagos Version="1.0">
                        //<pago10:Pago CtaBeneficiario="0481510398" CtaOrdenante="112180000019253614" FechaPago="2018-10-08T12:00:00" FormaDePagoP="03" MonedaP="MXN" Monto="9846.54" RfcEmisorCtaBen="BBA830831LJ2" RfcEmisorCtaOrd="BMI9704113PA">
                        //<pago10:DoctoRelacionado Folio="16023" IdDocumento="44E3FF30-E405-4759-A627-3B13F96FBB92" ImpPagado="4674.80" ImpSaldoAnt="4674.80" ImpSaldoInsoluto="0.00" MetodoDePagoDR="PPD" MonedaDR="MXN" NumParcialidad="1"></pago10:DoctoRelacionado>
                        //<pago10:DoctoRelacionado Folio="16065" IdDocumento="6114AAEE-BF86-4D1F-B10C-7D21DF6DAF41" ImpPagado="5171.74" ImpSaldoAnt="5171.74" ImpSaldoInsoluto="0.00" MetodoDePagoDR="PPD" MonedaDR="MXN" NumParcialidad="1"></pago10:DoctoRelacionado>
                        //</pago10:Pago>
                        //</pago10:Pagos>

                        //if (validarPago(IdDocumento, ImpPagado))
                        //{

                        //}

                    }
                }

                
            }
            catch (Exception error)
            {
                //Enviar error
                ErrorFX.mostrar(error, true, true, "frmPrincipalNuevo - 330", true);
            }
            return false;
        }

        private bool validarPago(string idDocumento, decimal impPagado)
        {
            try
            {
                ModeloDocumentosFXContainer dbContext = new ModeloDocumentosFXContainer();
                //Buscar el UUID pagado en Visual
                var result = from b in dbContext.cxpConciliacionSet
                                             .Where(a => a.uuid.Contains(idDocumento))
                            select b;

                List<cxpConciliacion> dt = result.ToList();
                
                foreach (cxpConciliacion registro in dt)
                {
                    if (registro != null)
                    {
                        //Buscar el monto en el pago
                        buscarPago(registro, impPagado);
                    }

                }

                dbContext.Dispose();
            }
            catch (Exception error)
            {
                ErrorFX.mostrar(error, true, true,"frmPrincipalNuevo - 361", true);
            }
            return false;
        }

        private void buscarPago(cxpConciliacion registro, decimal impPagado)
        {
            string sSQL = @"
            SELECT TOP 1 *
            FROM CASH_DISBURSEMENT AS cd INNER JOIN
            CASH_DISBURSE_LINE AS cdl ON cd.BANK_ACCOUNT_ID = cdl.BANK_ACCOUNT_ID AND cd.CONTROL_NO = cdl.CONTROL_NO INNER JOIN
            PAYABLE AS p ON cdl.VOUCHER_ID = p.VOUCHER_ID INNER JOIN
            BANK_ACCOUNT ON cd.BANK_ACCOUNT_ID = BANK_ACCOUNT.ID INNER JOIN
            VENDOR as v ON cd.VENDOR_ID = v.ID 
            WHERE p.VOICHER_ID='" + registro.voucherId + @"'
            AND cdl.AMOUNT=" + impPagado.ToString() + @"
            ";
            //DataTable oDataTable = oData_ERPp.EjecutarConsulta(sSQL);

        }

        private void enviarArchivosIngresos()
        {
            foreach (cCFDI c in lista)
            {
                if (!c.status)
                {
                    if ((c.tipoDeComprobante.Equals("I")) | c.tipoDeComprobante.Equals("E"))
                    {
                        asigar_texto_actualizacion("CFDI Folio: " + c.folio + " RFC: " + c.rfcEmisor + "  UUID: " + c.uuid + " "
                        + DateTime.Now.ToShortDateString()
                        + " " + DateTime.Now.ToShortTimeString());

                        //Validar reglas PPD y 99

                        subirArchivo(c);
                    }
                    
                }
            }
        }





        private void bgwFacturas_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            asigar_texto_actualizacion("Finalizado:" + DateTime.Now.ToShortDateString() + " " + DateTime.Now.ToShortTimeString());
            bgwFacturas.RunWorkerAsync();
        }
        private void bgwFacturas_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            asigar_texto_actualizacion(e.UserState.ToString());
        }
        private void asigar_texto_actualizacionPago(string texto)
        {
            if (textoHora.InvokeRequired)
            {
                MethodInvoker invoker = new MethodInvoker(delegate
                {
                    label1.Text = texto + " " + DateTime.Now.ToShortDateString() + " " + DateTime.Now.ToShortTimeString();
                });
                textoHora.Invoke(invoker);
            }
            else
            {
                textoHora.Text = texto;
            }
        }

        private void asigar_texto_actualizacion(string texto)
        {
            if (textoHora.InvokeRequired)
            {
                MethodInvoker invoker = new MethodInvoker(delegate
                {
                    textoHora.Text = texto + " " + DateTime.Now.ToShortDateString() + " " + DateTime.Now.ToShortTimeString();
                });
                textoHora.Invoke(invoker);
            }
            else
            {
                textoHora.Text = texto;
            }
        }

        private void asigar_total_actualizacion(string texto)
        {
            if (lblTotal.InvokeRequired)
            {
                MethodInvoker invoker = new MethodInvoker(delegate
                {
                    lblTotal.Text = "Total: " + lista.Count.ToString() ;
                });
                lblTotal.Invoke(invoker);
            }
            else
            {
                lblTotal.Text = texto;
            }
        }

        //TODO: Subir todos en Gatwaey 
        //TODO: Cargar local Validar
        //TODO: Lista de pagos y postearlas
        //TODO: Agregar un plantilla para el formato de pago

        private bool validarUUID(string uuid)
        {
            using (WebClient client = new WebClient())
            {
                string url = ConfigurationManager.AppSettings["portal"].ToString() + "Conector/existeUUID/";

                HttpWebRequest requestToServerEndpoint =
                (HttpWebRequest)WebRequest.Create(url);
                string boundaryString = "----SomeRandomText";
                // Set the http request header \\
                requestToServerEndpoint.Method = WebRequestMethods.Http.Post;
                requestToServerEndpoint.ContentType = "multipart/form-data; boundary=" + boundaryString;
                requestToServerEndpoint.KeepAlive = true;
                requestToServerEndpoint.Credentials = System.Net.CredentialCache.DefaultCredentials;



                MemoryStream postDataStream = new MemoryStream();
                StreamWriter postDataWriter = new StreamWriter(postDataStream);

                // Include value from the myFileDescription text area in the post data
                postDataWriter.Write("\r\n--" + boundaryString + "\r\n");
                postDataWriter.Write("Content-Disposition: form-data; name=\"{0}\"\r\n\r\n{1}", "uuid", uuid);
                postDataWriter.Flush();

                // Set the http request body content length
                requestToServerEndpoint.ContentLength = postDataStream.Length;

                // Dump the post data from the memory stream to the request stream
                using (Stream s = requestToServerEndpoint.GetRequestStream())
                {
                    postDataStream.WriteTo(s);
                }
                postDataStream.Close();

                WebResponse response = requestToServerEndpoint.GetResponse();
                StreamReader responseReader = new StreamReader(response.GetResponseStream());
                string replyFromServer = responseReader.ReadToEnd();

                //Convertir en Json ver los errores
                try
                {
                    cResultadoSubir resultado = JsonConvert.DeserializeObject<cResultadoSubir>(replyFromServer);
                    return resultado.status;
                }
                catch (Exception error)
                {
                    ErrorFX.mostrar(error + Environment.NewLine
                        + replyFromServer + " CxP frmPrincipalNuevo 356 ", false, true, true);
                }

                return true;
            }
        }

        private void subirArchivo(cCFDI c)
        {
            //Validar existencia en el portal
            //Existe el UUID
            //string uuditr = drv.Cells["UUID"].Value.ToString();
            //System.Threading.Thread.Sleep(10000);
            if (!this.validarUUID(c.uuid))
            {
                c.status = true;
                return;
            }
            //Guardar en base de datos

            archivoSync registroNuevo = new archivoSync();
            registroNuevo.idDocumentoSet = "";
            registroNuevo.folio = c.folio;
            registroNuevo.xml = c.archivo;
            registroNuevo.pdf = c.archivo.Replace(".xml",".pdf");
            registroNuevo.rfcEmisor = c.rfcEmisor;
            registroNuevo.rfcReceptor = c.rfcReceptor;
            registroNuevo.UUID = c.uuid;
            registroNuevo.tipoComprobante = c.tipoDeComprobante;
            registroNuevo.proceso = 0;
            dbContext.archivoSyncSet.Add(registroNuevo);
            dbContext.SaveChanges();

            //System.Threading.Thread.Sleep(5000);
            using (WebClient client = new WebClient())
            {
                

                string url = ConfigurationManager.AppSettings["portal"].ToString() + "Conector/guardarCxP/";


                HttpWebRequest requestToServerEndpoint =
                (HttpWebRequest)WebRequest.Create(url);

                string boundaryString = "----SomeRandomText";
                string fileUrl = c.archivo;

                // Set the http request header \\
                requestToServerEndpoint.Method = WebRequestMethods.Http.Post;
                requestToServerEndpoint.ContentType = "multipart/form-data; boundary=" + boundaryString;
                requestToServerEndpoint.KeepAlive = true;
                requestToServerEndpoint.Credentials = System.Net.CredentialCache.DefaultCredentials;

                // Use a MemoryStream to form the post data request,
                // so that we can get the content-length attribute.
                MemoryStream postDataStream = new MemoryStream();
                StreamWriter postDataWriter = new StreamWriter(postDataStream);

                // Include value from the myFileDescription text area in the post data
                postDataWriter.Write("\r\n--" + boundaryString + "\r\n");
                postDataWriter.Write("Content-Disposition: form-data; name=\"{0}\"\r\n\r\n{1}", "total", c.total);

                postDataWriter.Write("\r\n--" + boundaryString + "\r\n");
                postDataWriter.Write("Content-Disposition: form-data; name=\"{0}\"\r\n\r\n{1}", "uuid", c.uuid);

                postDataWriter.Write("\r\n--" + boundaryString + "\r\n");
                postDataWriter.Write("Content-Disposition: form-data; name=\"{0}\"\r\n\r\n{1}", "version", c.version);

                postDataWriter.Write("\r\n--" + boundaryString + "\r\n");
                postDataWriter.Write("Content-Disposition: form-data; name=\"{0}\"\r\n\r\n{1}", "folio", c.folio);

                postDataWriter.Write("\r\n--" + boundaryString + "\r\n");
                postDataWriter.Write("Content-Disposition: form-data; name=\"{0}\"\r\n\r\n{1}", "moneda", c.moneda);

                postDataWriter.Write("\r\n--" + boundaryString + "\r\n");
                postDataWriter.Write("Content-Disposition: form-data; name=\"{0}\"\r\n\r\n{1}", "fecha", c.fechaFactura.ToString("yyyy-MM-dd"));

                postDataWriter.Write("\r\n--" + boundaryString + "\r\n");
                postDataWriter.Write("Content-Disposition: form-data; name=\"{0}\"\r\n\r\n{1}", "tipoComprobante", c.tipoDeComprobante);

                postDataWriter.Write("\r\n--" + boundaryString + "\r\n");
                postDataWriter.Write("Content-Disposition: form-data; name=\"{0}\"\r\n\r\n{1}", "rfcReceptor", c.rfcReceptor);

                postDataWriter.Write("\r\n--" + boundaryString + "\r\n");
                postDataWriter.Write("Content-Disposition: form-data; name=\"{0}\"\r\n\r\n{1}", "rfcEmisor", c.rfcEmisor);

                postDataWriter.Write("\r\n--" + boundaryString + "\r\n");
                postDataWriter.Write("Content-Disposition: form-data; name=\"{0}\"\r\n\r\n{1}", "metodoDePago", c.metodoPago);

                postDataWriter.Write("\r\n--" + boundaryString + "\r\n");
                postDataWriter.Write("Content-Disposition: form-data; name=\"{0}\"\r\n\r\n{1}", "formaDePago", c.formaPago);

                postDataWriter.Write("\r\n--" + boundaryString + "\r\n");
                postDataWriter.Write("Content-Disposition: form-data; name=\"{0}\"\r\n\r\n{1}", "UsoCFDI", c.usoCFDI);

                postDataWriter.Write("\r\n--" + boundaryString + "\r\n");
                postDataWriter.Write("Content-Disposition: form-data; name=\"{0}\"\r\n\r\n{1}", "name", c.nombre);

                postDataWriter.Write("\r\n--" + boundaryString + "\r\n");
                postDataWriter.Write("Content-Disposition: form-data; name=\"{0}\"\r\n\r\n{1}", "lugarexpedicion", c.lugarexpedicion);

                postDataWriter.Write("\r\n--" + boundaryString + "\r\n");
                postDataWriter.Write("Content-Disposition: form-data; name=\"{0}\"\r\n\r\n{1}", "tipoCambio", c.tipoCambio);

                // Include the file in the post data
                postDataWriter.Write("\r\n--" + boundaryString + "\r\n");
                postDataWriter.Write("Content-Disposition: form-data;"
                + "name=\"{0}\";"
                + "filename=\"{1}\""
                + "\r\nContent-Type: {2}\r\n\r\n",
                "XML_FILE",
                Path.GetFileName(fileUrl),
                Path.GetExtension(fileUrl));
                postDataWriter.Flush();
                // Read the file
                FileStream fileStream = new FileStream(fileUrl, FileMode.Open, FileAccess.Read);
                byte[] buffer = new byte[1024];
                int bytesRead = 0;
                while ((bytesRead = fileStream.Read(buffer, 0, buffer.Length)) != 0)
                {
                    postDataStream.Write(buffer, 0, bytesRead);
                }
                fileStream.Close();
                postDataWriter.Write("\r\n--" + boundaryString + "--\r\n");
                postDataWriter.Flush();




                // Set the http request body content length
                requestToServerEndpoint.ContentLength = postDataStream.Length;

                // Dump the post data from the memory stream to the request stream
                using (Stream s = requestToServerEndpoint.GetRequestStream())
                {
                    postDataStream.WriteTo(s);
                }
                postDataStream.Close();

                WebResponse response = requestToServerEndpoint.GetResponse();
                StreamReader responseReader = new StreamReader(response.GetResponseStream());
                string replyFromServer = responseReader.ReadToEnd();

                //Convertir en Json ver los errores
                try
                {
                    cResultadoSubir resultado = JsonConvert.DeserializeObject<cResultadoSubir>(replyFromServer);
                    c.status = resultado.status;
                    


                }
                catch (Exception error)
                {
                    ErrorFX.mostrar(error + Environment.NewLine
                        + replyFromServer + " CxP frmPrincipalNuevo 510 ", false, true, true);
                    //System.Threading.Thread.Sleep(10000);
                    //subirArchivo(c);
                }

            }
        }

        private void button2_Click(object sender, EventArgs e)
        {

        }
        private int counter = 0;
        private void ejecutarAutomatico_CheckedChanged(object sender, EventArgs e)
        {
            if (ejecutarAutomatico.Checked)
            {
                counter = 0;
                timer1.Start();
            }
            else
            {
                counter = 0;
                timer1.Stop();
            }
        }

        private void button2_Click_1(object sender, EventArgs e)
        {
            cargar();
        }

        private void cargar()
        {
            try
            {
                string[] files;
                if (Directory.Exists(ruta.Text))
                {
                    files = Directory.GetFiles(ruta.Text, @"*.xml", SearchOption.AllDirectories);
                    lista = new List<cCFDI>();
                    foreach (string file in files)
                    {
                        validar33(file);
                    }
                    actualizarFacturas();

                }
            }
            catch (Exception error)
            {
                ErrorFX.mostrar(error, false, true, " CxP frmPrincipalNuevo 561 ", true);
            }
        }
        int segundos = 3600;
        private void timer1_Tick(object sender, EventArgs e)
        {
            if (ejecutarAutomatico.Checked)
            {
                counter++;

                proxima.Text = (segundos - counter).ToString();


                if (counter == segundos)
                {
                    timer1.Stop();
                    counter = 0;
                    cargar();
                    

                    proxima.Text = (segundos - counter).ToString();
                    
                }
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            buscar();
        }

        private void buscar()
        {
            try
            {
                using (var fbd = new FolderBrowserDialog())
                {
                    DialogResult result = fbd.ShowDialog();

                    if (result == DialogResult.OK && !string.IsNullOrWhiteSpace(fbd.SelectedPath))
                    {
                        ruta.Text = fbd.SelectedPath;
                    }
                }

            }
            catch
            {

            }
        }
    }
}
