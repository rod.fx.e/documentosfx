﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Generales
{
    public sealed class ErrorFX
    {
        public static void mostrar(Exception error, bool mostrar, bool enviarCorreo, bool enviarCorreoCliente=false)
        {
            //Agregar el cliente
            string cliente = "Cliente: Sin configurar."  + Environment.NewLine 
                + "Instrucción: Agregar en el archivo .config en AppSettings la llave cliente " + Environment.NewLine
                + "Ejemplo:" + Environment.NewLine
                + "< add key = \"cliente\" value = \"Nombre del cliente\" />" + Environment.NewLine;
            try
            {
                cliente="Cliente:" + ConfigurationManager.AppSettings["cliente"].ToString();
            }
            catch
            {

            }

            string mensaje = cliente+error.Message.ToString();
            if (error.InnerException != null)
            {
                mensaje += Environment.NewLine + error.InnerException.Message.ToString();
                if (error.InnerException.InnerException!=null)
                {
                    mensaje += Environment.NewLine + error.InnerException.InnerException.ToString();
                }
            }

            if (mostrar)
            {
                MessageBox.Show(mensaje, Application.ProductName + "-" + Application.ProductVersion, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            if (enviarCorreo)
            {
                sendSMTPMail("Documentos FX: Versión " + Application.ProductName + "-" + Application.ProductVersion
                , mensaje);
            }

            if (enviarCorreoCliente)
            {
                sendSMTPMailCliente("Documentos FX: Versión " + Application.ProductName + "-" + Application.ProductVersion
                , mensaje);
            }

            BitacoraFX.Log(mensaje);
        }

        public static void mostrar(Exception error, bool mostrar, bool enviarCorreo, string complementoMensaje, bool enviarCorreoCliente = false)
        {
            //Agregar el cliente
            string cliente = "Cliente: Sin configurar." + Environment.NewLine
                + " Instrucción: Agregar en el archivo .config en AppSettings la llave cliente " + Environment.NewLine
                + " Ejemplo:" + Environment.NewLine
                + "< add key = \"cliente\" value = \"Nombre del cliente\" />" + Environment.NewLine;
            try
            {
                cliente = "Cliente:" + ConfigurationManager.AppSettings["cliente"].ToString();
            }
            catch
            {

            }
            

            string mensaje = cliente + Environment.NewLine + error.Message.ToString();
            if (error.InnerException != null)
            {
                mensaje += Environment.NewLine + error.InnerException.Message.ToString();
            }
            if (!String.IsNullOrEmpty(complementoMensaje))
            {
                mensaje = complementoMensaje + Environment.NewLine + mensaje;
            }
            if (!Globales.automatico)
            {
                if (mostrar)
                {
                    MessageBox.Show(mensaje, Application.ProductName + "-" + Application.ProductVersion
                        , MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }

            if (enviarCorreo)
            {
                sendSMTPMail("Documentos FX: Versión " + Application.ProductName + "-" + Application.ProductVersion
                , mensaje);
            }

            if (enviarCorreoCliente)
            {
                sendSMTPMailCliente("Documentos FX: Versión " + Application.ProductName + "-" + Application.ProductVersion
                , mensaje);
            }

            BitacoraFX.Log(mensaje);

        }

        public static void mostrar(String error, bool mostrar, bool enviarCorreo, bool enviarCorreoCliente)
        {
            //Agregar el cliente
            string cliente = "Cliente: Sin configurar." + Environment.NewLine
                + "Instrucción: Agregar en el archivo .config en AppSettings la llave cliente " + Environment.NewLine
                + "Ejemplo:" + Environment.NewLine
                + "< add key = \"cliente\" value = \"Nombre del cliente\" />" + Environment.NewLine;
            try
            {
                cliente = "Cliente:" + ConfigurationManager.AppSettings["cliente"].ToString();
            }
            catch
            {

            }
            error = cliente + error;

            if (!Globales.automatico)
            {
                if (mostrar)
                {
                    MessageBox.Show(error, Application.ProductName + "-" + Application.ProductVersion, MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }

            if (enviarCorreo)
            {
                sendSMTPMail("Documentos FX: Versión " + Application.ProductName + "-" + Application.ProductVersion
                , error);
            }

            if (enviarCorreoCliente)
            {
                sendSMTPMailCliente(" Documentos FX: Versión " + Application.ProductName + "-" + Application.ProductVersion
                , error);
            }

            BitacoraFX.Log(error);

        }
        public static void sendSMTPMailCliente(string asunto, string mensaje)
        {
            try
            {
                
                MailMessage mm = new MailMessage();
                mm.IsBodyHtml = true;
                
                try
                {
                    mm.From = new MailAddress(cENCRIPTACION.Decrypt(ConfigurationManager.AppSettings["errorFromCliente"].ToString()), ConfigurationManager.AppSettings["errorFromNameCliente"].ToString(), Encoding.UTF8);
                }
                catch
                {
                    mm.From = new MailAddress("cfdi.manager@gmail.com", "DocumentosFX", Encoding.UTF8);

                }

                mm.Subject = asunto;
                mm.Body = mensaje;
                mm.SubjectEncoding = System.Text.Encoding.UTF8;
                mm.IsBodyHtml = true;
                mm.Priority = MailPriority.High;

                string[] rEmailCC = ConfigurationManager.AppSettings["errorCorreosCliente"].ToString().Split(';');
                for (int i = 0; i < rEmailCC.Length; i++)
                {
                    if (!String.IsNullOrEmpty(rEmailCC[i].ToString().Trim()))
                    {
                        if (Globales.isEmail(rEmailCC[i].ToString().Trim()))
                        {
                            mm.CC.Add(rEmailCC[i].ToString().Trim());
                        }
                    }
                }

                //Validar si es Nota de crédito
                if(mensaje.Contains("CFDI relacionados"))
                {
                    try
                    {
                        string[] rEmailCCNotaCredito = ConfigurationManager.AppSettings["errorCorreosClienteNotaCredito"].ToString().Split(';');
                        for (int i = 0; i < rEmailCCNotaCredito.Length; i++)
                        {
                            if (!String.IsNullOrEmpty(rEmailCCNotaCredito[i].ToString().Trim()))
                            {
                                if (Globales.isEmail(rEmailCCNotaCredito[i].ToString().Trim()))
                                {
                                    mm.CC.Add(rEmailCCNotaCredito[i].ToString().Trim());
                                }
                            }
                        }
                    }
                    catch (Exception errorNDC)
                    {

                    }
                }

                if (mensaje.Contains("Fracción Arancelaría"))
                {
                    try
                    {
                        string[] rEmailCCFraccionArancelaria = ConfigurationManager.AppSettings["errorCorreosClienteFraccionArancelaria"].ToString().Split(';');
                        for (int i = 0; i < rEmailCCFraccionArancelaria.Length; i++)
                        {
                            if (!String.IsNullOrEmpty(rEmailCCFraccionArancelaria[i].ToString().Trim()))
                            {
                                if (Globales.isEmail(rEmailCCFraccionArancelaria[i].ToString().Trim()))
                                {
                                    mm.CC.Add(rEmailCCFraccionArancelaria[i].ToString().Trim());
                                }
                            }
                        }
                    }
                    catch(Exception errorFraccion)
                    {
                        
                    }
                }

                try
                {
                    EnviarMensaje(mm);
                }
                catch (Exception error)
                {
                    string mensajetr = error.Message.ToString();
                    if (error.InnerException != null)
                    {
                        mensajetr += Environment.NewLine + error.InnerException.Message.ToString();
                    }
                    BitacoraFX.Log("Envio de correo ErrorFX: " + mensajetr);
                }
            }
            catch(Exception error)
            {
                mostrar(error, false, true, "ErrorFX.cs 139", false);
            }

        }


        public static void sendSMTPMail(string asunto, string mensaje)
        {


            MailMessage mm = new MailMessage();
            mm.IsBodyHtml = true;
            try
            {
                mm.From = new MailAddress(cENCRIPTACION.Decrypt(Properties.Settings.Default.errorFrom), Properties.Settings.Default.errorFromName, Encoding.UTF8);

            }
            catch
            {
                mm.From = new MailAddress("cfdi.manager@gmail.com ", "DocumentosFX", Encoding.UTF8);

            }


            mm.Subject = asunto;
            mm.Body = mensaje;
            mm.SubjectEncoding = System.Text.Encoding.UTF8;
            mm.IsBodyHtml = true;
            mm.Priority = MailPriority.High;

            string[] rEmailCC = Properties.Settings.Default.errorCorreos.ToString().Split(';');
            for (int i = 0; i < rEmailCC.Length; i++)
            {
                if (!String.IsNullOrEmpty(rEmailCC[i].ToString().Trim()))
                {
                    if (Globales.isEmail(rEmailCC[i].ToString().Trim()))
                    {
                        mm.CC.Add(rEmailCC[i].ToString().Trim());
                    }
                }
            }
            try
            {
                EnviarMensaje(mm);
            }
            catch (Exception error)
            {
                string mensajetr = error.Message.ToString();
                if (error.InnerException != null)
                {
                    mensajetr += Environment.NewLine + error.InnerException.Message.ToString();
                }
                BitacoraFX.Log("Envio de correo ErrorFX: " + mensajetr);
            }

        }

        private static void EnviarMensaje(MailMessage mm)
        {
            //Rodrigo Escalona: 15/03/2023 Cargar la Empresa
            cEMPRESA ocEMPRESA = new cEMPRESA();
            ocEMPRESA = ocEMPRESA.todos("").FirstOrDefault();
            string UsuarioTr = cENCRIPTACION.Decrypt(Properties.Settings.Default.errorUser);
            string PasswordTr = cENCRIPTACION.Decrypt(Properties.Settings.Default.errorPassword);
            string HostTr = Properties.Settings.Default.errorHost;
            int PortTr = 587;
            if (ocEMPRESA.ErrorCorreo != null)
            {
                if (ocEMPRESA.ErrorCorreo.Equals("True"))
                {
                    mm.From = new MailAddress(ocEMPRESA.USUARIO, "DocumentosFX", Encoding.UTF8);
                    UsuarioTr = ocEMPRESA.USUARIO;
                    PasswordTr = cENCRIPTACION.Decrypt(ocEMPRESA.PASSWORD_EMAIL);
                    HostTr = ocEMPRESA.SMTP;
                    PortTr = int.Parse(ocEMPRESA.PUERTO);
                }
            }

            var smtp = new SmtpClient
            {
                Host = HostTr,
                Port = PortTr,
                EnableSsl = true,
                DeliveryMethod = SmtpDeliveryMethod.Network,
                UseDefaultCredentials = false,
                Credentials = new System.Net.NetworkCredential(UsuarioTr, PasswordTr)
            };
            smtp.Send(mm);
        }
    }

}
