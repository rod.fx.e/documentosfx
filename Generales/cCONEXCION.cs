﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Text.RegularExpressions;
using System.Windows.Forms;

namespace Generales
{
    public class cCONEXCION
    {

        public string sConn;
        public SqlDataAdapter oDataAdapter;
        public SqlConnection oConn;
        public string no_existe_BD = "";
        public string sTipo = "SQLServer";

        public cCONEXCION()
        {
            
        }

        public cCONEXCION(string pTipo, string pServer, string pDatabase, string pUsername, string pPassword)
        {
            sTipo = pTipo;
            sConn = @"server=" + pServer + ";database=" + pDatabase + ";uid=" + pUsername + ";pwd=" + pPassword + ";";
        }
        public string convertir_fecha_general(DateTime pfecha, string como = "n")
        {
            string fecha = "";
            //SQLServer
            fecha = "CONVERT(DATETIME,'" + pfecha.Year.ToString() + "-" + pfecha.Month.ToString() + "-" + pfecha.Day.ToString() + " " + pfecha.Hour.ToString() + ":" + pfecha.Minute.ToString() + ":" + pfecha.Second.ToString() + "',102)";
            if (como == "i")
            {
                fecha = "CONVERT(DATETIME,'" + pfecha.Year.ToString() + "-" + pfecha.Month.ToString() + "-" + pfecha.Day.ToString() + " 00:00:00',102)";
            }
            if (como == "f")
            {
                fecha = "CONVERT(DATETIME,'" + pfecha.Year.ToString() + "-" + pfecha.Month.ToString() + "-" + pfecha.Day.ToString() + " 23:59:59',102)";
            }
            return fecha;
        }


        public cCONEXCION(string pConn)
        {
            this.sConn = pConn;
        }


        public bool CrearConexion()
        {
            oConn = new SqlConnection(sConn);
            try
            {
                oConn.Open();
                return true;
            }
            catch (SqlException error)
            {
                string mensaje = "";
                mensaje = error.Message.ToString();
                if (error.InnerException != null)
                {
                    mensaje += " " + error.InnerException.Message.ToString();
                }
                BitacoraFX.Log(mensaje);
                return false;
            }
        }

        public void DestruirConexion()
        {
            try
            {
                if (oConn!=null)
                {
                    oConn.Close();
                    oConn.Dispose();
                    oConn = null;
                }
                
            }
            catch (SqlException e)
            {
                return;
            }
        }

        public DataTable EjecutarConsulta(string pQuery, bool mostrar_mensaje=true)
        {
            try
            {
                DataTable oDT = new DataTable();
                //Ejecutar para SQL Server
                oDT = new DataTable();
                oConn = new SqlConnection(sConn);
                oDataAdapter = new SqlDataAdapter();
                SqlCommand oSqlCommand = new SqlCommand(pQuery, oConn);
                oSqlCommand.CommandTimeout = 10000000;
                oDataAdapter.SelectCommand = oSqlCommand;
                oDataAdapter.Fill(oDT);
                return oDT;
            }
            catch (SqlException e)
            {
                //MessageBox.Show(e.Message + "\n" + pQuery, "Exepción " + Application.ProductName + "-" + Application.ProductVersion, MessageBoxButtons.OK, MessageBoxIcon.Stop);
                ErrorFX.mostrar(e.Message + "\n" + pQuery, mostrar_mensaje, true,false);
                BitacoraFX.Log(sConn);
                return null;
            }

        }

        public string Trunca_y_formatea(decimal numero)
        {
            string resultado = "0.0000";
            decimal numero_f = Truncar_decimales(decimal.Parse(numero.ToString()));
            resultado = formato_decimal(numero_f.ToString(), "N");
            return (resultado);
        }

        public decimal Truncar_decimales(decimal numero)
        {
            int numero_truncar = 100;
            decimal parte_decimal = 0, pasar_a_decimal = 0, resultado = 0;
            Int64 parte_entera, separar_2_primeros_decimales;

            parte_entera = (Int64)numero;
            if (parte_entera != 0)
            {
                parte_decimal = numero % parte_entera;
            }
            else
            {
                parte_decimal = numero;
            }
            separar_2_primeros_decimales = (Int64)(parte_decimal * numero_truncar);
            pasar_a_decimal = (decimal)separar_2_primeros_decimales / numero_truncar;
            resultado = (decimal)parte_entera + pasar_a_decimal;

            return (resultado);
        }
        public DataTable EjecutarCommando(SqlCommand pQuery)
        {
            try
            {
                DataTable oDT = new DataTable();
                //Ejecutar para SQL Server
                oDT = new DataTable();
                oConn = new SqlConnection(sConn);
                oDataAdapter = new SqlDataAdapter();
                pQuery.Connection = oConn;
                pQuery.CommandTimeout = 1000000;
                oDataAdapter.SelectCommand = pQuery;
                oDataAdapter.Fill(oDT);
                return oDT;
            }
            catch (SqlException e)
            {
                //MessageBox.Show(e.Message + "\n" + pQuery, "Exepción " + Application.ProductName + "-" + Application.ProductVersion, MessageBoxButtons.OK, MessageBoxIcon.Stop);
                ErrorFX.mostrar(e.Message + "\n" + pQuery, true, true, false);
                return null;
            }
        }
        public string formato_decimal(string valor, string con_coma, int cantidad_decimales = 2)
        {
            string conversion;
            double Plantilla;
            if (valor.Trim() == "")
            {
                valor = "0";
            }
            Plantilla = double.Parse(valor.ToString());
            if (con_coma == "")
            {
                if (cantidad_decimales == 2)
                {
                    conversion = Plantilla.ToString("#,###,###,##0.00");
                }
                else
                {
                    conversion = Plantilla.ToString("#,###,###,##0.0000");
                }
            }
            else
            {

                if (cantidad_decimales == 2)
                {
                    conversion = Plantilla.ToString("#########0.00");
                }
                else
                {
                    conversion = Plantilla.ToString("#########0.0000");
                }
            }
            return conversion;
        }
        public bool IsBoolean(string type)
        {
            return type.Equals(typeof(Boolean));
        }
        public string IsNullNumero(object Expression)
        {
            string valor = Trunca_y_formatea(1);
            if (Expression != null)
            {
                valor = Expression.ToString();
            }
            else
            {
                if (Expression.ToString() == "")
                {
                    valor = Trunca_y_formatea(1);
                }
            }
            return valor;
        }

        public string IsNull(object Expression)
        {
            string valor = "";
            if (Expression != null)
            {
                if (Expression.ToString() == "01/01/1900 00:00:00")
                {
                    valor = "";
                }
                else
                {
                    valor = Expression.ToString();
                }
            }
            return valor;
        }


        public void ReplaceInFile(string filePath, string searchText, string replaceText)
        {
            StreamReader reader = new StreamReader(filePath);
            string content = reader.ReadToEnd();
            reader.Close();

            content = Regex.Replace(content, searchText, replaceText);

            StreamWriter writer = new StreamWriter(filePath);
            writer.Write(content);
            writer.Close();
        }
        public cCONEXCION(string sServer, string sDatabase, string sUsername, string sPassword)
        {
            sTipo = "SQLSERVER";
            sConn = @"server=" + sServer + ";database=" + sDatabase + ";uid=" + sUsername + ";pwd=" + sPassword + ";";
        }
        public string convertir_fecha(DateTime pfecha, string como = "n")
        {
            string fecha = "";
            //SQLServer
            fecha = "CONVERT(DATETIME,'" + pfecha.Year.ToString() + "-" + pfecha.Month.ToString() + "-" + pfecha.Day.ToString() + " " + pfecha.Hour.ToString() + ":" + pfecha.Minute.ToString() + ":" + pfecha.Second.ToString() + "',102)";
            if (como == "i")
            {
                fecha = "CONVERT(DATETIME,'" + pfecha.Year.ToString() + "-" + pfecha.Month.ToString() + "-" + pfecha.Day.ToString() + " 00:00:00',102)";
            }
            if (como == "f")
            {
                fecha = "CONVERT(DATETIME,'" + pfecha.Year.ToString() + "-" + pfecha.Month.ToString() + "-" + pfecha.Day.ToString() + " 23:59:59',102)";
            }
            return fecha;
        }

        public string convertir_cadena_fecha(string fecha)
        {
            try
            {
                DateTime fecha_tr = DateTime.Parse(fecha);
                return convertir_fecha(fecha_tr, "");
            }
            catch
            {
                return "NULL";
            }
        }

        public cCONEXCION PrimeraEmpresa()
        {
            //Crear conexion de empresa
            cEMPRESA OcEMPRESA = new cEMPRESA();
            List<cEMPRESA> lista = OcEMPRESA.todos(string.Empty);
            foreach (cEMPRESA Empresa in lista )
            {
                cCONEXCION OcCONEXCION= new cCONEXCION(Empresa.TIPO, Empresa.SERVIDOR
                    , Empresa.BD, Empresa.USUARIO_BD, Empresa.PASSWORD_BD);
                return OcCONEXCION;
            }
            //Cargar datos de la primera empresa
            return null;
        }
    }

}
