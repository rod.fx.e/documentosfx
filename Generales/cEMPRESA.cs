﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Windows;
using System.Windows.Forms;
using Generales;
using ModeloDocumentosFX;

namespace Generales
{
    public class cEMPRESA
    {
        private string sSQL = "";
        public string ROW_ID { get; set; }
        public string ENTITY_ID { get; set; }
        public string BD_AUXILIAR { get; set; }
        public string SITE_ID { get; set; }
        public string ID { get; set; }
        public string RFC { get; set; }
        public string CALLE { get; set; }
        public string EXTERIOR { get; set; }
        public string INTERIOR { get; set; }
        public string COLONIA { get; set; }
        public string CP { get; set; }
        public string LOCALIDAD { get; set; }
        public string REFERENCIA { get; set; }
        public string PAIS { get; set; }
        public string ESTADO { get; set; }
        public string MUNICIPIO { get; set; }
        public string SMTP { get; set; }
        public string USUARIO { get; set; }
        public string PASSWORD_EMAIL { get; set; }
        public string PUERTO { get; set; }
        public string SSL { get; set; }
        public string CRENDENCIALES { get; set; }
        public string REGIMEN_FISCAL { get; set; }

        public string TIPO { get; set; }
        public string SERVIDOR { get; set; }
        public string USUARIO_BD { get; set; }
        public string PASSWORD_BD { get; set; }
        public string BD { get; set; }

        public string ACTIVO { get; set; }

        public string CUSTOMER_ID { get; set; }
        public string ENVIO_CORREO { get; set; }

        private cCONEXCION oData = new cCONEXCION("");
        public bool IMPRESION_AUTOMATICA;
        public string IMPRESORA;
        public bool relacionLinea;

        public cEMPRESA()
        {
            ModeloDocumentosFXContainer dbContext = new ModeloDocumentosFXContainer();
            try
            {
                string conection = dbContext.Database.Connection.ConnectionString;
                oData = new cCONEXCION(conection);
            }
            catch (Exception err)
            {
                ErrorFX.mostrar(err, false, true, "cEMPRESA - 67 - ");
            }
            limpiar();
        }
        public cEMPRESA(string conection)
        {
            oData = new cCONEXCION(conection);
            limpiar();
        }
        public override string ToString()
        {
            return ENTITY_ID;
        }

        public cEMPRESA(cCONEXCION pData)
        {
            oData = pData;
            limpiar();
        }

        public void limpiar()
        {
            ROW_ID = "";
            ENTITY_ID = "";
            BD_AUXILIAR = "";
            ID = "";
            RFC = "";
            CALLE = "";
            EXTERIOR = "";
            INTERIOR = "";
            COLONIA = "";
            CP = "";
            LOCALIDAD = "";
            REFERENCIA = "";
            PAIS = "";
            ESTADO = "";
            MUNICIPIO = "";
            SMTP = "";
            USUARIO = "";
            PASSWORD_EMAIL = "";
            PUERTO = "";
            SSL = "";
            CRENDENCIALES = "";
            REGIMEN_FISCAL = "";
            ACTIVO = "";
            SITE_ID = "";
            TIPO = "";
            SERVIDOR = "";
            USUARIO_BD = "";
            PASSWORD_BD = "";
            BD = "";
            DESCUENTO_LINEA = "False";
            CUSTOMER_ID = "";
            ENVIO_CORREO = "";
            IMPRESORA = "";
            UsarCustomerPartDescripcionCliente = "";
            UsarPartNotationCliente = "";
            IMPRESION_AUTOMATICA = false;
            IMPRESION_AUTOMATICA_NDC = false;
            relacionLinea = false;
        }
        public void cargarEntity(string eNTITY_IDp)
        {
            sSQL = " SELECT * ";
            sSQL += " FROM EMPRESA ";
            sSQL += " WHERE ENTITY_ID='" + eNTITY_IDp + "' ";
            DataTable oDataTable = oData.EjecutarConsulta(sSQL);
            if (oDataTable != null)
            {
                foreach (DataRow oDataRow in oDataTable.Rows)
                {
                    this.cargar(oDataRow["ROW_ID"].ToString());
                }
            }
        }
        public List<cEMPRESA> todos(string BUSQUEDA, bool mostrar_activo = false
            , bool automatizacion = false)
        {

            List<cEMPRESA> lista = new List<cEMPRESA>();

            sSQL = " SELECT * ";
            sSQL += " FROM EMPRESA ";
            sSQL += " WHERE ID LIKE '%" + BUSQUEDA + "%' ";
            if (mostrar_activo)
            {
                sSQL += " AND ACTIVO='True' ";
            }
            if (Globales.automatico)
            {
                if (automatizacion)
                {
                    sSQL += " AND automatizacion='True' ";
                }
            }

            DataTable oDataTable = oData.EjecutarConsulta(sSQL);
            foreach (DataRow oDataRow in oDataTable.Rows)
            {
                cEMPRESA oEmpresatr = new cEMPRESA()
                {
                    ROW_ID = oDataRow["ROW_ID"].ToString()
                    ,
                    ENTITY_ID = oDataRow["ENTITY_ID"].ToString()
                    ,
                    BD_AUXILIAR = oDataRow["BD_AUXILIAR"].ToString()
                    ,
                    SITE_ID = oDataRow["SITE_ID"].ToString()
                    ,
                    ID = oDataRow["ID"].ToString()
                    ,
                    RFC = oDataRow["RFC"].ToString()
                    ,
                    CALLE = oDataRow["CALLE"].ToString()
                    ,
                    EXTERIOR = oDataRow["EXTERIOR"].ToString()
                    ,
                    INTERIOR = oDataRow["INTERIOR"].ToString()
                    ,
                    COLONIA = oDataRow["COLONIA"].ToString()
                    ,
                    CP = oDataRow["CP"].ToString()
                    ,
                    LOCALIDAD = oDataRow["LOCALIDAD"].ToString()
                    ,
                    REFERENCIA = oDataRow["REFERENCIA"].ToString()
                    ,
                    PAIS = oDataRow["PAIS"].ToString()
                    ,
                    ESTADO = oDataRow["ESTADO"].ToString()
                    ,
                    MUNICIPIO = oDataRow["MUNICIPIO"].ToString()
                    ,
                    SMTP = oDataRow["SMTP"].ToString()
                    ,
                    USUARIO = oDataRow["USUARIO"].ToString()
                    ,
                    PASSWORD_EMAIL = oDataRow["PASSWORD_EMAIL"].ToString()
                    ,
                    PUERTO = oDataRow["PUERTO"].ToString()
                    ,
                    SSL = oDataRow["SSL"].ToString()
                    ,
                    CRENDENCIALES = oDataRow["CRENDENCIALES"].ToString()
                    ,
                    DESCUENTO_LINEA = oDataRow["DESCUENTO_LINEA"].ToString()
                    ,
                    REGIMEN_FISCAL = oDataRow["REGIMEN_FISCAL"].ToString()
,
                    ACTIVO = oDataRow["ACTIVO"].ToString()
,
                    TIPO = oDataRow["TIPO"].ToString()
,
                    SERVIDOR = oDataRow["SERVIDOR"].ToString()
,
                    USUARIO_BD = oDataRow["USUARIO_BD"].ToString()
,
                    PASSWORD_BD = cENCRIPTACION.Decrypt(oDataRow["PASSWORD_BD"].ToString())
,
                    BD = oDataRow["BD"].ToString()
,
                    CUSTOMER_ID = oDataRow["CUSTOMER_ID"].ToString()
                    ,
                    ENVIO_CORREO = oDataRow["ENVIO_CORREO"].ToString(),
                    portal = oDataRow["portal"].ToString(),
                    ErrorCorreo = oDataRow["ErrorCorreo"].ToString(),
                    USAR_COMPLEMENTO_ADDR3 = oDataRow["USAR_COMPLEMENTO_ADDR3"].ToString(),
                };


                try
                {
                    oEmpresatr.IMPRESION_AUTOMATICA = bool.Parse(oDataRow["IMPRESION_AUTOMATICA"].ToString());
                }
                catch
                {

                }
                oEmpresatr.IMPRESION_AUTOMATICA_NDC = false;
                try
                {
                    oEmpresatr.IMPRESION_AUTOMATICA_NDC = bool.Parse(oDataRow["IMPRESION_AUTOMATICA_NDC"].ToString());
                }
                catch
                {

                }
                oEmpresatr.ACTIVAR_COMPLEMENTO_CCE = true;
                try
                {
                    oEmpresatr.ACTIVAR_COMPLEMENTO_CCE = bool.Parse(oDataRow["ACTIVAR_COMPLEMENTO_CCE"].ToString());
                }
                catch
                {
                }
                oEmpresatr.relacionLinea = false;
                try
                {
                    oEmpresatr.relacionLinea = bool.Parse(oDataRow["relacionLinea"].ToString());
                }
                catch
                {
                }


                oEmpresatr.automatizacion = false;
                try
                {
                    oEmpresatr.automatizacion = bool.Parse(oDataRow["automatizacion"].ToString());
                }
                catch
                {
                }

                oEmpresatr.UsarDescripcionPersonalizada = false;
                try
                {
                    oEmpresatr.UsarDescripcionPersonalizada = bool.Parse(oDataRow["UsarDescripcionPersonalizada"].ToString());
                }
                catch
                {
                }

                oEmpresatr.UsarDescripcionPersonalizadaCampo = string.Empty;
                try
                {
                    oEmpresatr.UsarDescripcionPersonalizadaCampo = oDataRow["UsarDescripcionPersonalizadaCampo"].ToString();
                }
                catch
                {
                }

                oEmpresatr.UsarCustomerPartDescripcionCliente = oDataRow["UsarCustomerPartDescripcionCliente"].ToString();
                oEmpresatr.UsarPartNotationCliente = oDataRow["UsarPartNotationCliente"].ToString();

                try
                {
                    oEmpresatr.DetenerRelacionRMA = bool.Parse(oDataRow["DetenerRelacionRMA"].ToString());
                }
                catch
                {
                }
                try
                {
                    oEmpresatr.DetenerAutomaticoRelacionado = bool.Parse(oDataRow["DetenerAutomaticoRelacionado"].ToString());
                }
                catch
                {
                }



                lista.Add(oEmpresatr);
            }

            return lista;

        }

        public bool eliminar(string ROW_IDp)
        {
            if (ROW_IDp != "")
            {
                sSQL = "DELETE FROM EMPRESA WHERE ROW_ID=" + ROW_IDp;
                oData.EjecutarConsulta(sSQL);
                return true;
            }
            return false;

        }
        public bool cargar(string ROW_IDp)
        {
            sSQL = " SELECT * ";
            sSQL += " FROM EMPRESA ";
            sSQL += " WHERE ROW_ID=" + ROW_IDp + " ";
            DataTable oDataTable = oData.EjecutarConsulta(sSQL);
            if (oDataTable != null)
            {
                foreach (DataRow oDataRow in oDataTable.Rows)
                {
                    //Añadir el Registro
                    ROW_ID = oDataRow["ROW_ID"].ToString();
                    BD_AUXILIAR = oDataRow["BD_AUXILIAR"].ToString();
                    SITE_ID = oDataRow["SITE_ID"].ToString();
                    ENTITY_ID = oDataRow["ENTITY_ID"].ToString();
                    ID = oDataRow["ID"].ToString();
                    RFC = oDataRow["RFC"].ToString();
                    CALLE = oDataRow["CALLE"].ToString();
                    EXTERIOR = oDataRow["EXTERIOR"].ToString();
                    INTERIOR = oDataRow["INTERIOR"].ToString();
                    COLONIA = oDataRow["COLONIA"].ToString();
                    CP = oDataRow["CP"].ToString();
                    LOCALIDAD = oDataRow["LOCALIDAD"].ToString();
                    REFERENCIA = oDataRow["REFERENCIA"].ToString();
                    PAIS = oDataRow["PAIS"].ToString();
                    ESTADO = oDataRow["ESTADO"].ToString();
                    MUNICIPIO = oDataRow["MUNICIPIO"].ToString();
                    DESCUENTO_LINEA = oDataRow["DESCUENTO_LINEA"].ToString();
                    if (DESCUENTO_LINEA == "")
                    {
                        DESCUENTO_LINEA = "false";
                    }
                    SMTP = oDataRow["SMTP"].ToString();
                    USUARIO = oDataRow["USUARIO"].ToString();
                    if (oDataRow["PASSWORD_EMAIL"].ToString() != "")
                    {
                        //if (oDataRow["PASSWORD_EMAIL"].ToString().Length > 3)
                        //{
                        PASSWORD_EMAIL = cENCRIPTACION.Decrypt(oDataRow["PASSWORD_EMAIL"].ToString());
                        //}
                    }
                    PUERTO = oDataRow["PUERTO"].ToString();
                    SSL = oDataRow["SSL"].ToString();
                    CRENDENCIALES = oDataRow["CRENDENCIALES"].ToString();
                    REGIMEN_FISCAL = oDataRow["REGIMEN_FISCAL"].ToString();
                    ACTIVO = oDataRow["ACTIVO"].ToString();
                    CUSTOMER_ESPECIFICACION_LINEA = oDataRow["CUSTOMER_ESPECIFICACION_LINEA"].ToString();
                    TIPO = oDataRow["TIPO"].ToString();
                    SERVIDOR = oDataRow["SERVIDOR"].ToString();
                    USUARIO_BD = oDataRow["USUARIO_BD"].ToString();
                    PASSWORD_BD = cENCRIPTACION.Decrypt(oDataRow["PASSWORD_BD"].ToString());
                    BD = oDataRow["BD"].ToString();
                    CUSTOMER_ID = oDataRow["CUSTOMER_ID"].ToString();
                    ENVIO_CORREO = oDataRow["ENVIO_CORREO"].ToString();
                    APROXIMACION = oDataRow["APROXIMACION"].ToString();
                    VALORES_DECIMALES = oDataRow["VALORES_DECIMALES"].ToString();
                    IMPRESION_AUTOMATICA = false;
                    try
                    {
                        IMPRESION_AUTOMATICA = bool.Parse(oDataRow["IMPRESION_AUTOMATICA"].ToString());
                    }
                    catch
                    {

                    }
                    IMPRESION_AUTOMATICA_NDC = false;
                    try
                    {
                        IMPRESION_AUTOMATICA_NDC = bool.Parse(oDataRow["IMPRESION_AUTOMATICA_NDC"].ToString());
                    }
                    catch
                    {

                    }
                    ACTIVAR_COMPLEMENTO_CCE = true;
                    try
                    {
                        ACTIVAR_COMPLEMENTO_CCE = bool.Parse(oDataRow["ACTIVAR_COMPLEMENTO_CCE"].ToString());
                    }
                    catch
                    {
                    }
                    relacionLinea = false;
                    try
                    {
                        relacionLinea = bool.Parse(oDataRow["relacionLinea"].ToString());
                    }
                    catch
                    {
                    }
                    IMPRESORA = oDataRow["IMPRESORA"].ToString();

                    automatizacion = false;
                    try
                    {
                        this.automatizacion = bool.Parse(oDataRow["automatizacion"].ToString());
                    }
                    catch
                    {
                    }

                    portal = oDataRow["portal"].ToString();

                    this.PRIORIDAD_PART = "False";
                    try
                    {
                        this.PRIORIDAD_PART = oDataRow["PRIORIDAD_PART"].ToString();
                    }
                    catch
                    {
                    }

                    this.PrioridadPartFiltro = string.Empty;
                    try
                    {
                        this.PrioridadPartFiltro = oDataRow["PrioridadPartFiltro"].ToString();
                    }
                    catch
                    {
                    }




                    UsarCustomerPartDescripcionCliente = oDataRow["UsarCustomerPartDescripcionCliente"].ToString();
                    UsarPartNotationCliente = oDataRow["UsarPartNotationCliente"].ToString();

                    this.USAR_COMPLEMENTO_ADDR3 = "False";
                    try
                    {
                        this.USAR_COMPLEMENTO_ADDR3 = oDataRow["USAR_COMPLEMENTO_ADDR3"].ToString();
                    }
                    catch
                    {
                    }
                    this.AGREGAR_LOTE = "False";
                    try
                    {
                        if (oData.IsBoolean(oDataRow["AGREGAR_LOTE"].ToString()))
                        {
                            this.AGREGAR_LOTE = oDataRow["AGREGAR_LOTE"].ToString();
                        }
                    }
                    catch
                    {
                    }
                    this.CUSTOM_NOMBRE_CFDI = "";
                    try
                    {
                        this.CUSTOM_NOMBRE_CFDI = oDataRow["CUSTOM_NOMBRE_CFDI"].ToString();
                    }
                    catch
                    {
                    }
                    this.RAZON_SOLO_CLIENTE = false;
                    try
                    {
                        this.RAZON_SOLO_CLIENTE = bool.Parse(oDataRow["RAZON_SOLO_CLIENTE"].ToString());
                    }
                    catch
                    {
                    }

                    this.UsarDescripcionPersonalizada = false;
                    try
                    {
                        this.UsarDescripcionPersonalizada = bool.Parse(oDataRow["UsarDescripcionPersonalizada"].ToString());
                    }
                    catch
                    {
                    }


                    DetenerRelacionRMA = false;
                    try
                    {
                        this.DetenerRelacionRMA = bool.Parse(oDataRow["DetenerRelacionRMA"].ToString());
                    }
                    catch
                    {
                    }

                    DetenerAutomaticoRelacionado = false;
                    try
                    {
                        this.DetenerAutomaticoRelacionado = bool.Parse(oDataRow["DetenerAutomaticoRelacionado"].ToString());
                    }
                    catch
                    {
                    }

                    TipoRelacionRMA = string.Empty;
                    try
                    {
                        this.TipoRelacionRMA = oDataRow["TipoRelacionRMA"].ToString();
                    }
                    catch
                    {
                    }




                    this.UsarDescripcionPersonalizadaCampo = string.Empty;
                    try
                    {
                        this.UsarDescripcionPersonalizadaCampo = oDataRow["UsarDescripcionPersonalizadaCampo"].ToString();
                    }
                    catch
                    {
                    }

                    this.UsarCustomerPartDescripcion = false;
                    try
                    {
                        this.UsarCustomerPartDescripcion = bool.Parse(oDataRow["UsarCustomerPartDescripcion"].ToString());
                    }
                    catch
                    {
                    }

                    this.UsarPartNotation = false;
                    try
                    {
                        this.UsarPartNotation = bool.Parse(oDataRow["UsarPartNotation"].ToString());
                    }
                    catch
                    {
                    }

                    this.ErrorCorreo = "false";
                    try
                    {
                        this.ErrorCorreo = oDataRow["ErrorCorreo"].ToString();
                    }
                    catch
                    {
                    }

                    return true;
                }
            }
            return false;
        }

        public bool guardar()
        {
            if (RFC.Length != 12 & RFC.Length != 13)
            {
                MessageBox.Show("El RFC es invalido.", Application.ProductName + "-" + Application.ProductVersion, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return false;
            }

            if (ENTITY_ID.Trim() == "")
            {
                MessageBox.Show("La Entidad es un campo requerido.", Application.ProductName + "-" + Application.ProductVersion, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return false;
            }

            if (ID.Trim() == "")
            {
                MessageBox.Show("El Nombre es un campo requerido.", Application.ProductName + "-" + Application.ProductVersion, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return false;
            }

            if (CALLE == "")
            {
                MessageBox.Show("La calle es un campo requerido.", Application.ProductName + "-" + Application.ProductVersion, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return false;
            }

            if (MUNICIPIO == "")
            {
                MessageBox.Show("El municipio es un campo requerido.", Application.ProductName + "-" + Application.ProductVersion, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return false;
            }

            if (ESTADO == "")
            {
                MessageBox.Show("El estado es un campo requerido.", Application.ProductName + "-" + Application.ProductVersion, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return false;
            }

            if (PAIS == "")
            {
                MessageBox.Show("El país es un campo requerido.", Application.ProductName + "-" + Application.ProductVersion, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return false;
            }

            if (CP == "")
            {
                MessageBox.Show("El código postal es un campo requerido.", Application.ProductName + "-" + Application.ProductVersion, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return false;
            }

            if (CP.Length < 5)
            {
                MessageBox.Show("El código postal debe tener al menos 5 caracteres.", Application.ProductName + "-" + Application.ProductVersion, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return false;
            }
            if (bool.Parse(ENVIO_CORREO))
            {
                if (SMTP == "")
                {
                    MessageBox.Show("El Servidor SMTP para envio de correos es un campo requerido.", Application.ProductName + "-" + Application.ProductVersion, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                    return false;
                }

                if (USUARIO == "")
                {
                    MessageBox.Show("El Usuario para envio de mail es un campo requerido.", Application.ProductName + "-" + Application.ProductVersion, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                    return false;
                }

                if (PUERTO == "")
                {
                    MessageBox.Show("El Puerto es un campo requerido.", Application.ProductName + "-" + Application.ProductVersion, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                    return false;
                }

                //if (PASSWORD_EMAIL.Trim() == "")
                //{
                //    MessageBox.Show("La contraseña para el envio de mail es un campo requerido.", Application.ProductName + "-" + Application.ProductVersion, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                //    return false;
                //}
                if (PASSWORD_EMAIL != "")
                {
                    PASSWORD_EMAIL = cENCRIPTACION.Encrypt(PASSWORD_EMAIL);
                }
            }
            PASSWORD_BD = cENCRIPTACION.Encrypt(PASSWORD_BD);

            if (REGIMEN_FISCAL.Trim() == "")
            {
                MessageBox.Show("El Regimen Fiscal para la formación de la facturación electrónica.", Application.ProductName + "-" + Application.ProductVersion, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return false;
            }

            if (ROW_ID == "")
            {

                sSQL = " INSERT INTO EMPRESA ";
                sSQL += " ( ";
                sSQL += " ENTITY_ID,BD_AUXILIAR, SITE_ID, ID, RFC ";
                sSQL += ", CALLE, EXTERIOR, INTERIOR ";
                sSQL += ", COLONIA, CP, LOCALIDAD ";
                sSQL += ", REFERENCIA, PAIS, ESTADO ";
                sSQL += ", MUNICIPIO ";
                sSQL += ", SMTP, USUARIO, PASSWORD_EMAIL";
                sSQL += ", PUERTO, SSL, CRENDENCIALES";
                sSQL += ",ACTIVO,REGIMEN_FISCAL";
                sSQL += ",TIPO,SERVIDOR";
                sSQL += ",USUARIO_BD,PASSWORD_BD";
                sSQL += ",BD,CUSTOMER_ID,ENVIO_CORREO";
                sSQL += ",DESCUENTO_LINEA,CUSTOMER_ESPECIFICACION_LINEA, PRIORIDAD_PART,PrioridadPartFiltro, USAR_COMPLEMENTO_ADDR3";
                sSQL += ",IMPRESION_AUTOMATICA, IMPRESORA,IMPRESION_AUTOMATICA_NDC,AGREGAR_LOTE";
                sSQL += ",ACTIVAR_COMPLEMENTO_CCE,RAZON_SOLO_CLIENTE";
                sSQL += ",APROXIMACION, VALORES_DECIMALES";
                sSQL += @",relacionLinea,automatizacion,portal,CUSTOM_NOMBRE_CFDI
                ,UsarDescripcionPersonalizada, UsarDescripcionPersonalizadaCampo, UsarCustomerPartDescripcion, UsarPartNotation
                ,UsarCustomerPartDescripcionCliente,UsarPartNotationCliente
                ,ActivarComplementoCartaPorte,ErrorCorreo,DetenerRelacionRMA,TipoRelacionRMA
                ,DetenerAutomaticoRelacionado
                ";
                sSQL += " )";
                sSQL += " VALUES ";
                sSQL += "(";
                sSQL += " '" + ENTITY_ID + "','" + BD_AUXILIAR + "','" + SITE_ID + "','" + ID + "','" + RFC + "'";
                sSQL += ",'" + CALLE + "','" + EXTERIOR + "','" + INTERIOR + "'";
                sSQL += ",'" + COLONIA + "','" + CP + "','" + LOCALIDAD + "'";
                sSQL += ",'" + REFERENCIA + "','" + PAIS + "','" + ESTADO + "'";
                sSQL += ",'" + MUNICIPIO + "'";
                sSQL += ",'" + SMTP + "' ,'" + USUARIO + "' ,'" + PASSWORD_EMAIL + "' ";
                sSQL += ",'" + PUERTO + "' ,'" + SSL + "' ,'" + CRENDENCIALES + "' ";
                sSQL += ",'" + ACTIVO + "','" + REGIMEN_FISCAL + "'";
                sSQL += ",'" + TIPO + "','" + SERVIDOR + "'";
                sSQL += ",'" + USUARIO_BD + "','" + PASSWORD_BD + "'";
                sSQL += ",'" + BD + "','" + CUSTOMER_ID + "','" + ENVIO_CORREO + "'";
                sSQL += ",'" + DESCUENTO_LINEA + "', '" + CUSTOMER_ESPECIFICACION_LINEA + "','" + PRIORIDAD_PART + "','" + PrioridadPartFiltro + "','" + USAR_COMPLEMENTO_ADDR3 + "'";
                sSQL += ",'" + IMPRESION_AUTOMATICA + "', '" + IMPRESORA + "','" + IMPRESION_AUTOMATICA_NDC + "','" + AGREGAR_LOTE + "'";
                sSQL += ",'" + ACTIVAR_COMPLEMENTO_CCE.ToString() + "','" + RAZON_SOLO_CLIENTE.ToString() + "'";
                sSQL += ",'" + APROXIMACION.ToString() + "', '" + VALORES_DECIMALES.ToString() + "'";
                sSQL += @",'" + relacionLinea + "','" + automatizacion.ToString() + "','" + portal + "','" + CUSTOM_NOMBRE_CFDI + @"' 
                ,'" + UsarDescripcionPersonalizada + "','" + UsarDescripcionPersonalizadaCampo + "','" + UsarCustomerPartDescripcion +
                "','" + UsarPartNotation + "','" + UsarCustomerPartDescripcionCliente + "','" + UsarPartNotationCliente + @"'
                ,'" + ActivarComplementoCartaPorte + @"','" + ErrorCorreo + @"','" + DetenerRelacionRMA + @"','" + TipoRelacionRMA + @"'
                ,'" + DetenerAutomaticoRelacionado + @"'
                ";
                sSQL += ")";

                DataTable oDataTable = oData.EjecutarConsulta(sSQL);
                if (oDataTable != null)
                {

                    sSQL = "SELECT TOP 1 MAX(ROW_ID) as ROW_ID FROM EMPRESA";
                    oDataTable = oData.EjecutarConsulta(sSQL);
                    if (oDataTable != null)
                    {
                        foreach (DataRow oDataRow in oDataTable.Rows)
                        {
                            ROW_ID = oDataRow["ROW_ID"].ToString();
                        }
                    }
                }

            }
            else
            {
                sSQL = " UPDATE EMPRESA SET ";
                sSQL += " ENTITY_ID='" + ENTITY_ID + "',BD_AUXILIAR='" + BD_AUXILIAR + "',SITE_ID='" + SITE_ID + "',ID='" + ID + "',RFC='" + RFC + "'";
                sSQL += ",CALLE='" + CALLE + "',EXTERIOR='" + EXTERIOR + "',INTERIOR='" + INTERIOR + "'";
                sSQL += ",COLONIA='" + COLONIA + "',CP='" + CP + "',LOCALIDAD='" + LOCALIDAD + "'";
                sSQL += ",REFERENCIA='" + REFERENCIA + "',PAIS='" + PAIS + "',ESTADO='" + ESTADO + "'";
                sSQL += ",MUNICIPIO='" + MUNICIPIO + "'";
                sSQL += ",SMTP='" + SMTP + "' ,USUARIO='" + USUARIO + "' ,PASSWORD_EMAIL='" + PASSWORD_EMAIL + "' ";
                sSQL += ",PUERTO='" + PUERTO + "' ,SSL='" + SSL + "' ,CRENDENCIALES='" + CRENDENCIALES + "' ";
                sSQL += ",ACTIVO='" + ACTIVO + "',REGIMEN_FISCAL='" + REGIMEN_FISCAL + "'";
                sSQL += ",TIPO='" + TIPO + "',SERVIDOR='" + SERVIDOR + "'";
                sSQL += ",USUARIO_BD='" + USUARIO_BD + "',PASSWORD_BD='" + PASSWORD_BD + "'";
                sSQL += ",BD='" + BD + "',CUSTOMER_ID='" + CUSTOMER_ID + "',ENVIO_CORREO='" + ENVIO_CORREO + "'";
                sSQL += ",DESCUENTO_LINEA='" + DESCUENTO_LINEA + "', PRIORIDAD_PART='" + PRIORIDAD_PART + "', PrioridadPartFiltro='" + PrioridadPartFiltro + "', CUSTOMER_ESPECIFICACION_LINEA='" + CUSTOMER_ESPECIFICACION_LINEA + "'";
                sSQL += ",IMPRESION_AUTOMATICA='" + IMPRESION_AUTOMATICA + "', IMPRESORA='" + IMPRESORA + "',CUSTOM_NOMBRE_CFDI='" + CUSTOM_NOMBRE_CFDI + "',IMPRESION_AUTOMATICA_NDC='" + IMPRESION_AUTOMATICA_NDC + "'";
                sSQL += ",ACTIVAR_COMPLEMENTO_CCE='" + ACTIVAR_COMPLEMENTO_CCE.ToString() + "',RAZON_SOLO_CLIENTE='" + RAZON_SOLO_CLIENTE.ToString()
                    + @"',AGREGAR_LOTE='" + AGREGAR_LOTE + "'";
                sSQL += ",APROXIMACION='" + APROXIMACION.ToString() + "', VALORES_DECIMALES=" + VALORES_DECIMALES.ToString() + ",USAR_COMPLEMENTO_ADDR3='" + USAR_COMPLEMENTO_ADDR3 + "'";
                sSQL += ",relacionLinea='" + relacionLinea + "',automatizacion='" + automatizacion.ToString() + "', portal='" + portal + @"'
                ,UsarDescripcionPersonalizada='" + UsarDescripcionPersonalizada +
                "',UsarDescripcionPersonalizadaCampo='" + UsarDescripcionPersonalizadaCampo + "'" +
                ",UsarCustomerPartDescripcion='" + UsarCustomerPartDescripcion + "'" +
                ",UsarPartNotation='" + UsarPartNotation + @"'
                ,UsarCustomerPartDescripcionCliente='" + UsarCustomerPartDescripcionCliente + @"'
                ,UsarPartNotationCliente='" + UsarPartNotationCliente + @"'
                ,ActivarComplementoCartaPorte='" + ActivarComplementoCartaPorte + @"'
                ,DetenerRelacionRMA='" + DetenerRelacionRMA + @"'
                ,TipoRelacionRMA='" + TipoRelacionRMA + @"'
                ,DetenerAutomaticoRelacionado='" + DetenerAutomaticoRelacionado + @"'
                ,ErrorCorreo='" + ErrorCorreo + @"'
                "
                ;
                sSQL += " WHERE ROW_ID=" + ROW_ID + " ";
                oData.EjecutarConsulta(sSQL);

            }
            return true;

        }

        public string DESCUENTO_LINEA { get; set; }

        public string PRIORIDAD_PART { get; set; }
        public string PrioridadPartFiltro { get; set; }
        public string USAR_COMPLEMENTO_ADDR3 { get; set; }
        public string COMPLETAR_DESCRIPCION_CFDI { get; set; }

        public string CUSTOMER_ESPECIFICACION_LINEA { get; set; }

        public bool IMPRESION_AUTOMATICA_NDC { get; set; }

        public bool ACTIVAR_COMPLEMENTO_CCE { get; set; }

        public string VALORES_DECIMALES { get; set; }

        public string APROXIMACION { get; set; }
        public bool automatizacion { get; set; }
        public string portal { get; set; }
        public string AGREGAR_LOTE { get; set; }
        public bool RAZON_SOLO_CLIENTE { get; set; }
        public string CUSTOM_NOMBRE_CFDI { get; set; }
        public bool UsarDescripcionPersonalizada { get; set; }
        public string UsarDescripcionPersonalizadaCampo { get; set; }
        public bool UsarCustomerPartDescripcion { get; set; }
        public bool UsarPartNotation { get; set; }
        public string UsarCustomerPartDescripcionCliente { get; set; }
        public string UsarPartNotationCliente { get; set; }
        public bool ActivarComplementoCartaPorte { get; set; }
        public string ErrorCorreo { get; set; }
        public bool DetenerRelacionRMA { get; set; }
        public bool DetenerAutomaticoRelacionado { get; set; }

        public string TipoRelacionRMA { get; set; }
    }

}
