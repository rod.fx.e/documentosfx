﻿using ModeloDocumentosFX;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;
using System.IO;

namespace Generales
{
    public static class Globales
    {
        public static usuarios usuario;
        public static bool automatico;
        public static CFDI_USUARIO oCFDI_USUARIO;
        public static string VMX_FE_Tabla = "VMX_FE";
        public static string VMX_FE_INVOICE_ID="";
        public static bool preguntarRelacionados=true;

        static Globales()
        {
            
            try
            {
                ModeloDocumentosFXContainer dbContext = new ModeloDocumentosFXContainer();
                //Cargar el primer registro de configuración de usuarios
                oCFDI_USUARIO =  (from r in dbContext.CFDI_USUARIO
                                  select r).FirstOrDefault();
                dbContext.Dispose();
            }
            catch (Exception err)
            {
                ErrorFX.mostrar(err, false, true, "frmPrincipalFX - 3816 - ", false);
            }
            //try
            //{
            //    VMX_FE_INVOICE_ID = ConfigurationManager.AppSettings["VMX_FE.INVOICE_ID"].ToString();
            //    string TABLA_VMX_FE = ConfigurationManager.AppSettings["VMX_FE"].ToString();
            //    if (TABLA_VMX_FE != "")
            //    {
            //        VMX_FE_Tabla = "FXINV000";
            //    }
            //}
            //catch (Exception err)
            //{
            //    ErrorFX.mostrar(err, false, true, "frmPrincipalFX - 3816 - ");
            //}
        }
        public static void cargarUsuarioSL(string ROW_ID_EMPRESA="")
        {
            try
            {
                ModeloDocumentosFXContainer dbContext = new ModeloDocumentosFXContainer();
                oCFDI_USUARIO = new CFDI_USUARIO();
                

                //Cargar el primer registro de configuración de usuarios
                if (!String.IsNullOrEmpty(ROW_ID_EMPRESA))
                {
                    oCFDI_USUARIO = (from r in dbContext.CFDI_USUARIO
                 .Where(b => b.ROW_ID_EMPRESA.ToString().Equals(ROW_ID_EMPRESA) & b.TIPO.Contains("SOLUCION"))
                                     select r).FirstOrDefault();
                }
                else
                {
                    oCFDI_USUARIO = (from r in dbContext.CFDI_USUARIO
                 .Where(b => b.TIPO.Contains("SOLUCION"))
                                     select r).FirstOrDefault();
                }

                dbContext.Dispose();
            }
            catch (Exception err)
            {
                ErrorFX.mostrar(err, false, true, "Globales - 62 - ", false);
            }
        }
        public static string actualizar_invoice_id(string sSQLp)
        {
            //Buscar Alias de VMX_FE
            try
            {
                
                if (!String.IsNullOrEmpty(VMX_FE_INVOICE_ID))
                {
                    return sSQLp.Replace("INVOICE_ID", "ID");
                }

            }
            catch
            {
            }
            return sSQLp;
        }

        public static bool IsNumeric(this string text)
        {
            double test;
            return double.TryParse(text, out test);
        }

        public static bool isEmail(string inputEmail)
        {
            if (inputEmail == null || inputEmail.Length == 0)
            {
                return false;
            }

            const string expression = @"^([a-zA-Z0-9_\-\.]+)@((\[[0-9]{1,3}" +
                                      @"\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9\-]+\" +
                                      @".)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$";

            Regex regex = new Regex(expression);
            return regex.IsMatch(inputEmail);
        }

        

        public static bool IsDirectoryWritable(string dirPath, bool throwIfFails = false)
        {
            try
            {
                using (FileStream fs = File.Create(
                    Path.Combine(
                        dirPath, 
                        Path.GetRandomFileName()
                    ), 
                    1,
                    FileOptions.DeleteOnClose)
                )
                { }
                return true;
            }
            catch
            {
                if (throwIfFails)
                    throw;
                else
                    return false;
            }
        }


        public static string formato_double(string valor, string con_coma)
        {
            string conversion;
            double Plantilla;
            if (valor.Trim() == "")
            {
                valor = "0";
            }
            Plantilla = double.Parse(valor.ToString());
            if (con_coma == "")
            {
                conversion = Plantilla.ToString("#,###,###,##0.00");
            }
            else
            {
                conversion = Plantilla.ToString("#########0.00");
            }
            return conversion;
        }
        public static bool IsBool(string Expression)
        {
            try
            {

                bool resultado = bool.Parse(Expression);
                return true;
            }
            catch
            {
                return false;
            }
        }
        public static bool IsDate(string inputDate)
        {
            bool isDate = true;
            try
            {
                DateTime dt = DateTime.Parse(inputDate);
            }
            catch
            {
                isDate = false;
            }
            return isDate;
        }

        public static bool IsNumeric(object Expression)
        {
            bool isNum;
            double retNum;

            isNum = Double.TryParse(Convert.ToString(Expression), System.Globalization.NumberStyles.Any, System.Globalization.NumberFormatInfo.InvariantInfo, out retNum);
            return isNum;
        }

        public static bool cargarTimbrado(string proveedor, string ROW_ID_EMPRESA="")
        {
            try
            {
                if (proveedor.Equals("STO"))
                {
                    try
                    {
                        if (!bool.Parse(ConfigurationManager.AppSettings["STO"].ToString()))
                        {
                            return false;
                        }
                    }
                    catch (Exception err)
                    {
                        return false;
                    }
                }

                ModeloDocumentosFXContainer dbContext = new ModeloDocumentosFXContainer();
                //Cargar el primer registro de configuración de usuariosr
                if (!String.IsNullOrEmpty(ROW_ID_EMPRESA))
                {
                    oCFDI_USUARIO = (from r in dbContext.CFDI_USUARIO
                                 .Where(b => b.TIPO.Contains(proveedor) & b.ROW_ID_EMPRESA.ToString().Equals(ROW_ID_EMPRESA))
                                     select r).FirstOrDefault();
                    if (oCFDI_USUARIO != null)
                    {
                        return true;
                    }
                }
                
                //Cargar sin el ROW_ID_EMPRESA
                oCFDI_USUARIO = (from r in dbContext.CFDI_USUARIO
                                 .Where(b => b.TIPO.Contains(proveedor))
                                 select r).FirstOrDefault();
                if (oCFDI_USUARIO != null)
                {
                    return true;
                }
                return false;
            }
            catch (Exception err)
            {
                ErrorFX.mostrar(err, false, true, "Globales.cs - 157 - ",false);
            }
            return false;
        }

        public static bool GuardarFXEstadisticaUso(string usuario = null)
        {

            return true;
        }

    }

}
