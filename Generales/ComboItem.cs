﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Generales
{
    public class ComboItem
    {
        public string Name { get; set; }
        public string Value { get; set; }
        public override string ToString() {
            if(String.IsNullOrEmpty(Name) & String.IsNullOrEmpty(Value))
            {
                return "";
            }
            else
            {
                return this.Value + "-" + this.Name;
            }

             }
    }
}
