#region Copyright Syncfusion Inc. 2001 - 2017
// Copyright Syncfusion Inc. 2001 - 2017. All rights reserved.
// Use of this code is subject to the terms of our license.
// A copy of the current license can be obtained at any time by e-mailing
// licensing@syncfusion.com. Any infringement will be prosecuted under
// applicable laws. 
#endregion
using Generales;
using ModeloDocumentosFX;
using OfficeOpenXml;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace auxiliarContable
{
    public partial class frmPrincipal : Syncfusion.Windows.Forms.MetroForm
    {
        private cCONEXCION oData;
        string ENTITY_ID_tr = "";
        public frmPrincipal()
        {
            ModeloDocumentosFXContainer dbContext = new ModeloDocumentosFXContainer();
            try
            {
                string conection = dbContext.Database.Connection.ConnectionString;
                oData = new cCONEXCION(conection);
            }
            catch (Exception err)
            {
                ErrorFX.mostrar(err, false, true, "cCERTIFICADO - 34 - ");
            }
            InitializeComponent();

            ENTITY_ID_tr = "ENTITY_ID";

            if (verificar_version() == "7.1.2" || verificar_version() == "8.0.0" || verificar_version().Contains("9"))
            {
                ENTITY_ID_tr = "SITE_ID";
            }

        }

        private void button2_Click(object sender, EventArgs e)
        {
            generar();
        }

        private void generar_columnas()
        {

            dtgrdGeneral.Columns.Clear();
            int i = 0;
            //Agregar las Columnas
            i = dtgrdGeneral.Columns.Add("Cuenta", "Cuenta");
            dtgrdGeneral.Columns[i].ReadOnly = true;
            dtgrdGeneral.Columns[i].SortMode = DataGridViewColumnSortMode.NotSortable;

            //Agregar las Columnas
            i = dtgrdGeneral.Columns.Add("Descripcion", "Descripción");
            dtgrdGeneral.Columns[i].ReadOnly = true;
            dtgrdGeneral.Columns[i].SortMode = DataGridViewColumnSortMode.NotSortable;
            dtgrdGeneral.Columns[i].Width = 200;

            //Agregar las Columnas
            i = dtgrdGeneral.Columns.Add("Moneda", "Moneda");
            dtgrdGeneral.Columns[i].ReadOnly = true;
            dtgrdGeneral.Columns[i].Width = 30;
            dtgrdGeneral.Columns[i].SortMode = DataGridViewColumnSortMode.NotSortable;
            dtgrdGeneral.Columns[i].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;

            //Agregar las Columnas
            i = dtgrdGeneral.Columns.Add("Tipo", "Tipo");
            dtgrdGeneral.Columns[i].ReadOnly = true;
            dtgrdGeneral.Columns[i].Width = 30;
            dtgrdGeneral.Columns[i].SortMode = DataGridViewColumnSortMode.NotSortable;
            dtgrdGeneral.Columns[i].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;

            //Agregar las Columnas
            i = dtgrdGeneral.Columns.Add("Lote", "Lote");
            dtgrdGeneral.Columns[i].ReadOnly = true;
            dtgrdGeneral.Columns[i].Width = 60;
            dtgrdGeneral.Columns[i].SortMode = DataGridViewColumnSortMode.NotSortable;
            dtgrdGeneral.Columns[i].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;

            //Agregar las Columnas
            i = dtgrdGeneral.Columns.Add("Saldo Inicial", "Saldo Inicial");
            dtgrdGeneral.Columns[i].ReadOnly = true;
            dtgrdGeneral.Columns[i].SortMode = DataGridViewColumnSortMode.NotSortable;
            dtgrdGeneral.Columns[i].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;

            //Agregar las Columnas
            i = dtgrdGeneral.Columns.Add("Cargos", "Cargos");
            dtgrdGeneral.Columns[i].ReadOnly = true;
            dtgrdGeneral.Columns[i].SortMode = DataGridViewColumnSortMode.NotSortable;
            dtgrdGeneral.Columns[i].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;

            //Agregar las Columnas
            i = dtgrdGeneral.Columns.Add("Abonos", "Abonos");
            dtgrdGeneral.Columns[i].ReadOnly = true;
            dtgrdGeneral.Columns[i].SortMode = DataGridViewColumnSortMode.NotSortable;
            dtgrdGeneral.Columns[i].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;

            //Agregar las Columnas
            i = dtgrdGeneral.Columns.Add("Saldo Final", "Saldo Final");
            dtgrdGeneral.Columns[i].ReadOnly = true;
            dtgrdGeneral.Columns[i].SortMode = DataGridViewColumnSortMode.NotSortable;
            dtgrdGeneral.Columns[i].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;

            //Agregar las Columnas
            i = dtgrdGeneral.Columns.Add("Fecha", "Fecha");
            dtgrdGeneral.Columns[i].ReadOnly = true;
            dtgrdGeneral.Columns[i].SortMode = DataGridViewColumnSortMode.NotSortable;
            dtgrdGeneral.Columns[i].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;

            //Agregar las Columnas
            i = dtgrdGeneral.Columns.Add("Referencia", "Referencia");
            dtgrdGeneral.Columns[i].ReadOnly = true;
            dtgrdGeneral.Columns[i].SortMode = DataGridViewColumnSortMode.NotSortable;

            //Agregar las Columnas
            i = dtgrdGeneral.Columns.Add("User Lote", "User Lote");
            dtgrdGeneral.Columns[i].ReadOnly = true;
            dtgrdGeneral.Columns[i].SortMode = DataGridViewColumnSortMode.NotSortable;

            //-------------------------------------------------------

            //Agregar las Columnas
            i = dtgrdGeneral.Columns.Add("Cliente", "Cliente");
            dtgrdGeneral.Columns[i].ReadOnly = true;
            dtgrdGeneral.Columns[i].SortMode = DataGridViewColumnSortMode.NotSortable;

            //Agregar las Columnas
            i = dtgrdGeneral.Columns.Add("CuentaNueva", "Cuenta");
            dtgrdGeneral.Columns[i].ReadOnly = true;
            dtgrdGeneral.Columns[i].SortMode = DataGridViewColumnSortMode.NotSortable;

            //Agregar las Columnas
            i = dtgrdGeneral.Columns.Add("ID", "ID");
            dtgrdGeneral.Columns[i].ReadOnly = true;
            dtgrdGeneral.Columns[i].SortMode = DataGridViewColumnSortMode.NotSortable;

            //Agregar las Columnas
            i = dtgrdGeneral.Columns.Add("Lineas", "Lineas");
            dtgrdGeneral.Columns[i].ReadOnly = true;
            dtgrdGeneral.Columns[i].SortMode = DataGridViewColumnSortMode.NotSortable;




            //Agregar las Columnas
            i = dtgrdGeneral.Columns.Add("", "");
            dtgrdGeneral.Columns[i].Visible = false;

            toolStripStatusLabel1.Text = "Columnas Generadas " + DateTime.Now.ToShortDateString() + " " + DateTime.Now.ToLongTimeString();
        }


        private void generar()
        {
            double saldo_inicial = 0;
            double saldo_final = 0;
            int n = 0;
            string fecha_inicial = "";
            string fecha_final = "";
            string sSQL = "";
            //Eliminar todos los registros
            sSQL = " DELETE FROM [VMX_AUXILIAR_CONTABLE] ";
            sSQL += " WHERE USUARIO='" + Globales.usuario.usuario + "' ";
            oData.EjecutarConsulta(sSQL);

            dtgrdGeneral.Rows.Clear();

            generar_columnas();

            string sSQL_Where = "";

            if (ACCOUNT_ID_1.Text != "")
            {
                sSQL_Where += " AND ACCOUNT.ID>='" + ACCOUNT_ID_1.Text + "' ";
            }
            if (ACCOUNT_ID_2.Text != "")
            {
                sSQL_Where += " AND ACCOUNT.ID<='" + ACCOUNT_ID_2.Text + "' ";
            }

            //Agregar Filas
            sSQL = " SELECT ACCOUNT.ID as ID, ACCOUNT.DESCRIPTION as DESCRIPTION, ACCOUNT.TYPE as TIPO ";
            sSQL += " FROM ACCOUNT ";
            //sSQL += " WHERE CURRENCY_ID = '" + MONEDA.Text + "' AND ACTIVE_FLAG='Y' AND SUMMARY_ACCOUNT='N'";
            sSQL += " WHERE SUMMARY_ACCOUNT='N'";
            sSQL += sSQL_Where;

            DataTable oDataTable = oData.EjecutarConsulta(sSQL);

            toolStripProgressBar1.Value = 0;
            toolStripProgressBar1.Minimum = 0;
            toolStripProgressBar1.Maximum = oDataTable.Rows.Count + 1;

            foreach (DataRow oDataRow in oDataTable.Rows)
            {

                System.Windows.Forms.Application.DoEvents();
                string ACCOUNT_ID = oDataRow["ID"].ToString();
                string DESCRIPCION = oDataRow["DESCRIPTION"].ToString();
                string TIPO = oDataRow["TIPO"].ToString();

                toolStripStatusLabel1.Text = "Procesando Cuenta: " + ACCOUNT_ID;
                saldo_inicial = 0;
                saldo_final = 0;
                //Calculo de Saldo Inicial y Saldo Final
                //Saldo Inicial
                int anio = FECHA_INICIO.Value.Year;
                int mes = FECHA_INICIO.Value.Month;
                fecha_inicial = "";
                DateTime primerdia = new DateTime(anio, mes, 1);
                fecha_inicial = primerdia.ToShortDateString();
                //  fecha_inicial = oData.convertir_fecha_DBMS(primerdia);}
                fecha_inicial = oData.convertir_fecha(primerdia);

                //Saldo Final
                int anio_final = FECHA_FINAL.Value.Year;
                int mes_final = FECHA_FINAL.Value.Month;
                fecha_final = "";
                DateTime ultimodia = new DateTime(anio_final, mes_final, DateTime.DaysInMonth(anio_final, mes_final));
                fecha_final = ultimodia.ToShortDateString();
                //  fecha_final = oData.convertir_fecha_DBMS(ultimodia);
                fecha_final = oData.convertir_fecha(ultimodia);

                //Saldo Inicial

                saldo_inicial = saldo(fecha_inicial, TIPO, ACCOUNT_ID, MONEDA.Text, ENTIDAD.Text);

                double debe = 0;
                double haber = 0;
                sSQL = " SELECT ISNULL(ACCOUNT_BALANCE.DEBIT_AMOUNT,0) as DEBIT_AMOUNT";
                sSQL += " ,ISNULL(ACCOUNT_BALANCE.CREDIT_AMOUNT,0) as CREDIT_AMOUNT ";
                sSQL += " From ACCOUNT_BALANCE, ACCOUNT_PERIOD, ACCOUNT ";
                sSQL += " Where ACCOUNT.ID=ACCOUNT_BALANCE.ACCOUNT_ID AND ACCOUNT_BALANCE.ACCT_YEAR = ACCOUNT_PERIOD.ACCT_YEAR And ACCOUNT_BALANCE.ACCT_PERIOD = ACCOUNT_PERIOD.ACCT_PERIOD ";
                //12072017  sSQL += " AND ACCOUNT_BALANCE.CURRENCY_ID = '" + MONEDA.Text + "' AND ACCOUNT_BALANCE.ACCOUNT_ID = '" + ACCOUNT_ID + "' ";
                sSQL += " AND ACCOUNT_BALANCE.CURRENCY_ID = '" + MONEDA.Text + "' AND ACCOUNT_BALANCE.ACCOUNT_ID = '" + ACCOUNT_ID + "' ";
                if (verificar_version() == "7.1.2" || verificar_version() == "8.0.0" || verificar_version().Contains("9"))
                {
                    sSQL += " AND ACCOUNT_BALANCE.SITE_ID = '" + ENTIDAD.Text + "' ";
                }
                else
                {
                    sSQL += " AND ACCOUNT_BALANCE.ENTITY_ID = '" + ENTIDAD.Text + "' ";
                }

                sSQL += " AND ACCOUNT_PERIOD.BEGIN_DATE>= " + fecha_inicial + " ";
                sSQL += " AND ACCOUNT_PERIOD.END_DATE<= " + fecha_final + " ";
                sSQL += " ORDER BY ACCOUNT_PERIOD.END_DATE";


                DataTable oDataTable1 = oData.EjecutarConsulta(sSQL);
                foreach (DataRow oDataRow1 in oDataTable1.Rows)
                {
                    debe += double.Parse(oDataRow1["DEBIT_AMOUNT"].ToString());
                    haber += double.Parse(oDataRow1["CREDIT_AMOUNT"].ToString());
                }
                saldo_final = (debe - haber) + saldo_inicial;


                generar_all(ACCOUNT_ID, DESCRIPCION, TIPO, saldo_inicial, saldo_final, MONEDA.Text);


                //Guardar el Total
                guardar(Globales.usuario.usuario, ACCOUNT_ID, DESCRIPCION, double.Parse(Math.Round(saldo_inicial, 2).ToString()), 0, 0, double.Parse(Math.Round(saldo_final, 2).ToString()), "Linea Final", "", "", "", TIPO);

                n = dtgrdGeneral.Rows.Add();
                for (int a = 0; a < dtgrdGeneral.Columns.Count; a++)
                {
                    dtgrdGeneral.Rows[n].Cells[a].Value = "";
                }

                System.Windows.Forms.Application.DoEvents();
                dtgrdGeneral.Rows[n].Cells["Cuenta"].Value = ACCOUNT_ID;
                dtgrdGeneral.Rows[n].Cells["Descripcion"].Value = DESCRIPCION.Replace("'", "");

                dtgrdGeneral.Rows[n].Cells["Moneda"].Value = MONEDA.Text;
                dtgrdGeneral.Rows[n].Cells["Tipo"].Value = "";
                dtgrdGeneral.Rows[n].Cells["Lote"].Value = "";

                dtgrdGeneral.Rows[n].Cells["Saldo Inicial"].Value = formato_decimal(Math.Round(saldo_inicial, 2).ToString());

                dtgrdGeneral.Rows[n].Cells["Cargos"].Value = formato_decimal(Math.Round(debe, 2).ToString());

                dtgrdGeneral.Rows[n].Cells["Abonos"].Value = formato_decimal(Math.Round(haber, 2).ToString());

                dtgrdGeneral.Rows[n].Cells["Saldo Final"].Value = formato_decimal(Math.Round(saldo_final, 2).ToString());

                dtgrdGeneral.Rows[n].Cells["Fecha"].Value = "";

                dtgrdGeneral.Rows[n].Cells["Referencia"].Value = "Final de Cuenta " + ACCOUNT_ID;
                dtgrdGeneral.Rows[n].DefaultCellStyle.BackColor = Color.LightGray;
                dtgrdGeneral.Rows[n].DefaultCellStyle.Font = new Font(dtgrdGeneral.DefaultCellStyle.Font.FontFamily, dtgrdGeneral.DefaultCellStyle.Font.Size, FontStyle.Bold);

                //NOTA REALIZAR FUNCION PARA INGRESAR 
                //DATOS GENERALES
                toolStripProgressBar1.Value += 1;
            }
            toolStripStatusLabel1.Text = "Generado " + DateTime.Now.ToShortDateString() + " " + DateTime.Now.ToLongTimeString();

        }

        private double saldo(string fecha, string TIPO, string Cuenta, string SYSTEM_CURRENCY_ID, string MFG_ENTITY_ID, string ENTITY_ID_tr)
        {
            double calculo = 0;
            double CREDIT_AMOUNT = 0;
            double DEBIT_AMOUNT = 0;
            double CURR_BALANCE = 0;

            string sSQL = "";

            if (TIPO == "E" || TIPO == "R")
            {
                sSQL = " SELECT ACCOUNT_BALANCE.DEBIT_AMOUNT,ACCOUNT_BALANCE.CREDIT_AMOUNT ";
                sSQL += " FROM ACCOUNT_BALANCE INNER JOIN ACCOUNT_PERIOD ON ACCOUNT_BALANCE.ACCT_YEAR = ACCOUNT_PERIOD.ACCT_YEAR AND ACCOUNT_BALANCE.ACCT_PERIOD = ACCOUNT_PERIOD.ACCT_PERIOD  ";
                sSQL += " WHERE ACCOUNT_BALANCE." + ENTITY_ID_tr + " = '" + MFG_ENTITY_ID + "'";
                sSQL += " AND ACCOUNT_BALANCE.CURRENCY_ID = '" + SYSTEM_CURRENCY_ID + "'";
                sSQL += " AND ACCOUNT_BALANCE.ACCOUNT_ID='" + Cuenta + "'";
                sSQL += " AND ACCOUNT_PERIOD.END_DATE<" + fecha + "";
                sSQL += " AND DATEPART(yyyy, ACCOUNT_PERIOD.END_DATE)=DATEPART(yyyy, " + fecha + ")";
                sSQL += " ORDER BY ACCOUNT_BALANCE.ACCOUNT_ID DESC, ACCOUNT_PERIOD.BEGIN_DATE DESC, ACCOUNT_PERIOD.END_DATE DESC ";
            }
            else
            {
                sSQL = " SELECT ACCOUNT_BALANCE.DEBIT_AMOUNT,ACCOUNT_BALANCE.CREDIT_AMOUNT ";
                sSQL += " FROM ACCOUNT_BALANCE INNER JOIN ACCOUNT_PERIOD ON ACCOUNT_BALANCE.ACCT_YEAR = ACCOUNT_PERIOD.ACCT_YEAR AND ACCOUNT_BALANCE.ACCT_PERIOD = ACCOUNT_PERIOD.ACCT_PERIOD ";
                sSQL += " WHERE ACCOUNT_BALANCE." + ENTITY_ID_tr + " = '" + MFG_ENTITY_ID + "'";
                sSQL += " AND ACCOUNT_BALANCE.CURRENCY_ID = '" + SYSTEM_CURRENCY_ID + "'";
                sSQL += " AND ACCOUNT_BALANCE.ACCOUNT_ID='" + Cuenta + "'";
                sSQL += " AND ACCOUNT_PERIOD.END_DATE<" + fecha + " ";
                sSQL += " ORDER BY ACCOUNT_BALANCE.ACCOUNT_ID DESC, ACCOUNT_PERIOD.BEGIN_DATE DESC, ACCOUNT_PERIOD.END_DATE DESC ";

            }

            DataTable oDataTable1 = oData.EjecutarConsulta(sSQL);
            foreach (DataRow oDataRow1 in oDataTable1.Rows)
            {
                DEBIT_AMOUNT = double.Parse(oDataRow1["DEBIT_AMOUNT"].ToString());
                CREDIT_AMOUNT = double.Parse(oDataRow1["CREDIT_AMOUNT"].ToString());
                CURR_BALANCE = (DEBIT_AMOUNT + CURR_BALANCE) - CREDIT_AMOUNT;
            }

            return CURR_BALANCE;

        }
        private string verificar_version()
        {
            string sSQL = "SELECT DBVERSION FROM APPLICATION_GLOBAL";
            DataTable oDataTable = oData.EjecutarConsulta(sSQL);
            if (oDataTable != null)
            {
                foreach (DataRow registro in oDataTable.Rows)
                {
                    return registro["DBVERSION"].ToString();
                }
            }
            return "";
        }

        private string condiciones(string ACCOUNT_ID)
        {
            // string sSQL_Where = " WHERE (NOT (BATCH_ID IS NULL)) AND CURRENCY_ID='" + MONEDA.Text + "' ";
            //12072017  string sSQL_Where = " AND  (NOT (BATCH_ID IS NULL)) AND CURRENCY_ID='" + MONEDA.Text + "' ";
            string sSQL_Where = " AND  (NOT (BATCH_ID IS NULL))  ";

            if (verificar_version() == "7.1.2" || verificar_version() == "8.0.0" || verificar_version().Contains("9"))
            {
                sSQL_Where += " AND SITE_ID = '" + ENTIDAD.Text + "' ";
            }
            else
            {
                sSQL_Where += " AND ENTITY_ID = '" + ENTIDAD.Text + "' ";
            }
            //AND ENTITY_ID='" + ENTIDAD.Text + "' ";


            sSQL_Where += " AND GL_ACCOUNT_ID='" + ACCOUNT_ID + "'";


            string fecha_inicial = "";
            string fecha_final = "";

            //Saldo Inicial
            int anio = FECHA_INICIO.Value.Year;
            int mes = FECHA_INICIO.Value.Month;
            fecha_inicial = "";
            DateTime primerdia = new DateTime(anio, mes, 1);
            fecha_inicial = primerdia.ToShortDateString();
            fecha_inicial = oData.convertir_fecha(primerdia);

            //Saldo Final
            int anio_final = FECHA_FINAL.Value.Year;
            int mes_final = FECHA_FINAL.Value.Month;
            fecha_final = "";
            DateTime ultimodia = new DateTime(anio_final, mes_final, DateTime.DaysInMonth(anio_final, mes_final));
            fecha_final = ultimodia.ToShortDateString();
            fecha_final = oData.convertir_fecha(ultimodia);

            sSQL_Where += " AND POSTING_DATE>= " + fecha_inicial + " ";
            sSQL_Where += " AND POSTING_DATE<= " + fecha_final + " ";

            return sSQL_Where;
        }

        private double saldo(string fecha, string TIPO, string Cuenta, string SYSTEM_CURRENCY_ID, string MFG_ENTITY_ID)
        {
            double calculo = 0;
            double CREDIT_AMOUNT = 0;
            double DEBIT_AMOUNT = 0;
            double CURR_BALANCE = 0;

            string sSQL = "";

            if (TIPO == "E" || TIPO == "R")
            {
                sSQL = " SELECT ACCOUNT_BALANCE.DEBIT_AMOUNT,ACCOUNT_BALANCE.CREDIT_AMOUNT ";
                sSQL += " FROM ACCOUNT_BALANCE INNER JOIN ACCOUNT_PERIOD ON ACCOUNT_BALANCE.ACCT_YEAR = ACCOUNT_PERIOD.ACCT_YEAR AND ACCOUNT_BALANCE.ACCT_PERIOD = ACCOUNT_PERIOD.ACCT_PERIOD  ";
                sSQL += " WHERE ACCOUNT_BALANCE." + ENTITY_ID_tr + " = '" + MFG_ENTITY_ID + "'";
                sSQL += " AND ACCOUNT_BALANCE.CURRENCY_ID = '" + SYSTEM_CURRENCY_ID + "'";
                sSQL += " AND ACCOUNT_BALANCE.ACCOUNT_ID='" + Cuenta + "'";
                sSQL += " AND ACCOUNT_PERIOD.END_DATE<" + fecha + "";
                sSQL += " AND DATEPART(yyyy, ACCOUNT_PERIOD.END_DATE)=DATEPART(yyyy, " + fecha + ")";
                sSQL += " ORDER BY ACCOUNT_BALANCE.ACCOUNT_ID DESC, ACCOUNT_PERIOD.BEGIN_DATE DESC, ACCOUNT_PERIOD.END_DATE DESC ";
            }
            else
            {
                sSQL = " SELECT ACCOUNT_BALANCE.DEBIT_AMOUNT,ACCOUNT_BALANCE.CREDIT_AMOUNT ";
                sSQL += " FROM ACCOUNT_BALANCE INNER JOIN ACCOUNT_PERIOD ON ACCOUNT_BALANCE.ACCT_YEAR = ACCOUNT_PERIOD.ACCT_YEAR AND ACCOUNT_BALANCE.ACCT_PERIOD = ACCOUNT_PERIOD.ACCT_PERIOD ";
                sSQL += " WHERE ACCOUNT_BALANCE." + ENTITY_ID_tr + " = '" + MFG_ENTITY_ID + "'";
                sSQL += " AND ACCOUNT_BALANCE.CURRENCY_ID = '" + SYSTEM_CURRENCY_ID + "'";
                sSQL += " AND ACCOUNT_BALANCE.ACCOUNT_ID='" + Cuenta + "'";
                sSQL += " AND ACCOUNT_PERIOD.END_DATE<" + fecha + " ";
                sSQL += " ORDER BY ACCOUNT_BALANCE.ACCOUNT_ID DESC, ACCOUNT_PERIOD.BEGIN_DATE DESC, ACCOUNT_PERIOD.END_DATE DESC ";

            }

            DataTable oDataTable1 = oData.EjecutarConsulta(sSQL);
            foreach (DataRow oDataRow1 in oDataTable1.Rows)
            {
                DEBIT_AMOUNT = double.Parse(oDataRow1["DEBIT_AMOUNT"].ToString());
                CREDIT_AMOUNT = double.Parse(oDataRow1["CREDIT_AMOUNT"].ToString());
                CURR_BALANCE = (DEBIT_AMOUNT + CURR_BALANCE) - CREDIT_AMOUNT;
            }

            return CURR_BALANCE;

        }

        private string condiciones(string ACCOUNT_ID, string ALIAS)
        {
            string sSQL_Where = " WHERE (NOT (" + ALIAS + ".BATCH_ID IS NULL)) AND " + ALIAS + ".CURRENCY_ID='" + MONEDA.Text + "' ";


            if (verificar_version() == "7.1.2" || verificar_version() == "8.0.0" || verificar_version().Contains("9"))
            {
                sSQL_Where += " AND " + ALIAS + ".SITE_ID='" + ENTIDAD.Text + "' ";
            }
            else
            {
                sSQL_Where += " AND " + ALIAS + ".ENTITY_ID='" + ENTIDAD.Text + "' ";
            }


            sSQL_Where += " AND " + ALIAS + ".GL_ACCOUNT_ID='" + ACCOUNT_ID + "'";


            string fecha_inicial = "";
            string fecha_final = "";


            //Saldo Inicial
            int anio = FECHA_INICIO.Value.Year;
            int mes = FECHA_INICIO.Value.Month;
            fecha_inicial = "";
            DateTime primerdia = new DateTime(anio, mes, 1);
            fecha_inicial = primerdia.ToShortDateString();
            // fecha_inicial = oData.convertir_fecha_DBMS(primerdia);
            fecha_inicial = oData.convertir_fecha(primerdia);

            //Saldo Final
            int anio_final = FECHA_FINAL.Value.Year;
            int mes_final = FECHA_FINAL.Value.Month;
            fecha_final = "";
            DateTime ultimodia = new DateTime(anio_final, mes_final, DateTime.DaysInMonth(anio_final, mes_final));
            fecha_final = ultimodia.ToShortDateString();
            //  fecha_final = oData.convertir_fecha_DBMS(ultimodia);

            fecha_final = oData.convertir_fecha(ultimodia);


            sSQL_Where += " AND " + ALIAS + ".POSTING_DATE>= " + fecha_inicial + " ";
            sSQL_Where += " AND " + ALIAS + ".POSTING_DATE<= " + fecha_final + " ";

            return sSQL_Where;
        }

        public string formato_decimal(string valor)
        {
            string conversion;
            double Plantilla;
            Plantilla = double.Parse(valor.ToString());
            conversion = Plantilla.ToString("#,###,##0.00;");
            return conversion;
        }

        private void generar_all(string ACCOUNT_ID, string DESCRIPTION, string TIPO, double saldo_inicial, double saldo_final, string CURRENCY_ID)
        {
            string sSQL = @" SELECT *
            FROM [VTR_AUX_CONT] 
            WHERE GL_ACCOUNT_ID='" + ACCOUNT_ID + @"' AND CURRENCY_ID='" + CURRENCY_ID + @"'
            " + condiciones(ACCOUNT_ID) + @"
            ORDER BY GL_ACCOUNT_ID, POSTING_DATE
            ";
            DataTable oDataTable = oData.EjecutarConsulta(sSQL);

            toolStripStatusLabel1.Text = ACCOUNT_ID;
            toolStripProgressBar1.Value = 0;
            toolStripProgressBar1.Minimum = 0;
            toolStripProgressBar1.Maximum = oDataTable.Rows.Count + 1;
            int n;
            foreach (DataRow oDataRow in oDataTable.Rows)
            {

                System.Windows.Forms.Application.DoEvents();
                n = dtgrdGeneral.Rows.Add();
                for (int a = 0; a < dtgrdGeneral.Columns.Count; a++)
                {
                    dtgrdGeneral.Rows[n].Cells[a].Value = "";
                }

                dtgrdGeneral.Rows[n].Cells["Cuenta"].Value = ACCOUNT_ID;
                dtgrdGeneral.Rows[n].Cells["Descripcion"].Value = DESCRIPTION.Replace("'", "");
                dtgrdGeneral.Rows[n].Cells["Moneda"].Value = MONEDA.Text;
                dtgrdGeneral.Rows[n].Cells["Tipo"].Value = oDataRow["TYPE"].ToString();
                dtgrdGeneral.Rows[n].Cells["Lote"].Value = oDataRow["BATCH_ID"].ToString();


                dtgrdGeneral.Rows[n].Cells["Saldo Inicial"].Value = formato_decimal(Math.Round(saldo_inicial, 2).ToString());

                dtgrdGeneral.Rows[n].Cells["Cargos"].Value = formato_decimal(Math.Round(double.Parse(oDataRow["DR"].ToString()), 2).ToString());

                dtgrdGeneral.Rows[n].Cells["Abonos"].Value = formato_decimal(Math.Round(double.Parse(oDataRow["CR"].ToString()), 2).ToString());

                saldo_final = (double.Parse(oDataRow["DR"].ToString()) - double.Parse(oDataRow["CR"].ToString())) + saldo_inicial;
                

                dtgrdGeneral.Rows[n].Cells["Saldo Final"].Value = formato_decimal(Math.Round(saldo_final, 2).ToString());

                dtgrdGeneral.Rows[n].Cells["Fecha"].Value = DateTime.Parse(oDataRow["POSTING_DATE"].ToString()).ToShortDateString();

                dtgrdGeneral.Rows[n].Cells["Referencia"].Value = oDataRow["REFERENCIA"].ToString().Replace("'", "");

                dtgrdGeneral.Rows[n].Cells["User Lote"].Value = oDataRow["BATCH_USER_ID"].ToString().Replace("'", "");

                //-----------------------
                dtgrdGeneral.Rows[n].Cells["Cliente"].Value = oDataRow["Cliente"].ToString().Replace("'", "");

                dtgrdGeneral.Rows[n].Cells["CuentaNueva"].Value = oDataRow["Cuenta"].ToString().Replace("'", "");

                dtgrdGeneral.Rows[n].Cells["ID"].Value = oDataRow["ID"].ToString().Replace("'", "");

                dtgrdGeneral.Rows[n].Cells["Lineas"].Value = oDataRow["Comentario"].ToString().Replace("'", "");
                //---------------------------------
                double debe = 0;
                debe = double.Parse(Math.Round(double.Parse(dtgrdGeneral.Rows[n].Cells["Cargos"].Value.ToString()), 2).ToString());
                double haber = 0;
                haber = double.Parse(Math.Round(double.Parse(dtgrdGeneral.Rows[n].Cells["Abonos"].Value.ToString()), 2).ToString());

                guardar(Globales.usuario.usuario, dtgrdGeneral.Rows[n].Cells["Cuenta"].Value.ToString()
                    , dtgrdGeneral.Rows[n].Cells["Descripcion"].Value.ToString()
                    , double.Parse(Math.Round(saldo_inicial, 2).ToString()), debe, haber, double.Parse(Math.Round(saldo_final, 2).ToString())
                    , dtgrdGeneral.Rows[n].Cells["Referencia"].Value.ToString()
                    , dtgrdGeneral.Rows[n].Cells["Fecha"].Value.ToString()
                    , oDataRow["CURRENCY_ID"].ToString()
                    , oDataRow["BATCH_ID"].ToString()
                    , oDataRow["TYPE"].ToString());

                saldo_inicial = saldo_final;

                toolStripProgressBar1.Value++;
            }
        }

        private void guardar(string USUARIO, string CUENTA, string DESCRIPCION, double SALDO_INICIAL, double DEBE, double HABER, double SALDO_FINAL, string REFERENCIA, string FECHA, string CURRENCY, string BATCH, string TYPE)
        {
            string sSQL = "";
            System.Windows.Forms.Application.DoEvents();
            sSQL = " INSERT INTO [VMX_AUXILIAR_CONTABLE] ";
            sSQL += " ([USUARIO],[CUENTA] ,[DESCRIPCION],[SALDO_INICIAL],[DEBE] ,[HABER],[SALDO_FINAL],[FECHA],[REFERENCIA],[CURRENCY],[BATCH],[TYPE]) ";
            sSQL += " VALUES ";
            sSQL += " ('" + USUARIO + "','" + CUENTA + "','" + DESCRIPCION + "'," + SALDO_INICIAL.ToString() + " ";
            sSQL += " ," + DEBE.ToString() + "," + HABER.ToString() + "," + SALDO_FINAL.ToString() + ",'" + FECHA + "','" + REFERENCIA + "','" + CURRENCY + "','" + BATCH + "','" + TYPE + "'";
            sSQL += " ) ";
            oData.EjecutarConsulta(sSQL);
        }

        private void button3_Click(object sender, EventArgs e)
        {
            exportar();
        }
        private DataTable GetDataTableFromDGV(DataGridView dgv)
        {
            var dt = new DataTable();
            foreach (DataGridViewColumn column in dgv.Columns)
            {
                //if (column.Visible)
                //{
                // You could potentially name the column based on the DGV column name (beware of dupes)
                // or assign a type based on the data type of the data bound to this DGV column.
                dt.Columns.Add(column.HeaderText);
                //}
            }

            object[] cellValues = new object[dgv.Columns.Count];
            foreach (DataGridViewRow row in dgv.Rows)
            {
                for (int i = 0; i < row.Cells.Count; i++)
                {
                    cellValues[i] = row.Cells[i].Value;
                }
                dt.Rows.Add(cellValues);
            }

            return dt;
        }

        private void exportar()
        {
            try
            {
                var fileName = "Auxiliar contable " + DateTime.Now.ToString("yyyy-MM-dd--hh-mm-ss") + ".xlsx";
                if (folderBrowserDialog1.ShowDialog() == DialogResult.OK)
                {
                    fileName = folderBrowserDialog1.SelectedPath + "\\" + fileName;
                    System.IO.Stream myStream = new System.IO.FileStream(fileName, System.IO.FileMode.Create, System.IO.FileAccess.ReadWrite, System.IO.FileShare.None);

                    using (ExcelPackage pck = new ExcelPackage())
                    {
                        DataTable oDataTable = new DataTable();

                        oDataTable = GetDataTableFromDGV(dtgrdGeneral);
                        ExcelWorksheet ws1 = pck.Workbook.Worksheets.Add("Cuentas");
                        ws1.Cells["A1"].LoadFromDataTable(oDataTable, true);
                        pck.SaveAs(myStream);
                    }
                    myStream.Close();
                    MessageBox.Show("Exportación del archivo " + Environment.NewLine + fileName + " finalizada.", Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
            catch (Exception e)
            {

            }
        }
    }
}
