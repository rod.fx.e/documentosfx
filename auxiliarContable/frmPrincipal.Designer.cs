#region Copyright Syncfusion Inc. 2001 - 2017
// Copyright Syncfusion Inc. 2001 - 2017. All rights reserved.
// Use of this code is subject to the terms of our license.
// A copy of the current license can be obtained at any time by e-mailing
// licensing@syncfusion.com. Any infringement will be prosecuted under
// applicable laws. 
#endregion
namespace auxiliarContable
{
    partial class frmPrincipal
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmPrincipal));
            this.DESCRIPCION_2 = new System.Windows.Forms.Label();
            this.DESCRIPCION_1 = new System.Windows.Forms.Label();
            this.ACCOUNT_ID_2 = new System.Windows.Forms.TextBox();
            this.button1 = new System.Windows.Forms.Button();
            this.ACCOUNT_ID_1 = new System.Windows.Forms.TextBox();
            this.btnBuscar = new System.Windows.Forms.Button();
            this.FECHA_FINAL = new System.Windows.Forms.DateTimePicker();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.FECHA_INICIO = new System.Windows.Forms.DateTimePicker();
            this.ENTIDAD = new System.Windows.Forms.ComboBox();
            this.MONEDA = new System.Windows.Forms.ComboBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.dtgrdGeneral = new System.Windows.Forms.DataGridView();
            this.button2 = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.toolStripStatusLabel1 = new System.Windows.Forms.ToolStripStatusLabel();
            this.toolStripProgressBar1 = new System.Windows.Forms.ToolStripProgressBar();
            this.toolStripStatusLabel2 = new System.Windows.Forms.ToolStripStatusLabel();
            this.folderBrowserDialog1 = new System.Windows.Forms.FolderBrowserDialog();
            ((System.ComponentModel.ISupportInitialize)(this.dtgrdGeneral)).BeginInit();
            this.statusStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // DESCRIPCION_2
            // 
            this.DESCRIPCION_2.AutoSize = true;
            this.DESCRIPCION_2.Location = new System.Drawing.Point(311, 276);
            this.DESCRIPCION_2.Name = "DESCRIPCION_2";
            this.DESCRIPCION_2.Size = new System.Drawing.Size(0, 13);
            this.DESCRIPCION_2.TabIndex = 106;
            // 
            // DESCRIPCION_1
            // 
            this.DESCRIPCION_1.AutoSize = true;
            this.DESCRIPCION_1.Location = new System.Drawing.Point(311, 276);
            this.DESCRIPCION_1.Name = "DESCRIPCION_1";
            this.DESCRIPCION_1.Size = new System.Drawing.Size(0, 13);
            this.DESCRIPCION_1.TabIndex = 105;
            // 
            // ACCOUNT_ID_2
            // 
            this.ACCOUNT_ID_2.Location = new System.Drawing.Point(351, 39);
            this.ACCOUNT_ID_2.Name = "ACCOUNT_ID_2";
            this.ACCOUNT_ID_2.Size = new System.Drawing.Size(111, 20);
            this.ACCOUNT_ID_2.TabIndex = 104;
            // 
            // button1
            // 
            this.button1.AccessibleDescription = "Buscar";
            this.button1.BackColor = System.Drawing.Color.LightSeaGreen;
            this.button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button1.ForeColor = System.Drawing.Color.White;
            this.button1.Location = new System.Drawing.Point(234, 38);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(111, 23);
            this.button1.TabIndex = 103;
            this.button1.Text = "Cuenta &Final";
            this.button1.UseVisualStyleBackColor = false;
            // 
            // ACCOUNT_ID_1
            // 
            this.ACCOUNT_ID_1.Location = new System.Drawing.Point(117, 39);
            this.ACCOUNT_ID_1.Name = "ACCOUNT_ID_1";
            this.ACCOUNT_ID_1.Size = new System.Drawing.Size(111, 20);
            this.ACCOUNT_ID_1.TabIndex = 102;
            // 
            // btnBuscar
            // 
            this.btnBuscar.AccessibleDescription = "Buscar";
            this.btnBuscar.BackColor = System.Drawing.Color.LightSeaGreen;
            this.btnBuscar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnBuscar.ForeColor = System.Drawing.Color.White;
            this.btnBuscar.Location = new System.Drawing.Point(0, 38);
            this.btnBuscar.Name = "btnBuscar";
            this.btnBuscar.Size = new System.Drawing.Size(111, 23);
            this.btnBuscar.TabIndex = 101;
            this.btnBuscar.Text = "Cuenta &Inicial";
            this.btnBuscar.UseVisualStyleBackColor = false;
            // 
            // FECHA_FINAL
            // 
            this.FECHA_FINAL.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.FECHA_FINAL.Location = new System.Drawing.Point(40, 12);
            this.FECHA_FINAL.Name = "FECHA_FINAL";
            this.FECHA_FINAL.Size = new System.Drawing.Size(102, 20);
            this.FECHA_FINAL.TabIndex = 95;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(148, 16);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(21, 13);
            this.label2.TabIndex = 93;
            this.label2.Text = "Fin";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(2, 16);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(32, 13);
            this.label1.TabIndex = 92;
            this.label1.Text = "Inicio";
            // 
            // FECHA_INICIO
            // 
            this.FECHA_INICIO.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.FECHA_INICIO.Location = new System.Drawing.Point(175, 12);
            this.FECHA_INICIO.Name = "FECHA_INICIO";
            this.FECHA_INICIO.Size = new System.Drawing.Size(102, 20);
            this.FECHA_INICIO.TabIndex = 94;
            // 
            // ENTIDAD
            // 
            this.ENTIDAD.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.ENTIDAD.FormattingEnabled = true;
            this.ENTIDAD.Location = new System.Drawing.Point(511, 15);
            this.ENTIDAD.Name = "ENTIDAD";
            this.ENTIDAD.Size = new System.Drawing.Size(121, 21);
            this.ENTIDAD.TabIndex = 100;
            // 
            // MONEDA
            // 
            this.MONEDA.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.MONEDA.FormattingEnabled = true;
            this.MONEDA.Location = new System.Drawing.Point(335, 13);
            this.MONEDA.Name = "MONEDA";
            this.MONEDA.Size = new System.Drawing.Size(121, 21);
            this.MONEDA.TabIndex = 99;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(462, 18);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(43, 13);
            this.label4.TabIndex = 98;
            this.label4.Text = "Entidad";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(283, 16);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(46, 13);
            this.label3.TabIndex = 97;
            this.label3.Text = "Moneda";
            // 
            // dtgrdGeneral
            // 
            this.dtgrdGeneral.AllowUserToAddRows = false;
            this.dtgrdGeneral.AllowUserToDeleteRows = false;
            this.dtgrdGeneral.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dtgrdGeneral.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.dtgrdGeneral.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dtgrdGeneral.Location = new System.Drawing.Point(0, 96);
            this.dtgrdGeneral.Name = "dtgrdGeneral";
            this.dtgrdGeneral.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dtgrdGeneral.Size = new System.Drawing.Size(658, 338);
            this.dtgrdGeneral.TabIndex = 96;
            // 
            // button2
            // 
            this.button2.AccessibleDescription = "Buscar";
            this.button2.BackColor = System.Drawing.Color.DarkOrange;
            this.button2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button2.ForeColor = System.Drawing.Color.White;
            this.button2.Location = new System.Drawing.Point(0, 67);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(111, 23);
            this.button2.TabIndex = 107;
            this.button2.Text = "Procesar";
            this.button2.UseVisualStyleBackColor = false;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // button3
            // 
            this.button3.AccessibleDescription = "Buscar";
            this.button3.BackColor = System.Drawing.Color.YellowGreen;
            this.button3.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button3.ForeColor = System.Drawing.Color.Transparent;
            this.button3.Location = new System.Drawing.Point(117, 66);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(111, 23);
            this.button3.TabIndex = 108;
            this.button3.Text = "Exportar";
            this.button3.UseVisualStyleBackColor = false;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // statusStrip1
            // 
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripStatusLabel1,
            this.toolStripProgressBar1,
            this.toolStripStatusLabel2});
            this.statusStrip1.Location = new System.Drawing.Point(0, 437);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(658, 22);
            this.statusStrip1.TabIndex = 109;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // toolStripStatusLabel1
            // 
            this.toolStripStatusLabel1.AutoSize = false;
            this.toolStripStatusLabel1.Name = "toolStripStatusLabel1";
            this.toolStripStatusLabel1.Size = new System.Drawing.Size(300, 17);
            this.toolStripStatusLabel1.Text = "Estado";
            // 
            // toolStripProgressBar1
            // 
            this.toolStripProgressBar1.Name = "toolStripProgressBar1";
            this.toolStripProgressBar1.Size = new System.Drawing.Size(100, 16);
            // 
            // toolStripStatusLabel2
            // 
            this.toolStripStatusLabel2.Name = "toolStripStatusLabel2";
            this.toolStripStatusLabel2.Size = new System.Drawing.Size(118, 17);
            this.toolStripStatusLabel2.Text = "toolStripStatusLabel2";
            // 
            // frmPrincipal
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(658, 459);
            this.Controls.Add(this.statusStrip1);
            this.Controls.Add(this.button3);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.DESCRIPCION_2);
            this.Controls.Add(this.DESCRIPCION_1);
            this.Controls.Add(this.ACCOUNT_ID_2);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.ACCOUNT_ID_1);
            this.Controls.Add(this.btnBuscar);
            this.Controls.Add(this.FECHA_FINAL);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.FECHA_INICIO);
            this.Controls.Add(this.ENTIDAD);
            this.Controls.Add(this.MONEDA);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.dtgrdGeneral);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MetroColor = System.Drawing.Color.Transparent;
            this.Name = "frmPrincipal";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Auxiliar contable";
            ((System.ComponentModel.ISupportInitialize)(this.dtgrdGeneral)).EndInit();
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label DESCRIPCION_2;
        private System.Windows.Forms.Label DESCRIPCION_1;
        private System.Windows.Forms.TextBox ACCOUNT_ID_2;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.TextBox ACCOUNT_ID_1;
        private System.Windows.Forms.Button btnBuscar;
        private System.Windows.Forms.DateTimePicker FECHA_FINAL;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.DateTimePicker FECHA_INICIO;
        private System.Windows.Forms.ComboBox ENTIDAD;
        private System.Windows.Forms.ComboBox MONEDA;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.DataGridView dtgrdGeneral;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel1;
        private System.Windows.Forms.ToolStripProgressBar toolStripProgressBar1;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel2;
        private System.Windows.Forms.FolderBrowserDialog folderBrowserDialog1;
    }
}