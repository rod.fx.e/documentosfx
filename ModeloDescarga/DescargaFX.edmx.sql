
-- --------------------------------------------------
-- Entity Designer DDL Script for SQL Server 2005, 2008, 2012 and Azure
-- --------------------------------------------------
-- Date Created: 02/09/2023 06:34:08
-- Generated from EDMX file: D:\Proyectos\DocumentosFX\SAT\ImportarCatalogos\ModeloDescarga\DescargaFX.edmx
-- --------------------------------------------------

SET QUOTED_IDENTIFIER OFF;
GO
USE [documentosfx];
GO
IF SCHEMA_ID(N'dbo') IS NULL EXECUTE(N'CREATE SCHEMA [dbo]');
GO

-- --------------------------------------------------
-- Dropping existing FOREIGN KEY constraints
-- --------------------------------------------------


-- --------------------------------------------------
-- Dropping existing tables
-- --------------------------------------------------

IF OBJECT_ID(N'[dbo].[DescargaSolicitudSet]', 'U') IS NOT NULL
    DROP TABLE [dbo].[DescargaSolicitudSet];
GO
IF OBJECT_ID(N'[dbo].[DescargaConfiguracionSet]', 'U') IS NOT NULL
    DROP TABLE [dbo].[DescargaConfiguracionSet];
GO

-- --------------------------------------------------
-- Creating all tables
-- --------------------------------------------------

-- Creating table 'DescargaSolicitudSet'
CREATE TABLE [dbo].[DescargaSolicitudSet] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Inicio] datetime  NOT NULL,
    [Rfc] nvarchar(max)  NOT NULL,
    [Tipo] nvarchar(max)  NOT NULL,
    [ArchivoPFX] nvarchar(max)  NOT NULL,
    [Fin] datetime  NOT NULL,
    [UltimaVerificacion] datetime  NULL,
    [Solicitud] nvarchar(max)  NOT NULL,
    [Estado] nvarchar(max)  NOT NULL,
    [Paquete] nvarchar(max)  NULL,
    [Archivo] nvarchar(max)  NULL,
    [TotalCFDI] int  NOT NULL,
    [Creado] datetime  NOT NULL,
    [Usuario] nvarchar(max)  NOT NULL
);
GO

-- Creating table 'DescargaConfiguracionSet'
CREATE TABLE [dbo].[DescargaConfiguracionSet] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [PasswordZip] nvarchar(max)  NOT NULL,
    [VerificarAutomatica] bit  NOT NULL
);
GO

-- --------------------------------------------------
-- Creating all PRIMARY KEY constraints
-- --------------------------------------------------

-- Creating primary key on [Id] in table 'DescargaSolicitudSet'
ALTER TABLE [dbo].[DescargaSolicitudSet]
ADD CONSTRAINT [PK_DescargaSolicitudSet]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'DescargaConfiguracionSet'
ALTER TABLE [dbo].[DescargaConfiguracionSet]
ADD CONSTRAINT [PK_DescargaConfiguracionSet]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- --------------------------------------------------
-- Creating all FOREIGN KEY constraints
-- --------------------------------------------------

-- --------------------------------------------------
-- Script has ended
-- --------------------------------------------------