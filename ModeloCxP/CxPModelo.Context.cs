﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace ModeloCxP
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Infrastructure;
    
    public partial class CxPEntities : DbContext
    {
        public CxPEntities()
            : base("name=CxPEntities")
        {
        }
    
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            throw new UnintentionalCodeFirstException();
        }
    
        public virtual DbSet<CxPReglas> CxPReglasSet { get; set; }
        public virtual DbSet<DeclaracionImpuesto> DeclaracionImpuestoSet { get; set; }
        public virtual DbSet<Flujoefectivo> FlujoefectivoSet { get; set; }
        public virtual DbSet<Flujoefectivogastofijo> FlujoefectivogastofijoSet { get; set; }
        public virtual DbSet<GastoFijo> GastoFijoSet { get; set; }
        public virtual DbSet<CxCCobranza> CxCCobranzaSet { get; set; }
        public virtual DbSet<CxCCobranzaDia> CxCCobranzaDiaSet { get; set; }
    }
}
