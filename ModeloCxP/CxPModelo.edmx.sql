
-- --------------------------------------------------
-- Entity Designer DDL Script for SQL Server 2005, 2008, 2012 and Azure
-- --------------------------------------------------
-- Date Created: 08/28/2024 12:33:12
-- Generated from EDMX file: D:\Proyectos\DocumentosFX\SAT\ImportarCatalogos\ModeloCxP\CxPModelo.edmx
-- --------------------------------------------------

SET QUOTED_IDENTIFIER OFF;
GO
USE [documentosfx];
GO
IF SCHEMA_ID(N'dbo') IS NULL EXECUTE(N'CREATE SCHEMA [dbo]');
GO

-- --------------------------------------------------
-- Dropping existing FOREIGN KEY constraints
-- --------------------------------------------------


-- --------------------------------------------------
-- Dropping existing tables
-- --------------------------------------------------

IF OBJECT_ID(N'[dbo].[CxPReglasSet]', 'U') IS NOT NULL
    DROP TABLE [dbo].[CxPReglasSet];
GO
IF OBJECT_ID(N'[dbo].[DeclaracionImpuestoSet]', 'U') IS NOT NULL
    DROP TABLE [dbo].[DeclaracionImpuestoSet];
GO
IF OBJECT_ID(N'[dbo].[FlujoefectivoSet]', 'U') IS NOT NULL
    DROP TABLE [dbo].[FlujoefectivoSet];
GO
IF OBJECT_ID(N'[dbo].[FlujoefectivogastofijoSet]', 'U') IS NOT NULL
    DROP TABLE [dbo].[FlujoefectivogastofijoSet];
GO
IF OBJECT_ID(N'[dbo].[GastoFijoSet]', 'U') IS NOT NULL
    DROP TABLE [dbo].[GastoFijoSet];
GO
IF OBJECT_ID(N'[dbo].[CxCCobranzaSet]', 'U') IS NOT NULL
    DROP TABLE [dbo].[CxCCobranzaSet];
GO
IF OBJECT_ID(N'[dbo].[CxCCobranzaDiaSet]', 'U') IS NOT NULL
    DROP TABLE [dbo].[CxCCobranzaDiaSet];
GO

-- --------------------------------------------------
-- Creating all tables
-- --------------------------------------------------

-- Creating table 'CxPReglasSet'
CREATE TABLE [dbo].[CxPReglasSet] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Tipo] nvarchar(max)  NOT NULL,
    [Valor] nvarchar(max)  NOT NULL,
    [CuentaContable] nvarchar(max)  NOT NULL,
    [Negativo] bit  NOT NULL,
    [CondicionTipo] nvarchar(max)  NULL,
    [CondicionValor] nvarchar(max)  NULL
);
GO

-- Creating table 'DeclaracionImpuestoSet'
CREATE TABLE [dbo].[DeclaracionImpuestoSet] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Inicio] datetime  NOT NULL,
    [Fin] datetime  NOT NULL,
    [FechaCreacion] datetime  NOT NULL,
    [UsuarioCreacion] datetime  NOT NULL
);
GO

-- Creating table 'FlujoefectivoSet'
CREATE TABLE [dbo].[FlujoefectivoSet] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Inicio] datetime  NOT NULL,
    [Fin] datetime  NOT NULL,
    [FechaCreacion] datetime  NOT NULL,
    [UsuarioCreacion] datetime  NOT NULL
);
GO

-- Creating table 'FlujoefectivogastofijoSet'
CREATE TABLE [dbo].[FlujoefectivogastofijoSet] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [FechaCreacion] datetime  NOT NULL,
    [UsuarioCreacion] datetime  NOT NULL,
    [Concepto] nvarchar(max)  NOT NULL,
    [Monto] decimal(16,4)  NOT NULL
);
GO

-- Creating table 'GastoFijoSet'
CREATE TABLE [dbo].[GastoFijoSet] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Proveedor] nvarchar(max)  NOT NULL,
    [Nombre] nvarchar(max)  NOT NULL,
    [Monto] decimal(16,4)  NOT NULL
);
GO

-- Creating table 'CxCCobranzaSet'
CREATE TABLE [dbo].[CxCCobranzaSet] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Cliente] nvarchar(max)  NOT NULL,
    [FechaCreacion] datetime  NOT NULL,
    [UsuarioCreacion] datetime  NOT NULL,
    [Comentario] nvarchar(max)  NOT NULL
);
GO

-- Creating table 'CxCCobranzaDiaSet'
CREATE TABLE [dbo].[CxCCobranzaDiaSet] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Cliente] nvarchar(max)  NOT NULL,
    [FechaCreacion] datetime  NOT NULL,
    [UsuarioCreacion] datetime  NOT NULL,
    [Comentario] nvarchar(max)  NOT NULL,
    [Dia] nvarchar(max)  NOT NULL,
    [Factura] nvarchar(max)  NULL
);
GO

-- --------------------------------------------------
-- Creating all PRIMARY KEY constraints
-- --------------------------------------------------

-- Creating primary key on [Id] in table 'CxPReglasSet'
ALTER TABLE [dbo].[CxPReglasSet]
ADD CONSTRAINT [PK_CxPReglasSet]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'DeclaracionImpuestoSet'
ALTER TABLE [dbo].[DeclaracionImpuestoSet]
ADD CONSTRAINT [PK_DeclaracionImpuestoSet]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'FlujoefectivoSet'
ALTER TABLE [dbo].[FlujoefectivoSet]
ADD CONSTRAINT [PK_FlujoefectivoSet]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'FlujoefectivogastofijoSet'
ALTER TABLE [dbo].[FlujoefectivogastofijoSet]
ADD CONSTRAINT [PK_FlujoefectivogastofijoSet]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'GastoFijoSet'
ALTER TABLE [dbo].[GastoFijoSet]
ADD CONSTRAINT [PK_GastoFijoSet]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'CxCCobranzaSet'
ALTER TABLE [dbo].[CxCCobranzaSet]
ADD CONSTRAINT [PK_CxCCobranzaSet]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'CxCCobranzaDiaSet'
ALTER TABLE [dbo].[CxCCobranzaDiaSet]
ADD CONSTRAINT [PK_CxCCobranzaDiaSet]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- --------------------------------------------------
-- Creating all FOREIGN KEY constraints
-- --------------------------------------------------

-- --------------------------------------------------
-- Script has ended
-- --------------------------------------------------