﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.OleDb;
using System.Drawing;

using System.Text;
using System.Windows.Forms;
using Generales;

namespace FE_FX
{
    public partial class frmLogin : Form
    {
        private cCONEXCION oData = new cCONEXCION("");
        bool isDEMO;
        string BD;
        public frmLogin(bool IsTrial)
        {
            string directorio=AppDomain.CurrentDomain.BaseDirectory;

                InitializeComponent();


                try
                {
                    BD = directorio + "BD.accdb";
                    oData = new cCONEXCION(BD, "", "");
                }
                catch (Exception e)
                {
                    MessageBox.Show("Verificar: " + Environment.NewLine + e.Message);
                    Application.Exit();
                }

                

                isDEMO = IsTrial;
                if (IsTrial)
                {
                    USUARIO.Text = "DEMO";
                    USUARIO.Enabled = false;
                    PASSWORD.Text = "DEMO";
                    PASSWORD.Enabled = false;
                    
                    
                    this.Text = Application.ProductName + " - Versión: " + Application.ProductVersion + " VERSION DEMO";

                }
                else
                {

                    this.Text = Application.ProductName + " - Versión: " + Application.ProductVersion;

                }

                
        }


        private void loguearse()
        {

            //Versión Normal
            cUSUARIO oUsuario = new cUSUARIO(oData);
            if (oUsuario.login(USUARIO.Text, PASSWORD.Text))
            {
                this.Hide();
                //frmPrincipal oMainWindow = new frmPrincipal(oData.sConn, USUARIO.Text);
                ////Mostrar certificados
                ////frmCertificados oMainWindow = new frmCertificados(oData.sConn, "");
                //oMainWindow.Show();
            }
            
        }

        private void button6_Click(object sender, EventArgs e)
        {
            loguearse();
        }

        private void frmLogin_Load(object sender, EventArgs e)
        {
            if (oData.no_existe_BD == "ERROR")
            {
                Application.Exit();
            }

        }

        private void PASSWORD_KeyPress(object sender, KeyPressEventArgs e)
        {
            //Precionar Enter buscar
            if (e.KeyChar.ToString() == "\r")
            {
                loguearse();
            }
        }

        private void USUARIO_KeyPress(object sender, KeyPressEventArgs e)
        {
            //Precionar Enter buscar
            if (e.KeyChar.ToString() == "\r")
            {
                loguearse();
            }
        }
    }
}
