﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Xml.Serialization;
using System.IO;
using System.Linq;
using System.Web;
using CryptoSysPKI;
using System.Diagnostics;
using System.Windows.Forms;
using System.Xml;
using System.Xml.Xsl;
using System.Xml.XPath;
using System.Text.RegularExpressions;
using System.Security.Cryptography.X509Certificates;
using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;
using System.Net.Mail;
using System.Net.NetworkInformation;
using System.Threading;
using System.Runtime.InteropServices;
using System.Globalization;
using System.Data;
using com.google.zxing.qrcode;
using com.google.zxing.common;
using System.Drawing;
using System.Drawing.Imaging;
using System.Configuration;
using FE_FX.Clases;
using Generales;
using ModeloDocumentosFX;
using System.Xml.Linq;

namespace FE_FX
{
    //TODO: Bradford Global actualizar los estados y sincronizador con el Global Search



    class cGeneracion
    {
        private cCONEXCION oData = new cCONEXCION("");
        private cCONEXCION oData_ERP = new cCONEXCION("");
        public string VMX_FE_Tabla = "VMX_FE";


        public cGeneracion(cCONEXCION pData, cCONEXCION pData_ERP)
        {
            oData = pData;
            oData_ERP = pData_ERP;
            actualizar_tabla();
        }
        public void actualizar_tabla()
        {
            //Buscar Alias de VMX_FE
            try
            {
                string TABLA_VMX_FE = ConfigurationManager.AppSettings["VMX_FE"].ToString();
                if (TABLA_VMX_FE != "")
                {
                    VMX_FE_Tabla = "FXINV000";
                }

            }
            catch
            {
            }
        }

        private bool FileExists(string pPath)
        {
            string sPath = pPath;
            try
            {
                if (File.Exists(sPath))
                {
                    return true;
                }
                else
                {
                    return false;
                }

            }
            catch
            {
                return false;
            }
        }

        public bool FileInUse(string path)
        {
            bool devolucion = false;
            if (FileExists(path))
            {
                try
                {
                    //Just opening the file as open/create
                    using (FileStream fs = new FileStream(path, FileMode.OpenOrCreate))
                    {
                        //If required we can check for read/write by using fs.CanRead or fs.CanWrite
                    }
                    return false;
                }
                catch (IOException)
                {
                    // The access requested is not permitted by the operating system
                    // for the specified path, such as when access is Write or ReadWrite
                    // and the file or directory is set for read-only access. 
                    return true;
                }
            }
            return devolucion;
        }

        public bool IsDate(object Expression)
        {
            if (Expression != null)
            {
                if (Expression is DateTime)
                {
                    return true;
                }
                if (Expression is string)
                {
                    try
                    {
                        DateTime dt;
                        DateTime.TryParse(Expression.ToString(), out dt);
                        if (dt != DateTime.MinValue && dt != DateTime.MaxValue)
                        {
                            return true;
                        }
                        else
                        {
                            //Verificar Factura 13-5-2011
                            string cadena = Expression.ToString();
                            string[] sDiv = cadena.Split('-');
                            DateTime fechaaux = new DateTime(int.Parse(sDiv[2]), int.Parse(sDiv[1]), int.Parse(sDiv[0]), 0, 0, 0);
                            if (fechaaux is DateTime)
                            {
                                return true;
                            }
                        }
                        return false;
                    }
                    catch (Exception)
                    {
                    }
                }
            }
            return false;
        }

        static string ExtractNumbers(string expr)
        {
            return string.Join(null, System.Text.RegularExpressions.Regex.Split(expr, "[^\\d]"));
        }

        public bool generar(string INVOICE_ID, string ADDR_NO, string ROW_ID_EMPRESA
            , string version, bool complementoComercioExterior = false
            , bool sinRelacion = false, bool sinCEE = false
            , bool fechaActual = false
            )
        {
            actualizar_tabla();



            try
            {
                //Valor por defecto del valorUnitario es de 2
                //Validar el XML 4 decimales
                int valorUnitariodecimales = 4;
                try
                {
                    valorUnitariodecimales = int.Parse(ConfigurationManager.AppSettings["valorUnitariodecimales"].ToString());
                }
                catch
                {

                }

                string sDirectory = AppDomain.CurrentDomain.BaseDirectory;
                //Buscar la Serie configurada
                //string SERIE_tr = INVOICE_ID.Replace("0", "").Replace("1", "").Replace("2", "").Replace("3", "").Replace("4", "").Replace("5", "").Replace("6", "").Replace("7", "").Replace("8", "").Replace("9", "").Trim();
                //Rodrigo Escalona 21/01/2021 Buscar la serie S21 en la factura
                cSERIE ocSERIE = new cSERIE();
                string SERIE_tr = ocSERIE.obtenerSerie(INVOICE_ID);
                if (!ocSERIE.cargar_serie(SERIE_tr, ROW_ID_EMPRESA))
                {
                    MessageBox.Show("La Serie " + SERIE_tr + " no esta configurada." + Environment.NewLine
                        + "cGeneracion - 186", Application.ProductName + "-" + Application.ProductVersion, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                    return false;
                }
                //Cargar Certificado
                //MessageBox.Show("Cargar el Certificado");
                cCERTIFICADO ocCERTIFICADO = new cCERTIFICADO();
                ocCERTIFICADO.cargar(ocSERIE.ROW_ID_CERTIFICADO);
                //MessageBox.Show("Cargar el Empresa");
                cEMPRESA ocEMPRESA = new cEMPRESA(oData);
                //MessageBox.Show("Certificado es " + ocCERTIFICADO.ROW_ID_EMPRESA);
                ocEMPRESA.cargar(ocCERTIFICADO.ROW_ID_EMPRESA);

                //MessageBox.Show("XLT");
                if (version == "3.2")
                {
                    if (!FileExists(sDirectory + "/XSLT/cadenaoriginal_3_2.xslt"))
                    {
                        MessageBox.Show("No se tiene acceso al archivo "
                              + Environment.NewLine
                        + sDirectory + "XSLT/cadenaoriginal_3_2.xslt "
                        , Application.ProductName + " - " + Application.ProductVersion, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);

                        return false;
                    }
                }
                if (version == "3.3")
                {
                    if (!FileExists(sDirectory + "/XSLT/cadenaoriginal_3_3.xslt"))
                    {
                        MessageBox.Show("No se tiene acceso al archivo "
                              + Environment.NewLine
                        + sDirectory + "XSLT/cadenaoriginal_3_3.xslt"
                            , Application.ProductName + "-" + Application.ProductVersion, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);

                        return false;
                    }
                }
                //MessageBox.Show("Cargado XLT");
                XmlSerializerNamespaces myNamespaces = new XmlSerializerNamespaces();
                myNamespaces.Add("cfdi", "http://www.sat.gob.mx/cfd/3");
                //MessageBox.Show("Cargado XLT " + ocCERTIFICADO.CERTIFICADO);
                if (!File.Exists(ocCERTIFICADO.CERTIFICADO))
                {
                    //MessageBox.Show(, Application.ProductName + "-" + Application.ProductVersion, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                    ErrorFX.mostrar(" cGeneracion 220 "
                        + Environment.NewLine
                        + "No se tiene acceso al archivo " + ocCERTIFICADO.CERTIFICADO + "", true, true, true);
                    return false;
                }
                X509Certificate cert = X509Certificate.CreateFromCertFile(ocCERTIFICADO.CERTIFICADO);
                string NoSerie = cert.GetSerialNumberString();

                StringBuilder SerieHex = new StringBuilder();
                for (int i = 0; i <= NoSerie.Length - 2; i += 2)
                {
                    SerieHex.Append(Convert.ToString(Convert.ToChar(Int32.Parse(NoSerie.Substring(i, 2), System.Globalization.NumberStyles.HexNumber))));
                }
                string NO_CERTIFICADO = SerieHex.ToString();

                cFACTURA oFACTURA = new cFACTURA(oData_ERP);
                oFACTURA.datos(INVOICE_ID, ocSERIE.ACCOUNT_ID_RETENIDO, ADDR_NO, ocEMPRESA, ocSERIE.ACCOUNT_ID_ANTICIPO);
                DateTime INVOICE_DATE = oFACTURA.traer_INVOICE_DATE(INVOICE_ID);
                bool resultado = false;
                Encapsular oEncapsular = new Encapsular();
                oEncapsular.ocEMPRESA = ocEMPRESA;
                oEncapsular.oData_ERP = oData_ERP;
                //if (version == "3.2")
                //{
                //    //generar32 ogenerar32 = new generar32();
                //    //resultado = ogenerar32.generar(oFACTURA,ocSERIE,oEncapsular,ocCERTIFICADO);
                //}
                //Validar la licencia
                if (oFACTURA.CREATE_DATE.Year > 2025)
                {
                    MessageBox.Show("Producto FX Group. www.fxgroup.com.mx - correo: info@fxgroup.com.mx - Telefono: 4498948398 - Licencia expirada ");
                    return false;
                }

                if (version == "3.3")
                {
                    generar33 ogenerar33 = new generar33();
                    resultado = ogenerar33.generar(oFACTURA, ocSERIE
                        , oEncapsular, ocCERTIFICADO, complementoComercioExterior
                        , sinRelacion, sinCEE);
                }
                if (version == "4.0")
                {
                    Generar40 OGenerar40 = new Generar40();
                    resultado = OGenerar40.Generar(oFACTURA, ocSERIE
                        , oEncapsular, ocCERTIFICADO, complementoComercioExterior
                        , sinRelacion, sinCEE, fechaActual);
                }
                return resultado;
            }
            catch (Exception errores)
            {
                ErrorFX.mostrar(errores, true, true, "cGeneracion - 257");
                return false;
            }
        }


        private string aplicarDescrip100(string p)
        {
            return Regex.Replace(p, "[A-Z]|[a-z]|[0-9]| |Ñ|ñ|!|&quot;|%|&amp;|&apos;|´|-|:|;|>|=|&lt;|@|_|,|\\{|\\}|`|~|á|é|í|ó|ú|Á|É|Í|Ó|Ú|ü|Ü){1,100}", "");
        }

        private string extraeLocalidad(string source)
        {
            string[] stringSeparators = new string[] { "LOC." };
            string[] result;
            result = source.Split(stringSeparators, StringSplitOptions.None);
            if (result.Length == 1)
            {
                return "";
            }
            return result[1];
        }

        private string extraeExterior(string source)
        {
            string[] stringSeparators = new string[] { "EXT.", "INT." };
            string[] result;
            result = source.Split(stringSeparators, StringSplitOptions.None);
            if (result.Length == 1)
            {
                return "";
            }
            return result[1];
        }

        private string extraeInterior(string source)
        {
            string[] stringSeparators = new string[] { "INT." };
            string[] result;
            result = source.Split(stringSeparators, StringSplitOptions.None);
            if (result.Length == 1)
            {
                return "";
            }
            return result[1];
        }

        private string extraeDireccion(string source)
        {
            string[] stringSeparators = new string[] { "EXT.", "INT." };
            string[] result;
            result = source.Split(stringSeparators, StringSplitOptions.None);
            if (result.Length == 1)
            {
                return source;
            }

            return result[0];
        }

        public cce11_v32.c_FraccionArancelaria GetColours(string fraccionarancelaria)
        {
            foreach (cce11_v32.c_FraccionArancelaria mc in Enum.GetValues(typeof(cce11_v32.c_FraccionArancelaria)))
                if (mc.ToString() == fraccionarancelaria)
                    return mc;

            return cce11_v32.c_FraccionArancelaria.Item01011001;
        }

        public decimal otroTruncar(decimal valor, int decimales)
        {
            try
            {

                decimal result = 0;
                string[] xs = valor.ToString().Split('.');

                string decimalparte = xs[1] + "0000000000000000000000";
                result = decimal.Parse(xs[0] + "." + decimalparte.Substring(0, Math.Min(decimalparte.Length, decimales)));
                return result;
            }
            catch
            {
                return valor;
            }
        }

        public decimal otroTruncar(decimal valor, cEMPRESA oEMPRESA)
        {
            try
            {

                decimal result = 0;
                string[] xs = valor.ToString().Split('.');

                string decimalparte = xs[1] + "0000000000000000000000";
                if (oEMPRESA.APROXIMACION == "Truncar")
                {
                    result = decimal.Parse(xs[0] + "." + decimalparte.Substring(0, Math.Min(decimalparte.Length, int.Parse(oEMPRESA.VALORES_DECIMALES))));
                }
                else
                {
                    result = Math.Round(valor, int.Parse(oEMPRESA.VALORES_DECIMALES));
                    xs = result.ToString().Split('.');
                    if (xs[1].Length != int.Parse(oEMPRESA.VALORES_DECIMALES))
                    {
                        decimalparte = xs[1] + "0000000000000000000000";
                        result = decimal.Parse(xs[0] + "." + decimalparte.Substring(0, int.Parse(oEMPRESA.VALORES_DECIMALES)));
                    }
                }
                return result;
            }
            catch
            {
                return valor;
            }
        }


        public string Fechammmyy(string pFecha)
        {
            DateTime Fecha = DateTime.Parse(pFecha.ToString());
            string FechaFormateada = "";
            string[] mMeses = new string[13];

            mMeses[1] = "Ene";
            mMeses[2] = "Feb";
            mMeses[3] = "Mar";
            mMeses[4] = "Abr";
            mMeses[5] = "May";
            mMeses[6] = "Jun";
            mMeses[7] = "Jul";
            mMeses[8] = "Agt";
            mMeses[9] = "Sep";
            mMeses[10] = "Oct";
            mMeses[11] = "Nov";
            mMeses[12] = "Dic";

            int Mes = int.Parse(Fecha.Month.ToString());

            FechaFormateada = mMeses[Mes] + AgregarCero(Fecha.Year.ToString());

            return FechaFormateada;
        }

        public string AgregarCero(string p)
        {

            String regreso = p;

            if (p.Length < 2)

                regreso = "0" + p;

            return regreso;
        }

        public void parsar_d1p1(string ARCHIVO)
        {
            ReplaceInFile(ARCHIVO, "d1p1", "xsi");
            //Fecha
            ReplaceInFile(ARCHIVO, "-06:00", "");

            ReplaceInFile(ARCHIVO, "xsi:cfdi=\"http://www.sat.gob.mx/cfd/3\"", "");
        }
        public string actualizar_invoice_id(string sSQLp)
        {
            //Buscar Alias de VMX_FE
            try
            {
                string VMX_FE_INVOICE_ID = ConfigurationManager.AppSettings["VMX_FE.INVOICE_ID"].ToString();
                if (VMX_FE_INVOICE_ID != "")
                {
                    return sSQLp.Replace("INVOICE_ID", "ID");
                }

            }
            catch
            {
            }
            return sSQLp;
        }

        public bool pdf(string INVOICE_ID, cFACTURA oFACTURA, cFACTURA_BANDEJA ocFACTURA_BANDEJA
            , string ROW_ID_EMPRESAp, string FORMATO = "")
        {
            try
            {

                string BD_Auxiliar = "";
                cEMPRESA oEMPRESAp = new cEMPRESA(oData);
                oEMPRESAp.cargar(ROW_ID_EMPRESAp);

                if (oEMPRESAp.BD_AUXILIAR != "")
                {
                    BD_Auxiliar = oEMPRESAp.BD_AUXILIAR + ".dbo.";
                }

                string sSQL = "UPDATE " + BD_Auxiliar + VMX_FE_Tabla + " SET PDF_ESTADO='' WHERE INVOICE_ID='" + INVOICE_ID + "'";
                sSQL = actualizar_invoice_id(sSQL);
                oData_ERP.EjecutarConsulta(sSQL);

                string VISTA = "VMX_FE_VISTA";
                //Buscar Alias de VMX_FE
                try
                {
                    VISTA = ConfigurationManager.AppSettings["VMX_FE_VISTA"].ToString();
                }
                catch
                {
                }

                //Serie
                cSERIE ocSERIE = new cSERIE();
                ocSERIE.cargar_serie(oFACTURA.SERIE, ROW_ID_EMPRESAp);
                //Formato
                ReportDocument oReport;
                oReport = new ReportDocument();
                string directorio = AppDomain.CurrentDomain.BaseDirectory;
                string ARCHIVO_RPT = ocSERIE.FORMATO; //directorio + @"\SWISSMEX.rpt";
                //Buscar el FORMATO y verificar la vista
                //Buscar en formatos
                cFORMATO ocFORMATO = new cFORMATO();
                if (!String.IsNullOrEmpty(FORMATO))
                {
                    if (ocFORMATO.cargar_id(FORMATO))
                    {
                        ARCHIVO_RPT = ocFORMATO.REPORTE;
                        if (ocFORMATO.VISTA != "")
                        {
                            VISTA = ocFORMATO.VISTA;
                        }
                    }
                }
                if (!File.Exists(ARCHIVO_RPT))
                {
                    //Rodrigo Escalona 30/09/2021 Cargar el archivo
                    OpenFileDialog openFileDialog1 = new OpenFileDialog();
                    openFileDialog1.CheckFileExists = true;
                    openFileDialog1.CheckPathExists = true;
                    openFileDialog1.Multiselect = false;
                    openFileDialog1.Filter = "Crystal Report (*.rpt)|*.txt|All files (*.*)|*.*";
                    if (openFileDialog1.ShowDialog() == DialogResult.OK)
                    {
                        
                        foreach (String file in openFileDialog1.FileNames)
                        {
                            ARCHIVO_RPT = file;
                            break;
                        }
                    }
                    else
                    {
                        ErrorFX.mostrar("cGeneracion 568 - pdf - No existe un RPT para seleccionar ", true, true, true);
                        return false;
                    }
                }

                if (!File.Exists(ARCHIVO_RPT))
                {

                    if (FORMATO == "")
                    {
                        FORMATO = ocSERIE.FORMATO;
                        //Si es NDC cargar Formato de NDC
                        if (oFACTURA.TIPO == "NOTA DE CREDITO")
                        {
                            FORMATO = ocFORMATO.buscarNC();
                            if (FORMATO == "")
                            {
                                FORMATO = FORMATO = ocSERIE.FORMATO;
                            }
                        }
                    }


                }

                if (oData_ERP.sTipo.ToUpper() == "ORACLE")
                {
                    sSQL = "SELECT * ";
                    sSQL += " FROM  FXINV001";
                    sSQL += " WHERE INVOICE_ID='" + INVOICE_ID + "' ";
                }
                else
                {
                    //Generar PDF
                    sSQL = "SELECT * ";
                    sSQL += " FROM  " + BD_Auxiliar + VISTA;
                    sSQL += " WHERE INVOICE_ID='" + INVOICE_ID + "' ";

                }
                //Buscar cfdiRelacionados
                ModeloDocumentosFXContainer dbContext = new ModeloDocumentosFXContainer();
                string documentosRelacionados = "";
                try
                {
                    var result = from b in dbContext.ndcFacturaSet
                                        .Where(a => a.INVOICE_ID.Equals(INVOICE_ID))
                                 select b;
                    List<ndcFactura> dt = result.ToList();
                    foreach (ndcFactura registro in dt)
                    {

                        if (documentosRelacionados != "")
                        {
                            documentosRelacionados += "," + registro.UUID;
                        }
                        else
                        {
                            documentosRelacionados += registro.UUID;
                        }
                    }
                }
                catch (Exception ex)
                {
                    ErrorFX.mostrar(ex, true, true, "cGeneracion 547 - cfdiRelacionados ", true);
                    return false;
                }

                DataTable oDataTable = new DataTable();
                oDataTable = oData_ERP.EjecutarConsulta(sSQL);
                int linea = 0;
                if (oDataTable == null)
                {
                    ErrorFX.mostrar("cGeneracion 568 - pdf - Verificar la vista de generación de PDF " + Environment.NewLine + sSQL, true, true, true);
                    return false;
                }

                foreach (DataRow oDataRow in oDataTable.Rows)
                {
                    //Agregar los campos de clasificador y um del SAT
                    try
                    {
                        if (oFACTURA.LINEAS != null)
                        {
                            if (oFACTURA.LINEAS.Length > linea)
                            {
                                oDataRow["MAT_GL_ACCT_ID"] = "";
                                if (!String.IsNullOrEmpty(oFACTURA.LINEAS[linea].ClaveProdServ))
                                {
                                    oDataRow["MAT_GL_ACCT_ID"] = oFACTURA.LINEAS[linea].ClaveProdServ;
                                }
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        ErrorFX.mostrar(ex, true, true, "cGeneracion 560 - ClaveProdServ ", true);
                        //return false;
                    }
                    //Buscar relacionados
                    try
                    {
                        oDataRow["AUTHORIZATION_ID"] = documentosRelacionados;
                        string lineatr = (oDataRow["LINE_NO"]).ToString();

                        ndcFacturaLinea resultado = (from b in dbContext.ndcFacturaLineaSet
                                        .Where(a => a.INVOICE_ID.Equals(INVOICE_ID) & a.LINE_NO.Equals(lineatr))
                                                     select b).FirstOrDefault();
                        if (resultado != null)
                        {
                            oDataRow["USER_5_CUSTORDERLIN"] = "Clasificador: " + resultado.clasificacion;
                            oDataRow["CUSTOMER_PART_ID"] = "PN: " + resultado.PART_ID;

                        }
                    }
                    catch (Exception ex)
                    {
                        ErrorFX.mostrar(ex, true, true, "cGeneracion 587 - Clasificador ", true);
                        //return false;
                    }
                    linea++;



                    try
                    {
                        string QR_Code = oDataRow["QR_Code"].ToString();
                        if (QR_Code.Length > 0)
                        {
                            int size = 320;
                            QRCodeWriter writer = new QRCodeWriter();
                            com.google.zxing.common.ByteMatrix matrix;
                            matrix = writer.encode(QR_Code, com.google.zxing.BarcodeFormat.QR_CODE, size, size, null);
                            Bitmap img = new Bitmap(size, size);
                            Color Color = Color.FromArgb(0, 0, 0);

                            for (int y = 0; y < matrix.Height; ++y)
                            {
                                for (int x = 0; x < matrix.Width; ++x)
                                {
                                    Color pixelColor = img.GetPixel(x, y);

                                    //Find the colour of the dot
                                    if (matrix.get_Renamed(x, y) == -1)
                                    {
                                        img.SetPixel(x, y, Color.White);
                                    }
                                    else
                                    {
                                        img.SetPixel(x, y, Color.Black);
                                    }
                                }
                            }


                            string nombre = Path.GetTempPath() + "QRP_TEMP.BMP";
                            img.Save(nombre);

                            FileStream fs = new FileStream(nombre, System.IO.FileMode.Open, System.IO.FileAccess.Read);
                            byte[] Image = new byte[fs.Length];
                            fs.Read(Image, 0, Convert.ToInt32(fs.Length));
                            int image_32 = Convert.ToInt32(fs.Length);
                            fs.Close();
                            File.Delete(nombre);

                            oDataRow["IMAGEN_QR_Code"] = Image;
                        }
                    }
                    catch (Exception ex)
                    {
                        ErrorFX.mostrar(ex, true, true, "cGeneracion 615 ", true);
                        return false;
                    }

                }


                if (ARCHIVO_RPT.Trim() == "")
                {
                    MessageBox.Show("El Formato para la serie " + ocSERIE.ID + " no existe "
                        + Environment.NewLine
                        + "cGeneracion 665");
                    return false;
                }

                BitacoraFX.Log("Imprimir el formato " + FORMATO + " Archivo: " + ARCHIVO_RPT);

                if (!File.Exists(ARCHIVO_RPT))
                {
                    //Verificar el archivo 
                    string filenameWithoutPath = Path.GetFileName(ARCHIVO_RPT);
                    if (!File.Exists(filenameWithoutPath))
                    {
                        MessageBox.Show("El Formato para la serie " + ocSERIE.ID + " no existe " + ARCHIVO_RPT);
                        return false;
                    }
                    else
                    {
                        ARCHIVO_RPT = filenameWithoutPath;
                    }
                }

                if (true)
                {
                    try
                    {
                        oReport.Load(ARCHIVO_RPT);
                        oReport.SetDataSource(oDataTable);
                        //int SubRerports = oReport.Subreports.Count;
                        //for (int i = 0; i < SubRerports; i++)
                        //{
                        //    string sSQLNoPartes = @"
                        //    SELECT * 
                        //    FROM  [documentosfx]..[configuracionPartesSet]";
                        //    DataTable oDataTablePartes = new DataTable();
                        //    oDataTablePartes = oData_ERP.EjecutarConsulta(sSQLNoPartes);
                        //    oReport.Subreports[i].SetDataSource(oDataTablePartes);
                        //}

                        int cnt = oReport.DataDefinition.ParameterFields.Count;
                        for (int i = 0; i < cnt; i++)
                        {
                            ParameterValues myvals = new ParameterValues();
                            ParameterDiscreteValue myDiscrete = new ParameterDiscreteValue();
                            switch (oReport.DataDefinition.ParameterFields[i].ParameterFieldName)
                            {
                                case "CADENA":
                                    myDiscrete.Value = "";
                                    if (ocFACTURA_BANDEJA.CADENA != null)
                                    {
                                        myDiscrete.Value = ocFACTURA_BANDEJA.CADENA;
                                    }

                                    myvals.Add(myDiscrete);
                                    oReport.DataDefinition.ParameterFields[i].CurrentValues.Add(myDiscrete);
                                    oReport.DataDefinition.ParameterFields[i].ApplyCurrentValues(myvals);
                                    break;
                                case "SELLO":
                                    myDiscrete.Value = "";
                                    if (ocFACTURA_BANDEJA.SELLO != null)
                                    {
                                        myDiscrete.Value = ocFACTURA_BANDEJA.SELLO;
                                    }
                                    myvals.Add(myDiscrete);
                                    oReport.DataDefinition.ParameterFields[i].CurrentValues.Add(myDiscrete);
                                    oReport.DataDefinition.ParameterFields[i].ApplyCurrentValues(myvals);
                                    break;
                                //case "Pm-VMX_FE_VISTA.ID":
                                //    myDiscrete.Value = "";
                                //    if (ocFACTURA_BANDEJA.SELLO != null)
                                //    {
                                //        myDiscrete.Value = ocFACTURA_BANDEJA.SELLO;
                                //    }
                                //    myvals.Add(myDiscrete);
                                //    oReport.DataDefinition.ParameterFields[i].CurrentValues.Add(myDiscrete);
                                //    oReport.DataDefinition.ParameterFields[i].ApplyCurrentValues(myvals);
                                //    break;
                                default:
                                    //Verificar el
                                    if (oReport.DataDefinition.ParameterFields[i].ValueType.ToString() == "StringField")
                                    {
                                        frmEntradaParametro ObjetoFROM = new frmEntradaParametro();
                                        ObjetoFROM.Text = oReport.DataDefinition.ParameterFields[i].ParameterFieldName;
                                        myDiscrete.Value = "";
                                        if (ObjetoFROM.ShowDialog() == DialogResult.OK)
                                        {
                                            myDiscrete.Value = ObjetoFROM.VALORp;

                                        }
                                        myvals.Add(myDiscrete);
                                        oReport.DataDefinition.ParameterFields[i].CurrentValues.Add(myDiscrete);
                                        oReport.DataDefinition.ParameterFields[i].ApplyCurrentValues(myvals);
                                    }

                                    if (oReport.DataDefinition.ParameterFields[i].ValueType.ToString() == "BooleanField")
                                    {
                                        frmEntradaParametroSiNo ObjetoFROM = new frmEntradaParametroSiNo(oReport.DataDefinition.ParameterFields[i].ParameterFieldName);
                                        ObjetoFROM.Text = oReport.DataDefinition.ParameterFields[i].ParameterFieldName;
                                        myDiscrete.Value = false;
                                        if (ObjetoFROM.ShowDialog() == DialogResult.OK)
                                        {
                                            myDiscrete.Value = bool.Parse(ObjetoFROM.VALORp);

                                        }
                                        myvals.Add(myDiscrete);
                                        oReport.DataDefinition.ParameterFields[i].CurrentValues.Add(myDiscrete);
                                        oReport.DataDefinition.ParameterFields[i].ApplyCurrentValues(myvals);
                                    }

                                    break;
                            }
                        }
                        //string  = "";
                        //PDF_Archivo = DIRECTORIO_ARCHIVOS + @"\" + INVOICE_ID + "_" + oFACTURA.CUSTOMER_ID + ".pdf";
                        //Generar la IMAGEN

                        //Asignar a los Subreportes la Carga

                        //ConnectionInfo coninfo = new ConnectionInfo();
                        //coninfo.DatabaseName = oData_ERP.sDatabase;
                        //coninfo.ServerName = oData_ERP.sServer;
                        //coninfo.UserID = oData_ERP.sUsername;
                        //coninfo.Password = oData_ERP.sPassword;

                        //foreach (CrystalDecisions.CrystalReports.Engine.Table crtbl in oReport.Database.Tables)
                        //{
                        //    TableLogOnInfo logoninfo = crtbl.LogOnInfo;
                        //    logoninfo.ConnectionInfo = coninfo;
                        //    crtbl.ApplyLogOnInfo(logoninfo);
                        //}

                        //////sSQL  = " SELECT * ";
                        //////sSQL += " FROM FEFX_PEDIMENTOS ";
                        //////sSQL += " WHERE INVOICE_ID = '" + INVOICE_ID + "' ";
                        //////DataTable oDataTable_Sub=oData_ERP.EjecutarConsulta(sSQL);
                        //////oReport.OpenSubreport("Pedimentos").SetDataSource(oDataTable_Sub);
                        //////oReport.SetParameterValue(1,oFACTURA.INVOICE_ID);
                        //////oReport.SetDatabaseLogon(oData_ERP.sUsername,oData_ERP.sPassword);
                        //////oReport.SetParameterValue("Pm-VMX_FE_VISTA.INVOICE_ID", oFACTURA.INVOICE_ID, "Pedimentos");

                        //Rodrigo Escalona: Cargar las Parts de XML 
                        try
                        {
                            //XmlDocument doc = new XmlDocument();
                            //doc.Load(ocFACTURA_BANDEJA.XML);
                            //XmlNodeList Partes = doc.GetElementsByTagName("cfdi:Parte");

                            if (oReport.Subreports.Count != 0)
                            {
                                //Generar PDF
                                sSQL = @"
                                SELECT * 
                                FROM  FXPartes
                                WHERE INVOICE_ID='" + INVOICE_ID + "' ";
                                
                                cCONEXCION DbFX = new cCONEXCION(dbContext.Database.Connection.ConnectionString);

                                DataTable oDataTablePartes = new DataTable();
                                oDataTablePartes = DbFX.EjecutarConsulta(sSQL);
                                foreach (ReportDocument sub in oReport.Subreports)
                                {
                                    sub.SetDataSource(oDataTablePartes);
                                }
                            }
                        }
                        catch (Exception ESubReporte)
                        {
                            ErrorFX.mostrar(ESubReporte, true, false, "cGeneracion 967 ", false);
                            return false;
                        }


                        if (!Directory.Exists(Path.GetDirectoryName(ocFACTURA_BANDEJA.PDF)))
                        {
                            //El directorio Path.GetDirectoryName(PDF_Archivo)

                        }
                        //PDF_Archivo = @"C:\Users\Rodrigo\Desktop\TEST.pdf";
                        




                        ExportOptions CrExportOptions = new ExportOptions();
                        DiskFileDestinationOptions CrDiskFileDestinationOptions = new DiskFileDestinationOptions();
                        PdfRtfWordFormatOptions CrFormatTypeOptions = new PdfRtfWordFormatOptions();
                        CrDiskFileDestinationOptions.DiskFileName = ocFACTURA_BANDEJA.PDF;
                        CrExportOptions = oReport.ExportOptions;
                        CrExportOptions.ExportDestinationType = ExportDestinationType.DiskFile;
                        CrExportOptions.ExportFormatType = ExportFormatType.PortableDocFormat;
                        CrExportOptions.DestinationOptions = CrDiskFileDestinationOptions;
                        CrExportOptions.FormatOptions = CrFormatTypeOptions;
                        oReport.Export();
                        //Fin de Generar PDF
                        if (oFACTURA.TYPE == "I")
                        {
                            if (oEMPRESAp.IMPRESION_AUTOMATICA)
                            {
                                oReport.PrintOptions.PrinterName = oEMPRESAp.IMPRESORA;
                                oReport.PrintToPrinter(1, true, 1, 1);
                            }
                        }

                        if (oFACTURA.TYPE == "M")
                        {
                            if (oEMPRESAp.IMPRESION_AUTOMATICA_NDC)
                            {
                                oReport.PrintOptions.PrinterName = oEMPRESAp.IMPRESORA;
                                oReport.PrintToPrinter(1, true, 1, 1);
                            }
                        }
                        //Fin de Impresión automatica

                        oReport.Close();
                        oReport.Dispose();
                        oReport = null;
                        GC.Collect();
                        GC.WaitForPendingFinalizers();

                    }
                    catch (Exception eerr)
                    {
                        MessageBox.Show("Error CRYSTAL REPORT 788 " + Environment.NewLine + eerr.ToString());
                        return false;
                    }

                }
                else
                {
                    MessageBox.Show("El Archivo " + ARCHIVO_RPT + " no existe");
                    return false;
                }
            }
            catch (Exception ex)
            {
                ErrorFX.mostrar(ex, true, true, "cGeneracion 790 ", true);
                return false;
            }
            return true;

        }

        public cce11_v32.c_FraccionArancelaria GetFraccionArancelaria(string valor)
        {
            foreach (cce11_v32.c_FraccionArancelaria mc in Enum.GetValues(typeof(cce11_v32.c_FraccionArancelaria)))
                if (mc.ToString() == "Item" + valor)
                    return mc;

            return cce11_v32.c_FraccionArancelaria.Item01011001;
        }

        static public void ReplaceInFile(string filePath, string searchText, string replaceText)
        {
            StreamReader reader = new StreamReader(filePath);
            string content = reader.ReadToEnd();
            reader.Close();

            content = Regex.Replace(content, searchText, replaceText);

            StreamWriter writer = new StreamWriter(filePath);
            writer.Write(content);
            writer.Close();
        }

        public string validacion(string cadena)
        {
            string devolucion = "";
            devolucion = cadena.ToUpper();

            devolucion = devolucion.Replace("á", "A");
            devolucion = devolucion.Replace("é", "E");
            devolucion = devolucion.Replace("í", "I");
            devolucion = devolucion.Replace("ó", "O");
            devolucion = devolucion.Replace("ú", "U");
            devolucion = devolucion.Replace("Á", "A");
            devolucion = devolucion.Replace("É", "E");
            devolucion = devolucion.Replace("Í", "I");
            devolucion = devolucion.Replace("Ó", "O");
            devolucion = devolucion.Replace("Ú", "U");
            devolucion = devolucion.Replace(">", "");
            devolucion = devolucion.Replace("<", "");
            //devolucion = devolucion.Replace("&", "");
            devolucion = devolucion.Replace("'", "");
            devolucion = devolucion.Replace("#", "");
            devolucion = devolucion.Replace("|", "");
            //devolucion = HtmlEncode(devolucion);

            devolucion = Regex.Replace(devolucion, @"[\u0000-\u001F]", string.Empty);


            return devolucion; //Regex.Replace(cadena, "[á,é,í,ó,ú,Á,É,Í,Ó,Ú,#,|,&,',>,<]", "");
        }

        public decimal truncar4(decimal importe)
        {
            return decimal.Parse((Math.Truncate(100000 * importe) / 100000).ToString("0.0000"));
        }


        public string validacion_AMP(string cadena)
        {
            string devolucion = cadena;
            //devolucion = devolucion.Replace("&", "&amp;");
            return devolucion;
        }

        public static string HtmlEncode(string text)
        {
            if (text == null)
                return null;

            StringBuilder sb = new StringBuilder(text.Length);

            int len = text.Length;
            for (int i = 0; i < len; i++)
            {
                switch (text[i])
                {

                    case '<':
                        sb.Append("&lt;");
                        break;
                    case '>':
                        sb.Append("&gt;");
                        break;
                    case '"':
                        sb.Append("&quot;");
                        break;
                    case '&':
                        sb.Append("&amp;");
                        break;
                    default:
                        if (text[i] > 159)
                        {
                            // decimal numeric entity
                            sb.Append("&#");
                            sb.Append(((int)text[i]).ToString(CultureInfo.InvariantCulture));
                            sb.Append(";");
                        }
                        else
                            sb.Append(text[i]);
                        break;
                }
            }
            return sb.ToString();
        }


        public void verificar_CROWN_Order(string INVOICE_IDp, cEMPRESA ocEMPRESA)
        {
            cFACTURA_BANDEJA ocFACTURA_BANDEJA = new cFACTURA_BANDEJA(oData_ERP, ocEMPRESA);
            ocFACTURA_BANDEJA.cargar_ID(INVOICE_IDp, ocEMPRESA);

            //Modificación de CROWN
            bool CROWN_Order = false;
            try
            {
                CROWN_Order = bool.Parse(ConfigurationManager.AppSettings["CROWN_Order"].ToString());
            }
            catch
            {

            }
            try
            {
                if (CROWN_Order)
                {
                    //Cargar el XML
                    XmlDocument xDoc = new XmlDocument();
                    xDoc.Load(ocFACTURA_BANDEJA.XML);
                    XmlAttribute xKey = xDoc.CreateAttribute("Orden");
                    XmlNodeList list = xDoc.GetElementsByTagName("cfdi:Concepto");
                    int i = 0;
                    foreach (XmlNode node in list)
                    {
                        i++;
                        // if element already there, it will override
                        XmlAttribute newAttr = xDoc.CreateAttribute("Orden");
                        newAttr.Value = i.ToString();
                        node.Attributes.Append(newAttr);
                    }


                    xDoc.Save(ocFACTURA_BANDEJA.XML);
                }
                //Fin de Modificación de CROWN
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message);
            }
        }

        public string SelloRSA(string LLAVE, string PASSWORD, string strData, decimal ANIO)
        {

            try
            {
                StringBuilder sbPassword;
                StringBuilder sbPrivateKey;
                byte[] b;
                byte[] block;
                int keyBytes;

                string strKeyFile = LLAVE;

                sbPassword = new StringBuilder(PASSWORD);

                sbPrivateKey = Rsa.ReadEncPrivateKey(strKeyFile, sbPassword.ToString());

                keyBytes = Rsa.KeyBytes(sbPrivateKey.ToString());

                b = System.Text.Encoding.UTF8.GetBytes(strData);


                block = Rsa.EncodeMsgForSignature(keyBytes, b, HashAlgorithm.Sha256);

                block = Rsa.RawPrivate(block, sbPrivateKey.ToString());

                string resultado = System.Convert.ToBase64String(block);

                Wipe.String(sbPassword);
                Wipe.String(sbPrivateKey);
                Wipe.Data(block);

                return resultado;
            }
            catch (Exception exLeyendaFiscal)
            {
                ErrorFX.mostrar(exLeyendaFiscal, true, true, "generar33 - SelloRSA - 1666", true);
                return null;
            }

        }

    }
}
