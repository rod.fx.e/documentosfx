﻿using System;
using System.Collections.Generic;

using System.Text;

namespace FE_FX
{
    public class cLINEA
    {
        public string LINEA;
        public string QTY;
        public string PART_ID;
        public string CUSTOMER_PART_ID;
        public string GL_ACCOUNT_ID;
        public string DESCRIPCION;
        public string UNIT_PRICE;
        public string Descuento;
        public string AMOUNT;
        public string STOCK_UM;
        public string UM_CODE;
        public string PRODUCT_CODE;
        public List<cLINEARETENCION> retencion;
        public string VAT_AMOUNT;
        public string VAT_PERCENT;
        public string ClaveProdServ;
        internal string SERVICE_CHARGE_ID;
        public string CustomerPoRef;
        public cLINEA()
        {
            QTY = "";
            PART_ID = "";
            GL_ACCOUNT_ID = "";
            DESCRIPCION = "";
            UNIT_PRICE = "";
            Descuento = "";
            AMOUNT = "";
            STOCK_UM = "";
            PRODUCT_CODE = "";
            VAT_AMOUNT = "0";
            VAT_PERCENT = "0";
            ClaveProdServ = "";
            UM_CODE = "";
            CUSTOMER_PART_ID = "";
            retencion = new List<cLINEARETENCION>();
            CustomerPoRef = "";
    }

        public cLINEA(string LINEAp, string QTYp, string PART_IDp, string DESCRIPCIONp
            , string UNIT_PRICEp, string AMOUNTp, string STOCK_UMp, string PRODUCT_CODEp
            , string VAT_AMOUNTp, string VAT_PERCENTp, string ClaveProdServp
            , string GL_ACCOUNT_IDp, string SERVICE_CHARGE_IDp, string CUSTOMER_PART_IDp = "", string UM_CODEp = "", string Descuentop = ""
            , string customerPoRef = "")
        {
            LINEA = LINEAp;
            QTY = QTYp;
            PART_ID = PART_IDp;
            DESCRIPCION = DESCRIPCIONp;
            UNIT_PRICE = UNIT_PRICEp;
            AMOUNT = AMOUNTp;
            STOCK_UM = STOCK_UMp;
            PRODUCT_CODE = PRODUCT_CODEp;
            VAT_AMOUNT = VAT_AMOUNTp;
            VAT_PERCENT = VAT_PERCENTp;
            ClaveProdServ = ClaveProdServp;
            GL_ACCOUNT_ID = GL_ACCOUNT_IDp;
            SERVICE_CHARGE_ID = SERVICE_CHARGE_IDp;
            CUSTOMER_PART_ID = CUSTOMER_PART_IDp;
            UM_CODE = UM_CODEp;
            Descuento = Descuentop;
            CustomerPoRef = customerPoRef;
        }
    }
}
