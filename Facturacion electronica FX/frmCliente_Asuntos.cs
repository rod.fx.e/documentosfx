﻿using DevComponents.DotNetBar;
using OfficeOpenXml;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Text;
using System.Windows.Forms;
using Generales;

namespace FE_FX
{
    public partial class frmCliente_Asuntos : Form
    {
        private cCONEXCION oData = new cCONEXCION("");
        private cCONEXCION oData_ERP = new cCONEXCION("");
        string devolver = "";
        public DataGridViewSelectedRowCollection selecionados;


        public frmCliente_Asuntos(string sConn,string sConn_ERP, string devolverp = "")
        {
            oData.sConn = sConn;
            oData_ERP.sConn = sConn_ERP;
            devolver = devolverp;
            InitializeComponent();
            ajax_loader.Visible = false;
            Columnas(dtgrdGeneral);
        }

        private void Columnas(DataGridView dtg)
        {
            int n = 0;

            n = dtg.Columns.Add("ROW_ID", "ROW_ID");
            dtg.Columns[n].ReadOnly = true;
            dtg.Columns[n].Visible = false;
            dtg.Columns[n].Width = 50;

            n = dtg.Columns.Add("CUSTOMER_ID", "Cliente");
            dtg.Columns[n].ReadOnly = true;
            dtg.Columns[n].Visible = true;
            dtg.Columns[n].Width = 100;

            n = dtg.Columns.Add("ASUNTO", "Asunto");
            dtg.Columns[n].ReadOnly = true;
            dtg.Columns[n].Visible = true;
            dtg.Columns[n].Width = 200;

            n = dtg.Columns.Add("CORREO", "Correos");
            dtg.Columns[n].ReadOnly = true;
            dtg.Columns[n].Visible = true;
            dtg.Columns[n].Width = 200;

        }

        private void cargar()
        {
            ajax_loader.Visible = true;
            int n;
            cCLIENTE_CONFIGURACION oObjeto = new cCLIENTE_CONFIGURACION(oData);
            toolStripStatusLabel1.Text = "Cargando ";
            dtgrdGeneral.Rows.Clear();
            foreach (cCLIENTE_CONFIGURACION registro in oObjeto.todos(BUSCAR.Text, top.Text))
            {
                n = dtgrdGeneral.Rows.Add();
                dtgrdGeneral.Rows[n].Cells["ROW_ID"].Value = registro.ROW_ID;
                dtgrdGeneral.Rows[n].Cells["ASUNTO"].Value = registro.ASUNTO;
                dtgrdGeneral.Rows[n].Cells["CORREO"].Value = registro.CORREO;
                dtgrdGeneral.Rows[n].Cells["CUSTOMER_ID"].Value = registro.CUSTOMER_ID;
            }
            toolStripStatusLabel1.Text = "Total " + dtgrdGeneral.Rows.Count.ToString();
            ajax_loader.Visible = false;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            cargar();
        }

        private void BUSCAR_KeyPress(object sender, KeyPressEventArgs e)
        {
            //Precionar Enter buscar
            if (e.KeyChar.ToString() == "\r")
            {
                cargar();
            }
            
        }

        private void frmUsuarios_Load(object sender, EventArgs e)
        {
            cargar();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            frmCliente_Asunto oObjeto = new frmCliente_Asunto(oData.sConn,oData_ERP.sConn);
            oObjeto.ShowDialog();
            cargar();
        }

        private void dtgrdGeneral_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            modificar();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            if (dtgrdGeneral.SelectedRows.Count > 0)
            {
                DataGridViewSelectedRowCollection arrSelectedRows = dtgrdGeneral.SelectedRows;
                string ROW_ID = (string)arrSelectedRows[0].Cells[0].Value;
                frmCliente_Asunto oObjeto = new frmCliente_Asunto(oData.sConn, oData_ERP.sConn, ROW_ID);
                oObjeto.ShowDialog();
                cargar();
            }
        }

        public void modificar()
        {
            if (dtgrdGeneral.SelectedRows.Count > 0)
            {
                DataGridViewSelectedRowCollection arrSelectedRows = dtgrdGeneral.SelectedRows;
                if (devolver == "")
                {
                    
                    string ROW_ID = (string)arrSelectedRows[0].Cells[0].Value;
                    frmCliente_Asunto oObjeto = new frmCliente_Asunto(oData.sConn,oData_ERP.sConn, ROW_ID);
                    oObjeto.ShowDialog();
                    cargar();
                }
                else
                {
                    selecionados = arrSelectedRows;
                    this.DialogResult = DialogResult.OK;
                }
            }
        }

        private void frmFormatos_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)27)
            {
                this.Close();
            }
            else
            {
                if ((e.KeyChar.ToString() == "<")
                                | (e.KeyChar.ToString() == ">")
                                
                                | (e.KeyChar.ToString() == "'")
                                | (e.KeyChar.ToString() == "#")
                                )
                {
                    e.Handled = true;
                }
            }
        }

        private void button5_Click(object sender, EventArgs e)
        {
            crear_plantilla();
        }

        private void crear_plantilla()
        {
            MessageBoxEx.Show("La configuración de  la Plantilla para migrar fracción arancelaría:" + Environment.NewLine
            + "Fila 1 - Encabezado" + Environment.NewLine
            + "Columna 1: Cliente " + Environment.NewLine
            + "Columna 2: Asunto " + Environment.NewLine
            + "Columna 3: Mensaje " + Environment.NewLine
            + "Columna 4: Formato " + Environment.NewLine
            + "Columna 5: Correo eléctronico " + Environment.NewLine
            + "Columna 6: Archivo " + Environment.NewLine
            + "Columna 7: Leyenda fiscal " + Environment.NewLine
            + "Si no tiene esta configuración no será cargada la información. "
            + "Será generada la demas información desde Visual. "
            , Application.ProductName + "-" + Application.ProductVersion.ToString()
            , MessageBoxButtons.OK
            );

            var fileName = " Plantilla de Clientes " + DateTime.Now.ToString("yyyy-MM-dd-hh-mm-ss") + ".xlsx";
            if (folderBrowserDialog1.ShowDialog() == DialogResult.OK)
            {
                fileName = folderBrowserDialog1.SelectedPath + "\\" + fileName;
                System.IO.Stream myStream = new System.IO.FileStream(fileName, System.IO.FileMode.Create, System.IO.FileAccess.ReadWrite, System.IO.FileShare.None);

                using (ExcelPackage pck = new ExcelPackage())
                {

                    ExcelWorksheet ws1 = pck.Workbook.Worksheets.Add("Plantilla");
                    ws1.Cells[1, 1].Value = "Cliente";
                    ws1.Cells[1, 2].Value = "Asunto";
                    ws1.Cells[1, 3].Value = "Mensaje";
                    ws1.Cells[1, 4].Value = "Formato";
                    ws1.Cells[1, 5].Value = "Correo eléctronico";
                    ws1.Cells[1, 6].Value = "Archivo";
                    ws1.Cells[1, 7].Value = "Leyenda fiscal";
                    pck.SaveAs(myStream);
                }
                myStream.Close();
                MessageBoxEx.Show("Creación de plantilla realizada " + Environment.NewLine + fileName + " finalizada.", Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private void button4_Click(object sender, EventArgs e)
        {
            importar_visual();
        }

        private void importar_visual()
        {
            OpenFileDialog openFileDialog1 = new OpenFileDialog();
            openFileDialog1.Filter = "Excel files (*.xlsx)|*.xlsx|All files (*.*)|*.*";
            openFileDialog1.FilterIndex = 1;
            openFileDialog1.RestoreDirectory = true;

            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                try
                {
                    procesar_visual(1, openFileDialog1.FileName);
                }
                catch (Exception)
                {
                    MessageBoxEx.Show("No existe el Archivo seleccionado", Application.ProductName + "-" + Application.ProductVersion, MessageBoxButtons.OK);
                }
            }
        }

        private void procesar_visual(int hoja, string archivo)
        {
            try
            {
                

                int n;
                int rowIndex = 1;
                FileInfo existingFile = new FileInfo(archivo);
                using (ExcelPackage package = new ExcelPackage(existingFile))
                {
                    ExcelWorksheet worksheet = package.Workbook.Worksheets[hoja];
                    while (worksheet.Cells[rowIndex, 2].Value != null)
                    {
                        toolStripStatusLabel1.Text = "Registro " + rowIndex.ToString();
                        //Agregar las lineas de la cabecera
                        if (rowIndex != 1)
                        {
                            string cliente = worksheet.Cells[rowIndex, 1].Text;
                            string asunto = worksheet.Cells[rowIndex, 2].Text;
                            string mensaje = worksheet.Cells[rowIndex, 3].Text;
                            string formato = worksheet.Cells[rowIndex, 4].Text;
                            string correo_electronico = worksheet.Cells[rowIndex, 5].Text;
                            string archivo_columna = worksheet.Cells[rowIndex, 6].Text;
                            string leyenda_fiscal = worksheet.Cells[rowIndex, 7].Text;
                            cCLIENTE_CONFIGURACION oObjeto = new cCLIENTE_CONFIGURACION(oData);
                            oObjeto.cargar_CUSTOMER_ID(cliente);
                            oObjeto.CUSTOMER_ID = cliente;
                            oObjeto.ASUNTO = asunto;
                            oObjeto.MENSAJE = mensaje;
                            oObjeto.FORMATO= formato;
                            oObjeto.CORREO = correo_electronico;
                            oObjeto.ARCHIVO = archivo_columna;
                            oObjeto.LEYENDA_FISCAL = leyenda_fiscal;
                            oObjeto.guardar();
                        }
                        rowIndex++;

                    }
                    MessageBoxEx.Show("Archivo Importado", Application.ProductName);
                }
            }
            catch (Exception e)
            {
                MessageBoxEx.Show(e.Message.ToString());
            }
        }

    }
}
