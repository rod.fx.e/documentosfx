﻿namespace FE_FX
{
    partial class frmCliente_Asunto
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmCliente_Asunto));
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.toolStripStatusLabel1 = new System.Windows.Forms.ToolStripStatusLabel();
            this.button3 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.ajax_loader = new System.Windows.Forms.PictureBox();
            this.label3 = new System.Windows.Forms.Label();
            this.ASUNTO = new System.Windows.Forms.TextBox();
            this.CUSTOMER_ID = new System.Windows.Forms.TextBox();
            this.button7 = new System.Windows.Forms.Button();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.MENSAJE = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.ARCHIVO = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.LEYENDA_FISCAL = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.FORMATO = new System.Windows.Forms.ComboBox();
            this.CORREO = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.paisEntrega = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.iva0 = new System.Windows.Forms.CheckBox();
            this.label10 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.USO_CFDI = new System.Windows.Forms.ComboBox();
            this.label12 = new System.Windows.Forms.Label();
            this.ShipTo = new System.Windows.Forms.TextBox();
            this.label13 = new System.Windows.Forms.Label();
            this.Europeo = new System.Windows.Forms.TextBox();
            this.label14 = new System.Windows.Forms.Label();
            this.Exportacion = new System.Windows.Forms.ComboBox();
            this.statusStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ajax_loader)).BeginInit();
            this.SuspendLayout();
            // 
            // statusStrip1
            // 
            this.statusStrip1.Dock = System.Windows.Forms.DockStyle.Top;
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripStatusLabel1});
            this.statusStrip1.Location = new System.Drawing.Point(0, 0);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(623, 22);
            this.statusStrip1.TabIndex = 0;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // toolStripStatusLabel1
            // 
            this.toolStripStatusLabel1.Name = "toolStripStatusLabel1";
            this.toolStripStatusLabel1.Size = new System.Drawing.Size(42, 17);
            this.toolStripStatusLabel1.Text = "Estado";
            // 
            // button3
            // 
            this.button3.BackColor = System.Drawing.SystemColors.Control;
            this.button3.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button3.Image = global::FE_FX.Properties.Resources.Guardar;
            this.button3.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button3.Location = new System.Drawing.Point(80, 28);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(83, 23);
            this.button3.TabIndex = 2;
            this.button3.Text = "Guardar";
            this.button3.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.button3.UseVisualStyleBackColor = false;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // button2
            // 
            this.button2.BackColor = System.Drawing.SystemColors.Control;
            this.button2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button2.Image = global::FE_FX.Properties.Resources.Nuevo;
            this.button2.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button2.Location = new System.Drawing.Point(4, 28);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(70, 23);
            this.button2.TabIndex = 1;
            this.button2.Text = "Nuevo";
            this.button2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.button2.UseVisualStyleBackColor = false;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // ajax_loader
            // 
            this.ajax_loader.AccessibleDescription = "";
            this.ajax_loader.Image = global::FE_FX.Properties.Resources.ajax_loader;
            this.ajax_loader.Location = new System.Drawing.Point(256, 28);
            this.ajax_loader.Name = "ajax_loader";
            this.ajax_loader.Size = new System.Drawing.Size(58, 22);
            this.ajax_loader.TabIndex = 38;
            this.ajax_loader.TabStop = false;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(6, 112);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(40, 13);
            this.label3.TabIndex = 12;
            this.label3.Text = "Asunto";
            // 
            // ASUNTO
            // 
            this.ASUNTO.Location = new System.Drawing.Point(66, 109);
            this.ASUNTO.Name = "ASUNTO";
            this.ASUNTO.Size = new System.Drawing.Size(545, 20);
            this.ASUNTO.TabIndex = 13;
            this.ASUNTO.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.frmUsuario_KeyPress);
            // 
            // CUSTOMER_ID
            // 
            this.CUSTOMER_ID.Location = new System.Drawing.Point(66, 57);
            this.CUSTOMER_ID.Name = "CUSTOMER_ID";
            this.CUSTOMER_ID.Size = new System.Drawing.Size(97, 20);
            this.CUSTOMER_ID.TabIndex = 5;
            this.CUSTOMER_ID.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.frmUsuario_KeyPress);
            // 
            // button7
            // 
            this.button7.BackColor = System.Drawing.SystemColors.Control;
            this.button7.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button7.Image = global::FE_FX.Properties.Resources.Eliminar_Linea;
            this.button7.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button7.Location = new System.Drawing.Point(169, 28);
            this.button7.Name = "button7";
            this.button7.Size = new System.Drawing.Size(83, 23);
            this.button7.TabIndex = 3;
            this.button7.Text = "Eliminar";
            this.button7.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.button7.UseVisualStyleBackColor = false;
            this.button7.Click += new System.EventHandler(this.button7_Click);
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.FileName = "openFileDialog1";
            // 
            // MENSAJE
            // 
            this.MENSAJE.Location = new System.Drawing.Point(66, 148);
            this.MENSAJE.Multiline = true;
            this.MENSAJE.Name = "MENSAJE";
            this.MENSAJE.Size = new System.Drawing.Size(545, 68);
            this.MENSAJE.TabIndex = 16;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(6, 151);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(47, 13);
            this.label1.TabIndex = 15;
            this.label1.Text = "Mensaje";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(63, 132);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(418, 13);
            this.label2.TabIndex = 14;
            this.label2.Text = "{FORMA_DE_PAGO},{CUENTA_BANCARIA},{BILL_TO_NAME},<<NUMFACTURA>>";
            // 
            // ARCHIVO
            // 
            this.ARCHIVO.Location = new System.Drawing.Point(66, 275);
            this.ARCHIVO.Name = "ARCHIVO";
            this.ARCHIVO.Size = new System.Drawing.Size(545, 20);
            this.ARCHIVO.TabIndex = 22;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(6, 278);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(43, 13);
            this.label4.TabIndex = 21;
            this.label4.Text = "Archivo";
            // 
            // LEYENDA_FISCAL
            // 
            this.LEYENDA_FISCAL.Location = new System.Drawing.Point(4, 338);
            this.LEYENDA_FISCAL.Multiline = true;
            this.LEYENDA_FISCAL.Name = "LEYENDA_FISCAL";
            this.LEYENDA_FISCAL.Size = new System.Drawing.Size(607, 122);
            this.LEYENDA_FISCAL.TabIndex = 25;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(6, 302);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(78, 13);
            this.label5.TabIndex = 23;
            this.label5.Text = "Leyenda Fiscal";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(6, 225);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(45, 13);
            this.label6.TabIndex = 17;
            this.label6.Text = "Formato";
            // 
            // FORMATO
            // 
            this.FORMATO.FormattingEnabled = true;
            this.FORMATO.Location = new System.Drawing.Point(66, 222);
            this.FORMATO.Name = "FORMATO";
            this.FORMATO.Size = new System.Drawing.Size(545, 21);
            this.FORMATO.TabIndex = 18;
            // 
            // CORREO
            // 
            this.CORREO.Location = new System.Drawing.Point(96, 249);
            this.CORREO.Name = "CORREO";
            this.CORREO.Size = new System.Drawing.Size(515, 20);
            this.CORREO.TabIndex = 20;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(6, 252);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(82, 13);
            this.label7.TabIndex = 19;
            this.label7.Text = "Correo de envio";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(21, 322);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(339, 13);
            this.label8.TabIndex = 24;
            this.label8.Text = "Si no tienen especificación, toma el valor y lo agrega a la ficha blanca.";
            // 
            // paisEntrega
            // 
            this.paisEntrega.Location = new System.Drawing.Point(243, 57);
            this.paisEntrega.Name = "paisEntrega";
            this.paisEntrega.Size = new System.Drawing.Size(117, 20);
            this.paisEntrega.TabIndex = 8;
            this.paisEntrega.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.frmUsuario_KeyPress);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(169, 60);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(68, 13);
            this.label9.TabIndex = 7;
            this.label9.Text = "País entrega";
            // 
            // iva0
            // 
            this.iva0.AutoSize = true;
            this.iva0.Location = new System.Drawing.Point(366, 58);
            this.iva0.Name = "iva0";
            this.iva0.Size = new System.Drawing.Size(71, 17);
            this.iva0.TabIndex = 9;
            this.iva0.Text = "IVA al 0%";
            this.iva0.UseVisualStyleBackColor = true;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(6, 60);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(51, 13);
            this.label10.TabIndex = 4;
            this.label10.Text = "Id Cliente";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(6, 469);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(70, 13);
            this.label11.TabIndex = 26;
            this.label11.Text = "Uso del CFDI";
            // 
            // USO_CFDI
            // 
            this.USO_CFDI.FormattingEnabled = true;
            this.USO_CFDI.Items.AddRange(new object[] {
            "Pago en una sola exhibición",
            "Pago en parcialidades",
            "Pago a credito"});
            this.USO_CFDI.Location = new System.Drawing.Point(80, 466);
            this.USO_CFDI.Name = "USO_CFDI";
            this.USO_CFDI.Size = new System.Drawing.Size(531, 21);
            this.USO_CFDI.TabIndex = 27;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(6, 86);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(145, 13);
            this.label12.TabIndex = 10;
            this.label12.Text = "ShipTo Separados por coma ";
            // 
            // ShipTo
            // 
            this.ShipTo.Location = new System.Drawing.Point(157, 83);
            this.ShipTo.Name = "ShipTo";
            this.ShipTo.Size = new System.Drawing.Size(454, 20);
            this.ShipTo.TabIndex = 11;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(6, 496);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(146, 13);
            this.label13.TabIndex = 28;
            this.label13.Text = " EU Advanced Manifest ICS2";
            // 
            // Europeo
            // 
            this.Europeo.Location = new System.Drawing.Point(157, 493);
            this.Europeo.Name = "Europeo";
            this.Europeo.Size = new System.Drawing.Size(454, 20);
            this.Europeo.TabIndex = 0;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(6, 522);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(207, 13);
            this.label14.TabIndex = 28;
            this.label14.Text = "Comercio Exterior Exportacion por Defecto";
            // 
            // Exportacion
            // 
            this.Exportacion.FormattingEnabled = true;
            this.Exportacion.Items.AddRange(new object[] {
            "Pago en una sola exhibición",
            "Pago en parcialidades",
            "Pago a credito"});
            this.Exportacion.Location = new System.Drawing.Point(219, 519);
            this.Exportacion.Name = "Exportacion";
            this.Exportacion.Size = new System.Drawing.Size(392, 21);
            this.Exportacion.TabIndex = 39;
            // 
            // frmCliente_Asunto
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(623, 555);
            this.Controls.Add(this.Exportacion);
            this.Controls.Add(this.ShipTo);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.USO_CFDI);
            this.Controls.Add(this.iva0);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.FORMATO);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label14);
            this.Controls.Add(this.label13);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.LEYENDA_FISCAL);
            this.Controls.Add(this.CORREO);
            this.Controls.Add(this.Europeo);
            this.Controls.Add(this.ARCHIVO);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.MENSAJE);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.button7);
            this.Controls.Add(this.paisEntrega);
            this.Controls.Add(this.CUSTOMER_ID);
            this.Controls.Add(this.ASUNTO);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.button3);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.ajax_loader);
            this.Controls.Add(this.statusStrip1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.KeyPreview = true;
            this.MinimizeBox = false;
            this.Name = "frmCliente_Asunto";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Asunto por Cliente";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frmUsuario_FormClosing);
            this.Load += new System.EventHandler(this.frmCliente_Asunto_Load);
            this.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.frmUsuario_KeyPress);
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ajax_loader)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel1;
        private System.Windows.Forms.PictureBox ajax_loader;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox ASUNTO;
        private System.Windows.Forms.TextBox CUSTOMER_ID;
        private System.Windows.Forms.Button button7;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private System.Windows.Forms.TextBox MENSAJE;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox ARCHIVO;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox LEYENDA_FISCAL;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.ComboBox FORMATO;
        private System.Windows.Forms.TextBox CORREO;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox paisEntrega;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.CheckBox iva0;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.ComboBox USO_CFDI;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.TextBox ShipTo;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.TextBox Europeo;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.ComboBox Exportacion;
    }
}