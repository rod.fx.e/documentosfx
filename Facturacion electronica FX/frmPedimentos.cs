﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;

using System.Text;
using System.Windows.Forms;
using Generales;

namespace FE_FX
{
    public partial class frmPedimentos : Form
    {
        private cCONEXCION oData = new cCONEXCION("");
        string devolver = "";
        string INVOICE_ID = "";
        cEMPRESA ocEMPRESA;
        public DataGridViewSelectedRowCollection selecionados;


        public frmPedimentos(string sConn,Object ocEMPRESAp, string devolverp = "")
        {
            ocEMPRESA = (cEMPRESA)ocEMPRESAp;
            oData.sConn = sConn;
            
            devolver = devolverp;
            InitializeComponent();
            ajax_loader.Visible = false;
            Columnas(dtgrdGeneral);
        }

        public frmPedimentos(string sConn, string INVOICE_IDp, Object ocEMPRESAp, string devolverp = "")
        {
            ocEMPRESA = (cEMPRESA)ocEMPRESAp;
            oData.sConn = sConn;
            devolver = devolverp;
            InitializeComponent();
            INVOICE_ID = INVOICE_IDp;
            this.Text = "Pedimentos de la factura: " + INVOICE_ID;
            ajax_loader.Visible = false;
            Columnas(dtgrdGeneral);
        }

        private void Columnas(DataGridView dtg)
        {
            int n = 0;

            n = dtg.Columns.Add("ROW_ID", "ROW_ID");
            dtg.Columns[n].ReadOnly = true;
            dtg.Columns[n].Visible = false;
            dtg.Columns[n].Width = 50;

            n = dtg.Columns.Add("INVOICE_ID", "Factura");
            dtg.Columns[n].ReadOnly = true;
            dtg.Columns[n].Visible = true;
            dtg.Columns[n].Width = 100;

            n = dtg.Columns.Add("LINE_NO", "Linea");
            dtg.Columns[n].ReadOnly = true;
            dtg.Columns[n].Visible = true;
            dtg.Columns[n].Width = 30;

            n = dtg.Columns.Add("PEDIMENTO", "Pedimento");
            dtg.Columns[n].ReadOnly = true;
            dtg.Columns[n].Visible = true;
            dtg.Columns[n].Width = 100;

            n = dtg.Columns.Add("ADUANA", "Aduana");
            dtg.Columns[n].ReadOnly = true;
            dtg.Columns[n].Visible = true;
            dtg.Columns[n].Width = 100;

            n = dtg.Columns.Add("FECHA", "Fecha");
            dtg.Columns[n].ReadOnly = true;
            dtg.Columns[n].Visible = true;
            dtg.Columns[n].Width = 100;

        }

        private void cargar()
        {
            ajax_loader.Visible = true;
            int n;
            cPEDIMENTO oObjeto = new cPEDIMENTO(oData, ocEMPRESA);
            toolStripStatusLabel1.Text = "Cargando ";
            dtgrdGeneral.Rows.Clear();
            foreach (cPEDIMENTO registro in oObjeto.todos(INVOICE_ID,BUSCAR.Text))
            {
                n = dtgrdGeneral.Rows.Add();
                dtgrdGeneral.Rows[n].Cells["ROW_ID"].Value = registro.ROW_ID;
                dtgrdGeneral.Rows[n].Cells["INVOICE_ID"].Value = registro.INVOICE_ID;
                dtgrdGeneral.Rows[n].Cells["LINE_NO"].Value = registro.LINE_NO;
                dtgrdGeneral.Rows[n].Cells["PEDIMENTO"].Value = registro.PEDIMENTO;
                dtgrdGeneral.Rows[n].Cells["ADUANA"].Value = registro.ADUANA;
                dtgrdGeneral.Rows[n].Cells["FECHA"].Value = registro.FECHA;
            }
            toolStripStatusLabel1.Text = "Total " + dtgrdGeneral.Rows.Count.ToString();
            ajax_loader.Visible = false;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            cargar();
        }

        private void BUSCAR_KeyPress(object sender, KeyPressEventArgs e)
        {
            //Precionar Enter buscar
            if (e.KeyChar.ToString() == "\r")
            {
                cargar();
            }
            
        }

        private void frmUsuarios_Load(object sender, EventArgs e)
        {
            cargar();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            frmPedimento oObjeto = new frmPedimento(oData.sConn,INVOICE_ID,"",ocEMPRESA);
            oObjeto.ShowDialog();
            cargar();
        }

        private void dtgrdGeneral_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            modificar();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            if (dtgrdGeneral.SelectedRows.Count > 0)
            {
                DataGridViewSelectedRowCollection arrSelectedRows = dtgrdGeneral.SelectedRows;
                string ROW_ID = (string)arrSelectedRows[0].Cells[0].Value;
                frmPedimento oObjeto = new frmPedimento(oData.sConn, ROW_ID,ocEMPRESA);
                oObjeto.ShowDialog();
                cargar();
            }
        }

        public void modificar()
        {
            if (dtgrdGeneral.SelectedRows.Count > 0)
            {
                DataGridViewSelectedRowCollection arrSelectedRows = dtgrdGeneral.SelectedRows;
                if (devolver == "")
                {
                    
                    string ROW_ID = (string)arrSelectedRows[0].Cells[0].Value;
                    frmPedimento oObjeto = new frmPedimento(oData.sConn, ROW_ID,ocEMPRESA);
                    oObjeto.ShowDialog();
                    cargar();
                }
                else
                {
                    selecionados = arrSelectedRows;
                    this.DialogResult = DialogResult.OK;
                }
            }
        }

        private void frmFormatos_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)27)
            {
                this.Close();
            }
            else
            {
                if ((e.KeyChar.ToString() == "<")
                                | (e.KeyChar.ToString() == ">")
                                
                                | (e.KeyChar.ToString() == "'")
                                | (e.KeyChar.ToString() == "#")
                                )
                {
                    e.Handled = true;
                }
            }
        }
    }
}
