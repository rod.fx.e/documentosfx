﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.Entity.Validation;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Windows.Forms;
using Generales;

namespace FE_FX
{
    public partial class frmMetodo_Cuenta : Form
    {
        private cCONEXCION oData = new cCONEXCION("");
        private bool modificado;
        string CUSTOMER_ID = "";
        cFACTURA_BANDEJA ocFACTURA_BANDEJA;
        cEMPRESA ocEMPRESA;
        public frmMetodo_Cuenta(string sConn,string sTipo, string INVOICE_IDp, cEMPRESA ocEMPRESAp, Object oDatap,string CUSTOMER_ID)
        {
            oData = (cCONEXCION)oDatap;
            //MessageBox.Show("Ingrese");
            
            ocEMPRESA= ocEMPRESAp;
            
            modificado = false;
            oData.sConn = sConn;
            oData.sTipo = sTipo;
            ocFACTURA_BANDEJA = new cFACTURA_BANDEJA(oData, ocEMPRESA);
            ocFACTURA_BANDEJA.INVOICE_ID = INVOICE_IDp;
            InitializeComponent();
            cargarCombos();
            CargarCartaPorteCatalogos();
            cargar();
            CargarInformacionGlobal();
        }
        private void GuardarInformacionGlobal()
        {
            try
            {
                ModeloDocumentosFX.ModeloDocumentosFXContainer Db = new ModeloDocumentosFX.ModeloDocumentosFXContainer();
                ModeloDocumentosFX.InformacionGlobal OInformacionGlobal = Db.InformacionGlobalSet.Where(a => a.INVOICE_ID.Equals(ocFACTURA_BANDEJA.INVOICE_ID))
                    .FirstOrDefault();
                if (OInformacionGlobal == null)
                {
                    OInformacionGlobal = new ModeloDocumentosFX.InformacionGlobal();
                    OInformacionGlobal.INVOICE_ID = ocFACTURA_BANDEJA.INVOICE_ID;
                    OInformacionGlobal.InformacionGlobalAnio = InformacionGlobalAnio.Text;

                    string InformacionGlobalMesValor = ((ComboItem)this.InformacionGlobalMes.SelectedItem).Value.ToString();
                    OInformacionGlobal.InformacionGlobalMes = InformacionGlobalMesValor;
                    string InformacionGlobalPeriodicidadValor = ((ComboItem)this.InformacionGlobalPeriodicidad.SelectedItem).Value.ToString();
                    OInformacionGlobal.InformacionGlobalPeriodicidad = InformacionGlobalPeriodicidadValor;

                    Db.InformacionGlobalSet.Add(OInformacionGlobal);
                }
                else
                {
                    string InformacionGlobalMesValor = ((ComboItem)this.InformacionGlobalMes.SelectedItem).Value.ToString();
                    OInformacionGlobal.InformacionGlobalMes = InformacionGlobalMesValor;
                    string InformacionGlobalPeriodicidadValor = ((ComboItem)this.InformacionGlobalPeriodicidad.SelectedItem).Value.ToString();
                    OInformacionGlobal.InformacionGlobalPeriodicidad = InformacionGlobalPeriodicidadValor;

                    Db.InformacionGlobalSet.Attach(OInformacionGlobal);
                    Db.Entry(OInformacionGlobal).State = System.Data.Entity.EntityState.Modified;
                }
                Db.SaveChanges();

                Db.Dispose();
            }
            catch (DbEntityValidationException error)
            {
                foreach (var eve in error.EntityValidationErrors)
                {
                    foreach (var ve in eve.ValidationErrors)
                    {
                        MessageBox.Show(this, ve.PropertyName + " Error: " + ve.ErrorMessage, Application.ProductName
                            , MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
            }
            catch (Exception error)
            {

                string MensajeTr = error.Message;
                if (error.InnerException!=null)
                {
                    MensajeTr += Environment.NewLine + error.InnerException;
                }

                MessageBox.Show(this, MensajeTr, Application.ProductName
                            , MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        private void CargarInformacionGlobal()
        {
            InformacionGlobalAnio.Text = DateTime.Now.Year.ToString();
            CultureInfo culture = new CultureInfo("es-ES");
            InformacionGlobalMes.Items.Clear();
            string[] meses = culture.DateTimeFormat.MonthNames;
            for (int i = 0; i < meses.Length; i++)
            {
                if (!String.IsNullOrEmpty(meses[i]))
                {
                    InformacionGlobalMes.Items.Add(new ComboItem() { Name = meses[i], Value = (i + 1).ToString("D2") });
                }
            }
            InformacionGlobalMes.SelectedIndex = DateTime.Now.Month - 1;

            InformacionGlobalPeriodicidad.Items.Clear();
            List<ComboItem> periodicidadItems = new List<ComboItem>()
            {
                new ComboItem() { Name = "Diario", Value = "01" },
                new ComboItem() { Name = "Semanal", Value = "02" },
                new ComboItem() { Name = "Quincenal", Value = "03" },
                new ComboItem() { Name = "Mensual", Value = "04" },
                new ComboItem() { Name = "Bimestral", Value = "05" }
            };
            InformacionGlobalPeriodicidad.DisplayMember = "Name";
            InformacionGlobalPeriodicidad.ValueMember = "Value";
            InformacionGlobalPeriodicidad.DataSource = periodicidadItems;            
            InformacionGlobalPeriodicidad.SelectedIndex = 0;

            try
            {
                ModeloDocumentosFX.ModeloDocumentosFXContainer Db = new ModeloDocumentosFX.ModeloDocumentosFXContainer();
                ModeloDocumentosFX.InformacionGlobal OInformacionGlobal = Db.InformacionGlobalSet.Where(a => a.INVOICE_ID.Equals(ocFACTURA_BANDEJA.INVOICE_ID))
                    .FirstOrDefault();
                if (OInformacionGlobal!=null)
                {
                    InformacionGlobalAnio.Text = OInformacionGlobal.InformacionGlobalAnio;

                    foreach (ComboItem cbi in this.InformacionGlobalPeriodicidad.Items)
                    {
                        if (cbi.Value.Equals(OInformacionGlobal.InformacionGlobalPeriodicidad.ToString()))
                        {
                            InformacionGlobalPeriodicidad.SelectedItem = cbi;
                            break;
                        }
                    }

                    foreach (ComboItem cbi in this.InformacionGlobalMes.Items)
                    {
                        if (cbi.Value.Equals(OInformacionGlobal.InformacionGlobalMes.ToString()))
                        {
                            InformacionGlobalMes.SelectedItem = cbi;
                            break;
                        }
                    }
                }                
                Db.Dispose();
            }
            catch (DbEntityValidationException error)
            {
                foreach (var eve in error.EntityValidationErrors)
                {
                    foreach (var ve in eve.ValidationErrors)
                    {
                        MessageBox.Show(this, ve.PropertyName + " Error: " + ve.ErrorMessage, Application.ProductName
                            , MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
            }
            catch (Exception error)
            {
                string MensajeTr = error.Message;
                if (error.InnerException != null)
                {
                    MensajeTr += Environment.NewLine + error.InnerException;
                }

                MessageBox.Show(this, MensajeTr, Application.ProductName
                            , MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void CargarCartaPorteCatalogos()
        {
            try
            {
                ModeloCartaPorte.CartaPorteEntities Db = new ModeloCartaPorte.CartaPorteEntities();
                List<ComboItem> Lista = Db.TransporteSet
                                             .OrderBy(a => a.Placa)
                                             .ToList()
                                             .Select(                    
                                             a => new ComboItem { Name = a.PlacaRemolque + " " + a.Placa  , Value = a.Id.ToString() }
                                             )
                          .ToList();
                Transporte.Items.Clear();
                foreach (ComboItem registro in Lista)
                {
                    if (registro != null)
                    {
                        Transporte.Items.Add(registro);
                    }
                }

                Transporte.SelectedIndex = -1;
                Db.Dispose();
            }
            catch (DbEntityValidationException error)
            {
                foreach (var eve in error.EntityValidationErrors)
                {
                    foreach (var ve in eve.ValidationErrors)
                    {
                        MessageBox.Show(this, ve.PropertyName + " Error: " + ve.ErrorMessage, Application.ProductName
                            , MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
            }

            try
            {
                ModeloCartaPorte.CartaPorteEntities Db = new ModeloCartaPorte.CartaPorteEntities();
                List<ComboItem> Lista = Db.TransportistaSet
                                             .OrderBy(a => a.Nombre)
                                             .ToList()
                                             .Select(
                                             a => new ComboItem { Name = a.Nombre, Value = a.Id.ToString() }
                                             )
                          .ToList();
                Transportista.Items.Clear();
                foreach (ComboItem registro in Lista)
                {
                    if (registro != null)
                    {
                        Transportista.Items.Add(registro);
                    }
                }
                Transportista.SelectedIndex = -1;

                Db.Dispose();
            }
            catch (DbEntityValidationException error)
            {
                foreach (var eve in error.EntityValidationErrors)
                {
                    foreach (var ve in eve.ValidationErrors)
                    {
                        MessageBox.Show(this, ve.PropertyName + " Error: " + ve.ErrorMessage, Application.ProductName
                            , MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
            }


        }

        private void cargarCombos()
        {
            USO_CFDI.Items.Clear();
            USO_CFDI.Items.Add(new ComboItem { Name = "Adquisición de mercancias", Value = "G01" });
            USO_CFDI.Items.Add(new ComboItem { Name = "Devoluciones, descuentos o bonificaciones", Value = "G02" });
            USO_CFDI.Items.Add(new ComboItem { Name = "Gastos en general", Value = "G03" });
            USO_CFDI.Items.Add(new ComboItem { Name = "Construcciones", Value = "I01" });
            USO_CFDI.Items.Add(new ComboItem { Name = "Mobilario y equipo de oficina por inversiones", Value = "I02" });
            USO_CFDI.Items.Add(new ComboItem { Name = "Equipo de transporte", Value = "I03" });
            USO_CFDI.Items.Add(new ComboItem { Name = "Equipo de computo y accesorios", Value = "I04" });
            USO_CFDI.Items.Add(new ComboItem { Name = "Dados, troqueles, moldes, matrices y herramental", Value = "I05" });
            USO_CFDI.Items.Add(new ComboItem { Name = "Comunicaciones telefónicas", Value = "I06" });
            USO_CFDI.Items.Add(new ComboItem { Name = "Comunicaciones satelitales", Value = "I07" });
            USO_CFDI.Items.Add(new ComboItem { Name = "Otra maquinaria y equipo", Value = "I08" });
            USO_CFDI.Items.Add(new ComboItem { Name = "Honorarios médicos, dentales y gastos hospitalarios.", Value = "D01" });
            USO_CFDI.Items.Add(new ComboItem { Name = "Gastos médicos por incapacidad o discapacidad", Value = "D02" });
            USO_CFDI.Items.Add(new ComboItem { Name = "Gastos funerales.", Value = "D03" });
            USO_CFDI.Items.Add(new ComboItem { Name = "Donativos.", Value = "D04" });
            USO_CFDI.Items.Add(new ComboItem { Name = "Intereses reales efectivamente pagados por créditos hipotecarios (casa habitación).", Value = "D05" });
            USO_CFDI.Items.Add(new ComboItem { Name = "Aportaciones voluntarias al SAR.", Value = "D06" });
            USO_CFDI.Items.Add(new ComboItem { Name = "Primas por seguros de gastos médicos.", Value = "D07" });
            USO_CFDI.Items.Add(new ComboItem { Name = "Gastos de transportación escolar obligatoria.", Value = "D08" });
            USO_CFDI.Items.Add(new ComboItem { Name = "Depósitos en cuentas para el ahorro, primas que tengan como base planes de pensiones.", Value = "D09" });
            USO_CFDI.Items.Add(new ComboItem { Name = "Pagos por servicios educativos (colegiaturas)", Value = "D10" });
            USO_CFDI.Items.Add(new ComboItem { Name = "Sin efectos fiscales", Value = "S01" });
            ComboItem oComboItem = new ComboItem { Name = "Por definir", Value = "P01" };
            USO_CFDI.Items.Add(oComboItem);
            USO_CFDI.SelectedItem = oComboItem;

            METODO_DE_PAGO.Items.Clear();
            METODO_DE_PAGO.Items.Add(new ComboItem { Name = "Pago en una sola exhibición", Value = "PUE" });
            ComboItem oComboItemMETODO_DE_PAGO = new ComboItem { Name = "Pago en parcialidades o diferido", Value = "PPD" };
            METODO_DE_PAGO.Items.Add(oComboItemMETODO_DE_PAGO);
            METODO_DE_PAGO.SelectedItem = oComboItemMETODO_DE_PAGO;

            FORMA_DE_PAGO.Items.Clear();
            FORMA_DE_PAGO.Items.Add(new ComboItem { Name = "Efectivo", Value = "01" });
            FORMA_DE_PAGO.Items.Add(new ComboItem { Name = "Cheque nominativo", Value = "02" });
            FORMA_DE_PAGO.Items.Add(new ComboItem { Name = "Transferencia electrónica de fondos", Value = "03" });
            FORMA_DE_PAGO.Items.Add(new ComboItem { Name = "Tarjeta de crédito", Value = "04" });
            FORMA_DE_PAGO.Items.Add(new ComboItem { Name = "Monedero electrónico", Value = "05" });
            FORMA_DE_PAGO.Items.Add(new ComboItem { Name = "Dinero electrónico", Value = "06" });
            FORMA_DE_PAGO.Items.Add(new ComboItem { Name = "Vales de despensa", Value = "08" });
            FORMA_DE_PAGO.Items.Add(new ComboItem { Name = "Dación en pago", Value = "12" });
            FORMA_DE_PAGO.Items.Add(new ComboItem { Name = "Pago por subrogación", Value = "13" });
            FORMA_DE_PAGO.Items.Add(new ComboItem { Name = "Pago por consignación", Value = "14" });
            FORMA_DE_PAGO.Items.Add(new ComboItem { Name = "Condonación", Value = "15" });
            FORMA_DE_PAGO.Items.Add(new ComboItem { Name = "Compensación", Value = "17" });
            FORMA_DE_PAGO.Items.Add(new ComboItem { Name = "Novación", Value = "23" });
            FORMA_DE_PAGO.Items.Add(new ComboItem { Name = "Confusión", Value = "24" });
            FORMA_DE_PAGO.Items.Add(new ComboItem { Name = "Remisión de deuda", Value = "25" });
            FORMA_DE_PAGO.Items.Add(new ComboItem { Name = "Prescripción o caducidad", Value = "26" });
            FORMA_DE_PAGO.Items.Add(new ComboItem { Name = "A satisfacción del acreedor", Value = "27" });
            FORMA_DE_PAGO.Items.Add(new ComboItem { Name = "Tarjeta de débito", Value = "28" });
            FORMA_DE_PAGO.Items.Add(new ComboItem { Name = "Tarjeta de servicios", Value = "29" });
            FORMA_DE_PAGO.Items.Add(new ComboItem { Name = "Aplicación de anticipos", Value = "30" });
            ComboItem oComboItemFORMA_DE_PAGO = new ComboItem { Name = "Por definir", Value = "99" };
            FORMA_DE_PAGO.Items.Add(oComboItemFORMA_DE_PAGO);
            FORMA_DE_PAGO.SelectedItem = oComboItemFORMA_DE_PAGO;


            Exportacion.Items.Clear();
            Exportacion.Items.Add(new ComboItem { Name = "No aplica", Value = "01" });
            Exportacion.Items.Add(new ComboItem { Name = "Definitiva", Value = "02" });
            Exportacion.Items.Add(new ComboItem { Name = "Temporal", Value = "03" });
            Exportacion.Items.Add(new ComboItem { Name = "Definitiva con clave distinta a A1 o cuando no existe enajenación en términos del CFF", Value = "04" });
            



        }

        private bool guardar()
        {
            //ajax_loader.Visible = true;
            //string metodoDePago = METODO_DE_PAGO.Text.Trim();
            //if (metodoDePago != "NA")
            //{
            //    metodoDePago = Regex.Replace(METODO_DE_PAGO.Text, "[^0-9,]", "");
            //}


            ocFACTURA_BANDEJA.METODO_DE_PAGO = ((ComboItem)METODO_DE_PAGO.SelectedItem).Value.ToString();
            ocFACTURA_BANDEJA.CTA_BANCO = CTA_BANCO.Text;
            ocFACTURA_BANDEJA.FORMA_DE_PAGO = ((ComboItem)FORMA_DE_PAGO.SelectedItem).Value.ToString();
            ocFACTURA_BANDEJA.USO_CFDI = ((ComboItem)USO_CFDI.SelectedItem).Value.ToString();
            ocFACTURA_BANDEJA.subdivision = subdivision.Text;
            ocFACTURA_BANDEJA.RegimenFiscal = RegimenFiscal.Text;
            try
            {
                ocFACTURA_BANDEJA.Exportacion = ((ComboItem)Exportacion.SelectedItem).Value.ToString();
            }
            catch
            {

            }
            //if (ocFACTURA_BANDEJA.METODO_DE_PAGO.Trim().Length==0)
            //{
            //    ajax_loader.Visible = false;
            //    MessageBox.Show("El metodo de pago " + ocFACTURA_BANDEJA.METODO_DE_PAGO.Trim() + " no es valido.", Application.ProductName + "-" + Application.ProductVersion, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            //    return false;
            //}

            //if(!cUtilidades.validarMetodopago(ocFACTURA_BANDEJA.METODO_DE_PAGO))
            //{
            //    ajax_loader.Visible = false;
            //    ocFACTURA_BANDEJA.METODO_DE_PAGO = "";
            //    METODO_DE_PAGO.Text = "";
            //    MessageBox.Show("El metodo de pago " + ocFACTURA_BANDEJA.METODO_DE_PAGO.Trim() + " no es valido.", Application.ProductName + "-" + Application.ProductVersion, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            //    return false;
            //}

            //Guadar datos de Carta porte
            GuardarCartaPorte();
            GuardarInformacionGlobal();
            if (ocFACTURA_BANDEJA.guardar_metodo())
            {
                ajax_loader.Visible = false;
                toolStripStatusLabel1.Text = "Datos Actualizados " + DateTime.Now.ToShortDateString() + " " + DateTime.Now.ToLongTimeString();
                modificado = false;
            }
            return true;
        }

        private void GuardarCartaPorte()
        {
            try
            {
                ModeloCartaPorte.CartaPorteEntities Db = new ModeloCartaPorte.CartaPorteEntities();
                ModeloCartaPorte.CartaPorte OCartaPorte = Db.CartaPorteSet
                    .Where(a => a.InvoiceId.Equals(ocFACTURA_BANDEJA.INVOICE_ID))
                    .FirstOrDefault();

                if ((Transporte.SelectedIndex != -1)
                    & (Transportista.SelectedIndex != -1))
                {

                    if (OCartaPorte == null)
                    {
                        //Guardar
                        OCartaPorte = new ModeloCartaPorte.CartaPorte();
                        OCartaPorte.InvoiceId = ocFACTURA_BANDEJA.INVOICE_ID;
                        OCartaPorte.FechaCreacion = DateTime.Now;
                        OCartaPorte.Usuario = String.IsNullOrEmpty(Generales.Globales.usuario.usuarioVISUAL) ? Globales.usuario.usuario
                            : Generales.Globales.usuario.usuarioVISUAL;
                        //Buscar el transporte
                        string IdTransporte = ((ComboItem)Transporte.SelectedItem).Value.ToString();
                        ModeloCartaPorte.Transporte OTransporte = Db.TransporteSet
                                                 .Where(a => a.Id.ToString().Equals(IdTransporte)).FirstOrDefault();
                        OCartaPorte.Transporte = OTransporte;
                        //Buscar el transportista
                        string IdTransportista = ((ComboItem)Transportista.SelectedItem).Value.ToString();
                        ModeloCartaPorte.Transportista OTransportista = Db.TransportistaSet
                                                 .Where(a => a.Id.ToString().Equals(IdTransportista)).FirstOrDefault();
                        OCartaPorte.Transportista = OTransportista;
                        OCartaPorte.TotalDistRec = TotalDistRec.Text;
                        OCartaPorte.RFCRemitenteDestinatario = RFCRemitenteDestinatario.Text;
                        OCartaPorte.NombreRemitenteDestinatario = NombreRemitenteDestinatario.Text;
                        Db.CartaPorteSet.Add(OCartaPorte);
                    }
                    else
                    {
                        //Buscar el transporte
                        string IdTransporte = ((ComboItem)Transporte.SelectedItem).Value.ToString();
                        ModeloCartaPorte.Transporte OTransporte = Db.TransporteSet
                                                 .Where(a => a.Id.ToString().Equals(IdTransporte)).FirstOrDefault();
                        OCartaPorte.Transporte = OTransporte;
                        //Buscar el transportista
                        string IdTransportista = ((ComboItem)Transportista.SelectedItem).Value.ToString();
                        ModeloCartaPorte.Transportista OTransportista = Db.TransportistaSet
                                                 .Where(a => a.Id.ToString().Equals(IdTransportista)).FirstOrDefault();
                        OCartaPorte.Transportista = OTransportista;
                        OCartaPorte.TotalDistRec = TotalDistRec.Text;
                        OCartaPorte.RFCRemitenteDestinatario = RFCRemitenteDestinatario.Text;
                        OCartaPorte.NombreRemitenteDestinatario = NombreRemitenteDestinatario.Text;
                        //Modificar
                        Db.CartaPorteSet.Attach(OCartaPorte);
                        Db.Entry(OCartaPorte).State = System.Data.Entity.EntityState.Modified;
                    }
                    Db.SaveChanges();
                }
                else
                {
                    if (OCartaPorte != null)
                    {
                        //Eliminar
                        Db.CartaPorteSet.Attach(OCartaPorte);
                        Db.CartaPorteSet.Remove(OCartaPorte);
                        Db.SaveChanges();
                    }
                }
            }
            catch (Exception err)
            {
                ErrorFX.mostrar(err, false, true, "Guardar CartaPorte - 3816 - ", false);
            }
        }

        private void cargar()
        {
            ajax_loader.Visible = true;
            string BD_Auxiliartr = "";
            if (ocEMPRESA.BD_AUXILIAR != "")
            {
                BD_Auxiliartr = ocEMPRESA.BD_AUXILIAR + ".dbo.";
            }
            //Cargar la configuración de VMX_FE si no existe cargar la ultima
            string sSQL = @"
            SELECT TOP 1 v.*
            FROM "+ BD_Auxiliartr + @"VMX_FE v
            WHERE v.INVOICE_ID COLLATE SQL_Latin1_General_CP1_CI_AS='" + ocFACTURA_BANDEJA.INVOICE_ID + @"'
            ORDER BY ROW_ID DESC 
            ";
            DataTable oDataTable = oData.EjecutarConsulta(sSQL);
            if (oDataTable != null)
            {
                if (oDataTable.Rows.Count != 0)
                {
                    foreach (DataRow registro in oDataTable.Rows)
                    {
                        METODO_DE_PAGO.SelectedItem = METODO_DE_PAGO.Items.Equals(registro["METODO_DE_PAGO"].ToString());
                        CTA_BANCO.Text = registro["CTA_BANCO"].ToString();
                        FORMA_DE_PAGO.SelectedValue = METODO_DE_PAGO.Items.Equals(registro["FORMA_DE_PAGO"].ToString()); 
                        USO_CFDI.SelectedValue = USO_CFDI.Items.Equals(registro["USO_CFDI"].ToString());
                        RegimenFiscal.Text = registro["RegimenFiscal"].ToString();

                        try
                        {
                            foreach (ComboItem cbi in this.Exportacion.Items)
                            {
                                if (cbi.Value.Equals(registro["Exportacion"].ToString()))
                                {
                                    Exportacion.SelectedItem = cbi;
                                    break;
                                }
                            }

                        }
                        catch
                        {

                        }
                        ajax_loader.Visible = false;
                        break;
                    }
                }
                else
                {
                    cargarUltimaCliente();
                }

            }

            toolStripStatusLabel1.Text  = "Datos Cargados " + DateTime.Now.ToShortDateString() + " " + DateTime.Now.ToLongTimeString();
            modificado = false;

            CargarCartaPorte();

            ajax_loader.Visible = false;
        }

        private void CargarCartaPorte()
        {
            try
            {
                ModeloCartaPorte.CartaPorteEntities Db = new ModeloCartaPorte.CartaPorteEntities();
                ModeloCartaPorte.CartaPorte OCartaPorte = Db.CartaPorteSet
                    .Where(a => a.InvoiceId.Equals(ocFACTURA_BANDEJA.INVOICE_ID))
                    .FirstOrDefault();
                if (OCartaPorte != null)
                {
                    foreach (ComboItem cbi in Transportista.Items)
                    {
                        if (cbi.Value.Equals(OCartaPorte.Transportista.Id.ToString()))
                        {
                            Transportista.SelectedItem = cbi;
                            break;
                        }
                    }

                    foreach (ComboItem cbi in Transporte.Items)
                    {
                        if (cbi.Value.Equals(OCartaPorte.Transporte.Id.ToString()))
                        {
                            Transporte.SelectedItem = cbi;
                            break;
                        }
                    }

                    TotalDistRec.Text = OCartaPorte.TotalDistRec;
                    RFCRemitenteDestinatario.Text = OCartaPorte.RFCRemitenteDestinatario;
                    NombreRemitenteDestinatario.Text = OCartaPorte.NombreRemitenteDestinatario;
                }
                Db.Dispose();
            }
            catch (DbEntityValidationException error)
            {
                foreach (var eve in error.EntityValidationErrors)
                {
                    foreach (var ve in eve.ValidationErrors)
                    {
                        MessageBox.Show(this, ve.PropertyName + " Error: " + ve.ErrorMessage, Application.ProductName
                            , MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
            }
            catch (Exception error)
            {

                string MensajeTr = error.Message;
                if (error.InnerException != null)
                {
                    MensajeTr += Environment.NewLine + error.InnerException;
                }

                MessageBox.Show(this, MensajeTr, Application.ProductName
                            , MessageBoxButtons.OK, MessageBoxIcon.Error);

            }
        }

        private void cargarUltimaCliente()
        {
            string BD_Auxiliartr = "";
            if (ocEMPRESA.BD_AUXILIAR != "")
            {
                BD_Auxiliartr = ocEMPRESA.BD_AUXILIAR + ".dbo.";
            }

            //Cargar la ultima configuración guarda en VMX_FE
            string sSQL = @"
            SELECT TOP 1 v.*
            FROM " + BD_Auxiliartr + @"VMX_FE v
            INNER JOIN RECEIVABLE r ON r.INVOICE_ID  COLLATE SQL_Latin1_General_CP1_CI_AS=v.INVOICE_ID
            WHERE r.CUSTOMER_ID='" + CUSTOMER_ID + @"'
            ORDER BY ROW_ID DESC 
            ";

            DataTable oDataTable = oData.EjecutarConsulta(sSQL);
            if (oDataTable != null)
            {
                foreach (DataRow registro in oDataTable.Rows)
                {
                    METODO_DE_PAGO.SelectedItem = METODO_DE_PAGO.Items.Equals(registro["METODO_DE_PAGO"].ToString());
                    CTA_BANCO.Text = registro["CTA_BANCO"].ToString();
                    FORMA_DE_PAGO.SelectedValue = METODO_DE_PAGO.Items.Equals(registro["FORMA_DE_PAGO"].ToString());
                    USO_CFDI.SelectedValue = METODO_DE_PAGO.Items.Equals(registro["USO_CFDI"].ToString());
                    RegimenFiscal.Text = registro["RegimenFiscal"].ToString();
                    ajax_loader.Visible = false;
                }

            }
            //MessageBox.Show("Cargando");
        }

        private void button3_Click(object sender, EventArgs e)
        {
            if(guardar())
            {
                this.Close();
            }
            
        }


        private void frmUsuario_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (modificado)
            {
                DialogResult Resultado = MessageBox.Show("¿Desea guardar los cambios?", Application.ProductName + "-" + Application.ProductVersion, MessageBoxButtons.YesNo);
                if (Resultado.ToString() == "Yes")
                {
                    guardar();
                }
            }
        }


    }
}
