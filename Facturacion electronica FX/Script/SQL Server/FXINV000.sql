DROP TABLE [dbo].[FXINV000] 
GO
CREATE TABLE [dbo].[FXINV000](
	[ROW_ID] [bigint] IDENTITY(1,1) NOT NULL,
	[ID] [varchar](50) NULL,
	[XML] [varchar](500) NULL,
	[STATUS] [varchar](500) NULL,
	[SELLO] [varchar](500) NULL,
	[CADENA] [varchar](500) NULL,
	[TIMBRE] [varchar](500) NULL,
	[CREATE_DATE] [datetime] NULL,
	[ARCHIVO_DESTINO] [varchar](500) NULL,
	[PDF]  [varchar](500) NULL,
 CONSTRAINT [PK_FXINV000] PRIMARY KEY CLUSTERED 
(
	[ROW_ID] ASC
)
ON [PRIMARY]
) ON [PRIMARY]

GO
