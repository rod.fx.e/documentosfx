ALTER TABLE EMPRESA ADD ErrorCorreo varchar(max) DEFAULT 'False';
ALTER TABLE [dbo].[pagoSet] ADD [version] nvarchar(max)  NOT NULL DEFAULT('3.3');
ALTER TABLE [dbo].CLIENTE_CONFIGURACION ADD [Europeo] nvarchar(max);
ALTER TABLE [dbo].[SERIE] ADD
	[CLIENTE_TRASLADO] [varchar](max) NULL,
	[UnirCarpeta] [varchar](max) NULL;