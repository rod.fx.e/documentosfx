ALTER TABLE VMX_FE
ADD UUID varchar(max)
ALTER TABLE VMX_FE
ADD FechaTimbrado varchar(max)
ALTER TABLE VMX_FE
ADD noCertificadoSAT  varchar(max)
ALTER TABLE VMX_FE
ADD selloSAT  varchar(max)
ALTER TABLE VMX_FE
ADD Cadena_TFD  varchar(max)
ALTER TABLE VMX_FE
ADD QR_Code  varchar(max)
ALTER TABLE VMX_FE
ADD IMAGEN_QR_Code IMAGE
ALTER TABLE VMX_FE
ADD CADENA varchar(max)
ALTER TABLE VMX_FE
ALTER COLUMN SELLO varchar(max)
ALTER TABLE VMX_FE
ADD ESTADO varchar(max)
ALTER TABLE VMX_FE
ADD CANCELADO varchar(max)
ALTER TABLE VMX_FE
ADD noCertificado  varchar(max)
ALTER TABLE VMX_FE
ADD ADDR_NO  varchar(max)
ALTER TABLE VMX_FE
ADD PEDIMENTOS  varchar(max)
ALTER TABLE VMX_FE
ADD PDF_ESTADO  varchar(max)
---Oracle
ALTER TABLE SYSADM.FXINV000 ADD ESTADO VARCHAR2(40);
ALTER TABLE SYSADM.FXINV000 ADD CANCELADO VARCHAR2(400);
ALTER TABLE SYSADM.FXINV000 ADD noCertificado VARCHAR2(400);
ALTER TABLE SYSADM.FXINV000 ADD ADDR_NO VARCHAR2(400);
ALTER TABLE SYSADM.FXINV000 ADD PEDIMENTOS VARCHAR2(400);
ALTER TABLE SYSADM.FXINV000 ADD PDF_ESTADO VARCHAR2(400);
COMMIT;

ALTER TABLE SYSADM.FXINV000 ADD ESTADO VARCHAR2(400);
COMMIT;
----AGREGAR EN LA VISTA

                            ,(SELECT     TOP (1) UUID
                            FROM          dbo.VMX_FE AS VMX_FE_2
                            WHERE      (INVOICE_ID = dbo.VMX_FE_VISTA2.INVOICE_ID)) AS UUID
                            ,(SELECT     TOP (1) FechaTimbrado
                            FROM          dbo.VMX_FE AS VMX_FE_2
                            WHERE      (INVOICE_ID = dbo.VMX_FE_VISTA2.INVOICE_ID)) AS FechaTimbrado
                            ,(SELECT     TOP (1) noCertificadoSAT
                            FROM          dbo.VMX_FE AS VMX_FE_2
                            WHERE      (INVOICE_ID = dbo.VMX_FE_VISTA2.INVOICE_ID)) AS noCertificadoSAT
                            ,(SELECT     TOP (1) selloSAT
                            FROM          dbo.VMX_FE AS VMX_FE_2
                            WHERE      (INVOICE_ID = dbo.VMX_FE_VISTA2.INVOICE_ID)) AS selloSAT
                            ,(SELECT     TOP (1) Cadena_TFD
                            FROM          dbo.VMX_FE AS VMX_FE_2
                            WHERE      (INVOICE_ID = dbo.VMX_FE_VISTA2.INVOICE_ID)) AS Cadena_TFD
                            ,(SELECT     TOP (1) QR_Code
                            FROM          dbo.VMX_FE AS VMX_FE_2
                            WHERE      (INVOICE_ID = dbo.VMX_FE_VISTA2.INVOICE_ID)) AS QR_Code
                            ,(SELECT     TOP (1) IMAGEN_QR_Code
                            FROM          dbo.VMX_FE AS VMX_FE_2
                            WHERE      (INVOICE_ID = dbo.VMX_FE_VISTA2.INVOICE_ID)) AS IMAGEN_QR_Code
                            ,(SELECT     TOP (1) CADENA
                            FROM          dbo.VMX_FE AS VMX_FE_2
                            WHERE      (INVOICE_ID = dbo.VMX_FE_VISTA2.INVOICE_ID)) AS CADENA
                            ,(SELECT     TOP (1) noCertificado
                            FROM          dbo.VMX_FE AS VMX_FE_2
                            WHERE      (INVOICE_ID = dbo.VMX_FE_VISTA2.INVOICE_ID)) AS noCertificado
							,(SELECT     TOP (1) ADDR_NO
                            FROM          dbo.VMX_FE AS VMX_FE_2
                            WHERE      (INVOICE_ID = dbo.VMX_FE_VISTA2.INVOICE_ID)) AS ADDR_NO
							,(SELECT     TOP (1) [dbo].[FEFX_PEDIMENTOS_fn](dbo.VMX_FE_VISTA2.INVOICE_ID, dbo.VMX_FE_VISTA2.LINE_NO)
                            FROM          dbo.VMX_FE AS VMX_FE_2
                            WHERE      (INVOICE_ID = dbo.VMX_FE_VISTA2.INVOICE_ID)) AS PEDIMENTOS		
,(SELECT     TOP (1) PDF_ESTADO
                            FROM          dbo.VMX_FE AS VMX_FE_2
                            WHERE      (INVOICE_ID = dbo.VMX_FE_VISTA2.INVOICE_ID)) AS PDF_ESTADO
							