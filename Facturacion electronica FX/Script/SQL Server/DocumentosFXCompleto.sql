USE [documentosfx]
GO

ALTER TABLE [dbo].[VMX_FE] DROP CONSTRAINT [DF__VMX_FE__subdivis__498EEC8D]
GO

ALTER TABLE [dbo].[VMX_FE] DROP CONSTRAINT [DF__VMX_FE__Fecha__47A6A41B]
GO

ALTER TABLE [dbo].[VMX_FE] DROP CONSTRAINT [DF__VMX_FE__timbrado__46B27FE2]
GO

/****** Object:  Table [dbo].[VMX_FE]    Script Date: 05/04/2021 10:04:10 a. m. ******/
DROP TABLE [dbo].[VMX_FE]
GO

/****** Object:  Table [dbo].[VMX_FE_CONSECUTIVOS]    Script Date: 05/04/2021 10:04:10 a. m. ******/
DROP TABLE [dbo].[VMX_FE_CONSECUTIVOS]
GO

/****** Object:  Table [dbo].[VMX_FE_FORMATOS]    Script Date: 05/04/2021 10:04:10 a. m. ******/
DROP TABLE [dbo].[VMX_FE_FORMATOS]
GO

/****** Object:  Table [dbo].[VMX_FE_PARAMETROS]    Script Date: 05/04/2021 10:04:10 a. m. ******/
DROP TABLE [dbo].[VMX_FE_PARAMETROS]
GO

/****** Object:  Table [dbo].[VMX_FE_SERIES]    Script Date: 05/04/2021 10:04:10 a. m. ******/
DROP TABLE [dbo].[VMX_FE_SERIES]
GO

/****** Object:  Table [dbo].[VMX_FE_SERIES_ACCOUNT]    Script Date: 05/04/2021 10:04:10 a. m. ******/
DROP TABLE [dbo].[VMX_FE_SERIES_ACCOUNT]
GO

/****** Object:  Table [dbo].[VMX_FE_SERIES_ACCOUNT]    Script Date: 05/04/2021 10:04:10 a. m. ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[VMX_FE_SERIES_ACCOUNT](
	[ROW_ID] [bigint] IDENTITY(1,1) NOT NULL,
	[ROW_ID_SERIE] [bigint] NULL,
	[ACCOUNT_ID] [varchar](50) NULL,
	[DESCRIPCION] [varchar](100) NULL,
 CONSTRAINT [PK_VMX_FE_SERIES_ACCOUNT] PRIMARY KEY CLUSTERED 
(
	[ROW_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]

GO

/****** Object:  Table [dbo].[VMX_FE_SERIES]    Script Date: 05/04/2021 10:04:10 a. m. ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[VMX_FE_SERIES](
	[ROW_ID] [bigint] IDENTITY(1,1) NOT NULL,
	[ID] [varchar](50) NULL,
	[FOLIO_INICIAL] [bigint] NULL,
	[FOLIO_FINAL] [bigint] NULL,
	[APROBACION] [varchar](50) NULL,
	[ANIO] [bigint] NULL,
	[DIRECTORIO] [varchar](250) NULL,
	[CALLE] [varchar](250) NULL,
	[EXTERIOR] [varchar](250) NULL,
	[INTERIOR] [varchar](250) NULL,
	[COLONIA] [varchar](250) NULL,
	[CP] [varchar](250) NULL,
	[LOCALIDAD] [varchar](250) NULL,
	[REFERENCIA] [varchar](250) NULL,
	[PAIS] [varchar](250) NULL,
	[ESTADO] [varchar](250) NULL,
	[MUNICIPIO] [varchar](250) NULL,
	[DE] [varchar](350) NULL,
	[CC] [varchar](350) NULL,
	[CCO] [varchar](350) NULL,
	[ASUNTO] [varchar](350) NULL,
	[MENSAJE] [varchar](1500) NULL,
	[RESTANTES] [bigint] NULL,
	[IMPRESOS] [bigint] NULL,
	[ROW_ID_CERTIFICADO] [bigint] NULL,
	[RUTA_PERSONALIZADA] [varchar](50) NULL,
	[NOMBRE_PERSONALIZADO] [varchar](180) NULL
) ON [PRIMARY]

GO

/****** Object:  Table [dbo].[VMX_FE_PARAMETROS]    Script Date: 05/04/2021 10:04:10 a. m. ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[VMX_FE_PARAMETROS](
	[ROW_ID] [bigint] IDENTITY(1,1) NOT NULL,
	[CERTIFICADO] [varchar](250) NULL,
	[LLAVE] [varchar](250) NULL,
	[NO_CERTIFICADO] [varchar](250) NULL,
	[PASSWORD] [varchar](50) NULL,
	[FECHA_DESDE] [datetime] NULL,
	[FECHA_HASTA] [datetime] NULL,
	[CALLE] [varchar](50) NULL,
	[EXTERIOR] [varchar](50) NULL,
	[INTERIOR] [varchar](50) NULL,
	[COLONIA] [varchar](50) NULL,
	[CP] [varchar](50) NULL,
	[LOCALIDAD] [varchar](50) NULL,
	[REFERENCIA] [varchar](50) NULL,
	[PAIS] [varchar](50) NULL,
	[ESTADO] [varchar](50) NULL,
	[MUNICIPIO] [varchar](50) NULL,
	[SERVIDOR] [varchar](50) NULL,
	[USUARIO] [varchar](50) NULL,
	[EMAIL_PASSWORD] [varchar](50) NULL,
	[PUERTO_SALIDA] [varchar](50) NULL,
	[SSL] [varchar](50) NULL,
	[CREDENCIALES] [varchar](50) NULL,
	[REQUIERE_ASIENTO] [varchar](50) NULL,
	[DIAS] [int] NULL,
	[REGIMEN_FISCAL] [varchar](200) NULL,
 CONSTRAINT [PK_VMX_FE_PARAMETROS] PRIMARY KEY CLUSTERED 
(
	[ROW_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]

GO

/****** Object:  Table [dbo].[VMX_FE_FORMATOS]    Script Date: 05/04/2021 10:04:10 a. m. ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[VMX_FE_FORMATOS](
	[ROW_ID] [bigint] IDENTITY(1,1) NOT NULL,
	[ID] [varchar](30) NULL,
	[DESCRIPCION] [varchar](100) NULL,
	[REPORTE] [varchar](100) NULL,
	[OBSERVACIONES] [varchar](250) NULL,
	[VISTA] [varchar](250) NULL,
	[ENTITY_ID] [varchar](50) NULL,
 CONSTRAINT [PK_VMX_FE_FORMATOS] PRIMARY KEY CLUSTERED 
(
	[ROW_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]

GO

/****** Object:  Table [dbo].[VMX_FE_CONSECUTIVOS]    Script Date: 05/04/2021 10:04:10 a. m. ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[VMX_FE_CONSECUTIVOS](
	[ROW_ID] [bigint] IDENTITY(1,1) NOT NULL,
	[CUSTOMER_ID] [varchar](50) NULL,
	[SHIPTO_ID] [varchar](50) NULL,
	[WAREHOUSE_ID] [varchar](50) NULL,
	[PREFIJO] [varchar](50) NULL,
	[CONSECUTIVO] [varchar](50) NULL,
	[SUFIJO] [varchar](50) NULL,
	[ROW_ID_SERIE] [bigint] NULL,
	[TIPO] [varchar](50) NULL
) ON [PRIMARY]

GO

/****** Object:  Table [dbo].[VMX_FE]    Script Date: 05/04/2021 10:04:10 a. m. ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[VMX_FE](
	[ROW_ID] [bigint] IDENTITY(1,1) NOT NULL,
	[INVOICE_ID] [varchar](30) NULL,
	[SELLO] [varchar](max) NULL,
	[PDF] [varchar](250) NULL,
	[XML] [varchar](250) NULL,
	[SERIE] [varchar](250) NULL,
	[FORMATO] [varchar](250) NULL,
	[PEDIMENTO] [varchar](50) NULL,
	[FECHA_PEDIMENTO] [datetime] NULL,
	[ADUANA] [varchar](50) NULL,
	[CREATE_DATE] [datetime] NULL,
	[UPDATE_DATE] [datetime] NULL,
	[FORMA_DE_PAGO] [nvarchar](max) NULL,
	[METODO_DE_PAGO] [nvarchar](max) NULL,
	[CTA_BANCO] [nvarchar](max) NULL,
	[UUID] [varchar](max) NULL,
	[FechaTimbrado] [varchar](max) NULL,
	[noCertificadoSAT] [varchar](max) NULL,
	[selloSAT] [varchar](max) NULL,
	[Cadena_TFD] [varchar](max) NULL,
	[QR_Code] [varchar](max) NULL,
	[IMAGEN_QR_Code] [varbinary](max) NULL,
	[CADENA] [varchar](max) NULL,
	[ESTADO] [varchar](max) NULL,
	[CANCELADO] [varchar](max) NULL,
	[noCertificado] [varchar](max) NULL,
	[ADDR_NO] [varchar](max) NULL,
	[PEDIMENTOS] [varchar](max) NULL,
	[PDF_ESTADO] [varchar](max) NULL,
	[timbrado_prueba] [varchar](50) NOT NULL,
	[Fecha] [varchar](50) NULL,
	[USO_CFDI] [varchar](150) NULL,
	[subdivision] [varchar](10) NOT NULL,
	[VERSION] [varchar](10) NULL,
 CONSTRAINT [PK_VMX_FE] PRIMARY KEY CLUSTERED 
(
	[ROW_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO

ALTER TABLE [dbo].[VMX_FE] ADD  DEFAULT ('False') FOR [timbrado_prueba]
GO

ALTER TABLE [dbo].[VMX_FE] ADD  DEFAULT (NULL) FOR [Fecha]
GO

ALTER TABLE [dbo].[VMX_FE] ADD  DEFAULT ('0') FOR [subdivision]
GO


