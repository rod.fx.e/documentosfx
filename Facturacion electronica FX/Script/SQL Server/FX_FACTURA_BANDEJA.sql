CREATE TABLE [dbo].[FX_FACTURA_BANDEJA](
	[ROW_ID] [bigint] IDENTITY(1,1) NOT NULL,
	[ID] [varchar](50) NULL,
	[ARCHIVO] [varchar](max) NULL,
	[ESTADO] [varchar](50) NULL,
	[ADDENDA_ID] [varchar](50) NULL,
	[ARCHIVO_DESTINO] [varchar](max) NULL,
	[referenceIdentification] [varchar](max) NULL,
	[buyer_gln] [varchar](max) NULL,
	[alternatePartyIdentification] [varchar](max) NULL,
	[referenceIdentification_ON] [varchar](max) NULL,
	[referenceIdentification_ATZ] [varchar](max) NULL,
	[seller_gln] [varchar](max) NULL,
	[ship_gln] [varchar](max) NULL,
	[alternatePartyIdentification_Value] [varchar](max) NULL,
 CONSTRAINT [PK_FX_FACTURA_BANDEJA] PRIMARY KEY CLUSTERED 
(
	[ROW_ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO


  ALTER TABLE VMX_FE
ADD ADDR_NO smallint
GO

