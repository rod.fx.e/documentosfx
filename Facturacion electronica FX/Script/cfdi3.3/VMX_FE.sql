ALTER TABLE [dbo].[VMX_FE] ADD [VERSION] [varchar](10) NULL
ALTER TABLE [dbo].[VMX_FE] ADD [USO_CFDI] [varchar](150) NULL
ALTER TABLE [dbo].[VMX_FE] ADD [leyendaFiscal] [varchar](MAX) NULL
ALTER TABLE [dbo].[CLIENTE_CONFIGURACION] ADD [paisEntregar] [varchar](10) NULL

ALTER TABLE [dbo].[SERIE] ADD [rfcExcepcion] [varchar](150) NULL

ALTER TABLE [dbo].[CFDI_USUARIO] ADD [USUARIO_PRUEBA] [varchar](30) NULL
ALTER TABLE [dbo].[CFDI_USUARIO] ADD [PASSWORD_PRUEBA] [varchar](30) NULL

ALTER TABLE [dbo].[configuracionProductoSet] ADD [unidadSAT] [varchar](50) NULL

ALTER TABLE [dbo].[EMPRESA] ADD [relacionLinea] [nvarchar](10) DEFAULT 'False'

ALTER TABLE ADDENDA DROP COLUMN ROW_ID; 
ALTER TABLE ADDENDA ADD  ROW_ID INT NOT NULL IDENTITY(1,1) ; 

ALTER TABLE [dbo].[VMX_FE] ADD [timbrado_prueba] [varchar](50) NULL

ALTER TABLE [dbo].[VMX_FE] ADD [Fecha] [varchar](40) NULL

ALTER TABLE SERIE ADD serieComprobante VARCHAR(10) NULL DEFAULT 'False'


ALTER TABLE SERIE ADD serieTraslado VARCHAR(10) NULL DEFAULT 'False'

ALTER TABLE VMX_FE ADD Fecha VARCHAR(50) NULL DEFAULT NULL


ALTER TABLE SERIE ADD ndcUsarPropio VARCHAR(10) NULL DEFAULT 'False'
ALTER TABLE SERIE ADD ndcMetodoPago VARCHAR(10) NULL
ALTER TABLE SERIE ADD ndcUsoCfdi VARCHAR(10) NULL
ALTER TABLE SERIE ADD ndcClaveProd VARCHAR(10) NULL
ALTER TABLE SERIE ADD ndcUnidad VARCHAR(10) NULL

ALTER TABLE SERIE DROP COLUMN ROW_ID; 
ALTER TABLE SERIE ADD  ROW_ID INT NOT NULL IDENTITY(1,1) ; 

ALTER TABLE EMPRESA ADD automatizacion VARCHAR(10)  DEFAULT 'True'


ALTER TABLE SERIE ADD  validarComprobantePago VARCHAR(10) NULL DEFAULT 'False'


ALTER TABLE [dbo].[pagoRelacionadoSet] ALTER COLUMN  [checkId] nvarchar(40)  NULL;

ALTER TABLE [dbo].[pagoSet] ALTER COLUMN  [checkId] nvarchar(40)  NULL;

ALTER TABLE [dbo].[CCE] ADD  [PART_ID_EXCEPCIONES] nvarchar(150)  NULL;

ALTER TABLE [dbo].[CCE] ADD  [PRODUCTCODE_EXCEPCIONES] nvarchar(150)  NULL;
ALTER TABLE [dbo].[CCE] ADD  [excelProductCommodityFraccion] nvarchar(150)  NULL;
ALTER TABLE CCE DROP COLUMN ROW_ID; 
ALTER TABLE CCE ADD  ROW_ID INT NOT NULL IDENTITY(1,1) ; 



alter table archivoSyncSet
add [tipoComprobante] nvarchar(15)  NULL;

ALTER TABLE EMPRESA ADD  
portal  nvarchar(100)  NULL;


ALTER TABLE [dbo].[VMX_FE] ADD [FechaTimbrado] [varchar](max) NULL;
ALTER TABLE [dbo].[VMX_FE] ADD [noCertificadoSAT] [varchar](max) NULL;
ALTER TABLE [dbo].[VMX_FE] ADD [selloSAT] [varchar](max) NULL;
ALTER TABLE [dbo].[VMX_FE] ADD [Cadena_TFD] [varchar](max) NULL;
ALTER TABLE [dbo].[VMX_FE] ADD [QR_Code] [varchar](max) NULL;
ALTER TABLE [dbo].[VMX_FE] ADD [IMAGEN_QR_Code] [image] NULL;
ALTER TABLE [dbo].[VMX_FE] ADD [CADENA] [varchar](max) NULL;
ALTER TABLE [dbo].[VMX_FE] ADD [ESTADO] [varchar](max) NULL;
ALTER TABLE [dbo].[VMX_FE] ADD [CANCELADO] [varchar](max) NULL;
ALTER TABLE [dbo].[VMX_FE] ADD [noCertificado] [varchar](max) NULL;
ALTER TABLE [dbo].[VMX_FE] ADD [ADDR_NO] [varchar](max) NULL;
ALTER TABLE [dbo].[VMX_FE] ADD [PEDIMENTOS] [varchar](max) NULL;
ALTER TABLE [dbo].[VMX_FE] ADD [PDF_ESTADO] [varchar](max) NULL;
ALTER TABLE [dbo].[VMX_FE] ADD [VERSION] [varchar](10) NULL;
ALTER TABLE [dbo].[VMX_FE] ADD [USO_CFDI] [varchar](150) NULL;
ALTER TABLE [dbo].[VMX_FE] ADD [timbrado_prueba] [varchar](50) NOT NULL;
ALTER TABLE [dbo].[VMX_FE] ADD [Fecha] [varchar](40) NULL;


ALTER TABLE [dbo].[VMX_FE] ADD subdivision [varchar](10)  NOT NULL DEFAULT('0');

ALTER TABLE [dbo].[VMX_FE] ADD [RegimenFiscal] [varchar](max) NULL;
