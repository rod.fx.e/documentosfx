﻿//namespace FE_FX
//{
//    using System.Xml.Serialization;


//    /// <comentarios/>
//    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.3038")]
//    [System.SerializableAttribute()]
//    [System.Diagnostics.DebuggerStepThroughAttribute()]
//    [System.ComponentModel.DesignerCategoryAttribute("code")]
//    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.sat.gob.mx/cfd/3")]
//    [System.Xml.Serialization.XmlRootAttribute(Namespace = "http://www.sat.gob.mx/cfd/3", IsNullable = false)]
//    public partial class Comprobante
//    {
//        //14-03-2011 Rodrigo Escalona, Añadir schemaLocation
//        [System.Xml.Serialization.XmlAttribute("schemaLocation", Namespace = "http://www.w3.org/2001/XMLSchema-instance")]
//        public string schemaLocation = "http://www.sat.gob.mx/cfd/3 http://www.sat.gob.mx/sitio_internet/cfd/3/cfdv32.xsd";

//        [System.Xml.Serialization.XmlAttribute("cfdi", Namespace = "http://www.w3.org/2001/XMLSchema-instance")]
//        public string cfdi = "http://www.sat.gob.mx/cfd/3";

//        private ComprobanteEmisor emisorField;

//        private ComprobanteReceptor receptorField;

//        private ComprobanteConcepto[] conceptosField;

//        private ComprobanteImpuestos impuestosField;

//        private ComprobanteComplemento complementoField;

//        private ComprobanteAddenda addendaField;

//        private string versionField;

//        private string serieField;

//        private string folioField;

//        private System.DateTime fechaField;

//        private string selloField;

//        private string noAprobacionField;

//        private string anoAprobacionField;

//        private string formaDePagoField;

//        private string noCertificadoField;

//        private string certificadoField;

//        private string condicionesDePagoField;

//        private decimal subTotalField;

//        private decimal descuentoField;

//        private bool descuentoFieldSpecified;

//        private string motivoDescuentoField;

//        private decimal totalField;

//        private string tipoCambioField;

//        private string monedaField;

//        private string metodoDePagoField;

//        private ComprobanteTipoDeComprobante tipoDeComprobanteField;

//        public Comprobante()
//        {
//            this.versionField = "3.0";
//        }

//        /// <comentarios/>
//        public ComprobanteEmisor Emisor
//        {
//            get
//            {
//                return this.emisorField;
//            }
//            set
//            {
//                this.emisorField = value;
//            }
//        }

//        /// <comentarios/>
//        public ComprobanteReceptor Receptor
//        {
//            get
//            {
//                return this.receptorField;
//            }
//            set
//            {
//                this.receptorField = value;
//            }
//        }

//        /// <comentarios/>
//        [System.Xml.Serialization.XmlArrayItemAttribute("Concepto", IsNullable = false)]
//        public ComprobanteConcepto[] Conceptos
//        {
//            get
//            {
//                return this.conceptosField;
//            }
//            set
//            {
//                this.conceptosField = value;
//            }
//        }

//        /// <comentarios/>
//        public ComprobanteImpuestos Impuestos
//        {
//            get
//            {
//                return this.impuestosField;
//            }
//            set
//            {
//                this.impuestosField = value;
//            }
//        }

//        /// <comentarios/>
//        public ComprobanteComplemento Complemento
//        {
//            get
//            {
//                return this.complementoField;
//            }
//            set
//            {
//                this.complementoField = value;
//            }
//        }

//        /// <comentarios/>
//        public ComprobanteAddenda Addenda
//        {
//            get
//            {
//                return this.addendaField;
//            }
//            set
//            {
//                this.addendaField = value;
//            }
//        }

//        /// <comentarios/>
//        [System.Xml.Serialization.XmlAttributeAttribute()]
//        public string version
//        {
//            get
//            {
//                return this.versionField;
//            }
//            set
//            {
//                this.versionField = value;
//            }
//        }

//        /// <comentarios/>
//        [System.Xml.Serialization.XmlAttributeAttribute()]
//        public string serie
//        {
//            get
//            {
//                return this.serieField;
//            }
//            set
//            {
//                this.serieField = value;
//            }
//        }

//        /// <comentarios/>
//        [System.Xml.Serialization.XmlAttributeAttribute()]
//        public string folio
//        {
//            get
//            {
//                return this.folioField;
//            }
//            set
//            {
//                this.folioField = value;
//            }
//        }

//        /// <comentarios/>
//        [System.Xml.Serialization.XmlAttributeAttribute()]
//        public System.DateTime fecha
//        {
//            get
//            {
//                return this.fechaField;
//            }
//            set
//            {
//                this.fechaField = value;
//            }
//        }

//        /// <comentarios/>
//        [System.Xml.Serialization.XmlAttributeAttribute()]
//        public string sello
//        {
//            get
//            {
//                return this.selloField;
//            }
//            set
//            {
//                this.selloField = value;
//            }
//        }

//        /// <comentarios/>
//        [System.Xml.Serialization.XmlAttributeAttribute(DataType = "integer")]
//        public string noAprobacion
//        {
//            get
//            {
//                return this.noAprobacionField;
//            }
//            set
//            {
//                this.noAprobacionField = value;
//            }
//        }

//        /// <comentarios/>
//        [System.Xml.Serialization.XmlAttributeAttribute(DataType = "integer")]
//        public string anoAprobacion
//        {
//            get
//            {
//                return this.anoAprobacionField;
//            }
//            set
//            {
//                this.anoAprobacionField = value;
//            }
//        }

//        /// <comentarios/>
//        [System.Xml.Serialization.XmlAttributeAttribute()]
//        public string formaDePago
//        {
//            get
//            {
//                return this.formaDePagoField;
//            }
//            set
//            {
//                this.formaDePagoField = value;
//            }
//        }

//        /// <comentarios/>
//        [System.Xml.Serialization.XmlAttributeAttribute()]
//        public string noCertificado
//        {
//            get
//            {
//                return this.noCertificadoField;
//            }
//            set
//            {
//                this.noCertificadoField = value;
//            }
//        }

//        /// <comentarios/>
//        [System.Xml.Serialization.XmlAttributeAttribute()]
//        public string certificado
//        {
//            get
//            {
//                return this.certificadoField;
//            }
//            set
//            {
//                this.certificadoField = value;
//            }
//        }

//        /// <comentarios/>
//        [System.Xml.Serialization.XmlAttributeAttribute()]
//        public string condicionesDePago
//        {
//            get
//            {
//                return this.condicionesDePagoField;
//            }
//            set
//            {
//                this.condicionesDePagoField = value;
//            }
//        }

//        /// <comentarios/>
//        [System.Xml.Serialization.XmlAttributeAttribute()]
//        public decimal subTotal
//        {
//            get
//            {
//                return this.subTotalField;
//            }
//            set
//            {
//                this.subTotalField = value;
//            }
//        }

//        /// <comentarios/>
//        [System.Xml.Serialization.XmlAttributeAttribute()]
//        public decimal descuento
//        {
//            get
//            {
//                return this.descuentoField;
//            }
//            set
//            {
//                this.descuentoField = value;
//            }
//        }

//        /// <comentarios/>
//        [System.Xml.Serialization.XmlIgnoreAttribute()]
//        public bool descuentoSpecified
//        {
//            get
//            {
//                return this.descuentoFieldSpecified;
//            }
//            set
//            {
//                this.descuentoFieldSpecified = value;
//            }
//        }

//        /// <comentarios/>
//        [System.Xml.Serialization.XmlAttributeAttribute()]
//        public string motivoDescuento
//        {
//            get
//            {
//                return this.motivoDescuentoField;
//            }
//            set
//            {
//                this.motivoDescuentoField = value;
//            }
//        }

//        // <comentarios/>
//        [System.Xml.Serialization.XmlAttributeAttribute()]
//        public string TipoCambio
//        {
//            get
//            {
//                return this.tipoCambioField;
//            }
//            set
//            {
//                this.tipoCambioField = value;
//            }
//        }

//        // <comentarios/>
//        [System.Xml.Serialization.XmlAttributeAttribute()]
//        public string Moneda
//        {
//            get
//            {
//                return this.monedaField;
//            }
//            set
//            {
//                this.monedaField = value;
//            }
//        }
//        /// <comentarios/>
//        [System.Xml.Serialization.XmlAttributeAttribute()]
//        public decimal total
//        {
//            get
//            {
//                return this.totalField;
//            }
//            set
//            {
//                this.totalField = value;
//            }
//        }

//        /// <comentarios/>
//        [System.Xml.Serialization.XmlAttributeAttribute()]
//        public string metodoDePago
//        {
//            get
//            {
//                return this.metodoDePagoField;
//            }
//            set
//            {
//                this.metodoDePagoField = value;
//            }
//        }

//        /// <comentarios/>
//        [System.Xml.Serialization.XmlAttributeAttribute()]
//        public ComprobanteTipoDeComprobante tipoDeComprobante
//        {
//            get
//            {
//                return this.tipoDeComprobanteField;
//            }
//            set
//            {
//                this.tipoDeComprobanteField = value;
//            }
//        }
//    }

//    /// <comentarios/>
//    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.3038")]
//    [System.SerializableAttribute()]
//    [System.Diagnostics.DebuggerStepThroughAttribute()]
//    [System.ComponentModel.DesignerCategoryAttribute("code")]
//    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.sat.gob.mx/cfd/2")]
//    public partial class ComprobanteEmisor
//    {

//        private t_UbicacionFiscal domicilioFiscalField;

//        private t_Ubicacion expedidoEnField;

//        private string rfcField;

//        private string nombreField;

//        /// <comentarios/>
//        public t_UbicacionFiscal DomicilioFiscal
//        {
//            get
//            {
//                return this.domicilioFiscalField;
//            }
//            set
//            {
//                this.domicilioFiscalField = value;
//            }
//        }

//        /// <comentarios/>
//        public t_Ubicacion ExpedidoEn
//        {
//            get
//            {
//                return this.expedidoEnField;
//            }
//            set
//            {
//                this.expedidoEnField = value;
//            }
//        }

//        /// <comentarios/>
//        [System.Xml.Serialization.XmlAttributeAttribute()]
//        public string rfc
//        {
//            get
//            {
//                return this.rfcField;
//            }
//            set
//            {
//                this.rfcField = value;
//            }
//        }

//        /// <comentarios/>
//        [System.Xml.Serialization.XmlAttributeAttribute()]
//        public string nombre
//        {
//            get
//            {
//                return this.nombreField;
//            }
//            set
//            {
//                this.nombreField = value;
//            }
//        }
//    }

//    /// <comentarios/>
//    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.3038")]
//    [System.SerializableAttribute()]
//    [System.Diagnostics.DebuggerStepThroughAttribute()]
//    [System.ComponentModel.DesignerCategoryAttribute("code")]
//    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://www.sat.gob.mx/cfd/2")]
//    public partial class t_UbicacionFiscal
//    {

//        private string calleField;

//        private string noExteriorField;

//        private string noInteriorField;

//        private string coloniaField;

//        private string localidadField;

//        private string referenciaField;

//        private string municipioField;

//        private string estadoField;

//        private string paisField;

//        private string codigoPostalField;

//        /// <comentarios/>
//        [System.Xml.Serialization.XmlAttributeAttribute()]
//        public string calle
//        {
//            get
//            {
//                return this.calleField;
//            }
//            set
//            {
//                this.calleField = value;
//            }
//        }

//        /// <comentarios/>
//        [System.Xml.Serialization.XmlAttributeAttribute()]
//        public string noExterior
//        {
//            get
//            {
//                return this.noExteriorField;
//            }
//            set
//            {
//                this.noExteriorField = value;
//            }
//        }

//        /// <comentarios/>
//        [System.Xml.Serialization.XmlAttributeAttribute()]
//        public string noInterior
//        {
//            get
//            {
//                return this.noInteriorField;
//            }
//            set
//            {
//                this.noInteriorField = value;
//            }
//        }

//        /// <comentarios/>
//        [System.Xml.Serialization.XmlAttributeAttribute()]
//        public string colonia
//        {
//            get
//            {
//                return this.coloniaField;
//            }
//            set
//            {
//                this.coloniaField = value;
//            }
//        }

//        /// <comentarios/>
//        [System.Xml.Serialization.XmlAttributeAttribute()]
//        public string localidad
//        {
//            get
//            {
//                return this.localidadField;
//            }
//            set
//            {
//                this.localidadField = value;
//            }
//        }

//        /// <comentarios/>
//        [System.Xml.Serialization.XmlAttributeAttribute()]
//        public string referencia
//        {
//            get
//            {
//                return this.referenciaField;
//            }
//            set
//            {
//                this.referenciaField = value;
//            }
//        }

//        /// <comentarios/>
//        [System.Xml.Serialization.XmlAttributeAttribute()]
//        public string municipio
//        {
//            get
//            {
//                return this.municipioField;
//            }
//            set
//            {
//                this.municipioField = value;
//            }
//        }

//        /// <comentarios/>
//        [System.Xml.Serialization.XmlAttributeAttribute()]
//        public string estado
//        {
//            get
//            {
//                return this.estadoField;
//            }
//            set
//            {
//                this.estadoField = value;
//            }
//        }

//        /// <comentarios/>
//        [System.Xml.Serialization.XmlAttributeAttribute()]
//        public string pais
//        {
//            get
//            {
//                return this.paisField;
//            }
//            set
//            {
//                this.paisField = value;
//            }
//        }

//        /// <comentarios/>
//        [System.Xml.Serialization.XmlAttributeAttribute()]
//        public string codigoPostal
//        {
//            get
//            {
//                return this.codigoPostalField;
//            }
//            set
//            {
//                this.codigoPostalField = value;
//            }
//        }
//    }

//    /// <comentarios/>
//    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.3038")]
//    [System.SerializableAttribute()]
//    [System.Diagnostics.DebuggerStepThroughAttribute()]
//    [System.ComponentModel.DesignerCategoryAttribute("code")]
//    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://www.sat.gob.mx/cfd/2")]
//    public partial class t_InformacionAduanera
//    {

//        private string numeroField;

//        private System.DateTime fechaField;

//        private string aduanaField;

//        /// <comentarios/>
//        [System.Xml.Serialization.XmlAttributeAttribute()]
//        public string numero
//        {
//            get
//            {
//                return this.numeroField;
//            }
//            set
//            {
//                this.numeroField = value;
//            }
//        }

//        /// <comentarios/>
//        [System.Xml.Serialization.XmlAttributeAttribute(DataType = "date")]
//        public System.DateTime fecha
//        {
//            get
//            {
//                return this.fechaField;
//            }
//            set
//            {
//                this.fechaField = value;
//            }
//        }

//        /// <comentarios/>
//        [System.Xml.Serialization.XmlAttributeAttribute()]
//        public string aduana
//        {
//            get
//            {
//                return this.aduanaField;
//            }
//            set
//            {
//                this.aduanaField = value;
//            }
//        }
//    }

//    /// <comentarios/>
//    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.3038")]
//    [System.SerializableAttribute()]
//    [System.Diagnostics.DebuggerStepThroughAttribute()]
//    [System.ComponentModel.DesignerCategoryAttribute("code")]
//    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://www.sat.gob.mx/cfd/2")]
//    public partial class t_Ubicacion
//    {

//        private string calleField;

//        private string noExteriorField;

//        private string noInteriorField;

//        private string coloniaField;

//        private string localidadField;

//        private string referenciaField;

//        private string municipioField;

//        private string estadoField;

//        private string paisField;

//        private string codigoPostalField;

//        /// <comentarios/>
//        [System.Xml.Serialization.XmlAttributeAttribute()]
//        public string calle
//        {
//            get
//            {
//                return this.calleField;
//            }
//            set
//            {
//                this.calleField = value;
//            }
//        }

//        /// <comentarios/>
//        [System.Xml.Serialization.XmlAttributeAttribute()]
//        public string noExterior
//        {
//            get
//            {
//                return this.noExteriorField;
//            }
//            set
//            {
//                this.noExteriorField = value;
//            }
//        }

//        /// <comentarios/>
//        [System.Xml.Serialization.XmlAttributeAttribute()]
//        public string noInterior
//        {
//            get
//            {
//                return this.noInteriorField;
//            }
//            set
//            {
//                this.noInteriorField = value;
//            }
//        }

//        /// <comentarios/>
//        [System.Xml.Serialization.XmlAttributeAttribute()]
//        public string colonia
//        {
//            get
//            {
//                return this.coloniaField;
//            }
//            set
//            {
//                this.coloniaField = value;
//            }
//        }

//        /// <comentarios/>
//        [System.Xml.Serialization.XmlAttributeAttribute()]
//        public string localidad
//        {
//            get
//            {
//                return this.localidadField;
//            }
//            set
//            {
//                this.localidadField = value;
//            }
//        }

//        /// <comentarios/>
//        [System.Xml.Serialization.XmlAttributeAttribute()]
//        public string referencia
//        {
//            get
//            {
//                return this.referenciaField;
//            }
//            set
//            {
//                this.referenciaField = value;
//            }
//        }

//        /// <comentarios/>
//        [System.Xml.Serialization.XmlAttributeAttribute()]
//        public string municipio
//        {
//            get
//            {
//                return this.municipioField;
//            }
//            set
//            {
//                this.municipioField = value;
//            }
//        }

//        /// <comentarios/>
//        [System.Xml.Serialization.XmlAttributeAttribute()]
//        public string estado
//        {
//            get
//            {
//                return this.estadoField;
//            }
//            set
//            {
//                this.estadoField = value;
//            }
//        }

//        /// <comentarios/>
//        [System.Xml.Serialization.XmlAttributeAttribute()]
//        public string pais
//        {
//            get
//            {
//                return this.paisField;
//            }
//            set
//            {
//                this.paisField = value;
//            }
//        }

//        /// <comentarios/>
//        [System.Xml.Serialization.XmlAttributeAttribute()]
//        public string codigoPostal
//        {
//            get
//            {
//                return this.codigoPostalField;
//            }
//            set
//            {
//                this.codigoPostalField = value;
//            }
//        }
//    }

//    /// <comentarios/>
//    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.3038")]
//    [System.SerializableAttribute()]
//    [System.Diagnostics.DebuggerStepThroughAttribute()]
//    [System.ComponentModel.DesignerCategoryAttribute("code")]
//    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.sat.gob.mx/cfd/2")]
//    public partial class ComprobanteReceptor
//    {

//        private t_Ubicacion domicilioField;

//        private string rfcField;

//        private string nombreField;

//        /// <comentarios/>
//        public t_Ubicacion Domicilio
//        {
//            get
//            {
//                return this.domicilioField;
//            }
//            set
//            {
//                this.domicilioField = value;
//            }
//        }

//        /// <comentarios/>
//        [System.Xml.Serialization.XmlAttributeAttribute()]
//        public string rfc
//        {
//            get
//            {
//                return this.rfcField;
//            }
//            set
//            {
//                this.rfcField = value;
//            }
//        }

//        /// <comentarios/>
//        [System.Xml.Serialization.XmlAttributeAttribute()]
//        public string nombre
//        {
//            get
//            {
//                return this.nombreField;
//            }
//            set
//            {
//                this.nombreField = value;
//            }
//        }
//    }

//    /// <comentarios/>
//    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.3038")]
//    [System.SerializableAttribute()]
//    [System.Diagnostics.DebuggerStepThroughAttribute()]
//    [System.ComponentModel.DesignerCategoryAttribute("code")]
//    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.sat.gob.mx/cfd/2")]
//    public partial class ComprobanteConcepto
//    {

//        private object[] itemsField;

//        private decimal cantidadField;

//        private string unidadField;

//        private string noIdentificacionField;

//        private string descripcionField;

//        private decimal valorUnitarioField;

//        private decimal importeField;

//        /// <comentarios/>
//        [System.Xml.Serialization.XmlElementAttribute("ComplementoConcepto", typeof(ComprobanteConceptoComplementoConcepto))]
//        [System.Xml.Serialization.XmlElementAttribute("CuentaPredial", typeof(ComprobanteConceptoCuentaPredial))]
//        [System.Xml.Serialization.XmlElementAttribute("InformacionAduanera", typeof(t_InformacionAduanera))]
//        [System.Xml.Serialization.XmlElementAttribute("Parte", typeof(ComprobanteConceptoParte))]
//        public object[] Items
//        {
//            get
//            {
//                return this.itemsField;
//            }
//            set
//            {
//                this.itemsField = value;
//            }
//        }

//        /// <comentarios/>
//        [System.Xml.Serialization.XmlAttributeAttribute()]
//        public decimal cantidad
//        {
//            get
//            {
//                return this.cantidadField;
//            }
//            set
//            {
//                this.cantidadField = value;
//            }
//        }

//        /// <comentarios/>
//        [System.Xml.Serialization.XmlAttributeAttribute()]
//        public string unidad
//        {
//            get
//            {
//                return this.unidadField;
//            }
//            set
//            {
//                this.unidadField = value;
//            }
//        }

//        /// <comentarios/>
//        [System.Xml.Serialization.XmlAttributeAttribute()]
//        public string noIdentificacion
//        {
//            get
//            {
//                return this.noIdentificacionField;
//            }
//            set
//            {
//                this.noIdentificacionField = value;
//            }
//        }

//        /// <comentarios/>
//        [System.Xml.Serialization.XmlAttributeAttribute()]
//        public string descripcion
//        {
//            get
//            {
//                return this.descripcionField;
//            }
//            set
//            {
//                this.descripcionField = value;
//            }
//        }

//        /// <comentarios/>
//        [System.Xml.Serialization.XmlAttributeAttribute()]
//        public decimal valorUnitario
//        {
//            get
//            {
//                return this.valorUnitarioField;
//            }
//            set
//            {
//                this.valorUnitarioField = value;
//            }
//        }

//        /// <comentarios/>
//        [System.Xml.Serialization.XmlAttributeAttribute()]
//        public decimal importe
//        {
//            get
//            {
//                return this.importeField;
//            }
//            set
//            {
//                this.importeField = value;
//            }
//        }
//    }

//    /// <comentarios/>
//    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.3038")]
//    [System.SerializableAttribute()]
//    [System.Diagnostics.DebuggerStepThroughAttribute()]
//    [System.ComponentModel.DesignerCategoryAttribute("code")]
//    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.sat.gob.mx/cfd/2")]
//    public partial class ComprobanteConceptoComplementoConcepto
//    {

//        private System.Xml.XmlElement[] anyField;

//        /// <comentarios/>
//        [System.Xml.Serialization.XmlAnyElementAttribute()]
//        public System.Xml.XmlElement[] Any
//        {
//            get
//            {
//                return this.anyField;
//            }
//            set
//            {
//                this.anyField = value;
//            }
//        }
//    }

//    /// <comentarios/>
//    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.3038")]
//    [System.SerializableAttribute()]
//    [System.Diagnostics.DebuggerStepThroughAttribute()]
//    [System.ComponentModel.DesignerCategoryAttribute("code")]
//    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.sat.gob.mx/cfd/2")]
//    public partial class ComprobanteConceptoCuentaPredial
//    {

//        private string numeroField;

//        /// <comentarios/>
//        [System.Xml.Serialization.XmlAttributeAttribute()]
//        public string numero
//        {
//            get
//            {
//                return this.numeroField;
//            }
//            set
//            {
//                this.numeroField = value;
//            }
//        }
//    }

//    /// <comentarios/>
//    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.3038")]
//    [System.SerializableAttribute()]
//    [System.Diagnostics.DebuggerStepThroughAttribute()]
//    [System.ComponentModel.DesignerCategoryAttribute("code")]
//    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.sat.gob.mx/cfd/2")]
//    public partial class ComprobanteConceptoParte
//    {

//        private t_InformacionAduanera[] informacionAduaneraField;

//        private decimal cantidadField;

//        private string unidadField;

//        private string noIdentificacionField;

//        private string descripcionField;

//        private decimal valorUnitarioField;

//        private bool valorUnitarioFieldSpecified;

//        private decimal importeField;

//        private bool importeFieldSpecified;

//        /// <comentarios/>
//        [System.Xml.Serialization.XmlElementAttribute("InformacionAduanera")]
//        public t_InformacionAduanera[] InformacionAduanera
//        {
//            get
//            {
//                return this.informacionAduaneraField;
//            }
//            set
//            {
//                this.informacionAduaneraField = value;
//            }
//        }

//        /// <comentarios/>
//        [System.Xml.Serialization.XmlAttributeAttribute()]
//        public decimal cantidad
//        {
//            get
//            {
//                return this.cantidadField;
//            }
//            set
//            {
//                this.cantidadField = value;
//            }
//        }

//        /// <comentarios/>
//        [System.Xml.Serialization.XmlAttributeAttribute()]
//        public string unidad
//        {
//            get
//            {
//                return this.unidadField;
//            }
//            set
//            {
//                this.unidadField = value;
//            }
//        }

//        /// <comentarios/>
//        [System.Xml.Serialization.XmlAttributeAttribute()]
//        public string noIdentificacion
//        {
//            get
//            {
//                return this.noIdentificacionField;
//            }
//            set
//            {
//                this.noIdentificacionField = value;
//            }
//        }

//        /// <comentarios/>
//        [System.Xml.Serialization.XmlAttributeAttribute()]
//        public string descripcion
//        {
//            get
//            {
//                return this.descripcionField;
//            }
//            set
//            {
//                this.descripcionField = value;
//            }
//        }

//        /// <comentarios/>
//        [System.Xml.Serialization.XmlAttributeAttribute()]
//        public decimal valorUnitario
//        {
//            get
//            {
//                return this.valorUnitarioField;
//            }
//            set
//            {
//                this.valorUnitarioField = value;
//            }
//        }

//        /// <comentarios/>
//        [System.Xml.Serialization.XmlIgnoreAttribute()]
//        public bool valorUnitarioSpecified
//        {
//            get
//            {
//                return this.valorUnitarioFieldSpecified;
//            }
//            set
//            {
//                this.valorUnitarioFieldSpecified = value;
//            }
//        }

//        /// <comentarios/>
//        [System.Xml.Serialization.XmlAttributeAttribute()]
//        public decimal importe
//        {
//            get
//            {
//                return this.importeField;
//            }
//            set
//            {
//                this.importeField = value;
//            }
//        }

//        /// <comentarios/>
//        [System.Xml.Serialization.XmlIgnoreAttribute()]
//        public bool importeSpecified
//        {
//            get
//            {
//                return this.importeFieldSpecified;
//            }
//            set
//            {
//                this.importeFieldSpecified = value;
//            }
//        }
//    }

//    /// <comentarios/>
//    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.3038")]
//    [System.SerializableAttribute()]
//    [System.Diagnostics.DebuggerStepThroughAttribute()]
//    [System.ComponentModel.DesignerCategoryAttribute("code")]
//    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.sat.gob.mx/cfd/2")]
//    public partial class ComprobanteImpuestos
//    {

//        private ComprobanteImpuestosRetencion[] retencionesField;

//        private ComprobanteImpuestosTraslado[] trasladosField;

//        private decimal totalImpuestosRetenidosField;

//        private bool totalImpuestosRetenidosFieldSpecified;

//        private decimal totalImpuestosTrasladadosField;

//        private bool totalImpuestosTrasladadosFieldSpecified;

//        /// <comentarios/>
//        [System.Xml.Serialization.XmlArrayItemAttribute("Retencion", IsNullable = false)]
//        public ComprobanteImpuestosRetencion[] Retenciones
//        {
//            get
//            {
//                return this.retencionesField;
//            }
//            set
//            {
//                this.retencionesField = value;
//            }
//        }

//        /// <comentarios/>
//        [System.Xml.Serialization.XmlArrayItemAttribute("Traslado", IsNullable = false)]
//        public ComprobanteImpuestosTraslado[] Traslados
//        {
//            get
//            {
//                return this.trasladosField;
//            }
//            set
//            {
//                this.trasladosField = value;
//            }
//        }

//        /// <comentarios/>
//        [System.Xml.Serialization.XmlAttributeAttribute()]
//        public decimal totalImpuestosRetenidos
//        {
//            get
//            {
//                return this.totalImpuestosRetenidosField;
//            }
//            set
//            {
//                this.totalImpuestosRetenidosField = value;
//            }
//        }

//        /// <comentarios/>
//        [System.Xml.Serialization.XmlIgnoreAttribute()]
//        public bool totalImpuestosRetenidosSpecified
//        {
//            get
//            {
//                return this.totalImpuestosRetenidosFieldSpecified;
//            }
//            set
//            {
//                this.totalImpuestosRetenidosFieldSpecified = value;
//            }
//        }

//        /// <comentarios/>
//        [System.Xml.Serialization.XmlAttributeAttribute()]
//        public decimal totalImpuestosTrasladados
//        {
//            get
//            {
//                return this.totalImpuestosTrasladadosField;
//            }
//            set
//            {
//                this.totalImpuestosTrasladadosField = value;
//            }
//        }

//        /// <comentarios/>
//        [System.Xml.Serialization.XmlIgnoreAttribute()]
//        public bool totalImpuestosTrasladadosSpecified
//        {
//            get
//            {
//                return this.totalImpuestosTrasladadosFieldSpecified;
//            }
//            set
//            {
//                this.totalImpuestosTrasladadosFieldSpecified = value;
//            }
//        }
//    }

//    /// <comentarios/>
//    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.3038")]
//    [System.SerializableAttribute()]
//    [System.Diagnostics.DebuggerStepThroughAttribute()]
//    [System.ComponentModel.DesignerCategoryAttribute("code")]
//    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.sat.gob.mx/cfd/2")]
//    public partial class ComprobanteImpuestosRetencion
//    {

//        private ComprobanteImpuestosRetencionImpuesto impuestoField;

//        private decimal importeField;

//        /// <comentarios/>
//        [System.Xml.Serialization.XmlAttributeAttribute()]
//        public ComprobanteImpuestosRetencionImpuesto impuesto
//        {
//            get
//            {
//                return this.impuestoField;
//            }
//            set
//            {
//                this.impuestoField = value;
//            }
//        }

//        /// <comentarios/>
//        [System.Xml.Serialization.XmlAttributeAttribute()]
//        public decimal importe
//        {
//            get
//            {
//                return this.importeField;
//            }
//            set
//            {
//                this.importeField = value;
//            }
//        }
//    }

//    /// <comentarios/>
//    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.3038")]
//    [System.SerializableAttribute()]
//    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.sat.gob.mx/cfd/2")]
//    public enum ComprobanteImpuestosRetencionImpuesto
//    {

//        /// <comentarios/>
//        ISR,

//        /// <comentarios/>
//        IVA,
//    }

//    /// <comentarios/>
//    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.3038")]
//    [System.SerializableAttribute()]
//    [System.Diagnostics.DebuggerStepThroughAttribute()]
//    [System.ComponentModel.DesignerCategoryAttribute("code")]
//    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.sat.gob.mx/cfd/2")]
//    public partial class ComprobanteImpuestosTraslado
//    {

//        private ComprobanteImpuestosTrasladoImpuesto impuestoField;

//        private decimal tasaField;

//        private decimal importeField;

//        /// <comentarios/>
//        [System.Xml.Serialization.XmlAttributeAttribute()]
//        public ComprobanteImpuestosTrasladoImpuesto impuesto
//        {
//            get
//            {
//                return this.impuestoField;
//            }
//            set
//            {
//                this.impuestoField = value;
//            }
//        }

//        /// <comentarios/>
//        [System.Xml.Serialization.XmlAttributeAttribute()]
//        public decimal tasa
//        {
//            get
//            {
//                return this.tasaField;
//            }
//            set
//            {
//                this.tasaField = value;
//            }
//        }

//        /// <comentarios/>
//        [System.Xml.Serialization.XmlAttributeAttribute()]
//        public decimal importe
//        {
//            get
//            {
//                return this.importeField;
//            }
//            set
//            {
//                this.importeField = value;
//            }
//        }
//    }

//    /// <comentarios/>
//    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.3038")]
//    [System.SerializableAttribute()]
//    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.sat.gob.mx/cfd/2")]
//    public enum ComprobanteImpuestosTrasladoImpuesto
//    {

//        /// <comentarios/>
//        IVA,

//        /// <comentarios/>
//        IEPS,
//    }

//    /// <comentarios/>
//    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.3038")]
//    [System.SerializableAttribute()]
//    [System.Diagnostics.DebuggerStepThroughAttribute()]
//    [System.ComponentModel.DesignerCategoryAttribute("code")]
//    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.sat.gob.mx/cfd/2")]
//    public partial class ComprobanteComplemento
//    {

//        private System.Xml.XmlElement[] anyField;

//        /// <comentarios/>
//        [System.Xml.Serialization.XmlAnyElementAttribute()]
//        public System.Xml.XmlElement[] Any
//        {
//            get
//            {
//                return this.anyField;
//            }
//            set
//            {
//                this.anyField = value;
//            }
//        }
//    }

//    /// <comentarios/>
//    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.3038")]
//    [System.SerializableAttribute()]
//    [System.Diagnostics.DebuggerStepThroughAttribute()]
//    [System.ComponentModel.DesignerCategoryAttribute("code")]
//    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.sat.gob.mx/cfd/2")]
//    public partial class ComprobanteAddenda
//    {

//        private ComprobanteAddendaRequestForPayment requestForPaymentField;

//        /// <comentarios/>
//        public ComprobanteAddendaRequestForPayment requestForPayment
//        {
//            get
//            {
//                return this.requestForPaymentField;
//            }
//            set
//            {
//                this.requestForPaymentField = value;
//            }
//        }
//    }

//    /// <comentarios/>
//    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.3038")]
//    [System.SerializableAttribute()]
//    [System.Diagnostics.DebuggerStepThroughAttribute()]
//    [System.ComponentModel.DesignerCategoryAttribute("code")]
//    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.sat.gob.mx/cfd/2")]
//    public partial class ComprobanteAddendaRequestForPayment
//    {

//        private ComprobanteAddendaRequestForPaymentRequestForPaymentIdentification requestForPaymentIdentificationField;

//        private ComprobanteAddendaRequestForPaymentSpecialInstruction[] specialInstructionField;

//        private ComprobanteAddendaRequestForPaymentOrderIdentification orderIdentificationField;

//        private ComprobanteAddendaRequestForPaymentReferenceIdentification[] additionalInformationField;

//        private ComprobanteAddendaRequestForPaymentDeliveryNote deliveryNoteField;

//        private ComprobanteAddendaRequestForPaymentBuyer buyerField;

//        private ComprobanteAddendaRequestForPaymentSeller sellerField;

//        private ComprobanteAddendaRequestForPaymentShipTo shipToField;

//        private ComprobanteAddendaRequestForPaymentInvoiceCreator invoiceCreatorField;

//        private ComprobanteAddendaRequestForPaymentCustoms[] customsField;

//        private ComprobanteAddendaRequestForPaymentCurrency[] currencyField;

//        private ComprobanteAddendaRequestForPaymentPaymentTerms paymentTermsField;

//        private object shipmentDetailField;

//        private ComprobanteAddendaRequestForPaymentAllowanceCharge[] allowanceChargeField;

//        private ComprobanteAddendaRequestForPaymentLineItem[] lineItemField;

//        private ComprobanteAddendaRequestForPaymentTotalAmount totalAmountField;

//        private ComprobanteAddendaRequestForPaymentTotalAllowanceCharge[] totalAllowanceChargeField;

//        private ComprobanteAddendaRequestForPaymentBaseAmount baseAmountField;

//        private ComprobanteAddendaRequestForPaymentTax[] taxField;

//        private ComprobanteAddendaRequestForPaymentPayableAmount payableAmountField;

//        private string typeField;

//        private string contentVersionField;

//        private string documentStructureVersionField;

//        private ComprobanteAddendaRequestForPaymentDocumentStatus documentStatusField;

//        private System.DateTime deliveryDateField;

//        private bool deliveryDateFieldSpecified;

//        public ComprobanteAddendaRequestForPayment()
//        {
//            this.typeField = "SimpleInvoiceType";
//            this.type = "SimpleInvoiceType";
//            this.contentVersionField = "1.3.1";
//        }

//        /// <comentarios/>
//        public ComprobanteAddendaRequestForPaymentRequestForPaymentIdentification requestForPaymentIdentification
//        {
//            get
//            {
//                return this.requestForPaymentIdentificationField;
//            }
//            set
//            {
//                this.requestForPaymentIdentificationField = value;
//            }
//        }

//        /// <comentarios/>
//        [System.Xml.Serialization.XmlElementAttribute("specialInstruction")]
//        public ComprobanteAddendaRequestForPaymentSpecialInstruction[] specialInstruction
//        {
//            get
//            {
//                return this.specialInstructionField;
//            }
//            set
//            {
//                this.specialInstructionField = value;
//            }
//        }

//        /// <comentarios/>
//        public ComprobanteAddendaRequestForPaymentOrderIdentification orderIdentification
//        {
//            get
//            {
//                return this.orderIdentificationField;
//            }
//            set
//            {
//                this.orderIdentificationField = value;
//            }
//        }

//        /// <comentarios/>
//        [System.Xml.Serialization.XmlArrayItemAttribute("referenceIdentification", IsNullable = false)]
//        public ComprobanteAddendaRequestForPaymentReferenceIdentification[] AdditionalInformation
//        {
//            get
//            {
//                return this.additionalInformationField;
//            }
//            set
//            {
//                this.additionalInformationField = value;
//            }
//        }

//        /// <comentarios/>
//        public ComprobanteAddendaRequestForPaymentDeliveryNote DeliveryNote
//        {
//            get
//            {
//                return this.deliveryNoteField;
//            }
//            set
//            {
//                this.deliveryNoteField = value;
//            }
//        }

//        /// <comentarios/>
//        public ComprobanteAddendaRequestForPaymentBuyer buyer
//        {
//            get
//            {
//                return this.buyerField;
//            }
//            set
//            {
//                this.buyerField = value;
//            }
//        }

//        /// <comentarios/>
//        public ComprobanteAddendaRequestForPaymentSeller seller
//        {
//            get
//            {
//                return this.sellerField;
//            }
//            set
//            {
//                this.sellerField = value;
//            }
//        }

//        /// <comentarios/>
//        public ComprobanteAddendaRequestForPaymentShipTo shipTo
//        {
//            get
//            {
//                return this.shipToField;
//            }
//            set
//            {
//                this.shipToField = value;
//            }
//        }

//        /// <comentarios/>
//        public ComprobanteAddendaRequestForPaymentInvoiceCreator InvoiceCreator
//        {
//            get
//            {
//                return this.invoiceCreatorField;
//            }
//            set
//            {
//                this.invoiceCreatorField = value;
//            }
//        }

//        /// <comentarios/>
//        [System.Xml.Serialization.XmlElementAttribute("Customs")]
//        public ComprobanteAddendaRequestForPaymentCustoms[] Customs
//        {
//            get
//            {
//                return this.customsField;
//            }
//            set
//            {
//                this.customsField = value;
//            }
//        }

//        /// <comentarios/>
//        [System.Xml.Serialization.XmlElementAttribute("currency")]
//        public ComprobanteAddendaRequestForPaymentCurrency[] currency
//        {
//            get
//            {
//                return this.currencyField;
//            }
//            set
//            {
//                this.currencyField = value;
//            }
//        }

//        /// <comentarios/>
//        public ComprobanteAddendaRequestForPaymentPaymentTerms paymentTerms
//        {
//            get
//            {
//                return this.paymentTermsField;
//            }
//            set
//            {
//                this.paymentTermsField = value;
//            }
//        }

//        /// <comentarios/>
//        public object shipmentDetail
//        {
//            get
//            {
//                return this.shipmentDetailField;
//            }
//            set
//            {
//                this.shipmentDetailField = value;
//            }
//        }

//        /// <comentarios/>
//        [System.Xml.Serialization.XmlElementAttribute("allowanceCharge")]
//        public ComprobanteAddendaRequestForPaymentAllowanceCharge[] allowanceCharge
//        {
//            get
//            {
//                return this.allowanceChargeField;
//            }
//            set
//            {
//                this.allowanceChargeField = value;
//            }
//        }

//        /// <comentarios/>
//        [System.Xml.Serialization.XmlElementAttribute("lineItem")]
//        public ComprobanteAddendaRequestForPaymentLineItem[] lineItem
//        {
//            get
//            {
//                return this.lineItemField;
//            }
//            set
//            {
//                this.lineItemField = value;
//            }
//        }

//        /// <comentarios/>
//        public ComprobanteAddendaRequestForPaymentTotalAmount totalAmount
//        {
//            get
//            {
//                return this.totalAmountField;
//            }
//            set
//            {
//                this.totalAmountField = value;
//            }
//        }

//        /// <comentarios/>
//        [System.Xml.Serialization.XmlElementAttribute("TotalAllowanceCharge")]
//        public ComprobanteAddendaRequestForPaymentTotalAllowanceCharge[] TotalAllowanceCharge
//        {
//            get
//            {
//                return this.totalAllowanceChargeField;
//            }
//            set
//            {
//                this.totalAllowanceChargeField = value;
//            }
//        }

//        /// <comentarios/>
//        public ComprobanteAddendaRequestForPaymentBaseAmount baseAmount
//        {
//            get
//            {
//                return this.baseAmountField;
//            }
//            set
//            {
//                this.baseAmountField = value;
//            }
//        }

//        /// <comentarios/>
//        [System.Xml.Serialization.XmlElementAttribute("tax")]
//        public ComprobanteAddendaRequestForPaymentTax[] tax
//        {
//            get
//            {
//                return this.taxField;
//            }
//            set
//            {
//                this.taxField = value;
//            }
//        }

//        /// <comentarios/>
//        public ComprobanteAddendaRequestForPaymentPayableAmount payableAmount
//        {
//            get
//            {
//                return this.payableAmountField;
//            }
//            set
//            {
//                this.payableAmountField = value;
//            }
//        }

//        /// <comentarios/>
//        [System.Xml.Serialization.XmlAttributeAttribute()]
//        [System.ComponentModel.DefaultValueAttribute("SimpleInvoiceType")]
//        public string type
//        {
//            get
//            {
//                return this.typeField;
//            }
//            set
//            {
//                this.typeField = value;
//            }
//        }

//        /// <comentarios/>
//        [System.Xml.Serialization.XmlAttributeAttribute()]
//        [System.ComponentModel.DefaultValueAttribute("1.3.1")]
//        public string contentVersion
//        {
//            get
//            {
//                return this.contentVersionField;
//            }
//            set
//            {
//                this.contentVersionField = value;
//            }
//        }

//        /// <comentarios/>
//        [System.Xml.Serialization.XmlAttributeAttribute()]
//        public string documentStructureVersion
//        {
//            get
//            {
//                return this.documentStructureVersionField;
//            }
//            set
//            {
//                this.documentStructureVersionField = value;
//            }
//        }

//        /// <comentarios/>
//        [System.Xml.Serialization.XmlAttributeAttribute()]
//        public ComprobanteAddendaRequestForPaymentDocumentStatus documentStatus
//        {
//            get
//            {
//                return this.documentStatusField;
//            }
//            set
//            {
//                this.documentStatusField = value;
//            }
//        }

//        /// <comentarios/>
//        [System.Xml.Serialization.XmlAttributeAttribute(DataType = "date")]
//        public System.DateTime DeliveryDate
//        {
//            get
//            {
//                return this.deliveryDateField;
//            }
//            set
//            {
//                this.deliveryDateField = value;
//            }
//        }

//        /// <comentarios/>
//        [System.Xml.Serialization.XmlIgnoreAttribute()]
//        public bool DeliveryDateSpecified
//        {
//            get
//            {
//                return this.deliveryDateFieldSpecified;
//            }
//            set
//            {
//                this.deliveryDateFieldSpecified = value;
//            }
//        }
//    }

//    /// <comentarios/>
//    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.3038")]
//    [System.SerializableAttribute()]
//    [System.Diagnostics.DebuggerStepThroughAttribute()]
//    [System.ComponentModel.DesignerCategoryAttribute("code")]
//    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.sat.gob.mx/cfd/2")]
//    public partial class ComprobanteAddendaRequestForPaymentRequestForPaymentIdentification
//    {

//        private ComprobanteAddendaRequestForPaymentRequestForPaymentIdentificationEntityType entityTypeField;

//        private string uniqueCreatorIdentificationField;

//        /// <comentarios/>
//        public ComprobanteAddendaRequestForPaymentRequestForPaymentIdentificationEntityType entityType
//        {
//            get
//            {
//                return this.entityTypeField;
//            }
//            set
//            {
//                this.entityTypeField = value;
//            }
//        }

//        /// <comentarios/>
//        public string uniqueCreatorIdentification
//        {
//            get
//            {
//                return this.uniqueCreatorIdentificationField;
//            }
//            set
//            {
//                this.uniqueCreatorIdentificationField = value;
//            }
//        }
//    }

//    /// <comentarios/>
//    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.3038")]
//    [System.SerializableAttribute()]
//    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.sat.gob.mx/cfd/2")]
//    public enum ComprobanteAddendaRequestForPaymentRequestForPaymentIdentificationEntityType
//    {

//        /// <comentarios/>
//        INVOICE,

//        /// <comentarios/>
//        DEBIT_NOTE,

//        /// <comentarios/>
//        CREDIT_NOTE,

//        /// <comentarios/>
//        LEASE_RECEIPT,

//        /// <comentarios/>
//        HONORARY_RECEIPT,

//        /// <comentarios/>
//        PARTIAL_INVOICE,

//        /// <comentarios/>
//        TRANSPORT_DOCUMENT,

//        /// <comentarios/>
//        AUTO_INVOICE,
//    }

//    /// <comentarios/>
//    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.3038")]
//    [System.SerializableAttribute()]
//    [System.Diagnostics.DebuggerStepThroughAttribute()]
//    [System.ComponentModel.DesignerCategoryAttribute("code")]
//    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.sat.gob.mx/cfd/2")]
//    public partial class ComprobanteAddendaRequestForPaymentSpecialInstruction
//    {

//        private string[] textField;

//        private string[] textField1;

//        private ComprobanteAddendaRequestForPaymentSpecialInstructionCode codeField;

//        /// <comentarios/>
//        [System.Xml.Serialization.XmlElementAttribute("text")]
//        public string[] text
//        {
//            get
//            {
//                return this.textField;
//            }
//            set
//            {
//                this.textField = value;
//            }
//        }

//        /// <comentarios/>
//        [System.Xml.Serialization.XmlTextAttribute()]
//        public string[] Text
//        {
//            get
//            {
//                return this.textField1;
//            }
//            set
//            {
//                this.textField1 = value;
//            }
//        }

//        /// <comentarios/>
//        [System.Xml.Serialization.XmlAttributeAttribute()]
//        public ComprobanteAddendaRequestForPaymentSpecialInstructionCode code
//        {
//            get
//            {
//                return this.codeField;
//            }
//            set
//            {
//                this.codeField = value;
//            }
//        }
//    }

//    /// <comentarios/>
//    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.3038")]
//    [System.SerializableAttribute()]
//    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.sat.gob.mx/cfd/2")]
//    public enum ComprobanteAddendaRequestForPaymentSpecialInstructionCode
//    {

//        /// <comentarios/>
//        AAB,

//        /// <comentarios/>
//        DUT,

//        /// <comentarios/>
//        PUR,

//        /// <comentarios/>
//        ZZZ,
//    }

//    /// <comentarios/>
//    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.3038")]
//    [System.SerializableAttribute()]
//    [System.Diagnostics.DebuggerStepThroughAttribute()]
//    [System.ComponentModel.DesignerCategoryAttribute("code")]
//    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.sat.gob.mx/cfd/2")]
//    public partial class ComprobanteAddendaRequestForPaymentOrderIdentification
//    {

//        private ComprobanteAddendaRequestForPaymentOrderIdentificationReferenceIdentification[] referenceIdentificationField;

//        private System.DateTime referenceDateField;

//        private bool referenceDateFieldSpecified;

//        /// <comentarios/>
//        [System.Xml.Serialization.XmlElementAttribute("referenceIdentification")]
//        public ComprobanteAddendaRequestForPaymentOrderIdentificationReferenceIdentification[] referenceIdentification
//        {
//            get
//            {
//                return this.referenceIdentificationField;
//            }
//            set
//            {
//                this.referenceIdentificationField = value;
//            }
//        }

//        /// <comentarios/>
//        [System.Xml.Serialization.XmlElementAttribute(DataType = "date")]
//        public System.DateTime ReferenceDate
//        {
//            get
//            {
//                return this.referenceDateField;
//            }
//            set
//            {
//                this.referenceDateField = value;
//            }
//        }

//        /// <comentarios/>
//        [System.Xml.Serialization.XmlIgnoreAttribute()]
//        public bool ReferenceDateSpecified
//        {
//            get
//            {
//                return this.referenceDateFieldSpecified;
//            }
//            set
//            {
//                this.referenceDateFieldSpecified = value;
//            }
//        }
//    }

//    /// <comentarios/>
//    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.3038")]
//    [System.SerializableAttribute()]
//    [System.Diagnostics.DebuggerStepThroughAttribute()]
//    [System.ComponentModel.DesignerCategoryAttribute("code")]
//    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.sat.gob.mx/cfd/2")]
//    public partial class ComprobanteAddendaRequestForPaymentOrderIdentificationReferenceIdentification
//    {

//        private ComprobanteAddendaRequestForPaymentOrderIdentificationReferenceIdentificationType typeField;

//        private string valueField;

//        public ComprobanteAddendaRequestForPaymentOrderIdentificationReferenceIdentification()
//        {
//            this.typeField = ComprobanteAddendaRequestForPaymentOrderIdentificationReferenceIdentificationType.ON;
//        }

//        /// <comentarios/>
//        [System.Xml.Serialization.XmlAttributeAttribute()]
//        public ComprobanteAddendaRequestForPaymentOrderIdentificationReferenceIdentificationType type
//        {
//            get
//            {
//                return this.typeField;
//            }
//            set
//            {
//                this.typeField = value;
//            }
//        }

//        /// <comentarios/>
//        [System.Xml.Serialization.XmlTextAttribute()]
//        public string Value
//        {
//            get
//            {
//                return this.valueField;
//            }
//            set
//            {
//                this.valueField = value;
//            }
//        }
//    }

//    /// <comentarios/>
//    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.3038")]
//    [System.SerializableAttribute()]
//    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.sat.gob.mx/cfd/2")]
//    public enum ComprobanteAddendaRequestForPaymentOrderIdentificationReferenceIdentificationType
//    {

//        /// <comentarios/>
//        ON,
//    }

//    /// <comentarios/>
//    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.3038")]
//    [System.SerializableAttribute()]
//    [System.Diagnostics.DebuggerStepThroughAttribute()]
//    [System.ComponentModel.DesignerCategoryAttribute("code")]
//    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.sat.gob.mx/cfd/2")]
//    public partial class ComprobanteAddendaRequestForPaymentReferenceIdentification
//    {

//        private ComprobanteAddendaRequestForPaymentReferenceIdentificationType typeField;

//        private string valueField;

//        /// <comentarios/>
//        [System.Xml.Serialization.XmlAttributeAttribute()]
//        public ComprobanteAddendaRequestForPaymentReferenceIdentificationType type
//        {
//            get
//            {
//                return this.typeField;
//            }
//            set
//            {
//                this.typeField = value;
//            }
//        }

//        /// <comentarios/>
//        [System.Xml.Serialization.XmlTextAttribute()]
//        public string Value
//        {
//            get
//            {
//                return this.valueField;
//            }
//            set
//            {
//                this.valueField = value;
//            }
//        }
//    }

//    /// <comentarios/>
//    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.3038")]
//    [System.SerializableAttribute()]
//    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.sat.gob.mx/cfd/2")]
//    public enum ComprobanteAddendaRequestForPaymentReferenceIdentificationType
//    {

//        /// <comentarios/>
//        AAE,

//        /// <comentarios/>
//        CK,

//        /// <comentarios/>
//        ACE,

//        /// <comentarios/>
//        ATZ,

//        /// <comentarios/>
//        DQ,

//        /// <comentarios/>
//        IV,

//        /// <comentarios/>
//        ON,

//        /// <comentarios/>
//        AWR,
//    }

//    /// <comentarios/>
//    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.3038")]
//    [System.SerializableAttribute()]
//    [System.Diagnostics.DebuggerStepThroughAttribute()]
//    [System.ComponentModel.DesignerCategoryAttribute("code")]
//    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.sat.gob.mx/cfd/2")]
//    public partial class ComprobanteAddendaRequestForPaymentDeliveryNote
//    {

//        private string[] referenceIdentificationField;

//        private System.DateTime referenceDateField;

//        private bool referenceDateFieldSpecified;

//        /// <comentarios/>
//        [System.Xml.Serialization.XmlElementAttribute("referenceIdentification")]
//        public string[] referenceIdentification
//        {
//            get
//            {
//                return this.referenceIdentificationField;
//            }
//            set
//            {
//                this.referenceIdentificationField = value;
//            }
//        }

//        /// <comentarios/>
//        [System.Xml.Serialization.XmlElementAttribute(DataType = "date")]
//        public System.DateTime ReferenceDate
//        {
//            get
//            {
//                return this.referenceDateField;
//            }
//            set
//            {
//                this.referenceDateField = value;
//            }
//        }

//        /// <comentarios/>
//        [System.Xml.Serialization.XmlIgnoreAttribute()]
//        public bool ReferenceDateSpecified
//        {
//            get
//            {
//                return this.referenceDateFieldSpecified;
//            }
//            set
//            {
//                this.referenceDateFieldSpecified = value;
//            }
//        }
//    }

//    /// <comentarios/>
//    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.3038")]
//    [System.SerializableAttribute()]
//    [System.Diagnostics.DebuggerStepThroughAttribute()]
//    [System.ComponentModel.DesignerCategoryAttribute("code")]
//    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.sat.gob.mx/cfd/2")]
//    public partial class ComprobanteAddendaRequestForPaymentBuyer
//    {

//        private string glnField;

//        private ComprobanteAddendaRequestForPaymentBuyerContactInformation contactInformationField;

//        /// <comentarios/>
//        public string gln
//        {
//            get
//            {
//                return this.glnField;
//            }
//            set
//            {
//                this.glnField = value;
//            }
//        }

//        /// <comentarios/>
//        public ComprobanteAddendaRequestForPaymentBuyerContactInformation contactInformation
//        {
//            get
//            {
//                return this.contactInformationField;
//            }
//            set
//            {
//                this.contactInformationField = value;
//            }
//        }
//    }

//    /// <comentarios/>
//    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.3038")]
//    [System.SerializableAttribute()]
//    [System.Diagnostics.DebuggerStepThroughAttribute()]
//    [System.ComponentModel.DesignerCategoryAttribute("code")]
//    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.sat.gob.mx/cfd/2")]
//    public partial class ComprobanteAddendaRequestForPaymentBuyerContactInformation
//    {

//        private ComprobanteAddendaRequestForPaymentBuyerContactInformationPersonOrDepartmentName personOrDepartmentNameField;

//        /// <comentarios/>
//        public ComprobanteAddendaRequestForPaymentBuyerContactInformationPersonOrDepartmentName personOrDepartmentName
//        {
//            get
//            {
//                return this.personOrDepartmentNameField;
//            }
//            set
//            {
//                this.personOrDepartmentNameField = value;
//            }
//        }
//    }

//    /// <comentarios/>
//    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.3038")]
//    [System.SerializableAttribute()]
//    [System.Diagnostics.DebuggerStepThroughAttribute()]
//    [System.ComponentModel.DesignerCategoryAttribute("code")]
//    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.sat.gob.mx/cfd/2")]
//    public partial class ComprobanteAddendaRequestForPaymentBuyerContactInformationPersonOrDepartmentName
//    {

//        private string textField;

//        /// <comentarios/>
//        public string text
//        {
//            get
//            {
//                return this.textField;
//            }
//            set
//            {
//                this.textField = value;
//            }
//        }
//    }

//    /// <comentarios/>
//    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.3038")]
//    [System.SerializableAttribute()]
//    [System.Diagnostics.DebuggerStepThroughAttribute()]
//    [System.ComponentModel.DesignerCategoryAttribute("code")]
//    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.sat.gob.mx/cfd/2")]
//    public partial class ComprobanteAddendaRequestForPaymentSeller
//    {

//        private string glnField;

//        private ComprobanteAddendaRequestForPaymentSellerAlternatePartyIdentification alternatePartyIdentificationField;

//        /// <comentarios/>
//        public string gln
//        {
//            get
//            {
//                return this.glnField;
//            }
//            set
//            {
//                this.glnField = value;
//            }
//        }

//        /// <comentarios/>
//        public ComprobanteAddendaRequestForPaymentSellerAlternatePartyIdentification alternatePartyIdentification
//        {
//            get
//            {
//                return this.alternatePartyIdentificationField;
//            }
//            set
//            {
//                this.alternatePartyIdentificationField = value;
//            }
//        }
//    }

//    /// <comentarios/>
//    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.3038")]
//    [System.SerializableAttribute()]
//    [System.Diagnostics.DebuggerStepThroughAttribute()]
//    [System.ComponentModel.DesignerCategoryAttribute("code")]
//    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.sat.gob.mx/cfd/2")]
//    public partial class ComprobanteAddendaRequestForPaymentSellerAlternatePartyIdentification
//    {

//        private ComprobanteAddendaRequestForPaymentSellerAlternatePartyIdentificationType typeField;

//        private string valueField;

//        /// <comentarios/>
//        [System.Xml.Serialization.XmlAttributeAttribute()]
//        public ComprobanteAddendaRequestForPaymentSellerAlternatePartyIdentificationType type
//        {
//            get
//            {
//                return this.typeField;
//            }
//            set
//            {
//                this.typeField = value;
//            }
//        }

//        /// <comentarios/>
//        [System.Xml.Serialization.XmlTextAttribute()]
//        public string Value
//        {
//            get
//            {
//                return this.valueField;
//            }
//            set
//            {
//                this.valueField = value;
//            }
//        }
//    }

//    /// <comentarios/>
//    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.3038")]
//    [System.SerializableAttribute()]
//    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.sat.gob.mx/cfd/2")]
//    public enum ComprobanteAddendaRequestForPaymentSellerAlternatePartyIdentificationType
//    {

//        /// <comentarios/>
//        SELLER_ASSIGNED_IDENTIFIER_FOR_A_PARTY,

//        /// <comentarios/>
//        IEPS_REFERENCE,
//    }

//    /// <comentarios/>
//    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.3038")]
//    [System.SerializableAttribute()]
//    [System.Diagnostics.DebuggerStepThroughAttribute()]
//    [System.ComponentModel.DesignerCategoryAttribute("code")]
//    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.sat.gob.mx/cfd/2")]
//    public partial class ComprobanteAddendaRequestForPaymentShipTo
//    {

//        private string glnField;

//        private ComprobanteAddendaRequestForPaymentShipToNameAndAddress nameAndAddressField;

//        /// <comentarios/>
//        public string gln
//        {
//            get
//            {
//                return this.glnField;
//            }
//            set
//            {
//                this.glnField = value;
//            }
//        }

//        /// <comentarios/>
//        public ComprobanteAddendaRequestForPaymentShipToNameAndAddress nameAndAddress
//        {
//            get
//            {
//                return this.nameAndAddressField;
//            }
//            set
//            {
//                this.nameAndAddressField = value;
//            }
//        }
//    }

//    /// <comentarios/>
//    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.3038")]
//    [System.SerializableAttribute()]
//    [System.Diagnostics.DebuggerStepThroughAttribute()]
//    [System.ComponentModel.DesignerCategoryAttribute("code")]
//    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.sat.gob.mx/cfd/2")]
//    public partial class ComprobanteAddendaRequestForPaymentShipToNameAndAddress
//    {

//        private string[] nameField;

//        private string[] streetAddressOneField;

//        private string[] cityField;

//        private string[] postalCodeField;

//        /// <comentarios/>
//        [System.Xml.Serialization.XmlElementAttribute("name")]
//        public string[] name
//        {
//            get
//            {
//                return this.nameField;
//            }
//            set
//            {
//                this.nameField = value;
//            }
//        }

//        /// <comentarios/>
//        [System.Xml.Serialization.XmlElementAttribute("streetAddressOne")]
//        public string[] streetAddressOne
//        {
//            get
//            {
//                return this.streetAddressOneField;
//            }
//            set
//            {
//                this.streetAddressOneField = value;
//            }
//        }

//        /// <comentarios/>
//        [System.Xml.Serialization.XmlElementAttribute("city")]
//        public string[] city
//        {
//            get
//            {
//                return this.cityField;
//            }
//            set
//            {
//                this.cityField = value;
//            }
//        }

//        /// <comentarios/>
//        [System.Xml.Serialization.XmlElementAttribute("postalCode")]
//        public string[] postalCode
//        {
//            get
//            {
//                return this.postalCodeField;
//            }
//            set
//            {
//                this.postalCodeField = value;
//            }
//        }
//    }

//    /// <comentarios/>
//    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.3038")]
//    [System.SerializableAttribute()]
//    [System.Diagnostics.DebuggerStepThroughAttribute()]
//    [System.ComponentModel.DesignerCategoryAttribute("code")]
//    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.sat.gob.mx/cfd/2")]
//    public partial class ComprobanteAddendaRequestForPaymentInvoiceCreator
//    {

//        private string glnField;

//        private ComprobanteAddendaRequestForPaymentInvoiceCreatorAlternatePartyIdentification alternatePartyIdentificationField;

//        private ComprobanteAddendaRequestForPaymentInvoiceCreatorNameAndAddress nameAndAddressField;

//        /// <comentarios/>
//        public string gln
//        {
//            get
//            {
//                return this.glnField;
//            }
//            set
//            {
//                this.glnField = value;
//            }
//        }

//        /// <comentarios/>
//        public ComprobanteAddendaRequestForPaymentInvoiceCreatorAlternatePartyIdentification alternatePartyIdentification
//        {
//            get
//            {
//                return this.alternatePartyIdentificationField;
//            }
//            set
//            {
//                this.alternatePartyIdentificationField = value;
//            }
//        }

//        /// <comentarios/>
//        public ComprobanteAddendaRequestForPaymentInvoiceCreatorNameAndAddress nameAndAddress
//        {
//            get
//            {
//                return this.nameAndAddressField;
//            }
//            set
//            {
//                this.nameAndAddressField = value;
//            }
//        }
//    }

//    /// <comentarios/>
//    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.3038")]
//    [System.SerializableAttribute()]
//    [System.Diagnostics.DebuggerStepThroughAttribute()]
//    [System.ComponentModel.DesignerCategoryAttribute("code")]
//    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.sat.gob.mx/cfd/2")]
//    public partial class ComprobanteAddendaRequestForPaymentInvoiceCreatorAlternatePartyIdentification
//    {

//        private ComprobanteAddendaRequestForPaymentInvoiceCreatorAlternatePartyIdentificationType typeField;

//        private string valueField;

//        /// <comentarios/>
//        [System.Xml.Serialization.XmlAttributeAttribute()]
//        public ComprobanteAddendaRequestForPaymentInvoiceCreatorAlternatePartyIdentificationType type
//        {
//            get
//            {
//                return this.typeField;
//            }
//            set
//            {
//                this.typeField = value;
//            }
//        }

//        /// <comentarios/>
//        [System.Xml.Serialization.XmlTextAttribute()]
//        public string Value
//        {
//            get
//            {
//                return this.valueField;
//            }
//            set
//            {
//                this.valueField = value;
//            }
//        }
//    }

//    /// <comentarios/>
//    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.3038")]
//    [System.SerializableAttribute()]
//    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.sat.gob.mx/cfd/2")]
//    public enum ComprobanteAddendaRequestForPaymentInvoiceCreatorAlternatePartyIdentificationType
//    {

//        /// <comentarios/>
//        VA,

//        /// <comentarios/>
//        IA,
//    }

//    /// <comentarios/>
//    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.3038")]
//    [System.SerializableAttribute()]
//    [System.Diagnostics.DebuggerStepThroughAttribute()]
//    [System.ComponentModel.DesignerCategoryAttribute("code")]
//    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.sat.gob.mx/cfd/2")]
//    public partial class ComprobanteAddendaRequestForPaymentInvoiceCreatorNameAndAddress
//    {

//        private string nameField;

//        private string streetAddressOneField;

//        private string cityField;

//        private string postalCodeField;

//        /// <comentarios/>
//        public string name
//        {
//            get
//            {
//                return this.nameField;
//            }
//            set
//            {
//                this.nameField = value;
//            }
//        }

//        /// <comentarios/>
//        public string streetAddressOne
//        {
//            get
//            {
//                return this.streetAddressOneField;
//            }
//            set
//            {
//                this.streetAddressOneField = value;
//            }
//        }

//        /// <comentarios/>
//        public string city
//        {
//            get
//            {
//                return this.cityField;
//            }
//            set
//            {
//                this.cityField = value;
//            }
//        }

//        /// <comentarios/>
//        public string postalCode
//        {
//            get
//            {
//                return this.postalCodeField;
//            }
//            set
//            {
//                this.postalCodeField = value;
//            }
//        }
//    }

//    /// <comentarios/>
//    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.3038")]
//    [System.SerializableAttribute()]
//    [System.Diagnostics.DebuggerStepThroughAttribute()]
//    [System.ComponentModel.DesignerCategoryAttribute("code")]
//    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.sat.gob.mx/cfd/2")]
//    public partial class ComprobanteAddendaRequestForPaymentCustoms
//    {

//        private string glnField;

//        private ComprobanteAddendaRequestForPaymentCustomsAlternatePartyIdentification alternatePartyIdentificationField;

//        private System.DateTime referenceDateField;

//        private ComprobanteAddendaRequestForPaymentCustomsNameAndAddress nameAndAddressField;

//        /// <comentarios/>
//        public string gln
//        {
//            get
//            {
//                return this.glnField;
//            }
//            set
//            {
//                this.glnField = value;
//            }
//        }

//        /// <comentarios/>
//        public ComprobanteAddendaRequestForPaymentCustomsAlternatePartyIdentification alternatePartyIdentification
//        {
//            get
//            {
//                return this.alternatePartyIdentificationField;
//            }
//            set
//            {
//                this.alternatePartyIdentificationField = value;
//            }
//        }

//        /// <comentarios/>
//        [System.Xml.Serialization.XmlElementAttribute(DataType = "date")]
//        public System.DateTime ReferenceDate
//        {
//            get
//            {
//                return this.referenceDateField;
//            }
//            set
//            {
//                this.referenceDateField = value;
//            }
//        }

//        /// <comentarios/>
//        public ComprobanteAddendaRequestForPaymentCustomsNameAndAddress nameAndAddress
//        {
//            get
//            {
//                return this.nameAndAddressField;
//            }
//            set
//            {
//                this.nameAndAddressField = value;
//            }
//        }
//    }

//    /// <comentarios/>
//    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.3038")]
//    [System.SerializableAttribute()]
//    [System.Diagnostics.DebuggerStepThroughAttribute()]
//    [System.ComponentModel.DesignerCategoryAttribute("code")]
//    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.sat.gob.mx/cfd/2")]
//    public partial class ComprobanteAddendaRequestForPaymentCustomsAlternatePartyIdentification
//    {

//        private ComprobanteAddendaRequestForPaymentCustomsAlternatePartyIdentificationType typeField;

//        private string valueField;

//        /// <comentarios/>
//        [System.Xml.Serialization.XmlAttributeAttribute()]
//        public ComprobanteAddendaRequestForPaymentCustomsAlternatePartyIdentificationType type
//        {
//            get
//            {
//                return this.typeField;
//            }
//            set
//            {
//                this.typeField = value;
//            }
//        }

//        /// <comentarios/>
//        [System.Xml.Serialization.XmlTextAttribute()]
//        public string Value
//        {
//            get
//            {
//                return this.valueField;
//            }
//            set
//            {
//                this.valueField = value;
//            }
//        }
//    }

//    /// <comentarios/>
//    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.3038")]
//    [System.SerializableAttribute()]
//    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.sat.gob.mx/cfd/2")]
//    public enum ComprobanteAddendaRequestForPaymentCustomsAlternatePartyIdentificationType
//    {

//        /// <comentarios/>
//        TN,
//    }

//    /// <comentarios/>
//    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.3038")]
//    [System.SerializableAttribute()]
//    [System.Diagnostics.DebuggerStepThroughAttribute()]
//    [System.ComponentModel.DesignerCategoryAttribute("code")]
//    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.sat.gob.mx/cfd/2")]
//    public partial class ComprobanteAddendaRequestForPaymentCustomsNameAndAddress
//    {

//        private string nameField;

//        private string cityField;

//        /// <comentarios/>
//        public string name
//        {
//            get
//            {
//                return this.nameField;
//            }
//            set
//            {
//                this.nameField = value;
//            }
//        }

//        /// <comentarios/>
//        public string city
//        {
//            get
//            {
//                return this.cityField;
//            }
//            set
//            {
//                this.cityField = value;
//            }
//        }
//    }

//    /// <comentarios/>
//    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.3038")]
//    [System.SerializableAttribute()]
//    [System.Diagnostics.DebuggerStepThroughAttribute()]
//    [System.ComponentModel.DesignerCategoryAttribute("code")]
//    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.sat.gob.mx/cfd/2")]
//    public partial class ComprobanteAddendaRequestForPaymentCurrency
//    {

//        private ComprobanteAddendaRequestForPaymentCurrencyCurrencyFunction[] currencyFunctionField;

//        private decimal rateOfChangeField;

//        private bool rateOfChangeFieldSpecified;

//        private ComprobanteAddendaRequestForPaymentCurrencyCurrencyISOCode currencyISOCodeField;

//        /// <comentarios/>
//        [System.Xml.Serialization.XmlElementAttribute("currencyFunction")]
//        public ComprobanteAddendaRequestForPaymentCurrencyCurrencyFunction[] currencyFunction
//        {
//            get
//            {
//                return this.currencyFunctionField;
//            }
//            set
//            {
//                this.currencyFunctionField = value;
//            }
//        }

//        /// <comentarios/>
//        public decimal rateOfChange
//        {
//            get
//            {
//                return this.rateOfChangeField;
//            }
//            set
//            {
//                this.rateOfChangeField = value;
//            }
//        }

//        /// <comentarios/>
//        [System.Xml.Serialization.XmlIgnoreAttribute()]
//        public bool rateOfChangeSpecified
//        {
//            get
//            {
//                return this.rateOfChangeFieldSpecified;
//            }
//            set
//            {
//                this.rateOfChangeFieldSpecified = value;
//            }
//        }

//        /// <comentarios/>
//        [System.Xml.Serialization.XmlAttributeAttribute()]
//        public ComprobanteAddendaRequestForPaymentCurrencyCurrencyISOCode currencyISOCode
//        {
//            get
//            {
//                return this.currencyISOCodeField;
//            }
//            set
//            {
//                this.currencyISOCodeField = value;
//            }
//        }
//    }

//    /// <comentarios/>
//    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.3038")]
//    [System.SerializableAttribute()]
//    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.sat.gob.mx/cfd/2")]
//    public enum ComprobanteAddendaRequestForPaymentCurrencyCurrencyFunction
//    {

//        /// <comentarios/>
//        BILLING_CURRENCY,

//        /// <comentarios/>
//        PRICE_CURRENCY,

//        /// <comentarios/>
//        PAYMENT_CURRENCY,
//    }

//    /// <comentarios/>
//    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.3038")]
//    [System.SerializableAttribute()]
//    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.sat.gob.mx/cfd/2")]
//    public enum ComprobanteAddendaRequestForPaymentCurrencyCurrencyISOCode
//    {

//        /// <comentarios/>
//        MXN,

//        /// <comentarios/>
//        XEU,

//        /// <comentarios/>
//        USD,
//    }

//    /// <comentarios/>
//    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.3038")]
//    [System.SerializableAttribute()]
//    [System.Diagnostics.DebuggerStepThroughAttribute()]
//    [System.ComponentModel.DesignerCategoryAttribute("code")]
//    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.sat.gob.mx/cfd/2")]
//    public partial class ComprobanteAddendaRequestForPaymentPaymentTerms
//    {

//        private ComprobanteAddendaRequestForPaymentPaymentTermsNetPayment netPaymentField;

//        private ComprobanteAddendaRequestForPaymentPaymentTermsDiscountPayment discountPaymentField;

//        private ComprobanteAddendaRequestForPaymentPaymentTermsPaymentTermsEvent paymentTermsEventField;

//        private bool paymentTermsEventFieldSpecified;

//        private ComprobanteAddendaRequestForPaymentPaymentTermsPaymentTermsRelationTime paymentTermsRelationTimeField;

//        private bool paymentTermsRelationTimeFieldSpecified;

//        /// <comentarios/>
//        public ComprobanteAddendaRequestForPaymentPaymentTermsNetPayment netPayment
//        {
//            get
//            {
//                return this.netPaymentField;
//            }
//            set
//            {
//                this.netPaymentField = value;
//            }
//        }

//        /// <comentarios/>
//        public ComprobanteAddendaRequestForPaymentPaymentTermsDiscountPayment discountPayment
//        {
//            get
//            {
//                return this.discountPaymentField;
//            }
//            set
//            {
//                this.discountPaymentField = value;
//            }
//        }

//        /// <comentarios/>
//        [System.Xml.Serialization.XmlAttributeAttribute()]
//        public ComprobanteAddendaRequestForPaymentPaymentTermsPaymentTermsEvent paymentTermsEvent
//        {
//            get
//            {
//                return this.paymentTermsEventField;
//            }
//            set
//            {
//                this.paymentTermsEventField = value;
//            }
//        }

//        /// <comentarios/>
//        [System.Xml.Serialization.XmlIgnoreAttribute()]
//        public bool paymentTermsEventSpecified
//        {
//            get
//            {
//                return this.paymentTermsEventFieldSpecified;
//            }
//            set
//            {
//                this.paymentTermsEventFieldSpecified = value;
//            }
//        }

//        /// <comentarios/>
//        [System.Xml.Serialization.XmlAttributeAttribute()]
//        public ComprobanteAddendaRequestForPaymentPaymentTermsPaymentTermsRelationTime PaymentTermsRelationTime
//        {
//            get
//            {
//                return this.paymentTermsRelationTimeField;
//            }
//            set
//            {
//                this.paymentTermsRelationTimeField = value;
//            }
//        }

//        /// <comentarios/>
//        [System.Xml.Serialization.XmlIgnoreAttribute()]
//        public bool PaymentTermsRelationTimeSpecified
//        {
//            get
//            {
//                return this.paymentTermsRelationTimeFieldSpecified;
//            }
//            set
//            {
//                this.paymentTermsRelationTimeFieldSpecified = value;
//            }
//        }
//    }

//    /// <comentarios/>
//    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.3038")]
//    [System.SerializableAttribute()]
//    [System.Diagnostics.DebuggerStepThroughAttribute()]
//    [System.ComponentModel.DesignerCategoryAttribute("code")]
//    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.sat.gob.mx/cfd/2")]
//    public partial class ComprobanteAddendaRequestForPaymentPaymentTermsNetPayment
//    {

//        private ComprobanteAddendaRequestForPaymentPaymentTermsNetPaymentPaymentTimePeriod paymentTimePeriodField;

//        private ComprobanteAddendaRequestForPaymentPaymentTermsNetPaymentNetPaymentTermsType netPaymentTermsTypeField;

//        /// <comentarios/>
//        public ComprobanteAddendaRequestForPaymentPaymentTermsNetPaymentPaymentTimePeriod paymentTimePeriod
//        {
//            get
//            {
//                return this.paymentTimePeriodField;
//            }
//            set
//            {
//                this.paymentTimePeriodField = value;
//            }
//        }

//        /// <comentarios/>
//        [System.Xml.Serialization.XmlAttributeAttribute()]
//        public ComprobanteAddendaRequestForPaymentPaymentTermsNetPaymentNetPaymentTermsType netPaymentTermsType
//        {
//            get
//            {
//                return this.netPaymentTermsTypeField;
//            }
//            set
//            {
//                this.netPaymentTermsTypeField = value;
//            }
//        }
//    }

//    /// <comentarios/>
//    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.3038")]
//    [System.SerializableAttribute()]
//    [System.Diagnostics.DebuggerStepThroughAttribute()]
//    [System.ComponentModel.DesignerCategoryAttribute("code")]
//    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.sat.gob.mx/cfd/2")]
//    public partial class ComprobanteAddendaRequestForPaymentPaymentTermsNetPaymentPaymentTimePeriod
//    {

//        private ComprobanteAddendaRequestForPaymentPaymentTermsNetPaymentPaymentTimePeriodTimePeriodDue timePeriodDueField;

//        /// <comentarios/>
//        public ComprobanteAddendaRequestForPaymentPaymentTermsNetPaymentPaymentTimePeriodTimePeriodDue timePeriodDue
//        {
//            get
//            {
//                return this.timePeriodDueField;
//            }
//            set
//            {
//                this.timePeriodDueField = value;
//            }
//        }
//    }

//    /// <comentarios/>
//    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.3038")]
//    [System.SerializableAttribute()]
//    [System.Diagnostics.DebuggerStepThroughAttribute()]
//    [System.ComponentModel.DesignerCategoryAttribute("code")]
//    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.sat.gob.mx/cfd/2")]
//    public partial class ComprobanteAddendaRequestForPaymentPaymentTermsNetPaymentPaymentTimePeriodTimePeriodDue
//    {

//        private string valueField;

//        private ComprobanteAddendaRequestForPaymentPaymentTermsNetPaymentPaymentTimePeriodTimePeriodDueTimePeriod timePeriodField;

//        /// <comentarios/>
//        public string value
//        {
//            get
//            {
//                return this.valueField;
//            }
//            set
//            {
//                this.valueField = value;
//            }
//        }

//        /// <comentarios/>
//        [System.Xml.Serialization.XmlAttributeAttribute()]
//        public ComprobanteAddendaRequestForPaymentPaymentTermsNetPaymentPaymentTimePeriodTimePeriodDueTimePeriod timePeriod
//        {
//            get
//            {
//                return this.timePeriodField;
//            }
//            set
//            {
//                this.timePeriodField = value;
//            }
//        }
//    }

//    /// <comentarios/>
//    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.3038")]
//    [System.SerializableAttribute()]
//    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.sat.gob.mx/cfd/2")]
//    public enum ComprobanteAddendaRequestForPaymentPaymentTermsNetPaymentPaymentTimePeriodTimePeriodDueTimePeriod
//    {

//        /// <comentarios/>
//        DAYS,
//    }

//    /// <comentarios/>
//    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.3038")]
//    [System.SerializableAttribute()]
//    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.sat.gob.mx/cfd/2")]
//    public enum ComprobanteAddendaRequestForPaymentPaymentTermsNetPaymentNetPaymentTermsType
//    {

//        /// <comentarios/>
//        BASIC_NET,

//        /// <comentarios/>
//        END_OF_MONTH,

//        /// <comentarios/>
//        BASIC_DISCOUNT_OFFERED,
//    }

//    /// <comentarios/>
//    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.3038")]
//    [System.SerializableAttribute()]
//    [System.Diagnostics.DebuggerStepThroughAttribute()]
//    [System.ComponentModel.DesignerCategoryAttribute("code")]
//    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.sat.gob.mx/cfd/2")]
//    public partial class ComprobanteAddendaRequestForPaymentPaymentTermsDiscountPayment
//    {

//        private string percentageField;

//        private ComprobanteAddendaRequestForPaymentPaymentTermsDiscountPaymentDiscountType discountTypeField;

//        /// <comentarios/>
//        public string percentage
//        {
//            get
//            {
//                return this.percentageField;
//            }
//            set
//            {
//                this.percentageField = value;
//            }
//        }

//        /// <comentarios/>
//        [System.Xml.Serialization.XmlAttributeAttribute()]
//        public ComprobanteAddendaRequestForPaymentPaymentTermsDiscountPaymentDiscountType discountType
//        {
//            get
//            {
//                return this.discountTypeField;
//            }
//            set
//            {
//                this.discountTypeField = value;
//            }
//        }
//    }

//    /// <comentarios/>
//    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.3038")]
//    [System.SerializableAttribute()]
//    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.sat.gob.mx/cfd/2")]
//    public enum ComprobanteAddendaRequestForPaymentPaymentTermsDiscountPaymentDiscountType
//    {

//        /// <comentarios/>
//        ALLOWANCE_BY_PAYMENT_ON_TIME,

//        /// <comentarios/>
//        SANCTION,
//    }

//    /// <comentarios/>
//    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.3038")]
//    [System.SerializableAttribute()]
//    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.sat.gob.mx/cfd/2")]
//    public enum ComprobanteAddendaRequestForPaymentPaymentTermsPaymentTermsEvent
//    {

//        /// <comentarios/>
//        DATE_OF_INVOICE,

//        /// <comentarios/>
//        EFFECTIVE_DATE,
//    }

//    /// <comentarios/>
//    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.3038")]
//    [System.SerializableAttribute()]
//    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.sat.gob.mx/cfd/2")]
//    public enum ComprobanteAddendaRequestForPaymentPaymentTermsPaymentTermsRelationTime
//    {

//        /// <comentarios/>
//        REFERENCE_AFTER,
//    }

//    /// <comentarios/>
//    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.3038")]
//    [System.SerializableAttribute()]
//    [System.Diagnostics.DebuggerStepThroughAttribute()]
//    [System.ComponentModel.DesignerCategoryAttribute("code")]
//    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.sat.gob.mx/cfd/2")]
//    public partial class ComprobanteAddendaRequestForPaymentAllowanceCharge
//    {

//        private ComprobanteAddendaRequestForPaymentAllowanceChargeSpecialServicesType specialServicesTypeField;

//        private bool specialServicesTypeFieldSpecified;

//        private ComprobanteAddendaRequestForPaymentAllowanceChargeMonetaryAmountOrPercentage monetaryAmountOrPercentageField;

//        private ComprobanteAddendaRequestForPaymentAllowanceChargeAllowanceChargeType allowanceChargeTypeField;

//        private ComprobanteAddendaRequestForPaymentAllowanceChargeSettlementType settlementTypeField;

//        private string sequenceNumberField;

//        /// <comentarios/>
//        public ComprobanteAddendaRequestForPaymentAllowanceChargeSpecialServicesType specialServicesType
//        {
//            get
//            {
//                return this.specialServicesTypeField;
//            }
//            set
//            {
//                this.specialServicesTypeField = value;
//            }
//        }

//        /// <comentarios/>
//        [System.Xml.Serialization.XmlIgnoreAttribute()]
//        public bool specialServicesTypeSpecified
//        {
//            get
//            {
//                return this.specialServicesTypeFieldSpecified;
//            }
//            set
//            {
//                this.specialServicesTypeFieldSpecified = value;
//            }
//        }

//        /// <comentarios/>
//        public ComprobanteAddendaRequestForPaymentAllowanceChargeMonetaryAmountOrPercentage monetaryAmountOrPercentage
//        {
//            get
//            {
//                return this.monetaryAmountOrPercentageField;
//            }
//            set
//            {
//                this.monetaryAmountOrPercentageField = value;
//            }
//        }

//        /// <comentarios/>
//        [System.Xml.Serialization.XmlAttributeAttribute()]
//        public ComprobanteAddendaRequestForPaymentAllowanceChargeAllowanceChargeType allowanceChargeType
//        {
//            get
//            {
//                return this.allowanceChargeTypeField;
//            }
//            set
//            {
//                this.allowanceChargeTypeField = value;
//            }
//        }

//        /// <comentarios/>
//        [System.Xml.Serialization.XmlAttributeAttribute()]
//        public ComprobanteAddendaRequestForPaymentAllowanceChargeSettlementType settlementType
//        {
//            get
//            {
//                return this.settlementTypeField;
//            }
//            set
//            {
//                this.settlementTypeField = value;
//            }
//        }

//        /// <comentarios/>
//        [System.Xml.Serialization.XmlAttributeAttribute()]
//        public string sequenceNumber
//        {
//            get
//            {
//                return this.sequenceNumberField;
//            }
//            set
//            {
//                this.sequenceNumberField = value;
//            }
//        }
//    }

//    /// <comentarios/>
//    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.3038")]
//    [System.SerializableAttribute()]
//    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.sat.gob.mx/cfd/2")]
//    public enum ComprobanteAddendaRequestForPaymentAllowanceChargeSpecialServicesType
//    {

//        /// <comentarios/>
//        AA,

//        /// <comentarios/>
//        AJ,

//        /// <comentarios/>
//        ADO,

//        /// <comentarios/>
//        ADT,

//        /// <comentarios/>
//        ADS,

//        /// <comentarios/>
//        ABZ,

//        /// <comentarios/>
//        DA,

//        /// <comentarios/>
//        EAA,

//        /// <comentarios/>
//        EAB,

//        /// <comentarios/>
//        PI,

//        /// <comentarios/>
//        TAE,

//        /// <comentarios/>
//        SAB,

//        /// <comentarios/>
//        RAA,

//        /// <comentarios/>
//        PAD,

//        /// <comentarios/>
//        FG,

//        /// <comentarios/>
//        FA,

//        /// <comentarios/>
//        TD,

//        /// <comentarios/>
//        TS,

//        /// <comentarios/>
//        TX,

//        /// <comentarios/>
//        TZ,

//        /// <comentarios/>
//        ZZZ,

//        /// <comentarios/>
//        VAB,

//        /// <comentarios/>
//        UM,

//        /// <comentarios/>
//        DI,

//        /// <comentarios/>
//        CAC,

//        /// <comentarios/>
//        COD,

//        /// <comentarios/>
//        [System.Xml.Serialization.XmlEnumAttribute("EAB")]
//        EAB1,

//        /// <comentarios/>
//        FC,

//        /// <comentarios/>
//        FI,

//        /// <comentarios/>
//        HD,

//        /// <comentarios/>
//        QD,
//    }

//    /// <comentarios/>
//    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.3038")]
//    [System.SerializableAttribute()]
//    [System.Diagnostics.DebuggerStepThroughAttribute()]
//    [System.ComponentModel.DesignerCategoryAttribute("code")]
//    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.sat.gob.mx/cfd/2")]
//    public partial class ComprobanteAddendaRequestForPaymentAllowanceChargeMonetaryAmountOrPercentage
//    {

//        private ComprobanteAddendaRequestForPaymentAllowanceChargeMonetaryAmountOrPercentageRate rateField;

//        /// <comentarios/>
//        public ComprobanteAddendaRequestForPaymentAllowanceChargeMonetaryAmountOrPercentageRate rate
//        {
//            get
//            {
//                return this.rateField;
//            }
//            set
//            {
//                this.rateField = value;
//            }
//        }
//    }

//    /// <comentarios/>
//    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.3038")]
//    [System.SerializableAttribute()]
//    [System.Diagnostics.DebuggerStepThroughAttribute()]
//    [System.ComponentModel.DesignerCategoryAttribute("code")]
//    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.sat.gob.mx/cfd/2")]
//    public partial class ComprobanteAddendaRequestForPaymentAllowanceChargeMonetaryAmountOrPercentageRate
//    {

//        private decimal percentageField;

//        private ComprobanteAddendaRequestForPaymentAllowanceChargeMonetaryAmountOrPercentageRateBase baseField;

//        /// <comentarios/>
//        public decimal percentage
//        {
//            get
//            {
//                return this.percentageField;
//            }
//            set
//            {
//                this.percentageField = value;
//            }
//        }

//        /// <comentarios/>
//        [System.Xml.Serialization.XmlAttributeAttribute()]
//        public ComprobanteAddendaRequestForPaymentAllowanceChargeMonetaryAmountOrPercentageRateBase @base
//        {
//            get
//            {
//                return this.baseField;
//            }
//            set
//            {
//                this.baseField = value;
//            }
//        }
//    }

//    /// <comentarios/>
//    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.3038")]
//    [System.SerializableAttribute()]
//    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.sat.gob.mx/cfd/2")]
//    public enum ComprobanteAddendaRequestForPaymentAllowanceChargeMonetaryAmountOrPercentageRateBase
//    {

//        /// <comentarios/>
//        INVOICE_VALUE,
//    }

//    /// <comentarios/>
//    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.3038")]
//    [System.SerializableAttribute()]
//    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.sat.gob.mx/cfd/2")]
//    public enum ComprobanteAddendaRequestForPaymentAllowanceChargeAllowanceChargeType
//    {

//        /// <comentarios/>
//        ALLOWANCE_GLOBAL,

//        /// <comentarios/>
//        CHARGE_GLOBAL,
//    }

//    /// <comentarios/>
//    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.3038")]
//    [System.SerializableAttribute()]
//    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.sat.gob.mx/cfd/2")]
//    public enum ComprobanteAddendaRequestForPaymentAllowanceChargeSettlementType
//    {

//        /// <comentarios/>
//        BILL_BACK,

//        /// <comentarios/>
//        OFF_INVOICE,
//    }

//    /// <comentarios/>
//    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.3038")]
//    [System.SerializableAttribute()]
//    [System.Diagnostics.DebuggerStepThroughAttribute()]
//    [System.ComponentModel.DesignerCategoryAttribute("code")]
//    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.sat.gob.mx/cfd/2")]
//    public partial class ComprobanteAddendaRequestForPaymentLineItem
//    {

//        private ComprobanteAddendaRequestForPaymentLineItemTradeItemIdentification tradeItemIdentificationField;

//        private ComprobanteAddendaRequestForPaymentLineItemAlternateTradeItemIdentification[] alternateTradeItemIdentificationField;

//        private ComprobanteAddendaRequestForPaymentLineItemTradeItemDescriptionInformation tradeItemDescriptionInformationField;

//        private ComprobanteAddendaRequestForPaymentLineItemInvoicedQuantity invoicedQuantityField;

//        private ComprobanteAddendaRequestForPaymentLineItemAditionalQuantity[] aditionalQuantityField;

//        private ComprobanteAddendaRequestForPaymentLineItemGrossPrice grossPriceField;

//        private ComprobanteAddendaRequestForPaymentLineItemNetPrice netPriceField;

//        private ComprobanteAddendaRequestForPaymentLineItemAdditionalInformation additionalInformationField;

//        private ComprobanteAddendaRequestForPaymentLineItemCustoms[] customsField;

//        private ComprobanteAddendaRequestForPaymentLineItemLogisticUnits logisticUnitsField;

//        private ComprobanteAddendaRequestForPaymentLineItemPalletInformation palletInformationField;

//        private ComprobanteAddendaRequestForPaymentLineItemLotNumber[] extendedAttributesField;

//        private ComprobanteAddendaRequestForPaymentLineItemAllowanceCharge[] allowanceChargeField;

//        private ComprobanteAddendaRequestForPaymentLineItemTradeItemTaxInformation[] tradeItemTaxInformationField;

//        private ComprobanteAddendaRequestForPaymentLineItemTotalLineAmount totalLineAmountField;

//        private string typeField;

//        private string numberField;

//        public ComprobanteAddendaRequestForPaymentLineItem()
//        {
//            this.typeField = "SimpleInvoiceLineItemType";
//        }

//        /// <comentarios/>
//        public ComprobanteAddendaRequestForPaymentLineItemTradeItemIdentification tradeItemIdentification
//        {
//            get
//            {
//                return this.tradeItemIdentificationField;
//            }
//            set
//            {
//                this.tradeItemIdentificationField = value;
//            }
//        }

//        /// <comentarios/>
//        [System.Xml.Serialization.XmlElementAttribute("alternateTradeItemIdentification")]
//        public ComprobanteAddendaRequestForPaymentLineItemAlternateTradeItemIdentification[] alternateTradeItemIdentification
//        {
//            get
//            {
//                return this.alternateTradeItemIdentificationField;
//            }
//            set
//            {
//                this.alternateTradeItemIdentificationField = value;
//            }
//        }

//        /// <comentarios/>
//        public ComprobanteAddendaRequestForPaymentLineItemTradeItemDescriptionInformation tradeItemDescriptionInformation
//        {
//            get
//            {
//                return this.tradeItemDescriptionInformationField;
//            }
//            set
//            {
//                this.tradeItemDescriptionInformationField = value;
//            }
//        }

//        /// <comentarios/>
//        public ComprobanteAddendaRequestForPaymentLineItemInvoicedQuantity invoicedQuantity
//        {
//            get
//            {
//                return this.invoicedQuantityField;
//            }
//            set
//            {
//                this.invoicedQuantityField = value;
//            }
//        }

//        /// <comentarios/>
//        [System.Xml.Serialization.XmlElementAttribute("aditionalQuantity")]
//        public ComprobanteAddendaRequestForPaymentLineItemAditionalQuantity[] aditionalQuantity
//        {
//            get
//            {
//                return this.aditionalQuantityField;
//            }
//            set
//            {
//                this.aditionalQuantityField = value;
//            }
//        }

//        /// <comentarios/>
//        public ComprobanteAddendaRequestForPaymentLineItemGrossPrice grossPrice
//        {
//            get
//            {
//                return this.grossPriceField;
//            }
//            set
//            {
//                this.grossPriceField = value;
//            }
//        }

//        /// <comentarios/>
//        public ComprobanteAddendaRequestForPaymentLineItemNetPrice netPrice
//        {
//            get
//            {
//                return this.netPriceField;
//            }
//            set
//            {
//                this.netPriceField = value;
//            }
//        }

//        /// <comentarios/>
//        public ComprobanteAddendaRequestForPaymentLineItemAdditionalInformation AdditionalInformation
//        {
//            get
//            {
//                return this.additionalInformationField;
//            }
//            set
//            {
//                this.additionalInformationField = value;
//            }
//        }

//        /// <comentarios/>
//        [System.Xml.Serialization.XmlElementAttribute("Customs")]
//        public ComprobanteAddendaRequestForPaymentLineItemCustoms[] Customs
//        {
//            get
//            {
//                return this.customsField;
//            }
//            set
//            {
//                this.customsField = value;
//            }
//        }

//        /// <comentarios/>
//        public ComprobanteAddendaRequestForPaymentLineItemLogisticUnits LogisticUnits
//        {
//            get
//            {
//                return this.logisticUnitsField;
//            }
//            set
//            {
//                this.logisticUnitsField = value;
//            }
//        }

//        /// <comentarios/>
//        public ComprobanteAddendaRequestForPaymentLineItemPalletInformation palletInformation
//        {
//            get
//            {
//                return this.palletInformationField;
//            }
//            set
//            {
//                this.palletInformationField = value;
//            }
//        }

//        /// <comentarios/>
//        [System.Xml.Serialization.XmlArrayItemAttribute("lotNumber", IsNullable = false)]
//        public ComprobanteAddendaRequestForPaymentLineItemLotNumber[] extendedAttributes
//        {
//            get
//            {
//                return this.extendedAttributesField;
//            }
//            set
//            {
//                this.extendedAttributesField = value;
//            }
//        }

//        /// <comentarios/>
//        [System.Xml.Serialization.XmlElementAttribute("allowanceCharge")]
//        public ComprobanteAddendaRequestForPaymentLineItemAllowanceCharge[] allowanceCharge
//        {
//            get
//            {
//                return this.allowanceChargeField;
//            }
//            set
//            {
//                this.allowanceChargeField = value;
//            }
//        }

//        /// <comentarios/>
//        [System.Xml.Serialization.XmlElementAttribute("tradeItemTaxInformation")]
//        public ComprobanteAddendaRequestForPaymentLineItemTradeItemTaxInformation[] tradeItemTaxInformation
//        {
//            get
//            {
//                return this.tradeItemTaxInformationField;
//            }
//            set
//            {
//                this.tradeItemTaxInformationField = value;
//            }
//        }

//        /// <comentarios/>
//        public ComprobanteAddendaRequestForPaymentLineItemTotalLineAmount totalLineAmount
//        {
//            get
//            {
//                return this.totalLineAmountField;
//            }
//            set
//            {
//                this.totalLineAmountField = value;
//            }
//        }

//        /// <comentarios/>
//        [System.Xml.Serialization.XmlAttributeAttribute()]
//        [System.ComponentModel.DefaultValueAttribute("SimpleInvoiceLineItemType")]
//        public string type
//        {
//            get
//            {
//                return this.typeField;
//            }
//            set
//            {
//                this.typeField = value;
//            }
//        }

//        /// <comentarios/>
//        [System.Xml.Serialization.XmlAttributeAttribute(DataType = "integer")]
//        public string number
//        {
//            get
//            {
//                return this.numberField;
//            }
//            set
//            {
//                this.numberField = value;
//            }
//        }
//    }

//    /// <comentarios/>
//    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.3038")]
//    [System.SerializableAttribute()]
//    [System.Diagnostics.DebuggerStepThroughAttribute()]
//    [System.ComponentModel.DesignerCategoryAttribute("code")]
//    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.sat.gob.mx/cfd/2")]
//    public partial class ComprobanteAddendaRequestForPaymentLineItemTradeItemIdentification
//    {

//        private string gtinField;

//        /// <comentarios/>
//        public string gtin
//        {
//            get
//            {
//                return this.gtinField;
//            }
//            set
//            {
//                this.gtinField = value;
//            }
//        }
//    }

//    /// <comentarios/>
//    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.3038")]
//    [System.SerializableAttribute()]
//    [System.Diagnostics.DebuggerStepThroughAttribute()]
//    [System.ComponentModel.DesignerCategoryAttribute("code")]
//    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.sat.gob.mx/cfd/2")]
//    public partial class ComprobanteAddendaRequestForPaymentLineItemAlternateTradeItemIdentification
//    {

//        private ComprobanteAddendaRequestForPaymentLineItemAlternateTradeItemIdentificationType typeField;

//        private string[] textField;

//        /// <comentarios/>
//        [System.Xml.Serialization.XmlAttributeAttribute()]
//        public ComprobanteAddendaRequestForPaymentLineItemAlternateTradeItemIdentificationType type
//        {
//            get
//            {
//                return this.typeField;
//            }
//            set
//            {
//                this.typeField = value;
//            }
//        }

//        /// <comentarios/>
//        [System.Xml.Serialization.XmlTextAttribute()]
//        public string[] Text
//        {
//            get
//            {
//                return this.textField;
//            }
//            set
//            {
//                this.textField = value;
//            }
//        }
//    }

//    /// <comentarios/>
//    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.3038")]
//    [System.SerializableAttribute()]
//    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.sat.gob.mx/cfd/2")]
//    public enum ComprobanteAddendaRequestForPaymentLineItemAlternateTradeItemIdentificationType
//    {

//        /// <comentarios/>
//        BUYER_ASSIGNED,

//        /// <comentarios/>
//        SUPPLIER_ASSIGNED,

//        /// <comentarios/>
//        SERIAL_NUMBER,

//        /// <comentarios/>
//        GLOBAL_TRADE_ITEM_IDENTIFICATION,
//    }

//    /// <comentarios/>
//    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.3038")]
//    [System.SerializableAttribute()]
//    [System.Diagnostics.DebuggerStepThroughAttribute()]
//    [System.ComponentModel.DesignerCategoryAttribute("code")]
//    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.sat.gob.mx/cfd/2")]
//    public partial class ComprobanteAddendaRequestForPaymentLineItemTradeItemDescriptionInformation
//    {

//        private string longTextField;

//        private ComprobanteAddendaRequestForPaymentLineItemTradeItemDescriptionInformationLanguage languageField;

//        private bool languageFieldSpecified;

//        /// <comentarios/>
//        public string longText
//        {
//            get
//            {
//                return this.longTextField;
//            }
//            set
//            {
//                this.longTextField = value;
//            }
//        }

//        /// <comentarios/>
//        [System.Xml.Serialization.XmlAttributeAttribute()]
//        public ComprobanteAddendaRequestForPaymentLineItemTradeItemDescriptionInformationLanguage language
//        {
//            get
//            {
//                return this.languageField;
//            }
//            set
//            {
//                this.languageField = value;
//            }
//        }

//        /// <comentarios/>
//        [System.Xml.Serialization.XmlIgnoreAttribute()]
//        public bool languageSpecified
//        {
//            get
//            {
//                return this.languageFieldSpecified;
//            }
//            set
//            {
//                this.languageFieldSpecified = value;
//            }
//        }
//    }

//    /// <comentarios/>
//    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.3038")]
//    [System.SerializableAttribute()]
//    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.sat.gob.mx/cfd/2")]
//    public enum ComprobanteAddendaRequestForPaymentLineItemTradeItemDescriptionInformationLanguage
//    {

//        /// <comentarios/>
//        ES,

//        /// <comentarios/>
//        EN,
//    }

//    /// <comentarios/>
//    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.3038")]
//    [System.SerializableAttribute()]
//    [System.Diagnostics.DebuggerStepThroughAttribute()]
//    [System.ComponentModel.DesignerCategoryAttribute("code")]
//    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.sat.gob.mx/cfd/2")]
//    public partial class ComprobanteAddendaRequestForPaymentLineItemInvoicedQuantity
//    {

//        private string unitOfMeasureField;

//        private string[] textField;

//        /// <comentarios/>
//        [System.Xml.Serialization.XmlAttributeAttribute(DataType = "NMTOKEN")]
//        public string unitOfMeasure
//        {
//            get
//            {
//                return this.unitOfMeasureField;
//            }
//            set
//            {
//                this.unitOfMeasureField = value;
//            }
//        }

//        /// <comentarios/>
//        [System.Xml.Serialization.XmlTextAttribute()]
//        public string[] Text
//        {
//            get
//            {
//                return this.textField;
//            }
//            set
//            {
//                this.textField = value;
//            }
//        }
//    }

//    /// <comentarios/>
//    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.3038")]
//    [System.SerializableAttribute()]
//    [System.Diagnostics.DebuggerStepThroughAttribute()]
//    [System.ComponentModel.DesignerCategoryAttribute("code")]
//    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.sat.gob.mx/cfd/2")]
//    public partial class ComprobanteAddendaRequestForPaymentLineItemAditionalQuantity
//    {

//        private ComprobanteAddendaRequestForPaymentLineItemAditionalQuantityQuantityType quantityTypeField;

//        private string[] textField;

//        /// <comentarios/>
//        [System.Xml.Serialization.XmlAttributeAttribute()]
//        public ComprobanteAddendaRequestForPaymentLineItemAditionalQuantityQuantityType QuantityType
//        {
//            get
//            {
//                return this.quantityTypeField;
//            }
//            set
//            {
//                this.quantityTypeField = value;
//            }
//        }

//        /// <comentarios/>
//        [System.Xml.Serialization.XmlTextAttribute()]
//        public string[] Text
//        {
//            get
//            {
//                return this.textField;
//            }
//            set
//            {
//                this.textField = value;
//            }
//        }
//    }

//    /// <comentarios/>
//    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.3038")]
//    [System.SerializableAttribute()]
//    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.sat.gob.mx/cfd/2")]
//    public enum ComprobanteAddendaRequestForPaymentLineItemAditionalQuantityQuantityType
//    {

//        /// <comentarios/>
//        NUM_CONSUMER_UNITS,

//        /// <comentarios/>
//        FREE_GOODS,
//    }

//    /// <comentarios/>
//    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.3038")]
//    [System.SerializableAttribute()]
//    [System.Diagnostics.DebuggerStepThroughAttribute()]
//    [System.ComponentModel.DesignerCategoryAttribute("code")]
//    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.sat.gob.mx/cfd/2")]
//    public partial class ComprobanteAddendaRequestForPaymentLineItemGrossPrice
//    {

//        private decimal amountField;

//        /// <comentarios/>
//        public decimal Amount
//        {
//            get
//            {
//                return this.amountField;
//            }
//            set
//            {
//                this.amountField = value;
//            }
//        }
//    }

//    /// <comentarios/>
//    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.3038")]
//    [System.SerializableAttribute()]
//    [System.Diagnostics.DebuggerStepThroughAttribute()]
//    [System.ComponentModel.DesignerCategoryAttribute("code")]
//    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.sat.gob.mx/cfd/2")]
//    public partial class ComprobanteAddendaRequestForPaymentLineItemNetPrice
//    {

//        private decimal amountField;

//        /// <comentarios/>
//        public decimal Amount
//        {
//            get
//            {
//                return this.amountField;
//            }
//            set
//            {
//                this.amountField = value;
//            }
//        }
//    }

//    /// <comentarios/>
//    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.3038")]
//    [System.SerializableAttribute()]
//    [System.Diagnostics.DebuggerStepThroughAttribute()]
//    [System.ComponentModel.DesignerCategoryAttribute("code")]
//    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.sat.gob.mx/cfd/2")]
//    public partial class ComprobanteAddendaRequestForPaymentLineItemAdditionalInformation
//    {

//        private ComprobanteAddendaRequestForPaymentLineItemAdditionalInformationReferenceIdentification referenceIdentificationField;

//        /// <comentarios/>
//        public ComprobanteAddendaRequestForPaymentLineItemAdditionalInformationReferenceIdentification referenceIdentification
//        {
//            get
//            {
//                return this.referenceIdentificationField;
//            }
//            set
//            {
//                this.referenceIdentificationField = value;
//            }
//        }
//    }

//    /// <comentarios/>
//    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.3038")]
//    [System.SerializableAttribute()]
//    [System.Diagnostics.DebuggerStepThroughAttribute()]
//    [System.ComponentModel.DesignerCategoryAttribute("code")]
//    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.sat.gob.mx/cfd/2")]
//    public partial class ComprobanteAddendaRequestForPaymentLineItemAdditionalInformationReferenceIdentification
//    {

//        private ComprobanteAddendaRequestForPaymentLineItemAdditionalInformationReferenceIdentificationType typeField;

//        private string valueField;

//        /// <comentarios/>
//        [System.Xml.Serialization.XmlAttributeAttribute()]
//        public ComprobanteAddendaRequestForPaymentLineItemAdditionalInformationReferenceIdentificationType type
//        {
//            get
//            {
//                return this.typeField;
//            }
//            set
//            {
//                this.typeField = value;
//            }
//        }

//        /// <comentarios/>
//        [System.Xml.Serialization.XmlTextAttribute()]
//        public string Value
//        {
//            get
//            {
//                return this.valueField;
//            }
//            set
//            {
//                this.valueField = value;
//            }
//        }
//    }

//    /// <comentarios/>
//    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.3038")]
//    [System.SerializableAttribute()]
//    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.sat.gob.mx/cfd/2")]
//    public enum ComprobanteAddendaRequestForPaymentLineItemAdditionalInformationReferenceIdentificationType
//    {

//        /// <comentarios/>
//        ON,
//    }

//    /// <comentarios/>
//    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.3038")]
//    [System.SerializableAttribute()]
//    [System.Diagnostics.DebuggerStepThroughAttribute()]
//    [System.ComponentModel.DesignerCategoryAttribute("code")]
//    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.sat.gob.mx/cfd/2")]
//    public partial class ComprobanteAddendaRequestForPaymentLineItemCustoms
//    {

//        private string glnField;

//        private ComprobanteAddendaRequestForPaymentLineItemCustomsAlternatePartyIdentification alternatePartyIdentificationField;

//        private System.DateTime referenceDateField;

//        private ComprobanteAddendaRequestForPaymentLineItemCustomsNameAndAddress nameAndAddressField;

//        /// <comentarios/>
//        public string gln
//        {
//            get
//            {
//                return this.glnField;
//            }
//            set
//            {
//                this.glnField = value;
//            }
//        }

//        /// <comentarios/>
//        public ComprobanteAddendaRequestForPaymentLineItemCustomsAlternatePartyIdentification alternatePartyIdentification
//        {
//            get
//            {
//                return this.alternatePartyIdentificationField;
//            }
//            set
//            {
//                this.alternatePartyIdentificationField = value;
//            }
//        }

//        /// <comentarios/>
//        [System.Xml.Serialization.XmlElementAttribute(DataType = "date")]
//        public System.DateTime ReferenceDate
//        {
//            get
//            {
//                return this.referenceDateField;
//            }
//            set
//            {
//                this.referenceDateField = value;
//            }
//        }

//        /// <comentarios/>
//        public ComprobanteAddendaRequestForPaymentLineItemCustomsNameAndAddress nameAndAddress
//        {
//            get
//            {
//                return this.nameAndAddressField;
//            }
//            set
//            {
//                this.nameAndAddressField = value;
//            }
//        }
//    }

//    /// <comentarios/>
//    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.3038")]
//    [System.SerializableAttribute()]
//    [System.Diagnostics.DebuggerStepThroughAttribute()]
//    [System.ComponentModel.DesignerCategoryAttribute("code")]
//    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.sat.gob.mx/cfd/2")]
//    public partial class ComprobanteAddendaRequestForPaymentLineItemCustomsAlternatePartyIdentification
//    {

//        private ComprobanteAddendaRequestForPaymentLineItemCustomsAlternatePartyIdentificationType typeField;

//        private string valueField;

//        /// <comentarios/>
//        [System.Xml.Serialization.XmlAttributeAttribute()]
//        public ComprobanteAddendaRequestForPaymentLineItemCustomsAlternatePartyIdentificationType type
//        {
//            get
//            {
//                return this.typeField;
//            }
//            set
//            {
//                this.typeField = value;
//            }
//        }

//        /// <comentarios/>
//        [System.Xml.Serialization.XmlTextAttribute()]
//        public string Value
//        {
//            get
//            {
//                return this.valueField;
//            }
//            set
//            {
//                this.valueField = value;
//            }
//        }
//    }

//    /// <comentarios/>
//    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.3038")]
//    [System.SerializableAttribute()]
//    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.sat.gob.mx/cfd/2")]
//    public enum ComprobanteAddendaRequestForPaymentLineItemCustomsAlternatePartyIdentificationType
//    {

//        /// <comentarios/>
//        TN,
//    }

//    /// <comentarios/>
//    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.3038")]
//    [System.SerializableAttribute()]
//    [System.Diagnostics.DebuggerStepThroughAttribute()]
//    [System.ComponentModel.DesignerCategoryAttribute("code")]
//    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.sat.gob.mx/cfd/2")]
//    public partial class ComprobanteAddendaRequestForPaymentLineItemCustomsNameAndAddress
//    {

//        private string nameField;

//        /// <comentarios/>
//        public string name
//        {
//            get
//            {
//                return this.nameField;
//            }
//            set
//            {
//                this.nameField = value;
//            }
//        }
//    }

//    /// <comentarios/>
//    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.3038")]
//    [System.SerializableAttribute()]
//    [System.Diagnostics.DebuggerStepThroughAttribute()]
//    [System.ComponentModel.DesignerCategoryAttribute("code")]
//    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.sat.gob.mx/cfd/2")]
//    public partial class ComprobanteAddendaRequestForPaymentLineItemLogisticUnits
//    {

//        private ComprobanteAddendaRequestForPaymentLineItemLogisticUnitsSerialShippingContainerCode serialShippingContainerCodeField;

//        /// <comentarios/>
//        public ComprobanteAddendaRequestForPaymentLineItemLogisticUnitsSerialShippingContainerCode serialShippingContainerCode
//        {
//            get
//            {
//                return this.serialShippingContainerCodeField;
//            }
//            set
//            {
//                this.serialShippingContainerCodeField = value;
//            }
//        }
//    }

//    /// <comentarios/>
//    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.3038")]
//    [System.SerializableAttribute()]
//    [System.Diagnostics.DebuggerStepThroughAttribute()]
//    [System.ComponentModel.DesignerCategoryAttribute("code")]
//    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.sat.gob.mx/cfd/2")]
//    public partial class ComprobanteAddendaRequestForPaymentLineItemLogisticUnitsSerialShippingContainerCode
//    {

//        private ComprobanteAddendaRequestForPaymentLineItemLogisticUnitsSerialShippingContainerCodeType typeField;

//        private string valueField;

//        /// <comentarios/>
//        [System.Xml.Serialization.XmlAttributeAttribute()]
//        public ComprobanteAddendaRequestForPaymentLineItemLogisticUnitsSerialShippingContainerCodeType type
//        {
//            get
//            {
//                return this.typeField;
//            }
//            set
//            {
//                this.typeField = value;
//            }
//        }

//        /// <comentarios/>
//        [System.Xml.Serialization.XmlTextAttribute()]
//        public string Value
//        {
//            get
//            {
//                return this.valueField;
//            }
//            set
//            {
//                this.valueField = value;
//            }
//        }
//    }

//    /// <comentarios/>
//    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.3038")]
//    [System.SerializableAttribute()]
//    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.sat.gob.mx/cfd/2")]
//    public enum ComprobanteAddendaRequestForPaymentLineItemLogisticUnitsSerialShippingContainerCodeType
//    {

//        /// <comentarios/>
//        BJ,

//        /// <comentarios/>
//        SRV,
//    }

//    /// <comentarios/>
//    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.3038")]
//    [System.SerializableAttribute()]
//    [System.Diagnostics.DebuggerStepThroughAttribute()]
//    [System.ComponentModel.DesignerCategoryAttribute("code")]
//    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.sat.gob.mx/cfd/2")]
//    public partial class ComprobanteAddendaRequestForPaymentLineItemPalletInformation
//    {

//        private string palletQuantityField;

//        private ComprobanteAddendaRequestForPaymentLineItemPalletInformationDescription descriptionField;

//        private ComprobanteAddendaRequestForPaymentLineItemPalletInformationTransport transportField;

//        /// <comentarios/>
//        public string palletQuantity
//        {
//            get
//            {
//                return this.palletQuantityField;
//            }
//            set
//            {
//                this.palletQuantityField = value;
//            }
//        }

//        /// <comentarios/>
//        public ComprobanteAddendaRequestForPaymentLineItemPalletInformationDescription description
//        {
//            get
//            {
//                return this.descriptionField;
//            }
//            set
//            {
//                this.descriptionField = value;
//            }
//        }

//        /// <comentarios/>
//        public ComprobanteAddendaRequestForPaymentLineItemPalletInformationTransport transport
//        {
//            get
//            {
//                return this.transportField;
//            }
//            set
//            {
//                this.transportField = value;
//            }
//        }
//    }

//    /// <comentarios/>
//    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.3038")]
//    [System.SerializableAttribute()]
//    [System.Diagnostics.DebuggerStepThroughAttribute()]
//    [System.ComponentModel.DesignerCategoryAttribute("code")]
//    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.sat.gob.mx/cfd/2")]
//    public partial class ComprobanteAddendaRequestForPaymentLineItemPalletInformationDescription
//    {

//        private ComprobanteAddendaRequestForPaymentLineItemPalletInformationDescriptionType typeField;

//        private string[] textField;

//        /// <comentarios/>
//        [System.Xml.Serialization.XmlAttributeAttribute()]
//        public ComprobanteAddendaRequestForPaymentLineItemPalletInformationDescriptionType type
//        {
//            get
//            {
//                return this.typeField;
//            }
//            set
//            {
//                this.typeField = value;
//            }
//        }

//        /// <comentarios/>
//        [System.Xml.Serialization.XmlTextAttribute()]
//        public string[] Text
//        {
//            get
//            {
//                return this.textField;
//            }
//            set
//            {
//                this.textField = value;
//            }
//        }
//    }

//    /// <comentarios/>
//    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.3038")]
//    [System.SerializableAttribute()]
//    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.sat.gob.mx/cfd/2")]
//    public enum ComprobanteAddendaRequestForPaymentLineItemPalletInformationDescriptionType
//    {

//        /// <comentarios/>
//        EXCHANGE_PALLETS,

//        /// <comentarios/>
//        RETURN_PALLETS,

//        /// <comentarios/>
//        PALLET_80x100,

//        /// <comentarios/>
//        CASE,

//        /// <comentarios/>
//        BOX,
//    }

//    /// <comentarios/>
//    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.3038")]
//    [System.SerializableAttribute()]
//    [System.Diagnostics.DebuggerStepThroughAttribute()]
//    [System.ComponentModel.DesignerCategoryAttribute("code")]
//    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.sat.gob.mx/cfd/2")]
//    public partial class ComprobanteAddendaRequestForPaymentLineItemPalletInformationTransport
//    {

//        private ComprobanteAddendaRequestForPaymentLineItemPalletInformationTransportMethodOfPayment methodOfPaymentField;

//        /// <comentarios/>
//        public ComprobanteAddendaRequestForPaymentLineItemPalletInformationTransportMethodOfPayment methodOfPayment
//        {
//            get
//            {
//                return this.methodOfPaymentField;
//            }
//            set
//            {
//                this.methodOfPaymentField = value;
//            }
//        }
//    }

//    /// <comentarios/>
//    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.3038")]
//    [System.SerializableAttribute()]
//    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.sat.gob.mx/cfd/2")]
//    public enum ComprobanteAddendaRequestForPaymentLineItemPalletInformationTransportMethodOfPayment
//    {

//        /// <comentarios/>
//        PREPAID_BY_SELLER,

//        /// <comentarios/>
//        PAID_BY_BUYER,
//    }

//    /// <comentarios/>
//    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.3038")]
//    [System.SerializableAttribute()]
//    [System.Diagnostics.DebuggerStepThroughAttribute()]
//    [System.ComponentModel.DesignerCategoryAttribute("code")]
//    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.sat.gob.mx/cfd/2")]
//    public partial class ComprobanteAddendaRequestForPaymentLineItemLotNumber
//    {

//        private System.DateTime productionDateField;

//        private bool productionDateFieldSpecified;

//        private string valueField;

//        /// <comentarios/>
//        [System.Xml.Serialization.XmlAttributeAttribute(DataType = "date")]
//        public System.DateTime productionDate
//        {
//            get
//            {
//                return this.productionDateField;
//            }
//            set
//            {
//                this.productionDateField = value;
//            }
//        }

//        /// <comentarios/>
//        [System.Xml.Serialization.XmlIgnoreAttribute()]
//        public bool productionDateSpecified
//        {
//            get
//            {
//                return this.productionDateFieldSpecified;
//            }
//            set
//            {
//                this.productionDateFieldSpecified = value;
//            }
//        }

//        /// <comentarios/>
//        [System.Xml.Serialization.XmlTextAttribute()]
//        public string Value
//        {
//            get
//            {
//                return this.valueField;
//            }
//            set
//            {
//                this.valueField = value;
//            }
//        }
//    }

//    /// <comentarios/>
//    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.3038")]
//    [System.SerializableAttribute()]
//    [System.Diagnostics.DebuggerStepThroughAttribute()]
//    [System.ComponentModel.DesignerCategoryAttribute("code")]
//    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.sat.gob.mx/cfd/2")]
//    public partial class ComprobanteAddendaRequestForPaymentLineItemAllowanceCharge
//    {

//        private ComprobanteAddendaRequestForPaymentLineItemAllowanceChargeSpecialServicesType specialServicesTypeField;

//        private bool specialServicesTypeFieldSpecified;

//        private ComprobanteAddendaRequestForPaymentLineItemAllowanceChargeMonetaryAmountOrPercentage monetaryAmountOrPercentageField;

//        private ComprobanteAddendaRequestForPaymentLineItemAllowanceChargeAllowanceChargeType allowanceChargeTypeField;

//        private ComprobanteAddendaRequestForPaymentLineItemAllowanceChargeSettlementType settlementTypeField;

//        private bool settlementTypeFieldSpecified;

//        private string sequenceNumberField;

//        /// <comentarios/>
//        public ComprobanteAddendaRequestForPaymentLineItemAllowanceChargeSpecialServicesType specialServicesType
//        {
//            get
//            {
//                return this.specialServicesTypeField;
//            }
//            set
//            {
//                this.specialServicesTypeField = value;
//            }
//        }

//        /// <comentarios/>
//        [System.Xml.Serialization.XmlIgnoreAttribute()]
//        public bool specialServicesTypeSpecified
//        {
//            get
//            {
//                return this.specialServicesTypeFieldSpecified;
//            }
//            set
//            {
//                this.specialServicesTypeFieldSpecified = value;
//            }
//        }

//        /// <comentarios/>
//        public ComprobanteAddendaRequestForPaymentLineItemAllowanceChargeMonetaryAmountOrPercentage monetaryAmountOrPercentage
//        {
//            get
//            {
//                return this.monetaryAmountOrPercentageField;
//            }
//            set
//            {
//                this.monetaryAmountOrPercentageField = value;
//            }
//        }

//        /// <comentarios/>
//        [System.Xml.Serialization.XmlAttributeAttribute()]
//        public ComprobanteAddendaRequestForPaymentLineItemAllowanceChargeAllowanceChargeType allowanceChargeType
//        {
//            get
//            {
//                return this.allowanceChargeTypeField;
//            }
//            set
//            {
//                this.allowanceChargeTypeField = value;
//            }
//        }

//        /// <comentarios/>
//        [System.Xml.Serialization.XmlAttributeAttribute()]
//        public ComprobanteAddendaRequestForPaymentLineItemAllowanceChargeSettlementType settlementType
//        {
//            get
//            {
//                return this.settlementTypeField;
//            }
//            set
//            {
//                this.settlementTypeField = value;
//            }
//        }

//        /// <comentarios/>
//        [System.Xml.Serialization.XmlIgnoreAttribute()]
//        public bool settlementTypeSpecified
//        {
//            get
//            {
//                return this.settlementTypeFieldSpecified;
//            }
//            set
//            {
//                this.settlementTypeFieldSpecified = value;
//            }
//        }

//        /// <comentarios/>
//        [System.Xml.Serialization.XmlAttributeAttribute()]
//        public string sequenceNumber
//        {
//            get
//            {
//                return this.sequenceNumberField;
//            }
//            set
//            {
//                this.sequenceNumberField = value;
//            }
//        }
//    }

//    /// <comentarios/>
//    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.3038")]
//    [System.SerializableAttribute()]
//    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.sat.gob.mx/cfd/2")]
//    public enum ComprobanteAddendaRequestForPaymentLineItemAllowanceChargeSpecialServicesType
//    {

//        /// <comentarios/>
//        AA,

//        /// <comentarios/>
//        ADS,

//        /// <comentarios/>
//        ADO,

//        /// <comentarios/>
//        ABZ,

//        /// <comentarios/>
//        DA,

//        /// <comentarios/>
//        EAA,

//        /// <comentarios/>
//        PI,

//        /// <comentarios/>
//        TAE,

//        /// <comentarios/>
//        SAB,

//        /// <comentarios/>
//        RAA,

//        /// <comentarios/>
//        PAD,

//        /// <comentarios/>
//        FG,

//        /// <comentarios/>
//        FA,

//        /// <comentarios/>
//        TD,

//        /// <comentarios/>
//        TS,

//        /// <comentarios/>
//        TX,

//        /// <comentarios/>
//        ZZZ,

//        /// <comentarios/>
//        VAB,

//        /// <comentarios/>
//        UM,

//        /// <comentarios/>
//        DI,

//        /// <comentarios/>
//        ADT,

//        /// <comentarios/>
//        AJ,

//        /// <comentarios/>
//        CAC,

//        /// <comentarios/>
//        COD,

//        /// <comentarios/>
//        EAB,

//        /// <comentarios/>
//        FC,

//        /// <comentarios/>
//        FI,

//        /// <comentarios/>
//        HD,

//        /// <comentarios/>
//        QD,
//    }

//    /// <comentarios/>
//    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.3038")]
//    [System.SerializableAttribute()]
//    [System.Diagnostics.DebuggerStepThroughAttribute()]
//    [System.ComponentModel.DesignerCategoryAttribute("code")]
//    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.sat.gob.mx/cfd/2")]
//    public partial class ComprobanteAddendaRequestForPaymentLineItemAllowanceChargeMonetaryAmountOrPercentage
//    {

//        private string percentagePerUnitField;

//        private ComprobanteAddendaRequestForPaymentLineItemAllowanceChargeMonetaryAmountOrPercentageRatePerUnit ratePerUnitField;

//        /// <comentarios/>
//        public string percentagePerUnit
//        {
//            get
//            {
//                return this.percentagePerUnitField;
//            }
//            set
//            {
//                this.percentagePerUnitField = value;
//            }
//        }

//        /// <comentarios/>
//        public ComprobanteAddendaRequestForPaymentLineItemAllowanceChargeMonetaryAmountOrPercentageRatePerUnit ratePerUnit
//        {
//            get
//            {
//                return this.ratePerUnitField;
//            }
//            set
//            {
//                this.ratePerUnitField = value;
//            }
//        }
//    }

//    /// <comentarios/>
//    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.3038")]
//    [System.SerializableAttribute()]
//    [System.Diagnostics.DebuggerStepThroughAttribute()]
//    [System.ComponentModel.DesignerCategoryAttribute("code")]
//    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.sat.gob.mx/cfd/2")]
//    public partial class ComprobanteAddendaRequestForPaymentLineItemAllowanceChargeMonetaryAmountOrPercentageRatePerUnit
//    {

//        private string amountPerUnitField;

//        /// <comentarios/>
//        public string amountPerUnit
//        {
//            get
//            {
//                return this.amountPerUnitField;
//            }
//            set
//            {
//                this.amountPerUnitField = value;
//            }
//        }
//    }

//    /// <comentarios/>
//    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.3038")]
//    [System.SerializableAttribute()]
//    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.sat.gob.mx/cfd/2")]
//    public enum ComprobanteAddendaRequestForPaymentLineItemAllowanceChargeAllowanceChargeType
//    {

//        /// <comentarios/>
//        ALLOWANCE_GLOBAL,

//        /// <comentarios/>
//        CHARGE_GLOBAL,
//    }

//    /// <comentarios/>
//    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.3038")]
//    [System.SerializableAttribute()]
//    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.sat.gob.mx/cfd/2")]
//    public enum ComprobanteAddendaRequestForPaymentLineItemAllowanceChargeSettlementType
//    {

//        /// <comentarios/>
//        OFF_INVOICE,

//        /// <comentarios/>
//        CHARGE_TO_BE_PAID_BY_VENDOR,

//        /// <comentarios/>
//        CHARGE_TO_BE_PAID_BY_CUSTOMER,
//    }

//    /// <comentarios/>
//    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.3038")]
//    [System.SerializableAttribute()]
//    [System.Diagnostics.DebuggerStepThroughAttribute()]
//    [System.ComponentModel.DesignerCategoryAttribute("code")]
//    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.sat.gob.mx/cfd/2")]
//    public partial class ComprobanteAddendaRequestForPaymentLineItemTradeItemTaxInformation
//    {

//        private ComprobanteAddendaRequestForPaymentLineItemTradeItemTaxInformationTaxTypeDescription taxTypeDescriptionField;

//        private string referenceNumberField;

//        private ComprobanteAddendaRequestForPaymentLineItemTradeItemTaxInformationTradeItemTaxAmount tradeItemTaxAmountField;

//        private ComprobanteAddendaRequestForPaymentLineItemTradeItemTaxInformationTaxCategory taxCategoryField;

//        private bool taxCategoryFieldSpecified;

//        /// <comentarios/>
//        public ComprobanteAddendaRequestForPaymentLineItemTradeItemTaxInformationTaxTypeDescription taxTypeDescription
//        {
//            get
//            {
//                return this.taxTypeDescriptionField;
//            }
//            set
//            {
//                this.taxTypeDescriptionField = value;
//            }
//        }

//        /// <comentarios/>
//        public string referenceNumber
//        {
//            get
//            {
//                return this.referenceNumberField;
//            }
//            set
//            {
//                this.referenceNumberField = value;
//            }
//        }

//        /// <comentarios/>
//        public ComprobanteAddendaRequestForPaymentLineItemTradeItemTaxInformationTradeItemTaxAmount tradeItemTaxAmount
//        {
//            get
//            {
//                return this.tradeItemTaxAmountField;
//            }
//            set
//            {
//                this.tradeItemTaxAmountField = value;
//            }
//        }

//        /// <comentarios/>
//        public ComprobanteAddendaRequestForPaymentLineItemTradeItemTaxInformationTaxCategory taxCategory
//        {
//            get
//            {
//                return this.taxCategoryField;
//            }
//            set
//            {
//                this.taxCategoryField = value;
//            }
//        }

//        /// <comentarios/>
//        [System.Xml.Serialization.XmlIgnoreAttribute()]
//        public bool taxCategorySpecified
//        {
//            get
//            {
//                return this.taxCategoryFieldSpecified;
//            }
//            set
//            {
//                this.taxCategoryFieldSpecified = value;
//            }
//        }
//    }

//    /// <comentarios/>
//    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.3038")]
//    [System.SerializableAttribute()]
//    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.sat.gob.mx/cfd/2")]
//    public enum ComprobanteAddendaRequestForPaymentLineItemTradeItemTaxInformationTaxTypeDescription
//    {

//        /// <comentarios/>
//        GST,

//        /// <comentarios/>
//        VAT,

//        /// <comentarios/>
//        LAC,

//        /// <comentarios/>
//        AAA,

//        /// <comentarios/>
//        ADD,

//        /// <comentarios/>
//        FRE,

//        /// <comentarios/>
//        LOC,

//        /// <comentarios/>
//        STT,

//        /// <comentarios/>
//        OTH,
//    }

//    /// <comentarios/>
//    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.3038")]
//    [System.SerializableAttribute()]
//    [System.Diagnostics.DebuggerStepThroughAttribute()]
//    [System.ComponentModel.DesignerCategoryAttribute("code")]
//    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.sat.gob.mx/cfd/2")]
//    public partial class ComprobanteAddendaRequestForPaymentLineItemTradeItemTaxInformationTradeItemTaxAmount
//    {

//        private decimal taxPercentageField;

//        private decimal taxAmountField;

//        /// <comentarios/>
//        public decimal taxPercentage
//        {
//            get
//            {
//                return this.taxPercentageField;
//            }
//            set
//            {
//                this.taxPercentageField = value;
//            }
//        }

//        /// <comentarios/>
//        public decimal taxAmount
//        {
//            get
//            {
//                return this.taxAmountField;
//            }
//            set
//            {
//                this.taxAmountField = value;
//            }
//        }
//    }

//    /// <comentarios/>
//    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.3038")]
//    [System.SerializableAttribute()]
//    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.sat.gob.mx/cfd/2")]
//    public enum ComprobanteAddendaRequestForPaymentLineItemTradeItemTaxInformationTaxCategory
//    {

//        /// <comentarios/>
//        TRANSFERIDO,

//        /// <comentarios/>
//        RETENIDO,
//    }

//    /// <comentarios/>
//    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.3038")]
//    [System.SerializableAttribute()]
//    [System.Diagnostics.DebuggerStepThroughAttribute()]
//    [System.ComponentModel.DesignerCategoryAttribute("code")]
//    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.sat.gob.mx/cfd/2")]
//    public partial class ComprobanteAddendaRequestForPaymentLineItemTotalLineAmount
//    {

//        private ComprobanteAddendaRequestForPaymentLineItemTotalLineAmountGrossAmount grossAmountField;

//        private ComprobanteAddendaRequestForPaymentLineItemTotalLineAmountNetAmount netAmountField;

//        /// <comentarios/>
//        public ComprobanteAddendaRequestForPaymentLineItemTotalLineAmountGrossAmount grossAmount
//        {
//            get
//            {
//                return this.grossAmountField;
//            }
//            set
//            {
//                this.grossAmountField = value;
//            }
//        }

//        /// <comentarios/>
//        public ComprobanteAddendaRequestForPaymentLineItemTotalLineAmountNetAmount netAmount
//        {
//            get
//            {
//                return this.netAmountField;
//            }
//            set
//            {
//                this.netAmountField = value;
//            }
//        }
//    }

//    /// <comentarios/>
//    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.3038")]
//    [System.SerializableAttribute()]
//    [System.Diagnostics.DebuggerStepThroughAttribute()]
//    [System.ComponentModel.DesignerCategoryAttribute("code")]
//    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.sat.gob.mx/cfd/2")]
//    public partial class ComprobanteAddendaRequestForPaymentLineItemTotalLineAmountGrossAmount
//    {

//        private decimal amountField;

//        /// <comentarios/>
//        public decimal Amount
//        {
//            get
//            {
//                return this.amountField;
//            }
//            set
//            {
//                this.amountField = value;
//            }
//        }
//    }

//    /// <comentarios/>
//    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.3038")]
//    [System.SerializableAttribute()]
//    [System.Diagnostics.DebuggerStepThroughAttribute()]
//    [System.ComponentModel.DesignerCategoryAttribute("code")]
//    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.sat.gob.mx/cfd/2")]
//    public partial class ComprobanteAddendaRequestForPaymentLineItemTotalLineAmountNetAmount
//    {

//        private decimal amountField;

//        /// <comentarios/>
//        public decimal Amount
//        {
//            get
//            {
//                return this.amountField;
//            }
//            set
//            {
//                this.amountField = value;
//            }
//        }
//    }

//    /// <comentarios/>
//    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.3038")]
//    [System.SerializableAttribute()]
//    [System.Diagnostics.DebuggerStepThroughAttribute()]
//    [System.ComponentModel.DesignerCategoryAttribute("code")]
//    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.sat.gob.mx/cfd/2")]
//    public partial class ComprobanteAddendaRequestForPaymentTotalAmount
//    {

//        private decimal amountField;

//        /// <comentarios/>
//        public decimal Amount
//        {
//            get
//            {
//                return this.amountField;
//            }
//            set
//            {
//                this.amountField = value;
//            }
//        }
//    }

//    /// <comentarios/>
//    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.3038")]
//    [System.SerializableAttribute()]
//    [System.Diagnostics.DebuggerStepThroughAttribute()]
//    [System.ComponentModel.DesignerCategoryAttribute("code")]
//    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.sat.gob.mx/cfd/2")]
//    public partial class ComprobanteAddendaRequestForPaymentTotalAllowanceCharge
//    {

//        private ComprobanteAddendaRequestForPaymentTotalAllowanceChargeSpecialServicesType specialServicesTypeField;

//        private bool specialServicesTypeFieldSpecified;

//        private decimal amountField;

//        private bool amountFieldSpecified;

//        private ComprobanteAddendaRequestForPaymentTotalAllowanceChargeAllowanceOrChargeType allowanceOrChargeTypeField;

//        /// <comentarios/>
//        public ComprobanteAddendaRequestForPaymentTotalAllowanceChargeSpecialServicesType specialServicesType
//        {
//            get
//            {
//                return this.specialServicesTypeField;
//            }
//            set
//            {
//                this.specialServicesTypeField = value;
//            }
//        }

//        /// <comentarios/>
//        [System.Xml.Serialization.XmlIgnoreAttribute()]
//        public bool specialServicesTypeSpecified
//        {
//            get
//            {
//                return this.specialServicesTypeFieldSpecified;
//            }
//            set
//            {
//                this.specialServicesTypeFieldSpecified = value;
//            }
//        }

//        /// <comentarios/>
//        public decimal Amount
//        {
//            get
//            {
//                return this.amountField;
//            }
//            set
//            {
//                this.amountField = value;
//            }
//        }

//        /// <comentarios/>
//        [System.Xml.Serialization.XmlIgnoreAttribute()]
//        public bool AmountSpecified
//        {
//            get
//            {
//                return this.amountFieldSpecified;
//            }
//            set
//            {
//                this.amountFieldSpecified = value;
//            }
//        }

//        /// <comentarios/>
//        [System.Xml.Serialization.XmlAttributeAttribute()]
//        public ComprobanteAddendaRequestForPaymentTotalAllowanceChargeAllowanceOrChargeType allowanceOrChargeType
//        {
//            get
//            {
//                return this.allowanceOrChargeTypeField;
//            }
//            set
//            {
//                this.allowanceOrChargeTypeField = value;
//            }
//        }
//    }

//    /// <comentarios/>
//    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.3038")]
//    [System.SerializableAttribute()]
//    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.sat.gob.mx/cfd/2")]
//    public enum ComprobanteAddendaRequestForPaymentTotalAllowanceChargeSpecialServicesType
//    {

//        /// <comentarios/>
//        AA,

//        /// <comentarios/>
//        ADS,

//        /// <comentarios/>
//        ADO,

//        /// <comentarios/>
//        ABZ,

//        /// <comentarios/>
//        DA,

//        /// <comentarios/>
//        EAA,

//        /// <comentarios/>
//        PI,

//        /// <comentarios/>
//        TAE,

//        /// <comentarios/>
//        SAB,

//        /// <comentarios/>
//        RAA,

//        /// <comentarios/>
//        PAD,

//        /// <comentarios/>
//        FG,

//        /// <comentarios/>
//        FA,

//        /// <comentarios/>
//        TD,

//        /// <comentarios/>
//        TS,

//        /// <comentarios/>
//        TX,

//        /// <comentarios/>
//        ZZZ,

//        /// <comentarios/>
//        VAB,

//        /// <comentarios/>
//        UM,

//        /// <comentarios/>
//        DI,

//        /// <comentarios/>
//        ADT,

//        /// <comentarios/>
//        AJ,

//        /// <comentarios/>
//        CAC,

//        /// <comentarios/>
//        COD,

//        /// <comentarios/>
//        EAB,

//        /// <comentarios/>
//        FC,

//        /// <comentarios/>
//        FI,

//        /// <comentarios/>
//        HD,

//        /// <comentarios/>
//        QD,
//    }

//    /// <comentarios/>
//    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.3038")]
//    [System.SerializableAttribute()]
//    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.sat.gob.mx/cfd/2")]
//    public enum ComprobanteAddendaRequestForPaymentTotalAllowanceChargeAllowanceOrChargeType
//    {

//        /// <comentarios/>
//        ALLOWANCE,

//        /// <comentarios/>
//        CHARGE,
//    }

//    /// <comentarios/>
//    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.3038")]
//    [System.SerializableAttribute()]
//    [System.Diagnostics.DebuggerStepThroughAttribute()]
//    [System.ComponentModel.DesignerCategoryAttribute("code")]
//    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.sat.gob.mx/cfd/2")]
//    public partial class ComprobanteAddendaRequestForPaymentBaseAmount
//    {

//        private decimal amountField;

//        /// <comentarios/>
//        public decimal Amount
//        {
//            get
//            {
//                return this.amountField;
//            }
//            set
//            {
//                this.amountField = value;
//            }
//        }
//    }

//    /// <comentarios/>
//    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.3038")]
//    [System.SerializableAttribute()]
//    [System.Diagnostics.DebuggerStepThroughAttribute()]
//    [System.ComponentModel.DesignerCategoryAttribute("code")]
//    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.sat.gob.mx/cfd/2")]
//    public partial class ComprobanteAddendaRequestForPaymentTax
//    {

//        private decimal taxPercentageField;

//        private bool taxPercentageFieldSpecified;

//        private decimal taxAmountField;

//        private bool taxAmountFieldSpecified;

//        private ComprobanteAddendaRequestForPaymentTaxTaxCategory taxCategoryField;

//        private bool taxCategoryFieldSpecified;

//        private ComprobanteAddendaRequestForPaymentTaxType typeField;

//        private bool typeFieldSpecified;

//        /// <comentarios/>
//        public decimal taxPercentage
//        {
//            get
//            {
//                return this.taxPercentageField;
//            }
//            set
//            {
//                this.taxPercentageField = value;
//            }
//        }

//        /// <comentarios/>
//        [System.Xml.Serialization.XmlIgnoreAttribute()]
//        public bool taxPercentageSpecified
//        {
//            get
//            {
//                return this.taxPercentageFieldSpecified;
//            }
//            set
//            {
//                this.taxPercentageFieldSpecified = value;
//            }
//        }

//        /// <comentarios/>
//        public decimal taxAmount
//        {
//            get
//            {
//                return this.taxAmountField;
//            }
//            set
//            {
//                this.taxAmountField = value;
//            }
//        }

//        /// <comentarios/>
//        [System.Xml.Serialization.XmlIgnoreAttribute()]
//        public bool taxAmountSpecified
//        {
//            get
//            {
//                return this.taxAmountFieldSpecified;
//            }
//            set
//            {
//                this.taxAmountFieldSpecified = value;
//            }
//        }

//        /// <comentarios/>
//        public ComprobanteAddendaRequestForPaymentTaxTaxCategory taxCategory
//        {
//            get
//            {
//                return this.taxCategoryField;
//            }
//            set
//            {
//                this.taxCategoryField = value;
//            }
//        }

//        /// <comentarios/>
//        [System.Xml.Serialization.XmlIgnoreAttribute()]
//        public bool taxCategorySpecified
//        {
//            get
//            {
//                return this.taxCategoryFieldSpecified;
//            }
//            set
//            {
//                this.taxCategoryFieldSpecified = value;
//            }
//        }

//        /// <comentarios/>
//        [System.Xml.Serialization.XmlAttributeAttribute()]
//        public ComprobanteAddendaRequestForPaymentTaxType type
//        {
//            get
//            {
//                return this.typeField;
//            }
//            set
//            {
//                this.typeField = value;
//            }
//        }

//        /// <comentarios/>
//        [System.Xml.Serialization.XmlIgnoreAttribute()]
//        public bool typeSpecified
//        {
//            get
//            {
//                return this.typeFieldSpecified;
//            }
//            set
//            {
//                this.typeFieldSpecified = value;
//            }
//        }
//    }

//    /// <comentarios/>
//    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.3038")]
//    [System.SerializableAttribute()]
//    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.sat.gob.mx/cfd/2")]
//    public enum ComprobanteAddendaRequestForPaymentTaxTaxCategory
//    {

//        /// <comentarios/>
//        TRANSFERIDO,

//        /// <comentarios/>
//        RETENIDO,
//    }

//    /// <comentarios/>
//    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.3038")]
//    [System.SerializableAttribute()]
//    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.sat.gob.mx/cfd/2")]
//    public enum ComprobanteAddendaRequestForPaymentTaxType
//    {

//        /// <comentarios/>
//        GST,

//        /// <comentarios/>
//        VAT,

//        /// <comentarios/>
//        LAC,
//    }

//    /// <comentarios/>
//    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.3038")]
//    [System.SerializableAttribute()]
//    [System.Diagnostics.DebuggerStepThroughAttribute()]
//    [System.ComponentModel.DesignerCategoryAttribute("code")]
//    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.sat.gob.mx/cfd/2")]
//    public partial class ComprobanteAddendaRequestForPaymentPayableAmount
//    {

//        private decimal amountField;

//        /// <comentarios/>
//        public decimal Amount
//        {
//            get
//            {
//                return this.amountField;
//            }
//            set
//            {
//                this.amountField = value;
//            }
//        }
//    }

//    /// <comentarios/>
//    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.3038")]
//    [System.SerializableAttribute()]
//    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.sat.gob.mx/cfd/2")]
//    public enum ComprobanteAddendaRequestForPaymentDocumentStatus
//    {

//        /// <comentarios/>
//        ORIGINAL,

//        /// <comentarios/>
//        COPY,

//        /// <comentarios/>
//        REEMPLAZA,

//        /// <comentarios/>
//        DELETE,
//    }

//    /// <comentarios/>
//    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.3038")]
//    [System.SerializableAttribute()]
//    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.sat.gob.mx/cfd/2")]
//    public enum ComprobanteTipoDeComprobante
//    {

//        /// <comentarios/>
//        ingreso,

//        /// <comentarios/>
//        egreso,

//        /// <comentarios/>
//        traslado,
//    }
//}
