﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Text.RegularExpressions;
using System.Windows.Forms;
using Generales;
using FE_FX.Clases;

namespace FE_FX
{
    public partial class frmCambioFecha : Form
    {
        Encapsular oEncapsulado;
        string facturas = "";
        public frmCambioFecha(Encapsular oEncapsuladop, string facturasp = "")
        {

            InitializeComponent();

            oEncapsulado = new Encapsular();
            oEncapsulado = oEncapsuladop;

            facturas = facturasp;

        }

        private void guardar()
        {
            try
            {
                string sSQL = "UPDATE RECEIVABLE SET INVOICE_DATE=" + oEncapsulado.oData_ERP.convertir_fecha(INICIO.Value, "i") + " WHERE INVOICE_ID IN (" + facturas + ")";
                oEncapsulado.oData_ERP.EjecutarConsulta(sSQL);

                MessageBox.Show(this, "Fecha cambiada.", Application.ProductName + "-" + Application.ProductVersion, MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            catch (Exception e)
            {
                ErrorFX.mostrar(e, true, true, false);
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            guardar();
        }
    }
}
