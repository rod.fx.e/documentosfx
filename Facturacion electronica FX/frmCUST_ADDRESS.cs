﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;

using System.Text;
using System.Windows.Forms;
using Generales;

namespace FE_FX
{
    public partial class frmCUST_ADDRESS : Form
    {
        private cCONEXCION oData = new cCONEXCION("");
        string devolver = "";
        string CUSTOMER_ID = "";
        public DataGridViewSelectedRowCollection selecionados;

        public frmCUST_ADDRESS(string sConn,string CUSTOMER_IDp, string devolverp = "")
        {
            CUSTOMER_ID=CUSTOMER_IDp;
            oData.sConn = sConn;
            devolver = devolverp;
            InitializeComponent();
            ajax_loader.Visible = false;
            Columnas(dtgrdGeneral);
        }

        private void Columnas(DataGridView dtg)
        {
            int n = 0;

            n = dtg.Columns.Add("ADDR_NO", "No");
            dtg.Columns[n].ReadOnly = true;
            dtg.Columns[n].Visible = true;
            dtg.Columns[n].Width = 45;

            n = dtg.Columns.Add("NAME", "Nombre");
            dtg.Columns[n].ReadOnly = true;
            dtg.Columns[n].Visible = true;
            dtg.Columns[n].Width = 200;

            n = dtg.Columns.Add("VAT_REGISTRATION", "RFC");
            dtg.Columns[n].ReadOnly = true;
            dtg.Columns[n].Visible = true;
            dtg.Columns[n].Width = 100;

            

        }

        private void cargar()
        {
            ajax_loader.Visible = true;
            int n;
            toolStripStatusLabel1.Text = "Cargando ";
            dtgrdGeneral.Rows.Clear();
            string sSQL = "SELECT * FROM CUST_ADDRESS WHERE CUSTOMER_ID='" + CUSTOMER_ID +"' AND NAME LIKE '%" + BUSCAR.Text + "%' ";
            DataTable oDataTable = oData.EjecutarConsulta(sSQL);
            if (oDataTable != null)
            {
                foreach (DataRow oDataRow in oDataTable.Rows)
                {
                    
                    n = dtgrdGeneral.Rows.Add();
                    dtgrdGeneral.Rows[n].Cells["ADDR_NO"].Value = oDataRow["ADDR_NO"].ToString();
                    dtgrdGeneral.Rows[n].Cells["NAME"].Value = oDataRow["NAME"].ToString();
                    dtgrdGeneral.Rows[n].Cells["VAT_REGISTRATION"].Value = oDataRow["VAT_REGISTRATION"].ToString();
                }
            }


            toolStripStatusLabel1.Text = "Total " + dtgrdGeneral.Rows.Count.ToString();
            ajax_loader.Visible = false;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            cargar();
        }

        private void BUSCAR_KeyPress(object sender, KeyPressEventArgs e)
        {
            //Precionar Enter buscar
            if (e.KeyChar.ToString() == "\r")
            {
                cargar();
            }
            
        }

        private void frmUsuarios_Load(object sender, EventArgs e)
        {
            cargar();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            frmEmpresa oObjeto = new frmEmpresa(oData.sConn);
            oObjeto.ShowDialog();
            cargar();
        }

        private void dtgrdGeneral_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            modificar();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            if (dtgrdGeneral.SelectedRows.Count > 0)
            {
                DataGridViewSelectedRowCollection arrSelectedRows = dtgrdGeneral.SelectedRows;
                string ROW_ID = (string)arrSelectedRows[0].Cells[0].Value;
                frmEmpresa oObjeto = new frmEmpresa(oData.sConn, ROW_ID);
                oObjeto.ShowDialog();
                cargar();
            }
        }

        public void modificar()
        {
            if (dtgrdGeneral.SelectedRows.Count > 0)
            {
                DataGridViewSelectedRowCollection arrSelectedRows = dtgrdGeneral.SelectedRows;
                string ROW_ID = (string)arrSelectedRows[0].Cells[0].Value;
                if (devolver == "")
                {

                    frmEmpresa oObjeto = new frmEmpresa(oData.sConn, ROW_ID);
                    oObjeto.ShowDialog();
                    cargar();
                }
                else
                {
                    selecionados = arrSelectedRows;
                    this.DialogResult = DialogResult.OK;
                }
            }
        }

        private void frmEmpresas_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)27)
            {
                this.Close();
            }
            else
            {
                if ((e.KeyChar.ToString() == "<")
                                | (e.KeyChar.ToString() == ">")
                                
                                | (e.KeyChar.ToString() == "'")
                                | (e.KeyChar.ToString() == "#")
                                )
                {
                    e.Handled = true;
                }
            }
        }
    }
}
