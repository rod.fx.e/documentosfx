﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;

using System.Text;
using System.Windows.Forms;
using Generales;

namespace FE_FX
{
    public partial class frmAddendas : Form
    {
        private cCONEXCION oData = new cCONEXCION("");
        string devolver = "";
        public DataGridViewSelectedRowCollection selecionados;


        public frmAddendas(string sConn, string devolverp = "")
        {
            oData.sConn = sConn;
            devolver = devolverp;
            InitializeComponent();
            ajax_loader.Visible = false;
            Columnas(dtgrdGeneral);
        }

        private void Columnas(DataGridView dtg)
        {
            int n = 0;

            n = dtg.Columns.Add("ROW_ID", "ROW_ID");
            dtg.Columns[n].ReadOnly = true;
            dtg.Columns[n].Visible = false;
            dtg.Columns[n].Width = 50;

            n = dtg.Columns.Add("ID", "Addenda");
            dtg.Columns[n].ReadOnly = true;
            dtg.Columns[n].Visible = true;
            dtg.Columns[n].Width = 160;

            n = dtg.Columns.Add("EJECUTABLE", "Ejecutable");
            dtg.Columns[n].ReadOnly = true;
            dtg.Columns[n].Visible = true;
            dtg.Columns[n].Width = 160;

            n = dtg.Columns.Add("CUSTOMER_ID", "Cliente");
            dtg.Columns[n].ReadOnly = true;
            dtg.Columns[n].Visible = true;
            dtg.Columns[n].Width = 160;

        }

        private void cargar()
        {
            ajax_loader.Visible = true;
            int n;
            cADDENDA oObjeto = new cADDENDA();
            toolStripStatusLabel1.Text = "Cargando ";
            dtgrdGeneral.Rows.Clear();
            foreach (cADDENDA registro in oObjeto.todos(BUSCAR.Text))
            {
                n = dtgrdGeneral.Rows.Add();
                dtgrdGeneral.Rows[n].Cells["ROW_ID"].Value = registro.ROW_ID;
                dtgrdGeneral.Rows[n].Cells["ID"].Value = registro.ID;
                dtgrdGeneral.Rows[n].Cells["EJECUTABLE"].Value = registro.EJECUTABLE;
                dtgrdGeneral.Rows[n].Cells["CUSTOMER_ID"].Value = registro.CUSTOMER_ID;
            }
            toolStripStatusLabel1.Text = "Total " + dtgrdGeneral.Rows.Count.ToString();
            ajax_loader.Visible = false;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            cargar();
        }

        private void BUSCAR_KeyPress(object sender, KeyPressEventArgs e)
        {
            //Precionar Enter buscar
            if (e.KeyChar.ToString() == "\r")
            {
                cargar();
            }
            
        }

        private void frmUsuarios_Load(object sender, EventArgs e)
        {
            cargar();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            frmAddenda oObjeto = new frmAddenda(oData.sConn);
            oObjeto.ShowDialog();
            cargar();
        }

        private void dtgrdGeneral_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            modificar();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            if (dtgrdGeneral.SelectedRows.Count > 0)
            {
                DataGridViewSelectedRowCollection arrSelectedRows = dtgrdGeneral.SelectedRows;
                string ROW_ID = (string)arrSelectedRows[0].Cells[0].Value;
                frmAddenda oObjeto = new frmAddenda(oData.sConn, ROW_ID);
                oObjeto.ShowDialog();
                cargar();
            }
        }

        public void modificar()
        {
            if (dtgrdGeneral.SelectedRows.Count > 0)
            {
                DataGridViewSelectedRowCollection arrSelectedRows = dtgrdGeneral.SelectedRows;
                if (devolver == "")
                {
                    
                    string ROW_ID = (string)arrSelectedRows[0].Cells[0].Value;
                    frmAddenda oObjeto = new frmAddenda(oData.sConn, ROW_ID);
                    oObjeto.ShowDialog();
                    cargar();
                }
                else
                {
                    selecionados = arrSelectedRows;
                    this.DialogResult = DialogResult.OK;
                }
            }
        }

        private void frmFormatos_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)27)
            {
                this.Close();
            }
            else
            {
                if ((e.KeyChar.ToString() == "<")
                                | (e.KeyChar.ToString() == ">")
                                
                                | (e.KeyChar.ToString() == "'")
                                | (e.KeyChar.ToString() == "#")
                                )
                {
                    e.Handled = true;
                }
            }
        }
    }
}
