﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;

using System.Text;
using System.Windows.Forms;
using System.IO;
using Generales;

namespace FE_FX
{
    public partial class frmAddenda : Form
    {
        private cCONEXCION oData = new cCONEXCION("");
        private cADDENDA oObjeto;
        private bool modificado;

        public frmAddenda(string sConn)
        {
            oData.sConn = sConn;
            oObjeto = new cADDENDA();
            InitializeComponent();
            ajax_loader.Visible = false;
            
        }
        public frmAddenda(string sConn, string ROW_IDp)
        {
            modificado = false;
            oData.sConn = sConn;
            oObjeto = new cADDENDA();

            InitializeComponent();

            limpiar();
            oObjeto.ROW_ID = ROW_IDp;
            cargar();
        }

        public void limpiar()
        {
            if (modificado)
            {

                DialogResult Resultado = MessageBox.Show("¿Desea guardar los cambios?", Application.ProductName + "-" + Application.ProductVersion, MessageBoxButtons.YesNo);
                if (Resultado.ToString() == "Yes")
                {
                    guardar();
                }


            }
            modificado = false;
            oObjeto = new cADDENDA();
            ID.Text = "";
            CUSTOMER_ID.Text = "";
            EJECUTABLE.Text = "";
        }

        private void guardar()
        {
            ajax_loader.Visible = true;
            oObjeto.ID = ID.Text;
            oObjeto.CUSTOMER_ID = CUSTOMER_ID.Text;
            oObjeto.EJECUTABLE = EJECUTABLE.Text;
            oObjeto.ACTIVO = ACTIVO.Checked.ToString();

            if (oObjeto.guardar())
            {
                toolStripStatusLabel1.Text = "Datos Actualizados " + DateTime.Now.ToShortDateString() + " " + DateTime.Now.ToLongTimeString();
                modificado = false;
            }
            ajax_loader.Visible = false;
        }

        private void cargar()
        {
            ajax_loader.Visible = true;
            if (oObjeto.cargar(oObjeto.ROW_ID))
            {
                ID.Text = oObjeto.ID;
                CUSTOMER_ID.Text = oObjeto.CUSTOMER_ID;
                EJECUTABLE.Text = oObjeto.EJECUTABLE;
                ACTIVO.Checked = bool.Parse(oObjeto.ACTIVO);

                toolStripStatusLabel1.Text  = "Datos Cargados " + DateTime.Now.ToShortDateString() + " " + DateTime.Now.ToLongTimeString();
                modificado = false;

            }
            ajax_loader.Visible = false;
        }

        private void button3_Click(object sender, EventArgs e)
        {
            guardar();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            limpiar();
        }

        private void frmUsuario_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (modificado)
            {
                DialogResult Resultado = MessageBox.Show("¿Desea guardar los cambios?", Application.ProductName + "-" + Application.ProductVersion, MessageBoxButtons.YesNo);
                if (Resultado.ToString() == "Yes")
                {
                    guardar();
                }
            }
        }

        private void frmUsuario_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)27)
            {
                this.Close();
            }
            else
            {
                modificado = true;
                if ((e.KeyChar.ToString() == "<")
                                | (e.KeyChar.ToString() == ">")
                                
                                | (e.KeyChar.ToString() == "'")
                                | (e.KeyChar.ToString() == "#")
                                )
                {
                    e.Handled = true;
                }
            }
        }

        private void button7_Click(object sender, EventArgs e)
        {
            eliminar();
        }

        private void eliminar()
        {
            DialogResult Resultado = MessageBox.Show("¿Esta seguro de eliminar?", Application.ProductName + "-" + Application.ProductVersion, MessageBoxButtons.YesNo);
            if (Resultado.ToString() != "Yes")
            {
                return;
            }
            if (oObjeto.eliminar(oObjeto.ROW_ID))
            {
                limpiar();
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            cargar_archivo();
        }

        private void cargar_archivo()
        {
            if (openFileDialog1.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                EJECUTABLE.Text = openFileDialog1.FileName;
            }
        }

    }
}
