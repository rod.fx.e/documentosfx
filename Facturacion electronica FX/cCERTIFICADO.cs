﻿using System;
using System.Collections.Generic;

using System.Text;
using System.Data;
using System.Windows;
using System.Windows.Forms;
using CryptoSysPKI;
using System.Security.Cryptography.X509Certificates;
using Generales;
using ModeloDocumentosFX;

namespace FE_FX
{
    public class cCERTIFICADO
    {
        private string sSQL = "";
        public string ROW_ID { get; set; }
        public string ID { get; set; }
        public string ROW_ID_EMPRESA { get; set; }
        public string EMPRESA { get; set; }
        public string CERTIFICADO { get; set; }
        public string NO_CERTIFICADO { get; set; }
        public string PASSWORD { get; set; }
        public string FECHA_DESDE { get; set; }
        public string FECHA_HASTA { get; set; }
        public string LLAVE { get; set; }
        public string PFX { get; set; }
        public string ACTIVO { get; set; }

        private cCONEXCION oData = new cCONEXCION("");

        public cCERTIFICADO()
        {
            ModeloDocumentosFXContainer dbContext = new ModeloDocumentosFXContainer();
            try
            {
                string conection = dbContext.Database.Connection.ConnectionString;
                oData = new cCONEXCION(conection);
            }
            catch (Exception err)
            {
                ErrorFX.mostrar(err, false, true, "cCERTIFICADO - 71 - ");
            }
            limpiar();
        }

        //Programar Cambiar
        public bool validar_ruta_archivos()
        {
            return false;

        }

        public bool verficar_password()
        {
            try
            {
                StringBuilder sbPassword;
                StringBuilder sbPrivateKey;

                string strKeyFile = LLAVE;

                sbPassword = new StringBuilder(PASSWORD);

                sbPrivateKey = Rsa.ReadEncPrivateKey(strKeyFile, sbPassword.ToString());

                if (sbPrivateKey.Length == 0)
                {
                    return false;
                }
                else
                {
                    return true;
                }
            }
            catch (Exception e)
            {
                ErrorFX.mostrar(e,true,true, "cCERTIFICADO - 31 - ", false);
            }
            finally
            {

            }
            return false;
        }
        public void limpiar()
        {
            ROW_ID = "";
            ID = "";
            ROW_ID_EMPRESA = "";
            EMPRESA = "";
            CERTIFICADO = "";
            NO_CERTIFICADO = "";
            PASSWORD = "";
            FECHA_DESDE = "";
            FECHA_HASTA = "";
            LLAVE = "";
            PFX = "";
            ACTIVO = "";
        }
        public string getPFX(string NO_CERTIFICADO)
        {
            try
            {
                sSQL = " SELECT * ";
                sSQL += " FROM CERTIFICADO ";
                sSQL += " WHERE NO_CERTIFICADO LIKE '" + NO_CERTIFICADO + "' ";
                DataTable oDataTable = oData.EjecutarConsulta(sSQL);
                foreach (DataRow oDataRow in oDataTable.Rows)
                {
                    cargar(oDataRow["ROW_ID"].ToString());
                    return oDataRow["PFX"].ToString();
                }

            }
            catch(Exception e)
            {
                ErrorFX.mostrar(e, true, true, true);
            }
            return null;
        }
        public List<cCERTIFICADO> todos(string BUSQUEDA)
        {

            List<cCERTIFICADO> lista = new List<cCERTIFICADO>();

            cEMPRESA oEMPRESA=new cEMPRESA(oData);

            sSQL  = " SELECT * ";
            sSQL += " FROM CERTIFICADO ";
            sSQL += " WHERE ID LIKE '%" + BUSQUEDA + "%' ";
            DataTable oDataTable = oData.EjecutarConsulta(sSQL);
            foreach (DataRow oDataRow in oDataTable.Rows)
            {
                oEMPRESA.cargar(oDataRow["ROW_ID_EMPRESA"].ToString());
                lista.Add(new cCERTIFICADO()
                {
                    ROW_ID = oDataRow["ROW_ID"].ToString()
                    ,ID = oDataRow["ID"].ToString()
                    ,ROW_ID_EMPRESA=oDataRow["ROW_ID_EMPRESA"].ToString()
                    ,EMPRESA = oEMPRESA.ID
                    ,CERTIFICADO=oDataRow["CERTIFICADO"].ToString()
                    ,NO_CERTIFICADO=oDataRow["NO_CERTIFICADO"].ToString()
                    ,PASSWORD=oDataRow["PASSWORD"].ToString()
                    ,FECHA_DESDE=oDataRow["FECHA_DESDE"].ToString()
                    ,FECHA_HASTA=oDataRow["FECHA_HASTA"].ToString()
                    ,LLAVE = oDataRow["LlAVE"].ToString()
                    ,
                    PFX = oDataRow["PFX"].ToString()
,
                    ACTIVO = oDataRow["ACTIVO"].ToString()

                });
            }

            return lista;

        }

        public bool eliminar(string ROW_IDp)
        {
            if (ROW_IDp != "")
            {
                sSQL = "DELETE FROM CERTIFICADO WHERE ROW_ID=" + ROW_IDp;
                oData.EjecutarConsulta(sSQL);
                return true;
            }
            return false;
        }

        public bool cargarCertificadoEmpresa(string ROW_ID_EMPRESAp)
        {
            sSQL = " SELECT * ";
            sSQL += " FROM CERTIFICADO ";
            sSQL += " WHERE ROW_ID_EMPRESA=" + ROW_ID_EMPRESAp + " ";
            DataTable oDataTable = oData.EjecutarConsulta(sSQL);
            if (oDataTable != null)
            {

                foreach (DataRow oDataRow in oDataTable.Rows)
                {
                    return cargar(oDataRow["ROW_ID"].ToString());
                }
            }
            return false;
        }

        public bool cargar(string ROW_IDp)
        {
            cEMPRESA oEMPRESA = new cEMPRESA(oData);

            sSQL  = " SELECT * ";
            sSQL += " FROM CERTIFICADO ";
            sSQL += " WHERE ROW_ID=" + ROW_IDp + " ";

            DataTable oDataTable = oData.EjecutarConsulta(sSQL);
            if (oDataTable != null)
            {
                
                foreach (DataRow oDataRow in oDataTable.Rows)
                {
                    //Añadir el Registro
                    oEMPRESA.cargar(oDataRow["ROW_ID_EMPRESA"].ToString());

                    ROW_ID = oDataRow["ROW_ID"].ToString();
                    ID = oDataRow["ID"].ToString();
                    ROW_ID_EMPRESA = oDataRow["ROW_ID_EMPRESA"].ToString();
                    EMPRESA = oEMPRESA.ID;
                    CERTIFICADO = oDataRow["CERTIFICADO"].ToString();
                    NO_CERTIFICADO = oDataRow["NO_CERTIFICADO"].ToString();
                    PASSWORD = cENCRIPTACION.Decrypt(oDataRow["PASSWORD"].ToString());
                    FECHA_DESDE = oDataRow["FECHA_DESDE"].ToString();
                    FECHA_HASTA = oDataRow["FECHA_HASTA"].ToString();
                    LLAVE = oDataRow["LlAVE"].ToString();
                    PFX = oDataRow["PFX"].ToString();
                    ACTIVO = oDataRow["ACTIVO"].ToString();
                    return true;
                }
            }
            return false;
        }

        private bool certificado_valido()
        {
            X509Certificate cert = X509Certificate.CreateFromCertFile(CERTIFICADO);
            string Dato = cert.GetName();
            if (Dato.IndexOf("OU=") > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public bool guardar()
        {
            if (ROW_ID_EMPRESA == "")
            {
                MessageBox.Show("Selecione una Empresa.", Application.ProductName + "-" + Application.ProductVersion, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return false;
            }

            if (CERTIFICADO == "")
            {
                MessageBox.Show("Selecione un certificado.", Application.ProductName + "-" + Application.ProductVersion, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return false;
            }

            if (LLAVE == "")
            {
                MessageBox.Show("Selecione una llave.", Application.ProductName + "-" + Application.ProductVersion, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return false;
            }

            if (PASSWORD.Trim() == "")
            {
                MessageBox.Show("Escriba un password.", Application.ProductName + "-" + Application.ProductVersion, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return false;
            }
            if(!verficar_password())
            {
                MessageBox.Show("La Contraseña es incorrecta, no es posible generar el Sello.", Application.ProductName + "-" + Application.ProductVersion, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return false;
            }
            if (!certificado_valido())
            {
                MessageBox.Show("El Certificado es incorrecto, no es posible generar el Sello.", Application.ProductName + "-" + Application.ProductVersion, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return false;
            }

            //Encriptar el Password
            PASSWORD=cENCRIPTACION.Encrypt(PASSWORD);

            if (ROW_ID == "")
            {

                sSQL = " INSERT INTO CERTIFICADO ";
                sSQL += " ( ";
                sSQL += "  ROW_ID_EMPRESA, ID, CERTIFICADO ";
                sSQL += ", NO_CERTIFICADO, [PASSWORD]";
                sSQL += ", FECHA_DESDE, FECHA_HASTA, LLAVE, PFX ";
                sSQL += ", ACTIVO";
                sSQL += " )";
                sSQL += " VALUES ";
                sSQL += "(";
                sSQL += "  " + ROW_ID_EMPRESA+ ",'" + ID+ "','" + CERTIFICADO + "'";
                sSQL += ",'" + NO_CERTIFICADO+ "','" + PASSWORD + "'";
                sSQL += ",'" + FECHA_DESDE + "','" + FECHA_HASTA + "','" + LLAVE + "','" + PFX + "'";
                sSQL += ",'" + ACTIVO + "'";
                sSQL += ")";
                DataTable oDataTable = oData.EjecutarConsulta(sSQL);
                if (oDataTable != null)
                {

                    sSQL = "SELECT TOP 1 MAX(ROW_ID) as ROW_ID FROM CERTIFICADO";
                    oDataTable = oData.EjecutarConsulta(sSQL);
                    if (oDataTable != null)
                    {
                        foreach (DataRow oDataRow in oDataTable.Rows)
                        {
                            ROW_ID = oDataRow["ROW_ID"].ToString();
                        }
                    }
                }
                else
                {
                    return false;
                }

            }
            else
            {
                sSQL = " UPDATE CERTIFICADO SET ";
                sSQL += "  ROW_ID_EMPRESA=" + ROW_ID_EMPRESA+ ",ID='" + ID+ "',CERTIFICADO='" + CERTIFICADO + "'";
                sSQL += ",NO_CERTIFICADO='" + NO_CERTIFICADO+ "',[PASSWORD]='" + PASSWORD + "'";
                sSQL += ",FECHA_DESDE='" + FECHA_DESDE+ "',FECHA_HASTA='" + FECHA_HASTA+ "',LLAVE='" + LLAVE + "'";
                sSQL += ",PFX='" + PFX + "'";
                sSQL += ",ACTIVO='" + ACTIVO + "'";
                sSQL += " WHERE ROW_ID=" + ROW_ID + " ";
                oData.EjecutarConsulta(sSQL);

            }
            return true;

        }

        internal bool cargar_NumCert(string numeroCertificado)
        {
            try
            {
                sSQL = " SELECT * ";
                sSQL += " FROM CERTIFICADO ";
                sSQL += " WHERE NO_CERTIFICADO='" + numeroCertificado + "' ";
                DataTable oDataTable = oData.EjecutarConsulta(sSQL);
                if (oDataTable != null)
                {

                    foreach (DataRow oDataRow in oDataTable.Rows)
                    {
                        return cargar(oDataRow["ROW_ID"].ToString());
                    }

                }
                return false;
            }
            catch (Exception e)
            {
                ErrorFX.mostrar(e, true, true, "cCERTIFICADO - 301 - Cargando el Certificado",true);
            }
            return false;
        }

        public bool validar_vencimiento()
        {
            string DIAS = "10";
            DateTime fecha_HASTA = DateTime.Parse(FECHA_HASTA);
            fecha_HASTA.AddDays(double.Parse(DIAS));
            if (fecha_HASTA < DateTime.Now)
            {
                MessageBox.Show("El Certificado tiene " + DIAS + " día(s) para caducarse, fecha de caducidad " + FECHA_HASTA + ".", Application.ProductName + "-" + Application.ProductVersion, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return false;
            }
            return true;
        }

        public bool validar()
        {
            DateTime fecha_DESDE = DateTime.Parse(FECHA_DESDE);
            DateTime fecha_HASTA = DateTime.Parse(FECHA_HASTA);


            if (fecha_DESDE > DateTime.Now)
            {
                MessageBox.Show("El Certificado configurado no se puede usar porque es valido desde la fecha " + FECHA_DESDE + ".", Application.ProductName + "-" + Application.ProductVersion, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return false;
            }

            if (fecha_HASTA < DateTime.Now)
            {
                MessageBox.Show("El Certificado esta caducado, fecha de caducidad " + FECHA_HASTA + ".", Application.ProductName + "-" + Application.ProductVersion, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return false;
            }
            return true;
        }
    }
}
