﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;

using System.Text;
using System.Windows.Forms;
using Generales;

namespace FE_FX
{
    public partial class frmUsuarios : Form
    {
        private cCONEXCION oData = new cCONEXCION("");
        string devolver = "";

        public frmUsuarios(string sConn, string devolverp = "")
        {
            oData.sConn = sConn;
            devolver = devolverp;
            InitializeComponent();
            ajax_loader.Visible = false;
            Columnas(dtgrdGeneral);
        }

        private void Columnas(DataGridView dtg)
        {
            int n = 0;

            n = dtg.Columns.Add("ROW_ID", "ROW_ID");
            dtg.Columns[n].ReadOnly = true;
            dtg.Columns[n].Visible = false;
            dtg.Columns[n].Width = 50;

            n = dtg.Columns.Add("USUARIO", "Usuario");
            dtg.Columns[n].ReadOnly = true;
            dtg.Columns[n].Visible = true;
            dtg.Columns[n].Width = 160;

            n = dtg.Columns.Add("NOMBRE", "Nombre");
            dtg.Columns[n].ReadOnly = true;
            dtg.Columns[n].Visible = true;
            dtg.Columns[n].Width = 160;

            n = dtg.Columns.Add("APELLIDO", "Apellido");
            dtg.Columns[n].ReadOnly = true;
            dtg.Columns[n].Visible = true;
            dtg.Columns[n].Width = 160;
        }

        private void cargar()
        {
            ajax_loader.Visible = true;
            int n;
            cUSUARIO oObjeto = new cUSUARIO();
            toolStripStatusLabel1.Text = "Cargando ";
            dtgrdGeneral.Rows.Clear();
            foreach (cUSUARIO registro in oObjeto.todos(BUSCAR.Text))
            {
                n = dtgrdGeneral.Rows.Add();
                dtgrdGeneral.Rows[n].Cells["ROW_ID"].Value = registro.ROW_ID;
                dtgrdGeneral.Rows[n].Cells["USUARIO"].Value = registro.USUARIO;
                dtgrdGeneral.Rows[n].Cells["NOMBRE"].Value = registro.NOMBRE;
                dtgrdGeneral.Rows[n].Cells["APELLIDO"].Value = registro.APELLIDO;
            }
            toolStripStatusLabel1.Text = "Total " + dtgrdGeneral.Rows.Count.ToString();
            ajax_loader.Visible = false;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            cargar();
        }

        private void BUSCAR_KeyPress(object sender, KeyPressEventArgs e)
        {

            if ((e.KeyChar.ToString() == "<")
                | (e.KeyChar.ToString() == ">")
                
                | (e.KeyChar.ToString() == "'")
                | (e.KeyChar.ToString() == "#")
                )
            {
                e.Handled = true;
            }

            //Precionar Enter buscar
            if (e.KeyChar.ToString() == "\r")
            {
                cargar();
            }
            
        }

        private void frmUsuarios_Load(object sender, EventArgs e)
        {
            cargar();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            frmUsuario oObjeto = new frmUsuario(oData.sConn);
            oObjeto.ShowDialog();
            cargar();
        }

        private void dtgrdGeneral_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            modificar();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            if (dtgrdGeneral.SelectedRows.Count > 0)
            {
                DataGridViewSelectedRowCollection arrSelectedRows = dtgrdGeneral.SelectedRows;
                string ROW_ID = (string)arrSelectedRows[0].Cells[0].Value;
                frmUsuario oObjeto = new frmUsuario(oData.sConn, ROW_ID);
                oObjeto.ShowDialog();
                cargar();
            }
        }

        public void modificar()
        {
            if (dtgrdGeneral.SelectedRows.Count > 0)
            {
                if (devolver == "")
                {
                    DataGridViewSelectedRowCollection arrSelectedRows = dtgrdGeneral.SelectedRows;
                    string ROW_ID = (string)arrSelectedRows[0].Cells[0].Value;
                    frmUsuario oObjeto = new frmUsuario(oData.sConn, ROW_ID);
                    oObjeto.ShowDialog();
                    cargar();
                }
                else
                {
                    this.DialogResult = DialogResult.OK;
                }
            }
        }

        private void frmUsuarios_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)27)
            {
                this.Close();
            }
            else
            {
                if ((e.KeyChar.ToString() == "<")
                                | (e.KeyChar.ToString() == ">")
                                
                                | (e.KeyChar.ToString() == "'")
                                | (e.KeyChar.ToString() == "#")
                                )
                {
                    e.Handled = true;
                }
            }
        }
    }
}
