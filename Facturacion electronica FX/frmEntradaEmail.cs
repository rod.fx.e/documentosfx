﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;

using System.Text;
using System.Windows.Forms;
using System.Text.RegularExpressions;

namespace FE_FX
{
    public partial class frmEntradaEmail : Form
    {
        public string EMAIL = "";
        public frmEntradaEmail()
        {
            EMAIL = "";
            InitializeComponent();
        }

        public static bool isEmail(string inputEmail)
        {
            if (inputEmail == null || inputEmail.Length == 0)
            {
                throw new ArgumentNullException("inputEmail");
            }

            const string expression = @"^([a-zA-Z0-9_\-\.]+)@((\[[0-9]{1,3}" +
                                      @"\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9\-]+\" +
                                      @".)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$";

            Regex regex = new Regex(expression);
            return regex.IsMatch(inputEmail);
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            EMAIL = EMAIL_text.Text;
            if(EMAIL.Trim()=="")
            {
                MessageBox.Show("El valor introducido no es una dirección de email con un formato válido.", Application.ProductName + "-" + Application.ProductVersion, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return;
            }
            if (isEmail(EMAIL))
            {
                DialogResult = DialogResult.OK;
            }
            else
            {
                MessageBox.Show("El valor introducido no es una dirección de email con un formato válido.", Application.ProductName + "-" + Application.ProductVersion, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return;
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Cancel;
        }
    }
}
