﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.OleDb;
using System.Drawing;
using System.Windows.Forms;
using Generales;

namespace FE_FX
{
    class cSCRIPT
    {
        private cCONEXCION oData = new cCONEXCION("");

        public void verificar_version()
        {
            string directorio = AppDomain.CurrentDomain.BaseDirectory;
            string BD = directorio + "BD.accdb";
            oData = new cCONEXCION(BD, "", "");
            


            //Existe la TABLA Version
            oData.CrearConexion();
            DataTable oDataTable = oData.oConn.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, new Object[] { null, null, "APLICACION_VERSION", null });
            if (oDataTable.Rows.Count > 0)
            {
                //Leer Version
                string sSQL = " SELECT * ";
                sSQL += " FROM APLICACION_VERSION ";
                oDataTable = oData.EjecutarConsulta(sSQL);
                foreach (DataRow oDataRow in oDataTable.Rows)
                {
                    string NO_VERSION = oDataRow["NO_VERSION"].ToString();
                    actualizar_version_3_2_0_45();
                    actualizar_envio_mail();
                    if (NO_VERSION != Application.ProductVersion)
                    {
                        switch (NO_VERSION)
                        {
                            case "2.0.0.6":
                                actualizar_version_3_2_0_7();
                                break;
                            case "3.2.0.6":
                                actualizar_version_3_2_0_7();
                                break;
                            case "3.2.0.7":
                                actualizar_version_3_2_0_8();
                                break;
                            case "3.2.0.8":
                                actualizar_version_3_2_0_9();
                                break;
                            case "3.2.0.9":
                                actualizar_version_3_2_0_10();
                                break;
                            case "3.2.0.10":
                                actualizar_version_3_2_0_11();
                                break;
                            case "3.2.0.11":
                            case "3.2.0.12":
                                actualizar_version_3_2_0_13();
                                break;
                            case "3.2.0.13":
                                actualizar_version_3_2_0_14();
                                break;
                            case "3.2.0.15":
                                actualizar_version_3_2_0_15();
                                break;
                            case "3.2.0.16":
                                actualizar_version_3_2_0_16();
                                break;
                            case "3.2.0.17":
                            case "3.2.0.18":
                            case "3.2.0.19":
                            case "3.2.0.20":
                            case "3.2.0.21":
                            case "3.2.0.22":
                            case "3.2.0.23":
                            case "3.2.0.24":
                                actualizar_version_3_2_0_25();
                                break;
                            case "3.2.0.25":
                                actualizar_version_3_2_0_26();
                                break;
                            case "3.2.0.26":
                                actualizar_version_3_2_0_30();
                                break;
                            case "3.2.0.27":
                            case "3.2.0.28":
                            case "3.2.0.29":
                            case "3.2.0.30":
                            case "3.2.0.31":
                            case "3.2.0.32":
                            case "3.2.0.33":
                            case "3.2.0.34":
                            case "3.2.0.35":
                                actualizar_version_3_2_0_35();
                                break;
                            case "3.2.0.36":
                            case "3.2.0.37":
                            case "3.2.0.38":
                            case "3.2.0.39":
                            case "3.2.0.40":
                            case "3.2.0.41":
                            case "3.2.0.42":
                            case "3.2.0.43":
                            case "3.2.0.44":
                            case "3.2.0.45":
                                actualizar_version_3_2_0_45();
                                break;
                            case "3.2.0.46":
                            case "3.2.0.47":
                            case "3.2.0.48":
                                actualizar_version_3_2_0_48();
                                break;
                            case "3.2.0.49":
                                actualizar_version_3_2_0_49();
                                break;
                            case "3.2.0.50":
                                actualizar_version_3_2_0_50();
                                break;
                            case "3.2.0.51":
                                actualizar_version_3_2_0_51();
                                break;
                            case "3.2.0.52":
                                actualizar_version_3_2_0_52();
                                break;
                            case "3.2.0.53":
                                actualizar_version_3_2_0_53();
                                break;
                            case "3.2.0.54":
                                actualizar_version_3_2_0_54();
                                break;
                            case "3.2.0.55":
                                actualizar_version_3_2_0_55();
                                break;
                            case "4.0.0.0":
                            case "4.0.0.1":
                            case "4.0.0.2":
                            case "4.0.0.3":
                            case "4.0.0.4":
                                actualizar_version_4_0_0_0();
                                break;
                            case "4.0.0.5":
                                actualizar_version_4_0_0_5();
                                break;
                            case "4.0.0.7":
                            case "4.0.0.8":
                                actualizar_version_4_0_0_7();
                                break;
                            case "4.0.0.9":
                                actualizar_version_4_0_0_10();
                                break;
                            case "4.0.0.10":
                                actualizar_version_4_0_0_10();
                                break;
                            case "4.0.0.12":
                                actualizar_version_4_0_0_12();
                                break;
                            case "4.0.0.13":
                                actualizar_version_4_0_0_13();
                                break;
                            case "4.0.0.14":
                            case "4.0.0.15":
                                actualizar_version_4_0_0_15();
                                break;
                            case "4.0.0.16":
                                actualizar_version_4_0_0_16();
                                break;
                            case "4.0.0.17":
                                actualizar_version_4_0_0_17();
                                break;
                            default:
                                actualizar_version_4_0_0_18();
                                break;
                        }
                    }
                }

            }
            else
            {
                string strTemp = " NO_VERSION Text ";
                OleDbCommand myCommand = new OleDbCommand();
                myCommand.Connection = oData.oConn;
                myCommand.CommandText = "CREATE TABLE APLICACION_VERSION(" + strTemp + ")";
                myCommand.ExecuteNonQuery();
                myCommand.CommandText = "INSERT INTO APLICACION_VERSION (NO_VERSION) VALUES ('" + Application.ProductVersion + "')";
                myCommand.ExecuteNonQuery();
                myCommand.Connection.Close();
                actualizar_version_1002();

            }
            oData.DestruirConexion();
        }

        private void actualizar_version_3_2_0_54()
        {
            oData.CrearConexion();
            OleDbCommand myCommand = new OleDbCommand();
            myCommand.Connection = oData.oConn;

            try
            {
                string sSQL = "";
                sSQL = "ALTER TABLE EMPRESA ADD ACTIVAR_COMPLEMENTO_CCE TEXT ";
                myCommand.CommandText = sSQL;
                myCommand.ExecuteNonQuery();
            }
            catch
            {
            }

            myCommand.Connection.Close();
            oData.DestruirConexion();
            actualizar_version_3_2_0_55();
        }
        private void actualizar_version_4_0_0_0()
        {
            actualizar_version_3_2_0_55();
            actualizar_version_4_0_0_5();

        }

        private void actualizar_version_4_0_0_5()
        {
            oData.CrearConexion();
            OleDbCommand myCommand = new OleDbCommand();
            myCommand.Connection = oData.oConn;
            
            try
            {
                myCommand.CommandText = "ALTER TABLE EMPRESA ADD APROXIMACION TEXT DEFAULT 'Redondeo' ";
                myCommand.ExecuteNonQuery();
            }
            catch
            {
            }

            try
            {
                myCommand.CommandText = "ALTER TABLE EMPRESA ADD VALORES_DECIMALES TEXT DEFAULT '2' ";
                myCommand.ExecuteNonQuery();
            }
            catch
            {
            }

            myCommand.CommandText = "UPDATE APLICACION_VERSION SET NO_VERSION='" + Application.ProductVersion + "'";
            myCommand.ExecuteNonQuery();

            myCommand.Connection.Close();
            oData.DestruirConexion();
            actualizar_version_4_0_0_7();
        }

        private void actualizar_version_4_0_0_7()
        {
            oData.CrearConexion();
            OleDbCommand myCommand = new OleDbCommand();
            myCommand.Connection = oData.oConn;

            try
            {
                myCommand.CommandText = "ALTER TABLE USUARIO ADD PROFORMA TEXT DEFAULT 'False' ";
                myCommand.ExecuteNonQuery();
            }
            catch
            {
            }

            try
            {
                myCommand.CommandText = "ALTER TABLE USUARIO ADD COMPROBANTE_PAGO TEXT DEFAULT 'False' ";
                myCommand.ExecuteNonQuery();
            }
            catch
            {
            }

            myCommand.CommandText = "UPDATE APLICACION_VERSION SET NO_VERSION='" + Application.ProductVersion + "'";
            myCommand.ExecuteNonQuery();

            myCommand.Connection.Close();
            oData.DestruirConexion();
            actualizar_version_4_0_0_10();
        }


        private void actualizar_version_4_0_0_10()
        {
            oData.CrearConexion();
            OleDbCommand myCommand = new OleDbCommand();
            myCommand.Connection = oData.oConn;

            try
            {
                myCommand.CommandText = "ALTER TABLE USUARIO ADD PROFORMA TEXT DEFAULT 'False' ";
                myCommand.ExecuteNonQuery();
            }
            catch
            {
            }

            try
            {
                myCommand.CommandText = "ALTER TABLE USUARIO ADD COMPROBANTE_PAGO TEXT DEFAULT 'False' ";
                myCommand.ExecuteNonQuery();
            }
            catch
            {
            }

            try
            {
                myCommand.CommandText = "ALTER TABLE EMPRESA ADD APROXIMACION TEXT DEFAULT 'Redondeo' ";
                myCommand.ExecuteNonQuery();
            }
            catch
            {
            }

            try
            {
                myCommand.CommandText = "ALTER TABLE EMPRESA ADD VALORES_DECIMALES TEXT DEFAULT '2' ";
                myCommand.ExecuteNonQuery();
            }
            catch
            {
            }

            try
            {

                myCommand.CommandText = @"
                CREATE TABLE cfdiPoliza
                (
                  ROW_ID AUTOINCREMENT
                , [fecha]  TEXT
                , [factura] NUMBER
                , [cuentaContable] TEXT
                , [referencia] TEXT
                , [cargo] NUMBER
                , [abono] NUMBER
                , [estado] TEXT
                )
                ";
                myCommand.ExecuteNonQuery();
            }
            catch
            {
            }

            myCommand.CommandText = "UPDATE APLICACION_VERSION SET NO_VERSION='" + Application.ProductVersion + "'";
            myCommand.ExecuteNonQuery();

            myCommand.Connection.Close();
            oData.DestruirConexion();
            actualizar_version_4_0_0_12();
        }

        private void actualizar_version_4_0_0_12()
        {
            oData.CrearConexion();
            OleDbCommand myCommand = new OleDbCommand();
            myCommand.Connection = oData.oConn;

            try
            {
                myCommand.CommandText = "ALTER TABLE USUARIO ADD PROFORMA TEXT DEFAULT 'False' ";
                myCommand.ExecuteNonQuery();
            }
            catch
            {
            }

            try
            {
                myCommand.CommandText = "ALTER TABLE USUARIO ADD COMPROBANTE_PAGO TEXT DEFAULT 'False' ";
                myCommand.ExecuteNonQuery();
            }
            catch
            {
            }

            try
            {
                myCommand.CommandText = "ALTER TABLE EMPRESA ADD APROXIMACION TEXT DEFAULT 'Redondeo' ";
                myCommand.ExecuteNonQuery();
            }
            catch
            {
            }

            try
            {
                myCommand.CommandText = "ALTER TABLE EMPRESA ADD VALORES_DECIMALES TEXT DEFAULT '2' ";
                myCommand.ExecuteNonQuery();
            }
            catch
            {
            }

            try
            {

                myCommand.CommandText = @"
                CREATE TABLE cfdiPoliza
                (
                  ROW_ID AUTOINCREMENT
                , [fecha]  TEXT
                , [factura] NUMBER
                , [cuentaContable] TEXT
                , [referencia] TEXT
                , [cargo] NUMBER
                , [abono] NUMBER
                , [estado] TEXT
                )
                ";
                myCommand.ExecuteNonQuery();
            }
            catch
            {
            }

            myCommand.CommandText = "UPDATE APLICACION_VERSION SET NO_VERSION='" + Application.ProductVersion + "'";
            myCommand.ExecuteNonQuery();

            myCommand.Connection.Close();
            oData.DestruirConexion();

            actualizar_version_4_0_0_13();
        }

        private void actualizar_version_4_0_0_13()
        {
            oData.CrearConexion();
            OleDbCommand myCommand = new OleDbCommand();
            myCommand.Connection = oData.oConn;

            try
            {
                myCommand.CommandText = "ALTER TABLE USUARIO ADD proformaConfiguracion TEXT DEFAULT 'False' ";
                myCommand.ExecuteNonQuery();
            }
            catch
            {
            }

            
            myCommand.CommandText = "UPDATE APLICACION_VERSION SET NO_VERSION='" + Application.ProductVersion + "'";
            myCommand.ExecuteNonQuery();

            myCommand.Connection.Close();
            oData.DestruirConexion();
            actualizar_version_4_0_0_15();
        }


        private void actualizar_version_4_0_0_15()
        {
            oData.CrearConexion();
            OleDbCommand myCommand = new OleDbCommand();
            myCommand.Connection = oData.oConn;

            try
            {

                myCommand.CommandText = "ALTER TABLE SERIE ADD rfcExcepcion TEXT DEFAULT '' ";
                myCommand.ExecuteNonQuery();
            }
            catch
            {
            }

            myCommand.CommandText = "UPDATE APLICACION_VERSION SET NO_VERSION='" + Application.ProductVersion + "'";
            myCommand.ExecuteNonQuery();

            myCommand.Connection.Close();
            oData.DestruirConexion();

            actualizar_version_4_0_0_16();
        }

        
        private void actualizar_version_4_0_0_16()
        {
            oData.CrearConexion();
            OleDbCommand myCommand = new OleDbCommand();
            myCommand.Connection = oData.oConn;

            try
            {

                myCommand.CommandText = "ALTER TABLE CCE ADD excelProductCommodityFraccion TEXT DEFAULT '' ";
                myCommand.ExecuteNonQuery();
            }
            catch
            {
            }

            myCommand.CommandText = "UPDATE APLICACION_VERSION SET NO_VERSION='" + Application.ProductVersion + "'";
            myCommand.ExecuteNonQuery();

            myCommand.Connection.Close();
            oData.DestruirConexion();

            actualizar_version_4_0_0_17();
        }

        private void actualizar_version_4_0_0_17()
        {
            oData.CrearConexion();
            OleDbCommand myCommand = new OleDbCommand();
            myCommand.Connection = oData.oConn;

            try
            {

                myCommand.CommandText = "ALTER TABLE CCE ADD excelProductCommodityFraccion TEXT DEFAULT '' ";
                myCommand.ExecuteNonQuery();
            }
            catch
            {
            }

            myCommand.CommandText = "UPDATE APLICACION_VERSION SET NO_VERSION='" + Application.ProductVersion + "'";
            myCommand.ExecuteNonQuery();

            myCommand.Connection.Close();
            oData.DestruirConexion();

            actualizar_version_4_0_0_18();

        }

        private void actualizar_version_4_0_0_18()
        {
            oData.CrearConexion();
            OleDbCommand myCommand = new OleDbCommand();
            myCommand.Connection = oData.oConn;
            try
            {
                string sSQL = "";
                sSQL = "ALTER TABLE CCE ADD PART_ID_EXCEPCIONES TEXT ";
                myCommand.CommandText = sSQL;
                myCommand.ExecuteNonQuery();
            }
            catch
            {
            }

            try
            {
                string sSQL = "";
                sSQL = "ALTER TABLE CCE ADD PRODUCTCODE_EXCEPCIONES TEXT ";
                myCommand.CommandText = sSQL;
                myCommand.ExecuteNonQuery();
            }
            catch
            {
            }


            myCommand.CommandText = "UPDATE APLICACION_VERSION SET NO_VERSION='" + Application.ProductVersion + "'";
            myCommand.ExecuteNonQuery();

            myCommand.Connection.Close();
            oData.DestruirConexion();
        }


        private void actualizar_version_3_2_0_55()
        {
            oData.CrearConexion();
            OleDbCommand myCommand = new OleDbCommand();
            myCommand.Connection = oData.oConn;

            try
            {
                myCommand.CommandText = "ALTER TABLE SERIE ADD ACCOUNT_ID_ANTICIPO TEXT ";
                myCommand.ExecuteNonQuery();
            }
            catch
            {
            }

            myCommand.CommandText = "UPDATE APLICACION_VERSION SET NO_VERSION='" + Application.ProductVersion + "'";
            myCommand.ExecuteNonQuery();

            myCommand.Connection.Close();
            oData.DestruirConexion();
        }


        private void actualizar_version_3_2_0_45()
        {
            oData.CrearConexion();
            OleDbCommand myCommand = new OleDbCommand();
            myCommand.Connection = oData.oConn;

            try
            {
                myCommand.CommandText = "ALTER TABLE EMPRESA ADD DESCUENTO_LINEA TEXT ";
                myCommand.ExecuteNonQuery();
            }
            catch
            {
            }

            myCommand.CommandText = "UPDATE APLICACION_VERSION SET NO_VERSION='" + Application.ProductVersion + "'";
            myCommand.ExecuteNonQuery();

            myCommand.Connection.Close();
            oData.DestruirConexion();
            actualizar_version_3_2_0_48();
        }

        private void actualizar_version_3_2_0_48()
        {
            oData.CrearConexion();
            OleDbCommand myCommand = new OleDbCommand();
            myCommand.Connection = oData.oConn;

            try
            {
                myCommand.CommandText = "ALTER TABLE EMPRESA ADD CUSTOMER_ESPECIFICACION_LINEA TEXT ";
                myCommand.ExecuteNonQuery();
            }
            catch
            {
            }

            myCommand.CommandText = "UPDATE APLICACION_VERSION SET NO_VERSION='" + Application.ProductVersion + "'";
            myCommand.ExecuteNonQuery();

            myCommand.Connection.Close();
            oData.DestruirConexion();

            actualizar_version_3_2_0_49();
        }


        private void actualizar_version_3_2_0_49()
        {
            oData.CrearConexion();
            OleDbCommand myCommand = new OleDbCommand();
            myCommand.Connection = oData.oConn;


            try
            {
                myCommand.CommandText = "ALTER TABLE CLIENTE_CONFIGURACION ADD LEYENDA_FISCAL TEXT ";
                myCommand.ExecuteNonQuery();
            }
            catch
            {
            }

            try
            {
                myCommand.CommandText = "ALTER TABLE CLIENTE_CONFIGURACION ADD FORMATO TEXT ";
                myCommand.ExecuteNonQuery();
            }
            catch
            {
            }

            try
            {
                myCommand.CommandText = "ALTER TABLE CLIENTE_CONFIGURACION ADD CORREO TEXT ";
                myCommand.ExecuteNonQuery();
            }
            catch
            {
            }

            myCommand.CommandText = "UPDATE APLICACION_VERSION SET NO_VERSION='" + Application.ProductVersion + "'";
            myCommand.ExecuteNonQuery();

            myCommand.Connection.Close();
            oData.DestruirConexion();
            actualizar_version_3_2_0_50();
        }

        private void actualizar_version_3_2_0_50()
        {
            oData.CrearConexion();
            OleDbCommand myCommand = new OleDbCommand();
            myCommand.Connection = oData.oConn;


            try
            {
                myCommand.CommandText = "ALTER TABLE EMPRESA ADD IMPRESION_AUTOMATICA TEXT DEFAULT 'false'";
                myCommand.ExecuteNonQuery();
            }
            catch
            {
            }

            try
            {
                myCommand.CommandText = "ALTER TABLE EMPRESA ADD IMPRESORA TEXT ";
                myCommand.ExecuteNonQuery();
            }
            catch
            {
            }

            myCommand.CommandText = "UPDATE APLICACION_VERSION SET NO_VERSION='" + Application.ProductVersion + "'";
            myCommand.ExecuteNonQuery();

            myCommand.Connection.Close();
            oData.DestruirConexion();

            actualizar_version_3_2_0_51();
        }

        private void actualizar_version_3_2_0_51()
        {
            oData.CrearConexion();
            OleDbCommand myCommand = new OleDbCommand();
            myCommand.Connection = oData.oConn;


            try
            {
                myCommand.CommandText = "ALTER TABLE EMPRESA ADD IMPRESION_AUTOMATICA_NDC TEXT DEFAULT 'false'";
                myCommand.ExecuteNonQuery();
            }
            catch
            {
            }

            myCommand.CommandText = "UPDATE APLICACION_VERSION SET NO_VERSION='" + Application.ProductVersion + "'";
            myCommand.ExecuteNonQuery();

            myCommand.Connection.Close();
            oData.DestruirConexion();
            actualizar_version_3_2_0_52();
        }

        private void actualizar_version_3_2_0_52()
        {
            oData.CrearConexion();
            OleDbCommand myCommand = new OleDbCommand();
            myCommand.Connection = oData.oConn;

            try
            {
                string sSQL = "";
                sSQL += "CREATE TABLE CCE  ";
                sSQL += " (ROW_ID AUTOINCREMENT, [UDFVMPRTMNT]  TEXT,[ROW_ID_EMPRESA] NUMBER, [CUSTOMER_EXCEPCIONES] TEXT)";
                myCommand.CommandText = sSQL;
                myCommand.ExecuteNonQuery();
            }
            catch
            {
            }

            myCommand.Connection.Close();
            oData.DestruirConexion();
            actualizar_version_3_2_0_53();
        }

        private void actualizar_version_3_2_0_53()
        {
            oData.CrearConexion();
            OleDbCommand myCommand = new OleDbCommand();
            myCommand.Connection = oData.oConn;

            try
            {
                string sSQL = "";
                sSQL = "ALTER TABLE CCE ADD CUSTOMER_EXCEPCIONES TEXT ";
                myCommand.CommandText = sSQL;
                myCommand.ExecuteNonQuery();
            }
            catch
            {
            }

            myCommand.Connection.Close();
            oData.DestruirConexion();
            actualizar_version_3_2_0_54();
        }

        private void actualizar_version_3_2_0_35()
        {
            oData.CrearConexion();
            OleDbCommand myCommand = new OleDbCommand();
            myCommand.Connection = oData.oConn;

            try
            {
                myCommand.CommandText = "ALTER TABLE SERIE ADD ACCOUNT_ID_ANTICIPO TEXT ";
                myCommand.ExecuteNonQuery();
            }
            catch
            {
            }

            myCommand.CommandText = "UPDATE APLICACION_VERSION SET NO_VERSION='" + Application.ProductVersion + "'";
            myCommand.ExecuteNonQuery();

            myCommand.Connection.Close();
            oData.DestruirConexion();

            actualizar_version_3_2_0_45();
        }

        private void actualizar_version_3_2_0_30()
        {
            oData.CrearConexion();
            OleDbCommand myCommand = new OleDbCommand();
            myCommand.Connection = oData.oConn;

            try
            {
                myCommand.CommandText = "ALTER TABLE CLIENTE_CONFIGURACION ADD LEYENDA_FISCAL TEXT ";
                myCommand.ExecuteNonQuery();
            }
            catch
            {
            }
            try
            {
                myCommand.CommandText = "ALTER TABLE CLIENTE_CONFIGURACION ADD LEYENDA_FISCAL TEXT ";
                myCommand.ExecuteNonQuery();
            }
            catch
            {
            }

            myCommand.CommandText = "UPDATE APLICACION_VERSION SET NO_VERSION='" + Application.ProductVersion + "'";
            myCommand.ExecuteNonQuery();

            myCommand.Connection.Close();
            oData.DestruirConexion();
            actualizar_version_3_2_0_35();
        }

        private void actualizar_version_3_2_0_25()
        {
            actualizar_version_3_2_0_20();
            oData.CrearConexion();
            OleDbCommand myCommand = new OleDbCommand();
            myCommand.Connection = oData.oConn;
            try
            {
                myCommand.CommandText = "ALTER TABLE EMPRESA ADD SITE_ID TEXT ";
                myCommand.ExecuteNonQuery();
            }
            catch
            {
            }
            try
            {
                myCommand.CommandText = "ALTER TABLE EMPRESA ADD BD_AUXILIAR TEXT ";
                myCommand.ExecuteNonQuery();
            }
            catch
            {
            }

            myCommand.CommandText = "UPDATE APLICACION_VERSION SET NO_VERSION='" + Application.ProductVersion + "'";
            myCommand.ExecuteNonQuery();

            myCommand.Connection.Close();
            oData.DestruirConexion();
            actualizar_version_3_2_0_26();
        }


        private void actualizar_version_3_2_0_26()
        {

            oData.CrearConexion();
            OleDbCommand myCommand = new OleDbCommand();
            myCommand.Connection = oData.oConn;
            try
            {
                myCommand.CommandText = "ALTER TABLE EMPRESA ADD SITE_ID TEXT ";
                myCommand.ExecuteNonQuery();
            }
            catch
            {
            }
            try
            {
                myCommand.CommandText = "ALTER TABLE EMPRESA ADD BD_AUXILIAR TEXT ";
                myCommand.ExecuteNonQuery();
            }
            catch
            {
            }

            try
            {
                myCommand.CommandText = "ALTER TABLE CLIENTE_CONFIGURACION ADD MENSAJE TEXT ";
                myCommand.ExecuteNonQuery();
            }
            catch
            {
            }
            try
            {
                myCommand.CommandText = "ALTER TABLE CLIENTE_CONFIGURACION ADD ARCHIVO TEXT ";
                myCommand.ExecuteNonQuery();
            }
            catch
            {
            }

            myCommand.CommandText = "UPDATE APLICACION_VERSION SET NO_VERSION='" + Application.ProductVersion + "'";
            myCommand.ExecuteNonQuery();

            myCommand.Connection.Close();
            oData.DestruirConexion();
        }


        private void actualizar_version_20001()
        {
            oData.CrearConexion();
            OleDbCommand myCommand = new OleDbCommand();
            myCommand.Connection = oData.oConn;

            try
            {
                myCommand.CommandText = "ALTER TABLE SERIE ADD ARCHIVO_NOMBRE TEXT ";
                myCommand.ExecuteNonQuery();
            }
            catch
            {
            }

            myCommand.CommandText = "UPDATE APLICACION_VERSION SET NO_VERSION='" + Application.ProductVersion + "'";
            myCommand.ExecuteNonQuery();

            myCommand.Connection.Close();
            oData.DestruirConexion();
            actualizar_version_20002();
        }

        private void actualizar_version_20002()
        {
            oData.CrearConexion();
            OleDbCommand myCommand = new OleDbCommand();
            myCommand.Connection = oData.oConn;

            try
            {
                myCommand.CommandText = "ALTER TABLE CLIENTE_BANCO ADD BANCO TEXT ";
                myCommand.ExecuteNonQuery();
            }
            catch
            {
            }
            try
            {
                myCommand.CommandText = "ALTER TABLE FACTURA ADD BANCO TEXT ";
                myCommand.ExecuteNonQuery();
            }
            catch
            {
            }
            myCommand.CommandText = "UPDATE APLICACION_VERSION SET NO_VERSION='" + Application.ProductVersion + "'";
            myCommand.ExecuteNonQuery();

            myCommand.Connection.Close();
            oData.DestruirConexion();

            actualizar_version_20003();
        }
        private void actualizar_version_20003()
        {
            oData.CrearConexion();
            OleDbCommand myCommand = new OleDbCommand();
            myCommand.Connection = oData.oConn;

            try
            {
                myCommand.CommandText = "ALTER TABLE FACTURA ADD FORMA_PAGO TEXT ";
                myCommand.ExecuteNonQuery();
            }
            catch
            {
            }

            myCommand.CommandText = "DROP VIEW FE_VISTA";
            myCommand.ExecuteNonQuery();

            try
            {
                string sSQL = "";
                sSQL = "CREATE VIEW FE_VISTA AS  ";
                sSQL += " SELECT FACTURA.ROW_ID, FACTURA.ID AS FACTURA, CLIENTE.EMAIL, CLIENTE.CONTACTO, FACTURA.FECHA_CREACION, FACTURA.CANCELADO, FACTURA.TIPO, SERIE.ID AS SERIE_ID, SERIE.ANIO AS SERIE_ANIO, SERIE.CALLE AS SERIE_CALLE, SERIE.ESTADO AS SERIE_ESTADO, SERIE.MUNICIPIO AS SERIE_MUNICIPIO, SERIE.EXTERIOR AS SERIE_EXTERIOR, SERIE.INTERIOR AS SERIE_INTERIOR, SERIE.COLONIA AS SERIE_COLONIA, SERIE.CP AS SERIE_CP, SERIE.PAIS AS SERIE_PAIS, SERIE.LOCALIDAD AS SERIE_LOCALIDAD, SERIE.REFERENCIA AS SERIE_REFERENCIA, FACTURA.IMPUESTO_DATO, FACTURA.SUBTOTAL, FACTURA.IMPUESTO, FACTURA.RETENCION, FACTURA.TOTAL, FACTURA.SELLO, FACTURA.CADENA, FACTURA_LINEA.CANTIDAD, FACTURA_LINEA.UNIDAD, FACTURA_LINEA.NO_IDENTIFICACION, FACTURA_LINEA.DESCRIPCION, FACTURA_LINEA.VALOR_UNITARIO, FACTURA_LINEA.IMPORTE, CLIENTE.ID AS CLIENTE, CLIENTE.RFC, CLIENTE.CALLE, CLIENTE.EXTERIOR, CLIENTE.INTERIOR, CLIENTE.COLONIA, CLIENTE.CP, CLIENTE.LOCALIDAD, CLIENTE.REFERENCIA, CLIENTE.PAIS, CLIENTE.ESTADO, CLIENTE.MUNICIPIO, FACTURA.OBSERVACIONES, FACTURA.TIPO_CAMBIO, FACTURA.APROBACION, CERTIFICADO.CERTIFICADO, CERTIFICADO.NO_CERTIFICADO, MONEDA.ID AS MONEDA_ID, EMPRESA.ID AS EMPRESA_ID, EMPRESA.RFC AS EMPRESA_RFC, EMPRESA.CALLE AS EMPRESA_CALLE, EMPRESA.EXTERIOR AS EMPRESA_EXTERIOR, EMPRESA.INTERIOR AS EMPRESA_INTERIOR, EMPRESA.COLONIA AS EMPRESA_COLONIA, EMPRESA.CP AS EMPRESA_CP, EMPRESA.LOCALIDAD AS EMPRESA_LOCALIDAD, EMPRESA.REFERENCIA AS EMPRESA_REFERENCIA, EMPRESA.PAIS AS EMPRESA_PAIS, EMPRESA.ESTADO AS EMPRESA_ESTADO, EMPRESA.MUNICIPIO AS EMPRESA_MUNICIPIO, FACTURA.USER_1, FACTURA.USER_2, FACTURA.USER_3, FACTURA.USER_4, FACTURA.USER_5, FACTURA.USER_6, IMPUESTO.ID AS IMPUESTO_LINEA_ID, IMPUESTO.PORCENTAJE AS IMPUESTO_LINEA_PORCENTAJE, IMPUESTO.RETENCION AS IMPUESTO_LINEA_RETENCION, FACTURA_LINEA.IMPUESTO AS IMPUESTO_LINEA, CLIENTE.USER_1 AS CLIENTE_USER1, CLIENTE.USER_2 AS CLIENTE_USER2, CLIENTE.USER_3 AS CLIENTE_USER3, CLIENTE.USER_4 AS CLIENTE_USER4, CLIENTE.USER_5 AS CLIENTE_USER5, CLIENTE.USER_6 AS CLIENTE_USER6, FACTURA.UUID AS UUID, FACTURA.FechaTimbrado AS FechaTimbrado, FACTURA.noCertificadoSAT AS noCertificadoSAT, FACTURA.selloSAT AS selloSAT, FACTURA.Cadena_TFD AS Cadena_TFD, FACTURA.QR_Code AS QR_Code";
                sSQL += " , FACTURA.IMAGEN AS IMAGEN, EMPRESA.REGIMEN_FISCAL as REGIMEN_FISCAL, FACTURA.METODO_PAGO as METODO_PAGO, FACTURA.CUENTA_BANCARIA as CUENTA_BANCARIA, FACTURA.FORMA_PAGO as FORMA_PAGO";
                sSQL += " FROM ((((((FACTURA INNER JOIN FACTURA_LINEA ON FACTURA.ROW_ID = FACTURA_LINEA.ROW_ID_FACTURA) INNER JOIN CLIENTE ON FACTURA.ROW_ID_CLIENTE = CLIENTE.ROW_ID) INNER JOIN SERIE ON FACTURA.ROW_ID_SERIE = SERIE.ROW_ID) INNER JOIN CERTIFICADO ON SERIE.ROW_ID_CERTIFICADO = CERTIFICADO.ROW_ID) INNER JOIN MONEDA ON FACTURA.ROW_ID_MONEDA = MONEDA.ROW_ID) INNER JOIN EMPRESA ON FACTURA.ROW_ID_EMPRESA = EMPRESA.ROW_ID) LEFT JOIN IMPUESTO ON FACTURA_LINEA.ROW_ID_IMPUESTO = IMPUESTO.ROW_ID;";
                myCommand.CommandText = sSQL;
                myCommand.ExecuteNonQuery();
            }
            catch
            {
            }

            myCommand.CommandText = "UPDATE APLICACION_VERSION SET NO_VERSION='" + Application.ProductVersion + "'";
            myCommand.ExecuteNonQuery();

            myCommand.Connection.Close();
            oData.DestruirConexion();
            actualizar_version_20004();
        }

        private void actualizar_version_20004()
        {
            oData.CrearConexion();
            OleDbCommand myCommand = new OleDbCommand();
            myCommand.Connection = oData.oConn;

            myCommand.CommandText = "DROP VIEW FE_VISTA";
            myCommand.ExecuteNonQuery();

            try
            {
                string sSQL = "";
                sSQL = "CREATE VIEW FE_VISTA AS  ";
                sSQL += " SELECT FACTURA.ROW_ID, FACTURA.ID AS FACTURA, CLIENTE.EMAIL, CLIENTE.CONTACTO, FACTURA.FECHA_CREACION, FACTURA.CANCELADO, FACTURA.TIPO, SERIE.ID AS SERIE_ID, SERIE.ANIO AS SERIE_ANIO, SERIE.CALLE AS SERIE_CALLE, SERIE.ESTADO AS SERIE_ESTADO, SERIE.MUNICIPIO AS SERIE_MUNICIPIO, SERIE.EXTERIOR AS SERIE_EXTERIOR, SERIE.INTERIOR AS SERIE_INTERIOR, SERIE.COLONIA AS SERIE_COLONIA, SERIE.CP AS SERIE_CP, SERIE.PAIS AS SERIE_PAIS, SERIE.LOCALIDAD AS SERIE_LOCALIDAD, SERIE.REFERENCIA AS SERIE_REFERENCIA, FACTURA.IMPUESTO_DATO, FACTURA.SUBTOTAL, FACTURA.IMPUESTO, FACTURA.RETENCION, FACTURA.TOTAL, FACTURA.SELLO, FACTURA.CADENA, FACTURA_LINEA.CANTIDAD, FACTURA_LINEA.UNIDAD, FACTURA_LINEA.NO_IDENTIFICACION, FACTURA_LINEA.DESCRIPCION, FACTURA_LINEA.VALOR_UNITARIO, FACTURA_LINEA.IMPORTE, CLIENTE.ID AS CLIENTE, CLIENTE.RFC, CLIENTE.CALLE, CLIENTE.EXTERIOR, CLIENTE.INTERIOR, CLIENTE.COLONIA, CLIENTE.CP, CLIENTE.LOCALIDAD, CLIENTE.REFERENCIA, CLIENTE.PAIS, CLIENTE.ESTADO, CLIENTE.MUNICIPIO, FACTURA.OBSERVACIONES, FACTURA.TIPO_CAMBIO, FACTURA.APROBACION, CERTIFICADO.CERTIFICADO, CERTIFICADO.NO_CERTIFICADO, MONEDA.ID AS MONEDA_ID, EMPRESA.ID AS EMPRESA_ID, EMPRESA.RFC AS EMPRESA_RFC, EMPRESA.CALLE AS EMPRESA_CALLE, EMPRESA.EXTERIOR AS EMPRESA_EXTERIOR, EMPRESA.INTERIOR AS EMPRESA_INTERIOR, EMPRESA.COLONIA AS EMPRESA_COLONIA, EMPRESA.CP AS EMPRESA_CP, EMPRESA.LOCALIDAD AS EMPRESA_LOCALIDAD, EMPRESA.REFERENCIA AS EMPRESA_REFERENCIA, EMPRESA.PAIS AS EMPRESA_PAIS, EMPRESA.ESTADO AS EMPRESA_ESTADO, EMPRESA.MUNICIPIO AS EMPRESA_MUNICIPIO, FACTURA.USER_1, FACTURA.USER_2, FACTURA.USER_3, FACTURA.USER_4, FACTURA.USER_5, FACTURA.USER_6, IMPUESTO.ID AS IMPUESTO_LINEA_ID, IMPUESTO.PORCENTAJE AS IMPUESTO_LINEA_PORCENTAJE, IMPUESTO.RETENCION AS IMPUESTO_LINEA_RETENCION, FACTURA_LINEA.IMPUESTO AS IMPUESTO_LINEA, CLIENTE.USER_1 AS CLIENTE_USER1, CLIENTE.USER_2 AS CLIENTE_USER2, CLIENTE.USER_3 AS CLIENTE_USER3, CLIENTE.USER_4 AS CLIENTE_USER4, CLIENTE.USER_5 AS CLIENTE_USER5, CLIENTE.USER_6 AS CLIENTE_USER6, FACTURA.UUID AS UUID, FACTURA.FechaTimbrado AS FechaTimbrado, FACTURA.noCertificadoSAT AS noCertificadoSAT, FACTURA.selloSAT AS selloSAT, FACTURA.Cadena_TFD AS Cadena_TFD, FACTURA.QR_Code AS QR_Code";
                sSQL += " , FACTURA.IMAGEN AS IMAGEN, EMPRESA.REGIMEN_FISCAL as REGIMEN_FISCAL, FACTURA.METODO_PAGO as METODO_PAGO, FACTURA.CUENTA_BANCARIA as CUENTA_BANCARIA, FACTURA.FORMA_PAGO as FORMA_PAGO, FACTURA.BANCO as BANCO";
                sSQL += " FROM ((((((FACTURA INNER JOIN FACTURA_LINEA ON FACTURA.ROW_ID = FACTURA_LINEA.ROW_ID_FACTURA) INNER JOIN CLIENTE ON FACTURA.ROW_ID_CLIENTE = CLIENTE.ROW_ID) INNER JOIN SERIE ON FACTURA.ROW_ID_SERIE = SERIE.ROW_ID) INNER JOIN CERTIFICADO ON SERIE.ROW_ID_CERTIFICADO = CERTIFICADO.ROW_ID) INNER JOIN MONEDA ON FACTURA.ROW_ID_MONEDA = MONEDA.ROW_ID) INNER JOIN EMPRESA ON FACTURA.ROW_ID_EMPRESA = EMPRESA.ROW_ID) LEFT JOIN IMPUESTO ON FACTURA_LINEA.ROW_ID_IMPUESTO = IMPUESTO.ROW_ID;";
                myCommand.CommandText = sSQL;
                myCommand.ExecuteNonQuery();
            }
            catch
            {
            }

            try
            {
                string sSQL = "";
                sSQL = "CREATE TABLE UM  ";
                sSQL += " (ROW_ID AUTOINCREMENT, [ID] TEXT, [DEFECTO] TEXT, [ACTIVO]  TEXT) ";
                myCommand.CommandText = sSQL;
                myCommand.ExecuteNonQuery();
            }
            catch
            {
            }




            myCommand.CommandText = "UPDATE APLICACION_VERSION SET NO_VERSION='" + Application.ProductVersion + "'";
            myCommand.ExecuteNonQuery();

            myCommand.Connection.Close();
            oData.DestruirConexion();
            actualizar_version_20005();
        }


        private void actualizar_version_20005()
        {
            oData.CrearConexion();
            OleDbCommand myCommand = new OleDbCommand();
            myCommand.Connection = oData.oConn;

            myCommand.CommandText = "DROP VIEW FE_VISTA";
            myCommand.ExecuteNonQuery();

            try
            {
                string sSQL = "";
                sSQL = "CREATE VIEW FE_VISTA AS  ";
                sSQL += " SELECT FACTURA.ROW_ID, FACTURA.ID AS FACTURA, CLIENTE.EMAIL, CLIENTE.CONTACTO, FACTURA.FECHA_CREACION, FACTURA.CANCELADO, FACTURA.TIPO, SERIE.ID AS SERIE_ID, SERIE.ANIO AS SERIE_ANIO, SERIE.CALLE AS SERIE_CALLE, SERIE.ESTADO AS SERIE_ESTADO, SERIE.MUNICIPIO AS SERIE_MUNICIPIO, SERIE.EXTERIOR AS SERIE_EXTERIOR, SERIE.INTERIOR AS SERIE_INTERIOR, SERIE.COLONIA AS SERIE_COLONIA, SERIE.CP AS SERIE_CP, SERIE.PAIS AS SERIE_PAIS, SERIE.LOCALIDAD AS SERIE_LOCALIDAD, SERIE.REFERENCIA AS SERIE_REFERENCIA, FACTURA.IMPUESTO_DATO, FACTURA.SUBTOTAL, FACTURA.IMPUESTO, FACTURA.RETENCION, FACTURA.TOTAL, FACTURA.SELLO, FACTURA.CADENA, FACTURA_LINEA.CANTIDAD, FACTURA_LINEA.UNIDAD, FACTURA_LINEA.NO_IDENTIFICACION, FACTURA_LINEA.DESCRIPCION, FACTURA_LINEA.VALOR_UNITARIO, FACTURA_LINEA.IMPORTE, CLIENTE.ID AS CLIENTE, CLIENTE.RFC, CLIENTE.CALLE, CLIENTE.EXTERIOR, CLIENTE.INTERIOR, CLIENTE.COLONIA, CLIENTE.CP, CLIENTE.LOCALIDAD, CLIENTE.REFERENCIA, CLIENTE.PAIS, CLIENTE.ESTADO, CLIENTE.MUNICIPIO, FACTURA.OBSERVACIONES, FACTURA.TIPO_CAMBIO, FACTURA.APROBACION, CERTIFICADO.CERTIFICADO, CERTIFICADO.NO_CERTIFICADO, MONEDA.ID AS MONEDA_ID, EMPRESA.ID AS EMPRESA_ID, EMPRESA.RFC AS EMPRESA_RFC, EMPRESA.CALLE AS EMPRESA_CALLE, EMPRESA.EXTERIOR AS EMPRESA_EXTERIOR, EMPRESA.INTERIOR AS EMPRESA_INTERIOR, EMPRESA.COLONIA AS EMPRESA_COLONIA, EMPRESA.CP AS EMPRESA_CP, EMPRESA.LOCALIDAD AS EMPRESA_LOCALIDAD, EMPRESA.REFERENCIA AS EMPRESA_REFERENCIA, EMPRESA.PAIS AS EMPRESA_PAIS, EMPRESA.ESTADO AS EMPRESA_ESTADO, EMPRESA.MUNICIPIO AS EMPRESA_MUNICIPIO, FACTURA.USER_1, FACTURA.USER_2, FACTURA.USER_3, FACTURA.USER_4, FACTURA.USER_5, FACTURA.USER_6, IMPUESTO.ID AS IMPUESTO_LINEA_ID, IMPUESTO.PORCENTAJE AS IMPUESTO_LINEA_PORCENTAJE, IMPUESTO.RETENCION AS IMPUESTO_LINEA_RETENCION, FACTURA_LINEA.IMPUESTO AS IMPUESTO_LINEA, CLIENTE.USER_1 AS CLIENTE_USER1, CLIENTE.USER_2 AS CLIENTE_USER2, CLIENTE.USER_3 AS CLIENTE_USER3, CLIENTE.USER_4 AS CLIENTE_USER4, CLIENTE.USER_5 AS CLIENTE_USER5, CLIENTE.USER_6 AS CLIENTE_USER6, FACTURA.UUID AS UUID, FACTURA.FechaTimbrado AS FechaTimbrado, FACTURA.noCertificadoSAT AS noCertificadoSAT, FACTURA.selloSAT AS selloSAT, FACTURA.Cadena_TFD AS Cadena_TFD, FACTURA.QR_Code AS QR_Code";
                sSQL += " , FACTURA.IMAGEN AS IMAGEN, EMPRESA.REGIMEN_FISCAL as REGIMEN_FISCAL, FACTURA.METODO_PAGO as METODO_PAGO, FACTURA.CUENTA_BANCARIA as CUENTA_BANCARIA, FACTURA.FORMA_PAGO as FORMA_PAGO, FACTURA.BANCO as BANCO, FACTURA_LINEA.ROW_ID as ROW_ID_LINEA";
                sSQL += " FROM ((((((FACTURA INNER JOIN FACTURA_LINEA ON FACTURA.ROW_ID = FACTURA_LINEA.ROW_ID_FACTURA) INNER JOIN CLIENTE ON FACTURA.ROW_ID_CLIENTE = CLIENTE.ROW_ID) INNER JOIN SERIE ON FACTURA.ROW_ID_SERIE = SERIE.ROW_ID) INNER JOIN CERTIFICADO ON SERIE.ROW_ID_CERTIFICADO = CERTIFICADO.ROW_ID) INNER JOIN MONEDA ON FACTURA.ROW_ID_MONEDA = MONEDA.ROW_ID) INNER JOIN EMPRESA ON FACTURA.ROW_ID_EMPRESA = EMPRESA.ROW_ID) LEFT JOIN IMPUESTO ON FACTURA_LINEA.ROW_ID_IMPUESTO = IMPUESTO.ROW_ID;";
                myCommand.CommandText = sSQL;
                myCommand.ExecuteNonQuery();
            }
            catch
            {
            }

            myCommand.CommandText = "UPDATE APLICACION_VERSION SET NO_VERSION='" + Application.ProductVersion + "'";
            myCommand.ExecuteNonQuery();

            myCommand.Connection.Close();
            oData.DestruirConexion();

            actualizar_version_20006();
        }

        private void actualizar_version_20006()
        {
            oData.CrearConexion();
            OleDbCommand myCommand = new OleDbCommand();
            myCommand.Connection = oData.oConn;

            try
            {
                myCommand.CommandText = "ALTER TABLE CLIENTE_BANCO ADD ACTIVO TEXT ";
                myCommand.ExecuteNonQuery();
            }
            catch
            {
            }

            try
            {
                myCommand.CommandText = "ALTER TABLE CLIENTE_BANCO ADD DEFECTO TEXT ";
                myCommand.ExecuteNonQuery();
            }
            catch
            {
            }

            myCommand.CommandText = "UPDATE APLICACION_VERSION SET NO_VERSION='" + Application.ProductVersion + "'";
            myCommand.ExecuteNonQuery();

            myCommand.Connection.Close();
            oData.DestruirConexion();

            actualizar_version_20007();
        }

        private void actualizar_version_20007()
        {
            oData.CrearConexion();
            OleDbCommand myCommand = new OleDbCommand();
            myCommand.Connection = oData.oConn;

            try
            {
                myCommand.CommandText = "UPDATE CLIENTE_BANCO SET ACTIVO='True' WHERE (ACTIVO IS NULL) OR (ACTIVO='') ";
                myCommand.ExecuteNonQuery();
            }
            catch
            {
            }

            try
            {
                string sSQL = "";
                sSQL = "CREATE VIEW FE_VISTA AS  ";
                sSQL += " SELECT FACTURA.ROW_ID, FACTURA.ID AS FACTURA, CLIENTE.EMAIL, CLIENTE.CONTACTO, FACTURA.FECHA_CREACION, FACTURA.CANCELADO, FACTURA.TIPO, SERIE.ID AS SERIE_ID, SERIE.ANIO AS SERIE_ANIO, SERIE.CALLE AS SERIE_CALLE, SERIE.ESTADO AS SERIE_ESTADO, SERIE.MUNICIPIO AS SERIE_MUNICIPIO, SERIE.EXTERIOR AS SERIE_EXTERIOR, SERIE.INTERIOR AS SERIE_INTERIOR, SERIE.COLONIA AS SERIE_COLONIA, SERIE.CP AS SERIE_CP, SERIE.PAIS AS SERIE_PAIS, SERIE.LOCALIDAD AS SERIE_LOCALIDAD, SERIE.REFERENCIA AS SERIE_REFERENCIA, FACTURA.IMPUESTO_DATO, FACTURA.SUBTOTAL, FACTURA.IMPUESTO, FACTURA.RETENCION, FACTURA.TOTAL, FACTURA.SELLO, FACTURA.CADENA, FACTURA_LINEA.CANTIDAD, FACTURA_LINEA.UNIDAD, FACTURA_LINEA.NO_IDENTIFICACION, FACTURA_LINEA.DESCRIPCION, FACTURA_LINEA.VALOR_UNITARIO, FACTURA_LINEA.IMPORTE, CLIENTE.ID AS CLIENTE, CLIENTE.RFC, CLIENTE.CALLE, CLIENTE.EXTERIOR, CLIENTE.INTERIOR, CLIENTE.COLONIA, CLIENTE.CP, CLIENTE.LOCALIDAD, CLIENTE.REFERENCIA, CLIENTE.PAIS, CLIENTE.ESTADO, CLIENTE.MUNICIPIO, FACTURA.OBSERVACIONES, FACTURA.TIPO_CAMBIO, FACTURA.APROBACION, CERTIFICADO.CERTIFICADO, CERTIFICADO.NO_CERTIFICADO, MONEDA.ID AS MONEDA_ID, EMPRESA.ID AS EMPRESA_ID, EMPRESA.RFC AS EMPRESA_RFC, EMPRESA.CALLE AS EMPRESA_CALLE, EMPRESA.EXTERIOR AS EMPRESA_EXTERIOR, EMPRESA.INTERIOR AS EMPRESA_INTERIOR, EMPRESA.COLONIA AS EMPRESA_COLONIA, EMPRESA.CP AS EMPRESA_CP, EMPRESA.LOCALIDAD AS EMPRESA_LOCALIDAD, EMPRESA.REFERENCIA AS EMPRESA_REFERENCIA, EMPRESA.PAIS AS EMPRESA_PAIS, EMPRESA.ESTADO AS EMPRESA_ESTADO, EMPRESA.MUNICIPIO AS EMPRESA_MUNICIPIO, FACTURA.USER_1, FACTURA.USER_2, FACTURA.USER_3, FACTURA.USER_4, FACTURA.USER_5, FACTURA.USER_6, IMPUESTO.ID AS IMPUESTO_LINEA_ID, IMPUESTO.PORCENTAJE AS IMPUESTO_LINEA_PORCENTAJE, IMPUESTO.RETENCION AS IMPUESTO_LINEA_RETENCION, FACTURA_LINEA.IMPUESTO AS IMPUESTO_LINEA, CLIENTE.USER_1 AS CLIENTE_USER1, CLIENTE.USER_2 AS CLIENTE_USER2, CLIENTE.USER_3 AS CLIENTE_USER3, CLIENTE.USER_4 AS CLIENTE_USER4, CLIENTE.USER_5 AS CLIENTE_USER5, CLIENTE.USER_6 AS CLIENTE_USER6, FACTURA.UUID AS UUID, FACTURA.FechaTimbrado AS FechaTimbrado, FACTURA.noCertificadoSAT AS noCertificadoSAT, FACTURA.selloSAT AS selloSAT, FACTURA.Cadena_TFD AS Cadena_TFD, FACTURA.QR_Code AS QR_Code";
                sSQL += " , FACTURA.IMAGEN AS IMAGEN, EMPRESA.REGIMEN_FISCAL as REGIMEN_FISCAL, FACTURA.METODO_PAGO as METODO_PAGO, FACTURA.CUENTA_BANCARIA as CUENTA_BANCARIA, FACTURA.FORMA_PAGO as FORMA_PAGO, FACTURA.BANCO as BANCO";
                sSQL += " FROM ((((((FACTURA INNER JOIN FACTURA_LINEA ON FACTURA.ROW_ID = FACTURA_LINEA.ROW_ID_FACTURA) INNER JOIN CLIENTE ON FACTURA.ROW_ID_CLIENTE = CLIENTE.ROW_ID) INNER JOIN SERIE ON FACTURA.ROW_ID_SERIE = SERIE.ROW_ID) INNER JOIN CERTIFICADO ON SERIE.ROW_ID_CERTIFICADO = CERTIFICADO.ROW_ID) INNER JOIN MONEDA ON FACTURA.ROW_ID_MONEDA = MONEDA.ROW_ID) INNER JOIN EMPRESA ON FACTURA.ROW_ID_EMPRESA = EMPRESA.ROW_ID) LEFT JOIN IMPUESTO ON FACTURA_LINEA.ROW_ID_IMPUESTO = IMPUESTO.ROW_ID";
                sSQL += " ORDER BY FACTURA.ROW_ID, FACTURA_LINEA.ROW_ID;";
                myCommand.CommandText = sSQL;
                myCommand.ExecuteNonQuery();
            }
            catch
            {
            }

            myCommand.CommandText = "UPDATE APLICACION_VERSION SET NO_VERSION='" + Application.ProductVersion + "'";
            myCommand.ExecuteNonQuery();

            myCommand.Connection.Close();
            oData.DestruirConexion();
        }

        private void actualizar_version_3_2_0_7()
        {
            oData.CrearConexion();
            OleDbCommand myCommand = new OleDbCommand();
            myCommand.Connection = oData.oConn;

            try
            {
                myCommand.CommandText = "ALTER TABLE SERIE ADD ENVIO_AUTOMATICO_MAIL TEXT ";
                myCommand.ExecuteNonQuery();
            }
            catch
            {
            }

            try
            {
                myCommand.CommandText = "UPDATE SERIE SET ENVIO_AUTOMATICO_MAIL='True' ";
                myCommand.ExecuteNonQuery();
            }
            catch
            {
            }

            myCommand.CommandText = "UPDATE APLICACION_VERSION SET NO_VERSION='" + Application.ProductVersion + "'";
            myCommand.ExecuteNonQuery();

            myCommand.Connection.Close();
            oData.DestruirConexion();
            actualizar_version_3_2_0_8();
        }

        private void actualizar_version_3_2_0_8()
        {
            oData.CrearConexion();
            OleDbCommand myCommand = new OleDbCommand();
            myCommand.Connection = oData.oConn;

            try
            {
                myCommand.CommandText = "ALTER TABLE EMPRESA ADD CUSTOMER_ID TEXT ";
                myCommand.ExecuteNonQuery();
            }
            catch
            {
            }

            myCommand.CommandText = "UPDATE APLICACION_VERSION SET NO_VERSION='" + Application.ProductVersion + "'";
            myCommand.ExecuteNonQuery();

            myCommand.Connection.Close();
            oData.DestruirConexion();
            actualizar_version_3_2_0_9();
        }

        private void actualizar_version_3_2_0_9()
        {
            oData.CrearConexion();
            OleDbCommand myCommand = new OleDbCommand();
            myCommand.Connection = oData.oConn;
            try
            {
                myCommand.CommandText = "ALTER TABLE EMPRESA ADD CUSTOMER_ID TEXT ";
                myCommand.ExecuteNonQuery();
            }
            catch
            {
            }

            try
            {
                myCommand.CommandText = "ALTER TABLE EMPRESA ADD ENVIO_CORREO TEXT ";
                myCommand.ExecuteNonQuery();
            }
            catch
            {
            }

            try
            {
                myCommand.CommandText = "UPDATE EMPRESA SET ENVIO_CORREO='False' ";
                myCommand.ExecuteNonQuery();
            }
            catch
            {
            }

            try
            {
                myCommand.CommandText = "ALTER TABLE SERIE ADD ENVIO_AUTOMATICO_MAIL TEXT ";
                myCommand.ExecuteNonQuery();
            }
            catch
            {
            }


            myCommand.CommandText = "UPDATE APLICACION_VERSION SET NO_VERSION='" + Application.ProductVersion + "'";
            myCommand.ExecuteNonQuery();

            myCommand.Connection.Close();
            oData.DestruirConexion();
            actualizar_version_3_2_0_10();
        }

        private void actualizar_version_3_2_0_10()
        {
            oData.CrearConexion();
            OleDbCommand myCommand = new OleDbCommand();
            myCommand.Connection = oData.oConn;

            try
            {
                string sSQL = "";
                sSQL += "CREATE TABLE PEDIMENTO_VISUAL  ";
                sSQL += " (ROW_ID AUTOINCREMENT, [INVOICE_ID] TEXT, [LINE_NO] NUMBER, [PEDIMENTO]  TEXT, [ADUANA]  TEXT, [FECHA]  TEXT)";
                myCommand.CommandText = sSQL;
                myCommand.ExecuteNonQuery();
            }
            catch
            {
            }

            myCommand.CommandText = "UPDATE APLICACION_VERSION SET NO_VERSION='" + Application.ProductVersion + "'";
            myCommand.ExecuteNonQuery();

            myCommand.Connection.Close();
            oData.DestruirConexion();
            actualizar_version_3_2_0_11();
        }

        private void actualizar_version_3_2_0_11()
        {
            oData.CrearConexion();
            OleDbCommand myCommand = new OleDbCommand();
            myCommand.Connection = oData.oConn;

            try
            {
                string sSQL = "";
                sSQL += "CREATE TABLE ADDENDA  ";
                sSQL += " (ROW_ID AUTOINCREMENT, [ID] TEXT, [EJECUTABLE]  TEXT, [CUSTOMER_ID]  TEXT, [TIPO]  TEXT, [ACTIVO]  TEXT)";
                myCommand.CommandText = sSQL;
                myCommand.ExecuteNonQuery();
            }
            catch
            {
            }

            myCommand.CommandText = "UPDATE APLICACION_VERSION SET NO_VERSION='" + Application.ProductVersion + "'";
            myCommand.ExecuteNonQuery();

            myCommand.Connection.Close();
            oData.DestruirConexion();
            actualizar_version_3_2_0_13();
        }

        private void actualizar_version_3_2_0_13()
        {
            oData.CrearConexion();
            OleDbCommand myCommand = new OleDbCommand();
            myCommand.Connection = oData.oConn;

            try
            {
                string sSQL = "";
                myCommand.CommandText = "ALTER TABLE FORMATO ADD ROW_ID_EMPRESA NUMBER ";
                myCommand.ExecuteNonQuery();
            }
            catch
            {
            }

            try
            {
                string sSQL = "";
                cEMPRESA ocEMPRESA = new cEMPRESA(oData);
                List<cEMPRESA> ocEMPRESA_Lista = ocEMPRESA.todos("");
                if (ocEMPRESA_Lista.Count > 0)
                {
                    myCommand.CommandText = "UPDATE FORMATO SET ROW_ID_EMPRESA=" + ocEMPRESA_Lista[0].ROW_ID;
                    myCommand.ExecuteNonQuery();
                }
            }
            catch
            {
            }

            myCommand.CommandText = "UPDATE APLICACION_VERSION SET NO_VERSION='" + Application.ProductVersion + "'";
            myCommand.ExecuteNonQuery();

            myCommand.Connection.Close();
            oData.DestruirConexion();

            actualizar_version_3_2_0_14();
        }

        private void actualizar_version_3_2_0_14()
        {
            oData.CrearConexion();
            OleDbCommand myCommand = new OleDbCommand();
            myCommand.Connection = oData.oConn;

            try
            {
                string sSQL = "";
                myCommand.CommandText = "ALTER TABLE USUARIO ADD CANCELAR TEXT ";
                myCommand.ExecuteNonQuery();
            }
            catch
            {
            }

            try
            {
                string sSQL = "";
                myCommand.CommandText = "ALTER TABLE USUARIO ADD VERIFICAR TEXT ";
                myCommand.ExecuteNonQuery();
            }
            catch
            {
            }

            try
            {
                string sSQL = "";
                myCommand.CommandText = "ALTER TABLE USUARIO ADD CAMBIAR TEXT ";
                myCommand.ExecuteNonQuery();
            }
            catch
            {
            }

            try
            {
                string sSQL = "";
                myCommand.CommandText = "ALTER TABLE USUARIO ADD PEDIMENTOS TEXT ";
                myCommand.ExecuteNonQuery();
            }
            catch
            {
            }

            try
            {
                string sSQL = "";
                myCommand.CommandText = "ALTER TABLE USUARIO ADD EXPORTAR TEXT ";
                myCommand.ExecuteNonQuery();
            }
            catch
            {
            }

            try
            {
                string sSQL = "";
                myCommand.CommandText = "ALTER TABLE USUARIO ADD IMPORTAR TEXT ";
                myCommand.ExecuteNonQuery();
            }
            catch
            {
            }

            try
            {
                string sSQL = "";
                myCommand.CommandText = "ALTER TABLE USUARIO ADD ADDENDA TEXT ";
                myCommand.ExecuteNonQuery();
            }
            catch
            {
            }

            try
            {
                string sSQL = "";
                myCommand.CommandText = "ALTER TABLE USUARIO ADD ADDENDA_INDIVIDUAL TEXT ";
                myCommand.ExecuteNonQuery();
            }
            catch
            {
            }

            myCommand.CommandText = "UPDATE APLICACION_VERSION SET NO_VERSION='" + Application.ProductVersion + "'";
            myCommand.ExecuteNonQuery();

            myCommand.Connection.Close();
            oData.DestruirConexion();
            actualizar_version_3_2_0_15();
        }

        private void actualizar_envio_mail()
        {
            oData.CrearConexion();
            OleDbCommand myCommand = new OleDbCommand();
            myCommand.Connection = oData.oConn;

            try
            {
                string sSQL = "";
                myCommand.CommandText = "UPDATE SERIE SET ENVIO_AUTOMATICO_MAIL='True' WHERE ENVIO_AUTOMATICO_MAIL IS NULL ";
                myCommand.ExecuteNonQuery();
            }
            catch
            {
            }

            myCommand.CommandText = "UPDATE APLICACION_VERSION SET NO_VERSION='" + Application.ProductVersion + "'";
            myCommand.ExecuteNonQuery();

            myCommand.Connection.Close();
            oData.DestruirConexion();
        }

        private void actualizar_version_3_2_0_15()
        {
            oData.CrearConexion();
            OleDbCommand myCommand = new OleDbCommand();
            myCommand.Connection = oData.oConn;

            try
            {
                string sSQL = "";
                myCommand.CommandText = "UPDATE SERIE ENVIO_AUTOMATICO_MAIL='True' WHERE ENVIO_AUTOMATICO_MAIL='' ";
                myCommand.ExecuteNonQuery();
            }
            catch
            {
            }

            myCommand.CommandText = "UPDATE APLICACION_VERSION SET NO_VERSION='" + Application.ProductVersion + "'";
            myCommand.ExecuteNonQuery();

            myCommand.Connection.Close();
            oData.DestruirConexion();
            actualizar_version_3_2_0_16();
        }

        private void actualizar_version_3_2_0_16()
        {
            oData.CrearConexion();
            OleDbCommand myCommand = new OleDbCommand();
            myCommand.Connection = oData.oConn;

            try
            {
                string sSQL = "";
                myCommand.CommandText = "ALTER TABLE SERIE ADD PARA TEXT ";
                myCommand.ExecuteNonQuery();
            }
            catch
            {
            }

            myCommand.CommandText = "UPDATE APLICACION_VERSION SET NO_VERSION='" + Application.ProductVersion + "'";
            myCommand.ExecuteNonQuery();

            myCommand.Connection.Close();
            oData.DestruirConexion();
            actualizar_version_3_2_0_20();
        }

        private void actualizar_version_3_2_0_20()
        {
            oData.CrearConexion();
            OleDbCommand myCommand = new OleDbCommand();
            myCommand.Connection = oData.oConn;

            try
            {
                string sSQL = "";
                sSQL += "CREATE TABLE CLIENTE_CONFIGURACION  ";
                sSQL += " (ROW_ID AUTOINCREMENT, [CUSTOMER_ID] TEXT, [ASUNTO]  TEXT)";
                myCommand.CommandText = sSQL;
                myCommand.ExecuteNonQuery();
                myCommand.ExecuteNonQuery();
            }
            catch
            {
            }

            myCommand.CommandText = "UPDATE APLICACION_VERSION SET NO_VERSION='" + Application.ProductVersion + "'";
            myCommand.ExecuteNonQuery();

            myCommand.Connection.Close();
            oData.DestruirConexion();
        }

        private void actualizar_version_20000()
        {
            oData.CrearConexion();
            OleDbCommand myCommand = new OleDbCommand();
            myCommand.Connection = oData.oConn;
            try
            {
                string sSQL = "";
                sSQL += "CREATE TABLE CFDI_USUARIO  ";
                sSQL += " (ROW_ID AUTOINCREMENT, [USUARIO] TEXT, [PASSWORD] TEXT, [TIPO]  TEXT) ";
                myCommand.CommandText = sSQL;
                myCommand.ExecuteNonQuery();
            }
            catch
            {
            }

            try
            {
                myCommand.CommandText = "ALTER TABLE CFDI_USUARIO ADD COLUMN ROW_ID_EMPRESA NUMBER ";
                myCommand.ExecuteNonQuery();
            }
            catch
            {
            }


            try
            {
                myCommand.CommandText = "ALTER TABLE FACTURA ADD COLUMN UUID TEXT ";
                myCommand.ExecuteNonQuery();
            }
            catch
            {
            }

            try
            {
                myCommand.CommandText = "ALTER TABLE FACTURA ADD COLUMN FechaTimbrado TEXT ";
                myCommand.ExecuteNonQuery();
            }
            catch
            {
            }

            try
            {
                myCommand.CommandText = "ALTER TABLE FACTURA ADD COLUMN noCertificadoSAT TEXT ";
                myCommand.ExecuteNonQuery();
            }
            catch
            {
            }
            try
            {
                myCommand.CommandText = "ALTER TABLE FACTURA ADD COLUMN selloSAT TEXT ";
                myCommand.ExecuteNonQuery();
            }
            catch
            {
            }

            try
            {
                myCommand.CommandText = "ALTER TABLE FACTURA ADD COLUMN Cadena_TFD TEXT ";
                myCommand.ExecuteNonQuery();
            }
            catch
            {
            }

            try
            {
                myCommand.CommandText = "ALTER TABLE FACTURA ADD COLUMN QR_Code TEXT ";
                myCommand.ExecuteNonQuery();
            }
            catch
            {
            }


            try
            {
                myCommand.CommandText = "ALTER TABLE FACTURA ADD COLUMN IMAGEN IMAGE ";
                myCommand.ExecuteNonQuery();
            }
            catch
            {
            }
            try
            {
                myCommand.CommandText = "ALTER TABLE FACTURA ADD COLUMN METODO_PAGO TEXT ";
                myCommand.ExecuteNonQuery();
            }
            catch
            {
            }
            try
            {
                myCommand.CommandText = "ALTER TABLE FACTURA ADD COLUMN CUENTA_BANCARIA TEXT ";
                myCommand.ExecuteNonQuery();
            }
            catch
            {
            }

            try
            {
                myCommand.CommandText = "ALTER TABLE EMPRESA ADD COLUMN REGIMEN_FISCAL TEXT ";
                myCommand.ExecuteNonQuery();
            }
            catch
            {
            }

            try
            {
                myCommand.CommandText = "ALTER TABLE CFDI_USUARIO ADD COLUMN ROW_ID_EMPRESA NUMBER ";
                myCommand.ExecuteNonQuery();
            }
            catch
            {
            }

            try
            {
                myCommand.CommandText = "ALTER TABLE FACTURA ADD COLUMN ESTADO_PAGO TEXT ";
                myCommand.ExecuteNonQuery();
            }
            catch
            {
            }

            try
            {
                myCommand.CommandText = "ALTER TABLE FACTURA ADD COLUMN DEPOSITO TEXT ";
                myCommand.ExecuteNonQuery();
            }
            catch
            {
            }

            try
            {
                myCommand.CommandText = "ALTER TABLE FACTURA ADD COLUMN FECHA_PAGO TEXT ";
                myCommand.ExecuteNonQuery();
            }
            catch
            {
            }

            try
            {
                string sSQL = "";
                sSQL += "CREATE TABLE CLIENTE_BANCO  ";
                sSQL += " (ROW_ID AUTOINCREMENT, ROW_ID_CLIENTE NUMBER, METODO_PAGO TEXT, CUENTA_BANCARIA  TEXT) ";
                myCommand.CommandText = sSQL;
                myCommand.ExecuteNonQuery();
            }
            catch
            {
            }


            myCommand.CommandText = "DROP VIEW FE_VISTA";
            myCommand.ExecuteNonQuery();

            try
            {
                string sSQL = "";
                sSQL = "CREATE VIEW FE_VISTA AS  ";
                sSQL += " SELECT FACTURA.ROW_ID, FACTURA.ID AS FACTURA, CLIENTE.EMAIL, CLIENTE.CONTACTO, FACTURA.FECHA_CREACION, FACTURA.CANCELADO, FACTURA.TIPO, SERIE.ID AS SERIE_ID, SERIE.ANIO AS SERIE_ANIO, SERIE.CALLE AS SERIE_CALLE, SERIE.ESTADO AS SERIE_ESTADO, SERIE.MUNICIPIO AS SERIE_MUNICIPIO, SERIE.EXTERIOR AS SERIE_EXTERIOR, SERIE.INTERIOR AS SERIE_INTERIOR, SERIE.COLONIA AS SERIE_COLONIA, SERIE.CP AS SERIE_CP, SERIE.PAIS AS SERIE_PAIS, SERIE.LOCALIDAD AS SERIE_LOCALIDAD, SERIE.REFERENCIA AS SERIE_REFERENCIA, FACTURA.IMPUESTO_DATO, FACTURA.SUBTOTAL, FACTURA.IMPUESTO, FACTURA.RETENCION, FACTURA.TOTAL, FACTURA.SELLO, FACTURA.CADENA, FACTURA_LINEA.CANTIDAD, FACTURA_LINEA.UNIDAD, FACTURA_LINEA.NO_IDENTIFICACION, FACTURA_LINEA.DESCRIPCION, FACTURA_LINEA.VALOR_UNITARIO, FACTURA_LINEA.IMPORTE, CLIENTE.ID AS CLIENTE, CLIENTE.RFC, CLIENTE.CALLE, CLIENTE.EXTERIOR, CLIENTE.INTERIOR, CLIENTE.COLONIA, CLIENTE.CP, CLIENTE.LOCALIDAD, CLIENTE.REFERENCIA, CLIENTE.PAIS, CLIENTE.ESTADO, CLIENTE.MUNICIPIO, FACTURA.OBSERVACIONES, FACTURA.TIPO_CAMBIO, FACTURA.APROBACION, CERTIFICADO.CERTIFICADO, CERTIFICADO.NO_CERTIFICADO, MONEDA.ID AS MONEDA_ID, EMPRESA.ID AS EMPRESA_ID, EMPRESA.RFC AS EMPRESA_RFC, EMPRESA.CALLE AS EMPRESA_CALLE, EMPRESA.EXTERIOR AS EMPRESA_EXTERIOR, EMPRESA.INTERIOR AS EMPRESA_INTERIOR, EMPRESA.COLONIA AS EMPRESA_COLONIA, EMPRESA.CP AS EMPRESA_CP, EMPRESA.LOCALIDAD AS EMPRESA_LOCALIDAD, EMPRESA.REFERENCIA AS EMPRESA_REFERENCIA, EMPRESA.PAIS AS EMPRESA_PAIS, EMPRESA.ESTADO AS EMPRESA_ESTADO, EMPRESA.MUNICIPIO AS EMPRESA_MUNICIPIO, FACTURA.USER_1, FACTURA.USER_2, FACTURA.USER_3, FACTURA.USER_4, FACTURA.USER_5, FACTURA.USER_6, IMPUESTO.ID AS IMPUESTO_LINEA_ID, IMPUESTO.PORCENTAJE AS IMPUESTO_LINEA_PORCENTAJE, IMPUESTO.RETENCION AS IMPUESTO_LINEA_RETENCION, FACTURA_LINEA.IMPUESTO AS IMPUESTO_LINEA, CLIENTE.USER_1 AS CLIENTE_USER1, CLIENTE.USER_2 AS CLIENTE_USER2, CLIENTE.USER_3 AS CLIENTE_USER3, CLIENTE.USER_4 AS CLIENTE_USER4, CLIENTE.USER_5 AS CLIENTE_USER5, CLIENTE.USER_6 AS CLIENTE_USER6, FACTURA.UUID AS UUID, FACTURA.FechaTimbrado AS FechaTimbrado, FACTURA.noCertificadoSAT AS noCertificadoSAT, FACTURA.selloSAT AS selloSAT, FACTURA.Cadena_TFD AS Cadena_TFD, FACTURA.QR_Code AS QR_Code";
                sSQL += " , FACTURA.IMAGEN AS IMAGEN, EMPRESA.REGIMEN_FISCAL as REGIMEN_FISCAL, FACTURA.METODO_PAGO as METODO_PAGO, FACTURA.CUENTA_BANCARIA as CUENTA_BANCARIA";
                sSQL += " FROM ((((((FACTURA INNER JOIN FACTURA_LINEA ON FACTURA.ROW_ID = FACTURA_LINEA.ROW_ID_FACTURA) INNER JOIN CLIENTE ON FACTURA.ROW_ID_CLIENTE = CLIENTE.ROW_ID) INNER JOIN SERIE ON FACTURA.ROW_ID_SERIE = SERIE.ROW_ID) INNER JOIN CERTIFICADO ON SERIE.ROW_ID_CERTIFICADO = CERTIFICADO.ROW_ID) INNER JOIN MONEDA ON FACTURA.ROW_ID_MONEDA = MONEDA.ROW_ID) INNER JOIN EMPRESA ON FACTURA.ROW_ID_EMPRESA = EMPRESA.ROW_ID) LEFT JOIN IMPUESTO ON FACTURA_LINEA.ROW_ID_IMPUESTO = IMPUESTO.ROW_ID;";
                myCommand.CommandText = sSQL;
                myCommand.ExecuteNonQuery();
            }
            catch
            {
            }

            myCommand.CommandText = "UPDATE APLICACION_VERSION SET NO_VERSION='" + Application.ProductVersion + "'";
            myCommand.ExecuteNonQuery();

            myCommand.Connection.Close();
            oData.DestruirConexion();
        }

        private void actualizar_version_1002()
        {
            oData.CrearConexion();
            OleDbCommand myCommand = new OleDbCommand();
            myCommand.Connection = oData.oConn;

            myCommand.CommandText = "ALTER TABLE SERIE ADD COLUMN CERO Text";
            myCommand.ExecuteNonQuery();

            myCommand.CommandText = "UPDATE SERIE SET CERO='False'";
            myCommand.ExecuteNonQuery();

            myCommand.CommandText = "ALTER TABLE TIPOCAMBIO ALTER COLUMN TC_VENTA Decimal(15,4)";
            myCommand.ExecuteNonQuery();

            myCommand.Connection.Close();
            oData.DestruirConexion();
        }

        private void actualizar_version_1003()
        {
            oData.CrearConexion();
            OleDbCommand myCommand = new OleDbCommand();
            myCommand.Connection = oData.oConn;

            myCommand.CommandText = "DROP VIEW FE_VISTA";
            myCommand.ExecuteNonQuery();

            string sSQL = "";
            sSQL += "CREATE VIEW FE_VISTA AS ";
            //sSQL += "(ROW_ID,FACTURA,FECHA_CREACION,TIPO,IMPUESTO_DATO,SUBTOTAL,IMPUESTO,RETENCION,TOTAL,SELLO,CADENA,CANTIDAD,UNIDAD,NO_IDENTIFICACION,DESCRIPCION,VALOR_UNITARIO,IMPORTE,CLIENTE,RFC,CALLE,EXTERIOR,INTERIOR,COLONIA,CP,LOCALIDAD,REFERENCIA,PAIS,ESTADO,MUNICIPIO,OBSERVACIONES,TIPO_CAMBIO,APROBACION,CERTIFICADO,NO_CERTIFICADO,MONEDA_ID,EMPRESA_ID,EMPRESA_RFC,EMPRESA_CALLE,EMPRESA_EXTERIOR,EMPRESA_INTERIOR,EMPRESA_COLONIA,EMPRESA_CP,EMPRESA_LOCALIDAD,EMPRESA_REFERENCIA,EMPRESA_PAIS,EMPRESA_ESTADO,EMPRESA_MUNICIPIO)";
            sSQL += "  SELECT FACTURA.ROW_ID, FACTURA.ID AS FACTURA, FACTURA.FECHA_CREACION, FACTURA.TIPO, FACTURA.IMPUESTO_DATO, FACTURA.SUBTOTAL";
            sSQL += ", FACTURA.IMPUESTO, FACTURA.RETENCION, FACTURA.TOTAL, FACTURA.SELLO, FACTURA.CADENA, FACTURA_LINEA.CANTIDAD, FACTURA_LINEA.UNIDAD";
            sSQL += ", FACTURA_LINEA.NO_IDENTIFICACION, FACTURA_LINEA.DESCRIPCION, FACTURA_LINEA.VALOR_UNITARIO, FACTURA_LINEA.IMPORTE, CLIENTE.ID AS CLIENTE";
            sSQL += ", CLIENTE.RFC, CLIENTE.CALLE, CLIENTE.EXTERIOR, CLIENTE.INTERIOR, CLIENTE.COLONIA, CLIENTE.CP, CLIENTE.LOCALIDAD, CLIENTE.REFERENCIA";
            sSQL += ", CLIENTE.PAIS, CLIENTE.ESTADO, CLIENTE.MUNICIPIO, FACTURA.OBSERVACIONES, FACTURA.TIPO_CAMBIO, FACTURA.APROBACION, CERTIFICADO.CERTIFICADO";
            sSQL += ", CERTIFICADO.NO_CERTIFICADO, MONEDA.ID AS MONEDA_ID, EMPRESA.ID AS EMPRESA_ID, EMPRESA.RFC AS EMPRESA_RFC, EMPRESA.CALLE AS EMPRESA_CALLE";
            sSQL += ", EMPRESA.EXTERIOR AS EMPRESA_EXTERIOR, EMPRESA.INTERIOR AS EMPRESA_INTERIOR, EMPRESA.COLONIA AS EMPRESA_COLONIA, EMPRESA.CP AS EMPRESA_CP";
            sSQL += ", EMPRESA.LOCALIDAD AS EMPRESA_LOCALIDAD, EMPRESA.REFERENCIA AS EMPRESA_REFERENCIA, EMPRESA.PAIS AS EMPRESA_PAIS, EMPRESA.ESTADO AS EMPRESA_ESTADO";
            sSQL += ", EMPRESA.MUNICIPIO AS EMPRESA_MUNICIPIO ";
            sSQL += "FROM (((((FACTURA INNER JOIN FACTURA_LINEA ON FACTURA.ROW_ID = FACTURA_LINEA.ROW_ID_FACTURA) INNER JOIN CLIENTE ON FACTURA.ROW_ID_CLIENTE = CLIENTE.ROW_ID) ";
            sSQL += "INNER JOIN SERIE ON FACTURA.ROW_ID_SERIE = SERIE.ROW_ID) INNER JOIN CERTIFICADO ON SERIE.ROW_ID_CERTIFICADO = CERTIFICADO.ROW_ID) ";
            sSQL += "INNER JOIN MONEDA ON FACTURA.ROW_ID_MONEDA = MONEDA.ROW_ID) INNER JOIN EMPRESA ON CERTIFICADO.ROW_ID_EMPRESA = EMPRESA.ROW_ID;";
            myCommand.CommandText = sSQL;
            myCommand.ExecuteNonQuery();

            myCommand.CommandText = "UPDATE APLICACION_VERSION SET NO_VERSION='" + Application.ProductVersion + "'";
            myCommand.ExecuteNonQuery();

            myCommand.Connection.Close();
            oData.DestruirConexion();
        }

        private void actualizar_version_1004()
        {
            oData.CrearConexion();
            OleDbCommand myCommand = new OleDbCommand();
            myCommand.Connection = oData.oConn;

            myCommand.CommandText = "DROP VIEW FE_VISTA";
            myCommand.ExecuteNonQuery();

            string sSQL = "";
            sSQL += "CREATE VIEW FE_VISTA AS ";
            sSQL += "  SELECT FACTURA.ROW_ID, FACTURA.ID AS FACTURA, CLIENTE.EMAIL, CLIENTE.CONTACTO, FACTURA.FECHA_CREACION, FACTURA.TIPO, FACTURA.IMPUESTO_DATO, FACTURA.SUBTOTAL";
            sSQL += ", FACTURA.IMPUESTO, FACTURA.RETENCION, FACTURA.TOTAL, FACTURA.SELLO, FACTURA.CADENA, FACTURA_LINEA.CANTIDAD, FACTURA_LINEA.UNIDAD";
            sSQL += ", FACTURA_LINEA.NO_IDENTIFICACION, FACTURA_LINEA.DESCRIPCION, FACTURA_LINEA.VALOR_UNITARIO, FACTURA_LINEA.IMPORTE, CLIENTE.ID AS CLIENTE";
            sSQL += ", CLIENTE.RFC, CLIENTE.CALLE, CLIENTE.EXTERIOR, CLIENTE.INTERIOR, CLIENTE.COLONIA, CLIENTE.CP, CLIENTE.LOCALIDAD, CLIENTE.REFERENCIA";
            sSQL += ", CLIENTE.PAIS, CLIENTE.ESTADO, CLIENTE.MUNICIPIO, FACTURA.OBSERVACIONES, FACTURA.TIPO_CAMBIO, FACTURA.APROBACION, CERTIFICADO.CERTIFICADO";
            sSQL += ", CERTIFICADO.NO_CERTIFICADO, MONEDA.ID AS MONEDA_ID, EMPRESA.ID AS EMPRESA_ID, EMPRESA.RFC AS EMPRESA_RFC, EMPRESA.CALLE AS EMPRESA_CALLE";
            sSQL += ", EMPRESA.EXTERIOR AS EMPRESA_EXTERIOR, EMPRESA.INTERIOR AS EMPRESA_INTERIOR, EMPRESA.COLONIA AS EMPRESA_COLONIA, EMPRESA.CP AS EMPRESA_CP";
            sSQL += ", EMPRESA.LOCALIDAD AS EMPRESA_LOCALIDAD, EMPRESA.REFERENCIA AS EMPRESA_REFERENCIA, EMPRESA.PAIS AS EMPRESA_PAIS, EMPRESA.ESTADO AS EMPRESA_ESTADO";
            sSQL += ", EMPRESA.MUNICIPIO AS EMPRESA_MUNICIPIO ";
            sSQL += "FROM (((((FACTURA INNER JOIN FACTURA_LINEA ON FACTURA.ROW_ID = FACTURA_LINEA.ROW_ID_FACTURA) INNER JOIN CLIENTE ON FACTURA.ROW_ID_CLIENTE = CLIENTE.ROW_ID) ";
            sSQL += "INNER JOIN SERIE ON FACTURA.ROW_ID_SERIE = SERIE.ROW_ID) INNER JOIN CERTIFICADO ON SERIE.ROW_ID_CERTIFICADO = CERTIFICADO.ROW_ID) ";
            sSQL += "INNER JOIN MONEDA ON FACTURA.ROW_ID_MONEDA = MONEDA.ROW_ID) INNER JOIN EMPRESA ON CERTIFICADO.ROW_ID_EMPRESA = EMPRESA.ROW_ID;";
            myCommand.CommandText = sSQL;
            myCommand.ExecuteNonQuery();

            myCommand.CommandText = "UPDATE APLICACION_VERSION SET NO_VERSION='" + Application.ProductVersion + "'";
            myCommand.ExecuteNonQuery();

            myCommand.Connection.Close();
            oData.DestruirConexion();
        }

        private void actualizar_version_1005()
        {
            oData.CrearConexion();
            OleDbCommand myCommand = new OleDbCommand();
            myCommand.Connection = oData.oConn;

            myCommand.CommandText = "UPDATE APLICACION_VERSION SET NO_VERSION='" + Application.ProductVersion + "'";
            myCommand.ExecuteNonQuery();

            myCommand.Connection.Close();
            oData.DestruirConexion();
        }

        private void actualizar_version_1006()
        {
            oData.CrearConexion();
            OleDbCommand myCommand = new OleDbCommand();
            myCommand.Connection = oData.oConn;

            myCommand.CommandText = "UPDATE APLICACION_VERSION SET NO_VERSION='" + Application.ProductVersion + "'";
            myCommand.ExecuteNonQuery();

            myCommand.Connection.Close();
            oData.DestruirConexion();
        }
        private void actualizar_version_1007()
        {
            oData.CrearConexion();
            OleDbCommand myCommand = new OleDbCommand();
            myCommand.Connection = oData.oConn;

            myCommand.CommandText = "UPDATE APLICACION_VERSION SET NO_VERSION='" + Application.ProductVersion + "'";
            myCommand.ExecuteNonQuery();

            myCommand.Connection.Close();
            oData.DestruirConexion();
        }

        private void actualizar_version_1008()
        {
            oData.CrearConexion();
            OleDbCommand myCommand = new OleDbCommand();
            myCommand.Connection = oData.oConn;

            myCommand.CommandText = "DROP VIEW FE_VISTA";
            myCommand.ExecuteNonQuery();

            string sSQL = "";
            sSQL += "CREATE VIEW FE_VISTA AS ";
            sSQL += "  SELECT FACTURA.ROW_ID, FACTURA.ID AS FACTURA, CLIENTE.EMAIL, CLIENTE.CONTACTO, FACTURA.FECHA_CREACION, FACTURA.TIPO, FACTURA.IMPUESTO_DATO, FACTURA.SUBTOTAL, FACTURA.IMPUESTO, FACTURA.RETENCION, FACTURA.TOTAL, FACTURA.SELLO, FACTURA.CADENA, FACTURA_LINEA.CANTIDAD, FACTURA_LINEA.UNIDAD, FACTURA_LINEA.NO_IDENTIFICACION, FACTURA_LINEA.DESCRIPCION, FACTURA_LINEA.VALOR_UNITARIO, FACTURA_LINEA.IMPORTE, CLIENTE.ID AS CLIENTE, CLIENTE.RFC, CLIENTE.CALLE, CLIENTE.EXTERIOR, CLIENTE.INTERIOR, CLIENTE.COLONIA, CLIENTE.CP, CLIENTE.LOCALIDAD, CLIENTE.REFERENCIA, CLIENTE.PAIS, CLIENTE.ESTADO, CLIENTE.MUNICIPIO, FACTURA.OBSERVACIONES, FACTURA.TIPO_CAMBIO, FACTURA.APROBACION, CERTIFICADO.CERTIFICADO, CERTIFICADO.NO_CERTIFICADO, MONEDA.ID AS MONEDA_ID, EMPRESA.ID AS EMPRESA_ID, EMPRESA.RFC AS EMPRESA_RFC, EMPRESA.CALLE AS EMPRESA_CALLE, EMPRESA.EXTERIOR AS EMPRESA_EXTERIOR, EMPRESA.INTERIOR AS EMPRESA_INTERIOR, EMPRESA.COLONIA AS EMPRESA_COLONIA, EMPRESA.CP AS EMPRESA_CP, EMPRESA.LOCALIDAD AS EMPRESA_LOCALIDAD, EMPRESA.REFERENCIA AS EMPRESA_REFERENCIA, EMPRESA.PAIS AS EMPRESA_PAIS, EMPRESA.ESTADO AS EMPRESA_ESTADO, EMPRESA.MUNICIPIO AS EMPRESA_MUNICIPIO ";
            sSQL += "  FROM (((((FACTURA INNER JOIN FACTURA_LINEA ON FACTURA.ROW_ID = FACTURA_LINEA.ROW_ID_FACTURA) INNER JOIN CLIENTE ON FACTURA.ROW_ID_CLIENTE = CLIENTE.ROW_ID) INNER JOIN SERIE ON FACTURA.ROW_ID_SERIE = SERIE.ROW_ID) INNER JOIN CERTIFICADO ON SERIE.ROW_ID_CERTIFICADO = CERTIFICADO.ROW_ID) INNER JOIN MONEDA ON FACTURA.ROW_ID_MONEDA = MONEDA.ROW_ID) INNER JOIN EMPRESA ON FACTURA.ROW_ID_EMPRESA = EMPRESA.ROW_ID;";
            myCommand.CommandText = sSQL;
            myCommand.ExecuteNonQuery();

            sSQL = "";
            sSQL += "CREATE TABLE CONCEPTO  ";
            sSQL += " (ROW_ID AUTOINCREMENT, ID TEXT, VALOR_UNITARIO TEXT, IMPORTE  TEXT) ";
            myCommand.CommandText = sSQL;
            myCommand.ExecuteNonQuery();

            myCommand.CommandText = "UPDATE APLICACION_VERSION SET NO_VERSION='" + Application.ProductVersion + "'";
            myCommand.ExecuteNonQuery();

            myCommand.Connection.Close();
            oData.DestruirConexion();
        }

        private void actualizar_version_1009()
        {
            oData.CrearConexion();
            string sSQL = "";
            OleDbCommand myCommand = new OleDbCommand();
            myCommand.Connection = oData.oConn;

            //Cargar Usuarios
            sSQL = "SELECT * FROM USUARIO ";
            DataTable oDataTable = oData.EjecutarConsulta(sSQL);
            if (oDataTable != null)
            {
                foreach (DataRow oDataRow in oDataTable.Rows)
                {
                    string ROW_ID_tr = oDataRow["ROW_ID"].ToString();
                    string PASSWORD_tr = oDataRow["PASSWORD"].ToString();
                    PASSWORD_tr = cENCRIPTACION.Encrypt(PASSWORD_tr);
                    oData.CrearConexion();
                    myCommand.Connection = oData.oConn;
                    myCommand.CommandText = "UPDATE USUARIO SET [PASSWORD]='" + PASSWORD_tr + "' WHERE ROW_ID=" + ROW_ID_tr;
                    myCommand.ExecuteNonQuery();
                }

            }

            //Cargar Empresas
            sSQL = "SELECT * FROM EMPRESA ";
            oDataTable = oData.EjecutarConsulta(sSQL);
            if (oDataTable != null)
            {
                foreach (DataRow oDataRow in oDataTable.Rows)
                {
                    string ROW_ID_tr = oDataRow["ROW_ID"].ToString();
                    string PASSWORD_tr = oDataRow["PASSWORD_EMAIL"].ToString();
                    PASSWORD_tr = cENCRIPTACION.Encrypt(PASSWORD_tr);
                    oData.CrearConexion();
                    myCommand.Connection = oData.oConn;

                    myCommand.CommandText = "UPDATE EMPRESA SET PASSWORD_EMAIL='" + PASSWORD_tr + "' WHERE ROW_ID=" + ROW_ID_tr;
                    myCommand.ExecuteNonQuery();
                }

            }
            //Cargar Certificados
            sSQL = "SELECT * FROM CERTIFICADO ";
            oDataTable = oData.EjecutarConsulta(sSQL);
            if (oDataTable != null)
            {
                foreach (DataRow oDataRow in oDataTable.Rows)
                {
                    string ROW_ID_tr = oDataRow["ROW_ID"].ToString();
                    string PASSWORD_tr = oDataRow["PASSWORD"].ToString();
                    PASSWORD_tr = cENCRIPTACION.Encrypt(PASSWORD_tr);
                    oData.CrearConexion();
                    myCommand.Connection = oData.oConn;

                    myCommand.CommandText = "UPDATE CERTIFICADO SET [PASSWORD]='" + PASSWORD_tr + "' WHERE ROW_ID=" + ROW_ID_tr;
                    myCommand.ExecuteNonQuery();
                }

            }

            sSQL = "";
            sSQL += "CREATE TABLE PEDIMENTO  ";
            sSQL += " (ROW_ID AUTOINCREMENT, ROW_ID_FACTURA NUMBER, ROW_ID_LINEA NUMBER, PEDIMENTO TEXT, FECHA  TEXT, ADUANA  TEXT) ";
            myCommand.CommandText = sSQL;
            myCommand.ExecuteNonQuery();

            myCommand.CommandText = "UPDATE APLICACION_VERSION SET NO_VERSION='" + Application.ProductVersion + "'";
            myCommand.ExecuteNonQuery();

            myCommand.Connection.Close();
            oData.DestruirConexion();
        }

        private void actualizar_version_1010()
        {
            oData.CrearConexion();
            string sSQL = "";
            OleDbCommand myCommand = new OleDbCommand();
            myCommand.Connection = oData.oConn;

            myCommand.CommandText = "DROP VIEW FE_VISTA";
            myCommand.ExecuteNonQuery();

            sSQL = "";
            sSQL += "CREATE VIEW FE_VISTA AS ";
            sSQL += " SELECT FACTURA.ROW_ID, FACTURA.ID AS FACTURA, CLIENTE.EMAIL, CLIENTE.CONTACTO, FACTURA.FECHA_CREACION, FACTURA.TIPO, SERIE.ANIO  as SERIE_ANIO, SERIE.CALLE as SERIE_CALLE, SERIE.ESTADO  as SERIE_ESTADO, SERIE.MUNICIPIO  as SERIE_MUNICIPIO, SERIE.EXTERIOR  as SERIE_EXTERIOR, SERIE.INTERIOR  as SERIE_INTERIOR, SERIE.COLONIA  as SERIE_COLONIA, SERIE.CP  as SERIE_CP, SERIE.PAIS  as SERIE_PAIS, SERIE.LOCALIDAD  as SERIE_LOCALIDAD, SERIE.REFERENCIA  as SERIE_REFERENCIA, FACTURA.IMPUESTO_DATO, FACTURA.SUBTOTAL, FACTURA.IMPUESTO, FACTURA.RETENCION, FACTURA.TOTAL, FACTURA.SELLO, FACTURA.CADENA, FACTURA_LINEA.CANTIDAD, FACTURA_LINEA.UNIDAD, FACTURA_LINEA.NO_IDENTIFICACION, FACTURA_LINEA.DESCRIPCION, FACTURA_LINEA.VALOR_UNITARIO, FACTURA_LINEA.IMPORTE, CLIENTE.ID AS CLIENTE, CLIENTE.RFC, CLIENTE.CALLE, CLIENTE.EXTERIOR, CLIENTE.INTERIOR, CLIENTE.COLONIA, CLIENTE.CP, CLIENTE.LOCALIDAD, CLIENTE.REFERENCIA, CLIENTE.PAIS, CLIENTE.ESTADO, CLIENTE.MUNICIPIO, FACTURA.OBSERVACIONES, FACTURA.TIPO_CAMBIO, FACTURA.APROBACION, CERTIFICADO.CERTIFICADO, CERTIFICADO.NO_CERTIFICADO, MONEDA.ID AS MONEDA_ID, EMPRESA.ID AS EMPRESA_ID, EMPRESA.RFC AS EMPRESA_RFC, EMPRESA.CALLE AS EMPRESA_CALLE, EMPRESA.EXTERIOR AS EMPRESA_EXTERIOR, EMPRESA.INTERIOR AS EMPRESA_INTERIOR, EMPRESA.COLONIA AS EMPRESA_COLONIA, EMPRESA.CP AS EMPRESA_CP, EMPRESA.LOCALIDAD AS EMPRESA_LOCALIDAD, EMPRESA.REFERENCIA AS EMPRESA_REFERENCIA, EMPRESA.PAIS AS EMPRESA_PAIS, EMPRESA.ESTADO AS EMPRESA_ESTADO, EMPRESA.MUNICIPIO AS EMPRESA_MUNICIPIO ";
            sSQL += " FROM (((((FACTURA INNER JOIN FACTURA_LINEA ON FACTURA.ROW_ID = FACTURA_LINEA.ROW_ID_FACTURA) INNER JOIN CLIENTE ON FACTURA.ROW_ID_CLIENTE = CLIENTE.ROW_ID) INNER JOIN SERIE ON FACTURA.ROW_ID_SERIE = SERIE.ROW_ID) INNER JOIN CERTIFICADO ON SERIE.ROW_ID_CERTIFICADO = CERTIFICADO.ROW_ID) INNER JOIN MONEDA ON FACTURA.ROW_ID_MONEDA = MONEDA.ROW_ID) INNER JOIN EMPRESA ON FACTURA.ROW_ID_EMPRESA = EMPRESA.ROW_ID;";
            myCommand.CommandText = sSQL;
            myCommand.ExecuteNonQuery();

            myCommand.CommandText = "UPDATE APLICACION_VERSION SET NO_VERSION='" + Application.ProductVersion + "'";
            myCommand.ExecuteNonQuery();

            myCommand.Connection.Close();
            oData.DestruirConexion();
        }

        private void actualizar_version_10013()
        {
            oData.CrearConexion();
            OleDbCommand myCommand = new OleDbCommand();
            myCommand.Connection = oData.oConn;
            string sSQL = "";

            sSQL = "";
            sSQL += "CREATE TABLE IMPUESTO  ";
            sSQL += " (ROW_ID AUTOINCREMENT, ID TEXT, PORCENTAJE TEXT, RETENCION  TEXT) ";
            myCommand.CommandText = sSQL;
            myCommand.ExecuteNonQuery();

            myCommand.CommandText = "UPDATE APLICACION_VERSION SET NO_VERSION='" + Application.ProductVersion + "'";
            myCommand.ExecuteNonQuery();

            myCommand.Connection.Close();
            oData.DestruirConexion();
        }

        private void actualizar_version_10014()
        {
            oData.CrearConexion();
            OleDbCommand myCommand = new OleDbCommand();
            myCommand.Connection = oData.oConn;
            string sSQL = "";

            sSQL = "";
            sSQL += " ALTER TABLE FACTURA_LINEA  ";
            sSQL += " ADD COLUMN ROW_ID_IMPUESTO NUMBER ";
            myCommand.CommandText = sSQL;
            myCommand.ExecuteNonQuery();

            sSQL = " ALTER TABLE FACTURA_LINEA  ";
            sSQL += " ADD COLUMN IMPUESTO TEXT ";
            myCommand.CommandText = sSQL;
            myCommand.ExecuteNonQuery();

            myCommand.CommandText = "UPDATE APLICACION_VERSION SET NO_VERSION='" + Application.ProductVersion + "'";
            myCommand.ExecuteNonQuery();

            myCommand.Connection.Close();
            oData.DestruirConexion();
        }

        private void actualizar_version_10015()
        {
            oData.CrearConexion();
            OleDbCommand myCommand = new OleDbCommand();
            myCommand.Connection = oData.oConn;
            string sSQL = "";

            sSQL = "";

            sSQL = " ALTER TABLE FACTURA  ";
            sSQL += " ADD COLUMN USER_1 TEXT ";
            myCommand.CommandText = sSQL;
            myCommand.ExecuteNonQuery();

            sSQL = " ALTER TABLE FACTURA  ";
            sSQL += " ADD COLUMN USER_2 TEXT ";
            myCommand.CommandText = sSQL;
            myCommand.ExecuteNonQuery();

            sSQL = " ALTER TABLE FACTURA  ";
            sSQL += " ADD COLUMN USER_3 TEXT ";
            myCommand.CommandText = sSQL;
            myCommand.ExecuteNonQuery();

            sSQL = " ALTER TABLE FACTURA  ";
            sSQL += " ADD COLUMN USER_4 TEXT ";
            myCommand.CommandText = sSQL;
            myCommand.ExecuteNonQuery();

            sSQL = " ALTER TABLE FACTURA  ";
            sSQL += " ADD COLUMN USER_5 TEXT ";
            myCommand.CommandText = sSQL;
            myCommand.ExecuteNonQuery();

            sSQL = " ALTER TABLE FACTURA  ";
            sSQL += " ADD COLUMN USER_6 TEXT ";
            myCommand.CommandText = sSQL;
            myCommand.ExecuteNonQuery();

            myCommand.CommandText = "UPDATE APLICACION_VERSION SET NO_VERSION='" + Application.ProductVersion + "'";
            myCommand.ExecuteNonQuery();

            myCommand.Connection.Close();
            oData.DestruirConexion();
        }

        private void actualizar_version_10016()
        {
            oData.CrearConexion();
            OleDbCommand myCommand = new OleDbCommand();
            myCommand.Connection = oData.oConn;
            string sSQL = "";

            sSQL = "";
            sSQL += "CREATE TABLE FACTURA_IMPUESTO  ";
            sSQL += " (ROW_ID AUTOINCREMENT, ROW_ID_FACTURA NUMBER, ROW_ID_IMPUESTO NUMBER, IMPUESTO  Decimal(15,4)) ";
            myCommand.CommandText = sSQL;
            myCommand.ExecuteNonQuery();

            myCommand.CommandText = "UPDATE APLICACION_VERSION SET NO_VERSION='" + Application.ProductVersion + "'";
            myCommand.ExecuteNonQuery();

            myCommand.Connection.Close();
            oData.DestruirConexion();
        }

        private void actualizar_version_10017()
        {
            oData.CrearConexion();
            string sSQL = "";
            OleDbCommand myCommand = new OleDbCommand();
            myCommand.Connection = oData.oConn;



            //sSQL = "";
            //sSQL += "CREATE VIEW FE_VISTA AS ";
            //sSQL += " SELECT FACTURA.ROW_ID, FACTURA.ID AS FACTURA, CLIENTE.EMAIL, CLIENTE.CONTACTO, FACTURA.FECHA_CREACION, FACTURA.TIPO, SERIE.ANIO AS SERIE_ANIO, SERIE.CALLE AS SERIE_CALLE, SERIE.ESTADO AS SERIE_ESTADO, SERIE.MUNICIPIO AS SERIE_MUNICIPIO, SERIE.EXTERIOR AS SERIE_EXTERIOR, SERIE.INTERIOR AS SERIE_INTERIOR, SERIE.COLONIA AS SERIE_COLONIA, SERIE.CP AS SERIE_CP, SERIE.PAIS AS SERIE_PAIS, SERIE.LOCALIDAD AS SERIE_LOCALIDAD, SERIE.REFERENCIA AS SERIE_REFERENCIA, FACTURA.IMPUESTO_DATO, FACTURA.SUBTOTAL, FACTURA.IMPUESTO, FACTURA.RETENCION, FACTURA.TOTAL, FACTURA.SELLO, FACTURA.CADENA, FACTURA_LINEA.CANTIDAD, FACTURA_LINEA.UNIDAD, FACTURA_LINEA.NO_IDENTIFICACION, FACTURA_LINEA.DESCRIPCION, FACTURA_LINEA.VALOR_UNITARIO, FACTURA_LINEA.IMPORTE, CLIENTE.ID AS CLIENTE, CLIENTE.RFC, CLIENTE.CALLE, CLIENTE.EXTERIOR, CLIENTE.INTERIOR, CLIENTE.COLONIA, CLIENTE.CP, CLIENTE.LOCALIDAD, CLIENTE.REFERENCIA, CLIENTE.PAIS, CLIENTE.ESTADO, CLIENTE.MUNICIPIO, FACTURA.OBSERVACIONES, FACTURA.TIPO_CAMBIO, FACTURA.APROBACION, CERTIFICADO.CERTIFICADO, CERTIFICADO.NO_CERTIFICADO, MONEDA.ID AS MONEDA_ID, EMPRESA.ID AS EMPRESA_ID, EMPRESA.RFC AS EMPRESA_RFC, EMPRESA.CALLE AS EMPRESA_CALLE, EMPRESA.EXTERIOR AS EMPRESA_EXTERIOR, EMPRESA.INTERIOR AS EMPRESA_INTERIOR, EMPRESA.COLONIA AS EMPRESA_COLONIA, EMPRESA.CP AS EMPRESA_CP, EMPRESA.LOCALIDAD AS EMPRESA_LOCALIDAD, EMPRESA.REFERENCIA AS EMPRESA_REFERENCIA, EMPRESA.PAIS AS EMPRESA_PAIS, EMPRESA.ESTADO AS EMPRESA_ESTADO, EMPRESA.MUNICIPIO AS EMPRESA_MUNICIPIO, FACTURA_LINEA.ROW_ID_IMPUESTO, FACTURA_LINEA.IMPUESTO AS FACTURA_LINEA_IMPUESTO, FACTURA_IMPUESTO.IMPUESTO AS IMPUESTO_FACTURA, IMPUESTO.ID AS IMPUESTO_ID, IMPUESTO.PORCENTAJE AS IMPUESTO_PORCENTAJE, IMPUESTO.RETENCION AS IMPUESTO_RETENCION ";
            //sSQL += " FROM (((((((FACTURA INNER JOIN FACTURA_LINEA ON FACTURA.ROW_ID = FACTURA_LINEA.ROW_ID_FACTURA) INNER JOIN CLIENTE ON FACTURA.ROW_ID_CLIENTE = CLIENTE.ROW_ID) INNER JOIN SERIE ON FACTURA.ROW_ID_SERIE = SERIE.ROW_ID) INNER JOIN CERTIFICADO ON SERIE.ROW_ID_CERTIFICADO = CERTIFICADO.ROW_ID) INNER JOIN MONEDA ON FACTURA.ROW_ID_MONEDA = MONEDA.ROW_ID) INNER JOIN EMPRESA ON FACTURA.ROW_ID_EMPRESA = EMPRESA.ROW_ID) LEFT JOIN FACTURA_IMPUESTO ON FACTURA.ROW_ID = FACTURA_IMPUESTO.ROW_ID_FACTURA) LEFT JOIN IMPUESTO ON FACTURA_IMPUESTO.ROW_ID_IMPUESTO = IMPUESTO.ROW_ID;";
            //myCommand.CommandText = sSQL;
            //myCommand.ExecuteNonQuery();

            sSQL = "";
            sSQL += "CREATE TABLE FACTURA_IMPUESTO_LINEA  ";
            sSQL += " (ROW_ID AUTOINCREMENT, ROW_ID_FACTURA NUMBER, ROW_ID_IMPUESTO NUMBER, IMPUESTO  Decimal(15,4)) ";
            myCommand.CommandText = sSQL;
            myCommand.ExecuteNonQuery();

            myCommand.CommandText = "DROP VIEW FE_VISTA";
            myCommand.ExecuteNonQuery();

            sSQL = "";
            sSQL += " CREATE VIEW FE_VISTA AS ";
            sSQL += " SELECT FACTURA.ROW_ID, FACTURA.ID AS FACTURA, CLIENTE.EMAIL, CLIENTE.CONTACTO, FACTURA.FECHA_CREACION, FACTURA.TIPO, SERIE.ANIO AS SERIE_ANIO, SERIE.CALLE AS SERIE_CALLE, SERIE.ESTADO AS SERIE_ESTADO, SERIE.MUNICIPIO AS SERIE_MUNICIPIO, SERIE.EXTERIOR AS SERIE_EXTERIOR, SERIE.INTERIOR AS SERIE_INTERIOR, SERIE.COLONIA AS SERIE_COLONIA, SERIE.CP AS SERIE_CP, SERIE.PAIS AS SERIE_PAIS, SERIE.LOCALIDAD AS SERIE_LOCALIDAD, SERIE.REFERENCIA AS SERIE_REFERENCIA, FACTURA.IMPUESTO_DATO, FACTURA.SUBTOTAL, FACTURA.IMPUESTO, FACTURA.RETENCION, FACTURA.TOTAL, FACTURA.SELLO, FACTURA.CADENA, FACTURA_LINEA.CANTIDAD, FACTURA_LINEA.UNIDAD, FACTURA_LINEA.NO_IDENTIFICACION, FACTURA_LINEA.DESCRIPCION, FACTURA_LINEA.VALOR_UNITARIO, FACTURA_LINEA.IMPORTE, CLIENTE.ID AS CLIENTE, CLIENTE.RFC, CLIENTE.CALLE, CLIENTE.EXTERIOR, CLIENTE.INTERIOR, CLIENTE.COLONIA, CLIENTE.CP, CLIENTE.LOCALIDAD, CLIENTE.REFERENCIA, CLIENTE.PAIS, CLIENTE.ESTADO, CLIENTE.MUNICIPIO, FACTURA.OBSERVACIONES, FACTURA.TIPO_CAMBIO, FACTURA.APROBACION, CERTIFICADO.CERTIFICADO, CERTIFICADO.NO_CERTIFICADO, MONEDA.ID AS MONEDA_ID, EMPRESA.ID AS EMPRESA_ID, EMPRESA.RFC AS EMPRESA_RFC, EMPRESA.CALLE AS EMPRESA_CALLE, EMPRESA.EXTERIOR AS EMPRESA_EXTERIOR, EMPRESA.INTERIOR AS EMPRESA_INTERIOR, EMPRESA.COLONIA AS EMPRESA_COLONIA, EMPRESA.CP AS EMPRESA_CP, EMPRESA.LOCALIDAD AS EMPRESA_LOCALIDAD, EMPRESA.REFERENCIA AS EMPRESA_REFERENCIA, EMPRESA.PAIS AS EMPRESA_PAIS, EMPRESA.ESTADO AS EMPRESA_ESTADO, EMPRESA.MUNICIPIO AS EMPRESA_MUNICIPIO, FACTURA.USER_1, FACTURA.USER_2, FACTURA.USER_3, FACTURA.USER_4, FACTURA.USER_5, FACTURA.USER_6, IMPUESTO.ID   as IMPUESTO_LINEA_ID, IMPUESTO.[PORCENTAJE]   as IMPUESTO_LINEA_PORCENTAJE, IMPUESTO.RETENCION  as IMPUESTO_LINEA_RETENCION, FACTURA_LINEA.IMPUESTO as IMPUESTO_LINEA FROM ((((((FACTURA INNER JOIN FACTURA_LINEA ON FACTURA.ROW_ID = FACTURA_LINEA.ROW_ID_FACTURA) INNER JOIN CLIENTE ON FACTURA.ROW_ID_CLIENTE = CLIENTE.ROW_ID) INNER JOIN SERIE ON FACTURA.ROW_ID_SERIE = SERIE.ROW_ID) INNER JOIN CERTIFICADO ON SERIE.ROW_ID_CERTIFICADO = CERTIFICADO.ROW_ID) INNER JOIN MONEDA ON FACTURA.ROW_ID_MONEDA = MONEDA.ROW_ID) INNER JOIN EMPRESA ON FACTURA.ROW_ID_EMPRESA = EMPRESA.ROW_ID) LEFT JOIN IMPUESTO ON FACTURA_LINEA.ROW_ID_IMPUESTO = IMPUESTO.ROW_ID; ";
            myCommand.CommandText = sSQL;
            myCommand.ExecuteNonQuery();

            myCommand.CommandText = "UPDATE APLICACION_VERSION SET NO_VERSION='" + Application.ProductVersion + "'";
            myCommand.ExecuteNonQuery();

            myCommand.Connection.Close();
            oData.DestruirConexion();
        }

        private void actualizar_version_10018()
        {
            oData.CrearConexion();
            string sSQL = "";
            OleDbCommand myCommand = new OleDbCommand();
            myCommand.Connection = oData.oConn;

            sSQL = "";
            sSQL += "CREATE TABLE FACTURA_USER  ";
            sSQL += " (ROW_ID AUTOINCREMENT, ID TEXT, USER_1  TEXT, USER_2  TEXT, USER_3  TEXT, USER_4  TEXT, USER_5  TEXT, USER_6  TEXT) ";
            myCommand.CommandText = sSQL;
            myCommand.ExecuteNonQuery();

            sSQL = " ALTER TABLE IMPUESTO  ";
            sSQL += " ADD COLUMN ISR TEXT ";
            myCommand.CommandText = sSQL;
            myCommand.ExecuteNonQuery();

            myCommand.CommandText = "UPDATE APLICACION_VERSION SET NO_VERSION='" + Application.ProductVersion + "'";
            myCommand.ExecuteNonQuery();

            myCommand.Connection.Close();
            oData.DestruirConexion();
        }

        private void actualizar_version_10019()
        {
            oData.CrearConexion();
            string sSQL = "";
            OleDbCommand myCommand = new OleDbCommand();
            myCommand.Connection = oData.oConn;

            myCommand.CommandText = "DROP VIEW FE_VISTA";
            myCommand.ExecuteNonQuery();

            sSQL = "";
            sSQL += " CREATE VIEW FE_VISTA AS ";
            sSQL += " SELECT FACTURA.ROW_ID, FACTURA.ID AS FACTURA, CLIENTE.EMAIL, CLIENTE.CONTACTO, FACTURA.FECHA_CREACION, FACTURA.TIPO, SERIE.ID as SERIE_ID, SERIE.ANIO AS SERIE_ANIO, SERIE.CALLE AS SERIE_CALLE, SERIE.ESTADO AS SERIE_ESTADO, SERIE.MUNICIPIO AS SERIE_MUNICIPIO, SERIE.EXTERIOR AS SERIE_EXTERIOR, SERIE.INTERIOR AS SERIE_INTERIOR, SERIE.COLONIA AS SERIE_COLONIA, SERIE.CP AS SERIE_CP, SERIE.PAIS AS SERIE_PAIS, SERIE.LOCALIDAD AS SERIE_LOCALIDAD, SERIE.REFERENCIA AS SERIE_REFERENCIA, FACTURA.IMPUESTO_DATO, FACTURA.SUBTOTAL, FACTURA.IMPUESTO, FACTURA.RETENCION, FACTURA.TOTAL, FACTURA.SELLO, FACTURA.CADENA, FACTURA_LINEA.CANTIDAD, FACTURA_LINEA.UNIDAD, FACTURA_LINEA.NO_IDENTIFICACION, FACTURA_LINEA.DESCRIPCION, FACTURA_LINEA.VALOR_UNITARIO, FACTURA_LINEA.IMPORTE, CLIENTE.ID AS CLIENTE, CLIENTE.RFC, CLIENTE.CALLE, CLIENTE.EXTERIOR, CLIENTE.INTERIOR, CLIENTE.COLONIA, CLIENTE.CP, CLIENTE.LOCALIDAD, CLIENTE.REFERENCIA, CLIENTE.PAIS, CLIENTE.ESTADO, CLIENTE.MUNICIPIO, FACTURA.OBSERVACIONES, FACTURA.TIPO_CAMBIO, FACTURA.APROBACION, CERTIFICADO.CERTIFICADO, CERTIFICADO.NO_CERTIFICADO, MONEDA.ID AS MONEDA_ID, EMPRESA.ID AS EMPRESA_ID, EMPRESA.RFC AS EMPRESA_RFC, EMPRESA.CALLE AS EMPRESA_CALLE, EMPRESA.EXTERIOR AS EMPRESA_EXTERIOR, EMPRESA.INTERIOR AS EMPRESA_INTERIOR, EMPRESA.COLONIA AS EMPRESA_COLONIA, EMPRESA.CP AS EMPRESA_CP, EMPRESA.LOCALIDAD AS EMPRESA_LOCALIDAD, EMPRESA.REFERENCIA AS EMPRESA_REFERENCIA, EMPRESA.PAIS AS EMPRESA_PAIS, EMPRESA.ESTADO AS EMPRESA_ESTADO, EMPRESA.MUNICIPIO AS EMPRESA_MUNICIPIO, FACTURA.USER_1, FACTURA.USER_2, FACTURA.USER_3, FACTURA.USER_4, FACTURA.USER_5, FACTURA.USER_6, IMPUESTO.ID AS IMPUESTO_LINEA_ID, IMPUESTO.PORCENTAJE AS IMPUESTO_LINEA_PORCENTAJE, IMPUESTO.RETENCION AS IMPUESTO_LINEA_RETENCION, FACTURA_LINEA.IMPUESTO AS IMPUESTO_LINEA FROM ((((((FACTURA INNER JOIN FACTURA_LINEA ON FACTURA.ROW_ID = FACTURA_LINEA.ROW_ID_FACTURA) INNER JOIN CLIENTE ON FACTURA.ROW_ID_CLIENTE = CLIENTE.ROW_ID) INNER JOIN SERIE ON FACTURA.ROW_ID_SERIE = SERIE.ROW_ID) INNER JOIN CERTIFICADO ON SERIE.ROW_ID_CERTIFICADO = CERTIFICADO.ROW_ID) INNER JOIN MONEDA ON FACTURA.ROW_ID_MONEDA = MONEDA.ROW_ID) INNER JOIN EMPRESA ON FACTURA.ROW_ID_EMPRESA = EMPRESA.ROW_ID) LEFT JOIN IMPUESTO ON FACTURA_LINEA.ROW_ID_IMPUESTO = IMPUESTO.ROW_ID;";

            myCommand.CommandText = sSQL;
            myCommand.ExecuteNonQuery();

            myCommand.CommandText = "UPDATE APLICACION_VERSION SET NO_VERSION='" + Application.ProductVersion + "'";
            myCommand.ExecuteNonQuery();

            myCommand.Connection.Close();
            oData.DestruirConexion();
        }




        private void actualizar_version_10020()
        {
            oData.CrearConexion();
            string sSQL = "";
            OleDbCommand myCommand = new OleDbCommand();
            myCommand.Connection = oData.oConn;

            myCommand.CommandText = "DROP VIEW FE_VISTA";
            myCommand.ExecuteNonQuery();

            sSQL = "";
            sSQL += " CREATE VIEW FE_VISTA AS ";
            sSQL += " SELECT FACTURA.ROW_ID, FACTURA.ID AS FACTURA, CLIENTE.EMAIL, CLIENTE.CONTACTO, FACTURA.FECHA_CREACION, FACTURA.CANCELADO, FACTURA.TIPO, SERIE.ID as SERIE_ID, SERIE.ANIO AS SERIE_ANIO, SERIE.CALLE AS SERIE_CALLE, SERIE.ESTADO AS SERIE_ESTADO, SERIE.MUNICIPIO AS SERIE_MUNICIPIO, SERIE.EXTERIOR AS SERIE_EXTERIOR, SERIE.INTERIOR AS SERIE_INTERIOR, SERIE.COLONIA AS SERIE_COLONIA, SERIE.CP AS SERIE_CP, SERIE.PAIS AS SERIE_PAIS, SERIE.LOCALIDAD AS SERIE_LOCALIDAD, SERIE.REFERENCIA AS SERIE_REFERENCIA, FACTURA.IMPUESTO_DATO, FACTURA.SUBTOTAL, FACTURA.IMPUESTO, FACTURA.RETENCION, FACTURA.TOTAL, FACTURA.SELLO, FACTURA.CADENA, FACTURA_LINEA.CANTIDAD, FACTURA_LINEA.UNIDAD, FACTURA_LINEA.NO_IDENTIFICACION, FACTURA_LINEA.DESCRIPCION, FACTURA_LINEA.VALOR_UNITARIO, FACTURA_LINEA.IMPORTE, CLIENTE.ID AS CLIENTE, CLIENTE.RFC, CLIENTE.CALLE, CLIENTE.EXTERIOR, CLIENTE.INTERIOR, CLIENTE.COLONIA, CLIENTE.CP, CLIENTE.LOCALIDAD, CLIENTE.REFERENCIA, CLIENTE.PAIS, CLIENTE.ESTADO, CLIENTE.MUNICIPIO, FACTURA.OBSERVACIONES, FACTURA.TIPO_CAMBIO, FACTURA.APROBACION, CERTIFICADO.CERTIFICADO, CERTIFICADO.NO_CERTIFICADO, MONEDA.ID AS MONEDA_ID, EMPRESA.ID AS EMPRESA_ID, EMPRESA.RFC AS EMPRESA_RFC, EMPRESA.CALLE AS EMPRESA_CALLE, EMPRESA.EXTERIOR AS EMPRESA_EXTERIOR, EMPRESA.INTERIOR AS EMPRESA_INTERIOR, EMPRESA.COLONIA AS EMPRESA_COLONIA, EMPRESA.CP AS EMPRESA_CP, EMPRESA.LOCALIDAD AS EMPRESA_LOCALIDAD, EMPRESA.REFERENCIA AS EMPRESA_REFERENCIA, EMPRESA.PAIS AS EMPRESA_PAIS, EMPRESA.ESTADO AS EMPRESA_ESTADO, EMPRESA.MUNICIPIO AS EMPRESA_MUNICIPIO, FACTURA.USER_1, FACTURA.USER_2, FACTURA.USER_3, FACTURA.USER_4, FACTURA.USER_5, FACTURA.USER_6, IMPUESTO.ID AS IMPUESTO_LINEA_ID, IMPUESTO.PORCENTAJE AS IMPUESTO_LINEA_PORCENTAJE, IMPUESTO.RETENCION AS IMPUESTO_LINEA_RETENCION, FACTURA_LINEA.IMPUESTO AS IMPUESTO_LINEA FROM ((((((FACTURA INNER JOIN FACTURA_LINEA ON FACTURA.ROW_ID = FACTURA_LINEA.ROW_ID_FACTURA) INNER JOIN CLIENTE ON FACTURA.ROW_ID_CLIENTE = CLIENTE.ROW_ID) INNER JOIN SERIE ON FACTURA.ROW_ID_SERIE = SERIE.ROW_ID) INNER JOIN CERTIFICADO ON SERIE.ROW_ID_CERTIFICADO = CERTIFICADO.ROW_ID) INNER JOIN MONEDA ON FACTURA.ROW_ID_MONEDA = MONEDA.ROW_ID) INNER JOIN EMPRESA ON FACTURA.ROW_ID_EMPRESA = EMPRESA.ROW_ID) LEFT JOIN IMPUESTO ON FACTURA_LINEA.ROW_ID_IMPUESTO = IMPUESTO.ROW_ID;";

            myCommand.CommandText = sSQL;
            myCommand.ExecuteNonQuery();

            myCommand.CommandText = "UPDATE APLICACION_VERSION SET NO_VERSION='" + Application.ProductVersion + "'";
            myCommand.ExecuteNonQuery();

            myCommand.Connection.Close();
            oData.DestruirConexion();
        }

        private void actualizar_version_10021()
        {
            oData.CrearConexion();
            string sSQL = "";
            OleDbCommand myCommand = new OleDbCommand();
            myCommand.Connection = oData.oConn;

            myCommand.CommandText = "UPDATE APLICACION_VERSION SET NO_VERSION='" + Application.ProductVersion + "'";
            myCommand.ExecuteNonQuery();

            myCommand.Connection.Close();
            oData.DestruirConexion();
        }

        private void actualizar_version_10022()
        {
            oData.CrearConexion();
            string sSQL = "";
            OleDbCommand myCommand = new OleDbCommand();
            myCommand.Connection = oData.oConn;

            myCommand.CommandText = "UPDATE APLICACION_VERSION SET NO_VERSION='" + Application.ProductVersion + "'";
            myCommand.ExecuteNonQuery();

            myCommand.Connection.Close();
            oData.DestruirConexion();
        }


        private void actualizar_version_10023()
        {
            oData.CrearConexion();
            string sSQL = "";
            OleDbCommand myCommand = new OleDbCommand();
            myCommand.Connection = oData.oConn;

            sSQL = " ALTER TABLE CLIENTE  ";
            sSQL += " ADD COLUMN USER_1 TEXT ";
            myCommand.CommandText = sSQL;
            myCommand.ExecuteNonQuery();

            sSQL = " ALTER TABLE CLIENTE  ";
            sSQL += " ADD COLUMN USER_2 TEXT ";
            myCommand.CommandText = sSQL;
            myCommand.ExecuteNonQuery();

            sSQL = " ALTER TABLE CLIENTE  ";
            sSQL += " ADD COLUMN USER_3 TEXT ";
            myCommand.CommandText = sSQL;
            myCommand.ExecuteNonQuery();

            sSQL = " ALTER TABLE CLIENTE  ";
            sSQL += " ADD COLUMN USER_4 TEXT ";
            myCommand.CommandText = sSQL;
            myCommand.ExecuteNonQuery();

            sSQL = " ALTER TABLE CLIENTE  ";
            sSQL += " ADD COLUMN USER_5 TEXT ";
            myCommand.CommandText = sSQL;
            myCommand.ExecuteNonQuery();

            sSQL = " ALTER TABLE CLIENTE  ";
            sSQL += " ADD COLUMN USER_6 TEXT ";
            myCommand.CommandText = sSQL;
            myCommand.ExecuteNonQuery();

            sSQL = "";
            sSQL += "CREATE TABLE CLIENTE_USER  ";
            sSQL += " (ROW_ID AUTOINCREMENT, ID TEXT, USER_1  TEXT, USER_2  TEXT, USER_3  TEXT, USER_4  TEXT, USER_5  TEXT, USER_6  TEXT) ";
            myCommand.CommandText = sSQL;
            myCommand.ExecuteNonQuery();

            myCommand.CommandText = "UPDATE APLICACION_VERSION SET NO_VERSION='" + Application.ProductVersion + "'";
            myCommand.ExecuteNonQuery();

            myCommand.Connection.Close();
            oData.DestruirConexion();
        }

        private void actualizar_version_10024()
        {
            oData.CrearConexion();
            string sSQL = "";
            OleDbCommand myCommand = new OleDbCommand();
            myCommand.Connection = oData.oConn;

            myCommand.CommandText = "DROP VIEW FE_VISTA";
            myCommand.ExecuteNonQuery();

            sSQL = " ALTER TABLE SERIE  ";
            sSQL += " ADD COLUMN ACTIVO TEXT ";
            myCommand.CommandText = sSQL;
            myCommand.ExecuteNonQuery();

            sSQL = " UPDATE SERIE SET ACTIVO='True'  ";
            myCommand.CommandText = sSQL;
            myCommand.ExecuteNonQuery();

            sSQL = " ALTER TABLE MONEDA  ";
            sSQL += " ADD COLUMN ACTIVO TEXT ";
            myCommand.CommandText = sSQL;
            myCommand.ExecuteNonQuery();

            sSQL = " UPDATE MONEDA SET ACTIVO='True'  ";
            myCommand.CommandText = sSQL;
            myCommand.ExecuteNonQuery();

            sSQL = " ALTER TABLE FORMATO  ";
            sSQL += " ADD COLUMN ACTIVO TEXT ";
            myCommand.CommandText = sSQL;
            myCommand.ExecuteNonQuery();

            sSQL = " UPDATE FORMATO SET ACTIVO='True'  ";
            myCommand.CommandText = sSQL;
            myCommand.ExecuteNonQuery();

            sSQL = " ALTER TABLE EMPRESA  ";
            sSQL += " ADD COLUMN ACTIVO TEXT ";
            myCommand.CommandText = sSQL;
            myCommand.ExecuteNonQuery();

            sSQL = " UPDATE EMPRESA SET ACTIVO='True'  ";
            myCommand.CommandText = sSQL;
            myCommand.ExecuteNonQuery();

            sSQL = " ALTER TABLE CLIENTE  ";
            sSQL += " ADD COLUMN ACTIVO TEXT ";
            myCommand.CommandText = sSQL;
            myCommand.ExecuteNonQuery();

            sSQL = " UPDATE CLIENTE SET ACTIVO='True'  ";
            myCommand.CommandText = sSQL;
            myCommand.ExecuteNonQuery();

            sSQL = " ALTER TABLE CERTIFICADO  ";
            sSQL += " ADD COLUMN ACTIVO TEXT ";
            myCommand.CommandText = sSQL;
            myCommand.ExecuteNonQuery();

            sSQL = " UPDATE CERTIFICADO SET ACTIVO='True'  ";
            myCommand.CommandText = sSQL;
            myCommand.ExecuteNonQuery();

            sSQL = " ALTER TABLE USUARIO  ";
            sSQL += " ADD COLUMN ACTIVO TEXT ";
            myCommand.CommandText = sSQL;
            myCommand.ExecuteNonQuery();

            sSQL = " UPDATE USUARIO SET ACTIVO='True'  ";
            myCommand.CommandText = sSQL;
            myCommand.ExecuteNonQuery();

            sSQL = " ALTER TABLE IMPUESTO  ";
            sSQL += " ADD COLUMN ACTIVO TEXT ";
            myCommand.CommandText = sSQL;
            myCommand.ExecuteNonQuery();

            sSQL = " UPDATE IMPUESTO SET ACTIVO='True'  ";
            myCommand.CommandText = sSQL;
            myCommand.ExecuteNonQuery();

            sSQL = "";
            sSQL += " CREATE VIEW FE_VISTA AS ";
            sSQL += " SELECT FACTURA.ROW_ID, FACTURA.ID AS FACTURA, CLIENTE.EMAIL, CLIENTE.CONTACTO, FACTURA.FECHA_CREACION, FACTURA.CANCELADO, FACTURA.TIPO, SERIE.ID AS SERIE_ID, SERIE.ANIO AS SERIE_ANIO, SERIE.CALLE AS SERIE_CALLE, SERIE.ESTADO AS SERIE_ESTADO, SERIE.MUNICIPIO AS SERIE_MUNICIPIO, SERIE.EXTERIOR AS SERIE_EXTERIOR, SERIE.INTERIOR AS SERIE_INTERIOR, SERIE.COLONIA AS SERIE_COLONIA, SERIE.CP AS SERIE_CP, SERIE.PAIS AS SERIE_PAIS, SERIE.LOCALIDAD AS SERIE_LOCALIDAD, SERIE.REFERENCIA AS SERIE_REFERENCIA, FACTURA.IMPUESTO_DATO, FACTURA.SUBTOTAL, FACTURA.IMPUESTO, FACTURA.RETENCION, FACTURA.TOTAL, FACTURA.SELLO, FACTURA.CADENA, FACTURA_LINEA.CANTIDAD, FACTURA_LINEA.UNIDAD, FACTURA_LINEA.NO_IDENTIFICACION, FACTURA_LINEA.DESCRIPCION, FACTURA_LINEA.VALOR_UNITARIO, FACTURA_LINEA.IMPORTE, CLIENTE.ID AS CLIENTE, CLIENTE.RFC, CLIENTE.CALLE, CLIENTE.EXTERIOR, CLIENTE.INTERIOR, CLIENTE.COLONIA, CLIENTE.CP, CLIENTE.LOCALIDAD, CLIENTE.REFERENCIA, CLIENTE.PAIS, CLIENTE.ESTADO, CLIENTE.MUNICIPIO, FACTURA.OBSERVACIONES, FACTURA.TIPO_CAMBIO, FACTURA.APROBACION, CERTIFICADO.CERTIFICADO, CERTIFICADO.NO_CERTIFICADO, MONEDA.ID AS MONEDA_ID, EMPRESA.ID AS EMPRESA_ID, EMPRESA.RFC AS EMPRESA_RFC, EMPRESA.CALLE AS EMPRESA_CALLE, EMPRESA.EXTERIOR AS EMPRESA_EXTERIOR, EMPRESA.INTERIOR AS EMPRESA_INTERIOR, EMPRESA.COLONIA AS EMPRESA_COLONIA, EMPRESA.CP AS EMPRESA_CP, EMPRESA.LOCALIDAD AS EMPRESA_LOCALIDAD, EMPRESA.REFERENCIA AS EMPRESA_REFERENCIA, EMPRESA.PAIS AS EMPRESA_PAIS, EMPRESA.ESTADO AS EMPRESA_ESTADO, EMPRESA.MUNICIPIO AS EMPRESA_MUNICIPIO, FACTURA.USER_1, FACTURA.USER_2, FACTURA.USER_3, FACTURA.USER_4, FACTURA.USER_5, FACTURA.USER_6, IMPUESTO.ID AS IMPUESTO_LINEA_ID, IMPUESTO.PORCENTAJE AS IMPUESTO_LINEA_PORCENTAJE, IMPUESTO.RETENCION AS IMPUESTO_LINEA_RETENCION, FACTURA_LINEA.IMPUESTO AS IMPUESTO_LINEA, CLIENTE.USER_1 as CLIENTE_USER1, CLIENTE.USER_2  as CLIENTE_USER2, CLIENTE.USER_3  as CLIENTE_USER3, CLIENTE.USER_4  as CLIENTE_USER4, CLIENTE.USER_5  as CLIENTE_USER5, CLIENTE.USER_6  as CLIENTE_USER6 FROM ((((((FACTURA INNER JOIN FACTURA_LINEA ON FACTURA.ROW_ID = FACTURA_LINEA.ROW_ID_FACTURA) INNER JOIN CLIENTE ON FACTURA.ROW_ID_CLIENTE = CLIENTE.ROW_ID) INNER JOIN SERIE ON FACTURA.ROW_ID_SERIE = SERIE.ROW_ID) INNER JOIN CERTIFICADO ON SERIE.ROW_ID_CERTIFICADO = CERTIFICADO.ROW_ID) INNER JOIN MONEDA ON FACTURA.ROW_ID_MONEDA = MONEDA.ROW_ID) INNER JOIN EMPRESA ON FACTURA.ROW_ID_EMPRESA = EMPRESA.ROW_ID) LEFT JOIN IMPUESTO ON FACTURA_LINEA.ROW_ID_IMPUESTO = IMPUESTO.ROW_ID;";

            myCommand.CommandText = sSQL;
            myCommand.ExecuteNonQuery();

            myCommand.CommandText = "UPDATE APLICACION_VERSION SET NO_VERSION='" + Application.ProductVersion + "'";
            myCommand.ExecuteNonQuery();

            myCommand.Connection.Close();
            oData.DestruirConexion();
        }


        private void actualizar_version_10025()
        {
            oData.CrearConexion();
            string sSQL = "";
            OleDbCommand myCommand = new OleDbCommand();
            myCommand.Connection = oData.oConn;

            sSQL = "";
            sSQL += "CREATE TABLE ESTADO  ";
            sSQL += " (ROW_ID AUTOINCREMENT, ID TEXT, ACTIVO  TEXT) ";
            myCommand.CommandText = sSQL;
            myCommand.ExecuteNonQuery();

            sSQL = " INSERT INTO ESTADO (ID, ACTIVO) VALUES ('AGUASCALIENTES','True')";
            myCommand.CommandText = sSQL;
            myCommand.ExecuteNonQuery();

            sSQL = " INSERT INTO ESTADO (ID, ACTIVO) VALUES ('BAJA CALIFORNIA','True')";
            myCommand.CommandText = sSQL;
            myCommand.ExecuteNonQuery();

            sSQL = " INSERT INTO ESTADO (ID, ACTIVO) VALUES ('BAJA CALIFORNIA SUR','True')";
            myCommand.CommandText = sSQL;
            myCommand.ExecuteNonQuery();

            sSQL = " INSERT INTO ESTADO (ID, ACTIVO) VALUES ('CAMPECHE','True')";
            myCommand.CommandText = sSQL;
            myCommand.ExecuteNonQuery();

            sSQL = " INSERT INTO ESTADO (ID, ACTIVO) VALUES ('CHIAPAS','True')";
            myCommand.CommandText = sSQL;
            myCommand.ExecuteNonQuery();

            sSQL = " INSERT INTO ESTADO (ID, ACTIVO) VALUES ('CHIHUAHUA','True')";
            myCommand.CommandText = sSQL;
            myCommand.ExecuteNonQuery();

            sSQL = " INSERT INTO ESTADO (ID, ACTIVO) VALUES ('COAHUILA','True')";
            myCommand.CommandText = sSQL;
            myCommand.ExecuteNonQuery();

            sSQL = " INSERT INTO ESTADO (ID, ACTIVO) VALUES ('COLIMA','True')";
            myCommand.CommandText = sSQL;
            myCommand.ExecuteNonQuery();

            sSQL = " INSERT INTO ESTADO (ID, ACTIVO) VALUES ('DISTRITO FEDERAL','True')";
            myCommand.CommandText = sSQL;
            myCommand.ExecuteNonQuery();

            sSQL = " INSERT INTO ESTADO (ID, ACTIVO) VALUES ('DURANGO','True')";
            myCommand.CommandText = sSQL;
            myCommand.ExecuteNonQuery();

            sSQL = "INSERT INTO ESTADO (ID, ACTIVO) VALUES ('DURANGO','True')";
            myCommand.CommandText = sSQL;
            myCommand.ExecuteNonQuery();

            sSQL = "INSERT INTO ESTADO (ID, ACTIVO) VALUES ('GUANAJUATO','True')";
            myCommand.CommandText = sSQL;
            myCommand.ExecuteNonQuery();

            sSQL = "INSERT INTO ESTADO (ID, ACTIVO) VALUES ('GUERRERO','True')";
            myCommand.CommandText = sSQL;
            myCommand.ExecuteNonQuery();

            sSQL = "INSERT INTO ESTADO (ID, ACTIVO) VALUES ('HIDALGO','True')";
            myCommand.CommandText = sSQL;
            myCommand.ExecuteNonQuery();

            sSQL = "INSERT INTO ESTADO (ID, ACTIVO) VALUES ('JALISCO','True')";
            myCommand.CommandText = sSQL;
            myCommand.ExecuteNonQuery();

            sSQL = "INSERT INTO ESTADO (ID, ACTIVO) VALUES ('MEXICO','True')";
            myCommand.CommandText = sSQL;
            myCommand.ExecuteNonQuery();

            sSQL = "INSERT INTO ESTADO (ID, ACTIVO) VALUES ('MICHOACAN','True')";
            myCommand.CommandText = sSQL;
            myCommand.ExecuteNonQuery();

            sSQL = "INSERT INTO ESTADO (ID, ACTIVO) VALUES ('MORELOS','True')";
            myCommand.CommandText = sSQL;
            myCommand.ExecuteNonQuery();

            sSQL = "INSERT INTO ESTADO (ID, ACTIVO) VALUES ('NAYARIT','True')";
            myCommand.CommandText = sSQL;
            myCommand.ExecuteNonQuery();

            sSQL = "INSERT INTO ESTADO (ID, ACTIVO) VALUES ('NUEVO LEON','True')";
            myCommand.CommandText = sSQL;
            myCommand.ExecuteNonQuery();

            sSQL = "INSERT INTO ESTADO (ID, ACTIVO) VALUES ('OAXACA','True')";
            myCommand.CommandText = sSQL;
            myCommand.ExecuteNonQuery();

            sSQL = "INSERT INTO ESTADO (ID, ACTIVO) VALUES ('PUEBLA','True')";
            myCommand.CommandText = sSQL;
            myCommand.ExecuteNonQuery();

            sSQL = "INSERT INTO ESTADO (ID, ACTIVO) VALUES ('QUERETARO','True')";
            myCommand.CommandText = sSQL;
            myCommand.ExecuteNonQuery();

            sSQL = "INSERT INTO ESTADO (ID, ACTIVO) VALUES ('QUINTANA ROO','True')";
            myCommand.CommandText = sSQL;
            myCommand.ExecuteNonQuery();

            sSQL = "INSERT INTO ESTADO (ID, ACTIVO) VALUES ('SAN LUIS POTOSI','True')";
            myCommand.CommandText = sSQL;
            myCommand.ExecuteNonQuery();

            sSQL = "INSERT INTO ESTADO (ID, ACTIVO) VALUES ('SINALOA','True')";
            myCommand.CommandText = sSQL;
            myCommand.ExecuteNonQuery();

            sSQL = "INSERT INTO ESTADO (ID, ACTIVO) VALUES ('SONORA','True')";
            myCommand.CommandText = sSQL;
            myCommand.ExecuteNonQuery();

            sSQL = "INSERT INTO ESTADO (ID, ACTIVO) VALUES ('TABASCO','True')";
            myCommand.CommandText = sSQL;
            myCommand.ExecuteNonQuery();

            sSQL = "INSERT INTO ESTADO (ID, ACTIVO) VALUES ('TAMAULIPAS','True')";
            myCommand.CommandText = sSQL;
            myCommand.ExecuteNonQuery();

            sSQL = "INSERT INTO ESTADO (ID, ACTIVO) VALUES ('TLAXCALA','True')";
            myCommand.CommandText = sSQL;
            myCommand.ExecuteNonQuery();

            sSQL = "INSERT INTO ESTADO (ID, ACTIVO) VALUES ('VERACRUZ','True')";
            myCommand.CommandText = sSQL;
            myCommand.ExecuteNonQuery();

            sSQL = "INSERT INTO ESTADO (ID, ACTIVO) VALUES ('YUCATAN','True')";
            myCommand.CommandText = sSQL;
            myCommand.ExecuteNonQuery();

            sSQL = "INSERT INTO ESTADO (ID, ACTIVO) VALUES ('ZACATECAS','True')";
            myCommand.CommandText = sSQL;
            myCommand.ExecuteNonQuery();

            myCommand.CommandText = "UPDATE APLICACION_VERSION SET NO_VERSION='" + Application.ProductVersion + "'";
            myCommand.ExecuteNonQuery();

            myCommand.Connection.Close();
            oData.DestruirConexion();
        }

        private void actualizar_version_10026()
        {
            oData.CrearConexion();
            string sSQL = "";
            OleDbCommand myCommand = new OleDbCommand();
            myCommand.Connection = oData.oConn;

            sSQL = " ALTER TABLE SERIE  ";
            sSQL += " ALTER COLUMN MENSAJE MEMO ";
            myCommand.CommandText = sSQL;
            myCommand.ExecuteNonQuery();

            myCommand.CommandText = sSQL;
            myCommand.ExecuteNonQuery();

            myCommand.CommandText = "UPDATE APLICACION_VERSION SET NO_VERSION='" + Application.ProductVersion + "'";
            myCommand.ExecuteNonQuery();

            myCommand.Connection.Close();
            oData.DestruirConexion();
        }

        private void actualizar_version_10027()
        {
            oData.CrearConexion();
            string sSQL = "";
            OleDbCommand myCommand = new OleDbCommand();
            myCommand.Connection = oData.oConn;

            myCommand.CommandText = "UPDATE APLICACION_VERSION SET NO_VERSION='" + Application.ProductVersion + "'";
            myCommand.ExecuteNonQuery();

            myCommand.Connection.Close();
            oData.DestruirConexion();
        }

        private void actualizar_version_10028()
        {
            oData.CrearConexion();
            string sSQL = "";
            OleDbCommand myCommand = new OleDbCommand();
            myCommand.Connection = oData.oConn;

            sSQL = " ALTER TABLE FACTURA  ";
            sSQL += " ADD COLUMN FECHA_CANCELADO TEXT ";
            myCommand.CommandText = sSQL;
            myCommand.ExecuteNonQuery();

            sSQL = " ALTER TABLE IMPUESTO  ";
            sSQL += " ADD COLUMN IEPS TEXT ";
            myCommand.CommandText = sSQL;
            myCommand.ExecuteNonQuery();

            sSQL = " UPDATE IMPUESTO  SET IEPS='False' ";
            myCommand.CommandText = sSQL;
            myCommand.ExecuteNonQuery();


            myCommand.CommandText = "UPDATE APLICACION_VERSION SET NO_VERSION='" + Application.ProductVersion + "'";
            myCommand.ExecuteNonQuery();

            myCommand.Connection.Close();
            oData.DestruirConexion();
        }


        private void actualizar_version_10029()
        {
            oData.CrearConexion();
            string sSQL = "";
            OleDbCommand myCommand = new OleDbCommand();
            myCommand.Connection = oData.oConn;

            sSQL = " ALTER TABLE SERIE  ";
            sSQL += " ADD COLUMN IEPS TEXT ";
            myCommand.CommandText = sSQL;
            myCommand.ExecuteNonQuery();


            myCommand.CommandText = "UPDATE APLICACION_VERSION SET NO_VERSION='" + Application.ProductVersion + "'";
            myCommand.ExecuteNonQuery();

            myCommand.Connection.Close();
            oData.DestruirConexion();
        }

        private void actualizar_version_10030()
        {
            oData.CrearConexion();
            string sSQL = "";
            OleDbCommand myCommand = new OleDbCommand();
            myCommand.Connection = oData.oConn;

            sSQL = " ALTER TABLE SERIE  ";
            sSQL += " ADD COLUMN REMITENTE TEXT ";
            myCommand.CommandText = sSQL;
            myCommand.ExecuteNonQuery();


            myCommand.CommandText = "UPDATE APLICACION_VERSION SET NO_VERSION='" + Application.ProductVersion + "'";
            myCommand.ExecuteNonQuery();

            myCommand.Connection.Close();
            oData.DestruirConexion();
        }

        private void actualizar_version_10031()
        {
            oData.CrearConexion();
            string sSQL = "";
            OleDbCommand myCommand = new OleDbCommand();
            myCommand.Connection = oData.oConn;

            //sSQL = " ALTER TABLE FACTURA_LINEA  ";
            //sSQL += " ALTER COLUMN DESCRIPCION MEMO ";
            //myCommand.CommandText = sSQL;
            //myCommand.ExecuteNonQuery();


            myCommand.CommandText = "UPDATE APLICACION_VERSION SET NO_VERSION='" + Application.ProductVersion + "'";
            myCommand.ExecuteNonQuery();

            myCommand.Connection.Close();
            oData.DestruirConexion();
        }

        private void actualizar_version_10032()
        {
            oData.CrearConexion();
            string sSQL = "";
            OleDbCommand myCommand = new OleDbCommand();
            myCommand.Connection = oData.oConn;

            sSQL = " UPDATE FACTURA  SET FECHA_CANCELADO=FECHA_CREACION ";
            sSQL += " WHERE CANCELADO='True' AND FECHA_CANCELADO IS NULL ";
            myCommand.CommandText = sSQL;
            myCommand.ExecuteNonQuery();


            myCommand.CommandText = "UPDATE APLICACION_VERSION SET NO_VERSION='" + Application.ProductVersion + "'";
            myCommand.ExecuteNonQuery();

            myCommand.Connection.Close();
            oData.DestruirConexion();
        }

        private void actualizar_version_10033()
        {
            oData.CrearConexion();
            string sSQL = "";
            OleDbCommand myCommand = new OleDbCommand();
            myCommand.Connection = oData.oConn;

            sSQL = " UPDATE IMPUESTO  SET RETENCION='False' ";
            sSQL += " WHERE RETENCION='' ";
            myCommand.CommandText = sSQL;
            myCommand.ExecuteNonQuery();


            myCommand.CommandText = "UPDATE APLICACION_VERSION SET NO_VERSION='" + Application.ProductVersion + "'";
            myCommand.ExecuteNonQuery();

            myCommand.Connection.Close();
            oData.DestruirConexion();
        }

        private void actualizar_version_10034()
        {
            oData.CrearConexion();
            string sSQL = "";
            OleDbCommand myCommand = new OleDbCommand();
            myCommand.Connection = oData.oConn;

            myCommand.CommandText = "UPDATE APLICACION_VERSION SET NO_VERSION='" + Application.ProductVersion + "'";
            myCommand.ExecuteNonQuery();

            myCommand.Connection.Close();
            oData.DestruirConexion();
        }

        private void actualizar_version_10040()
        {
            oData.CrearConexion();
            OleDbCommand myCommand = new OleDbCommand();
            myCommand.Connection = oData.oConn;

            string sSQL = "";

            sSQL = "";
            sSQL += "CREATE TABLE CFDI_USUARIO  ";
            sSQL += " (ROW_ID AUTOINCREMENT, [USUARIO] TEXT, [PASSWORD] TEXT, [TIPO]  TEXT) ";
            myCommand.CommandText = sSQL;
            myCommand.ExecuteNonQuery();

            myCommand.CommandText = "UPDATE APLICACION_VERSION SET NO_VERSION='" + Application.ProductVersion + "'";
            myCommand.ExecuteNonQuery();

            myCommand.Connection.Close();
            oData.DestruirConexion();
        }

    }
}
