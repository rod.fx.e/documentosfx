﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Windows.Forms;
using System.Globalization;
using DevComponents.DotNetBar;
using System.Configuration;
using CFDI32;
using Generales;
using ModeloDocumentosFX;
using cce11_v32;

namespace FE_FX
{
    public class cFACTURA
    {
        private string sSQL = "";
        public bool anticipo = false;
        public decimal anticipoVAT_AMOUNT = 0;
        public decimal anticipoAMOUNT = 0;

        public bool errorValidacion = false;
        public string errorMensaje = "";

        public cLINEA[] LINEAS;

        public List<cLINEA> lineasConRetencion;

        public string INVOICE_ID = "";
        public DateTime INVOICE_DATE = new DateTime();
        public DateTime CREATE_DATE = new DateTime();
        public string STATUS = "";
        public decimal DESCUENTO_TOTAL = 0;
        public string TYPE = "";
        public string FE = "";
        public string CREATE_DATE_FE = "";
        public string CUSTOMER_ID = "";
        public string CUSTOMER_PO_REF = "";
        public string VAT_REGISTRATION = "";
        public string BILL_TO_NAME = "";
        public string BILL_TO_ADDR_1 = "";
        public string BILL_TO_ADDR_2 = "";
        public string BILL_TO_ADDR_3 = "";
        public string BILL_TO_ADDR_4 = "";
        public string BILL_TO_CITY = "";
        public string BILL_TO_STATE = "";
        public string BILL_TO_COUNTRY = "";
        public string BILL_TO_ZIPCODE = "";


        public string NAME = "";
        public string ADDR_1 = "";
        public string ADDR_2 = "";
        public string ADDR_3 = "";
        public string ADDR_4 = "";
        public string CITY = "";
        public string ZIPCODE = "";



        public string PDF = "";
        public string SERIE = "";
        public string FORMATO = "";
        public string SELLO = "";
        public string XML = "";
        public string CURRENCY_ID = "";
        public string CONTACT_EMAIL = "";
        public string CONTACT_SALUTATION = "";
        public string CONTACT_POSITION = "";
        public string CONTACT_FIRST_NAME = "";
        public string TIPO = "";
        public string CONTACT_LAST_NAME = "";
        public string CONTACT_INITIAL = "";
        public string CONTACT_HONORIFIC = "";
        public string POSTING_DATE = "";
        public decimal TOTAL_AMOUNT;
        public decimal TOTAL_VAT_AMOUNT;
        public decimal TOTAL_RETENIDO;
        public decimal VAT_PERCENT;
        public decimal SELL_RATE;
        public string TERMS_NET_DAYS = "";
        public string FORMA_DE_PAGO = "";
        public string CUENTA_BANCARIA = "";
        public string PAGO = "";


        public string UUID { get; set; }
        public string FechaTimbrado { get; set; }
        public string noCertificadoSAT { get; set; }
        public string selloSAT { get; set; }
        public string Cadena_TFD { get; set; }
        public string QR_Code { get; set; }
        public string SERVICE_CHARGE_ID { get; set; }
    public string ESPECIFICACIONES { get; set; }

        public string ADDR_NO = "";
        public CFDI32.t_Ubicacion ot_Ubicacion_Receptor;

        private cCONEXCION oData = new cCONEXCION("");
        int valorUnitariodecimales = 4;
        public string VAT_REGISTRATION_DESTINO;
        public string NAME_DESTINO;

        public cFACTURA(cCONEXCION pData)
        {
            oData = pData;
            limpiar();
            //Valor por defecto del valorUnitario es de 2
            //Validar el XML 4 decimales

            try
            {
                valorUnitariodecimales = int.Parse(ConfigurationManager.AppSettings["valorUnitariodecimales"].ToString());
            }
            catch
            {

            }
        }

        public cFACTURA()
        {
            ModeloDocumentosFXContainer dbContext = new ModeloDocumentosFXContainer();
            try
            {
                string conection = dbContext.Database.Connection.ConnectionString;
                oData = new cCONEXCION(conection);
            }
            catch (Exception err)
            {
                ErrorFX.mostrar(err, false, true, "cCERTIFICADO - 71 - ");
            }
            limpiar();
        }

        public void limpiar()
        {
            INVOICE_ID = "";
            INVOICE_DATE = DateTime.Now;
            CREATE_DATE = DateTime.Now;
            STATUS = "";
            TYPE = "";
            FE = "";
            TIPO = "";
            CREATE_DATE_FE = "";
            CUSTOMER_ID = "";
            CUSTOMER_PO_REF = "";
            VAT_REGISTRATION = "";
            ESPECIFICACIONES = "";
            BILL_TO_NAME = "";
            BILL_TO_ADDR_1 = "";
            BILL_TO_ADDR_2 = "";
            BILL_TO_ADDR_3 = "";
            BILL_TO_ADDR_4 = "";
            BILL_TO_CITY = "";
            BILL_TO_STATE = "";
            BILL_TO_COUNTRY = "";
            BILL_TO_ZIPCODE = "";
            PDF = "";
            SERIE = "";
            FORMATO = "";
            SELLO = "";
            XML = "";
            TOTAL_AMOUNT = 0;
            TOTAL_RETENIDO = 0;
            CURRENCY_ID = "";
            POSTING_DATE = "";
            STATE = "";
            ot_Ubicacion_Receptor = new CFDI32.t_Ubicacion();
        }

        public DateTime traer_INVOICE_DATE(string INVOICE_IDp)
        {
            try
            {
                //Buscar la fecha de la factura
                sSQL = " SELECT ";
                sSQL += "   CAST(DAY(INVOICE_DATE) as varchar(2))+'-'+CAST(MONTH(INVOICE_DATE) as varchar(2))+'-'+CAST(YEAR(INVOICE_DATE) as varchar(4)) as FORMATO_INVOICE_DATE ";
                sSQL += " , DAY(INVOICE_DATE) as DIA_INVOICE_DATE ";
                sSQL += " , MONTH(INVOICE_DATE) as MES_INVOICE_DATE ";
                sSQL += " , YEAR(INVOICE_DATE)  as ANIO_INVOICE_DATE ";
                sSQL += " , CAST(DAY(CREATE_DATE) as varchar(2))+'-'+CAST(MONTH(CREATE_DATE) as varchar(2))+'-'+CAST(YEAR(CREATE_DATE) as varchar(4)) as FORMATO_CREATE_DATE ";
                sSQL += " , DAY(CREATE_DATE) as DIA_CREATE_DATE ";
                sSQL += " , MONTH(CREATE_DATE) as MES_CREATE_DATE ";
                sSQL += " , YEAR(CREATE_DATE)  as ANIO_CREATE_DATE ";
                sSQL += " , DATEPART(hour,CREATE_DATE) as HOUR_CREATE_DATE ";
                sSQL += " , DATEPART(minute,CREATE_DATE) as MIN_CREATE_DATE ";
                sSQL += " , DATEPART(second,CREATE_DATE)  as SECOND_CREATE_DATE ";
                sSQL += " FROM RECEIVABLE ";
                sSQL += " WHERE INVOICE_ID='" + INVOICE_IDp + "'";

                if (oData.sTipo == "ORACLE")
                {
                    sSQL = " SELECT ";
                    sSQL += "  TO_CHAR(INVOICE_DATE, 'DD-MM-YYYY') as FORMATO_INVOICE_DATE";
                    sSQL += " ,TO_CHAR(CREATE_DATE, 'DD-MM-YYYY') as FORMATO_CREATE_DATE";
                    sSQL += " ,TO_CHAR(INVOICE_DATE, 'DD') as DIA_INVOICE_DATE";
                    sSQL += " ,TO_CHAR(INVOICE_DATE, 'MM') as MES_INVOICE_DATE";
                    sSQL += " ,TO_CHAR(INVOICE_DATE, 'YYYY') as ANIO_INVOICE_DATE";
                    sSQL += " ,TO_CHAR(CREATE_DATE, 'DD')  as DIA_CREATE_DATE ";
                    sSQL += " ,TO_CHAR(CREATE_DATE, 'MM') as MES_CREATE_DATE ";
                    sSQL += " ,TO_CHAR(CREATE_DATE, 'YYYY') as ANIO_CREATE_DATE ";
                    sSQL += " ,TO_CHAR(CREATE_DATE, 'HH') as HOUR_CREATE_DATE ";
                    sSQL += " ,TO_CHAR(CREATE_DATE, 'MI') as MIN_CREATE_DATE ";
                    sSQL += " ,TO_CHAR(CREATE_DATE, 'SS') as SECOND_CREATE_DATE ";
                    sSQL += " FROM RECEIVABLE ";
                    sSQL += " WHERE INVOICE_ID='" + INVOICE_IDp + "'";
                }


                DateTime INVOICE_DATE_tr = new DateTime();
                DataTable oDataTable = oData.EjecutarConsulta(sSQL);
                if (oDataTable != null)
                {
                    foreach (DataRow oDataRow in oDataTable.Rows)
                    {
                        INVOICE_DATE_tr = new DateTime(int.Parse(oDataRow["ANIO_INVOICE_DATE"].ToString()), int.Parse(oDataRow["MES_INVOICE_DATE"].ToString()), int.Parse(oDataRow["DIA_INVOICE_DATE"].ToString())
                            , int.Parse(oDataRow["HOUR_CREATE_DATE"].ToString()), int.Parse(oDataRow["MIN_CREATE_DATE"].ToString()), int.Parse(oDataRow["SECOND_CREATE_DATE"].ToString())

                            );
                    }
                }
                return INVOICE_DATE_tr;
            }
            catch (Exception e)
            {
                MessageBox.Show("Error en traer_INVOICE_DATE: " + e.ToString());
            }
            return DateTime.Now;
        }

        public bool actualizar_fecha(string INVOICE_IDp, bool test)
        {
            string sSQL = "";
            if (test)
            {
                return true;
            }
            //Ejecutando el Trigger
            bool CAMBIO_AUTOMATICO_72 = false;
            try
            {
                CAMBIO_AUTOMATICO_72 = bool.Parse(ConfigurationManager.AppSettings["CAMBIO_AUTOMATICO_72"].ToString());
            }
            catch
            {

            }
            if (CAMBIO_AUTOMATICO_72)
            {
                if (oData.sTipo == "SQLSERVER")
                {
                    sSQL = " SELECT TOP 1 DATEDIFF(hh,INVOICE_DATE,GETDATE()) as Diferencia";
                    sSQL += " FROM RECEIVABLE ";
                    sSQL += " WHERE INVOICE_ID='" + INVOICE_IDp + "' AND DATEDIFF(hh,INVOICE_DATE,GETDATE())>72";

                    DataTable oDataTable = oData.EjecutarConsulta(sSQL);
                    if (oDataTable != null)
                    {
                        foreach (DataRow oDataRow in oDataTable.Rows)
                        {

                            DialogResult Resultado = MessageBox.Show("La fecha ha sobrepasado las 72 horas no puede ser cambiada, desea cambiarla a la fecha actual?", Application.ProductName + "-" + Application.ProductVersion, MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                            if (Resultado.ToString() == "Yes")
                            {
                                DateTime Ahora = new DateTime();
                                Ahora = DateTime.Now;
                                sSQL = " UPDATE RECEIVABLE SET INVOICE_DATE = " + oData.convertir_fecha_general(Ahora, "i") + ", ";
                                sSQL += " CREATE_DATE = GETDATE(),  ";
                                sSQL += " PRINTED_DATE = GETDATE()  ";
                                sSQL += " WHERE INVOICE_ID='" + INVOICE_IDp + "'";
                                oData.EjecutarConsulta(sSQL);
                                return true;
                            }
                            else
                            {
                                return false;
                            }
                        }
                    }
                }
            }
            return true;
        }

        public DateTime traer_CREATE_DATE(string INVOICE_IDp)
        {


            //Buscar la fecha de la factura
            string sSQL = " SELECT ";
            sSQL = " SELECT ";
            sSQL += "   CAST(DAY(INVOICE_DATE) as varchar(2))+'-'+CAST(MONTH(INVOICE_DATE) as varchar(2))+'-'+CAST(YEAR(INVOICE_DATE) as varchar(4)) as FORMATO_INVOICE_DATE ";
            sSQL += " , DAY(INVOICE_DATE) as DIA_INVOICE_DATE ";
            sSQL += " , MONTH(INVOICE_DATE) as MES_INVOICE_DATE ";
            sSQL += " , YEAR(INVOICE_DATE)  as ANIO_INVOICE_DATE ";
            sSQL += " , CAST(DAY(CREATE_DATE) as varchar(2))+'-'+CAST(MONTH(CREATE_DATE) as varchar(2))+'-'+CAST(YEAR(CREATE_DATE) as varchar(4)) as FORMATO_CREATE_DATE ";
            sSQL += " , DAY(CREATE_DATE) as DIA_CREATE_DATE ";
            sSQL += " , MONTH(CREATE_DATE) as MES_CREATE_DATE ";
            sSQL += " , YEAR(CREATE_DATE)  as ANIO_CREATE_DATE ";
            sSQL += " , DATEPART(hour,CREATE_DATE) as HOUR_CREATE_DATE ";
            sSQL += " , DATEPART(minute,CREATE_DATE) as MIN_CREATE_DATE ";
            sSQL += " , DATEPART(second,CREATE_DATE)  as SECOND_CREATE_DATE ";
            sSQL += " FROM RECEIVABLE ";
            sSQL += " WHERE INVOICE_ID='" + INVOICE_IDp + "'";

            if (oData.sTipo == "ORACLE")
            {
                sSQL = " SELECT ";
                sSQL += "  TO_CHAR(INVOICE_DATE, 'DD-MM-YYYY') as FORMATO_INVOICE_DATE";
                sSQL += " ,TO_CHAR(CREATE_DATE, 'DD-MM-YYYY') as FORMATO_CREATE_DATE";
                sSQL += " ,TO_CHAR(INVOICE_DATE, 'DD') as DIA_INVOICE_DATE";
                sSQL += " ,TO_CHAR(INVOICE_DATE, 'MM') as MES_INVOICE_DATE";
                sSQL += " ,TO_CHAR(INVOICE_DATE, 'YYYY') as ANIO_INVOICE_DATE";
                sSQL += " ,TO_CHAR(CREATE_DATE, 'DD')  as DIA_CREATE_DATE ";
                sSQL += " ,TO_CHAR(CREATE_DATE, 'MM') as MES_CREATE_DATE ";
                sSQL += " ,TO_CHAR(CREATE_DATE, 'YYYY') as ANIO_CREATE_DATE ";
                sSQL += " ,TO_CHAR(CREATE_DATE, 'HH') as HOUR_CREATE_DATE ";
                sSQL += " ,TO_CHAR(CREATE_DATE, 'MI') as MIN_CREATE_DATE ";
                sSQL += " ,TO_CHAR(CREATE_DATE, 'SS') as SECOND_CREATE_DATE ";
                sSQL += " FROM RECEIVABLE ";
                sSQL += " WHERE INVOICE_ID='" + INVOICE_IDp + "'";
            }


            DateTime CREATE_DATE_tr = new DateTime();
            DataTable oDataTable = oData.EjecutarConsulta(sSQL);
            if (oDataTable != null)
            {
                foreach (DataRow oDataRow in oDataTable.Rows)
                {
                    CREATE_DATE_tr = new DateTime(int.Parse(oDataRow["ANIO_CREATE_DATE"].ToString()), int.Parse(oDataRow["MES_CREATE_DATE"].ToString()), int.Parse(oDataRow["DIA_CREATE_DATE"].ToString())
                        , int.Parse(oDataRow["HOUR_CREATE_DATE"].ToString()), int.Parse(oDataRow["MIN_CREATE_DATE"].ToString()), int.Parse(oDataRow["SECOND_CREATE_DATE"].ToString())
                        );
                }
            }
            return CREATE_DATE_tr;

        }
        private string cargar_bd_auxiliar(cEMPRESA ocEMPRESA)
        {
            string BD_Auxiliar = ocEMPRESA.BD_AUXILIAR + ".dbo.";
            if (ocEMPRESA.BD_AUXILIAR == "")
            {
                BD_Auxiliar = "";
            }
            return BD_Auxiliar;
        }

        public void datos(string pINVOICE_ID, string pACCOUNT_ID, string VERIFICAR, cEMPRESA ocEMPRESA
            , string pACCOUNT_ID_ANTICIPO)
        {
            try
            {
                limpiar();
                //Rodrigo Escalona 31/01/2020 Buscar los de retencions y quitarlos
                string SERIE_tr = pINVOICE_ID.Replace("0", "").Replace("1", "").Replace("2", "").Replace("3", "").Replace("4", "").Replace("5", "").Replace("6", "").Replace("7", "").Replace("8", "").Replace("9", "").Trim();
                cSERIE ocSERIE = new cSERIE();
                if (ocSERIE.cargar_serie(SERIE_tr, ocEMPRESA.ROW_ID))
                {
                    pACCOUNT_ID = ocSERIE.ACCOUNT_ID_RETENIDO;
                }

                INVOICE_ID = pINVOICE_ID;
                errorValidacion = false;
                errorMensaje = "";
                string BD_Auxiliar = cargar_bd_auxiliar(ocEMPRESA);

                sSQL += " SELECT *  FROM RECEIVABLE ";
                sSQL += " WHERE INVOICE_ID='" + pINVOICE_ID + "'";

                DataTable oDataTable = oData.EjecutarConsulta(sSQL);
                if (oDataTable != null)
                {
                    foreach (DataRow oDataRow in oDataTable.Rows)
                    {
                        INVOICE_ID = oDataRow["INVOICE_ID"].ToString().ToUpper();

                        //INVOICE_DATE = new DateTime(int.Parse(oDataRow["ANIO_INVOICE_DATE"].ToString()), int.Parse(oDataRow["MES_INVOICE_DATE"].ToString()), int.Parse(oDataRow["DIA_INVOICE_DATE"].ToString())
                        //    , int.Parse(oDataRow["HOUR_CREATE_DATE"].ToString()), int.Parse(oDataRow["MIN_CREATE_DATE"].ToString()), int.Parse(oDataRow["SECOND_CREATE_DATE"].ToString())

                        //    );

                        //CREATE_DATE = new DateTime(int.Parse(oDataRow["ANIO_CREATE_DATE"].ToString()), int.Parse(oDataRow["MES_CREATE_DATE"].ToString()), int.Parse(oDataRow["DIA_CREATE_DATE"].ToString())
                        //    , int.Parse(oDataRow["HOUR_CREATE_DATE"].ToString()), int.Parse(oDataRow["MIN_CREATE_DATE"].ToString()), int.Parse(oDataRow["SECOND_CREATE_DATE"].ToString())
                        //    );
                        INVOICE_DATE = traer_INVOICE_DATE(INVOICE_ID);
                        CREATE_DATE = traer_CREATE_DATE(INVOICE_ID);

                        TERMS_NET_DAYS = oDataRow["TERMS_DESCRIPTION"].ToString();  // oDataRow["TERMS_NET_DAYS"].ToString().ToUpper();
                        CUSTOMER_ID = oDataRow["CUSTOMER_ID"].ToString().ToUpper();
                        TOTAL_AMOUNT = decimal.Parse(oDataRow["TOTAL_AMOUNT"].ToString());
                        SELL_RATE = decimal.Parse(oDataRow["SELL_RATE"].ToString());
                        CURRENCY_ID = oDataRow["CURRENCY_ID"].ToString().ToUpper();
                        STATUS = oDataRow["STATUS"].ToString().ToUpper();
                        TYPE = oDataRow["TYPE"].ToString().ToUpper();

                        switch (TYPE)
                        {
                            case "I":
                                TIPO = "FACTURA";
                                break;
                            case "M":
                                TIPO = "NOTA DE CREDITO";
                                break;
                            default:
                                TIPO = "NO DEFINIDO";
                                break;
                        }
                        POSTING_DATE = oDataRow["POSTING_DATE"].ToString().ToUpper();

                        //Cargar los datos de facturación
                        sSQL = " SELECT * ";
                        sSQL += " FROM CUSTOMER ";
                        sSQL += " WHERE ID='" + CUSTOMER_ID + "'";
                        DataTable oDataTable1 = oData.EjecutarConsulta(sSQL);
                        if (oDataTable1 != null)
                        {
                            foreach (DataRow oDataRow1 in oDataTable1.Rows)
                            {
                                VAT_REGISTRATION = oDataRow1["VAT_REGISTRATION"].ToString().ToUpper().Trim();
                                TAX_ID_NUMBER = oDataRow1["TAX_ID_NUMBER"].ToString().ToUpper().Trim();

                                BILL_TO_NAME = oDataRow1["BILL_TO_NAME"].ToString().ToUpper();
                                BILL_TO_ADDR_1 = oDataRow1["BILL_TO_ADDR_1"].ToString().ToUpper();
                                BILL_TO_ADDR_2 = oDataRow1["BILL_TO_ADDR_2"].ToString().ToUpper();
                                BILL_TO_ADDR_3 = oDataRow1["BILL_TO_ADDR_3"].ToString().ToUpper();
                                BILL_TO_CITY = oDataRow1["BILL_TO_CITY"].ToString().ToUpper();
                                BILL_TO_STATE = oDataRow1["BILL_TO_STATE"].ToString().ToUpper();
                                BILL_TO_COUNTRY = oDataRow1["BILL_TO_COUNTRY"].ToString().ToUpper();
                                BILL_TO_ZIPCODE = oDataRow1["BILL_TO_ZIPCODE"].ToString().ToUpper().Trim();

                                NAME = oDataRow1["NAME"].ToString().ToUpper();
                                ADDR_1 = oDataRow1["ADDR_1"].ToString().ToUpper();
                                ADDR_2 = oDataRow1["ADDR_2"].ToString().ToUpper();
                                ADDR_3 = oDataRow1["ADDR_3"].ToString().ToUpper();
                                CITY = oDataRow1["CITY"].ToString().ToUpper();
                                STATE = oDataRow1["STATE"].ToString().ToUpper();
                                COUNTRY = oDataRow1["COUNTRY"].ToString().ToUpper();
                                STATE = oDataRow1["STATE"].ToString().ToUpper();
                                ZIPCODE = oDataRow1["ZIPCODE"].ToString().ToUpper();

                                if (BILL_TO_COUNTRY.Trim().Length == 0)
                                {
                                    BILL_TO_COUNTRY = COUNTRY;
                                }
                                if (BILL_TO_NAME.Trim().Length == 0)
                                {
                                    BILL_TO_NAME = NAME;
                                }

                                //}
                                //Validar el COUNTRY
                                //COUNTRY = oDataRow1["COUNTRY"].ToString().ToUpper();

                                //Complemento del Nombre Completo
                                //Rodrigo Escalona:
                                //Usar complementos ? si o no?
                                //bool usarComplemento = false;
                                //try
                                //{
                                //    usarComplemento = bool.Parse(ConfigurationManager.AppSettings["usarComplemento"].ToString());
                                //}
                                //catch { }
                                //if (usarComplemento)
                                //{
                                //    BILL_TO_NAME = BILL_TO_NAME + BILL_TO_ADDR_3;
                                //}

                                //Datos del Correo
                                CONTACT_EMAIL = oDataRow1["CONTACT_EMAIL"].ToString();

                                if (CONTACT_EMAIL.Length > 0)
                                {
                                    if (CONTACT_EMAIL.Substring(0, 1) == ";")
                                    {
                                        CONTACT_EMAIL = CONTACT_EMAIL.Replace(";", "");
                                    }
                                    else
                                    {
                                        CONTACT_EMAIL = "";
                                    }
                                }

                                CONTACT_SALUTATION = oDataRow1["CONTACT_SALUTATION"].ToString();
                                CONTACT_POSITION = oDataRow1["CONTACT_POSITION"].ToString();
                                CONTACT_FIRST_NAME = oDataRow1["CONTACT_FIRST_NAME"].ToString();
                                CONTACT_LAST_NAME = oDataRow1["CONTACT_LAST_NAME"].ToString();
                                CONTACT_INITIAL = oDataRow1["CONTACT_INITIAL"].ToString();
                                CONTACT_POSITION = oDataRow1["CONTACT_POSITION"].ToString();
                                CONTACT_HONORIFIC = oDataRow1["CONTACT_HONORIFIC"].ToString();
                                CONTACT_SALUTATION = oDataRow1["CONTACT_SALUTATION"].ToString();

                                //if(BILL_TO_COUNTRY.ToUpper().Trim()!="MEXICO")
                                //{
                                //    VAT_REGISTRATION = "XEXX010101000";
                                //}   

                                //Cargar complento de Comercio Exterior 
                                FREE_ON_BOARD = cce11_v32.c_INCOTERM.EXW;
                                try
                                {
                                    if (!String.IsNullOrEmpty(oDataRow1["FREE_ON_BOARD"].ToString()))
                                    {
                                        if (ocEMPRESA.ENTITY_ID.Contains("FIMA"))
                                        {
                                            cce11_v32.c_INCOTERM oc_INCOTERM = (c_INCOTERM)System.Enum.Parse(typeof(c_INCOTERM), oDataRow1["FREE_ON_BOARD"].ToString().Substring(0, 3));
                                            FREE_ON_BOARD = oc_INCOTERM;
                                        }
                                        else
                                        {
                                            //Validar si tiene un -
                                            //EXW
                                            string incotermtr = oDataRow1["FREE_ON_BOARD"].ToString().Substring(0, 3);
                                            if (incotermtr.Contains("EX"))
                                            {
                                                FREE_ON_BOARD = cce11_v32.c_INCOTERM.EXW;
                                            }
                                            else
                                            {
                                                if (incotermtr.Contains("DEL"))
                                                {
                                                    //cce11_v32.c_INCOTERM oc_INCOTERM = (c_INCOTERM)System.Enum.Parse(typeof(c_INCOTERM), oDataRow1["FREE_ON_BOARD"].ToString().Substring(0, 3));
                                                    //FREE_ON_BOARD = oc_INCOTERM;
                                                    FREE_ON_BOARD = cce11_v32.c_INCOTERM.EXW;
                                                }
                                                else
                                                {
                                                    cce11_v32.c_INCOTERM oc_INCOTERM = (c_INCOTERM)System.Enum.Parse(typeof(c_INCOTERM), oDataRow1["FREE_ON_BOARD"].ToString().Substring(0, 3));
                                                    FREE_ON_BOARD = oc_INCOTERM;
                                                }

                                            }
                                        }
                                    }
                                }
                                catch (Exception errorINCOTERM)
                                {
                                    ErrorFX.mostrar("Factura: " + INVOICE_ID + " cFACTURA 490 Error Inconterm " + errorINCOTERM.Message.ToString(), false, false, false);
                                }
                            }
                        }
                        cargar_direccion_entrega(INVOICE_ID, ocEMPRESA);
                        //Cliente
                        if (ADDR_NO != "")
                        {
                            //Cargar la ADDR_NO 
                            sSQL = " SELECT * ";
                            sSQL += " FROM CUST_ADDRESS ";
                            sSQL += " WHERE CUSTOMER_ID='" + CUSTOMER_ID + "' AND ADDR_NO=" + ADDR_NO;
                            DataTable oDataTableADDR_NO = oData.EjecutarConsulta(sSQL);
                            if (oDataTableADDR_NO != null)
                            {
                                foreach (DataRow oDataRow1 in oDataTableADDR_NO.Rows)
                                {

                                    //VAT_REGISTRATION = oDataRow1["VAT_REGISTRATION"].ToString().ToUpper().Trim();
                                    //Rodrigo Escalona 28/12/2020 
                                    if (!ocEMPRESA.RAZON_SOLO_CLIENTE)
                                    {
                                        NAME = oDataRow1["NAME"].ToString().ToUpper();
                                        ADDR_1 = oDataRow1["ADDR_1"].ToString().ToUpper();
                                        ADDR_2 = " " + oDataRow1["ADDR_2"].ToString().ToUpper();
                                        ADDR_3 = " " + oDataRow1["ADDR_3"].ToString().ToUpper();
                                        CITY = oDataRow1["CITY"].ToString().ToUpper();
                                        STATE = oDataRow1["STATE"].ToString().ToUpper();
                                        COUNTRY = oDataRow1["COUNTRY"].ToString().ToUpper();
                                        ZIPCODE = oDataRow1["ZIPCODE"].ToString().ToUpper().Trim();
                                    }

                                    VAT_REGISTRATION_DESTINO = oDataRow1["VAT_REGISTRATION"].ToString().ToUpper().Trim();

                                    //NAME_DESTINO = oDataRow1["NAME"].ToString().ToUpper().Trim();
                                    //Cargar complento de Comercio Exterior 
                                    FREE_ON_BOARD = cce11_v32.c_INCOTERM.EXW;
                                    //FREE_ON_BOARD = cce11_v32.c_INCOTERM.EXW;
                                    try
                                    {
                                        if (!String.IsNullOrEmpty(oDataRow1["FREE_ON_BOARD"].ToString()))
                                        {
                                            if (ocEMPRESA.ENTITY_ID.Contains("FIMA"))
                                            {
                                                cce11_v32.c_INCOTERM oc_INCOTERM = (c_INCOTERM)System.Enum.Parse(typeof(c_INCOTERM), oDataRow1["FREE_ON_BOARD"].ToString().Substring(0, 3));
                                                FREE_ON_BOARD = oc_INCOTERM;
                                            }
                                            else
                                            {
                                                //Caso Bradford
                                                //Se quedo en correción
                                                if (ocEMPRESA.ENTITY_ID.Contains("FIMA"))
                                                {

                                                }
                                                else
                                                {
                                                    cce11_v32.c_INCOTERM oc_INCOTERM = (c_INCOTERM)System.Enum.Parse(typeof(c_INCOTERM), oDataRow1["FREE_ON_BOARD"].ToString().Substring(0, 3));
                                                    FREE_ON_BOARD = oc_INCOTERM;
                                                }

                                            }
                                        }
                                    }
                                    catch (Exception errorINCOTERM)
                                    {
                                        if (!oDataRow1["FREE_ON_BOARD"].ToString().Contains("EX-"))
                                        {
                                            ErrorFX.mostrar("Factura: " + INVOICE_ID + " cFACTURA 542 Error Inconterm " + errorINCOTERM.Message.ToString(), false,false, false);

                                        }
                                    }

                                }
                            }
                        }
                        //Buscar Contactos del Cliente
                        sSQL = "SELECT CONTACT_EMAIL ";
                        sSQL += "FROM CUSTOMER_CONTACT ";
                        sSQL += "WHERE CUSTOMER_ID = '" + CUSTOMER_ID + "' AND CONTACT_EMAIL LIKE ';%'";
                        oDataTable1 = oData.EjecutarConsulta(sSQL);
                        foreach (DataRow oDataRow1 in oDataTable1.Rows)
                        {
                            CONTACT_EMAIL += ";" + oDataRow1["CONTACT_EMAIL"].ToString().Replace(";", "");
                        }


                        //Buscar las Lineas
                        generar_lineas(pACCOUNT_ID, ocEMPRESA, pACCOUNT_ID_ANTICIPO);
                        //////Cargar las Lineas divididas con Retencion
                        ////if (cargarRetencionLinea)
                        ////{
                        ////    cargarLinasRentencion(pACCOUNT_ID, ocEMPRESA, pACCOUNT_ID_ANTICIPO);
                        ////}
                        

                        //MessageBox.Show("Cargar Banco");
                        cargar_banco(ocEMPRESA);
                        //MessageBox.Show("Cargar retenido");
                        generar_retenido(pACCOUNT_ID, ocEMPRESA);

                        SERIE = INVOICE_ID.Replace("0", "").Replace("1", "").Replace("2", "").Replace("3", "").Replace("4", "").Replace("5", "").Replace("6", "").Replace("7", "").Replace("8", "").Replace("9", "").Trim();
                        //MessageBox.Show("La Serie es:" + SERIE);

                        //Rodrigo Escalona
                        //Cargar las especificaciones de la factura
                        //Para ser cargadas en complementos
                        //17/02/2015
                        if (oData.sTipo != "ORACLE")
                        {
                            cargar_especificaciones();
                        }

                    }
                }
            }
            catch (Exception err)
            {
                MessageBox.Show("Error en carga de datos de Facturas: " + err.ToString());
            }

        }

        //private void cargarLinasRentencion(string pACCOUNT_ID, cEMPRESA ocEMPRESA, string pACCOUNT_ID_ANTICIPO)
        //{
        //    try
        //    {
        //        //Separar por ,
        //        string[] cuentas = pACCOUNT_ID.Split(',');

        //        lineasConRetencion = new List<cLINEA>();
        //        string BD_Auxiliar = cargar_bd_auxiliar(ocEMPRESA);
        //        //Lineas sin Iva Retenido
        //        string sSQL = @" SELECT rl.*, r.TYPE
        //        FROM RECEIVABLE_LINE rl
        //        INNER JOIN RECEIVABLE r ON r.INVOICE_ID=rl.INVOICE_ID
        //        WHERE rl.INVOICE_ID = '" + INVOICE_ID + @"' 
        //        ORDER BY LINE_NO ";
        //        DataTable oDataTable = oData.EjecutarConsulta(sSQL);
        //        foreach (DataRow oDataRow in oDataTable.Rows)
        //        {
        //            //Cargar y validar la cantidad
        //            if (cuentas.Contains(oDataRow["GL_ACCOUNT_ID"].ToString()))
        //            {
        //                //Agregar a la linea actual la retención
        //            }
        //            else
        //            {
        //                //Agregar la información de la linea
        //                string QTY = oData.IsNullNumero(oDataRow["QTY"].ToString());
        //                if (QTY.Trim() == "")
        //                {
        //                    QTY = "1";
        //                }
        //                if (decimal.Parse(QTY) != 0)
        //                {
        //                    //AMOUNT
        //                    string AMOUNT = oData.IsNullNumero(oDataRow["AMOUNT"].ToString());
        //                    //VAT_AMOUNT
        //                    string VAT_AMOUNT = "0";
        //                    if (!String.IsNullOrEmpty(oDataRow["VAT_AMOUNT"].ToString()))
        //                    {
        //                        VAT_AMOUNT = oData.IsNullNumero(oDataRow["VAT_AMOUNT"].ToString());
        //                    }
        //                    string VAT_AMOUNT_ln = oData.IsNullNumero(oDataRow["VAT_AMOUNT"].ToString());
        //                    string VAT_PERCENT_ln = oData.IsNullNumero(oDataRow["VAT_PERCENT"].ToString());

        //                }
        //            }
        //        }


        //    } 
        //    catch (Exception error)
        //    {
        //        ErrorFX.mostrar(error, true, true, "cargarLinasRentencion 1394 ", false);
        //    }
        //}

        /**/
        public decimal tipoCambio(string moneda, DateTime fecha, cCONEXCION oDatap)
        {
            try
            {

                string fecha_tr = oDatap.convertir_fecha(fecha, oDatap.sTipo);
                sSQL = @" SELECT SELL_RATE FROM CURRENCY_EXCHANGE WHERE CURRENCY_ID='" + moneda + "' AND EFFECTIVE_DATE<=" + fecha_tr + " ORDER BY EFFECTIVE_DATE DESC";
                BitacoraFX.Log(sSQL);
                DataTable oDataTableR = oData.EjecutarConsulta(sSQL);
                if (oDataTableR != null)
                {
                    foreach (DataRow oDataRow1 in oDataTableR.Rows)
                    {
                        return decimal.Parse(oDataRow1["SELL_RATE"].ToString());
                    }
                }
                return 1;
            }
            catch
            {
                return 0;
            }
        }
        public string CargarNotas()
        {
            string Notas = string.Empty;
            try
            {
                
                string Sql= @"
                SELECT CAST(CAST([NOTE] AS VARBINARY(MAX)) AS VARCHAR(MAX)) AS Notas
                FROM NOTATION
                WHERE [TYPE]='RI' AND OWNER_ID='" + INVOICE_ID + "' ";
                DataTable oDataTableR = oData.EjecutarConsulta(Sql);
                if (oDataTableR != null)
                {
                    foreach (DataRow oDataRow1 in oDataTableR.Rows)
                    {
                        Notas = oDataRow1["Notas"].ToString();
                    }
                }

                if (IsChinese(ESPECIFICACIONES))
                {
                    Sql = @"
                    SELECT CAST(CAST([NOTE] AS VARBINARY(MAX)) AS VARCHAR(MAX)) AS Notas
                    FROM NOTATION
                    WHERE [TYPE]='RI' AND OWNER_ID='" + INVOICE_ID + "' ";
                    oDataTableR = oData.EjecutarConsulta(Sql);
                    if (oDataTableR != null)
                    {
                        foreach (DataRow oDataRow1 in oDataTableR.Rows)
                        {
                            Notas = oDataRow1["Notas"].ToString();
                        }
                    }
                }
            }
            catch (Exception error)
            {
                ErrorFX.mostrar(error, true, true, "cFACTURA 783 ", false);
            }
        
            return Notas;
        }
        public void cargar_especificaciones()
        {
            try
            {
                sSQL = " SELECT CAST(CAST(BITS AS VARBINARY(MAX)) AS NVARCHAR(MAX)) AS Especificacion ";
                sSQL += " FROM [RECEIVABLE_BINARY]  ";
                sSQL += " WHERE INVOICE_ID='" + INVOICE_ID + "' ";
                DataTable oDataTableR = oData.EjecutarConsulta(sSQL);
                if (oDataTableR != null)
                {
                    foreach (DataRow oDataRow1 in oDataTableR.Rows)
                    {
                        ESPECIFICACIONES = oDataRow1["Especificacion"].ToString();
                    }
                }

                if (IsChinese(ESPECIFICACIONES))
                {
                    sSQL = " SELECT CAST(CAST(BITS AS VARBINARY(MAX)) AS VARCHAR(MAX)) AS Especificacion ";
                    sSQL += " FROM [RECEIVABLE_BINARY]  ";
                    sSQL += " WHERE INVOICE_ID='" + INVOICE_ID + "' ";
                    oDataTableR = oData.EjecutarConsulta(sSQL);
                    if (oDataTableR != null)
                    {
                        foreach (DataRow oDataRow1 in oDataTableR.Rows)
                        {
                            ESPECIFICACIONES = oDataRow1["Especificacion"].ToString();
                        }
                    }
                }


            }
            catch
            {

            }
        }


        public bool IsChinese(string text)
        {
            return text.Any(c => (uint)c >= 0x4E00 && (uint)c <= 0x2FA1F);
        }


        public void cargar_banco(cEMPRESA ocEMPRESA)
        {
            //Buscar el Primer Pedido de la Factura
            //Del Pedido Buscar el BANK_ID
            sSQL = " SELECT * ";
            sSQL += " FROM CUSTOMER_BANK";
            sSQL += " WHERE CUSTOMER_ID = '" + CUSTOMER_ID + "' AND IS_DEFAULT=1";
            DataTable oDataTableR = oData.EjecutarConsulta(sSQL);
            if (oDataTableR != null)
            {
                foreach (DataRow oDataRow1 in oDataTableR.Rows)
                {
                    CUENTA_BANCARIA = oDataRow1["ACCOUNT_NO"].ToString();
                    PAGO = oDataRow1["IBAN"].ToString();
                    FORMA_DE_PAGO = oDataRow1["SWIFT"].ToString();
                }
            }
            string sSQL_TOP = "TOP 1";
            string sSQL_WHERE_row = "";
            if (oData.sTipo == "ORACLE")
            {
                sSQL_TOP = "";
                sSQL_WHERE_row = " AND rownum < 1";
            }

            //Cargar de la Primera Linea
            sSQL = @" SELECT " + sSQL_TOP + @" 
                (SELECT " + sSQL_TOP + " CUSTOMER_ORDER.CUST_BANK_ID FROM CUSTOMER_ORDER WHERE CUSTOMER_ORDER.ID=RECEIVABLE_LINE.CUST_ORDER_ID "
                + sSQL_WHERE_row + @") as CUST_BANK_ID ";
            sSQL += " FROM RECEIVABLE_LINE ";
            sSQL += " WHERE RECEIVABLE_LINE.INVOICE_ID = '" + INVOICE_ID + "' AND NOT(RECEIVABLE_LINE.CUST_ORDER_ID IS NULL) " + sSQL_WHERE_row;


            oDataTableR = oData.EjecutarConsulta(sSQL);
            if (oDataTableR != null)
            {
                foreach (DataRow oDataRow1 in oDataTableR.Rows)
                {
                    string CUST_BANK_ID = oDataRow1["CUST_BANK_ID"].ToString();
                    sSQL = " SELECT * ";
                    sSQL += " FROM CUSTOMER_BANK";
                    sSQL += " WHERE CUSTOMER_ID = '" + CUSTOMER_ID + "' AND BANK_ID = '" + CUST_BANK_ID + "'";
                    DataTable oDataTableCUSTOMER_BANK = oData.EjecutarConsulta(sSQL);
                    if (oDataTableCUSTOMER_BANK != null)
                    {
                        foreach (DataRow oDataRowCUSTOMER_BANK in oDataTableCUSTOMER_BANK.Rows)
                        {
                            CUENTA_BANCARIA = oDataRowCUSTOMER_BANK["ACCOUNT_NO"].ToString();
                            PAGO = oDataRowCUSTOMER_BANK["IBAN"].ToString();
                            FORMA_DE_PAGO = oDataRowCUSTOMER_BANK["SWIFT"].ToString();
                        }
                    }
                }
            }
            if (TYPE == "M")
            {
                CUENTA_BANCARIA = "NO IDENTIFICADO";
                //CUENTA_BANCARIA = "NA";
                //PAGO = "NO IDENTIFICADO";
                //PAGO = "NO IDENTIFICADO";
                //PAGO = "98";
            }

            cFACTURA_BANDEJA ocFACTURA_BANDEJA = new cFACTURA_BANDEJA(oData, ocEMPRESA);
            ocFACTURA_BANDEJA.INVOICE_ID = INVOICE_ID;
            if (ocFACTURA_BANDEJA.cargar_banco())
            {
                if (ocFACTURA_BANDEJA.METODO_DE_PAGO != "")
                {
                    CUENTA_BANCARIA = ocFACTURA_BANDEJA.CTA_BANCO;
                }
                if (ocFACTURA_BANDEJA.CTA_BANCO != "")
                {
                    PAGO = ocFACTURA_BANDEJA.METODO_DE_PAGO;
                }
                if (ocFACTURA_BANDEJA.FORMA_DE_PAGO != "")
                {
                    FORMA_DE_PAGO = ocFACTURA_BANDEJA.FORMA_DE_PAGO;
                }
            }

            if (CUENTA_BANCARIA == "")
            {
                CUENTA_BANCARIA = "NO IDENTIFICADO";
                CUENTA_BANCARIA = "0000";
            }
            if (FORMA_DE_PAGO == "")
            {
                FORMA_DE_PAGO = "NO IDENTIFICADO";
                FORMA_DE_PAGO = "NA";
                //Agregar PAGO EN UNA SOLA EXHIBICION por defecto debe ser una opción
                try
                {
                    FORMA_DE_PAGO = ConfigurationManager.AppSettings["FORMA_PAGO_DEFECTO"].ToString();
                }
                catch
                {

                }

            }
            /*
            if (PAGO == "")
            {
                PAGO = "99";
            }
            */
        }
        public void cargar_direccion_entrega(string INVOICE_IDp, cEMPRESA ocEMPRESA)
        {
            string BD_Auxiliar = cargar_bd_auxiliar(ocEMPRESA);

            //Buscar la linea en la factura
            sSQL = @" SELECT rl.INVOICE_ID, r.CUSTOMER_ID, ca.ADDR_NO
            FROM RECEIVABLE_LINE AS rl 
            INNER JOIN CUST_ORDER_LINE AS col ON rl.CUST_ORDER_ID = col.CUST_ORDER_ID AND rl.CUST_ORDER_LINE_NO = col.LINE_NO
            INNER JOIN RECEIVABLE AS r ON r.INVOICE_ID = rl.INVOICE_ID 
            INNER JOIN CUST_ADDRESS AS ca ON r.CUSTOMER_ID = ca.CUSTOMER_ID AND col.SHIPTO_ID = ca.SHIPTO_ID
            WHERE (rl.INVOICE_ID = '" + INVOICE_IDp + "') AND (NOT(col.SHIPTO_ID IS NULL))";

            DataTable oDataTableR = oData.EjecutarConsulta(sSQL);
            if (oDataTableR != null)
            {
                foreach (DataRow oDataRow1 in oDataTableR.Rows)
                {
                    ADDR_NO = oDataRow1["ADDR_NO"].ToString();
                    return;
                }
            }

            sSQL = " SELECT CUSTOMER_ORDER.SHIP_TO_ADDR_NO ";
            sSQL += " FROM CUSTOMER_ORDER INNER JOIN RECEIVABLE_LINE ON CUSTOMER_ORDER.ID = RECEIVABLE_LINE.CUST_ORDER_ID ";
            sSQL += " WHERE     (RECEIVABLE_LINE.INVOICE_ID = '" + INVOICE_IDp + "') ";

            if (oData.sTipo.Contains("Oracle"))
            {
                sSQL = " SELECT CUSTOMER_ORDER.SHIP_TO_ADDR_NO ";
                sSQL += " FROM CUSTOMER_ORDER INNER JOIN RECEIVABLE_LINE ON CUSTOMER_ORDER.ID = RECEIVABLE_LINE.CUST_ORDER_ID ";
                sSQL += " WHERE     (RECEIVABLE_LINE.INVOICE_ID = '" + INVOICE_IDp + "') ";
            }


            oDataTableR = oData.EjecutarConsulta(sSQL);
            if (oDataTableR != null)
            {
                foreach (DataRow oDataRow1 in oDataTableR.Rows)
                {
                    ADDR_NO = oDataRow1["SHIP_TO_ADDR_NO"].ToString();
                    return;
                }
            }




            return;

        }
        public string cargar_pais_entrega(string CUSTOMER_IDp, string ADDR_NOp)
        {
            if (ADDR_NOp == "")
            {
                return "";
            }
            sSQL = " SELECT  * ";
            sSQL += " FROM CUST_ADDRESS ";
            sSQL += " WHERE CUST_ADDRESS.CUSTOMER_ID = '" + CUSTOMER_IDp + "' AND CUST_ADDRESS.ADDR_NO=" + ADDR_NOp + " ";
            DataTable oDataTableR = oData.EjecutarConsulta(sSQL);
            if (oDataTableR != null)
            {
                foreach (DataRow oDataRow1 in oDataTableR.Rows)
                {
                    return oDataRow1["COUNTRY"].ToString().ToUpper();
                }
            }
            return "";
        }


        public void generar_retenido(string ACCOUNT_ID, cEMPRESA ocEMPRESA)
        {
            string BD_Auxiliar = cargar_bd_auxiliar(ocEMPRESA);
            if (ACCOUNT_ID != "")
            {
                //Lineas sin Iva Retenido
                string sSQL = " SELECT * ";
                sSQL += " FROM RECEIVABLE_LINE ";
                sSQL += " WHERE INVOICE_ID = '" + INVOICE_ID + "' ";
                if (ACCOUNT_ID != "")
                {
                    //Multiples cuentas , 

                    char[] delimiterChars = { ',' };
                    string[] ACCOUNT_ID_Arreglo = ACCOUNT_ID.Split(delimiterChars);
                    string ACCOUNT_ID_in = "";
                    foreach (string account_id in ACCOUNT_ID_Arreglo)
                    {
                        if (ACCOUNT_ID_in == "")
                        {
                            ACCOUNT_ID_in += "'" + account_id + "'";
                        }
                        else
                        {
                            ACCOUNT_ID_in += ",'" + account_id + "'";
                        }
                    }

                    //Cargar todas las lineas y hacer un IN
                    sSQL += " AND GL_ACCOUNT_ID IN (" + ACCOUNT_ID_in + ")";
                }
                sSQL += " ORDER BY LINE_NO ";
                DataTable oDataTable = oData.EjecutarConsulta(sSQL);
                TOTAL_RETENIDO = 0;
                foreach (DataRow oDataRow in oDataTable.Rows)
                {
                    //AMOUNT
                    string AMOUNT = oData.IsNullNumero(oDataRow["AMOUNT"].ToString());
                    //TOTAL_RETENIDO
                    TOTAL_RETENIDO += decimal.Parse(AMOUNT);

                }
            }
        }

        public void generar_lineas(string ACCOUNT_IDp, cEMPRESA ocEMPRESA, string ACCOUNT_ID_ANTICIPOp)
        {
            try
            {
                string BD_Auxiliar = cargar_bd_auxiliar(ocEMPRESA);
                //Lineas sin Iva Retenido
                string sSQL = " SELECT rl.*, r.TYPE ";
                sSQL += ", ISNULL((SELECT TOP 1 co.CUSTOMER_PO_REF FROM CUSTOMER_ORDER co WHERE co.ID=rl.CUST_ORDER_ID),'') as CustomerPoRef ";
                sSQL += " FROM RECEIVABLE_LINE rl ";
                sSQL += " INNER JOIN RECEIVABLE r ON r.INVOICE_ID=rl.INVOICE_ID";
                sSQL += " WHERE rl.INVOICE_ID = '" + INVOICE_ID + "' ";
                if (ACCOUNT_IDp != "")
                {
                    //Separar por ,
                    string[] cuentas = ACCOUNT_IDp.Split(',');
                    //En la tabla VMX_FE_SERIES_ACCOUNT guardar si existe
                    string SQL_in = "";
                    for (int i = 0; i < cuentas.Length; i++)
                    {
                        if (SQL_in != "")
                        {
                            SQL_in += ",";
                        }
                        SQL_in += "'" + cuentas[i] + "'";
                    }
                    sSQL += " AND NOT(GL_ACCOUNT_ID IN (" + SQL_in + "))";
                }
                sSQL += " ORDER BY LINE_NO ";
                BitacoraFX.Log(" generar_lineas 1021 sSQL: " + sSQL);
                DataTable oDataTable = oData.EjecutarConsulta(sSQL);
                TOTAL_AMOUNT = 0;
                //TOTAL_VAT_AMOUNT
                TOTAL_VAT_AMOUNT = 0;
                VAT_PERCENT = 0;
                DESCUENTO_TOTAL = 0;
                string LINE_NO = "";
                int contador = 0;
                LINEAS = new cLINEA[oDataTable.Rows.Count];
                foreach (DataRow oDataRow in oDataTable.Rows)
                {
                    //LINE_NO
                    LINE_NO = oData.IsNullNumero(oDataRow["LINE_NO"].ToString()); //oDataRow["LINE_NO"].ToString();
                    string PACKLIST_ID = oDataRow["PACKLIST_ID"].ToString();
                    string PACKLIST_LINE_NO = oDataRow["PACKLIST_LINE_NO"].ToString();
                    string CustomerPoRef = oDataRow["CustomerPoRef"].ToString();
                    //QTY

                    //Verifica si es descuento con la cuenta contable
                    if (oDataRow["GL_ACCOUNT_ID"].ToString() == ACCOUNT_ID_ANTICIPOp)
                    {
                        //DESCUENTO_TOTAL += Math.Abs(decimal.Parse(oData.IsNullNumero(oDataRow["AMOUNT"].ToString())));
                        DESCUENTO_TOTAL = 0;
                        anticipo = true;
                        anticipoVAT_AMOUNT = Math.Abs(decimal.Parse(oData.IsNullNumero(oDataRow["VAT_AMOUNT"].ToString())));
                        anticipoAMOUNT = Math.Abs(decimal.Parse(oData.IsNullNumero(oDataRow["AMOUNT"].ToString())));
                    }
                    else
                    {
                        string QTY = oData.IsNullNumero(oDataRow["QTY"].ToString());
                        if (QTY.Trim() == "")
                        {
                            QTY = "1";
                        }


                        if (decimal.Parse(QTY) != 0)
                        {
                            //QTY = "0";

                            //AMOUNT
                            string AMOUNT = oData.IsNullNumero(oDataRow["AMOUNT"].ToString());
                            //VAT_AMOUNT
                            string VAT_AMOUNT = "0";
                            if (!String.IsNullOrEmpty(oDataRow["VAT_AMOUNT"].ToString()))
                            {
                                VAT_AMOUNT = oData.IsNullNumero(oDataRow["VAT_AMOUNT"].ToString());
                            }

                            string VAT_AMOUNT_ln = oData.IsNullNumero(oDataRow["VAT_AMOUNT"].ToString());
                            string VAT_PERCENT_ln = oData.IsNullNumero(oDataRow["VAT_PERCENT"].ToString());

                            //TOTAL_AMOUNT
                            if (!Globales.IsNumeric(AMOUNT))
                            {
                                AMOUNT = "0";
                            }
                            if (!Globales.IsNumeric(VAT_AMOUNT))
                            {
                                VAT_AMOUNT = "0";
                            }
                            TOTAL_AMOUNT += decimal.Parse(AMOUNT);
                            //TOTAL_VAT_AMOUNT
                            TOTAL_VAT_AMOUNT += decimal.Parse(VAT_AMOUNT);
                            //VAT_PERCENT
                            //Selecianar Max VAT_PERCENT
                            string VAT_PERCENT_tr = oData.IsNullNumero(oDataRow["VAT_PERCENT"].ToString());
                            if (!Globales.IsNumeric(VAT_PERCENT_tr))
                            {
                                VAT_PERCENT_tr = "0";
                            }

                            if (VAT_PERCENT < decimal.Parse(VAT_PERCENT_tr))
                            {
                                VAT_PERCENT = decimal.Parse(oData.IsNullNumero(oDataRow["VAT_PERCENT"].ToString()));
                            }

                            //UNIT_PRICE
                            string UNIT_PRICE = oData.Trunca_y_formatea(0);
                            if (decimal.Parse(QTY) != 0)
                            {
                                //UNIT_PRICE = otroTruncar((decimal.Parse(AMOUNT) / decimal.Parse(QTY)), ocEMPRESA).ToString("#######.############");
                                UNIT_PRICE = (decimal.Parse(AMOUNT) / decimal.Parse(QTY)).ToString("#######.############");
                            }


                            string CUST_ORDER_ID = oData.IsNull(oDataRow["CUST_ORDER_ID"].ToString());
                            string CUST_ORDER_LINE_NO = oData.IsNullNumero(oDataRow["CUST_ORDER_LINE_NO"].ToString());
                            //Si es NDC no debe tener PACJLIST_ID
                            if (oDataRow["TYPE"].ToString().ToUpper().Equals("M"))
                            {
                                if (!String.IsNullOrEmpty(CUST_ORDER_ID))
                                {
                                    CUST_ORDER_ID = "";
                                }
                            }

                            //CUSTOMER ORDER
                            string CUSTOMER_ID = "";
                            sSQL = "  SELECT CUSTOMER_PO_REF,CUSTOMER_ID  ";
                            sSQL += " FROM CUSTOMER_ORDER ";
                            sSQL += " WHERE  ID='" + CUST_ORDER_ID + "' ";

                            DataTable oDataTable2 = oData.EjecutarConsulta(sSQL);

                            if (oDataTable2 != null)
                            {
                                foreach (DataRow oDataRow2 in oDataTable2.Rows)
                                {
                                    CUSTOMER_PO_REF = oData.IsNull(oDataRow2["CUSTOMER_PO_REF"].ToString());
                                    CUSTOMER_ID = oData.IsNull(oDataRow2["CUSTOMER_ID"].ToString());
                                }
                            }
                            //UM
                            string UM = "H87";//"NO APLICA";
                            string UM_CODE = "H87";
                            decimal TRADE_DISC_PERCENT;
                            //DESCRIPCION
                            string DESCRIPCION = "";
                            string PART_ID = "";
                            string PRODUCT_CODE = "";
                            string CUSTOMER_PART_ID = "";
                            decimal descuento = 0;
                            if (CUST_ORDER_ID != "")
                            {
                                //PART_ID
                                sSQL = @"  SELECT CUSTOMER_PART_ID,PART_ID, UNIT_PRICE,TRADE_DISC_PERCENT
                                , ISNULL(SELLING_UM,'') as SELLING_UM, PRODUCT_CODE,SERVICE_CHARGE_ID ";
                                sSQL += " FROM CUST_ORDER_LINE ";
                                sSQL += " WHERE  CUST_ORDER_ID='" + CUST_ORDER_ID + "' AND LINE_NO=" + CUST_ORDER_LINE_NO + " ";
                                if (oData.sTipo == "ORACLE")
                                {
                                    sSQL = sSQL.Replace("ISNULL", "NVL");
                                }

                                bool descuentoTr = false;
                                try
                                {
                                    descuentoTr = bool.Parse(ocEMPRESA.DESCUENTO_LINEA);
                                }
                                catch
                                {
                                }


                                DataTable oDataTable1 = oData.EjecutarConsulta(sSQL);
                                if (oDataTable1 != null)
                                {
                                    foreach (DataRow oDataRow1 in oDataTable1.Rows)
                                    {
                                        PART_ID = oData.IsNull(oDataRow1["PART_ID"].ToString()).Trim();
                                        SERVICE_CHARGE_ID = oData.IsNull(oDataRow1["SERVICE_CHARGE_ID"].ToString()).Trim();
                                        PRODUCT_CODE = oData.IsNull(oDataRow1["PRODUCT_CODE"].ToString()).Trim();
                                        CUSTOMER_PART_ID = oData.IsNull(oDataRow1["CUSTOMER_PART_ID"].ToString()).Trim();
                                        if (String.IsNullOrEmpty(PART_ID)) //(PART_ID.Trim() == "")
                                        {
                                            if (String.IsNullOrEmpty(CUSTOMER_PART_ID))//(CUSTOMER_PART_ID.Trim() != "")
                                            {
                                                //Buscar el PART_ID
                                                string sSQL_part = @"
                                                SELECT PART_ID
                                                FROM CUSTOMER_PRICE
                                                WHERE CUSTOMER_ID='" + CUSTOMER_ID + @"'
                                                AND CUSTOMER_PART_ID='" + CUSTOMER_PART_ID.Trim() + "'";

                                                DataTable oDataTable_part = oData.EjecutarConsulta(sSQL_part);
                                                if (oDataTable_part != null)
                                                {
                                                    foreach (DataRow oDataRow_part in oDataTable_part.Rows)
                                                    {
                                                        PART_ID = oDataRow_part["PART_ID"].ToString();
                                                    }
                                                }
                                            }

                                        }

                                        UNIT_PRICE = oDataRow1["UNIT_PRICE"].ToString();
                                        TRADE_DISC_PERCENT = decimal.Parse(oDataRow1["TRADE_DISC_PERCENT"].ToString());
                                        UM = oDataRow1["SELLING_UM"].ToString();
                                        UM_CODE = oDataRow1["SELLING_UM"].ToString();
                                        descuento = (decimal.Parse(UNIT_PRICE) * (TRADE_DISC_PERCENT / 100));
                                        if (TRADE_DISC_PERCENT != 0)
                                        {
                                            DESCUENTO_TOTAL += (descuento * decimal.Parse(QTY));
                                            UNIT_PRICE = (decimal.Parse(UNIT_PRICE) - descuento).ToString();
                                        }

                                        if (descuentoTr)
                                        {
                                            //DESCUENTO_TOTAL = 0;
                                            //UNIT_PRICE = (decimal.Parse(UNIT_PRICE)).ToString();

                                            //Rodrigo Escalna 30/04/2024
                                            UNIT_PRICE = (decimal.Parse(UNIT_PRICE) - descuento).ToString();

                                        }
                                        else
                                        {
                                            //Traer el 
                                            //string PACKLIST_ID = oDataRow["PACKLIST_ID"].ToString();
                                            //string PACKLIST_LINE_NO = oDataRow["PACKLIST_LINE_NO"].ToString();
                                            //Si es NDC no debe tener PACJLIST_ID
                                            if (oDataRow["TYPE"].ToString().ToUpper().Equals("M"))
                                            {
                                                if (String.IsNullOrEmpty(CUST_ORDER_ID))
                                                {
                                                    PACKLIST_ID = "";
                                                }
                                            }

                                            if (PACKLIST_ID != "")
                                            {

                                                sSQL = @"SELECT SHIPPER_LINE.*, (SHIPPED_QTY*UNIT_PRICE) as AMOUNT 
                                                        FROM SHIPPER_LINE 
                                                        WHERE PACKLIST_ID='" + PACKLIST_ID + "' AND LINE_NO=" + PACKLIST_LINE_NO;
                                                DataTable oDataTable6 = oData.EjecutarConsulta(sSQL);
                                                if (oDataTable1 != null)
                                                {
                                                    foreach (DataRow oDataRow6 in oDataTable6.Rows)
                                                    {
                                                        UNIT_PRICE = oDataRow6["UNIT_PRICE"].ToString();
                                                        AMOUNT = oDataRow6["AMOUNT"].ToString();
                                                    }
                                                }
                                            }
                                            else
                                            {
                                                UNIT_PRICE = (decimal.Parse(UNIT_PRICE) + descuento).ToString();
                                                AMOUNT = (decimal.Parse(UNIT_PRICE) * decimal.Parse(QTY)).ToString();
                                            }
                                        }

                                        
                                    }
                                }
                                if (PART_ID != "")
                                {
                                    sSQL = " SELECT * ";
                                    sSQL += " FROM PART ";
                                    sSQL += " WHERE  ID='" + PART_ID + "' ";
                                    oDataTable1 = oData.EjecutarConsulta(sSQL);
                                    if (oDataTable1 != null)
                                    {
                                        foreach (DataRow oDataRow1 in oDataTable1.Rows)
                                        {
                                            UM = oData.IsNull(oDataRow1["STOCK_UM"].ToString());
                                            UM_CODE = oData.IsNull(oDataRow1["STOCK_UM"].ToString());
                                            DESCRIPCION = "";
                                            if (!String.IsNullOrEmpty(oDataRow1["DESCRIPTION"].ToString()))
                                            {
                                                DESCRIPCION = oData.IsNull(oDataRow1["DESCRIPTION"].ToString());
                                            }
                                            //Rodrigo Escalona 02/04/2021 Descripción en Español para el XML
                                            if (TYPE.Equals("I"))
                                            {
                                                try
                                                {
                                                    if (ocEMPRESA.UsarDescripcionPersonalizada)
                                                    {

                                                        if (CUSTOMER_ID!="157")
                                                        {
                                                            DESCRIPCION = oData.IsNull(oDataRow1[
                                                            ocEMPRESA.UsarDescripcionPersonalizadaCampo
                                                            ].ToString());
                                                        }
                                                        
                                                    }
                                                }
                                                catch
                                                {

                                                }
                                            }
                                            
                                            //Validar si la descripción no esta 
                                            if (String.IsNullOrEmpty(DESCRIPCION.Trim()))
                                            {
                                                DESCRIPCION = oData.IsNull(oDataRow1["DESCRIPTION"].ToString());
                                            }


                                            if (!String.IsNullOrEmpty(PRODUCT_CODE))
                                            {
                                                if (PRODUCT_CODE.Equals(oData.IsNull(oDataRow1["PRODUCT_CODE"].ToString())))
                                                {
                                                    PRODUCT_CODE = oData.IsNull(oDataRow1["PRODUCT_CODE"].ToString());
                                                }
                                            }
                                            else
                                            {
                                                PRODUCT_CODE = oData.IsNull(oDataRow1["PRODUCT_CODE"].ToString());
                                            }
                                        }
                                    }
                                }
                                else
                                {
                                    //Buscar la WO Asignada
                                    sSQL = " SELECT SUPPLY_BASE_ID,SUPPLY_LOT_ID,SUPPLY_SPLIT_ID  " +
                                    " FROM DEMAND_SUPPLY_LINK " +
                                    " WHERE DEMAND_TYPE='CO' AND DEMAND_BASE_ID='" + CUST_ORDER_ID + "' AND SUPPLY_TYPE='WO'" +
                                    " AND DEMAND_SEQ_NO=" + CUST_ORDER_LINE_NO + "";
                                    oDataTable1 = oData.EjecutarConsulta(sSQL);
                                    if (oDataTable1 != null)
                                    {
                                        foreach (DataRow oDataRow1 in oDataTable1.Rows)
                                        {
                                            string SUPPLY_BASE_ID = oData.IsNull(oDataRow1["SUPPLY_BASE_ID"].ToString());
                                            string SUPPLY_LOT_ID = oData.IsNull(oDataRow1["SUPPLY_LOT_ID"].ToString());
                                            string SUPPLY_SPLIT_ID = oData.IsNull(oDataRow1["SUPPLY_SPLIT_ID"].ToString());

                                            sSQL = " SELECT PART.STOCK_UM, WORK_ORDER.BASE_ID, WORK_ORDER.LOT_ID, WORK_ORDER.SPLIT_ID,PART.PRODUCT_CODE " +
                                            " FROM WORK_ORDER INNER JOIN PART ON WORK_ORDER.PART_ID = PART.ID " +
                                            " WHERE (WORK_ORDER.BASE_ID = '" + SUPPLY_BASE_ID + "') AND (WORK_ORDER.LOT_ID = '" + SUPPLY_LOT_ID + "') AND (WORK_ORDER.SPLIT_ID = '" + SUPPLY_SPLIT_ID + "')";
                                            DataTable oDataTable_WORK_ORDER = oData.EjecutarConsulta(sSQL);
                                            if (oDataTable_WORK_ORDER != null)
                                            {
                                                foreach (DataRow oDataRow_WORK_ORDER in oDataTable_WORK_ORDER.Rows)
                                                {
                                                    UM = oData.IsNull(oDataRow1["STOCK_UM"].ToString());
                                                    UM_CODE = oData.IsNull(oDataRow1["STOCK_UM"].ToString());
                                                    if (!String.IsNullOrEmpty(PRODUCT_CODE))
                                                    {
                                                        PRODUCT_CODE = oData.IsNull(oDataRow1["PRODUCT_CODE"].ToString());
                                                    }
                                                }
                                            }
                                        }
                                    }


                                }


                                //Caso Tighitco
                                //Expedite Fee
                                //if (ocEMPRESA.ENTITY_ID.Equals("TIGLA"))
                                //{
                                //    if ((PRODUCT_CODE == "203") | (PRODUCT_CODE == "208"))
                                //    {
                                //        //Buscar la clasificación del primer numero de parte encontrado de abajo para arriba
                                //        sSQL = @"select TOP 1 part_id 
                                //        from CUST_ORDER_LINE col
                                //        where col.CUST_ORDER_ID='" + CUST_ORDER_ID + "' AND col.line_no<" + CUST_ORDER_LINE_NO
                                //        + " ORDER BY LINE_NO DESC";
                                //        DataTable oDataTableEXFEE = oData.EjecutarConsulta(sSQL);
                                //        if (oDataTableEXFEE != null)
                                //        {
                                //            foreach (DataRow oDataRowEXFEE in oDataTableEXFEE.Rows)
                                //            {
                                //                PART_ID = oData.IsNull(oDataRowEXFEE["part_id"].ToString());
                                //            }
                                //        }
                                //    }
                                //}

                            }

                            //Rodrigo Escalona
                            //Cambio de decimales
                            //10-7-2016
                            //UNIT_PRICE = Math.Round(decimal.Parse(UNIT_PRICE), valorUnitariodecimales).ToString();

                            string referenceTr=oData.IsNull(oDataRow["REFERENCE"].ToString());
                            if (!String.IsNullOrEmpty(referenceTr))
                            {
                                if (referenceTr.Length > 0)
                                {
                                    if (TYPE.Equals("I"))
                                    {
                                        if (!ocEMPRESA.UsarDescripcionPersonalizada)
                                        {
                                            DESCRIPCION = referenceTr;
                                        }
                                        if (String.IsNullOrEmpty(DESCRIPCION))
                                        {
                                            DESCRIPCION = referenceTr;
                                        }
                                    }
                                    else
                                    {
                                        DESCRIPCION = referenceTr;
                                    }                                 
                                }                                
                            }

                            //Descripcion de la Unidad de Medida
                            sSQL = "SELECT * FROM UNITS WHERE UNIT_OF_MEASURE='" + UM + "'";
                            DataTable oDataTable3 = oData.EjecutarConsulta(sSQL);
                            if (oDataTable3 != null)
                            {
                                foreach (DataRow oDataRow3 in oDataTable3.Rows)
                                {
                                    UM = oData.IsNull(oDataRow3["DESCRIPTION"].ToString()).ToLower();
                                }
                            }

                            //Cambiar las UM
                            if (UM == "SERVICES-CON")
                            {
                                UM = "SERVICIO";
                            }
                            if (PART_ID == "SERVICES-CON")
                            {
                                UM = "SERVICIO";
                            }
                            if (PART_ID == "SERVICES-MN")
                            {
                                UM = "SERVICIO";
                            }
                            if (UM == "SERVICES-MN")
                            {
                                UM = "SERVICIO";
                            }
                            if (UM == "EA")
                            {
                                UM = "PZ";
                            }
                            if (UM.ToLower() == "each")
                            {
                                UM = "PZ";
                            }


                            try
                            {
                                if (ocEMPRESA.UsarCustomerPartDescripcion)
                                {
                                    //Rodrigo Escalona: 26/04/2021 Si es * es para todos
                                    if (ocEMPRESA.UsarCustomerPartDescripcionCliente.Contains("*"))
                                    {
                                        if (CUSTOMER_PART_ID != "")
                                        {
                                            DESCRIPCION = CUSTOMER_PART_ID + " " + DESCRIPCION;
                                        }
                                    }
                                    else
                                    {
                                        if (ocEMPRESA.UsarCustomerPartDescripcionCliente.Contains(CUSTOMER_ID))
                                        {
                                            if (CUSTOMER_PART_ID != "")
                                            {
                                                
                                            }
                                            //Rodrigo Escalona: 28/04/2021 Agregar el 
                                            DESCRIPCION = CUSTOMER_PART_ID + " " + CUSTOMER_PO_REF + " " + DESCRIPCION;
                                        }
                                    }
                                }
                            }
                            catch
                            {

                            }

                            try
                            {
                                if (ocEMPRESA.UsarPartNotation)
                                {
                                    string notacion = obtenerNotation(PART_ID, oData);
                                    if (!String.IsNullOrEmpty(notacion))
                                    {
                                        DESCRIPCION = notacion;
                                    }
                                }
                            }
                            catch
                            {

                            }
                            

                            //Especificación de Linea del Binary
                            string RECV_LINE_BINARY = "";
                            if (ocEMPRESA.CUSTOMER_ESPECIFICACION_LINEA != null)
                            {
                                bool buscarEspecificacionLinea = false;
                                if (ocEMPRESA.CUSTOMER_ESPECIFICACION_LINEA.Trim().Equals("*"))
                                {
                                    buscarEspecificacionLinea = true;
                                }
                                else
                                {
                                    if (ocEMPRESA.CUSTOMER_ESPECIFICACION_LINEA.Contains(CUSTOMER_ID))
                                    {
                                        buscarEspecificacionLinea = true;
                                    }
                                }

                                if (buscarEspecificacionLinea)
                                {
                                    try
                                    {
                                        //Descripcion de la Unidad de Medida
                                        //if (!oData.sTipo.Contains("ORA"))
                                        //{
                                        sSQL = @"SELECT CAST(CAST(BITS AS VARBINARY(max)) AS NVARCHAR(max)) 
                                            COLLATE SQL_Latin1_General_CP1_CI_AS as Especificacion
                                            FROM RECV_LINE_BINARY WHERE [INVOICE_ID]='" + INVOICE_ID + @"' 
                                            AND [RECV_LINE_NO]='" + LINE_NO + "'";
                                        DataTable oDataTable5 = oData.EjecutarConsulta(sSQL);
                                        if (oDataTable5 != null)
                                        {
                                            foreach (DataRow oDataRow5 in oDataTable5.Rows)
                                            {
                                                RECV_LINE_BINARY = oData.IsNull(oDataRow5["Especificacion"].ToString()).ToLower();
                                                //Validar RECV_LINE_BINARY 
                                                //Validar que no este en Units measures
                                                //Descripcion de la Unidad de Medida
                                                //sSQL = "SELECT * FROM UNITS WHERE UNIT_OF_MEASURE='" + RECV_LINE_BINARY + "'";
                                                //DataTable oDataTableUi = oData.EjecutarConsulta(sSQL);
                                                //if (oDataTableUi != null)
                                                //{
                                                //    foreach (DataRow oDataRowUi in oDataTableUi.Rows)
                                                //    {
                                                //        UM = oData.IsNull(oDataRowUi["UNIT_OF_MEASURE"].ToString()).ToUpper();
                                                //    }
                                                //}

                                            }
                                            //    }
                                            //}
                                        }
                                    }
                                    catch
                                    {
                                    }
                                }
                            }

                            //Rodrigo Escalona 23/12/2020 
                            try
                            {
                                if (ocEMPRESA.AGREGAR_LOTE != null){
                                    if (ocEMPRESA.AGREGAR_LOTE.Equals("True"))
                                    {
                                        DESCRIPCION += buscarLote(INVOICE_ID, LINE_NO, oData);
                                    }
                                }
                            }
                            catch (Exception error)
                            {
                                ErrorFX.mostrar(error, true, true, "cFACTURA 1476 ", false);
                            }

                            //CFDI 3.3
                            //Buscar el producto
                            //Buscar el product code
                            //Buscar la cuenta contable
                            string GL_ACCOUNT_ID = "";
                            if (!String.IsNullOrEmpty(oDataRow["GL_ACCOUNT_ID"].ToString()))
                            {
                                GL_ACCOUNT_ID = oDataRow["GL_ACCOUNT_ID"].ToString();
                            }
                            //BitacoraFX.Log("Cuenta contable: " + GL_ACCOUNT_ID + " Part: " + PART_ID + " Product Code: " + PRODUCT_CODE);
                            configuracionProducto ClaveProdServtr = buscarClave(PART_ID, PRODUCT_CODE, GL_ACCOUNT_ID,SERVICE_CHARGE_ID);
                            string ClaveProdServ = "";
                            if (ClaveProdServtr != null)
                            {
                                if (ClaveProdServtr.Id != 0)
                                {
                                    ClaveProdServ = ClaveProdServtr.clasificacionSAT;
                                    if (!String.IsNullOrEmpty(ClaveProdServtr.unidadSAT))
                                    {
                                        UM = ClaveProdServtr.unidadSAT;
                                    }
                                }
                            }
                            //BitacoraFX.Log("Cuenta contable UM " + UM);

                            if (String.IsNullOrEmpty(DESCRIPCION))
                            {

                            }



                            //Llenar Arreglos
                            LINEAS[contador] = new cLINEA(LINE_NO, QTY, PART_ID, DESCRIPCION + RECV_LINE_BINARY, UNIT_PRICE, AMOUNT, UM
                                , PRODUCT_CODE, VAT_AMOUNT_ln, VAT_PERCENT_ln.ToString(), ClaveProdServ, oDataRow["GL_ACCOUNT_ID"].ToString()
                                ,SERVICE_CHARGE_ID, CUSTOMER_PART_ID, UM_CODE, descuento.ToString(), CustomerPoRef
                                );

                            //Validar si la proxima linea no es retencion si no agregarla
                            if (!String.IsNullOrEmpty(ACCOUNT_IDp))
                            {
                                LINEAS[contador].retencion = new List<cLINEARETENCION>();
                                sSQL = " SELECT rl.*, r.TYPE ";
                                sSQL += " FROM RECEIVABLE_LINE rl ";
                                sSQL += " INNER JOIN RECEIVABLE r ON r.INVOICE_ID=rl.INVOICE_ID";
                                sSQL += " WHERE rl.INVOICE_ID = '" + INVOICE_ID + "' ";
                                if (ACCOUNT_IDp != "")
                                {
                                    //Separar por ,
                                    string[] cuentas = ACCOUNT_IDp.Split(',');
                                    //En la tabla VMX_FE_SERIES_ACCOUNT guardar si existe
                                    string SQL_in = "";
                                    for (int i = 0; i < cuentas.Length; i++)
                                    {
                                        if (SQL_in != "")
                                        {
                                            SQL_in += ",";
                                        }
                                        SQL_in += "'" + cuentas[i] + "'";
                                    }
                                    sSQL += " AND (GL_ACCOUNT_ID IN (" + SQL_in + "))";
                                }
                                sSQL += " AND LINE_NO>" + LINE_NO;
                                sSQL += " ORDER BY  LINE_NO ";
                                BitacoraFX.Log("generar_lineas 1499 " + sSQL);
                                //Si hay lineas agregarla como retencion
                                DataTable oDataTableRetencion = oData.EjecutarConsulta(sSQL);
                                if (oDataTableRetencion != null)
                                {
                                    foreach (DataRow oDataRowRetencion in oDataTableRetencion.Rows)
                                    {
                                        TOTAL_RETENIDO += Math.Abs(decimal.Parse(oData.IsNullNumero(oDataRowRetencion["AMOUNT"].ToString())));
                                        cLINEARETENCION retencion = new cLINEARETENCION();
                                        retencion.GL_ACCOUNT_ID = oDataRowRetencion["GL_ACCOUNT_ID"].ToString();
                                        retencion.AMOUNT = oData.IsNullNumero(oDataRowRetencion["AMOUNT"].ToString()); 
                                        LINEAS[contador].retencion.Add(retencion);
                                    }
                                }
                            }
                            contador++;
                        }

                    }
                }

                if (anticipo)
                {
                    //Cargar el monto Total
                    TOTAL_AMOUNT = totalAmount(INVOICE_ID);
                }
            }
            catch (Exception error)
            {
                ErrorFX.mostrar(error, true, true, "cFACTURA 1394 ", false);
            }
        }

        private string obtenerNotation(string pART_ID, cCONEXCION oDataP)
        {
            try
            {
                string @sSQL = @"
                    sELECT CAST(CAST(nOTE AS VARBINARY(mAX)) AS NVARCHAR(mAX))  as Nota
                    from NOTATION where TYPE='P' AND OWNER_ID='" + pART_ID + @"'
                ";

                DataTable oDataTableLote = oDataP.EjecutarConsulta(sSQL);
                if (oDataTableLote != null)
                {
                    foreach (DataRow oDataRowLote in oDataTableLote.Rows)
                    {
                        return oDataRowLote["Nota"].ToString();
                    }
                }
                return string.Empty;
            }
            catch (Exception error)
            {
                ErrorFX.mostrar(error, false, true, "cFACTURA - 1571 obtenerNotation ", false);
            }
            return string.Empty;
        }

        private string buscarLote(string INVOICE_ID, string LINE_NO, cCONEXCION oDataP)
        {
            try
            {
                string @sSQL = @"
                    SELECT TOP 1 documentosfx.dbo.[buscarLote]('" + INVOICE_ID + @"', " + LINE_NO + @") as Lote
                    FROM RECEIVABLE_LINE rl
                    WHERE rl.INVOICE_ID='" + INVOICE_ID + @"'
                    AND rl.LINE_NO='" + LINE_NO + @"'
                ";

                DataTable oDataTableLote = oDataP.EjecutarConsulta(sSQL);
                if (oDataTableLote != null)
                {
                    foreach (DataRow oDataRowLote in oDataTableLote.Rows)
                    {
                        return oDataRowLote["Lote"].ToString();
                    }
                }
                return "";
            }
            catch (Exception error)
            {
                ErrorFX.mostrar(error, false, true, "cFACTURA - 1571 buscarLote ", false);
            }
            return "";
        }

        internal configuracionProducto buscarClave(string PART_ID, string PRODUCT_CODE, string GL_ACCOUNT_ID, string SERVICE_CHARGE_ID)
        {
            try
            {
                //Prioridad 1
                if (!String.IsNullOrEmpty(PART_ID))
                {
                    configuracionProducto producto = buscarProducto(PART_ID);
                    if (producto != null)
                    {
                        return producto;
                    }
                }
                //Prioridad 2
                if (!String.IsNullOrEmpty(PRODUCT_CODE))
                {
                    configuracionProducto producto = buscarProducto(PRODUCT_CODE);
                    if (producto != null)
                    {
                        return producto;
                    }
                }
                //Prioridad 3
                if (!String.IsNullOrEmpty(SERVICE_CHARGE_ID))
                {
                    configuracionProducto producto = buscarProducto(SERVICE_CHARGE_ID);
                    if (producto != null)
                    {
                        return producto;
                    }
                }
                //Prioridad 4
                if (!String.IsNullOrEmpty(GL_ACCOUNT_ID))
                {
                    configuracionProducto producto = buscarProducto(GL_ACCOUNT_ID);
                    if (producto != null)
                    {
                        return producto;
                    }
                }


                //string productoBuscar = "";
                //if (!String.IsNullOrEmpty(SERVICE_CHARGE_ID))
                //{
                //    productoBuscar = SERVICE_CHARGE_ID;
                //}
                //else
                //{
                //    if (!String.IsNullOrEmpty(PART_ID))
                //    {
                //        productoBuscar = PART_ID;
                //    }
                //}

                //if (!String.IsNullOrEmpty(productoBuscar))
                //{
                //    configuracionProducto producto = buscarProducto(productoBuscar);
                //    bool buscarProducCode = false;
                //    if (producto == null)
                //    {
                //        buscarProducCode = true;
                //    }
                //    else
                //    {
                //        if (producto.Id == 0)
                //        {
                //            buscarProducCode = true;
                //        }
                //    }
                //    if (buscarProducCode)
                //    {
                //        //Validar el product Code
                //        if (!String.IsNullOrEmpty(PRODUCT_CODE))
                //        {
                //            //Buscar en Producto
                //            return buscarProducto(PRODUCT_CODE);
                //        }
                //    }
                //    else
                //    {
                //        return producto;
                //    }
                //}
                //else
                //{
                //    if (!String.IsNullOrEmpty(PRODUCT_CODE))
                //    {
                //        //Buscar en Producto
                //        configuracionProducto producto = buscarProducto(PRODUCT_CODE);
                //        if (producto == null)
                //        {
                //            errorValidacion = true;
                //            errorMensaje += "No existe clasificación por product code " + PRODUCT_CODE
                //                + Environment.NewLine;
                //        }
                //        else
                //        {
                //            return producto;
                //        }
                //    }
                //    else
                //    {
                //        //Buscar la cuenta contable y la asignación que tiene en la clave del producto
                //        if (!String.IsNullOrEmpty(GL_ACCOUNT_ID))
                //        {
                //            configuracionProducto producto = buscarProducto(GL_ACCOUNT_ID);
                //            if (producto == null)
                //            {
                //                errorValidacion = true;
                //                errorMensaje += "No existe clasificación para la cuenta contable " + GL_ACCOUNT_ID
                //                    + Environment.NewLine;
                //            }
                //            else
                //            {
                //                return producto;
                //            }
                //        }
                //    }
                //}
                ErrorFX.mostrar("El Producto " + PART_ID + " o familia " + PRODUCT_CODE + " o cuenta contable " + GL_ACCOUNT_ID + @" 
                    o servicio de cargo " + SERVICE_CHARGE_ID + " no tiene clasificador del SAT - cFACTURA 1433 ", true, true, true);

                return null;
            }
            catch (Exception error)
            {
                ErrorFX.mostrar(error, true, true, "cFACTURA 1406 ");
            }
            return null;
        }

        private configuracionProducto buscarProducto(string pART_ID)
        {
            configuracionProducto resultado = new configuracionProducto();
            ModeloDocumentosFXContainer dbContext = new ModeloDocumentosFXContainer();
            try
            {
                configuracionProducto registro = (from r in dbContext.configuracionProductoSet
                                                  .Where(a => a.producto == pART_ID)
                                                  select r).FirstOrDefault();
                BitacoraFX.Log("cfACTURA  1879 - buscarproducto " + pART_ID); 
                
                if (registro != null)
                {
                    resultado = registro;
                    return resultado;
                }
            }
            catch (Exception err)
            {
                ErrorFX.mostrar(err, true, true, "cFACTURA - 1265 - ", false);
            }
            return null;
        }

        private decimal totalAmount(string pINVOICE_ID)
        {
            sSQL = " SELECT TOTAL_AMOUNT  FROM RECEIVABLE ";
            sSQL += " WHERE INVOICE_ID='" + pINVOICE_ID + "'";

            DataTable oDataTable = oData.EjecutarConsulta(sSQL);
            if (oDataTable != null)
            {
                foreach (DataRow oDataRow in oDataTable.Rows)
                {
                    return decimal.Parse(oDataRow["TOTAL_AMOUNT"].ToString().ToUpper());
                }
            }
            return 0;
        }



        public decimal otroTruncar(decimal valor, cEMPRESA oEMPRESA)
        {
            try
            {
                decimal result = 0;
                string[] xs = valor.ToString().Split('.');

                string decimalparte = xs[1] + "0000000000000000000000";
                if (oEMPRESA.APROXIMACION == "Truncar")
                {
                    result = decimal.Parse(xs[0] + "." + decimalparte.Substring(0, Math.Min(decimalparte.Length, int.Parse(oEMPRESA.VALORES_DECIMALES))));
                }
                else
                {
                    result = Math.Round(valor, int.Parse(oEMPRESA.VALORES_DECIMALES));
                    xs = result.ToString().Split('.');
                    if (xs[1].Length != int.Parse(oEMPRESA.VALORES_DECIMALES))
                    {
                        decimalparte = xs[1] + "0000000000000000000000";
                        result = decimal.Parse(xs[0] + "." + decimalparte.Substring(0, int.Parse(oEMPRESA.VALORES_DECIMALES)));
                    }
                }
                return result;
            }
            catch
            {
                return valor;
            }


        }


        public cce11_v32.c_INCOTERM FREE_ON_BOARD { get; set; }

        public string TAX_ID_NUMBER { get; set; }

        public string COUNTRY { get; set; }
        public string STATE { get; set; }


        public string WEIGHT_UM { get; set; }

        internal decimal cargarPartWEIGHT(string noIdentificacion, bool convertirLB = true)
        {
            decimal resultado = 1;
            try
            {
                sSQL = "select WEIGHT,WEIGHT_UM  from part where id='" + noIdentificacion + "'";
                DataTable oDataTable = oData.EjecutarConsulta(sSQL);
                if (oDataTable != null)
                {
                    foreach (DataRow oDataRow in oDataTable.Rows)
                    {
                        if (String.IsNullOrEmpty(oDataRow["WEIGHT"].ToString()))
                        {
                            throw new Exception("El producto " + noIdentificacion + " no tiene peso en Visual por favor configúrelo en el maestro de artículos.");
                        }
                        else
                        {
                            if (String.IsNullOrEmpty(oDataRow["WEIGHT_UM"].ToString()))
                            {
                                throw new Exception("El producto " + noIdentificacion + " no tiene unidad de medida de peso en Visual por favor configúrelo en el maestro de artículos.");
                            }
                            else
                            {
                                resultado = decimal.Parse(oDataRow["WEIGHT"].ToString().ToUpper().Trim());
                                WEIGHT_UM = oDataRow["WEIGHT_UM"].ToString().ToUpper().Trim();
                                if (convertirLB)
                                {
                                    if (oDataRow["WEIGHT_UM"].ToString().Trim().Equals("LB"))
                                    {
                                        //Convertir a KG
                                        resultado = resultado * decimal.Parse("0.453592");
                                    }
                                }
                            }
                        }

                    }
                }
            }
            catch (Exception err)
            {
                ErrorFX.mostrar(err, true, true, "cFACTURA - 1525 - ", false);
            }
            return resultado;
        }
    }
}
