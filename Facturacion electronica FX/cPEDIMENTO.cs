﻿using System;
using System.Collections.Generic;

using System.Text;
using System.Data;
using System.Windows;
using System.Windows.Forms;
using Generales;
using ModeloDocumentosFX;

namespace FE_FX
{
    class cPEDIMENTO
    {
        private string sSQL = "";
        public string ROW_ID { get; set; }
        public string INVOICE_ID { get; set; }
        public string LINE_NO { get; set; }
        public string PEDIMENTO { get; set; }
        public string ADUANA { get; set; }
        public string FECHA { get; set; }
        public string BD_Auxiliar { get; set; }
        private cCONEXCION oData = new cCONEXCION("");
        private cEMPRESA ocEMPRESA = new cEMPRESA();
        public cPEDIMENTO(cEMPRESA ocEMPRESAp)
        {
            ocEMPRESA = ocEMPRESAp;
            BD_Auxiliar = ocEMPRESAp.BD_AUXILIAR + ".dbo.";
            if (ocEMPRESAp.BD_AUXILIAR == "")
            {
                BD_Auxiliar = "";
            }
            BD_Auxiliar = "";
            ModeloDocumentosFXContainer dbContext = new ModeloDocumentosFXContainer();

            string conection = dbContext.Database.Connection.ConnectionString;
            oData = new cCONEXCION(conection);
            limpiar();
        }

        public cPEDIMENTO(cCONEXCION pData, cEMPRESA ocEMPRESAp)
        {
            if (ocEMPRESAp != null)
            {
                BD_Auxiliar = ocEMPRESAp.BD_AUXILIAR + ".dbo.";
                if (ocEMPRESAp.BD_AUXILIAR == "")
                {
                    BD_Auxiliar = "";
                }
            }
            else
            {
                BD_Auxiliar = "";
            }
            BD_Auxiliar = "";

            ModeloDocumentosFXContainer dbContext = new ModeloDocumentosFXContainer();

            string conection = dbContext.Database.Connection.ConnectionString;
            oData = new cCONEXCION(conection);

            limpiar();
        }

        public void  limpiar()
        {
            ROW_ID = "";
            INVOICE_ID = "";
            LINE_NO = "";
            PEDIMENTO = "";
            ADUANA = "";
            FECHA = "";
        }

        public List<cPEDIMENTO> todos(string INVOICE_ID,string BUSQUEDA)
        {

            List<cPEDIMENTO> lista = new List<cPEDIMENTO>();

            sSQL  = " SELECT * ";
            sSQL += " FROM " + BD_Auxiliar +  @"FEFX_PEDIMENTOS ";
            sSQL += " WHERE INVOICE_ID = '" + INVOICE_ID + "' ";
            sSQL += " AND PEDIMENTO like '%" + BUSQUEDA + "%' ";
            DataTable oDataTable = oData.EjecutarConsulta(sSQL);
            foreach (DataRow oDataRow in oDataTable.Rows)
            {
                lista.Add(new cPEDIMENTO(ocEMPRESA)
                {
                    ROW_ID = oDataRow["ROW_ID"].ToString()
                   ,INVOICE_ID = oDataRow["INVOICE_ID"].ToString()
                   ,LINE_NO = oDataRow["LINE_NO"].ToString()
                   ,PEDIMENTO = oDataRow["PEDIMENTO"].ToString()
                   ,
                    ADUANA = oDataRow["ADUANA"].ToString()
,
                    FECHA = oDataRow["FECHA"].ToString()
                });
            }
            return lista;
        }

        public cPEDIMENTO buscar_ADUANA(string BUSQUEDA,string ADUANA)
        {

            cPEDIMENTO lista = new cPEDIMENTO(ocEMPRESA);

            sSQL = " SELECT TOP 1 * ";
            sSQL += " FROM " + BD_Auxiliar + @"FEFX_PEDIMENTOS ";
            sSQL += " WHERE INVOICE_ID LIKE '%" + BUSQUEDA + "%' ";
            sSQL += " AND ADUANA='" + ADUANA + "' ";
            DataTable oDataTable = oData.EjecutarConsulta(sSQL);
            foreach (DataRow oDataRow in oDataTable.Rows)
            {
                lista.ROW_ID = oDataRow["ROW_ID"].ToString();
                lista. INVOICE_ID = oDataRow["INVOICE_ID"].ToString();
                lista.LINE_NO = oDataRow["LINE_NO"].ToString();
                lista.PEDIMENTO = oDataRow["PEDIMENTO"].ToString();
                lista.ADUANA = oDataRow["ADUANA"].ToString();

            }
            return lista;
        }


        public bool cargar(string ROW_IDp)
        {
            sSQL  = " SELECT * ";
            sSQL += " FROM " + BD_Auxiliar + @"FEFX_PEDIMENTOS ";
            sSQL += " WHERE ROW_ID=" + ROW_IDp + " ";
            DataTable oDataTable = oData.EjecutarConsulta(sSQL);
            if (oDataTable != null)
            {
                foreach (DataRow oDataRow in oDataTable.Rows)
                {
                    //Añadir el Registro
                    ROW_ID = oDataRow["ROW_ID"].ToString();
                    INVOICE_ID = oDataRow["INVOICE_ID"].ToString();
                    LINE_NO = oDataRow["LINE_NO"].ToString();
                    PEDIMENTO = oDataRow["PEDIMENTO"].ToString();
                    ADUANA = oDataRow["ADUANA"].ToString();
                    FECHA = oDataRow["FECHA"].ToString();
                    return true;
                }
            }
            return false;
        }

        public bool cargar_INVOICE_ID(string INVOICE_IDp)
        {
            sSQL = " SELECT * ";
            sSQL += " FROM PEDIMENTO ";
            sSQL += " WHERE INVOICE_ID='" + INVOICE_IDp + "' ";
            DataTable oDataTable = oData.EjecutarConsulta(sSQL);
            if (oDataTable != null)
            {
                foreach (DataRow oDataRow in oDataTable.Rows)
                {
                    //Añadir el Registro
                    ROW_ID = oDataRow["ROW_ID"].ToString();
                    INVOICE_ID = oDataRow["INVOICE_ID"].ToString();
                    LINE_NO = oDataRow["LINE_NO"].ToString();
                    PEDIMENTO = oDataRow["PEDIMENTO"].ToString();
                    ADUANA = oDataRow["ADUANA"].ToString();
                    FECHA = oDataRow["FECHA"].ToString();
                    return true;
                }
            }
            return false;
        }

        public List<cPEDIMENTO> obtener_linea(string INVOICE_IDp, string LINE_NOp, cEMPRESA ocEMPRESAp)
        {
            //string BD_Auxiliar = "";
            //if (ocEMPRESAp.BD_AUXILIAR != "")
            //{
            //    BD_Auxiliar = ocEMPRESAp.BD_AUXILIAR + ".dbo.";
            //}

            List<cPEDIMENTO> lista = new List<cPEDIMENTO>();
            try
            {
                sSQL = " SELECT * ";
                sSQL += " FROM " + BD_Auxiliar + "FEFX_PEDIMENTOS ";
                sSQL += " WHERE INVOICE_ID='" + INVOICE_IDp + "' ";
                sSQL += " AND LINE_NO=" + LINE_NOp + " ";
                DataTable oDataTable = oData.EjecutarConsulta(sSQL);
                foreach (DataRow oDataRow in oDataTable.Rows)
                {
                    lista.Add(new cPEDIMENTO(ocEMPRESA)
                    {
                        ROW_ID = oDataRow["ROW_ID"].ToString()
                       ,
                        INVOICE_ID = oDataRow["INVOICE_ID"].ToString()
                       ,
                        LINE_NO = oDataRow["LINE_NO"].ToString()
                       ,
                        PEDIMENTO = oDataRow["PEDIMENTO"].ToString()
                       ,
                        ADUANA = oDataRow["ADUANA"].ToString()
                        ,
                        FECHA = oDataRow["FECHA"].ToString()
                    });
                }
            }
            catch(Exception e)
            {

            }
            return lista;
        }

        public bool guardar()
        {
            if (INVOICE_ID.Trim() == "")
            {
                MessageBox.Show("La Factura es requerida.", Application.ProductName + "-" + Application.ProductVersion, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return false;
            }

            if (LINE_NO.Trim() == "")
            {
                MessageBox.Show("La linea es requerida.", Application.ProductName + "-" + Application.ProductVersion, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return false;
            }

            //if (ADUANA.Trim() == "")
            //{
            //    MessageBox.Show("La Aduana es requerida.", Application.ProductName + "-" + Application.ProductVersion, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            //    return false;
            //}

            if (ROW_ID == "")
            {

                sSQL = " INSERT INTO " + BD_Auxiliar + @"FEFX_PEDIMENTOS ";
                sSQL += " ( ";
                sSQL += " INVOICE_ID, LINE_NO, PEDIMENTO, ADUANA";
                sSQL += ", FECHA";
                sSQL += " )";
                sSQL += " VALUES ";
                sSQL += "(";
                sSQL += " '" + INVOICE_ID + "','" + LINE_NO + "','" + PEDIMENTO + "','" + ADUANA + "' ";
                sSQL += ", '" + FECHA + "'";
                sSQL += ")";
                DataTable oDataTable = oData.EjecutarConsulta(sSQL);
                if (oDataTable!=null)
                {

                    sSQL = "SELECT TOP 1 MAX(ROW_ID) as ROW_ID FROM " + BD_Auxiliar + @"FEFX_PEDIMENTOS";
                    oDataTable = oData.EjecutarConsulta(sSQL);
                    if (oDataTable != null)
                    {
                        foreach (DataRow oDataRow in oDataTable.Rows)
                        {
                            ROW_ID = oDataRow["ROW_ID"].ToString();
                        }
                    }
                }

            }
            else
            {
                sSQL = " UPDATE " + BD_Auxiliar + @"FEFX_PEDIMENTOS ";
                sSQL += " SET INVOICE_ID='" + INVOICE_ID + "'";
                sSQL += ",LINE_NO='" + LINE_NO + "',PEDIMENTO='" + PEDIMENTO + "' ,ADUANA='" + ADUANA + "' ";
                sSQL += ",FECHA='" + FECHA + "'";
                sSQL += " WHERE ROW_ID=" + ROW_ID + " ";
                oData.EjecutarConsulta(sSQL);

            }
            return true;

        }

        public bool eliminar(string ROW_IDp)
        {
            if (ROW_IDp != "")
            {
                sSQL = "DELETE FROM " + BD_Auxiliar + @"FEFX_PEDIMENTOS WHERE ROW_ID=" + ROW_IDp;
                oData.EjecutarConsulta(sSQL);
                return true;
            }
            return false;

        }
    }
}
