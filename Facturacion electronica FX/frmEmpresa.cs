﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;

using System.Text;
using System.Windows.Forms;

using System.Net.Mail;
using Generales;
using FE_FX.Clases;

namespace FE_FX
{
    public partial class frmEmpresa : Form
    {
        private cCONEXCION oData = new cCONEXCION("");
        private cCONEXCION oData_ERP = new cCONEXCION("");
        private cEMPRESA oObjeto;
        private bool modificado;

        public frmEmpresa(string sConn)
        {
            oData.sConn = sConn;
            oObjeto = new cEMPRESA(oData);
            InitializeComponent();
            ajax_loader.Visible = false;
            pictureBox1.Visible = false;
            label6.Visible = false;
            limpiar();
        }
        public frmEmpresa(string sConn, string ROW_IDp)
        {
            modificado = false;
            oData.sConn = sConn;
            oObjeto = new cEMPRESA(oData);

            InitializeComponent();
            cargarImpresoras();
            pictureBox1.Visible = false;
            label6.Visible = false;
            limpiar();
            oObjeto.ROW_ID = ROW_IDp;
            cargar();
        }

        private void cargarImpresoras()
        {
            IMPRESORA.Items.Clear();
            IMPRESORA.Items.Add("");    
            foreach (string printer in System.Drawing.Printing.PrinterSettings.InstalledPrinters)
            {
                IMPRESORA.Items.Add(printer);    
            }
        }

        public void limpiar()
        {
            if (modificado)
            {

                DialogResult Resultado = MessageBox.Show("¿Desea guardar los cambios?", Application.ProductName + "-" + Application.ProductVersion, MessageBoxButtons.YesNo);
                if (Resultado.ToString() == "Yes")
                {
                    guardar();
                }


            }
            modificado = false;
            oObjeto = new cEMPRESA(oData);
            ID.Text = "";
            RFC.Text = "";
            CALLE.Text = "";
            INTERIOR.Text = "";
            EXTERIOR.Text = "";
            COLONIA.Text = "";
            CP.Text = "";
            LOCALIDAD.Text = "";
            REFERENCIA.Text = "";
            ESTADO.Text = "";
            PAIS.Text = "";
            PAIS.Text = "MEX";
            MUNICIPIO.Text = "";
            SMTP.Text = "";
            USUARIO.Text = "";
            PASSWORD_EMAIL.Text = "";
            PUERTO.Text = "";
            SSL.Checked = false;
            CREDENCIALES.Checked = false;
            tabControl1.SelectedIndex = 0;
            ENTITY_ID.Text = "";
            CUSTOMER_ESPECIFICACION_LINEA.Text = "";
            VALORES_DECIMALES.Text = "2";
            APROXIMACION.Text = "Redondear";
            //validarComplementos();
        }

        //private void validarComplementos()
        //{
            
            
        //    if (!cConfiguracion.cce)
        //    {
        //        tabControl1.TabPages.Remove(tabPage5);
        //        tabControl1.TabPages.Remove(tabPage6);
        //    }

        //}

        private void guardar()
        {
            ajax_loader.Visible = true;
            oObjeto.ENTITY_ID = ENTITY_ID.Text;
            oObjeto.BD_AUXILIAR = BD_AUXILIAR.Text;
            oObjeto.SITE_ID = SITE_ID.Text;
            oObjeto.ID = ID.Text;
            oObjeto.RFC = RFC.Text;
            oObjeto.CALLE = CALLE.Text;
            oObjeto.INTERIOR = INTERIOR.Text;
            oObjeto.EXTERIOR = EXTERIOR.Text;
            oObjeto.COLONIA = COLONIA.Text;
            oObjeto.CP = CP.Text;
            oObjeto.LOCALIDAD = LOCALIDAD.Text;
            oObjeto.REFERENCIA = REFERENCIA.Text;
            oObjeto.ESTADO = ESTADO.Text;
            oObjeto.PAIS = PAIS.Text;
            oObjeto.MUNICIPIO = MUNICIPIO.Text;
            oObjeto.SMTP = SMTP.Text;
            oObjeto.USUARIO = USUARIO.Text;
            oObjeto.PASSWORD_EMAIL = PASSWORD_EMAIL.Text;
            oObjeto.PUERTO = PUERTO.Text;
            oObjeto.SSL = SSL.Checked.ToString();
            oObjeto.CRENDENCIALES = CREDENCIALES.Checked.ToString();
            oObjeto.ACTIVO = ACTIVO.Checked.ToString();
            oObjeto.REGIMEN_FISCAL = REGIMEN_FISCAL.Text;
            oObjeto.CUSTOMER_ESPECIFICACION_LINEA = CUSTOMER_ESPECIFICACION_LINEA.Text;
            oObjeto.TIPO = TIPO.Text;
            oObjeto.SERVIDOR = SERVIDOR.Text;
            oObjeto.BD = BD.Text;
            oObjeto.USUARIO_BD = USUARIO_BD.Text;
            oObjeto.PASSWORD_BD = PASSWORD_BD.Text;
            oObjeto.BD_AUXILIAR = BD_AUXILIAR.Text;
            oObjeto.SITE_ID = SITE_ID.Text;
            oObjeto.CUSTOMER_ID = CUSTOMER_ID.Text;
            oObjeto.DESCUENTO_LINEA = DESCUENTO_LINEAS.Checked.ToString();
            oObjeto.ENVIO_CORREO = ENVIO_CORREO.Checked.ToString();
            oObjeto.AGREGAR_LOTE = AGREGAR_LOTE.Checked.ToString();
            oObjeto.IMPRESION_AUTOMATICA = IMPRESION_AUTOMATICA.Checked;
            oObjeto.IMPRESION_AUTOMATICA_NDC = IMPRESION_AUTOMATICA_NDC.Checked;
            oObjeto.ACTIVAR_COMPLEMENTO_CCE = ACTIVAR_COMPLEMENTO_CCE.Checked;
            oObjeto.PRIORIDAD_PART = PRIORIDAD_PART.Checked.ToString();
            oObjeto.ErrorCorreo= ErrorCorreo.Checked.ToString();
            oObjeto.PrioridadPartFiltro = PrioridadPartFiltro.Text;
            oObjeto.DetenerRelacionRMA = DetenerRelacionRMA.Checked;
            oObjeto.DetenerAutomaticoRelacionado = DetenerAutomaticoRelacionado.Checked;
            

            oObjeto.TipoRelacionRMA = TipoRelacionRMA.Text;

            oObjeto.USAR_COMPLEMENTO_ADDR3 = USAR_COMPLEMENTO_ADDR3.Checked.ToString();
            oObjeto.RAZON_SOLO_CLIENTE= RAZON_SOLO_CLIENTE.Checked;
            oObjeto.IMPRESORA = IMPRESORA.Text;
            oObjeto.CUSTOM_NOMBRE_CFDI = CUSTOM_NOMBRE_CFDI.Text;
            oObjeto.VALORES_DECIMALES = VALORES_DECIMALES.Text;
            oObjeto.APROXIMACION = APROXIMACION.Text;

            oObjeto.relacionLinea = relacionLinea.Checked;
            oObjeto.automatizacion = automatizacion.Checked;
            oObjeto.portal = portal.Text;

            oObjeto.UsarDescripcionPersonalizada = UsarDescripcionPersonalizada.Checked;
            oObjeto.UsarDescripcionPersonalizadaCampo = UsarDescripcionPersonalizadaCampo.Text;

            oObjeto.UsarCustomerPartDescripcionCliente = UsarCustomerPartDescripcionCliente.Text;
            oObjeto.UsarPartNotationCliente = UsarPartNotationCliente.Text;


            oObjeto.UsarCustomerPartDescripcion = UsarCustomerPartDescripcion.Checked;
            oObjeto.UsarPartNotation = UsarPartNotation.Checked;

            oObjeto.ActivarComplementoCartaPorte = ActivarComplementoCartaPorte.Checked;

            if (oObjeto.guardar())
            {
                toolStripStatusLabel1.Text = "Datos Actualizados " + DateTime.Now.ToShortDateString() + " " + DateTime.Now.ToLongTimeString();
                modificado = false;
            }
            else
            {
                toolStripStatusLabel1.Text = "Error " + DateTime.Now.ToShortDateString() + " " + DateTime.Now.ToLongTimeString();
            }

            ajax_loader.Visible = false;
        }

        private void cargar()
        {
            ajax_loader.Visible = true;
            if (oObjeto.cargar(oObjeto.ROW_ID))
            {
                ENTITY_ID.Text = oObjeto.ENTITY_ID;
                BD_AUXILIAR.Text = oObjeto.BD_AUXILIAR;
                SITE_ID.Text=oObjeto.SITE_ID;
                ID.Text = oObjeto.ID;
                RFC.Text = oObjeto.RFC;
                CALLE.Text = oObjeto.CALLE;
                INTERIOR.Text = oObjeto.INTERIOR;
                EXTERIOR.Text = oObjeto.EXTERIOR;
                COLONIA.Text = oObjeto.COLONIA;
                CP.Text = oObjeto.CP;
                LOCALIDAD.Text = oObjeto.LOCALIDAD;
                REFERENCIA.Text = oObjeto.REFERENCIA;
                ESTADO.Text = oObjeto.ESTADO;
                PAIS.Text = oObjeto.PAIS;
                MUNICIPIO.Text = oObjeto.MUNICIPIO;
                SMTP.Text = oObjeto.SMTP;
                USUARIO.Text = oObjeto.USUARIO;
                PASSWORD_EMAIL.Text = oObjeto.PASSWORD_EMAIL;
                PUERTO.Text = oObjeto.PUERTO;
                SSL.Checked = bool.Parse(oObjeto.SSL);
                CREDENCIALES.Checked = bool.Parse(oObjeto.CRENDENCIALES);
                ACTIVO.Checked = bool.Parse(oObjeto.ACTIVO);
                REGIMEN_FISCAL.Text = oObjeto.REGIMEN_FISCAL;
                ENVIO_CORREO.Checked = bool.Parse(oObjeto.ENVIO_CORREO);
                AGREGAR_LOTE.Checked = bool.Parse(oObjeto.AGREGAR_LOTE);
                TIPO.Text=oObjeto.TIPO;
                SERVIDOR.Text = oObjeto.SERVIDOR;
                BD.Text=oObjeto.BD;
                USUARIO_BD.Text=oObjeto.USUARIO_BD;
                PASSWORD_BD.Text=oObjeto.PASSWORD_BD;
                CUSTOMER_ESPECIFICACION_LINEA.Text = oObjeto.CUSTOMER_ESPECIFICACION_LINEA;
                DESCUENTO_LINEAS.Checked = bool.Parse(oObjeto.DESCUENTO_LINEA);
                PRIORIDAD_PART.Checked = false;
                try
                {
                    PRIORIDAD_PART.Checked = bool.Parse(oObjeto.PRIORIDAD_PART);
                }
                catch
                {

                }
                
                PrioridadPartFiltro.Text=oObjeto.PrioridadPartFiltro;

                USAR_COMPLEMENTO_ADDR3.Checked = false;
                try
                {
                    USAR_COMPLEMENTO_ADDR3.Checked = bool.Parse(oObjeto.USAR_COMPLEMENTO_ADDR3);
                }
                catch
                {

                }
                try
                {
                    ErrorCorreo.Checked = bool.Parse(oObjeto.ErrorCorreo);
                }
                catch
                {

                }

                DetenerRelacionRMA.Checked = oObjeto.DetenerRelacionRMA;
                TipoRelacionRMA.Text = oObjeto.TipoRelacionRMA;
                DetenerAutomaticoRelacionado.Checked = oObjeto.DetenerAutomaticoRelacionado;

                CUSTOM_NOMBRE_CFDI.Text = oObjeto.CUSTOM_NOMBRE_CFDI;
                UsarCustomerPartDescripcion.Checked = oObjeto.UsarCustomerPartDescripcion;
                CUSTOMER_ID.Text = oObjeto.CUSTOMER_ID;
                IMPRESION_AUTOMATICA.Checked = false;
                try
                {
                    IMPRESION_AUTOMATICA.Checked = oObjeto.IMPRESION_AUTOMATICA;
                }
                catch
                {
                }
                try
                {
                    IMPRESION_AUTOMATICA_NDC.Checked = oObjeto.IMPRESION_AUTOMATICA_NDC;
                }
                catch
                {
                }

                try
                {
                    ACTIVAR_COMPLEMENTO_CCE.Checked = oObjeto.ACTIVAR_COMPLEMENTO_CCE;
                }
                catch
                {
                }

                try
                {
                    relacionLinea.Checked = oObjeto.relacionLinea;
                }
                catch
                {
                }

                UsarDescripcionPersonalizada.Checked= oObjeto.UsarDescripcionPersonalizada;
                UsarDescripcionPersonalizadaCampo.Text = oObjeto.UsarDescripcionPersonalizadaCampo;

                UsarPartNotation.Checked = oObjeto.UsarPartNotation;

                VALORES_DECIMALES.Text = oObjeto.VALORES_DECIMALES;
                if (!Globales.IsNumeric(VALORES_DECIMALES.Text))
                {
                    VALORES_DECIMALES.Text = "0";
                }

                IMPRESORA.Text = oObjeto.IMPRESORA;

                automatizacion.Checked = false;
                try
                {
                    automatizacion.Checked = oObjeto.automatizacion;
                }
                catch
                {
                }
                RAZON_SOLO_CLIENTE.Checked = false;
                try
                {
                    RAZON_SOLO_CLIENTE.Checked = oObjeto.RAZON_SOLO_CLIENTE;
                }
                catch
                {
                }

                try
                {
                    UsarCustomerPartDescripcionCliente.Text = oObjeto.UsarCustomerPartDescripcionCliente;
                }
                catch
                {
                }

                try
                {
                    UsarPartNotationCliente.Text = oObjeto.UsarPartNotationCliente;
                }
                catch
                {
                }

                try
                {
                    ActivarComplementoCartaPorte.Checked = oObjeto.ActivarComplementoCartaPorte;
                }
                catch
                {
                }

                try
                {
                    ErrorCorreo.Checked = bool.Parse(oObjeto.ErrorCorreo);
                }
                catch
                {
                }

                portal.Text=oObjeto.portal;

                toolStripStatusLabel1.Text = "Datos Cargados " + DateTime.Now.ToShortDateString() + " " + DateTime.Now.ToLongTimeString();
                modificado = false;

            }
            ajax_loader.Visible = false;
        }

        private void button3_Click(object sender, EventArgs e)
        {
            guardar();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            limpiar();
        }

        private void frmUsuario_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (modificado)
            {
                DialogResult Resultado = MessageBox.Show("¿Desea guardar los cambios?", Application.ProductName + "-" + Application.ProductVersion, MessageBoxButtons.YesNo);
                if (Resultado.ToString() == "Yes")
                {
                    guardar();
                }
            }
        }

        private void frmUsuario_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)27)
            {
                this.Close();
            }
            else
            {
                modificado = true;
                if ((e.KeyChar.ToString() == "<")
                                | (e.KeyChar.ToString() == ">")
                                
                                | (e.KeyChar.ToString() == "'")
                                | (e.KeyChar.ToString() == "#")
                                )
                {
                    e.Handled = true;
                }
            }
        }

        private void Texto_Cambiado_TextChanged(object sender, EventArgs e)
        {
            modificado = true;
        }

        private void Solo_Numero_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsDigit(e.KeyChar) && !char.IsControl(e.KeyChar) && !char.IsPunctuation(e.KeyChar)) e.Handled = true;
        }


        public void enviar_mail()
        {
            try
            {
                pictureBox1.Visible = true;
                label6.Visible = true;
                string FROM_CORREO = USUARIO.Text;
                //frmEntradaEmail ObjetoFROM = new frmEntradaEmail();
                //ObjetoFROM.Text = "Correo de Origen";
                //if (ObjetoFROM.ShowDialog(this) == DialogResult.OK)
                //{
                //    FROM_CORREO = ObjetoFROM.EMAIL;
                //}
                //else
                //{
                //    MessageBox.Show("Acción cancelada.", Application.ProductName + "-" + Application.ProductVersion, MessageBoxButtons.OK, MessageBoxIcon.Information);
                //    pictureBox1.Visible = false;
                //    label6.Visible = false;
                //    return;
                //}
                MailAddress from = new MailAddress(FROM_CORREO,
                               "Prueba de Facturacion Electronica",
                                System.Text.Encoding.UTF8);
                //Destinatarios.
                frmEntradaEmail Objeto = new frmEntradaEmail();
                Objeto.Text = "Correo Destino";
                if (Objeto.ShowDialog(this) == DialogResult.OK)
                {
                    MailAddress to = new MailAddress(Objeto.EMAIL);
                    // Specify the message content.
                    MailMessage message = new MailMessage(from, to);
                    message.Body = "Esta es una prueba de envio de Facturación Electrónica. Fecha y Hora: " + DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss");
                    message.BodyEncoding = System.Text.Encoding.UTF8;
                    message.Subject = "Prueba de Facturación Electrónica";
                    message.SubjectEncoding = System.Text.Encoding.UTF8;
                    message.IsBodyHtml = true;
                    message.Priority = MailPriority.High;

                    //Configurar SMPT
                    SmtpClient client = new SmtpClient(SMTP.Text, int.Parse(PUERTO.Text));
                    client.SendCompleted += new SendCompletedEventHandler(client_SendCompleted);
                    //Configurar Crendiciales de Salida
                    client.Host = SMTP.Text;
                    client.Port = int.Parse(PUERTO.Text);
                    client.DeliveryMethod = SmtpDeliveryMethod.Network;

                    client.UseDefaultCredentials = bool.Parse(CREDENCIALES.Checked.ToString());
                    client.EnableSsl = bool.Parse(SSL.Checked.ToString());
                    client.Timeout = 20000;
                    System.Net.NetworkCredential credenciales = new System.Net.NetworkCredential(USUARIO.Text, PASSWORD_EMAIL.Text);
                    client.Credentials = credenciales;
                    toolStripStatusLabel1.Text = "Enviando Mail a " + DateTime.Now.ToShortDateString() + " " + DateTime.Now.ToLongTimeString();

                    object userState = message;
                    try
                    {
                        client.SendAsync(message, userState);
                    }
                    catch (System.Net.Mail.SmtpException ex)
                    {
                        MessageBox.Show(ex.Message + " " + ex.Data.ToString() + " " + ex.TargetSite.ToString(), Application.ProductName + "-" + Application.ProductVersion, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                        toolStripStatusLabel1.Text = "Error de Envio " + DateTime.Now.ToShortDateString() + " " + DateTime.Now.ToLongTimeString();
                    }
                }
                else
                {
                    MessageBox.Show("Acción cancelada.", Application.ProductName + "-" + Application.ProductVersion, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                    pictureBox1.Visible = false;
                    label6.Visible = false;
                    return;
                }
            }
            catch(Exception err)
            {
                ErrorFX.mostrar(err, true, true, "frmEmpresa - 444 - ", false);
            }

        }

        public void client_SendCompleted(object sender, AsyncCompletedEventArgs e)
        {
            MailMessage mail = (MailMessage)e.UserState;


            if (e.Error != null)
            {
                MessageBox.Show(e.Error.ToString(), Application.ProductName + "-" + Application.ProductVersion, MessageBoxButtons.OK, MessageBoxIcon.Error);
                toolStripStatusLabel1.Text = e.Error.Message + DateTime.Now.ToShortDateString() + " " + DateTime.Now.ToLongTimeString();
            }
            else
            {
                toolStripStatusLabel1.Text = "Correo Enviado " + DateTime.Now.ToShortDateString() + " " + DateTime.Now.ToLongTimeString();
            }
            pictureBox1.Visible = false;
            label6.Visible = false;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            enviar_mail();
        }

        private void Verificar(object sender, KeyPressEventArgs e)
        {
            modificado = true;
            if ((e.KeyChar.ToString() == "<")
                            | (e.KeyChar.ToString() == ">")
                            
                            | (e.KeyChar.ToString() == "'")
                            | (e.KeyChar.ToString() == "#")
                            )
            {
                e.Handled = true;
            }
        }

        private void frmEmpresa_Load(object sender, EventArgs e)
        {
            cConfiguracion.cargar();
            //if (!cConfiguracion.cce)
            //{
            //    tabControl1.TabPages.Remove(tabPage6);
            //    tabControl1.TabPages.Remove(tabPage5);
            //}
        }

        private void button4_Click(object sender, EventArgs e)
        {
            abrir_configuracion_cfdi();
        }

        private void abrir_configuracion_cfdi()
        {
            Encapsular oEncapsular = new Encapsular();
            oEncapsular.oData_ERP=oData_ERP;
            oEncapsular.ocEMPRESA=oObjeto;

        frmCambio_Password oBuscador = new frmCambio_Password(oData.sConn, oEncapsular,oObjeto.ROW_ID);
            oBuscador.ShowDialog();
        }

        private void button5_Click(object sender, EventArgs e)
        {
            probar();
        }

        public void probar()
        {
            cCONEXCION ocCONEXCION = new cCONEXCION(TIPO.Text, SERVIDOR.Text, BD.Text, USUARIO_BD.Text, PASSWORD_BD.Text);
            if (ocCONEXCION.CrearConexion())
            {
                MessageBox.Show("Conexión exitosa");
            }
            else
            {
                MessageBox.Show("Verifique el server.");
            }
        }

        private void button7_Click(object sender, EventArgs e)
        {
            eliminar();
        }

        private void eliminar()
        {
            DialogResult Resultado = MessageBox.Show("¿Esta seguro de eliminar?", Application.ProductName + "-" + Application.ProductVersion, MessageBoxButtons.YesNo);
            if (Resultado.ToString() != "Yes")
            {
                return;
            }
            if (oObjeto.eliminar(oObjeto.ROW_ID))
            {
                limpiar();
            }
        }

        private void button10_Click(object sender, EventArgs e)
        {
            duplicar();
        }

        private void duplicar()
        {
            oObjeto.ROW_ID = "";
            oObjeto.ID = "";
            ID.Text = "";
        }

        private void button8_Click(object sender, EventArgs e)
        {
            cCONEXCION ocCONEXCION = new cCONEXCION(TIPO.Text, SERVIDOR.Text, BD.Text, USUARIO_BD.Text, PASSWORD_BD.Text);
            frmCUSTOMER ofrmCUSTOMER = new frmCUSTOMER(ocCONEXCION.sConn, "S");
            if(ofrmCUSTOMER.ShowDialog()==DialogResult.OK)
            {
                CUSTOMER_ID.Text=ofrmCUSTOMER.selecionados[0].Cells["ID"].Value.ToString();
            }
        }

        private void button8_Click_1(object sender, EventArgs e)
        {
            cargar_entidades();
        }

        private void cargar_entidades()
        {
            try
            {
                oData_ERP = new cCONEXCION(TIPO.Text, SERVIDOR.Text, BD.Text, USUARIO_BD.Text, PASSWORD_BD.Text);
                ENTITY_ID.Items.Clear();
                string sSQL = " select * from information_schema.tables where table_name = 'ENTITY'";
                DataTable oDataTable1 = oData_ERP.EjecutarConsulta(sSQL);
                sSQL = "SELECT * FROM ENTITY";
                if (oDataTable1.Rows.Count == 0)
                {
                    sSQL = "SELECT * FROM SITE";
                }



                DataTable oDataTable = oData_ERP.EjecutarConsulta(sSQL);
                foreach (DataRow oDataRow in oDataTable.Rows)
                {
                    ENTITY_ID.Items.Add(oDataRow["ID"].ToString());
                }
                if (ENTITY_ID.SelectedIndex > 0)
                {
                    ENTITY_ID.SelectedIndex = 0;
                }
            }
            catch(Exception e)
            {
                ErrorFX.mostrar(e, false, true, "frmEmpresa - 521 - ", false);
            }

        }

        private void button9_Click(object sender, EventArgs e)
        {
            oData_ERP = new cCONEXCION(TIPO.Text, SERVIDOR.Text, BD.Text, USUARIO_BD.Text, PASSWORD_BD.Text);
            frmCliente_Asuntos ofrmCliente_Asuntos = new frmCliente_Asuntos(oData.sConn,oData_ERP.sConn);
            ofrmCliente_Asuntos.ShowDialog();
        }

        private void checkBox1_CheckedChanged(object sender, EventArgs e)
        {
            PASSWORD_EMAIL.UseSystemPasswordChar = !checkBox1.Checked;

        }

        private void verPassword_CheckedChanged(object sender, EventArgs e)
        {
            if (verPassword.Checked)
            {
                PASSWORD_BD.UseSystemPasswordChar = false;
            }
            else{
                PASSWORD_BD.UseSystemPasswordChar = true;
            }
            
        }

        private void UsarCustomerPartDescripcion_CheckedChanged(object sender, EventArgs e)
        {
            cambiarEstadoUsarCustomerPartDescripcion();
        }

        private void cambiarEstadoUsarPartNotation()
        {
            UsarPartNotationCliente.Enabled = UsarPartNotation.Checked;

            if (!UsarPartNotation.Checked)
            {
                UsarPartNotationCliente.Text = string.Empty;
            }
        }

        private void cambiarEstadoUsarCustomerPartDescripcion()
        {
            UsarCustomerPartDescripcionCliente.Enabled = UsarCustomerPartDescripcion.Checked;

            if (!UsarCustomerPartDescripcion.Checked)
            {
                UsarCustomerPartDescripcionCliente.Text = string.Empty;
            }
        }

        private void UsarPartNotation_CheckedChanged(object sender, EventArgs e)
        {
            cambiarEstadoUsarPartNotation();
        }

        private void button11_Click(object sender, EventArgs e)
        {
            RealizarPruebaError();
        }

        private void RealizarPruebaError()
        {
            ErrorFX.sendSMTPMail("Error: Mensaje de error", "Error: Mensaje de error");
        }
    }
}