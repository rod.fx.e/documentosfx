﻿using Spire.Pdf;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Drawing.Printing;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace FE_FX
{
    public partial class frmPDF : Form
    {
        string archivop;
        private int _zoom = 150;
        private bool _isZoomDynamic = false;

        public frmPDF(string archivo)
        {
            InitializeComponent();
            archivop = archivo;
            //this.pdfDocumentViewer1.ZoomFator = 1f;
            this.pdfDocumentViewer1.LoadFromFile(archivo);
            toolStripTextBox1.Text = archivo;

            int[] intZooms = new Int32[] { 25, 50, 75, 100, 125, 150, 200, 300, 400, 500, 600, 700, 800, 900, 1000 };
            foreach (int zoom in intZooms)
            {
                this.comBoxZoom.Items.Add(zoom.ToString());
            }
            this.comBoxZoom.SelectedIndex = 5;
            this.pdfDocumentViewer1.MouseWheel += new MouseEventHandler(this.pdfDocumentViewer1_MouseWheel);
            this.pdfDocumentViewer1.LostFocus += new EventHandler(this.pdfDocumentViewer_LostFocus);

        }
        private void pdfDocumentViewer_LostFocus(Object sender, EventArgs args)
        {
            this._isZoomDynamic = false;
            this._zoom = 150;
        }

        private void pdfDocumentViewer1_MouseWheel(Object sender, MouseEventArgs args)
        {
            if (this._isZoomDynamic)
            {
                int wheelValue = (Int32)args.Delta / 24;
                this._zoom += wheelValue;
                if (this._zoom < 0)
                    this._zoom = 0;
                this.pdfDocumentViewer1.ZoomTo(this._zoom);
            }
        }

        private void comBoxZoom_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (this.pdfDocumentViewer1.PageCount > 0)
            {
                int zoomValue = Int32.Parse(this.comBoxZoom.SelectedItem.ToString());
                this.pdfDocumentViewer1.ZoomTo(zoomValue);
            }
        }

        private void btnZoomOut_Click(object sender, EventArgs e)
        {
            if (this.pdfDocumentViewer1.PageCount > 0)
            {
                int delta = 10;
                this._zoom += delta;
                this.pdfDocumentViewer1.ZoomTo(this._zoom);
            }
        }

        private void btnZoonIn_Click(object sender, EventArgs e)
        {
            if (this.pdfDocumentViewer1.PageCount > 0)
            {
                int delta = 5;
                this._zoom -= delta;
                if (this._zoom < 0)
                    this._zoom = 0;
                this.pdfDocumentViewer1.ZoomTo(this._zoom);
            }
        }

        private void toolStripButton1_Click(object sender, EventArgs e)
        {
            pdfDocumentViewer1.Print();
        }

        private void toolStripButton2_Click(object sender, EventArgs e)
        {
            using (var fbd = new FolderBrowserDialog())
            {
                DialogResult result = fbd.ShowDialog();

                if (result == DialogResult.OK && !string.IsNullOrEmpty(fbd.SelectedPath))
                {
                    string archivoTMP = Path.GetFileName(archivop);
                    pdfDocumentViewer1.SaveToFile(fbd.SelectedPath + @"\" + archivoTMP);
                }
            }

        }

        private void frmPDF_Resize(object sender, EventArgs e)
        {
            pdfDocumentViewer1.Height = this.Height - toolStrip1.Height;
        }

        private void toolStripButton1_Click_1(object sender, EventArgs e)
        {
            //if (this.pdfDocumentViewer1.PageCount > 0)
            //{
            //    this.pdfDocumentViewer1.Print();
            //}
            PdfDocument doc = new PdfDocument();
            doc.LoadFromFile(archivop);
            PrintDialog dialogPrint = new PrintDialog();
            dialogPrint.AllowPrintToFile = true;
            dialogPrint.AllowSomePages = true;
            dialogPrint.PrinterSettings.MinimumPage = 1;
            dialogPrint.PrinterSettings.MaximumPage = doc.Pages.Count;
            dialogPrint.PrinterSettings.FromPage = 1;
            dialogPrint.PrinterSettings.ToPage = doc.Pages.Count;
            if (dialogPrint.ShowDialog() == DialogResult.OK)
            {
                doc.PrintFromPage = dialogPrint.PrinterSettings.FromPage;
                doc.PrintToPage = dialogPrint.PrinterSettings.ToPage;
                doc.PrinterName = dialogPrint.PrinterSettings.PrinterName;
                PrintDocument printDoc = doc.PrintDocument;
                dialogPrint.Document = printDoc;
                printDoc.Print();
            }
        }

        private void toolStripButton2_Click_1(object sender, EventArgs e)
        {
            using (var fbd = new FolderBrowserDialog())
            {
                DialogResult result = fbd.ShowDialog();

                if (result == DialogResult.OK && !string.IsNullOrEmpty(fbd.SelectedPath))
                {
                    string archivoTMP = Path.GetFileName(archivop);
                    pdfDocumentViewer1.SaveToFile(fbd.SelectedPath + @"\" + archivoTMP);
                }
            }
        }

        private void toolStripButton3_Click(object sender, EventArgs e)
        {
            try
            {
                string directorio = Path.GetDirectoryName(toolStripTextBox1.Text);
                Process.Start(directorio);
            }
            catch
            {

            }
        }

        private void frmPDF_Load(object sender, EventArgs e)
        {

        }
    }
}
