#region Copyright Syncfusion Inc. 2001 - 2017
// Copyright Syncfusion Inc. 2001 - 2017. All rights reserved.
// Use of this code is subject to the terms of our license.
// A copy of the current license can be obtained at any time by e-mailing
// licensing@syncfusion.com. Any infringement will be prosecuted under
// applicable laws. 
#endregion
using FE_FX.Clases;
using FE_FX.EDICOM_Servicio;
using Generales;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Windows.Forms;
using System.Xml;
using System.Xml.Serialization;

namespace FE_FX.CFDI
{
    public partial class frmTraslados : Syncfusion.Windows.Forms.MetroForm
    {
        public frmTraslados()
        {
            InitializeComponent();
            Application.DoEvents();
            cargarEmpresas();
        }

        cCONEXCION oData_ERP = new cCONEXCION("");
        cEMPRESA ocEMPRESA = new cEMPRESA();
        cSERIE ocSERIE = new cSERIE();
        cCERTIFICADO ocCERTIFICADO = new cCERTIFICADO();
        ComboItemReceptor[] receptor;

        private void cargarEmpresas()
        {
            emisor.Items.Clear();
            cEMPRESA ocEMPRESA = new cEMPRESA();
            foreach (cEMPRESA registro in ocEMPRESA.todos("", true))
            {
                emisor.Items.Add(registro);
            }
            if (emisor.Items.Count > 0)
            {
                emisor.SelectedIndex = 0;
            }
        }
        private void cargarSerie()
        {
            try
            {
                serie.Text = "";
                ocEMPRESA = (cEMPRESA)emisor.SelectedItem;
                oData_ERP = new cCONEXCION(ocEMPRESA.TIPO, ocEMPRESA.SERVIDOR
                                   , ocEMPRESA.BD, ocEMPRESA.USUARIO_BD, ocEMPRESA.PASSWORD_BD);

                foreach (cSERIE registro in ocSERIE.todos_empresa("", ocEMPRESA.ROW_ID, false, true))
                {
                    ocCERTIFICADO = new cCERTIFICADO();
                    ocCERTIFICADO.cargar(registro.ROW_ID_CERTIFICADO);
                    ocSERIE = registro;
                    serie.Text = ocSERIE.ID;
                    directorio.Text = ocSERIE.DIRECTORIO;
                    cargar();
                    return;
                }
            }
            catch (Exception e)
            {
                ErrorFX.mostrar(e, true, true, "frmTraslado - 113 ");
            }
        }
        private void button2_Click(object sender, EventArgs e)
        {
            nuevo();
        }

        #region metodos

        private void cancelar()
        {
            try
            {
                if (dtgrdGeneral.SelectedRows.Count==0)
                {
                    MessageBox.Show("Seleccione los traslados a cancelar."
                            , Application.ProductName + "-" + Application.ProductVersion, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                    return;
                }
                string[] uuidTr = new string[dtgrdGeneral.SelectedRows.Count];
                int i = 0;
                foreach (DataGridViewRow drv in dtgrdGeneral.SelectedRows)
                {
                    uuidTr[i] = drv.Cells["uuid"].Value.ToString();
                    i++;
                }

                //Llamar el Servicio
                CFDiClient oCFDiClient = new CFDiClient();
                string pfx = ocCERTIFICADO.PFX;
                if (!File.Exists(pfx))
                {
                    //Validar el directorio
                    string directorio = AppDomain.CurrentDomain.BaseDirectory;
                    pfx = directorio + @"\" + ocCERTIFICADO.PFX;
                    if (!File.Exists(pfx))
                    {
                        MessageBox.Show("El PFX " + ocCERTIFICADO.PFX + " no existe."
                            , Application.ProductName + "-" + Application.ProductVersion, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                        return;
                    }
                }
                byte[] pfx_archivo = ReadBinaryFile(pfx);
                try
                {
                    CancelaResponse respuesta;
                    //Todo: Realizar cancelacion
                    //respuesta = oCFDiClient.cancelaCFDi(Globales.oCFDI_USUARIO.USUARIO, cENCRIPTACION.Decrypt(Globales.oCFDI_USUARIO.PASSWORD)
                    //    , ocEMPRESA.RFC, uuidTr, pfx_archivo, ocCERTIFICADO.PASSWORD);
                    MessageBox.Show("Traslados cancelados"
                        , Application.ProductName + "-" + Application.ProductVersion, MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                catch (FaultException oException)
                {
                    CFDiException oCFDiException = new CFDiException();
                    MessageBox.Show(oException.Message, Application.ProductName + "-" + Application.ProductVersion, MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }
            }
            catch (Exception e)
            {
                ErrorFX.mostrar(e, true, true, "frmTraslado - 113 ");
            }
        }
        public byte[] ReadBinaryFile(string fileName)
        {
            if (File.Exists(fileName))
            {
                try
                {
                    ///Open and read a file&#12290;
                    FileStream fileStream = File.OpenRead(fileName);
                    byte[] archivo = ConvertStreamToByteBuffer(fileStream);
                    fileStream.Close();
                    return archivo;
                }
                catch (Exception ex)
                {
                    return new byte[0];
                }
            }
            else
            {
                return new byte[0];
            }
        }

        public byte[] ConvertStreamToByteBuffer(System.IO.Stream theStream)
        {
            int b1;
            System.IO.MemoryStream tempStream = new System.IO.MemoryStream();
            while ((b1 = theStream.ReadByte()) != -1)
            {
                tempStream.WriteByte(((byte)b1));
            }
            return tempStream.ToArray();
        }


        private void nuevo()
        {
            frmTraslado o = new frmTraslado(oData_ERP,receptor, null);
            o.Show();
        }

        private void cargar()
        {
            return;
            dtgrdGeneral.Rows.Clear();
            if (!Directory.Exists(directorio.Text))
            {
                return;
            }

            dsCfdi.datosCFDIDataTable tabla = new dsCfdi.datosCFDIDataTable();
            //Cargar archivos
            foreach (string archivo in Directory.GetFiles(directorio.Text, "*.xml", SearchOption.AllDirectories))
            {
                try
                {
                    XmlSerializer serializer_CFD_3 = new XmlSerializer(typeof(CFDI33.Comprobante));
                    TextReader reader = new StreamReader(archivo);
                    CFDI33.Comprobante oComprobante;
                    oComprobante = (CFDI33.Comprobante)serializer_CFD_3.Deserialize(reader);
                    if (oComprobante.TipoDeComprobante.Equals("T"))
                    {
                        XmlDocument docXML = new XmlDocument();
                        docXML.Load(archivo);
                        XmlNodeList TimbreFiscalDigital = docXML.GetElementsByTagName("tfd:TimbreFiscalDigital");
                        string TimbreFiscalDigitaltr = "";
                        if (TimbreFiscalDigital != null)
                        {
                            if (TimbreFiscalDigital[0] != null)
                            {
                                TimbreFiscalDigitaltr = TimbreFiscalDigital[0].Attributes["UUID"].Value;
                            }
                        }

                        tabla.AdddatosCFDIRow(TimbreFiscalDigitaltr
                            , oComprobante.Total, oComprobante.Receptor.Rfc,oComprobante.Receptor.Nombre
                            , oComprobante.Serie, oComprobante.Folio, archivo, oComprobante.Fecha);
                    }
                }
                catch
                {

                }
            }
            DateTime iniciop = new DateTime(inicio.Value.Year, inicio.Value.Month, inicio.Value.Day, 0, 0, 0);
            DateTime finp = new DateTime(fin.Value.Year, fin.Value.Month, fin.Value.Day, 23, 59, 59);

            //Cargar tabla
            var dataQuery =
                        from tr in tabla
                        where tr.fecha >= iniciop & tr.fecha <= finp
                        select tr;
            foreach (dsCfdi.datosCFDIRow r in dataQuery)
            {
                int n = dtgrdGeneral.Rows.Add();
                dtgrdGeneral.Rows[n].Cells["archivo"].Value = r.archivo;
                dtgrdGeneral.Rows[n].Cells["folio"].Value = r.serie+r.folio;
                dtgrdGeneral.Rows[n].Cells["fecha"].Value = r.fecha;
                dtgrdGeneral.Rows[n].Cells["rfcReceptor"].Value = r.receptorRfc;
                dtgrdGeneral.Rows[n].Cells["nombre"].Value = r.receptorNombre;
                dtgrdGeneral.Rows[n].Cells["UUID"].Value = r.uuid;
            }

            dtgrdGeneral.AutoResizeColumns(DataGridViewAutoSizeColumnsMode.AllCells);

            //Cargar receptores
            var receptorTr = tabla.GroupBy(x => new { x.receptorRfc, x.receptorNombre })
                                .Select(y => new ComboItem()
                                {
                                    Name = y.Key.receptorNombre,
                                    Value = y.Key.receptorRfc
                                }
                                );
            int contador = 0;
            foreach (var x in receptorTr)
            {
                contador++;
            }
            receptor = new ComboItemReceptor[contador];
            contador = 0;
            foreach (var x in receptorTr)
            {
                receptor[contador] = new ComboItemReceptor();
                receptor[contador].Name = x.Name;
                receptor[contador].Value = x.Value;
                contador++;
            }

        }

        #endregion

        private void button1_Click(object sender, EventArgs e)
        {
            cargar();
        }

        private void emisor_SelectedIndexChanged(object sender, EventArgs e)
        {
            cargarSerie();
        }

        private void dtgrdGeneral_CellMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {

        }

        private void dtgrdGeneral_MouseClick(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Right)
            {
                contextMenuStrip1.Show(dtgrdGeneral, new Point(e.X, e.Y));
            }
        }

        private void cancelarToolStripMenuItem_Click(object sender, EventArgs e)
        {
            cancelar();
        }

        private void verXMLToolStripMenuItem_Click(object sender, EventArgs e)
        {
            verXML();
        }

        private void verXML()
        {
            try
            {
                if (dtgrdGeneral.SelectedRows.Count == 0)
                {
                    MessageBox.Show("Seleccione los traslados a mostrar."
                            , Application.ProductName + "-" + Application.ProductVersion, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                    return;
                }
                foreach (DataGridViewRow drv in dtgrdGeneral.SelectedRows)
                {
                    frmFacturaXML ofrmPDF = new frmFacturaXML(drv.Cells["archivo"].Value.ToString());
                    ofrmPDF.Show();
                    return;
                }
                
            }
            catch (Exception e)
            {
                ErrorFX.mostrar(e, true, true, "frmTraslados - 278 ");
            }
        }

        private void verPDF()
        {
            try
            {
                if (dtgrdGeneral.SelectedRows.Count == 0)
                {
                    MessageBox.Show("Seleccione los traslados a mostrar."
                            , Application.ProductName + "-" + Application.ProductVersion, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                    return;
                }
                foreach (DataGridViewRow drv in dtgrdGeneral.SelectedRows)
                {
                    frmPDF ofrmPDF = new frmPDF(drv.Cells["archivo"].Value.ToString().Replace("xml","pdf"));
                    ofrmPDF.Show();
                    return;
                }
            }
            catch (Exception e)
            {
                ErrorFX.mostrar(e, true, true, "frmTraslados - 291 ");
            }
        }

        private void verPDFToolStripMenuItem_Click(object sender, EventArgs e)
        {
            verPDF();
        }

        private void dtgrdGeneral_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            cargarCfdi(e);
        }

        private void cargarCfdi(DataGridViewCellEventArgs e)
        {
            try
            {
                string archivo = dtgrdGeneral.Rows[e.RowIndex].Cells["archivo"].Value.ToString();
                frmTraslado ofrmTraslado = new frmTraslado(oData_ERP,receptor, archivo);
                ofrmTraslado.Show();
            }
            catch (Exception error)
            {
                ErrorFX.mostrar(error, true, true, "frmTraslados - 335 ");
            }
        }

        private void button4_Click(object sender, EventArgs e)
        {            
            imprimir();
        }

        private void imprimir()
        {            
            cTraslado ocTraslado = new cTraslado();
            ocTraslado.imprimir("", true);
        }
    }
}
