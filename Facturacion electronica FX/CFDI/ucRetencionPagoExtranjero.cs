﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace FE_FX.CFDI
{
    public partial class ucRetencionPagoExtranjero : UserControl
    {
        public ucRetencionPagoExtranjero()
        {
            InitializeComponent();

            //Cargar Pais
            PaisDeResidParaEfecFisc.DataSource = Enum.GetNames(typeof(c_Paispagoextranjero));
            //ConceptoPago.DataSource = Enum.GetNames(typeof(c_TipoContribuyenteSujetoRetencion));
            PaisDeResidParaEfecFisc.Text = "US";
        }
    }
}
