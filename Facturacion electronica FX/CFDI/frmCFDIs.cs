﻿using CFDI32;
using DevComponents.DotNetBar;
using DevComponents.DotNetBar.Controls;
using FE_FX.CFDI;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Globalization;
using System.IO;
using System.Text;
using System.Windows.Forms;
using System.Xml;
using System.Xml.Serialization;
using Generales;

namespace FE_FX
{
    public partial class frmCFDIs : Form
    {
        private cCONEXCION oData = new cCONEXCION("");
        string devolver = "";
        public DataGridViewSelectedRowCollection selecionados;
        string tipo = "";
        private cEMPRESA ocEMPRESA;
        private cSERIE ocSERIE;
        private cCONEXCION oData_ERP = new cCONEXCION("");
        private CFDI.cPolizaConfiguracion ocPolizaConfiguracion = new CFDI.cPolizaConfiguracion();
        string sDirectory = AppDomain.CurrentDomain.BaseDirectory + "cPolizaConfiguracion";

        public frmCFDIs(string sConn, string usuariop)
        {
            oData.sConn = sConn;
            InitializeComponent();
            cargar_empresas();
            cargarConfiguracion();

            cUSUARIO ocUSUARIO = new cUSUARIO();
            if (ocUSUARIO.cargar_ID(usuariop))
            {
                if (ocUSUARIO.proformaConfiguracion != null)
                {
                    if (ocUSUARIO.proformaConfiguracion.Contains("False"))
                    {
                        tabControl1.TabPages.Remove(tabPage3);
                    }
                }
            }

            var americanCulture = new CultureInfo("es-MX");
            mes.Items.Clear();
            foreach(string mestr in americanCulture.DateTimeFormat.MonthNames)
            {
                if (mestr != "")
                {
                    mes.Items.Add(FirstLetterToUpper(mestr));
                }
            }
            string monthName = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day)
    .ToString("MMMM", americanCulture);
            mes.Text = monthName;

            año.Text = DateTime.Now.Year.ToString();
            año.Items.Clear();
            año.Items.Add(DateTime.Now.Year - 1);
            año.Items.Add(DateTime.Now.Year);
            año.Items.Add(DateTime.Now.Year +1);
            año.Items.Add(DateTime.Now.Year + 2);
            año.Items.Add(DateTime.Now.Year + 3);
            año.Items.Add(DateTime.Now.Year + 4);
            
        }

        private void cargarConfiguracion()
        {
            
            XmlSerializer serializer = new XmlSerializer(typeof(cPolizaConfiguracion));
            try
            {
                TextReader reader = new StreamReader(sDirectory);
                ocPolizaConfiguracion = (cPolizaConfiguracion)serializer.Deserialize(reader);
                reader.Close();
                accountAbonoImpuesto.Text = ocPolizaConfiguracion.AccountAbonoImpuesto;
                accountAbonoTotal.Text = ocPolizaConfiguracion.AccountAbonoTotal;
                accountCargoImpuesto.Text = ocPolizaConfiguracion.AccountCargoImpuesto;
                accountCargoTotal.Text = ocPolizaConfiguracion.AccountCargoTotal;
                comentarioDescuento.Text = ocPolizaConfiguracion.ComentarioDescuento;
                directorio.Text = ocPolizaConfiguracion.Directorio;
                serieDefecto.Text = ocPolizaConfiguracion.SerieDefecto;
                formatoDefecto.Text = ocPolizaConfiguracion.FormatoDefecto;
            }
            catch(Exception e)
            {

            }

        }
        private void cargar_empresas()
        {
            ENTITY_ID.Items.Clear();
            cEMPRESA ocEMPRESA = new cEMPRESA(oData);
            foreach (cEMPRESA registro in ocEMPRESA.todos("", true))
            {
                ENTITY_ID.Items.Add(registro);
            }
            if (ENTITY_ID.Items.Count > 0)
            {
                ENTITY_ID.SelectedIndex = 0;
            }
        }

        private void cargar()
        {
            int n;
            cADDENDA oObjeto = new cADDENDA();
            toolStripStatusLabel1.Text = "Cargando ";
            dtgrdGeneral.Rows.Clear();
            toolStripStatusLabel1.Text = "Total " + dtgrdGeneral.Rows.Count.ToString();

        }

        private void button1_Click(object sender, EventArgs e)
        {
            cargar();
        }

        private void BUSCAR_KeyPress(object sender, KeyPressEventArgs e)
        {
            //Precionar Enter buscar
            if (e.KeyChar.ToString() == "\r")
            {
                cargar();
            }
            
        }

        private void frmUsuarios_Load(object sender, EventArgs e)
        {
            cargar();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            frmAddenda oObjeto = new frmAddenda(oData.sConn);
            oObjeto.ShowDialog();
            cargar();
        }

        private void dtgrdGeneral_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            modificar();
        }

        public void modificar()
        {
            if (dtgrdGeneral.SelectedRows.Count > 0)
            {
                DataGridViewSelectedRowCollection arrSelectedRows = dtgrdGeneral.SelectedRows;
                string ARCHIVO = arrSelectedRows[0].Cells["ARCHIVO"].Value.ToString();
                string directorio = AppDomain.CurrentDomain.BaseDirectory;
                frmFacturaXML oObjeto = new frmFacturaXML(ARCHIVO);
                oObjeto.Show();
            }
        }

        private void frmFormatos_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)27)
            {
                this.Close();
            }
            else
            {
                if ((e.KeyChar.ToString() == "<")
                                | (e.KeyChar.ToString() == ">")
                                
                                | (e.KeyChar.ToString() == "'")
                                | (e.KeyChar.ToString() == "#")
                                )
                {
                    e.Handled = true;
                }
            }
        }

        private void buttonX2_Click(object sender, EventArgs e)
        {
            aplicarDescuento ofrmCFDI = new aplicarDescuento(oData.sConn, ENTITY_ID.Text,ENTITY_ID.SelectedIndex);
            ofrmCFDI.ShowDialog();
            cargar();
        }

        public string FirstLetterToUpper(string str)
        {
            if (str == null)
                return str;

            if (str.Length > 1)
                return char.ToUpper(str[0]) + str.Substring(1);

            return str.ToUpper();
        }

        private void buttonX3_Click(object sender, EventArgs e)
        {
            buscarSerie();
        }

        private void buscarSerie()
        {

            frmSeries oBuscador = new frmSeries(oData.sConn, "S", ocEMPRESA.ROW_ID);
            if (oBuscador.ShowDialog() == DialogResult.OK)
            {
                string ROW_ID = (string)oBuscador.selecionados[0].Cells["ROW_ID"].Value;
                string IDtr = (string)oBuscador.selecionados[0].Cells["ID"].Value;
               

                ocSERIE = new cSERIE();

                ocSERIE.cargar_serie(IDtr);
                ID.Text = ocSERIE.ID;
                
                //ID.Text = ocSERIE.consecutivo(ocSERIE.ROW_ID, false);
                cargar_archivos();

            }
        }

        private void cargar_archivos()
        {
            if (ocSERIE != null)
            {
                directorio.Text = ocSERIE.DIRECTORIO;
                if (this.ID.Text != "")
                {
                    directorio.Text = ocSERIE.DIRECTORIO + @"\" + this.ID.Text + @"\" + mes.Text.Substring(0, 3)+año.Text;
                }
            }
            
            dtgrdGeneral.Rows.Clear();
            dtgPolizas.Rows.Clear();
            dtgUSD.Rows.Clear();
            if (!Directory.Exists(directorio.Text))
            {
                return;
            }
            foreach (string archivo in Directory.GetFiles(directorio.Text, "*.xml", SearchOption.AllDirectories))
            {
                
                //Parsear archivo a CFDI
                //Añadir la addenda
                XmlSerializer serializer_CFD_3 = new XmlSerializer(typeof(Comprobante));
                TextReader reader = new StreamReader(archivo);
                CFDI33.Comprobante oComprobante;
                try
                {
                    oComprobante = (CFDI33.Comprobante)serializer_CFD_3.Deserialize(reader);

                    decimal tipoCambio = 1;
                    //if (!String.IsNullOrEmpty(oComprobante.TipoCambio))
                    //{
                    //    tipoCambio = oComprobante.TipoCambio;
                    //}
                    tipoCambio = oComprobante.TipoCambio;
                    int n = dtgrdGeneral.Rows.Add();
                    dtgrdGeneral.Rows[n].Cells["archivo"].Value = archivo;
                    dtgrdGeneral.Rows[n].Cells["serie"].Value = oComprobante.serie;
                    dtgrdGeneral.Rows[n].Cells["folio"].Value = oComprobante.folio;
                    dtgrdGeneral.Rows[n].Cells["fecha"].Value = oComprobante.fecha;
                    dtgrdGeneral.Rows[n].Cells["rfcEmisor"].Value = oComprobante.Emisor.rfc;
                    dtgrdGeneral.Rows[n].Cells["rfcReceptor"].Value = oComprobante.Receptor.rfc;
                    dtgrdGeneral.Rows[n].Cells["impuestos"].Value = oComprobante.Impuestos.totalImpuestosTrasladados;
                    dtgrdGeneral.Rows[n].Cells["total"].Value = oComprobante.total;

                    //Agregar polizas
                    string MyString="00000"+oComprobante.folio;
                    //Mostrar el tipo de cambio
                    string UUID = "";
                    try
                    {
                        XmlDocument doc = new XmlDocument();
                        doc.Load(archivo);
                        XmlNodeList TimbreFiscalDigital = doc.GetElementsByTagName("tfd:TimbreFiscalDigital");
                        
                        UUID = TimbreFiscalDigital[0].Attributes["UUID"].Value;
                    }
                    catch
                    {

                    }
                    string reference = "Factura: " + oComprobante.serie + "-" + MyString.Substring(MyString.Length - 5);
                    if (UUID != "")
                    {
                        reference += " UUID: " + UUID;
                    }
                    //Validar si las lineas tienen descuento y las sin descuento
                    decimal descuento=calcularTotalSinDescuento(oComprobante);
                    //Buscar las lineas que no sea descuentos o negativas
                    decimal totaltr = 0;
                    foreach(ComprobanteConcepto concepto in oComprobante.Conceptos ){
                        if (concepto.importe > 0)
                        {
                            totaltr += concepto.importe;
                        }
                    }
                    if (tipoCambio != 1)
                    {
                        reference += " TC: " + tipoCambio.ToString();
                    }
                    agregarLineaPoliza(ocPolizaConfiguracion.AccountAbonoTotal, reference, (totaltr * tipoCambio).ToString(), "0", dtgPolizas);
                    agregarLineaPoliza(ocPolizaConfiguracion.AccountCargoTotal, reference, "0", (totaltr * tipoCambio).ToString(), dtgPolizas);

                    if (tipoCambio != 1)
                    {
                        agregarLineaPoliza(ocPolizaConfiguracion.AccountAbonoTotal, reference, (totaltr).ToString(), "0", dtgUSD);
                        agregarLineaPoliza(ocPolizaConfiguracion.AccountCargoTotal, reference, "0", (totaltr).ToString(), dtgUSD);
                    }

                    //agregarLineaPoliza(ocPolizaConfiguracion.AccountAbonoImpuesto,reference,"0",dtgrdGeneral.Rows[n].Cells["impuestos"].Value.ToString());
                    //agregarLineaPoliza(ocPolizaConfiguracion.AccountCargoTotal,reference,dtgrdGeneral.Rows[n].Cells["impuestos"].Value.ToString(),"0");

                }
                catch (Exception xmlex)
                {
                    Bitacora.Log("Errpr :: " + xmlex.ToString());
                    return;
                }
                reader.Dispose();
                reader.Close();
            }
        }
        private decimal calcularTotalSinDescuento(CFDI32.Comprobante oComprobante)
        {
            int cantidadConcepto = 0;
            decimal TotalUSD = 0;
            foreach (CFDI32.ComprobanteConcepto concepto in oComprobante.Conceptos)
            {
                if (concepto.importe > 0)
                //if (true)
                {
                    cantidadConcepto++;
                    TotalUSD += concepto.importe;
                }
            }
            return TotalUSD;
        }

        private void agregarLineaPoliza(string ACCOUNT_IDp, string reference, string CARGOp, string ABONOp
            ,DataGridViewX dtg)
        {
            int n = dtg.Rows.Add();
            dtg.Rows[n].Cells[0].Value = ACCOUNT_IDp;
            dtg.Rows[n].Cells[1].Value = reference;
            dtg.Rows[n].Cells[2].Value = CARGOp;
            dtg.Rows[n].Cells[3].Value = ABONOp;
        }

        private void ENTITY_ID_SelectedIndexChanged(object sender, EventArgs e)
        {
            actualizar_conexcion();
        }

        private void actualizar_conexcion()
        {

            ocEMPRESA = ENTITY_ID.SelectedItem as cEMPRESA;
            if (ocEMPRESA != null)
            {
                EMPRESA_NOMBRE.Text = ocEMPRESA.ID;
                oData_ERP = new cCONEXCION(ocEMPRESA.TIPO, ocEMPRESA.SERVIDOR, ocEMPRESA.BD, ocEMPRESA.USUARIO_BD, ocEMPRESA.PASSWORD_BD);
            }
        }

        private void buttonX11_Click(object sender, EventArgs e)
        {
            guardar();
        }

        private void guardar()
        {
            XmlSerializer serializer = new XmlSerializer(typeof(cPolizaConfiguracion));
            StreamWriter writer = new StreamWriter(sDirectory);
            ocPolizaConfiguracion.AccountAbonoImpuesto = accountAbonoImpuesto.Text;
            ocPolizaConfiguracion.AccountAbonoTotal = accountAbonoTotal.Text;
            ocPolizaConfiguracion.AccountCargoImpuesto = accountCargoImpuesto.Text;
            ocPolizaConfiguracion.AccountCargoTotal = accountCargoTotal.Text;
            ocPolizaConfiguracion.ComentarioDescuento = comentarioDescuento.Text;
            ocPolizaConfiguracion.Directorio = directorio.Text;
            ocPolizaConfiguracion.SerieDefecto = serieDefecto.Text;
            ocPolizaConfiguracion.FormatoDefecto = formatoDefecto.Text;

            serializer.Serialize(writer, ocPolizaConfiguracion);
            writer.Close();
            toolStripStatusLabel1.Text = "Datos guardados.";

        }

        private void buttonX6_Click(object sender, EventArgs e)
        {
            frmACCOUNT ofrmACCOUNT = new frmACCOUNT(oData_ERP.sConn);
            if (ofrmACCOUNT.ShowDialog() == DialogResult.OK)
            {
                accountAbonoTotal.Text = ofrmACCOUNT.selecionados[0].Cells[0].Value.ToString();
            }
        }

        private void buttonX8_Click(object sender, EventArgs e)
        {
            generarPoliza(dtgPolizas, "Pesos");
            if (dtgUSD.Rows.Count > 0)
            {
                generarPoliza(dtgUSD, "Dolares");
            }
        }

        private void buttonX7_Click(object sender, EventArgs e)
        {
            frmACCOUNT ofrmACCOUNT = new frmACCOUNT(oData_ERP.sConn);
            if (ofrmACCOUNT.ShowDialog() == DialogResult.OK)
            {
                this.accountCargoTotal.Text = ofrmACCOUNT.selecionados[0].Cells[0].Value.ToString();
            }
        }

        private void buttonX10_Click(object sender, EventArgs e)
        {
            frmACCOUNT ofrmACCOUNT = new frmACCOUNT(oData_ERP.sConn);
            if (ofrmACCOUNT.ShowDialog() == DialogResult.OK)
            {
                this.accountAbonoImpuesto.Text = ofrmACCOUNT.selecionados[0].Cells[0].Value.ToString();
            }
        }

        private void buttonX9_Click(object sender, EventArgs e)
        {
            frmACCOUNT ofrmACCOUNT = new frmACCOUNT(oData_ERP.sConn);
            if (ofrmACCOUNT.ShowDialog() == DialogResult.OK)
            {
                this.accountCargoImpuesto.Text = ofrmACCOUNT.selecionados[0].Cells[0].Value.ToString();
            }
        }

        private void buttonX5_Click(object sender, EventArgs e)
        {
            cargarDirectorio();
        }

        private void cargarDirectorio()
        {
            if (folderBrowserDialog1.ShowDialog() == DialogResult.OK)
            {
                directorio.Text = folderBrowserDialog1.SelectedPath;
                cargar_archivos();
            }
        }

        private void buttonX4_Click(object sender, EventArgs e)
        {
            generarPoliza(dtgPolizas, " Pesos ");
            if (dtgUSD.Rows.Count > 0)
            {
                generarPoliza(dtgUSD," Dolares ");
            }
        }

        public void generarPoliza(DataGridViewX dtg, string nombre="")
        {
            dtg.SelectAll();
            dtg.EndEdit();
            if (dtg.SelectedCells.Count > 0)
            {
                var fileName = "Poliza " + nombre + DateTime.Now.ToString("yyyy-MM-dd-hh-mm-ss") + ".csv";
                if (folderBrowserDialog1.ShowDialog() == DialogResult.OK)
                {
                    string archivo = fileName;
                    fileName = folderBrowserDialog1.SelectedPath + "\\" + fileName;
                    using (StreamWriter sw = File.AppendText(fileName))
                    {
                        string cabecera = "Cuenta" + "," + "Referencia" + "," + "Cargo" + "," + "Abono" + Environment.NewLine;
                        sw.WriteLine(cabecera);
                        foreach (DataGridViewRow drv in dtg.SelectedRows)
                        {
                            string registro_tmp = "";
                            registro_tmp = drv.Cells[0].Value.ToString() + "," + drv.Cells[1].Value.ToString() + "," + oData.formato_decimal(drv.Cells[2].Value.ToString(), "S") + "," + oData.formato_decimal(drv.Cells[3].Value.ToString(), "S") + Environment.NewLine;
                            sw.WriteLine(registro_tmp);
                        }
                        sw.Close();
                    }
                    var tempFileName = Path.GetTempFileName();
                    try
                    {
                        using (var streamReader = new StreamReader(fileName))
                        using (var streamWriter = new StreamWriter(tempFileName))
                        {
                            string line;
                            while ((line = streamReader.ReadLine()) != null)
                            {
                                if (!String.IsNullOrEmpty(line))
                                    streamWriter.WriteLine(line);
                            }
                        }
                        File.Copy(tempFileName, fileName, true);
                    }
                    finally
                    {
                        File.Delete(tempFileName);
                    }
                    MessageBoxEx.Show("Poliza Generada del archivo " + Environment.NewLine + fileName + " finalizada.", Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
            else
            {
                MessageBoxEx.Show("Seleccione alguna línea.", Application.ProductName + "-" + Application.ProductVersion, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }

        private void buttonX1_Click_1(object sender, EventArgs e)
        {
            verPDF();
        }

        private void verPDF()
        {
            if (dtgrdGeneral.SelectedRows.Count > 0)
            {
                DataGridViewSelectedRowCollection arrSelectedRows = dtgrdGeneral.SelectedRows;
                string ARCHIVO = arrSelectedRows[0].Cells["ARCHIVO"].Value.ToString();
                string directorio = AppDomain.CurrentDomain.BaseDirectory;
                ARCHIVO = ARCHIVO.Replace(".xml", ".pdf");
                frmFacturaXML oObjeto = new frmFacturaXML(ARCHIVO);
                oObjeto.Show();
            }
        }

        private void labelX3_Click(object sender, EventArgs e)
        {

        }

        private void directorio_KeyPress(object sender, KeyPressEventArgs e)
        {
            //Precionar Enter buscar
            if (e.KeyChar.ToString() == "\r")
            {
                cargar_archivos();
            }
        }

        private void mes_SelectedIndexChanged(object sender, EventArgs e)
        {
            cargar_archivos();
        }

        private void año_SelectedIndexChanged(object sender, EventArgs e)
        {
            cargar_archivos();
        }

    }
}
