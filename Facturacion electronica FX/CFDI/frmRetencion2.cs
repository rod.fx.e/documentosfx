﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;

using System.Text;
using System.Windows.Forms;
using System.Xml.Xsl;
using System.Xml;
using System.Xml.Serialization;
using System.Security.Cryptography.X509Certificates;
using System.IO;
using System.Globalization;
using System.Xml.XPath;
using System.Text.RegularExpressions;
using Ionic.Zip;
using FE_FX.EDICOM_Servicio;
using System.ServiceModel;
using DevComponents.DotNetBar.Controls;
using DevComponents.DotNetBar;
using CrystalDecisions.CrystalReports.Engine;
using com.google.zxing.qrcode;
using FE_FX.CFDI;
using CFDI32;
using Generales;
using CryptoSysPKI;
using FE_FX.Clases;

namespace FE_FX
{
    public partial class frmRetencion2 : Form
    {

        string devolver = "";
        public DataGridViewSelectedRowCollection selecionados;
        string ARCHIVO = "";
        private cEMPRESA ocEMPRESA;
        private cSERIE ocSERIE;
        private cCONEXCION oData = new cCONEXCION("");
        private cCONEXCION oData_ERP = new cCONEXCION("");
        cCERTIFICADO ocCERTIFICADO = new cCERTIFICADO();
        Retenciones oRetenciones = new Retenciones();


        public frmRetencion2(string sConn)
        {
            oData = new cCONEXCION(sConn);
            oData.sConn = sConn;
            ocCERTIFICADO = new cCERTIFICADO();
            InitializeComponent();
            cargar_empresas();
            cargarFormatoImpresion();
        }

        private void cargarFormatoImpresion()
        {
            string[] filePaths = Directory.GetFiles(System.IO.Path.GetDirectoryName(Application.ExecutablePath), "*.rpt");
            FORMATO_IMPRESION.Items.Clear();
            foreach (string archivo in filePaths)
            {
                FORMATO_IMPRESION.Items.Add(System.IO.Path.GetFileName(archivo));
                FORMATO_IMPRESION.Text = System.IO.Path.GetFileName(archivo);
            }
        }

        private void cargarArchivo(string pathArchivo)
        {
            Comprobante oComprobante;
            //Pasear el comprobante
            try
            {
                XmlSerializer serializer_CFD_3 = new XmlSerializer(typeof(CFDI33.Comprobante));
                TextReader reader = new StreamReader(pathArchivo);
                oComprobante = (Comprobante)serializer_CFD_3.Deserialize(reader);
                reader.Dispose();
                reader.Close();


                       
            }
            catch (Exception e)
            {
                MessageBoxEx.Show("Cargar Comprobante: El archivo " + pathArchivo + " no es un CFDI válido.");
            }
        }


        private void cargar_empresas()
        {
            ENTITY_ID.Items.Clear();
            cEMPRESA ocEMPRESA = new cEMPRESA(oData);
            foreach (cEMPRESA registro in ocEMPRESA.todos("", true))
            {
                ENTITY_ID.Items.Add(registro);
            }
            if (ENTITY_ID.Items.Count > 0)
            {
                ENTITY_ID.SelectedIndex = 0;
            }

            //CveRetencCmb.DataSource = Enum.GetValues(typeof(c_CveRetenc));

            CveRetencCmb.Items.Clear();
            CveRetencCmb.DisplayMember = "Name";
            CveRetencCmb.ValueMember = "Value";

            List<Item> lista = new List<Item>();
            lista.Add(new Item("Servicios profesionales.", "01"));
            lista.Add(new Item("Regalías por derechos de autor.", "02"));
            lista.Add(new Item("Autotransporte terrestre de carga.", "03"));
            lista.Add(new Item("Servicios prestados por comisionistas.", "04"));
            lista.Add(new Item("Arrendamiento.", "05"));
            lista.Add(new Item("Enajenación de acciones.", "06"));
            lista.Add(new Item("Enajenación de bienes objeto de la LIEPS, a través de mediadores, agentes, representantes, corredores, consignatarios o distribuidores.", "07"));
            lista.Add(new Item("Enajenación de bienes inmuebles consignada en escritura pública.", "08"));
            lista.Add(new Item("Enajenación de otros bienes, no consignada en escritura pública.", "09"));
            lista.Add(new Item("Adquisición de desperdicios industriales.", "10"));
            lista.Add(new Item("Adquisición de bienes consignada en escritura pública.", "11"));
            lista.Add(new Item("Adquisición de otros bienes, no consignada en escritura pública.", "12"));
            lista.Add(new Item("Otros retiros de AFORE.", "13"));
            lista.Add(new Item("Dividendos o utilidades distribuidas.", "14"));
            lista.Add(new Item("Remanente distribuible.", "15"));
            lista.Add(new Item("Intereses.", "16"));
            lista.Add(new Item("Arrendamiento en fideicomiso.", "17"));
            lista.Add(new Item("Pagos realizados a favor de residentes en el extranjero.", "18"));
            lista.Add(new Item("Enajenación de acciones u operaciones en bolsa de valores.", "19"));
            lista.Add(new Item("Obtención de premios.", "20"));
            lista.Add(new Item("Fideicomisos que no realizan actividades empresariales.", "21"));
            lista.Add(new Item("Planes personales de retiro.", "22"));
            lista.Add(new Item("Intereses reales deducibles por créditos hipotecarios.", "23"));
            lista.Add(new Item("Operaciones Financieras Derivadas de Capital.", "24"));
            lista.Add(new Item("Otro tipo de retenciones.", "25"));
            lista.Add(new Item("Servicios mediante Plataformas Tecnológicas ", "26"));


            CveRetencCmb.DataSource = lista;

        }

        public class Item
        {
            public Item(string name, string value)
            {
                this.Name = name;
                this.Value = value;
            }

            public override string ToString()
            {
                return this.Name;
            }
            public string Name { get; set; }
            public string Value { get; set; }
        }



    private void frmFormatos_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)27)
            {
                this.Close();
            }
            else
            {
                if ((e.KeyChar.ToString() == "<")
                                | (e.KeyChar.ToString() == ">")
                                
                                | (e.KeyChar.ToString() == "'")
                                | (e.KeyChar.ToString() == "#")
                                )
                {
                    e.Handled = true;
                }
            }
        }

        private void buttonX2_Click(object sender, EventArgs e)
        {
            frmRetencion2 ofrmRetencion2 = new frmRetencion2(oData.sConn);
            ofrmRetencion2.ShowDialog();
        }

        private void buttonX4_Click(object sender, EventArgs e)
        {
            frmVENDOR ofrmVENDOR = new frmVENDOR(oData_ERP.sConn);
            if (ofrmVENDOR.ShowDialog() == DialogResult.OK)
            {
                if (ofrmVENDOR.selecionados[0].Cells[2].Value!=null)
                {
                    rfc.Text = (string)ofrmVENDOR.selecionados[0].Cells[2].Value;
                }
                if (ofrmVENDOR.selecionados[0].Cells[1].Value != null)
                {
                    receptortxt.Text = (string)ofrmVENDOR.selecionados[0].Cells[1].Value;
                }
            }
        }

        private void ENTITY_ID_SelectedIndexChanged(object sender, EventArgs e)
        {
            actualizar_conexcion();
        }

        private void actualizar_conexcion()
        {

            ocEMPRESA = ENTITY_ID.SelectedItem as cEMPRESA;
            if (ocEMPRESA != null)
            {
                EMPRESA_NOMBRE.Text = ocEMPRESA.ID;
                oData_ERP = new cCONEXCION(ocEMPRESA.TIPO, ocEMPRESA.SERVIDOR, ocEMPRESA.BD, ocEMPRESA.USUARIO_BD, ocEMPRESA.PASSWORD_BD);
            }
        }

        private void buttonX2_Click_1(object sender, EventArgs e)
        {
            nuevo();
        }

        private void nuevo()
        {
            ocSERIE = new cSERIE();
            dtgPolizas.Rows.Clear();
            dtgTotales.Rows.Clear();
            ClearTextBoxes();

            montoTotOperacion.Text = decimal.Parse("0").ToString("####.00");
            montoTotRet.Text = decimal.Parse("0").ToString("####.00");
            montoTotGrav.Text = decimal.Parse("0").ToString("####.00");
            montoTotExent.Text = decimal.Parse("0").ToString("####.00");

        }
        private void ClearTextBoxes()
        {
            Action<Control.ControlCollection> func = null;

            func = (controls) =>
            {
                foreach (Control control in controls)
                    if (control is TextBox)
                        (control as TextBox).Clear();
                    else
                        func(control.Controls);
            };

            func(Controls);
        }
        private void labelX12_Click(object sender, EventArgs e)
        {

        }

        private void buttonX1_Click(object sender, EventArgs e)
        {
            buscarSerie();
        }

        private void buscarSerie()
        {
            
            frmSeries oBuscador = new frmSeries(oData.sConn, "S", ocEMPRESA.ROW_ID);
            if (oBuscador.ShowDialog() == DialogResult.OK)
            {
                string ROW_ID = (string)oBuscador.selecionados[0].Cells["ROW_ID"].Value;
                string IDtr = (string)oBuscador.selecionados[0].Cells["ID"].Value;
                SERIE.Text = IDtr;
                
                ocSERIE = new cSERIE();

                ocSERIE.cargar_serie(IDtr);
                ID.Text = ocSERIE.consecutivo(ocSERIE.ROW_ID,false);
            }
        }

    
        private void buttonX5_Click(object sender, EventArgs ex)
        {
            try
            {
                if (generar())
                {
                    if (timbrar())
                    {
                        if (ESTADO.Text != "BORRADOR")
                        {
                            ocSERIE.incrementar(ocSERIE.ROW_ID);
                            cargarTimbre(PATH.Text);
                        }
                        MessageBox.Show("Retención realizada correctamente.", Application.ProductName + "-" + Application.ProductVersion, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);

                    }
                }
            }
            catch (Exception e)
            {
                ErrorFX.mostrar(e, true, true, true);
            }

        }

        private void cargarTimbre(string p)
        {
            XmlSerializer serializer_CFD_3 = new XmlSerializer(typeof(Retenciones));
            TextReader reader = new StreamReader(p);
            Retenciones oRetenciones;
            try
            {
                oRetenciones = (Retenciones)serializer_CFD_3.Deserialize(reader);
                reader.Dispose();
                reader.Close();
            }
            catch
            {

            }
        }


        private bool timbrar()
        {
            //Timbrar
            informacion.Text = "Timbrando el XML " + ID.Text;
            bool usar_edicom = false;
            if(ESTADO.Text.Contains("BORRADOR")){
                usar_edicom = true;
            }
            try
            {
                
                if (usar_edicom)
                {
                    usar_edicom = this.generar_CFDI(2, Globales.oCFDI_USUARIO.USUARIO, cENCRIPTACION.Decrypt(Globales.oCFDI_USUARIO.PASSWORD));
                }
                else
                {
                    usar_edicom = this.generar_CFDI(1, Globales.oCFDI_USUARIO.USUARIO, cENCRIPTACION.Decrypt(Globales.oCFDI_USUARIO.PASSWORD));
                }
            }
            catch (Exception ex) {
                MessageBox.Show("Error timbrando en EDICOM" + Environment.NewLine + ex.InnerException.ToString(), Application.ProductVersion, MessageBoxButtons.OK);
                return false;
            }
            
            if (!usar_edicom)
            {
                ////Actualizar el Status
                //DialogResult Resultado = MessageBox.Show("La factura " + ID.Text + " no fue timbrada, Desea Imprimirla?", Application.ProductName + "-" + Application.ProductVersion, MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                //if (Resultado.ToString() == "Yes")
                //{
                //    usar_edicom = true;
                //}
                return false;
            }

            cargarTimbreFiscal();

            return true;
        }

        private void cargarTimbreFiscal()
        {
            XmlDocument doc = new XmlDocument();
            doc.Load(PATH.Text);
            XmlNodeList TimbreFiscalDigital = doc.GetElementsByTagName("tfd:TimbreFiscalDigital");
            if (TimbreFiscalDigital != null)
            {
                UUID.Text = TimbreFiscalDigital[0].Attributes["UUID"].Value;
            }
            
        }

        public bool generar_CFDI(int Opcion, string EDICOM_USUARIO, string EDICOM_PASSWORD)
        {
            try
            {
                //Comprimir Archivo
                string Archivo_Tmp = oRetenciones.FolioInt + ".xml";
                File.Copy(ARCHIVO, Archivo_Tmp, true);
                string ARCHIVO_zip = Archivo_Tmp.ToUpper().Replace("XML", "ZIP");
                using (ZipFile zip = new ZipFile())
                {
                    zip.AddFile(Archivo_Tmp);
                    zip.Save(ARCHIVO_zip);
                }
                //Fin de Compresion de Archivo
                File.Delete(Archivo_Tmp);

                byte[] ARCHIVO_Leido = ReadBinaryFile(ARCHIVO_zip);
                File.Delete(ARCHIVO_zip);
                CFDiClient oCFDiClient = new CFDiClient();

                byte[] PROCESO;
                string MENSAJE = "";

                switch (Opcion)
                {
                    case 1:
                        try
                        {
                            informacion.Text = "Generando CFDI ";
                            MENSAJE = "";
                            PROCESO = oCFDiClient.getCfdiRetenciones(EDICOM_USUARIO, EDICOM_PASSWORD, ARCHIVO_Leido);
                            guardar_archivo(PROCESO, oRetenciones.FolioInt, ARCHIVO);
                        }
                        catch (FaultException oException)
                        {

                            CFDiException oCFDiException = new CFDiException();
                            //MessageBox.Show(oException.Message, Application.ProductName + "-" + Application.ProductVersion, MessageBoxButtons.OK, MessageBoxIcon.Error);
                            ErrorFX.mostrar(oException, true, true, true);
                            return false;
                        }

                        break;
                    case 2:
                        try
                        {
                            informacion.Text = "Generando CFDI Test ";
                            MENSAJE = " TEST ";
                            PROCESO = oCFDiClient.getCfdiRetencionesTest(EDICOM_USUARIO, EDICOM_PASSWORD, ARCHIVO_Leido);
                            guardar_archivo(PROCESO, oRetenciones.FolioInt, ARCHIVO);
                        }
                        catch (FaultException oException)
                        {

                            CFDiException oCFDiException = new CFDiException();
                            //MessageBox.Show(oException.Message, Application.ProductName + "-" + Application.ProductVersion, MessageBoxButtons.OK, MessageBoxIcon.Error);

                            ErrorFX.mostrar(oException, true, true, true);

                            informacion.Text = "Error " + DateTime.Now.ToShortDateString() + " " + DateTime.Now.ToLongTimeString();
                            return false;
                        }
                        break;
                    case 3:
                        try
                        {
                            informacion.Text = "Generando Timbre CFDI  ";
                            PROCESO = oCFDiClient.getTimbreCfdi(EDICOM_USUARIO, EDICOM_PASSWORD, ARCHIVO_Leido);
                        }
                        catch (Exception oException)
                        {

                            CFDiException oCFDiException = new CFDiException();
                            //MessageBox.Show(oException.Message, Application.ProductName + "-" + Application.ProductVersion, MessageBoxButtons.OK, MessageBoxIcon.Error);

                            ErrorFX.mostrar(oException, true, true, true);

                            informacion.Text = "Error " + DateTime.Now.ToShortDateString() + " " + DateTime.Now.ToLongTimeString();
                            return false;
                        }
                        break;
                    case 4:
                        try
                        {
                            informacion.Text = "Generando Timbre CFDI Test ";
                            PROCESO = oCFDiClient.getTimbreCfdiTest(EDICOM_USUARIO, EDICOM_PASSWORD, ARCHIVO_Leido);
                        }
                        catch (Exception oException)
                        {

                            CFDiException oCFDiException = new CFDiException();
                            //MessageBox.Show(oException.Message, Application.ProductName + "-" + Application.ProductVersion, MessageBoxButtons.OK, MessageBoxIcon.Error);

                            ErrorFX.mostrar(oException, true, true, true);

                            informacion.Text = "Error " + DateTime.Now.ToShortDateString() + " " + DateTime.Now.ToLongTimeString();
                            return false;
                        }
                        break;
                }

                informacion.Text = "Generado y Sellado";
                return true;
            }
            catch (Exception oException)
            {
                MessageBox.Show("Generando CFDI " + oException.Message, Application.ProductName + "-" + Application.ProductVersion, MessageBoxButtons.OK, MessageBoxIcon.Error);
                informacion.Text = "Error " + DateTime.Now.ToShortDateString() + " " + DateTime.Now.ToLongTimeString();
                return false;
            }
        }

        private void guardar_archivo(byte[] PROCESO, string INVOICE_ID, string ARCHIVO)
        {
            //Guardar los PROCESO

            File.WriteAllBytes("TEMPORAL.ZIP", PROCESO);
            //Descomprimir
            String TargetDirectory = "TEMPORAL";
            using (Ionic.Zip.ZipFile zip = Ionic.Zip.ZipFile.Read("TEMPORAL.ZIP"))
            {
                zip.ExtractAll(TargetDirectory, Ionic.Zip.ExtractExistingFileAction.OverwriteSilently);
            }

            //Copiar el ARCHIVO Descrompimido por otro
            string ARCHIVO_descomprimido = TargetDirectory + @"\" + "SIGN_" + INVOICE_ID + ".XML";
            
            File.Copy(ARCHIVO_descomprimido, ARCHIVO, true);
            PATH.Text = ARCHIVO;
            File.Delete(ARCHIVO_descomprimido);
            File.Delete("TEMPORAL.ZIP");

        }



        private bool FileExists(string pPath)
        {
            string sPath = pPath;
            try
            {
                if (File.Exists(sPath))
                {
                    return true;
                }
                else
                {
                    return false;
                }

            }
            catch
            {
                return false;
            }
        }

        public string validacion(string cadena)
        {
            string devolucion = "";
            devolucion = cadena.ToUpper();

            devolucion = devolucion.Replace("á", "A");
            devolucion = devolucion.Replace("é", "E");
            devolucion = devolucion.Replace("í", "I");
            devolucion = devolucion.Replace("ó", "O");
            devolucion = devolucion.Replace("ú", "U");
            devolucion = devolucion.Replace("Á", "A");
            devolucion = devolucion.Replace("É", "E");
            devolucion = devolucion.Replace("Í", "I");
            devolucion = devolucion.Replace("Ó", "O");
            devolucion = devolucion.Replace("Ú", "U");
            devolucion = devolucion.Replace(">", "");
            devolucion = devolucion.Replace("<", "");
            //devolucion = devolucion.Replace("&", "");
            devolucion = devolucion.Replace("'", "");
            devolucion = devolucion.Replace("#", "");
            devolucion = devolucion.Replace("|", "");
            //devolucion = HtmlEncode(devolucion);

            devolucion = Regex.Replace(devolucion, @"[\u0000-\u001F]", string.Empty);


            return devolucion;
        }


        

        private byte[] ReadBinaryFile(string fileName)
        {
            if (File.Exists(fileName))
            {
                try
                {
                    ///Open and read a file&#12290;
                    FileStream fileStream = File.OpenRead(fileName);
                    byte[] archivo = ConvertStreamToByteBuffer(fileStream);
                    fileStream.Close();
                    return archivo;
                }
                catch (Exception ex)
                {
                    return new byte[0];
                }
            }
            else
            {
                return new byte[0];
            }
        }
        public byte[] ConvertStreamToByteBuffer(System.IO.Stream theStream)
        {
            int b1;
            System.IO.MemoryStream tempStream = new System.IO.MemoryStream();
            while ((b1 = theStream.ReadByte()) != -1)
            {
                tempStream.WriteByte(((byte)b1));
            }
            return tempStream.ToArray();
        }

        public bool generar()
        {
            try
            {
                //Valor por defecto del valorUnitario es de 2
                string INVOICE_ID = ID.Text;
                string sDirectory = AppDomain.CurrentDomain.BaseDirectory;
                if (ocSERIE == null)
                {
                    MessageBox.Show("Seleccione la serie", Application.ProductName + "-" + Application.ProductVersion, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                    return false;
                }
                if (String.IsNullOrEmpty(rfc.Text))
                {
                    MessageBox.Show("Seleccione el RFC ", Application.ProductName + "-" + Application.ProductVersion, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                    return false;
                }
                if (dtgTotales.Rows.Count == 0)
                {
                    MessageBox.Show("Ingrese al menos 1 línea de impuesto", Application.ProductName + "-" + Application.ProductVersion, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                    return false;
                }


                //Cargar Certificado
                ocCERTIFICADO = new cCERTIFICADO();
                ocCERTIFICADO.cargar(ocSERIE.ROW_ID_CERTIFICADO);
                cEMPRESA ocEMPRESA = new cEMPRESA(oData);
                ocEMPRESA.cargar(ocCERTIFICADO.ROW_ID_EMPRESA);

                //MessageBox.Show("XLT");
                if (!FileExists(sDirectory + "/XSLT/retenciones.xslt"))
                {
                    MessageBox.Show("No se tiene acceso al archivo retenciones.xslt.", Application.ProductName + "-" + Application.ProductVersion, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);

                    return false;
                }

                if (!File.Exists(ocCERTIFICADO.CERTIFICADO))
                {
                    MessageBox.Show("No se tiene acceso al archivo " + ocCERTIFICADO.CERTIFICADO + "", Application.ProductName + "-" + Application.ProductVersion, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                    return false;
                }
                X509Certificate cert = X509Certificate.CreateFromCertFile(ocCERTIFICADO.CERTIFICADO);
                string NoSerie = cert.GetSerialNumberString();

                StringBuilder SerieHex = new StringBuilder();
                for (int i = 0; i <= NoSerie.Length - 2; i += 2)
                {
                    SerieHex.Append(Convert.ToString(Convert.ToChar(Int32.Parse(NoSerie.Substring(i, 2), System.Globalization.NumberStyles.HexNumber))));
                }
                //MessageBox.Show("Comprobante");
                string NO_CERTIFICADO = SerieHex.ToString();

                DateTime INVOICE_DATE = DateTime.Now;
                IFormatProvider culture = new CultureInfo("es-MX", true);
                DateTime CREATE_DATE = DateTime.Now;

                INVOICE_DATE = DateTime.Parse(INVOICE_DATE.Day.ToString() + "/" + INVOICE_DATE.Month.ToString() + "/" + INVOICE_DATE.Year.ToString() + " " + CREATE_DATE.ToString("HH:mm:ss"), culture);

                oRetenciones = new Retenciones();
                cert = X509Certificate.CreateFromCertFile(ocCERTIFICADO.CERTIFICADO);
                string Certificado64 = System.Convert.ToBase64String(cert.GetRawCertData());
                oRetenciones.Certificado = Certificado64;


                var date = DateTime.Now;

                date = new DateTime(date.Year, date.Month, date.Day, date.Hour, date.Minute, date.Second, date.Kind);
                string formattedDate = date.ToString("yyyy-MM-ddTHH:mm:ss");
                oRetenciones.FechaExp = formattedDate;
                oRetenciones.LugarExpRetenc = LugarExpRetenc.Text;
                oRetenciones.DescRetenc = DescRetenc.Text;
                Item oItem = (Item) CveRetencCmb.SelectedItem;

                string valor = ("Item" + oItem.Value.ToString());
                oRetenciones.CveRetenc = (c_CveRetenc)Enum.Parse(typeof(c_CveRetenc), valor); 
                ///

                //oRetenciones.FechaExp = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day
                //    , DateTime.Now.Hour-1,DateTime.Now.Minute,DateTime.Now.Second);
                //
                oRetenciones.Periodo = new RetencionesPeriodo();

                oRetenciones.Periodo.MesFin = fin.Text;//int.Parse(fin.Text);
                oRetenciones.Periodo.MesIni = inicio.Text;// int.Parse(inicio.Text);
                oRetenciones.Periodo.Ejercicio = ejercicio.Text;// int.Parse(ejercicio.Text);

                oRetenciones.NoCertificado = ocCERTIFICADO.NO_CERTIFICADO;
                oRetenciones.FolioInt = SERIE.Text+ID.Text;
                oRetenciones.Totales = new RetencionesTotales();
                oRetenciones.Totales.MontoTotOperacion = decimal.Parse(montoTotOperacion.Text);

                //Emisor
                oRetenciones.Emisor = new RetencionesEmisor();
                oRetenciones.Emisor.RfcE = ocEMPRESA.RFC;
                oRetenciones.Emisor.NomDenRazSocE = Generar40.LimpiarNombre(ocEMPRESA.ID);
                oRetenciones.Emisor.RegimenFiscalE = "601";
                //oRetenciones.Emisor.CURPE = CURPE.Text;

                //Receptor
                oRetenciones.Receptor = new RetencionesReceptor();
                oRetenciones.Receptor.NacionalidadR = new RetencionesReceptorNacionalidadR();
                if (nacionalidad.Text.Equals("Nacional"))
                {
                    oRetenciones.Receptor.NacionalidadR = RetencionesReceptorNacionalidadR.Nacional;
                    RetencionesReceptorNacional oRetencionesReceptorNacional = new RetencionesReceptorNacional();
                    oRetencionesReceptorNacional.NomDenRazSocR = receptortxt.Text;
                    oRetencionesReceptorNacional.RfcR = rfc.Text;
                    if (CURPE.Text.Length != 0)
                    {
                        oRetencionesReceptorNacional.CurpR = CURPE.Text;
                    }
                    

                    oRetenciones.Receptor.Item = new RetencionesReceptorNacional();
                    oRetenciones.Receptor.Item = oRetencionesReceptorNacional;
                }
                else
                {
                    oRetenciones.Receptor.NacionalidadR = RetencionesReceptorNacionalidadR.Extranjero;
                    if (oRetenciones.Receptor.NacionalidadR == RetencionesReceptorNacionalidadR.Extranjero)
                    {
                        //<retenciones:Extranjero NomDenRazSocR="WALLACE &amp; SMITH CO. LTD" NumRegIdTrib="XEXX010101000"/>
                        RetencionesReceptorExtranjero oRetencionesReceptorExtranjero = new RetencionesReceptorExtranjero();
                        oRetencionesReceptorExtranjero.NomDenRazSocR = receptortxt.Text;
                        oRetencionesReceptorExtranjero.NumRegIdTribR = rfc.Text;

                        oRetenciones.Receptor.Item = new RetencionesReceptorExtranjero();
                        oRetenciones.Receptor.Item = oRetencionesReceptorExtranjero;
                    }
                }

                //Totales
                oRetenciones.Totales = new RetencionesTotales();
                oRetenciones.Totales.MontoTotRet = decimal.Parse(montoTotRet.Text);
                oRetenciones.Totales.MontoTotOperacion = decimal.Parse(montoTotOperacion.Text);
                oRetenciones.Totales.MontoTotGrav = decimal.Parse(montoTotGrav.Text);
                oRetenciones.Totales.MontoTotExent = decimal.Parse(montoTotExent.Text);
                        
                //Agregar lineas de impuestos
                oRetenciones.Totales = new RetencionesTotales();

                RetencionesTotalesImpRetenidos[] oRetencionesTotalesImpRetenidos;
                oRetencionesTotalesImpRetenidos = new RetencionesTotalesImpRetenidos[this.dtgTotales.Rows.Count];
                int contadorRegistro=0;
                
                foreach (DataGridViewRow registro in this.dtgTotales.Rows)
                {
                    RetencionesTotalesImpRetenidos oLinea = new RetencionesTotalesImpRetenidos();

                    oLinea.BaseRetSpecified = true;
                    oLinea.BaseRet = decimal.Parse(registro.Cells["BaseRet"].Value.ToString());
                    oLinea.MontoRet = decimal.Parse(registro.Cells["montoRet"].Value.ToString());
                    //oLinea.ImpuestoSpecified = true;
                    oLinea.TipoPagoRet = c_TipoPagoRet.Item01;
                    /*
                    Pago definitivo IVA
                    Pago definitivo IEPS
                    Pago definitivo ISR
                    Pago provisional ISR
                    */
                    if (registro.Cells["TipoPagoRet"].Value.ToString().Contains("Pago definitivo IVA"))
                    {
                        oLinea.TipoPagoRet = c_TipoPagoRet.Item01;//RetencionesTotalesImpRetenidosTipoPagoRet.Pagoprovisional;
                    }
                    if (registro.Cells["TipoPagoRet"].Value.ToString().Contains("Pago definitivo IEPS"))
                    {
                        oLinea.TipoPagoRet = c_TipoPagoRet.Item02;//RetencionesTotalesImpRetenidosTipoPagoRet.Pagoprovisional;
                    }
                    if (registro.Cells["TipoPagoRet"].Value.ToString().Contains("Pago definitivo ISR"))
                    {
                        oLinea.TipoPagoRet = c_TipoPagoRet.Item03;//RetencionesTotalesImpRetenidosTipoPagoRet.Pagoprovisional;
                    }
                    if (registro.Cells["TipoPagoRet"].Value.ToString().Contains("Pago provisional ISR"))
                    {
                        oLinea.TipoPagoRet = c_TipoPagoRet.Item04;//RetencionesTotalesImpRetenidosTipoPagoRet.Pagoprovisional;
                    }
                    //if (registro.Cells["TipoPagoRet"].Value.ToString().Contains("Pago provisional"))
                    //{
                    //    oLinea.TipoPagoRet = c_TipoPagoRet.Item04;//RetencionesTotalesImpRetenidosTipoPagoRet.Pagoprovisional;
                    //}
                    oLinea.ImpuestoRet = "001";//RetencionesTotalesImpRetenidosImpuesto.Item01;
                    if (registro.Cells["Impuesto"].Value.ToString().Contains("02"))
                    {
                        oLinea.ImpuestoRet = "002";//RetencionesTotalesImpRetenidosImpuesto.Item02;
                    }
                    if (registro.Cells["Impuesto"].Value.ToString().Contains("03"))
                    {
                        oLinea.ImpuestoRet = "003";//RetencionesTotalesImpRetenidosImpuesto.Item03;
                    }

                    oRetencionesTotalesImpRetenidos[contadorRegistro] = new RetencionesTotalesImpRetenidos();
                    oRetencionesTotalesImpRetenidos[contadorRegistro] = oLinea;
                    contadorRegistro++;
                }
                oRetenciones.Totales = new RetencionesTotales();
                oRetenciones.Totales.ImpRetenidos = new RetencionesTotalesImpRetenidos[this.dtgTotales.Rows.Count];
                oRetenciones.Totales.ImpRetenidos = oRetencionesTotalesImpRetenidos;
                oRetenciones.Totales.MontoTotExent = decimal.Parse(montoTotExent.Text);
                oRetenciones.Totales.MontoTotGrav = decimal.Parse(montoTotGrav.Text);
                oRetenciones.Totales.MontoTotOperacion = decimal.Parse(montoTotOperacion.Text);
                oRetenciones.Totales.MontoTotRet = decimal.Parse(montoTotRet.Text);

                //<pagosaextranjeros:Pagosaextranjeros Version="1.0" EsBenefEfectDelCobro="SI">
                //      <pagosaextranjeros:NoBeneficiario PaisDeResidParaEfecFisc="IT" ConceptoPago="6" DescripcionConcepto="ESTA ES LA DESCRIPCION"/>
                //      <pagosaextranjeros:Beneficiario RFC="AAAA010101AA0" CURP="AAAA010101HJCXXX01" NomDenRazSocB="PRUEBA DE PERSONA FISICA" ConceptoPago="6" DescripcionConcepto="ESTA ES LA DESCRIPCION"/>
                //    </pagosaextranjeros:Pagosaextranjeros>
                //string cadenaOriginalPagoextanjero="";
                try
                {
                    //Agregar los datos del comprobante de Extranjeros
                    if (oRetenciones.CveRetenc == c_CveRetenc.Item18)
                    {
                        if (ucRetencionPagoExtranjero1.PaisDeResidParaEfecFisc.Text != "")
                        {
                            oRetenciones.Complemento = new RetencionesComplemento();
                            Pagosaextranjeros oPagosaextranjeros = new Pagosaextranjeros();
                            oPagosaextranjeros.NoBeneficiario = new PagosaextranjerosNoBeneficiario();

                            try
                            {
                                c_Paispagoextranjero oc_Pais = (c_Paispagoextranjero)System.Enum.Parse(typeof(c_Paispagoextranjero), ucRetencionPagoExtranjero1.PaisDeResidParaEfecFisc.Text);
                                oPagosaextranjeros.NoBeneficiario.PaisDeResidParaEfecFisc = oc_Pais;
                            }
                            catch (Exception exc_Pais)
                            {
                                string mensaje = "Error en Pagos de Extranjero País. País (Valor: " + ucRetencionPagoExtranjero1.PaisDeResidParaEfecFisc.Text + "). " + exc_Pais.Message.ToString();
                                if (exc_Pais.InnerException != null)
                                {
                                    mensaje += Environment.NewLine + exc_Pais.InnerException.Message.ToString();
                                }
                                MessageBox.Show(mensaje, Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error);
                                return false;
                            }

                            try
                            {
                                //c_TipoContribuyenteSujetoRetencion oc_TipoContribuyenteSujetoRetencion = (c_TipoContribuyenteSujetoRetencion)System.Enum.Parse(typeof(c_Pais), ucRetencionPagoExtranjero1.ConceptoPago.Text);

                                oPagosaextranjeros.NoBeneficiario.ConceptoPago = c_TipoContribuyenteSujetoRetencion.Item1;
                                if (ucRetencionPagoExtranjero1.ConceptoPago.Text.Contains("2"))
                                {
                                    oPagosaextranjeros.NoBeneficiario.ConceptoPago = c_TipoContribuyenteSujetoRetencion.Item2;
                                }
                                if (ucRetencionPagoExtranjero1.ConceptoPago.Text.Contains("3"))
                                {
                                    oPagosaextranjeros.NoBeneficiario.ConceptoPago = c_TipoContribuyenteSujetoRetencion.Item3;
                                }
                                if (ucRetencionPagoExtranjero1.ConceptoPago.Text.Contains("4"))
                                {
                                    oPagosaextranjeros.NoBeneficiario.ConceptoPago = c_TipoContribuyenteSujetoRetencion.Item4;
                                }
                                if (ucRetencionPagoExtranjero1.ConceptoPago.Text.Contains("5"))
                                {
                                    oPagosaextranjeros.NoBeneficiario.ConceptoPago = c_TipoContribuyenteSujetoRetencion.Item5;
                                }
                                if (ucRetencionPagoExtranjero1.ConceptoPago.Text.Contains("6"))
                                {
                                    oPagosaextranjeros.NoBeneficiario.ConceptoPago = c_TipoContribuyenteSujetoRetencion.Item6;
                                }
                                if (ucRetencionPagoExtranjero1.ConceptoPago.Text.Contains("7"))
                                {
                                    oPagosaextranjeros.NoBeneficiario.ConceptoPago = c_TipoContribuyenteSujetoRetencion.Item7;
                                }
                                if (ucRetencionPagoExtranjero1.ConceptoPago.Text.Contains("8"))
                                {
                                    oPagosaextranjeros.NoBeneficiario.ConceptoPago = c_TipoContribuyenteSujetoRetencion.Item8;
                                }
                                if (ucRetencionPagoExtranjero1.ConceptoPago.Text.Contains("9"))
                                {
                                    oPagosaextranjeros.NoBeneficiario.ConceptoPago = c_TipoContribuyenteSujetoRetencion.Item9;
                                }

                            }
                            catch (Exception exc_TipoContribuyenteSujetoRetencion)
                            {
                                string mensaje = "Error en Pagos de Extranjero Concepto de Pago.  (Valor: " + ucRetencionPagoExtranjero1.ConceptoPago.Text + "). "
                                    + exc_TipoContribuyenteSujetoRetencion.Message.ToString();
                                if (exc_TipoContribuyenteSujetoRetencion.InnerException != null)
                                {
                                    mensaje += Environment.NewLine + exc_TipoContribuyenteSujetoRetencion.InnerException.Message.ToString();
                                }
                                MessageBox.Show(mensaje, Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error);
                                return false;
                            }
                            oPagosaextranjeros.EsBenefEfectDelCobro = PagosaextranjerosEsBenefEfectDelCobro.NO;
                            if (ucRetencionPagoExtranjero1.esBeficiarioCobro.Text.Contains("SI"))
                            {
                                oPagosaextranjeros.EsBenefEfectDelCobro = PagosaextranjerosEsBenefEfectDelCobro.SI;
                            }


                            oPagosaextranjeros.NoBeneficiario.DescripcionConcepto = ucRetencionPagoExtranjero1.DescripcionConcepto.Text;

                            oRetenciones.Complemento.Any = new XmlElement[1];
                            XmlDocument doc = new XmlDocument();

                            ////xmlns:pagosaextranjeros="http://www.sat.gob.mx/esquemas/retencionpago/1/pagosaextranjeros"
                            //XmlSerializerNamespaces myNamespaces_PagoExtranjeros= new XmlSerializerNamespaces();
                            //myNamespaces_PagoExtranjeros.Add("pagosaextranjeros", "http://www.sat.gob.mx/esquemas/retencionpago/1/pagosaextranjeros");

                            //using (XmlWriter writer = doc.CreateNavigator().AppendChild())
                            //{
                            //    new XmlSerializer(myNamespaces_PagoExtranjeros.GetType()).Serialize(writer, oPagosaextranjeros, myNamespaces_PagoExtranjeros);
                            //}

                            var serializer = new XmlSerializer(typeof(Pagosaextranjeros));

                            using (XmlWriter writer = doc.CreateNavigator().AppendChild())
                            {
                                serializer.Serialize(writer, oPagosaextranjeros);
                            }


                            int indice = oRetenciones.Complemento.Any.Length - 1;

                            doc.DocumentElement.Attributes.RemoveNamedItem("xmlns:xsd");
                            doc.DocumentElement.Attributes.RemoveNamedItem("xmlns:xsi");


                            //<pagosaextranjeros:Pagosaextranjeros Version="1.0" EsBenefEfectDelCobro="SI">
                            //<pagosaextranjeros:NoBeneficiario PaisDeResidParaEfecFisc="IT" ConceptoPago="6" DescripcionConcepto="ESTA ES LA DESCRIPCION"/>
                            //<pagosaextranjeros:Beneficiario RFC="AAAA010101AA0" CURP="AAAA010101HJCXXX01" NomDenRazSocB="PRUEBA DE PERSONA FISICA" ConceptoPago="6" DescripcionConcepto="ESTA ES LA DESCRIPCION"/>
                            //</pagosaextranjeros:Pagosaextranjeros>

                            //sDirectory + "/XSLT/retenciones.xslt"
                            oRetenciones.Complemento.Any[indice] = doc.DocumentElement;

                            //Cadena Original del PagosExtranjero
                            //cadenaOriginalPagoextanjero = cadenaOriginalPagosextranjeros(oPagosaextranjeros);
                        }
                    }
                }
                catch (Exception exPagosExtranjeros)
                {
                    MessageBox.Show("Error en Pagos Extranjeros. " + exPagosExtranjeros.Message.ToString()
                        , Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return false;
                }

                cGeneracion ocGeneracion = new cGeneracion(oData, oData_ERP);
                string DIRECTORIO_ARCHIVOS = "";
                DIRECTORIO_ARCHIVOS += ocSERIE.DIRECTORIO + @"\" + ocSERIE.ID + @"\" + ocGeneracion.Fechammmyy(oRetenciones.FechaExp.ToString());
                string XML = DIRECTORIO_ARCHIVOS;

                string PDF = XML + @"\" + "pdf";
                if (!Directory.Exists(PDF))
                {
                    try
                    {

                        Directory.CreateDirectory(PDF);
                    }
                    catch (IOException excep)
                    {
                        MessageBox.Show(excep.Message.ToString() + Environment.NewLine + "Creando directorio de PDF"
                            , Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error);
                        return false;
                    }
                }

                XML += @"\" + "xml";
                if (Directory.Exists(XML) == false)
                    Directory.CreateDirectory(XML);

                string XML_Archivo = "";

                string nombre_archivo = ocSERIE.ID + INVOICE_ID;

                if (Directory.Exists(DIRECTORIO_ARCHIVOS))
                {
                    XML_Archivo = XML + @"\" + nombre_archivo + ".xml";
                }
                else
                {
                    XML_Archivo = @"\" + nombre_archivo + ".xml"; ;
                }

                XML_Archivo = XML + @"\" + nombre_archivo + ".xml";

                generarArchivoXML(XML_Archivo,oRetenciones);

                
                //ReplaceInFile(ARCHIVO, " xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\"", "");

                //Generar Cadena
                string cadena = cadenaRetencion(ARCHIVO);
                //oRetenciones.Sello = ocGeneracion.SelloRSA(ocCERTIFICADO.LLAVE, ocCERTIFICADO.PASSWORD, cadena, decimal.Parse(ejercicio.Text));

                oRetenciones.Sello = sellarBase64(ocCERTIFICADO.LLAVE, ocCERTIFICADO.PASSWORD, cadena);
                generarArchivoXML(XML_Archivo, oRetenciones);


                return true;
            }
            catch (Exception errores)
            {
                string mensajeError = "Error en generación. " + errores.Message;

                if (errores.InnerException != null)
                {
                    mensajeError += Environment.NewLine + errores.InnerException.Message;
                }

                MessageBox.Show(mensajeError, "Error", MessageBoxButtons.OK);
                return false;
            }
        }

        private string sellarBase64(string llave, string password, string cadena)
        {
            try
            {
                StringBuilder sbPassword;
                StringBuilder sbPrivateKey;
                byte[] b;
                byte[] block;
                int keyBytes;



                string strKeyFile = llave;

                sbPassword = new StringBuilder(password);

                sbPrivateKey = Rsa.ReadEncPrivateKey(strKeyFile, sbPassword.ToString());

                keyBytes = Rsa.KeyBytes(sbPrivateKey.ToString());

                b = System.Text.Encoding.UTF8.GetBytes(cadena);


                block = Rsa.EncodeMsgForSignature(keyBytes, b, HashAlgorithm.Sha1);

                block = Rsa.RawPrivate(block, sbPrivateKey.ToString());

                string resultado = System.Convert.ToBase64String(block);
                return resultado;

                //Wipe.String(sbPassword);
                //Wipe.String(sbPrivateKey);
                //Wipe.Data(block);


                //var plainTextBytes = System.Text.Encoding.UTF8.GetBytes(cadena);
                //return System.Convert.ToBase64String(plainTextBytes);
            }
            catch (Exception e)
            {
                ErrorFX.mostrar(e, true, false, "frmTraslado - 687 - ");
            }
            return null;
        }

        private void generarArchivoXML(string XML_Archivo, Retenciones oRetenciones)
        {
            XmlSerializer serializerXML = new XmlSerializer(typeof(Retenciones));
            XmlSerializerNamespaces myNamespacesXML = new XmlSerializerNamespaces();
            myNamespacesXML.Add("retenciones", "http://www.sat.gob.mx/esquemas/retencionpago/2");
            if (oRetenciones.CveRetenc == c_CveRetenc.Item18)
            {
                myNamespacesXML.Add("pagosaextranjeros", "http://www.sat.gob.mx/esquemas/retencionpago/1/pagosaextranjeros");
            }

            XmlDocument docRetencion = new XmlDocument();
            using (XmlWriter writerRetencion = docRetencion.CreateNavigator().AppendChild())
            {
                serializerXML.Serialize(writerRetencion, oRetenciones);
            }
            TextWriter writerXML = new StreamWriter(XML_Archivo);
            serializerXML.Serialize(writerXML, oRetenciones, myNamespacesXML);
            writerXML.Close();
            ARCHIVO = XML_Archivo;
            PATH.Text = ARCHIVO;
            ReplaceInFile(ARCHIVO, "d1p1", "xsi");
            //xmlns:pagosaextranjeros
            if (oRetenciones.CveRetenc == c_CveRetenc.Item18)
            {
                ReplaceInFile(ARCHIVO, "Pagosaextranjeros", "pagosaextranjeros:Pagosaextranjeros");
                ReplaceInFile(ARCHIVO, "NoBeneficiario", "pagosaextranjeros:NoBeneficiario");
            }
            else
            {
                //ReplaceInFile(ARCHIVO, "http://www.sat.gob.mx/esquemas/retencionpago/2/pagosaextranjeros http://www.sat.gob.mx/esquemas/retencionpago/1/pagosaextranjeros/pagosaextranjeros.xsd", "");            
            }


        }

        private string obtenerCadenaOriginalPagoextanjero(string archivo)
        {
            try
            {
                XmlDocument doc = new XmlDocument();
                doc.Load(archivo);
                XmlNodeList nodo = doc.GetElementsByTagName("pagosaextranjeros:Pagosaextranjeros");
                if (nodo != null)
                {
                    //Crear Cadena
                    //Cargar xslt
                    string sDirectory = AppDomain.CurrentDomain.BaseDirectory;
                    string xslt = sDirectory + "/XSLT/pagosaextranjeros.xslt";
                    XslCompiledTransform myXslTrans = new XslCompiledTransform();
                    string nombre_tmp = "TMP" + DateTime.Now.ToString("ddMMyyyyhhmmss");
                    try
                    {
                        //Version 3
                        myXslTrans.Load(xslt);
                    }
                    catch (XmlException errores)
                    {
                        MessageBox.Show("Error en " + errores.InnerException.ToString());
                        return "";
                    }
                    XPathDocument myXPathDoc;
                    try
                    {
                        XmlSerializer serializer = new XmlSerializer(typeof(Pagosaextranjeros));
                        XmlSerializerNamespaces myNamespaces = new XmlSerializerNamespaces();
                        myNamespaces.Add("pagosaextranjeros", "http://www.sat.gob.mx/esquemas/retencionpago/1/pagosaextranjeros");
                        TextWriter writer = new StreamWriter(nombre_tmp + ".XML");
                        serializer.Serialize(writer, nodo, myNamespaces);
                        writer.Close();

                        myXPathDoc = new XPathDocument(nombre_tmp + ".XML");
                        //XslTransform myXslTrans = new XslTransform();
                        myXslTrans = new XslCompiledTransform();
                        myXslTrans.Load(xslt);
                        XmlUrlResolver resolver = new XmlUrlResolver();

                        resolver.Credentials = System.Net.CredentialCache.DefaultCredentials;
                        XsltSettings settings = new XsltSettings(true, true);
                    }
                    catch (XmlException errores)
                    {
                        MessageBox.Show("Error en " + errores.InnerException.ToString());
                        return "";
                    }

                    XmlTextWriter myWriter = new XmlTextWriter(nombre_tmp + ".TXT", null);

                    //Transformar al XML
                    myXslTrans.Transform(myXPathDoc, null, myWriter);

                    myWriter.Close();

                    //<pagosaextranjeros:Pagosaextranjeros Version="1.0" EsBenefEfectDelCobro="SI">
                    //<pagosaextranjeros:NoBeneficiario PaisDeResidParaEfecFisc="IT" ConceptoPago="6" DescripcionConcepto="ESTA ES LA DESCRIPCION"/>
                    //<pagosaextranjeros:Beneficiario RFC="AAAA010101AA0" CURP="AAAA010101HJCXXX01" NomDenRazSocB="PRUEBA DE PERSONA FISICA" ConceptoPago="6" DescripcionConcepto="ESTA ES LA DESCRIPCION"/>
                    //</pagosaextranjeros:Pagosaextranjeros>



                    //Abrir Archivo TXT
                    string CADENA_ORIGINAL = "";
                    StreamReader re = File.OpenText(nombre_tmp + ".TXT");
                    string input = null;
                    while ((input = re.ReadLine()) != null)
                    {
                        CADENA_ORIGINAL += input;
                    }
                    re.Close();

                    //Eliminar Archivos Temporales
                    File.Delete(nombre_tmp + ".XML");

                    //Buscar el &amp; en la Cadena y sustituirlo por &
                    CADENA_ORIGINAL = CADENA_ORIGINAL.Replace("&amp;", "&");


                    return CADENA_ORIGINAL;
                }
            }
            catch (Exception errores)
            {
                MessageBox.Show("Error en obtenerCadenaOriginalPagoextanjero. " + errores.Message.ToString() + Environment.NewLine + errores.InnerException.Message.ToString());
            }
            return null;
        }

        private string cadenaRetencion(string ARCHIVO)
        {
            //Cargar el XML
            StreamReader reader = new StreamReader(ARCHIVO);
            XPathDocument myXPathDoc = new XPathDocument(reader);

            //Cargando el XSLT
            string sDirectory = AppDomain.CurrentDomain.BaseDirectory;
            XslCompiledTransform myXslTrans = new XslCompiledTransform();
            myXslTrans.Load(sDirectory + "/XSLT/retenciones.xslt");

            StringWriter str = new StringWriter();
            XmlTextWriter myWriter = new XmlTextWriter(str);

            //Aplicando transformacion
            myXslTrans.Transform(myXPathDoc, null, myWriter);

            //Resultado
            string result = str.ToString();

            return result;
        }

        public string cadenaOriginalPagosextranjeros(Pagosaextranjeros oPagosaextranjeros)
        {
            //Crear Cadena
            //Cargar xslt
            string sDirectory = AppDomain.CurrentDomain.BaseDirectory;
            string xslt = sDirectory + "/XSLT/pagosaextranjeros.xslt";
            XslCompiledTransform myXslTrans = new XslCompiledTransform();
            string nombre_tmp = "TMP" + DateTime.Now.ToString("ddMMyyyyhhmmss");
            try
            {
                //Version 3
                myXslTrans.Load(xslt);
            }
            catch (XmlException errores)
            {
                MessageBox.Show("Error en " + errores.InnerException.ToString());
                return "";
            }
            XPathDocument myXPathDoc;
            try
            {
                XmlSerializer serializer = new XmlSerializer(typeof(Pagosaextranjeros));
                XmlSerializerNamespaces myNamespaces = new XmlSerializerNamespaces();
                myNamespaces.Add("pagosaextranjeros", "http://www.sat.gob.mx/esquemas/retencionpago/1/pagosaextranjeros");
                TextWriter writer = new StreamWriter(nombre_tmp + ".XML");
                serializer.Serialize(writer, oPagosaextranjeros, myNamespaces);
                writer.Close();
                ReplaceInFile(nombre_tmp + ".XML", "Pagosaextranjeros", "pagosaextranjeros:Pagosaextranjeros");
                ReplaceInFile(nombre_tmp + ".XML", "NoBeneficiario", "pagosaextranjeros:NoBeneficiario");

                myXPathDoc = new XPathDocument(nombre_tmp + ".XML");
                //XslTransform myXslTrans = new XslTransform();
                myXslTrans = new XslCompiledTransform();
                myXslTrans.Load(xslt);
                XmlUrlResolver resolver = new XmlUrlResolver();

                resolver.Credentials = System.Net.CredentialCache.DefaultCredentials;
                XsltSettings settings = new XsltSettings(true, true);
            }
            catch (XmlException errores)
            {
                MessageBox.Show("Error en " + errores.InnerException.ToString());
                return "";
            }

            XmlTextWriter myWriter = new XmlTextWriter(nombre_tmp + ".TXT", null);

            //Transformar al XML
            myXslTrans.Transform(myXPathDoc, null, myWriter);

            myWriter.Close();

            //<pagosaextranjeros:Pagosaextranjeros Version="1.0" EsBenefEfectDelCobro="SI">
            //<pagosaextranjeros:NoBeneficiario PaisDeResidParaEfecFisc="IT" ConceptoPago="6" DescripcionConcepto="ESTA ES LA DESCRIPCION"/>
            //<pagosaextranjeros:Beneficiario RFC="AAAA010101AA0" CURP="AAAA010101HJCXXX01" NomDenRazSocB="PRUEBA DE PERSONA FISICA" ConceptoPago="6" DescripcionConcepto="ESTA ES LA DESCRIPCION"/>
            //</pagosaextranjeros:Pagosaextranjeros>

            //xmlns:pagosaextranjeros


            //Abrir Archivo TXT
            string CADENA_ORIGINAL = "";
            StreamReader re = File.OpenText(nombre_tmp + ".TXT");
            string input = null;
            while ((input = re.ReadLine()) != null)
            {
                CADENA_ORIGINAL += input;
            }
            re.Close();

            //Eliminar Archivos Temporales
            File.Delete(nombre_tmp + ".XML");

            //Buscar el &amp; en la Cadena y sustituirlo por &
            CADENA_ORIGINAL = CADENA_ORIGINAL.Replace("&amp;", "&");


            return CADENA_ORIGINAL;
        }

        public void ReplaceInFile(string filePath, string searchText, string replaceText)
        {
            StreamReader reader = new StreamReader(filePath);
            string content = reader.ReadToEnd();
            reader.Close();

            content = Regex.Replace(content, searchText, replaceText);

            StreamWriter writer = new StreamWriter(filePath);
            writer.Write(content);
            writer.Close();
        }

        private void labelX13_Click(object sender, EventArgs e)
        {

        }

        private void buttonX18_Click(object sender, EventArgs e)
        {
            verArchivo(PATH.Text);
        }

        private void buttonX17_Click(object sender, EventArgs e)
        {
            verArchivo(PATH.Text);
        }

        private void verArchivo(string archivo)
        {
            string directorio = AppDomain.CurrentDomain.BaseDirectory;
                frmFacturaXML oObjeto = new frmFacturaXML(archivo);
                oObjeto.Show();
        }

        private void buttonX4_Click_1(object sender, EventArgs e)
        {
            insertaLineaPoliza();
        }

        private void insertaLineaPoliza()
        {
            dtgPolizas.Rows.Add();
        }

        private void buttonX6_Click(object sender, EventArgs e)
        {
            eliminarLineaPoliza();
        }

        private void eliminarLineaPoliza()
        {
            this.dtgPolizas.EndEdit();
            if (this.dtgPolizas.SelectedRows.Count > 0)
            {
                DialogResult Resultado = MessageBox.Show("¿Esta seguro de eliminar las líneas de las pólizas seleccionadas?", Application.ProductName + "-" + Application.ProductVersion, MessageBoxButtons.YesNo);
                if (Resultado.ToString() != "Yes")
                {
                    return;
                }
                foreach (DataGridViewRow drv in this.dtgPolizas.SelectedRows)
                {
                    this.dtgPolizas.Rows.Remove(drv);
                }
            }
        }


        private void buttonX3_Click(object sender, EventArgs e)
        {
            generarPoliza();
        }

        public void generarPoliza()
        {
            string TIPO_POLIZA = "Todas";
            dtgPolizas.SelectAll();
            dtgPolizas.EndEdit();
            if (dtgPolizas.SelectedCells.Count > 0)
            {
               var fileName = "Poliza " + oRetenciones.FolioInt + " " + DateTime.Now.ToString("yyyy-MM-dd-hh-mm-ss") + ".csv";
                if (folderBrowserDialog1.ShowDialog() == DialogResult.OK)
                {
                    string archivo = fileName;
                    fileName = folderBrowserDialog1.SelectedPath + "\\" + fileName;
                    using (StreamWriter sw = File.AppendText(fileName))
                    {
                        foreach (DataGridViewRow drv in dtgPolizas.SelectedRows)
                        {
                            string registro_tmp = "";
                            registro_tmp = drv.Cells["ACCOUNT_ID"].Value.ToString() + "," + drv.Cells["REFERENCE"].Value.ToString() + "," + oData.formato_decimal(drv.Cells["CARGO"].Value.ToString(), "S") + "," + oData.formato_decimal(drv.Cells["ABONO"].Value.ToString(), "S") + Environment.NewLine;
                            sw.WriteLine(registro_tmp);
                        }
                        sw.Close();
                    }
                    var tempFileName = Path.GetTempFileName();
                    try
                    {
                        using (var streamReader = new StreamReader(fileName))
                        using (var streamWriter = new StreamWriter(tempFileName))
                        {
                            string line;
                            while ((line = streamReader.ReadLine()) != null)
                            {
                                if (!String.IsNullOrEmpty(line))
                                    streamWriter.WriteLine(line);
                            }
                        }
                        File.Copy(tempFileName, fileName, true);
                    }
                    finally
                    {
                        File.Delete(tempFileName);
                    }
                    MessageBoxEx.Show("Poliza Generada del archivo " + Environment.NewLine + fileName + " finalizada.", Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
            else
            {
                MessageBoxEx.Show("Seleccione alguna línea.", Application.ProductName + "-" + Application.ProductVersion, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }



        }

        private void buttonX7_Click(object sender, EventArgs e)
        {
            imprimir();
        }

        private void imprimir()
        {

        }

        private void buttonX10_Click(object sender, EventArgs e)
        {
            agregarImpuesto();
        }

        private void buttonX11_Click(object sender, EventArgs e)
        {
            eliminarImpuesto();
        }

        private void agregarImpuesto()
        {
            int n=this.dtgTotales.Rows.Add();
            dtgTotales.Rows[n].Cells["Impuesto"].Value = "01-ISR";
            dtgTotales.Rows[n].Cells["TipoPagoRet"].Value = "Pago definitivo IVA";
            dtgTotales.Rows[n].Cells["montoRet"].Value = "0";
            dtgTotales.Rows[n].Cells["BaseRet"].Value = "0";
        }

        private void eliminarImpuesto()
        {
            this.dtgTotales.EndEdit();
            if (this.dtgTotales.SelectedRows.Count > 0)
            {
                DialogResult Resultado = MessageBox.Show("¿Esta seguro de eliminar las líneas de las impuestos seleccionadas?", Application.ProductName + "-" + Application.ProductVersion, MessageBoxButtons.YesNo);
                if (Resultado.ToString() != "Yes")
                {
                    return;
                }
                foreach (DataGridViewRow drv in this.dtgTotales.SelectedRows)
                {
                    this.dtgTotales.Rows.Remove(drv);
                }
            }
        }

        private void labelX5_Click(object sender, EventArgs e)
        {

        }

        private void frmRetencion2_Load(object sender, EventArgs e)
        {

        }

        private void calcular()
        {
            //double OPERACION_tr = 0;
            //double GRAVADO_tr = 0;
            //double RETENIDO_tr = 0;
            //double EXENTO_tr = 0;

            double montoRet_tr = 0;
            double BaseRet_tr = 0;

            for (int i = 0; i < dtgTotales.Rows.Count; i++)
            {
                if (Globales.IsNumeric(dtgTotales.Rows[i].Cells["montoRet"].Value.ToString()))
                {
                    montoRet_tr += double.Parse(dtgTotales.Rows[i].Cells["montoRet"].Value.ToString().Replace("$", ""));
                }
                if (Globales.IsNumeric(dtgTotales.Rows[i].Cells["BaseRet"].Value.ToString()))
                {
                    BaseRet_tr += double.Parse(dtgTotales.Rows[i].Cells["BaseRet"].Value.ToString().Replace("$", ""));

                }
                
            }

            montoTotOperacion.Text = BaseRet_tr.ToString();
            montoTotGrav.Text = BaseRet_tr.ToString();
            montoTotRet.Text = montoRet_tr.ToString();
        }

        private void dtgTotales_Leave(object sender, EventArgs e)
        {
            calcular();
        }

        private void buttonX8_Click(object sender, EventArgs e)
        {
            calcular();
        }

        private void buttonX9_Click(object sender, EventArgs e)
        {
            cancelar();
        }

        private void cancelar()
        {
            //Llamar el Servicio
            CFDiClient oCFDiClient = new CFDiClient();
            string[] uuid = new string[1];
            uuid[0] = UUID.Text;

            //Cargar el archivo pfx creado desde OpenSSL
            string pfx = ocCERTIFICADO.PFX;
            if (!File.Exists(pfx))
            {
                //Validar el directorio

                string directorio = AppDomain.CurrentDomain.BaseDirectory;
                pfx = directorio + @"\" + ocCERTIFICADO.PFX;
                if (!File.Exists(pfx))
                {
                    MessageBox.Show("El PFX " + ocCERTIFICADO.PFX + " no existe."
                        , Application.ProductName + "-" + Application.ProductVersion, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                    return;
                }
            }
            byte[] pfx_archivo = ReadBinaryFile(pfx);
            try
            {
                CancelaResponse respuesta;
                //Todo: Realizar cancelacion
                //respuesta = oCFDiClient.cancelaCFDi(Globales.oCFDI_USUARIO.USUARIO, cENCRIPTACION.Decrypt(Globales.oCFDI_USUARIO.PASSWORD), ocEMPRESA.RFC, uuid, pfx_archivo, ocCERTIFICADO.PASSWORD);
                return;
            }
            catch (FaultException oException)
            {

                CFDiException oCFDiException = new CFDiException();
                MessageBox.Show(oException.Message, Application.ProductName + "-" + Application.ProductVersion, MessageBoxButtons.OK, MessageBoxIcon.Error);
                informacion.Text = "Error: Factura NO CANCELADA " + DateTime.Now.ToShortDateString() + " " + DateTime.Now.ToLongTimeString();
                return;
            }
        }

        private void CveRetencCmb_SelectedValueChanged(object sender, EventArgs e)
        {
            if (CveRetencCmb.SelectedValue != null)
            {
                Item oItem = (Item) CveRetencCmb.SelectedItem;

                if (oItem.Value.ToString().Equals("18"))
                {
                    DescRetenc.Text = "";
                    DescRetenc.Text = "Pagos realizados a favor de residentes en el extranjero";
                }
            }

        }

        private void nacionalidad_SelectedValueChanged(object sender, EventArgs e)
        {
            if (nacionalidad.Text.Equals("Nacional"))
            {
                CURPE.Visible = this.labelX4.Visible = true;
            }
            else
            {
                CURPE.Visible = this.labelX4.Visible = false;
            }
        }
    }
}
