﻿namespace FE_FX
{
    partial class frmProformaComplementoCE
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmProformaComplementoCE));
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.informacion = new System.Windows.Forms.ToolStripStatusLabel();
            this.comboItem4 = new DevComponents.Editors.ComboItem();
            this.comboItem5 = new DevComponents.Editors.ComboItem();
            this.comboItem6 = new DevComponents.Editors.ComboItem();
            this.buttonX17 = new DevComponents.DotNetBar.ButtonX();
            this.labelX13 = new DevComponents.DotNetBar.LabelX();
            this.UUID = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.ESTADO = new DevComponents.DotNetBar.Controls.ComboBoxEx();
            this.comboItem3 = new DevComponents.Editors.ComboItem();
            this.comboItem1 = new DevComponents.Editors.ComboItem();
            this.comboItem2 = new DevComponents.Editors.ComboItem();
            this.SERIE = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.ID = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.comboItem14 = new DevComponents.Editors.ComboItem();
            this.comboItem15 = new DevComponents.Editors.ComboItem();
            this.comboItem16 = new DevComponents.Editors.ComboItem();
            this.ENTITY_ID = new DevComponents.DotNetBar.Controls.ComboBoxEx();
            this.comboItem17 = new DevComponents.Editors.ComboItem();
            this.comboItem18 = new DevComponents.Editors.ComboItem();
            this.comboItem19 = new DevComponents.Editors.ComboItem();
            this.labelX11 = new DevComponents.DotNetBar.LabelX();
            this.EMPRESA_NOMBRE = new DevComponents.DotNetBar.LabelX();
            this.labelX14 = new DevComponents.DotNetBar.LabelX();
            this.labelX15 = new DevComponents.DotNetBar.LabelX();
            this.PATH = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.labelX19 = new DevComponents.DotNetBar.LabelX();
            this.folderBrowserDialog1 = new System.Windows.Forms.FolderBrowserDialog();
            this.FORMATO_IMPRESION = new DevComponents.DotNetBar.Controls.ComboBoxEx();
            this.btnEmisor = new DevComponents.DotNetBar.ButtonX();
            this.buttonX9 = new DevComponents.DotNetBar.ButtonX();
            this.buttonX7 = new DevComponents.DotNetBar.ButtonX();
            this.buttonX5 = new DevComponents.DotNetBar.ButtonX();
            this.buttonX2 = new DevComponents.DotNetBar.ButtonX();
            this.buttonX1 = new DevComponents.DotNetBar.ButtonX();
            this.labelX2 = new DevComponents.DotNetBar.LabelX();
            this.labelX1 = new DevComponents.DotNetBar.LabelX();
            this.labelX3 = new DevComponents.DotNetBar.LabelX();
            this.labelX4 = new DevComponents.DotNetBar.LabelX();
            this.cantidad = new System.Windows.Forms.TextBox();
            this.valorUnitario = new System.Windows.Forms.TextBox();
            this.rfc = new System.Windows.Forms.TextBox();
            this.receptortxt = new System.Windows.Forms.TextBox();
            this.Concepto = new System.Windows.Forms.TextBox();
            this.METODO_DE_PAGO = new System.Windows.Forms.ComboBox();
            this.USO_CFDI = new System.Windows.Forms.ComboBox();
            this.FORMA_DE_PAGO = new System.Windows.Forms.ComboBox();
            this.label7 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.fecha = new System.Windows.Forms.DateTimePicker();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.ClaveUnidad = new System.Windows.Forms.TextBox();
            this.labelX8 = new DevComponents.DotNetBar.LabelX();
            this.ClaveProdServ = new System.Windows.Forms.TextBox();
            this.labelX7 = new DevComponents.DotNetBar.LabelX();
            this.importe = new System.Windows.Forms.TextBox();
            this.labelX5 = new DevComponents.DotNetBar.LabelX();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.labelX6 = new DevComponents.DotNetBar.LabelX();
            this.labelX9 = new DevComponents.DotNetBar.LabelX();
            this.textBox2 = new System.Windows.Forms.TextBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.ValorDolares = new System.Windows.Forms.TextBox();
            this.labelX10 = new DevComponents.DotNetBar.LabelX();
            this.CantidadAduana = new System.Windows.Forms.TextBox();
            this.ValorUnitarioAduana = new System.Windows.Forms.TextBox();
            this.labelX12 = new DevComponents.DotNetBar.LabelX();
            this.labelX16 = new DevComponents.DotNetBar.LabelX();
            this.statusStrip1.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.SuspendLayout();
            // 
            // statusStrip1
            // 
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.informacion});
            this.statusStrip1.Location = new System.Drawing.Point(0, 346);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(831, 22);
            this.statusStrip1.TabIndex = 33;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // informacion
            // 
            this.informacion.Name = "informacion";
            this.informacion.Size = new System.Drawing.Size(42, 17);
            this.informacion.Text = "Estado";
            // 
            // comboItem4
            // 
            this.comboItem4.Text = "FECHA";
            // 
            // comboItem5
            // 
            this.comboItem5.Text = "FACTURA";
            // 
            // comboItem6
            // 
            this.comboItem6.Text = "CLIENTE";
            // 
            // buttonX17
            // 
            this.buttonX17.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.buttonX17.BackColor = System.Drawing.Color.Moccasin;
            this.buttonX17.ColorTable = DevComponents.DotNetBar.eButtonColor.Flat;
            this.buttonX17.Font = new System.Drawing.Font("Segoe UI", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonX17.ImagePosition = DevComponents.DotNetBar.eImagePosition.Right;
            this.buttonX17.Location = new System.Drawing.Point(769, 35);
            this.buttonX17.Name = "buttonX17";
            this.buttonX17.Size = new System.Drawing.Size(54, 20);
            this.buttonX17.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.buttonX17.TabIndex = 10;
            this.buttonX17.Text = "Ver XML";
            this.buttonX17.TextColor = System.Drawing.Color.Black;
            this.buttonX17.Click += new System.EventHandler(this.buttonX17_Click);
            // 
            // labelX13
            // 
            this.labelX13.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX13.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX13.ForeColor = System.Drawing.Color.Black;
            this.labelX13.Location = new System.Drawing.Point(201, 11);
            this.labelX13.Name = "labelX13";
            this.labelX13.Size = new System.Drawing.Size(65, 20);
            this.labelX13.TabIndex = 4;
            this.labelX13.Text = "Folio Fiscal";
            this.labelX13.TextAlignment = System.Drawing.StringAlignment.Far;
            this.labelX13.Click += new System.EventHandler(this.labelX13_Click);
            // 
            // UUID
            // 
            this.UUID.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            // 
            // 
            // 
            this.UUID.Border.Class = "TextBoxBorder";
            this.UUID.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.UUID.ForeColor = System.Drawing.Color.Black;
            this.UUID.Location = new System.Drawing.Point(272, 12);
            this.UUID.Name = "UUID";
            this.UUID.ReadOnly = true;
            this.UUID.Size = new System.Drawing.Size(274, 20);
            this.UUID.TabIndex = 5;
            // 
            // ESTADO
            // 
            this.ESTADO.DisplayMember = "Text";
            this.ESTADO.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.ESTADO.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ESTADO.ForeColor = System.Drawing.Color.Black;
            this.ESTADO.FormattingEnabled = true;
            this.ESTADO.ItemHeight = 16;
            this.ESTADO.Items.AddRange(new object[] {
            this.comboItem3,
            this.comboItem1,
            this.comboItem2});
            this.ESTADO.Location = new System.Drawing.Point(721, 61);
            this.ESTADO.Name = "ESTADO";
            this.ESTADO.Size = new System.Drawing.Size(102, 22);
            this.ESTADO.TabIndex = 18;
            this.ESTADO.Text = "BORRADOR";
            // 
            // comboItem3
            // 
            this.comboItem3.Text = "BORRADOR";
            // 
            // comboItem1
            // 
            this.comboItem1.FontStyle = System.Drawing.FontStyle.Bold;
            this.comboItem1.Text = "TIMBRADO";
            // 
            // comboItem2
            // 
            this.comboItem2.FontStyle = System.Drawing.FontStyle.Bold;
            this.comboItem2.ForeColor = System.Drawing.Color.Red;
            this.comboItem2.Text = "CANCELADO";
            // 
            // SERIE
            // 
            this.SERIE.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            // 
            // 
            // 
            this.SERIE.Border.Class = "TextBoxBorder";
            this.SERIE.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.SERIE.Enabled = false;
            this.SERIE.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.SERIE.ForeColor = System.Drawing.Color.Black;
            this.SERIE.Location = new System.Drawing.Point(532, 62);
            this.SERIE.Margin = new System.Windows.Forms.Padding(0);
            this.SERIE.Name = "SERIE";
            this.SERIE.Size = new System.Drawing.Size(48, 22);
            this.SERIE.TabIndex = 15;
            this.SERIE.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // ID
            // 
            this.ID.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            // 
            // 
            // 
            this.ID.Border.Class = "TextBoxBorder";
            this.ID.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.ID.Enabled = false;
            this.ID.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ID.ForeColor = System.Drawing.Color.Black;
            this.ID.Location = new System.Drawing.Point(580, 62);
            this.ID.Margin = new System.Windows.Forms.Padding(0);
            this.ID.Name = "ID";
            this.ID.Size = new System.Drawing.Size(67, 22);
            this.ID.TabIndex = 16;
            this.ID.Text = "000000";
            this.ID.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // comboItem14
            // 
            this.comboItem14.Text = "FACTURA";
            // 
            // comboItem15
            // 
            this.comboItem15.Text = "NOTA DE CREDITO";
            // 
            // comboItem16
            // 
            this.comboItem16.Text = "RECIBO HONORARIOS";
            // 
            // ENTITY_ID
            // 
            this.ENTITY_ID.DisplayMember = "Text";
            this.ENTITY_ID.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.ENTITY_ID.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ENTITY_ID.ForeColor = System.Drawing.Color.Black;
            this.ENTITY_ID.FormattingEnabled = true;
            this.ENTITY_ID.ItemHeight = 16;
            this.ENTITY_ID.Items.AddRange(new object[] {
            this.comboItem17,
            this.comboItem18,
            this.comboItem19});
            this.ENTITY_ID.Location = new System.Drawing.Point(56, 62);
            this.ENTITY_ID.Name = "ENTITY_ID";
            this.ENTITY_ID.Size = new System.Drawing.Size(143, 22);
            this.ENTITY_ID.TabIndex = 12;
            this.ENTITY_ID.SelectedIndexChanged += new System.EventHandler(this.ENTITY_ID_SelectedIndexChanged);
            // 
            // comboItem17
            // 
            this.comboItem17.Text = "BORRADOR";
            // 
            // comboItem18
            // 
            this.comboItem18.FontStyle = System.Drawing.FontStyle.Bold;
            this.comboItem18.Text = "TIMBRADO";
            // 
            // comboItem19
            // 
            this.comboItem19.FontStyle = System.Drawing.FontStyle.Bold;
            this.comboItem19.ForeColor = System.Drawing.Color.Red;
            this.comboItem19.Text = "CANCELADO";
            // 
            // labelX11
            // 
            this.labelX11.AutoSize = true;
            this.labelX11.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX11.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX11.ForeColor = System.Drawing.Color.Black;
            this.labelX11.Location = new System.Drawing.Point(13, 66);
            this.labelX11.Name = "labelX11";
            this.labelX11.Size = new System.Drawing.Size(37, 15);
            this.labelX11.TabIndex = 11;
            this.labelX11.Text = "Emisor";
            // 
            // EMPRESA_NOMBRE
            // 
            this.EMPRESA_NOMBRE.BackColor = System.Drawing.Color.DarkGray;
            // 
            // 
            // 
            this.EMPRESA_NOMBRE.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.EMPRESA_NOMBRE.ForeColor = System.Drawing.Color.Black;
            this.EMPRESA_NOMBRE.Location = new System.Drawing.Point(205, 66);
            this.EMPRESA_NOMBRE.Name = "EMPRESA_NOMBRE";
            this.EMPRESA_NOMBRE.Size = new System.Drawing.Size(264, 15);
            this.EMPRESA_NOMBRE.TabIndex = 13;
            // 
            // labelX14
            // 
            this.labelX14.AutoSize = true;
            this.labelX14.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX14.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX14.ForeColor = System.Drawing.Color.Black;
            this.labelX14.Location = new System.Drawing.Point(552, 14);
            this.labelX14.Name = "labelX14";
            this.labelX14.Size = new System.Drawing.Size(43, 15);
            this.labelX14.TabIndex = 8;
            this.labelX14.Text = "Formato";
            this.labelX14.Visible = false;
            this.labelX14.Click += new System.EventHandler(this.labelX12_Click);
            // 
            // labelX15
            // 
            this.labelX15.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX15.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX15.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelX15.ForeColor = System.Drawing.Color.Black;
            this.labelX15.Location = new System.Drawing.Point(638, 92);
            this.labelX15.Name = "labelX15";
            this.labelX15.Size = new System.Drawing.Size(24, 17);
            this.labelX15.TabIndex = 21;
            this.labelX15.Text = "RFC";
            // 
            // PATH
            // 
            this.PATH.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            // 
            // 
            // 
            this.PATH.Border.Class = "TextBoxBorder";
            this.PATH.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.PATH.ForeColor = System.Drawing.Color.Black;
            this.PATH.Location = new System.Drawing.Point(272, 35);
            this.PATH.Name = "PATH";
            this.PATH.ReadOnly = true;
            this.PATH.Size = new System.Drawing.Size(491, 20);
            this.PATH.TabIndex = 7;
            // 
            // labelX19
            // 
            this.labelX19.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX19.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX19.ForeColor = System.Drawing.Color.Black;
            this.labelX19.Location = new System.Drawing.Point(219, 36);
            this.labelX19.Name = "labelX19";
            this.labelX19.Size = new System.Drawing.Size(47, 16);
            this.labelX19.TabIndex = 6;
            this.labelX19.Text = "Archivo";
            this.labelX19.TextAlignment = System.Drawing.StringAlignment.Far;
            this.labelX19.Click += new System.EventHandler(this.labelX13_Click);
            // 
            // FORMATO_IMPRESION
            // 
            this.FORMATO_IMPRESION.DisplayMember = "Text";
            this.FORMATO_IMPRESION.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.FORMATO_IMPRESION.ForeColor = System.Drawing.Color.Black;
            this.FORMATO_IMPRESION.FormattingEnabled = true;
            this.FORMATO_IMPRESION.ItemHeight = 16;
            this.FORMATO_IMPRESION.Location = new System.Drawing.Point(601, 10);
            this.FORMATO_IMPRESION.Name = "FORMATO_IMPRESION";
            this.FORMATO_IMPRESION.Size = new System.Drawing.Size(222, 22);
            this.FORMATO_IMPRESION.TabIndex = 9;
            this.FORMATO_IMPRESION.Visible = false;
            this.FORMATO_IMPRESION.WatermarkText = "Formato de impresión";
            // 
            // btnEmisor
            // 
            this.btnEmisor.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btnEmisor.BackColor = System.Drawing.Color.Moccasin;
            this.btnEmisor.ColorTable = DevComponents.DotNetBar.eButtonColor.Office2007WithBackground;
            this.btnEmisor.Image = global::FE_FX.Properties.Resources.ic_search_black_18dp_1x;
            this.btnEmisor.Location = new System.Drawing.Point(13, 91);
            this.btnEmisor.Name = "btnEmisor";
            this.btnEmisor.Size = new System.Drawing.Size(85, 20);
            this.btnEmisor.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.btnEmisor.TabIndex = 19;
            this.btnEmisor.Text = "Receptor";
            this.btnEmisor.TextAlignment = DevComponents.DotNetBar.eButtonTextAlignment.Left;
            this.btnEmisor.TextColor = System.Drawing.Color.Black;
            this.btnEmisor.Click += new System.EventHandler(this.buttonX4_Click);
            // 
            // buttonX9
            // 
            this.buttonX9.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.buttonX9.BackColor = System.Drawing.Color.DeepSkyBlue;
            this.buttonX9.ColorTable = DevComponents.DotNetBar.eButtonColor.Flat;
            this.buttonX9.Font = new System.Drawing.Font("Segoe UI", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonX9.Image = global::FE_FX.Properties.Resources.ic_cancel_black_24dp_1x;
            this.buttonX9.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top;
            this.buttonX9.Location = new System.Drawing.Point(155, 12);
            this.buttonX9.Name = "buttonX9";
            this.buttonX9.Size = new System.Drawing.Size(42, 41);
            this.buttonX9.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.buttonX9.TabIndex = 3;
            this.buttonX9.Text = "Cancelar";
            this.buttonX9.TextColor = System.Drawing.Color.Black;
            this.buttonX9.Visible = false;
            this.buttonX9.Click += new System.EventHandler(this.buttonX9_Click);
            // 
            // buttonX7
            // 
            this.buttonX7.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.buttonX7.BackColor = System.Drawing.Color.DeepSkyBlue;
            this.buttonX7.ColorTable = DevComponents.DotNetBar.eButtonColor.Flat;
            this.buttonX7.Font = new System.Drawing.Font("Segoe UI", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonX7.Image = global::FE_FX.Properties.Resources.ic_local_printshop_black_24dp_1x;
            this.buttonX7.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top;
            this.buttonX7.Location = new System.Drawing.Point(108, 12);
            this.buttonX7.Name = "buttonX7";
            this.buttonX7.Size = new System.Drawing.Size(41, 41);
            this.buttonX7.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.buttonX7.TabIndex = 2;
            this.buttonX7.Text = "Imprimir";
            this.buttonX7.TextColor = System.Drawing.Color.Black;
            this.buttonX7.Click += new System.EventHandler(this.buttonX7_Click);
            // 
            // buttonX5
            // 
            this.buttonX5.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.buttonX5.BackColor = System.Drawing.Color.DeepSkyBlue;
            this.buttonX5.ColorTable = DevComponents.DotNetBar.eButtonColor.Flat;
            this.buttonX5.Font = new System.Drawing.Font("Segoe UI", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonX5.Image = global::FE_FX.Properties.Resources.ic_save_black_24dp_1x;
            this.buttonX5.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top;
            this.buttonX5.Location = new System.Drawing.Point(60, 12);
            this.buttonX5.Name = "buttonX5";
            this.buttonX5.Size = new System.Drawing.Size(42, 41);
            this.buttonX5.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.buttonX5.TabIndex = 1;
            this.buttonX5.Text = "Guardar";
            this.buttonX5.TextColor = System.Drawing.Color.Black;
            this.buttonX5.Click += new System.EventHandler(this.buttonX5_Click);
            // 
            // buttonX2
            // 
            this.buttonX2.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.buttonX2.BackColor = System.Drawing.Color.DeepSkyBlue;
            this.buttonX2.ColorTable = DevComponents.DotNetBar.eButtonColor.Flat;
            this.buttonX2.Font = new System.Drawing.Font("Segoe UI", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonX2.Image = global::FE_FX.Properties.Resources.ic_create_new_folder_black_24dp_1x;
            this.buttonX2.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top;
            this.buttonX2.Location = new System.Drawing.Point(12, 12);
            this.buttonX2.Name = "buttonX2";
            this.buttonX2.Size = new System.Drawing.Size(42, 41);
            this.buttonX2.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.buttonX2.TabIndex = 0;
            this.buttonX2.Text = "Nuevo";
            this.buttonX2.TextColor = System.Drawing.Color.Black;
            this.buttonX2.Click += new System.EventHandler(this.buttonX2_Click_1);
            // 
            // buttonX1
            // 
            this.buttonX1.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.buttonX1.BackColor = System.Drawing.Color.Moccasin;
            this.buttonX1.ColorTable = DevComponents.DotNetBar.eButtonColor.Flat;
            this.buttonX1.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonX1.Image = global::FE_FX.Properties.Resources.ic_search_black_18dp_1x;
            this.buttonX1.Location = new System.Drawing.Point(475, 62);
            this.buttonX1.Name = "buttonX1";
            this.buttonX1.Size = new System.Drawing.Size(54, 22);
            this.buttonX1.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.buttonX1.TabIndex = 14;
            this.buttonX1.Text = "Serie";
            this.buttonX1.TextAlignment = DevComponents.DotNetBar.eButtonTextAlignment.Right;
            this.buttonX1.TextColor = System.Drawing.Color.Black;
            this.buttonX1.Click += new System.EventHandler(this.buttonX1_Click);
            // 
            // labelX2
            // 
            this.labelX2.AutoSize = true;
            this.labelX2.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX2.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX2.ForeColor = System.Drawing.Color.Black;
            this.labelX2.Location = new System.Drawing.Point(691, 66);
            this.labelX2.Name = "labelX2";
            this.labelX2.Size = new System.Drawing.Size(24, 15);
            this.labelX2.TabIndex = 17;
            this.labelX2.Text = "Tipo";
            this.labelX2.Visible = false;
            this.labelX2.Click += new System.EventHandler(this.labelX2_Click);
            // 
            // labelX1
            // 
            this.labelX1.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX1.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX1.ForeColor = System.Drawing.Color.Black;
            this.labelX1.Location = new System.Drawing.Point(277, 21);
            this.labelX1.Name = "labelX1";
            this.labelX1.Size = new System.Drawing.Size(61, 16);
            this.labelX1.TabIndex = 4;
            this.labelX1.Text = "Concepto";
            // 
            // labelX3
            // 
            this.labelX3.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX3.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX3.ForeColor = System.Drawing.Color.Black;
            this.labelX3.Location = new System.Drawing.Point(15, 21);
            this.labelX3.Name = "labelX3";
            this.labelX3.Size = new System.Drawing.Size(49, 16);
            this.labelX3.TabIndex = 0;
            this.labelX3.Text = "Cantidad";
            // 
            // labelX4
            // 
            this.labelX4.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX4.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX4.ForeColor = System.Drawing.Color.Black;
            this.labelX4.Location = new System.Drawing.Point(116, 21);
            this.labelX4.Name = "labelX4";
            this.labelX4.Size = new System.Drawing.Size(70, 16);
            this.labelX4.TabIndex = 2;
            this.labelX4.Text = "Valor Unitario";
            // 
            // cantidad
            // 
            this.cantidad.Location = new System.Drawing.Point(63, 19);
            this.cantidad.Name = "cantidad";
            this.cantidad.Size = new System.Drawing.Size(47, 20);
            this.cantidad.TabIndex = 1;
            // 
            // valorUnitario
            // 
            this.valorUnitario.Location = new System.Drawing.Point(192, 19);
            this.valorUnitario.Name = "valorUnitario";
            this.valorUnitario.Size = new System.Drawing.Size(79, 20);
            this.valorUnitario.TabIndex = 3;
            // 
            // rfc
            // 
            this.rfc.Location = new System.Drawing.Point(668, 91);
            this.rfc.Name = "rfc";
            this.rfc.Size = new System.Drawing.Size(155, 20);
            this.rfc.TabIndex = 22;
            // 
            // receptortxt
            // 
            this.receptortxt.Location = new System.Drawing.Point(108, 91);
            this.receptortxt.Name = "receptortxt";
            this.receptortxt.Size = new System.Drawing.Size(524, 20);
            this.receptortxt.TabIndex = 20;
            // 
            // Concepto
            // 
            this.Concepto.Location = new System.Drawing.Point(337, 19);
            this.Concepto.Name = "Concepto";
            this.Concepto.Size = new System.Drawing.Size(307, 20);
            this.Concepto.TabIndex = 5;
            // 
            // METODO_DE_PAGO
            // 
            this.METODO_DE_PAGO.FormattingEnabled = true;
            this.METODO_DE_PAGO.Items.AddRange(new object[] {
            "01 Efectivo",
            "02 Cheque nominativo",
            "03 Transferencia electrónica de fondos",
            "04 Tarjetas de crédito",
            "05 Monederos electrónicos",
            "06 Dinero electrónico",
            "07 Tarjetas digitales",
            "08 Vales de despensa",
            "28 Tarjeta de Débito",
            "29 Tarjeta de Servicio",
            "99 Otros",
            "NA"});
            this.METODO_DE_PAGO.Location = new System.Drawing.Point(108, 117);
            this.METODO_DE_PAGO.Name = "METODO_DE_PAGO";
            this.METODO_DE_PAGO.Size = new System.Drawing.Size(311, 21);
            this.METODO_DE_PAGO.TabIndex = 24;
            // 
            // USO_CFDI
            // 
            this.USO_CFDI.FormattingEnabled = true;
            this.USO_CFDI.Items.AddRange(new object[] {
            "Pago en una sola exhibición",
            "Pago en parcialidades",
            "Pago a credito"});
            this.USO_CFDI.Location = new System.Drawing.Point(108, 144);
            this.USO_CFDI.Name = "USO_CFDI";
            this.USO_CFDI.Size = new System.Drawing.Size(311, 21);
            this.USO_CFDI.TabIndex = 28;
            // 
            // FORMA_DE_PAGO
            // 
            this.FORMA_DE_PAGO.FormattingEnabled = true;
            this.FORMA_DE_PAGO.Items.AddRange(new object[] {
            "Pago en una sola exhibición",
            "Pago en parcialidades",
            "Pago a credito"});
            this.FORMA_DE_PAGO.Location = new System.Drawing.Point(511, 117);
            this.FORMA_DE_PAGO.Name = "FORMA_DE_PAGO";
            this.FORMA_DE_PAGO.Size = new System.Drawing.Size(312, 21);
            this.FORMA_DE_PAGO.TabIndex = 26;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(16, 147);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(70, 13);
            this.label7.TabIndex = 27;
            this.label7.Text = "Uso del CFDI";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(16, 121);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(86, 13);
            this.label1.TabIndex = 23;
            this.label1.Text = "Método de Pago";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(426, 121);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(79, 13);
            this.label3.TabIndex = 25;
            this.label3.Text = "Forma de Pago";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(426, 147);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(40, 13);
            this.label2.TabIndex = 29;
            this.label2.Text = "Fecha ";
            // 
            // fecha
            // 
            this.fecha.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.fecha.Location = new System.Drawing.Point(472, 144);
            this.fecha.Name = "fecha";
            this.fecha.Size = new System.Drawing.Size(108, 20);
            this.fecha.TabIndex = 30;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.ClaveUnidad);
            this.groupBox1.Controls.Add(this.labelX8);
            this.groupBox1.Controls.Add(this.ClaveProdServ);
            this.groupBox1.Controls.Add(this.labelX7);
            this.groupBox1.Controls.Add(this.importe);
            this.groupBox1.Controls.Add(this.labelX5);
            this.groupBox1.Controls.Add(this.cantidad);
            this.groupBox1.Controls.Add(this.valorUnitario);
            this.groupBox1.Controls.Add(this.labelX3);
            this.groupBox1.Controls.Add(this.labelX4);
            this.groupBox1.Controls.Add(this.Concepto);
            this.groupBox1.Controls.Add(this.labelX1);
            this.groupBox1.Location = new System.Drawing.Point(12, 171);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(812, 74);
            this.groupBox1.TabIndex = 31;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Concepto";
            // 
            // ClaveUnidad
            // 
            this.ClaveUnidad.Location = new System.Drawing.Point(277, 43);
            this.ClaveUnidad.Name = "ClaveUnidad";
            this.ClaveUnidad.Size = new System.Drawing.Size(101, 20);
            this.ClaveUnidad.TabIndex = 11;
            // 
            // labelX8
            // 
            this.labelX8.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX8.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX8.ForeColor = System.Drawing.Color.Black;
            this.labelX8.Location = new System.Drawing.Point(207, 45);
            this.labelX8.Name = "labelX8";
            this.labelX8.Size = new System.Drawing.Size(75, 16);
            this.labelX8.TabIndex = 10;
            this.labelX8.Text = "ClaveUnidad";
            // 
            // ClaveProdServ
            // 
            this.ClaveProdServ.Location = new System.Drawing.Point(96, 43);
            this.ClaveProdServ.Name = "ClaveProdServ";
            this.ClaveProdServ.Size = new System.Drawing.Size(101, 20);
            this.ClaveProdServ.TabIndex = 9;
            // 
            // labelX7
            // 
            this.labelX7.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX7.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX7.ForeColor = System.Drawing.Color.Black;
            this.labelX7.Location = new System.Drawing.Point(15, 45);
            this.labelX7.Name = "labelX7";
            this.labelX7.Size = new System.Drawing.Size(110, 16);
            this.labelX7.TabIndex = 8;
            this.labelX7.Text = "ClaveProdServ";
            // 
            // importe
            // 
            this.importe.Location = new System.Drawing.Point(696, 19);
            this.importe.Name = "importe";
            this.importe.Size = new System.Drawing.Size(109, 20);
            this.importe.TabIndex = 7;
            // 
            // labelX5
            // 
            this.labelX5.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX5.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX5.ForeColor = System.Drawing.Color.Black;
            this.labelX5.Location = new System.Drawing.Point(650, 21);
            this.labelX5.Name = "labelX5";
            this.labelX5.Size = new System.Drawing.Size(53, 16);
            this.labelX5.TabIndex = 6;
            this.labelX5.Text = "Importe";
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(121, 19);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(101, 20);
            this.textBox1.TabIndex = 2;
            // 
            // labelX6
            // 
            this.labelX6.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX6.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX6.ForeColor = System.Drawing.Color.Black;
            this.labelX6.Location = new System.Drawing.Point(5, 21);
            this.labelX6.Name = "labelX6";
            this.labelX6.Size = new System.Drawing.Size(110, 16);
            this.labelX6.TabIndex = 1;
            this.labelX6.Text = "Fracción arancelaría";
            // 
            // labelX9
            // 
            this.labelX9.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX9.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX9.ForeColor = System.Drawing.Color.Black;
            this.labelX9.Location = new System.Drawing.Point(227, 21);
            this.labelX9.Name = "labelX9";
            this.labelX9.Size = new System.Drawing.Size(110, 16);
            this.labelX9.TabIndex = 3;
            this.labelX9.Text = "Unidad de Medida";
            // 
            // textBox2
            // 
            this.textBox2.Location = new System.Drawing.Point(323, 19);
            this.textBox2.Name = "textBox2";
            this.textBox2.Size = new System.Drawing.Size(101, 20);
            this.textBox2.TabIndex = 4;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.CantidadAduana);
            this.groupBox2.Controls.Add(this.ValorUnitarioAduana);
            this.groupBox2.Controls.Add(this.labelX12);
            this.groupBox2.Controls.Add(this.labelX16);
            this.groupBox2.Controls.Add(this.ValorDolares);
            this.groupBox2.Controls.Add(this.labelX10);
            this.groupBox2.Controls.Add(this.textBox2);
            this.groupBox2.Controls.Add(this.labelX6);
            this.groupBox2.Controls.Add(this.textBox1);
            this.groupBox2.Controls.Add(this.labelX9);
            this.groupBox2.Location = new System.Drawing.Point(13, 251);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(811, 84);
            this.groupBox2.TabIndex = 32;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Complemento de Comerció Exterior";
            // 
            // ValorDolares
            // 
            this.ValorDolares.Location = new System.Drawing.Point(470, 45);
            this.ValorDolares.Name = "ValorDolares";
            this.ValorDolares.Size = new System.Drawing.Size(109, 20);
            this.ValorDolares.TabIndex = 0;
            // 
            // labelX10
            // 
            this.labelX10.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX10.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX10.ForeColor = System.Drawing.Color.Black;
            this.labelX10.Location = new System.Drawing.Point(381, 47);
            this.labelX10.Name = "labelX10";
            this.labelX10.Size = new System.Drawing.Size(83, 16);
            this.labelX10.TabIndex = 9;
            this.labelX10.Text = "Valor Dolares";
            // 
            // CantidadAduana
            // 
            this.CantidadAduana.Location = new System.Drawing.Point(121, 45);
            this.CantidadAduana.Name = "CantidadAduana";
            this.CantidadAduana.Size = new System.Drawing.Size(47, 20);
            this.CantidadAduana.TabIndex = 6;
            // 
            // ValorUnitarioAduana
            // 
            this.ValorUnitarioAduana.Location = new System.Drawing.Point(296, 45);
            this.ValorUnitarioAduana.Name = "ValorUnitarioAduana";
            this.ValorUnitarioAduana.Size = new System.Drawing.Size(79, 20);
            this.ValorUnitarioAduana.TabIndex = 8;
            // 
            // labelX12
            // 
            this.labelX12.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX12.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX12.ForeColor = System.Drawing.Color.Black;
            this.labelX12.Location = new System.Drawing.Point(20, 47);
            this.labelX12.Name = "labelX12";
            this.labelX12.Size = new System.Drawing.Size(95, 16);
            this.labelX12.TabIndex = 5;
            this.labelX12.Text = "Cantidad Aduana";
            // 
            // labelX16
            // 
            this.labelX16.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX16.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX16.ForeColor = System.Drawing.Color.Black;
            this.labelX16.Location = new System.Drawing.Point(174, 47);
            this.labelX16.Name = "labelX16";
            this.labelX16.Size = new System.Drawing.Size(116, 16);
            this.labelX16.TabIndex = 7;
            this.labelX16.Text = "Valor Unitario Aduana";
            // 
            // frmProformaComplementoCE
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(831, 368);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.fecha);
            this.Controls.Add(this.METODO_DE_PAGO);
            this.Controls.Add(this.USO_CFDI);
            this.Controls.Add(this.FORMA_DE_PAGO);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.receptortxt);
            this.Controls.Add(this.rfc);
            this.Controls.Add(this.labelX2);
            this.Controls.Add(this.FORMATO_IMPRESION);
            this.Controls.Add(this.ENTITY_ID);
            this.Controls.Add(this.buttonX17);
            this.Controls.Add(this.labelX19);
            this.Controls.Add(this.labelX13);
            this.Controls.Add(this.PATH);
            this.Controls.Add(this.UUID);
            this.Controls.Add(this.labelX15);
            this.Controls.Add(this.btnEmisor);
            this.Controls.Add(this.buttonX9);
            this.Controls.Add(this.buttonX7);
            this.Controls.Add(this.buttonX5);
            this.Controls.Add(this.buttonX2);
            this.Controls.Add(this.labelX11);
            this.Controls.Add(this.EMPRESA_NOMBRE);
            this.Controls.Add(this.labelX14);
            this.Controls.Add(this.ESTADO);
            this.Controls.Add(this.SERIE);
            this.Controls.Add(this.buttonX1);
            this.Controls.Add(this.ID);
            this.Controls.Add(this.statusStrip1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.KeyPreview = true;
            this.MaximizeBox = false;
            this.Name = "frmProformaComplementoCE";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Proforma con Complemento de CCE";
            this.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.frmFormatos_KeyPress);
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStripStatusLabel informacion;
        private DevComponents.Editors.ComboItem comboItem4;
        private DevComponents.Editors.ComboItem comboItem5;
        private DevComponents.Editors.ComboItem comboItem6;
        private DevComponents.DotNetBar.ButtonX buttonX17;
        private DevComponents.DotNetBar.LabelX labelX13;
        private DevComponents.DotNetBar.Controls.TextBoxX UUID;
        private DevComponents.DotNetBar.ButtonX btnEmisor;
        private DevComponents.DotNetBar.ButtonX buttonX9;
        private DevComponents.DotNetBar.ButtonX buttonX7;
        private DevComponents.DotNetBar.ButtonX buttonX5;
        private DevComponents.DotNetBar.ButtonX buttonX2;
        private DevComponents.DotNetBar.Controls.ComboBoxEx ESTADO;
        private DevComponents.Editors.ComboItem comboItem3;
        private DevComponents.Editors.ComboItem comboItem1;
        private DevComponents.Editors.ComboItem comboItem2;
        private DevComponents.DotNetBar.Controls.TextBoxX SERIE;
        private DevComponents.DotNetBar.ButtonX buttonX1;
        private DevComponents.DotNetBar.Controls.TextBoxX ID;
        private DevComponents.Editors.ComboItem comboItem14;
        private DevComponents.Editors.ComboItem comboItem15;
        private DevComponents.Editors.ComboItem comboItem16;
        private DevComponents.DotNetBar.Controls.ComboBoxEx ENTITY_ID;
        private DevComponents.Editors.ComboItem comboItem17;
        private DevComponents.Editors.ComboItem comboItem18;
        private DevComponents.Editors.ComboItem comboItem19;
        private DevComponents.DotNetBar.LabelX labelX11;
        private DevComponents.DotNetBar.LabelX EMPRESA_NOMBRE;
        private DevComponents.DotNetBar.LabelX labelX14;
        private DevComponents.DotNetBar.LabelX labelX15;
        private DevComponents.DotNetBar.Controls.TextBoxX PATH;
        private DevComponents.DotNetBar.LabelX labelX19;
        private System.Windows.Forms.FolderBrowserDialog folderBrowserDialog1;
        private DevComponents.DotNetBar.Controls.ComboBoxEx FORMATO_IMPRESION;
        private DevComponents.DotNetBar.LabelX labelX2;
        private DevComponents.DotNetBar.LabelX labelX1;
        private DevComponents.DotNetBar.LabelX labelX3;
        private DevComponents.DotNetBar.LabelX labelX4;
        private System.Windows.Forms.TextBox cantidad;
        private System.Windows.Forms.TextBox valorUnitario;
        private System.Windows.Forms.TextBox rfc;
        private System.Windows.Forms.TextBox receptortxt;
        private System.Windows.Forms.TextBox Concepto;
        private System.Windows.Forms.ComboBox METODO_DE_PAGO;
        private System.Windows.Forms.ComboBox USO_CFDI;
        private System.Windows.Forms.ComboBox FORMA_DE_PAGO;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.DateTimePicker fecha;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.TextBox importe;
        private DevComponents.DotNetBar.LabelX labelX5;
        private System.Windows.Forms.TextBox ClaveUnidad;
        private DevComponents.DotNetBar.LabelX labelX8;
        private System.Windows.Forms.TextBox ClaveProdServ;
        private DevComponents.DotNetBar.LabelX labelX7;
        private System.Windows.Forms.TextBox textBox1;
        private DevComponents.DotNetBar.LabelX labelX6;
        private DevComponents.DotNetBar.LabelX labelX9;
        private System.Windows.Forms.TextBox textBox2;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.TextBox ValorDolares;
        private DevComponents.DotNetBar.LabelX labelX10;
        private System.Windows.Forms.TextBox CantidadAduana;
        private System.Windows.Forms.TextBox ValorUnitarioAduana;
        private DevComponents.DotNetBar.LabelX labelX12;
        private DevComponents.DotNetBar.LabelX labelX16;
    }
}