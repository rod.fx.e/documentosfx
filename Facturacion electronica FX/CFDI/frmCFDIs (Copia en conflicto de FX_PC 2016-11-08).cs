﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Text;
using System.Windows.Forms;
using System.Xml;
using System.Xml.Serialization;
using FE_FX.CFDI;
using DevComponents.DotNetBar;

namespace FE_FX
{
    public partial class frmCFDIs : Form
    {
        private cCONEXCION oData = new cCONEXCION("");
        string devolver = "";
        public DataGridViewSelectedRowCollection selecionados;
        string tipo = "";
        private cEMPRESA ocEMPRESA;
        private cSERIE ocSERIE;
        private cCONEXCION oData_ERP = new cCONEXCION("");
        cPolizaConfiguracion ocPolizaConfiguracion = new cPolizaConfiguracion();

        public frmCFDIs(string sConn)
        {
            oData.sConn = sConn;
            InitializeComponent();
            cargar_empresas();
            cargarConfiguracionPoliza();
        }
        private void cargar_empresas()
        {
            ENTITY_ID.Items.Clear();
            cEMPRESA ocEMPRESA = new cEMPRESA(oData);
            foreach (cEMPRESA registro in ocEMPRESA.todos("", true))
            {
                ENTITY_ID.Items.Add(registro);
            }
            if (ENTITY_ID.Items.Count > 0)
            {
                ENTITY_ID.SelectedIndex = 0;
            }
        }

        private void cargar()
        {
            int n;
            cADDENDA oObjeto = new cADDENDA(oData);
            toolStripStatusLabel1.Text = "Cargando ";
            dtgrdGeneral.Rows.Clear();
            toolStripStatusLabel1.Text = "Total " + dtgrdGeneral.Rows.Count.ToString();

        }

        private void button1_Click(object sender, EventArgs e)
        {
            cargar();
        }

        private void BUSCAR_KeyPress(object sender, KeyPressEventArgs e)
        {
            //Precionar Enter buscar
            if (e.KeyChar.ToString() == "\r")
            {
                cargar();
            }
            
        }

        private void frmUsuarios_Load(object sender, EventArgs e)
        {
            cargar();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            frmAddenda oObjeto = new frmAddenda(oData.sConn);
            oObjeto.ShowDialog();
            cargar();
        }

        private void dtgrdGeneral_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            modificar();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            if (dtgrdGeneral.SelectedRows.Count > 0)
            {
                DataGridViewSelectedRowCollection arrSelectedRows = dtgrdGeneral.SelectedRows;
                string ROW_ID = (string)arrSelectedRows[0].Cells[0].Value;
                frmAddenda oObjeto = new frmAddenda(oData.sConn, ROW_ID);
                oObjeto.ShowDialog();
                cargar();
            }
        }

        public void modificar()
        {
            if (dtgrdGeneral.SelectedRows.Count > 0)
            {
                DataGridViewSelectedRowCollection arrSelectedRows = dtgrdGeneral.SelectedRows;
                string archivotr = (string)arrSelectedRows[0].Cells["archivo"].Value;
                frmFacturaXML oObjeto = new frmFacturaXML(archivotr);
                oObjeto.Show();
            }
        }

        private void frmFormatos_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)27)
            {
                this.Close();
            }
            else
            {
                if ((e.KeyChar.ToString() == "<")
                                | (e.KeyChar.ToString() == ">")
                                
                                | (e.KeyChar.ToString() == "'")
                                | (e.KeyChar.ToString() == "#")
                                )
                {
                    e.Handled = true;
                }
            }
        }

        private void buttonX2_Click(object sender, EventArgs e)
        {
            frmCFDI ofrmCFDI = new frmCFDI(oData.sConn);
            ofrmCFDI.ShowDialog();
            cargar();
        }

        private void buttonX1_Click(object sender, EventArgs e)
        {
            cargar_archivos();
        }

        private void buttonX3_Click(object sender, EventArgs e)
        {
            buscarSerie();
        }

        private void buscarSerie()
        {

            frmSeries oBuscador = new frmSeries(oData.sConn, "S", ocEMPRESA.ROW_ID);
            if (oBuscador.ShowDialog() == DialogResult.OK)
            {
                string ROW_ID = (string)oBuscador.selecionados[0].Cells["ROW_ID"].Value;
                string IDtr = (string)oBuscador.selecionados[0].Cells["ID"].Value;
                ID.Text = IDtr;

                ocSERIE = new cSERIE(oData);

                ocSERIE.cargar_serie(IDtr);
                directorio.Text = ocSERIE.DIRECTORIO;
                //ID.Text = ocSERIE.consecutivo(ocSERIE.ROW_ID, false);
                cargar_archivos();

            }
        }

        private void cargar_archivos()
        {
            dtgrdGeneral.Rows.Clear();
            dtgPolizas.Rows.Clear();
            try
            {
                foreach (string archivo in Directory.GetFiles(directorio.Text, "*.xml", SearchOption.AllDirectories))
                {
                   
                    //Parsear archivo a CFDI
                    //Añadir la addenda
                    XmlSerializer serializer_CFD_3 = new XmlSerializer(typeof(Comprobante));
                    TextReader reader = new StreamReader(archivo);
                    Comprobante oComprobante = new Comprobante();
                    try
                    {
                        oComprobante = (Comprobante)serializer_CFD_3.Deserialize(reader);
                        reader.Dispose();
                        reader.Close();
                        int n = dtgrdGeneral.Rows.Add();
                        dtgrdGeneral.Rows[n].Cells["serie"].Value = oComprobante.serie;
                        dtgrdGeneral.Rows[n].Cells["folio"].Value = oComprobante.folio;
                        dtgrdGeneral.Rows[n].Cells["archivo"].Value = archivo;
                        dtgrdGeneral.Rows[n].Cells["fecha"].Value = oComprobante.fecha;
                        dtgrdGeneral.Rows[n].Cells["rfcEmisor"].Value = oComprobante.Emisor.rfc;
                        dtgrdGeneral.Rows[n].Cells["rfcReceptor"].Value = oComprobante.Receptor.rfc;
                        dtgrdGeneral.Rows[n].Cells["total"].Value = oComprobante.total;
                        dtgrdGeneral.Rows[n].Cells["impuestos"].Value = oComprobante.Impuestos.totalImpuestosTrasladados;

                        int f = this.dtgPolizas.Rows.Add();
                        string referencia=dtgrdGeneral.Rows[n].Cells["serie"].Value.ToString() + dtgrdGeneral.Rows[n].Cells["folio"].Value.ToString();

                        dtgPolizas.Rows[f].Cells["ACCOUNT_ID"].Value = ocPolizaConfiguracion.AccountAbonoTotal;
                        dtgPolizas.Rows[f].Cells["REFERENCE"].Value = referencia;
                        dtgPolizas.Rows[f].Cells["CARGO"].Value = "0";
                        dtgPolizas.Rows[f].Cells["ABONO"].Value = oComprobante.total.ToString();
                        f = this.dtgPolizas.Rows.Add();
                        dtgPolizas.Rows[f].Cells["ACCOUNT_ID"].Value = ocPolizaConfiguracion.AccountCargoTotal;
                        dtgPolizas.Rows[f].Cells["REFERENCE"].Value = referencia;
                        dtgPolizas.Rows[f].Cells["CARGO"].Value = oComprobante.total.ToString();
                        dtgPolizas.Rows[f].Cells["ABONO"].Value = "0";
                        if (oComprobante.Impuestos.totalImpuestosTrasladados != 0)
                        {
                            f = this.dtgPolizas.Rows.Add();
                            dtgPolizas.Rows[f].Cells["ACCOUNT_ID"].Value = ocPolizaConfiguracion.AccountAbonoImpuesto;
                            dtgPolizas.Rows[f].Cells["REFERENCE"].Value = referencia;
                            dtgPolizas.Rows[f].Cells["CARGO"].Value = "0";
                            dtgPolizas.Rows[f].Cells["ABONO"].Value = oComprobante.Impuestos.totalImpuestosTrasladados.ToString();
                            f = this.dtgPolizas.Rows.Add();
                            dtgPolizas.Rows[f].Cells["ACCOUNT_ID"].Value = ocPolizaConfiguracion.AccountCargoImpuesto;
                            dtgPolizas.Rows[f].Cells["REFERENCE"].Value = referencia;
                            dtgPolizas.Rows[f].Cells["CARGO"].Value = oComprobante.Impuestos.totalImpuestosTrasladados.ToString();
                            dtgPolizas.Rows[f].Cells["ABONO"].Value = "0";
                        }
                        
                    }
                    catch (XmlException xmlex)
                    {
                        Bitacora.Log("Errpr :: " + xmlex.ToString());
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message.ToString(), Application.ProductVersion, MessageBoxButtons.OK);
            }
        }

        private void ENTITY_ID_SelectedIndexChanged(object sender, EventArgs e)
        {
            actualizar_conexcion();
        }

        private void actualizar_conexcion()
        {

            ocEMPRESA = ENTITY_ID.SelectedItem as cEMPRESA;
            if (ocEMPRESA != null)
            {
                EMPRESA_NOMBRE.Text = ocEMPRESA.ID;
                oData_ERP = new cCONEXCION(ocEMPRESA.TIPO, ocEMPRESA.SERVIDOR, ocEMPRESA.BD, ocEMPRESA.USUARIO_BD, ocEMPRESA.PASSWORD_BD);
            }
        }

        private void buttonX5_Click(object sender, EventArgs e)
        {
            cargarDirectorio();
        }

        private void cargarDirectorio()
        {
            FolderBrowserDialog Folder = new FolderBrowserDialog();
            if (Folder.ShowDialog() == DialogResult.OK)
            {
                directorio.Text = Folder.SelectedPath;
                cargar_archivos();
            }
        }

        private void buttonX6_Click(object sender, EventArgs e)
        {
            frmACCOUNT ofrmACCOUNT = new frmACCOUNT(oData_ERP.sConn);
            if (ofrmACCOUNT.ShowDialog() == DialogResult.OK)
            {
                accountAbonoTotal.Text = ofrmACCOUNT.selecionados[0].Cells[0].Value.ToString();
            }
        }

        private void buttonX7_Click(object sender, EventArgs e)
        {
            frmACCOUNT ofrmACCOUNT = new frmACCOUNT(oData_ERP.sConn);
            if (ofrmACCOUNT.ShowDialog() == DialogResult.OK)
            {
                accountCargoTotal.Text = ofrmACCOUNT.selecionados[0].Cells[0].Value.ToString();
            }
        }

        private void buttonX10_Click(object sender, EventArgs e)
        {
            frmACCOUNT ofrmACCOUNT = new frmACCOUNT(oData_ERP.sConn);
            if (ofrmACCOUNT.ShowDialog() == DialogResult.OK)
            {
                accountAbonoImpuesto.Text = ofrmACCOUNT.selecionados[0].Cells[0].Value.ToString();
            }
        }

        private void buttonX9_Click(object sender, EventArgs e)
        {
            frmACCOUNT ofrmACCOUNT = new frmACCOUNT(oData_ERP.sConn);
            if (ofrmACCOUNT.ShowDialog() == DialogResult.OK)
            {
                accountCargoImpuesto.Text = ofrmACCOUNT.selecionados[0].Cells[0].Value.ToString();
            }
        }

        private void buttonX11_Click(object sender, EventArgs e)
        {
            guardarConfiguracionPoliza();
        }

        private void guardarConfiguracionPoliza()
        {
            
            ocPolizaConfiguracion.AccountAbonoTotal = accountAbonoTotal.Text;
            ocPolizaConfiguracion.AccountCargoTotal = accountCargoTotal.Text;
            ocPolizaConfiguracion.AccountAbonoImpuesto = accountAbonoImpuesto.Text;
            ocPolizaConfiguracion.AccountCargoImpuesto = accountCargoImpuesto.Text;
            XmlSerializer SerializerObj = new XmlSerializer(typeof(cPolizaConfiguracion));
            TextWriter WriteFileStream = new StreamWriter(@"FEFXConfiguracionPolizas.xml");
            SerializerObj.Serialize(WriteFileStream, ocPolizaConfiguracion);
            WriteFileStream.Close();
        }

        private void cargarConfiguracionPoliza()
        {
            try
            {
                XmlSerializer SerializerObj = new XmlSerializer(typeof(cPolizaConfiguracion));
                FileStream ReadFileStream = new FileStream(@"FEFXConfiguracionPolizas.xml", FileMode.Open, FileAccess.Read, FileShare.Read);
                //cPolizaConfiguracion LoadedObj = (cPolizaConfiguracion)SerializerObj.Deserialize(ReadFileStream);
                ocPolizaConfiguracion = (cPolizaConfiguracion)SerializerObj.Deserialize(ReadFileStream);
                ReadFileStream.Close();
                accountAbonoTotal.Text = ocPolizaConfiguracion.AccountAbonoTotal;
                accountCargoTotal.Text = ocPolizaConfiguracion.AccountCargoTotal;
                accountAbonoImpuesto.Text = ocPolizaConfiguracion.AccountAbonoImpuesto;
                accountCargoImpuesto.Text = ocPolizaConfiguracion.AccountCargoImpuesto;
            }
            catch
            {

            }
        }

        private void buttonX8_Click(object sender, EventArgs e)
        {
            generarPoliza();
        }

        public void generarPoliza()
        {
            string TIPO_POLIZA = "Todas";
            dtgPolizas.SelectAll();
            dtgPolizas.EndEdit();
            if (dtgPolizas.SelectedCells.Count > 0)
            {
                var fileName = "Polizas " + DateTime.Now.ToString("yyyy-MM-dd-hh-mm-ss") + ".csv";
                if (folderBrowserDialog1.ShowDialog() == DialogResult.OK)
                {
                    string archivo = fileName;
                    fileName = folderBrowserDialog1.SelectedPath + "\\" + fileName;
                    using (StreamWriter sw = File.AppendText(fileName))
                    {
                        foreach (DataGridViewRow drv in dtgPolizas.SelectedRows)
                        {
                            string registro_tmp = "";
                            registro_tmp = drv.Cells["ACCOUNT_ID"].Value.ToString() + "," + drv.Cells["REFERENCE"].Value.ToString() + "," + oData.formato_decimal(drv.Cells["CARGO"].Value.ToString(), "S") + "," + oData.formato_decimal(drv.Cells["ABONO"].Value.ToString(), "S") + Environment.NewLine;
                            sw.WriteLine(registro_tmp);
                        }
                        sw.Close();
                    }
                    var tempFileName = Path.GetTempFileName();
                    try
                    {
                        using (var streamReader = new StreamReader(fileName))
                        using (var streamWriter = new StreamWriter(tempFileName))
                        {
                            string line;
                            while ((line = streamReader.ReadLine()) != null)
                            {
                                if (!String.IsNullOrEmpty(line))
                                    streamWriter.WriteLine(line);
                            }
                        }
                        File.Copy(tempFileName, fileName, true);
                    }
                    finally
                    {
                        File.Delete(tempFileName);
                    }
                    MessageBoxEx.Show("Poliza Generada del archivo " + Environment.NewLine + fileName + " finalizada.", Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
            else
            {
                MessageBoxEx.Show("Seleccione alguna línea.", Application.ProductName + "-" + Application.ProductVersion, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }

    }
}
