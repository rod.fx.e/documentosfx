#region Copyright Syncfusion Inc. 2001 - 2017
// Copyright Syncfusion Inc. 2001 - 2017. All rights reserved.
// Use of this code is subject to the terms of our license.
// A copy of the current license can be obtained at any time by e-mailing
// licensing@syncfusion.com. Any infringement will be prosecuted under
// applicable laws. 
#endregion
namespace FE_FX.CFDI
{
    partial class frmTraslado
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmTraslado));
            this.btnDeleteItem = new System.Windows.Forms.Button();
            this.btnNuevo = new System.Windows.Forms.Button();
            this.btnAddItem = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.btnImprimir = new System.Windows.Forms.Button();
            this.emisor = new System.Windows.Forms.ComboBox();
            this.button5 = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.serie = new System.Windows.Forms.TextBox();
            this.dtg = new System.Windows.Forms.DataGridView();
            this.Cantidad = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ClaveProdServ = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.NoIdentificacion = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ClaveUnidad = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Unidad = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Descripcion = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ValorUnitario = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Importe = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.NumeroPedimento = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Especie = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.label7 = new System.Windows.Forms.Label();
            this.uuid = new System.Windows.Forms.TextBox();
            this.pdf = new System.Windows.Forms.TextBox();
            this.xml = new System.Windows.Forms.TextBox();
            this.btnPDF = new System.Windows.Forms.Button();
            this.btnXML = new System.Windows.Forms.Button();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.LeyendaFiscal = new System.Windows.Forms.TextBox();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.observacionesPDF = new System.Windows.Forms.TextBox();
            this.fecha = new System.Windows.Forms.DateTimePicker();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.button6 = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.moneda = new System.Windows.Forms.ComboBox();
            this.tipoCambio = new System.Windows.Forms.TextBox();
            this.informacion = new System.Windows.Forms.Label();
            this.estado = new System.Windows.Forms.ComboBox();
            this.receptor = new System.Windows.Forms.ComboBox();
            this.nombre = new System.Windows.Forms.TextBox();
            this.button2 = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.origen = new System.Windows.Forms.ComboBox();
            this.label8 = new System.Windows.Forms.Label();
            this.destino = new System.Windows.Forms.ComboBox();
            this.label9 = new System.Windows.Forms.Label();
            this.button4 = new System.Windows.Forms.Button();
            this.button7 = new System.Windows.Forms.Button();
            this.label10 = new System.Windows.Forms.Label();
            this.UsoCFDI = new System.Windows.Forms.ComboBox();
            this.OrigenDireccion = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.DestinoDireccion = new System.Windows.Forms.TextBox();
            this.label13 = new System.Windows.Forms.Label();
            this.DomicilioFiscalReceptor = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.RegimenFiscalReceptor = new System.Windows.Forms.TextBox();
            this.label14 = new System.Windows.Forms.Label();
            this.button8 = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.dtg)).BeginInit();
            this.tabControl1.SuspendLayout();
            this.tabPage3.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.tabPage2.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnDeleteItem
            // 
            this.btnDeleteItem.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(219)))), ((int)(((byte)(94)))), ((int)(((byte)(94)))));
            this.btnDeleteItem.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnDeleteItem.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnDeleteItem.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnDeleteItem.Location = new System.Drawing.Point(303, 12);
            this.btnDeleteItem.Name = "btnDeleteItem";
            this.btnDeleteItem.Size = new System.Drawing.Size(94, 28);
            this.btnDeleteItem.TabIndex = 3;
            this.btnDeleteItem.Text = "Cancelar";
            this.btnDeleteItem.UseVisualStyleBackColor = false;
            this.btnDeleteItem.Click += new System.EventHandler(this.btnDeleteItem_Click);
            // 
            // btnNuevo
            // 
            this.btnNuevo.BackColor = System.Drawing.Color.SteelBlue;
            this.btnNuevo.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnNuevo.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnNuevo.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnNuevo.Location = new System.Drawing.Point(12, 12);
            this.btnNuevo.Name = "btnNuevo";
            this.btnNuevo.Size = new System.Drawing.Size(91, 28);
            this.btnNuevo.TabIndex = 0;
            this.btnNuevo.Text = "Nuevo";
            this.btnNuevo.UseVisualStyleBackColor = false;
            this.btnNuevo.Click += new System.EventHandler(this.buttonAdv1_Click);
            // 
            // btnAddItem
            // 
            this.btnAddItem.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(153)))), ((int)(((byte)(181)))), ((int)(((byte)(105)))));
            this.btnAddItem.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnAddItem.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAddItem.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnAddItem.Location = new System.Drawing.Point(109, 12);
            this.btnAddItem.Name = "btnAddItem";
            this.btnAddItem.Size = new System.Drawing.Size(91, 28);
            this.btnAddItem.TabIndex = 1;
            this.btnAddItem.Text = "Guardar";
            this.btnAddItem.UseVisualStyleBackColor = false;
            this.btnAddItem.Click += new System.EventHandler(this.btnAddItem_Click);
            // 
            // button1
            // 
            this.button1.BackColor = System.Drawing.Color.Green;
            this.button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button1.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button1.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.button1.Location = new System.Drawing.Point(12, 242);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(86, 28);
            this.button1.TabIndex = 21;
            this.button1.Text = "Importar";
            this.button1.UseVisualStyleBackColor = false;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // btnImprimir
            // 
            this.btnImprimir.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(128)))));
            this.btnImprimir.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnImprimir.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnImprimir.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnImprimir.Location = new System.Drawing.Point(206, 12);
            this.btnImprimir.Name = "btnImprimir";
            this.btnImprimir.Size = new System.Drawing.Size(91, 28);
            this.btnImprimir.TabIndex = 2;
            this.btnImprimir.Text = "Imprimir";
            this.btnImprimir.UseVisualStyleBackColor = false;
            this.btnImprimir.Click += new System.EventHandler(this.button2_Click);
            // 
            // emisor
            // 
            this.emisor.FormattingEnabled = true;
            this.emisor.Location = new System.Drawing.Point(65, 46);
            this.emisor.Name = "emisor";
            this.emisor.Size = new System.Drawing.Size(135, 21);
            this.emisor.TabIndex = 7;
            this.emisor.SelectedIndexChanged += new System.EventHandler(this.emisor_SelectedIndexChanged);
            // 
            // button5
            // 
            this.button5.BackColor = System.Drawing.Color.DarkKhaki;
            this.button5.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button5.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button5.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.button5.Location = new System.Drawing.Point(206, 42);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(76, 28);
            this.button5.TabIndex = 8;
            this.button5.Text = "Serie";
            this.button5.UseVisualStyleBackColor = false;
            this.button5.Click += new System.EventHandler(this.button5_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(15, 49);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(38, 13);
            this.label2.TabIndex = 6;
            this.label2.Text = "Emisor";
            // 
            // serie
            // 
            this.serie.Location = new System.Drawing.Point(288, 46);
            this.serie.Name = "serie";
            this.serie.Size = new System.Drawing.Size(109, 20);
            this.serie.TabIndex = 9;
            // 
            // dtg
            // 
            this.dtg.AllowUserToAddRows = false;
            this.dtg.AllowUserToDeleteRows = false;
            this.dtg.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dtg.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dtg.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Cantidad,
            this.ClaveProdServ,
            this.NoIdentificacion,
            this.ClaveUnidad,
            this.Unidad,
            this.Descripcion,
            this.ValorUnitario,
            this.Importe,
            this.NumeroPedimento,
            this.Especie});
            this.dtg.Location = new System.Drawing.Point(12, 276);
            this.dtg.Name = "dtg";
            this.dtg.RowHeadersVisible = false;
            this.dtg.Size = new System.Drawing.Size(774, 147);
            this.dtg.TabIndex = 23;
            // 
            // Cantidad
            // 
            this.Cantidad.HeaderText = "Cnt";
            this.Cantidad.Name = "Cantidad";
            this.Cantidad.Width = 50;
            // 
            // ClaveProdServ
            // 
            this.ClaveProdServ.HeaderText = "ClaveProdServ";
            this.ClaveProdServ.Name = "ClaveProdServ";
            this.ClaveProdServ.Width = 80;
            // 
            // NoIdentificacion
            // 
            this.NoIdentificacion.HeaderText = "NoIdentificacion";
            this.NoIdentificacion.Name = "NoIdentificacion";
            this.NoIdentificacion.ReadOnly = true;
            // 
            // ClaveUnidad
            // 
            this.ClaveUnidad.HeaderText = "ClaveUnidad";
            this.ClaveUnidad.Name = "ClaveUnidad";
            this.ClaveUnidad.ReadOnly = true;
            this.ClaveUnidad.Width = 90;
            // 
            // Unidad
            // 
            this.Unidad.HeaderText = "Unidad";
            this.Unidad.Name = "Unidad";
            this.Unidad.Width = 130;
            // 
            // Descripcion
            // 
            this.Descripcion.HeaderText = "Descripcion";
            this.Descripcion.Name = "Descripcion";
            this.Descripcion.ReadOnly = true;
            // 
            // ValorUnitario
            // 
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.ValorUnitario.DefaultCellStyle = dataGridViewCellStyle1;
            this.ValorUnitario.HeaderText = "ValorUnitario";
            this.ValorUnitario.Name = "ValorUnitario";
            this.ValorUnitario.Width = 70;
            // 
            // Importe
            // 
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.Importe.DefaultCellStyle = dataGridViewCellStyle2;
            this.Importe.HeaderText = "Importe";
            this.Importe.Name = "Importe";
            this.Importe.Width = 90;
            // 
            // NumeroPedimento
            // 
            this.NumeroPedimento.HeaderText = "NumeroPedimento";
            this.NumeroPedimento.Name = "NumeroPedimento";
            // 
            // Especie
            // 
            this.Especie.HeaderText = "Especie";
            this.Especie.Name = "Especie";
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage3);
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Location = new System.Drawing.Point(12, 429);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(774, 130);
            this.tabControl1.TabIndex = 24;
            // 
            // tabPage3
            // 
            this.tabPage3.Controls.Add(this.label7);
            this.tabPage3.Controls.Add(this.uuid);
            this.tabPage3.Controls.Add(this.pdf);
            this.tabPage3.Controls.Add(this.xml);
            this.tabPage3.Controls.Add(this.btnPDF);
            this.tabPage3.Controls.Add(this.btnXML);
            this.tabPage3.Location = new System.Drawing.Point(4, 22);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage3.Size = new System.Drawing.Size(766, 104);
            this.tabPage3.TabIndex = 2;
            this.tabPage3.Text = "Informaci�n general";
            this.tabPage3.UseVisualStyleBackColor = true;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(61, 79);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(56, 13);
            this.label7.TabIndex = 4;
            this.label7.Text = "Folio fiscal";
            // 
            // uuid
            // 
            this.uuid.Location = new System.Drawing.Point(123, 75);
            this.uuid.Name = "uuid";
            this.uuid.Size = new System.Drawing.Size(637, 20);
            this.uuid.TabIndex = 5;
            // 
            // pdf
            // 
            this.pdf.Location = new System.Drawing.Point(123, 45);
            this.pdf.Name = "pdf";
            this.pdf.Size = new System.Drawing.Size(637, 20);
            this.pdf.TabIndex = 3;
            // 
            // xml
            // 
            this.xml.Location = new System.Drawing.Point(123, 11);
            this.xml.Name = "xml";
            this.xml.Size = new System.Drawing.Size(637, 20);
            this.xml.TabIndex = 1;
            // 
            // btnPDF
            // 
            this.btnPDF.BackColor = System.Drawing.Color.Olive;
            this.btnPDF.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnPDF.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnPDF.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnPDF.Location = new System.Drawing.Point(6, 40);
            this.btnPDF.Name = "btnPDF";
            this.btnPDF.Size = new System.Drawing.Size(111, 28);
            this.btnPDF.TabIndex = 2;
            this.btnPDF.Text = "Archivo PDF";
            this.btnPDF.UseVisualStyleBackColor = false;
            this.btnPDF.Click += new System.EventHandler(this.button9_Click);
            // 
            // btnXML
            // 
            this.btnXML.BackColor = System.Drawing.Color.Olive;
            this.btnXML.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnXML.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnXML.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnXML.Location = new System.Drawing.Point(6, 6);
            this.btnXML.Name = "btnXML";
            this.btnXML.Size = new System.Drawing.Size(111, 28);
            this.btnXML.TabIndex = 0;
            this.btnXML.Text = "Archivo XML";
            this.btnXML.UseVisualStyleBackColor = false;
            this.btnXML.Click += new System.EventHandler(this.button8_Click);
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.LeyendaFiscal);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(766, 104);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Leyenda fiscal";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // LeyendaFiscal
            // 
            this.LeyendaFiscal.Dock = System.Windows.Forms.DockStyle.Fill;
            this.LeyendaFiscal.Location = new System.Drawing.Point(3, 3);
            this.LeyendaFiscal.Multiline = true;
            this.LeyendaFiscal.Name = "LeyendaFiscal";
            this.LeyendaFiscal.Size = new System.Drawing.Size(760, 98);
            this.LeyendaFiscal.TabIndex = 18;
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.observacionesPDF);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(766, 104);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Observaciones en PDF";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // observacionesPDF
            // 
            this.observacionesPDF.Dock = System.Windows.Forms.DockStyle.Fill;
            this.observacionesPDF.Location = new System.Drawing.Point(3, 3);
            this.observacionesPDF.Multiline = true;
            this.observacionesPDF.Name = "observacionesPDF";
            this.observacionesPDF.Size = new System.Drawing.Size(760, 98);
            this.observacionesPDF.TabIndex = 19;
            // 
            // fecha
            // 
            this.fecha.BackColor = System.Drawing.Color.White;
            this.fecha.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.fecha.ForeColor = System.Drawing.Color.Black;
            this.fecha.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.fecha.Location = new System.Drawing.Point(446, 45);
            this.fecha.Name = "fecha";
            this.fecha.Size = new System.Drawing.Size(93, 22);
            this.fecha.TabIndex = 11;
            this.fecha.ValueChanged += new System.EventHandler(this.fecha_ValueChanged);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(403, 50);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(37, 13);
            this.label3.TabIndex = 10;
            this.label3.Text = "Fecha";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(407, 12);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(81, 29);
            this.label4.TabIndex = 4;
            this.label4.Text = "Modo:";
            // 
            // button6
            // 
            this.button6.BackColor = System.Drawing.Color.Olive;
            this.button6.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button6.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button6.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.button6.Location = new System.Drawing.Point(104, 242);
            this.button6.Name = "button6";
            this.button6.Size = new System.Drawing.Size(111, 28);
            this.button6.TabIndex = 22;
            this.button6.Text = "Generar Plantilla";
            this.button6.UseVisualStyleBackColor = false;
            this.button6.Click += new System.EventHandler(this.button6_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(545, 49);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(46, 13);
            this.label1.TabIndex = 15;
            this.label1.Text = "Moneda";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(660, 49);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(21, 13);
            this.label6.TabIndex = 17;
            this.label6.Text = "TC";
            // 
            // moneda
            // 
            this.moneda.FormattingEnabled = true;
            this.moneda.Items.AddRange(new object[] {
            "USD",
            "MXN"});
            this.moneda.Location = new System.Drawing.Point(598, 45);
            this.moneda.Name = "moneda";
            this.moneda.Size = new System.Drawing.Size(56, 21);
            this.moneda.TabIndex = 16;
            // 
            // tipoCambio
            // 
            this.tipoCambio.Location = new System.Drawing.Point(687, 45);
            this.tipoCambio.Name = "tipoCambio";
            this.tipoCambio.Size = new System.Drawing.Size(99, 20);
            this.tipoCambio.TabIndex = 18;
            // 
            // informacion
            // 
            this.informacion.BackColor = System.Drawing.Color.DarkGray;
            this.informacion.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.informacion.ForeColor = System.Drawing.Color.Blue;
            this.informacion.Location = new System.Drawing.Point(12, 566);
            this.informacion.Name = "informacion";
            this.informacion.Size = new System.Drawing.Size(774, 28);
            this.informacion.TabIndex = 25;
            // 
            // estado
            // 
            this.estado.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.estado.FormattingEnabled = true;
            this.estado.Items.AddRange(new object[] {
            "BORRADOR",
            "REAL"});
            this.estado.Location = new System.Drawing.Point(483, 12);
            this.estado.Name = "estado";
            this.estado.Size = new System.Drawing.Size(171, 28);
            this.estado.TabIndex = 16;
            // 
            // receptor
            // 
            this.receptor.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.receptor.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.receptor.FormattingEnabled = true;
            this.receptor.Location = new System.Drawing.Point(99, 77);
            this.receptor.Name = "receptor";
            this.receptor.Size = new System.Drawing.Size(101, 21);
            this.receptor.TabIndex = 26;
            this.receptor.SelectedValueChanged += new System.EventHandler(this.receptor_SelectedValueChanged);
            // 
            // nombre
            // 
            this.nombre.Location = new System.Drawing.Point(206, 78);
            this.nombre.Name = "nombre";
            this.nombre.Size = new System.Drawing.Size(333, 20);
            this.nombre.TabIndex = 14;
            // 
            // button2
            // 
            this.button2.BackColor = System.Drawing.Color.Green;
            this.button2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button2.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button2.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.button2.Location = new System.Drawing.Point(221, 242);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(176, 28);
            this.button2.TabIndex = 28;
            this.button2.Text = "Transacciones de Inventario";
            this.button2.UseVisualStyleBackColor = false;
            this.button2.Click += new System.EventHandler(this.button2_Click_1);
            // 
            // button3
            // 
            this.button3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(219)))), ((int)(((byte)(94)))), ((int)(((byte)(94)))));
            this.button3.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button3.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button3.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.button3.Location = new System.Drawing.Point(692, 242);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(94, 28);
            this.button3.TabIndex = 29;
            this.button3.Text = "Eliminar L�nea";
            this.button3.UseVisualStyleBackColor = false;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // origen
            // 
            this.origen.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.origen.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.origen.FormattingEnabled = true;
            this.origen.Location = new System.Drawing.Point(65, 130);
            this.origen.Name = "origen";
            this.origen.Size = new System.Drawing.Size(276, 21);
            this.origen.TabIndex = 26;
            this.origen.SelectedValueChanged += new System.EventHandler(this.receptor_SelectedValueChanged);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(9, 134);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(38, 13);
            this.label8.TabIndex = 27;
            this.label8.Text = "Origen";
            // 
            // destino
            // 
            this.destino.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.destino.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.destino.FormattingEnabled = true;
            this.destino.Location = new System.Drawing.Point(403, 130);
            this.destino.Name = "destino";
            this.destino.Size = new System.Drawing.Size(383, 21);
            this.destino.TabIndex = 26;
            this.destino.SelectedIndexChanged += new System.EventHandler(this.destino_SelectedIndexChanged);
            this.destino.SelectedValueChanged += new System.EventHandler(this.receptor_SelectedValueChanged);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(347, 134);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(43, 13);
            this.label9.TabIndex = 27;
            this.label9.Text = "Destino";
            // 
            // button4
            // 
            this.button4.BackColor = System.Drawing.Color.Green;
            this.button4.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button4.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button4.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.button4.Location = new System.Drawing.Point(403, 242);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(70, 28);
            this.button4.TabIndex = 30;
            this.button4.Text = "Factura";
            this.button4.UseVisualStyleBackColor = false;
            this.button4.Click += new System.EventHandler(this.button4_Click);
            // 
            // button7
            // 
            this.button7.BackColor = System.Drawing.Color.Green;
            this.button7.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button7.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button7.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.button7.Location = new System.Drawing.Point(479, 242);
            this.button7.Name = "button7";
            this.button7.Size = new System.Drawing.Size(112, 28);
            this.button7.TabIndex = 30;
            this.button7.Text = "Lista de Empaque";
            this.button7.UseVisualStyleBackColor = false;
            this.button7.Click += new System.EventHandler(this.button7_Click);
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(545, 80);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(68, 13);
            this.label10.TabIndex = 17;
            this.label10.Text = "Uso de CFDI";
            // 
            // UsoCFDI
            // 
            this.UsoCFDI.FormattingEnabled = true;
            this.UsoCFDI.Items.AddRange(new object[] {
            "USD",
            "MXN"});
            this.UsoCFDI.Location = new System.Drawing.Point(619, 77);
            this.UsoCFDI.Name = "UsoCFDI";
            this.UsoCFDI.Size = new System.Drawing.Size(167, 21);
            this.UsoCFDI.TabIndex = 31;
            // 
            // OrigenDireccion
            // 
            this.OrigenDireccion.Location = new System.Drawing.Point(12, 170);
            this.OrigenDireccion.Multiline = true;
            this.OrigenDireccion.Name = "OrigenDireccion";
            this.OrigenDireccion.Size = new System.Drawing.Size(329, 66);
            this.OrigenDireccion.TabIndex = 32;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(9, 154);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(104, 13);
            this.label12.TabIndex = 27;
            this.label12.Text = "Direcci�n  de Origen";
            // 
            // DestinoDireccion
            // 
            this.DestinoDireccion.Location = new System.Drawing.Point(347, 170);
            this.DestinoDireccion.Multiline = true;
            this.DestinoDireccion.Name = "DestinoDireccion";
            this.DestinoDireccion.Size = new System.Drawing.Size(439, 66);
            this.DestinoDireccion.TabIndex = 32;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(347, 154);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(91, 13);
            this.label13.TabIndex = 27;
            this.label13.Text = "Direcci�n Destino";
            // 
            // DomicilioFiscalReceptor
            // 
            this.DomicilioFiscalReceptor.Location = new System.Drawing.Point(150, 104);
            this.DomicilioFiscalReceptor.Name = "DomicilioFiscalReceptor";
            this.DomicilioFiscalReceptor.Size = new System.Drawing.Size(132, 20);
            this.DomicilioFiscalReceptor.TabIndex = 14;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(9, 107);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(135, 13);
            this.label11.TabIndex = 27;
            this.label11.Text = "Domicilio fiscal del receptor";
            // 
            // RegimenFiscalReceptor
            // 
            this.RegimenFiscalReceptor.Location = new System.Drawing.Point(426, 104);
            this.RegimenFiscalReceptor.Name = "RegimenFiscalReceptor";
            this.RegimenFiscalReceptor.Size = new System.Drawing.Size(113, 20);
            this.RegimenFiscalReceptor.TabIndex = 14;
            this.RegimenFiscalReceptor.Text = "601";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(285, 107);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(135, 13);
            this.label14.TabIndex = 27;
            this.label14.Text = "Regimen fiscal del receptor";
            // 
            // button8
            // 
            this.button8.BackColor = System.Drawing.Color.Green;
            this.button8.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button8.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button8.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.button8.Location = new System.Drawing.Point(7, 73);
            this.button8.Name = "button8";
            this.button8.Size = new System.Drawing.Size(86, 28);
            this.button8.TabIndex = 21;
            this.button8.Text = "Receptor";
            this.button8.UseVisualStyleBackColor = false;
            this.button8.Click += new System.EventHandler(this.button8_Click_1);
            // 
            // frmTraslado
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(798, 603);
            this.Controls.Add(this.DestinoDireccion);
            this.Controls.Add(this.OrigenDireccion);
            this.Controls.Add(this.UsoCFDI);
            this.Controls.Add(this.button7);
            this.Controls.Add(this.button4);
            this.Controls.Add(this.button3);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.label13);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label14);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.destino);
            this.Controls.Add(this.origen);
            this.Controls.Add(this.receptor);
            this.Controls.Add(this.informacion);
            this.Controls.Add(this.tipoCambio);
            this.Controls.Add(this.estado);
            this.Controls.Add(this.moneda);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.fecha);
            this.Controls.Add(this.tabControl1);
            this.Controls.Add(this.dtg);
            this.Controls.Add(this.RegimenFiscalReceptor);
            this.Controls.Add(this.DomicilioFiscalReceptor);
            this.Controls.Add(this.nombre);
            this.Controls.Add(this.emisor);
            this.Controls.Add(this.button5);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.serie);
            this.Controls.Add(this.btnDeleteItem);
            this.Controls.Add(this.btnNuevo);
            this.Controls.Add(this.btnImprimir);
            this.Controls.Add(this.button6);
            this.Controls.Add(this.button8);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.btnAddItem);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MetroColor = System.Drawing.Color.Transparent;
            this.Name = "frmTraslado";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Traslado";
            ((System.ComponentModel.ISupportInitialize)(this.dtg)).EndInit();
            this.tabControl1.ResumeLayout(false);
            this.tabPage3.ResumeLayout(false);
            this.tabPage3.PerformLayout();
            this.tabPage1.ResumeLayout(false);
            this.tabPage1.PerformLayout();
            this.tabPage2.ResumeLayout(false);
            this.tabPage2.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnDeleteItem;
        private System.Windows.Forms.Button btnNuevo;
        private System.Windows.Forms.Button btnAddItem;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button btnImprimir;
        private System.Windows.Forms.ComboBox emisor;
        private System.Windows.Forms.Button button5;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox serie;
        private System.Windows.Forms.DataGridView dtg;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TextBox LeyendaFiscal;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.TextBox observacionesPDF;
        private System.Windows.Forms.DateTimePicker fecha;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button button6;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.ComboBox moneda;
        private System.Windows.Forms.TextBox tipoCambio;
        private System.Windows.Forms.TabPage tabPage3;
        private System.Windows.Forms.TextBox pdf;
        private System.Windows.Forms.TextBox xml;
        private System.Windows.Forms.Button btnPDF;
        private System.Windows.Forms.Button btnXML;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox uuid;
        private System.Windows.Forms.Label informacion;
        private System.Windows.Forms.ComboBox estado;
        private System.Windows.Forms.ComboBox receptor;
        private System.Windows.Forms.TextBox nombre;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.ComboBox origen;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.ComboBox destino;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.Button button7;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.ComboBox UsoCFDI;
        private System.Windows.Forms.TextBox OrigenDireccion;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.TextBox DestinoDireccion;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.TextBox DomicilioFiscalReceptor;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TextBox RegimenFiscalReceptor;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.DataGridViewTextBoxColumn Cantidad;
        private System.Windows.Forms.DataGridViewTextBoxColumn ClaveProdServ;
        private System.Windows.Forms.DataGridViewTextBoxColumn NoIdentificacion;
        private System.Windows.Forms.DataGridViewTextBoxColumn ClaveUnidad;
        private System.Windows.Forms.DataGridViewTextBoxColumn Unidad;
        private System.Windows.Forms.DataGridViewTextBoxColumn Descripcion;
        private System.Windows.Forms.DataGridViewTextBoxColumn ValorUnitario;
        private System.Windows.Forms.DataGridViewTextBoxColumn Importe;
        private System.Windows.Forms.DataGridViewTextBoxColumn NumeroPedimento;
        private System.Windows.Forms.DataGridViewTextBoxColumn Especie;
        private System.Windows.Forms.Button button8;
    }
}