﻿namespace FE_FX
{
    partial class frmRetenciones
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmRetenciones));
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.informacion = new System.Windows.Forms.ToolStripStatusLabel();
            this.dtgrdGeneral = new System.Windows.Forms.DataGridView();
            this.archivo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.folio = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.fecha = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.rfcEmisor = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Ejerc = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.MesIni = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.MesFin = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.UUID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.NumCert = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.buttonX2 = new DevComponents.DotNetBar.ButtonX();
            this.FACTURA = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.buttonX1 = new DevComponents.DotNetBar.ButtonX();
            this.comboItem4 = new DevComponents.Editors.ComboItem();
            this.comboItem5 = new DevComponents.Editors.ComboItem();
            this.comboItem6 = new DevComponents.Editors.ComboItem();
            this.labelX5 = new DevComponents.DotNetBar.LabelX();
            this.label3 = new DevComponents.DotNetBar.LabelX();
            this.label1 = new DevComponents.DotNetBar.LabelX();
            this.INICIO = new System.Windows.Forms.DateTimePicker();
            this.label2 = new DevComponents.DotNetBar.LabelX();
            this.FINAL = new System.Windows.Forms.DateTimePicker();
            this.CLIENTE = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.ID = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.buttonX3 = new DevComponents.DotNetBar.ButtonX();
            this.ENTITY_ID = new DevComponents.DotNetBar.Controls.ComboBoxEx();
            this.comboItem17 = new DevComponents.Editors.ComboItem();
            this.comboItem18 = new DevComponents.Editors.ComboItem();
            this.comboItem19 = new DevComponents.Editors.ComboItem();
            this.labelX11 = new DevComponents.DotNetBar.LabelX();
            this.directorio = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.EMPRESA_NOMBRE = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.buttonX5 = new DevComponents.DotNetBar.ButtonX();
            this.folderBrowserDialog1 = new System.Windows.Forms.FolderBrowserDialog();
            this.contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.cancelarToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.imprimirToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.buttonX4 = new DevComponents.DotNetBar.ButtonX();
            this.statusStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dtgrdGeneral)).BeginInit();
            this.contextMenuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // statusStrip1
            // 
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.informacion});
            this.statusStrip1.Location = new System.Drawing.Point(0, 403);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(841, 22);
            this.statusStrip1.TabIndex = 37;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // informacion
            // 
            this.informacion.Name = "informacion";
            this.informacion.Size = new System.Drawing.Size(42, 17);
            this.informacion.Text = "Estado";
            // 
            // dtgrdGeneral
            // 
            this.dtgrdGeneral.AllowUserToAddRows = false;
            this.dtgrdGeneral.AllowUserToDeleteRows = false;
            this.dtgrdGeneral.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dtgrdGeneral.BackgroundColor = System.Drawing.SystemColors.ControlLight;
            this.dtgrdGeneral.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.dtgrdGeneral.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dtgrdGeneral.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.archivo,
            this.folio,
            this.fecha,
            this.rfcEmisor,
            this.Ejerc,
            this.MesIni,
            this.MesFin,
            this.UUID,
            this.NumCert});
            this.dtgrdGeneral.Location = new System.Drawing.Point(0, 123);
            this.dtgrdGeneral.Name = "dtgrdGeneral";
            this.dtgrdGeneral.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dtgrdGeneral.Size = new System.Drawing.Size(841, 277);
            this.dtgrdGeneral.TabIndex = 14;
            this.dtgrdGeneral.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dtgrdGeneral_CellDoubleClick);
            this.dtgrdGeneral.MouseClick += new System.Windows.Forms.MouseEventHandler(this.dtgrdGeneral_MouseClick);
            // 
            // archivo
            // 
            this.archivo.HeaderText = "Archivo";
            this.archivo.Name = "archivo";
            this.archivo.ReadOnly = true;
            this.archivo.Visible = false;
            // 
            // folio
            // 
            this.folio.HeaderText = "Folio";
            this.folio.Name = "folio";
            this.folio.ReadOnly = true;
            // 
            // fecha
            // 
            this.fecha.HeaderText = "Fecha";
            this.fecha.Name = "fecha";
            this.fecha.ReadOnly = true;
            // 
            // rfcEmisor
            // 
            this.rfcEmisor.HeaderText = "RFC Emisor";
            this.rfcEmisor.Name = "rfcEmisor";
            this.rfcEmisor.ReadOnly = true;
            // 
            // Ejerc
            // 
            this.Ejerc.HeaderText = "Ejercicio";
            this.Ejerc.Name = "Ejerc";
            this.Ejerc.ReadOnly = true;
            // 
            // MesIni
            // 
            this.MesIni.HeaderText = "Inicio";
            this.MesIni.Name = "MesIni";
            this.MesIni.ReadOnly = true;
            // 
            // MesFin
            // 
            this.MesFin.HeaderText = "Fin";
            this.MesFin.Name = "MesFin";
            this.MesFin.ReadOnly = true;
            // 
            // UUID
            // 
            this.UUID.HeaderText = "Folio Fiscal";
            this.UUID.Name = "UUID";
            this.UUID.ReadOnly = true;
            // 
            // NumCert
            // 
            this.NumCert.HeaderText = "Certificado";
            this.NumCert.Name = "NumCert";
            this.NumCert.ReadOnly = true;
            // 
            // buttonX2
            // 
            this.buttonX2.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.buttonX2.BackColor = System.Drawing.Color.YellowGreen;
            this.buttonX2.ColorTable = DevComponents.DotNetBar.eButtonColor.Flat;
            this.buttonX2.Image = global::FE_FX.Properties.Resources.ic_create_new_folder_black_24dp_1x;
            this.buttonX2.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top;
            this.buttonX2.Location = new System.Drawing.Point(12, 12);
            this.buttonX2.Name = "buttonX2";
            this.buttonX2.Size = new System.Drawing.Size(42, 42);
            this.buttonX2.Style = DevComponents.DotNetBar.eDotNetBarStyle.Windows7;
            this.buttonX2.TabIndex = 0;
            this.buttonX2.Text = "Nuevo";
            this.buttonX2.TextColor = System.Drawing.Color.White;
            this.buttonX2.Click += new System.EventHandler(this.buttonX2_Click);
            // 
            // FACTURA
            // 
            this.FACTURA.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            // 
            // 
            // 
            this.FACTURA.Border.Class = "TextBoxBorder";
            this.FACTURA.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.FACTURA.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.FACTURA.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.249999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FACTURA.ForeColor = System.Drawing.Color.Black;
            this.FACTURA.Location = new System.Drawing.Point(302, 38);
            this.FACTURA.Name = "FACTURA";
            this.FACTURA.Size = new System.Drawing.Size(258, 20);
            this.FACTURA.TabIndex = 7;
            // 
            // buttonX1
            // 
            this.buttonX1.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.buttonX1.BackColor = System.Drawing.Color.Goldenrod;
            this.buttonX1.ColorTable = DevComponents.DotNetBar.eButtonColor.Flat;
            this.buttonX1.Image = global::FE_FX.Properties.Resources.ic_search_black_18dp_1x;
            this.buttonX1.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top;
            this.buttonX1.Location = new System.Drawing.Point(12, 64);
            this.buttonX1.Name = "buttonX1";
            this.buttonX1.Size = new System.Drawing.Size(42, 42);
            this.buttonX1.Style = DevComponents.DotNetBar.eDotNetBarStyle.Windows7;
            this.buttonX1.TabIndex = 1;
            this.buttonX1.Text = "Buscar";
            this.buttonX1.TextColor = System.Drawing.Color.White;
            this.buttonX1.Click += new System.EventHandler(this.buttonX1_Click);
            // 
            // comboItem4
            // 
            this.comboItem4.Text = "FECHA";
            // 
            // comboItem5
            // 
            this.comboItem5.Text = "FACTURA";
            // 
            // comboItem6
            // 
            this.comboItem6.Text = "CLIENTE";
            // 
            // labelX5
            // 
            this.labelX5.AutoSize = true;
            this.labelX5.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX5.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX5.ForeColor = System.Drawing.Color.Black;
            this.labelX5.Location = new System.Drawing.Point(396, 91);
            this.labelX5.Name = "labelX5";
            this.labelX5.Size = new System.Drawing.Size(47, 15);
            this.labelX5.TabIndex = 12;
            this.labelX5.Text = "Receptor";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.label3.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.label3.ForeColor = System.Drawing.Color.Black;
            this.label3.Location = new System.Drawing.Point(227, 40);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(69, 15);
            this.label3.TabIndex = 6;
            this.label3.Text = "Comprobante";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.label1.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.label1.ForeColor = System.Drawing.Color.Black;
            this.label1.Location = new System.Drawing.Point(113, 89);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(28, 15);
            this.label1.TabIndex = 8;
            this.label1.Text = "Inicio";
            // 
            // INICIO
            // 
            this.INICIO.BackColor = System.Drawing.Color.White;
            this.INICIO.ForeColor = System.Drawing.Color.Black;
            this.INICIO.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.INICIO.Location = new System.Drawing.Point(147, 88);
            this.INICIO.Name = "INICIO";
            this.INICIO.Size = new System.Drawing.Size(105, 20);
            this.INICIO.TabIndex = 9;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.label2.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.label2.ForeColor = System.Drawing.Color.Black;
            this.label2.Location = new System.Drawing.Point(258, 91);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(18, 15);
            this.label2.TabIndex = 10;
            this.label2.Text = "Fin";
            // 
            // FINAL
            // 
            this.FINAL.BackColor = System.Drawing.Color.White;
            this.FINAL.ForeColor = System.Drawing.Color.Black;
            this.FINAL.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.FINAL.Location = new System.Drawing.Point(282, 88);
            this.FINAL.Name = "FINAL";
            this.FINAL.Size = new System.Drawing.Size(108, 20);
            this.FINAL.TabIndex = 11;
            // 
            // CLIENTE
            // 
            this.CLIENTE.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            // 
            // 
            // 
            this.CLIENTE.Border.Class = "TextBoxBorder";
            this.CLIENTE.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.CLIENTE.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.249999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CLIENTE.ForeColor = System.Drawing.Color.Black;
            this.CLIENTE.Location = new System.Drawing.Point(449, 88);
            this.CLIENTE.Name = "CLIENTE";
            this.CLIENTE.Size = new System.Drawing.Size(206, 20);
            this.CLIENTE.TabIndex = 13;
            // 
            // ID
            // 
            this.ID.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            // 
            // 
            // 
            this.ID.Border.Class = "TextBoxBorder";
            this.ID.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.ID.Enabled = false;
            this.ID.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ID.ForeColor = System.Drawing.Color.Black;
            this.ID.Location = new System.Drawing.Point(147, 37);
            this.ID.Margin = new System.Windows.Forms.Padding(0);
            this.ID.Name = "ID";
            this.ID.Size = new System.Drawing.Size(77, 22);
            this.ID.TabIndex = 5;
            this.ID.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // buttonX3
            // 
            this.buttonX3.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.buttonX3.BackColor = System.Drawing.Color.CadetBlue;
            this.buttonX3.ColorTable = DevComponents.DotNetBar.eButtonColor.Flat;
            this.buttonX3.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonX3.Image = global::FE_FX.Properties.Resources.ic_search_black_18dp_1x;
            this.buttonX3.Location = new System.Drawing.Point(67, 38);
            this.buttonX3.Name = "buttonX3";
            this.buttonX3.Size = new System.Drawing.Size(77, 20);
            this.buttonX3.Style = DevComponents.DotNetBar.eDotNetBarStyle.Windows7;
            this.buttonX3.TabIndex = 4;
            this.buttonX3.Text = "Serie";
            this.buttonX3.TextAlignment = DevComponents.DotNetBar.eButtonTextAlignment.Right;
            this.buttonX3.TextColor = System.Drawing.Color.White;
            this.buttonX3.Click += new System.EventHandler(this.buttonX3_Click);
            // 
            // ENTITY_ID
            // 
            this.ENTITY_ID.DisplayMember = "Text";
            this.ENTITY_ID.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.ENTITY_ID.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ENTITY_ID.ForeColor = System.Drawing.Color.Black;
            this.ENTITY_ID.FormattingEnabled = true;
            this.ENTITY_ID.ItemHeight = 16;
            this.ENTITY_ID.Items.AddRange(new object[] {
            this.comboItem17,
            this.comboItem18,
            this.comboItem19});
            this.ENTITY_ID.Location = new System.Drawing.Point(147, 12);
            this.ENTITY_ID.Name = "ENTITY_ID";
            this.ENTITY_ID.Size = new System.Drawing.Size(149, 22);
            this.ENTITY_ID.TabIndex = 3;
            this.ENTITY_ID.SelectedIndexChanged += new System.EventHandler(this.ENTITY_ID_SelectedIndexChanged);
            // 
            // comboItem17
            // 
            this.comboItem17.Text = "BORRADOR";
            // 
            // comboItem18
            // 
            this.comboItem18.FontStyle = System.Drawing.FontStyle.Bold;
            this.comboItem18.Text = "TIMBRADO";
            // 
            // comboItem19
            // 
            this.comboItem19.FontStyle = System.Drawing.FontStyle.Bold;
            this.comboItem19.ForeColor = System.Drawing.Color.Red;
            this.comboItem19.Text = "CANCELADO";
            // 
            // labelX11
            // 
            this.labelX11.AutoSize = true;
            this.labelX11.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX11.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX11.ForeColor = System.Drawing.Color.Black;
            this.labelX11.Location = new System.Drawing.Point(67, 15);
            this.labelX11.Name = "labelX11";
            this.labelX11.Size = new System.Drawing.Size(37, 15);
            this.labelX11.TabIndex = 2;
            this.labelX11.Text = "Emisor";
            // 
            // directorio
            // 
            this.directorio.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            // 
            // 
            // 
            this.directorio.Border.Class = "TextBoxBorder";
            this.directorio.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.directorio.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.directorio.ForeColor = System.Drawing.Color.Black;
            this.directorio.Location = new System.Drawing.Point(147, 63);
            this.directorio.Margin = new System.Windows.Forms.Padding(0);
            this.directorio.Name = "directorio";
            this.directorio.Size = new System.Drawing.Size(508, 22);
            this.directorio.TabIndex = 38;
            // 
            // EMPRESA_NOMBRE
            // 
            this.EMPRESA_NOMBRE.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            // 
            // 
            // 
            this.EMPRESA_NOMBRE.Border.Class = "TextBoxBorder";
            this.EMPRESA_NOMBRE.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.EMPRESA_NOMBRE.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.EMPRESA_NOMBRE.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.249999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.EMPRESA_NOMBRE.ForeColor = System.Drawing.Color.Black;
            this.EMPRESA_NOMBRE.Location = new System.Drawing.Point(302, 12);
            this.EMPRESA_NOMBRE.Name = "EMPRESA_NOMBRE";
            this.EMPRESA_NOMBRE.Size = new System.Drawing.Size(353, 20);
            this.EMPRESA_NOMBRE.TabIndex = 39;
            // 
            // buttonX5
            // 
            this.buttonX5.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.buttonX5.BackColor = System.Drawing.Color.Teal;
            this.buttonX5.ColorTable = DevComponents.DotNetBar.eButtonColor.Flat;
            this.buttonX5.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonX5.Image = global::FE_FX.Properties.Resources.ic_search_black_18dp_1x;
            this.buttonX5.Location = new System.Drawing.Point(67, 63);
            this.buttonX5.Name = "buttonX5";
            this.buttonX5.Size = new System.Drawing.Size(77, 20);
            this.buttonX5.Style = DevComponents.DotNetBar.eDotNetBarStyle.Windows7;
            this.buttonX5.TabIndex = 40;
            this.buttonX5.Text = "Directorio";
            this.buttonX5.TextAlignment = DevComponents.DotNetBar.eButtonTextAlignment.Right;
            this.buttonX5.TextColor = System.Drawing.Color.White;
            this.buttonX5.Click += new System.EventHandler(this.buttonX5_Click);
            // 
            // contextMenuStrip1
            // 
            this.contextMenuStrip1.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.contextMenuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.cancelarToolStripMenuItem,
            this.imprimirToolStripMenuItem});
            this.contextMenuStrip1.Name = "contextMenuStrip1";
            this.contextMenuStrip1.Size = new System.Drawing.Size(121, 48);
            // 
            // cancelarToolStripMenuItem
            // 
            this.cancelarToolStripMenuItem.Name = "cancelarToolStripMenuItem";
            this.cancelarToolStripMenuItem.Size = new System.Drawing.Size(120, 22);
            this.cancelarToolStripMenuItem.Text = "Cancelar";
            this.cancelarToolStripMenuItem.Click += new System.EventHandler(this.cancelarToolStripMenuItem_Click);
            // 
            // imprimirToolStripMenuItem
            // 
            this.imprimirToolStripMenuItem.Name = "imprimirToolStripMenuItem";
            this.imprimirToolStripMenuItem.Size = new System.Drawing.Size(120, 22);
            this.imprimirToolStripMenuItem.Text = "Imprimir";
            // 
            // buttonX4
            // 
            this.buttonX4.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.buttonX4.BackColor = System.Drawing.Color.SkyBlue;
            this.buttonX4.ColorTable = DevComponents.DotNetBar.eButtonColor.Flat;
            this.buttonX4.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonX4.Image = global::FE_FX.Properties.Resources.ic_label_outline_black_24dp_1x;
            this.buttonX4.Location = new System.Drawing.Point(566, 38);
            this.buttonX4.Name = "buttonX4";
            this.buttonX4.Size = new System.Drawing.Size(89, 20);
            this.buttonX4.Style = DevComponents.DotNetBar.eDotNetBarStyle.Windows7;
            this.buttonX4.TabIndex = 42;
            this.buttonX4.Text = "Imprimir";
            this.buttonX4.TextAlignment = DevComponents.DotNetBar.eButtonTextAlignment.Right;
            this.buttonX4.TextColor = System.Drawing.Color.Black;
            this.buttonX4.Click += new System.EventHandler(this.buttonX4_Click);
            // 
            // frmRetenciones
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(841, 425);
            this.Controls.Add(this.buttonX4);
            this.Controls.Add(this.buttonX5);
            this.Controls.Add(this.EMPRESA_NOMBRE);
            this.Controls.Add(this.directorio);
            this.Controls.Add(this.ENTITY_ID);
            this.Controls.Add(this.labelX11);
            this.Controls.Add(this.ID);
            this.Controls.Add(this.buttonX3);
            this.Controls.Add(this.buttonX2);
            this.Controls.Add(this.FACTURA);
            this.Controls.Add(this.buttonX1);
            this.Controls.Add(this.labelX5);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.INICIO);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.FINAL);
            this.Controls.Add(this.CLIENTE);
            this.Controls.Add(this.statusStrip1);
            this.Controls.Add(this.dtgrdGeneral);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.KeyPreview = true;
            this.MaximizeBox = false;
            this.Name = "frmRetenciones";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Retenciones";
            this.Load += new System.EventHandler(this.frmUsuarios_Load);
            this.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.frmFormatos_KeyPress);
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dtgrdGeneral)).EndInit();
            this.contextMenuStrip1.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStripStatusLabel informacion;
        private System.Windows.Forms.DataGridView dtgrdGeneral;
        private DevComponents.DotNetBar.ButtonX buttonX2;
        private DevComponents.DotNetBar.Controls.TextBoxX FACTURA;
        private DevComponents.DotNetBar.ButtonX buttonX1;
        private DevComponents.Editors.ComboItem comboItem4;
        private DevComponents.Editors.ComboItem comboItem5;
        private DevComponents.Editors.ComboItem comboItem6;
        private DevComponents.DotNetBar.LabelX labelX5;
        private DevComponents.DotNetBar.LabelX label3;
        private DevComponents.DotNetBar.LabelX label1;
        private System.Windows.Forms.DateTimePicker INICIO;
        private DevComponents.DotNetBar.LabelX label2;
        private System.Windows.Forms.DateTimePicker FINAL;
        private DevComponents.DotNetBar.Controls.TextBoxX CLIENTE;
        private DevComponents.DotNetBar.Controls.TextBoxX ID;
        private DevComponents.DotNetBar.ButtonX buttonX3;
        private DevComponents.DotNetBar.Controls.ComboBoxEx ENTITY_ID;
        private DevComponents.Editors.ComboItem comboItem17;
        private DevComponents.Editors.ComboItem comboItem18;
        private DevComponents.Editors.ComboItem comboItem19;
        private DevComponents.DotNetBar.LabelX labelX11;
        private DevComponents.DotNetBar.Controls.TextBoxX directorio;
        private DevComponents.DotNetBar.Controls.TextBoxX EMPRESA_NOMBRE;
        private System.Windows.Forms.DataGridViewTextBoxColumn feha;
        private DevComponents.DotNetBar.ButtonX buttonX5;
        private System.Windows.Forms.FolderBrowserDialog folderBrowserDialog1;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip1;
        private System.Windows.Forms.ToolStripMenuItem cancelarToolStripMenuItem;
        private System.Windows.Forms.DataGridViewTextBoxColumn archivo;
        private System.Windows.Forms.DataGridViewTextBoxColumn folio;
        private System.Windows.Forms.DataGridViewTextBoxColumn fecha;
        private System.Windows.Forms.DataGridViewTextBoxColumn rfcEmisor;
        private System.Windows.Forms.DataGridViewTextBoxColumn Ejerc;
        private System.Windows.Forms.DataGridViewTextBoxColumn MesIni;
        private System.Windows.Forms.DataGridViewTextBoxColumn MesFin;
        private System.Windows.Forms.DataGridViewTextBoxColumn UUID;
        private System.Windows.Forms.DataGridViewTextBoxColumn NumCert;
        private System.Windows.Forms.ToolStripMenuItem imprimirToolStripMenuItem;
        private DevComponents.DotNetBar.ButtonX buttonX4;
    }
}