#region Copyright Syncfusion Inc. 2001 - 2017
// Copyright Syncfusion Inc. 2001 - 2017. All rights reserved.
// Use of this code is subject to the terms of our license.
// A copy of the current license can be obtained at any time by e-mailing
// licensing@syncfusion.com. Any infringement will be prosecuted under
// applicable laws. 
#endregion
namespace FE_FX.CFDI
{
    partial class frmTraslados
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmTraslados));
            this.dtgrdGeneral = new System.Windows.Forms.DataGridView();
            this.archivo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.folio = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.fecha = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.rfcReceptor = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.nombre = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.UUID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.button2 = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.fin = new System.Windows.Forms.DateTimePicker();
            this.inicio = new System.Windows.Forms.DateTimePicker();
            this.label3 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.folioFiltro = new System.Windows.Forms.TextBox();
            this.directorio = new System.Windows.Forms.TextBox();
            this.button3 = new System.Windows.Forms.Button();
            this.emisor = new System.Windows.Forms.ComboBox();
            this.button5 = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.serie = new System.Windows.Forms.TextBox();
            this.contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.cancelarToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.verXMLToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.verPDFToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.button4 = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.dtgrdGeneral)).BeginInit();
            this.contextMenuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // dtgrdGeneral
            // 
            this.dtgrdGeneral.AllowUserToAddRows = false;
            this.dtgrdGeneral.AllowUserToDeleteRows = false;
            this.dtgrdGeneral.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dtgrdGeneral.BackgroundColor = System.Drawing.SystemColors.ControlLight;
            this.dtgrdGeneral.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.dtgrdGeneral.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dtgrdGeneral.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.archivo,
            this.folio,
            this.fecha,
            this.rfcReceptor,
            this.nombre,
            this.UUID});
            this.dtgrdGeneral.Location = new System.Drawing.Point(12, 96);
            this.dtgrdGeneral.Name = "dtgrdGeneral";
            this.dtgrdGeneral.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dtgrdGeneral.Size = new System.Drawing.Size(783, 319);
            this.dtgrdGeneral.TabIndex = 15;
            this.dtgrdGeneral.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dtgrdGeneral_CellDoubleClick);
            this.dtgrdGeneral.CellMouseClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.dtgrdGeneral_CellMouseClick);
            this.dtgrdGeneral.MouseClick += new System.Windows.Forms.MouseEventHandler(this.dtgrdGeneral_MouseClick);
            // 
            // archivo
            // 
            this.archivo.HeaderText = "Archivo";
            this.archivo.Name = "archivo";
            this.archivo.ReadOnly = true;
            this.archivo.Visible = false;
            // 
            // folio
            // 
            this.folio.HeaderText = "Folio";
            this.folio.Name = "folio";
            this.folio.ReadOnly = true;
            // 
            // fecha
            // 
            this.fecha.HeaderText = "Fecha";
            this.fecha.Name = "fecha";
            this.fecha.ReadOnly = true;
            // 
            // rfcReceptor
            // 
            this.rfcReceptor.HeaderText = "RFC Receptor";
            this.rfcReceptor.Name = "rfcReceptor";
            this.rfcReceptor.ReadOnly = true;
            // 
            // nombre
            // 
            this.nombre.HeaderText = "Nombre";
            this.nombre.Name = "nombre";
            this.nombre.ReadOnly = true;
            this.nombre.Width = 160;
            // 
            // UUID
            // 
            this.UUID.HeaderText = "Folio Fiscal";
            this.UUID.Name = "UUID";
            this.UUID.ReadOnly = true;
            // 
            // button2
            // 
            this.button2.BackColor = System.Drawing.Color.YellowGreen;
            this.button2.FlatAppearance.BorderSize = 0;
            this.button2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button2.ForeColor = System.Drawing.Color.White;
            this.button2.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.button2.Location = new System.Drawing.Point(11, 39);
            this.button2.Margin = new System.Windows.Forms.Padding(2);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(82, 24);
            this.button2.TabIndex = 44;
            this.button2.Text = "Agregar";
            this.button2.UseVisualStyleBackColor = false;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // button1
            // 
            this.button1.BackColor = System.Drawing.Color.Goldenrod;
            this.button1.FlatAppearance.BorderSize = 0;
            this.button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button1.ForeColor = System.Drawing.Color.White;
            this.button1.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.button1.Location = new System.Drawing.Point(11, 11);
            this.button1.Margin = new System.Windows.Forms.Padding(2);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(82, 24);
            this.button1.TabIndex = 43;
            this.button1.Text = "Buscar";
            this.button1.UseVisualStyleBackColor = false;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(99, 17);
            this.label2.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(32, 13);
            this.label2.TabIndex = 39;
            this.label2.Text = "Inicio";
            // 
            // fin
            // 
            this.fin.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.fin.Location = new System.Drawing.Point(281, 13);
            this.fin.Margin = new System.Windows.Forms.Padding(2);
            this.fin.Name = "fin";
            this.fin.ShowCheckBox = true;
            this.fin.Size = new System.Drawing.Size(113, 20);
            this.fin.TabIndex = 41;
            // 
            // inicio
            // 
            this.inicio.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.inicio.Location = new System.Drawing.Point(134, 13);
            this.inicio.Margin = new System.Windows.Forms.Padding(2);
            this.inicio.Name = "inicio";
            this.inicio.ShowCheckBox = true;
            this.inicio.Size = new System.Drawing.Size(118, 20);
            this.inicio.TabIndex = 42;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(256, 17);
            this.label3.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(21, 13);
            this.label3.TabIndex = 40;
            this.label3.Text = "Fin";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(208, 45);
            this.label1.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(51, 13);
            this.label1.TabIndex = 45;
            this.label1.Text = "Receptor";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(97, 45);
            this.label5.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(29, 13);
            this.label5.TabIndex = 46;
            this.label5.Text = "Folio";
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(259, 42);
            this.textBox1.Margin = new System.Windows.Forms.Padding(2);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(135, 20);
            this.textBox1.TabIndex = 47;
            // 
            // folioFiltro
            // 
            this.folioFiltro.Location = new System.Drawing.Point(134, 42);
            this.folioFiltro.Margin = new System.Windows.Forms.Padding(2);
            this.folioFiltro.Name = "folioFiltro";
            this.folioFiltro.Size = new System.Drawing.Size(70, 20);
            this.folioFiltro.TabIndex = 48;
            // 
            // directorio
            // 
            this.directorio.Location = new System.Drawing.Point(186, 69);
            this.directorio.Margin = new System.Windows.Forms.Padding(2);
            this.directorio.Name = "directorio";
            this.directorio.Size = new System.Drawing.Size(609, 20);
            this.directorio.TabIndex = 47;
            // 
            // button3
            // 
            this.button3.BackColor = System.Drawing.Color.DarkKhaki;
            this.button3.FlatAppearance.BorderSize = 0;
            this.button3.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button3.ForeColor = System.Drawing.Color.White;
            this.button3.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.button3.Location = new System.Drawing.Point(100, 67);
            this.button3.Margin = new System.Windows.Forms.Padding(2);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(82, 24);
            this.button3.TabIndex = 49;
            this.button3.Text = "Directorio";
            this.button3.UseVisualStyleBackColor = false;
            // 
            // emisor
            // 
            this.emisor.FormattingEnabled = true;
            this.emisor.Location = new System.Drawing.Point(453, 13);
            this.emisor.Name = "emisor";
            this.emisor.Size = new System.Drawing.Size(135, 21);
            this.emisor.TabIndex = 51;
            this.emisor.SelectedIndexChanged += new System.EventHandler(this.emisor_SelectedIndexChanged);
            // 
            // button5
            // 
            this.button5.BackColor = System.Drawing.Color.DarkKhaki;
            this.button5.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button5.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button5.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.button5.Location = new System.Drawing.Point(594, 9);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(76, 28);
            this.button5.TabIndex = 52;
            this.button5.Text = "Serie";
            this.button5.UseVisualStyleBackColor = false;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(403, 16);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(38, 13);
            this.label4.TabIndex = 50;
            this.label4.Text = "Emisor";
            // 
            // serie
            // 
            this.serie.Location = new System.Drawing.Point(676, 13);
            this.serie.Name = "serie";
            this.serie.Size = new System.Drawing.Size(119, 20);
            this.serie.TabIndex = 53;
            // 
            // contextMenuStrip1
            // 
            this.contextMenuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.cancelarToolStripMenuItem,
            this.verXMLToolStripMenuItem,
            this.verPDFToolStripMenuItem});
            this.contextMenuStrip1.Name = "contextMenuStrip1";
            this.contextMenuStrip1.Size = new System.Drawing.Size(121, 70);
            // 
            // cancelarToolStripMenuItem
            // 
            this.cancelarToolStripMenuItem.Name = "cancelarToolStripMenuItem";
            this.cancelarToolStripMenuItem.Size = new System.Drawing.Size(120, 22);
            this.cancelarToolStripMenuItem.Text = "Cancelar";
            this.cancelarToolStripMenuItem.Click += new System.EventHandler(this.cancelarToolStripMenuItem_Click);
            // 
            // verXMLToolStripMenuItem
            // 
            this.verXMLToolStripMenuItem.Name = "verXMLToolStripMenuItem";
            this.verXMLToolStripMenuItem.Size = new System.Drawing.Size(120, 22);
            this.verXMLToolStripMenuItem.Text = "Ver XML";
            this.verXMLToolStripMenuItem.Click += new System.EventHandler(this.verXMLToolStripMenuItem_Click);
            // 
            // verPDFToolStripMenuItem
            // 
            this.verPDFToolStripMenuItem.Name = "verPDFToolStripMenuItem";
            this.verPDFToolStripMenuItem.Size = new System.Drawing.Size(120, 22);
            this.verPDFToolStripMenuItem.Text = "Ver PDF";
            this.verPDFToolStripMenuItem.Click += new System.EventHandler(this.verPDFToolStripMenuItem_Click);
            // 
            // button4
            // 
            this.button4.BackColor = System.Drawing.Color.YellowGreen;
            this.button4.FlatAppearance.BorderSize = 0;
            this.button4.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button4.ForeColor = System.Drawing.Color.White;
            this.button4.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.button4.Location = new System.Drawing.Point(11, 67);
            this.button4.Margin = new System.Windows.Forms.Padding(2);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(82, 24);
            this.button4.TabIndex = 44;
            this.button4.Text = "Imprimir";
            this.button4.UseVisualStyleBackColor = false;
            this.button4.Click += new System.EventHandler(this.button4_Click);
            // 
            // frmTraslados
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(807, 427);
            this.Controls.Add(this.emisor);
            this.Controls.Add(this.button5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.serie);
            this.Controls.Add(this.button3);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.directorio);
            this.Controls.Add(this.textBox1);
            this.Controls.Add(this.folioFiltro);
            this.Controls.Add(this.button4);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.fin);
            this.Controls.Add(this.inicio);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.dtgrdGeneral);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MetroColor = System.Drawing.Color.Transparent;
            this.Name = "frmTraslados";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Traslados";
            ((System.ComponentModel.ISupportInitialize)(this.dtgrdGeneral)).EndInit();
            this.contextMenuStrip1.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView dtgrdGeneral;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.DateTimePicker fin;
        private System.Windows.Forms.DateTimePicker inicio;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.TextBox folioFiltro;
        private System.Windows.Forms.TextBox directorio;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.ComboBox emisor;
        private System.Windows.Forms.Button button5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox serie;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip1;
        private System.Windows.Forms.ToolStripMenuItem cancelarToolStripMenuItem;
        private System.Windows.Forms.DataGridViewTextBoxColumn archivo;
        private System.Windows.Forms.DataGridViewTextBoxColumn folio;
        private System.Windows.Forms.DataGridViewTextBoxColumn fecha;
        private System.Windows.Forms.DataGridViewTextBoxColumn rfcReceptor;
        private System.Windows.Forms.DataGridViewTextBoxColumn nombre;
        private System.Windows.Forms.DataGridViewTextBoxColumn UUID;
        private System.Windows.Forms.ToolStripMenuItem verXMLToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem verPDFToolStripMenuItem;
        private System.Windows.Forms.Button button4;
    }
}