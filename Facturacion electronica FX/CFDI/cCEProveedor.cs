﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace FE_FX.CFDI
{
    [Serializable()]
    public class cCEProveedor
    {
        private string numRegIdTrib;
        public string NumRegIdTrib
        {
            get { return numRegIdTrib; }
            set { numRegIdTrib = value; }
        }

        private string pais;
        public string Pais
        {
            get { return pais; }
            set { pais = value; }
        }

        private string calle;
        public string Calle
        {
            get { return calle; }
            set { calle = value; }
        }

        private string noExterior;
        public string NoExterior
        {
            get { return noExterior; }
            set { noExterior = value; }
        }

        private string noInterior;
        public string NoInterior
        {
            get { return noInterior; }
            set { noInterior = value; }
        }

        private string estado;
        public string Estado
        {
            get { return estado; }
            set { estado = value; }
        }


        private string codigoPostal;
        public string CodigoPostal
        {
            get { return codigoPostal; }
            set { codigoPostal = value; }
        }

    }
}
