﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;

using System.Text;
using System.Windows.Forms;
using System.IO;
using DevComponents.DotNetBar;
using Generales;

namespace FE_FX
{
    public partial class frmCFDIConceptoCCE : Form
    {
        private bool modificado;
        private string ROW_ID_EMPRESA = "";
        public decimal cantidadP =0;
        public string unidadP = "";
        public string noIdentificacionP = "";
        public string descripcionP = "";
        public decimal valorUnitarioP = 0;
        public decimal importeP = 0;
        public string fraccionArancelariaP = "";

        public string ClaveProdServP = "";
        public string ClaveUnidadP = "";
        public string SubmodeloP = "";
        public string MarcaP = "";
        public string ModeloP = "";
        public string NumeroSerieP = "";

        public string aduanaP = "";
        public string fechaP = "";
        public string numeroP = "";
        public string CantidadAduanaP = "";
        public string ValorUnitarioAduanaP = "";
        public string ValorDolaresP = "";

        private cCONEXCION oData_ERP = new cCONEXCION("");
        private cCONEXCION oData = new cCONEXCION("");
        public frmCFDIConceptoCCE(string sConn, string ROW_ID_EMPRESAp, string oData_sConn)
        {
            ROW_ID_EMPRESA = ROW_ID_EMPRESAp;
            oData_ERP = new cCONEXCION(sConn);
            oData = new cCONEXCION(oData_sConn);
            InitializeComponent();   
        }

        public frmCFDIConceptoCCE(decimal p, string p_2, string p_3, string p_4
            , decimal p_5, decimal p_6, string fraccionArancelariap, string sConn
            , string ROW_ID_EMPRESAp, string oData_sConn, string aduana, string fecha, string numero
            , string ClaveProdServ, string ClaveUnidad, string Submodelo, string Marca, string Modelo, string NumeroSerie
            ,string ValorUnitarioAduana ,string ValorDolares, string CantidadAduana)
        {

    
            ROW_ID_EMPRESA = ROW_ID_EMPRESAp;
            oData = new cCONEXCION(oData_sConn);
            oData_ERP = new cCONEXCION(sConn);
            InitializeComponent();

            CANTIDAD.Text= p.ToString();
            UNIDAD.Text = p_2;
            NO_IDENTIFICACION.Text= p_3;
            DESCRIPCION.Text = p_4;
            VALOR_UNITARIO.Text = p_5.ToString();
            IMPORTE.Text= p_6.ToString();
            fraccionArancelaria.Text = fraccionArancelariap;

            this.ClaveProdServ.Text = ClaveProdServ;
            this.ClaveUnidad.Text = ClaveUnidad;
            this.Submodelo.Text = Submodelo;
            this.Marca.Text = Marca;
            this.Modelo.Text = Modelo;
            this.NumeroSerie.Text = NumeroSerie;
            this.CantidadAduana.Text = CantidadAduana;
            this.ValorUnitarioAduana.Text = ValorUnitarioAduana;
            this.ValorDolares.Text = ValorDolares;


            this.aduana.Text = aduana;
            this.fecha.Text = fecha;
            this.numero.Text = numero;
        }



        private void button3_Click(object sender, EventArgs e)
        {
            if (UNIDAD.Text.Trim()== "")
            {
                MessageBoxEx.Show("La Unidad es requerida.", Application.ProductName + "-" + Application.ProductVersion, MessageBoxButtons.OK);
                return;
            }

            if (DESCRIPCION.Text.Trim() == "")
            {
                MessageBoxEx.Show("La Descripción es requerida.", Application.ProductName + "-" + Application.ProductVersion, MessageBoxButtons.OK);
                return;
            }

            cantidadP = decimal.Parse(oData.IsNullNumero(CANTIDAD.Text));
            CANTIDAD.Text = cantidadP.ToString();
            valorUnitarioP = decimal.Parse(oData.IsNullNumero(VALOR_UNITARIO.Text));
            VALOR_UNITARIO.Text = valorUnitarioP.ToString();
            importeP = decimal.Parse(oData.IsNullNumero(IMPORTE.Text));
            IMPORTE.Text = importeP.ToString();

            descripcionP = DESCRIPCION.Text;
            noIdentificacionP = NO_IDENTIFICACION.Text;
            unidadP = UNIDAD.Text;
            fraccionArancelariaP = fraccionArancelaria.Text;

            aduanaP = aduana.Text;
            fechaP = fecha.Text;
            numeroP = numero.Text;

            ClaveProdServP=this.ClaveProdServ.Text;
            ClaveUnidadP = this.ClaveUnidad.Text;
            SubmodeloP = this.Submodelo.Text;
            MarcaP = this.Marca.Text;
            ModeloP = this.Modelo.Text;
            NumeroSerieP = this.NumeroSerie.Text;
            CantidadAduanaP = this.CantidadAduana.Text;
            ValorUnitarioAduanaP =this.ValorUnitarioAduana.Text;
            ValorDolaresP=this.ValorDolares.Text;

            DialogResult = DialogResult.OK;
        }

        private void CANTIDAD_Leave(object sender, EventArgs e)
        {
            calcular();
        }

        public void calcular()
        {
            decimal VALOR_UNITARIO_tr = 1;
            decimal IMPORTE_tr = 1;
            decimal CANTIDAD_tr = 1;
            if (CANTIDAD.Text.Trim() == "")
            {
                CANTIDAD.Text = "1";
            }
            if (IMPORTE.Text.Trim() == "")
            {
                IMPORTE.Text = "0";
            }

            if (VALOR_UNITARIO.Text.Trim() != "")
            {
                IMPORTE.Enabled = false;
                VALOR_UNITARIO_tr = decimal.Parse(VALOR_UNITARIO.Text);
                CANTIDAD_tr = decimal.Parse(CANTIDAD.Text);
                IMPORTE_tr = CANTIDAD_tr * VALOR_UNITARIO_tr;
            }
            else
            {
                IMPORTE.Enabled = true;
                IMPORTE_tr = decimal.Parse(IMPORTE.Text);
            }
            //IMPORTE.Text = cUTILERIA.Trunca_y_formatea(IMPORTE_tr);
            IMPORTE.Text = IMPORTE_tr.ToString();
        }

        private void Solo_Numero_KeyPress(object sender, KeyPressEventArgs e)
        {
            modificado = true;
            if (!char.IsDigit(e.KeyChar) && !char.IsControl(e.KeyChar) && !char.IsPunctuation(e.KeyChar)) e.Handled = true;
        }

        private void buttonX5_Click(object sender, EventArgs e)
        {
            cargarPart();
        }

        private void cargarPart()
        {
            frmPART ofrmPART = new frmPART(oData_ERP.sConn);
            if (ofrmPART.ShowDialog() == DialogResult.OK)
            {
                NO_IDENTIFICACION.Text = (string)ofrmPART.selecionados[0].Cells[0].Value;
                cargarFraccion(NO_IDENTIFICACION.Text);
            }
        }

        private void cargarFraccion(string PARTp)
        {
            cCCE ocCCE = new cCCE();
            if (ocCCE.cargar(ROW_ID_EMPRESA))
            {
                string fraccion = ocCCE.obtenerFraccion(PARTp,"","", oData_ERP,"");
                fraccionArancelaria.Text = fraccion;
            }
        }

        private void VALOR_UNITARIO_TextChanged(object sender, EventArgs e)
        {

        }

        private void UNIDAD_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(UNIDAD.Text))
            {
                ClaveUnidad.Text = UNIDAD.Text;
            }
        }
    }
}
