﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;

using System.Text;
using System.Windows.Forms;
using System.IO;
using DevComponents.DotNetBar;
using Generales;
using FE_FX.Clases;

namespace FE_FX
{
    public partial class frmCancelacion : Form
    {
        public string Motivo { get; set; }
        public string Sustitucion { get; set; }
        public bool CancelarPrueba { get; set; }
        public frmCancelacion()
        {
            InitializeComponent();
            CargarMotivoCancelacion();
        }
        public frmCancelacion(string invoiceId, string customerId, Encapsular oEncapsular)
        {
            InitializeComponent();
            CargarMotivoCancelacion();
            CargarFacturas(invoiceId, customerId, oEncapsular);
        }
        public frmCancelacion(string uuid, Encapsular oEncapsular)
        {
            InitializeComponent();
            CargarMotivoCancelacion();
            
        }
        private void CargarFacturas(string invoiceId,string customerId, Encapsular oEncapsular)
        {
            Dtg.Rows.Clear();
            string Sql = @"SELECT vm.INVOICE_ID, vm.UUID,r.INVOICE_DATE, r.TOTAL_AMOUNT, r.CURRENCY_ID
            FROM " + oEncapsular.ocEMPRESA.BD_AUXILIAR+ @"..VMX_FE vm
            INNER JOIN RECEIVABLE r ON r.INVOICE_ID COLLATE SQL_Latin1_General_CP1_CI_AS =vm.INVOICE_ID 
            WHERE r.INVOICE_ID>'" + invoiceId + "' AND r.CUSTOMER_ID COLLATE SQL_Latin1_General_CP1_CI_AS ='" + customerId + "'";
            DataTable oDataTable = oEncapsular.oData_ERP.EjecutarConsulta(Sql);
            if (oDataTable != null)
            {
                foreach (DataRow oDataRow in oDataTable.Rows)
                {
                    int n = Dtg.Rows.Add();
                    Dtg.Rows[n].Cells["UUIDtr"].Value = oDataRow["UUID"].ToString();
                    Dtg.Rows[n].Cells["Factura"].Value = oDataRow["INVOICE_ID"].ToString();
                    Dtg.Rows[n].Cells["Fecha"].Value = oDataRow["INVOICE_DATE"].ToString();
                    Dtg.Rows[n].Cells["Monto"].Value = oDataRow["TOTAL_AMOUNT"].ToString();
                    Dtg.Rows[n].Cells["Moneda"].Value = oDataRow["CURRENCY_ID"].ToString();
                }
            }
        }

        private void CargarMotivoCancelacion()
        {
            MotivoCancelacion.Items.Clear();
            MotivoCancelacion.Items.Add(new ComboItem { Name = "Comprobantes emitidos con errores con relación", Value = "01" });
            MotivoCancelacion.Items.Add(new ComboItem { Name = "Comprobantes emitidos con errores sin relación.", Value = "02" });
            MotivoCancelacion.Items.Add(new ComboItem { Name = "Operación nominativa relacionada en una factura global", Value = "04" });
            ComboItem oComboItemS01 = new ComboItem { Name = "No se llevó a cabo la operación", Value = "03" };
            MotivoCancelacion.Items.Add(oComboItemS01);
            MotivoCancelacion.SelectedItem = oComboItemS01;
        }

        private void button7_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Cancel;
        }

        private void button3_Click(object sender, EventArgs e)
        {
            Guardar();
        }

        private void Guardar()
        {
            try
            {
                this.Motivo = ((ComboItem)MotivoCancelacion.SelectedItem).Value.ToString();                
            }
            catch (Exception error)
            {
                ErrorFX.mostrar(error, true, true, "Cancelación - 1228 - Seleccione un motivo correcto ", true);
            }
            if (this.Motivo.Equals("01"))
            {
                if (String.IsNullOrEmpty(SustitucionTxt.Text.Trim()))
                {
                    ErrorFX.mostrar("Cancelación - 1228 - Escriba el folio fiscal ", true, true, true);
                    return;
                }
            }

            this.Sustitucion = SustitucionTxt.Text;
            this.CancelarPrueba = CancelarPruebaChk.Checked;
            DialogResult = DialogResult.OK;
        }

        private void Dtg_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            CopiarValor();
        }

        private void CopiarValor()
        {
            foreach (DataGridViewRow Selecion in Dtg.SelectedRows)
            {
                SustitucionTxt.Text = Selecion.Cells["UUIDtr"].Value.ToString();
                return;
            }
        }
    }
}
