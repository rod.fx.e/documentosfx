#region Copyright Syncfusion Inc. 2001 - 2017
// Copyright Syncfusion Inc. 2001 - 2017. All rights reserved.
// Use of this code is subject to the terms of our license.
// A copy of the current license can be obtained at any time by e-mailing
// licensing@syncfusion.com. Any infringement will be prosecuted under
// applicable laws. 
#endregion
using CFDI33;
using com.google.zxing.qrcode;
using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;
using DevComponents.DotNetBar;
using FE_FX.Clases;
using FE_FX.EDICOM_Servicio;
using Generales;
using Ionic.Zip;
using ModeloDocumentosFX;
using OfficeOpenXml;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Drawing;
using System.Globalization;
using System.IO;
using System.Security.Cryptography.X509Certificates;
using System.ServiceModel;
using System.Text;
using System.Text.RegularExpressions;
using System.Windows.Forms;
using System.Xml;
using System.Xml.Serialization;
using System.Xml.XPath;
using System.Xml.Xsl;

namespace FE_FX.CFDI
{

    //TODO: Crear menu y acceso solo a traslado,
    //TODO: Crear seguridad para usuarios solos de traslado
    //TODO: Cancelar traslado
    //TODO: Imprimir traslado, solo interpretar lo que tiene XML Timbrado
    //TODO: Cargar traslado de la pantalla principal

    public partial class frmTraslado : Syncfusion.Windows.Forms.MetroForm
    {
        bool modificado = false;
        cSERIE ocSERIE = new cSERIE();
        cEMPRESA ocEMPRESA = new cEMPRESA();
        cCERTIFICADO ocCERTIFICADO = new cCERTIFICADO();

        //public frmTraslado(ComboItemReceptor[] receptorp)
        //{


        //    InitializeComponent();
        //    limpiar();

        //    receptor.Items.Clear();
        //    foreach (ComboItemReceptor x in receptorp)
        //    {
        //        receptor.Items.Add(new ComboItemReceptor { Name = x.Name, Value = x.Value });

        //    }


        //}
        cCONEXCION oData_ERP = new cCONEXCION("");
        cCONEXCION oData = new cCONEXCION("");
        bool TrabajarIndependiente = false;
        public frmTraslado(cCONEXCION oData_ERP,ComboItemReceptor[] receptorp, string archivo)
        {
            ModeloDocumentosFXContainer Db = new ModeloDocumentosFXContainer();
            string Conexion = Db.Database.Connection.ConnectionString;
            oData = new cCONEXCION(Conexion);

            this.oData_ERP = oData_ERP;
            try
            {
                TrabajarIndependiente = bool.Parse(ConfigurationManager.AppSettings["TrabajarIndependiente"].ToString());                
            }
            catch
            {

            }
            InitializeComponent();
            limpiar();

            receptor.Items.Clear();
            receptor.Items.Add(new ComboItemReceptor { Name = "RFC Generico", Value = "XEXX010101000" });
            if (receptorp != null)
            {
                foreach (ComboItemReceptor x in receptorp)
                {
                    receptor.Items.Add(new ComboItemReceptor { Name = x.Name, Value = x.Value });

                }
            }
            receptor.SelectedIndex = 0;
            if (archivo!=null)
            {
                cargar(archivo);
            }
            
            cargarAlmacen();
            CargarCombo();
            DomicilioFiscalReceptor.Text = ocSERIE.CP;
        }

        private void CargarCombo()
        {
            UsoCFDI.Items.Clear();
            UsoCFDI.Items.Add(new ComboItem { Name = "Adquisici�n de mercancias", Value = "G01" });
            UsoCFDI.Items.Add(new ComboItem { Name = "Devoluciones, descuentos o bonificaciones", Value = "G02" });
            //UsoCFDI.Items.Add(new ComboItem { Name = "Gastos en general", Value = "G03" });
            UsoCFDI.Items.Add(new ComboItem { Name = "Construcciones", Value = "I01" });
            UsoCFDI.Items.Add(new ComboItem { Name = "Mobilario y equipo de oficina por inversiones", Value = "I02" });
            UsoCFDI.Items.Add(new ComboItem { Name = "Equipo de transporte", Value = "I03" });
            UsoCFDI.Items.Add(new ComboItem { Name = "Equipo de computo y accesorios", Value = "I04" });
            UsoCFDI.Items.Add(new ComboItem { Name = "Dados, troqueles, moldes, matrices y herramental", Value = "I05" });
            UsoCFDI.Items.Add(new ComboItem { Name = "Comunicaciones telef�nicas", Value = "I06" });
            UsoCFDI.Items.Add(new ComboItem { Name = "Comunicaciones satelitales", Value = "I07" });
            UsoCFDI.Items.Add(new ComboItem { Name = "Otra maquinaria y equipo", Value = "I08" });
            UsoCFDI.Items.Add(new ComboItem { Name = "Honorarios m�dicos, dentales y gastos hospitalarios.", Value = "D01" });
            UsoCFDI.Items.Add(new ComboItem { Name = "Gastos m�dicos por incapacidad o discapacidad", Value = "D02" });
            UsoCFDI.Items.Add(new ComboItem { Name = "Gastos funerales.", Value = "D03" });
            UsoCFDI.Items.Add(new ComboItem { Name = "Donativos.", Value = "D04" });
            UsoCFDI.Items.Add(new ComboItem { Name = "Intereses reales efectivamente pagados por cr�ditos hipotecarios (casa habitaci�n).", Value = "D05" });
            UsoCFDI.Items.Add(new ComboItem { Name = "Aportaciones voluntarias al SAR.", Value = "D06" });
            UsoCFDI.Items.Add(new ComboItem { Name = "Primas por seguros de gastos m�dicos.", Value = "D07" });
            UsoCFDI.Items.Add(new ComboItem { Name = "Gastos de transportaci�n escolar obligatoria.", Value = "D08" });
            UsoCFDI.Items.Add(new ComboItem { Name = "Dep�sitos en cuentas para el ahorro, primas que tengan como base planes de pensiones.", Value = "D09" });
            UsoCFDI.Items.Add(new ComboItem { Name = "Pagos por servicios educativos (colegiaturas)", Value = "D10" });
            //ComboItem oComboItem = new ComboItem { Name = "Sin efectos fiscales.", Value = "S01" };
            ComboItem oComboItem = new ComboItem { Name = "Gastos en general", Value = "G03" };
            UsoCFDI.Items.Add(oComboItem);
            ComboItem oComboItemS01 = new ComboItem { Name = "Sin efectos fiscales", Value = "S01" };
            UsoCFDI.Items.Add(oComboItemS01);
            UsoCFDI.SelectedItem = oComboItemS01;
        }

        private void cargarAlmacen()
        {
            try
            {
                this.origen.Items.Clear();
                this.destino.Items.Clear();
                if (TrabajarIndependiente)
                {
                    return;
                }
                string sSQL = @"select ID,
                ISNULL(ADDR_1 + ' ', ' ') +
                ISNULL(ADDR_2 + ' ', ' ') +
                ISNULL(CITY + ' ', ' ') +
                ISNULL(STATE + ', ', ' ') +
                ISNULL(COUNTRY + ' ', ' ') +
                ISNULL(' C.P. ' + ZIPCODE, ' ') as Direccion
                , ISNULL(ADDR_3, '') as Permiso
                from warehouse";
                DataTable oDataTable = oData_ERP.EjecutarConsulta(sSQL);
                if (oDataTable != null)
                {
                    foreach (DataRow oDataRow in oDataTable.Rows)
                    {
                        ComboItemReceptor oComboItemReceptor = new ComboItemReceptor
                        {
                            Name = oDataRow["ID"].ToString()
                            ,
                            Value = oDataRow["Direccion"].ToString()
                             ,
                            OtroValor = oDataRow["Permiso"].ToString()
                        };

                        origen.Items.Add(oComboItemReceptor);
                        origen.SelectedIndex = 0;
                        destino.Items.Add(oComboItemReceptor);
                        destino.SelectedIndex = 0;
                    }
                }            
            }
            catch
            {

            }
        }

        private void buttonAdv1_Click(object sender, EventArgs e)
        {
            limpiar();
        }

        private void btnAddItem_Click(object sender, EventArgs e)
        {
            Guardar40();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            imprimir();
        }

        private void btnDeleteItem_Click(object sender, EventArgs e)
        {
            cancelar();
        }

        #region metodos
        private void cargar(string archivo)
        {
            try
            {
                CFDI33.Comprobante oComprobante = new CFDI33.Comprobante();
                XmlSerializer serializer_CFD_3 = new XmlSerializer(typeof(CFDI33.Comprobante));
                TextReader reader = new StreamReader(archivo);
                oComprobante = (CFDI33.Comprobante)serializer_CFD_3.Deserialize(reader);
                if (oComprobante.TipoDeComprobante.Equals("T"))
                {
                    xml.Text = archivo;
                    pdf.Text = archivo.Replace("XML", "PDF");

                    receptor.Text = oComprobante.Receptor.Rfc;
                    nombre.Text = oComprobante.Receptor.Nombre;

                }

            }
            catch (Exception e)
            {
                ErrorFX.mostrar(e, true, true, "frmTraslado - 95 ");
            }
            return;
        }
        private void limpiar()
        {
            if (modificado)
            {
                DialogResult Resultado = MessageBox.Show(this, "�Desea guardar los cambios?", Application.ProductName + "-" + Application.ProductVersion, MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                if (Resultado.ToString() == "Yes")
                {
                    guardar();
                }
            }
            modificado = false;
            foreach (var c in this.Controls)
            {
                if (c is TextBox)
                {
                    ((TextBox)c).Text = "";
                }
            }
            xml.Text = "";
            pdf.Text = "";
            dtg.Rows.Clear();
            estado.SelectedIndex = 0;
            moneda.SelectedIndex = 0;
            cargarEmpresas();
        }
        private void cargarEmpresas()
        {
            emisor.Items.Clear();
            cEMPRESA ocEMPRESA = new cEMPRESA();
            foreach (cEMPRESA registro in ocEMPRESA.todos("", true))
            {
                emisor.Items.Add(registro);
            }
            if (emisor.Items.Count > 0)
            {
                emisor.SelectedIndex = 0;
            }
        }
        private void Guardar40()
        {
            try
            {
                if (!validar())
                {
                    return;
                }


                CFDI40.Comprobante oComprobante = new CFDI40.Comprobante();

                //Crear cabecera
                //LugarExpedicion = "31074" TipoDeComprobante = "T" Total = "0" TipoCambio = "19.4232" Moneda = "USD" Certificado = "MIIGMzCCBBugAwIBAgIUMDAwMDEwMDAwMDA0MDI2OTAxOTIwDQYJKoZIhvcNAQELBQAwggGyMTgwNgYDVQQDDC9BLkMuIGRlbCBTZXJ2aWNpbyBkZSBBZG1pbmlzdHJhY2nDs24gVHJpYnV0YXJpYTEvMC0GA1UECgwmU2VydmljaW8gZGUgQWRtaW5pc3RyYWNpw7NuIFRyaWJ1dGFyaWExODA2BgNVBAsML0FkbWluaXN0cmFjacOzbiBkZSBTZWd1cmlkYWQgZGUgbGEgSW5mb3JtYWNpw7NuMR8wHQYJKoZIhvcNAQkBFhBhY29kc0BzYXQuZ29iLm14MSYwJAYDVQQJDB1Bdi4gSGlkYWxnbyA3NywgQ29sLiBHdWVycmVybzEOMAwGA1UEEQwFMDYzMDAxCzAJBgNVBAYTAk1YMRkwFwYDVQQIDBBEaXN0cml0byBGZWRlcmFsMRQwEgYDVQQHDAtDdWF1aHTDqW1vYzEVMBMGA1UELRMMU0FUOTcwNzAxTk4zMV0wWwYJKoZIhvcNAQkCDE5SZXNwb25zYWJsZTogQWRtaW5pc3RyYWNpw7NuIENlbnRyYWwgZGUgU2VydmljaW9zIFRyaWJ1dGFyaW9zIGFsIENvbnRyaWJ1eWVudGUwHhcNMTYwNjAyMTU1ODAxWhcNMjAwNjAyMTU1ODAxWjCB0zEoMCYGA1UEAxMfVElHSElUQ08gTEFUSU5PQU1FUklDQSBTQSBERSBDVjEoMCYGA1UEKRMfVElHSElUQ08gTEFUSU5PQU1FUklDQSBTQSBERSBDVjEoMCYGA1UEChMfVElHSElUQ08gTEFUSU5PQU1FUklDQSBTQSBERSBDVjElMCMGA1UELRMcVExBMDEwMjI3QzUwIC8gU0FNSDcyMTIwNkVDMzEeMBwGA1UEBRMVIC8gU0FNSDcyMTIwNkhUU05STTA2MQwwCgYDVQQLEwNDVVUwggEiMA0GCSqGSIb3DQEBAQUAA4IBDwAwggEKAoIBAQCKowTKrWnXDNQY1HVywpAzVC1rVjyPXdbz69qfkcyLNC8xGvKQ8F3d8Gu5oR2yocBZIc4VQpdQbTVQm12WludTd5t+q7HO0+hb9Teaw1+e8hynLMLgyUaSm9NMAj36DD70TAIHsoyqQnEgsWhUPI9/ZdJwc7IIUsGWfhDjrEV4eIKz7ErXqPUasAWoddaqejxe7D1JJ8p/19roas3Bsn3l9q9C7NSQVxMwl919aTfTAHJzH5WZuQK3hP+aI7UC659oNSqSSRKlpKly1YYDdmb1NuYr69IPOOhFlF46TrKRtYRRmAVE1OGNwPjF+R2wwk7YVG14OQwIlsLZukgzr2o3AgMBAAGjHTAbMAwGA1UdEwEB/wQCMAAwCwYDVR0PBAQDAgbAMA0GCSqGSIb3DQEBCwUAA4ICAQAlgEOKY0Mc1bksNmxDZeoiugtO24KO9pnUBosvtIvHNKIjF/rVOjY3OYPNOyArtGQIhzZSnaCYokK8PsIRTLon7AWu824zD/K46T45H/2CwfhA14rE1z1fyzcbdeSK/KPBoi12TCSXNrDDuQgjd81YkyKn51UtX/8rtL0qw1TzpzpFkkAq+pzqJbd/eOcgIKNt1NVVXWkVfXDGdIQTi9Hj94RQJMj7Jc0u3ZahgQiorl467mqCLnP+v7munAVT0W9om2QkVbwfv2PAoSWJ3aVFd2/aG1E+1fft/pMpBc8KZBJbw8gTcZqnrX4bXZTDObeDGiyKugPlOyQCrywQZJPuJw40OxZ+GGq7iyq8nU5CxB07X7YEG+Rd6YLEOIfRVajj0mKEUcDFe6MZwgDCN4M81tsfeW+ZN5zExlT/4V77qfU2wjy22QQvzewS7bUmcLZiaeDuwDD5Qq04sx4nEzCmbYwZfA7jHZ5jregF+B/IsGec9Y8t82tVa8VT5ymGOXXegSRN3wFdzUC5Zjeikq5t4/o8tOlIbaKll2M0UOXz9PM6Lk7N43UyAaRofcjlYDlPHNHVTzuvVgJadvyhCCQRJ0JkN3F3dTy7sa1/HOpL0iZgUlkc6pWs4uvtrPhvXJaAU8nOSwMS9u2VVJ6XpLOOAKNsiELMcXDZ1qTdQkGcfg==" SubTotal = "0" NoCertificado = "00001000000402690192" Sello = "MXf7SaQrANFAuhsBA7RUBebqL2EV2SpNvgmW/zDQ+zsOUMlmTjJxqmcNdKNOjNRfzwBlBa4csuWc/dwSOeoXvJm/6HKi7J99a9Rhc44jZcABq5a0uQRtpgKEDv/B4RDWwbZBIVk5Yor3gsFQSSZDNBCe0+Xsz3y69+V3I7hc3AtlK/0x9SAVpolHZf0X4xQpzyrOHm3nmYsmXSM7+I4UeMuOD07kvBv2B7fR9GlYtLeHQBNk4bCjUhusae7yO0oRjMvvfoS3zNjtsCe8sTHN8+2WFPkflsFo/y5Vj+7qd5tiYSry42LzNsVTU+W6we0rkOx6bJ6xbJs37vEtyxDKSQ==" Fecha = "2018-05-08T10:07:29" Folio = "113" Serie = "B" Version = "3.3" xmlns: cfdi = "http://www.sat.gob.mx/cfd/3"
                oComprobante.LugarExpedicion = ocSERIE.CP;
                oComprobante.TipoDeComprobante = CFDI40.c_TipoDeComprobante.T;
                oComprobante.MetodoPago = null;
                oComprobante.SubTotal = oComprobante.Total = 0;
                oComprobante.Serie = ocSERIE.ID;
                oComprobante.Folio = ocSERIE.IMPRESOS;
                oComprobante.Descuento = 0;

                DateTime fechaTr = fecha.Value;
                IFormatProvider culture = new CultureInfo("es-MX", true);
                DateTime CREATE_DATE = DateTime.Now;

                fechaTr = DateTime.Parse(fechaTr.Day.ToString() + "/" + fechaTr.Month.ToString() + "/" + fechaTr.Year.ToString() + " " + fecha.Value.ToString("HH:mm:ss"), culture);

                oComprobante.Fecha = fechaTr;
                oComprobante.Moneda = moneda.Text;
                oComprobante.TipoCambio = decimal.Parse(tipoCambio.Text);
                oComprobante.TipoCambioSpecified = true;
                oComprobante.Exportacion = "01";

                X509Certificate cert = X509Certificate.CreateFromCertFile(ocCERTIFICADO.CERTIFICADO);
                try
                {
                    string NoSerie = cert.GetSerialNumberString();
                    string Certificado64 = System.Convert.ToBase64String(cert.GetRawCertData());
                    oComprobante.Certificado = Certificado64;
                    oComprobante.NoCertificado = timbrado33.validacion(ocCERTIFICADO.NO_CERTIFICADO);

                }
                catch (Exception e)
                {
                    ErrorFX.mostrar(e, true, true, "frmTraslado - Certificado - 110 ");
                    return;
                }

                //Emisor
                oComprobante.Emisor = new CFDI40.ComprobanteEmisor();
                oComprobante.Emisor.Rfc = ocEMPRESA.RFC;
                oComprobante.Emisor.Nombre = Generar40.LimpiarNombre(ocEMPRESA.ID).ToUpper();
               
                oComprobante.Emisor.RegimenFiscal = CFDI40.c_RegimenFiscal.Item601; // ocEMPRESA.REGIMEN_FISCAL;
                
                //Receptor
                oComprobante.Receptor = new CFDI40.ComprobanteReceptor();
                oComprobante.Receptor.Rfc = receptor.Text.ToUpper();
                oComprobante.Receptor.Nombre = nombre.Text.ToUpper();
                oComprobante.Receptor.UsoCFDI = CFDI40.c_UsoCFDI.P01;
                
                oComprobante.Receptor.UsoCFDI = CFDI40.c_UsoCFDI.G03;
                try
                {
                    
                    oComprobante.Receptor.UsoCFDI = (CFDI40.c_UsoCFDI)System.Enum.Parse(typeof(CFDI40.c_UsoCFDI)
                            , ((ComboItem)UsoCFDI.SelectedItem).Value.ToString());
                    
                }
                catch (Exception error)
                {
                    ErrorFX.mostrar(error, true, true, "Generar40 - 190 - UsoCFDI ", true);
                }
                oComprobante.Receptor.DomicilioFiscalReceptor = DomicilioFiscalReceptor.Text;
                oComprobante.Receptor.RegimenFiscalReceptor = CFDI40.c_RegimenFiscal.Item601;


                var regimenFiscal = RegimenFiscalReceptor.Text; // Este valor se reemplaza seg�n la l�gica de tu aplicaci�n
                switch (regimenFiscal)
                {
                    case "601":
                        oComprobante.Receptor.RegimenFiscalReceptor = CFDI40.c_RegimenFiscal.Item601;
                        break;
                    case "603":
                        oComprobante.Receptor.RegimenFiscalReceptor = CFDI40.c_RegimenFiscal.Item603;
                        break;
                    case "605":
                        oComprobante.Receptor.RegimenFiscalReceptor = CFDI40.c_RegimenFiscal.Item605;
                        break;
                    case "606":
                        oComprobante.Receptor.RegimenFiscalReceptor = CFDI40.c_RegimenFiscal.Item606;
                        break;
                    case "607":
                        oComprobante.Receptor.RegimenFiscalReceptor = CFDI40.c_RegimenFiscal.Item607;
                        break;
                    case "608":
                        oComprobante.Receptor.RegimenFiscalReceptor = CFDI40.c_RegimenFiscal.Item608;
                        break;
                    case "609":
                        oComprobante.Receptor.RegimenFiscalReceptor = CFDI40.c_RegimenFiscal.Item609;
                        break;
                    case "610":
                        oComprobante.Receptor.RegimenFiscalReceptor = CFDI40.c_RegimenFiscal.Item610;
                        break;
                    case "611":
                        oComprobante.Receptor.RegimenFiscalReceptor = CFDI40.c_RegimenFiscal.Item611;
                        break;
                    case "612":
                        oComprobante.Receptor.RegimenFiscalReceptor = CFDI40.c_RegimenFiscal.Item612;
                        break;
                    case "614":
                        oComprobante.Receptor.RegimenFiscalReceptor = CFDI40.c_RegimenFiscal.Item614;
                        break;
                    case "615":
                        oComprobante.Receptor.RegimenFiscalReceptor = CFDI40.c_RegimenFiscal.Item615;
                        break;
                    case "616":
                        oComprobante.Receptor.RegimenFiscalReceptor = CFDI40.c_RegimenFiscal.Item616;
                        break;
                    case "620":
                        oComprobante.Receptor.RegimenFiscalReceptor = CFDI40.c_RegimenFiscal.Item620;
                        break;
                    case "621":
                        oComprobante.Receptor.RegimenFiscalReceptor = CFDI40.c_RegimenFiscal.Item621;
                        break;
                    case "622":
                        oComprobante.Receptor.RegimenFiscalReceptor = CFDI40.c_RegimenFiscal.Item622;
                        break;
                    case "623":
                        oComprobante.Receptor.RegimenFiscalReceptor = CFDI40.c_RegimenFiscal.Item623;
                        break;
                    case "624":
                        oComprobante.Receptor.RegimenFiscalReceptor = CFDI40.c_RegimenFiscal.Item624;
                        break;
                    case "625":
                        oComprobante.Receptor.RegimenFiscalReceptor = CFDI40.c_RegimenFiscal.Item625;
                        break;
                    case "626":
                        oComprobante.Receptor.RegimenFiscalReceptor = CFDI40.c_RegimenFiscal.Item626;
                        break;
                    case "628":
                        oComprobante.Receptor.RegimenFiscalReceptor = CFDI40.c_RegimenFiscal.Item628;
                        break;
                    case "629":
                        oComprobante.Receptor.RegimenFiscalReceptor = CFDI40.c_RegimenFiscal.Item629;
                        break;
                    case "630":
                        oComprobante.Receptor.RegimenFiscalReceptor = CFDI40.c_RegimenFiscal.Item630;
                        break;
                    // A�ade m�s casos seg�n sea necesario
                    default:
                        oComprobante.Receptor.RegimenFiscalReceptor = CFDI40.c_RegimenFiscal.Item601;
                        break;
                }


                if (oComprobante.Receptor.Rfc.Equals("XEXX010101000"))
                {
                    oComprobante.Receptor.RegimenFiscalReceptor = CFDI40.c_RegimenFiscal.Item616;
                }
                

                //Agregar las lineas
                oComprobante.Conceptos = new CFDI40.ComprobanteConcepto[dtg.Rows.Count];
                int i = 0;
                foreach (DataGridViewRow r in dtg.Rows)
                {
                    oComprobante.Conceptos[i] = new CFDI40.ComprobanteConcepto();
                    oComprobante.Conceptos[i].Cantidad = decimal.Parse(r.Cells["Cantidad"].Value.ToString());
                    oComprobante.Conceptos[i].ClaveProdServ = r.Cells["ClaveProdServ"].Value.ToString();
                    if (r.Cells["Unidad"].Value != null)
                    {
                        if (!String.IsNullOrEmpty(r.Cells["Unidad"].Value.ToString()))
                        {
                            oComprobante.Conceptos[i].Unidad = r.Cells["Unidad"].Value.ToString();
                        }
                    }



                    oComprobante.Conceptos[i].Descripcion = r.Cells["Descripcion"].Value.ToString();
                    oComprobante.Conceptos[i].ClaveUnidad = r.Cells["ClaveUnidad"].Value.ToString();
                    if (r.Cells["NoIdentificacion"].Value != null)
                    {
                        if (!String.IsNullOrEmpty(r.Cells["NoIdentificacion"].Value.ToString()))
                        {
                            oComprobante.Conceptos[i].NoIdentificacion = r.Cells["NoIdentificacion"].Value.ToString();
                        }
                    }
                    oComprobante.Conceptos[i].ObjetoImp = "01"; 
                    oComprobante.Conceptos[i].ValorUnitario = decimal.Parse(r.Cells["ValorUnitario"].Value.ToString());
                    oComprobante.Conceptos[i].Importe = decimal.Parse(r.Cells["Importe"].Value.ToString());
                    if (!String.IsNullOrEmpty(r.Cells["NumeroPedimento"].Value.ToString()))
                    {
                        oComprobante.Conceptos[i].InformacionAduanera = new CFDI40.ComprobanteConceptoInformacionAduanera[1];
                        oComprobante.Conceptos[i].InformacionAduanera[0] = new CFDI40.ComprobanteConceptoInformacionAduanera();
                        oComprobante.Conceptos[i].InformacionAduanera[0].NumeroPedimento = r.Cells["NumeroPedimento"].Value.ToString();

                        //Ejemplo de pedimento: 10 47 3807 8003832
                    }
                    i++;
                }

                //Agregar leyendaFiscal
                int totalComplementos = 0;
                try
                {
                    if (LeyendaFiscal.Text != "")
                    {
                        totalComplementos++;
                        oComprobante.Complemento = new CFDI40.ComprobanteComplemento();
                        //oComprobante.Complemento[0] = new CFDI40.ComprobanteComplemento();
                        oComprobante.Complemento.Any = new XmlElement[totalComplementos];
                        
                        LeyendasFiscales oLeyendasFiscales = new LeyendasFiscales();
                        oLeyendasFiscales.Leyenda = new LeyendasFiscalesLeyenda[1];
                        oLeyendasFiscales.Leyenda[0] = new LeyendasFiscalesLeyenda();
                        oLeyendasFiscales.Leyenda[0].textoLeyenda = LeyendaFiscal.Text;
                        
                        
                        XmlDocument doc = new XmlDocument();
                        XmlSerializerNamespaces myNamespaces_leyenda = new XmlSerializerNamespaces();
                        myNamespaces_leyenda.Add("leyendasFisc", "http://www.sat.gob.mx/leyendasFiscales");

                        using (XmlWriter writer = doc.CreateNavigator().AppendChild())
                        {
                            new XmlSerializer(oLeyendasFiscales.GetType()).Serialize(writer, oLeyendasFiscales, myNamespaces_leyenda);
                        }
                        oComprobante.Complemento.Any[0] = doc.DocumentElement;
                    }
                }
                catch (Exception e)
                {
                    ErrorFX.mostrar(e, true, true, "frmTraslado Error en Leyenda Fiscal - 189 ");
                    return;
                }


                string cadena = timbrado40.cadena(oComprobante);
                oComprobante.Sello = timbrado33.SelloRSA(ocCERTIFICADO.LLAVE, ocCERTIFICADO.PASSWORD, cadena, fecha.Value.Year);
                bool timbradoReal = true;
                if (estado.Text.Contains("BORRADOR"))
                {
                    timbradoReal = false;
                }
                

                string archivotr = timbrado40.guardarComprobante(oComprobante, null, ocSERIE, estado.Text, timbradoReal);
                if (!String.IsNullOrEmpty(archivotr))
                {
                    xml.Text = archivotr;
                    Timbrar40(oComprobante);

                }
            }
            catch (Exception e)
            {
                ErrorFX.mostrar(e, true, true, "frmTraslado - 84 ");
            }
        }


        private void guardar()
        {
            try
            {
                if (!validar())
                {
                    return;
                }


                CFDI33.Comprobante oComprobante = new CFDI33.Comprobante();

                //Crear cabecera
                //LugarExpedicion = "31074" TipoDeComprobante = "T" Total = "0" TipoCambio = "19.4232" Moneda = "USD" Certificado = "MIIGMzCCBBugAwIBAgIUMDAwMDEwMDAwMDA0MDI2OTAxOTIwDQYJKoZIhvcNAQELBQAwggGyMTgwNgYDVQQDDC9BLkMuIGRlbCBTZXJ2aWNpbyBkZSBBZG1pbmlzdHJhY2nDs24gVHJpYnV0YXJpYTEvMC0GA1UECgwmU2VydmljaW8gZGUgQWRtaW5pc3RyYWNpw7NuIFRyaWJ1dGFyaWExODA2BgNVBAsML0FkbWluaXN0cmFjacOzbiBkZSBTZWd1cmlkYWQgZGUgbGEgSW5mb3JtYWNpw7NuMR8wHQYJKoZIhvcNAQkBFhBhY29kc0BzYXQuZ29iLm14MSYwJAYDVQQJDB1Bdi4gSGlkYWxnbyA3NywgQ29sLiBHdWVycmVybzEOMAwGA1UEEQwFMDYzMDAxCzAJBgNVBAYTAk1YMRkwFwYDVQQIDBBEaXN0cml0byBGZWRlcmFsMRQwEgYDVQQHDAtDdWF1aHTDqW1vYzEVMBMGA1UELRMMU0FUOTcwNzAxTk4zMV0wWwYJKoZIhvcNAQkCDE5SZXNwb25zYWJsZTogQWRtaW5pc3RyYWNpw7NuIENlbnRyYWwgZGUgU2VydmljaW9zIFRyaWJ1dGFyaW9zIGFsIENvbnRyaWJ1eWVudGUwHhcNMTYwNjAyMTU1ODAxWhcNMjAwNjAyMTU1ODAxWjCB0zEoMCYGA1UEAxMfVElHSElUQ08gTEFUSU5PQU1FUklDQSBTQSBERSBDVjEoMCYGA1UEKRMfVElHSElUQ08gTEFUSU5PQU1FUklDQSBTQSBERSBDVjEoMCYGA1UEChMfVElHSElUQ08gTEFUSU5PQU1FUklDQSBTQSBERSBDVjElMCMGA1UELRMcVExBMDEwMjI3QzUwIC8gU0FNSDcyMTIwNkVDMzEeMBwGA1UEBRMVIC8gU0FNSDcyMTIwNkhUU05STTA2MQwwCgYDVQQLEwNDVVUwggEiMA0GCSqGSIb3DQEBAQUAA4IBDwAwggEKAoIBAQCKowTKrWnXDNQY1HVywpAzVC1rVjyPXdbz69qfkcyLNC8xGvKQ8F3d8Gu5oR2yocBZIc4VQpdQbTVQm12WludTd5t+q7HO0+hb9Teaw1+e8hynLMLgyUaSm9NMAj36DD70TAIHsoyqQnEgsWhUPI9/ZdJwc7IIUsGWfhDjrEV4eIKz7ErXqPUasAWoddaqejxe7D1JJ8p/19roas3Bsn3l9q9C7NSQVxMwl919aTfTAHJzH5WZuQK3hP+aI7UC659oNSqSSRKlpKly1YYDdmb1NuYr69IPOOhFlF46TrKRtYRRmAVE1OGNwPjF+R2wwk7YVG14OQwIlsLZukgzr2o3AgMBAAGjHTAbMAwGA1UdEwEB/wQCMAAwCwYDVR0PBAQDAgbAMA0GCSqGSIb3DQEBCwUAA4ICAQAlgEOKY0Mc1bksNmxDZeoiugtO24KO9pnUBosvtIvHNKIjF/rVOjY3OYPNOyArtGQIhzZSnaCYokK8PsIRTLon7AWu824zD/K46T45H/2CwfhA14rE1z1fyzcbdeSK/KPBoi12TCSXNrDDuQgjd81YkyKn51UtX/8rtL0qw1TzpzpFkkAq+pzqJbd/eOcgIKNt1NVVXWkVfXDGdIQTi9Hj94RQJMj7Jc0u3ZahgQiorl467mqCLnP+v7munAVT0W9om2QkVbwfv2PAoSWJ3aVFd2/aG1E+1fft/pMpBc8KZBJbw8gTcZqnrX4bXZTDObeDGiyKugPlOyQCrywQZJPuJw40OxZ+GGq7iyq8nU5CxB07X7YEG+Rd6YLEOIfRVajj0mKEUcDFe6MZwgDCN4M81tsfeW+ZN5zExlT/4V77qfU2wjy22QQvzewS7bUmcLZiaeDuwDD5Qq04sx4nEzCmbYwZfA7jHZ5jregF+B/IsGec9Y8t82tVa8VT5ymGOXXegSRN3wFdzUC5Zjeikq5t4/o8tOlIbaKll2M0UOXz9PM6Lk7N43UyAaRofcjlYDlPHNHVTzuvVgJadvyhCCQRJ0JkN3F3dTy7sa1/HOpL0iZgUlkc6pWs4uvtrPhvXJaAU8nOSwMS9u2VVJ6XpLOOAKNsiELMcXDZ1qTdQkGcfg==" SubTotal = "0" NoCertificado = "00001000000402690192" Sello = "MXf7SaQrANFAuhsBA7RUBebqL2EV2SpNvgmW/zDQ+zsOUMlmTjJxqmcNdKNOjNRfzwBlBa4csuWc/dwSOeoXvJm/6HKi7J99a9Rhc44jZcABq5a0uQRtpgKEDv/B4RDWwbZBIVk5Yor3gsFQSSZDNBCe0+Xsz3y69+V3I7hc3AtlK/0x9SAVpolHZf0X4xQpzyrOHm3nmYsmXSM7+I4UeMuOD07kvBv2B7fR9GlYtLeHQBNk4bCjUhusae7yO0oRjMvvfoS3zNjtsCe8sTHN8+2WFPkflsFo/y5Vj+7qd5tiYSry42LzNsVTU+W6we0rkOx6bJ6xbJs37vEtyxDKSQ==" Fecha = "2018-05-08T10:07:29" Folio = "113" Serie = "B" Version = "3.3" xmlns: cfdi = "http://www.sat.gob.mx/cfd/3"
                oComprobante.LugarExpedicion = ocSERIE.CP;
                oComprobante.TipoDeComprobante = "T";
                oComprobante.MetodoPago = null;
                oComprobante.SubTotal = oComprobante.Total = 0;
                oComprobante.Serie = ocSERIE.ID;
                oComprobante.Folio = ocSERIE.IMPRESOS;
                oComprobante.Descuento = 0;

                DateTime fechaTr = fecha.Value;
                IFormatProvider culture = new CultureInfo("es-MX", true);
                DateTime CREATE_DATE = DateTime.Now;

                fechaTr = DateTime.Parse(fechaTr.Day.ToString() + "/" + fechaTr.Month.ToString() + "/" + fechaTr.Year.ToString() + " " + fecha.Value.ToString("HH:mm:ss"), culture);

                oComprobante.Fecha = fechaTr;
                oComprobante.Moneda = moneda.Text;
                oComprobante.TipoCambio = decimal.Parse(tipoCambio.Text);
                oComprobante.TipoCambioSpecified = true;

                X509Certificate cert = X509Certificate.CreateFromCertFile(ocCERTIFICADO.CERTIFICADO);
                try
                {
                    string NoSerie = cert.GetSerialNumberString();
                    string Certificado64 = System.Convert.ToBase64String(cert.GetRawCertData());
                    oComprobante.Certificado = Certificado64;
                    oComprobante.NoCertificado = timbrado33.validacion(ocCERTIFICADO.NO_CERTIFICADO);

                }
                catch (Exception e)
                {
                    ErrorFX.mostrar(e, true, true, "frmTraslado - Certificado - 110 ");
                    return;
                }

                //Emisor
                oComprobante.Emisor = new CFDI33.ComprobanteEmisor();
                oComprobante.Emisor.Rfc = ocEMPRESA.RFC;
                oComprobante.Emisor.Nombre = ocEMPRESA.ID;
                oComprobante.Emisor.RegimenFiscal = ocEMPRESA.REGIMEN_FISCAL;

                //Receptor
                oComprobante.Receptor = new CFDI33.ComprobanteReceptor();
                oComprobante.Receptor.Rfc = receptor.Text;
                oComprobante.Receptor.Nombre = nombre.Text;
                oComprobante.Receptor.UsoCFDI = "P01";
                try
                {
                    oComprobante.Receptor.UsoCFDI = ((ComboItem)UsoCFDI.SelectedItem).Value.ToString();
                }
                catch
                {

                }
                //Agregar las lineas
                oComprobante.Conceptos = new CFDI33.ComprobanteConcepto[dtg.Rows.Count];
                int i = 0;
                foreach (DataGridViewRow r in dtg.Rows)
                {
                    oComprobante.Conceptos[i] = new CFDI33.ComprobanteConcepto();
                    oComprobante.Conceptos[i].Cantidad = decimal.Parse(r.Cells["Cantidad"].Value.ToString());
                    oComprobante.Conceptos[i].ClaveProdServ = r.Cells["ClaveProdServ"].Value.ToString();
                    if (r.Cells["Unidad"].Value != null)
                    {
                        if (!String.IsNullOrEmpty(r.Cells["Unidad"].Value.ToString()))
                        {
                            oComprobante.Conceptos[i].Unidad = r.Cells["Unidad"].Value.ToString();
                        }
                    }



                    oComprobante.Conceptos[i].Descripcion = r.Cells["Descripcion"].Value.ToString();
                    oComprobante.Conceptos[i].ClaveUnidad = r.Cells["ClaveUnidad"].Value.ToString();
                    if (r.Cells["NoIdentificacion"].Value != null)
                    {
                        if (!String.IsNullOrEmpty(r.Cells["NoIdentificacion"].Value.ToString()))
                        {
                            oComprobante.Conceptos[i].NoIdentificacion = r.Cells["NoIdentificacion"].Value.ToString();
                        }
                    }
                    oComprobante.Conceptos[i].ValorUnitario = decimal.Parse(r.Cells["ValorUnitario"].Value.ToString());
                    oComprobante.Conceptos[i].Importe = decimal.Parse(r.Cells["Importe"].Value.ToString());
                    if (!String.IsNullOrEmpty(r.Cells["NumeroPedimento"].Value.ToString()))
                    {
                        oComprobante.Conceptos[i].InformacionAduanera = new CFDI33.ComprobanteConceptoInformacionAduanera[1];
                        oComprobante.Conceptos[i].InformacionAduanera[0] = new CFDI33.ComprobanteConceptoInformacionAduanera();
                        oComprobante.Conceptos[i].InformacionAduanera[0].NumeroPedimento = r.Cells["NumeroPedimento"].Value.ToString();

                        //Ejemplo de pedimento: 10 47 3807 8003832
                    }
                    i++;
                }

                //Agregar leyendaFiscal
                try
                {
                    if (LeyendaFiscal.Text != "")
                    {
                        oComprobante.Complemento = new CFDI33.ComprobanteComplemento[1];
                        LeyendasFiscales oLeyendasFiscales = new LeyendasFiscales();
                        oLeyendasFiscales.Leyenda = new LeyendasFiscalesLeyenda[1];
                        oLeyendasFiscales.Leyenda[0] = new LeyendasFiscalesLeyenda();
                        oLeyendasFiscales.Leyenda[0].textoLeyenda = LeyendaFiscal.Text;

                        XmlDocument doc = new XmlDocument();
                        XmlSerializerNamespaces myNamespaces_leyenda = new XmlSerializerNamespaces();
                        myNamespaces_leyenda.Add("leyendasFisc", "http://www.sat.gob.mx/leyendasFiscales");

                        using (XmlWriter writer = doc.CreateNavigator().AppendChild())
                        {
                            new XmlSerializer(oLeyendasFiscales.GetType()).Serialize(writer, oLeyendasFiscales, myNamespaces_leyenda);
                        }
                        oComprobante.Complemento = new CFDI33.ComprobanteComplemento[1];
                        oComprobante.Complemento[0] = new CFDI33.ComprobanteComplemento();
                        oComprobante.Complemento[0].Any = new XmlElement[1];
                        oComprobante.Complemento[0].Any[0] = doc.DocumentElement;
                    }
                }
                catch (Exception e)
                {
                    ErrorFX.mostrar(e, true, true, "frmTraslado Error en Leyenda Fiscal - 189 ");
                    return;
                }


                string cadena = timbrado33.cadena(oComprobante);
                oComprobante.Sello = timbrado33.SelloRSA(ocCERTIFICADO.LLAVE, ocCERTIFICADO.PASSWORD, cadena, fecha.Value.Year);

                string archivotr = timbrado33.guardarComprobante(oComprobante,null, ocSERIE, estado.Text);
                if (!String.IsNullOrEmpty(archivotr))
                {
                    xml.Text = archivotr;
                    timbrar(oComprobante);

                }
            }
            catch (Exception e)
            {
                ErrorFX.mostrar(e, true, true, "frmTraslado - 84 ");
            }
        }

        private bool timbrar(CFDI33.Comprobante oComprobante)
        {
            try
            {
                //Timbrar
                informacion.Text = "Timbrando el XML " + serie.Text;
                bool usar_edicom = false;
                if (estado.Text.Contains("BORRADOR"))
                {
                    usar_edicom = true;
                }
                try
                {
                    if (usar_edicom)
                    {
                        usar_edicom = this.generar_CFDI(oComprobante, 2, Globales.oCFDI_USUARIO.USUARIO, cENCRIPTACION.Decrypt(Globales.oCFDI_USUARIO.PASSWORD));

                    }
                    else
                    {
                        usar_edicom = this.generar_CFDI(oComprobante, 1, Globales.oCFDI_USUARIO.USUARIO, cENCRIPTACION.Decrypt(Globales.oCFDI_USUARIO.PASSWORD));

                        ocSERIE.incrementar(ocSERIE.ROW_ID);
                        guardarCambios();
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Error timbrando en EDICOM" + Environment.NewLine + ex.InnerException.ToString(), Application.ProductVersion, MessageBoxButtons.OK);
                    return false;
                }
                if (!estado.Text.Contains("BORRADOR"))
                {
                    return true;
                }
                return false;
            }
            catch (Exception e)
            {
                ErrorFX.mostrar(e, true, true, "frmTraslado - 449 ");
            }
            return true;
        }
        private bool Timbrar40(CFDI40.Comprobante oComprobante)
        {
            try
            {
                //Timbrar
                informacion.Text = "Timbrando el XML " + serie.Text;
                bool usar_edicom = false;
                if (estado.Text.Contains("BORRADOR"))
                {
                    usar_edicom = true;
                }
                try
                {
                    if (usar_edicom)
                    {
                        usar_edicom = this.GenerarCFDI40(oComprobante, 2, Globales.oCFDI_USUARIO.USUARIO, cENCRIPTACION.Decrypt(Globales.oCFDI_USUARIO.PASSWORD));

                    }
                    else
                    {
                        usar_edicom = this.GenerarCFDI40(oComprobante, 1, Globales.oCFDI_USUARIO.USUARIO, cENCRIPTACION.Decrypt(Globales.oCFDI_USUARIO.PASSWORD));

                        ocSERIE.incrementar(ocSERIE.ROW_ID);
                        guardarCambios();
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Error timbrando en EDICOM" + Environment.NewLine + ex.InnerException.ToString(), Application.ProductVersion, MessageBoxButtons.OK);
                    return false;
                }
                if (!estado.Text.Contains("BORRADOR"))
                {
                    return true;
                }
                return false;
            }
            catch (Exception e)
            {
                ErrorFX.mostrar(e, true, true, "frmTraslado 40 - 449 ");
            }
            return true;
        }

        private void guardarCambios()
        {
            try
            {
                //Generar el PDF
                imprimir();
                //Aumentar la serie
                ocSERIE.incrementar(ocSERIE.ROW_ID);
                bloquearControles();
            }
            catch (Exception e)
            {
                ErrorFX.mostrar(e, true, true, "frmTraslado - 286 ");
            }
            return;
        }

        private void imprimir()
        {
            string embarcadoTr = "";
            string permisoTr = "";
            string entregarTr = "";
            if (origen.SelectedIndex > -1)
            {
                ComboItemReceptor oComboItemReceptor = (ComboItemReceptor)origen.SelectedItem;
                embarcadoTr = oComboItemReceptor.Value;
                permisoTr = oComboItemReceptor.OtroValor;
            }            
            //if (destino.SelectedIndex > -1)
            //{
            //    ComboItemReceptor oComboItemReceptor = (ComboItemReceptor)destino.SelectedItem;
            //    entregarTr = oComboItemReceptor.Value;
            //}
            if (!String.IsNullOrEmpty(DestinoDireccion.Text))
            {
                entregarTr = DestinoDireccion.Text;
            }

            cTraslado ocTraslado = new cTraslado();
            if (origen.SelectedIndex > -1)
            {
                ComboItemReceptor oComboItemReceptor = (ComboItemReceptor)origen.SelectedItem;
                embarcadoTr = oComboItemReceptor.Value;
                permisoTr = oComboItemReceptor.OtroValor;
            }

            //if (destino.SelectedIndex > -1)
            //{
            //    ComboItemReceptor oComboItemReceptor = (ComboItemReceptor)destino.SelectedItem;
            //    entregarTr = oComboItemReceptor.Value;
            //}
            entregarTr = DestinoDireccion.Text;

            // Crear una lista para almacenar los valores de la columna "Especie"
            List<string> listaEspecies = new List<string>();

            // Recorrer cada fila en el DataGridView
            foreach (DataGridViewRow fila in dtg.Rows)
            {
                // Verificar que la fila no sea una fila nueva (para evitar una excepci�n)
                if (!fila.IsNewRow)
                {
                    // Obtener el valor de la columna "Especie" y agregarlo a la lista
                    string especie = fila.Cells["Especie"].Value?.ToString();
                    if (!string.IsNullOrEmpty(especie)) // Evitar valores nulos o vac�os
                    {
                        listaEspecies.Add(especie);
                    }
                }
            }

            // Ahora "listaEspecies" contiene todos los valores de la columna "Especie"


            ocTraslado.imprimir(xml.Text, true, embarcadoTr, permisoTr, entregarTr
                ,LeyendaFiscal.Text, listaEspecies);
        }

        private void bloquearControles()
        {
            try
            {
                foreach (var c in this.Controls)
                {
                    if (c is Button)
                    {
                        ((Button)c).Enabled = false;
                    }

                    if (c is ComboBox)
                    {
                        ((ComboBox)c).Enabled = false;
                    }

                    if (c is TextBox)
                    {
                        ((TextBox)c).Enabled = false;
                    }
                }

                btnNuevo.Enabled = btnXML.Enabled = btnPDF.Enabled = btnImprimir.Enabled = true;

            }
            catch (Exception e)
            {
                ErrorFX.mostrar(e, true, true, "frmTraslado - 286 ");
            }
        }

        public bool GenerarCFDI40(CFDI40.Comprobante oComprobante, int Opcion, string EDICOM_USUARIO, string EDICOM_PASSWORD)
        {
            try
            {
                //Comprimir Archivo
                string Archivo_Tmp = oComprobante.Serie + oComprobante.Folio + ".xml";

                File.Copy(xml.Text, Archivo_Tmp, true);
                string ARCHIVO_zip = Archivo_Tmp.ToUpper().Replace("XML", "ZIP");
                using (ZipFile zip = new ZipFile())
                {
                    zip.AddFile(Archivo_Tmp);
                    zip.Save(ARCHIVO_zip);
                }
                //Fin de Compresion de Archivo
                File.Delete(Archivo_Tmp);

                byte[] ARCHIVO_Leido = ReadBinaryFile(ARCHIVO_zip);
                File.Delete(ARCHIVO_zip);
                CFDiClient oCFDiClient = new CFDiClient();

                byte[] PROCESO;

                switch (Opcion)
                {
                    case 1:
                        try
                        {
                            informacion.Text = "Generando CFDI ";
                            PROCESO = oCFDiClient.getCfdi(EDICOM_USUARIO, EDICOM_PASSWORD, ARCHIVO_Leido);
                            guardar_archivo(PROCESO, oComprobante.Serie + oComprobante.Folio, xml.Text);
                        }
                        catch (FaultException oException)
                        {

                            CFDiException oCFDiException = new CFDiException();
                            MessageBox.Show(oException.Message, Application.ProductName + "-" + Application.ProductVersion, MessageBoxButtons.OK, MessageBoxIcon.Error);
                            return false;
                        }

                        break;
                    case 2:
                        try
                        {
                            informacion.Text = "Generando CFDI Borrador ";
                            PROCESO = oCFDiClient.getCfdiTest(EDICOM_USUARIO, EDICOM_PASSWORD, ARCHIVO_Leido);
                            guardar_archivo(PROCESO, oComprobante.Serie + oComprobante.Folio, xml.Text);
                        }
                        catch (FaultException oException)
                        {

                            CFDiException oCFDiException = new CFDiException();
                            MessageBox.Show(oException.Message, Application.ProductName + "-" + Application.ProductVersion, MessageBoxButtons.OK, MessageBoxIcon.Error);
                            informacion.Text = "Error " + DateTime.Now.ToShortDateString() + " " + DateTime.Now.ToLongTimeString();
                            return false;
                        }
                        break;
                    case 3:
                        try
                        {
                            informacion.Text = "Generando Timbre CFDI  ";
                            PROCESO = oCFDiClient.getTimbreCfdi(EDICOM_USUARIO, EDICOM_PASSWORD, ARCHIVO_Leido);
                        }
                        catch (Exception oException)
                        {

                            CFDiException oCFDiException = new CFDiException();
                            MessageBox.Show(oException.Message, Application.ProductName + "-" + Application.ProductVersion, MessageBoxButtons.OK, MessageBoxIcon.Error);
                            informacion.Text = "Error " + DateTime.Now.ToShortDateString() + " " + DateTime.Now.ToLongTimeString();
                            return false;
                        }
                        break;
                    case 4:
                        try
                        {
                            informacion.Text = "Generando Timbre CFDI Test ";
                            PROCESO = oCFDiClient.getTimbreCfdiTest(EDICOM_USUARIO, EDICOM_PASSWORD, ARCHIVO_Leido);
                        }
                        catch (Exception oException)
                        {

                            CFDiException oCFDiException = new CFDiException();
                            MessageBox.Show(oException.Message, Application.ProductName + "-" + Application.ProductVersion, MessageBoxButtons.OK, MessageBoxIcon.Error);
                            informacion.Text = "Error " + DateTime.Now.ToShortDateString() + " " + DateTime.Now.ToLongTimeString();
                            return false;
                        }
                        break;
                }

                informacion.Text = "Generado y Sellado en Modo Borrador " + DateTime.Now.Day.ToString() + "/" + DateTime.Now.Month.ToString() + "/" + DateTime.Now.Year.ToString() + " " + DateTime.Now.ToString("HH:mm:ss");
                if (Opcion == 1)
                {
                    informacion.Text = informacion.Text.Replace("Borrador", "");
                }


                return true;
            }
            catch (Exception oException)
            {
                MessageBox.Show("Generando CFDI " + oException.Message, Application.ProductName + "-" + Application.ProductVersion, MessageBoxButtons.OK, MessageBoxIcon.Error);
                informacion.Text = "Error " + DateTime.Now.ToShortDateString() + " " + DateTime.Now.ToLongTimeString();
                return false;
            }
        }

        public bool generar_CFDI(CFDI33.Comprobante oComprobante, int Opcion, string EDICOM_USUARIO, string EDICOM_PASSWORD)
        {
            try
            {
                //Comprimir Archivo
                string Archivo_Tmp = oComprobante.Serie + oComprobante.Folio + ".xml";

                File.Copy(xml.Text, Archivo_Tmp, true);
                string ARCHIVO_zip = Archivo_Tmp.ToUpper().Replace("XML", "ZIP");
                using (ZipFile zip = new ZipFile())
                {
                    zip.AddFile(Archivo_Tmp);
                    zip.Save(ARCHIVO_zip);
                }
                //Fin de Compresion de Archivo
                File.Delete(Archivo_Tmp);

                byte[] ARCHIVO_Leido = ReadBinaryFile(ARCHIVO_zip);
                File.Delete(ARCHIVO_zip);
                CFDiClient oCFDiClient = new CFDiClient();

                byte[] PROCESO;

                switch (Opcion)
                {
                    case 1:
                        try
                        {
                            informacion.Text = "Generando CFDI ";
                            PROCESO = oCFDiClient.getCfdi(EDICOM_USUARIO, EDICOM_PASSWORD, ARCHIVO_Leido);
                            guardar_archivo(PROCESO, oComprobante.Serie + oComprobante.Folio, xml.Text);
                        }
                        catch (FaultException oException)
                        {

                            CFDiException oCFDiException = new CFDiException();
                            MessageBox.Show(oException.Message, Application.ProductName + "-" + Application.ProductVersion, MessageBoxButtons.OK, MessageBoxIcon.Error);
                            return false;
                        }

                        break;
                    case 2:
                        try
                        {
                            informacion.Text = "Generando CFDI Borrador ";
                            PROCESO = oCFDiClient.getCfdiTest(EDICOM_USUARIO, EDICOM_PASSWORD, ARCHIVO_Leido);
                            guardar_archivo(PROCESO, oComprobante.Serie + oComprobante.Folio, xml.Text);
                        }
                        catch (FaultException oException)
                        {

                            CFDiException oCFDiException = new CFDiException();
                            MessageBox.Show(oException.Message, Application.ProductName + "-" + Application.ProductVersion, MessageBoxButtons.OK, MessageBoxIcon.Error);
                            informacion.Text = "Error " + DateTime.Now.ToShortDateString() + " " + DateTime.Now.ToLongTimeString();
                            return false;
                        }
                        break;
                    case 3:
                        try
                        {
                            informacion.Text = "Generando Timbre CFDI  ";
                            PROCESO = oCFDiClient.getTimbreCfdi(EDICOM_USUARIO, EDICOM_PASSWORD, ARCHIVO_Leido);
                        }
                        catch (Exception oException)
                        {

                            CFDiException oCFDiException = new CFDiException();
                            MessageBox.Show(oException.Message, Application.ProductName + "-" + Application.ProductVersion, MessageBoxButtons.OK, MessageBoxIcon.Error);
                            informacion.Text = "Error " + DateTime.Now.ToShortDateString() + " " + DateTime.Now.ToLongTimeString();
                            return false;
                        }
                        break;
                    case 4:
                        try
                        {
                            informacion.Text = "Generando Timbre CFDI Test ";
                            PROCESO = oCFDiClient.getTimbreCfdiTest(EDICOM_USUARIO, EDICOM_PASSWORD, ARCHIVO_Leido);
                        }
                        catch (Exception oException)
                        {

                            CFDiException oCFDiException = new CFDiException();
                            MessageBox.Show(oException.Message, Application.ProductName + "-" + Application.ProductVersion, MessageBoxButtons.OK, MessageBoxIcon.Error);
                            informacion.Text = "Error " + DateTime.Now.ToShortDateString() + " " + DateTime.Now.ToLongTimeString();
                            return false;
                        }
                        break;
                }

                informacion.Text = "Generado y Sellado en Modo Borrador " + DateTime.Now.Day.ToString() + "/" + DateTime.Now.Month.ToString() + "/" + DateTime.Now.Year.ToString() + " " + DateTime.Now.ToString("HH:mm:ss");
                if (Opcion == 1)
                {
                    informacion.Text = informacion.Text.Replace("Borrador", "");
                }


                return true;
            }
            catch (Exception oException)
            {
                MessageBox.Show("Generando CFDI " + oException.Message, Application.ProductName + "-" + Application.ProductVersion, MessageBoxButtons.OK, MessageBoxIcon.Error);
                informacion.Text = "Error " + DateTime.Now.ToShortDateString() + " " + DateTime.Now.ToLongTimeString();
                return false;
            }
        }

        private void guardar_archivo(byte[] PROCESO, string INVOICE_ID, string ARCHIVO)
        {
            //Guardar los PROCESO

            File.WriteAllBytes("TEMPORAL.ZIP", PROCESO);
            //Descomprimir
            String TargetDirectory = "TEMPORAL";
            using (Ionic.Zip.ZipFile zip = Ionic.Zip.ZipFile.Read("TEMPORAL.ZIP"))
            {
                zip.ExtractAll(TargetDirectory, Ionic.Zip.ExtractExistingFileAction.OverwriteSilently);
            }

            //Copiar el ARCHIVO Descrompimido por otro
            string ARCHIVO_descomprimido = TargetDirectory + @"\" + "SIGN_" + INVOICE_ID + ".XML";

            File.Copy(ARCHIVO_descomprimido, ARCHIVO, true);
            xml.Text = ARCHIVO;
            File.Delete(ARCHIVO_descomprimido);
            File.Delete("TEMPORAL.ZIP");

        }

        private bool validar()
        {
            try
            {
                if (!timbrado33.IsNumeric(tipoCambio.Text))
                {
                    tipoCambio.SelectAll();
                    tipoCambio.Focus();
                    return false;
                }

                if (String.IsNullOrEmpty(moneda.Text))
                {
                    moneda.SelectAll();
                    moneda.Focus();
                    return false;
                }

                if (!File.Exists(ocCERTIFICADO.CERTIFICADO))
                {
                    ErrorFX.mostrar("El archivo para el certificado no se tiene acceso", true, false, false);
                    return false;
                }

                return true;
            }
            catch (Exception e)
            {
                ErrorFX.mostrar(e, true, true, "frmTraslado - 212 ");
            }
            return false;

        }


        private void exportarPDF(ReportDocument oReport, string UUID)
        {
            try
            {
                ExportOptions CrExportOptions = new ExportOptions();
                DiskFileDestinationOptions CrDiskFileDestinationOptions = new DiskFileDestinationOptions();
                PdfRtfWordFormatOptions CrFormatTypeOptions = new PdfRtfWordFormatOptions();
                //string sDirectory = xml.Text.Replace("xml", "pdf"); //ocSERIE.DIRECTORIO + @"\PDF\";
                //if (!Directory.Exists(sDirectory))
                //{
                //    Directory.CreateDirectory(sDirectory);
                //}
                string archivo = xml.Text.Replace("xml", "pdf");
                if (File.Exists(archivo))
                {
                    File.Delete(archivo);
                }
                CrDiskFileDestinationOptions.DiskFileName = archivo;
                CrExportOptions = oReport.ExportOptions;
                CrExportOptions.ExportDestinationType = ExportDestinationType.DiskFile;
                CrExportOptions.ExportFormatType = ExportFormatType.PortableDocFormat;
                CrExportOptions.DestinationOptions = CrDiskFileDestinationOptions;
                CrExportOptions.FormatOptions = CrFormatTypeOptions;
                oReport.Export();
                pdf.Text = archivo;
                frmPDF ofrmPDF = new frmPDF(pdf.Text);
                ofrmPDF.Show();
            }
            catch (Exception error)
            {
                Bitacora.Log(error.Message.ToString() + Environment.NewLine);
            }
        }

        private void cancelar()
        {
            try
            {
                //Llamar el Servicio
                CFDiClient oCFDiClient = new CFDiClient();
                string[] uuidTr = new string[1];
                uuidTr[0] = uuid.Text;

                string pfx = ocCERTIFICADO.PFX;
                if (!File.Exists(pfx))
                {
                    //Validar el directorio
                    string directorio = AppDomain.CurrentDomain.BaseDirectory;
                    pfx = directorio + @"\" + ocCERTIFICADO.PFX;
                    if (!File.Exists(pfx))
                    {
                        MessageBox.Show("El PFX " + ocCERTIFICADO.PFX + " no existe."
                            , Application.ProductName + "-" + Application.ProductVersion, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                        return;
                    }
                }
                byte[] pfx_archivo = ReadBinaryFile(pfx);
                try
                {
                    CancelaResponse respuesta;
                    ////Todo: Realizar cancelacion
                    //respuesta = oCFDiClient.cancelaCFDi(Globales.oCFDI_USUARIO.USUARIO, Globales.oCFDI_USUARIO.PASSWORD
                    //, ocEMPRESA.RFC, uuidTr, pfx_archivo, ocCERTIFICADO.PASSWORD);

                }
                catch (FaultException oException)
                {
                    CFDiException oCFDiException = new CFDiException();
                    MessageBox.Show(oException.Message, Application.ProductName + "-" + Application.ProductVersion, MessageBoxButtons.OK, MessageBoxIcon.Error);
                    informacion.Text = "Error: Cfdi No cancelado " + DateTime.Now.ToShortDateString() + " " + DateTime.Now.ToLongTimeString();
                    return;
                }
            }
            catch (Exception e)
            {
                ErrorFX.mostrar(e, true, true, "frmTraslado - 113 ");
            }
        }
        #endregion

        private void emisor_SelectedIndexChanged(object sender, EventArgs e)
        {
            cargarSerie();
        }

        private void cargarSerie()
        {
            try
            {
                serie.Text = "";
                ocEMPRESA = (cEMPRESA)emisor.SelectedItem;

                foreach (cSERIE registro in ocSERIE.todos_empresa("", ocEMPRESA.ROW_ID, false, true))
                {
                    ocCERTIFICADO = new cCERTIFICADO();
                    ocCERTIFICADO.cargar(registro.ROW_ID_CERTIFICADO);
                    ocSERIE = registro;
                    serie.Text = ocSERIE.ID + ocSERIE.consecutivo(ocSERIE.ROW_ID, false);
                }

                cargarTC();
            }
            catch (Exception e)
            {
                ErrorFX.mostrar(e, true, true, "frmTraslado - 113 ");
            }
        }

        private void button5_Click(object sender, EventArgs e)
        {
            //frmSeries o = new frmSeries(ocEMPRESA);
            //o.ShowDialog();
            //cargarSerie();
            buscarSerie();
        }
        private void buscarSerie()
        {

            frmSeries oBuscador = new frmSeries(oData.sConn, "S", ocEMPRESA.ROW_ID);
            if (oBuscador.ShowDialog() == DialogResult.OK)
            {
                string ROW_ID = (string)oBuscador.selecionados[0].Cells["ROW_ID"].Value;
                string IDtr = (string)oBuscador.selecionados[0].Cells["ID"].Value;
                serie.Text = IDtr;

                ocSERIE = new cSERIE();

                ocSERIE.cargar_serie(IDtr);
                cargarSerie(ocSERIE);

            }
        }
        private void cargarSerie(cSERIE ocSERIE)
        {
            serie.Text = ocSERIE.ID + ocSERIE.consecutivo(ocSERIE.ROW_ID, false);

            DomicilioFiscalReceptor.Text = ocSERIE.CP;
            ocEMPRESA = new cEMPRESA();
            ocCERTIFICADO = new cCERTIFICADO();
            ocCERTIFICADO.cargar(ocSERIE.ROW_ID_CERTIFICADO);
            ocEMPRESA.cargar(ocCERTIFICADO.ROW_ID_EMPRESA);
        }

        private void button1_Click(object sender, EventArgs e)
        {
            importar();
        }

        private void importar()
        {
            try
            {
                Application.DoEvents();
                OpenFileDialog openFileDialog1 = new OpenFileDialog();
                openFileDialog1.Filter = "Excel files (*.xlsx)|*.xlsx|All files (*.*)|*.*";
                openFileDialog1.FilterIndex = 1;
                openFileDialog1.RestoreDirectory = true;

                if (openFileDialog1.ShowDialog() == DialogResult.OK)
                {
                    try
                    {
                        procesar(dtg, 1, openFileDialog1.FileName);
                    }
                    catch (Exception)
                    {
                        MessageBox.Show("No existe el Archivo seleccionado", Application.ProductName + "-" + Application.ProductVersion, MessageBoxButtons.OK);
                    }
                    finally
                    {
                        Application.DoEvents();
                    }
                }
            }
            catch (Exception e)
            {
                ErrorFX.mostrar(e, true, true, "frmTraslado - 485 ");
            }
        }

        private void procesar(DataGridView dtg, int hoja, string archivo)
        {
            string tmpFile = Path.GetTempFileName();
            try
            {
                if (!File.Exists(archivo))
                {
                    MessageBox.Show(this, "El archivo " + archivo + " no existe.", Application.ProductName
                        , MessageBoxButtons.OK, MessageBoxIcon.Error);
                }

                File.Delete(tmpFile);
                File.Copy(archivo, tmpFile);

                dtg.Rows.Clear();
                int rowIndex = 2;
                FileInfo existingFile = new FileInfo(tmpFile);
                using (ExcelPackage package = new ExcelPackage(existingFile))
                {
                    try
                    {
                        ExcelWorksheet worksheet = package.Workbook.Worksheets[hoja];
                        while (worksheet.Cells[rowIndex, 2].Value != null)
                        {
                            try
                            {
                                string Cnt = worksheet.Cells[rowIndex, 1].Text;
                                string ClaveProdServ = worksheet.Cells[rowIndex, 2].Text.Trim();
                                string NoIdentificacion = worksheet.Cells[rowIndex, 3].Text.Trim();
                                string ClaveUnidad = worksheet.Cells[rowIndex, 4].Text.Trim();
                                string Unidad = worksheet.Cells[rowIndex, 5].Text;
                                string Descripcion = worksheet.Cells[rowIndex, 6].Text;
                                string ValorUnitario = worksheet.Cells[rowIndex, 7].Text;
                                string Importe = worksheet.Cells[rowIndex, 8].Text;
                                string NumeroPedimento = worksheet.Cells[rowIndex, 9].Text;
                                string Especie = worksheet.Cells[rowIndex, 10].Text;

                                //Validar el NumeroPedimento
                                if (!String.IsNullOrEmpty(NumeroPedimento))
                                {
                                    if (NumeroPedimento.Length != 15 + 6)
                                    {
                                        string mensajetr = @"Se  debe registrar  el  n�mero  del  pedimento  correspondiente  a  la importaci�n del bien, el cual se integra de izquierda a derecha de la siguiente manera: �ltimos 2 d�gitos del a�o de validaci�n seguidos por dos espacios,  2 d�gitos de la aduana de despacho seguidos por dos  espacios, 4 d�gitos del n�mero de la patente seguidos por dos espacios, 1 d�gito que corresponde al �ltimo d�gito del a�o en curso, salvo que se trate de un pedimento consolidado, iniciado en el a�o inmediato anterior o del pedimento original de unarectificaci�n, 6 d�gitos de la numeraci�n progresiva por aduana. Se  debe  registrar  la  informaci�n  en  este  campo  cuando  el CFDI no contenga el complemento de comercio exterior (es una venta de primera mano nacional). Para validar la estructura de este campo puede consultar la documentaci�n t�cnica publicada en el Portal del SAT.
                                        Ejemplo:  " + Environment.NewLine + @"
                                        10  47  3807  8003832
                                        Usted tiene este: " + Environment.NewLine + @"
                                        " + NumeroPedimento + @" " + Environment.NewLine + @"
                                        Haga la correci�n
                                        ";
                                        MessageBox.Show(this, "El Numero de Pedimento esta erroneo debe ser " +
                                            Environment.NewLine +
                                            mensajetr
                                            , Application.ProductName
                                                                , MessageBoxButtons.OK, MessageBoxIcon.Error);
                                        return;
                                    }
                                }
                                if (String.IsNullOrEmpty(ValorUnitario))
                                {
                                    ValorUnitario = "0";
                                }
                                if (String.IsNullOrEmpty(Importe))
                                {
                                    Importe = "0";
                                }
                                int n = dtg.Rows.Add();
                                dtg.Rows[n].Cells["Cantidad"].Value = Cnt;
                                dtg.Rows[n].Cells["ClaveProdServ"].Value = ClaveProdServ;
                                dtg.Rows[n].Cells["NoIdentificacion"].Value = NoIdentificacion;
                                dtg.Rows[n].Cells["ClaveUnidad"].Value = ClaveUnidad;
                                dtg.Rows[n].Cells["Unidad"].Value = Unidad;
                                dtg.Rows[n].Cells["Descripcion"].Value = Descripcion;
                                dtg.Rows[n].Cells["ValorUnitario"].Value = ValorUnitario;
                                dtg.Rows[n].Cells["Importe"].Value = Importe;
                                dtg.Rows[n].Cells["NumeroPedimento"].Value = NumeroPedimento;
                                dtg.Rows[n].Cells["Especie"].Value = Especie;
                            }
                            catch (Exception e)
                            {
                                ErrorFX.mostrar(e, true, true, "frmTraslado - 540 ");
                            }
                            rowIndex++;
                        }
                    }
                    catch
                    {
                        MessageBox.Show("El archivo esta corrupto, abralo y guardelo de nuevo."
                            , Application.ProductName + "-" + Application.ProductVersion, MessageBoxButtons.OK, MessageBoxIcon.Error);

                    }
                }
            }
            catch (Exception e)
            {
                ErrorFX.mostrar(e, true, false,
                "frmTraslado - 557 - "
                );
            }
            finally
            {
                File.Delete(tmpFile);
            }

        }


        private void button6_Click(object sender, EventArgs e)
        {
            exportarArchivo();
        }

        private void exportarArchivo()
        {
            try
            {
                int n = dtg.Rows.Add();
                ExcelFX.exportar(dtg, "Plantilla importar ");
                dtg.Rows.Clear();
            }
            catch (Exception e)
            {
                ErrorFX.mostrar(e, true, false, "frmTraslado - 557 - ");
            }
        }
        private byte[] ReadBinaryFile(string fileName)
        {
            if (File.Exists(fileName))
            {
                try
                {
                    ///Open and read a file&#12290;
                    FileStream fileStream = File.OpenRead(fileName);
                    byte[] archivo = ConvertStreamToByteBuffer(fileStream);
                    fileStream.Close();
                    return archivo;
                }
                catch (Exception ex)
                {
                    return new byte[0];
                }
            }
            else
            {
                return new byte[0];
            }
        }
        public byte[] ConvertStreamToByteBuffer(System.IO.Stream theStream)
        {
            int b1;
            System.IO.MemoryStream tempStream = new System.IO.MemoryStream();
            while ((b1 = theStream.ReadByte()) != -1)
            {
                tempStream.WriteByte(((byte)b1));
            }
            return tempStream.ToArray();
        }


        private void copiarExcel()
        {
            DataObject o = (DataObject)Clipboard.GetDataObject();
            if (o.GetDataPresent(DataFormats.Text))
            {
                if (dtg.RowCount > 0)
                    dtg.Rows.Clear();

                string[] pastedRows = Regex.Split(o.GetData(DataFormats.Text).ToString().TrimEnd("\r\n".ToCharArray()), "\r\n");
                foreach (string pastedRow in pastedRows)
                {
                    string[] pastedRowCells = pastedRow.Split(new char[] { '\t' });
                    if (pastedRowCells.Length != 9)
                    {
                        MessageBox.Show("Las celdas copiadas no tiene el mismo formato " + Environment.NewLine
                            + "La configuraci�n es: " + Environment.NewLine
                            + "Cantidad " + Environment.NewLine
                            + "Unidad" + Environment.NewLine
                            + "Descripci�n" + Environment.NewLine
                            + "No Identificacion" + Environment.NewLine
                            + "Valor Unitario" + Environment.NewLine
                            + "Importe" + Environment.NewLine
                            + "Fracci�n"
                            , Application.ProductName + "-" + Application.ProductVersion, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                        return;
                    }
                    int n = dtg.Rows.Add();
                    using (DataGridViewRow myDataGridViewRow = dtg.Rows[n])
                    {
                        for (int i = 0; i < pastedRowCells.Length; i++)
                        {
                            myDataGridViewRow.Cells[i].Value = pastedRowCells[i];
                            myDataGridViewRow.Cells[i].Value = pastedRowCells[i].ToString().Replace("$", "");
                        }
                    }
                }
            }


        }

        private void fecha_ValueChanged(object sender, EventArgs e)
        {
            cargarTC();
        }

        private void cargarTC()
        {
            try
            {
                ocEMPRESA = (cEMPRESA)emisor.SelectedItem;                
                tipoCambio.Text = "1";
                if (moneda.Text.Contains("USD"))
                {
                    if (TrabajarIndependiente)
                    {
                        TipoCambioMultiple.Clases.Banxico OBanxico = new TipoCambioMultiple.Clases.Banxico();
                        informacion.Text = "Cargando...";
                        informacion.ForeColor = Color.Black;
                        decimal TipoCambioTr = OBanxico.TipoCambio(fecha.Value);
                        tipoCambio.Text = TipoCambioTr.ToString("N4");
                    }
                    else
                    {
                        string sSQL = "";
                        sSQL = @"SELECT TOP 1 *
                        FROM CURRENCY_EXCHANGE
                        WHERE (EFFECTIVE_DATE <= " + oData_ERP.convertir_fecha(fecha.Value) + @")
                        AND CURRENCY_ID='USD' 
                        ORDER BY EFFECTIVE_DATE DESC";
                        DataTable oDataTable = oData_ERP.EjecutarConsulta(sSQL);
                        if (oDataTable != null)
                        {
                            foreach (DataRow oDataRow in oDataTable.Rows)
                            {
                                //A�adir el Registro
                                tipoCambio.Text = double.Parse(oDataRow["SELL_RATE"].ToString()).ToString("N4");
                            }
                        }
                    }                    
                }
            }
            catch (Exception e)
            {
                ErrorFX.mostrar(e, true, false, "frmTraslado - 687 - ");
            }
        }

        private void button8_Click(object sender, EventArgs e)
        {
            verXML();
        }

        private void verXML()
        {
            string direccion = xml.Text;
            if (!File.Exists(direccion))
            {
                if (!File.Exists(direccion))
                {
                    MessageBox.Show("No se puede tener acceso a: " + direccion);
                    return;
                }
            }
            frmFacturaXML oObjeto = new frmFacturaXML(direccion);
            oObjeto.Show();
        }

        private void button9_Click(object sender, EventArgs e)
        {

        }

        private void verPDF()
        {
            string direccion = pdf.Text;
            if (!File.Exists(direccion))
            {
                if (!File.Exists(direccion))
                {
                    MessageBox.Show("No se puede tener acceso a: " + direccion);
                    return;
                }
            }

            frmPDF ofrmPDF = new frmPDF(direccion);
            ofrmPDF.Show();
        }

        private void receptor_SelectedValueChanged(object sender, EventArgs e)
        {
            asignarNombre();
        }

        private void asignarNombre()
        {
            var valor = receptor.SelectedItem;
            if (valor!=null)
            {
                ComboItemReceptor seleccionado = (ComboItemReceptor)receptor.SelectedItem;
                nombre.Text = seleccionado.Name;
            }
        }
        
        private string RemoveInvalidXmlChars(string strIn)
        {
            // Replace invalid characters with empty strings.
            try
            {
                return Regex.Replace(strIn, @"[^\w\.@-]", "",
                                     RegexOptions.None, TimeSpan.FromSeconds(1.5));
            }
            // If we timeout when replacing invalid characters,
            // we should return Empty.
            catch (RegexMatchTimeoutException)
            {
                return strIn;
            }
        }

        private void button2_Click_1(object sender, EventArgs e)
        {
            frmINVENTORY_TRANS ofrmINVENTORY_TRANS = new frmINVENTORY_TRANS(oData_ERP.sConn);
            if(ofrmINVENTORY_TRANS.ShowDialog()== DialogResult.OK)
            {
                agregarLineas(ofrmINVENTORY_TRANS.seleccionados);
            }
        }

        private void agregarLineas(DataGridViewSelectedRowCollection seleccionados)
        {
            generar33 ogenerar33tr = new generar33();

            foreach (DataGridViewRow transacion in seleccionados)
            {
                configuracionUM oConfiguracionUM = ogenerar33tr.buscarClaveUnidad(transacion.Cells["UM"].Value.ToString());
                string ClaveUnidadTr = "H87";
                if (oConfiguracionUM!=null)
                {
                    ClaveUnidadTr = oConfiguracionUM.SAT;
                }
                cFACTURA oFACTURA = new cFACTURA();
                configuracionProducto oConfiguracionProducto = oFACTURA.buscarClave(transacion.Cells["Producto"].Value.ToString()
                                                            , transacion.Cells["PRODUCT_CODE"].Value.ToString(),"","");
                string clasificacionSA = string.Empty;
                if (oConfiguracionProducto!=null)
                {
                    clasificacionSA = oConfiguracionProducto.clasificacionSAT;
                }
                //Losifra: Cargar del USER_1 de PART el Codigo 
                if (String.IsNullOrEmpty(clasificacionSA))
                {
                    if (transacion.Cells["ClaveServProd"].Value != null)
                    {
                        clasificacionSA = transacion.Cells["ClaveServProd"].Value.ToString();
                    }
                }

                //Losifra
                string User_6_tr = oData_ERP.IsNullNumero(transacion.Cells["USER_6"].Value);
                decimal metros = 0;
                if (Generales.Globales.IsNumeric(User_6_tr))
                {
                    metros = (decimal.Parse(User_6_tr)*decimal.Parse(transacion.Cells["Cantidad"].Value.ToString()));
                }

                int n = dtg.Rows.Add();
                dtg.Rows[n].Cells["Cantidad"].Value =  decimal.Parse(transacion.Cells["Cantidad"].Value.ToString()).ToString("#.##");
                dtg.Rows[n].Cells["ClaveProdServ"].Value = clasificacionSA;
                dtg.Rows[n].Cells["NoIdentificacion"].Value = transacion.Cells["Producto"].Value.ToString();
                dtg.Rows[n].Cells["ClaveUnidad"].Value = ClaveUnidadTr;


                dtg.Rows[n].Cells["Unidad"].Value = metros.ToString("#.####");
                if (emisor.Text.Contains("FIMA"))
                {
                    dtg.Rows[n].Cells["Unidad"].Value = transacion.Cells["UM"].Value.ToString();
                }


                dtg.Rows[n].Cells["Descripcion"].Value = RemoveInvalidXmlChars(transacion.Cells["Descripci�n"].Value.ToString());
                dtg.Rows[n].Cells["ValorUnitario"].Value = 0;
                dtg.Rows[n].Cells["Importe"].Value = 0;
                dtg.Rows[n].Cells["NumeroPedimento"].Value = "";

            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            eliminarLinea();
        }

        private void eliminarLinea()
        {
            dtg.EndEdit();
            

            if (dtg.SelectedCells.Count > 0)
            {
                DialogResult Resultado = MessageBox.Show("�Esta seguro de eliminar las l�neas seleccionadas?", Application.ProductName + "-" + Application.ProductVersion, MessageBoxButtons.YesNo);
                if (Resultado.ToString() != "Yes")
                {
                    return;
                }
                foreach (DataGridViewCell drv in dtg.SelectedCells)
                {
                    dtg.Rows.RemoveAt(drv.RowIndex);
                }
            }
        }

        private void button4_Click(object sender, EventArgs e)
        {
            cargarFactura();
        }

        private void cargarFactura()
        {
            frmRECEIVABLE_LINE oFrm = new frmRECEIVABLE_LINE(oData_ERP.sConn);
            if (oFrm.ShowDialog() == DialogResult.OK)
            {
                agregarLineasListaEmpaque(oFrm.seleccionados);
            }
        }

        private void button7_Click(object sender, EventArgs e)
        {
            cargarListaEmpaque();
        }

        private void cargarListaEmpaque()
        {
            frmSHIPPER_LINE oFrm = new frmSHIPPER_LINE(oData_ERP.sConn);
            if (oFrm.ShowDialog() == DialogResult.OK)
            {
                agregarLineasListaEmpaque(oFrm.seleccionados);
            }
        }

        private void agregarLineasListaEmpaque(DataGridViewSelectedRowCollection seleccionados)
        {

            foreach (DataGridViewRow transacion in seleccionados)
            {
                receptor.Text = transacion.Cells["VAT_REGISTRATION"].Value.ToString();
                nombre.Text = transacion.Cells["NAME"].Value.ToString();

                agregarLineasCompletas(transacion);
            }
        }

        private void agregarLineasCompletas(DataGridViewRow transacion)
        {
            generar33 ogenerar33tr = new generar33();
            configuracionUM oConfiguracionUM = ogenerar33tr.buscarClaveUnidad(transacion.Cells["UM"].Value.ToString());
            string ClaveUnidadTr = "H87";
            if (oConfiguracionUM != null)
            {
                ClaveUnidadTr = oConfiguracionUM.SAT;
            }
            cFACTURA oFACTURA = new cFACTURA();
            configuracionProducto oConfiguracionProducto = oFACTURA.buscarClave(transacion.Cells["Producto"].Value.ToString()
                                                        , transacion.Cells["PRODUCT_CODE"].Value.ToString(), "", "");
            string clasificacionSA = string.Empty;
            if (oConfiguracionProducto != null)
            {
                clasificacionSA = oConfiguracionProducto.clasificacionSAT;
            }

            int n = dtg.Rows.Add();
            dtg.Rows[n].Cells["Cantidad"].Value = decimal.Parse(transacion.Cells["Cantidad"].Value.ToString()).ToString("#.##");
            dtg.Rows[n].Cells["ClaveProdServ"].Value = clasificacionSA;
            dtg.Rows[n].Cells["NoIdentificacion"].Value = transacion.Cells["Producto"].Value.ToString();
            dtg.Rows[n].Cells["ClaveUnidad"].Value = ClaveUnidadTr;

            //Losifra
            try
            {
                if (emisor.Text.Contains("Losif"))
                {
                    string User_6_tr = oData_ERP.IsNullNumero(transacion.Cells["USER_6"].Value);
                    decimal metros = 0;
                    if (Generales.Globales.IsNumeric(User_6_tr))
                    {
                        metros = (decimal.Parse(User_6_tr) * decimal.Parse(transacion.Cells["Cantidad"].Value.ToString()));
                        dtg.Rows[n].Cells["Unidad"].Value = metros.ToString("#.####");
                    }
                }
            }
            catch
            {

            }

            if (emisor.Text.Contains("FIMA"))
            {
                dtg.Rows[n].Cells["Unidad"].Value = transacion.Cells["UM"].Value.ToString();
            }

            dtg.Rows[n].Cells["Descripcion"].Value = transacion.Cells["Descripci�n"].Value.ToString();
            dtg.Rows[n].Cells["ValorUnitario"].Value = 0;
            dtg.Rows[n].Cells["Importe"].Value = 0;
            dtg.Rows[n].Cells["NumeroPedimento"].Value = "";
        }

        private void destino_SelectedIndexChanged(object sender, EventArgs e)
        {
            ComboItemReceptor oComboItemReceptor = (ComboItemReceptor)destino.SelectedItem;
            this.DestinoDireccion.Text = oComboItemReceptor.Value;
        }

        private void button8_Click_1(object sender, EventArgs e)
        {
            CargarReceptor();
        }
        private void CargarReceptor()
        {
            frmVENDOR ofrmVENDOR = new frmVENDOR(oData_ERP.sConn);
            if (ofrmVENDOR.ShowDialog() == DialogResult.OK)
            {
                try
                {
                    receptor.Text = (string)ofrmVENDOR.selecionados[0].Cells[2].Value;
                    nombre.Text = (string)ofrmVENDOR.selecionados[0].Cells[1].Value;
                    /*
                    string numeroRegistro = "";
                    //receptor.Text = "XEXX010101000";
                    if (ofrmVENDOR.selecionados[0].Cells[3].Value != null)
                    {
                        numeroRegistro = ofrmVENDOR.selecionados[0].Cells[3].Value.ToString();
                    }
                    if (numeroRegistro == "")
                    {
                        receptor.Text = "XEXX010101000";
                    }
                    else
                    {
                        //NumRegIdTrib.Text = numeroRegistro;
                    }
                    */
                }
                catch
                {

                }

            }
        }

    }
}


