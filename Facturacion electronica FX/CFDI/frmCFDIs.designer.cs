﻿namespace FE_FX
{
    partial class frmCFDIs
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle7 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle8 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle11 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle12 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle9 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle10 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmCFDIs));
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.toolStripStatusLabel1 = new System.Windows.Forms.ToolStripStatusLabel();
            this.dtgrdGeneral = new System.Windows.Forms.DataGridView();
            this.archivo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.serie = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.folio = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.fecha = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.rfcEmisor = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.rfcReceptor = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.impuestos = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.total = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.comboItem4 = new DevComponents.Editors.ComboItem();
            this.comboItem5 = new DevComponents.Editors.ComboItem();
            this.comboItem6 = new DevComponents.Editors.ComboItem();
            this.ID = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.ENTITY_ID = new DevComponents.DotNetBar.Controls.ComboBoxEx();
            this.comboItem17 = new DevComponents.Editors.ComboItem();
            this.comboItem18 = new DevComponents.Editors.ComboItem();
            this.comboItem19 = new DevComponents.Editors.ComboItem();
            this.labelX11 = new DevComponents.DotNetBar.LabelX();
            this.directorio = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.EMPRESA_NOMBRE = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.buttonX1 = new DevComponents.DotNetBar.ButtonX();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.tabControl2 = new System.Windows.Forms.TabControl();
            this.tabPage4 = new System.Windows.Forms.TabPage();
            this.dtgPolizas = new DevComponents.DotNetBar.Controls.DataGridViewX();
            this.ACCOUNT_ID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.REFERENCE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CARGO = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ABONO = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tabPage5 = new System.Windows.Forms.TabPage();
            this.dtgUSD = new DevComponents.DotNetBar.Controls.DataGridViewX();
            this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.buttonX8 = new DevComponents.DotNetBar.ButtonX();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.labelX3 = new DevComponents.DotNetBar.LabelX();
            this.formatoDefecto = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.labelX1 = new DevComponents.DotNetBar.LabelX();
            this.serieDefecto = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.labelX2 = new DevComponents.DotNetBar.LabelX();
            this.comentarioDescuento = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.buttonX11 = new DevComponents.DotNetBar.ButtonX();
            this.accountCargoImpuesto = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.buttonX9 = new DevComponents.DotNetBar.ButtonX();
            this.accountAbonoImpuesto = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.buttonX10 = new DevComponents.DotNetBar.ButtonX();
            this.accountCargoTotal = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.buttonX7 = new DevComponents.DotNetBar.ButtonX();
            this.accountAbonoTotal = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.buttonX6 = new DevComponents.DotNetBar.ButtonX();
            this.buttonX5 = new DevComponents.DotNetBar.ButtonX();
            this.buttonX3 = new DevComponents.DotNetBar.ButtonX();
            this.buttonX2 = new DevComponents.DotNetBar.ButtonX();
            this.folderBrowserDialog1 = new System.Windows.Forms.FolderBrowserDialog();
            this.labelX4 = new DevComponents.DotNetBar.LabelX();
            this.mes = new DevComponents.DotNetBar.Controls.ComboBoxEx();
            this.labelX5 = new DevComponents.DotNetBar.LabelX();
            this.año = new DevComponents.DotNetBar.Controls.ComboBoxEx();
            this.statusStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dtgrdGeneral)).BeginInit();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.tabPage2.SuspendLayout();
            this.tabControl2.SuspendLayout();
            this.tabPage4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dtgPolizas)).BeginInit();
            this.tabPage5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dtgUSD)).BeginInit();
            this.tabPage3.SuspendLayout();
            this.SuspendLayout();
            // 
            // statusStrip1
            // 
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripStatusLabel1});
            this.statusStrip1.Location = new System.Drawing.Point(0, 403);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(711, 22);
            this.statusStrip1.TabIndex = 37;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // toolStripStatusLabel1
            // 
            this.toolStripStatusLabel1.Name = "toolStripStatusLabel1";
            this.toolStripStatusLabel1.Size = new System.Drawing.Size(42, 17);
            this.toolStripStatusLabel1.Text = "Estado";
            // 
            // dtgrdGeneral
            // 
            this.dtgrdGeneral.AllowUserToAddRows = false;
            this.dtgrdGeneral.AllowUserToDeleteRows = false;
            this.dtgrdGeneral.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dtgrdGeneral.BackgroundColor = System.Drawing.SystemColors.ControlLight;
            this.dtgrdGeneral.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.dtgrdGeneral.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dtgrdGeneral.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.archivo,
            this.serie,
            this.folio,
            this.fecha,
            this.rfcEmisor,
            this.rfcReceptor,
            this.impuestos,
            this.total});
            this.dtgrdGeneral.Location = new System.Drawing.Point(6, 30);
            this.dtgrdGeneral.Name = "dtgrdGeneral";
            this.dtgrdGeneral.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dtgrdGeneral.Size = new System.Drawing.Size(691, 253);
            this.dtgrdGeneral.TabIndex = 1;
            this.dtgrdGeneral.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dtgrdGeneral_CellDoubleClick);
            // 
            // archivo
            // 
            this.archivo.HeaderText = "archivo";
            this.archivo.Name = "archivo";
            this.archivo.ReadOnly = true;
            this.archivo.Visible = false;
            // 
            // serie
            // 
            this.serie.HeaderText = "Serie";
            this.serie.Name = "serie";
            this.serie.ReadOnly = true;
            this.serie.Width = 40;
            // 
            // folio
            // 
            this.folio.HeaderText = "Folio";
            this.folio.Name = "folio";
            this.folio.ReadOnly = true;
            this.folio.Width = 60;
            // 
            // fecha
            // 
            this.fecha.HeaderText = "Fecha";
            this.fecha.Name = "fecha";
            this.fecha.ReadOnly = true;
            this.fecha.Width = 115;
            // 
            // rfcEmisor
            // 
            this.rfcEmisor.HeaderText = "RFC Emisor";
            this.rfcEmisor.Name = "rfcEmisor";
            this.rfcEmisor.ReadOnly = true;
            // 
            // rfcReceptor
            // 
            this.rfcReceptor.HeaderText = "RFC Receptor";
            this.rfcReceptor.Name = "rfcReceptor";
            this.rfcReceptor.ReadOnly = true;
            // 
            // impuestos
            // 
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.impuestos.DefaultCellStyle = dataGridViewCellStyle1;
            this.impuestos.HeaderText = "Impuestos";
            this.impuestos.Name = "impuestos";
            this.impuestos.ReadOnly = true;
            // 
            // total
            // 
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.total.DefaultCellStyle = dataGridViewCellStyle2;
            this.total.HeaderText = "Total";
            this.total.Name = "total";
            this.total.ReadOnly = true;
            // 
            // comboItem4
            // 
            this.comboItem4.Text = "FECHA";
            // 
            // comboItem5
            // 
            this.comboItem5.Text = "FACTURA";
            // 
            // comboItem6
            // 
            this.comboItem6.Text = "CLIENTE";
            // 
            // ID
            // 
            this.ID.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.ID.Border.Class = "TextBoxBorder";
            this.ID.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.ID.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ID.ForeColor = System.Drawing.Color.Black;
            this.ID.Location = new System.Drawing.Point(134, 34);
            this.ID.Margin = new System.Windows.Forms.Padding(0);
            this.ID.Name = "ID";
            this.ID.Size = new System.Drawing.Size(86, 22);
            this.ID.TabIndex = 5;
            // 
            // ENTITY_ID
            // 
            this.ENTITY_ID.DisplayMember = "Text";
            this.ENTITY_ID.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.ENTITY_ID.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ENTITY_ID.ForeColor = System.Drawing.Color.Black;
            this.ENTITY_ID.FormattingEnabled = true;
            this.ENTITY_ID.ItemHeight = 16;
            this.ENTITY_ID.Items.AddRange(new object[] {
            this.comboItem17,
            this.comboItem18,
            this.comboItem19});
            this.ENTITY_ID.Location = new System.Drawing.Point(134, 9);
            this.ENTITY_ID.Name = "ENTITY_ID";
            this.ENTITY_ID.Size = new System.Drawing.Size(86, 22);
            this.ENTITY_ID.TabIndex = 2;
            this.ENTITY_ID.SelectedIndexChanged += new System.EventHandler(this.ENTITY_ID_SelectedIndexChanged);
            // 
            // comboItem17
            // 
            this.comboItem17.Text = "BORRADOR";
            // 
            // comboItem18
            // 
            this.comboItem18.FontStyle = System.Drawing.FontStyle.Bold;
            this.comboItem18.Text = "TIMBRADO";
            // 
            // comboItem19
            // 
            this.comboItem19.FontStyle = System.Drawing.FontStyle.Bold;
            this.comboItem19.ForeColor = System.Drawing.Color.Red;
            this.comboItem19.Text = "CANCELADO";
            // 
            // labelX11
            // 
            this.labelX11.AutoSize = true;
            this.labelX11.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX11.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX11.ForeColor = System.Drawing.Color.Black;
            this.labelX11.Location = new System.Drawing.Point(94, 11);
            this.labelX11.Name = "labelX11";
            this.labelX11.Size = new System.Drawing.Size(37, 15);
            this.labelX11.TabIndex = 1;
            this.labelX11.Text = "Emisor";
            // 
            // directorio
            // 
            this.directorio.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.directorio.Border.Class = "TextBoxBorder";
            this.directorio.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.directorio.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.directorio.ForeColor = System.Drawing.Color.Black;
            this.directorio.Location = new System.Drawing.Point(134, 59);
            this.directorio.Margin = new System.Windows.Forms.Padding(0);
            this.directorio.Name = "directorio";
            this.directorio.Size = new System.Drawing.Size(491, 22);
            this.directorio.TabIndex = 9;
            this.directorio.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.directorio_KeyPress);
            // 
            // EMPRESA_NOMBRE
            // 
            this.EMPRESA_NOMBRE.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.EMPRESA_NOMBRE.Border.Class = "TextBoxBorder";
            this.EMPRESA_NOMBRE.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.EMPRESA_NOMBRE.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.EMPRESA_NOMBRE.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.249999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.EMPRESA_NOMBRE.ForeColor = System.Drawing.Color.Black;
            this.EMPRESA_NOMBRE.Location = new System.Drawing.Point(222, 10);
            this.EMPRESA_NOMBRE.Name = "EMPRESA_NOMBRE";
            this.EMPRESA_NOMBRE.Size = new System.Drawing.Size(403, 20);
            this.EMPRESA_NOMBRE.TabIndex = 3;
            // 
            // tabControl1
            // 
            this.tabControl1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Controls.Add(this.tabPage3);
            this.tabControl1.Location = new System.Drawing.Point(0, 85);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(711, 315);
            this.tabControl1.TabIndex = 44;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.buttonX1);
            this.tabPage1.Controls.Add(this.dtgrdGeneral);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(703, 289);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "CFDI";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // buttonX1
            // 
            this.buttonX1.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.buttonX1.BackColor = System.Drawing.Color.Navy;
            this.buttonX1.ColorTable = DevComponents.DotNetBar.eButtonColor.Flat;
            this.buttonX1.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonX1.Image = global::FE_FX.Properties.Resources.ic_search_black_18dp_1x;
            this.buttonX1.Location = new System.Drawing.Point(6, 4);
            this.buttonX1.Name = "buttonX1";
            this.buttonX1.Size = new System.Drawing.Size(76, 20);
            this.buttonX1.Style = DevComponents.DotNetBar.eDotNetBarStyle.OfficeXP;
            this.buttonX1.TabIndex = 0;
            this.buttonX1.Text = "Ver PDF";
            this.buttonX1.TextAlignment = DevComponents.DotNetBar.eButtonTextAlignment.Left;
            this.buttonX1.Click += new System.EventHandler(this.buttonX1_Click_1);
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.tabControl2);
            this.tabPage2.Controls.Add(this.buttonX8);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(703, 289);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Generación de Póliza";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // tabControl2
            // 
            this.tabControl2.Controls.Add(this.tabPage4);
            this.tabControl2.Controls.Add(this.tabPage5);
            this.tabControl2.Location = new System.Drawing.Point(4, 51);
            this.tabControl2.Name = "tabControl2";
            this.tabControl2.SelectedIndex = 0;
            this.tabControl2.Size = new System.Drawing.Size(693, 232);
            this.tabControl2.TabIndex = 604;
            // 
            // tabPage4
            // 
            this.tabPage4.Controls.Add(this.dtgPolizas);
            this.tabPage4.Location = new System.Drawing.Point(4, 22);
            this.tabPage4.Name = "tabPage4";
            this.tabPage4.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage4.Size = new System.Drawing.Size(685, 206);
            this.tabPage4.TabIndex = 0;
            this.tabPage4.Text = "Pesos";
            this.tabPage4.UseVisualStyleBackColor = true;
            // 
            // dtgPolizas
            // 
            this.dtgPolizas.AllowUserToAddRows = false;
            this.dtgPolizas.AllowUserToDeleteRows = false;
            this.dtgPolizas.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dtgPolizas.BackgroundColor = System.Drawing.Color.White;
            this.dtgPolizas.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle3.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dtgPolizas.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle3;
            this.dtgPolizas.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dtgPolizas.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.ACCOUNT_ID,
            this.REFERENCE,
            this.CARGO,
            this.ABONO});
            dataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle6.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle6.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle6.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle6.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle6.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle6.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dtgPolizas.DefaultCellStyle = dataGridViewCellStyle6;
            this.dtgPolizas.EnableHeadersVisualStyles = false;
            this.dtgPolizas.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(170)))), ((int)(((byte)(170)))), ((int)(((byte)(170)))));
            this.dtgPolizas.Location = new System.Drawing.Point(6, 3);
            this.dtgPolizas.Name = "dtgPolizas";
            dataGridViewCellStyle7.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle7.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle7.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle7.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle7.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle7.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle7.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dtgPolizas.RowHeadersDefaultCellStyle = dataGridViewCellStyle7;
            this.dtgPolizas.RowHeadersVisible = false;
            this.dtgPolizas.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dtgPolizas.Size = new System.Drawing.Size(673, 200);
            this.dtgPolizas.TabIndex = 59;
            // 
            // ACCOUNT_ID
            // 
            this.ACCOUNT_ID.HeaderText = "Cuenta Contable";
            this.ACCOUNT_ID.Name = "ACCOUNT_ID";
            this.ACCOUNT_ID.Width = 150;
            // 
            // REFERENCE
            // 
            this.REFERENCE.HeaderText = "Referencia";
            this.REFERENCE.Name = "REFERENCE";
            this.REFERENCE.Width = 350;
            // 
            // CARGO
            // 
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.CARGO.DefaultCellStyle = dataGridViewCellStyle4;
            this.CARGO.HeaderText = "Cargo";
            this.CARGO.Name = "CARGO";
            this.CARGO.Width = 75;
            // 
            // ABONO
            // 
            dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.ABONO.DefaultCellStyle = dataGridViewCellStyle5;
            this.ABONO.HeaderText = "Abono";
            this.ABONO.Name = "ABONO";
            this.ABONO.Width = 75;
            // 
            // tabPage5
            // 
            this.tabPage5.Controls.Add(this.dtgUSD);
            this.tabPage5.Location = new System.Drawing.Point(4, 22);
            this.tabPage5.Name = "tabPage5";
            this.tabPage5.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage5.Size = new System.Drawing.Size(685, 206);
            this.tabPage5.TabIndex = 1;
            this.tabPage5.Text = "Dolares";
            this.tabPage5.UseVisualStyleBackColor = true;
            // 
            // dtgUSD
            // 
            this.dtgUSD.AllowUserToAddRows = false;
            this.dtgUSD.AllowUserToDeleteRows = false;
            this.dtgUSD.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dtgUSD.BackgroundColor = System.Drawing.Color.White;
            this.dtgUSD.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            dataGridViewCellStyle8.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle8.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle8.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle8.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle8.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle8.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle8.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dtgUSD.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle8;
            this.dtgUSD.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dtgUSD.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn1,
            this.dataGridViewTextBoxColumn2,
            this.dataGridViewTextBoxColumn3,
            this.dataGridViewTextBoxColumn4});
            dataGridViewCellStyle11.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle11.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle11.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle11.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle11.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle11.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle11.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dtgUSD.DefaultCellStyle = dataGridViewCellStyle11;
            this.dtgUSD.EnableHeadersVisualStyles = false;
            this.dtgUSD.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(170)))), ((int)(((byte)(170)))), ((int)(((byte)(170)))));
            this.dtgUSD.Location = new System.Drawing.Point(6, 3);
            this.dtgUSD.Name = "dtgUSD";
            dataGridViewCellStyle12.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle12.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle12.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle12.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle12.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle12.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle12.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dtgUSD.RowHeadersDefaultCellStyle = dataGridViewCellStyle12;
            this.dtgUSD.RowHeadersVisible = false;
            this.dtgUSD.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dtgUSD.Size = new System.Drawing.Size(673, 200);
            this.dtgUSD.TabIndex = 60;
            // 
            // dataGridViewTextBoxColumn1
            // 
            this.dataGridViewTextBoxColumn1.HeaderText = "Cuenta Contable";
            this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
            this.dataGridViewTextBoxColumn1.Width = 150;
            // 
            // dataGridViewTextBoxColumn2
            // 
            this.dataGridViewTextBoxColumn2.HeaderText = "Referencia";
            this.dataGridViewTextBoxColumn2.Name = "dataGridViewTextBoxColumn2";
            this.dataGridViewTextBoxColumn2.Width = 350;
            // 
            // dataGridViewTextBoxColumn3
            // 
            dataGridViewCellStyle9.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.dataGridViewTextBoxColumn3.DefaultCellStyle = dataGridViewCellStyle9;
            this.dataGridViewTextBoxColumn3.HeaderText = "Cargo";
            this.dataGridViewTextBoxColumn3.Name = "dataGridViewTextBoxColumn3";
            this.dataGridViewTextBoxColumn3.Width = 75;
            // 
            // dataGridViewTextBoxColumn4
            // 
            dataGridViewCellStyle10.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.dataGridViewTextBoxColumn4.DefaultCellStyle = dataGridViewCellStyle10;
            this.dataGridViewTextBoxColumn4.HeaderText = "Abono";
            this.dataGridViewTextBoxColumn4.Name = "dataGridViewTextBoxColumn4";
            this.dataGridViewTextBoxColumn4.Width = 75;
            // 
            // buttonX8
            // 
            this.buttonX8.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.buttonX8.BackColor = System.Drawing.Color.Navy;
            this.buttonX8.ColorTable = DevComponents.DotNetBar.eButtonColor.Office2007WithBackground;
            this.buttonX8.Font = new System.Drawing.Font("Segoe UI", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonX8.Image = global::FE_FX.Properties.Resources.ic_donut_large_black_24dp_1x;
            this.buttonX8.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top;
            this.buttonX8.Location = new System.Drawing.Point(6, 3);
            this.buttonX8.Name = "buttonX8";
            this.buttonX8.Size = new System.Drawing.Size(42, 41);
            this.buttonX8.Style = DevComponents.DotNetBar.eDotNetBarStyle.OfficeXP;
            this.buttonX8.TabIndex = 603;
            this.buttonX8.Text = "Generar";
            this.buttonX8.Click += new System.EventHandler(this.buttonX8_Click);
            // 
            // tabPage3
            // 
            this.tabPage3.Controls.Add(this.labelX3);
            this.tabPage3.Controls.Add(this.formatoDefecto);
            this.tabPage3.Controls.Add(this.labelX1);
            this.tabPage3.Controls.Add(this.serieDefecto);
            this.tabPage3.Controls.Add(this.labelX2);
            this.tabPage3.Controls.Add(this.comentarioDescuento);
            this.tabPage3.Controls.Add(this.buttonX11);
            this.tabPage3.Controls.Add(this.accountCargoImpuesto);
            this.tabPage3.Controls.Add(this.buttonX9);
            this.tabPage3.Controls.Add(this.accountAbonoImpuesto);
            this.tabPage3.Controls.Add(this.buttonX10);
            this.tabPage3.Controls.Add(this.accountCargoTotal);
            this.tabPage3.Controls.Add(this.buttonX7);
            this.tabPage3.Controls.Add(this.accountAbonoTotal);
            this.tabPage3.Controls.Add(this.buttonX6);
            this.tabPage3.Location = new System.Drawing.Point(4, 22);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage3.Size = new System.Drawing.Size(703, 289);
            this.tabPage3.TabIndex = 2;
            this.tabPage3.Text = "Configuración";
            this.tabPage3.UseVisualStyleBackColor = true;
            // 
            // labelX3
            // 
            this.labelX3.AutoSize = true;
            this.labelX3.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX3.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX3.ForeColor = System.Drawing.Color.Black;
            this.labelX3.Location = new System.Drawing.Point(21, 168);
            this.labelX3.Name = "labelX3";
            this.labelX3.Size = new System.Drawing.Size(101, 15);
            this.labelX3.TabIndex = 13;
            this.labelX3.Text = "Formato por defecto";
            this.labelX3.Click += new System.EventHandler(this.labelX3_Click);
            // 
            // formatoDefecto
            // 
            this.formatoDefecto.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.formatoDefecto.Border.Class = "TextBoxBorder";
            this.formatoDefecto.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.formatoDefecto.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.formatoDefecto.ForeColor = System.Drawing.Color.Black;
            this.formatoDefecto.Location = new System.Drawing.Point(130, 164);
            this.formatoDefecto.Margin = new System.Windows.Forms.Padding(0);
            this.formatoDefecto.Name = "formatoDefecto";
            this.formatoDefecto.Size = new System.Drawing.Size(252, 22);
            this.formatoDefecto.TabIndex = 14;
            // 
            // labelX1
            // 
            this.labelX1.AutoSize = true;
            this.labelX1.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX1.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX1.ForeColor = System.Drawing.Color.Black;
            this.labelX1.Location = new System.Drawing.Point(35, 142);
            this.labelX1.Name = "labelX1";
            this.labelX1.Size = new System.Drawing.Size(88, 15);
            this.labelX1.TabIndex = 11;
            this.labelX1.Text = "Serie por Defecto";
            // 
            // serieDefecto
            // 
            this.serieDefecto.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.serieDefecto.Border.Class = "TextBoxBorder";
            this.serieDefecto.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.serieDefecto.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.serieDefecto.ForeColor = System.Drawing.Color.Black;
            this.serieDefecto.Location = new System.Drawing.Point(130, 138);
            this.serieDefecto.Margin = new System.Windows.Forms.Padding(0);
            this.serieDefecto.Name = "serieDefecto";
            this.serieDefecto.Size = new System.Drawing.Size(252, 22);
            this.serieDefecto.TabIndex = 12;
            // 
            // labelX2
            // 
            this.labelX2.AutoSize = true;
            this.labelX2.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX2.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX2.ForeColor = System.Drawing.Color.Black;
            this.labelX2.Location = new System.Drawing.Point(8, 115);
            this.labelX2.Name = "labelX2";
            this.labelX2.Size = new System.Drawing.Size(134, 15);
            this.labelX2.TabIndex = 9;
            this.labelX2.Text = "Comentarios de descuento";
            // 
            // comentarioDescuento
            // 
            this.comentarioDescuento.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.comentarioDescuento.Border.Class = "TextBoxBorder";
            this.comentarioDescuento.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.comentarioDescuento.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.comentarioDescuento.ForeColor = System.Drawing.Color.Black;
            this.comentarioDescuento.Location = new System.Drawing.Point(145, 112);
            this.comentarioDescuento.Margin = new System.Windows.Forms.Padding(0);
            this.comentarioDescuento.Name = "comentarioDescuento";
            this.comentarioDescuento.Size = new System.Drawing.Size(538, 22);
            this.comentarioDescuento.TabIndex = 10;
            // 
            // buttonX11
            // 
            this.buttonX11.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.buttonX11.ColorTable = DevComponents.DotNetBar.eButtonColor.Office2007WithBackground;
            this.buttonX11.Image = global::FE_FX.Properties.Resources.ic_donut_large_black_24dp_1x;
            this.buttonX11.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top;
            this.buttonX11.Location = new System.Drawing.Point(8, 6);
            this.buttonX11.Name = "buttonX11";
            this.buttonX11.Size = new System.Drawing.Size(90, 47);
            this.buttonX11.Style = DevComponents.DotNetBar.eDotNetBarStyle.OfficeXP;
            this.buttonX11.TabIndex = 0;
            this.buttonX11.Text = "Guardar";
            this.buttonX11.Click += new System.EventHandler(this.buttonX11_Click);
            // 
            // accountCargoImpuesto
            // 
            this.accountCargoImpuesto.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.accountCargoImpuesto.Border.Class = "TextBoxBorder";
            this.accountCargoImpuesto.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.accountCargoImpuesto.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.accountCargoImpuesto.ForeColor = System.Drawing.Color.Black;
            this.accountCargoImpuesto.Location = new System.Drawing.Point(475, 59);
            this.accountCargoImpuesto.Margin = new System.Windows.Forms.Padding(0);
            this.accountCargoImpuesto.Name = "accountCargoImpuesto";
            this.accountCargoImpuesto.Size = new System.Drawing.Size(208, 22);
            this.accountCargoImpuesto.TabIndex = 4;
            this.accountCargoImpuesto.Visible = false;
            // 
            // buttonX9
            // 
            this.buttonX9.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.buttonX9.BackColor = System.Drawing.Color.Navy;
            this.buttonX9.ColorTable = DevComponents.DotNetBar.eButtonColor.Flat;
            this.buttonX9.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonX9.Image = global::FE_FX.Properties.Resources.ic_search_black_18dp_1x;
            this.buttonX9.Location = new System.Drawing.Point(350, 59);
            this.buttonX9.Name = "buttonX9";
            this.buttonX9.Size = new System.Drawing.Size(122, 20);
            this.buttonX9.Style = DevComponents.DotNetBar.eDotNetBarStyle.OfficeXP;
            this.buttonX9.TabIndex = 3;
            this.buttonX9.Text = "Cargo Impuesto";
            this.buttonX9.TextAlignment = DevComponents.DotNetBar.eButtonTextAlignment.Right;
            this.buttonX9.Visible = false;
            this.buttonX9.Click += new System.EventHandler(this.buttonX9_Click);
            // 
            // accountAbonoImpuesto
            // 
            this.accountAbonoImpuesto.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.accountAbonoImpuesto.Border.Class = "TextBoxBorder";
            this.accountAbonoImpuesto.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.accountAbonoImpuesto.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.accountAbonoImpuesto.ForeColor = System.Drawing.Color.Black;
            this.accountAbonoImpuesto.Location = new System.Drawing.Point(475, 85);
            this.accountAbonoImpuesto.Margin = new System.Windows.Forms.Padding(0);
            this.accountAbonoImpuesto.Name = "accountAbonoImpuesto";
            this.accountAbonoImpuesto.Size = new System.Drawing.Size(208, 22);
            this.accountAbonoImpuesto.TabIndex = 8;
            this.accountAbonoImpuesto.Visible = false;
            // 
            // buttonX10
            // 
            this.buttonX10.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.buttonX10.BackColor = System.Drawing.Color.Navy;
            this.buttonX10.ColorTable = DevComponents.DotNetBar.eButtonColor.Flat;
            this.buttonX10.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonX10.Image = global::FE_FX.Properties.Resources.ic_search_black_18dp_1x;
            this.buttonX10.Location = new System.Drawing.Point(350, 85);
            this.buttonX10.Name = "buttonX10";
            this.buttonX10.Size = new System.Drawing.Size(122, 20);
            this.buttonX10.Style = DevComponents.DotNetBar.eDotNetBarStyle.OfficeXP;
            this.buttonX10.TabIndex = 7;
            this.buttonX10.Text = "Abono Impuesto";
            this.buttonX10.TextAlignment = DevComponents.DotNetBar.eButtonTextAlignment.Right;
            this.buttonX10.Visible = false;
            this.buttonX10.Click += new System.EventHandler(this.buttonX10_Click);
            // 
            // accountCargoTotal
            // 
            this.accountCargoTotal.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.accountCargoTotal.Border.Class = "TextBoxBorder";
            this.accountCargoTotal.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.accountCargoTotal.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.accountCargoTotal.ForeColor = System.Drawing.Color.Black;
            this.accountCargoTotal.Location = new System.Drawing.Point(130, 85);
            this.accountCargoTotal.Margin = new System.Windows.Forms.Padding(0);
            this.accountCargoTotal.Name = "accountCargoTotal";
            this.accountCargoTotal.Size = new System.Drawing.Size(217, 22);
            this.accountCargoTotal.TabIndex = 6;
            // 
            // buttonX7
            // 
            this.buttonX7.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.buttonX7.BackColor = System.Drawing.Color.Navy;
            this.buttonX7.ColorTable = DevComponents.DotNetBar.eButtonColor.Flat;
            this.buttonX7.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonX7.Image = global::FE_FX.Properties.Resources.ic_search_black_18dp_1x;
            this.buttonX7.Location = new System.Drawing.Point(8, 85);
            this.buttonX7.Name = "buttonX7";
            this.buttonX7.Size = new System.Drawing.Size(119, 20);
            this.buttonX7.Style = DevComponents.DotNetBar.eDotNetBarStyle.OfficeXP;
            this.buttonX7.TabIndex = 5;
            this.buttonX7.Text = "Abono Proforma";
            this.buttonX7.TextAlignment = DevComponents.DotNetBar.eButtonTextAlignment.Right;
            this.buttonX7.Click += new System.EventHandler(this.buttonX7_Click);
            // 
            // accountAbonoTotal
            // 
            this.accountAbonoTotal.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.accountAbonoTotal.Border.Class = "TextBoxBorder";
            this.accountAbonoTotal.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.accountAbonoTotal.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.accountAbonoTotal.ForeColor = System.Drawing.Color.Black;
            this.accountAbonoTotal.Location = new System.Drawing.Point(130, 59);
            this.accountAbonoTotal.Margin = new System.Windows.Forms.Padding(0);
            this.accountAbonoTotal.Name = "accountAbonoTotal";
            this.accountAbonoTotal.Size = new System.Drawing.Size(217, 22);
            this.accountAbonoTotal.TabIndex = 2;
            // 
            // buttonX6
            // 
            this.buttonX6.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.buttonX6.BackColor = System.Drawing.Color.Navy;
            this.buttonX6.ColorTable = DevComponents.DotNetBar.eButtonColor.Flat;
            this.buttonX6.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonX6.Image = global::FE_FX.Properties.Resources.ic_search_black_18dp_1x;
            this.buttonX6.Location = new System.Drawing.Point(8, 59);
            this.buttonX6.Name = "buttonX6";
            this.buttonX6.Size = new System.Drawing.Size(119, 20);
            this.buttonX6.Style = DevComponents.DotNetBar.eDotNetBarStyle.OfficeXP;
            this.buttonX6.TabIndex = 1;
            this.buttonX6.Text = "Cargo Proforma";
            this.buttonX6.TextAlignment = DevComponents.DotNetBar.eButtonTextAlignment.Right;
            this.buttonX6.Click += new System.EventHandler(this.buttonX6_Click);
            // 
            // buttonX5
            // 
            this.buttonX5.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.buttonX5.BackColor = System.Drawing.Color.Navy;
            this.buttonX5.ColorTable = DevComponents.DotNetBar.eButtonColor.Flat;
            this.buttonX5.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonX5.Image = global::FE_FX.Properties.Resources.ic_search_black_18dp_1x;
            this.buttonX5.Location = new System.Drawing.Point(54, 60);
            this.buttonX5.Name = "buttonX5";
            this.buttonX5.Size = new System.Drawing.Size(77, 20);
            this.buttonX5.Style = DevComponents.DotNetBar.eDotNetBarStyle.OfficeXP;
            this.buttonX5.TabIndex = 8;
            this.buttonX5.Text = "Directorio";
            this.buttonX5.TextAlignment = DevComponents.DotNetBar.eButtonTextAlignment.Right;
            this.buttonX5.Click += new System.EventHandler(this.buttonX5_Click);
            // 
            // buttonX3
            // 
            this.buttonX3.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.buttonX3.BackColor = System.Drawing.Color.Navy;
            this.buttonX3.ColorTable = DevComponents.DotNetBar.eButtonColor.Flat;
            this.buttonX3.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonX3.Image = global::FE_FX.Properties.Resources.ic_search_black_18dp_1x;
            this.buttonX3.Location = new System.Drawing.Point(54, 35);
            this.buttonX3.Name = "buttonX3";
            this.buttonX3.Size = new System.Drawing.Size(77, 20);
            this.buttonX3.Style = DevComponents.DotNetBar.eDotNetBarStyle.OfficeXP;
            this.buttonX3.TabIndex = 4;
            this.buttonX3.Text = "Serie";
            this.buttonX3.TextAlignment = DevComponents.DotNetBar.eButtonTextAlignment.Right;
            this.buttonX3.Click += new System.EventHandler(this.buttonX3_Click);
            // 
            // buttonX2
            // 
            this.buttonX2.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.buttonX2.ColorTable = DevComponents.DotNetBar.eButtonColor.Office2007WithBackground;
            this.buttonX2.Image = global::FE_FX.Properties.Resources.ic_create_new_folder_black_24dp_1x;
            this.buttonX2.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top;
            this.buttonX2.Location = new System.Drawing.Point(4, 10);
            this.buttonX2.Name = "buttonX2";
            this.buttonX2.Size = new System.Drawing.Size(42, 47);
            this.buttonX2.Style = DevComponents.DotNetBar.eDotNetBarStyle.OfficeXP;
            this.buttonX2.TabIndex = 0;
            this.buttonX2.Text = "Nuevo";
            this.buttonX2.Click += new System.EventHandler(this.buttonX2_Click);
            // 
            // labelX4
            // 
            this.labelX4.AutoSize = true;
            this.labelX4.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX4.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX4.ForeColor = System.Drawing.Color.Black;
            this.labelX4.Location = new System.Drawing.Point(223, 38);
            this.labelX4.Name = "labelX4";
            this.labelX4.Size = new System.Drawing.Size(23, 15);
            this.labelX4.TabIndex = 6;
            this.labelX4.Text = "Mes";
            // 
            // mes
            // 
            this.mes.DisplayMember = "Text";
            this.mes.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.mes.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.mes.ForeColor = System.Drawing.Color.Black;
            this.mes.FormattingEnabled = true;
            this.mes.ItemHeight = 16;
            this.mes.Location = new System.Drawing.Point(252, 34);
            this.mes.Name = "mes";
            this.mes.Size = new System.Drawing.Size(86, 22);
            this.mes.TabIndex = 7;
            this.mes.SelectedIndexChanged += new System.EventHandler(this.mes_SelectedIndexChanged);
            // 
            // labelX5
            // 
            this.labelX5.AutoSize = true;
            this.labelX5.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX5.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX5.ForeColor = System.Drawing.Color.Black;
            this.labelX5.Location = new System.Drawing.Point(354, 38);
            this.labelX5.Name = "labelX5";
            this.labelX5.Size = new System.Drawing.Size(22, 15);
            this.labelX5.TabIndex = 45;
            this.labelX5.Text = "Año";
            // 
            // año
            // 
            this.año.DisplayMember = "Text";
            this.año.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.año.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.año.ForeColor = System.Drawing.Color.Black;
            this.año.FormattingEnabled = true;
            this.año.ItemHeight = 16;
            this.año.Location = new System.Drawing.Point(382, 35);
            this.año.Name = "año";
            this.año.Size = new System.Drawing.Size(86, 22);
            this.año.TabIndex = 46;
            this.año.SelectedIndexChanged += new System.EventHandler(this.año_SelectedIndexChanged);
            // 
            // frmCFDIs
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(711, 425);
            this.Controls.Add(this.año);
            this.Controls.Add(this.labelX5);
            this.Controls.Add(this.mes);
            this.Controls.Add(this.labelX4);
            this.Controls.Add(this.tabControl1);
            this.Controls.Add(this.buttonX5);
            this.Controls.Add(this.EMPRESA_NOMBRE);
            this.Controls.Add(this.directorio);
            this.Controls.Add(this.ENTITY_ID);
            this.Controls.Add(this.labelX11);
            this.Controls.Add(this.ID);
            this.Controls.Add(this.buttonX3);
            this.Controls.Add(this.buttonX2);
            this.Controls.Add(this.statusStrip1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.KeyPreview = true;
            this.Name = "frmCFDIs";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "CFDI";
            this.Load += new System.EventHandler(this.frmUsuarios_Load);
            this.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.frmFormatos_KeyPress);
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dtgrdGeneral)).EndInit();
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage2.ResumeLayout(false);
            this.tabControl2.ResumeLayout(false);
            this.tabPage4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dtgPolizas)).EndInit();
            this.tabPage5.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dtgUSD)).EndInit();
            this.tabPage3.ResumeLayout(false);
            this.tabPage3.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel1;
        private System.Windows.Forms.DataGridView dtgrdGeneral;
        private DevComponents.DotNetBar.ButtonX buttonX2;
        private DevComponents.Editors.ComboItem comboItem4;
        private DevComponents.Editors.ComboItem comboItem5;
        private DevComponents.Editors.ComboItem comboItem6;
        private DevComponents.DotNetBar.Controls.TextBoxX ID;
        private DevComponents.DotNetBar.ButtonX buttonX3;
        private DevComponents.DotNetBar.Controls.ComboBoxEx ENTITY_ID;
        private DevComponents.Editors.ComboItem comboItem17;
        private DevComponents.Editors.ComboItem comboItem18;
        private DevComponents.Editors.ComboItem comboItem19;
        private DevComponents.DotNetBar.LabelX labelX11;
        private DevComponents.DotNetBar.Controls.TextBoxX directorio;
        private DevComponents.DotNetBar.Controls.TextBoxX EMPRESA_NOMBRE;
        private DevComponents.DotNetBar.ButtonX buttonX5;
        private System.Windows.Forms.DataGridViewTextBoxColumn archivo;
        private System.Windows.Forms.DataGridViewTextBoxColumn serie;
        private System.Windows.Forms.DataGridViewTextBoxColumn folio;
        private System.Windows.Forms.DataGridViewTextBoxColumn fecha;
        private System.Windows.Forms.DataGridViewTextBoxColumn rfcEmisor;
        private System.Windows.Forms.DataGridViewTextBoxColumn rfcReceptor;
        private System.Windows.Forms.DataGridViewTextBoxColumn impuestos;
        private System.Windows.Forms.DataGridViewTextBoxColumn total;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TabPage tabPage2;
        private DevComponents.DotNetBar.Controls.DataGridViewX dtgPolizas;
        private System.Windows.Forms.DataGridViewTextBoxColumn ACCOUNT_ID;
        private System.Windows.Forms.DataGridViewTextBoxColumn REFERENCE;
        private System.Windows.Forms.DataGridViewTextBoxColumn CARGO;
        private System.Windows.Forms.DataGridViewTextBoxColumn ABONO;
        private DevComponents.DotNetBar.ButtonX buttonX8;
        private System.Windows.Forms.TabPage tabPage3;
        private DevComponents.DotNetBar.Controls.TextBoxX accountCargoImpuesto;
        private DevComponents.DotNetBar.ButtonX buttonX9;
        private DevComponents.DotNetBar.Controls.TextBoxX accountAbonoImpuesto;
        private DevComponents.DotNetBar.ButtonX buttonX10;
        private DevComponents.DotNetBar.Controls.TextBoxX accountCargoTotal;
        private DevComponents.DotNetBar.ButtonX buttonX7;
        private DevComponents.DotNetBar.Controls.TextBoxX accountAbonoTotal;
        private DevComponents.DotNetBar.ButtonX buttonX6;
        private DevComponents.DotNetBar.ButtonX buttonX11;
        private System.Windows.Forms.FolderBrowserDialog folderBrowserDialog1;
        private DevComponents.DotNetBar.LabelX labelX2;
        private DevComponents.DotNetBar.Controls.TextBoxX comentarioDescuento;
        private DevComponents.DotNetBar.ButtonX buttonX1;
        private DevComponents.DotNetBar.LabelX labelX1;
        private DevComponents.DotNetBar.Controls.TextBoxX serieDefecto;
        private DevComponents.DotNetBar.LabelX labelX3;
        private DevComponents.DotNetBar.Controls.TextBoxX formatoDefecto;
        private DevComponents.DotNetBar.LabelX labelX4;
        private DevComponents.DotNetBar.Controls.ComboBoxEx mes;
        private DevComponents.DotNetBar.LabelX labelX5;
        private DevComponents.DotNetBar.Controls.ComboBoxEx año;
        private System.Windows.Forms.TabControl tabControl2;
        private System.Windows.Forms.TabPage tabPage4;
        private System.Windows.Forms.TabPage tabPage5;
        private DevComponents.DotNetBar.Controls.DataGridViewX dtgUSD;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn2;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn3;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn4;
    }
}