﻿namespace FE_FX
{
    partial class frmCFDIConcepto
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmCFDIConcepto));
            this.button3 = new System.Windows.Forms.Button();
            this.button7 = new System.Windows.Forms.Button();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.UNIDAD = new System.Windows.Forms.ComboBox();
            this.NO_IDENTIFICACION = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.DESCRIPCION = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.IMPORTE = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.VALOR_UNITARIO = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.CANTIDAD = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.label4 = new DevComponents.DotNetBar.LabelX();
            this.label3 = new DevComponents.DotNetBar.LabelX();
            this.label2 = new DevComponents.DotNetBar.LabelX();
            this.label1 = new DevComponents.DotNetBar.LabelX();
            this.labelX1 = new DevComponents.DotNetBar.LabelX();
            this.fraccionArancelaria = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.labelX2 = new DevComponents.DotNetBar.LabelX();
            this.buttonX5 = new DevComponents.DotNetBar.ButtonX();
            this.labelX3 = new DevComponents.DotNetBar.LabelX();
            this.aduana = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.labelX4 = new DevComponents.DotNetBar.LabelX();
            this.fecha = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.numero = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.labelX5 = new DevComponents.DotNetBar.LabelX();
            this.SuspendLayout();
            // 
            // button3
            // 
            this.button3.BackColor = System.Drawing.SystemColors.Control;
            this.button3.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button3.Image = global::FE_FX.Properties.Resources.Guardar;
            this.button3.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button3.Location = new System.Drawing.Point(16, 15);
            this.button3.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(111, 28);
            this.button3.TabIndex = 0;
            this.button3.Text = "Guardar";
            this.button3.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.button3.UseVisualStyleBackColor = false;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // button7
            // 
            this.button7.BackColor = System.Drawing.SystemColors.Control;
            this.button7.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button7.Image = global::FE_FX.Properties.Resources.Eliminar_Linea;
            this.button7.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button7.Location = new System.Drawing.Point(135, 15);
            this.button7.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.button7.Name = "button7";
            this.button7.Size = new System.Drawing.Size(111, 28);
            this.button7.TabIndex = 1;
            this.button7.Text = "Eliminar";
            this.button7.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.button7.UseVisualStyleBackColor = false;
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.FileName = "openFileDialog1";
            // 
            // UNIDAD
            // 
            this.UNIDAD.FormattingEnabled = true;
            this.UNIDAD.Items.AddRange(new object[] {
            "KILO",
            "GRAMO",
            "METRO LINEAL",
            "METRO CUADRADO",
            "METRO CÚBICO",
            "PIEZA",
            "CABEZA",
            "LITRO",
            "PAR",
            "KILOWATT",
            "MILLAR",
            "JUEGO",
            "KILOWATT/HORA",
            "TONELADA",
            "BARRIL",
            "GRAMO NETO",
            "DECENAS",
            "CIENTOS",
            "DOCENAS",
            "CAJA",
            "BOTELLA"});
            this.UNIDAD.Location = new System.Drawing.Point(352, 50);
            this.UNIDAD.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.UNIDAD.Name = "UNIDAD";
            this.UNIDAD.Size = new System.Drawing.Size(160, 24);
            this.UNIDAD.TabIndex = 5;
            // 
            // NO_IDENTIFICACION
            // 
            this.NO_IDENTIFICACION.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            // 
            // 
            // 
            this.NO_IDENTIFICACION.Border.Class = "TextBoxBorder";
            this.NO_IDENTIFICACION.ForeColor = System.Drawing.Color.Black;
            this.NO_IDENTIFICACION.Location = new System.Drawing.Point(172, 82);
            this.NO_IDENTIFICACION.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.NO_IDENTIFICACION.Name = "NO_IDENTIFICACION";
            this.NO_IDENTIFICACION.Size = new System.Drawing.Size(341, 22);
            this.NO_IDENTIFICACION.TabIndex = 7;
            // 
            // DESCRIPCION
            // 
            this.DESCRIPCION.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            // 
            // 
            // 
            this.DESCRIPCION.Border.Class = "TextBoxBorder";
            this.DESCRIPCION.ForeColor = System.Drawing.Color.Black;
            this.DESCRIPCION.Location = new System.Drawing.Point(172, 112);
            this.DESCRIPCION.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.DESCRIPCION.Multiline = true;
            this.DESCRIPCION.Name = "DESCRIPCION";
            this.DESCRIPCION.Size = new System.Drawing.Size(665, 62);
            this.DESCRIPCION.TabIndex = 9;
            // 
            // IMPORTE
            // 
            this.IMPORTE.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            // 
            // 
            // 
            this.IMPORTE.Border.Class = "TextBoxBorder";
            this.IMPORTE.Enabled = false;
            this.IMPORTE.ForeColor = System.Drawing.Color.Black;
            this.IMPORTE.Location = new System.Drawing.Point(383, 181);
            this.IMPORTE.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.IMPORTE.Name = "IMPORTE";
            this.IMPORTE.Size = new System.Drawing.Size(140, 22);
            this.IMPORTE.TabIndex = 13;
            this.IMPORTE.Text = "0";
            this.IMPORTE.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.IMPORTE.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.Solo_Numero_KeyPress);
            this.IMPORTE.Leave += new System.EventHandler(this.CANTIDAD_Leave);
            // 
            // VALOR_UNITARIO
            // 
            this.VALOR_UNITARIO.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            // 
            // 
            // 
            this.VALOR_UNITARIO.Border.Class = "TextBoxBorder";
            this.VALOR_UNITARIO.ForeColor = System.Drawing.Color.Black;
            this.VALOR_UNITARIO.Location = new System.Drawing.Point(172, 181);
            this.VALOR_UNITARIO.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.VALOR_UNITARIO.Name = "VALOR_UNITARIO";
            this.VALOR_UNITARIO.Size = new System.Drawing.Size(123, 22);
            this.VALOR_UNITARIO.TabIndex = 11;
            this.VALOR_UNITARIO.Text = "0";
            this.VALOR_UNITARIO.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.VALOR_UNITARIO.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.Solo_Numero_KeyPress);
            this.VALOR_UNITARIO.Leave += new System.EventHandler(this.CANTIDAD_Leave);
            // 
            // CANTIDAD
            // 
            this.CANTIDAD.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            // 
            // 
            // 
            this.CANTIDAD.Border.Class = "TextBoxBorder";
            this.CANTIDAD.ForeColor = System.Drawing.Color.Black;
            this.CANTIDAD.Location = new System.Drawing.Point(172, 51);
            this.CANTIDAD.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.CANTIDAD.Name = "CANTIDAD";
            this.CANTIDAD.Size = new System.Drawing.Size(109, 22);
            this.CANTIDAD.TabIndex = 3;
            this.CANTIDAD.Text = "0";
            this.CANTIDAD.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.CANTIDAD.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.Solo_Numero_KeyPress);
            this.CANTIDAD.Leave += new System.EventHandler(this.CANTIDAD_Leave);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(319, 184);
            this.label4.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(47, 17);
            this.label4.TabIndex = 12;
            this.label4.Text = "Importe";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(119, 184);
            this.label3.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(39, 17);
            this.label3.TabIndex = 10;
            this.label3.Text = "Precio";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(289, 54);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(44, 17);
            this.label2.TabIndex = 4;
            this.label2.Text = "Unidad";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(101, 54);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(55, 17);
            this.label1.TabIndex = 2;
            this.label1.Text = "Cantidad";
            // 
            // labelX1
            // 
            this.labelX1.AutoSize = true;
            this.labelX1.Location = new System.Drawing.Point(84, 113);
            this.labelX1.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.labelX1.Name = "labelX1";
            this.labelX1.Size = new System.Drawing.Size(71, 17);
            this.labelX1.TabIndex = 8;
            this.labelX1.Text = "Descripción";
            // 
            // fraccionArancelaria
            // 
            this.fraccionArancelaria.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            // 
            // 
            // 
            this.fraccionArancelaria.Border.Class = "TextBoxBorder";
            this.fraccionArancelaria.ForeColor = System.Drawing.Color.Black;
            this.fraccionArancelaria.Location = new System.Drawing.Point(676, 181);
            this.fraccionArancelaria.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.fraccionArancelaria.Name = "fraccionArancelaria";
            this.fraccionArancelaria.Size = new System.Drawing.Size(161, 22);
            this.fraccionArancelaria.TabIndex = 15;
            // 
            // labelX2
            // 
            this.labelX2.AutoSize = true;
            this.labelX2.Location = new System.Drawing.Point(531, 184);
            this.labelX2.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.labelX2.Name = "labelX2";
            this.labelX2.Size = new System.Drawing.Size(122, 17);
            this.labelX2.TabIndex = 14;
            this.labelX2.Text = "Fracción Arancelaría";
            // 
            // buttonX5
            // 
            this.buttonX5.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.buttonX5.BackColor = System.Drawing.Color.Navy;
            this.buttonX5.ColorTable = DevComponents.DotNetBar.eButtonColor.Flat;
            this.buttonX5.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonX5.Image = global::FE_FX.Properties.Resources.ic_search_black_18dp_1x;
            this.buttonX5.Location = new System.Drawing.Point(16, 81);
            this.buttonX5.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.buttonX5.Name = "buttonX5";
            this.buttonX5.Size = new System.Drawing.Size(148, 25);
            this.buttonX5.Style = DevComponents.DotNetBar.eDotNetBarStyle.OfficeXP;
            this.buttonX5.TabIndex = 6;
            this.buttonX5.Text = "No identificación";
            this.buttonX5.TextAlignment = DevComponents.DotNetBar.eButtonTextAlignment.Right;
            this.buttonX5.Click += new System.EventHandler(this.buttonX5_Click);
            // 
            // labelX3
            // 
            this.labelX3.AutoSize = true;
            this.labelX3.Location = new System.Drawing.Point(117, 214);
            this.labelX3.Margin = new System.Windows.Forms.Padding(4);
            this.labelX3.Name = "labelX3";
            this.labelX3.Size = new System.Drawing.Size(47, 17);
            this.labelX3.TabIndex = 16;
            this.labelX3.Text = "Aduana";
            // 
            // aduana
            // 
            this.aduana.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            // 
            // 
            // 
            this.aduana.Border.Class = "TextBoxBorder";
            this.aduana.ForeColor = System.Drawing.Color.Black;
            this.aduana.Location = new System.Drawing.Point(172, 211);
            this.aduana.Margin = new System.Windows.Forms.Padding(4);
            this.aduana.Name = "aduana";
            this.aduana.Size = new System.Drawing.Size(161, 22);
            this.aduana.TabIndex = 17;
            // 
            // labelX4
            // 
            this.labelX4.AutoSize = true;
            this.labelX4.Location = new System.Drawing.Point(341, 214);
            this.labelX4.Margin = new System.Windows.Forms.Padding(4);
            this.labelX4.Name = "labelX4";
            this.labelX4.Size = new System.Drawing.Size(39, 17);
            this.labelX4.TabIndex = 18;
            this.labelX4.Text = "Fecha";
            // 
            // fecha
            // 
            this.fecha.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            // 
            // 
            // 
            this.fecha.Border.Class = "TextBoxBorder";
            this.fecha.ForeColor = System.Drawing.Color.Black;
            this.fecha.Location = new System.Drawing.Point(390, 211);
            this.fecha.Margin = new System.Windows.Forms.Padding(4);
            this.fecha.Name = "fecha";
            this.fecha.Size = new System.Drawing.Size(161, 22);
            this.fecha.TabIndex = 19;
            // 
            // numero
            // 
            this.numero.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            // 
            // 
            // 
            this.numero.Border.Class = "TextBoxBorder";
            this.numero.ForeColor = System.Drawing.Color.Black;
            this.numero.Location = new System.Drawing.Point(609, 211);
            this.numero.Margin = new System.Windows.Forms.Padding(4);
            this.numero.Name = "numero";
            this.numero.Size = new System.Drawing.Size(161, 22);
            this.numero.TabIndex = 21;
            // 
            // labelX5
            // 
            this.labelX5.AutoSize = true;
            this.labelX5.Location = new System.Drawing.Point(560, 214);
            this.labelX5.Margin = new System.Windows.Forms.Padding(4);
            this.labelX5.Name = "labelX5";
            this.labelX5.Size = new System.Drawing.Size(49, 17);
            this.labelX5.TabIndex = 20;
            this.labelX5.Text = "Número";
            // 
            // frmCFDIConcepto
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(844, 246);
            this.Controls.Add(this.numero);
            this.Controls.Add(this.labelX5);
            this.Controls.Add(this.buttonX5);
            this.Controls.Add(this.fecha);
            this.Controls.Add(this.labelX4);
            this.Controls.Add(this.aduana);
            this.Controls.Add(this.labelX3);
            this.Controls.Add(this.fraccionArancelaria);
            this.Controls.Add(this.labelX2);
            this.Controls.Add(this.UNIDAD);
            this.Controls.Add(this.NO_IDENTIFICACION);
            this.Controls.Add(this.DESCRIPCION);
            this.Controls.Add(this.IMPORTE);
            this.Controls.Add(this.VALOR_UNITARIO);
            this.Controls.Add(this.CANTIDAD);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.labelX1);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.button7);
            this.Controls.Add(this.button3);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.KeyPreview = true;
            this.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.MinimizeBox = false;
            this.Name = "frmCFDIConcepto";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Concepto";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Button button7;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private System.Windows.Forms.ComboBox UNIDAD;
        private DevComponents.DotNetBar.Controls.TextBoxX NO_IDENTIFICACION;
        private DevComponents.DotNetBar.Controls.TextBoxX DESCRIPCION;
        private DevComponents.DotNetBar.Controls.TextBoxX IMPORTE;
        private DevComponents.DotNetBar.Controls.TextBoxX VALOR_UNITARIO;
        private DevComponents.DotNetBar.Controls.TextBoxX CANTIDAD;
        private DevComponents.DotNetBar.LabelX label4;
        private DevComponents.DotNetBar.LabelX label3;
        private DevComponents.DotNetBar.LabelX label2;
        private DevComponents.DotNetBar.LabelX label1;
        private DevComponents.DotNetBar.LabelX labelX1;
        private DevComponents.DotNetBar.Controls.TextBoxX fraccionArancelaria;
        private DevComponents.DotNetBar.LabelX labelX2;
        private DevComponents.DotNetBar.ButtonX buttonX5;
        private DevComponents.DotNetBar.LabelX labelX3;
        private DevComponents.DotNetBar.Controls.TextBoxX aduana;
        private DevComponents.DotNetBar.LabelX labelX4;
        private DevComponents.DotNetBar.Controls.TextBoxX fecha;
        private DevComponents.DotNetBar.Controls.TextBoxX numero;
        private DevComponents.DotNetBar.LabelX labelX5;
    }
}