﻿namespace FE_FX.CFDI
{
    partial class ucRetencionPagoExtranjero
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.labelX24 = new DevComponents.DotNetBar.LabelX();
            this.labelX23 = new DevComponents.DotNetBar.LabelX();
            this.labelX22 = new DevComponents.DotNetBar.LabelX();
            this.DescripcionConcepto = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.labelX1 = new DevComponents.DotNetBar.LabelX();
            this.PaisDeResidParaEfecFisc = new DevComponents.DotNetBar.Controls.ComboBoxEx();
            this.ConceptoPago = new DevComponents.DotNetBar.Controls.ComboBoxEx();
            this.comboItem1 = new DevComponents.Editors.ComboItem();
            this.comboItem2 = new DevComponents.Editors.ComboItem();
            this.comboItem3 = new DevComponents.Editors.ComboItem();
            this.comboItem4 = new DevComponents.Editors.ComboItem();
            this.comboItem5 = new DevComponents.Editors.ComboItem();
            this.comboItem6 = new DevComponents.Editors.ComboItem();
            this.comboItem7 = new DevComponents.Editors.ComboItem();
            this.comboItem8 = new DevComponents.Editors.ComboItem();
            this.comboItem9 = new DevComponents.Editors.ComboItem();
            this.labelX21 = new DevComponents.DotNetBar.LabelX();
            this.esBeficiarioCobro = new DevComponents.DotNetBar.Controls.ComboBoxEx();
            this.comboItem10 = new DevComponents.Editors.ComboItem();
            this.comboItem11 = new DevComponents.Editors.ComboItem();
            this.SuspendLayout();
            // 
            // labelX24
            // 
            this.labelX24.BackColor = System.Drawing.Color.Transparent;
            this.labelX24.ForeColor = System.Drawing.Color.Black;
            this.labelX24.Location = new System.Drawing.Point(104, 29);
            this.labelX24.Name = "labelX24";
            this.labelX24.Size = new System.Drawing.Size(68, 20);
            this.labelX24.TabIndex = 6;
            this.labelX24.Text = "Descripción";
            // 
            // labelX23
            // 
            this.labelX23.BackColor = System.Drawing.Color.Transparent;
            this.labelX23.ForeColor = System.Drawing.Color.Black;
            this.labelX23.Location = new System.Drawing.Point(3, 57);
            this.labelX23.Name = "labelX23";
            this.labelX23.Size = new System.Drawing.Size(95, 20);
            this.labelX23.TabIndex = 4;
            this.labelX23.Text = "Concepto de Pago";
            // 
            // labelX22
            // 
            this.labelX22.BackColor = System.Drawing.Color.Transparent;
            this.labelX22.ForeColor = System.Drawing.Color.Black;
            this.labelX22.Location = new System.Drawing.Point(2, 29);
            this.labelX22.Name = "labelX22";
            this.labelX22.Size = new System.Drawing.Size(27, 20);
            this.labelX22.TabIndex = 2;
            this.labelX22.Text = "País";
            // 
            // DescripcionConcepto
            // 
            this.DescripcionConcepto.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.DescripcionConcepto.Border.Class = "TextBoxBorder";
            this.DescripcionConcepto.ForeColor = System.Drawing.Color.Black;
            this.DescripcionConcepto.Location = new System.Drawing.Point(178, 29);
            this.DescripcionConcepto.Name = "DescripcionConcepto";
            this.DescripcionConcepto.Size = new System.Drawing.Size(479, 20);
            this.DescripcionConcepto.TabIndex = 7;
            // 
            // labelX1
            // 
            this.labelX1.BackColor = System.Drawing.Color.Transparent;
            this.labelX1.ForeColor = System.Drawing.Color.Black;
            this.labelX1.Location = new System.Drawing.Point(3, 3);
            this.labelX1.Name = "labelX1";
            this.labelX1.Size = new System.Drawing.Size(94, 20);
            this.labelX1.TabIndex = 0;
            this.labelX1.Text = "Pago al extranjero";
            // 
            // PaisDeResidParaEfecFisc
            // 
            this.PaisDeResidParaEfecFisc.DisplayMember = "Text";
            this.PaisDeResidParaEfecFisc.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.PaisDeResidParaEfecFisc.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.PaisDeResidParaEfecFisc.ForeColor = System.Drawing.Color.Black;
            this.PaisDeResidParaEfecFisc.FormattingEnabled = true;
            this.PaisDeResidParaEfecFisc.ItemHeight = 16;
            this.PaisDeResidParaEfecFisc.Location = new System.Drawing.Point(34, 27);
            this.PaisDeResidParaEfecFisc.Name = "PaisDeResidParaEfecFisc";
            this.PaisDeResidParaEfecFisc.Size = new System.Drawing.Size(62, 22);
            this.PaisDeResidParaEfecFisc.TabIndex = 3;
            // 
            // ConceptoPago
            // 
            this.ConceptoPago.DisplayMember = "Text";
            this.ConceptoPago.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.ConceptoPago.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ConceptoPago.ForeColor = System.Drawing.Color.Black;
            this.ConceptoPago.FormattingEnabled = true;
            this.ConceptoPago.ItemHeight = 16;
            this.ConceptoPago.Items.AddRange(new object[] {
            this.comboItem1,
            this.comboItem2,
            this.comboItem3,
            this.comboItem4,
            this.comboItem5,
            this.comboItem6,
            this.comboItem7,
            this.comboItem8,
            this.comboItem9});
            this.ConceptoPago.Location = new System.Drawing.Point(104, 55);
            this.ConceptoPago.Name = "ConceptoPago";
            this.ConceptoPago.Size = new System.Drawing.Size(553, 22);
            this.ConceptoPago.TabIndex = 5;
            // 
            // comboItem1
            // 
            this.comboItem1.Text = "1 Artistas, deportistas y espectaculos publicos";
            // 
            // comboItem2
            // 
            this.comboItem2.Text = "2 Otras personas fisicas";
            // 
            // comboItem3
            // 
            this.comboItem3.Text = "3 Persona moral";
            // 
            // comboItem4
            // 
            this.comboItem4.Text = "4 Fideicomiso";
            // 
            // comboItem5
            // 
            this.comboItem5.Text = "5 Asociacio en participacion";
            // 
            // comboItem6
            // 
            this.comboItem6.Text = "6 Organizaciones Internacionales o de gobierno";
            // 
            // comboItem7
            // 
            this.comboItem7.Text = "7 Organizaciones exentas";
            // 
            // comboItem8
            // 
            this.comboItem8.Text = "8 Agentes pagadores";
            // 
            // comboItem9
            // 
            this.comboItem9.Text = "9 Otros";
            // 
            // labelX21
            // 
            this.labelX21.BackColor = System.Drawing.Color.Transparent;
            this.labelX21.ForeColor = System.Drawing.Color.Black;
            this.labelX21.Location = new System.Drawing.Point(3, 83);
            this.labelX21.Name = "labelX21";
            this.labelX21.Size = new System.Drawing.Size(135, 20);
            this.labelX21.TabIndex = 8;
            this.labelX21.Text = "¿Es beficiario del cobro?";
            // 
            // esBeficiarioCobro
            // 
            this.esBeficiarioCobro.DisplayMember = "Text";
            this.esBeficiarioCobro.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.esBeficiarioCobro.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.esBeficiarioCobro.ForeColor = System.Drawing.Color.Black;
            this.esBeficiarioCobro.FormattingEnabled = true;
            this.esBeficiarioCobro.ItemHeight = 16;
            this.esBeficiarioCobro.Items.AddRange(new object[] {
            this.comboItem10,
            this.comboItem11});
            this.esBeficiarioCobro.Location = new System.Drawing.Point(132, 82);
            this.esBeficiarioCobro.Name = "esBeficiarioCobro";
            this.esBeficiarioCobro.Size = new System.Drawing.Size(62, 22);
            this.esBeficiarioCobro.TabIndex = 9;
            // 
            // comboItem10
            // 
            this.comboItem10.Text = "SI";
            // 
            // comboItem11
            // 
            this.comboItem11.Text = "NO";
            // 
            // ucRetencionPagoExtranjero
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.esBeficiarioCobro);
            this.Controls.Add(this.labelX21);
            this.Controls.Add(this.ConceptoPago);
            this.Controls.Add(this.PaisDeResidParaEfecFisc);
            this.Controls.Add(this.labelX1);
            this.Controls.Add(this.labelX24);
            this.Controls.Add(this.labelX23);
            this.Controls.Add(this.labelX22);
            this.Controls.Add(this.DescripcionConcepto);
            this.Name = "ucRetencionPagoExtranjero";
            this.Size = new System.Drawing.Size(660, 109);
            this.ResumeLayout(false);

        }

        #endregion

        private DevComponents.DotNetBar.LabelX labelX24;
        private DevComponents.DotNetBar.LabelX labelX23;
        private DevComponents.DotNetBar.LabelX labelX22;
        public DevComponents.DotNetBar.Controls.TextBoxX DescripcionConcepto;
        private DevComponents.DotNetBar.LabelX labelX1;
        public DevComponents.DotNetBar.Controls.ComboBoxEx PaisDeResidParaEfecFisc;
        public DevComponents.DotNetBar.Controls.ComboBoxEx ConceptoPago;
        private DevComponents.Editors.ComboItem comboItem1;
        private DevComponents.Editors.ComboItem comboItem2;
        private DevComponents.Editors.ComboItem comboItem3;
        private DevComponents.Editors.ComboItem comboItem4;
        private DevComponents.Editors.ComboItem comboItem5;
        private DevComponents.Editors.ComboItem comboItem6;
        private DevComponents.Editors.ComboItem comboItem7;
        private DevComponents.Editors.ComboItem comboItem8;
        private DevComponents.Editors.ComboItem comboItem9;
        private DevComponents.DotNetBar.LabelX labelX21;
        public DevComponents.DotNetBar.Controls.ComboBoxEx esBeficiarioCobro;
        private DevComponents.Editors.ComboItem comboItem10;
        private DevComponents.Editors.ComboItem comboItem11;
    }
}
