﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;

using System.Text;
using System.Windows.Forms;
using System.Xml.Xsl;
using System.Xml;
using System.Xml.Serialization;
using System.Security.Cryptography.X509Certificates;
using System.IO;
using System.Globalization;
using System.Xml.XPath;
using System.Text.RegularExpressions;
using Ionic.Zip;
using FE_FX.EDICOM_Servicio;
using System.ServiceModel;
using DevComponents.DotNetBar.Controls;
using DevComponents.DotNetBar;
using CrystalDecisions.CrystalReports.Engine;
using com.google.zxing.qrcode;
using FE_FX.CFDI;
using CFDI33;
using Generales;
using System.Configuration;

namespace FE_FX
{
    public partial class frmProformaComplementoCE : Form
    {

        string devolver = "";
        public DataGridViewSelectedRowCollection selecionados;
        string ARCHIVO = "";
        private cEMPRESA ocEMPRESA;
        private cSERIE ocSERIE;
        private cCONEXCION oData = new cCONEXCION("");
        private cCONEXCION oData_ERP = new cCONEXCION("");
        cCERTIFICADO ocCERTIFICADO = new cCERTIFICADO();
        CFDI33.Comprobante oComprobante = new CFDI33.Comprobante();


        public frmProformaComplementoCE(string sConn)
        {
            oData = new cCONEXCION(sConn);
            oData.sConn = sConn;
            ocCERTIFICADO = new cCERTIFICADO();
            InitializeComponent();
            cargar_empresas();
            cargarFormatoImpresion();
        }

        private void cargarFormatoImpresion()
        {
            string[] filePaths = Directory.GetFiles(System.IO.Path.GetDirectoryName(Application.ExecutablePath), "*.rpt");
            FORMATO_IMPRESION.Items.Clear();
            foreach (string archivo in filePaths)
            {
                FORMATO_IMPRESION.Items.Add(System.IO.Path.GetFileName(archivo));
                FORMATO_IMPRESION.Text = System.IO.Path.GetFileName(archivo);
            }
        }

        private void cargarArchivo(string pathArchivo)
        {
            Comprobante oComprobante;
            //Pasear el comprobante
            try
            {
                XmlSerializer serializer_CFD_3 = new XmlSerializer(typeof(CFDI33.Comprobante));
                TextReader reader = new StreamReader(pathArchivo);
                oComprobante = (Comprobante)serializer_CFD_3.Deserialize(reader);
                reader.Dispose();
                reader.Close();


                       
            }
            catch (Exception e)
            {
                MessageBoxEx.Show("Cargar Comprobante: El archivo " + pathArchivo + " no es un CFDI válido.");
            }
        }

        private void cargar_empresas()
        {
            ENTITY_ID.Items.Clear();
            cEMPRESA ocEMPRESA = new cEMPRESA(oData);
            foreach (cEMPRESA registro in ocEMPRESA.todos("", true))
            {
                ENTITY_ID.Items.Add(registro);
            }
            if (ENTITY_ID.Items.Count > 0)
            {
                ENTITY_ID.SelectedIndex = 0;
            }
        }

        

        private void frmFormatos_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)27)
            {
                this.Close();
            }
            else
            {
                if ((e.KeyChar.ToString() == "<")
                                | (e.KeyChar.ToString() == ">")
                                
                                | (e.KeyChar.ToString() == "'")
                                | (e.KeyChar.ToString() == "#")
                                )
                {
                    e.Handled = true;
                }
            }
        }

        private void buttonX2_Click(object sender, EventArgs e)
        {
            frmProformaComplementoCE ofrmProformaComplementoCE = new frmProformaComplementoCE(oData.sConn);
            ofrmProformaComplementoCE.ShowDialog();
        }

        private void buttonX4_Click(object sender, EventArgs e)
        {
            frmVENDOR ofrmVENDOR = new frmVENDOR(oData_ERP.sConn);
            if (ofrmVENDOR.ShowDialog() == DialogResult.OK)
            {
                rfc.Text=(string)ofrmVENDOR.selecionados[0].Cells[2].Value;
                receptortxt.Text = (string)ofrmVENDOR.selecionados[0].Cells[1].Value;
            }
        }

        private void ENTITY_ID_SelectedIndexChanged(object sender, EventArgs e)
        {
            actualizar_conexcion();
        }

        private void actualizar_conexcion()
        {

            ocEMPRESA = ENTITY_ID.SelectedItem as cEMPRESA;
            if (ocEMPRESA != null)
            {
                EMPRESA_NOMBRE.Text = ocEMPRESA.ID;
                oData_ERP = new cCONEXCION(ocEMPRESA.TIPO, ocEMPRESA.SERVIDOR, ocEMPRESA.BD, ocEMPRESA.USUARIO_BD, ocEMPRESA.PASSWORD_BD);
            }
        }

        private void buttonX2_Click_1(object sender, EventArgs e)
        {
            nuevo();
        }

        private void nuevo()
        {
            ocSERIE = new cSERIE();
            ClearTextBoxes();
        }
        private void ClearTextBoxes()
        {
            Action<Control.ControlCollection> func = null;

            func = (controls) =>
            {
                foreach (Control control in controls)
                    if (control is TextBox)
                        (control as TextBox).Clear();
                    else
                        func(control.Controls);
            };

            func(Controls);
        }
        private void labelX12_Click(object sender, EventArgs e)
        {

        }

        private void buttonX1_Click(object sender, EventArgs e)
        {
            buscarSerie();
        }

        private void buscarSerie()
        {
            
            frmSeries oBuscador = new frmSeries(oData.sConn, "S", ocEMPRESA.ROW_ID);
            if (oBuscador.ShowDialog() == DialogResult.OK)
            {
                string ROW_ID = (string)oBuscador.selecionados[0].Cells["ROW_ID"].Value;
                string IDtr = (string)oBuscador.selecionados[0].Cells["ID"].Value;
                SERIE.Text = IDtr;
                
                ocSERIE = new cSERIE();

                ocSERIE.cargar_serie(IDtr);
                ID.Text = ocSERIE.consecutivo(ocSERIE.ROW_ID,false);
            }
        }

    
        private void buttonX5_Click(object sender, EventArgs e)
        {
            if (generar())
            {
                if(timbrar())
                {
                    if (ESTADO.Text != "BORRADOR")
                    {
                        ocSERIE.incrementar(ocSERIE.ROW_ID);
                        cargarTimbre(PATH.Text);
                    }
                    MessageBox.Show("Retención realizada correctamente.", Application.ProductName + "-" + Application.ProductVersion, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                    
                }
            }
        }

        private void cargarTimbre(string p)
        {
            XmlSerializer serializer_CFD_3 = new XmlSerializer(typeof(Retenciones));
            TextReader reader = new StreamReader(p);
            Retenciones oComprobante;
            try
            {
                oComprobante = (Retenciones)serializer_CFD_3.Deserialize(reader);
                reader.Dispose();
                reader.Close();
            }
            catch
            {

            }
        }

        private bool timbrar()
        {
            //Timbrar
            informacion.Text = "Timbrando el XML " + ID.Text;
            bool usar_edicom = false;
            if(ESTADO.Text.Contains("BORRADOR")){
                usar_edicom = true;
            }
            try
            {
                if (usar_edicom)
                {
                    usar_edicom = this.generar_CFDI(2, Globales.oCFDI_USUARIO.USUARIO, Globales.oCFDI_USUARIO.PASSWORD);
                }
                else
                {
                    usar_edicom = this.generar_CFDI(1, Globales.oCFDI_USUARIO.USUARIO, Globales.oCFDI_USUARIO.PASSWORD);
                }
            }
            catch (Exception ex) {
                MessageBox.Show("Error timbrando en EDICOM" + Environment.NewLine + ex.InnerException.ToString(), Application.ProductVersion, MessageBoxButtons.OK);
                return false;
            }
            
            if (!usar_edicom)
            {
                ////Actualizar el Status
                //DialogResult Resultado = MessageBox.Show("La factura " + ID.Text + " no fue timbrada, Desea Imprimirla?", Application.ProductName + "-" + Application.ProductVersion, MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                //if (Resultado.ToString() == "Yes")
                //{
                //    usar_edicom = true;
                //}
                return false;
            }

            cargarTimbreFiscal();

            return true;
        }

        private void cargarTimbreFiscal()
        {
            XmlDocument doc = new XmlDocument();
            doc.Load(PATH.Text);
            XmlNodeList TimbreFiscalDigital = doc.GetElementsByTagName("tfd:TimbreFiscalDigital");
            if (TimbreFiscalDigital != null)
            {
                UUID.Text = TimbreFiscalDigital[0].Attributes["UUID"].Value;
            }
            
        }

        public bool generar_CFDI(int Opcion, string EDICOM_USUARIO, string EDICOM_PASSWORD)
        {
            try
            {
                //Comprimir Archivo
                string folioCompleto = oComprobante.Serie + oComprobante.Folio;
                string Archivo_Tmp = folioCompleto + ".xml";
                File.Copy(ARCHIVO, Archivo_Tmp, true);
                string ARCHIVO_zip = Archivo_Tmp.ToUpper().Replace("XML", "ZIP");
                using (ZipFile zip = new ZipFile())
                {
                    zip.AddFile(Archivo_Tmp);
                    zip.Save(ARCHIVO_zip);
                }
                //Fin de Compresion de Archivo
                File.Delete(Archivo_Tmp);

                byte[] ARCHIVO_Leido = ReadBinaryFile(ARCHIVO_zip);
                File.Delete(ARCHIVO_zip);
                CFDiClient oCFDiClient = new CFDiClient();

                byte[] PROCESO;
                string MENSAJE = "";

                switch (Opcion)
                {
                    case 1:
                        try
                        {
                            informacion.Text = "Generando CFDI ";
                            MENSAJE = "";
                            PROCESO = oCFDiClient.getCfdiRetenciones(EDICOM_USUARIO, EDICOM_PASSWORD, ARCHIVO_Leido);
                            guardar_archivo(PROCESO, folioCompleto, ARCHIVO);
                        }
                        catch (FaultException oException)
                        {

                            CFDiException oCFDiException = new CFDiException();
                            MessageBox.Show(oException.Message, Application.ProductName + "-" + Application.ProductVersion, MessageBoxButtons.OK, MessageBoxIcon.Error);
                            return false;
                        }

                        break;
                    case 2:
                        try
                        {
                            informacion.Text = "Generando CFDI Test ";
                            MENSAJE = " TEST ";
                            PROCESO = oCFDiClient.getCfdiRetencionesTest(EDICOM_USUARIO, EDICOM_PASSWORD, ARCHIVO_Leido);
                            guardar_archivo(PROCESO, folioCompleto, ARCHIVO);
                        }
                        catch (FaultException oException)
                        {

                            CFDiException oCFDiException = new CFDiException();
                            MessageBox.Show(oException.Message, Application.ProductName + "-" + Application.ProductVersion, MessageBoxButtons.OK, MessageBoxIcon.Error);
                            informacion.Text = "Error " + DateTime.Now.ToShortDateString() + " " + DateTime.Now.ToLongTimeString();
                            return false;
                        }
                        break;
                    case 3:
                        try
                        {
                            informacion.Text = "Generando Timbre CFDI  ";
                            PROCESO = oCFDiClient.getTimbreCfdi(EDICOM_USUARIO, EDICOM_PASSWORD, ARCHIVO_Leido);
                        }
                        catch (Exception oException)
                        {

                            CFDiException oCFDiException = new CFDiException();
                            MessageBox.Show(oException.Message, Application.ProductName + "-" + Application.ProductVersion, MessageBoxButtons.OK, MessageBoxIcon.Error);
                            informacion.Text = "Error " + DateTime.Now.ToShortDateString() + " " + DateTime.Now.ToLongTimeString();
                            return false;
                        }
                        break;
                    case 4:
                        try
                        {
                            informacion.Text = "Generando Timbre CFDI Test ";
                            PROCESO = oCFDiClient.getTimbreCfdiTest(EDICOM_USUARIO, EDICOM_PASSWORD, ARCHIVO_Leido);
                        }
                        catch (Exception oException)
                        {

                            CFDiException oCFDiException = new CFDiException();
                            MessageBox.Show(oException.Message, Application.ProductName + "-" + Application.ProductVersion, MessageBoxButtons.OK, MessageBoxIcon.Error);
                            informacion.Text = "Error " + DateTime.Now.ToShortDateString() + " " + DateTime.Now.ToLongTimeString();
                            return false;
                        }
                        break;
                }

                informacion.Text = "Generado y Sellado";
                return true;
            }
            catch (Exception oException)
            {
                MessageBox.Show("Generando CFDI " + oException.Message, Application.ProductName + "-" + Application.ProductVersion, MessageBoxButtons.OK, MessageBoxIcon.Error);
                informacion.Text = "Error " + DateTime.Now.ToShortDateString() + " " + DateTime.Now.ToLongTimeString();
                return false;
            }
        }

        private void guardar_archivo(byte[] PROCESO, string INVOICE_ID, string ARCHIVO)
        {
            //Guardar los PROCESO

            File.WriteAllBytes("TEMPORAL.ZIP", PROCESO);
            //Descomprimir
            String TargetDirectory = "TEMPORAL";
            using (Ionic.Zip.ZipFile zip = Ionic.Zip.ZipFile.Read("TEMPORAL.ZIP"))
            {
                zip.ExtractAll(TargetDirectory, Ionic.Zip.ExtractExistingFileAction.OverwriteSilently);
            }

            //Copiar el ARCHIVO Descrompimido por otro
            string ARCHIVO_descomprimido = TargetDirectory + @"\" + "SIGN_" + INVOICE_ID + ".XML";
            
            File.Copy(ARCHIVO_descomprimido, ARCHIVO, true);
            PATH.Text = ARCHIVO;
            File.Delete(ARCHIVO_descomprimido);
            File.Delete("TEMPORAL.ZIP");

        }



        private bool FileExists(string pPath)
        {
            string sPath = pPath;
            try
            {
                if (File.Exists(sPath))
                {
                    return true;
                }
                else
                {
                    return false;
                }

            }
            catch
            {
                return false;
            }
        }

        public string validacion(string cadena)
        {
            string devolucion = "";
            devolucion = cadena.ToUpper();

            devolucion = devolucion.Replace("á", "A");
            devolucion = devolucion.Replace("é", "E");
            devolucion = devolucion.Replace("í", "I");
            devolucion = devolucion.Replace("ó", "O");
            devolucion = devolucion.Replace("ú", "U");
            devolucion = devolucion.Replace("Á", "A");
            devolucion = devolucion.Replace("É", "E");
            devolucion = devolucion.Replace("Í", "I");
            devolucion = devolucion.Replace("Ó", "O");
            devolucion = devolucion.Replace("Ú", "U");
            devolucion = devolucion.Replace(">", "");
            devolucion = devolucion.Replace("<", "");
            //devolucion = devolucion.Replace("&", "");
            devolucion = devolucion.Replace("'", "");
            devolucion = devolucion.Replace("#", "");
            devolucion = devolucion.Replace("|", "");
            //devolucion = HtmlEncode(devolucion);

            devolucion = Regex.Replace(devolucion, @"[\u0000-\u001F]", string.Empty);


            return devolucion;
        }


        

        private byte[] ReadBinaryFile(string fileName)
        {
            if (File.Exists(fileName))
            {
                try
                {
                    ///Open and read a file&#12290;
                    FileStream fileStream = File.OpenRead(fileName);
                    byte[] archivo = ConvertStreamToByteBuffer(fileStream);
                    fileStream.Close();
                    return archivo;
                }
                catch (Exception ex)
                {
                    return new byte[0];
                }
            }
            else
            {
                return new byte[0];
            }
        }
        public byte[] ConvertStreamToByteBuffer(System.IO.Stream theStream)
        {
            int b1;
            System.IO.MemoryStream tempStream = new System.IO.MemoryStream();
            while ((b1 = theStream.ReadByte()) != -1)
            {
                tempStream.WriteByte(((byte)b1));
            }
            return tempStream.ToArray();
        }

        public bool generar()
        {
            try
            {
                //Valor por defecto del valorUnitario es de 2
                string INVOICE_ID = ID.Text;
                string sDirectory = AppDomain.CurrentDomain.BaseDirectory;
                if (ocSERIE == null)
                {
                    MessageBox.Show("Seleccione la serie", Application.ProductName + "-" + Application.ProductVersion, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                    return false;
                }
                if (String.IsNullOrEmpty(rfc.Text))
                {
                    MessageBox.Show("Seleccione el RFC ", Application.ProductName + "-" + Application.ProductVersion, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                    return false;
                }


                //Cargar Certificado
                ocCERTIFICADO = new cCERTIFICADO();
                ocCERTIFICADO.cargar(ocSERIE.ROW_ID_CERTIFICADO);
                cEMPRESA ocEMPRESA = new cEMPRESA(oData);
                ocEMPRESA.cargar(ocCERTIFICADO.ROW_ID_EMPRESA);

                //MessageBox.Show("XLT");
                if (!FileExists(sDirectory + "/XSLT/cadenaoriginal_3_3.xslt"))
                {
                    MessageBox.Show("No se tiene acceso al archivo cadenaoriginal_3_3.xslt.", Application.ProductName + "-" + Application.ProductVersion, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);

                    return false;
                }




                //Llenado de proforma con Complemento CEE

               


                if (!File.Exists(ocCERTIFICADO.CERTIFICADO))
                {
                    MessageBox.Show("No se tiene acceso al archivo " + ocCERTIFICADO.CERTIFICADO + "", Application.ProductName + "-" + Application.ProductVersion, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                    return false;
                }
                X509Certificate cert = X509Certificate.CreateFromCertFile(ocCERTIFICADO.CERTIFICADO);
                string NoSerie = cert.GetSerialNumberString();

                StringBuilder SerieHex = new StringBuilder();
                for (int i = 0; i <= NoSerie.Length - 2; i += 2)
                {
                    SerieHex.Append(Convert.ToString(Convert.ToChar(Int32.Parse(NoSerie.Substring(i, 2), System.Globalization.NumberStyles.HexNumber))));
                }
                //MessageBox.Show("Comprobante");
                string NO_CERTIFICADO = SerieHex.ToString();

                DateTime INVOICE_DATE = DateTime.Now;
                IFormatProvider culture = new CultureInfo("es-MX", true);
                DateTime CREATE_DATE = DateTime.Now;

                INVOICE_DATE = DateTime.Parse(INVOICE_DATE.Day.ToString() + "/" + INVOICE_DATE.Month.ToString() + "/" + INVOICE_DATE.Year.ToString() + " " + CREATE_DATE.ToString("HH:mm:ss"), culture);


                oComprobante = new CFDI33.Comprobante();
                cert = X509Certificate.CreateFromCertFile(ocCERTIFICADO.CERTIFICADO);
                string Certificado64 = System.Convert.ToBase64String(cert.GetRawCertData());
                oComprobante.Certificado = Certificado64;

                var date = DateTime.Now;

                date = new DateTime(date.Year, date.Month, date.Day, date.Hour, date.Minute, date.Second, date.Kind);
                
                cGeneracion ocGeneracion = new cGeneracion(oData, oData_ERP);
                string DIRECTORIO_ARCHIVOS = "";
                DIRECTORIO_ARCHIVOS += ocSERIE.DIRECTORIO + @"\" + ocSERIE.ID + @"\" + ocGeneracion.Fechammmyy(oComprobante.Fecha.ToString());
                string XML = DIRECTORIO_ARCHIVOS;

                string PDF = XML + @"\" + "pdf";
                if (!Directory.Exists(PDF))
                {
                    try
                    {

                        Directory.CreateDirectory(PDF);
                    }
                    catch (IOException excep)
                    {
                        MessageBox.Show(excep.Message.ToString() + Environment.NewLine + "Creando directorio de PDF"
                            , Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error);
                        return false;
                    }
                }

                XML += @"\" + "xml";
                if (Directory.Exists(XML) == false)
                    Directory.CreateDirectory(XML);

                string XML_Archivo = "";

                string nombre_archivo = ocSERIE.ID + INVOICE_ID;

                if (Directory.Exists(DIRECTORIO_ARCHIVOS))
                {
                    XML_Archivo = XML + @"\" + nombre_archivo + ".xml";
                }
                else
                {
                    XML_Archivo = @"\" + nombre_archivo + ".xml"; ;
                }

                XML_Archivo = XML + @"\" + nombre_archivo + ".xml";

                generarArchivoXML(XML_Archivo);

                
                //ReplaceInFile(ARCHIVO, " xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\"", "");

                //Generar Cadena
                string cadena = cadenaRetencion(ARCHIVO);
                oComprobante.Sello = ocGeneracion.SelloRSA(ocCERTIFICADO.LLAVE, ocCERTIFICADO.PASSWORD, cadena, decimal.Parse(fecha.Value.Year.ToString()));
                generarArchivoXML(XML_Archivo);


                return true;
            }
            catch (Exception errores)
            {
                MessageBox.Show("Error en generación. " + errores.Message.ToString() + Environment.NewLine + errores.InnerException.Message.ToString());
                return false;
            }
        }

        private void generarArchivoXML(string XML_Archivo)
        {
            XmlSerializer serializerXML = new XmlSerializer(typeof(Retenciones));
            XmlSerializerNamespaces myNamespacesXML = new XmlSerializerNamespaces();
            myNamespacesXML.Add("retenciones", "http://www.sat.gob.mx/esquemas/retencionpago/1");
            myNamespacesXML.Add("pagosaextranjeros", "http://www.sat.gob.mx/esquemas/retencionpago/1/pagosaextranjeros");
            XmlDocument docRetencion = new XmlDocument();
            using (XmlWriter writerRetencion = docRetencion.CreateNavigator().AppendChild())
            {
                serializerXML.Serialize(writerRetencion, oComprobante);
            }
            TextWriter writerXML = new StreamWriter(XML_Archivo);
            serializerXML.Serialize(writerXML, oComprobante, myNamespacesXML);
            writerXML.Close();
            ARCHIVO = XML_Archivo;
            PATH.Text = ARCHIVO;
            ReplaceInFile(ARCHIVO, "d1p1", "xsi");
            //xmlns:pagosaextranjeros
            ReplaceInFile(ARCHIVO, "Pagosaextranjeros", "pagosaextranjeros:Pagosaextranjeros");
            ReplaceInFile(ARCHIVO, "NoBeneficiario", "pagosaextranjeros:NoBeneficiario");

        }


        private string cadenaRetencion(string ARCHIVO)
        {
            //Cargar el XML
            StreamReader reader = new StreamReader(ARCHIVO);
            XPathDocument myXPathDoc = new XPathDocument(reader);

            //Cargando el XSLT
            string sDirectory = AppDomain.CurrentDomain.BaseDirectory;
            XslCompiledTransform myXslTrans = new XslCompiledTransform();
            //Cargar xslt
            try
            {
                //Version 3
                myXslTrans.Load(sDirectory + "/XSLT/cadenaoriginal_3_3.xslt");
            }
            catch (XmlException errores)
            {
                ErrorFX.mostrar(errores, true, true, "generar33 - 961 ", false);
                return null;
            }

            StringWriter str = new StringWriter();
            XmlTextWriter myWriter = new XmlTextWriter(str);

            //Aplicando transformacion
            myXslTrans.Transform(myXPathDoc, null, myWriter);

            //Resultado
            string result = str.ToString();

            return result;
        }

        public string cadenaOriginalPagosextranjeros(Pagosaextranjeros oPagosaextranjeros)
        {
            //Crear Cadena
            //Cargar xslt
            string sDirectory = AppDomain.CurrentDomain.BaseDirectory;
            string xslt = sDirectory + "/XSLT/pagosaextranjeros.xslt";
            XslCompiledTransform myXslTrans = new XslCompiledTransform();
            string nombre_tmp = "TMP" + DateTime.Now.ToString("ddMMyyyyhhmmss");
            try
            {
                //Version 3
                myXslTrans.Load(xslt);
            }
            catch (XmlException errores)
            {
                MessageBox.Show("Error en " + errores.InnerException.ToString());
                return "";
            }
            XPathDocument myXPathDoc;
            try
            {
                XmlSerializer serializer = new XmlSerializer(typeof(Pagosaextranjeros));
                XmlSerializerNamespaces myNamespaces = new XmlSerializerNamespaces();
                myNamespaces.Add("pagosaextranjeros", "http://www.sat.gob.mx/esquemas/retencionpago/1/pagosaextranjeros");
                TextWriter writer = new StreamWriter(nombre_tmp + ".XML");
                serializer.Serialize(writer, oPagosaextranjeros, myNamespaces);
                writer.Close();
                ReplaceInFile(nombre_tmp + ".XML", "Pagosaextranjeros", "pagosaextranjeros:Pagosaextranjeros");
                ReplaceInFile(nombre_tmp + ".XML", "NoBeneficiario", "pagosaextranjeros:NoBeneficiario");

                myXPathDoc = new XPathDocument(nombre_tmp + ".XML");
                //XslTransform myXslTrans = new XslTransform();
                myXslTrans = new XslCompiledTransform();
                myXslTrans.Load(xslt);
                XmlUrlResolver resolver = new XmlUrlResolver();

                resolver.Credentials = System.Net.CredentialCache.DefaultCredentials;
                XsltSettings settings = new XsltSettings(true, true);
            }
            catch (XmlException errores)
            {
                MessageBox.Show("Error en " + errores.InnerException.ToString());
                return "";
            }

            XmlTextWriter myWriter = new XmlTextWriter(nombre_tmp + ".TXT", null);

            //Transformar al XML
            myXslTrans.Transform(myXPathDoc, null, myWriter);

            myWriter.Close();

            //<pagosaextranjeros:Pagosaextranjeros Version="1.0" EsBenefEfectDelCobro="SI">
            //<pagosaextranjeros:NoBeneficiario PaisDeResidParaEfecFisc="IT" ConceptoPago="6" DescripcionConcepto="ESTA ES LA DESCRIPCION"/>
            //<pagosaextranjeros:Beneficiario RFC="AAAA010101AA0" CURP="AAAA010101HJCXXX01" NomDenRazSocB="PRUEBA DE PERSONA FISICA" ConceptoPago="6" DescripcionConcepto="ESTA ES LA DESCRIPCION"/>
            //</pagosaextranjeros:Pagosaextranjeros>

            //xmlns:pagosaextranjeros


            //Abrir Archivo TXT
            string CADENA_ORIGINAL = "";
            StreamReader re = File.OpenText(nombre_tmp + ".TXT");
            string input = null;
            while ((input = re.ReadLine()) != null)
            {
                CADENA_ORIGINAL += input;
            }
            re.Close();

            //Eliminar Archivos Temporales
            File.Delete(nombre_tmp + ".XML");

            //Buscar el &amp; en la Cadena y sustituirlo por &
            CADENA_ORIGINAL = CADENA_ORIGINAL.Replace("&amp;", "&");


            return CADENA_ORIGINAL;
        }

        public void ReplaceInFile(string filePath, string searchText, string replaceText)
        {
            StreamReader reader = new StreamReader(filePath);
            string content = reader.ReadToEnd();
            reader.Close();

            content = Regex.Replace(content, searchText, replaceText);

            StreamWriter writer = new StreamWriter(filePath);
            writer.Write(content);
            writer.Close();
        }

        private void labelX13_Click(object sender, EventArgs e)
        {

        }

        private void buttonX18_Click(object sender, EventArgs e)
        {
            verArchivo(PATH.Text);
        }

        private void buttonX17_Click(object sender, EventArgs e)
        {
            verArchivo(PATH.Text);
        }

        private void verArchivo(string archivo)
        {
            string directorio = AppDomain.CurrentDomain.BaseDirectory;
                frmFacturaXML oObjeto = new frmFacturaXML(archivo);
                oObjeto.Show();
        }

        private void buttonX7_Click(object sender, EventArgs e)
        {
            imprimir();
        }

        private void imprimir()
        {
            ReportDocument oReport = new ReportDocument();
            FORMATO_IMPRESION.Text = "RETENCION.RPT";
            string FORMATO = FORMATO_IMPRESION.Text;
            if (!File.Exists(FORMATO))
            {
                MessageBoxEx.Show("El Reporte " + FORMATO + " no existe en su directorio. La retencion no fue Generada.", Application.ProductName + "-" + Application.ProductVersion, MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            if (!File.Exists(PATH.Text))
            {
                MessageBoxEx.Show("La Retención no existe en su directorio. La retencion no fue Generada.", Application.ProductName + "-" + Application.ProductVersion, MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            oReport.Load(FORMATO);
            DataSet oDataSet = new DataSet();
            byte[] image = generarImagen(PATH.Text);
            oDataSet.ReadXml(PATH.Text);

            dbImageb.dtImagenDataTable oDATOS = new dbImageb.dtImagenDataTable();
            oDATOS.AdddtImagenRow(image);

            oReport.SetDataSource(oDataSet);
            if (oReport.Database.Tables.Count > 0)
            {
                //oReport.SetDataSource(oDataSet);
                foreach (CrystalDecisions.CrystalReports.Engine.Table tabla in oReport.Database.Tables)
                {
                    if (tabla.Name.ToString() == "dtImagen")
                    {
                        oReport.Database.Tables["dtImagen"].SetDataSource((System.Data.DataTable)oDATOS);
                    }
                    if (tabla.Name.ToString() == "Emisor")
                    {
                        oReport.Database.Tables["Emisor"].SetDataSource(oDataSet.Tables["retenciones:Emisor"]);
                    }
                    if (tabla.Name.ToString() == "ImpRetenidos")
                    {
                        oReport.Database.Tables["ImpRetenidos"].SetDataSource(oDataSet.Tables["retenciones:ImpRetenidos"]);
                    }
                    if (tabla.Name.ToString() == "NoBeneficiario")
                    {
                        oReport.Database.Tables["NoBeneficiario"].SetDataSource(oDataSet.Tables["retenciones:NoBeneficiario"]);
                    }
                    if (tabla.Name.ToString() == "Periodo")
                    {
                        oReport.Database.Tables["Periodo"].SetDataSource(oDataSet.Tables["retenciones:Periodo"]);
                    }
                    if (tabla.Name.ToString() == "Retenciones")
                    {
                        oReport.Database.Tables["Retenciones"].SetDataSource(oDataSet.Tables["retenciones:Retenciones"]);
                    }
                    if (tabla.Name.ToString() == "TimbreFiscal")
                    {
                        oReport.Database.Tables["TimbreFiscal"].SetDataSource(oDataSet.Tables["retenciones:TimbreFiscal"]);
                    }
                    if (tabla.Name.ToString() == "Totales")
                    {
                        oReport.Database.Tables["Totales"].SetDataSource(oDataSet.Tables["retenciones:Totales"]);
                    }
                }
            }
            frmCFDIImpresion ofrmReporte = new frmCFDIImpresion(oReport);
            ofrmReporte.ShowDialog();
        }
        private byte[] generarImagen(string archivo)
        {
            //?re=XAXX010101000&rr=XAXX010101000&tt=1234567890.123456&id=ad662d33-6934-459c-a128-BDf0393f0f44
            string QR_Code = "";
            XmlSerializer serializer_CFD_3 = new XmlSerializer(typeof(CFDI33.Comprobante));
            TextReader reader = new StreamReader(archivo);
            CFDI33.Comprobante oComprobanteLocal;
            try
            {
                XmlDocument doc = new XmlDocument();
                doc.Load(archivo);


                XmlNodeList TimbreFiscalDigital = doc.GetElementsByTagName("tfd:TimbreFiscalDigital");

                string UUID = "";
                UUID = TimbreFiscalDigital[0].Attributes["UUID"].Value;

                oComprobanteLocal = (CFDI33.Comprobante)serializer_CFD_3.Deserialize(reader);
                QR_Code += "?re=" + oComprobanteLocal.Emisor.Rfc;
                QR_Code += "&rr=" + oComprobanteLocal.Receptor.Rfc;
                QR_Code += "&tt=" + oComprobanteLocal.Total.ToString();
                QR_Code += "&id=" + UUID;

            }
            catch (Exception xmlex)
            {
                Bitacora.Log("Errpr :: " + xmlex.ToString());
                return null;
            }
            int size = 320;
            QRCodeWriter writer = new QRCodeWriter();
            com.google.zxing.common.ByteMatrix matrix;
            matrix = writer.encode(QR_Code, com.google.zxing.BarcodeFormat.QR_CODE, size, size, null);
            Bitmap img = new Bitmap(size, size);
            Color Color = Color.FromArgb(0, 0, 0);

            for (int y = 0; y < matrix.Height; ++y)
            {
                for (int x = 0; x < matrix.Width; ++x)
                {
                    Color pixelColor = img.GetPixel(x, y);

                    //Find the colour of the dot
                    if (matrix.get_Renamed(x, y) == -1)
                    {
                        img.SetPixel(x, y, Color.White);
                    }
                    else
                    {
                        img.SetPixel(x, y, Color.Black);
                    }
                }
            }


            string nombre = Path.GetTempPath() + "QRP_TEMP.BMP";
            img.Save(nombre);

            FileStream fs = new FileStream(nombre, System.IO.FileMode.Open, System.IO.FileAccess.Read);
            byte[] Image = new byte[fs.Length];
            fs.Read(Image, 0, Convert.ToInt32(fs.Length));
            int image_32 = Convert.ToInt32(fs.Length);
            fs.Close();
            File.Delete(nombre);

            return Image;

        }

        private void buttonX9_Click(object sender, EventArgs e)
        {
            cancelar();
        }

        private void cancelar()
        {
            //Llamar el Servicio
            CFDiClient oCFDiClient = new CFDiClient();
            string[] uuid = new string[1];
            uuid[0] = UUID.Text;

            //Cargar el archivo pfx creado desde OpenSSL
            string pfx = ocCERTIFICADO.PFX;
            if (!File.Exists(pfx))
            {
                //Validar el directorio

                string directorio = AppDomain.CurrentDomain.BaseDirectory;
                pfx = directorio + @"\" + ocCERTIFICADO.PFX;
                if (!File.Exists(pfx))
                {
                    MessageBox.Show("El PFX " + ocCERTIFICADO.PFX + " no existe."
                        , Application.ProductName + "-" + Application.ProductVersion, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                    return;
                }
            }
            byte[] pfx_archivo = ReadBinaryFile(pfx);
            try
            {
                CancelaResponse respuesta;
                //Todo: Realizar cancelacion
                //respuesta = oCFDiClient.cancelaCFDi(Globales.oCFDI_USUARIO.USUARIO, Globales.oCFDI_USUARIO.PASSWORD, ocEMPRESA.RFC, uuid, pfx_archivo, ocCERTIFICADO.PASSWORD);
                return;
            }
            catch (FaultException oException)
            {

                CFDiException oCFDiException = new CFDiException();
                MessageBox.Show(oException.Message, Application.ProductName + "-" + Application.ProductVersion, MessageBoxButtons.OK, MessageBoxIcon.Error);
                informacion.Text = "Error: Factura NO CANCELADA " + DateTime.Now.ToShortDateString() + " " + DateTime.Now.ToLongTimeString();
                return;
            }
        }

        private void labelX2_Click(object sender, EventArgs e)
        {

        }
    }
}
