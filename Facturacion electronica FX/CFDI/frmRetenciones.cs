﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Text;
using System.Windows.Forms;
using System.Xml.Serialization;
using Generales;
using System.Xml;
using FE_FX.EDICOM_Servicio;
using System.ServiceModel;
using FE_FX.CFDI;
using FE_FX.Clases;

namespace FE_FX
{
    public partial class frmRetenciones : Form
    {
        private cCONEXCION oData = new cCONEXCION("");
        string devolver = "";
        public DataGridViewSelectedRowCollection selecionados;
        private cCONEXCION oData_ERP = new cCONEXCION("");
        string tipo = "";
        private cEMPRESA ocEMPRESA;
        private cSERIE ocSERIE;

        public frmRetenciones(string sConn)
        {
            oData.sConn = sConn;
            InitializeComponent();
            cargar_empresas();
        }
        private void cargar_empresas()
        {
            ENTITY_ID.Items.Clear();
            cEMPRESA ocEMPRESA = new cEMPRESA(oData);
            foreach (cEMPRESA registro in ocEMPRESA.todos("", true))
            {
                ENTITY_ID.Items.Add(registro);
            }
            if (ENTITY_ID.Items.Count > 0)
            {
                ENTITY_ID.SelectedIndex = 0;
            }
        }

        private void cargar()
        {
            int n;
            cADDENDA oObjeto = new cADDENDA();
            informacion.Text = "Cargando ";
            dtgrdGeneral.Rows.Clear();
            informacion.Text = "Total " + dtgrdGeneral.Rows.Count.ToString();

        }

        private void button1_Click(object sender, EventArgs e)
        {
            cargar();
        }
        private void ENTITY_ID_SelectedIndexChanged(object sender, EventArgs e)
        {
            actualizar_conexcion();
        }

        private void actualizar_conexcion()
        {

            ocEMPRESA = ENTITY_ID.SelectedItem as cEMPRESA;
            if (ocEMPRESA != null)
            {
                EMPRESA_NOMBRE.Text = ocEMPRESA.ID;
                oData_ERP = new cCONEXCION("SQLSERVER",ocEMPRESA.SERVIDOR, ocEMPRESA.BD, ocEMPRESA.USUARIO_BD, ocEMPRESA.PASSWORD_BD);
            }
        }
        private void BUSCAR_KeyPress(object sender, KeyPressEventArgs e)
        {
            //Precionar Enter buscar
            if (e.KeyChar.ToString() == "\r")
            {
                cargar();
            }
            
        }

        private void frmUsuarios_Load(object sender, EventArgs e)
        {
            cargar();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            frmAddenda oObjeto = new frmAddenda(oData.sConn);
            oObjeto.ShowDialog();
            cargar();
        }

        private void dtgrdGeneral_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            modificar();
        }

        public void modificar()
        {
            if (dtgrdGeneral.SelectedRows.Count > 0)
            {
                DataGridViewSelectedRowCollection arrSelectedRows = dtgrdGeneral.SelectedRows;
                string ARCHIVO = arrSelectedRows[0].Cells["ARCHIVO"].Value.ToString();
                string directorio = AppDomain.CurrentDomain.BaseDirectory;
                frmFacturaXML oObjeto = new frmFacturaXML(ARCHIVO);
                oObjeto.Show();
            }
        }

        private void frmFormatos_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)27)
            {
                this.Close();
            }
            else
            {
                if ((e.KeyChar.ToString() == "<")
                                | (e.KeyChar.ToString() == ">")
                                
                                | (e.KeyChar.ToString() == "'")
                                | (e.KeyChar.ToString() == "#")
                                )
                {
                    e.Handled = true;
                }
            }
        }

        private void buttonX2_Click(object sender, EventArgs e)
        {
            frmRetencion2 ofrmRetencion = new frmRetencion2(oData.sConn);
            ofrmRetencion.ShowDialog();
            cargar();
        }

        private void buttonX1_Click(object sender, EventArgs e)
        {
            cargar_archivos();
        }

        private void buttonX3_Click(object sender, EventArgs e)
        {
            buscarSerie();
        }

        private void buscarSerie()
        {

            frmSeries oBuscador = new frmSeries(oData.sConn, "S", ocEMPRESA.ROW_ID);
            if (oBuscador.ShowDialog() == DialogResult.OK)
            {
                string ROW_ID = (string)oBuscador.selecionados[0].Cells["ROW_ID"].Value;
                string IDtr = (string)oBuscador.selecionados[0].Cells["ID"].Value;
                ID.Text = IDtr;

                ocSERIE = new cSERIE();

                ocSERIE.cargar_serie(IDtr);
                ID.Text = ocSERIE.consecutivo(ocSERIE.ROW_ID, false);

                directorio.Text = ocSERIE.DIRECTORIO;
                cargar_archivos();
            }
        }

        private void cargar_archivos()
        {
            dtgrdGeneral.Rows.Clear();
            if (!Directory.Exists(directorio.Text))
            {
                return;
            }
            DataSetRetencion.dtRetencionDataTable tabla = new DataSetRetencion.dtRetencionDataTable();
            //Cargar archivos
            foreach (string archivo in Directory.GetFiles(directorio.Text, "*.xml", SearchOption.AllDirectories))
            {
                try
                {
                    XmlSerializer serializer_CFD_3 = new XmlSerializer(typeof(Retenciones));
                    TextReader reader = new StreamReader(archivo);
                    Retenciones oComprobante;
                    oComprobante = (Retenciones)serializer_CFD_3.Deserialize(reader);

                    XmlDocument docXML = new XmlDocument();
                    docXML.Load(archivo);
                    XmlNodeList TimbreFiscalDigital = docXML.GetElementsByTagName("tfd:TimbreFiscalDigital");
                    string TimbreFiscalDigitaltr = "";
                    if (TimbreFiscalDigital != null)
                    {
                        if (TimbreFiscalDigital[0] != null)
                        {
                            TimbreFiscalDigitaltr = TimbreFiscalDigital[0].Attributes["UUID"].Value;
                        }
                    }

                    tabla.AdddtRetencionRow(
                        archivo
                        , oComprobante.FolioInt
                        , DateTime.Parse(oComprobante.FechaExp)
                        , oComprobante.Emisor.RfcE
                        , oComprobante.Periodo.MesIni.ToString()
                        , oComprobante.Periodo.MesFin.ToString()
                        , oComprobante.Periodo.Ejercicio.ToString()
                        , oComprobante.Certificado
                        , TimbreFiscalDigitaltr);

                }
                catch
                {

                }
            }

            //Cargar tabla
            var dataQuery =
                        from tr in tabla
                        where tr.fecha >= INICIO.Value & tr.fecha <= FINAL.Value
                        select tr;
            foreach (DataSetRetencion.dtRetencionRow r in dataQuery)
            {
                int n = dtgrdGeneral.Rows.Add();
                dtgrdGeneral.Rows[n].Cells["archivo"].Value = r.archivo;
                dtgrdGeneral.Rows[n].Cells["folio"].Value = r.folio;
                dtgrdGeneral.Rows[n].Cells["fecha"].Value = r.fecha;
                dtgrdGeneral.Rows[n].Cells["rfcEmisor"].Value = r.rfcEmisor;
                dtgrdGeneral.Rows[n].Cells["MesIni"].Value = r.MesIni;
                dtgrdGeneral.Rows[n].Cells["MesFin"].Value = r.MesFin;
                dtgrdGeneral.Rows[n].Cells["Ejerc"].Value = r.Ejerc;
                dtgrdGeneral.Rows[n].Cells["NumCert"].Value = r.NumCert;
                dtgrdGeneral.Rows[n].Cells["UUID"].Value = r.UUID;
            }

            dtgrdGeneral.AutoResizeColumns(DataGridViewAutoSizeColumnsMode.AllCells);
        }

        private void buttonX5_Click(object sender, EventArgs e)
        {
            cargarDirectorio();
        }

        private void cargarDirectorio()
        {
            if (folderBrowserDialog1.ShowDialog() == DialogResult.OK)
            {
                directorio.Text = folderBrowserDialog1.SelectedPath;
                cargar_archivos();
            }
        }

        private void dtgrdGeneral_MouseClick(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Right)
            {
                contextMenuStrip1.Show(dtgrdGeneral, new Point(e.X, e.Y));
            }
        }

        private void cancelarToolStripMenuItem_Click(object sender, EventArgs e)
        {
            cancelarSeleccionadas();
        }
        private void cancelarSeleccionadas()
        {
            try
            {
                if (dtgrdGeneral.SelectedRows.Count > 0)
                {
                    DataGridViewSelectedRowCollection arrSelectedRows = dtgrdGeneral.SelectedRows;
                    for (int n = arrSelectedRows.Count - 1; n >= 0; n--)
                    {
                        
                        cancelar(arrSelectedRows[n].Cells["UUID"].Value.ToString()
                            , arrSelectedRows[n].Cells["NumCert"].Value.ToString(), arrSelectedRows[n].Index);
                    }
                }
            }
            catch (Exception e)
            {
                ErrorFX.mostrar(e, true, true, true);
            }
        }

        public byte[] ReadBinaryFile(string fileName)
        {
            if (File.Exists(fileName))
            {
                try
                {
                    ///Open and read a file&#12290;
                    FileStream fileStream = File.OpenRead(fileName);
                    byte[] archivo = ConvertStreamToByteBuffer(fileStream);
                    fileStream.Close();
                    return archivo;
                }
                catch (Exception ex)
                {
                    return new byte[0];
                }
            }
            else
            {
                return new byte[0];
            }
        }

        public byte[] ConvertStreamToByteBuffer(System.IO.Stream theStream)
        {
            int b1;
            System.IO.MemoryStream tempStream = new System.IO.MemoryStream();
            while ((b1 = theStream.ReadByte()) != -1)
            {
                tempStream.WriteByte(((byte)b1));
            }
            return tempStream.ToArray();
        }


        private void cancelar(string uuidp, string numeroCertificado, int indice)
        {
            //NumCert
            //Llamar el Servicio
            CFDiClient oCFDiClient = new CFDiClient();
            string[] uuid = new string[1];
            uuid[0] = uuidp;
            cCERTIFICADO ocCERTIFICADO = new cCERTIFICADO();

            string pfx=ocCERTIFICADO.getPFX(numeroCertificado);
            if (String.IsNullOrEmpty(pfx))
            {
                MessageBox.Show("El PFX no existe para el certificado " + numeroCertificado
                                        , Application.ProductName + "-" + Application.ProductVersion, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return;
            }

            //Cargar el archivo pfx creado desde OpenSSL
            
            if (!File.Exists(pfx))
            {
                //Validar el directorio

                string directorio = AppDomain.CurrentDomain.BaseDirectory;
                pfx = directorio + @"\" + ocCERTIFICADO.PFX;
                if (!File.Exists(pfx))
                {
                    MessageBox.Show("El PFX " + ocCERTIFICADO.PFX + " no existe."
                        , Application.ProductName + "-" + Application.ProductVersion, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                    return;
                }
            }
            byte[] pfx_archivo = ReadBinaryFile(pfx);
            try
            {
                CancelaResponse respuesta;
                //Todo: Realizar cancelacion
                //respuesta = oCFDiClient.cancelaCFDiRetenciones(Globales.oCFDI_USUARIO.USUARIO, cENCRIPTACION.Decrypt(Globales.oCFDI_USUARIO.PASSWORD), ocEMPRESA.RFC, uuid, pfx_archivo, ocCERTIFICADO.PASSWORD);
                return;
            }
            catch (FaultException oException)
            {

                CFDiException oCFDiException = new CFDiException();
                MessageBox.Show(oException.Message, Application.ProductName + "-" + Application.ProductVersion, MessageBoxButtons.OK, MessageBoxIcon.Error);
                informacion.Text = "Error: Factura NO CANCELADA " + DateTime.Now.ToShortDateString() + " " + DateTime.Now.ToLongTimeString();
                return;
            }
        }

        private void buttonX4_Click(object sender, EventArgs e)
        {
            cRetencion ocRetencion = new cRetencion();
            ocRetencion.imprimirXML();
        }
    }
}
