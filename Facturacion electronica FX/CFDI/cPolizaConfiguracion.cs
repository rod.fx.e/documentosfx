﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace FE_FX.CFDI
{
    [Serializable()]
    public class cPolizaConfiguracion
    {
        private string accountAbonoTotal;
        public string AccountAbonoTotal
        {
            get { return accountAbonoTotal; }
            set { accountAbonoTotal = value; }
        }

        private string accountCargoTotal;
        public string AccountCargoTotal
        {
            get { return accountCargoTotal; }
            set { accountCargoTotal = value; }
        }

        private string accountAbonoImpuesto;
        public string AccountAbonoImpuesto
        {
            get { return accountAbonoImpuesto; }
            set { accountAbonoImpuesto = value; }
        }

        private string accountCargoImpuesto;
        public string AccountCargoImpuesto
        {
            get { return accountCargoImpuesto; }
            set { accountCargoImpuesto = value; }
        }

        private string comentarioDescuento;
        public string ComentarioDescuento
        {
            get { return comentarioDescuento; }
            set { comentarioDescuento = value; }
        }

        private string directorio;
        public string Directorio
        {
            get { return directorio; }
            set { directorio = value; }
        }


        public string serieDefecto;
        public string SerieDefecto
        {
            get { return serieDefecto; }
            set { serieDefecto = value; }
        }



        public string formatoDefecto;
        public string FormatoDefecto
        {
            get { return formatoDefecto; }
            set { formatoDefecto = value; }
        }
    }
}
