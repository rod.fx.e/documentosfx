﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;

using System.Text;
using System.Windows.Forms;
using System.Xml.Xsl;
using System.Xml;
using System.Xml.Serialization;
using System.Security.Cryptography.X509Certificates;
using System.IO;
using System.Globalization;
using System.Xml.XPath;
using System.Text.RegularExpressions;
using Ionic.Zip;
using FE_FX.EDICOM_Servicio;
using System.ServiceModel;
using DevComponents.DotNetBar.Controls;
using DevComponents.DotNetBar;
using CrystalDecisions.CrystalReports.Engine;
using FE_FX.CFDI;
using com.google.zxing.qrcode;
using CrystalDecisions.Shared;
using OfficeOpenXml;
using System.Net.Mail;
using CFDI32;
using cce11_v32;
using Generales;

namespace FE_FX
{
    public partial class aplicarDescuento : Form
    {

        string devolver = "";
        public DataGridViewSelectedRowCollection selecionados;
        string ARCHIVO = "";
        private cEMPRESA ocEMPRESA;
        private cSERIE ocSERIE;
        private cCONEXCION oData = new cCONEXCION("");
        private cCONEXCION oData_ERP = new cCONEXCION("");
        cCERTIFICADO ocCERTIFICADO = new cCERTIFICADO();
        Comprobante oComprobante = new Comprobante();
        private CFDI.cPolizaConfiguracion ocPolizaConfiguracion = new CFDI.cPolizaConfiguracion();
        string sDirectory = AppDomain.CurrentDomain.BaseDirectory + "cPolizaConfiguracion";
        string sDirectoryCE = AppDomain.CurrentDomain.BaseDirectory + "cCEProveedor_";
        string ENTITY_IDlocal = "";
        int indiceSeleccionado = 0;

        public aplicarDescuento(string sConn, string ENTITY_IDp, int indiceSeleccionadop)
        {
            oData = new cCONEXCION(sConn);
            oData.sConn = sConn;
            ocCERTIFICADO = new cCERTIFICADO();
            InitializeComponent();
            
            cargar_empresas();
            ENTITY_IDlocal = ENTITY_IDp;
            indiceSeleccionado = indiceSeleccionadop;
            METODO_DE_PAGO.SelectedIndex = 0;
            //IMPUESTO_DATO.SelectedIndex = 0;
            cargarFormatoImpresion();
            FECHA.Value = DateTime.Today;
            cargarMoneda();
            cargarIncoterm();
            cargarConfiguracion();
            cargarSeriePorDefecto();
            //Cargar empresa 
            if (ENTITY_IDp != "")
            {
                for (int i = 0; i < ENTITY_ID.Items.Count; i++)
                {
                    if (ENTITY_IDp == ENTITY_ID.Items[i].ToString())
                    {
                        ENTITY_ID.SelectedItem = ENTITY_ID.Items[i];
                    }
                }
            }
            cambiarTraslado();
        }
        private void generarPlantilla()
        {
            MessageBoxEx.Show("La configuración de  la Plantilla para migración de datos en Excel" + Environment.NewLine
            + "Si no tiene esta configuración no será cargada la información. "
            , Application.ProductName + "-" + Application.ProductVersion.ToString()
            , MessageBoxButtons.OK
            );

            var fileName = " Plantilla de Lineas " + DateTime.Now.ToString("yyyy-MM-dd-hh-mm-ss") + ".xlsx";
            if (folderBrowserDialog1.ShowDialog() == DialogResult.OK)
            {
                fileName = folderBrowserDialog1.SelectedPath + "\\" + fileName;
                System.IO.Stream myStream = new System.IO.FileStream(fileName, System.IO.FileMode.Create, System.IO.FileAccess.ReadWrite, System.IO.FileShare.None);

                using (ExcelPackage pck = new ExcelPackage())
                {

                    ExcelWorksheet ws1 = pck.Workbook.Worksheets.Add("Lineas");
                    ws1.Cells[1, 1].Value = "Cantidad";
                    ws1.Cells[1, 2].Value = "Unidad";
                    ws1.Cells[1, 3].Value = "Descripción";
                    ws1.Cells[1, 4].Value = "No Identificacion";
                    ws1.Cells[1, 5].Value = "Valor Unitario";
                    ws1.Cells[1, 6].Value = "Importe";
                    ws1.Cells[1, 7].Value = "Fracción";

                    ExcelWorksheet ws2 = pck.Workbook.Worksheets.Add("Comercio Exterior");
                    ws2.Cells[1, 1].Value = "NumRegIdTrib";
                    ws2.Cells[1, 2].Value = "Pais";
                    ws2.Cells[1, 3].Value = "Calle";
                    ws2.Cells[1, 4].Value = "No Exterior";
                    ws2.Cells[1, 5].Value = "No Interior";
                    ws2.Cells[1, 6].Value = "Estado";
                    ws2.Cells[1, 7].Value = "Código Postal";

                    pck.SaveAs(myStream);
                }
                myStream.Close();
                MessageBoxEx.Show("Creación de plantilla realizada " + Environment.NewLine + fileName + " finalizada.", Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private void cargarConfiguracion()
        {

            XmlSerializer serializer = new XmlSerializer(typeof(cPolizaConfiguracion));
            try
            {
                TextReader reader = new StreamReader(sDirectory);
                ocPolizaConfiguracion = (cPolizaConfiguracion)serializer.Deserialize(reader);
                reader.Close();
            }
            catch (Exception e)
            {

            }

        }

        private void cargarSeriePorDefecto()
        {
            ocSERIE = new cSERIE();
            if (ocPolizaConfiguracion.SerieDefecto != "")
            {
                //Validar el primer
                string serieAcargar = "";
                string[] serieTRs = ocPolizaConfiguracion.SerieDefecto.Split(',');
                if (serieTRs.Length == 0)
                {
                    serieAcargar = ocPolizaConfiguracion.SerieDefecto;
                }
                else
                {
                    int o = 0;
                    foreach (string serieTR in serieTRs)
                    {
                        if (o == indiceSeleccionado)
                        {
                            ocSERIE.cargar_serie(serieTR);
                            serieAcargar = serieTR;
                            
                            break;
                        }
                        o++;
                        
                    }
                }


                ocSERIE.cargar_serie(serieAcargar);
                ID.Text = ocSERIE.consecutivo(ocSERIE.ROW_ID, false);
                SERIE.Text = ocSERIE.ID;
            }
            if(ocPolizaConfiguracion.FormatoDefecto!=""){
                FORMATO_IMPRESION.Text = ocPolizaConfiguracion.FormatoDefecto;
            }
            
        }

        private void cargarIncoterm()
        {
            //Incoterm.Items.Clear();
            Incoterm.SelectedIndex = 0;
        }

        private void cargarFormatoImpresion()
        {
            string[] filePaths = Directory.GetFiles(System.IO.Path.GetDirectoryName(Application.ExecutablePath), "*.rpt");
            FORMATO_IMPRESION.Items.Clear();
            foreach (string archivo in filePaths)
            {
                FORMATO_IMPRESION.Items.Add(System.IO.Path.GetFileName(archivo));
                FORMATO_IMPRESION.Text = System.IO.Path.GetFileName(archivo);
            }
        }

        private void cargarArchivo(string pathArchivo)
        {
            Comprobante oComprobante;
            //Pasear el comprobante
            try
            {
                XmlSerializer serializer_CFD_3 = new XmlSerializer(typeof(Comprobante));
                TextReader reader = new StreamReader(pathArchivo);
                oComprobante = (Comprobante)serializer_CFD_3.Deserialize(reader);
                reader.Dispose();
                reader.Close();


                       
            }
            catch (Exception e)
            {
                MessageBoxEx.Show("Cargar Comprobante: El archivo " + pathArchivo + " no es un CFDI válido.");
            }
        }


        private void cargar()
        {
            int n;
            cADDENDA oObjeto = new cADDENDA();
            informacion.Text = "Cargando ";
            dtgrdGeneral.Rows.Clear();
            informacion.Text = "Total " + dtgrdGeneral.Rows.Count.ToString();

        }

        private void cargar_empresas()
        {
            ENTITY_ID.Items.Clear();
            cEMPRESA ocEMPRESA = new cEMPRESA(oData);
            foreach (cEMPRESA registro in ocEMPRESA.todos("", true))
            {
                ENTITY_ID.Items.Add(registro);
            }
            if (ENTITY_ID.Items.Count > 0)
            {
                ENTITY_ID.SelectedIndex = 0;
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            cargar();
        }

        private void BUSCAR_KeyPress(object sender, KeyPressEventArgs e)
        {
            //Precionar Enter buscar
            if (e.KeyChar.ToString() == "\r")
            {
                cargar();
            }
            
        }

        private void frmUsuarios_Load(object sender, EventArgs e)
        {
            cargar();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            frmAddenda oObjeto = new frmAddenda(oData.sConn);
            oObjeto.ShowDialog();
            cargar();
        }

        private void dtgrdGeneral_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            modificar();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            if (dtgrdGeneral.SelectedRows.Count > 0)
            {
                DataGridViewSelectedRowCollection arrSelectedRows = dtgrdGeneral.SelectedRows;
                string ROW_ID = (string)arrSelectedRows[0].Cells[0].Value;
                frmAddenda oObjeto = new frmAddenda(oData.sConn, ROW_ID);
                oObjeto.ShowDialog();
                cargar();
            }
        }

        public void modificar()
        {
            if (dtgrdGeneral.SelectedRows.Count > 0)
            {
                DataGridViewSelectedRowCollection arrSelectedRows = dtgrdGeneral.SelectedRows;
                if (devolver == "")
                {
                    
                    string ROW_ID = (string)arrSelectedRows[0].Cells[0].Value;
                    frmAddenda oObjeto = new frmAddenda(oData.sConn, ROW_ID);
                    oObjeto.ShowDialog();
                    cargar();
                }
                else
                {
                    selecionados = arrSelectedRows;
                    this.DialogResult = DialogResult.OK;
                }
            }
        }

        private void frmFormatos_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)27)
            {
                this.Close();
            }
            else
            {
                if ((e.KeyChar.ToString() == "<")
                                | (e.KeyChar.ToString() == ">")
                                
                                | (e.KeyChar.ToString() == "'")
                                | (e.KeyChar.ToString() == "#")
                                )
                {
                    e.Handled = true;
                }
            }
        }

        private void buttonX4_Click(object sender, EventArgs e)
        {
            frmVENDOR ofrmVENDOR = new frmVENDOR(oData_ERP.sConn);
            if (ofrmVENDOR.ShowDialog() == DialogResult.OK)
            {
                try
                {
                    rfc.Text=(string)ofrmVENDOR.selecionados[0].Cells[2].Value;
                    string numeroRegistro = "";
                    rfc.Text = "XEXX010101000";
                    if (ofrmVENDOR.selecionados[0].Cells[3].Value != null)
                    {
                        numeroRegistro = ofrmVENDOR.selecionados[0].Cells[3].Value.ToString();
                    }
                    if (numeroRegistro == "")
                    {
                        rfc.Text = "XEXX010101000";
                    }
                    else
                    {
                        NumRegIdTrib.Text = numeroRegistro;
                    }
                    METODO_DE_PAGO.Text = "NA";
                    
                }
                catch
                {

                }
                receptor.Text = (string)ofrmVENDOR.selecionados[0].Cells[1].Value;
                if (ofrmVENDOR.selecionados[0].Cells[3].Value != null)
                {
                    NumRegIdTrib.Text = "";
                    try
                    {
                        NumRegIdTrib.Text = (string)ofrmVENDOR.selecionados[0].Cells[3].Value;
                    }
                    catch
                    {

                    }
                }

                cargarCE();
            }
        }

        private void ENTITY_ID_SelectedIndexChanged(object sender, EventArgs e)
        {
            actualizar_conexcion();
        }

        private void actualizar_conexcion()
        {
            ocEMPRESA = ENTITY_ID.SelectedItem as cEMPRESA;
            if (ocEMPRESA != null)
            {
                EMPRESA_NOMBRE.Text = ocEMPRESA.ID;
                oData_ERP = new cCONEXCION(ocEMPRESA.TIPO, ocEMPRESA.SERVIDOR, ocEMPRESA.BD, ocEMPRESA.USUARIO_BD, ocEMPRESA.PASSWORD_BD);
                cargar();
            }
        }

        private void buttonX2_Click_1(object sender, EventArgs e)
        {
            nuevo();
        }

        private void nuevo()
        {
            dtgrdGeneral.Rows.Clear();
            SUBTOTAL.Text = decimal.Parse("0").ToString("####.00");
            TOTAL.Text = decimal.Parse("0").ToString("####.00");
            calcular();
            oComprobante = new Comprobante();
            ocSERIE = new cSERIE();
            dtgPolizas.Rows.Clear();
        }

        private void labelX12_Click(object sender, EventArgs e)
        {

        }

        private void cargarMoneda()
        {
            //if (oData_ERP != null)
            //{

            //    string sSQL = "SELECT SHORT_NAME FROM CURRENCY";
            //    DataTable oDataTable = oData_ERP.EjecutarConsulta(sSQL);
            //    if (oDataTable != null)
            //    {
            //        foreach (DataRow oDataRow1 in oDataTable.Rows)
            //        {
            //            MONEDA.Items.Add(oDataRow1["SHORT_NAME"].ToString());
            //        }
            //    }
            //}
        }

        private void buttonX1_Click(object sender, EventArgs e)
        {
            buscarSerie();
        }

        private void buscarSerie()
        {
            
            frmSeries oBuscador = new frmSeries(oData.sConn, "S", ocEMPRESA.ROW_ID);
            if (oBuscador.ShowDialog() == DialogResult.OK)
            {
                string ROW_ID = (string)oBuscador.selecionados[0].Cells["ROW_ID"].Value;
                string IDtr = (string)oBuscador.selecionados[0].Cells["ID"].Value;
                SERIE.Text = IDtr;
                
                ocSERIE = new cSERIE();

                ocSERIE.cargar_serie(IDtr);
                ID.Text = ocSERIE.consecutivo(ocSERIE.ROW_ID,false);
                ocEMPRESA = new cEMPRESA(oData);
                ocCERTIFICADO = new cCERTIFICADO();
                ocCERTIFICADO.cargar(ocSERIE.ROW_ID_CERTIFICADO);
                ocEMPRESA.cargar(ocCERTIFICADO.ROW_ID_EMPRESA);

                actualizar_conexcion();

            }
        }


        private void buttonX10_Click(object sender, EventArgs e)
        {
            insertarConcepto();
        }

        private void insertarConcepto()
        {
            frmCFDIConcepto oFacturaLinea = new frmCFDIConcepto(oData_ERP.sConn, ocEMPRESA.ROW_ID
                , oData.sConn);
            if (oFacturaLinea.ShowDialog() == DialogResult.OK)
            {
                insertar_linea(oFacturaLinea.cantidadP, oFacturaLinea.unidadP, oFacturaLinea.descripcionP
                    , oFacturaLinea.valorUnitarioP, oFacturaLinea.importeP, oFacturaLinea.noIdentificacionP
                    , oFacturaLinea.fraccionArancelariaP
                    , oFacturaLinea.aduanaP, oFacturaLinea.fechaP, oFacturaLinea.numeroP
                    );

            }
        }

        private void insertar_linea(decimal cantidad, string unidad, string descripcion
    , decimal valorUnitario, decimal importe, string noIdentificacion
            , string fraccion
            , string aduana, string fecha, string numero
    )
        {
            int n;
            n = dtgrdGeneral.Rows.Add();
            for (int i = 0; i < dtgrdGeneral.Columns.Count; i++)
            {
                dtgrdGeneral.Rows[n].Cells[i].Value = "";
            }

            dtgrdGeneral.Rows[n].Cells["cantidad"].Value = cantidad.ToString();
            dtgrdGeneral.Rows[n].Cells["unidad"].Value = unidad;
            dtgrdGeneral.Rows[n].Cells["descripcion"].Value = descripcion;
            dtgrdGeneral.Rows[n].Cells["valorUnitario"].Value = valorUnitario.ToString();
            dtgrdGeneral.Rows[n].Cells["importe"].Value = importe.ToString();
            dtgrdGeneral.Rows[n].Cells["noIdentificacion"].Value = noIdentificacion;
            dtgrdGeneral.Rows[n].Cells["fraccionArancelaria"].Value = fraccion;

            dtgrdGeneral.Rows[n].Cells["informacionAduaneraAduana"].Value = aduana;
            dtgrdGeneral.Rows[n].Cells["informacionAduaneraFecha"].Value = fecha;
            dtgrdGeneral.Rows[n].Cells["informacionAduaneraNumero"].Value = numero;
            calcular();
        }

        private void calcular()
        {
            double SUBTOTAL_tr = 0;
            double IMPUESTO_tr = 0;
            double RETENCION_tr = 0;
            double DESCUENTO_tr = 0;
            double TOTAL_tr = 0;
            for (int i = 0; i < dtgrdGeneral.Rows.Count; i++)
            {
                SUBTOTAL_tr += double.Parse(dtgrdGeneral.Rows[i].Cells["importe"].Value.ToString().Replace("$",""));
            }
            //Calculos de los IEPS
            double IEPS = double.Parse(IEPStr.Text);
            double IMPUESTO_IEPS = 0;
            switch (IMPUESTO_DATO.SelectedIndex)
            {
                case 0:
                    //IVA 16%
                    IMPUESTO_tr = SUBTOTAL_tr * 0.16;
                    IMPUESTO_IEPS = IEPS * 0.16;
                    break;
                case 1:
                    //RETENCION IVA 16%
                    IMPUESTO_tr = SUBTOTAL_tr * 0.16;
                    RETENCION_tr = IMPUESTO_tr;
                    IMPUESTO_IEPS = IEPS * 0.16;
                    break;
                case 2:
                    //0%
                    IMPUESTO_tr = SUBTOTAL_tr * 0;
                    IEPS += IEPS * 0;
                    break;
                case 3:
                    //11%
                    IMPUESTO_tr = SUBTOTAL_tr * 0.11;
                    IMPUESTO_IEPS = IEPS * 0.11;
                    break;
                case 4:
                    //RETENCION 11%
                    IMPUESTO_tr = SUBTOTAL_tr * 0.11;
                    RETENCION_tr = IMPUESTO_tr;
                    IMPUESTO_IEPS = IEPS * 0.11;
                    break;
            }
            
            DESCUENTO_tr = 0;
            if (Globales.IsNumeric(DESCUENTO.Text))
            {
                
                DESCUENTO_tr = double.Parse(DESCUENTO.Text.ToString());
                DESCUENTO.Text = cUTILERIA.Trunca_y_formatea(decimal.Parse(DESCUENTO_tr.ToString()));
            }
            //if (aplicarDescuentoAutomatico.Checked)
            //{
            //    DESCUENTO.Text = cUTILERIA.Trunca_y_formatea(decimal.Parse(SUBTOTAL_tr.ToString()));
            //    DESCUENTO_tr = double.Parse(SUBTOTAL_tr.ToString());
            //}

            IMPUESTO_tr += IMPUESTO_IEPS;

            TOTAL_tr = (Math.Round(SUBTOTAL_tr,3) + IMPUESTO_tr + IEPS) - RETENCION_tr - DESCUENTO_tr;

            SUBTOTAL.Text = SUBTOTAL_tr.ToString(); // cUTILERIA.Trunca_y_formatea(decimal.Parse(SUBTOTAL_tr.ToString()));
            IMPUESTO.Text = IMPUESTO_tr.ToString();  //cUTILERIA.Trunca_y_formatea(decimal.Parse(IMPUESTO_tr.ToString()));
            RETENCION.Text = RETENCION_tr.ToString(); //cUTILERIA.Trunca_y_formatea(decimal.Parse(RETENCION_tr.ToString()));

            IMPUESTO.Text = cUTILERIA.Trunca_y_formatea(decimal.Parse(IMPUESTO_tr.ToString()));

            TOTAL.Text = cUTILERIA.Trunca_y_formatea(decimal.Parse(TOTAL_tr.ToString()));

            //Formatear
            SUBTOTAL_tr = Math.Round(SUBTOTAL_tr, 3);
            SUBTOTAL.Text = cUTILERIA.Trunca_y_formatea(decimal.Parse(SUBTOTAL_tr.ToString()));
            TOTAL.Text = cUTILERIA.Trunca_y_formatea(decimal.Parse(TOTAL_tr.ToString()));
            calcularTotalSinDescuento();
        }

        private void calcularTotalSinDescuento()
        {
            decimal DESCUENTO_tr = 0;
            for (int i = 0; i < dtgrdGeneral.Rows.Count; i++)
            {
                decimal importetr=decimal.Parse(dtgrdGeneral.Rows[i].Cells["importe"].Value.ToString().Replace("$", ""));
                if(importetr>0){
                    DESCUENTO_tr+=importetr;
                }
            }
            totalSinDescuento.Text = oData.Trunca_y_formatea(DESCUENTO_tr);
        }

        private void dtgrdGeneral_CellDoubleClick_1(object sender, DataGridViewCellEventArgs e)
        {
            editar_linea();
        }


        private void editar_linea()
        {
            if (dtgrdGeneral.SelectedRows.Count > 0)
            {
                DataGridViewSelectedRowCollection arrSelectedRows = dtgrdGeneral.SelectedRows;
                //string ROW_ID = (string)arrSelectedRows[0].Cells[0].Value;

                frmCFDIConcepto oFacturaLinea = new frmCFDIConcepto(
                    decimal.Parse(arrSelectedRows[0].Cells["cantidad"].Value.ToString())
                    , arrSelectedRows[0].Cells["unidad"].Value.ToString()
                    , arrSelectedRows[0].Cells["noIdentificacion"].Value.ToString()
                    , arrSelectedRows[0].Cells["descripcion"].Value.ToString()
                    , decimal.Parse(arrSelectedRows[0].Cells["valorUnitario"].Value.ToString())
                    , decimal.Parse(arrSelectedRows[0].Cells["importe"].Value.ToString())
                    , arrSelectedRows[0].Cells["fraccionArancelaria"].Value.ToString()
                    , oData_ERP.sConn
                    , ocEMPRESA.ROW_ID
                    , oData.sConn
                    , arrSelectedRows[0].Cells["informacionAduaneraAduana"].Value.ToString()
                    , arrSelectedRows[0].Cells["informacionAduaneraFecha"].Value.ToString()
                    , arrSelectedRows[0].Cells["informacionAduaneraNumero"].Value.ToString()

                    );
                if (oFacturaLinea.ShowDialog() == DialogResult.OK)
                {
                    arrSelectedRows[0].Cells["cantidad"].Value = oFacturaLinea.cantidadP;
                    arrSelectedRows[0].Cells["unidad"].Value = oFacturaLinea.unidadP;
                    arrSelectedRows[0].Cells["noIdentificacion"].Value = oFacturaLinea.noIdentificacionP;
                    arrSelectedRows[0].Cells["descripcion"].Value = oFacturaLinea.descripcionP;
                    arrSelectedRows[0].Cells["valorUnitario"].Value = oFacturaLinea.valorUnitarioP;
                    arrSelectedRows[0].Cells["importe"].Value = oFacturaLinea.importeP;
                    arrSelectedRows[0].Cells["fraccionArancelaria"].Value = oFacturaLinea.fraccionArancelariaP;

                    arrSelectedRows[0].Cells["informacionAduaneraAduana"].Value = oFacturaLinea.aduanaP;
                    arrSelectedRows[0].Cells["informacionAduaneraFecha"].Value = oFacturaLinea.fechaP;
                    arrSelectedRows[0].Cells["informacionAduaneraNumero"].Value = oFacturaLinea.numeroP;
                    calcular();
                }

            }
        }

        private void IMPUESTO_DATO_SelectedIndexChanged(object sender, EventArgs e)
        {
            calcular();
        }

        private void buttonX5_Click(object sender, EventArgs e)
        {
            if (generar())
            {
                if (timbrar())
                {
                    imprimir(false);
                    guardarCE();
                    if(ESTADO.Text!="BORRADOR")
                    {
                        ocSERIE.incrementar(ocSERIE.ROW_ID);
                        MessageBox.Show("CFDI realizado correctamente.", Application.ProductName + "-" + Application.ProductVersion, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);

                    }
                }
                else
                {
                    imprimir(false);
                    if(ESTADO.Text.Contains("BORRADOR"))
                    {
                        MessageBox.Show("BORRADOR CFDI realizado correctamente.", Application.ProductName + "-" + Application.ProductVersion, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    } 
                }

                
            }
        }

        private void guardarCE()
        {
            if(NumRegIdTrib.Text!=""){
                string sDirectoryCE_tr = AppDomain.CurrentDomain.BaseDirectory + "cCEProveedor_"+NumRegIdTrib.Text;
                XmlSerializer serializer = new XmlSerializer(typeof(cCEProveedor));
                StreamWriter writer = new StreamWriter(sDirectoryCE_tr);
                cCEProveedor ocCEProveedor = new cCEProveedor();
                ocCEProveedor.Calle = calle.Text;
                ocCEProveedor.CodigoPostal = CodigoPostal.Text;
                ocCEProveedor.Estado = EstadoCE.Text;
                ocCEProveedor.NoExterior = noExterior.Text;
                ocCEProveedor.NoInterior = noInterior.Text;
                ocCEProveedor.NumRegIdTrib = NumRegIdTrib.Text;
                ocCEProveedor.Pais = Pais.Text;

                serializer.Serialize(writer, ocCEProveedor);
                writer.Close();
            }
            
        }


        private void cargarCE()
        {
            if (NumRegIdTrib.Text != "")
            {
                XmlSerializer serializer = new XmlSerializer(typeof(cCEProveedor));
                try
                {
                    string sDirectoryCE_tr = AppDomain.CurrentDomain.BaseDirectory + "cCEProveedor_" + NumRegIdTrib.Text;

                    TextReader reader = new StreamReader(sDirectory);
                    cCEProveedor ocCEProveedor = new cCEProveedor();
                    ocCEProveedor = (cCEProveedor)serializer.Deserialize(reader);
                    reader.Close();

                    calle.Text = ocCEProveedor.Calle;
                    CodigoPostal.Text = ocCEProveedor.CodigoPostal;
                    EstadoCE.Text = ocCEProveedor.Estado;
                    noExterior.Text = ocCEProveedor.NoExterior;
                    noInterior.Text = ocCEProveedor.NoInterior;
                    NumRegIdTrib.Text = ocCEProveedor.NumRegIdTrib;
                    Pais.Text = ocCEProveedor.Pais;

                }
                catch (Exception e)
                {

                }
            }
        }

        private bool timbrar()
        {
            //Timbrar
            informacion.Text = "Timbrando el XML " + ID.Text;
            bool usar_edicom = false;
            if(ESTADO.Text.Contains("BORRADOR")){
                usar_edicom = true;
            }
            try
            {
                if (usar_edicom)
                {
                    usar_edicom = this.generar_CFDI(2, Globales.oCFDI_USUARIO.USUARIO, Globales.oCFDI_USUARIO.PASSWORD);
                }
                else
                {
                    usar_edicom = this.generar_CFDI(1, Globales.oCFDI_USUARIO.USUARIO, Globales.oCFDI_USUARIO.PASSWORD);
                }
            }
            catch (Exception ex) {
                MessageBox.Show("Error timbrando en EDICOM" + Environment.NewLine + ex.InnerException.ToString(), Application.ProductVersion, MessageBoxButtons.OK);
                return false;
            }
            if (!ESTADO.Text.Contains("BORRADOR"))
            {
                return true;
            }
            return false;
        }

        public bool generar_CFDI(int Opcion, string EDICOM_USUARIO, string EDICOM_PASSWORD)
        {
            try
            {
                //Comprimir Archivo
                string Archivo_Tmp = oComprobante.serie+oComprobante.folio + ".xml";
                File.Copy(ARCHIVO, Archivo_Tmp, true);
                string ARCHIVO_zip = Archivo_Tmp.ToUpper().Replace("XML", "ZIP");
                using (ZipFile zip = new ZipFile())
                {
                    zip.AddFile(Archivo_Tmp);
                    zip.Save(ARCHIVO_zip);
                }
                //Fin de Compresion de Archivo
                File.Delete(Archivo_Tmp);

                byte[] ARCHIVO_Leido = ReadBinaryFile(ARCHIVO_zip);
                File.Delete(ARCHIVO_zip);
                CFDiClient oCFDiClient = new CFDiClient();

                byte[] PROCESO;
                string MENSAJE = "";

                switch (Opcion)
                {
                    case 1:
                        try
                        {
                            informacion.Text = "Generando CFDI ";
                            MENSAJE = "";
                            PROCESO = oCFDiClient.getCfdi(EDICOM_USUARIO, EDICOM_PASSWORD, ARCHIVO_Leido);
                            guardar_archivo(PROCESO, oComprobante.serie + oComprobante.folio, ARCHIVO);
                        }
                        catch (FaultException oException)
                        {

                            CFDiException oCFDiException = new CFDiException();
                            MessageBox.Show(oException.Message, Application.ProductName + "-" + Application.ProductVersion, MessageBoxButtons.OK, MessageBoxIcon.Error);
                            return false;
                        }

                        break;
                    case 2:
                        try
                        {
                            informacion.Text = "Generando CFDI Test ";
                            MENSAJE = " TEST ";
                            PROCESO = oCFDiClient.getCfdiTest(EDICOM_USUARIO, EDICOM_PASSWORD, ARCHIVO_Leido);
                            guardar_archivo(PROCESO, oComprobante.serie+oComprobante.folio, ARCHIVO);
                        }
                        catch (FaultException oException)
                        {

                            CFDiException oCFDiException = new CFDiException();
                            MessageBox.Show(oException.Message, Application.ProductName + "-" + Application.ProductVersion, MessageBoxButtons.OK, MessageBoxIcon.Error);
                            informacion.Text = "Error " + DateTime.Now.ToShortDateString() + " " + DateTime.Now.ToLongTimeString();
                            return false;
                        }
                        break;
                    case 3:
                        try
                        {
                            informacion.Text = "Generando Timbre CFDI  ";
                            PROCESO = oCFDiClient.getTimbreCfdi(EDICOM_USUARIO, EDICOM_PASSWORD, ARCHIVO_Leido);
                        }
                        catch (Exception oException)
                        {

                            CFDiException oCFDiException = new CFDiException();
                            MessageBox.Show(oException.Message, Application.ProductName + "-" + Application.ProductVersion, MessageBoxButtons.OK, MessageBoxIcon.Error);
                            informacion.Text = "Error " + DateTime.Now.ToShortDateString() + " " + DateTime.Now.ToLongTimeString();
                            return false;
                        }
                        break;
                    case 4:
                        try
                        {
                            informacion.Text = "Generando Timbre CFDI Test ";
                            PROCESO = oCFDiClient.getTimbreCfdiTest(EDICOM_USUARIO, EDICOM_PASSWORD, ARCHIVO_Leido);
                        }
                        catch (Exception oException)
                        {

                            CFDiException oCFDiException = new CFDiException();
                            MessageBox.Show(oException.Message, Application.ProductName + "-" + Application.ProductVersion, MessageBoxButtons.OK, MessageBoxIcon.Error);
                            informacion.Text = "Error " + DateTime.Now.ToShortDateString() + " " + DateTime.Now.ToLongTimeString();
                            return false;
                        }
                        break;
                }

                informacion.Text = "Generado y Sellado";
                return true;
            }
            catch (Exception oException)
            {
                MessageBox.Show("Generando CFDI " + oException.Message, Application.ProductName + "-" + Application.ProductVersion, MessageBoxButtons.OK, MessageBoxIcon.Error);
                informacion.Text = "Error " + DateTime.Now.ToShortDateString() + " " + DateTime.Now.ToLongTimeString();
                return false;
            }
        }

        private void guardar_archivo(byte[] PROCESO, string INVOICE_ID, string ARCHIVO)
        {
            //Guardar los PROCESO

            File.WriteAllBytes("TEMPORAL.ZIP", PROCESO);
            //Descomprimir
            String TargetDirectory = "TEMPORAL";
            using (Ionic.Zip.ZipFile zip = Ionic.Zip.ZipFile.Read("TEMPORAL.ZIP"))
            {
                zip.ExtractAll(TargetDirectory, Ionic.Zip.ExtractExistingFileAction.OverwriteSilently);
            }

            //Copiar el ARCHIVO Descrompimido por otro
            string ARCHIVO_descomprimido = TargetDirectory + @"\" + "SIGN_" + INVOICE_ID + ".XML";
            
            File.Copy(ARCHIVO_descomprimido, ARCHIVO, true);
            PATH.Text = ARCHIVO;
            File.Delete(ARCHIVO_descomprimido);
            File.Delete("TEMPORAL.ZIP");

        }



        private bool FileExists(string pPath)
        {
            string sPath = pPath;
            try
            {
                if (File.Exists(sPath))
                {
                    return true;
                }
                else
                {
                    return false;
                }

            }
            catch
            {
                return false;
            }
        }

        public string validacion(string cadena)
        {
            string devolucion = "";
            devolucion = cadena.ToUpper();

            devolucion = devolucion.Replace("á", "A");
            devolucion = devolucion.Replace("é", "E");
            devolucion = devolucion.Replace("í", "I");
            devolucion = devolucion.Replace("ó", "O");
            devolucion = devolucion.Replace("ú", "U");
            devolucion = devolucion.Replace("Á", "A");
            devolucion = devolucion.Replace("É", "E");
            devolucion = devolucion.Replace("Í", "I");
            devolucion = devolucion.Replace("Ó", "O");
            devolucion = devolucion.Replace("Ú", "U");
            devolucion = devolucion.Replace(">", "");
            devolucion = devolucion.Replace("<", "");
            //devolucion = devolucion.Replace("&", "");
            devolucion = devolucion.Replace("'", "");
            devolucion = devolucion.Replace("#", "");
            devolucion = devolucion.Replace("|", "");
            //devolucion = HtmlEncode(devolucion);

            devolucion = Regex.Replace(devolucion, @"[\u0000-\u001F]", string.Empty);


            return devolucion;
        }


        

        private byte[] ReadBinaryFile(string fileName)
        {
            if (File.Exists(fileName))
            {
                try
                {
                    ///Open and read a file&#12290;
                    FileStream fileStream = File.OpenRead(fileName);
                    byte[] archivo = ConvertStreamToByteBuffer(fileStream);
                    fileStream.Close();
                    return archivo;
                }
                catch (Exception ex)
                {
                    return new byte[0];
                }
            }
            else
            {
                return new byte[0];
            }
        }
        public byte[] ConvertStreamToByteBuffer(System.IO.Stream theStream)
        {
            int b1;
            System.IO.MemoryStream tempStream = new System.IO.MemoryStream();
            while ((b1 = theStream.ReadByte()) != -1)
            {
                tempStream.WriteByte(((byte)b1));
            }
            return tempStream.ToArray();
        }
        public bool generar()
        {
            try
            {
                //Valor por defecto del valorUnitario es de 2
                string INVOICE_ID = ID.Text;
                string sDirectory = AppDomain.CurrentDomain.BaseDirectory;
                if (ocSERIE == null)
                {
                    MessageBox.Show("Seleccione la serie", Application.ProductName + "-" + Application.ProductVersion, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                    return false;
                }
                if (String.IsNullOrEmpty(rfc.Text))
                {
                    MessageBox.Show("Seleccione el RFC ", Application.ProductName + "-" + Application.ProductVersion, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                    return false;
                }
                if (dtgrdGeneral.Rows.Count==0)
                {
                    MessageBox.Show("Ingrese al menos 1 línea", Application.ProductName + "-" + Application.ProductVersion, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                    return false;
                }
                

                //Cargar Certificado
                ocCERTIFICADO = new cCERTIFICADO();
                ocCERTIFICADO.cargar(ocSERIE.ROW_ID_CERTIFICADO);
                cEMPRESA ocEMPRESA = new cEMPRESA(oData);
                ocEMPRESA.cargar(ocCERTIFICADO.ROW_ID_EMPRESA);

                
                if (!FileExists(sDirectory + "/XSLT/cadenaoriginal_3_2.xslt"))
                {
                    MessageBox.Show("No se tiene acceso al archivo " 
                        + Environment.NewLine
                        + sDirectory + "/XSLT/cadenaoriginal_3_2.xslt ", Application.ProductName + " - " + Application.ProductVersion, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);

                    return false;
                }
                //MessageBox.Show("Cargado XLT");
                XmlSerializerNamespaces myNamespaces = new XmlSerializerNamespaces();
                myNamespaces.Add("cfdi", "http://www.sat.gob.mx/cfd/3");
                //MessageBox.Show("Cargado XLT " + ocCERTIFICADO.CERTIFICADO);
                if (!File.Exists(ocCERTIFICADO.CERTIFICADO))
                {
                    MessageBox.Show("No se tiene acceso al archivo " + ocCERTIFICADO.CERTIFICADO + "", Application.ProductName + "-" + Application.ProductVersion, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                    return false;
                }
                X509Certificate cert = X509Certificate.CreateFromCertFile(ocCERTIFICADO.CERTIFICADO);
                string NoSerie = cert.GetSerialNumberString();

                StringBuilder SerieHex = new StringBuilder();
                for (int i = 0; i <= NoSerie.Length - 2; i += 2)
                {
                    SerieHex.Append(Convert.ToString(Convert.ToChar(Int32.Parse(NoSerie.Substring(i, 2), System.Globalization.NumberStyles.HexNumber))));
                }
                //MessageBox.Show("Comprobante");
                string NO_CERTIFICADO = SerieHex.ToString();

                DateTime INVOICE_DATE = FECHA.Value;
                IFormatProvider culture = new CultureInfo("es-MX", true);
                DateTime CREATE_DATE = DateTime.Now;
                
                INVOICE_DATE = DateTime.Parse(INVOICE_DATE.Day.ToString() + "/" + INVOICE_DATE.Month.ToString() + "/" + INVOICE_DATE.Year.ToString() + " " + CREATE_DATE.ToString("HH:mm:ss"), culture);
                
                oComprobante.fecha = INVOICE_DATE;//FECHA_FACTURA;

                //MessageBox.Show("Comprobante1");
                XmlSerializer serializer = new XmlSerializer(typeof(Comprobante));

                oComprobante = new Comprobante();
                DateTime FECHA_FACTURA = new DateTime();

                //Serie
                oComprobante.serie = ocSERIE.ID;
                //Folio
                oComprobante.folio = ID.Text;
                oComprobante.Moneda = MONEDA.Text;
                oComprobante.TipoCambio = TIPO_CAMBIO.Text;

                if ((oComprobante.condicionesDePago == "")
                    ||
                    (oComprobante.condicionesDePago == null))
                {
                    oComprobante.condicionesDePago = "PAGO EN UNA SOLA EXHIBICION";
                }


                //CDFI 3.2
                oComprobante.LugarExpedicion = ocSERIE.ESTADO + "," + ocSERIE.MUNICIPIO;

                INVOICE_DATE = FECHA.Value;
                oComprobante.fecha = DateTime.Today.AddHours(-1);

                //Aprobacion
                //Tipo de Comprobante
                switch (TIPO_FACTURA.Text)
                {
                    case "ingreso":
                        oComprobante.tipoDeComprobante = ComprobanteTipoDeComprobante.ingreso;
                        break;
                    case "egreso":
                        oComprobante.tipoDeComprobante = ComprobanteTipoDeComprobante.egreso;
                        break;
                    case "traslado":
                        oComprobante.tipoDeComprobante = ComprobanteTipoDeComprobante.traslado;
                        break;
                    default:
                        oComprobante.tipoDeComprobante = ComprobanteTipoDeComprobante.ingreso;
                        break;
                }
                if(decimal.Parse(DESCUENTO.Text)!=0)
                {
                    oComprobante.descuento = decimal.Parse(DESCUENTO.Text);
                    oComprobante.descuentoSpecified = true;
                    oComprobante.motivoDescuento = motivoDescuento.Text;
                }

                oComprobante.formaDePago = FORMA_PAGO.Text;
                oComprobante.NumCtaPago = CUENTA_BANCARIA.Text;
                oComprobante.metodoDePago = METODO_DE_PAGO.Text;
                cert = X509Certificate.CreateFromCertFile(ocCERTIFICADO.CERTIFICADO);
                string Certificado64 = System.Convert.ToBase64String(cert.GetRawCertData());
                oComprobante.certificado = Certificado64;
                //MessageBox.Show("Comprobante9");
                //Version 3
                oComprobante.noCertificado = validacion(NO_CERTIFICADO);
                //SubTotal
                oComprobante.subTotal = Math.Abs(decimal.Parse(TOTAL.Text)) - Math.Abs(decimal.Parse(DESCUENTO.Text)) - Math.Abs(decimal.Parse(IMPUESTO.Text));
                oComprobante.descuento = 0;
                if (decimal.Parse(DESCUENTO.Text) != 0)
                {
                    oComprobante.descuento = Math.Abs(decimal.Parse(DESCUENTO.Text));
                    oComprobante.descuentoSpecified = true;
                }

                if (oComprobante.tipoDeComprobante == ComprobanteTipoDeComprobante.ingreso)
                {
                    oComprobante.total = (Math.Abs((oComprobante.subTotal + Math.Abs(decimal.Parse(IMPUESTO.Text))) - Math.Abs(decimal.Parse(RETENCION.Text)))) - oComprobante.descuento;
                }

                //Elemento Emisor
                ComprobanteEmisor oComprobanteEmisor = new ComprobanteEmisor();

                //Cargar los datos de la Compañia apartir del Certificado
                cert = X509Certificate.CreateFromCertFile(ocCERTIFICADO.CERTIFICADO);
                string Dato = cert.GetName();
                //MessageBox.Show("Comprobante11");

                char[] delimiterChars = { ',' };
                string[] TipoCer = Dato.Split(delimiterChars);

                //RFC Posicion 4
                string RFC_Certificado = TipoCer[4];
                char[] RFC_Delimiter = { '=' };
                string[] RFC_Arreglo = RFC_Certificado.Split(RFC_Delimiter);
                string RFC_Arreglo_Certificado = RFC_Arreglo[1];
                RFC_Arreglo_Certificado = RFC_Arreglo_Certificado.Replace("\"", "");
                RFC_Arreglo_Certificado = RFC_Arreglo_Certificado.Replace("/", "");
                RFC_Arreglo_Certificado = RFC_Arreglo_Certificado.Trim();
                //MessageBox.Show("Comprobante12");
                //RFC
                oComprobanteEmisor.rfc = validacion(ocEMPRESA.RFC);
                //Nombre
                oComprobanteEmisor.nombre = validacion(ocEMPRESA.ID);
                //Regimen Fiscal
                ComprobanteEmisorRegimenFiscal[] oComprobanteEmisorRegimenFiscal = new ComprobanteEmisorRegimenFiscal[1];
                oComprobanteEmisorRegimenFiscal[0] = new ComprobanteEmisorRegimenFiscal();
                oComprobanteEmisorRegimenFiscal[0].Regimen = ocEMPRESA.REGIMEN_FISCAL;
                oComprobanteEmisor.RegimenFiscal = oComprobanteEmisorRegimenFiscal;
                //MessageBox.Show("Comprobante13");
                t_UbicacionFiscal ot_UbicacionFiscal = new t_UbicacionFiscal();

                //Domicilio Fiscal
                if (ocEMPRESA.CALLE != "")
                {
                    ot_UbicacionFiscal.calle = validacion(ocEMPRESA.CALLE);
                }
                if (ocEMPRESA.EXTERIOR != "")
                {
                    ot_UbicacionFiscal.noExterior = validacion(ocEMPRESA.EXTERIOR);
                }
                if (ocEMPRESA.INTERIOR != "")
                {
                    ot_UbicacionFiscal.noInterior = validacion(ocEMPRESA.INTERIOR);
                }
                if (ocEMPRESA.COLONIA != "")
                {
                    ot_UbicacionFiscal.colonia = validacion(ocEMPRESA.COLONIA);
                }
                if (ocEMPRESA.LOCALIDAD != "")
                {
                    ot_UbicacionFiscal.localidad = validacion(ocEMPRESA.LOCALIDAD);
                }
                if (ocEMPRESA.REFERENCIA != "")
                {
                    ot_UbicacionFiscal.referencia = validacion(ocEMPRESA.REFERENCIA);
                }
                if (ocEMPRESA.MUNICIPIO != "")
                {
                    ot_UbicacionFiscal.municipio = validacion(ocEMPRESA.MUNICIPIO);
                }
                if (ocEMPRESA.ESTADO != "")
                {
                    ot_UbicacionFiscal.estado = validacion(ocEMPRESA.ESTADO);
                }
                if (ocEMPRESA.PAIS != "")
                {
                    ot_UbicacionFiscal.pais = validacion(ocEMPRESA.PAIS);
                }
                if (ocEMPRESA.CP != "")
                {
                    ot_UbicacionFiscal.codigoPostal = validacion(ocEMPRESA.CP);
                }
                oComprobanteEmisor.DomicilioFiscal = ot_UbicacionFiscal;

                //Expedido En
                t_Ubicacion ot_Ubicacion = new t_Ubicacion();
                if (ocSERIE.CALLE != "")
                {
                    ot_Ubicacion.calle = validacion(ocSERIE.CALLE);
                }
                if (ocSERIE.EXTERIOR != "")
                {
                    ot_Ubicacion.noExterior = validacion(ocSERIE.EXTERIOR);
                }
                if (ocSERIE.INTERIOR != "")
                {
                    ot_Ubicacion.noInterior = validacion(ocSERIE.INTERIOR);
                }
                if (ocSERIE.COLONIA != "")
                {
                    ot_Ubicacion.colonia = validacion(ocSERIE.COLONIA);
                }
                if (ocSERIE.LOCALIDAD != "")
                {
                    ot_Ubicacion.localidad = validacion(ocSERIE.LOCALIDAD);
                }
                if (ocSERIE.REFERENCIA != "")
                {
                    ot_Ubicacion.referencia = validacion(ocSERIE.REFERENCIA);
                }
                if (ocSERIE.MUNICIPIO != "")
                {
                    ot_Ubicacion.municipio = validacion(ocSERIE.MUNICIPIO);
                }
                if (ocSERIE.ESTADO != "")
                {
                    ot_Ubicacion.estado = validacion(ocSERIE.ESTADO);
                }
                if (ocSERIE.PAIS != "")
                {
                    ot_Ubicacion.pais = validacion(ocSERIE.PAIS);
                }
                if (ocSERIE.CP != "")
                {
                    ot_Ubicacion.codigoPostal = validacion(ocSERIE.CP);
                }

                oComprobanteEmisor.ExpedidoEn = ot_Ubicacion;

                oComprobante.Emisor = oComprobanteEmisor;
                //MessageBox.Show("Comprobante15");
                //Elemento Receptor
                ComprobanteReceptor oComprobanteReceptor = new ComprobanteReceptor();
                oComprobanteReceptor.rfc = validacion(rfc.Text);
                oComprobanteReceptor.rfc = oComprobanteReceptor.rfc;
                oComprobanteReceptor.nombre = receptor.Text;
                oComprobante.Receptor = oComprobanteReceptor;
                //Agregar direccion de Destino

                //Elemento Conceptos
                ComprobanteConcepto[] oComprobanteConceptos;
                oComprobanteConceptos = new ComprobanteConcepto[dtgrdGeneral.Rows.Count];
                int index = 0;
                foreach (DataGridViewRow row in this.dtgrdGeneral.Rows)
                {

                    //MessageBox.Show("Comprobante18");
                    CFDI33.ComprobanteConcepto oComprobanteConcepto = new CFDI33.ComprobanteConcepto();
                    //oComprobanteConcepto.cantidad = Math.Abs(decimal.Parse(row.Cells["cantidad"].Value.ToString()));
                    oComprobanteConcepto.cantidad = decimal.Parse(row.Cells["cantidad"].Value.ToString());
                    oComprobanteConcepto.unidad = row.Cells["unidad"].Value.ToString();

                    if (row.Cells["noIdentificacion"].Value.ToString() != "")
                    {
                        oComprobanteConcepto.noIdentificacion = row.Cells["noIdentificacion"].Value.ToString();
                    }
                    
                    oComprobanteConcepto.descripcion = row.Cells["descripcion"].Value.ToString();
                    //oComprobanteConcepto.valorUnitario = Math.Abs(decimal.Parse(row.Cells["valorUnitario"].Value.ToString()));
                    oComprobanteConcepto.valorUnitario = decimal.Parse(row.Cells["valorUnitario"].Value.ToString());
                    //oComprobanteConcepto.importe = Math.Abs(decimal.Parse(row.Cells["importe"].Value.ToString()));
                    oComprobanteConcepto.importe = decimal.Parse(row.Cells["importe"].Value.ToString());

                    //Tiene información aduanera
                    if (row.Cells["informacionAduaneraAduana"].Value.ToString() != "")
                    {
                        t_InformacionAduanera[] ot_InformacionAduanera;
                        ot_InformacionAduanera = new t_InformacionAduanera[1];
                        for (int t = 0; t < 1; t++)
                        {
                            ot_InformacionAduanera[t] = new t_InformacionAduanera();
                            ot_InformacionAduanera[t].aduana = row.Cells["informacionAduaneraAduana"].Value.ToString();
                            ot_InformacionAduanera[t].fecha = DateTime.Parse(row.Cells["informacionAduanerFecha"].Value.ToString());
                            ot_InformacionAduanera[t].numero = row.Cells["informacionAduanerNumero"].Value.ToString();
                        }
                        oComprobanteConcepto.Items = ot_InformacionAduanera;
                    }

                    oComprobanteConceptos[index] = new ComprobanteConcepto();
                    oComprobanteConceptos[index] = oComprobanteConcepto;
                    index++;
                }
                oComprobante.Conceptos = oComprobanteConceptos;
                ComprobanteImpuestos oImpuesto = new ComprobanteImpuestos();
                //Impuestos  Trasladados

                oImpuesto.totalImpuestosTrasladadosSpecified = true;

                oImpuesto.totalImpuestosTrasladados = Math.Abs(decimal.Parse(IMPUESTO.Text));

                //Impuestos  Trasladados
                ComprobanteImpuestosTraslado[] oComprobanteImpuestosTraslados;
                oComprobanteImpuestosTraslados = new ComprobanteImpuestosTraslado[2];
                //IVA
                ComprobanteImpuestosTraslado oComprobanteImpuestosTraslado = new ComprobanteImpuestosTraslado();
                oComprobanteImpuestosTraslado.importe = oImpuesto.totalImpuestosTrasladados;
                oComprobanteImpuestosTraslado.tasa = 16;
                oComprobanteImpuestosTraslado.impuesto = ComprobanteImpuestosTrasladoImpuesto.IVA;
                oComprobanteImpuestosTraslados[0] = new ComprobanteImpuestosTraslado();
                oComprobanteImpuestosTraslados[0] = oComprobanteImpuestosTraslado;
                //IEPS
                oComprobanteImpuestosTraslado = new ComprobanteImpuestosTraslado();
                oComprobanteImpuestosTraslado.importe = 0;
                oComprobanteImpuestosTraslado.tasa = 0;
                oComprobanteImpuestosTraslado.impuesto = ComprobanteImpuestosTrasladoImpuesto.IEPS;
                oComprobanteImpuestosTraslados[1] = new ComprobanteImpuestosTraslado();
                oComprobanteImpuestosTraslados[1] = oComprobanteImpuestosTraslado;
                oImpuesto.Traslados = oComprobanteImpuestosTraslados;
                //Retenidos
                if (Math.Abs(decimal.Parse(RETENCION.Text)) != 0)
                {
                    ComprobanteImpuestosRetencion[] oComprobanteImpuestosRetencion = new ComprobanteImpuestosRetencion[1];
                    oComprobanteImpuestosRetencion[0] = new ComprobanteImpuestosRetencion();
                    oComprobanteImpuestosRetencion[0].importe = Math.Abs(decimal.Parse(RETENCION.Text));
                    oComprobanteImpuestosRetencion[0].impuesto = ComprobanteImpuestosRetencionImpuesto.IVA;
                    oImpuesto.Retenciones = oComprobanteImpuestosRetencion;
                    oImpuesto.totalImpuestosRetenidosSpecified = true;
                    oImpuesto.totalImpuestosRetenidos = Math.Abs(decimal.Parse(RETENCION.Text));
                }
                oComprobante.Impuestos = oImpuesto;

                //Agregar leyendaFiscal
                try
                {
                    if (OBSERVACIONES.Text != "")
                    {
                        oComprobante.Complemento = new ComprobanteComplemento();
                        LeyendasFiscales oLeyendasFiscales = new LeyendasFiscales();
                        oLeyendasFiscales.Leyenda = new LeyendasFiscalesLeyenda[1];
                        oLeyendasFiscales.Leyenda[0] = new LeyendasFiscalesLeyenda();
                        oLeyendasFiscales.Leyenda[0].textoLeyenda = OBSERVACIONES.Text;
                        oComprobante.Complemento.Any = new XmlElement[1];
                        XmlDocument doc = new XmlDocument();

                        XmlSerializerNamespaces myNamespaces_leyenda = new XmlSerializerNamespaces();
                        myNamespaces_leyenda.Add("leyendasFisc", "http://www.sat.gob.mx/leyendasFiscales");

                        using (XmlWriter writer = doc.CreateNavigator().AppendChild())
                        {
                            new XmlSerializer(oLeyendasFiscales.GetType()).Serialize(writer, oLeyendasFiscales, myNamespaces_leyenda);
                        }

                        int indice = oComprobante.Complemento.Any.Length - 1;
                        oComprobante.Complemento.Any[indice] = doc.DocumentElement;


                    }
                }
                catch (Exception exLeyendaFiscal)
                {
                    MessageBox.Show("Error en Leyenda Fiscal. " + exLeyendaFiscal.Message.ToString()
                        , Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return false;
                }

               

                XslCompiledTransform myXslTrans = new XslCompiledTransform();
                XslCompiledTransform trans = new XslCompiledTransform();
                string nombre_tmp = INVOICE_ID + "TMP" + DateTime.Now.ToString("ddMMyyyyhhmmss");
                if (File.Exists(nombre_tmp + ".XML"))
                {
                    File.Delete(nombre_tmp + ".XML");
                }

                XPathDocument myXPathDoc;
                try
                {
                    TextWriter writer = new StreamWriter(nombre_tmp + ".XML");
                    serializer.Serialize(writer, oComprobante, myNamespaces);
                    writer.Close();

                    myXPathDoc = new XPathDocument(nombre_tmp + ".XML");
                    //XslTransform myXslTrans = new XslTransform();
                    myXslTrans = new XslCompiledTransform();

                    trans = new XslCompiledTransform();
                    XmlUrlResolver resolver = new XmlUrlResolver();

                    resolver.Credentials = System.Net.CredentialCache.DefaultCredentials;
                    XsltSettings settings = new XsltSettings(true, true);
                }
                catch (XmlException errores)
                {
                    MessageBox.Show("Error en " + errores.InnerException.ToString());
                    return false;
                }
                //Cargar xslt
                try
                {
                    //Version 3
                    myXslTrans.Load(sDirectory + "/XSLT/cadenaoriginal_3_2.xslt");
                }
                catch (XmlException errores)
                {
                    MessageBox.Show("Error en " + errores.InnerException.ToString());
                    return false;
                }

                //Crear Archivo Temporal

                XmlTextWriter myWriter = new XmlTextWriter(nombre_tmp + ".TXT", null);

                //Transformar al XML
                myXslTrans.Transform(myXPathDoc, null, myWriter);

                myWriter.Close();

                //Abrir Archivo TXT
                string CADENA_ORIGINAL = "";
                StreamReader re = File.OpenText(nombre_tmp + ".TXT");
                string input = null;
                while ((input = re.ReadLine()) != null)
                {
                    CADENA_ORIGINAL += input;
                }
                re.Close();

                //Eliminar Archivos Temporales
                File.Delete(nombre_tmp + ".TXT");
                File.Delete(nombre_tmp + ".XML");

                //Buscar el &amp; en la Cadena y sustituirlo por &
                CADENA_ORIGINAL = CADENA_ORIGINAL.Replace("&amp;", "&");
                cGeneracion ocGeneracion = new cGeneracion(oData, oData_ERP);
                oComprobante.sello = ocGeneracion.SelloRSA(ocCERTIFICADO.LLAVE, ocCERTIFICADO.PASSWORD, CADENA_ORIGINAL, FECHA_FACTURA.Year); ;

                string DIRECTORIO_ARCHIVOS = "";
                //if (!Directory.Exists(ocSERIE.DIRECTORIO))
                //{
                //    ocSERIE.DIRECTORIO=Application.StartupPath;
                //}


                DIRECTORIO_ARCHIVOS += ocSERIE.DIRECTORIO + @"\" + ocSERIE.ID + @"\" + ocGeneracion.Fechammmyy(oComprobante.fecha.ToString());
                string XML = DIRECTORIO_ARCHIVOS;

                string PDF = XML + @"\" + "pdf";
                if (!Directory.Exists(PDF))
                {
                    try
                    {

                        Directory.CreateDirectory(PDF);
                    }
                    catch (IOException excep)
                    {
                        MessageBox.Show(excep.Message.ToString() + Environment.NewLine + "Creando directorio de PDF"
                            , Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error);
                        return false;
                    }
                }

                XML += @"\" + "xml";
                if (Directory.Exists(XML) == false)
                    Directory.CreateDirectory(XML);

                string XML_Archivo = "";

                string nombre_archivo = ocSERIE.ID+INVOICE_ID;

                if (Directory.Exists(DIRECTORIO_ARCHIVOS))
                {
                    XML_Archivo = XML + @"\" + nombre_archivo + ".xml";
                }
                else
                {
                    XML_Archivo = @"\" + nombre_archivo + ".xml"; ;
                }

                XML_Archivo = XML + @"\" + nombre_archivo + ".xml";

                TextWriter writerXML = new StreamWriter(XML_Archivo);
                serializer.Serialize(writerXML, oComprobante, myNamespaces);
                writerXML.Close();
                ocGeneracion.parsar_d1p1(XML_Archivo);
                ARCHIVO = XML_Archivo;
                return true;
            }
            catch (Exception errores)
            {
                string inner="";
                if(errores.InnerException!=null){
                    inner = errores.InnerException.Message.ToString();
                }
                MessageBox.Show("Error en generación. " + errores.Message.ToString()
                    + Environment.NewLine 
                    + inner);
                return false;

            }
        }

        private void labelX13_Click(object sender, EventArgs e)
        {

        }

        private void buttonX18_Click(object sender, EventArgs e)
        {
            verArchivo(PATH.Text);
        }

        private void buttonX17_Click(object sender, EventArgs e)
        {
            verArchivo(PATH.Text);
        }

        private void verArchivo(string archivo)
        {
            string directorio = AppDomain.CurrentDomain.BaseDirectory;
            if(File.Exists(archivo))
            {
                frmFacturaXML oObjeto = new frmFacturaXML(archivo);
                oObjeto.Show();
            }
        }

        private void buttonX4_Click_1(object sender, EventArgs e)
        {
            insertaLineaPoliza();
        }

        private void insertaLineaPoliza()
        {
            dtgPolizas.Rows.Add();
        }

        private void buttonX6_Click(object sender, EventArgs e)
        {
            eliminarLineaPoliza();
        }

        private void eliminarLineaPoliza()
        {
            this.dtgPolizas.EndEdit();
            if (this.dtgPolizas.SelectedRows.Count > 0)
            {
                DialogResult Resultado = MessageBox.Show("¿Esta seguro de eliminar las líneas de las pólizas seleccionadas?", Application.ProductName + "-" + Application.ProductVersion, MessageBoxButtons.YesNo);
                if (Resultado.ToString() != "Yes")
                {
                    return;
                }
                foreach (DataGridViewRow drv in this.dtgPolizas.SelectedRows)
                {
                    this.dtgPolizas.Rows.Remove(drv);
                }
                calcular();
            }
        }

        private void buttonX11_Click(object sender, EventArgs e)
        {
            eliminarLinea();
        }

        private void eliminarLinea()
        {
            dtgrdGeneral.EndEdit();
            if (dtgrdGeneral.SelectedRows.Count > 0)
            {
                DialogResult Resultado = MessageBox.Show("¿Esta seguro de eliminar las líneas seleccionadas?", Application.ProductName + "-" + Application.ProductVersion, MessageBoxButtons.YesNo);
                if (Resultado.ToString() != "Yes")
                {
                    return;
                }
                foreach (DataGridViewRow drv in dtgrdGeneral.SelectedRows)
                {
                    dtgrdGeneral.Rows.Remove(drv);
                }
                calcular();
            }
        }

        private void buttonX3_Click(object sender, EventArgs e)
        {
            generarPoliza();
        }

        public void generarPoliza()
        {
            dtgPolizas.SelectAll();
            dtgPolizas.EndEdit();
            if (dtgPolizas.SelectedCells.Count > 0)
            {
               var fileName = "Poliza " + oComprobante.serie + oComprobante.folio + " " + DateTime.Now.ToString("yyyy-MM-dd-hh-mm-ss") + ".csv";
                if (folderBrowserDialog1.ShowDialog() == DialogResult.OK)
                {
                    string archivo = fileName;
                    fileName = folderBrowserDialog1.SelectedPath + "\\" + fileName;
                    using (StreamWriter sw = File.AppendText(fileName))
                    {
                        foreach (DataGridViewRow drv in dtgPolizas.SelectedRows)
                        {
                            string registro_tmp = "";
                            registro_tmp = drv.Cells["ACCOUNT_ID"].Value.ToString() + "," + drv.Cells["REFERENCE"].Value.ToString() + "," + oData.formato_decimal(drv.Cells["CARGO"].Value.ToString(), "S") + "," + oData.formato_decimal(drv.Cells["ABONO"].Value.ToString(), "S") + Environment.NewLine;
                            sw.WriteLine(registro_tmp);
                        }
                        sw.Close();
                    }
                    var tempFileName = Path.GetTempFileName();
                    try
                    {
                        using (var streamReader = new StreamReader(fileName))
                        using (var streamWriter = new StreamWriter(tempFileName))
                        {
                            string line;
                            while ((line = streamReader.ReadLine()) != null)
                            {
                                if (!String.IsNullOrEmpty(line))
                                    streamWriter.WriteLine(line);
                            }
                        }
                        File.Copy(tempFileName, fileName, true);
                    }
                    finally
                    {
                        File.Delete(tempFileName);
                    }
                    MessageBoxEx.Show("Poliza Generada del archivo " + Environment.NewLine + fileName + " finalizada.", Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
            else
            {
                MessageBoxEx.Show("Seleccione alguna línea.", Application.ProductName + "-" + Application.ProductVersion, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }

        private void buttonX7_Click(object sender, EventArgs e)
        {
            imprimir();
        }

        private void imprimir(bool verPDF=true)
        {
            ReportDocument oReport = new ReportDocument();

            string FORMATO = FORMATO_IMPRESION.Text;
            if (!File.Exists(FORMATO))
            {
                MessageBoxEx.Show("El Reporte " + FORMATO + " no existe en su directorio. La factura no fue Generada.", Application.ProductName + "-" + Application.ProductVersion, MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            if (!File.Exists(PATH.Text))
            {
                return;
            }
            try
            {
                XmlSerializer serializer_CFD_3 = new XmlSerializer(typeof(Comprobante));
                TextReader reader = new StreamReader(PATH.Text);
                Comprobante oComprobanteLocal = (Comprobante)serializer_CFD_3.Deserialize(reader);
                //cfdiCE.comprobanteDataTable ocomprobanteDataTable = new cfdiCE.comprobanteDataTable();
                //ocomprobanteDataTable.AddcomprobanteRow(
                //    oComprobanteLocal.Emisor.rfc, oComprobanteLocal.Emisor.nombre, oComprobanteLocal.Emisor.RegimenFiscal[0].Regimen.ToString(), oComprobanteLocal.LugarExpedicion
                //    , oComprobanteLocal.Receptor.rfc, oComprobanteLocal.Receptor.nombre
                //    , 0
                //    , decimal.Parse(oComprobanteLocal.subTotal.ToString()), decimal.Parse(oComprobanteLocal.Impuestos.totalImpuestosTrasladados.ToString()), decimal.Parse(oComprobanteLocal.total.ToString())
                //    );
                cfdiCE.conceptosDataTable oconceptosDataTable = new cfdiCE.conceptosDataTable();
                foreach (ComprobanteConcepto concepto in oComprobanteLocal.Conceptos)
                {
                    oconceptosDataTable.AddconceptosRow(concepto.cantidad, concepto.descripcion
                        , concepto.noIdentificacion, concepto.valorUnitario, concepto.importe, 0,
                        concepto.unidad);
                }

                oReport.Load(FORMATO);
                //DataSet oDataSet = new DataSet();
                //oDataSet.ReadXml(PATH.Text);
                //oReport.SetDataSource(oDataSet);
                XmlDocument doc = new XmlDocument();
                doc.Load(PATH.Text);
                XmlNodeList TimbreFiscalDigital = doc.GetElementsByTagName("tfd:TimbreFiscalDigital");

                string UUID = "";
                UUID = TimbreFiscalDigital[0].Attributes["UUID"].Value;
                string FechaTimbrado = "";
                FechaTimbrado = TimbreFiscalDigital[0].Attributes["FechaTimbrado"].Value;
                string noCertificadoSAT = "";
                noCertificadoSAT = TimbreFiscalDigital[0].Attributes["noCertificadoSAT"].Value;
                string selloSAT = "";
                selloSAT = TimbreFiscalDigital[0].Attributes["selloSAT"].Value;

                byte[] image=generarImagen(PATH.Text);
                string cadenaOriginal = generarCadenaOriginal(PATH.Text); 
                string cadenaTFD=generarCadenaTFD(PATH.Text);
                cfdiCE.DATOSDataTable oDATOS = new cfdiCE.DATOSDataTable();
                oDATOS.AddDATOSRow(image, cadenaOriginal, cadenaTFD, direccionEmbarque.Text
                    , notasPDF.Text,0
                    , oComprobanteLocal.Emisor.rfc, oComprobanteLocal.Emisor.nombre, oComprobanteLocal.Emisor.RegimenFiscal[0].Regimen.ToString(), oComprobanteLocal.LugarExpedicion
                    , oComprobanteLocal.Receptor.rfc, oComprobanteLocal.Receptor.nombre
                    , oComprobanteLocal.fecha.ToString(),oComprobanteLocal.serie,oComprobanteLocal.folio.ToString(), oComprobanteLocal.noCertificado.ToString()
                    , oComprobanteLocal.tipoDeComprobante.ToString(),oComprobanteLocal.subTotal, oComprobanteLocal.Impuestos.totalImpuestosTrasladados, oComprobanteLocal.total
                    , UUID, oComprobanteLocal.formaDePago,  FechaTimbrado, noCertificadoSAT, selloSAT
                    , facturarA.Text
                    );

            
                
                //ConnectionInfo oConnectionInfo=new ConnectionInfo();
                //oConnectionInfo.ServerName=PATH.Text;
                //oConnectionInfo.DatabaseName = "NewDataset";
                //oConnectionInfo.UserID = "";
                //oConnectionInfo.Password = "";
                //CrystalDecisions.Shared.TableLogOnInfo crpTableLogOnInfo= new TableLogOnInfo();
                //foreach(CrystalDecisions.CrystalReports.Engine.Table tabla in oReport.Database.Tables ){
                //    tabla.LogOnInfo.ConnectionInfo.ServerName=PATH.Text;
                //    tabla.LogOnInfo.ConnectionInfo.Type = CrystalDecisions.Shared.ConnectionInfoType.MetaData;
                //}


                if (oReport.Database.Tables.Count > 0)
                {
                    //oReport.SetDataSource(oDataSet);
                    foreach (CrystalDecisions.CrystalReports.Engine.Table tabla in oReport.Database.Tables)
                    {
                        if (tabla.Name.ToString() == "DATOS")
                        {
                            oReport.Database.Tables["DATOS"].SetDataSource((System.Data.DataTable)oDATOS);
                        }
                        if (tabla.Name.ToString() == "conceptos")
                        {
                            oReport.Database.Tables["conceptos"].SetDataSource((System.Data.DataTable)oconceptosDataTable);
                        }
                        //if (tabla.Name.ToString() == "Comprobante")
                        //{
                        //    oReport.Database.Tables["Comprobante"].SetDataSource(oDataSet.Tables["cfdi:Comprobante"]);
                        //}
                        //if (tabla.Name.ToString() == "Conceptos")
                        //{
                        //    oReport.Database.Tables["Conceptos"].SetDataSource(oDataSet.Tables["cfdi:Conceptos"]);
                        //}
                        //if (tabla.Name.ToString() == "Concepto")
                        //{
                        //    oReport.Database.Tables["Concepto"].SetDataSource(oDataSet.Tables["cfdi:Concepto"]);
                        //}
                        //if (tabla.Name.ToString() == "Traslados")
                        //{
                        //    oReport.Database.Tables["Traslados"].SetDataSource(oDataSet.Tables["cfdi:Traslados"]);
                        //}
                        //if (tabla.Name.ToString() == "Traslado")
                        //{
                        //    oReport.Database.Tables["Traslado"].SetDataSource(oDataSet.Tables["cfdi:Traslado"]);
                        //}
                        //if (tabla.Name.ToString() == "Impuestos")
                        //{
                        //    oReport.Database.Tables["Impuestos"].SetDataSource(oDataSet.Tables["cfdi:Impuestos"]);
                        //}
                        //if (tabla.Name.ToString() == "RegimenFiscal")
                        //{
                        //    oReport.Database.Tables["RegimenFiscal"].SetDataSource(oDataSet.Tables["cfdi:RegimenFiscal"]);
                        //}
                        //if (tabla.Name.ToString() == "TimbreFiscalDigital")
                        //{
                        //    oReport.Database.Tables["TimbreFiscalDigital"].SetDataSource(oDataSet.Tables["cfdi:TimbreFiscalDigital"]);
                        //}
                        //if (tabla.Name.ToString() == "Complemento")
                        //{
                        //    oReport.Database.Tables["Complemento"].SetDataSource(oDataSet.Tables["cfdi:Complemento"]);
                        //}
                        //if (tabla.Name.ToString() == "Emisor")
                        //{
                        //    oReport.Database.Tables["Emisor"].SetDataSource(oDataSet.Tables["cfdi:Emisor"]);
                        //}
                        //if (tabla.Name.ToString() == "Receptor")
                        //{
                        //    if (oDataSet.Tables["cfdi:Receptor"] != null)
                        //    {
                        //        oReport.Database.Tables["Receptor"].SetDataSource(oDataSet.Tables["cfdi:Receptor"]);
                        //    }
                        
                        //}
                    }

                }


                try
                {
                    ExportOptions CrExportOptions = new ExportOptions();
                    DiskFileDestinationOptions CrDiskFileDestinationOptions = new DiskFileDestinationOptions();
                    PdfRtfWordFormatOptions CrFormatTypeOptions = new PdfRtfWordFormatOptions();
                    string pdftr = PATH.Text.Replace(".xml", ".pdf");
                    pdftr = pdftr.Replace(@"\xml", @"\pdf");
                    CrDiskFileDestinationOptions.DiskFileName = pdftr;
                    CrExportOptions = oReport.ExportOptions;
                    CrExportOptions.ExportDestinationType = ExportDestinationType.DiskFile;
                    CrExportOptions.ExportFormatType = ExportFormatType.PortableDocFormat;
                    CrExportOptions.DestinationOptions = CrDiskFileDestinationOptions;
                    CrExportOptions.FormatOptions = CrFormatTypeOptions;
                    oReport.Export();
                }
                catch(Exception e)
                {
                    MessageBox.Show(e.Message.ToString());
                }
                if (verPDF)
                {
                    frmCFDIImpresion ofrmReporte = new frmCFDIImpresion(oReport);
                    ofrmReporte.ShowDialog();
                }
            }
            catch (Exception err)
            {
                MessageBox.Show(err.Message.ToString());
            }
            

        }

        private string generarCadenaOriginal(string archivo)
        {
            XPathDocument myXPathDoc = new XPathDocument(archivo);
            //XslTransform myXslTrans = new XslTransform();
            XslCompiledTransform myXslTrans = new XslCompiledTransform();
            XslCompiledTransform trans = new XslCompiledTransform();
            XmlUrlResolver resolver = new XmlUrlResolver();

            resolver.Credentials = System.Net.CredentialCache.DefaultCredentials;
            XsltSettings settings = new XsltSettings(true, true);
            string sDirectory = AppDomain.CurrentDomain.BaseDirectory;
            //Cargar xslt
            try
            {
                //Version 3
                myXslTrans.Load(sDirectory + "/XSLT/cadenaoriginal_3_2.xslt");
            }
            catch (XmlException errores)
            {
                MessageBox.Show("Error. Conectarse en el Portal del SAT al " + errores.InnerException.ToString());
                return "";
            }

            //Crear Archivo Temporal
            string nombre_tmp = "TMP" + DateTime.Now.ToString("ddMMyyyyhhmmss");
            XmlTextWriter myWriter = new XmlTextWriter(nombre_tmp + ".TXT", null);

            //Transformar al XML
            myXslTrans.Transform(myXPathDoc, null, myWriter);

            myWriter.Close();
            string CADENA_ORIGINAL = "";
            //Abrir Archivo TXT
            StreamReader re = File.OpenText(nombre_tmp + ".TXT");
            string input = null;
            while ((input = re.ReadLine()) != null)
            {
                CADENA_ORIGINAL += input;
            }
            re.Close();

            return CADENA_ORIGINAL;
        }

        private string generarCadenaTFD(string archivo)
        {
            string nombre_tmp ="TMP" + DateTime.Now.ToString("ddMMyyyyhhmmss");
            XmlDocument doc = new XmlDocument();
            doc.Load(archivo);
            XslCompiledTransform myXslTrans = new XslCompiledTransform();
            XslCompiledTransform trans = new XslCompiledTransform();
            XmlUrlResolver resolver = new XmlUrlResolver();

            resolver.Credentials = System.Net.CredentialCache.DefaultCredentials;
            XsltSettings settings = new XsltSettings(true, true);

            //Cargar xslt
            try
            {
                myXslTrans.Load(AppDomain.CurrentDomain.BaseDirectory + "/XSLT/cadenaoriginal_TFD_1_1.xslt");
            }
            catch (XmlException errores)
            {
                MessageBox.Show("Error en " + errores.InnerException.ToString());
                return "";
            }

            //Crear Archivo Temporal

            XmlTextWriter myWriter = new XmlTextWriter(nombre_tmp + ".TXT", null);
            XmlNodeList TimbreFiscalDigital = doc.GetElementsByTagName("tfd:TimbreFiscalDigital");
            //Transformar al XML
            myXslTrans.Transform(TimbreFiscalDigital[0], null, myWriter);

            myWriter.Close();

            //Abrir Archivo TXT
            string cadenaoriginal_TFD_1_0 = "";
            StreamReader re = File.OpenText(nombre_tmp + ".TXT");
            string input = null;
            while ((input = re.ReadLine()) != null)
            {
                cadenaoriginal_TFD_1_0 += input;
            }
            re.Close();

            //Eliminar Archivos Temporales
            File.Delete(nombre_tmp + ".TXT");
            return cadenaoriginal_TFD_1_0;

        }

        private byte[] generarImagen(string archivo)
        {
            //?re=XAXX010101000&rr=XAXX010101000&tt=1234567890.123456&id=ad662d33-6934-459c-a128-BDf0393f0f44
            string QR_Code = "";
            XmlSerializer serializer_CFD_3 = new XmlSerializer(typeof(Comprobante));
            TextReader reader = new StreamReader(archivo);
            Comprobante oComprobanteLocal;
            try
            {
                XmlDocument doc = new XmlDocument();
                doc.Load(archivo);
                

                XmlNodeList TimbreFiscalDigital = doc.GetElementsByTagName("tfd:TimbreFiscalDigital");

                string UUID = "";
                UUID = TimbreFiscalDigital[0].Attributes["UUID"].Value;
                
                oComprobanteLocal = (Comprobante)serializer_CFD_3.Deserialize(reader);
                QR_Code += "?re=" + oComprobanteLocal.Emisor.rfc;
                QR_Code += "&rr=" + oComprobanteLocal.Receptor.rfc;
                QR_Code += "&tt=" + oComprobanteLocal.total.ToString();
                QR_Code += "&id=" + UUID;

            }
            catch (Exception xmlex)
            {
                Bitacora.Log("Errpr :: " + xmlex.ToString());
                return null;
            }
            int size = 320;
            QRCodeWriter writer = new QRCodeWriter();
            com.google.zxing.common.ByteMatrix matrix;
            matrix = writer.encode(QR_Code, com.google.zxing.BarcodeFormat.QR_CODE, size, size, null);
            Bitmap img = new Bitmap(size, size);
            Color Color = Color.FromArgb(0, 0, 0);

            for (int y = 0; y < matrix.Height; ++y)
            {
                for (int x = 0; x < matrix.Width; ++x)
                {
                    Color pixelColor = img.GetPixel(x, y);

                    //Find the colour of the dot
                    if (matrix.get_Renamed(x, y) == -1)
                    {
                        img.SetPixel(x, y, Color.White);
                    }
                    else
                    {
                        img.SetPixel(x, y, Color.Black);
                    }
                }
            }


            string nombre = Path.GetTempPath() + "QRP_TEMP.BMP";
            img.Save(nombre);

            FileStream fs = new FileStream(nombre, System.IO.FileMode.Open, System.IO.FileAccess.Read);
            byte[] Image = new byte[fs.Length];
            fs.Read(Image, 0, Convert.ToInt32(fs.Length));
            int image_32 = Convert.ToInt32(fs.Length);
            fs.Close();
            File.Delete(nombre);

            return Image;

        }

        private void buttonX3_Click_1(object sender, EventArgs e)
        {
            guardarPoliza();
        }

        private void guardarPoliza()
        {
            string TIPO_POLIZA = "Todas";
            dtgPolizas.SelectAll();
            dtgPolizas.EndEdit();
            if (dtgPolizas.SelectedCells.Count > 0)
            {

                foreach (DataGridViewRow drv in dtgPolizas.SelectedRows)
                {
                    cfdiPoliza ocfdiPoliza = new cfdiPoliza(oData);
                    ocfdiPoliza.cuentaContable = drv.Cells["ACCOUNT_ID"].Value.ToString();
                    ocfdiPoliza.referencia = drv.Cells["REFERENCE"].Value.ToString();
                    ocfdiPoliza.cargo = decimal.Parse(oData.formato_decimal(drv.Cells["CARGO"].Value.ToString(), "S"));
                    ocfdiPoliza.abono = decimal.Parse(oData.formato_decimal(drv.Cells["ABONO"].Value.ToString(), "S"));
                    ocfdiPoliza.fecha = FECHA.Value.ToShortDateString();
                    ocfdiPoliza.factura = oComprobante.serie + oComprobante.folio;
                    ocfdiPoliza.estado = "Guardado";
                    ocfdiPoliza.guardar();
                }
                MessageBoxEx.Show("Poliza Guardada.", Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
            else
            {
                MessageBoxEx.Show("Seleccione alguna línea.", Application.ProductName + "-" + Application.ProductVersion, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }

        private void buttonX8_Click(object sender, EventArgs e)
        {
            generarPoliza();
        }

        private void buttonX6_Click_1(object sender, EventArgs e)
        {
            eliminarLineaPoliza();
        }

        private void buttonX4_Click_2(object sender, EventArgs e)
        {
            insertaLineaPoliza();
        }

        private void MONEDA_SelectedIndexChanged(object sender, EventArgs e)
        {
            cargarTC();
        }

        private void FECHA_ValueChanged(object sender, EventArgs e)
        {
            cargarTC();
        }

        private void cargarTC()
        {
            string sSQL = @"SELECT TOP 1 SELL_RATE
                FROM  CURRENCY_EXCHANGE
                WHERE CURRENCY_ID = '" + MONEDA.Text + @"'  
                AND EFFECTIVE_DATE <= " + oData.convertir_fecha_general(FECHA.Value);

            DataTable oDataTableR = oData_ERP.EjecutarConsulta(sSQL);
            if (oDataTableR != null)
            {
                foreach (DataRow oDataRow1 in oDataTableR.Rows)
                {
                    TIPO_CAMBIO.Text = oDataRow1["SELL_RATE"].ToString();
                    return;
                }

                
            }
            TIPO_CAMBIO.Text = "1";
        }

        private void buttonX17_Click_1(object sender, EventArgs e)
        {
            verArchivo(PATH.Text);
        }

        private void labelX16_Click(object sender, EventArgs e)
        {

        }

        private void buttonX8_Click_1(object sender, EventArgs e)
        {
            generarPoliza();
        }

        private void buttonX13_Click(object sender, EventArgs e)
        {
            copiarExcel();
        }

        private void copiarExcel()
        {
            DataObject o = (DataObject)Clipboard.GetDataObject();
            if (o.GetDataPresent(DataFormats.Text))
            {
                if (dtgrdGeneral.RowCount > 0)
                    dtgrdGeneral.Rows.Clear();

                string[] pastedRows = Regex.Split(o.GetData(DataFormats.Text).ToString().TrimEnd("\r\n".ToCharArray()), "\r\n");
                foreach (string pastedRow in pastedRows)
                {
                    string[] pastedRowCells = pastedRow.Split(new char[] { '\t' });
                    if (pastedRowCells.Length != 7)
                    {
                        MessageBox.Show("Las celdas copiadas no tiene el mismo formato " + Environment.NewLine
                            + "La configuración es: " + Environment.NewLine
                            + "Cantidad " + Environment.NewLine
                            + "Unidad" + Environment.NewLine
                            + "Descripción" + Environment.NewLine
                            + "No Identificacion" + Environment.NewLine
                            + "Valor Unitario" + Environment.NewLine
                            + "Importe" + Environment.NewLine
                            + "Fracción"
                            , Application.ProductName + "-" + Application.ProductVersion, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                        return;
                    }
                    int n=dtgrdGeneral.Rows.Add();

                    using (DataGridViewRow myDataGridViewRow = dtgrdGeneral.Rows[n])
                    {
                        for (int i = 0; i < pastedRowCells.Length; i++)
                        {
                            myDataGridViewRow.Cells[i].Value = pastedRowCells[i];
                            if((i==4) | (i==5))
                            {
                                myDataGridViewRow.Cells[i].Value = pastedRowCells[i].ToString().Replace("$", "");
                            }
                        }
                        if (aplicarDescuentoAutomatico.Checked)
                        {
                            cargarLineaDescuento(n);
                        }
                        calcular();
                    }
                }
                if (aplicarDescuentoAutomatico.Checked)
                {
                    //cargarLineaDescuento();
                }
                
            }


        }

        private void cargarLineaDescuento()
        {
            
            //Sumatoria
            //decimal importe_tr = 0;
            int rows=dtgrdGeneral.Rows.Count;
            for (int i = 0; i < rows; i++)
            {
                DataGridViewRow registro = dtgrdGeneral.Rows[i];
                decimal valorUnitariotr=decimal.Parse(registro.Cells["valorUnitario"].Value.ToString().Replace("$",""));
                decimal importetr = decimal.Parse(registro.Cells["importe"].Value.ToString().Replace("$", ""));
                
                int n=dtgrdGeneral.Rows.Add();
                dtgrdGeneral.Rows[n].Cells["cantidad"].Value = registro.Cells["cantidad"].Value;
                dtgrdGeneral.Rows[n].Cells["descripcion"].Value = ocPolizaConfiguracion.ComentarioDescuento;
                dtgrdGeneral.Rows[n].Cells["unidad"].Value = "NA";
                dtgrdGeneral.Rows[n].Cells["valorUnitario"].Value = (valorUnitariotr * -1).ToString();
                dtgrdGeneral.Rows[n].Cells["importe"].Value = (importetr * -1).ToString();
                dtgrdGeneral.Rows[n].Cells["noIdentificacion"].Value = "";//registro.Cells["noIdentificacion"].Value;
                dtgrdGeneral.Rows[n].Cells["fraccionArancelaria"].Value = registro.Cells["fraccionArancelaria"].Value;
                

            }
            calcular();
        }

        private void cargarLineaDescuento(int nlinea)
        {

            //Sumatoria
            //decimal importe_tr = 0;

            DataGridViewRow registro = dtgrdGeneral.Rows[nlinea];
            decimal valorUnitariotr = decimal.Parse(registro.Cells["valorUnitario"].Value.ToString().Replace("$", ""));
            decimal importetr = decimal.Parse(registro.Cells["importe"].Value.ToString().Replace("$", ""));

            int n = dtgrdGeneral.Rows.Add();
            dtgrdGeneral.Rows[n].Cells["cantidad"].Value = registro.Cells["cantidad"].Value;
            dtgrdGeneral.Rows[n].Cells["descripcion"].Value = ocPolizaConfiguracion.ComentarioDescuento;
            dtgrdGeneral.Rows[n].Cells["unidad"].Value = "NA";
            dtgrdGeneral.Rows[n].Cells["valorUnitario"].Value = (valorUnitariotr * -1).ToString();
            dtgrdGeneral.Rows[n].Cells["importe"].Value = (importetr * -1).ToString();
            dtgrdGeneral.Rows[n].Cells["noIdentificacion"].Value = "";//registro.Cells["noIdentificacion"].Value;
            dtgrdGeneral.Rows[n].Cells["fraccionArancelaria"].Value = registro.Cells["fraccionArancelaria"].Value;

        }

        private void buttonX11_Click_1(object sender, EventArgs e)
        {
            eliminarLinea();
        }

        private void NumRegIdTrib_Leave(object sender, EventArgs e)
        {
            if (NumRegIdTrib.Text != "")
            {
                cargarCE();
            }
        }

        private void buttonX14_Click(object sender, EventArgs e)
        {
            pegarCE();
        }

        private void pegarCE()
        {
            DataObject o = (DataObject)Clipboard.GetDataObject();
            if (o.GetDataPresent(DataFormats.Text))
            {

                string[] pastedRows = Regex.Split(o.GetData(DataFormats.Text).ToString().TrimEnd("\r\n".ToCharArray()), "\r\n");
                foreach (string pastedRow in pastedRows)
                {
                    string[] pastedRowCells = pastedRow.Split(new char[] { '\t' });
                    if (pastedRowCells.Length != 7)
                    {
                        MessageBox.Show("Las celdas copiadas no tiene el mismo formato " + Environment.NewLine
                            + "La configuración es: " + Environment.NewLine
                            + "NumRegIdTrib " + Environment.NewLine
                            + "Pais" + Environment.NewLine
                            + "Calle" + Environment.NewLine
                            + "No Exterior" + Environment.NewLine
                            + "No Interior" + Environment.NewLine
                            + "Estado" + Environment.NewLine
                            + "Código Postal"
                            , Application.ProductName + "-" + Application.ProductVersion, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                        return;
                    }
                    for (int i = 0; i < pastedRowCells.Length; i++)
                    {
                        NumRegIdTrib.Text = pastedRowCells[0];
                            
                        if (NumRegIdTrib.Text != "")
                        {
                            string sDirectoryCE_tr = AppDomain.CurrentDomain.BaseDirectory + "cCEProveedor_" + NumRegIdTrib.Text;
                            if (File.Exists(sDirectoryCE_tr))
                            {
                                DialogResult Resultado = MessageBox.Show("¿Desea cargar los datos del Número de Registro Tributario?"
                                    , Application.ProductName + "-" + Application.ProductVersion, MessageBoxButtons.YesNo);
                                if (Resultado.ToString() == "Yes")
                                {
                                    cargarCE();
                                }
                            }
                        }
                        Pais.Text = pastedRowCells[1];
                        calle.Text = pastedRowCells[2];
                        noExterior.Text = pastedRowCells[3];
                        noInterior.Text = pastedRowCells[4];
                        EstadoCE.Text = pastedRowCells[5];
                        CodigoPostal.Text = pastedRowCells[6];

                        break;
                    }
                       
                    
                }
            }
        }

        private void buttonX7_Click_1(object sender, EventArgs e)
        {
            imprimir();
        }

        private void aplicarDescuentoAutomatico_Click(object sender, EventArgs e)
        {
            calcular();
        }

        private void buttonX15_Click(object sender, EventArgs e)
        {
            generarPlantilla();
        }

        private void buttonX16_Click(object sender, EventArgs e)
        {
            verArchivo(PATH.Text);
            
        }

        private void buttonX18_Click_1(object sender, EventArgs e)
        {
            enviar_mail(true);
        }

        private void enviar_mail(bool adjuntar)
        {
            // Specify the message content.
            MailMessage message = new MailMessage();
            try
            {
                cEMPRESA ocEMPRESA = new cEMPRESA(oData);
                ocEMPRESA.cargar((ENTITY_ID.SelectedItem as cEMPRESA).ROW_ID);
                ocEMPRESA.cargar(ocCERTIFICADO.ROW_ID_EMPRESA);
                if (!bool.Parse(ocEMPRESA.ENVIO_CORREO))
                {
                    return;
                }
                ocEMPRESA.cargar(ocCERTIFICADO.ROW_ID_EMPRESA);
                //Validar el envio automatico

                string FROM_CORREO = "";

                if (isEmail(PARA.Text))
                {
                    FROM_CORREO = PARA.Text;
                }
                else
                {
                    MessageBox.Show("El para " + PARA.Text + " no es correo.", Application.ProductName + "-" + Application.ProductVersion, MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }

                informacion.Text = "Configurando Envio " + " a las " + DateTime.Now.ToShortDateString() + " " + DateTime.Now.ToLongTimeString();

                MailAddress from = new MailAddress(FROM_CORREO, ocSERIE.REMITENTE,
                                System.Text.Encoding.UTF8);

                
                message.From = from;
                //Destinatarios.                
                //TO
                string[] rEmail = PARA.Text.Split(';');
                for (int i = 0; i < rEmail.Length; i++)
                {
                    if (!String.IsNullOrEmpty(rEmail[i].ToString().Trim()))
                    {
                        if (isEmail(rEmail[i].ToString().Trim()))
                        {
                            message.To.Add(rEmail[i].ToString().Trim());
                        }
                    }
                }

                if (message.To.Count == 0)
                {
                    MessageBox.Show("El correo no puede enviarse, no existen destinatarios.", Application.ProductName + "-" + Application.ProductVersion, MessageBoxButtons.OK, MessageBoxIcon.Error);
                    informacion.Text = "Error envio de Correo";
                    return;
                }

                //CC
                string[] rEmailCC = ocSERIE.CC.Split(';');
                for (int i = 0; i < rEmailCC.Length; i++)
                {
                    if (!String.IsNullOrEmpty(rEmailCC[i].ToString().Trim()))
                    {
                        if (isEmail(rEmailCC[i].ToString().Trim()))
                        {
                            message.CC.Add(rEmailCC[i].ToString().Trim());
                        }
                    }
                }

                //CCO
                string[] rEmailCCO = ocSERIE.CCO.Split(';');
                for (int i = 0; i < rEmailCC.Length; i++)
                {
                    if (!String.IsNullOrEmpty(rEmailCCO[i].ToString().Trim()))
                    {
                        if (isEmail(rEmailCCO[i].ToString().Trim()))
                        {
                            message.Bcc.Add(rEmailCCO[i].ToString().Trim());
                        }
                    }
                }


                informacion.Text = "Enviando " + " a las " + DateTime.Now.ToShortDateString() + " " + DateTime.Now.ToLongTimeString();

                if (message.To.Count == 0)
                {
                    MessageBox.Show("El correo no puede enviarse, no existen destinatarios.", Application.ProductName + "-" + Application.ProductVersion, MessageBoxButtons.OK, MessageBoxIcon.Error);
                    informacion.Text = "Error";
                    return;
                }
                //Configurar Mensaje
                string mensaje_enviar = MENSAJE.Text.Replace("<<NUMFACTURA>>", ID.Text);
                string asunto_enviar = ASUNTO.Text.Replace("<<NUMFACTURA>>", ID.Text);

                message.Body = mensaje_enviar;
                message.BodyEncoding = System.Text.Encoding.UTF8;

                message.Subject = asunto_enviar;
                message.SubjectEncoding = System.Text.Encoding.UTF8;
                message.IsBodyHtml = true;
                message.Priority = MailPriority.High;

                //Añadir Atachment
                Attachment data;
                string pdf = PATH.Text.Replace("xml", "pdf");
                string xml = PATH.Text;
                string factura = SERIE.Text+ID.Text;

                    //Adjuntar los Archivos XML y PDF
                if (!String.IsNullOrEmpty(xml.ToString().Trim()))
                    {
                        if (File.Exists(xml.ToString().Trim()))
                        {
                            data = new Attachment(xml.ToString().Trim());
                            data.Name = factura + ".xml";
                            message.Attachments.Add(data);
                        }
                        else
                        {
                            MessageBox.Show("El Archivo " + xml + " no existe, no es posible enviar el mail.", Application.ProductName + "-" + Application.ProductVersion, MessageBoxButtons.OK, MessageBoxIcon.Error);
                            return;
                        }
                    }


                if (!String.IsNullOrEmpty(pdf.ToString().Trim()))
                    {
                        if (File.Exists(pdf.ToString().Trim()))
                        {
                            data = new Attachment(pdf.ToString().Trim());
                            data.Name = factura + ".pdf";
                            message.Attachments.Add(data);
                        }
                        else
                        {
                            MessageBox.Show("El Archivo " + pdf + " no existe, no es posible enviar el mail.", Application.ProductName + "-" + Application.ProductVersion, MessageBoxButtons.OK, MessageBoxIcon.Error);
                            return;
                        }
                    }
                

                //Configurar SMPT
                SmtpClient client = new SmtpClient(ocEMPRESA.SMTP, int.Parse(ocEMPRESA.PUERTO));
                client.SendCompleted += new SendCompletedEventHandler(client_SendCompleted);
                //Configurar Crendiciales de Salida
                if (ocEMPRESA.SMTP.Trim() == "")
                {
                    MessageBox.Show("La configuración del correo electrónico esta erroneo.", Application.ProductName + "-" + Application.ProductVersion, MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }
                client.Host = ocEMPRESA.SMTP;
                client.Port = int.Parse(ocEMPRESA.PUERTO);
                client.DeliveryMethod = SmtpDeliveryMethod.Network;

                client.UseDefaultCredentials = bool.Parse(ocEMPRESA.CRENDENCIALES.ToString());
                client.EnableSsl = bool.Parse(ocEMPRESA.SSL.ToString());
                client.Timeout = 2000000;
                System.Net.NetworkCredential credenciales = new System.Net.NetworkCredential(ocEMPRESA.USUARIO, ocEMPRESA.PASSWORD_EMAIL);
                client.Credentials = credenciales;

                //            ServicePointManager.ServerCertificateValidationCallback =
                //delegate(object sender, X509Certificate certificate, X509Chain chain,
                //   SslPolicyErrors sslPolicyErrors) { return true; };

                object userState = "";
                userState = "";
                client.Send(message);
            }
            catch (System.Net.Mail.SmtpException ex)
            {
                MessageBox.Show("Envio de correo: " + ex.Message, Application.ProductName + "-" + Application.ProductVersion, MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            message.Dispose();
            message = null;
            return;
        }

        public void client_SendCompleted(object sender, AsyncCompletedEventArgs e)
        {
            try
            {
                string VALORES = (string)e.UserState;
                if (e.Error != null)
                {
                    MessageBox.Show("Envio de correo: " + e.Error.Message.ToString());

                    //MessageBox.Show("Error de Envio " + e.Error.InnerException.ToString());
                }
                else
                {
                    //MessageBox.Show("Enviado " + " a las " + DateTime.Now.ToShortDateString() + " " + DateTime.Now.ToLongTimeString());
                    informacion.Text = "Enviado " + " a las " + DateTime.Now.ToShortDateString() + " " + DateTime.Now.ToLongTimeString();
                }
            }
            catch (Exception error_envio)
            {
                MessageBox.Show(error_envio.Data.ToString() + Environment.NewLine + error_envio.Source.ToString());
            }

        }

        public static bool isEmail(string inputEmail)
        {
            if (inputEmail == null || inputEmail.Length == 0)
            {
                return false;
            }

            const string expression = @"^([a-zA-Z0-9_\-\.]+)@((\[[0-9]{1,3}" +
                                      @"\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9\-]+\" +
                                      @".)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$";

            Regex regex = new Regex(expression);
            return regex.IsMatch(inputEmail);
        }

        private void buttonX19_Click(object sender, EventArgs e)
        {
            verArchivo(PATH.Text.Replace("xml", "pdf"));
        }

        private void FECHA_ValueChanged_1(object sender, EventArgs e)
        {
            buscaTCvisual();
        }

        private void buscaTCvisual()
        {
            try
            {
                TIPO_CAMBIO.Text = "1";
                if (MONEDA.Text.Contains("USD"))
                {
                    string sSQL = "";
                    sSQL = @"SELECT TOP 1 *
                    FROM CURRENCY_EXCHANGE
                    WHERE (EFFECTIVE_DATE <= " + oData.convertir_fecha(FECHA.Value) + @")
                    AND CURRENCY_ID='USD' 
                    ORDER BY EFFECTIVE_DATE DESC";
                    DataTable oDataTable = oData_ERP.EjecutarConsulta(sSQL);
                    if (oDataTable != null)
                    {
                        foreach (DataRow oDataRow in oDataTable.Rows)
                        {
                            //Añadir el Registro
                            TIPO_CAMBIO.Text = oDataRow["SELL_RATE"].ToString();
                        }
                    }
                }

            }
            catch
            {

            }
        }

        private void MONEDA_SelectedIndexChanged_1(object sender, EventArgs e)
        {
            buscaTCvisual();
        }

        private void rfc_TextChanged(object sender, EventArgs e)
        {

        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void dtgrdGeneral_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void TIPO_FACTURA_SelectedIndexChanged(object sender, EventArgs e)
        {
            cambiarTraslado();
        }

        private void cambiarTraslado()
        {
            motivoTraslado.Enabled = false;
            notasPDF.Text = "";
            tabControl1.TabPages.Remove(tabPage3);
            if (TIPO_FACTURA.Text.Contains("traslado"))
            {
                notasPDF.Text="THIS INVOICE IS ONLY FOR CUSTOMS PURPOSES - NOT FOR CUSTOMER REFERENCE";
                motivoTraslado.Enabled = true;
                tabControl1.TabPages.Add(tabPage3);
            }
        }

        private void buttonX9_Click(object sender, EventArgs e)
        {
            //cancelar();
        }
    }
}
