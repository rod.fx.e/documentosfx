﻿namespace FE_FX
{
    partial class frmCFDIConceptoCCE
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmCFDIConceptoCCE));
            this.button3 = new System.Windows.Forms.Button();
            this.button7 = new System.Windows.Forms.Button();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.UNIDAD = new System.Windows.Forms.ComboBox();
            this.NO_IDENTIFICACION = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.DESCRIPCION = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.IMPORTE = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.VALOR_UNITARIO = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.CANTIDAD = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.label4 = new DevComponents.DotNetBar.LabelX();
            this.label3 = new DevComponents.DotNetBar.LabelX();
            this.label2 = new DevComponents.DotNetBar.LabelX();
            this.label1 = new DevComponents.DotNetBar.LabelX();
            this.labelX1 = new DevComponents.DotNetBar.LabelX();
            this.fraccionArancelaria = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.labelX2 = new DevComponents.DotNetBar.LabelX();
            this.buttonX5 = new DevComponents.DotNetBar.ButtonX();
            this.labelX3 = new DevComponents.DotNetBar.LabelX();
            this.aduana = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.labelX4 = new DevComponents.DotNetBar.LabelX();
            this.numero = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.labelX5 = new DevComponents.DotNetBar.LabelX();
            this.labelX6 = new DevComponents.DotNetBar.LabelX();
            this.ClaveProdServ = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.ClaveUnidad = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.labelX7 = new DevComponents.DotNetBar.LabelX();
            this.labelX8 = new DevComponents.DotNetBar.LabelX();
            this.Marca = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.labelX9 = new DevComponents.DotNetBar.LabelX();
            this.Modelo = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.labelX10 = new DevComponents.DotNetBar.LabelX();
            this.Submodelo = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.labelX11 = new DevComponents.DotNetBar.LabelX();
            this.NumeroSerie = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.labelX12 = new DevComponents.DotNetBar.LabelX();
            this.ValorUnitarioAduana = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.labelX13 = new DevComponents.DotNetBar.LabelX();
            this.ValorDolares = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.labelX14 = new DevComponents.DotNetBar.LabelX();
            this.CantidadAduana = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.fecha = new System.Windows.Forms.DateTimePicker();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.SuspendLayout();
            // 
            // button3
            // 
            this.button3.BackColor = System.Drawing.SystemColors.Control;
            this.button3.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button3.Image = global::FE_FX.Properties.Resources.Guardar;
            this.button3.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button3.Location = new System.Drawing.Point(12, 12);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(83, 23);
            this.button3.TabIndex = 0;
            this.button3.Text = "Guardar";
            this.button3.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.button3.UseVisualStyleBackColor = false;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // button7
            // 
            this.button7.BackColor = System.Drawing.SystemColors.Control;
            this.button7.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button7.Image = global::FE_FX.Properties.Resources.Eliminar_Linea;
            this.button7.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button7.Location = new System.Drawing.Point(101, 12);
            this.button7.Name = "button7";
            this.button7.Size = new System.Drawing.Size(83, 23);
            this.button7.TabIndex = 1;
            this.button7.Text = "Eliminar";
            this.button7.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.button7.UseVisualStyleBackColor = false;
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.FileName = "openFileDialog1";
            // 
            // UNIDAD
            // 
            this.UNIDAD.FormattingEnabled = true;
            this.UNIDAD.Items.AddRange(new object[] {
            "H87",
            "EA",
            "E48",
            "ACT",
            "KGM",
            "E51",
            "A9",
            "MTR",
            "AB",
            "BB",
            "KT",
            "SET",
            "LTR",
            "XBX",
            "MON",
            "HUR",
            "MTK",
            "11",
            "MGM",
            "XPK",
            "XKI",
            "AS",
            "GRM",
            "PR",
            "DPC",
            "xun",
            "DAY",
            "XLT",
            "10",
            "MLT",
            "E54"});
            this.UNIDAD.Location = new System.Drawing.Point(264, 41);
            this.UNIDAD.Name = "UNIDAD";
            this.UNIDAD.Size = new System.Drawing.Size(121, 21);
            this.UNIDAD.TabIndex = 5;
            this.UNIDAD.SelectedIndexChanged += new System.EventHandler(this.UNIDAD_SelectedIndexChanged);
            // 
            // NO_IDENTIFICACION
            // 
            this.NO_IDENTIFICACION.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            // 
            // 
            // 
            this.NO_IDENTIFICACION.Border.Class = "TextBoxBorder";
            this.NO_IDENTIFICACION.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.NO_IDENTIFICACION.ForeColor = System.Drawing.Color.Black;
            this.NO_IDENTIFICACION.Location = new System.Drawing.Point(129, 93);
            this.NO_IDENTIFICACION.Name = "NO_IDENTIFICACION";
            this.NO_IDENTIFICACION.Size = new System.Drawing.Size(256, 20);
            this.NO_IDENTIFICACION.TabIndex = 11;
            // 
            // DESCRIPCION
            // 
            this.DESCRIPCION.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            // 
            // 
            // 
            this.DESCRIPCION.Border.Class = "TextBoxBorder";
            this.DESCRIPCION.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.DESCRIPCION.ForeColor = System.Drawing.Color.Black;
            this.DESCRIPCION.Location = new System.Drawing.Point(129, 119);
            this.DESCRIPCION.Multiline = true;
            this.DESCRIPCION.Name = "DESCRIPCION";
            this.DESCRIPCION.Size = new System.Drawing.Size(490, 50);
            this.DESCRIPCION.TabIndex = 13;
            // 
            // IMPORTE
            // 
            this.IMPORTE.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            // 
            // 
            // 
            this.IMPORTE.Border.Class = "TextBoxBorder";
            this.IMPORTE.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.IMPORTE.ForeColor = System.Drawing.Color.Black;
            this.IMPORTE.Location = new System.Drawing.Point(302, 175);
            this.IMPORTE.Name = "IMPORTE";
            this.IMPORTE.Size = new System.Drawing.Size(111, 20);
            this.IMPORTE.TabIndex = 17;
            this.IMPORTE.Text = "0";
            this.IMPORTE.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.IMPORTE.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.Solo_Numero_KeyPress);
            this.IMPORTE.Leave += new System.EventHandler(this.CANTIDAD_Leave);
            // 
            // VALOR_UNITARIO
            // 
            this.VALOR_UNITARIO.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            // 
            // 
            // 
            this.VALOR_UNITARIO.Border.Class = "TextBoxBorder";
            this.VALOR_UNITARIO.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.VALOR_UNITARIO.ForeColor = System.Drawing.Color.Black;
            this.VALOR_UNITARIO.Location = new System.Drawing.Point(129, 175);
            this.VALOR_UNITARIO.Name = "VALOR_UNITARIO";
            this.VALOR_UNITARIO.Size = new System.Drawing.Size(121, 20);
            this.VALOR_UNITARIO.TabIndex = 15;
            this.VALOR_UNITARIO.Text = "0";
            this.VALOR_UNITARIO.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.VALOR_UNITARIO.TextChanged += new System.EventHandler(this.VALOR_UNITARIO_TextChanged);
            this.VALOR_UNITARIO.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.Solo_Numero_KeyPress);
            this.VALOR_UNITARIO.Leave += new System.EventHandler(this.CANTIDAD_Leave);
            // 
            // CANTIDAD
            // 
            this.CANTIDAD.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            // 
            // 
            // 
            this.CANTIDAD.Border.Class = "TextBoxBorder";
            this.CANTIDAD.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.CANTIDAD.ForeColor = System.Drawing.Color.Black;
            this.CANTIDAD.Location = new System.Drawing.Point(129, 41);
            this.CANTIDAD.Name = "CANTIDAD";
            this.CANTIDAD.Size = new System.Drawing.Size(82, 20);
            this.CANTIDAD.TabIndex = 3;
            this.CANTIDAD.Text = "0";
            this.CANTIDAD.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.CANTIDAD.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.Solo_Numero_KeyPress);
            this.CANTIDAD.Leave += new System.EventHandler(this.CANTIDAD_Leave);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            // 
            // 
            // 
            this.label4.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.label4.Location = new System.Drawing.Point(256, 178);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(40, 15);
            this.label4.TabIndex = 16;
            this.label4.Text = "Importe";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            // 
            // 
            // 
            this.label3.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.label3.Location = new System.Drawing.Point(89, 178);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(34, 15);
            this.label3.TabIndex = 14;
            this.label3.Text = "Precio";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            // 
            // 
            // 
            this.label2.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.label2.Location = new System.Drawing.Point(217, 44);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(37, 15);
            this.label2.TabIndex = 4;
            this.label2.Text = "Unidad";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            // 
            // 
            // 
            this.label1.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.label1.Location = new System.Drawing.Point(76, 44);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(47, 15);
            this.label1.TabIndex = 2;
            this.label1.Text = "Cantidad";
            // 
            // labelX1
            // 
            this.labelX1.AutoSize = true;
            // 
            // 
            // 
            this.labelX1.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX1.Location = new System.Drawing.Point(63, 120);
            this.labelX1.Name = "labelX1";
            this.labelX1.Size = new System.Drawing.Size(60, 15);
            this.labelX1.TabIndex = 12;
            this.labelX1.Text = "Descripción";
            // 
            // fraccionArancelaria
            // 
            this.fraccionArancelaria.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            // 
            // 
            // 
            this.fraccionArancelaria.Border.Class = "TextBoxBorder";
            this.fraccionArancelaria.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.fraccionArancelaria.ForeColor = System.Drawing.Color.Black;
            this.fraccionArancelaria.Location = new System.Drawing.Point(113, 45);
            this.fraccionArancelaria.Name = "fraccionArancelaria";
            this.fraccionArancelaria.Size = new System.Drawing.Size(121, 20);
            this.fraccionArancelaria.TabIndex = 25;
            // 
            // labelX2
            // 
            this.labelX2.AutoSize = true;
            // 
            // 
            // 
            this.labelX2.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX2.Location = new System.Drawing.Point(4, 46);
            this.labelX2.Name = "labelX2";
            this.labelX2.Size = new System.Drawing.Size(103, 15);
            this.labelX2.TabIndex = 24;
            this.labelX2.Text = "Fracción Arancelaría";
            // 
            // buttonX5
            // 
            this.buttonX5.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.buttonX5.BackColor = System.Drawing.Color.SkyBlue;
            this.buttonX5.ColorTable = DevComponents.DotNetBar.eButtonColor.Flat;
            this.buttonX5.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonX5.Image = global::FE_FX.Properties.Resources.ic_search_black_18dp_1x;
            this.buttonX5.Location = new System.Drawing.Point(12, 93);
            this.buttonX5.Name = "buttonX5";
            this.buttonX5.Size = new System.Drawing.Size(111, 20);
            this.buttonX5.Style = DevComponents.DotNetBar.eDotNetBarStyle.Metro;
            this.buttonX5.TabIndex = 10;
            this.buttonX5.Text = "No identificación";
            this.buttonX5.TextAlignment = DevComponents.DotNetBar.eButtonTextAlignment.Right;
            this.buttonX5.Click += new System.EventHandler(this.buttonX5_Click);
            // 
            // labelX3
            // 
            this.labelX3.AutoSize = true;
            // 
            // 
            // 
            this.labelX3.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX3.Location = new System.Drawing.Point(16, 22);
            this.labelX3.Name = "labelX3";
            this.labelX3.Size = new System.Drawing.Size(40, 15);
            this.labelX3.TabIndex = 18;
            this.labelX3.Text = "Aduana";
            // 
            // aduana
            // 
            this.aduana.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            // 
            // 
            // 
            this.aduana.Border.Class = "TextBoxBorder";
            this.aduana.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.aduana.ForeColor = System.Drawing.Color.Black;
            this.aduana.Location = new System.Drawing.Point(57, 19);
            this.aduana.Name = "aduana";
            this.aduana.Size = new System.Drawing.Size(121, 20);
            this.aduana.TabIndex = 19;
            // 
            // labelX4
            // 
            this.labelX4.AutoSize = true;
            // 
            // 
            // 
            this.labelX4.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX4.Location = new System.Drawing.Point(184, 22);
            this.labelX4.Name = "labelX4";
            this.labelX4.Size = new System.Drawing.Size(33, 15);
            this.labelX4.TabIndex = 20;
            this.labelX4.Text = "Fecha";
            // 
            // numero
            // 
            this.numero.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            // 
            // 
            // 
            this.numero.Border.Class = "TextBoxBorder";
            this.numero.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.numero.ForeColor = System.Drawing.Color.Black;
            this.numero.Location = new System.Drawing.Point(395, 19);
            this.numero.Name = "numero";
            this.numero.Size = new System.Drawing.Size(215, 20);
            this.numero.TabIndex = 23;
            // 
            // labelX5
            // 
            this.labelX5.AutoSize = true;
            // 
            // 
            // 
            this.labelX5.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX5.Location = new System.Drawing.Point(347, 22);
            this.labelX5.Name = "labelX5";
            this.labelX5.Size = new System.Drawing.Size(42, 15);
            this.labelX5.TabIndex = 22;
            this.labelX5.Text = "Número";
            // 
            // labelX6
            // 
            this.labelX6.AutoSize = true;
            // 
            // 
            // 
            this.labelX6.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX6.Location = new System.Drawing.Point(46, 70);
            this.labelX6.Name = "labelX6";
            this.labelX6.Size = new System.Drawing.Size(77, 15);
            this.labelX6.TabIndex = 8;
            this.labelX6.Text = "ClaveProdServ";
            // 
            // ClaveProdServ
            // 
            this.ClaveProdServ.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            // 
            // 
            // 
            this.ClaveProdServ.Border.Class = "TextBoxBorder";
            this.ClaveProdServ.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.ClaveProdServ.ForeColor = System.Drawing.Color.Black;
            this.ClaveProdServ.Location = new System.Drawing.Point(129, 67);
            this.ClaveProdServ.Name = "ClaveProdServ";
            this.ClaveProdServ.Size = new System.Drawing.Size(82, 20);
            this.ClaveProdServ.TabIndex = 9;
            // 
            // ClaveUnidad
            // 
            this.ClaveUnidad.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            // 
            // 
            // 
            this.ClaveUnidad.Border.Class = "TextBoxBorder";
            this.ClaveUnidad.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.ClaveUnidad.ForeColor = System.Drawing.Color.Black;
            this.ClaveUnidad.Location = new System.Drawing.Point(459, 41);
            this.ClaveUnidad.Name = "ClaveUnidad";
            this.ClaveUnidad.Size = new System.Drawing.Size(105, 20);
            this.ClaveUnidad.TabIndex = 7;
            // 
            // labelX7
            // 
            this.labelX7.AutoSize = true;
            // 
            // 
            // 
            this.labelX7.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX7.Location = new System.Drawing.Point(387, 44);
            this.labelX7.Name = "labelX7";
            this.labelX7.Size = new System.Drawing.Size(66, 15);
            this.labelX7.TabIndex = 6;
            this.labelX7.Text = "ClaveUnidad";
            // 
            // labelX8
            // 
            this.labelX8.AutoSize = true;
            // 
            // 
            // 
            this.labelX8.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX8.Location = new System.Drawing.Point(15, 19);
            this.labelX8.Name = "labelX8";
            this.labelX8.Size = new System.Drawing.Size(33, 15);
            this.labelX8.TabIndex = 0;
            this.labelX8.Text = "Marca";
            // 
            // Marca
            // 
            this.Marca.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            // 
            // 
            // 
            this.Marca.Border.Class = "TextBoxBorder";
            this.Marca.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.Marca.ForeColor = System.Drawing.Color.Black;
            this.Marca.Location = new System.Drawing.Point(54, 16);
            this.Marca.Name = "Marca";
            this.Marca.Size = new System.Drawing.Size(204, 20);
            this.Marca.TabIndex = 1;
            // 
            // labelX9
            // 
            this.labelX9.AutoSize = true;
            // 
            // 
            // 
            this.labelX9.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX9.Location = new System.Drawing.Point(9, 45);
            this.labelX9.Name = "labelX9";
            this.labelX9.Size = new System.Drawing.Size(39, 15);
            this.labelX9.TabIndex = 4;
            this.labelX9.Text = "Modelo";
            // 
            // Modelo
            // 
            this.Modelo.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            // 
            // 
            // 
            this.Modelo.Border.Class = "TextBoxBorder";
            this.Modelo.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.Modelo.ForeColor = System.Drawing.Color.Black;
            this.Modelo.Location = new System.Drawing.Point(54, 42);
            this.Modelo.Name = "Modelo";
            this.Modelo.Size = new System.Drawing.Size(204, 20);
            this.Modelo.TabIndex = 5;
            // 
            // labelX10
            // 
            this.labelX10.AutoSize = true;
            // 
            // 
            // 
            this.labelX10.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX10.Location = new System.Drawing.Point(264, 45);
            this.labelX10.Name = "labelX10";
            this.labelX10.Size = new System.Drawing.Size(58, 15);
            this.labelX10.TabIndex = 6;
            this.labelX10.Text = "Submodelo";
            // 
            // Submodelo
            // 
            this.Submodelo.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            // 
            // 
            // 
            this.Submodelo.Border.Class = "TextBoxBorder";
            this.Submodelo.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.Submodelo.ForeColor = System.Drawing.Color.Black;
            this.Submodelo.Location = new System.Drawing.Point(328, 42);
            this.Submodelo.Name = "Submodelo";
            this.Submodelo.Size = new System.Drawing.Size(282, 20);
            this.Submodelo.TabIndex = 7;
            // 
            // labelX11
            // 
            this.labelX11.AutoSize = true;
            // 
            // 
            // 
            this.labelX11.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX11.Location = new System.Drawing.Point(264, 19);
            this.labelX11.Name = "labelX11";
            this.labelX11.Size = new System.Drawing.Size(85, 15);
            this.labelX11.TabIndex = 2;
            this.labelX11.Text = "Numero de Serie";
            // 
            // NumeroSerie
            // 
            this.NumeroSerie.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            // 
            // 
            // 
            this.NumeroSerie.Border.Class = "TextBoxBorder";
            this.NumeroSerie.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.NumeroSerie.ForeColor = System.Drawing.Color.Black;
            this.NumeroSerie.Location = new System.Drawing.Point(355, 16);
            this.NumeroSerie.Name = "NumeroSerie";
            this.NumeroSerie.Size = new System.Drawing.Size(255, 20);
            this.NumeroSerie.TabIndex = 3;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.labelX8);
            this.groupBox1.Controls.Add(this.NumeroSerie);
            this.groupBox1.Controls.Add(this.Marca);
            this.groupBox1.Controls.Add(this.labelX11);
            this.groupBox1.Controls.Add(this.labelX9);
            this.groupBox1.Controls.Add(this.Submodelo);
            this.groupBox1.Controls.Add(this.Modelo);
            this.groupBox1.Controls.Add(this.labelX10);
            this.groupBox1.Location = new System.Drawing.Point(12, 345);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(616, 70);
            this.groupBox1.TabIndex = 32;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Descripción Especifica";
            // 
            // labelX12
            // 
            this.labelX12.AutoSize = true;
            // 
            // 
            // 
            this.labelX12.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX12.Location = new System.Drawing.Point(209, 22);
            this.labelX12.Name = "labelX12";
            this.labelX12.Size = new System.Drawing.Size(110, 15);
            this.labelX12.TabIndex = 28;
            this.labelX12.Text = "Valor Unitario Aduana";
            // 
            // ValorUnitarioAduana
            // 
            this.ValorUnitarioAduana.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            // 
            // 
            // 
            this.ValorUnitarioAduana.Border.Class = "TextBoxBorder";
            this.ValorUnitarioAduana.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.ValorUnitarioAduana.ForeColor = System.Drawing.Color.Black;
            this.ValorUnitarioAduana.Location = new System.Drawing.Point(325, 19);
            this.ValorUnitarioAduana.Name = "ValorUnitarioAduana";
            this.ValorUnitarioAduana.Size = new System.Drawing.Size(89, 20);
            this.ValorUnitarioAduana.TabIndex = 29;
            // 
            // labelX13
            // 
            this.labelX13.AutoSize = true;
            // 
            // 
            // 
            this.labelX13.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX13.Location = new System.Drawing.Point(420, 22);
            this.labelX13.Name = "labelX13";
            this.labelX13.Size = new System.Drawing.Size(69, 15);
            this.labelX13.TabIndex = 30;
            this.labelX13.Text = "Valor Dolares";
            // 
            // ValorDolares
            // 
            this.ValorDolares.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            // 
            // 
            // 
            this.ValorDolares.Border.Class = "TextBoxBorder";
            this.ValorDolares.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.ValorDolares.ForeColor = System.Drawing.Color.Black;
            this.ValorDolares.Location = new System.Drawing.Point(495, 19);
            this.ValorDolares.Name = "ValorDolares";
            this.ValorDolares.Size = new System.Drawing.Size(108, 20);
            this.ValorDolares.TabIndex = 31;
            // 
            // labelX14
            // 
            this.labelX14.AutoSize = true;
            // 
            // 
            // 
            this.labelX14.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX14.Location = new System.Drawing.Point(25, 22);
            this.labelX14.Name = "labelX14";
            this.labelX14.Size = new System.Drawing.Size(87, 15);
            this.labelX14.TabIndex = 26;
            this.labelX14.Text = "Cantidad Aduana";
            // 
            // CantidadAduana
            // 
            this.CantidadAduana.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            // 
            // 
            // 
            this.CantidadAduana.Border.Class = "TextBoxBorder";
            this.CantidadAduana.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.CantidadAduana.ForeColor = System.Drawing.Color.Black;
            this.CantidadAduana.Location = new System.Drawing.Point(113, 19);
            this.CantidadAduana.Name = "CantidadAduana";
            this.CantidadAduana.Size = new System.Drawing.Size(89, 20);
            this.CantidadAduana.TabIndex = 27;
            // 
            // fecha
            // 
            this.fecha.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.fecha.Location = new System.Drawing.Point(230, 19);
            this.fecha.Name = "fecha";
            this.fecha.Size = new System.Drawing.Size(111, 20);
            this.fecha.TabIndex = 21;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.labelX2);
            this.groupBox2.Controls.Add(this.fraccionArancelaria);
            this.groupBox2.Controls.Add(this.labelX12);
            this.groupBox2.Controls.Add(this.labelX14);
            this.groupBox2.Controls.Add(this.ValorUnitarioAduana);
            this.groupBox2.Controls.Add(this.CantidadAduana);
            this.groupBox2.Controls.Add(this.labelX13);
            this.groupBox2.Controls.Add(this.ValorDolares);
            this.groupBox2.Location = new System.Drawing.Point(12, 201);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(616, 80);
            this.groupBox2.TabIndex = 33;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Mercancia";
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.aduana);
            this.groupBox3.Controls.Add(this.fecha);
            this.groupBox3.Controls.Add(this.labelX3);
            this.groupBox3.Controls.Add(this.labelX4);
            this.groupBox3.Controls.Add(this.labelX5);
            this.groupBox3.Controls.Add(this.numero);
            this.groupBox3.Location = new System.Drawing.Point(12, 287);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(616, 52);
            this.groupBox3.TabIndex = 34;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Pedimento";
            // 
            // frmCFDIConceptoCCE
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(635, 423);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.ClaveUnidad);
            this.Controls.Add(this.labelX7);
            this.Controls.Add(this.buttonX5);
            this.Controls.Add(this.ClaveProdServ);
            this.Controls.Add(this.labelX6);
            this.Controls.Add(this.UNIDAD);
            this.Controls.Add(this.NO_IDENTIFICACION);
            this.Controls.Add(this.DESCRIPCION);
            this.Controls.Add(this.IMPORTE);
            this.Controls.Add(this.VALOR_UNITARIO);
            this.Controls.Add(this.CANTIDAD);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.labelX1);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.button7);
            this.Controls.Add(this.button3);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.KeyPreview = true;
            this.MinimizeBox = false;
            this.Name = "frmCFDIConceptoCCE";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Concepto";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Button button7;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private System.Windows.Forms.ComboBox UNIDAD;
        private DevComponents.DotNetBar.Controls.TextBoxX NO_IDENTIFICACION;
        private DevComponents.DotNetBar.Controls.TextBoxX DESCRIPCION;
        private DevComponents.DotNetBar.Controls.TextBoxX IMPORTE;
        private DevComponents.DotNetBar.Controls.TextBoxX VALOR_UNITARIO;
        private DevComponents.DotNetBar.Controls.TextBoxX CANTIDAD;
        private DevComponents.DotNetBar.LabelX label4;
        private DevComponents.DotNetBar.LabelX label3;
        private DevComponents.DotNetBar.LabelX label2;
        private DevComponents.DotNetBar.LabelX label1;
        private DevComponents.DotNetBar.LabelX labelX1;
        private DevComponents.DotNetBar.Controls.TextBoxX fraccionArancelaria;
        private DevComponents.DotNetBar.LabelX labelX2;
        private DevComponents.DotNetBar.ButtonX buttonX5;
        private DevComponents.DotNetBar.LabelX labelX3;
        private DevComponents.DotNetBar.Controls.TextBoxX aduana;
        private DevComponents.DotNetBar.LabelX labelX4;
        private DevComponents.DotNetBar.Controls.TextBoxX numero;
        private DevComponents.DotNetBar.LabelX labelX5;
        private DevComponents.DotNetBar.LabelX labelX6;
        private DevComponents.DotNetBar.Controls.TextBoxX ClaveProdServ;
        private DevComponents.DotNetBar.Controls.TextBoxX ClaveUnidad;
        private DevComponents.DotNetBar.LabelX labelX7;
        private DevComponents.DotNetBar.LabelX labelX8;
        private DevComponents.DotNetBar.Controls.TextBoxX Marca;
        private DevComponents.DotNetBar.LabelX labelX9;
        private DevComponents.DotNetBar.Controls.TextBoxX Modelo;
        private DevComponents.DotNetBar.LabelX labelX10;
        private DevComponents.DotNetBar.Controls.TextBoxX Submodelo;
        private DevComponents.DotNetBar.LabelX labelX11;
        private DevComponents.DotNetBar.Controls.TextBoxX NumeroSerie;
        private System.Windows.Forms.GroupBox groupBox1;
        private DevComponents.DotNetBar.LabelX labelX12;
        private DevComponents.DotNetBar.Controls.TextBoxX ValorUnitarioAduana;
        private DevComponents.DotNetBar.LabelX labelX13;
        private DevComponents.DotNetBar.Controls.TextBoxX ValorDolares;
        private DevComponents.DotNetBar.LabelX labelX14;
        private DevComponents.DotNetBar.Controls.TextBoxX CantidadAduana;
        private System.Windows.Forms.DateTimePicker fecha;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.GroupBox groupBox3;
    }
}