﻿namespace FE_FX
{
    partial class aplicarDescuento
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle7 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle8 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle9 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(aplicarDescuento));
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.informacion = new System.Windows.Forms.ToolStripStatusLabel();
            this.comboItem4 = new DevComponents.Editors.ComboItem();
            this.comboItem5 = new DevComponents.Editors.ComboItem();
            this.comboItem6 = new DevComponents.Editors.ComboItem();
            this.buttonX17 = new DevComponents.DotNetBar.ButtonX();
            this.BANCO = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.CUENTA_BANCARIA = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.labelX13 = new DevComponents.DotNetBar.LabelX();
            this.UUID = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.FORMA_PAGO = new DevComponents.DotNetBar.Controls.ComboBoxEx();
            this.comboItem11 = new DevComponents.Editors.ComboItem();
            this.comboItem13 = new DevComponents.Editors.ComboItem();
            this.labelX5 = new DevComponents.DotNetBar.LabelX();
            this.labelX7 = new DevComponents.DotNetBar.LabelX();
            this.labelX9 = new DevComponents.DotNetBar.LabelX();
            this.labelX10 = new DevComponents.DotNetBar.LabelX();
            this.labelX12 = new DevComponents.DotNetBar.LabelX();
            this.MONEDA = new DevComponents.DotNetBar.Controls.ComboBoxEx();
            this.labelX8 = new DevComponents.DotNetBar.LabelX();
            this.FECHA = new System.Windows.Forms.DateTimePicker();
            this.dtgrdGeneral = new DevComponents.DotNetBar.Controls.DataGridViewX();
            this.cantidad = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.unidad = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.descripcion = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.noIdentificacion = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.valorUnitario = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.importe = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.fraccionArancelaria = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.informacionAduaneraFecha = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.informacionAduaneraAduana = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.informacionAduaneraNumero = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.OBSERVACIONES = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.ESTADO = new DevComponents.DotNetBar.Controls.ComboBoxEx();
            this.comboItem3 = new DevComponents.Editors.ComboItem();
            this.comboItem1 = new DevComponents.Editors.ComboItem();
            this.comboItem2 = new DevComponents.Editors.ComboItem();
            this.SERIE = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.TIPO_CAMBIO = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.labelX6 = new DevComponents.DotNetBar.LabelX();
            this.IMPUESTO_DATO = new DevComponents.DotNetBar.Controls.ComboBoxEx();
            this.comboItem7 = new DevComponents.Editors.ComboItem();
            this.comboItem8 = new DevComponents.Editors.ComboItem();
            this.comboItem9 = new DevComponents.Editors.ComboItem();
            this.comboItem10 = new DevComponents.Editors.ComboItem();
            this.comboItem12 = new DevComponents.Editors.ComboItem();
            this.ID = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.comboItem14 = new DevComponents.Editors.ComboItem();
            this.comboItem15 = new DevComponents.Editors.ComboItem();
            this.comboItem16 = new DevComponents.Editors.ComboItem();
            this.IMPUESTO = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.RETENCION = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.labelX4 = new DevComponents.DotNetBar.LabelX();
            this.TOTAL = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.labelX3 = new DevComponents.DotNetBar.LabelX();
            this.labelX2 = new DevComponents.DotNetBar.LabelX();
            this.SUBTOTAL = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.labelX1 = new DevComponents.DotNetBar.LabelX();
            this.ENTITY_ID = new DevComponents.DotNetBar.Controls.ComboBoxEx();
            this.comboItem17 = new DevComponents.Editors.ComboItem();
            this.comboItem18 = new DevComponents.Editors.ComboItem();
            this.comboItem19 = new DevComponents.Editors.ComboItem();
            this.labelX11 = new DevComponents.DotNetBar.LabelX();
            this.EMPRESA_NOMBRE = new DevComponents.DotNetBar.LabelX();
            this.labelX14 = new DevComponents.DotNetBar.LabelX();
            this.rfc = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.labelX15 = new DevComponents.DotNetBar.LabelX();
            this.METODO_DE_PAGO = new System.Windows.Forms.ComboBox();
            this.IEPStr = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.labelX17 = new DevComponents.DotNetBar.LabelX();
            this.TIPO = new DevComponents.DotNetBar.LabelX();
            this.TIPO_FACTURA = new DevComponents.DotNetBar.Controls.ComboBoxEx();
            this.comboItem20 = new DevComponents.Editors.ComboItem();
            this.comboItem21 = new DevComponents.Editors.ComboItem();
            this.comboItem22 = new DevComponents.Editors.ComboItem();
            this.labelX18 = new DevComponents.DotNetBar.LabelX();
            this.DESCUENTO = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.PATH = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.labelX19 = new DevComponents.DotNetBar.LabelX();
            this.dtgPolizas = new DevComponents.DotNetBar.Controls.DataGridViewX();
            this.ACCOUNT_ID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.REFERENCE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CARGO = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ABONO = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.folderBrowserDialog1 = new System.Windows.Forms.FolderBrowserDialog();
            this.FORMATO_IMPRESION = new DevComponents.DotNetBar.Controls.ComboBoxEx();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.buttonX13 = new DevComponents.DotNetBar.ButtonX();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.buttonX3 = new DevComponents.DotNetBar.ButtonX();
            this.buttonX8 = new DevComponents.DotNetBar.ButtonX();
            this.buttonX4 = new DevComponents.DotNetBar.ButtonX();
            this.buttonX6 = new DevComponents.DotNetBar.ButtonX();
            this.tabPage5 = new System.Windows.Forms.TabPage();
            this.label23 = new System.Windows.Forms.Label();
            this.PARA = new System.Windows.Forms.TextBox();
            this.ASUNTO = new System.Windows.Forms.TextBox();
            this.label18 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.MENSAJE = new System.Windows.Forms.RichTextBox();
            this.buttonX18 = new DevComponents.DotNetBar.ButtonX();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.comboBoxEx1 = new DevComponents.DotNetBar.Controls.ComboBoxEx();
            this.comboItem39 = new DevComponents.Editors.ComboItem();
            this.labelX32 = new DevComponents.DotNetBar.LabelX();
            this.buttonX14 = new DevComponents.DotNetBar.ButtonX();
            this.CodigoPostal = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.labelX26 = new DevComponents.DotNetBar.LabelX();
            this.EstadoCE = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.labelX27 = new DevComponents.DotNetBar.LabelX();
            this.noInterior = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.labelX25 = new DevComponents.DotNetBar.LabelX();
            this.labelX22 = new DevComponents.DotNetBar.LabelX();
            this.Pais = new DevComponents.DotNetBar.Controls.ComboBoxEx();
            this.comboItem23 = new DevComponents.Editors.ComboItem();
            this.noExterior = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.labelX24 = new DevComponents.DotNetBar.LabelX();
            this.calle = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.labelX23 = new DevComponents.DotNetBar.LabelX();
            this.NumRegIdTrib = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.labelX21 = new DevComponents.DotNetBar.LabelX();
            this.Incoterm = new DevComponents.DotNetBar.Controls.ComboBoxEx();
            this.comboItem24 = new DevComponents.Editors.ComboItem();
            this.comboItem25 = new DevComponents.Editors.ComboItem();
            this.comboItem26 = new DevComponents.Editors.ComboItem();
            this.comboItem27 = new DevComponents.Editors.ComboItem();
            this.comboItem28 = new DevComponents.Editors.ComboItem();
            this.comboItem29 = new DevComponents.Editors.ComboItem();
            this.comboItem30 = new DevComponents.Editors.ComboItem();
            this.comboItem31 = new DevComponents.Editors.ComboItem();
            this.comboItem32 = new DevComponents.Editors.ComboItem();
            this.comboItem33 = new DevComponents.Editors.ComboItem();
            this.comboItem34 = new DevComponents.Editors.ComboItem();
            this.comboItem35 = new DevComponents.Editors.ComboItem();
            this.comboItem36 = new DevComponents.Editors.ComboItem();
            this.comboItem37 = new DevComponents.Editors.ComboItem();
            this.comboItem38 = new DevComponents.Editors.ComboItem();
            this.labelX20 = new DevComponents.DotNetBar.LabelX();
            this.activarCE = new System.Windows.Forms.CheckBox();
            this.tabPage4 = new System.Windows.Forms.TabPage();
            this.labelX29 = new DevComponents.DotNetBar.LabelX();
            this.direccionEmbarque = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.tabPage6 = new System.Windows.Forms.TabPage();
            this.labelX33 = new DevComponents.DotNetBar.LabelX();
            this.facturarA = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.btnEmisor = new DevComponents.DotNetBar.ButtonX();
            this.buttonX10 = new DevComponents.DotNetBar.ButtonX();
            this.buttonX11 = new DevComponents.DotNetBar.ButtonX();
            this.buttonX9 = new DevComponents.DotNetBar.ButtonX();
            this.buttonX7 = new DevComponents.DotNetBar.ButtonX();
            this.buttonX5 = new DevComponents.DotNetBar.ButtonX();
            this.buttonX2 = new DevComponents.DotNetBar.ButtonX();
            this.buttonX1 = new DevComponents.DotNetBar.ButtonX();
            this.buttonX12 = new DevComponents.DotNetBar.ButtonX();
            this.motivoDescuento = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.labelX28 = new DevComponents.DotNetBar.LabelX();
            this.aplicarDescuentoAutomatico = new System.Windows.Forms.CheckBox();
            this.notasPDF = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.labelX30 = new DevComponents.DotNetBar.LabelX();
            this.buttonX15 = new DevComponents.DotNetBar.ButtonX();
            this.labelX31 = new DevComponents.DotNetBar.LabelX();
            this.totalSinDescuento = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.buttonX16 = new DevComponents.DotNetBar.ButtonX();
            this.buttonX19 = new DevComponents.DotNetBar.ButtonX();
            this.receptor = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.labelX16 = new DevComponents.DotNetBar.LabelX();
            this.motivoTraslado = new System.Windows.Forms.ComboBox();
            this.statusStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dtgrdGeneral)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtgPolizas)).BeginInit();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.tabPage2.SuspendLayout();
            this.tabPage5.SuspendLayout();
            this.tabPage3.SuspendLayout();
            this.tabPage4.SuspendLayout();
            this.tabPage6.SuspendLayout();
            this.SuspendLayout();
            // 
            // statusStrip1
            // 
            this.statusStrip1.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.informacion});
            this.statusStrip1.Location = new System.Drawing.Point(0, 498);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(863, 22);
            this.statusStrip1.TabIndex = 56;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // informacion
            // 
            this.informacion.Name = "informacion";
            this.informacion.Size = new System.Drawing.Size(42, 17);
            this.informacion.Text = "Estado";
            // 
            // comboItem4
            // 
            this.comboItem4.Text = "FECHA";
            // 
            // comboItem5
            // 
            this.comboItem5.Text = "FACTURA";
            // 
            // comboItem6
            // 
            this.comboItem6.Text = "CLIENTE";
            // 
            // buttonX17
            // 
            this.buttonX17.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.buttonX17.ColorTable = DevComponents.DotNetBar.eButtonColor.Flat;
            this.buttonX17.Font = new System.Drawing.Font("Segoe UI", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonX17.ImagePosition = DevComponents.DotNetBar.eImagePosition.Right;
            this.buttonX17.Location = new System.Drawing.Point(811, 168);
            this.buttonX17.Name = "buttonX17";
            this.buttonX17.Size = new System.Drawing.Size(47, 20);
            this.buttonX17.Style = DevComponents.DotNetBar.eDotNetBarStyle.OfficeXP;
            this.buttonX17.TabIndex = 27;
            this.buttonX17.Text = "Ver XML";
            this.buttonX17.Click += new System.EventHandler(this.buttonX17_Click_1);
            // 
            // BANCO
            // 
            this.BANCO.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.BANCO.Border.Class = "TextBoxBorder";
            this.BANCO.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.BANCO.ForeColor = System.Drawing.Color.Black;
            this.BANCO.Location = new System.Drawing.Point(614, 145);
            this.BANCO.Name = "BANCO";
            this.BANCO.Size = new System.Drawing.Size(244, 20);
            this.BANCO.TabIndex = 39;
            this.BANCO.Text = "NA";
            // 
            // CUENTA_BANCARIA
            // 
            this.CUENTA_BANCARIA.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.CUENTA_BANCARIA.Border.Class = "TextBoxBorder";
            this.CUENTA_BANCARIA.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.CUENTA_BANCARIA.ForeColor = System.Drawing.Color.Black;
            this.CUENTA_BANCARIA.Location = new System.Drawing.Point(493, 145);
            this.CUENTA_BANCARIA.Name = "CUENTA_BANCARIA";
            this.CUENTA_BANCARIA.Size = new System.Drawing.Size(75, 20);
            this.CUENTA_BANCARIA.TabIndex = 37;
            this.CUENTA_BANCARIA.Text = "0000";
            // 
            // labelX13
            // 
            this.labelX13.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX13.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX13.ForeColor = System.Drawing.Color.Black;
            this.labelX13.Location = new System.Drawing.Point(13, 171);
            this.labelX13.Name = "labelX13";
            this.labelX13.Size = new System.Drawing.Size(65, 15);
            this.labelX13.TabIndex = 24;
            this.labelX13.Text = "Folio Fiscal";
            this.labelX13.TextAlignment = System.Drawing.StringAlignment.Far;
            // 
            // UUID
            // 
            this.UUID.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.UUID.Border.Class = "TextBoxBorder";
            this.UUID.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.UUID.ForeColor = System.Drawing.Color.Black;
            this.UUID.Location = new System.Drawing.Point(84, 169);
            this.UUID.Name = "UUID";
            this.UUID.ReadOnly = true;
            this.UUID.Size = new System.Drawing.Size(256, 20);
            this.UUID.TabIndex = 25;
            // 
            // FORMA_PAGO
            // 
            this.FORMA_PAGO.DisplayMember = "Text";
            this.FORMA_PAGO.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.FORMA_PAGO.ForeColor = System.Drawing.Color.Black;
            this.FORMA_PAGO.FormattingEnabled = true;
            this.FORMA_PAGO.ItemHeight = 16;
            this.FORMA_PAGO.Items.AddRange(new object[] {
            this.comboItem11,
            this.comboItem13});
            this.FORMA_PAGO.Location = new System.Drawing.Point(493, 118);
            this.FORMA_PAGO.Name = "FORMA_PAGO";
            this.FORMA_PAGO.Size = new System.Drawing.Size(365, 22);
            this.FORMA_PAGO.TabIndex = 35;
            this.FORMA_PAGO.Text = "UNA SOLA EXHIBICIÓN";
            this.FORMA_PAGO.WatermarkText = "Selecione la Forma de Pago...";
            // 
            // comboItem11
            // 
            this.comboItem11.Text = "UNA SOLA EXHIBICIÓN";
            // 
            // comboItem13
            // 
            this.comboItem13.Text = "PAGOS";
            // 
            // labelX5
            // 
            this.labelX5.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX5.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX5.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelX5.ForeColor = System.Drawing.Color.Black;
            this.labelX5.Location = new System.Drawing.Point(574, 146);
            this.labelX5.Name = "labelX5";
            this.labelX5.Size = new System.Drawing.Size(34, 19);
            this.labelX5.TabIndex = 38;
            this.labelX5.Text = "Banco";
            // 
            // labelX7
            // 
            this.labelX7.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX7.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX7.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelX7.ForeColor = System.Drawing.Color.Black;
            this.labelX7.Location = new System.Drawing.Point(405, 145);
            this.labelX7.Name = "labelX7";
            this.labelX7.Size = new System.Drawing.Size(85, 20);
            this.labelX7.TabIndex = 36;
            this.labelX7.Text = "Cuenta Bancaria";
            // 
            // labelX9
            // 
            this.labelX9.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX9.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX9.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelX9.ForeColor = System.Drawing.Color.Black;
            this.labelX9.Location = new System.Drawing.Point(406, 119);
            this.labelX9.Name = "labelX9";
            this.labelX9.Size = new System.Drawing.Size(85, 20);
            this.labelX9.TabIndex = 34;
            this.labelX9.Text = "Forma de Pago";
            // 
            // labelX10
            // 
            this.labelX10.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX10.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX10.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelX10.ForeColor = System.Drawing.Color.Black;
            this.labelX10.Location = new System.Drawing.Point(574, 91);
            this.labelX10.Name = "labelX10";
            this.labelX10.Size = new System.Drawing.Size(85, 19);
            this.labelX10.TabIndex = 32;
            this.labelX10.Text = "Método de Pago";
            // 
            // labelX12
            // 
            this.labelX12.AutoSize = true;
            this.labelX12.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX12.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX12.ForeColor = System.Drawing.Color.Black;
            this.labelX12.Location = new System.Drawing.Point(156, 122);
            this.labelX12.Name = "labelX12";
            this.labelX12.Size = new System.Drawing.Size(42, 15);
            this.labelX12.TabIndex = 18;
            this.labelX12.Text = "Moneda";
            this.labelX12.Click += new System.EventHandler(this.labelX12_Click);
            // 
            // MONEDA
            // 
            this.MONEDA.AutoCompleteCustomSource.AddRange(new string[] {
            "AED",
            "AFN",
            "ALL",
            "AMD",
            "ANG",
            "AOA",
            "ARS",
            "AUD",
            "AWG",
            "AZN",
            "BAM",
            "BBD",
            "BDT",
            "BGN",
            "BHD",
            "BIF",
            "BMD",
            "BND",
            "BOB",
            "BOV",
            "BRL",
            "BSD",
            "BTN",
            "BWP",
            "BYR",
            "BZD",
            "CAD",
            "CDF",
            "CHF",
            "CLF",
            "CLP",
            "CNY",
            "COP",
            "COU",
            "CRC",
            "CSD",
            "CUP",
            "CUC",
            "CVE",
            "CZK",
            "DJF",
            "DKK",
            "DOP",
            "DZD",
            "EGP",
            "ERN",
            "ETB",
            "EUR",
            "FJD",
            "FKP",
            "GBP",
            "GEL",
            "GHS",
            "GIP",
            "GMD",
            "GNF",
            "GTQ",
            "GYD",
            "HKD",
            "HNL",
            "HRK",
            "HTG",
            "HUF",
            "IDR",
            "ILS",
            "INR",
            "IQD",
            "IRR",
            "ISK",
            "JMD",
            "JOD",
            "JPY",
            "KES",
            "KGS",
            "KHR",
            "KMF",
            "KPW",
            "KRW",
            "KWD",
            "KYD",
            "KZT",
            "LAK",
            "LBP",
            "LKR",
            "LRD",
            "LSL",
            "LTL",
            "LVL",
            "LYD",
            "MAD",
            "MDL",
            "MGA",
            "MKD",
            "MMK",
            "MNT",
            "MOP",
            "MRO",
            "MUR",
            "MVR",
            "MWK",
            "MXN",
            "MXV",
            "MYR",
            "MZN",
            "NAD",
            "NGN",
            "NIO",
            "NOK",
            "NPR",
            "NZD",
            "OMR",
            "PAB",
            "PEN",
            "PGK",
            "PHP",
            "PKR",
            "PLN",
            "PYG",
            "QAR",
            "RON",
            "RUB",
            "RWF",
            "SAR",
            "SBD",
            "SCR",
            "SDG",
            "SEK",
            "SGD",
            "SHP",
            "SLL",
            "SOS",
            "SRD",
            "STD",
            "SYP",
            "SZL",
            "THB",
            "TJS",
            "TMT",
            "TND",
            "TOP",
            "TRY",
            "TTD",
            "TWD",
            "TZS",
            "UAH",
            "UGX",
            "USD",
            "USN",
            "USS",
            "UYU",
            "UZS",
            "VEF",
            "VND",
            "VUV",
            "WST",
            "XAF",
            "XAG",
            "XAU",
            "XBA",
            "XBB",
            "XBC",
            "XBD",
            "XCD",
            "XDR",
            "XFO",
            "XFU",
            "XOF",
            "XPD",
            "XPF",
            "XPT",
            "XTS",
            "XXX",
            "YER",
            "ZAR",
            "ZMW",
            "ZWL"});
            this.MONEDA.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.MONEDA.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource;
            this.MONEDA.DisplayMember = "Text";
            this.MONEDA.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.MONEDA.ForeColor = System.Drawing.Color.Black;
            this.MONEDA.FormattingEnabled = true;
            this.MONEDA.ItemHeight = 16;
            this.MONEDA.Location = new System.Drawing.Point(205, 118);
            this.MONEDA.Name = "MONEDA";
            this.MONEDA.Size = new System.Drawing.Size(91, 22);
            this.MONEDA.TabIndex = 19;
            this.MONEDA.Text = "USD";
            this.MONEDA.SelectedIndexChanged += new System.EventHandler(this.MONEDA_SelectedIndexChanged_1);
            // 
            // labelX8
            // 
            this.labelX8.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX8.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX8.ForeColor = System.Drawing.Color.Black;
            this.labelX8.Location = new System.Drawing.Point(651, 333);
            this.labelX8.Name = "labelX8";
            this.labelX8.Size = new System.Drawing.Size(55, 22);
            this.labelX8.TabIndex = 42;
            this.labelX8.Text = "Impuesto";
            this.labelX8.TextAlignment = System.Drawing.StringAlignment.Far;
            // 
            // FECHA
            // 
            this.FECHA.BackColor = System.Drawing.Color.White;
            this.FECHA.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FECHA.ForeColor = System.Drawing.Color.Black;
            this.FECHA.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.FECHA.Location = new System.Drawing.Point(203, 89);
            this.FECHA.Name = "FECHA";
            this.FECHA.Size = new System.Drawing.Size(93, 22);
            this.FECHA.TabIndex = 14;
            this.FECHA.ValueChanged += new System.EventHandler(this.FECHA_ValueChanged_1);
            // 
            // dtgrdGeneral
            // 
            this.dtgrdGeneral.AllowUserToAddRows = false;
            this.dtgrdGeneral.AllowUserToDeleteRows = false;
            this.dtgrdGeneral.BackgroundColor = System.Drawing.Color.White;
            this.dtgrdGeneral.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dtgrdGeneral.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.dtgrdGeneral.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dtgrdGeneral.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.cantidad,
            this.unidad,
            this.descripcion,
            this.noIdentificacion,
            this.valorUnitario,
            this.importe,
            this.fraccionArancelaria,
            this.informacionAduaneraFecha,
            this.informacionAduaneraAduana,
            this.informacionAduaneraNumero});
            dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle5.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle5.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle5.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle5.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle5.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle5.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dtgrdGeneral.DefaultCellStyle = dataGridViewCellStyle5;
            this.dtgrdGeneral.EnableHeadersVisualStyles = false;
            this.dtgrdGeneral.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(208)))), ((int)(((byte)(215)))), ((int)(((byte)(229)))));
            this.dtgrdGeneral.Location = new System.Drawing.Point(55, 3);
            this.dtgrdGeneral.Name = "dtgrdGeneral";
            this.dtgrdGeneral.ReadOnly = true;
            dataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle6.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle6.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle6.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle6.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle6.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle6.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dtgrdGeneral.RowHeadersDefaultCellStyle = dataGridViewCellStyle6;
            this.dtgrdGeneral.RowHeadersVisible = false;
            this.dtgrdGeneral.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dtgrdGeneral.Size = new System.Drawing.Size(782, 100);
            this.dtgrdGeneral.TabIndex = 40;
            this.dtgrdGeneral.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dtgrdGeneral_CellContentClick);
            this.dtgrdGeneral.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dtgrdGeneral_CellDoubleClick_1);
            // 
            // cantidad
            // 
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.cantidad.DefaultCellStyle = dataGridViewCellStyle2;
            this.cantidad.HeaderText = "Cantidad";
            this.cantidad.Name = "cantidad";
            this.cantidad.ReadOnly = true;
            this.cantidad.Width = 60;
            // 
            // unidad
            // 
            this.unidad.HeaderText = "Unidad";
            this.unidad.Name = "unidad";
            this.unidad.ReadOnly = true;
            this.unidad.Width = 60;
            // 
            // descripcion
            // 
            this.descripcion.HeaderText = "Descripción";
            this.descripcion.Name = "descripcion";
            this.descripcion.ReadOnly = true;
            this.descripcion.Width = 240;
            // 
            // noIdentificacion
            // 
            this.noIdentificacion.HeaderText = "No Identificacion";
            this.noIdentificacion.Name = "noIdentificacion";
            this.noIdentificacion.ReadOnly = true;
            this.noIdentificacion.Width = 120;
            // 
            // valorUnitario
            // 
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.valorUnitario.DefaultCellStyle = dataGridViewCellStyle3;
            this.valorUnitario.HeaderText = "Valor Unitario";
            this.valorUnitario.Name = "valorUnitario";
            this.valorUnitario.ReadOnly = true;
            this.valorUnitario.Width = 150;
            // 
            // importe
            // 
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.importe.DefaultCellStyle = dataGridViewCellStyle4;
            this.importe.HeaderText = "Importe";
            this.importe.Name = "importe";
            this.importe.ReadOnly = true;
            // 
            // fraccionArancelaria
            // 
            this.fraccionArancelaria.HeaderText = "Fracción";
            this.fraccionArancelaria.Name = "fraccionArancelaria";
            this.fraccionArancelaria.ReadOnly = true;
            // 
            // informacionAduaneraFecha
            // 
            this.informacionAduaneraFecha.HeaderText = "Fecha";
            this.informacionAduaneraFecha.Name = "informacionAduaneraFecha";
            this.informacionAduaneraFecha.ReadOnly = true;
            // 
            // informacionAduaneraAduana
            // 
            this.informacionAduaneraAduana.HeaderText = "Aduana";
            this.informacionAduaneraAduana.Name = "informacionAduaneraAduana";
            this.informacionAduaneraAduana.ReadOnly = true;
            // 
            // informacionAduaneraNumero
            // 
            this.informacionAduaneraNumero.HeaderText = "Número";
            this.informacionAduaneraNumero.Name = "informacionAduaneraNumero";
            this.informacionAduaneraNumero.ReadOnly = true;
            // 
            // OBSERVACIONES
            // 
            this.OBSERVACIONES.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.OBSERVACIONES.Border.Class = "TextBoxBorder";
            this.OBSERVACIONES.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.OBSERVACIONES.ForeColor = System.Drawing.Color.Black;
            this.OBSERVACIONES.Location = new System.Drawing.Point(12, 362);
            this.OBSERVACIONES.Multiline = true;
            this.OBSERVACIONES.Name = "OBSERVACIONES";
            this.OBSERVACIONES.Size = new System.Drawing.Size(631, 56);
            this.OBSERVACIONES.TabIndex = 41;
            this.OBSERVACIONES.WatermarkText = "Escriba aquí las leyenda físcal generales...";
            // 
            // ESTADO
            // 
            this.ESTADO.DisplayMember = "Text";
            this.ESTADO.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.ESTADO.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ESTADO.ForeColor = System.Drawing.Color.Black;
            this.ESTADO.FormattingEnabled = true;
            this.ESTADO.ItemHeight = 16;
            this.ESTADO.Items.AddRange(new object[] {
            this.comboItem3,
            this.comboItem1,
            this.comboItem2});
            this.ESTADO.Location = new System.Drawing.Point(302, 89);
            this.ESTADO.Name = "ESTADO";
            this.ESTADO.Size = new System.Drawing.Size(98, 22);
            this.ESTADO.TabIndex = 15;
            this.ESTADO.Text = "BORRADOR";
            // 
            // comboItem3
            // 
            this.comboItem3.Text = "BORRADOR";
            // 
            // comboItem1
            // 
            this.comboItem1.FontStyle = System.Drawing.FontStyle.Bold;
            this.comboItem1.Text = "TIMBRADO";
            // 
            // comboItem2
            // 
            this.comboItem2.FontStyle = System.Drawing.FontStyle.Bold;
            this.comboItem2.ForeColor = System.Drawing.Color.Red;
            this.comboItem2.Text = "CANCELADO";
            // 
            // SERIE
            // 
            this.SERIE.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.SERIE.Border.Class = "TextBoxBorder";
            this.SERIE.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.SERIE.Enabled = false;
            this.SERIE.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.SERIE.ForeColor = System.Drawing.Color.Black;
            this.SERIE.Location = new System.Drawing.Point(69, 89);
            this.SERIE.Margin = new System.Windows.Forms.Padding(0);
            this.SERIE.Name = "SERIE";
            this.SERIE.Size = new System.Drawing.Size(64, 22);
            this.SERIE.TabIndex = 12;
            this.SERIE.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // TIPO_CAMBIO
            // 
            this.TIPO_CAMBIO.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.TIPO_CAMBIO.Border.Class = "TextBoxBorder";
            this.TIPO_CAMBIO.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.TIPO_CAMBIO.ForeColor = System.Drawing.Color.Black;
            this.TIPO_CAMBIO.Location = new System.Drawing.Point(335, 119);
            this.TIPO_CAMBIO.Name = "TIPO_CAMBIO";
            this.TIPO_CAMBIO.Size = new System.Drawing.Size(65, 20);
            this.TIPO_CAMBIO.TabIndex = 21;
            this.TIPO_CAMBIO.Text = "0";
            // 
            // labelX6
            // 
            this.labelX6.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX6.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX6.ForeColor = System.Drawing.Color.Black;
            this.labelX6.Location = new System.Drawing.Point(302, 119);
            this.labelX6.Name = "labelX6";
            this.labelX6.Size = new System.Drawing.Size(27, 20);
            this.labelX6.TabIndex = 20;
            this.labelX6.Text = "TC";
            this.labelX6.TextAlignment = System.Drawing.StringAlignment.Far;
            // 
            // IMPUESTO_DATO
            // 
            this.IMPUESTO_DATO.DisplayMember = "Text";
            this.IMPUESTO_DATO.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.IMPUESTO_DATO.ForeColor = System.Drawing.Color.Black;
            this.IMPUESTO_DATO.FormattingEnabled = true;
            this.IMPUESTO_DATO.ItemHeight = 16;
            this.IMPUESTO_DATO.Items.AddRange(new object[] {
            this.comboItem7,
            this.comboItem8,
            this.comboItem9,
            this.comboItem10,
            this.comboItem12});
            this.IMPUESTO_DATO.Location = new System.Drawing.Point(712, 333);
            this.IMPUESTO_DATO.Name = "IMPUESTO_DATO";
            this.IMPUESTO_DATO.Size = new System.Drawing.Size(142, 22);
            this.IMPUESTO_DATO.TabIndex = 43;
            this.IMPUESTO_DATO.Text = "0%";
            this.IMPUESTO_DATO.SelectedIndexChanged += new System.EventHandler(this.IMPUESTO_DATO_SelectedIndexChanged);
            // 
            // comboItem7
            // 
            this.comboItem7.Text = "16%";
            // 
            // comboItem8
            // 
            this.comboItem8.Text = "RETENCION 16%";
            // 
            // comboItem9
            // 
            this.comboItem9.Text = "0%";
            // 
            // comboItem10
            // 
            this.comboItem10.Text = "11%";
            // 
            // comboItem12
            // 
            this.comboItem12.Text = "RETENCION 11%";
            // 
            // ID
            // 
            this.ID.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.ID.Border.Class = "TextBoxBorder";
            this.ID.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.ID.Enabled = false;
            this.ID.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ID.ForeColor = System.Drawing.Color.Black;
            this.ID.Location = new System.Drawing.Point(133, 89);
            this.ID.Margin = new System.Windows.Forms.Padding(0);
            this.ID.Name = "ID";
            this.ID.Size = new System.Drawing.Size(67, 22);
            this.ID.TabIndex = 13;
            this.ID.Text = "000000";
            this.ID.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // comboItem14
            // 
            this.comboItem14.Text = "FACTURA";
            // 
            // comboItem15
            // 
            this.comboItem15.Text = "NOTA DE CREDITO";
            // 
            // comboItem16
            // 
            this.comboItem16.Text = "RECIBO HONORARIOS";
            // 
            // IMPUESTO
            // 
            this.IMPUESTO.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.IMPUESTO.Border.Class = "TextBoxBorder";
            this.IMPUESTO.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.IMPUESTO.ForeColor = System.Drawing.Color.Black;
            this.IMPUESTO.Location = new System.Drawing.Point(712, 398);
            this.IMPUESTO.Name = "IMPUESTO";
            this.IMPUESTO.ReadOnly = true;
            this.IMPUESTO.Size = new System.Drawing.Size(142, 20);
            this.IMPUESTO.TabIndex = 49;
            this.IMPUESTO.Text = "0";
            this.IMPUESTO.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // RETENCION
            // 
            this.RETENCION.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.RETENCION.Border.Class = "TextBoxBorder";
            this.RETENCION.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.RETENCION.ForeColor = System.Drawing.Color.Black;
            this.RETENCION.Location = new System.Drawing.Point(712, 440);
            this.RETENCION.Name = "RETENCION";
            this.RETENCION.ReadOnly = true;
            this.RETENCION.Size = new System.Drawing.Size(142, 20);
            this.RETENCION.TabIndex = 53;
            this.RETENCION.Text = "0";
            this.RETENCION.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // labelX4
            // 
            this.labelX4.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX4.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX4.ForeColor = System.Drawing.Color.Black;
            this.labelX4.Location = new System.Drawing.Point(650, 440);
            this.labelX4.Name = "labelX4";
            this.labelX4.Size = new System.Drawing.Size(56, 20);
            this.labelX4.TabIndex = 52;
            this.labelX4.Text = "Retención";
            this.labelX4.TextAlignment = System.Drawing.StringAlignment.Far;
            // 
            // TOTAL
            // 
            this.TOTAL.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.TOTAL.Border.Class = "TextBoxBorder";
            this.TOTAL.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.TOTAL.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TOTAL.ForeColor = System.Drawing.Color.Black;
            this.TOTAL.Location = new System.Drawing.Point(712, 461);
            this.TOTAL.Name = "TOTAL";
            this.TOTAL.ReadOnly = true;
            this.TOTAL.Size = new System.Drawing.Size(142, 29);
            this.TOTAL.TabIndex = 55;
            this.TOTAL.Text = "0";
            this.TOTAL.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // labelX3
            // 
            this.labelX3.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX3.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX3.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelX3.ForeColor = System.Drawing.Color.Black;
            this.labelX3.Location = new System.Drawing.Point(650, 463);
            this.labelX3.Name = "labelX3";
            this.labelX3.Size = new System.Drawing.Size(56, 27);
            this.labelX3.TabIndex = 54;
            this.labelX3.Text = "Total";
            this.labelX3.TextAlignment = System.Drawing.StringAlignment.Far;
            // 
            // labelX2
            // 
            this.labelX2.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX2.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX2.ForeColor = System.Drawing.Color.Black;
            this.labelX2.Location = new System.Drawing.Point(650, 398);
            this.labelX2.Name = "labelX2";
            this.labelX2.Size = new System.Drawing.Size(56, 20);
            this.labelX2.TabIndex = 48;
            this.labelX2.Text = "Impuesto";
            this.labelX2.TextAlignment = System.Drawing.StringAlignment.Far;
            // 
            // SUBTOTAL
            // 
            this.SUBTOTAL.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.SUBTOTAL.Border.Class = "TextBoxBorder";
            this.SUBTOTAL.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.SUBTOTAL.ForeColor = System.Drawing.Color.Black;
            this.SUBTOTAL.Location = new System.Drawing.Point(712, 356);
            this.SUBTOTAL.Name = "SUBTOTAL";
            this.SUBTOTAL.ReadOnly = true;
            this.SUBTOTAL.Size = new System.Drawing.Size(142, 20);
            this.SUBTOTAL.TabIndex = 45;
            this.SUBTOTAL.Text = "0";
            this.SUBTOTAL.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // labelX1
            // 
            this.labelX1.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX1.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX1.ForeColor = System.Drawing.Color.Black;
            this.labelX1.Location = new System.Drawing.Point(650, 356);
            this.labelX1.Name = "labelX1";
            this.labelX1.Size = new System.Drawing.Size(56, 20);
            this.labelX1.TabIndex = 44;
            this.labelX1.Text = "Subtotal";
            this.labelX1.TextAlignment = System.Drawing.StringAlignment.Far;
            // 
            // ENTITY_ID
            // 
            this.ENTITY_ID.DisplayMember = "Text";
            this.ENTITY_ID.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.ENTITY_ID.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ENTITY_ID.ForeColor = System.Drawing.Color.Black;
            this.ENTITY_ID.FormattingEnabled = true;
            this.ENTITY_ID.ItemHeight = 16;
            this.ENTITY_ID.Items.AddRange(new object[] {
            this.comboItem17,
            this.comboItem18,
            this.comboItem19});
            this.ENTITY_ID.Location = new System.Drawing.Point(56, 62);
            this.ENTITY_ID.Name = "ENTITY_ID";
            this.ENTITY_ID.Size = new System.Drawing.Size(344, 22);
            this.ENTITY_ID.TabIndex = 9;
            this.ENTITY_ID.SelectedIndexChanged += new System.EventHandler(this.ENTITY_ID_SelectedIndexChanged);
            // 
            // comboItem17
            // 
            this.comboItem17.Text = "BORRADOR";
            // 
            // comboItem18
            // 
            this.comboItem18.FontStyle = System.Drawing.FontStyle.Bold;
            this.comboItem18.Text = "TIMBRADO";
            // 
            // comboItem19
            // 
            this.comboItem19.FontStyle = System.Drawing.FontStyle.Bold;
            this.comboItem19.ForeColor = System.Drawing.Color.Red;
            this.comboItem19.Text = "CANCELADO";
            // 
            // labelX11
            // 
            this.labelX11.AutoSize = true;
            this.labelX11.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX11.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX11.ForeColor = System.Drawing.Color.Black;
            this.labelX11.Location = new System.Drawing.Point(13, 66);
            this.labelX11.Name = "labelX11";
            this.labelX11.Size = new System.Drawing.Size(37, 15);
            this.labelX11.TabIndex = 8;
            this.labelX11.Text = "Emisor";
            // 
            // EMPRESA_NOMBRE
            // 
            this.EMPRESA_NOMBRE.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.EMPRESA_NOMBRE.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.EMPRESA_NOMBRE.ForeColor = System.Drawing.Color.Black;
            this.EMPRESA_NOMBRE.Location = new System.Drawing.Point(205, 66);
            this.EMPRESA_NOMBRE.Name = "EMPRESA_NOMBRE";
            this.EMPRESA_NOMBRE.Size = new System.Drawing.Size(195, 15);
            this.EMPRESA_NOMBRE.TabIndex = 10;
            // 
            // labelX14
            // 
            this.labelX14.AutoSize = true;
            this.labelX14.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX14.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX14.ForeColor = System.Drawing.Color.Black;
            this.labelX14.Location = new System.Drawing.Point(35, 150);
            this.labelX14.Name = "labelX14";
            this.labelX14.Size = new System.Drawing.Size(43, 15);
            this.labelX14.TabIndex = 22;
            this.labelX14.Text = "Formato";
            this.labelX14.Click += new System.EventHandler(this.labelX12_Click);
            // 
            // rfc
            // 
            this.rfc.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.rfc.Border.Class = "TextBoxBorder";
            this.rfc.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.rfc.ForeColor = System.Drawing.Color.Black;
            this.rfc.Location = new System.Drawing.Point(457, 90);
            this.rfc.Name = "rfc";
            this.rfc.Size = new System.Drawing.Size(111, 20);
            this.rfc.TabIndex = 31;
            this.rfc.TextChanged += new System.EventHandler(this.rfc_TextChanged);
            // 
            // labelX15
            // 
            this.labelX15.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX15.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX15.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelX15.ForeColor = System.Drawing.Color.Black;
            this.labelX15.Location = new System.Drawing.Point(406, 92);
            this.labelX15.Name = "labelX15";
            this.labelX15.Size = new System.Drawing.Size(45, 17);
            this.labelX15.TabIndex = 30;
            this.labelX15.Text = "RFC";
            // 
            // METODO_DE_PAGO
            // 
            this.METODO_DE_PAGO.FormattingEnabled = true;
            this.METODO_DE_PAGO.Items.AddRange(new object[] {
            "01 Efectivo",
            "02 Cheque nominativo",
            "03 Transferencia electrónica de fondos",
            "04 Tarjetas de crédito",
            "05 Monederos electrónicos",
            "06 Dinero electrónico",
            "07 Tarjetas digitales",
            "08 Vales de despensa",
            "28 Tarjeta de Débito",
            "29 Tarjeta de Servicio",
            "99 Otros",
            "NA"});
            this.METODO_DE_PAGO.Location = new System.Drawing.Point(665, 89);
            this.METODO_DE_PAGO.Name = "METODO_DE_PAGO";
            this.METODO_DE_PAGO.Size = new System.Drawing.Size(193, 21);
            this.METODO_DE_PAGO.TabIndex = 33;
            this.METODO_DE_PAGO.Text = "NA";
            // 
            // IEPStr
            // 
            this.IEPStr.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.IEPStr.Border.Class = "TextBoxBorder";
            this.IEPStr.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.IEPStr.ForeColor = System.Drawing.Color.Black;
            this.IEPStr.Location = new System.Drawing.Point(712, 419);
            this.IEPStr.Name = "IEPStr";
            this.IEPStr.ReadOnly = true;
            this.IEPStr.Size = new System.Drawing.Size(142, 20);
            this.IEPStr.TabIndex = 51;
            this.IEPStr.Text = "0";
            this.IEPStr.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // labelX17
            // 
            this.labelX17.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX17.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX17.ForeColor = System.Drawing.Color.Black;
            this.labelX17.Location = new System.Drawing.Point(650, 420);
            this.labelX17.Name = "labelX17";
            this.labelX17.Size = new System.Drawing.Size(56, 19);
            this.labelX17.TabIndex = 50;
            this.labelX17.Text = "IEPS";
            this.labelX17.TextAlignment = System.Drawing.StringAlignment.Far;
            // 
            // TIPO
            // 
            this.TIPO.AutoSize = true;
            this.TIPO.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.TIPO.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.TIPO.ForeColor = System.Drawing.Color.Black;
            this.TIPO.Location = new System.Drawing.Point(13, 120);
            this.TIPO.Name = "TIPO";
            this.TIPO.Size = new System.Drawing.Size(24, 15);
            this.TIPO.TabIndex = 16;
            this.TIPO.Text = "Tipo";
            this.TIPO.Click += new System.EventHandler(this.labelX12_Click);
            // 
            // TIPO_FACTURA
            // 
            this.TIPO_FACTURA.DisplayMember = "Text";
            this.TIPO_FACTURA.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.TIPO_FACTURA.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TIPO_FACTURA.ForeColor = System.Drawing.Color.Black;
            this.TIPO_FACTURA.FormattingEnabled = true;
            this.TIPO_FACTURA.ItemHeight = 16;
            this.TIPO_FACTURA.Items.AddRange(new object[] {
            this.comboItem20,
            this.comboItem21,
            this.comboItem22});
            this.TIPO_FACTURA.Location = new System.Drawing.Point(56, 118);
            this.TIPO_FACTURA.Name = "TIPO_FACTURA";
            this.TIPO_FACTURA.Size = new System.Drawing.Size(94, 22);
            this.TIPO_FACTURA.TabIndex = 17;
            this.TIPO_FACTURA.Text = "ingreso";
            this.TIPO_FACTURA.SelectedIndexChanged += new System.EventHandler(this.TIPO_FACTURA_SelectedIndexChanged);
            // 
            // comboItem20
            // 
            this.comboItem20.Text = "ingreso";
            // 
            // comboItem21
            // 
            this.comboItem21.FontStyle = System.Drawing.FontStyle.Bold;
            this.comboItem21.Text = "egreso";
            // 
            // comboItem22
            // 
            this.comboItem22.FontStyle = System.Drawing.FontStyle.Bold;
            this.comboItem22.ForeColor = System.Drawing.Color.Black;
            this.comboItem22.Text = "traslado";
            // 
            // labelX18
            // 
            this.labelX18.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX18.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX18.ForeColor = System.Drawing.Color.Black;
            this.labelX18.Location = new System.Drawing.Point(650, 377);
            this.labelX18.Name = "labelX18";
            this.labelX18.Size = new System.Drawing.Size(56, 20);
            this.labelX18.TabIndex = 46;
            this.labelX18.Text = "Descuento";
            this.labelX18.TextAlignment = System.Drawing.StringAlignment.Far;
            // 
            // DESCUENTO
            // 
            this.DESCUENTO.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.DESCUENTO.Border.Class = "TextBoxBorder";
            this.DESCUENTO.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.DESCUENTO.ForeColor = System.Drawing.Color.Black;
            this.DESCUENTO.Location = new System.Drawing.Point(712, 377);
            this.DESCUENTO.Name = "DESCUENTO";
            this.DESCUENTO.Size = new System.Drawing.Size(142, 20);
            this.DESCUENTO.TabIndex = 47;
            this.DESCUENTO.Text = "0";
            this.DESCUENTO.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // PATH
            // 
            this.PATH.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.PATH.Border.Class = "TextBoxBorder";
            this.PATH.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.PATH.ForeColor = System.Drawing.Color.Black;
            this.PATH.Location = new System.Drawing.Point(393, 168);
            this.PATH.Name = "PATH";
            this.PATH.ReadOnly = true;
            this.PATH.Size = new System.Drawing.Size(412, 20);
            this.PATH.TabIndex = 25;
            // 
            // labelX19
            // 
            this.labelX19.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX19.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX19.ForeColor = System.Drawing.Color.Black;
            this.labelX19.Location = new System.Drawing.Point(346, 171);
            this.labelX19.Name = "labelX19";
            this.labelX19.Size = new System.Drawing.Size(41, 15);
            this.labelX19.TabIndex = 24;
            this.labelX19.Text = "Archivo";
            // 
            // dtgPolizas
            // 
            this.dtgPolizas.AllowUserToAddRows = false;
            this.dtgPolizas.AllowUserToDeleteRows = false;
            this.dtgPolizas.BackgroundColor = System.Drawing.Color.White;
            this.dtgPolizas.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            dataGridViewCellStyle7.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle7.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle7.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle7.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle7.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle7.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle7.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dtgPolizas.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle7;
            this.dtgPolizas.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dtgPolizas.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.ACCOUNT_ID,
            this.REFERENCE,
            this.CARGO,
            this.ABONO});
            dataGridViewCellStyle8.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle8.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle8.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle8.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle8.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle8.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle8.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dtgPolizas.DefaultCellStyle = dataGridViewCellStyle8;
            this.dtgPolizas.EnableHeadersVisualStyles = false;
            this.dtgPolizas.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(208)))), ((int)(((byte)(215)))), ((int)(((byte)(229)))));
            this.dtgPolizas.Location = new System.Drawing.Point(6, 53);
            this.dtgPolizas.Name = "dtgPolizas";
            dataGridViewCellStyle9.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle9.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle9.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle9.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle9.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle9.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle9.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dtgPolizas.RowHeadersDefaultCellStyle = dataGridViewCellStyle9;
            this.dtgPolizas.RowHeadersVisible = false;
            this.dtgPolizas.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dtgPolizas.Size = new System.Drawing.Size(825, 47);
            this.dtgPolizas.TabIndex = 58;
            // 
            // ACCOUNT_ID
            // 
            this.ACCOUNT_ID.HeaderText = "Cuenta Contable";
            this.ACCOUNT_ID.Name = "ACCOUNT_ID";
            this.ACCOUNT_ID.Width = 150;
            // 
            // REFERENCE
            // 
            this.REFERENCE.HeaderText = "Referencia";
            this.REFERENCE.Name = "REFERENCE";
            this.REFERENCE.Width = 200;
            // 
            // CARGO
            // 
            this.CARGO.HeaderText = "Cargo";
            this.CARGO.Name = "CARGO";
            this.CARGO.Width = 75;
            // 
            // ABONO
            // 
            this.ABONO.HeaderText = "Abono";
            this.ABONO.Name = "ABONO";
            this.ABONO.Width = 75;
            // 
            // FORMATO_IMPRESION
            // 
            this.FORMATO_IMPRESION.DisplayMember = "Text";
            this.FORMATO_IMPRESION.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.FORMATO_IMPRESION.ForeColor = System.Drawing.Color.Black;
            this.FORMATO_IMPRESION.FormattingEnabled = true;
            this.FORMATO_IMPRESION.ItemHeight = 16;
            this.FORMATO_IMPRESION.Location = new System.Drawing.Point(84, 143);
            this.FORMATO_IMPRESION.Name = "FORMATO_IMPRESION";
            this.FORMATO_IMPRESION.Size = new System.Drawing.Size(315, 22);
            this.FORMATO_IMPRESION.TabIndex = 598;
            this.FORMATO_IMPRESION.WatermarkText = "Formato de impresión";
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Controls.Add(this.tabPage5);
            this.tabControl1.Controls.Add(this.tabPage3);
            this.tabControl1.Controls.Add(this.tabPage4);
            this.tabControl1.Controls.Add(this.tabPage6);
            this.tabControl1.Location = new System.Drawing.Point(13, 195);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(845, 132);
            this.tabControl1.TabIndex = 604;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.dtgrdGeneral);
            this.tabPage1.Controls.Add(this.buttonX13);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3, 3, 3, 3);
            this.tabPage1.Size = new System.Drawing.Size(837, 106);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Líneas";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // buttonX13
            // 
            this.buttonX13.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.buttonX13.BackColor = System.Drawing.Color.DarkOliveGreen;
            this.buttonX13.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.buttonX13.ColorTable = DevComponents.DotNetBar.eButtonColor.Flat;
            this.buttonX13.Font = new System.Drawing.Font("Segoe UI", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonX13.ImagePosition = DevComponents.DotNetBar.eImagePosition.Bottom;
            this.buttonX13.Location = new System.Drawing.Point(3, 3);
            this.buttonX13.Name = "buttonX13";
            this.buttonX13.Size = new System.Drawing.Size(46, 97);
            this.buttonX13.Style = DevComponents.DotNetBar.eDotNetBarStyle.Office2003;
            this.buttonX13.TabIndex = 606;
            this.buttonX13.Text = "Pegar desde Excel";
            this.buttonX13.Click += new System.EventHandler(this.buttonX13_Click);
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.buttonX3);
            this.tabPage2.Controls.Add(this.buttonX8);
            this.tabPage2.Controls.Add(this.dtgPolizas);
            this.tabPage2.Controls.Add(this.buttonX4);
            this.tabPage2.Controls.Add(this.buttonX6);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3, 3, 3, 3);
            this.tabPage2.Size = new System.Drawing.Size(837, 106);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Póliza";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // buttonX3
            // 
            this.buttonX3.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.buttonX3.BackColor = System.Drawing.Color.Navy;
            this.buttonX3.ColorTable = DevComponents.DotNetBar.eButtonColor.Office2007WithBackground;
            this.buttonX3.Font = new System.Drawing.Font("Segoe UI", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonX3.Image = global::FE_FX.Properties.Resources.ic_donut_large_black_24dp_1x;
            this.buttonX3.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top;
            this.buttonX3.Location = new System.Drawing.Point(7, 6);
            this.buttonX3.Name = "buttonX3";
            this.buttonX3.Size = new System.Drawing.Size(42, 41);
            this.buttonX3.Style = DevComponents.DotNetBar.eDotNetBarStyle.OfficeXP;
            this.buttonX3.TabIndex = 599;
            this.buttonX3.Text = "Póliza";
            // 
            // buttonX8
            // 
            this.buttonX8.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.buttonX8.BackColor = System.Drawing.Color.Navy;
            this.buttonX8.ColorTable = DevComponents.DotNetBar.eButtonColor.Office2007WithBackground;
            this.buttonX8.Font = new System.Drawing.Font("Segoe UI", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonX8.Image = global::FE_FX.Properties.Resources.ic_donut_large_black_24dp_1x;
            this.buttonX8.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top;
            this.buttonX8.Location = new System.Drawing.Point(152, 6);
            this.buttonX8.Name = "buttonX8";
            this.buttonX8.Size = new System.Drawing.Size(42, 41);
            this.buttonX8.Style = DevComponents.DotNetBar.eDotNetBarStyle.OfficeXP;
            this.buttonX8.TabIndex = 602;
            this.buttonX8.Text = "Generar";
            this.buttonX8.Click += new System.EventHandler(this.buttonX8_Click_1);
            // 
            // buttonX4
            // 
            this.buttonX4.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.buttonX4.BackColor = System.Drawing.Color.Navy;
            this.buttonX4.ColorTable = DevComponents.DotNetBar.eButtonColor.Office2007WithBackground;
            this.buttonX4.Font = new System.Drawing.Font("Segoe UI", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonX4.Image = global::FE_FX.Properties.Resources.ic_donut_large_black_24dp_1x;
            this.buttonX4.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top;
            this.buttonX4.Location = new System.Drawing.Point(55, 6);
            this.buttonX4.Name = "buttonX4";
            this.buttonX4.Size = new System.Drawing.Size(42, 41);
            this.buttonX4.Style = DevComponents.DotNetBar.eDotNetBarStyle.OfficeXP;
            this.buttonX4.TabIndex = 600;
            this.buttonX4.Text = "+ Línea";
            // 
            // buttonX6
            // 
            this.buttonX6.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.buttonX6.BackColor = System.Drawing.Color.Navy;
            this.buttonX6.ColorTable = DevComponents.DotNetBar.eButtonColor.Office2007WithBackground;
            this.buttonX6.Font = new System.Drawing.Font("Segoe UI", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonX6.Image = global::FE_FX.Properties.Resources.ic_donut_large_black_24dp_1x;
            this.buttonX6.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top;
            this.buttonX6.Location = new System.Drawing.Point(103, 6);
            this.buttonX6.Name = "buttonX6";
            this.buttonX6.Size = new System.Drawing.Size(42, 41);
            this.buttonX6.Style = DevComponents.DotNetBar.eDotNetBarStyle.OfficeXP;
            this.buttonX6.TabIndex = 601;
            this.buttonX6.Text = "- Línea";
            // 
            // tabPage5
            // 
            this.tabPage5.Controls.Add(this.label23);
            this.tabPage5.Controls.Add(this.PARA);
            this.tabPage5.Controls.Add(this.ASUNTO);
            this.tabPage5.Controls.Add(this.label18);
            this.tabPage5.Controls.Add(this.label17);
            this.tabPage5.Controls.Add(this.MENSAJE);
            this.tabPage5.Controls.Add(this.buttonX18);
            this.tabPage5.Location = new System.Drawing.Point(4, 22);
            this.tabPage5.Name = "tabPage5";
            this.tabPage5.Padding = new System.Windows.Forms.Padding(3, 3, 3, 3);
            this.tabPage5.Size = new System.Drawing.Size(837, 106);
            this.tabPage5.TabIndex = 4;
            this.tabPage5.Text = "Enviar Correo";
            this.tabPage5.UseVisualStyleBackColor = true;
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Location = new System.Drawing.Point(6, 30);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(29, 13);
            this.label23.TabIndex = 621;
            this.label23.Text = "Para";
            // 
            // PARA
            // 
            this.PARA.Location = new System.Drawing.Point(67, 27);
            this.PARA.Name = "PARA";
            this.PARA.Size = new System.Drawing.Size(764, 20);
            this.PARA.TabIndex = 620;
            // 
            // ASUNTO
            // 
            this.ASUNTO.Location = new System.Drawing.Point(67, 4);
            this.ASUNTO.Name = "ASUNTO";
            this.ASUNTO.Size = new System.Drawing.Size(764, 20);
            this.ASUNTO.TabIndex = 617;
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(6, 7);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(40, 13);
            this.label18.TabIndex = 619;
            this.label18.Text = "Asunto";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(6, 56);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(47, 13);
            this.label17.TabIndex = 618;
            this.label17.Text = "Mensaje";
            // 
            // MENSAJE
            // 
            this.MENSAJE.Location = new System.Drawing.Point(67, 56);
            this.MENSAJE.Name = "MENSAJE";
            this.MENSAJE.Size = new System.Drawing.Size(764, 44);
            this.MENSAJE.TabIndex = 622;
            this.MENSAJE.Text = "";
            // 
            // buttonX18
            // 
            this.buttonX18.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.buttonX18.BackColor = System.Drawing.Color.Navy;
            this.buttonX18.ColorTable = DevComponents.DotNetBar.eButtonColor.Flat;
            this.buttonX18.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonX18.Location = new System.Drawing.Point(6, 78);
            this.buttonX18.Name = "buttonX18";
            this.buttonX18.Size = new System.Drawing.Size(55, 22);
            this.buttonX18.Style = DevComponents.DotNetBar.eDotNetBarStyle.OfficeXP;
            this.buttonX18.TabIndex = 616;
            this.buttonX18.Text = "Enviar";
            this.buttonX18.Click += new System.EventHandler(this.buttonX18_Click_1);
            // 
            // tabPage3
            // 
            this.tabPage3.Controls.Add(this.comboBoxEx1);
            this.tabPage3.Controls.Add(this.labelX32);
            this.tabPage3.Controls.Add(this.buttonX14);
            this.tabPage3.Controls.Add(this.CodigoPostal);
            this.tabPage3.Controls.Add(this.labelX26);
            this.tabPage3.Controls.Add(this.EstadoCE);
            this.tabPage3.Controls.Add(this.labelX27);
            this.tabPage3.Controls.Add(this.noInterior);
            this.tabPage3.Controls.Add(this.labelX25);
            this.tabPage3.Controls.Add(this.labelX22);
            this.tabPage3.Controls.Add(this.Pais);
            this.tabPage3.Controls.Add(this.noExterior);
            this.tabPage3.Controls.Add(this.labelX24);
            this.tabPage3.Controls.Add(this.calle);
            this.tabPage3.Controls.Add(this.labelX23);
            this.tabPage3.Controls.Add(this.NumRegIdTrib);
            this.tabPage3.Controls.Add(this.labelX21);
            this.tabPage3.Controls.Add(this.Incoterm);
            this.tabPage3.Controls.Add(this.labelX20);
            this.tabPage3.Controls.Add(this.activarCE);
            this.tabPage3.Location = new System.Drawing.Point(4, 22);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Size = new System.Drawing.Size(837, 106);
            this.tabPage3.TabIndex = 2;
            this.tabPage3.Text = "Comercio Exterior";
            this.tabPage3.UseVisualStyleBackColor = true;
            // 
            // comboBoxEx1
            // 
            this.comboBoxEx1.DisplayMember = "Text";
            this.comboBoxEx1.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.comboBoxEx1.ForeColor = System.Drawing.Color.Black;
            this.comboBoxEx1.FormattingEnabled = true;
            this.comboBoxEx1.ItemHeight = 16;
            this.comboBoxEx1.Items.AddRange(new object[] {
            this.comboItem39});
            this.comboBoxEx1.Location = new System.Drawing.Point(597, 76);
            this.comboBoxEx1.Name = "comboBoxEx1";
            this.comboBoxEx1.Size = new System.Drawing.Size(89, 22);
            this.comboBoxEx1.TabIndex = 19;
            this.comboBoxEx1.Text = "A1";
            // 
            // comboItem39
            // 
            this.comboItem39.Text = "CAN";
            // 
            // labelX32
            // 
            this.labelX32.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX32.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX32.ForeColor = System.Drawing.Color.Black;
            this.labelX32.Location = new System.Drawing.Point(474, 79);
            this.labelX32.Name = "labelX32";
            this.labelX32.Size = new System.Drawing.Size(117, 15);
            this.labelX32.TabIndex = 18;
            this.labelX32.Text = "Tipo de documento";
            // 
            // buttonX14
            // 
            this.buttonX14.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.buttonX14.BackColor = System.Drawing.Color.LightSteelBlue;
            this.buttonX14.ColorTable = DevComponents.DotNetBar.eButtonColor.Flat;
            this.buttonX14.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonX14.Location = new System.Drawing.Point(242, 3);
            this.buttonX14.Name = "buttonX14";
            this.buttonX14.Size = new System.Drawing.Size(100, 17);
            this.buttonX14.Style = DevComponents.DotNetBar.eDotNetBarStyle.OfficeXP;
            this.buttonX14.TabIndex = 17;
            this.buttonX14.Text = "Pegar desde Excel";
            this.buttonX14.TextAlignment = DevComponents.DotNetBar.eButtonTextAlignment.Left;
            this.buttonX14.Click += new System.EventHandler(this.buttonX14_Click);
            // 
            // CodigoPostal
            // 
            this.CodigoPostal.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.CodigoPostal.Border.Class = "TextBoxBorder";
            this.CodigoPostal.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.CodigoPostal.ForeColor = System.Drawing.Color.Black;
            this.CodigoPostal.Location = new System.Drawing.Point(318, 76);
            this.CodigoPostal.Name = "CodigoPostal";
            this.CodigoPostal.Size = new System.Drawing.Size(151, 20);
            this.CodigoPostal.TabIndex = 16;
            // 
            // labelX26
            // 
            this.labelX26.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX26.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX26.ForeColor = System.Drawing.Color.Black;
            this.labelX26.Location = new System.Drawing.Point(242, 79);
            this.labelX26.Name = "labelX26";
            this.labelX26.Size = new System.Drawing.Size(70, 15);
            this.labelX26.TabIndex = 15;
            this.labelX26.Text = "Código Postal";
            // 
            // EstadoCE
            // 
            this.EstadoCE.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.EstadoCE.Border.Class = "TextBoxBorder";
            this.EstadoCE.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.EstadoCE.ForeColor = System.Drawing.Color.Black;
            this.EstadoCE.Location = new System.Drawing.Point(55, 76);
            this.EstadoCE.Name = "EstadoCE";
            this.EstadoCE.Size = new System.Drawing.Size(177, 20);
            this.EstadoCE.TabIndex = 14;
            // 
            // labelX27
            // 
            this.labelX27.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX27.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX27.ForeColor = System.Drawing.Color.Black;
            this.labelX27.Location = new System.Drawing.Point(5, 79);
            this.labelX27.Name = "labelX27";
            this.labelX27.Size = new System.Drawing.Size(44, 15);
            this.labelX27.TabIndex = 13;
            this.labelX27.Text = "Estado";
            // 
            // noInterior
            // 
            this.noInterior.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.noInterior.Border.Class = "TextBoxBorder";
            this.noInterior.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.noInterior.ForeColor = System.Drawing.Color.Black;
            this.noInterior.Location = new System.Drawing.Point(522, 50);
            this.noInterior.Name = "noInterior";
            this.noInterior.Size = new System.Drawing.Size(164, 20);
            this.noInterior.TabIndex = 12;
            // 
            // labelX25
            // 
            this.labelX25.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX25.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX25.ForeColor = System.Drawing.Color.Black;
            this.labelX25.Location = new System.Drawing.Point(474, 53);
            this.labelX25.Name = "labelX25";
            this.labelX25.Size = new System.Drawing.Size(42, 15);
            this.labelX25.TabIndex = 11;
            this.labelX25.Text = "Interior";
            // 
            // labelX22
            // 
            this.labelX22.AutoSize = true;
            this.labelX22.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX22.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX22.ForeColor = System.Drawing.Color.Black;
            this.labelX22.Location = new System.Drawing.Point(474, 27);
            this.labelX22.Name = "labelX22";
            this.labelX22.Size = new System.Drawing.Size(24, 15);
            this.labelX22.TabIndex = 5;
            this.labelX22.Text = "País";
            // 
            // Pais
            // 
            this.Pais.DisplayMember = "Text";
            this.Pais.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.Pais.ForeColor = System.Drawing.Color.Black;
            this.Pais.FormattingEnabled = true;
            this.Pais.ItemHeight = 16;
            this.Pais.Items.AddRange(new object[] {
            this.comboItem23});
            this.Pais.Location = new System.Drawing.Point(522, 24);
            this.Pais.Name = "Pais";
            this.Pais.Size = new System.Drawing.Size(69, 22);
            this.Pais.TabIndex = 6;
            this.Pais.Text = "USA";
            // 
            // comboItem23
            // 
            this.comboItem23.Text = "CAN";
            // 
            // noExterior
            // 
            this.noExterior.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.noExterior.Border.Class = "TextBoxBorder";
            this.noExterior.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.noExterior.ForeColor = System.Drawing.Color.Black;
            this.noExterior.Location = new System.Drawing.Point(318, 50);
            this.noExterior.Name = "noExterior";
            this.noExterior.Size = new System.Drawing.Size(151, 20);
            this.noExterior.TabIndex = 10;
            // 
            // labelX24
            // 
            this.labelX24.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX24.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX24.ForeColor = System.Drawing.Color.Black;
            this.labelX24.Location = new System.Drawing.Point(242, 53);
            this.labelX24.Name = "labelX24";
            this.labelX24.Size = new System.Drawing.Size(70, 15);
            this.labelX24.TabIndex = 9;
            this.labelX24.Text = "Exterior";
            // 
            // calle
            // 
            this.calle.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.calle.Border.Class = "TextBoxBorder";
            this.calle.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.calle.ForeColor = System.Drawing.Color.Black;
            this.calle.Location = new System.Drawing.Point(55, 50);
            this.calle.Name = "calle";
            this.calle.Size = new System.Drawing.Size(177, 20);
            this.calle.TabIndex = 8;
            // 
            // labelX23
            // 
            this.labelX23.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX23.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX23.ForeColor = System.Drawing.Color.Black;
            this.labelX23.Location = new System.Drawing.Point(5, 53);
            this.labelX23.Name = "labelX23";
            this.labelX23.Size = new System.Drawing.Size(44, 15);
            this.labelX23.TabIndex = 7;
            this.labelX23.Text = "Calle";
            // 
            // NumRegIdTrib
            // 
            this.NumRegIdTrib.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.NumRegIdTrib.Border.Class = "TextBoxBorder";
            this.NumRegIdTrib.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.NumRegIdTrib.ForeColor = System.Drawing.Color.Black;
            this.NumRegIdTrib.Location = new System.Drawing.Point(318, 24);
            this.NumRegIdTrib.Name = "NumRegIdTrib";
            this.NumRegIdTrib.Size = new System.Drawing.Size(151, 20);
            this.NumRegIdTrib.TabIndex = 4;
            this.NumRegIdTrib.Leave += new System.EventHandler(this.NumRegIdTrib_Leave);
            // 
            // labelX21
            // 
            this.labelX21.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX21.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX21.ForeColor = System.Drawing.Color.Black;
            this.labelX21.Location = new System.Drawing.Point(155, 26);
            this.labelX21.Name = "labelX21";
            this.labelX21.Size = new System.Drawing.Size(157, 15);
            this.labelX21.TabIndex = 3;
            this.labelX21.Text = "Número de Registro Tributarío";
            // 
            // Incoterm
            // 
            this.Incoterm.DisplayMember = "Text";
            this.Incoterm.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.Incoterm.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Incoterm.ForeColor = System.Drawing.Color.Black;
            this.Incoterm.FormattingEnabled = true;
            this.Incoterm.ItemHeight = 16;
            this.Incoterm.Items.AddRange(new object[] {
            this.comboItem24,
            this.comboItem25,
            this.comboItem26,
            this.comboItem27,
            this.comboItem28,
            this.comboItem29,
            this.comboItem30,
            this.comboItem31,
            this.comboItem32,
            this.comboItem33,
            this.comboItem34,
            this.comboItem35,
            this.comboItem36,
            this.comboItem37,
            this.comboItem38});
            this.Incoterm.Location = new System.Drawing.Point(55, 23);
            this.Incoterm.Name = "Incoterm";
            this.Incoterm.Size = new System.Drawing.Size(78, 22);
            this.Incoterm.TabIndex = 2;
            // 
            // comboItem24
            // 
            this.comboItem24.Text = "CFR";
            // 
            // comboItem25
            // 
            this.comboItem25.Text = "CIF";
            // 
            // comboItem26
            // 
            this.comboItem26.Text = "CPT";
            // 
            // comboItem27
            // 
            this.comboItem27.Text = "CIP";
            // 
            // comboItem28
            // 
            this.comboItem28.Text = "DAF";
            // 
            // comboItem29
            // 
            this.comboItem29.Text = "DAP";
            // 
            // comboItem30
            // 
            this.comboItem30.Text = "DAT\r\n";
            // 
            // comboItem31
            // 
            this.comboItem31.Text = "DES";
            // 
            // comboItem32
            // 
            this.comboItem32.Text = "DEQ";
            // 
            // comboItem33
            // 
            this.comboItem33.Text = "DDU";
            // 
            // comboItem34
            // 
            this.comboItem34.Text = "DDP";
            // 
            // comboItem35
            // 
            this.comboItem35.Text = "EXW";
            // 
            // comboItem36
            // 
            this.comboItem36.Text = "FCA";
            // 
            // comboItem37
            // 
            this.comboItem37.Text = "FAS";
            // 
            // comboItem38
            // 
            this.comboItem38.Text = "FOB";
            // 
            // labelX20
            // 
            this.labelX20.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX20.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX20.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelX20.ForeColor = System.Drawing.Color.Black;
            this.labelX20.Location = new System.Drawing.Point(5, 26);
            this.labelX20.Name = "labelX20";
            this.labelX20.Size = new System.Drawing.Size(44, 17);
            this.labelX20.TabIndex = 1;
            this.labelX20.Text = "Incoterm";
            // 
            // activarCE
            // 
            this.activarCE.AutoSize = true;
            this.activarCE.Checked = true;
            this.activarCE.CheckState = System.Windows.Forms.CheckState.Checked;
            this.activarCE.Location = new System.Drawing.Point(3, 3);
            this.activarCE.Name = "activarCE";
            this.activarCE.Size = new System.Drawing.Size(231, 17);
            this.activarCE.TabIndex = 0;
            this.activarCE.Text = "Activar complemento de Comercion Exterior";
            this.activarCE.UseVisualStyleBackColor = true;
            // 
            // tabPage4
            // 
            this.tabPage4.Controls.Add(this.labelX29);
            this.tabPage4.Controls.Add(this.direccionEmbarque);
            this.tabPage4.Location = new System.Drawing.Point(4, 22);
            this.tabPage4.Name = "tabPage4";
            this.tabPage4.Padding = new System.Windows.Forms.Padding(3, 3, 3, 3);
            this.tabPage4.Size = new System.Drawing.Size(837, 106);
            this.tabPage4.TabIndex = 3;
            this.tabPage4.Text = "Dirección de embarque";
            this.tabPage4.UseVisualStyleBackColor = true;
            // 
            // labelX29
            // 
            this.labelX29.AutoSize = true;
            this.labelX29.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX29.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX29.ForeColor = System.Drawing.Color.Black;
            this.labelX29.Location = new System.Drawing.Point(6, 6);
            this.labelX29.Name = "labelX29";
            this.labelX29.Size = new System.Drawing.Size(116, 15);
            this.labelX29.TabIndex = 612;
            this.labelX29.Text = "Dirección de embarque";
            // 
            // direccionEmbarque
            // 
            this.direccionEmbarque.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.direccionEmbarque.Border.Class = "TextBoxBorder";
            this.direccionEmbarque.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.direccionEmbarque.ForeColor = System.Drawing.Color.Black;
            this.direccionEmbarque.Location = new System.Drawing.Point(6, 24);
            this.direccionEmbarque.Multiline = true;
            this.direccionEmbarque.Name = "direccionEmbarque";
            this.direccionEmbarque.Size = new System.Drawing.Size(825, 76);
            this.direccionEmbarque.TabIndex = 611;
            this.direccionEmbarque.WatermarkText = "Escriba aquí la dirección de embarque...";
            // 
            // tabPage6
            // 
            this.tabPage6.Controls.Add(this.labelX33);
            this.tabPage6.Controls.Add(this.facturarA);
            this.tabPage6.Location = new System.Drawing.Point(4, 22);
            this.tabPage6.Name = "tabPage6";
            this.tabPage6.Padding = new System.Windows.Forms.Padding(3, 3, 3, 3);
            this.tabPage6.Size = new System.Drawing.Size(837, 106);
            this.tabPage6.TabIndex = 5;
            this.tabPage6.Text = "Facturar a";
            this.tabPage6.UseVisualStyleBackColor = true;
            // 
            // labelX33
            // 
            this.labelX33.AutoSize = true;
            this.labelX33.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX33.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX33.ForeColor = System.Drawing.Color.Black;
            this.labelX33.Location = new System.Drawing.Point(6, 6);
            this.labelX33.Name = "labelX33";
            this.labelX33.Size = new System.Drawing.Size(52, 15);
            this.labelX33.TabIndex = 614;
            this.labelX33.Text = "Facturar a";
            // 
            // facturarA
            // 
            this.facturarA.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.facturarA.Border.Class = "TextBoxBorder";
            this.facturarA.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.facturarA.ForeColor = System.Drawing.Color.Black;
            this.facturarA.Location = new System.Drawing.Point(6, 24);
            this.facturarA.Multiline = true;
            this.facturarA.Name = "facturarA";
            this.facturarA.Size = new System.Drawing.Size(825, 76);
            this.facturarA.TabIndex = 613;
            this.facturarA.WatermarkText = "Escriba aquí facturar a...";
            // 
            // btnEmisor
            // 
            this.btnEmisor.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btnEmisor.ColorTable = DevComponents.DotNetBar.eButtonColor.Office2007WithBackground;
            this.btnEmisor.Image = global::FE_FX.Properties.Resources.ic_search_black_18dp_1x;
            this.btnEmisor.Location = new System.Drawing.Point(405, 64);
            this.btnEmisor.Name = "btnEmisor";
            this.btnEmisor.Size = new System.Drawing.Size(74, 20);
            this.btnEmisor.Style = DevComponents.DotNetBar.eDotNetBarStyle.OfficeXP;
            this.btnEmisor.TabIndex = 29;
            this.btnEmisor.Text = "Receptor";
            this.btnEmisor.TextAlignment = DevComponents.DotNetBar.eButtonTextAlignment.Left;
            this.btnEmisor.Click += new System.EventHandler(this.buttonX4_Click);
            // 
            // buttonX10
            // 
            this.buttonX10.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.buttonX10.BackColor = System.Drawing.Color.Navy;
            this.buttonX10.ColorTable = DevComponents.DotNetBar.eButtonColor.Office2007WithBackground;
            this.buttonX10.Font = new System.Drawing.Font("Segoe UI", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonX10.Image = global::FE_FX.Properties.Resources.ic_label_outline_black_24dp_1x;
            this.buttonX10.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top;
            this.buttonX10.Location = new System.Drawing.Point(254, 12);
            this.buttonX10.Name = "buttonX10";
            this.buttonX10.Size = new System.Drawing.Size(42, 41);
            this.buttonX10.Style = DevComponents.DotNetBar.eDotNetBarStyle.OfficeXP;
            this.buttonX10.TabIndex = 5;
            this.buttonX10.Text = "Insertar";
            this.buttonX10.Click += new System.EventHandler(this.buttonX10_Click);
            // 
            // buttonX11
            // 
            this.buttonX11.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.buttonX11.BackColor = System.Drawing.Color.Navy;
            this.buttonX11.ColorTable = DevComponents.DotNetBar.eButtonColor.Office2007WithBackground;
            this.buttonX11.Font = new System.Drawing.Font("Segoe UI", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonX11.Image = global::FE_FX.Properties.Resources.ic_delete_sweep_black_24dp_1x;
            this.buttonX11.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top;
            this.buttonX11.Location = new System.Drawing.Point(298, 12);
            this.buttonX11.Name = "buttonX11";
            this.buttonX11.Size = new System.Drawing.Size(42, 41);
            this.buttonX11.Style = DevComponents.DotNetBar.eDotNetBarStyle.OfficeXP;
            this.buttonX11.TabIndex = 6;
            this.buttonX11.Text = "Eliminar";
            this.buttonX11.Click += new System.EventHandler(this.buttonX11_Click_1);
            // 
            // buttonX9
            // 
            this.buttonX9.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.buttonX9.BackColor = System.Drawing.Color.Navy;
            this.buttonX9.ColorTable = DevComponents.DotNetBar.eButtonColor.Office2007WithBackground;
            this.buttonX9.Font = new System.Drawing.Font("Segoe UI", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonX9.Image = global::FE_FX.Properties.Resources.ic_cancel_black_24dp_1x;
            this.buttonX9.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top;
            this.buttonX9.Location = new System.Drawing.Point(176, 12);
            this.buttonX9.Name = "buttonX9";
            this.buttonX9.Size = new System.Drawing.Size(42, 41);
            this.buttonX9.Style = DevComponents.DotNetBar.eDotNetBarStyle.OfficeXP;
            this.buttonX9.TabIndex = 4;
            this.buttonX9.Text = "Cancelar";
            this.buttonX9.Click += new System.EventHandler(this.buttonX9_Click);
            // 
            // buttonX7
            // 
            this.buttonX7.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.buttonX7.BackColor = System.Drawing.Color.Navy;
            this.buttonX7.ColorTable = DevComponents.DotNetBar.eButtonColor.Office2007WithBackground;
            this.buttonX7.Font = new System.Drawing.Font("Segoe UI", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonX7.Image = global::FE_FX.Properties.Resources.ic_local_printshop_black_24dp_1x;
            this.buttonX7.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top;
            this.buttonX7.Location = new System.Drawing.Point(133, 12);
            this.buttonX7.Name = "buttonX7";
            this.buttonX7.Size = new System.Drawing.Size(41, 41);
            this.buttonX7.Style = DevComponents.DotNetBar.eDotNetBarStyle.OfficeXP;
            this.buttonX7.TabIndex = 3;
            this.buttonX7.Text = "Imprimir";
            this.buttonX7.Click += new System.EventHandler(this.buttonX7_Click_1);
            // 
            // buttonX5
            // 
            this.buttonX5.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.buttonX5.BackColor = System.Drawing.Color.Navy;
            this.buttonX5.ColorTable = DevComponents.DotNetBar.eButtonColor.Office2007WithBackground;
            this.buttonX5.Font = new System.Drawing.Font("Segoe UI", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonX5.Image = global::FE_FX.Properties.Resources.ic_save_black_24dp_1x;
            this.buttonX5.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top;
            this.buttonX5.Location = new System.Drawing.Point(56, 12);
            this.buttonX5.Name = "buttonX5";
            this.buttonX5.Size = new System.Drawing.Size(42, 41);
            this.buttonX5.Style = DevComponents.DotNetBar.eDotNetBarStyle.OfficeXP;
            this.buttonX5.TabIndex = 1;
            this.buttonX5.Text = "Guardar";
            this.buttonX5.Click += new System.EventHandler(this.buttonX5_Click);
            // 
            // buttonX2
            // 
            this.buttonX2.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.buttonX2.BackColor = System.Drawing.Color.Navy;
            this.buttonX2.ColorTable = DevComponents.DotNetBar.eButtonColor.Office2007WithBackground;
            this.buttonX2.Font = new System.Drawing.Font("Segoe UI", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonX2.Image = global::FE_FX.Properties.Resources.ic_create_new_folder_black_24dp_1x;
            this.buttonX2.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top;
            this.buttonX2.Location = new System.Drawing.Point(12, 12);
            this.buttonX2.Name = "buttonX2";
            this.buttonX2.Size = new System.Drawing.Size(42, 41);
            this.buttonX2.Style = DevComponents.DotNetBar.eDotNetBarStyle.OfficeXP;
            this.buttonX2.TabIndex = 0;
            this.buttonX2.Text = "Nuevo";
            this.buttonX2.Click += new System.EventHandler(this.buttonX2_Click_1);
            // 
            // buttonX1
            // 
            this.buttonX1.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.buttonX1.BackColor = System.Drawing.Color.Navy;
            this.buttonX1.ColorTable = DevComponents.DotNetBar.eButtonColor.Flat;
            this.buttonX1.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonX1.Image = global::FE_FX.Properties.Resources.ic_search_black_18dp_1x;
            this.buttonX1.Location = new System.Drawing.Point(12, 89);
            this.buttonX1.Name = "buttonX1";
            this.buttonX1.Size = new System.Drawing.Size(54, 22);
            this.buttonX1.Style = DevComponents.DotNetBar.eDotNetBarStyle.OfficeXP;
            this.buttonX1.TabIndex = 11;
            this.buttonX1.Text = "Serie";
            this.buttonX1.TextAlignment = DevComponents.DotNetBar.eButtonTextAlignment.Right;
            this.buttonX1.Click += new System.EventHandler(this.buttonX1_Click);
            // 
            // buttonX12
            // 
            this.buttonX12.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.buttonX12.BackColor = System.Drawing.Color.Navy;
            this.buttonX12.ColorTable = DevComponents.DotNetBar.eButtonColor.Office2007WithBackground;
            this.buttonX12.Font = new System.Drawing.Font("Segoe UI", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonX12.Image = global::FE_FX.Properties.Resources.ic_delete_sweep_black_24dp_1x;
            this.buttonX12.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top;
            this.buttonX12.Location = new System.Drawing.Point(342, 12);
            this.buttonX12.Name = "buttonX12";
            this.buttonX12.Size = new System.Drawing.Size(42, 41);
            this.buttonX12.Style = DevComponents.DotNetBar.eDotNetBarStyle.OfficeXP;
            this.buttonX12.TabIndex = 605;
            this.buttonX12.Text = "Importar";
            // 
            // motivoDescuento
            // 
            this.motivoDescuento.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.motivoDescuento.Border.Class = "TextBoxBorder";
            this.motivoDescuento.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.motivoDescuento.ForeColor = System.Drawing.Color.Black;
            this.motivoDescuento.Location = new System.Drawing.Point(368, 334);
            this.motivoDescuento.Multiline = true;
            this.motivoDescuento.Name = "motivoDescuento";
            this.motivoDescuento.Size = new System.Drawing.Size(275, 20);
            this.motivoDescuento.TabIndex = 607;
            this.motivoDescuento.WatermarkText = "Escriba aquí el motivo de descuento";
            // 
            // labelX28
            // 
            this.labelX28.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX28.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX28.ForeColor = System.Drawing.Color.Black;
            this.labelX28.Location = new System.Drawing.Point(254, 334);
            this.labelX28.Name = "labelX28";
            this.labelX28.Size = new System.Drawing.Size(108, 20);
            this.labelX28.TabIndex = 608;
            this.labelX28.Text = "Motivo de descuento";
            // 
            // aplicarDescuentoAutomatico
            // 
            this.aplicarDescuentoAutomatico.AutoSize = true;
            this.aplicarDescuentoAutomatico.Location = new System.Drawing.Point(543, 195);
            this.aplicarDescuentoAutomatico.Name = "aplicarDescuentoAutomatico";
            this.aplicarDescuentoAutomatico.Size = new System.Drawing.Size(213, 17);
            this.aplicarDescuentoAutomatico.TabIndex = 609;
            this.aplicarDescuentoAutomatico.Text = "Aplicar líneas de descuento automatico";
            this.aplicarDescuentoAutomatico.UseVisualStyleBackColor = true;
            this.aplicarDescuentoAutomatico.Visible = false;
            this.aplicarDescuentoAutomatico.Click += new System.EventHandler(this.aplicarDescuentoAutomatico_Click);
            // 
            // notasPDF
            // 
            this.notasPDF.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.notasPDF.Border.Class = "TextBoxBorder";
            this.notasPDF.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.notasPDF.ForeColor = System.Drawing.Color.Black;
            this.notasPDF.Location = new System.Drawing.Point(13, 440);
            this.notasPDF.Multiline = true;
            this.notasPDF.Name = "notasPDF";
            this.notasPDF.Size = new System.Drawing.Size(631, 50);
            this.notasPDF.TabIndex = 610;
            this.notasPDF.Text = "THIS INVOICE IS ONLY FOR CUSTOMS PURPOSES - NOT FOR CUSTOMER REFERENCE";
            this.notasPDF.WatermarkText = "Escriba aquí notas en el PDF ... ";
            // 
            // labelX30
            // 
            this.labelX30.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX30.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX30.ForeColor = System.Drawing.Color.Black;
            this.labelX30.Location = new System.Drawing.Point(13, 419);
            this.labelX30.Name = "labelX30";
            this.labelX30.Size = new System.Drawing.Size(100, 19);
            this.labelX30.TabIndex = 611;
            this.labelX30.Text = "Notas en el PDF";
            // 
            // buttonX15
            // 
            this.buttonX15.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.buttonX15.BackColor = System.Drawing.Color.Navy;
            this.buttonX15.ColorTable = DevComponents.DotNetBar.eButtonColor.Flat;
            this.buttonX15.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonX15.Location = new System.Drawing.Point(405, 12);
            this.buttonX15.Name = "buttonX15";
            this.buttonX15.Size = new System.Drawing.Size(93, 22);
            this.buttonX15.Style = DevComponents.DotNetBar.eDotNetBarStyle.OfficeXP;
            this.buttonX15.TabIndex = 612;
            this.buttonX15.Text = "Generar plantilla";
            this.buttonX15.TextAlignment = DevComponents.DotNetBar.eButtonTextAlignment.Left;
            this.buttonX15.Click += new System.EventHandler(this.buttonX15_Click);
            // 
            // labelX31
            // 
            this.labelX31.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX31.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX31.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelX31.ForeColor = System.Drawing.Color.Black;
            this.labelX31.Location = new System.Drawing.Point(13, 329);
            this.labelX31.Name = "labelX31";
            this.labelX31.Size = new System.Drawing.Size(120, 27);
            this.labelX31.TabIndex = 613;
            this.labelX31.Text = "Total sin descuento";
            this.labelX31.TextAlignment = System.Drawing.StringAlignment.Far;
            // 
            // totalSinDescuento
            // 
            this.totalSinDescuento.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.totalSinDescuento.Border.Class = "TextBoxBorder";
            this.totalSinDescuento.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.totalSinDescuento.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.totalSinDescuento.ForeColor = System.Drawing.Color.Black;
            this.totalSinDescuento.Location = new System.Drawing.Point(139, 330);
            this.totalSinDescuento.Name = "totalSinDescuento";
            this.totalSinDescuento.ReadOnly = true;
            this.totalSinDescuento.Size = new System.Drawing.Size(109, 25);
            this.totalSinDescuento.TabIndex = 614;
            this.totalSinDescuento.Text = "0";
            this.totalSinDescuento.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // buttonX16
            // 
            this.buttonX16.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.buttonX16.BackColor = System.Drawing.Color.Navy;
            this.buttonX16.ColorTable = DevComponents.DotNetBar.eButtonColor.Flat;
            this.buttonX16.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonX16.Location = new System.Drawing.Point(504, 12);
            this.buttonX16.Name = "buttonX16";
            this.buttonX16.Size = new System.Drawing.Size(82, 22);
            this.buttonX16.Style = DevComponents.DotNetBar.eDotNetBarStyle.OfficeXP;
            this.buttonX16.TabIndex = 615;
            this.buttonX16.Text = "Descargar XML";
            this.buttonX16.TextAlignment = DevComponents.DotNetBar.eButtonTextAlignment.Left;
            this.buttonX16.Click += new System.EventHandler(this.buttonX16_Click);
            // 
            // buttonX19
            // 
            this.buttonX19.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.buttonX19.BackColor = System.Drawing.Color.Navy;
            this.buttonX19.ColorTable = DevComponents.DotNetBar.eButtonColor.Flat;
            this.buttonX19.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonX19.Location = new System.Drawing.Point(592, 12);
            this.buttonX19.Name = "buttonX19";
            this.buttonX19.Size = new System.Drawing.Size(81, 22);
            this.buttonX19.Style = DevComponents.DotNetBar.eDotNetBarStyle.OfficeXP;
            this.buttonX19.TabIndex = 616;
            this.buttonX19.Text = "Descargar PDF";
            this.buttonX19.TextAlignment = DevComponents.DotNetBar.eButtonTextAlignment.Left;
            this.buttonX19.Click += new System.EventHandler(this.buttonX19_Click);
            // 
            // receptor
            // 
            this.receptor.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.receptor.Border.Class = "TextBoxBorder";
            this.receptor.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.receptor.ForeColor = System.Drawing.Color.Black;
            this.receptor.Location = new System.Drawing.Point(485, 64);
            this.receptor.Name = "receptor";
            this.receptor.Size = new System.Drawing.Size(373, 20);
            this.receptor.TabIndex = 31;
            this.receptor.TextChanged += new System.EventHandler(this.rfc_TextChanged);
            // 
            // labelX16
            // 
            this.labelX16.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX16.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX16.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelX16.ForeColor = System.Drawing.Color.Black;
            this.labelX16.Location = new System.Drawing.Point(405, 39);
            this.labelX16.Name = "labelX16";
            this.labelX16.Size = new System.Drawing.Size(100, 19);
            this.labelX16.TabIndex = 617;
            this.labelX16.Text = "Motivo de traslado";
            // 
            // motivoTraslado
            // 
            this.motivoTraslado.FormattingEnabled = true;
            this.motivoTraslado.Items.AddRange(new object[] {
            "01-Envío de mercancias facturadas con anterioridad",
            "02-Reubicación de mercancías propias",
            "03-Envío de mercancías objeto de contrato de consignación",
            "04-Envío de mercancías para posterior enajenación",
            "05-Envío de mercancías propiedad de terceros",
            "99-Otros"});
            this.motivoTraslado.Location = new System.Drawing.Point(504, 37);
            this.motivoTraslado.Name = "motivoTraslado";
            this.motivoTraslado.Size = new System.Drawing.Size(354, 21);
            this.motivoTraslado.TabIndex = 618;
            this.motivoTraslado.Text = "01-Envío de mercancias facturadas con anterioridad";
            this.motivoTraslado.SelectedIndexChanged += new System.EventHandler(this.comboBox1_SelectedIndexChanged);
            // 
            // aplicarDescuento
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(863, 520);
            this.Controls.Add(this.motivoTraslado);
            this.Controls.Add(this.labelX16);
            this.Controls.Add(this.buttonX19);
            this.Controls.Add(this.buttonX16);
            this.Controls.Add(this.totalSinDescuento);
            this.Controls.Add(this.labelX31);
            this.Controls.Add(this.buttonX15);
            this.Controls.Add(this.labelX30);
            this.Controls.Add(this.notasPDF);
            this.Controls.Add(this.aplicarDescuentoAutomatico);
            this.Controls.Add(this.labelX28);
            this.Controls.Add(this.motivoDescuento);
            this.Controls.Add(this.buttonX12);
            this.Controls.Add(this.tabControl1);
            this.Controls.Add(this.FORMATO_IMPRESION);
            this.Controls.Add(this.TIPO_FACTURA);
            this.Controls.Add(this.METODO_DE_PAGO);
            this.Controls.Add(this.ENTITY_ID);
            this.Controls.Add(this.OBSERVACIONES);
            this.Controls.Add(this.IEPStr);
            this.Controls.Add(this.IMPUESTO);
            this.Controls.Add(this.RETENCION);
            this.Controls.Add(this.TOTAL);
            this.Controls.Add(this.DESCUENTO);
            this.Controls.Add(this.SUBTOTAL);
            this.Controls.Add(this.buttonX17);
            this.Controls.Add(this.BANCO);
            this.Controls.Add(this.receptor);
            this.Controls.Add(this.rfc);
            this.Controls.Add(this.CUENTA_BANCARIA);
            this.Controls.Add(this.labelX19);
            this.Controls.Add(this.labelX13);
            this.Controls.Add(this.PATH);
            this.Controls.Add(this.UUID);
            this.Controls.Add(this.FORMA_PAGO);
            this.Controls.Add(this.labelX5);
            this.Controls.Add(this.labelX15);
            this.Controls.Add(this.labelX7);
            this.Controls.Add(this.labelX9);
            this.Controls.Add(this.labelX10);
            this.Controls.Add(this.btnEmisor);
            this.Controls.Add(this.buttonX10);
            this.Controls.Add(this.buttonX11);
            this.Controls.Add(this.buttonX9);
            this.Controls.Add(this.buttonX7);
            this.Controls.Add(this.buttonX5);
            this.Controls.Add(this.buttonX2);
            this.Controls.Add(this.labelX11);
            this.Controls.Add(this.EMPRESA_NOMBRE);
            this.Controls.Add(this.labelX14);
            this.Controls.Add(this.TIPO);
            this.Controls.Add(this.labelX12);
            this.Controls.Add(this.MONEDA);
            this.Controls.Add(this.FECHA);
            this.Controls.Add(this.ESTADO);
            this.Controls.Add(this.SERIE);
            this.Controls.Add(this.TIPO_CAMBIO);
            this.Controls.Add(this.labelX6);
            this.Controls.Add(this.IMPUESTO_DATO);
            this.Controls.Add(this.buttonX1);
            this.Controls.Add(this.ID);
            this.Controls.Add(this.statusStrip1);
            this.Controls.Add(this.labelX4);
            this.Controls.Add(this.labelX3);
            this.Controls.Add(this.labelX17);
            this.Controls.Add(this.labelX18);
            this.Controls.Add(this.labelX2);
            this.Controls.Add(this.labelX1);
            this.Controls.Add(this.labelX8);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.KeyPreview = true;
            this.MaximizeBox = false;
            this.Name = "aplicarDescuento";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "CFDI";
            this.Load += new System.EventHandler(this.frmUsuarios_Load);
            this.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.frmFormatos_KeyPress);
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dtgrdGeneral)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtgPolizas)).EndInit();
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage2.ResumeLayout(false);
            this.tabPage5.ResumeLayout(false);
            this.tabPage5.PerformLayout();
            this.tabPage3.ResumeLayout(false);
            this.tabPage3.PerformLayout();
            this.tabPage4.ResumeLayout(false);
            this.tabPage4.PerformLayout();
            this.tabPage6.ResumeLayout(false);
            this.tabPage6.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStripStatusLabel informacion;
        private DevComponents.Editors.ComboItem comboItem4;
        private DevComponents.Editors.ComboItem comboItem5;
        private DevComponents.Editors.ComboItem comboItem6;
        private DevComponents.DotNetBar.ButtonX buttonX17;
        private DevComponents.DotNetBar.Controls.TextBoxX BANCO;
        private DevComponents.DotNetBar.Controls.TextBoxX CUENTA_BANCARIA;
        private DevComponents.DotNetBar.LabelX labelX13;
        private DevComponents.DotNetBar.Controls.TextBoxX UUID;
        private DevComponents.DotNetBar.Controls.ComboBoxEx FORMA_PAGO;
        private DevComponents.Editors.ComboItem comboItem11;
        private DevComponents.Editors.ComboItem comboItem13;
        private DevComponents.DotNetBar.LabelX labelX5;
        private DevComponents.DotNetBar.LabelX labelX7;
        private DevComponents.DotNetBar.LabelX labelX9;
        private DevComponents.DotNetBar.LabelX labelX10;
        private DevComponents.DotNetBar.ButtonX btnEmisor;
        private DevComponents.DotNetBar.ButtonX buttonX10;
        private DevComponents.DotNetBar.ButtonX buttonX11;
        private DevComponents.DotNetBar.ButtonX buttonX9;
        private DevComponents.DotNetBar.ButtonX buttonX7;
        private DevComponents.DotNetBar.ButtonX buttonX5;
        private DevComponents.DotNetBar.ButtonX buttonX2;
        private DevComponents.DotNetBar.LabelX labelX12;
        private DevComponents.DotNetBar.Controls.ComboBoxEx MONEDA;
        private DevComponents.DotNetBar.LabelX labelX8;
        private System.Windows.Forms.DateTimePicker FECHA;
        private DevComponents.DotNetBar.Controls.DataGridViewX dtgrdGeneral;
        private DevComponents.DotNetBar.Controls.TextBoxX OBSERVACIONES;
        private DevComponents.DotNetBar.Controls.ComboBoxEx ESTADO;
        private DevComponents.Editors.ComboItem comboItem3;
        private DevComponents.Editors.ComboItem comboItem1;
        private DevComponents.Editors.ComboItem comboItem2;
        private DevComponents.DotNetBar.Controls.TextBoxX SERIE;
        private DevComponents.DotNetBar.Controls.TextBoxX TIPO_CAMBIO;
        private DevComponents.DotNetBar.LabelX labelX6;
        private DevComponents.DotNetBar.Controls.ComboBoxEx IMPUESTO_DATO;
        private DevComponents.Editors.ComboItem comboItem7;
        private DevComponents.Editors.ComboItem comboItem8;
        private DevComponents.Editors.ComboItem comboItem9;
        private DevComponents.Editors.ComboItem comboItem10;
        private DevComponents.Editors.ComboItem comboItem12;
        private DevComponents.DotNetBar.ButtonX buttonX1;
        private DevComponents.DotNetBar.Controls.TextBoxX ID;
        private DevComponents.Editors.ComboItem comboItem14;
        private DevComponents.Editors.ComboItem comboItem15;
        private DevComponents.Editors.ComboItem comboItem16;
        private DevComponents.DotNetBar.Controls.TextBoxX IMPUESTO;
        private DevComponents.DotNetBar.Controls.TextBoxX RETENCION;
        private DevComponents.DotNetBar.LabelX labelX4;
        private DevComponents.DotNetBar.Controls.TextBoxX TOTAL;
        private DevComponents.DotNetBar.LabelX labelX3;
        private DevComponents.DotNetBar.LabelX labelX2;
        private DevComponents.DotNetBar.Controls.TextBoxX SUBTOTAL;
        private DevComponents.DotNetBar.LabelX labelX1;
        private DevComponents.DotNetBar.Controls.ComboBoxEx ENTITY_ID;
        private DevComponents.Editors.ComboItem comboItem17;
        private DevComponents.Editors.ComboItem comboItem18;
        private DevComponents.Editors.ComboItem comboItem19;
        private DevComponents.DotNetBar.LabelX labelX11;
        private DevComponents.DotNetBar.LabelX EMPRESA_NOMBRE;
        private DevComponents.DotNetBar.LabelX labelX14;
        private DevComponents.DotNetBar.Controls.TextBoxX rfc;
        private DevComponents.DotNetBar.LabelX labelX15;
        private System.Windows.Forms.ComboBox METODO_DE_PAGO;
        private DevComponents.DotNetBar.Controls.TextBoxX IEPStr;
        private DevComponents.DotNetBar.LabelX labelX17;
        private DevComponents.DotNetBar.LabelX TIPO;
        private DevComponents.DotNetBar.Controls.ComboBoxEx TIPO_FACTURA;
        private DevComponents.Editors.ComboItem comboItem20;
        private DevComponents.Editors.ComboItem comboItem21;
        private DevComponents.Editors.ComboItem comboItem22;
        private DevComponents.DotNetBar.LabelX labelX18;
        private DevComponents.DotNetBar.Controls.TextBoxX DESCUENTO;
        private DevComponents.DotNetBar.Controls.TextBoxX PATH;
        private DevComponents.DotNetBar.LabelX labelX19;
        private DevComponents.DotNetBar.Controls.DataGridViewX dtgPolizas;
        private System.Windows.Forms.FolderBrowserDialog folderBrowserDialog1;
        private System.Windows.Forms.DataGridViewTextBoxColumn ACCOUNT_ID;
        private System.Windows.Forms.DataGridViewTextBoxColumn REFERENCE;
        private System.Windows.Forms.DataGridViewTextBoxColumn CARGO;
        private System.Windows.Forms.DataGridViewTextBoxColumn ABONO;
        private DevComponents.DotNetBar.Controls.ComboBoxEx FORMATO_IMPRESION;
        private DevComponents.DotNetBar.ButtonX buttonX6;
        private DevComponents.DotNetBar.ButtonX buttonX4;
        private DevComponents.DotNetBar.ButtonX buttonX3;
        private DevComponents.DotNetBar.ButtonX buttonX8;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TabPage tabPage2;
        private DevComponents.DotNetBar.ButtonX buttonX12;
        private DevComponents.DotNetBar.ButtonX buttonX13;
        private System.Windows.Forms.TabPage tabPage3;
        private DevComponents.DotNetBar.ButtonX buttonX14;
        private DevComponents.DotNetBar.Controls.TextBoxX CodigoPostal;
        private DevComponents.DotNetBar.LabelX labelX26;
        private DevComponents.DotNetBar.Controls.TextBoxX EstadoCE;
        private DevComponents.DotNetBar.LabelX labelX27;
        private DevComponents.DotNetBar.Controls.TextBoxX noInterior;
        private DevComponents.DotNetBar.LabelX labelX25;
        private DevComponents.DotNetBar.LabelX labelX22;
        private DevComponents.DotNetBar.Controls.ComboBoxEx Pais;
        private DevComponents.Editors.ComboItem comboItem23;
        private DevComponents.DotNetBar.Controls.TextBoxX noExterior;
        private DevComponents.DotNetBar.LabelX labelX24;
        private DevComponents.DotNetBar.Controls.TextBoxX calle;
        private DevComponents.DotNetBar.LabelX labelX23;
        private DevComponents.DotNetBar.Controls.TextBoxX NumRegIdTrib;
        private DevComponents.DotNetBar.LabelX labelX21;
        private DevComponents.DotNetBar.Controls.ComboBoxEx Incoterm;
        private DevComponents.Editors.ComboItem comboItem24;
        private DevComponents.Editors.ComboItem comboItem25;
        private DevComponents.Editors.ComboItem comboItem26;
        private DevComponents.Editors.ComboItem comboItem27;
        private DevComponents.Editors.ComboItem comboItem28;
        private DevComponents.Editors.ComboItem comboItem29;
        private DevComponents.Editors.ComboItem comboItem30;
        private DevComponents.Editors.ComboItem comboItem31;
        private DevComponents.Editors.ComboItem comboItem32;
        private DevComponents.Editors.ComboItem comboItem33;
        private DevComponents.Editors.ComboItem comboItem34;
        private DevComponents.Editors.ComboItem comboItem35;
        private DevComponents.Editors.ComboItem comboItem36;
        private DevComponents.Editors.ComboItem comboItem37;
        private DevComponents.Editors.ComboItem comboItem38;
        private DevComponents.DotNetBar.LabelX labelX20;
        private System.Windows.Forms.CheckBox activarCE;
        private DevComponents.DotNetBar.Controls.TextBoxX motivoDescuento;
        private DevComponents.DotNetBar.LabelX labelX28;
        private System.Windows.Forms.CheckBox aplicarDescuentoAutomatico;
        private System.Windows.Forms.TabPage tabPage4;
        private DevComponents.DotNetBar.Controls.TextBoxX notasPDF;
        private DevComponents.DotNetBar.LabelX labelX29;
        private DevComponents.DotNetBar.Controls.TextBoxX direccionEmbarque;
        private DevComponents.DotNetBar.LabelX labelX30;
        private DevComponents.DotNetBar.ButtonX buttonX15;
        private DevComponents.DotNetBar.LabelX labelX31;
        private DevComponents.DotNetBar.Controls.TextBoxX totalSinDescuento;
        private System.Windows.Forms.TabPage tabPage5;
        private DevComponents.DotNetBar.ButtonX buttonX16;
        private DevComponents.DotNetBar.ButtonX buttonX18;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.TextBox PARA;
        private System.Windows.Forms.TextBox ASUNTO;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.RichTextBox MENSAJE;
        private DevComponents.DotNetBar.ButtonX buttonX19;
        private DevComponents.DotNetBar.Controls.ComboBoxEx comboBoxEx1;
        private DevComponents.Editors.ComboItem comboItem39;
        private DevComponents.DotNetBar.LabelX labelX32;
        private System.Windows.Forms.TabPage tabPage6;
        private DevComponents.DotNetBar.LabelX labelX33;
        private DevComponents.DotNetBar.Controls.TextBoxX facturarA;
        private DevComponents.DotNetBar.Controls.TextBoxX receptor;
        private DevComponents.DotNetBar.LabelX labelX16;
        private System.Windows.Forms.ComboBox motivoTraslado;
        private System.Windows.Forms.DataGridViewTextBoxColumn cantidad;
        private System.Windows.Forms.DataGridViewTextBoxColumn unidad;
        private System.Windows.Forms.DataGridViewTextBoxColumn descripcion;
        private System.Windows.Forms.DataGridViewTextBoxColumn noIdentificacion;
        private System.Windows.Forms.DataGridViewTextBoxColumn valorUnitario;
        private System.Windows.Forms.DataGridViewTextBoxColumn importe;
        private System.Windows.Forms.DataGridViewTextBoxColumn fraccionArancelaria;
        private System.Windows.Forms.DataGridViewTextBoxColumn informacionAduaneraFecha;
        private System.Windows.Forms.DataGridViewTextBoxColumn informacionAduaneraAduana;
        private System.Windows.Forms.DataGridViewTextBoxColumn informacionAduaneraNumero;
    }
}