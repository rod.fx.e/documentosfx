﻿namespace FE_FX
{
    partial class frmRetencion2
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmRetencion2));
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.informacion = new System.Windows.Forms.ToolStripStatusLabel();
            this.comboItem4 = new DevComponents.Editors.ComboItem();
            this.comboItem5 = new DevComponents.Editors.ComboItem();
            this.comboItem6 = new DevComponents.Editors.ComboItem();
            this.buttonX17 = new DevComponents.DotNetBar.ButtonX();
            this.labelX13 = new DevComponents.DotNetBar.LabelX();
            this.UUID = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.ESTADO = new DevComponents.DotNetBar.Controls.ComboBoxEx();
            this.comboItem3 = new DevComponents.Editors.ComboItem();
            this.comboItem1 = new DevComponents.Editors.ComboItem();
            this.comboItem2 = new DevComponents.Editors.ComboItem();
            this.SERIE = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.ID = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.comboItem14 = new DevComponents.Editors.ComboItem();
            this.comboItem15 = new DevComponents.Editors.ComboItem();
            this.comboItem16 = new DevComponents.Editors.ComboItem();
            this.montoTotOperacion = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.labelX3 = new DevComponents.DotNetBar.LabelX();
            this.ENTITY_ID = new DevComponents.DotNetBar.Controls.ComboBoxEx();
            this.comboItem17 = new DevComponents.Editors.ComboItem();
            this.comboItem18 = new DevComponents.Editors.ComboItem();
            this.comboItem19 = new DevComponents.Editors.ComboItem();
            this.labelX11 = new DevComponents.DotNetBar.LabelX();
            this.EMPRESA_NOMBRE = new DevComponents.DotNetBar.LabelX();
            this.labelX14 = new DevComponents.DotNetBar.LabelX();
            this.rfc = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.labelX15 = new DevComponents.DotNetBar.LabelX();
            this.PATH = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.labelX19 = new DevComponents.DotNetBar.LabelX();
            this.dtgPolizas = new DevComponents.DotNetBar.Controls.DataGridViewX();
            this.ACCOUNT_ID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.REFERENCE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CARGO = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ABONO = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.folderBrowserDialog1 = new System.Windows.Forms.FolderBrowserDialog();
            this.FORMATO_IMPRESION = new DevComponents.DotNetBar.Controls.ComboBoxEx();
            this.buttonX6 = new DevComponents.DotNetBar.ButtonX();
            this.buttonX4 = new DevComponents.DotNetBar.ButtonX();
            this.buttonX3 = new DevComponents.DotNetBar.ButtonX();
            this.btnEmisor = new DevComponents.DotNetBar.ButtonX();
            this.buttonX9 = new DevComponents.DotNetBar.ButtonX();
            this.buttonX7 = new DevComponents.DotNetBar.ButtonX();
            this.buttonX5 = new DevComponents.DotNetBar.ButtonX();
            this.buttonX2 = new DevComponents.DotNetBar.ButtonX();
            this.buttonX1 = new DevComponents.DotNetBar.ButtonX();
            this.receptortxt = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.dtgTotales = new DevComponents.DotNetBar.Controls.DataGridViewX();
            this.buttonX10 = new DevComponents.DotNetBar.ButtonX();
            this.buttonX11 = new DevComponents.DotNetBar.ButtonX();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.montoTotGrav = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.labelX5 = new DevComponents.DotNetBar.LabelX();
            this.montoTotExent = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.labelX6 = new DevComponents.DotNetBar.LabelX();
            this.montoTotRet = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.labelX2 = new DevComponents.DotNetBar.LabelX();
            this.DescRetenc = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.CURPE = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.labelX4 = new DevComponents.DotNetBar.LabelX();
            this.ejercicio = new DevComponents.DotNetBar.Controls.ComboBoxEx();
            this.comboItem7 = new DevComponents.Editors.ComboItem();
            this.comboItem8 = new DevComponents.Editors.ComboItem();
            this.comboItem9 = new DevComponents.Editors.ComboItem();
            this.comboItem10 = new DevComponents.Editors.ComboItem();
            this.comboItem11 = new DevComponents.Editors.ComboItem();
            this.labelX1 = new DevComponents.DotNetBar.LabelX();
            this.fin = new DevComponents.DotNetBar.Controls.ComboBoxEx();
            this.labelX7 = new DevComponents.DotNetBar.LabelX();
            this.inicio = new DevComponents.DotNetBar.Controls.ComboBoxEx();
            this.labelX8 = new DevComponents.DotNetBar.LabelX();
            this.buttonX8 = new DevComponents.DotNetBar.ButtonX();
            this.labelX9 = new DevComponents.DotNetBar.LabelX();
            this.CveRetencCmb = new DevComponents.DotNetBar.Controls.ComboBoxEx();
            this.labelX10 = new DevComponents.DotNetBar.LabelX();
            this.labelX12 = new DevComponents.DotNetBar.LabelX();
            this.nacionalidad = new DevComponents.DotNetBar.Controls.ComboBoxEx();
            this.comboItem23 = new DevComponents.Editors.ComboItem();
            this.comboItem24 = new DevComponents.Editors.ComboItem();
            this.labelX16 = new DevComponents.DotNetBar.LabelX();
            this.LugarExpRetenc = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.ucRetencionPagoExtranjero1 = new FE_FX.CFDI.ucRetencionPagoExtranjero();
            this.TipoPagoRet = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.montoRet = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.BaseRet = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Impuesto = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.statusStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dtgPolizas)).BeginInit();
            this.tabControl1.SuspendLayout();
            this.tabPage2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dtgTotales)).BeginInit();
            this.tabPage3.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.SuspendLayout();
            // 
            // statusStrip1
            // 
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.informacion});
            this.statusStrip1.Location = new System.Drawing.Point(0, 504);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(725, 22);
            this.statusStrip1.TabIndex = 42;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // informacion
            // 
            this.informacion.Name = "informacion";
            this.informacion.Size = new System.Drawing.Size(42, 17);
            this.informacion.Text = "Estado";
            // 
            // comboItem4
            // 
            this.comboItem4.Text = "FECHA";
            // 
            // comboItem5
            // 
            this.comboItem5.Text = "FACTURA";
            // 
            // comboItem6
            // 
            this.comboItem6.Text = "CLIENTE";
            // 
            // buttonX17
            // 
            this.buttonX17.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.buttonX17.BackColor = System.Drawing.Color.CadetBlue;
            this.buttonX17.ColorTable = DevComponents.DotNetBar.eButtonColor.Flat;
            this.buttonX17.Font = new System.Drawing.Font("Segoe UI", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonX17.ImagePosition = DevComponents.DotNetBar.eImagePosition.Right;
            this.buttonX17.Location = new System.Drawing.Point(673, 35);
            this.buttonX17.Name = "buttonX17";
            this.buttonX17.Size = new System.Drawing.Size(47, 20);
            this.buttonX17.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.buttonX17.TabIndex = 10;
            this.buttonX17.Text = "Ver XML";
            this.buttonX17.TextColor = System.Drawing.Color.White;
            this.buttonX17.Click += new System.EventHandler(this.buttonX17_Click);
            // 
            // labelX13
            // 
            this.labelX13.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX13.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX13.ForeColor = System.Drawing.Color.Black;
            this.labelX13.Location = new System.Drawing.Point(226, 11);
            this.labelX13.Name = "labelX13";
            this.labelX13.Size = new System.Drawing.Size(65, 20);
            this.labelX13.TabIndex = 4;
            this.labelX13.Text = "Folio Fiscal";
            this.labelX13.TextAlignment = System.Drawing.StringAlignment.Far;
            this.labelX13.Click += new System.EventHandler(this.labelX13_Click);
            // 
            // UUID
            // 
            this.UUID.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            // 
            // 
            // 
            this.UUID.Border.Class = "TextBoxBorder";
            this.UUID.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.UUID.ForeColor = System.Drawing.Color.Black;
            this.UUID.Location = new System.Drawing.Point(297, 12);
            this.UUID.Name = "UUID";
            this.UUID.ReadOnly = true;
            this.UUID.Size = new System.Drawing.Size(249, 20);
            this.UUID.TabIndex = 5;
            // 
            // ESTADO
            // 
            this.ESTADO.DisplayMember = "Text";
            this.ESTADO.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.ESTADO.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ESTADO.ForeColor = System.Drawing.Color.Black;
            this.ESTADO.FormattingEnabled = true;
            this.ESTADO.ItemHeight = 16;
            this.ESTADO.Items.AddRange(new object[] {
            this.comboItem3,
            this.comboItem1,
            this.comboItem2});
            this.ESTADO.Location = new System.Drawing.Point(447, 62);
            this.ESTADO.Name = "ESTADO";
            this.ESTADO.Size = new System.Drawing.Size(85, 22);
            this.ESTADO.TabIndex = 21;
            this.ESTADO.Text = "BORRADOR";
            // 
            // comboItem3
            // 
            this.comboItem3.Text = "BORRADOR";
            // 
            // comboItem1
            // 
            this.comboItem1.FontStyle = System.Drawing.FontStyle.Bold;
            this.comboItem1.Text = "TIMBRADO";
            // 
            // comboItem2
            // 
            this.comboItem2.FontStyle = System.Drawing.FontStyle.Bold;
            this.comboItem2.ForeColor = System.Drawing.Color.Red;
            this.comboItem2.Text = "CANCELADO";
            // 
            // SERIE
            // 
            this.SERIE.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            // 
            // 
            // 
            this.SERIE.Border.Class = "TextBoxBorder";
            this.SERIE.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.SERIE.Enabled = false;
            this.SERIE.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.SERIE.ForeColor = System.Drawing.Color.Black;
            this.SERIE.Location = new System.Drawing.Point(592, 62);
            this.SERIE.Margin = new System.Windows.Forms.Padding(0);
            this.SERIE.Name = "SERIE";
            this.SERIE.Size = new System.Drawing.Size(48, 22);
            this.SERIE.TabIndex = 15;
            this.SERIE.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // ID
            // 
            this.ID.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            // 
            // 
            // 
            this.ID.Border.Class = "TextBoxBorder";
            this.ID.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.ID.Enabled = false;
            this.ID.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ID.ForeColor = System.Drawing.Color.Black;
            this.ID.Location = new System.Drawing.Point(640, 62);
            this.ID.Margin = new System.Windows.Forms.Padding(0);
            this.ID.Name = "ID";
            this.ID.Size = new System.Drawing.Size(76, 22);
            this.ID.TabIndex = 16;
            this.ID.Text = "000000";
            this.ID.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // comboItem14
            // 
            this.comboItem14.Text = "FACTURA";
            // 
            // comboItem15
            // 
            this.comboItem15.Text = "NOTA DE CREDITO";
            // 
            // comboItem16
            // 
            this.comboItem16.Text = "RECIBO HONORARIOS";
            // 
            // montoTotOperacion
            // 
            this.montoTotOperacion.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            // 
            // 
            // 
            this.montoTotOperacion.Border.Class = "TextBoxBorder";
            this.montoTotOperacion.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.montoTotOperacion.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.montoTotOperacion.ForeColor = System.Drawing.Color.Black;
            this.montoTotOperacion.Location = new System.Drawing.Point(183, 438);
            this.montoTotOperacion.Name = "montoTotOperacion";
            this.montoTotOperacion.Size = new System.Drawing.Size(98, 25);
            this.montoTotOperacion.TabIndex = 31;
            this.montoTotOperacion.Text = "0";
            this.montoTotOperacion.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // labelX3
            // 
            this.labelX3.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX3.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX3.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelX3.ForeColor = System.Drawing.Color.Black;
            this.labelX3.Location = new System.Drawing.Point(70, 438);
            this.labelX3.Name = "labelX3";
            this.labelX3.Size = new System.Drawing.Size(107, 27);
            this.labelX3.TabIndex = 30;
            this.labelX3.Text = "Total Operación";
            this.labelX3.TextAlignment = System.Drawing.StringAlignment.Far;
            // 
            // ENTITY_ID
            // 
            this.ENTITY_ID.DisplayMember = "Text";
            this.ENTITY_ID.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.ENTITY_ID.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ENTITY_ID.ForeColor = System.Drawing.Color.Black;
            this.ENTITY_ID.FormattingEnabled = true;
            this.ENTITY_ID.ItemHeight = 16;
            this.ENTITY_ID.Items.AddRange(new object[] {
            this.comboItem17,
            this.comboItem18,
            this.comboItem19});
            this.ENTITY_ID.Location = new System.Drawing.Point(56, 62);
            this.ENTITY_ID.Name = "ENTITY_ID";
            this.ENTITY_ID.Size = new System.Drawing.Size(143, 22);
            this.ENTITY_ID.TabIndex = 12;
            this.ENTITY_ID.SelectedIndexChanged += new System.EventHandler(this.ENTITY_ID_SelectedIndexChanged);
            // 
            // comboItem17
            // 
            this.comboItem17.Text = "BORRADOR";
            // 
            // comboItem18
            // 
            this.comboItem18.FontStyle = System.Drawing.FontStyle.Bold;
            this.comboItem18.Text = "TIMBRADO";
            // 
            // comboItem19
            // 
            this.comboItem19.FontStyle = System.Drawing.FontStyle.Bold;
            this.comboItem19.ForeColor = System.Drawing.Color.Red;
            this.comboItem19.Text = "CANCELADO";
            // 
            // labelX11
            // 
            this.labelX11.AutoSize = true;
            this.labelX11.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX11.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX11.ForeColor = System.Drawing.Color.Black;
            this.labelX11.Location = new System.Drawing.Point(13, 66);
            this.labelX11.Name = "labelX11";
            this.labelX11.Size = new System.Drawing.Size(37, 15);
            this.labelX11.TabIndex = 11;
            this.labelX11.Text = "Emisor";
            // 
            // EMPRESA_NOMBRE
            // 
            this.EMPRESA_NOMBRE.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.EMPRESA_NOMBRE.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.EMPRESA_NOMBRE.ForeColor = System.Drawing.Color.Black;
            this.EMPRESA_NOMBRE.Location = new System.Drawing.Point(205, 66);
            this.EMPRESA_NOMBRE.Name = "EMPRESA_NOMBRE";
            this.EMPRESA_NOMBRE.Size = new System.Drawing.Size(195, 15);
            this.EMPRESA_NOMBRE.TabIndex = 13;
            // 
            // labelX14
            // 
            this.labelX14.AutoSize = true;
            this.labelX14.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX14.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX14.ForeColor = System.Drawing.Color.Black;
            this.labelX14.Location = new System.Drawing.Point(552, 14);
            this.labelX14.Name = "labelX14";
            this.labelX14.Size = new System.Drawing.Size(43, 15);
            this.labelX14.TabIndex = 6;
            this.labelX14.Text = "Formato";
            this.labelX14.Visible = false;
            this.labelX14.Click += new System.EventHandler(this.labelX12_Click);
            // 
            // rfc
            // 
            this.rfc.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            // 
            // 
            // 
            this.rfc.Border.Class = "TextBoxBorder";
            this.rfc.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.rfc.ForeColor = System.Drawing.Color.Black;
            this.rfc.Location = new System.Drawing.Point(404, 90);
            this.rfc.Name = "rfc";
            this.rfc.Size = new System.Drawing.Size(107, 20);
            this.rfc.TabIndex = 20;
            // 
            // labelX15
            // 
            this.labelX15.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX15.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX15.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelX15.ForeColor = System.Drawing.Color.Black;
            this.labelX15.Location = new System.Drawing.Point(374, 92);
            this.labelX15.Name = "labelX15";
            this.labelX15.Size = new System.Drawing.Size(24, 17);
            this.labelX15.TabIndex = 19;
            this.labelX15.Text = "RFC";
            // 
            // PATH
            // 
            this.PATH.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            // 
            // 
            // 
            this.PATH.Border.Class = "TextBoxBorder";
            this.PATH.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.PATH.ForeColor = System.Drawing.Color.Black;
            this.PATH.Location = new System.Drawing.Point(297, 35);
            this.PATH.Name = "PATH";
            this.PATH.ReadOnly = true;
            this.PATH.Size = new System.Drawing.Size(370, 20);
            this.PATH.TabIndex = 9;
            // 
            // labelX19
            // 
            this.labelX19.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX19.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX19.ForeColor = System.Drawing.Color.Black;
            this.labelX19.Location = new System.Drawing.Point(244, 37);
            this.labelX19.Name = "labelX19";
            this.labelX19.Size = new System.Drawing.Size(47, 16);
            this.labelX19.TabIndex = 8;
            this.labelX19.Text = "Archivo";
            this.labelX19.TextAlignment = System.Drawing.StringAlignment.Far;
            this.labelX19.Click += new System.EventHandler(this.labelX13_Click);
            // 
            // dtgPolizas
            // 
            this.dtgPolizas.AllowUserToAddRows = false;
            this.dtgPolizas.AllowUserToDeleteRows = false;
            this.dtgPolizas.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.dtgPolizas.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dtgPolizas.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.dtgPolizas.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dtgPolizas.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.ACCOUNT_ID,
            this.REFERENCE,
            this.CARGO,
            this.ABONO});
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dtgPolizas.DefaultCellStyle = dataGridViewCellStyle2;
            this.dtgPolizas.EnableHeadersVisualStyles = false;
            this.dtgPolizas.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(208)))), ((int)(((byte)(215)))), ((int)(((byte)(229)))));
            this.dtgPolizas.Location = new System.Drawing.Point(6, 50);
            this.dtgPolizas.Name = "dtgPolizas";
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle3.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dtgPolizas.RowHeadersDefaultCellStyle = dataGridViewCellStyle3;
            this.dtgPolizas.RowHeadersVisible = false;
            this.dtgPolizas.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dtgPolizas.Size = new System.Drawing.Size(688, 158);
            this.dtgPolizas.TabIndex = 58;
            // 
            // ACCOUNT_ID
            // 
            this.ACCOUNT_ID.HeaderText = "Cuenta Contable";
            this.ACCOUNT_ID.Name = "ACCOUNT_ID";
            this.ACCOUNT_ID.Width = 150;
            // 
            // REFERENCE
            // 
            this.REFERENCE.HeaderText = "Referencia";
            this.REFERENCE.Name = "REFERENCE";
            this.REFERENCE.Width = 250;
            // 
            // CARGO
            // 
            this.CARGO.HeaderText = "Cargo";
            this.CARGO.Name = "CARGO";
            this.CARGO.Width = 75;
            // 
            // ABONO
            // 
            this.ABONO.HeaderText = "Abono";
            this.ABONO.Name = "ABONO";
            this.ABONO.Width = 75;
            // 
            // FORMATO_IMPRESION
            // 
            this.FORMATO_IMPRESION.DisplayMember = "Text";
            this.FORMATO_IMPRESION.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.FORMATO_IMPRESION.ForeColor = System.Drawing.Color.Black;
            this.FORMATO_IMPRESION.FormattingEnabled = true;
            this.FORMATO_IMPRESION.ItemHeight = 16;
            this.FORMATO_IMPRESION.Location = new System.Drawing.Point(601, 10);
            this.FORMATO_IMPRESION.Name = "FORMATO_IMPRESION";
            this.FORMATO_IMPRESION.Size = new System.Drawing.Size(119, 22);
            this.FORMATO_IMPRESION.TabIndex = 7;
            this.FORMATO_IMPRESION.Visible = false;
            this.FORMATO_IMPRESION.WatermarkText = "Formato de impresión";
            // 
            // buttonX6
            // 
            this.buttonX6.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.buttonX6.BackColor = System.Drawing.Color.Moccasin;
            this.buttonX6.ColorTable = DevComponents.DotNetBar.eButtonColor.Flat;
            this.buttonX6.Font = new System.Drawing.Font("Segoe UI", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonX6.Image = global::FE_FX.Properties.Resources.ic_donut_large_black_24dp_1x;
            this.buttonX6.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top;
            this.buttonX6.Location = new System.Drawing.Point(102, 3);
            this.buttonX6.Name = "buttonX6";
            this.buttonX6.Size = new System.Drawing.Size(42, 41);
            this.buttonX6.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.buttonX6.TabIndex = 60;
            this.buttonX6.Text = "- Línea";
            this.buttonX6.TextColor = System.Drawing.Color.Black;
            this.buttonX6.Click += new System.EventHandler(this.buttonX6_Click);
            // 
            // buttonX4
            // 
            this.buttonX4.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.buttonX4.BackColor = System.Drawing.Color.Moccasin;
            this.buttonX4.ColorTable = DevComponents.DotNetBar.eButtonColor.Flat;
            this.buttonX4.Font = new System.Drawing.Font("Segoe UI", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonX4.Image = global::FE_FX.Properties.Resources.ic_donut_large_black_24dp_1x;
            this.buttonX4.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top;
            this.buttonX4.Location = new System.Drawing.Point(54, 3);
            this.buttonX4.Name = "buttonX4";
            this.buttonX4.Size = new System.Drawing.Size(42, 41);
            this.buttonX4.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.buttonX4.TabIndex = 59;
            this.buttonX4.Text = "+ Línea";
            this.buttonX4.TextColor = System.Drawing.Color.Black;
            this.buttonX4.Click += new System.EventHandler(this.buttonX4_Click_1);
            // 
            // buttonX3
            // 
            this.buttonX3.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.buttonX3.BackColor = System.Drawing.Color.Moccasin;
            this.buttonX3.ColorTable = DevComponents.DotNetBar.eButtonColor.Flat;
            this.buttonX3.Font = new System.Drawing.Font("Segoe UI", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonX3.Image = global::FE_FX.Properties.Resources.ic_donut_large_black_24dp_1x;
            this.buttonX3.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top;
            this.buttonX3.Location = new System.Drawing.Point(6, 3);
            this.buttonX3.Name = "buttonX3";
            this.buttonX3.Size = new System.Drawing.Size(42, 41);
            this.buttonX3.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.buttonX3.TabIndex = 57;
            this.buttonX3.Text = "Póliza";
            this.buttonX3.TextColor = System.Drawing.Color.Black;
            this.buttonX3.Click += new System.EventHandler(this.buttonX3_Click);
            // 
            // btnEmisor
            // 
            this.btnEmisor.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btnEmisor.BackColor = System.Drawing.Color.Moccasin;
            this.btnEmisor.ColorTable = DevComponents.DotNetBar.eButtonColor.Office2007WithBackground;
            this.btnEmisor.Image = global::FE_FX.Properties.Resources.ic_search_black_18dp_1x;
            this.btnEmisor.Location = new System.Drawing.Point(13, 91);
            this.btnEmisor.Name = "btnEmisor";
            this.btnEmisor.Size = new System.Drawing.Size(85, 20);
            this.btnEmisor.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.btnEmisor.TabIndex = 17;
            this.btnEmisor.Text = "Receptor";
            this.btnEmisor.TextAlignment = DevComponents.DotNetBar.eButtonTextAlignment.Left;
            this.btnEmisor.TextColor = System.Drawing.Color.Black;
            this.btnEmisor.Click += new System.EventHandler(this.buttonX4_Click);
            // 
            // buttonX9
            // 
            this.buttonX9.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.buttonX9.BackColor = System.Drawing.Color.Brown;
            this.buttonX9.ColorTable = DevComponents.DotNetBar.eButtonColor.Flat;
            this.buttonX9.Font = new System.Drawing.Font("Segoe UI", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonX9.Image = global::FE_FX.Properties.Resources.ic_cancel_black_24dp_1x;
            this.buttonX9.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top;
            this.buttonX9.Location = new System.Drawing.Point(155, 12);
            this.buttonX9.Name = "buttonX9";
            this.buttonX9.Size = new System.Drawing.Size(42, 41);
            this.buttonX9.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.buttonX9.TabIndex = 3;
            this.buttonX9.Text = "Cancelar";
            this.buttonX9.TextColor = System.Drawing.Color.White;
            this.buttonX9.Visible = false;
            this.buttonX9.Click += new System.EventHandler(this.buttonX9_Click);
            // 
            // buttonX7
            // 
            this.buttonX7.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.buttonX7.BackColor = System.Drawing.Color.Moccasin;
            this.buttonX7.ColorTable = DevComponents.DotNetBar.eButtonColor.Flat;
            this.buttonX7.Font = new System.Drawing.Font("Segoe UI", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonX7.Image = global::FE_FX.Properties.Resources.ic_local_printshop_black_24dp_1x;
            this.buttonX7.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top;
            this.buttonX7.Location = new System.Drawing.Point(108, 12);
            this.buttonX7.Name = "buttonX7";
            this.buttonX7.Size = new System.Drawing.Size(41, 41);
            this.buttonX7.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.buttonX7.TabIndex = 2;
            this.buttonX7.Text = "Imprimir";
            this.buttonX7.TextColor = System.Drawing.Color.Black;
            this.buttonX7.Click += new System.EventHandler(this.buttonX7_Click);
            // 
            // buttonX5
            // 
            this.buttonX5.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.buttonX5.BackColor = System.Drawing.Color.SkyBlue;
            this.buttonX5.ColorTable = DevComponents.DotNetBar.eButtonColor.Flat;
            this.buttonX5.Font = new System.Drawing.Font("Segoe UI", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonX5.Image = global::FE_FX.Properties.Resources.ic_save_black_24dp_1x;
            this.buttonX5.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top;
            this.buttonX5.Location = new System.Drawing.Point(60, 12);
            this.buttonX5.Name = "buttonX5";
            this.buttonX5.Size = new System.Drawing.Size(42, 41);
            this.buttonX5.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.buttonX5.TabIndex = 1;
            this.buttonX5.Text = "Guardar";
            this.buttonX5.TextColor = System.Drawing.Color.Black;
            this.buttonX5.Click += new System.EventHandler(this.buttonX5_Click);
            // 
            // buttonX2
            // 
            this.buttonX2.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.buttonX2.BackColor = System.Drawing.Color.YellowGreen;
            this.buttonX2.ColorTable = DevComponents.DotNetBar.eButtonColor.Flat;
            this.buttonX2.Font = new System.Drawing.Font("Segoe UI", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonX2.Image = global::FE_FX.Properties.Resources.ic_create_new_folder_black_24dp_1x;
            this.buttonX2.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top;
            this.buttonX2.Location = new System.Drawing.Point(12, 12);
            this.buttonX2.Name = "buttonX2";
            this.buttonX2.Size = new System.Drawing.Size(42, 41);
            this.buttonX2.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.buttonX2.TabIndex = 0;
            this.buttonX2.Text = "Nuevo";
            this.buttonX2.TextColor = System.Drawing.Color.White;
            this.buttonX2.Click += new System.EventHandler(this.buttonX2_Click_1);
            // 
            // buttonX1
            // 
            this.buttonX1.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.buttonX1.BackColor = System.Drawing.Color.DarkGoldenrod;
            this.buttonX1.ColorTable = DevComponents.DotNetBar.eButtonColor.Flat;
            this.buttonX1.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonX1.Image = global::FE_FX.Properties.Resources.ic_search_black_18dp_1x;
            this.buttonX1.Location = new System.Drawing.Point(535, 62);
            this.buttonX1.Name = "buttonX1";
            this.buttonX1.Size = new System.Drawing.Size(54, 22);
            this.buttonX1.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.buttonX1.TabIndex = 14;
            this.buttonX1.Text = "Serie";
            this.buttonX1.TextAlignment = DevComponents.DotNetBar.eButtonTextAlignment.Right;
            this.buttonX1.TextColor = System.Drawing.Color.White;
            this.buttonX1.Click += new System.EventHandler(this.buttonX1_Click);
            // 
            // receptortxt
            // 
            this.receptortxt.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            // 
            // 
            // 
            this.receptortxt.Border.Class = "TextBoxBorder";
            this.receptortxt.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.receptortxt.ForeColor = System.Drawing.Color.Black;
            this.receptortxt.Location = new System.Drawing.Point(104, 91);
            this.receptortxt.Name = "receptortxt";
            this.receptortxt.Size = new System.Drawing.Size(260, 20);
            this.receptortxt.TabIndex = 18;
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Controls.Add(this.tabPage3);
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Location = new System.Drawing.Point(12, 199);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(708, 233);
            this.tabControl1.TabIndex = 29;
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.dtgTotales);
            this.tabPage2.Controls.Add(this.buttonX10);
            this.tabPage2.Controls.Add(this.buttonX11);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(700, 207);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Impuestos retenidos";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // dtgTotales
            // 
            this.dtgTotales.AllowUserToAddRows = false;
            this.dtgTotales.AllowUserToDeleteRows = false;
            this.dtgTotales.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.dtgTotales.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle4.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle4.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle4.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle4.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle4.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dtgTotales.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle4;
            this.dtgTotales.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dtgTotales.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.TipoPagoRet,
            this.montoRet,
            this.BaseRet,
            this.Impuesto});
            dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle5.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            dataGridViewCellStyle5.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle5.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle5.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle5.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle5.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dtgTotales.DefaultCellStyle = dataGridViewCellStyle5;
            this.dtgTotales.EnableHeadersVisualStyles = false;
            this.dtgTotales.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(208)))), ((int)(((byte)(215)))), ((int)(((byte)(229)))));
            this.dtgTotales.Location = new System.Drawing.Point(6, 52);
            this.dtgTotales.Name = "dtgTotales";
            dataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle6.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle6.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle6.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle6.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle6.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle6.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dtgTotales.RowHeadersDefaultCellStyle = dataGridViewCellStyle6;
            this.dtgTotales.RowHeadersVisible = false;
            this.dtgTotales.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dtgTotales.Size = new System.Drawing.Size(688, 149);
            this.dtgTotales.TabIndex = 2;
            this.dtgTotales.Leave += new System.EventHandler(this.dtgTotales_Leave);
            // 
            // buttonX10
            // 
            this.buttonX10.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.buttonX10.BackColor = System.Drawing.Color.Moccasin;
            this.buttonX10.ColorTable = DevComponents.DotNetBar.eButtonColor.Flat;
            this.buttonX10.Font = new System.Drawing.Font("Segoe UI", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonX10.Image = global::FE_FX.Properties.Resources.ic_donut_large_black_24dp_1x;
            this.buttonX10.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top;
            this.buttonX10.Location = new System.Drawing.Point(6, 6);
            this.buttonX10.Name = "buttonX10";
            this.buttonX10.Size = new System.Drawing.Size(42, 41);
            this.buttonX10.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.buttonX10.TabIndex = 0;
            this.buttonX10.Text = "+ Línea";
            this.buttonX10.TextColor = System.Drawing.Color.Black;
            this.buttonX10.Click += new System.EventHandler(this.buttonX10_Click);
            // 
            // buttonX11
            // 
            this.buttonX11.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.buttonX11.BackColor = System.Drawing.Color.Moccasin;
            this.buttonX11.ColorTable = DevComponents.DotNetBar.eButtonColor.Flat;
            this.buttonX11.Font = new System.Drawing.Font("Segoe UI", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonX11.Image = global::FE_FX.Properties.Resources.ic_donut_large_black_24dp_1x;
            this.buttonX11.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top;
            this.buttonX11.Location = new System.Drawing.Point(54, 6);
            this.buttonX11.Name = "buttonX11";
            this.buttonX11.Size = new System.Drawing.Size(42, 41);
            this.buttonX11.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.buttonX11.TabIndex = 1;
            this.buttonX11.Text = "- Línea";
            this.buttonX11.TextColor = System.Drawing.Color.Black;
            this.buttonX11.Click += new System.EventHandler(this.buttonX11_Click);
            // 
            // tabPage3
            // 
            this.tabPage3.Controls.Add(this.ucRetencionPagoExtranjero1);
            this.tabPage3.Location = new System.Drawing.Point(4, 22);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage3.Size = new System.Drawing.Size(700, 207);
            this.tabPage3.TabIndex = 2;
            this.tabPage3.Text = "Pagos al extranjero";
            this.tabPage3.UseVisualStyleBackColor = true;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.dtgPolizas);
            this.tabPage1.Controls.Add(this.buttonX3);
            this.tabPage1.Controls.Add(this.buttonX4);
            this.tabPage1.Controls.Add(this.buttonX6);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(700, 207);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Generación de Pólizas";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // montoTotGrav
            // 
            this.montoTotGrav.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            // 
            // 
            // 
            this.montoTotGrav.Border.Class = "TextBoxBorder";
            this.montoTotGrav.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.montoTotGrav.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.montoTotGrav.ForeColor = System.Drawing.Color.Black;
            this.montoTotGrav.Location = new System.Drawing.Point(183, 469);
            this.montoTotGrav.Name = "montoTotGrav";
            this.montoTotGrav.Size = new System.Drawing.Size(98, 25);
            this.montoTotGrav.TabIndex = 35;
            this.montoTotGrav.Text = "0";
            this.montoTotGrav.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // labelX5
            // 
            this.labelX5.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX5.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX5.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelX5.ForeColor = System.Drawing.Color.Black;
            this.labelX5.Location = new System.Drawing.Point(67, 465);
            this.labelX5.Name = "labelX5";
            this.labelX5.Size = new System.Drawing.Size(95, 27);
            this.labelX5.TabIndex = 34;
            this.labelX5.Text = "Total Gravado";
            this.labelX5.TextAlignment = System.Drawing.StringAlignment.Far;
            this.labelX5.Click += new System.EventHandler(this.labelX5_Click);
            // 
            // montoTotExent
            // 
            this.montoTotExent.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            // 
            // 
            // 
            this.montoTotExent.Border.Class = "TextBoxBorder";
            this.montoTotExent.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.montoTotExent.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.montoTotExent.ForeColor = System.Drawing.Color.Black;
            this.montoTotExent.Location = new System.Drawing.Point(401, 468);
            this.montoTotExent.Name = "montoTotExent";
            this.montoTotExent.Size = new System.Drawing.Size(88, 25);
            this.montoTotExent.TabIndex = 37;
            this.montoTotExent.Text = "0";
            this.montoTotExent.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // labelX6
            // 
            this.labelX6.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX6.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX6.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelX6.ForeColor = System.Drawing.Color.Black;
            this.labelX6.Location = new System.Drawing.Point(287, 468);
            this.labelX6.Name = "labelX6";
            this.labelX6.Size = new System.Drawing.Size(108, 27);
            this.labelX6.TabIndex = 36;
            this.labelX6.Text = "Total Exento";
            this.labelX6.TextAlignment = System.Drawing.StringAlignment.Far;
            // 
            // montoTotRet
            // 
            this.montoTotRet.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            // 
            // 
            // 
            this.montoTotRet.Border.Class = "TextBoxBorder";
            this.montoTotRet.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.montoTotRet.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.montoTotRet.ForeColor = System.Drawing.Color.Black;
            this.montoTotRet.Location = new System.Drawing.Point(401, 438);
            this.montoTotRet.Name = "montoTotRet";
            this.montoTotRet.Size = new System.Drawing.Size(88, 25);
            this.montoTotRet.TabIndex = 33;
            this.montoTotRet.Text = "0";
            this.montoTotRet.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // labelX2
            // 
            this.labelX2.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX2.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX2.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelX2.ForeColor = System.Drawing.Color.Black;
            this.labelX2.Location = new System.Drawing.Point(285, 438);
            this.labelX2.Name = "labelX2";
            this.labelX2.Size = new System.Drawing.Size(110, 27);
            this.labelX2.TabIndex = 32;
            this.labelX2.Text = "Total Retenido";
            this.labelX2.TextAlignment = System.Drawing.StringAlignment.Far;
            // 
            // DescRetenc
            // 
            this.DescRetenc.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            // 
            // 
            // 
            this.DescRetenc.Border.Class = "TextBoxBorder";
            this.DescRetenc.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.DescRetenc.ForeColor = System.Drawing.Color.Black;
            this.DescRetenc.Location = new System.Drawing.Point(157, 173);
            this.DescRetenc.Name = "DescRetenc";
            this.DescRetenc.Size = new System.Drawing.Size(559, 20);
            this.DescRetenc.TabIndex = 28;
            this.DescRetenc.Text = "Pagos realizados a favor de residentes en el extranjero";
            this.DescRetenc.WatermarkText = "Descripción de la retención";
            // 
            // CURPE
            // 
            this.CURPE.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            // 
            // 
            // 
            this.CURPE.Border.Class = "TextBoxBorder";
            this.CURPE.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.CURPE.ForeColor = System.Drawing.Color.Black;
            this.CURPE.Location = new System.Drawing.Point(565, 117);
            this.CURPE.Name = "CURPE";
            this.CURPE.Size = new System.Drawing.Size(151, 20);
            this.CURPE.TabIndex = 27;
            // 
            // labelX4
            // 
            this.labelX4.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX4.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX4.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelX4.ForeColor = System.Drawing.Color.Black;
            this.labelX4.Location = new System.Drawing.Point(526, 117);
            this.labelX4.Name = "labelX4";
            this.labelX4.Size = new System.Drawing.Size(33, 17);
            this.labelX4.TabIndex = 26;
            this.labelX4.Text = "CURP";
            // 
            // ejercicio
            // 
            this.ejercicio.DisplayMember = "Text";
            this.ejercicio.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.ejercicio.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ejercicio.ForeColor = System.Drawing.Color.Black;
            this.ejercicio.FormattingEnabled = true;
            this.ejercicio.ItemHeight = 16;
            this.ejercicio.Items.AddRange(new object[] {
            this.comboItem7,
            this.comboItem8,
            this.comboItem9,
            this.comboItem10,
            this.comboItem11});
            this.ejercicio.Location = new System.Drawing.Point(447, 115);
            this.ejercicio.Name = "ejercicio";
            this.ejercicio.Size = new System.Drawing.Size(64, 22);
            this.ejercicio.TabIndex = 50;
            this.ejercicio.Text = "2016";
            // 
            // comboItem7
            // 
            this.comboItem7.Text = "2014";
            // 
            // comboItem8
            // 
            this.comboItem8.Text = "2015";
            // 
            // comboItem9
            // 
            this.comboItem9.Text = "2016";
            // 
            // comboItem10
            // 
            this.comboItem10.Text = "2017";
            // 
            // comboItem11
            // 
            this.comboItem11.Text = "2018";
            // 
            // labelX1
            // 
            this.labelX1.AutoSize = true;
            this.labelX1.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX1.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX1.ForeColor = System.Drawing.Color.Black;
            this.labelX1.Location = new System.Drawing.Point(397, 118);
            this.labelX1.Name = "labelX1";
            this.labelX1.Size = new System.Drawing.Size(44, 15);
            this.labelX1.TabIndex = 49;
            this.labelX1.Text = "Ejercicio";
            // 
            // fin
            // 
            this.fin.DisplayMember = "Text";
            this.fin.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.fin.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.fin.ForeColor = System.Drawing.Color.Black;
            this.fin.FormattingEnabled = true;
            this.fin.ItemHeight = 16;
            this.fin.Location = new System.Drawing.Point(336, 115);
            this.fin.Name = "fin";
            this.fin.Size = new System.Drawing.Size(55, 22);
            this.fin.TabIndex = 48;
            this.fin.Text = "12";
            // 
            // labelX7
            // 
            this.labelX7.AutoSize = true;
            this.labelX7.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX7.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX7.ForeColor = System.Drawing.Color.Black;
            this.labelX7.Location = new System.Drawing.Point(312, 118);
            this.labelX7.Name = "labelX7";
            this.labelX7.Size = new System.Drawing.Size(18, 15);
            this.labelX7.TabIndex = 47;
            this.labelX7.Text = "Fin";
            // 
            // inicio
            // 
            this.inicio.DisplayMember = "Text";
            this.inicio.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.inicio.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.inicio.ForeColor = System.Drawing.Color.Black;
            this.inicio.FormattingEnabled = true;
            this.inicio.ItemHeight = 16;
            this.inicio.Location = new System.Drawing.Point(244, 115);
            this.inicio.Name = "inicio";
            this.inicio.Size = new System.Drawing.Size(55, 22);
            this.inicio.TabIndex = 52;
            this.inicio.Text = "01";
            // 
            // labelX8
            // 
            this.labelX8.AutoSize = true;
            this.labelX8.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX8.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX8.ForeColor = System.Drawing.Color.Black;
            this.labelX8.Location = new System.Drawing.Point(210, 118);
            this.labelX8.Name = "labelX8";
            this.labelX8.Size = new System.Drawing.Size(28, 15);
            this.labelX8.TabIndex = 51;
            this.labelX8.Text = "Inicio";
            // 
            // buttonX8
            // 
            this.buttonX8.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.buttonX8.BackColor = System.Drawing.Color.Moccasin;
            this.buttonX8.ColorTable = DevComponents.DotNetBar.eButtonColor.Flat;
            this.buttonX8.Font = new System.Drawing.Font("Segoe UI", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonX8.Image = global::FE_FX.Properties.Resources.ic_donut_large_black_24dp_1x;
            this.buttonX8.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top;
            this.buttonX8.Location = new System.Drawing.Point(12, 443);
            this.buttonX8.Name = "buttonX8";
            this.buttonX8.Size = new System.Drawing.Size(42, 41);
            this.buttonX8.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.buttonX8.TabIndex = 53;
            this.buttonX8.Text = "Calcular";
            this.buttonX8.TextColor = System.Drawing.Color.Black;
            this.buttonX8.Click += new System.EventHandler(this.buttonX8_Click);
            // 
            // labelX9
            // 
            this.labelX9.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX9.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX9.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelX9.ForeColor = System.Drawing.Color.Black;
            this.labelX9.Location = new System.Drawing.Point(16, 148);
            this.labelX9.Name = "labelX9";
            this.labelX9.Size = new System.Drawing.Size(84, 17);
            this.labelX9.TabIndex = 54;
            this.labelX9.Text = "Clave Retención";
            // 
            // CveRetencCmb
            // 
            this.CveRetencCmb.DisplayMember = "Text";
            this.CveRetencCmb.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.CveRetencCmb.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CveRetencCmb.ForeColor = System.Drawing.Color.Black;
            this.CveRetencCmb.FormattingEnabled = true;
            this.CveRetencCmb.ItemHeight = 16;
            this.CveRetencCmb.Location = new System.Drawing.Point(104, 145);
            this.CveRetencCmb.Name = "CveRetencCmb";
            this.CveRetencCmb.Size = new System.Drawing.Size(612, 22);
            this.CveRetencCmb.TabIndex = 55;
            this.CveRetencCmb.SelectedValueChanged += new System.EventHandler(this.CveRetencCmb_SelectedValueChanged);
            // 
            // labelX10
            // 
            this.labelX10.AutoSize = true;
            this.labelX10.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX10.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX10.ForeColor = System.Drawing.Color.Black;
            this.labelX10.Location = new System.Drawing.Point(16, 174);
            this.labelX10.Name = "labelX10";
            this.labelX10.Size = new System.Drawing.Size(135, 15);
            this.labelX10.TabIndex = 56;
            this.labelX10.Text = "Descripción de la retención";
            // 
            // labelX12
            // 
            this.labelX12.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX12.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX12.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelX12.ForeColor = System.Drawing.Color.Black;
            this.labelX12.Location = new System.Drawing.Point(518, 92);
            this.labelX12.Name = "labelX12";
            this.labelX12.Size = new System.Drawing.Size(69, 17);
            this.labelX12.TabIndex = 19;
            this.labelX12.Text = "Nacionalidad";
            // 
            // nacionalidad
            // 
            this.nacionalidad.DisplayMember = "Text";
            this.nacionalidad.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.nacionalidad.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.nacionalidad.ForeColor = System.Drawing.Color.Black;
            this.nacionalidad.FormattingEnabled = true;
            this.nacionalidad.ItemHeight = 16;
            this.nacionalidad.Items.AddRange(new object[] {
            this.comboItem23,
            this.comboItem24});
            this.nacionalidad.Location = new System.Drawing.Point(592, 90);
            this.nacionalidad.Name = "nacionalidad";
            this.nacionalidad.Size = new System.Drawing.Size(124, 22);
            this.nacionalidad.TabIndex = 57;
            this.nacionalidad.Text = "Nacional";
            this.nacionalidad.SelectedValueChanged += new System.EventHandler(this.nacionalidad_SelectedValueChanged);
            // 
            // comboItem23
            // 
            this.comboItem23.Text = "Nacional";
            // 
            // comboItem24
            // 
            this.comboItem24.Text = "Extranjero";
            // 
            // labelX16
            // 
            this.labelX16.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX16.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX16.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelX16.ForeColor = System.Drawing.Color.Black;
            this.labelX16.Location = new System.Drawing.Point(12, 118);
            this.labelX16.Name = "labelX16";
            this.labelX16.Size = new System.Drawing.Size(137, 17);
            this.labelX16.TabIndex = 19;
            this.labelX16.Text = "Lugar de Expedición";
            // 
            // LugarExpRetenc
            // 
            this.LugarExpRetenc.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            // 
            // 
            // 
            this.LugarExpRetenc.Border.Class = "TextBoxBorder";
            this.LugarExpRetenc.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.LugarExpRetenc.ForeColor = System.Drawing.Color.Black;
            this.LugarExpRetenc.Location = new System.Drawing.Point(118, 117);
            this.LugarExpRetenc.Name = "LugarExpRetenc";
            this.LugarExpRetenc.Size = new System.Drawing.Size(79, 20);
            this.LugarExpRetenc.TabIndex = 20;
            // 
            // ucRetencionPagoExtranjero1
            // 
            this.ucRetencionPagoExtranjero1.Location = new System.Drawing.Point(6, 4);
            this.ucRetencionPagoExtranjero1.Name = "ucRetencionPagoExtranjero1";
            this.ucRetencionPagoExtranjero1.Size = new System.Drawing.Size(688, 197);
            this.ucRetencionPagoExtranjero1.TabIndex = 0;
            // 
            // TipoPagoRet
            // 
            this.TipoPagoRet.HeaderText = "Tipo de Pago Retenido";
            this.TipoPagoRet.Items.AddRange(new object[] {
            "Pago definitivo IVA",
            "Pago definitivo IEPS",
            "Pago definitivo ISR",
            "Pago provisional ISR"});
            this.TipoPagoRet.Name = "TipoPagoRet";
            this.TipoPagoRet.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.TipoPagoRet.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.TipoPagoRet.Width = 200;
            // 
            // montoRet
            // 
            this.montoRet.HeaderText = "Monto Retenido";
            this.montoRet.Name = "montoRet";
            this.montoRet.Width = 150;
            // 
            // BaseRet
            // 
            this.BaseRet.HeaderText = "Base Ret";
            this.BaseRet.Name = "BaseRet";
            // 
            // Impuesto
            // 
            this.Impuesto.HeaderText = "Impuesto";
            this.Impuesto.Items.AddRange(new object[] {
            "01-ISR",
            "02-IVA",
            "03-IEPS"});
            this.Impuesto.Name = "Impuesto";
            this.Impuesto.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.Impuesto.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            // 
            // frmRetencion2
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(725, 526);
            this.Controls.Add(this.nacionalidad);
            this.Controls.Add(this.labelX10);
            this.Controls.Add(this.CveRetencCmb);
            this.Controls.Add(this.labelX9);
            this.Controls.Add(this.buttonX8);
            this.Controls.Add(this.inicio);
            this.Controls.Add(this.labelX8);
            this.Controls.Add(this.ejercicio);
            this.Controls.Add(this.labelX1);
            this.Controls.Add(this.fin);
            this.Controls.Add(this.labelX7);
            this.Controls.Add(this.CURPE);
            this.Controls.Add(this.labelX4);
            this.Controls.Add(this.DescRetenc);
            this.Controls.Add(this.montoTotRet);
            this.Controls.Add(this.labelX2);
            this.Controls.Add(this.montoTotExent);
            this.Controls.Add(this.labelX6);
            this.Controls.Add(this.montoTotGrav);
            this.Controls.Add(this.labelX5);
            this.Controls.Add(this.receptortxt);
            this.Controls.Add(this.FORMATO_IMPRESION);
            this.Controls.Add(this.ENTITY_ID);
            this.Controls.Add(this.montoTotOperacion);
            this.Controls.Add(this.buttonX17);
            this.Controls.Add(this.LugarExpRetenc);
            this.Controls.Add(this.rfc);
            this.Controls.Add(this.labelX19);
            this.Controls.Add(this.labelX13);
            this.Controls.Add(this.PATH);
            this.Controls.Add(this.UUID);
            this.Controls.Add(this.labelX16);
            this.Controls.Add(this.labelX12);
            this.Controls.Add(this.labelX15);
            this.Controls.Add(this.btnEmisor);
            this.Controls.Add(this.buttonX9);
            this.Controls.Add(this.buttonX7);
            this.Controls.Add(this.buttonX5);
            this.Controls.Add(this.buttonX2);
            this.Controls.Add(this.labelX11);
            this.Controls.Add(this.EMPRESA_NOMBRE);
            this.Controls.Add(this.labelX14);
            this.Controls.Add(this.ESTADO);
            this.Controls.Add(this.SERIE);
            this.Controls.Add(this.buttonX1);
            this.Controls.Add(this.ID);
            this.Controls.Add(this.statusStrip1);
            this.Controls.Add(this.labelX3);
            this.Controls.Add(this.tabControl1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.KeyPreview = true;
            this.MaximizeBox = false;
            this.Name = "frmRetencion2";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Retención";
            this.Load += new System.EventHandler(this.frmRetencion2_Load);
            this.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.frmFormatos_KeyPress);
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dtgPolizas)).EndInit();
            this.tabControl1.ResumeLayout(false);
            this.tabPage2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dtgTotales)).EndInit();
            this.tabPage3.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStripStatusLabel informacion;
        private DevComponents.Editors.ComboItem comboItem4;
        private DevComponents.Editors.ComboItem comboItem5;
        private DevComponents.Editors.ComboItem comboItem6;
        private DevComponents.DotNetBar.ButtonX buttonX17;
        private DevComponents.DotNetBar.LabelX labelX13;
        private DevComponents.DotNetBar.Controls.TextBoxX UUID;
        private DevComponents.DotNetBar.ButtonX btnEmisor;
        private DevComponents.DotNetBar.ButtonX buttonX9;
        private DevComponents.DotNetBar.ButtonX buttonX7;
        private DevComponents.DotNetBar.ButtonX buttonX5;
        private DevComponents.DotNetBar.ButtonX buttonX2;
        private DevComponents.DotNetBar.Controls.ComboBoxEx ESTADO;
        private DevComponents.Editors.ComboItem comboItem3;
        private DevComponents.Editors.ComboItem comboItem1;
        private DevComponents.Editors.ComboItem comboItem2;
        private DevComponents.DotNetBar.Controls.TextBoxX SERIE;
        private DevComponents.DotNetBar.ButtonX buttonX1;
        private DevComponents.DotNetBar.Controls.TextBoxX ID;
        private DevComponents.Editors.ComboItem comboItem14;
        private DevComponents.Editors.ComboItem comboItem15;
        private DevComponents.Editors.ComboItem comboItem16;
        private DevComponents.DotNetBar.Controls.TextBoxX montoTotOperacion;
        private DevComponents.DotNetBar.LabelX labelX3;
        private DevComponents.DotNetBar.Controls.ComboBoxEx ENTITY_ID;
        private DevComponents.Editors.ComboItem comboItem17;
        private DevComponents.Editors.ComboItem comboItem18;
        private DevComponents.Editors.ComboItem comboItem19;
        private DevComponents.DotNetBar.LabelX labelX11;
        private DevComponents.DotNetBar.LabelX EMPRESA_NOMBRE;
        private DevComponents.DotNetBar.LabelX labelX14;
        private DevComponents.DotNetBar.Controls.TextBoxX rfc;
        private DevComponents.DotNetBar.LabelX labelX15;
        private DevComponents.DotNetBar.Controls.TextBoxX PATH;
        private DevComponents.DotNetBar.LabelX labelX19;
        private DevComponents.DotNetBar.ButtonX buttonX3;
        private DevComponents.DotNetBar.Controls.DataGridViewX dtgPolizas;
        private DevComponents.DotNetBar.ButtonX buttonX4;
        private DevComponents.DotNetBar.ButtonX buttonX6;
        private System.Windows.Forms.FolderBrowserDialog folderBrowserDialog1;
        private DevComponents.DotNetBar.Controls.ComboBoxEx FORMATO_IMPRESION;
        private DevComponents.DotNetBar.Controls.TextBoxX receptortxt;
        private CFDI.ucRetencionPagoExtranjero ucRetencionPagoExtranjero1;
        private System.Windows.Forms.DataGridViewTextBoxColumn ACCOUNT_ID;
        private System.Windows.Forms.DataGridViewTextBoxColumn REFERENCE;
        private System.Windows.Forms.DataGridViewTextBoxColumn CARGO;
        private System.Windows.Forms.DataGridViewTextBoxColumn ABONO;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.TabPage tabPage3;
        private System.Windows.Forms.TabPage tabPage1;
        private DevComponents.DotNetBar.Controls.TextBoxX montoTotGrav;
        private DevComponents.DotNetBar.LabelX labelX5;
        private DevComponents.DotNetBar.Controls.TextBoxX montoTotExent;
        private DevComponents.DotNetBar.LabelX labelX6;
        private DevComponents.DotNetBar.Controls.DataGridViewX dtgTotales;
        private DevComponents.DotNetBar.ButtonX buttonX10;
        private DevComponents.DotNetBar.ButtonX buttonX11;
        private DevComponents.DotNetBar.Controls.TextBoxX montoTotRet;
        private DevComponents.DotNetBar.LabelX labelX2;
        private DevComponents.DotNetBar.Controls.TextBoxX DescRetenc;
        private DevComponents.DotNetBar.Controls.TextBoxX CURPE;
        private DevComponents.DotNetBar.LabelX labelX4;
        private DevComponents.DotNetBar.Controls.ComboBoxEx ejercicio;
        private DevComponents.DotNetBar.LabelX labelX1;
        private DevComponents.DotNetBar.Controls.ComboBoxEx fin;
        private DevComponents.DotNetBar.LabelX labelX7;
        private DevComponents.DotNetBar.Controls.ComboBoxEx inicio;
        private DevComponents.DotNetBar.LabelX labelX8;
        private DevComponents.Editors.ComboItem comboItem7;
        private DevComponents.Editors.ComboItem comboItem8;
        private DevComponents.Editors.ComboItem comboItem9;
        private DevComponents.Editors.ComboItem comboItem10;
        private DevComponents.Editors.ComboItem comboItem11;
        private DevComponents.DotNetBar.ButtonX buttonX8;
        private DevComponents.DotNetBar.LabelX labelX9;
        private DevComponents.DotNetBar.Controls.ComboBoxEx CveRetencCmb;
        private DevComponents.DotNetBar.LabelX labelX10;
        private DevComponents.DotNetBar.LabelX labelX12;
        private DevComponents.DotNetBar.Controls.ComboBoxEx nacionalidad;
        private DevComponents.Editors.ComboItem comboItem23;
        private DevComponents.Editors.ComboItem comboItem24;
        private DevComponents.DotNetBar.LabelX labelX16;
        private DevComponents.DotNetBar.Controls.TextBoxX LugarExpRetenc;
        private System.Windows.Forms.DataGridViewComboBoxColumn TipoPagoRet;
        private System.Windows.Forms.DataGridViewTextBoxColumn montoRet;
        private System.Windows.Forms.DataGridViewTextBoxColumn BaseRet;
        private System.Windows.Forms.DataGridViewComboBoxColumn Impuesto;
    }
}