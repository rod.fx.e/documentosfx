﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;

using System.Text;
using System.Windows.Forms;
using Generales;

namespace FE_FX
{
    public partial class frmPART : Form
    {
        private cCONEXCION oData = new cCONEXCION("");
        string devolver = "";
        public DataGridViewSelectedRowCollection selecionados;
        string tipo = "";

        public frmPART(string sConn)
        {
            oData.sConn = sConn;
            InitializeComponent();
        }

        private void cargar()
        {
             ////FROM
             ////   (
             ////       SELECT [ID],[NAME],[VAT_REGISTRATION]
             ////       FROM [CUSTOMER]
             ////       UNION ALL
             ////       SELECT [ID],[NAME],[VAT_REGISTRATION]
             ////       FROM [VENDOR]
             ////   ) TABLA

            int n;
            toolStripStatusLabel1.Text = "Cargando ";
            
            string sSQL = @"
                SELECT ID as [ID], DESCRIPTION as [Descripción]
                FROM [PART]
                WHERE ID LIKE '%" + BUSCAR.Text + "%' OR DESCRIPTION LIKE '%" + BUSCAR.Text + "%'";

            DataTable oDataTablePART = oData.EjecutarConsulta(sSQL);
            dtgrdGeneral.DataSource = oDataTablePART;
            toolStripStatusLabel1.Text = "Total " + dtgrdGeneral.Rows.Count.ToString();

        }

        private void button1_Click(object sender, EventArgs e)
        {
            cargar();
        }

        private void BUSCAR_KeyPress(object sender, KeyPressEventArgs e)
        {
            //Precionar Enter buscar
            if (e.KeyChar.ToString() == "\r")
            {
                cargar();
            }
            
        }

        private void frmUsuarios_Load(object sender, EventArgs e)
        {
            cargar();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            frmAddenda oObjeto = new frmAddenda(oData.sConn);
            oObjeto.ShowDialog();
            cargar();
        }

        private void dtgrdGeneral_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            modificar();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            if (dtgrdGeneral.SelectedRows.Count > 0)
            {
                DataGridViewSelectedRowCollection arrSelectedRows = dtgrdGeneral.SelectedRows;
                string ROW_ID = (string)arrSelectedRows[0].Cells[0].Value;
                frmAddenda oObjeto = new frmAddenda(oData.sConn, ROW_ID);
                oObjeto.ShowDialog();
                cargar();
            }
        }

        public void modificar()
        {
            if (dtgrdGeneral.SelectedRows.Count > 0)
            {
                DataGridViewSelectedRowCollection arrSelectedRows = dtgrdGeneral.SelectedRows;
                selecionados = arrSelectedRows;
                this.DialogResult = DialogResult.OK;
                
            }
        }

        private void frmFormatos_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)27)
            {
                this.Close();
            }
            else
            {
                if ((e.KeyChar.ToString() == "<")
                                | (e.KeyChar.ToString() == ">")
                                
                                | (e.KeyChar.ToString() == "'")
                                | (e.KeyChar.ToString() == "#")
                                )
                {
                    e.Handled = true;
                }
            }
        }
    }
}
