﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;

using System.Text;
using System.Windows.Forms;
using System.Net.Mail;
using System.Text.RegularExpressions;
using System.IO;
using Microsoft.Win32;
using System.Runtime.InteropServices;
using System.Globalization;
using System.Reflection;
using System.Xml;
using System.Xml.Xsl;
using System.Xml.XPath;
using System.Xml.Serialization;
using FE_FX.EDICOM_Servicio;
using System.IO.Compression;
using Ionic.Zip;
using System.ServiceModel;
using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;
using System.Security.Cryptography.X509Certificates;
using System.Diagnostics;
using OfficeOpenXml;
using System.Configuration;
using System.Net;
using System.Net.Security;
using Generales;
using ModeloDocumentosFX;
using FE_FX.Clases;
using FE_FX.ComprobantePago;
using FE_FX.CFDI;
using ModeloComprobantePago;
using System.Linq;

namespace FE_FX
{
    //Ejemplo: checkId 451023

    public partial class frmBandejaComprobantePago : Form
    {
        private cCONEXCION oData = new cCONEXCION("");
        public string VMX_FE_Tabla = "VMX_FE";
        public string BD_Auxiliar = "";
        cUSUARIO ocUSUARIO;
        string USUARIO;
        bool procesar = true;
        bool servicioGeneral = false;

        public frmBandejaComprobantePago(string sConn)
        {
            oData = new cCONEXCION(sConn);
            InitializeComponent();
            limpiar();
            EMPRESA_NOMBRE.Text = "Todas las empresas";
            oTimer.Enabled = false;
            CFDI_PRUEBA.Checked = false;
            oTimer.Stop();
        }

        private void cargarSerieComprobantePago(cEMPRESA ocEMPRESA)
        {
            //Cargar la serie de comprobante de pago
            ocSERIE = new cSERIE();
            cCERTIFICADO ocCERTIFICADO = new cCERTIFICADO();
            foreach (cSERIE registroSerie in ocSERIE.todos_empresa("", ocEMPRESA.ROW_ID, true, false))
            {
                ocCERTIFICADO = new cCERTIFICADO();
                ocCERTIFICADO.cargar(registroSerie.ROW_ID_CERTIFICADO);
                ocSERIE = registroSerie;
                break;
            }
        }

        public frmBandejaComprobantePago(string sConn, string USUARIOp, bool servicio = false)
        {
            USUARIO = USUARIOp;
            oData = new cCONEXCION(sConn);
            servicioGeneral = servicio;
            InitializeComponent();
            limpiar();
            EMPRESA_NOMBRE.Text = "Todas las empresas";
            //Foco al Tab de Busqieda por defecto
            ribbonTabItem1.Focus();

            progressBar1.Visible = false;
            this.Text = Application.ProductVersion + " " + Application.ProductName;
            Columnas_2(dtgrdGeneral);
            cargar_empresas();

            cargar_cfdi_test();
            //Activar el servicio
            if (servicio)
            {
                ejecutarAutomatico.Checked = true;
                oTimer.Enabled = true;
                CFDI_PRUEBA.Checked = false;
                oTimer.Start();
            }

        }

        private void cargar_cfdi_test()
        {
            try
            {
                CFDI_PRUEBA.Checked = bool.Parse(ConfigurationManager.AppSettings["CFDI_TEST"].ToString());
            }
            catch
            {

            }

            try
            {
                PDF_VER.Checked = bool.Parse(ConfigurationManager.AppSettings["PDF_VER"].ToString());
            }
            catch
            {

            }
        }
        private void cargar_empresas()
        {
            ENTITY_ID.Items.Clear();
            ENTITY_ID.Items.Add("Todos");
            cEMPRESA ocEMPRESA = new cEMPRESA(oData);
            foreach (cEMPRESA registro in ocEMPRESA.todos("", true, Globales.automatico))
            {
                ENTITY_ID.Items.Add(registro);
            }
            if (ENTITY_ID.Items.Count > 0)
            {
                ENTITY_ID.SelectedIndex = 0;
            }
        }

        private void Columnas_2(DataGridView dtg)
        {
            int n = 0;

            n = dtg.Columns.Add("Id", "Id");
            dtg.Columns[n].ReadOnly = true;
            dtg.Columns[n].Visible = false;
            dtg.Columns[n].Width = 70;

            n = dtg.Columns.Add("Version", "Versión");
            dtg.Columns[n].ReadOnly = true;
            dtg.Columns[n].Visible = true;
            dtg.Columns[n].Width = 70;

            n = dtg.Columns.Add("SerieFolio", "Serie+Folio");
            dtg.Columns[n].ReadOnly = true;
            dtg.Columns[n].Visible = true;
            dtg.Columns[n].Width = 110;

            n = dtg.Columns.Add("CHECK_ID", "Pago");
            dtg.Columns[n].ReadOnly = true;
            dtg.Columns[n].Visible = true;
            dtg.Columns[n].Width = 110;

            n = dtg.Columns.Add("facturas", "Facturas");
            dtg.Columns[n].ReadOnly = true;
            dtg.Columns[n].Visible = false;
            dtg.Columns[n].Width = 110;

            n = dtg.Columns.Add("CUSTOMER_ID", "Cliente");
            dtg.Columns[n].ReadOnly = true;
            dtg.Columns[n].Width = 80;

            n = dtg.Columns.Add("CUSTOMER_NAME", "Nombre");
            dtg.Columns[n].ReadOnly = true;
            dtg.Columns[n].Width = 180;

            n = dtg.Columns.Add("CHECK_DATE", "Fecha");
            dtg.Columns[n].ReadOnly = true;
            dtg.Columns[n].Width = 70;

            n = dtg.Columns.Add("CURRENCY_ID", "Moneda");
            dtg.Columns[n].ReadOnly = true;
            dtg.Columns[n].Width = 70;


            n = dtg.Columns.Add("FORMA_PAGO", "Forma de Pago");
            dtg.Columns[n].ReadOnly = true;
            dtg.Columns[n].Width = 70;


            n = dtg.Columns.Add("AMOUNT", "Monto");
            dtg.Columns[n].ReadOnly = true;
            dtg.Columns[n].Width = 70;
            dtg.Columns[n].SortMode = DataGridViewColumnSortMode.NotSortable;
            dtg.Columns[n].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            dtg.Columns[n].DefaultCellStyle.Format = "C2";

            n = dtg.Columns.Add("Estado", "Estado");
            dtg.Columns[n].ReadOnly = true;
            dtg.Columns[n].Width = 120;

            n = dtg.Columns.Add("Cancelado", "Cancelado");
            dtg.Columns[n].ReadOnly = true;
            dtg.Columns[n].Width = 120;

            n = dtg.Columns.Add("UUID", "UUID");
            dtg.Columns[n].ReadOnly = true;
            dtg.Columns[n].Width = 70;

            n = dtg.Columns.Add("PDF", "PDF");
            dtg.Columns[n].ReadOnly = true;
            dtg.Columns[n].Width = 240;
            dtg.Columns[n].Visible = false;

            n = dtg.Columns.Add("XML", "XML");
            dtg.Columns[n].ReadOnly = true;
            dtg.Columns[n].Visible = false;
            dtg.Columns[n].Width = 70;

            n = dtg.Columns.Add("PDF_ESTADO", "Impresión PDF");
            dtg.Columns[n].ReadOnly = true;
            dtg.Columns[n].Width = 120;


            n = dtg.Columns.Add("diferenciaMonto", "Diferencia de Monto");
            dtg.Columns[n].ReadOnly = true;
            dtg.Columns[n].Width = 170;

        }


        public byte[] ReadBinaryFile(string fileName)
        {
            if (File.Exists(fileName))
            {
                try
                {
                    ///Open and read a file&#12290;
                    FileStream fileStream = File.OpenRead(fileName);
                    byte[] archivo = ConvertStreamToByteBuffer(fileStream);
                    fileStream.Close();
                    return archivo;
                }
                catch (Exception ex)
                {
                    return new byte[0];
                }
            }
            else
            {
                return new byte[0];
            }
        }

        public byte[] ConvertStreamToByteBuffer(System.IO.Stream theStream)
        {
            int b1;
            System.IO.MemoryStream tempStream = new System.IO.MemoryStream();
            while ((b1 = theStream.ReadByte()) != -1)
            {
                tempStream.WriteByte(((byte)b1));
            }
            return tempStream.ToArray();
        }

        private void frmBandejaComprobantePago_Load(object sender, EventArgs e)
        {
            limpiar();
            //cargar();

        }

        private void frmBandejaComprobantePago_FormClosed(object sender, FormClosedEventArgs e)
        {
            //Application.Exit();
        }

        private void limpiar()
        {
            this.Text = Application.ProductName + " " + Application.ProductVersion;
            DateTime firstDayOfCurrentMonth = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1);
            INICIO.Value = firstDayOfCurrentMonth;
            //cargar_formatos();
        }
        //private void cargar_formatos()
        //{
        //    cEMPRESA ocEMPRESA = new cEMPRESA(oData);
        //    ocEMPRESA = ENTITY_ID.SelectedItem as cEMPRESA;
        //    if (ocEMPRESA != null)
        //    {
        //        FORMATO.Items.Clear();
        //        FORMATO.Text = "";
        //        cFORMATO ocFORMATO = new cFORMATO();
        //        foreach (cFORMATO registro in ocFORMATO.todos_empresa("", ocEMPRESA.ROW_ID,true))
        //        {
        //            FORMATO.Items.Add(registro.ID);
        //        }

        //    }
        //    else
        //    {
        //        //Cargar todos los formatos
        //        FORMATO.Items.Clear();
        //        FORMATO.Text = "";
        //        cFORMATO ocFORMATO = new cFORMATO();
        //        foreach (cFORMATO registro in ocFORMATO.todos(""))
        //        {
        //            FORMATO.Items.Add(registro.ID);
        //        }
        //    }
        //}

        private void btnRefresh_Click(object sender, EventArgs e)
        {
            cargar(dtgrdGeneral);
        }

        private void toolStripButton3_Click(object sender, EventArgs e)
        {
            ver_xml();
        }
        private void ver_pdf()
        {
            if (dtgrdGeneral.SelectedRows.Count > 0)
            {
                abrir_pdf();

            }
        }


        private void ver_xml()
        {
            if (dtgrdGeneral.SelectedRows.Count > 0)
            {
                DataGridViewSelectedRowCollection arrSelectedRows = dtgrdGeneral.SelectedRows;
                string ARCHIVO = arrSelectedRows[0].Cells["XML"].Value.ToString();
                string directorio = AppDomain.CurrentDomain.BaseDirectory;
                frmFacturaXML oObjeto = new frmFacturaXML(ARCHIVO);
                oObjeto.Show();
            }
        }

        private void cargar(DataGridView dtg)
        {
            try
            {
                dtgrdGeneral.Rows.Clear();

                string facturasCargar = INVOICE_IDs.Text;
                string estadoTr = estadoFiltro.Text;
                DataTable oDataTable = new DataTable();
                if (ENTITY_ID.Text == "Todos")
                {
                    //Navegar por los items
                    foreach (var item in ENTITY_ID.Items)
                    {
                        try
                        {
                            cEMPRESA ocEMPRESAtr = item as cEMPRESA;
                            DataTable oDataTabletr = new DataTable();
                            if (ocEMPRESAtr != null)
                            {
                                cCONEXCION oData_ERPp = new cCONEXCION(ocEMPRESAtr.TIPO, ocEMPRESAtr.SERVIDOR
                                    , ocEMPRESAtr.BD, ocEMPRESAtr.USUARIO_BD, ocEMPRESAtr.PASSWORD_BD);
                                //cargar_formatos();
                                oDataTable = cargarComplemento(oData_ERPp, ocEMPRESAtr, facturasCargar,false, estadoTr);
                                cargarDataTable(oDataTable, dtgrdGeneral, oData_ERPp, ocEMPRESAtr, false);
                            }

                        }
                        catch (Exception error)
                        {
                            ErrorFX.mostrar(error, true, true, "frmBandejaComprobantePago - 572 - ", true);
                        }
                    }
                }
                else
                {
                    cEMPRESA ocEMPRESA = ENTITY_ID.SelectedItem as cEMPRESA;
                    if (ocEMPRESA != null)
                    {
                        EMPRESA_NOMBRE.Text = ocEMPRESA.ID;
                        cCONEXCION oData_ERPp = new cCONEXCION(ocEMPRESA.TIPO, ocEMPRESA.SERVIDOR, ocEMPRESA.BD, ocEMPRESA.USUARIO_BD, ocEMPRESA.PASSWORD_BD);
                        //cargar_formatos();
                        oDataTable = cargarComplemento(oData_ERPp, ocEMPRESA, facturasCargar,false, estadoTr);
                        cargarDataTable(oDataTable, dtgrdGeneral, oData_ERPp, ocEMPRESA, false);
                    }
                }

            }
            catch (Exception error)
            {
                ErrorFX.mostrar(error, true, true, "frmBandejaComprobantePago - 1994 - ", true);
            }

        }
        public bool IsNumeric(string text)
        {
            double test;
            return double.TryParse(text, out test);
        }
        private void cargarDataTable(DataTable oDataTable, DataGridView dtg, cCONEXCION oData_ERPp
            , cEMPRESA ocEMPRESA, bool limpiar = true)
        {
            if (oDataTable != null)
            {
                ModeloComprobantePago.documentosfxEntities dbContext = new documentosfxEntities();
                Application.DoEvents();
                if (limpiar)
                {
                    dtg.Rows.Clear();
                }
                toolStripStatusLabel1.Text = "Cargando ";

                int contador = 0;
                foreach (DataRow oDataRow in oDataTable.Rows)
                {
                    contador++;
                    bool mostrar = true;
                    string checkIdTr = oDataRow["CHECK_ID"].ToString();
                    string customerIdTr = oDataRow["CUSTOMER_ID"].ToString();
                    pago oRegistroTr = dbContext.pagoSet.Where(a => a.checkId.Equals(checkIdTr) & a.customerId.Equals(customerIdTr)).FirstOrDefault();
                    if (oRegistroTr != null)
                    {
                        if (!oRegistroTr.estado.ToUpper().Contains(" REAL"))
                        {
                            mostrar = true;
                        }
                        else
                        {
                            if (oRegistroTr.estado.Contains("Error"))
                            {
                                mostrar = true;
                            }
                            else
                            {
                                if (oRegistroTr.estado.Contains("Timbrado "))
                                {
                                    mostrar = true;
                                }
                                else
                                {

                                    if (this.PENDIENTES.Checked)
                                    {
                                        mostrar = false;
                                    }
                                    else
                                    {
                                        mostrar = true;
                                    }
                                }
                            }

                        }
                    }

                    if (mostrar)
                    {
                        int n = dtg.Rows.Add();

                        for (int i = 0; i < dtg.Columns.Count; i++)
                        {
                            dtg.Rows[n].Cells[i].Value = "";
                        }
                        Encapsular oEncapsular = new Encapsular();
                        oEncapsular.oData_ERP = oData_ERPp;
                        oEncapsular.ocEMPRESA = ocEMPRESA;
                        dtg.Rows[n].Tag = oEncapsular;


                        //dtg.Rows[n].Cells["SerieFolio"].Value = oDataRow["SerieFolio"].ToString();
                        dtg.Rows[n].Cells["CHECK_ID"].Value = oDataRow["CHECK_ID"].ToString().Replace("'", "\\'");
                            ;
                        dtg.Rows[n].Cells["CUSTOMER_ID"].Value = oDataRow["CUSTOMER_ID"].ToString();
                        dtg.Rows[n].Cells["CUSTOMER_NAME"].Value = oDataRow["NAME"].ToString();
                        dtg.Rows[n].Cells["facturas"].Value = oDataRow["Facturas"].ToString();
                        dtg.Rows[n].Cells["CHECK_DATE"].Value = DateTime.Parse(oDataRow["CHECK_DATE"].ToString()).ToShortDateString();
                        dtg.Rows[n].Cells["CURRENCY_ID"].Value = oDataRow["CURRENCY_ID"].ToString();
                        dtg.Rows[n].Cells["AMOUNT"].Value = decimal.Parse(oDataRow["AMOUNT"].ToString()).ToString("C2");

                        actualizarRegistro(n, oDataRow["CHECK_ID"].ToString(), oDataRow["CUSTOMER_ID"].ToString(), oRegistroTr);
                    }
                }
                dbContext.Dispose();
                dtg.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.DisplayedCells;
            }
            Application.DoEvents();
            toolStripStatusLabel1.Text = "Fin ";
        }

        private void actualizarRegistro(int n, string checkId, string customerId, pago oRegistrop = null)
        {
            try
            {
                dtgrdGeneral.ShowRowErrors = true;

                ModeloComprobantePago.documentosfxEntities dbContext = new documentosfxEntities();
                pago oRegistroTr = new pago();
                if (oRegistrop == null)
                {
                    oRegistroTr = dbContext.pagoSet.Where(a => a.checkId.Equals(checkId)
                    & a.customerId.Equals(customerId)).FirstOrDefault();
                    oRegistrop = oRegistroTr;
                }
                else
                {
                    oRegistroTr = oRegistrop;
                }
                if (oRegistroTr == null)
                {
                    return;
                }
                dtgrdGeneral.Rows[n].Cells["Id"].Value = oRegistrop.Id;
                dtgrdGeneral.Rows[n].Cells["SerieFolio"].Value = oRegistrop.serie+oRegistrop.folio;
                dtgrdGeneral.Rows[n].Cells["Estado"].Tag = oRegistrop;
                dtgrdGeneral.Rows[n].Cells["version"].Value = oRegistroTr.version;
                dtgrdGeneral.Rows[n].Cells["Estado"].Value = oRegistroTr.estado;
                dtgrdGeneral.Rows[n].Cells["Cancelado"].Value = oRegistroTr.cancelado;
                if (dtgrdGeneral.Rows[n].Cells["Cancelado"].Value != null)
                {
                    if (dtgrdGeneral.Rows[n].Cells["Cancelado"].Value.ToString().Contains("Cancelado"))
                    {
                        dtgrdGeneral.Rows[n].DefaultCellStyle.BackColor = Color.OrangeRed;
                    }
                }
                dtgrdGeneral.Rows[n].Cells["UUID"].Value = oRegistroTr.uuid;
                dtgrdGeneral.Rows[n].Cells["XML"].Value = oRegistroTr.xml;
                dtgrdGeneral.Rows[n].Cells["PDF_ESTADO"].Value = oRegistroTr.pdfEstado;
                dtgrdGeneral.Rows[n].Cells["PDF"].Value = oRegistroTr.pdf;
                dtgrdGeneral.Rows[n].Cells["FORMA_PAGO"].Value = oRegistroTr.formaDePagoP;

                if (oRegistroTr.estado.Contains("BORRADOR"))
                {
                    dtgrdGeneral.Rows[n].DefaultCellStyle.BackColor = Color.Yellow;
                }
                if (oRegistroTr.estado.Contains("Error"))
                {
                    dtgrdGeneral.Rows[n].DefaultCellStyle.BackColor = Color.Red;
                    dtgrdGeneral.Rows[n].ErrorText = oRegistroTr.estado;
                }

                dbContext.Dispose();
            }
            catch (Exception error)
            {
                ErrorFX.mostrar(error, true, true, "frmBandejaComprobantePago - 677 - ", true);
            }
        }

        private DataTable cargarComplemento(cCONEXCION oData_ERPp, cEMPRESA ocEMPRESA, string facturasCargar = ""
            , bool mostrarRelacionados = false, string estadoFiltrP="")
        {
            string BD_Auxiliar = "";
            if (ocEMPRESA.BD_AUXILIAR != "")
            {
                BD_Auxiliar = ocEMPRESA.BD_AUXILIAR + ".dbo.";
            }
            string topTr = TOPtxt.Text;
            if (!IsNumeric(topTr))
            {
                topTr = "25";
                TOPtxt.Text = topTr;
            }

            string facturasTr = @",STUFF((SELECT distinct ', ' + t1.INVOICE_ID
            from CASH_RECEIPT_LINE t1
            where t1.CHECK_ID = cr.CHECK_ID
            FOR XML PATH(''), TYPE
            ).value('.', 'NVARCHAR(MAX)')
            ,1,2,'') as Facturas";

            string columnaSQL = ",'' as Facturas";
            if (mostrarRelacionados)
            {
                columnaSQL = ",crl.INVOICE_ID as Factura,crl.AMOUNT as Factura";
            }
            ModeloComprobantePago.documentosfxEntities dbContext = new documentosfxEntities();
            string DatabaseName = dbContext.Database.Connection.Database;

            string SqlFolio = @"
            , ISNULL((SELECT TOP 1 p.[serie]+p.[folio]
            FROM [" + DatabaseName + @"]..[pagoSet] p
            WHERE p.[checkId] collate SQL_Latin1_General_CP1_CI_AS = cr.CHECK_ID AND p.[customerId] collate SQL_Latin1_General_CP1_CI_AS=cr.CUSTOMER_ID ),'') as SerieFolio
            ";

            string sSQL = @"
            SELECT TOP (" + topTr + @") cr.CUSTOMER_ID, c.NAME, cr.CHECK_ID, cr.BANK_ACCOUNT_ID, ba.DESCRIPTION
            , cr.AMOUNT, cr.USER_ID, cr.CHECK_DATE, cr.STATUS, cr.CURRENCY_ID, cr.PAYMENT_METHOD
            
            " + columnaSQL + @" 
            FROM CASH_RECEIPT AS cr
            INNER JOIN CUSTOMER AS c ON cr.CUSTOMER_ID = c.ID 
            INNER JOIN BANK_ACCOUNT AS ba ON cr.BANK_ACCOUNT_ID = ba.ID";

            if (mostrarRelacionados)
            {
                sSQL += @"
                    INNER JOIN CASH_RECEIPT_LINE AS crl ON crl.CHECK_ID = cr.CHECK_ID
                ";
            }


            string sSQL_Where = " WHERE 1=1 AND cr.AMOUNT<>0 ";

            //Agregar la entidad
            try
            {
                if (existeENTITY_ID(oData_ERPp))
                {
                    if (ocEMPRESA.ENTITY_ID != "")
                    {
                        sSQL_Where += @" AND cr.ENTITY_ID ='" + ocEMPRESA.ENTITY_ID + "'";
                    }
                }
                else
                {
                    if (ocEMPRESA.ENTITY_ID != "")
                    {
                        sSQL_Where += @" AND cr.SITE_ID ='" + ocEMPRESA.ENTITY_ID + "'";
                    }
                }
            }
            catch(Exception error)
            {
                ErrorFX.mostrar(error, false, false, "Validacion de Entidad en VIsual -frmBandejaComprobantePago - 620 - ", false);
                return null;
            }
            


            string FECHA_INICIO = oData.convertir_fecha(new DateTime(INICIO.Value.Year, INICIO.Value.Month, INICIO.Value.Day, 0, 0, 0), oData_ERPp.sTipo);
            sSQL_Where += " AND cr.CHECK_DATE>=" + FECHA_INICIO + "";
            string FECHA_FINAL = oData.convertir_fecha(new DateTime(FINAL.Value.Year, FINAL.Value.Month, FINAL.Value.Day, 0, 0, 0), oData_ERPp.sTipo);
            sSQL_Where += " AND cr.CHECK_DATE<=" + FECHA_FINAL + "";

            if (!String.IsNullOrEmpty(CHECK_ID.Text))
            {
                sSQL_Where += " AND cr.CHECK_ID  like '%" + CHECK_ID.Text + "%'";
            }
            if (!String.IsNullOrEmpty(CUSTOMER_NAME.Text))
            {
                sSQL_Where += " AND (cr.CUSTOMER_ID  like '%" + CUSTOMER_NAME.Text + "%' OR c.NAME  like '%" + CUSTOMER_NAME.Text + "%')";
            }
            if (!String.IsNullOrEmpty(INVOICE_IDs.Text))
            {
                sSQL_Where += @" AND STUFF((SELECT distinct ', ' + t1.INVOICE_ID
                                from CASH_RECEIPT_LINE t1
                                where t1.CHECK_ID = cr.CHECK_ID
                                FOR XML PATH(''), TYPE
                                ).value('.', 'NVARCHAR(MAX)')
                                ,1,2,'')  like '%" + INVOICE_IDs.Text + "%'";
            }

            if (!String.IsNullOrEmpty(estadoFiltrP))
            {
                sSQL_Where += @" AND ";
            }

            sSQL += sSQL_Where;

            //Ordenar por fecha de pago
            sSQL += " ORDER BY cr.CHECK_DATE";


            DataTable oDataTable = oData_ERPp.EjecutarConsulta(sSQL); ;


            return oDataTable;
        }

        private bool existeENTITY_ID(cCONEXCION oData_ERPp)
        {
            try
            {
                string sSQL = @"
                SELECT TOP 1 *
                FROM INFORMATION_SCHEMA.COLUMNS
                WHERE 
                [TABLE_NAME] = 'CASH_RECEIPT'
                AND[COLUMN_NAME] = 'ENTITY_ID'
                ";
                DataTable oDataTable1 = oData_ERPp.EjecutarConsulta(sSQL);
                if (oDataTable1.Rows.Count == 0)
                {
                    return false;
                }
                return true;
            }
            catch (Exception error)
            {
                ErrorFX.mostrar(error, false, false, "frmBandejaComprobantePago - 2318 - ", false);
            }
            return false;
        }

        private void toolStripButton9_Click(object sender, EventArgs e)
        {

            abrir_pdf();
        }

        private void abrir_pdf()
        {
            if (dtgrdGeneral.SelectedRows.Count > 0)
            {
                DataGridViewSelectedRowCollection arrSelectedRows = dtgrdGeneral.SelectedRows;

                int d = 0;
                for (int n = arrSelectedRows.Count - 1; n >= 0; n--)
                {
                    if (arrSelectedRows[n].Cells["PDF"].Value != null)
                    {
                        if (String.IsNullOrEmpty(arrSelectedRows[n].Cells["PDF"].Value.ToString()))
                        {
                            return;
                        }
                        string pdf = arrSelectedRows[n].Cells["PDF"].Value.ToString();
                        if (!File.Exists(pdf))
                        {
                            MessageBox.Show("No se puede tener acceso a: " + pdf);
                            return;
                        }
                        frmPDF ofrmPDF = new frmPDF(pdf);
                        ofrmPDF.Show();
                    }

                }
            }
        }

        private void buttonItem8_Click(object sender, EventArgs e)
        {
            actualizar();
        }

        private void actualizar()
        {
            informacion.Text = "";
            oTimer.Stop();
            progressBar1.Visible = true;
            Application.DoEvents();
            cargar(dtgrdGeneral);
            Application.DoEvents();
            progressBar1.Visible = false;

        }

        private void buttonItem2_Click_1(object sender, EventArgs e)
        {
            generarRecepcionPagos();
        }

        private void regenerarPdf()
        {
            if (dtgrdGeneral.SelectedRows.Count > 0)
            {
                MessageBox.Show("Recuerde que debe cerrar el PDF para poder regenerar el archivo.", Application.ProductName
                    , MessageBoxButtons.OK, MessageBoxIcon.Information);
                DataGridViewSelectedRowCollection arrSelectedRows = dtgrdGeneral.SelectedRows;

                for (int n = arrSelectedRows.Count - 1; n >= 0; n--)
                {
                    if (arrSelectedRows[n].Tag != null)
                    {
                        Encapsular oEncapsular = (Encapsular)arrSelectedRows[n].Tag;
                        pago oPago = (pago)arrSelectedRows[n].Cells["Estado"].Tag;
                        cComprobantePago ocComprobantePago = new cComprobantePago();
                        ocComprobantePago.imprimir(oPago, oEncapsular,PDF_VER.Checked);
                    }
                }
                MessageBox.Show("PDF de las Comprobantes fueron regenerados.", Application.ProductName
                    , MessageBoxButtons.OK, MessageBoxIcon.Information);
            }

        }

        private void buttonMargins_Click(object sender, EventArgs e)
        {
            exportar();
        }

        private void exportar()
        {
            ExcelFX.exportar(dtgrdGeneral, "Comprobantes de pago " + DateTime.Now.ToString("yyyy-MM-dd--hh-mm-ss"));
        }

        private DataTable GetDataTableFromDGV(DataGridView dgv)
        {
            var dt = new DataTable();
            foreach (DataGridViewColumn column in dgv.Columns)
            {
                //if (column.Visible)
                //{
                // You could potentially name the column based on the DGV column name (beware of dupes)
                // or assign a type based on the data type of the data bound to this DGV column.
                dt.Columns.Add(column.HeaderText);
                //}
            }

            object[] cellValues = new object[dgv.Columns.Count];
            foreach (DataGridViewRow row in dgv.Rows)
            {
                for (int i = 0; i < row.Cells.Count; i++)
                {
                    cellValues[i] = row.Cells[i].Value;
                }
                dt.Rows.Add(cellValues);
            }

            return dt;
        }

        private void buttonItem23_Click(object sender, EventArgs e)
        {
            string url = ConfigurationManager.AppSettings["SOPORTE"].ToString(); ;
            System.Diagnostics.Process.Start(url);
        }

        private void enviar_mail(bool adjuntar, bool solicitarCorreo = false)
        {
            if (dtgrdGeneral.SelectedRows.Count > 0)
            {
                string correoElectronico = "";
                if (solicitarCorreo)
                {
                    frmEntradaEmail Objeto = new frmEntradaEmail();
                    Objeto.Text = "Correo Destino";
                    if (Objeto.ShowDialog(this) == DialogResult.OK)
                    {
                        correoElectronico = Objeto.EMAIL;
                    }
                }


                DataGridViewSelectedRowCollection arrSelectedRows = dtgrdGeneral.SelectedRows;
                for (int n = arrSelectedRows.Count - 1; n >= 0; n--)
                {
                    int indice = arrSelectedRows[n].Index;
                    string CHECK_IDtr = arrSelectedRows[n].Cells["CHECK_ID"].Value.ToString();
                    string CUSTOMER_IDtr = arrSelectedRows[n].Cells["CUSTOMER_ID"].Value.ToString();


                    Encapsular oEncapsular = (Encapsular)arrSelectedRows[n].Tag;
                    enviar(oEncapsular, CHECK_IDtr, CUSTOMER_IDtr, true, correoElectronico);

                }
                //if (this.Visible)
                //{
                //    MessageBox.Show("Facturas en Proceso de Envio.");
                //}
            }
        }

        private void enviar(Encapsular oEncapsular, string CHECK_IDtr, string CUSTOMER_IDtr, bool adjuntar, string correoElectronico = null)
        {
            try
            {
                ModeloComprobantePago.documentosfxEntities dbContext = new documentosfxEntities();
                pago oRegistro = dbContext.pagoSet.Where(a => a.checkId.Equals(CHECK_IDtr) & a.customerId.Equals(CUSTOMER_IDtr)).FirstOrDefault();
                if (oRegistro != null)
                {

                    cComprobantePago ocComprobantePago = new cComprobantePago();
                    ocComprobantePago.enviar(oRegistro, adjuntar, false, false, oEncapsular);

                    informacion.Text = "Enviado el " + DateTime.Now.ToShortTimeString();
                }
                dbContext.Dispose();

            }
            catch (Exception e)
            {
                ErrorFX.mostrar(e, true, true, this.Name + " - 828");
            }
        }

        private void ENTITY_ID_SelectedIndexChanged(object sender, EventArgs e)
        {
            actualizar_conexcion();
        }

        private void actualizar_conexcion()
        {
            try
            {
                if (ENTITY_ID.SelectedText == "Todos")
                {
                    EMPRESA_NOMBRE.Text = "Todas las empresas";
                }
                else
                {
                    cEMPRESA ocEMPRESA = ENTITY_ID.SelectedItem as cEMPRESA;
                    if (ocEMPRESA != null)
                    {
                        EMPRESA_NOMBRE.Text = ocEMPRESA.ID;
                        //cargar_formatos();
                        cargar(dtgrdGeneral);

                        cargarSerieComprobantePago(ocEMPRESA);
                    }
                }
            }
            catch (Exception e)
            {

            }
        }

        private void buttonExit_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void cancelar()
        {
            if (dtgrdGeneral.SelectedRows.Count > 0)
            {
                bool mostrar = false;

                DataGridViewSelectedRowCollection arrSelectedRows = dtgrdGeneral.SelectedRows;
                int contador = 0;

                for (int n = arrSelectedRows.Count - 1; n >= 0; n--)
                {
                    int indice = arrSelectedRows[n].Index;
                    Encapsular oEncapsular = (Encapsular)arrSelectedRows[n].Tag;
                    if (arrSelectedRows[n].Cells["UUID"].Value != null)
                    {
                        string UUID = arrSelectedRows[n].Cells["UUID"].Value.ToString();

                        if (UUID != "")
                        {
                            ModeloComprobantePago.documentosfxEntities dbContext = new documentosfxEntities();
                            pago oRegistro = dbContext.pagoSet.Where(a => a.uuid == UUID).FirstOrDefault();
                            if (!String.IsNullOrEmpty(oRegistro.cancelado))
                            {
                                //Comprobante ya cancelado
                                return;
                            }
                            cancelarComprobante(UUID, oEncapsular);
                            oRegistro.cancelado = "Cancelado por el Usuario " + DateTime.Now.ToString("dd/MM/yyyy HH:MM:ss");

                            if (oRegistro != null)
                            {
                                dbContext.pagoSet.Attach(oRegistro);
                                dbContext.Entry(oRegistro).State = System.Data.Entity.EntityState.Modified;
                                dbContext.SaveChanges();
                                dbContext.Dispose();
                            }
                        }
                    }
                }


                if (mostrar)
                {
                    MessageBox.Show(contador.ToString() + " Comprobantes cancelados.", Application.ProductName
                        , MessageBoxButtons.OK, MessageBoxIcon.Information);
                }

            }
        }

        private bool cancelarComprobante(string UUID, Encapsular oEncapsular)
        {
            ModeloComprobantePago.documentosfxEntities dbContext = new documentosfxEntities();
            pago oRegistro = dbContext.pagoSet.Where(a => a.uuid == UUID).FirstOrDefault();
            if (!String.IsNullOrEmpty(oRegistro.cancelado))
            {
                //Comprobante ya cancelado
                return false;
            }
            cargarSerieComprobantePago(oEncapsular.ocEMPRESA);
            cCERTIFICADO ocCERTIFICADO = new cCERTIFICADO();
            ocCERTIFICADO.cargar(ocSERIE.ROW_ID_CERTIFICADO);
            //Llamar el Servicio
            CFDiClient oCFDiClient = new CFDiClient();
            string[] uuid = new string[1];
            uuid[0] = UUID;


            //Cargar el archivo pfx creado desde OpenSSL
            string pfx = ocCERTIFICADO.PFX;
            if (!File.Exists(pfx))
            {
                //Validar el directorio

                string directorio = AppDomain.CurrentDomain.BaseDirectory;
                pfx = directorio + @"\" + ocCERTIFICADO.PFX;
                if (!File.Exists(pfx))
                {
                    MessageBox.Show("El PFX " + ocCERTIFICADO.PFX + " no existe."
                        , Application.ProductName + "-" + Application.ProductVersion, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                    return false;
                }
            }
            byte[] pfx_archivo = ReadBinaryFile(pfx);
            try
            {
                frmCancelacion OfrmCancelacion = new frmCancelacion(UUID, oEncapsular);
                if (OfrmCancelacion.ShowDialog() == DialogResult.Cancel)
                {
                    MessageBox.Show("El proceso fue cancelado por el usuario"
                        , Application.ProductName + "-" + Application.ProductVersion, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                    return false;
                }
                string[] UUIDs = { UUID };

                //Todo: Realizar cancelacion
                BitacoraFX.Log("Cancelar con el Password: " + cENCRIPTACION.Decrypt(Globales.oCFDI_USUARIO.PASSWORD));

                CancelData respuesta = oCFDiClient.cancelCFDiAsync(Globales.oCFDI_USUARIO.USUARIO
                    , cENCRIPTACION.Decrypt(Globales.oCFDI_USUARIO.PASSWORD)
                , oEncapsular.ocEMPRESA.RFC, oRegistro.receptor
                , UUID, 0
                , pfx_archivo, "abc123"
                , OfrmCancelacion.Motivo, OfrmCancelacion.Sustitucion, OfrmCancelacion.CancelarPrueba);

                oRegistro.cancelado = "Cancelado " + DateTime.Now.ToString("dd/MM/yyyy HH:MM:ss");

                if (oRegistro != null)
                {
                    dbContext.pagoSet.Attach(oRegistro);
                    dbContext.Entry(oRegistro).State = System.Data.Entity.EntityState.Modified;
                    dbContext.SaveChanges();
                    dbContext.Dispose();
                }

                return true;
            }
            catch (FaultException oException)
            {

                CFDiException oCFDiException = new CFDiException();
                MessageBox.Show(oException.Message, Application.ProductName + "-" + Application.ProductVersion, MessageBoxButtons.OK, MessageBoxIcon.Error);
                toolStripStatusLabel1.Text = "Error: Comprobante No Cancelado " + DateTime.Now.ToShortDateString() + " " + DateTime.Now.ToLongTimeString();

                return false;
            }
            catch (Exception oException)
            {

                CFDiException oCFDiException = new CFDiException();
                MessageBox.Show(oException.Message, Application.ProductName + "-" + Application.ProductVersion, MessageBoxButtons.OK, MessageBoxIcon.Error);
                toolStripStatusLabel1.Text = "Error: Comprobante No Cancelado " + DateTime.Now.ToShortDateString() + " " + DateTime.Now.ToLongTimeString();

                return false;
            }
        }

        private void buttonItem29_Click(object sender, EventArgs e)
        {
            //Cargar el manual
            string archivo_temporal = ConfigurationManager.AppSettings["MANUAL"].ToString();
            System.Diagnostics.Process.Start(archivo_temporal);
        }


        private void oTimer_Tick(object sender, EventArgs e)
        {

            //Actualizar fecha final al GET DATE
            GC.Collect();
            GC.WaitForPendingFinalizers();
            FINAL.Value = DateTime.Now;
            actualizar();
            if (dtgrdGeneral.Rows.Count > 0)
            {
                PDF_VER.Checked = false;
                dtgrdGeneral.SelectAll();
                oTimer.Stop();
                generarRecepcionPagos();
            }


            oTimer.Start();
        }

        private void buttonItem3_Click_1(object sender, EventArgs e)
        {
            ModeloDocumentosFXContainer dbContext = new ModeloDocumentosFXContainer();
            try
            {
                string conection = dbContext.Database.Connection.ConnectionString;
                frmCliente_Asuntos ofrmCliente_Asuntos = new frmCliente_Asuntos(oData.sConn, conection);
                ofrmCliente_Asuntos.ShowDialog();
            }
            catch (Exception err)
            {
                ErrorFX.mostrar(err, false, true, "frmBandejaComprobantePago - 3907 - ", false);
            }

        }



        private void dtgrdGeneral_MouseClick(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Right)
            {
                contextMenuStrip1.Show(dtgrdGeneral, new Point(e.X, e.Y));
            }
        }

        private void buttonItem21_Click_1(object sender, EventArgs e)
        {
            generarRecepcionPagos();
        }

        private void buttonItem24_Click(object sender, EventArgs e)
        {
            generarRecepcionPagos();
        }

        private void pDFToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ver_pdf();
        }

        private void xMLToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ver_xml();
        }


        private void regenerarPDFToolStripMenuItem_Click(object sender, EventArgs e)
        {
            regenerarPdf();
        }



        private void exportarToolStripMenuItem_Click(object sender, EventArgs e)
        {
            exportar();
        }

        private void enviarPorCorreoPDFYXMLToolStripMenuItem_Click(object sender, EventArgs e)
        {
            enviar_mail(false);
        }

        private void button1_Click(object sender, EventArgs e)
        {

            actualizar();
        }

        private void cancelarToolStripMenuItem_Click(object sender, EventArgs e)
        {
            cancelar();
        }

        private void buttonItem1_Click_1(object sender, EventArgs e)
        {
            frmEmpresas oObjeto = new frmEmpresas(oData.sConn);
            oObjeto.Show();
        }

        private void buttonItem7_Click(object sender, EventArgs e)
        {
            frmSeries oObjeto = new frmSeries(oData.sConn);
            oObjeto.Show();
        }

        private void buttonItem9_Click_1(object sender, EventArgs e)
        {
            frmCertificados oObjeto = new frmCertificados(oData.sConn);
            oObjeto.Show();
        }

        private void buttonItem14_Click_1(object sender, EventArgs e)
        {
            frmFormatos oObjeto = new frmFormatos(oData.sConn);
            oObjeto.Show();
        }

        private void generarToolStripMenuItem_Click(object sender, EventArgs e)
        {
            generarRecepcionPagos();
        }
        cSERIE ocSERIE;
        private void generarRecepcionPagos(bool autoAjustar = false, bool omitirValidacionNDC=false, bool tipoCambioPiguarlarlo = true, bool eliminarRegistro=false)
        {
            try
            {
                if (dtgrdGeneral.SelectedRows.Count > 0)
                {
                    foreach (DataGridViewRow registro in dtgrdGeneral.SelectedRows)
                    {
                        Encapsular oEncapsular = (Encapsular)registro.Tag;
                        cargarSerieComprobantePago(oEncapsular.ocEMPRESA);
                        //Generar el comprobante de pago
                        cComprobantePago ocComprobantePago = new cComprobantePago();

                        if (eliminarRegistro)
                        {
                            ocComprobantePago.eliminar(registro.Cells["CHECK_ID"].Value.ToString(), registro.Cells["CUSTOMER_ID"].Value.ToString());
                        }


                        string estadoTr = "Timbrado " + DateTime.Now.ToShortDateString() + " " + DateTime.Now.ToShortTimeString();
                        if (CFDI_PRUEBA.Checked)
                        {
                            estadoTr = "BORRADOR";
                        }

                        if (String.IsNullOrEmpty(ocSERIE.ROW_ID))
                        {
                            ErrorFX.mostrar("Configure una serie para los comprobantes fiscales y vuelva a timbrar.", true, false, false);
                            frmSeries ofrmSeries = new frmSeries(oEncapsular.ocEMPRESA.ROW_ID);
                            ofrmSeries.Show();
                            return;
                        }
                        pago oPagoTr = new pago();
                        if (registro.Cells["FORMA_PAGO"].Tag != null)
                        {
                            oPagoTr = (pago)registro.Cells["FORMA_PAGO"].Tag;
                        }
                        else
                        {
                            oPagoTr = cargarUltimoPago(registro.Cells["CHECK_ID"].Value.ToString(), registro.Cells["CUSTOMER_ID"].Value.ToString());
                        }
                        //Validar el estado para que no volverlo a timbrar

                        if (registro.Cells["Estado"].Value != null)
                        {
                            if (!String.IsNullOrEmpty(registro.Cells["Estado"].Value.ToString()))
                            {
                                if (registro.Cells["Estado"].Value.ToString().ToUpper().Contains("REAL"))
                                {
                                    if (registro.Cells["Cancelado"].Value != null)
                                    {
                                        if (!registro.Cells["Cancelado"].Value.ToString().Contains("Cancelado"))
                                        {
                                            if (!ocComprobantePago.validarRelacion(registro.Cells["CHECK_ID"].Value.ToString(), registro.Cells["CUSTOMER_ID"].Value.ToString()))
                                            {
                                                ErrorFX.mostrar("1186 - El comprobante de pago ya esta timbrado en modo real Check: " + registro.Cells["CHECK_ID"].Value.ToString()
                                                + " Cliente: " + registro.Cells["CUSTOMER_ID"].Value.ToString(), true, false, false);
                                                break;
                                            }

                                        }
                                    }
                                    else
                                    {
                                        if (!ocComprobantePago.validarRelacion(registro.Cells["CHECK_ID"].Value.ToString(), registro.Cells["CUSTOMER_ID"].Value.ToString()))
                                        {
                                            ErrorFX.mostrar("1197 - El comprobante de pago ya esta timbrado en modo real Check: " + registro.Cells["CHECK_ID"].Value.ToString()
                                            + " Cliente: " + registro.Cells["CUSTOMER_ID"].Value.ToString(), true, false, false);
                                            break;
                                        }
                                    }

                                }
                            }

                        }



                        //bool resultado = ocComprobantePago.generar(registro.Cells["CHECK_ID"].Value.ToString(), registro.Cells["CUSTOMER_ID"].Value.ToString()
                        //    , oEncapsular, ocSERIE
                        //    , estadoTr, Version.Text, oPagoTr, omitirValidacionNDC, tipoCambioPiguarlarlo);
                        
                        bool resultado = ocComprobantePago.GenerarNuevo(registro.Cells["CHECK_ID"].Value.ToString(), registro.Cells["CUSTOMER_ID"].Value.ToString()
                            , oEncapsular, ocSERIE
                            , estadoTr, Version.Text, oPagoTr, omitirValidacionNDC, tipoCambioPiguarlarlo);
                        if (resultado)
                        {
                            if (Version.Text == "4.0")
                            {
                                Generar40 OGenerar40 = new Generar40();
                                CFDI40.Comprobante oComprobante = OGenerar40.generarComprobantePago(ocComprobantePago.oRegistro, oEncapsular, autoAjustar
                                    , !CFDI_PRUEBA.Checked);
                                if (oComprobante != null)
                                {
                                    ModeloComprobantePago.documentosfxEntities dbContext = new documentosfxEntities();
                                    pago oRegistro = dbContext.pagoSet.Where(a => a.Id == ocComprobantePago.oRegistro.Id).FirstOrDefault();
                                    dtgrdGeneral.Rows[registro.Index].ErrorText = "";
                                    if (ocComprobantePago.timbrar40(oComprobante, estadoTr, oRegistro, ocSERIE))
                                    {
                                        ocComprobantePago.imprimir(oRegistro, oEncapsular, PDF_VER.Checked);
                                        this.actualizarRegistro(registro.Index, registro.Cells["CHECK_ID"].Value.ToString(), registro.Cells["CUSTOMER_ID"].Value.ToString());
                                    }
                                    else
                                    {
                                        dtgrdGeneral.Rows[registro.Index].ErrorText = "Error de timbrado " + ocComprobantePago.informacion;
                                        actualizarRegistro(registro.Index, registro.Cells["CHECK_ID"].Value.ToString(), registro.Cells["CUSTOMER_ID"].Value.ToString());

                                    }
                                    dbContext.Dispose();
                                }
                            }
                            else
                            {
                                generar33 oGenerar33 = new generar33();
                                CFDI33.Comprobante oComprobante = oGenerar33.generarComprobantePago(ocComprobantePago.oRegistro, oEncapsular, autoAjustar);
                                if (oComprobante != null)
                                {
                                    ModeloComprobantePago.documentosfxEntities dbContext = new documentosfxEntities();
                                    pago oRegistro = dbContext.pagoSet.Where(a => a.Id == ocComprobantePago.oRegistro.Id).FirstOrDefault();

                                    if (ocComprobantePago.timbrar(oComprobante, estadoTr, oRegistro, ocSERIE))
                                    {
                                        ocComprobantePago.imprimir(oRegistro, oEncapsular, PDF_VER.Checked);
                                        this.actualizarRegistro(registro.Index, registro.Cells["CHECK_ID"].Value.ToString(), registro.Cells["CUSTOMER_ID"].Value.ToString());
                                    }
                                    else
                                    {
                                        dtgrdGeneral.Rows[registro.Index].ErrorText = "Error de timbrado " + ocComprobantePago.informacion;
                                        actualizarRegistro(registro.Index, registro.Cells["CHECK_ID"].Value.ToString(), registro.Cells["CUSTOMER_ID"].Value.ToString());

                                    }
                                    dbContext.Dispose();
                                }

                            }



                        }
                        else
                        {
                            ocComprobantePago.guardarError(ocComprobantePago.oRegistro, ocComprobantePago.informacion);
                            string mensajeError = "";
                            if (!String.IsNullOrEmpty(ocComprobantePago.informacion))
                            {
                                mensajeError += ocComprobantePago.informacion + " ";
                            }
                            if (!String.IsNullOrEmpty(ocComprobantePago.mensaje))
                            {
                                mensajeError += ocComprobantePago.mensaje + " ";
                            }
                                dtgrdGeneral.Rows[registro.Index].ErrorText = mensajeError;


                            dtgrdGeneral.Rows[registro.Index].Cells["Estado"].Value = "Error 1223 - " + mensajeError;
                        }

                    }
                }
                else
                {
                    ErrorFX.mostrar("Debe seleccionar un pago para aplicarle el complemento de pago.", true, false, false);
                }
            }
            catch (Exception e)
            {
                ErrorFX.mostrar(e, true, true, false);
            }
        }

        private void enviarPorCorreoPDFYXMLCorreoPersonalizadoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            enviar_mail(false, true);
        }

        private void button2_Click(object sender, EventArgs e)
        {
            generarRecepcionPagos();
        }

        private void cambiarFormaDePagoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            mostrarFormaPago();
        }

        private void mostrarFormaPago()
        {
            try
            {
                if (dtgrdGeneral.SelectedRows.Count > 0)
                {
                    DataGridViewSelectedRowCollection arrSelectedRows = dtgrdGeneral.SelectedRows;
                    for (int n = arrSelectedRows.Count - 1; n >= 0; n--)
                    {
                        int indice = arrSelectedRows[n].Index;
                        string CHECK_IDtr = arrSelectedRows[n].Cells["CHECK_ID"].Value.ToString();
                        string CUSTOMER_ID = arrSelectedRows[n].Cells["CUSTOMER_ID"].Value.ToString();

                        bool cambiar = true;
                        if (!String.IsNullOrEmpty(arrSelectedRows[n].Cells["Estado"].Value.ToString()))
                        {
                            if (!arrSelectedRows[n].Cells["Estado"].Value.ToString().Contains("BORRADOR")
                                &
                                 !arrSelectedRows[n].Cells["Estado"].Value.ToString().Contains("Error"))
                            {
                                //Validar si no tiene relacionado
                                cComprobantePago ocComprobantePago = new cComprobantePago();
                                if (!ocComprobantePago.validarRelacion(CHECK_IDtr, CUSTOMER_ID))
                                {
                                    cambiar = false;
                                }
                            }
                        }


                        if (cambiar)
                        {
                            pago oPagoTr = new pago();
                            if (arrSelectedRows[n].Cells["FORMA_PAGO"].Tag != null)
                            {
                                oPagoTr = (pago)arrSelectedRows[n].Cells["FORMA_PAGO"].Tag;
                            }
                            else
                            {
                                //Cargar el ultimo
                                oPagoTr = cargarUltimoPago(CHECK_IDtr, CUSTOMER_ID);
                            }

                            //if ()
                            //{
                            //    ocComprobantePago.generar(registro.Cells["CHECK_ID"].Value.ToString(), registro.Cells["CUSTOMER_ID"].Value.ToString()
                            //                , oEncapsular, ocSERIE
                            //                , estadoTr, oPagoTr, omitirValidacionNDC, tipoCambioPiguarlarlo);
                            //}


                            frmEditarComprobantePago EditarTr = new frmEditarComprobantePago(CHECK_IDtr, CUSTOMER_ID);
                            if (EditarTr.ShowDialog() == DialogResult.OK)
                            {
                                arrSelectedRows[n].Cells["FORMA_PAGO"].Value = EditarTr.oRegistroP.formaDePagoP;
                                arrSelectedRows[n].Cells["FORMA_PAGO"].Tag = EditarTr.oRegistroP;
                            }

                            //frmComprobanteCambioForma ofrmComprobanteCambioForma = new frmComprobanteCambioForma(CHECK_IDtr, CUSTOMER_ID);
                            //if (ofrmComprobanteCambioForma.ShowDialog() == DialogResult.OK)
                            //{                                
                            //    arrSelectedRows[n].Cells["FORMA_PAGO"].Value = ofrmComprobanteCambioForma.oRegistroP.formaDePagoP;
                            //    arrSelectedRows[n].Cells["FORMA_PAGO"].Tag = ofrmComprobanteCambioForma.oRegistroP;
                            //}

                        }
                        else
                        {
                            MessageBox.Show("No puede modificar la Factura, se encuetra Timbrada o no tiene documentos relacionados.");
                        }
                    }
                }
            }
            catch (Exception e)
            {
                ErrorFX.mostrar(e, true, true, "frmBandejaComprobantePago - 1315 ");
            }
            return;
        }

        private pago cargarUltimoPago(string checkId, string customerId)
        {
            try
            {
                ModeloComprobantePago.documentosfxEntities dbContext = new documentosfxEntities();
                pago oRegistroTr = new pago();
                oRegistroTr = dbContext.pagoSet.Where(a => a.customerId.Equals(customerId))
                    .OrderByDescending(b => b.Id)
                    .FirstOrDefault();
                dbContext.Dispose();
                return oRegistroTr;
            }
            catch (Exception e)
            {
                ErrorFX.mostrar(e, true, true, "frmBandejaComprobantePago - 1315 ");
            }
            return null;
        }

        private void button3_Click(object sender, EventArgs e)
        {

        }

        private void button4_Click(object sender, EventArgs e)
        {
            reporte();
        }

        private void reporte()
        {
            try
            {
                DataTable oDataTable = new DataTable();
                if (ENTITY_ID.Text == "Todos")
                {
                    //Navegar por los items
                    foreach (var item in ENTITY_ID.Items)
                    {
                        try
                        {
                            cEMPRESA ocEMPRESAtr = item as cEMPRESA;
                            DataTable oDataTabletr = new DataTable();
                            if (ocEMPRESAtr != null)
                            {
                                cCONEXCION oData_ERPp = new cCONEXCION(ocEMPRESAtr.TIPO, ocEMPRESAtr.SERVIDOR
                                    , ocEMPRESAtr.BD, ocEMPRESAtr.USUARIO_BD, ocEMPRESAtr.PASSWORD_BD);
                                //cargar_formatos();
                                oDataTable = cargarComplemento(oData_ERPp, ocEMPRESAtr, "", true);
                                ExcelFX.exportarDataTable(oDataTable, "Relacion de pagos y facturas");
                            }

                        }
                        catch (Exception error)
                        {
                            ErrorFX.mostrar(error, true, true, "frmBandejaComprobantePago - 572 - ", true);
                        }
                    }
                }
                else
                {
                    cEMPRESA ocEMPRESA = ENTITY_ID.SelectedItem as cEMPRESA;
                    if (ocEMPRESA != null)
                    {
                        EMPRESA_NOMBRE.Text = ocEMPRESA.ID;
                        cCONEXCION oData_ERPp = new cCONEXCION(ocEMPRESA.TIPO, ocEMPRESA.SERVIDOR, ocEMPRESA.BD, ocEMPRESA.USUARIO_BD, ocEMPRESA.PASSWORD_BD);
                        //cargar_formatos();
                        oDataTable = cargarComplemento(oData_ERPp, ocEMPRESA, "", true);

                        ExcelFX.exportarDataTable(oDataTable, "Relacion de pagos y facturas");
                    }
                }

            }
            catch (Exception e)
            {
                ErrorFX.mostrar(e, true, true, "frmBandejaComprobantePago - 1320 ");
            }
            return;
        }

        private void ribbonTabItem2_Click(object sender, EventArgs e)
        {

        }

        private void button5_Click(object sender, EventArgs e)
        {
            analizar();
        }

        private void analizar()
        {
            try
            {

            }
            catch (Exception e)
            {
                ErrorFX.mostrar(e, true, true, "frmBandejaComprobantePago - 1320 ");
            }
            return;
        }

        private void ejecutarAutomatico_CheckedChanged(object sender, EventArgs e)
        {

        }

        private void ejecutarAutomatico_CheckedChanged_1(object sender, EventArgs e)
        {
            if (ejecutarAutomatico.Checked)
            {
                oTimer.Start();
            }
            else
            {
                oTimer.Stop();
            }
        }

        private void relacionarComplmentoDePagoCanceladoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            relacionarDocumentos();
        }

        private void relacionarDocumentos()
        {
            try
            {
                if (dtgrdGeneral.SelectedRows.Count > 0)
                {
                    DataGridViewSelectedRowCollection arrSelectedRows = dtgrdGeneral.SelectedRows;
                    for (int n = arrSelectedRows.Count - 1; n >= 0; n--)
                    {
                        Encapsular oEncapsular = (Encapsular)arrSelectedRows[n].Tag;
                        string checkIdTr = arrSelectedRows[n].Cells["CHECK_ID"].Value.ToString();
                        string customerIdTr = arrSelectedRows[n].Cells["CUSTOMER_ID"].Value.ToString();
                        frmComprobanteRelacionar ofrmComprobanteRelacionar = new frmComprobanteRelacionar(checkIdTr, customerIdTr);
                        ofrmComprobanteRelacionar.Show();

                    }
                }
            }
            catch (Exception e)
            {
                ErrorFX.mostrar(e, true, true, false);
            }
        }

        private void button5_Click_1(object sender, EventArgs e)
        {
            cComprobantePago oComprobantePago = new cComprobantePago();
            oComprobantePago.cargarDetalle(INICIO.Value, this.FINAL.Value);
        }



        private void generarReporteDeNDCNoAsociadasToolStripMenuItem_Click(object sender, EventArgs e)
        {
            cargarNDCNoAsociadas();
        }

        private void cargarNDCNoAsociadas()
        {
            try
            {
                if (dtgrdGeneral.SelectedRows.Count > 0)
                {
                    dsNdcAplicada.dtNdcDataTable oRetornar = new dsNdcAplicada.dtNdcDataTable();
                    DataGridViewSelectedRowCollection arrSelectedRows = dtgrdGeneral.SelectedRows;
                    for (int n = arrSelectedRows.Count - 1; n >= 0; n--)
                    {
                        string checkId = arrSelectedRows[n].Cells["CHECK_ID"].Value.ToString();
                        string customerId = arrSelectedRows[n].Cells["CUSTOMER_ID"].Value.ToString();
                        Encapsular oEncapsular = (Encapsular)arrSelectedRows[n].Tag;
                        cComprobantePago ocComprobantePago = new cComprobantePago();
                        ocComprobantePago.validarNDCListado(checkId, customerId, oEncapsular);

                        if (!String.IsNullOrEmpty(ocComprobantePago.errorNDC))
                        {
                            oRetornar.AdddtNdcRow(customerId, checkId, ocComprobantePago.errorNDC);
                        }


                    }

                    if (oRetornar.Rows.Count > 0)
                    {

                        DataTable oDataTable = (DataTable)oRetornar;
                        ExcelFX.exportarDataTable(oDataTable, "Listado de NDC mal aplicadas");
                    }
                    else
                    {
                        MessageBox.Show("Todos los pagos estan aplicados de forma exitosa."
                            , Application.ProductName + " " + Application.ProductVersion
                            , MessageBoxButtons.OK
                            , MessageBoxIcon.Information
                            );
                    }
                }
            }
            catch (Exception e)
            {
                ErrorFX.mostrar(e, true, true, false);
            }
        }

        private void procesarComplementosAutoajustandoElMontoPagadoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            generarRecepcionPagos(true);
        }

        private void compararDepositosConComplementoDePagoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            comparaCFDI();
        }

        private void comparaCFDI()
        {
            try
            {
                if (dtgrdGeneral.SelectedRows.Count > 0)
                {
                    DataGridViewSelectedRowCollection arrSelectedRows = dtgrdGeneral.SelectedRows;
                    for (int n = arrSelectedRows.Count - 1; n >= 0; n--)
                    {
                        if (arrSelectedRows[n].Tag != null)
                        {
                            pago oPago = (pago)arrSelectedRows[n].Cells["Estado"].Tag;
                            if (oPago != null)
                            {
                                cComprobantePago oComprobantePago = new cComprobantePago();
                                decimal monto = 0;
                                if (File.Exists(oPago.xml))
                                {
                                    monto = oComprobantePago.cargarComplementoPago(oPago.xml);
                                }
                                dtgrdGeneral.Rows[arrSelectedRows[n].Index].Cells["diferenciaMonto"].Value = "";

                                if (oPago.monto != monto)
                                {
                                    decimal montoTr = 0;
                                    if (oPago.monto != null)
                                    {
                                        montoTr = (decimal)oPago.monto;
                                        string mensaje = "Monto diferente Erp: "
                                        + montoTr.ToString("C") + " CFDI: " + monto.ToString("C");

                                        dtgrdGeneral.Rows[arrSelectedRows[n].Index].ErrorText = mensaje;

                                        dtgrdGeneral.Rows[arrSelectedRows[n].Index].Cells["diferenciaMonto"].Value = mensaje;
                                    }
                                }
                            }

                        }
                    }
                    MessageBox.Show("Documentos comprobados " + dtgrdGeneral.SelectedRows.Count.ToString()
    , Application.ProductName + " " + Application.ProductVersion
    , MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
            catch (Exception e)
            {
                ErrorFX.mostrar(e, true, true, false);
            }
        }

        private void autorelacionarConElMismoPagoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            autorelacionar();
        }

        private void autorelacionar()
        {
            try
            {
                if (dtgrdGeneral.SelectedRows.Count > 0)
                {
                    DataGridViewSelectedRowCollection arrSelectedRows = dtgrdGeneral.SelectedRows;
                    for (int n = arrSelectedRows.Count - 1; n >= 0; n--)
                    {
                        if (arrSelectedRows[n].Tag != null)
                        {
                            pago oPago = (pago)arrSelectedRows[n].Cells["Estado"].Tag;
                            if (oPago != null)
                            {
                                cComprobantePago oComprobantePago = new cComprobantePago();
                                oComprobantePago.autorelacionar(oPago);
                            }
                        }
                    }
                    MessageBox.Show("Documentos autorelacionados " + dtgrdGeneral.SelectedRows.Count.ToString()
                        , Application.ProductName + " " + Application.ProductVersion
                        , MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
            catch (Exception e)
            {
                ErrorFX.mostrar(e, true, true, this.Name + " - 1604");
            }
        }

        private void cFDIAExcelToolStripMenuItem_Click(object sender, EventArgs e)
        {
            cfdiExcel();
        }

        private void cfdiExcel()
        {
            try
            {
                if (dtgrdGeneral.SelectedRows.Count > 0)
                {
                    List<string> archivos = new List<string>();
                    DataGridViewSelectedRowCollection arrSelectedRows = dtgrdGeneral.SelectedRows;
                    for (int n = arrSelectedRows.Count - 1; n >= 0; n--)
                    {
                        if (arrSelectedRows[n].Tag != null)
                        {
                            pago oPago = (pago)arrSelectedRows[n].Cells["Estado"].Tag;
                            if (oPago != null)
                            {

                                if (File.Exists(oPago.xml))
                                {
                                    archivos.Add(oPago.xml);
                                }

                            }

                        }
                    }
                    //cComprobantePago oComprobantePago = new cComprobantePago();
                    //oComprobantePago.cargarDetalle

                    MessageBox.Show("Documentos migrados a Excel " + dtgrdGeneral.SelectedRows.Count.ToString()
                    , Application.ProductName + " " + Application.ProductVersion
                    , MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
            catch (Exception e)
            {
                ErrorFX.mostrar(e, true, true, this.Name + " - 1604");
            }
        }

        private void copiarCFDISeleccionadosACarpetaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            copiarAcarpeta();
        }

        private void copiarAcarpeta()
        {
            try
            {
                if (dtgrdGeneral.SelectedRows.Count > 0)
                {
                    string directorio = "";
                    using (var fbd = new FolderBrowserDialog())
                    {
                        DialogResult result = fbd.ShowDialog();

                        if (result == DialogResult.OK && !string.IsNullOrWhiteSpace(fbd.SelectedPath))
                        {
                            directorio = fbd.SelectedPath;
                        }
                        else
                        {
                            return;
                        }
                    }

                    List<string> archivos = new List<string>();
                    DataGridViewSelectedRowCollection arrSelectedRows = dtgrdGeneral.SelectedRows;
                    for (int n = arrSelectedRows.Count - 1; n >= 0; n--)
                    {
                        if (arrSelectedRows[n].Tag != null)
                        {
                            string archivoTr = arrSelectedRows[n].Cells["XML"].Value.ToString();
                            
                            if (File.Exists(archivoTr))
                            {
                                string rutaNueva = directorio + "//" + Path.GetFileName(archivoTr);

                                File.Copy(archivoTr, rutaNueva,true);
                            }
                               
                        }
                    }
                    MessageBox.Show("Documentos copiados " + dtgrdGeneral.SelectedRows.Count.ToString()
                    , Application.ProductName + " " + Application.ProductVersion
                    , MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
            catch (Exception e)
            {
                ErrorFX.mostrar(e, true, true, this.Name + " - 1604");
            }
        }

        private void generarSinRelacionarNDCToolStripMenuItem_Click(object sender, EventArgs e)
        {
            generarRecepcionPagos(false, true);
        }

        private void GenerarComprobanteConTCDeCambioToolStripMenuItem_Click(object sender, EventArgs e)
        {
            generarRecepcionPagos(false, false, false);
        }

        private void rToolStripMenuItem_Click(object sender, EventArgs e)
        {
            reactivarComprobantePago();
        }

        private void reactivarComprobantePago()
        {
            try
            {
                if (dtgrdGeneral.SelectedRows.Count > 0)
                {
                    DataGridViewSelectedRowCollection arrSelectedRows = dtgrdGeneral.SelectedRows;
                    for (int n = arrSelectedRows.Count - 1; n >= 0; n--)
                    {
                        if (arrSelectedRows[n].Cells["Id"].Value!=null)
                        {
                            string IdTr = arrSelectedRows[n].Cells["Id"].Value.ToString();
                            if (Globales.IsNumeric(IdTr))
                            {
                                cComprobantePago.Eliminar(int.Parse(IdTr));
                            }
                            
                        }
                    }
                    MessageBox.Show("Documentos activados " + dtgrdGeneral.SelectedRows.Count.ToString()
                    , Application.ProductName + " " + Application.ProductVersion
                    , MessageBoxButtons.OK, MessageBoxIcon.Information);
                    actualizar();
                }
                
            }
            catch (Exception e)
            {
                ErrorFX.mostrar(e, true, true, this.Name + " - 1830");
            }
        }

        private void procesarConRegeneraciónDeDatosToolStripMenuItem_Click(object sender, EventArgs e)
        {
            procesarRegeneracion();
        }

        private void procesarRegeneracion()
        {
            generarRecepcionPagos(false, false, true, true);
        }

        private void migrarCFDIToolStripMenuItem_Click(object sender, EventArgs e)
        {
            MigrarCFDI();
        }

        private void MigrarCFDI()
        {
            try
            {
                cEMPRESA ocEMPRESA = ENTITY_ID.SelectedItem as cEMPRESA;
                if (ocEMPRESA==null)
                {
                    MessageBox.Show("Debe, seleccionar alguna empresa");
                    return;
                }
                cCONEXCION oData_ERPp = new cCONEXCION(ocEMPRESA.TIPO, ocEMPRESA.SERVIDOR, ocEMPRESA.BD, ocEMPRESA.USUARIO_BD, ocEMPRESA.PASSWORD_BD);
                Encapsular oEncapsular = new Encapsular();
                oEncapsular.oData_ERP = oData_ERPp;
                oEncapsular.ocEMPRESA = ocEMPRESA;
                Formularios.frmMigrararCFDI O = new Formularios.frmMigrararCFDI(oEncapsular,oData.sConn);
                O.Show();
            }
            catch (Exception e)
            {
                ErrorFX.mostrar(e, true, true, false);
            }
        }

        private void nuevoComprobanteDePagoManualToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmComprobanteCambioForma Objeto = new frmComprobanteCambioForma();
            Objeto.Show();
        }
    }
}
