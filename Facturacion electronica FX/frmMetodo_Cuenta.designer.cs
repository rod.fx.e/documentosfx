﻿namespace FE_FX
{
    partial class frmMetodo_Cuenta
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmMetodo_Cuenta));
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.toolStripStatusLabel1 = new System.Windows.Forms.ToolStripStatusLabel();
            this.button3 = new System.Windows.Forms.Button();
            this.ajax_loader = new System.Windows.Forms.PictureBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.CTA_BANCO = new System.Windows.Forms.TextBox();
            this.METODO_DE_PAGO = new System.Windows.Forms.ComboBox();
            this.FORMA_DE_PAGO = new System.Windows.Forms.ComboBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.USO_CFDI = new System.Windows.Forms.ComboBox();
            this.label4 = new System.Windows.Forms.Label();
            this.subdivision = new System.Windows.Forms.TextBox();
            this.Transporte = new System.Windows.Forms.ComboBox();
            this.Transportista = new System.Windows.Forms.ComboBox();
            this.label8 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.NombreRemitenteDestinatario = new System.Windows.Forms.TextBox();
            this.label13 = new System.Windows.Forms.Label();
            this.RFCRemitenteDestinatario = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.TotalDistRec = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.RegimenFiscal = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.tabPage4 = new System.Windows.Forms.TabPage();
            this.InformacionGlobalPeriodicidad = new System.Windows.Forms.ComboBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.InformacionGlobalAnio = new System.Windows.Forms.TextBox();
            this.label15 = new System.Windows.Forms.Label();
            this.InformacionGlobalMes = new System.Windows.Forms.ComboBox();
            this.label16 = new System.Windows.Forms.Label();
            this.Exportacion = new System.Windows.Forms.ComboBox();
            this.statusStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ajax_loader)).BeginInit();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.tabPage2.SuspendLayout();
            this.tabPage3.SuspendLayout();
            this.tabPage4.SuspendLayout();
            this.SuspendLayout();
            // 
            // statusStrip1
            // 
            this.statusStrip1.Dock = System.Windows.Forms.DockStyle.Top;
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripStatusLabel1});
            this.statusStrip1.Location = new System.Drawing.Point(0, 0);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(585, 22);
            this.statusStrip1.TabIndex = 37;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // toolStripStatusLabel1
            // 
            this.toolStripStatusLabel1.Name = "toolStripStatusLabel1";
            this.toolStripStatusLabel1.Size = new System.Drawing.Size(42, 17);
            this.toolStripStatusLabel1.Text = "Estado";
            // 
            // button3
            // 
            this.button3.BackColor = System.Drawing.SystemColors.Control;
            this.button3.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button3.Image = global::FE_FX.Properties.Resources.Guardar;
            this.button3.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button3.Location = new System.Drawing.Point(12, 25);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(83, 23);
            this.button3.TabIndex = 0;
            this.button3.Text = "Guardar";
            this.button3.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.button3.UseVisualStyleBackColor = false;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // ajax_loader
            // 
            this.ajax_loader.AccessibleDescription = "";
            this.ajax_loader.Image = global::FE_FX.Properties.Resources.ajax_loader;
            this.ajax_loader.Location = new System.Drawing.Point(101, 26);
            this.ajax_loader.Name = "ajax_loader";
            this.ajax_loader.Size = new System.Drawing.Size(58, 22);
            this.ajax_loader.TabIndex = 38;
            this.ajax_loader.TabStop = false;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(7, 10);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(86, 13);
            this.label1.TabIndex = 3;
            this.label1.Text = "Método de Pago";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(7, 216);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(88, 13);
            this.label2.TabIndex = 9;
            this.label2.Text = "Cuenta Bancaría";
            // 
            // CTA_BANCO
            // 
            this.CTA_BANCO.Location = new System.Drawing.Point(99, 213);
            this.CTA_BANCO.Name = "CTA_BANCO";
            this.CTA_BANCO.Size = new System.Drawing.Size(58, 20);
            this.CTA_BANCO.TabIndex = 5;
            // 
            // METODO_DE_PAGO
            // 
            this.METODO_DE_PAGO.FormattingEnabled = true;
            this.METODO_DE_PAGO.Items.AddRange(new object[] {
            "01 Efectivo",
            "02 Cheque nominativo",
            "03 Transferencia electrónica de fondos",
            "04 Tarjetas de crédito",
            "05 Monederos electrónicos",
            "06 Dinero electrónico",
            "07 Tarjetas digitales",
            "08 Vales de despensa",
            "28 Tarjeta de Débito",
            "29 Tarjeta de Servicio",
            "99 Otros",
            "NA"});
            this.METODO_DE_PAGO.Location = new System.Drawing.Point(99, 6);
            this.METODO_DE_PAGO.Name = "METODO_DE_PAGO";
            this.METODO_DE_PAGO.Size = new System.Drawing.Size(393, 21);
            this.METODO_DE_PAGO.TabIndex = 2;
            // 
            // FORMA_DE_PAGO
            // 
            this.FORMA_DE_PAGO.FormattingEnabled = true;
            this.FORMA_DE_PAGO.Items.AddRange(new object[] {
            "Pago en una sola exhibición",
            "Pago en parcialidades",
            "Pago a credito"});
            this.FORMA_DE_PAGO.Location = new System.Drawing.Point(99, 33);
            this.FORMA_DE_PAGO.Name = "FORMA_DE_PAGO";
            this.FORMA_DE_PAGO.Size = new System.Drawing.Size(393, 21);
            this.FORMA_DE_PAGO.TabIndex = 3;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(7, 36);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(79, 13);
            this.label3.TabIndex = 5;
            this.label3.Text = "Forma de Pago";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(7, 63);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(70, 13);
            this.label7.TabIndex = 7;
            this.label7.Text = "Uso del CFDI";
            // 
            // USO_CFDI
            // 
            this.USO_CFDI.FormattingEnabled = true;
            this.USO_CFDI.Items.AddRange(new object[] {
            "Pago en una sola exhibición",
            "Pago en parcialidades",
            "Pago a credito"});
            this.USO_CFDI.Location = new System.Drawing.Point(99, 60);
            this.USO_CFDI.Name = "USO_CFDI";
            this.USO_CFDI.Size = new System.Drawing.Size(393, 21);
            this.USO_CFDI.TabIndex = 4;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(16, 15);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(61, 13);
            this.label4.TabIndex = 12;
            this.label4.Text = "Subdivisión";
            // 
            // subdivision
            // 
            this.subdivision.Location = new System.Drawing.Point(97, 12);
            this.subdivision.Name = "subdivision";
            this.subdivision.Size = new System.Drawing.Size(81, 20);
            this.subdivision.TabIndex = 6;
            this.subdivision.Text = "0";
            // 
            // Transporte
            // 
            this.Transporte.FormattingEnabled = true;
            this.Transporte.Items.AddRange(new object[] {
            "Pago en una sola exhibición",
            "Pago en parcialidades",
            "Pago a credito"});
            this.Transporte.Location = new System.Drawing.Point(84, 42);
            this.Transporte.Name = "Transporte";
            this.Transporte.Size = new System.Drawing.Size(398, 21);
            this.Transporte.TabIndex = 1;
            // 
            // Transportista
            // 
            this.Transportista.FormattingEnabled = true;
            this.Transportista.Items.AddRange(new object[] {
            "Pago en una sola exhibición",
            "Pago en parcialidades",
            "Pago a credito"});
            this.Transportista.Location = new System.Drawing.Point(84, 15);
            this.Transportista.Name = "Transportista";
            this.Transportista.Size = new System.Drawing.Size(398, 21);
            this.Transportista.TabIndex = 0;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(15, 45);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(58, 13);
            this.label8.TabIndex = 2;
            this.label8.Text = "Transporte";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(12, 18);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(68, 13);
            this.label6.TabIndex = 0;
            this.label6.Text = "Transportista";
            // 
            // NombreRemitenteDestinatario
            // 
            this.NombreRemitenteDestinatario.Location = new System.Drawing.Point(138, 121);
            this.NombreRemitenteDestinatario.Name = "NombreRemitenteDestinatario";
            this.NombreRemitenteDestinatario.Size = new System.Drawing.Size(344, 20);
            this.NombreRemitenteDestinatario.TabIndex = 3;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(29, 124);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(103, 13);
            this.label13.TabIndex = 12;
            this.label13.Text = "Nombre Destinatario";
            // 
            // RFCRemitenteDestinatario
            // 
            this.RFCRemitenteDestinatario.Location = new System.Drawing.Point(138, 95);
            this.RFCRemitenteDestinatario.Name = "RFCRemitenteDestinatario";
            this.RFCRemitenteDestinatario.Size = new System.Drawing.Size(187, 20);
            this.RFCRemitenteDestinatario.TabIndex = 3;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(45, 98);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(87, 13);
            this.label12.TabIndex = 12;
            this.label12.Text = "RFC Destinatario";
            // 
            // TotalDistRec
            // 
            this.TotalDistRec.Location = new System.Drawing.Point(138, 69);
            this.TotalDistRec.Name = "TotalDistRec";
            this.TotalDistRec.Size = new System.Drawing.Size(58, 20);
            this.TotalDistRec.TabIndex = 2;
            this.TotalDistRec.Text = "0";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(12, 72);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(120, 13);
            this.label11.TabIndex = 12;
            this.label11.Text = "Total distancia recorrida";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(7, 90);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(79, 13);
            this.label9.TabIndex = 1;
            this.label9.Text = "Regimen Fiscal";
            // 
            // RegimenFiscal
            // 
            this.RegimenFiscal.Location = new System.Drawing.Point(99, 87);
            this.RegimenFiscal.Name = "RegimenFiscal";
            this.RegimenFiscal.Size = new System.Drawing.Size(58, 20);
            this.RegimenFiscal.TabIndex = 1;
            // 
            // label10
            // 
            this.label10.Location = new System.Drawing.Point(7, 116);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(481, 94);
            this.label10.TabIndex = 9;
            this.label10.Text = resources.GetString("label10.Text");
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage3);
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Controls.Add(this.tabPage4);
            this.tabControl1.Location = new System.Drawing.Point(12, 54);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(558, 292);
            this.tabControl1.TabIndex = 0;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.Transporte);
            this.tabPage1.Controls.Add(this.Transportista);
            this.tabPage1.Controls.Add(this.label11);
            this.tabPage1.Controls.Add(this.label8);
            this.tabPage1.Controls.Add(this.TotalDistRec);
            this.tabPage1.Controls.Add(this.label6);
            this.tabPage1.Controls.Add(this.label12);
            this.tabPage1.Controls.Add(this.NombreRemitenteDestinatario);
            this.tabPage1.Controls.Add(this.RFCRemitenteDestinatario);
            this.tabPage1.Controls.Add(this.label13);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(550, 266);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Carta Porte";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.InformacionGlobalAnio);
            this.tabPage2.Controls.Add(this.InformacionGlobalMes);
            this.tabPage2.Controls.Add(this.label15);
            this.tabPage2.Controls.Add(this.InformacionGlobalPeriodicidad);
            this.tabPage2.Controls.Add(this.label5);
            this.tabPage2.Controls.Add(this.label14);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(550, 266);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Información Global";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // tabPage3
            // 
            this.tabPage3.Controls.Add(this.RegimenFiscal);
            this.tabPage3.Controls.Add(this.label3);
            this.tabPage3.Controls.Add(this.METODO_DE_PAGO);
            this.tabPage3.Controls.Add(this.label1);
            this.tabPage3.Controls.Add(this.USO_CFDI);
            this.tabPage3.Controls.Add(this.CTA_BANCO);
            this.tabPage3.Controls.Add(this.label9);
            this.tabPage3.Controls.Add(this.FORMA_DE_PAGO);
            this.tabPage3.Controls.Add(this.label7);
            this.tabPage3.Controls.Add(this.label2);
            this.tabPage3.Controls.Add(this.label10);
            this.tabPage3.Location = new System.Drawing.Point(4, 22);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage3.Size = new System.Drawing.Size(550, 266);
            this.tabPage3.TabIndex = 2;
            this.tabPage3.Text = "General";
            this.tabPage3.UseVisualStyleBackColor = true;
            // 
            // tabPage4
            // 
            this.tabPage4.Controls.Add(this.Exportacion);
            this.tabPage4.Controls.Add(this.label16);
            this.tabPage4.Controls.Add(this.subdivision);
            this.tabPage4.Controls.Add(this.label4);
            this.tabPage4.Location = new System.Drawing.Point(4, 22);
            this.tabPage4.Name = "tabPage4";
            this.tabPage4.Size = new System.Drawing.Size(550, 266);
            this.tabPage4.TabIndex = 3;
            this.tabPage4.Text = "Complemento de Comercio Exterior";
            this.tabPage4.UseVisualStyleBackColor = true;
            // 
            // InformacionGlobalPeriodicidad
            // 
            this.InformacionGlobalPeriodicidad.FormattingEnabled = true;
            this.InformacionGlobalPeriodicidad.Items.AddRange(new object[] {
            "Pago en una sola exhibición",
            "Pago en parcialidades",
            "Pago a credito"});
            this.InformacionGlobalPeriodicidad.Location = new System.Drawing.Point(85, 59);
            this.InformacionGlobalPeriodicidad.Name = "InformacionGlobalPeriodicidad";
            this.InformacionGlobalPeriodicidad.Size = new System.Drawing.Size(385, 21);
            this.InformacionGlobalPeriodicidad.TabIndex = 5;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(14, 62);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(65, 13);
            this.label5.TabIndex = 4;
            this.label5.Text = "Periodicidad";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(52, 9);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(26, 13);
            this.label14.TabIndex = 0;
            this.label14.Text = "Año";
            // 
            // InformacionGlobalAnio
            // 
            this.InformacionGlobalAnio.Location = new System.Drawing.Point(85, 6);
            this.InformacionGlobalAnio.Name = "InformacionGlobalAnio";
            this.InformacionGlobalAnio.Size = new System.Drawing.Size(58, 20);
            this.InformacionGlobalAnio.TabIndex = 1;
            this.InformacionGlobalAnio.Text = "0";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(52, 35);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(27, 13);
            this.label15.TabIndex = 2;
            this.label15.Text = "Mes";
            // 
            // InformacionGlobalMes
            // 
            this.InformacionGlobalMes.FormattingEnabled = true;
            this.InformacionGlobalMes.Items.AddRange(new object[] {
            "Pago en una sola exhibición",
            "Pago en parcialidades",
            "Pago a credito"});
            this.InformacionGlobalMes.Location = new System.Drawing.Point(85, 32);
            this.InformacionGlobalMes.Name = "InformacionGlobalMes";
            this.InformacionGlobalMes.Size = new System.Drawing.Size(120, 21);
            this.InformacionGlobalMes.TabIndex = 3;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(16, 41);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(63, 13);
            this.label16.TabIndex = 12;
            this.label16.Text = "Exportación";
            // 
            // Exportacion
            // 
            this.Exportacion.FormattingEnabled = true;
            this.Exportacion.Items.AddRange(new object[] {
            "Pago en una sola exhibición",
            "Pago en parcialidades",
            "Pago a credito"});
            this.Exportacion.Location = new System.Drawing.Point(97, 38);
            this.Exportacion.Name = "Exportacion";
            this.Exportacion.Size = new System.Drawing.Size(437, 21);
            this.Exportacion.TabIndex = 13;
            // 
            // frmMetodo_Cuenta
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(585, 356);
            this.Controls.Add(this.tabControl1);
            this.Controls.Add(this.button3);
            this.Controls.Add(this.ajax_loader);
            this.Controls.Add(this.statusStrip1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.KeyPreview = true;
            this.MinimizeBox = false;
            this.Name = "frmMetodo_Cuenta";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Cambios";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frmUsuario_FormClosing);
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ajax_loader)).EndInit();
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage1.PerformLayout();
            this.tabPage2.ResumeLayout(false);
            this.tabPage2.PerformLayout();
            this.tabPage3.ResumeLayout(false);
            this.tabPage3.PerformLayout();
            this.tabPage4.ResumeLayout(false);
            this.tabPage4.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel1;
        private System.Windows.Forms.PictureBox ajax_loader;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox CTA_BANCO;
        private System.Windows.Forms.ComboBox METODO_DE_PAGO;
        private System.Windows.Forms.ComboBox FORMA_DE_PAGO;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ComboBox USO_CFDI;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox subdivision;
        private System.Windows.Forms.ComboBox Transporte;
        private System.Windows.Forms.ComboBox Transportista;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox RegimenFiscal;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox TotalDistRec;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TextBox RFCRemitenteDestinatario;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.TextBox NombreRemitenteDestinatario;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage3;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.TabPage tabPage4;
        private System.Windows.Forms.ComboBox InformacionGlobalPeriodicidad;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.TextBox InformacionGlobalAnio;
        private System.Windows.Forms.ComboBox InformacionGlobalMes;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.ComboBox Exportacion;
    }
}