﻿using System;
using System.Collections.Generic;

using System.Text;
using System.Data;
using System.Windows;
using System.Windows.Forms;
using Generales;
using ModeloDocumentosFX;

namespace FE_FX
{
    class cADDENDA
    {
        private string sSQL = "";
        public string ROW_ID { get; set; }
        public string ID { get; set; }
        public string EJECUTABLE { get; set; }
        public string CUSTOMER_ID { get; set; }
        public string TIPO { get; set; }

        public string ACTIVO { get; set; }

        private cCONEXCION oData = new cCONEXCION("");

        public cADDENDA()
        {
            ModeloDocumentosFXContainer dbContext = new ModeloDocumentosFXContainer();
            try
            {
                string conection = dbContext.Database.Connection.ConnectionString;
                oData = new cCONEXCION(conection);
            }
            catch (Exception err)
            {
                ErrorFX.mostrar(err, false, true, "cCERTIFICADO - 71 - ");
            }
            limpiar();
        }

        public void limpiar()
        {
            ROW_ID = "";
            ID = "";
            EJECUTABLE = "";
            CUSTOMER_ID = "";
            TIPO = "";
            ACTIVO = "";
        }

        public List<cADDENDA> todos(string BUSQUEDA)
        {

            List<cADDENDA> lista = new List<cADDENDA>();

            sSQL  = " SELECT * ";
            sSQL += " FROM ADDENDA ";
            sSQL += " WHERE ID LIKE '%" + BUSQUEDA + "%' ";
            DataTable oDataTable = oData.EjecutarConsulta(sSQL);
            foreach (DataRow oDataRow in oDataTable.Rows)
            {
                lista.Add(new cADDENDA()
                {
                    ROW_ID = oDataRow["ROW_ID"].ToString()
                   ,ID = oDataRow["ID"].ToString()
                   ,EJECUTABLE = oDataRow["EJECUTABLE"].ToString()
                   ,CUSTOMER_ID = oDataRow["CUSTOMER_ID"].ToString()
                   ,
                    TIPO = oDataRow["TIPO"].ToString()
,
                    ACTIVO = oDataRow["ACTIVO"].ToString()
                });
            }
            return lista;
        }

        public cADDENDA buscar_tipo(string BUSQUEDA,string TIPO)
        {

            cADDENDA lista = new cADDENDA();

            sSQL = " SELECT TOP 1 * ";
            sSQL += " FROM ADDENDA ";
            sSQL += " WHERE ID LIKE '%" + BUSQUEDA + "%' ";
            sSQL += " AND TIPO='" + TIPO + "' ";
            DataTable oDataTable = oData.EjecutarConsulta(sSQL);
            foreach (DataRow oDataRow in oDataTable.Rows)
            {
                lista.ROW_ID = oDataRow["ROW_ID"].ToString();
                lista. ID = oDataRow["ID"].ToString();
                lista.EJECUTABLE = oDataRow["EJECUTABLE"].ToString();
                lista.CUSTOMER_ID = oDataRow["CUSTOMER_ID"].ToString();
                lista.TIPO = oDataRow["TIPO"].ToString();

            }
            return lista;
        }


        public bool cargar(string ROW_IDp)
        {
            sSQL  = " SELECT * ";
            sSQL += " FROM ADDENDA ";
            sSQL += " WHERE ROW_ID=" + ROW_IDp + " ";
            DataTable oDataTable = oData.EjecutarConsulta(sSQL);
            if (oDataTable != null)
            {
                foreach (DataRow oDataRow in oDataTable.Rows)
                {
                    //Añadir el Registro
                    ROW_ID = oDataRow["ROW_ID"].ToString();
                    ID = oDataRow["ID"].ToString();
                    EJECUTABLE = oDataRow["EJECUTABLE"].ToString();
                    CUSTOMER_ID = oDataRow["CUSTOMER_ID"].ToString();
                    TIPO = oDataRow["TIPO"].ToString();
                    ACTIVO = oDataRow["ACTIVO"].ToString();
                    return true;
                }
            }
            return false;
        }

        public bool cargar_id(string IDp)
        {
            sSQL = " SELECT * ";
            sSQL += " FROM ADDENDA ";
            sSQL += " WHERE ID='" + IDp + "' ";
            DataTable oDataTable = oData.EjecutarConsulta(sSQL);
            if (oDataTable != null)
            {
                foreach (DataRow oDataRow in oDataTable.Rows)
                {
                    //Añadir el Registro
                    ROW_ID = oDataRow["ROW_ID"].ToString();
                    ID = oDataRow["ID"].ToString();
                    EJECUTABLE = oDataRow["EJECUTABLE"].ToString();
                    CUSTOMER_ID = oDataRow["CUSTOMER_ID"].ToString();
                    TIPO = oDataRow["TIPO"].ToString();
                    ACTIVO = oDataRow["ACTIVO"].ToString();
                    return true;
                }
            }
            return false;
        }

        public bool cargar_CUSTOMER_ID(string CUSTOMER_IDp)
        {
            sSQL = " SELECT * ";
            sSQL += " FROM ADDENDA ";
            sSQL += " WHERE CUSTOMER_ID='" + CUSTOMER_IDp + "'";
            DataTable oDataTable = oData.EjecutarConsulta(sSQL);
            if (oDataTable != null)
            {
                foreach (DataRow oDataRow in oDataTable.Rows)
                {
                    //Añadir el Registro
                    ROW_ID = oDataRow["ROW_ID"].ToString();
                    ID = oDataRow["ID"].ToString();
                    EJECUTABLE = oDataRow["EJECUTABLE"].ToString();
                    CUSTOMER_ID = oDataRow["CUSTOMER_ID"].ToString();
                    TIPO = oDataRow["TIPO"].ToString();
                    ACTIVO = oDataRow["ACTIVO"].ToString();
                    return true;
                }
            }
            return false;
        }


        public bool guardar()
        {
            if (ID.Trim() == "")
            {
                MessageBox.Show("El Nombre es un campo requerido.", Application.ProductName + "-" + Application.ProductVersion, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return false;
            }

            if (EJECUTABLE.Trim() == "")
            {
                MessageBox.Show("El Reporte es un campo requerido.", Application.ProductName + "-" + Application.ProductVersion, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return false;
            }

            //if (TIPO.Trim() == "")
            //{
            //    MessageBox.Show("El Tipo es un campo requerido.", Application.ProductName + "-" + Application.ProductVersion, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            //    return false;
            //}

            if (ROW_ID == "")
            {

                sSQL = " INSERT INTO ADDENDA ";
                sSQL += " ( ";
                sSQL += " ID, EJECUTABLE, CUSTOMER_ID, TIPO";
                sSQL += ", ACTIVO";
                sSQL += " )";
                sSQL += " VALUES ";
                sSQL += "(";
                sSQL += " '" + ID + "','" + EJECUTABLE + "','" + CUSTOMER_ID + "','" + TIPO + "' ";
                sSQL += ", '" + ACTIVO + "'";
                sSQL += ")";
                DataTable oDataTable = oData.EjecutarConsulta(sSQL);
                if (oDataTable!=null)
                {

                    sSQL = "SELECT TOP 1 MAX(ROW_ID) as ROW_ID FROM ADDENDA";
                    oDataTable = oData.EjecutarConsulta(sSQL);
                    if (oDataTable != null)
                    {
                        foreach (DataRow oDataRow in oDataTable.Rows)
                        {
                            ROW_ID = oDataRow["ROW_ID"].ToString();
                        }
                    }
                }

            }
            else
            {
                sSQL = " UPDATE ADDENDA ";
                sSQL += " SET ID='" + ID + "'";
                sSQL += ",EJECUTABLE='" + EJECUTABLE + "',CUSTOMER_ID='" + CUSTOMER_ID + "' ,TIPO='" + TIPO + "' ";
                sSQL += ",ACTIVO='" + ACTIVO + "'";
                sSQL += " WHERE ROW_ID=" + ROW_ID + " ";
                oData.EjecutarConsulta(sSQL);

            }
            return true;

        }

        public bool eliminar(string ROW_IDp)
        {
            if (ROW_IDp != "")
            {
                sSQL = "DELETE FROM ADDENDA WHERE ROW_ID=" + ROW_IDp;
                oData.EjecutarConsulta(sSQL);
                return true;
            }
            return false;

        }
    }
}
