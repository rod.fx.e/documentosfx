﻿using Generales;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FE_FX.Clases
{
    public class IngresoImportacionLineaExportacion
    {
        public IngresoImportacionLineaExportacion()
        {
        }

        public string ClaveProdServ { get; set; }
        public string Descripcion { get; set; }
        public decimal Cantidad { get; set; }
        public string ClaveUnidad { get; set; }
        public string Unidad { get; set; }
        public string Dimensiones { get; set; }
        public string MaterialPeligroso { get; set; }
        public string CveMaterialPeligroso { get; set; }
        public string Embalaje { get; set; }
        public string DescripEmbalaje { get; set; }
        public decimal PesoEnKg { get; set; }
        public decimal ValorMercancia { get; set; }
        public string Moneda { get; set; }
        public string FraccionArancelaria { get; set; }
        public string UUID { get; set; }
        public string Pedimento { get; set; }
        public string Sello { get; set; }
        public IngresoImportacionLineaExportacion(string claveProdServ, string descripcion, decimal cantidad, string claveUnidad, string unidad, string dimensiones, string materialPeligroso, string cveMaterialPeligroso, string embalaje, string descripEmbalaje, decimal pesoEnKg, decimal valorMercancia, string moneda, string fraccionArancelaria, string uUID, string pedimento)
        {
            ClaveProdServ = claveProdServ;
            Descripcion = descripcion;
            Cantidad = cantidad;
            ClaveUnidad = claveUnidad;
            Unidad = unidad;
            Dimensiones = dimensiones;
            MaterialPeligroso = materialPeligroso;
            CveMaterialPeligroso = cveMaterialPeligroso;
            Embalaje = embalaje;
            DescripEmbalaje = descripEmbalaje;
            PesoEnKg = pesoEnKg;
            ValorMercancia = valorMercancia;
            Moneda = moneda;
            FraccionArancelaria = fraccionArancelaria;
            UUID = uUID;
            Pedimento = pedimento;
        }


        public List<IngresoImportacionLineaExportacion> Obtener(TipoCartaPorte tipoCartaPorte, List<string> facturas)
        {
            List<IngresoImportacionLineaExportacion> lista = new List<IngresoImportacionLineaExportacion>();
            try
            {
                if (tipoCartaPorte == TipoCartaPorte.Embarque)
                {
                    lista = ObtenerEmbarque(facturas);
                }

                return lista;
            }
            catch (Exception error)
            {
                string mensaje = "";
                mensaje = error.Message.ToString();
                if (error.InnerException != null)
                {
                    mensaje += " " + error.InnerException.Message.ToString();
                }
                throw new Exception(mensaje);
            }
        }

        private List<IngresoImportacionLineaExportacion> ObtenerEmbarque(List<string> facturas)
        {

            List<IngresoImportacionLineaExportacion> lista = new List<IngresoImportacionLineaExportacion>();
            
            try
            {
                
                string Sql = @"
                    SELECT
                    rl.INVOICE_ID,rl.QTY, rl.REFERENCE, rl.AMOUNT, r.CURRENCY_ID, p.PRODUCT_CODE, p.ID
                    , COALESCE(col.SELLING_UM, p.STOCK_UM) as UM
                    , p.HTS_CODE as FraccionArancelaria
                    , COALESCE(p.WEIGHT,0) as WEIGHT, p.WEIGHT_UM
                    , p.USER_6
                    from FIMA..RECEIVABLE_LINE rl
                    inner join FIMA..CUST_ORDER_LINE col ON col.CUST_ORDER_ID=rl.CUST_ORDER_ID AND col.LINE_NO=rl.CUST_ORDER_LINE_NO
                    inner join FIMA..PART p ON p.ID=col.PART_ID
                    inner join FIMA..RECEIVABLE r ON r.INVOICE_ID=rl.INVOICE_ID
                    WHERE rl.INVOICE_ID in ('" + string.Join("','", facturas) + @"')
                    UNION ALL
                    SELECT
                    rl.INVOICE_ID,rl.QTY, rl.REFERENCE, rl.AMOUNT, r.CURRENCY_ID, p.PRODUCT_CODE, p.ID
                    , COALESCE(col.SELLING_UM, p.STOCK_UM) as UM
                    , p.HTS_CODE as FraccionArancelaria
                    , COALESCE(p.WEIGHT,0) as WEIGHT, p.WEIGHT_UM
                    , p.USER_6
                    from FIMA2021..RECEIVABLE_LINE rl
                    inner join FIMA2021..CUST_ORDER_LINE col ON col.CUST_ORDER_ID=rl.CUST_ORDER_ID AND col.LINE_NO=rl.CUST_ORDER_LINE_NO
                    inner join FIMA2021..PART p ON p.ID=col.PART_ID
                    inner join FIMA2021..RECEIVABLE r ON r.INVOICE_ID=rl.INVOICE_ID
                    WHERE rl.INVOICE_ID in ('" + string.Join("','", facturas) + @"')
                ";

                ModeloDocumentosFX.ModeloDocumentosFXContainer dbContextERP = new ModeloDocumentosFX.ModeloDocumentosFXContainer();
                string conection = dbContextERP.Database.Connection.ConnectionString;
                cCONEXCION OData = new cCONEXCION(conection);
                DataTable ODataTableLista = OData.EjecutarConsulta(Sql);
                foreach (System.Data.DataRow Linea in ODataTableLista.Rows)
                {
                    IngresoImportacionLineaExportacion OIngresoImportacionLineaLocal = new IngresoImportacionLineaExportacion();
                    OIngresoImportacionLineaLocal.Cantidad = decimal.Parse(Linea["QTY"].ToString());
                    OIngresoImportacionLineaLocal.Descripcion = Linea["USER_6"].ToString();
                    OIngresoImportacionLineaLocal.ValorMercancia = decimal.Parse(Linea["AMOUNT"].ToString());
                    OIngresoImportacionLineaLocal.Moneda = Linea["CURRENCY_ID"].ToString();
                    if (OIngresoImportacionLineaLocal.Moneda.Equals("DLLS"))
                    {
                        OIngresoImportacionLineaLocal.Moneda = "USD";
                    }
                    else
                    {
                        OIngresoImportacionLineaLocal.Moneda = "MXN";
                    }
                    decimal Weight = decimal.Parse(Linea["WEIGHT"].ToString());
                    OIngresoImportacionLineaLocal.PesoEnKg = Weight;
                    if (Weight != 0)
                    {
                        if (Linea["WEIGHT_UM"].ToString() == "LB")
                        {
                            OIngresoImportacionLineaLocal.PesoEnKg = Weight * decimal.Parse("0.453592");
                        }
                    }
                    OIngresoImportacionLineaLocal.PesoEnKg = OIngresoImportacionLineaLocal.PesoEnKg * OIngresoImportacionLineaLocal.Cantidad;
                    OIngresoImportacionLineaLocal.ClaveProdServ = ObtenerClaveProdServ(Linea["PRODUCT_CODE"].ToString());
                    OIngresoImportacionLineaLocal.ClaveUnidad = ObtenerClaveUnidad(Linea["UM"].ToString());
                    OIngresoImportacionLineaLocal.Unidad = Linea["UM"].ToString();
                    OIngresoImportacionLineaLocal.FraccionArancelaria = Linea["FraccionArancelaria"].ToString();
                    OIngresoImportacionLineaLocal.UUID = ObtenerUUID(Linea["INVOICE_ID"].ToString());
                    lista.Add(OIngresoImportacionLineaLocal);
                }

                return lista;
            }
            catch (Exception error)
            {
                string mensaje = "";
                mensaje = error.Message.ToString();
                if (error.InnerException != null)
                {
                    mensaje += " " + error.InnerException.Message.ToString();
                }
                throw new Exception(mensaje);
            }
        }
        private string ObtenerUUID(string invoiceId)
        {
            try
            {
                string Sql = @"
                    SELECT TOP 1 [UUID]
                    FROM [APP]..[VMX_FE]
                    WHERE INVOICE_ID='" + invoiceId + @"'
                ";
                List<string> facturas = new List<string>();
                ModeloDocumentosFX.ModeloDocumentosFXContainer dbContext = new ModeloDocumentosFX.ModeloDocumentosFXContainer();
                string conection = dbContext.Database.Connection.ConnectionString;
                cCONEXCION OData = new cCONEXCION(conection);
                DataTable ODataTable = OData.EjecutarConsulta(Sql);
                foreach (System.Data.DataRow Linea in ODataTable.Rows)
                {
                    return Linea["UUID"].ToString();
                }
                return string.Empty;
            }
            catch (Exception error)
            {
                string mensaje = "";
                mensaje = error.Message.ToString();
                if (error.InnerException != null)
                {
                    mensaje += " " + error.InnerException.Message.ToString();
                }
                throw new Exception(mensaje);
            }
        }

        private string ObtenerClaveUnidad(string um)
        {
            try
            {
                string Sql = @"
                    SELECT TOP (1000) [Id]
                    ,[UnidadMedida]
                    ,[SAT]
                    ,[CE]
                    ,[FactorCE]
                    FROM [configuracionUMSet]
                    WHERE UnidadMedida='" + um + @"'
                ";
                List<string> facturas = new List<string>();
                ModeloDocumentosFX.ModeloDocumentosFXContainer dbContext = new ModeloDocumentosFX.ModeloDocumentosFXContainer();
                string conection = dbContext.Database.Connection.ConnectionString;
                cCONEXCION OData = new cCONEXCION(conection);
                DataTable ODataTable = OData.EjecutarConsulta(Sql);
                foreach (System.Data.DataRow Linea in ODataTable.Rows)
                {
                    return Linea["SAT"].ToString();
                }
                return string.Empty;
            }
            catch (Exception error)
            {
                string mensaje = "";
                mensaje = error.Message.ToString();
                if (error.InnerException != null)
                {
                    mensaje += " " + error.InnerException.Message.ToString();
                }
                throw new Exception(mensaje);
            }
        }

        private string ObtenerClaveProdServ(string productCode)
        {
            try
            {
                string Sql = @"
                    SELECT TOP (1000) [Id]
                      ,[fechaCreacion]
                      ,[producto]
                      ,[clasificacionSAT]
                      ,[unidadSAT]
                    FROM [documentosfx]..[configuracionProductoSet]
                    WHERE producto='" + productCode + @"'
                ";
                List<string> facturas = new List<string>();
                ModeloDocumentosFX.ModeloDocumentosFXContainer dbContext = new ModeloDocumentosFX.ModeloDocumentosFXContainer();
                string conection = dbContext.Database.Connection.ConnectionString;
                cCONEXCION OData = new cCONEXCION(conection);
                DataTable ODataTable = OData.EjecutarConsulta(Sql);
                foreach (System.Data.DataRow Linea in ODataTable.Rows)
                {
                    return Linea["clasificacionSAT"].ToString();
                }
                return string.Empty;
            }
            catch (Exception error)
            {
                string mensaje = "";
                mensaje = error.Message.ToString();
                if (error.InnerException != null)
                {
                    mensaje += " " + error.InnerException.Message.ToString();
                }
                throw new Exception(mensaje);
            }
        }

    }

}
