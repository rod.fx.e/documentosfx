﻿using CryptoSysPKI;

using Generales;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Serialization;
using System.Xml.XPath;
using System.Xml.Xsl;
using ModeloComprobantePago;

namespace FE_FX.Clases
{
    class timbrado40
    {
        public static string validacion(string cadena)
        {
            string devolucion = "";
            devolucion = cadena.ToUpper();
            devolucion = devolucion.Replace("á", "A");
            devolucion = devolucion.Replace("é", "E");
            devolucion = devolucion.Replace("í", "I");
            devolucion = devolucion.Replace("ó", "O");
            devolucion = devolucion.Replace("ú", "U");
            devolucion = devolucion.Replace("Á", "A");
            devolucion = devolucion.Replace("É", "E");
            devolucion = devolucion.Replace("Í", "I");
            devolucion = devolucion.Replace("Ó", "O");
            devolucion = devolucion.Replace("Ú", "U");
            devolucion = devolucion.Replace(">", "");
            devolucion = devolucion.Replace("<", "");
            devolucion = devolucion.Replace("'", "");
            devolucion = devolucion.Replace("#", "");
            devolucion = devolucion.Replace("|", "");
            devolucion = Regex.Replace(devolucion, @"[\u0000-\u001F]", string.Empty);
            return devolucion;
        }

        public static string cadena(CFDI40.Comprobante oComprobante)
        {
            XslCompiledTransform myXslTrans = new XslCompiledTransform();

            XslCompiledTransform trans = new XslCompiledTransform();
            string nombre_tmp = "TMP" + DateTime.Now.ToString("ddMMyyyyhhmmss");
            if (System.IO.File.Exists(nombre_tmp + ".XML"))
            {
                System.IO.File.Delete(nombre_tmp + ".XML");
            }

            XPathDocument myXPathDoc;
            XmlSerializer serializer = new XmlSerializer(typeof(CFDI40.Comprobante));
            try
            {

                XmlSerializerNamespaces myNamespaces_leyenda = new XmlSerializerNamespaces();
                myNamespaces_leyenda.Add("leyendasFisc", "http://www.sat.gob.mx/leyendasFiscales");

                XmlSerializerNamespaces myNamespaces = new XmlSerializerNamespaces();
                myNamespaces.Add("cfdi", "http://www.sat.gob.mx/cfd/4");


                TextWriter writer = new StreamWriter(nombre_tmp + ".XML");
                serializer.Serialize(writer, oComprobante, myNamespaces);
                writer.Close();

                myXPathDoc = new XPathDocument(nombre_tmp + ".XML");
                //XslTransform myXslTrans = new XslTransform();
                myXslTrans = new XslCompiledTransform();


                trans = new XslCompiledTransform();
                XmlUrlResolver resolver = new XmlUrlResolver();

                resolver.Credentials = System.Net.CredentialCache.DefaultCredentials;
                XsltSettings settings = new XsltSettings(true, true);
            }
            catch (XmlException errores)
            {
                ErrorFX.mostrar(errores, true, true, "timbrado40 - 949 ", false);
                return null;
            }
            string sDirectory = AppDomain.CurrentDomain.BaseDirectory;
            //Cargar xslt
            try
            {
                //Version 3
                myXslTrans.Load(sDirectory + "/XSLT/cadenaoriginal_4_0.xslt");
            }
            catch (XmlException errores)
            {
                ErrorFX.mostrar(errores, true, true, "timbrado40 - 961 ", false);
                return null;
            }

            //Crear Archivo Temporal

            XmlTextWriter myWriter = new XmlTextWriter(nombre_tmp + ".TXT", null);

            //Transformar al XML
            myXslTrans.Transform(myXPathDoc, null, myWriter);

            myWriter.Close();

            //Abrir Archivo TXT
            string CADENA_ORIGINAL = "";
            StreamReader re = System.IO.File.OpenText(nombre_tmp + ".TXT");
            string input = null;
            while ((input = re.ReadLine()) != null)
            {
                CADENA_ORIGINAL += input;
            }
            re.Close();

            //Eliminar Archivos Temporales
            System.IO.File.Delete(nombre_tmp + ".TXT");
            System.IO.File.Delete(nombre_tmp + ".XML");

            //Buscar el &amp; en la Cadena y sustituirlo por &
            CADENA_ORIGINAL = CADENA_ORIGINAL.Replace("&amp;", "&");

            return CADENA_ORIGINAL;

        }

        public static string SelloRSA(string LLAVE, string PASSWORD, string strData, decimal ANIO)
        {
            try
            {
                StringBuilder sbPassword;
                StringBuilder sbPrivateKey;
                byte[] b;
                byte[] block;
                int keyBytes;



                string strKeyFile = LLAVE;

                sbPassword = new StringBuilder(PASSWORD);

                sbPrivateKey = Rsa.ReadEncPrivateKey(strKeyFile, sbPassword.ToString());

                keyBytes = Rsa.KeyBytes(sbPrivateKey.ToString());

                b = System.Text.Encoding.UTF8.GetBytes(strData);


                block = Rsa.EncodeMsgForSignature(keyBytes, b, HashAlgorithm.Sha256);

                block = Rsa.RawPrivate(block, sbPrivateKey.ToString());

                string resultado = System.Convert.ToBase64String(block);

                Wipe.String(sbPassword);
                Wipe.String(sbPrivateKey);
                Wipe.Data(block);

                return resultado;
            }
            catch (Exception exLeyendaFiscal)
            {
                ErrorFX.mostrar(exLeyendaFiscal, true, true, "timbrado40 - SelloRSA - 166", true);
                return null;
            }

        }


        public static bool IsNumeric(string s)
        {
            float output;
            return float.TryParse(s, out output);
        }
        public static string Fechammmyy(DateTime fecha)
        {
            string FechaFormateada = "";
            string[] mMeses = new string[13];

            mMeses[1] = "Ene";
            mMeses[2] = "Feb";
            mMeses[3] = "Mar";
            mMeses[4] = "Abr";
            mMeses[5] = "May";
            mMeses[6] = "Jun";
            mMeses[7] = "Jul";
            mMeses[8] = "Agt";
            mMeses[9] = "Sep";
            mMeses[10] = "Oct";
            mMeses[11] = "Nov";
            mMeses[12] = "Dic";

            int Mes = int.Parse(fecha.Month.ToString());

            FechaFormateada = mMeses[Mes] + AgregarCero(fecha.Year.ToString());

            return FechaFormateada;
        }
        public static string AgregarCero(string p)
        {

            String regreso = p;

            if (p.Length < 2)

                regreso = "0" + p;

            return regreso;
        }
        static public void ReplaceInFile(string filePath, string searchText, string replaceText)
        {
            StreamReader reader = new StreamReader(filePath);
            string content = reader.ReadToEnd();
            reader.Close();
            reader.Dispose();

            content = Regex.Replace(content, searchText, replaceText);

            StreamWriter writer = new StreamWriter(filePath);
            writer.Write(content);
            writer.Close();
            writer.Dispose();
        }
        public static void parsar_d1p1(string ARCHIVO)
        {
            ReplaceInFile(ARCHIVO, "d1p1", "xsi");
            ReplaceInFile(ARCHIVO, "xsi:cfdi=\"http://www.sat.gob.mx/cfd/4\"", "");
            ReplaceInFile(ARCHIVO, " xmlns:pago20=\"http://www.sat.gob.mx/Pagos20\"", "");
            ReplaceInFile(ARCHIVO, "xsi:schemaLocation=\"http://www.sat.gob.mx/cfd/4 http://www.sat.gob.mx/sitio_internet/cfd/4/cfdv40.xsd\"",
            "xsi:schemaLocation=\"http://www.sat.gob.mx/cfd/4 http://www.sat.gob.mx/sitio_internet/cfd/4/cfdv40.xsd http://www.sat.gob.mx/Pagos20 http://www.sat.gob.mx/sitio_internet/cfd/Pagos/Pagos20.xsd\"  xmlns:pago20=\"http://www.sat.gob.mx/Pagos20\"");
            //ReplaceInFile(ARCHIVO, "<Pagos ", "<pago20:Pagos ");
            //ReplaceInFile(ARCHIVO, "<Totales ", "<pago20:Totales ");
            //ReplaceInFile(ARCHIVO, "<DoctoRelacionado ", "<pago20:DoctoRelacionado ");
            //ReplaceInFile(ARCHIVO, "<ImpuestosDR ", "<pago20:ImpuestosDR ");
            //ReplaceInFile(ARCHIVO, "<TrasladosDR ", "<pago20:TrasladosDR ");
            //ReplaceInFile(ARCHIVO, "</Pagos", "</pago20:Pagos");
            //ReplaceInFile(ARCHIVO, "</Totales", "</pago20:Totales");
            //ReplaceInFile(ARCHIVO, "</DoctoRelacionado", "</pago20:DoctoRelacionado");
            //ReplaceInFile(ARCHIVO, "</ImpuestosDR", "</pago20:ImpuestosDR");
            //ReplaceInFile(ARCHIVO, "</TrasladosDR", "</pago20:TrasladosDR");

            //
            //ReplaceInFile(ARCHIVO, " xmlns=\"http://www.sat.gob.mx/Pagos\"", "");
            //
            //ReplaceInFile(ARCHIVO, " xmlns:Pago20=\"http://www.sat.gob.mx/Pagos\"", " ");
            //ReplaceInFile(ARCHIVO, "LugarExpedicion", "xmlns:pago20=\"http://www.sat.gob.mx/Pagos\" LugarExpedicion");
        }


        

        internal static string guardarComprobante(CFDI40.Comprobante oComprobante,pago oPago
            ,cSERIE ocSERIE, string estado, bool timbradorReal, DateTime? fechaOpcional = null)
        {
            try
            {
                string DIRECTORIO_ARCHIVOS = "";

                DateTime fecha= oComprobante.Fecha;
                if (fechaOpcional!=null)
                {
                    fecha = (DateTime)fechaOpcional;
                }
                DIRECTORIO_ARCHIVOS += ocSERIE.DIRECTORIO + @"\" + ocSERIE.ID + @"\" + Fechammmyy(fecha);



                string XML = DIRECTORIO_ARCHIVOS;

                string PDF = XML + @"\" + "pdf";
                if (!Directory.Exists(PDF))
                {
                    Directory.CreateDirectory(PDF);
                }

                XML += @"\" + "xml";
                if (Directory.Exists(XML) == false)
                    Directory.CreateDirectory(XML);

                string XML_Archivo = "";

                string nombre_archivo = oComprobante.Serie+oComprobante.Folio;
                
                if (ocSERIE.ARCHIVO_NOMBRE != "")
                {
                    if (oPago!=null)
                    {
                        nombre_archivo = ocSERIE.ARCHIVO_NOMBRE.Replace("{FACTURA}", oPago.checkId).Replace("{CLIENTE}", oPago.customerId)
                        .Replace("{SERIE}", oPago.serie).Replace("{FOLIO}", oPago.folio).Replace("{ID del Cliente}", oPago.customerId)
                        .Replace("{DEPOSIT_ID}", oPago.deposit);
                    }

                }

                //Rodrigo Escalon 07/09/2020 No guardar el XML como borrador
                nombre_archivo = nombre_archivo.Replace("_borrador", "");
                //if (estado.Contains("BORRADOR"))
                if(!timbradorReal)
                {
                    nombre_archivo += "_borrador";
                }


                if (Directory.Exists(DIRECTORIO_ARCHIVOS))
                {
                    XML_Archivo = XML + @"\" + nombre_archivo + ".xml";
                }
                else
                {
                    XML_Archivo = @"\" + nombre_archivo + ".xml"; ;
                }

                XML_Archivo = XML + @"\" + nombre_archivo + ".xml";

                //Rodrigo Escalona: 23/06/2023 Guardar el nombre del archivo
                if (oPago != null)
                {

                    ModeloComprobantePago.documentosfxEntities dbContext = new ModeloComprobantePago.documentosfxEntities();
                    int IdTr = oPago.Id;
                    ModeloComprobantePago.pago oRegistro = dbContext.pagoSet.Where(a => a.Id.Equals(IdTr)).FirstOrDefault();
                    if (oRegistro != null)
                    {
                        oRegistro.xml = XML_Archivo;
                        dbContext.pagoSet.Attach(oRegistro);
                        dbContext.Entry(oRegistro).State = System.Data.Entity.EntityState.Modified;
                        dbContext.SaveChanges();
                    }
                }


                XmlSerializer serializer = new XmlSerializer(typeof(CFDI40.Comprobante));
                TextWriter writerXML = new StreamWriter(XML_Archivo);
                XmlSerializerNamespaces myNamespaces = new XmlSerializerNamespaces();
                myNamespaces.Add("cfdi", "http://www.sat.gob.mx/cfd/4");
                serializer.Serialize(writerXML, oComprobante, myNamespaces);
                writerXML.Close();
                writerXML.Dispose();
                parsar_d1p1(XML_Archivo);

                return XML_Archivo;

            }
            catch (Exception error)
            {
                ErrorFX.mostrar(error, true, true, "timbrado40 - guardarComprobante - 249", true);
                return null;
            }
        }
        internal static string generarCadenaTFD(string archivo)
        {
            string nombre_tmp = "TMP" + DateTime.Now.ToString("ddMMyyyyhhmmss");
            XmlDocument doc = new XmlDocument();
            doc.Load(archivo);
            XslCompiledTransform myXslTrans = new XslCompiledTransform();
            XslCompiledTransform trans = new XslCompiledTransform();
            XmlUrlResolver resolver = new XmlUrlResolver();

            resolver.Credentials = System.Net.CredentialCache.DefaultCredentials;
            XsltSettings settings = new XsltSettings(true, true);

            //Cargar xslt
            try
            {
                myXslTrans.Load(AppDomain.CurrentDomain.BaseDirectory + "/XSLT/cadenaoriginal_TFD_1_1.xslt");
            }
            catch (XmlException error)
            {
                ErrorFX.mostrar(error, true, true, "timbrado40 - generarCadenaTFD - 309", true);
                return null;
            }

            //Crear Archivo Temporal

            XmlTextWriter myWriter = new XmlTextWriter(nombre_tmp + ".TXT", null);
            XmlNodeList TimbreFiscalDigital = doc.GetElementsByTagName("tfd:TimbreFiscalDigital");
            //Transformar al XML
            myXslTrans.Transform(TimbreFiscalDigital[0], null, myWriter);

            myWriter.Close();

            //Abrir Archivo TXT
            string cadenaoriginal_TFD_1_0 = "";
            StreamReader re = System.IO.File.OpenText(nombre_tmp + ".TXT");
            string input = null;
            while ((input = re.ReadLine()) != null)
            {
                cadenaoriginal_TFD_1_0 += input;
            }
            re.Close();

            //Eliminar Archivos Temporales
            System.IO.File.Delete(nombre_tmp + ".TXT");
            if (!String.IsNullOrEmpty(cadenaoriginal_TFD_1_0))
            {
                return cadenaoriginal_TFD_1_0;
            }

            return
                "";
        }

    }
}
