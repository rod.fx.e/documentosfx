﻿using com.google.zxing.qrcode;
using CryptoSysPKI;
using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;
using FE_FX.EDICOM_Servicio;
using Generales;
using Ionic.Zip;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml;
using System.Xml.Serialization;
using System.Xml.XPath;
using System.Xml.Xsl;
using System.Drawing;
using System.IO;
using FE_FX.CFDI;
using System.Data;

namespace FE_FX.Clases
{
    class CartaPorteGeneracion
    {
        CFDI33.Comprobante Comprobante = new CFDI33.Comprobante();

        internal void Generar(string invoiceId, string archivo
            , cCERTIFICADO oCertificado, cSERIE oSerie, DateTime invoiceDate, string customerId
            ,string estado)
        {
            //Rodrigo Escalona Verificar si la factura requiere Carta Porte
            CartaPorteGeneracion OCartaPorteGeneracion = new CartaPorteGeneracion();
            if (OCartaPorteGeneracion.DoCartaPorte(invoiceId))
            {
                CFDI33.Comprobante Comprobante = new CFDI33.Comprobante();
                XmlSerializer serializer_CFD_3 = new XmlSerializer(typeof(CFDI33.Comprobante));
                TextReader reader = new StreamReader(archivo);
                Comprobante = (CFDI33.Comprobante)serializer_CFD_3.Deserialize(reader);

                //Generar la carta porte para guardarla y posteriormente timbrarla
                OCartaPorteGeneracion.GenerarTraslado(Comprobante, invoiceId
                    , oCertificado,  oSerie,  invoiceDate,  customerId
                    , estado);
            }

        }


        internal string GenerarTraslado(CFDI33.Comprobante comprobante, string invoiceId
            , cCERTIFICADO oCertificado,cSERIE oSerie, DateTime invoiceDate, string customerId
            , string estado)
        {
            try
            {
                Comprobante = comprobante;
                DateTime INVOICE_DATE = DateTime.Now;
                INVOICE_DATE = new DateTime(INVOICE_DATE.Year, INVOICE_DATE.Month, INVOICE_DATE.Day, 0, 0, 0);
                Comprobante.Fecha = INVOICE_DATE;
                //Crear el Traslado
                Comprobante.TipoDeComprobante = "T";
                Comprobante.CondicionesDePago = null;
                Comprobante.MetodoPago = null;
                Comprobante.FormaPago = null;
                Comprobante.Moneda = "XXX";
                Comprobante.TipoCambio = 0;
                Comprobante.Receptor.UsoCFDI = "P01";
                Comprobante.TipoCambioSpecified = false;
                Comprobante.SubTotal = 0;
                Comprobante.Total = 0;
                if (Comprobante.Conceptos != null)
                {
                    foreach (CFDI33.ComprobanteConcepto concepto in Comprobante.Conceptos)
                    {
                        concepto.ValorUnitario = 0;
                        concepto.Importe = 0;
                        concepto.Impuestos = null;
                    }

                }
                Comprobante.Impuestos = null;

                
                if (DoCartaPorte(invoiceId))
                {
                    XmlElement OCartePorte = Complemento(invoiceId);
                    Comprobante.Complemento = new CFDI33.ComprobanteComplemento[1];
                    Comprobante.Complemento[0] = new CFDI33.ComprobanteComplemento();
                    Comprobante.Complemento[0].Any = new XmlElement[1];
                    Comprobante.Complemento[0].Any[0] = OCartePorte;
                }

                string NombreArchivo=GenerarCFDI(invoiceId);
                string Cadena = GenerarCadena(NombreArchivo);
                
                //cCERTIFICADO oCertificado, cSERIE oSerie,cFACTURA oFactura

                Comprobante.Sello = SelloRSA(oCertificado.LLAVE, oCertificado.PASSWORD, Cadena, DateTime.Now.Year);
                //Guardar archivo con Sello
                string Xml = GuardarArchivo(Comprobante,oSerie, invoiceId, invoiceDate, customerId);

                TextWriter writerXML = new StreamWriter(Xml);
                XmlSerializer serializer = new XmlSerializer(typeof(CFDI33.Comprobante));

                XmlSerializerNamespaces myNamespaces = new XmlSerializerNamespaces();
                myNamespaces.Add("cfdi", "http://www.sat.gob.mx/cfd/3");
                myNamespaces.Add("cartaporte20", "http://www.sat.gob.mx/CartaPorte20");
                serializer.Serialize(writerXML, Comprobante, myNamespaces);
                writerXML.Close();
                writerXML.Dispose();
                ParsarD1p1(Xml);


                //Realizar el timbrado
                Timbrar(oSerie, estado, Xml);

                //Realizar la impresion del PDF
                string Pdf = "";
                try
                {
                    Pdf=Imprimir(Xml);
                }
                catch
                {
                    Pdf = "Error de impresión de Carta porte de traslado.";
                }
                

                //Guardar en base de datos
                ModeloCartaPorte.CartaPorteEntities Db = new ModeloCartaPorte.CartaPorteEntities();
                ModeloCartaPorte.CartaPorte OCartaPorte = Db.CartaPorteSet
                    .Where(a => a.InvoiceId.Equals(invoiceId))
                    .FirstOrDefault();
                if (OCartaPorte != null)
                {
                    OCartaPorte.Xml = Xml;
                    OCartaPorte.Pdf = Pdf;
                    OCartaPorte.Estado = estado;
                    Db.CartaPorteSet.Attach(OCartaPorte);
                    Db.Entry(OCartaPorte).State = System.Data.Entity.EntityState.Modified;
                }
                else
                {
                    OCartaPorte = new ModeloCartaPorte.CartaPorte();
                    OCartaPorte.FechaCreacion = DateTime.Now;
                    Db.CartaPorteSet.Add(OCartaPorte);

                }
                Db.SaveChanges();
                Db.Dispose();

                return Xml;
            }
            catch (Exception Error)
            {
                ErrorFX.mostrar(Error, true, true, "generar33 - Carta Porte - 2010", true);
            }
            return string.Empty;
        }
        public void ParsarD1p1(string archivo)
        {
            ReplaceInFile(archivo, "d1p1", "xsi");
            ReplaceInFile(archivo, "xsi:cfdi=\"http://www.sat.gob.mx/cfd/3\"", "");
            ReplaceInFile(archivo, " cartaporte20=\"http://www.sat.gob.mx/CartaPorte20\"", "");
        }

        public bool GenerarCFDI(string xml, int Opcion, string EDICOM_USUARIO, string EDICOM_PASSWORD)
        {
            try
            {
                //Comprimir Archivo
                string Archivo_Tmp = Comprobante.Serie + Comprobante.Folio + ".xml";

                File.Copy(xml, Archivo_Tmp, true);
                string ARCHIVO_zip = Archivo_Tmp.ToUpper().Replace("XML", "ZIP");
                using (ZipFile zip = new ZipFile())
                {
                    zip.AddFile(Archivo_Tmp);
                    zip.Save(ARCHIVO_zip);
                }
                //Fin de Compresion de Archivo
                File.Delete(Archivo_Tmp);

                byte[] ARCHIVO_Leido = ReadBinaryFile(ARCHIVO_zip);
                File.Delete(ARCHIVO_zip);
                CFDiClient oCFDiClient = new CFDiClient();

                byte[] PROCESO;

                switch (Opcion)
                {
                    case 1:
                        try
                        {
                            PROCESO = oCFDiClient.getCfdi(EDICOM_USUARIO, EDICOM_PASSWORD, ARCHIVO_Leido);
                            GuardarArchivo(PROCESO, Comprobante.Serie + Comprobante.Folio, xml);
                        }
                        catch (FaultException oException)
                        {

                            CFDiException oCFDiException = new CFDiException();
                            MessageBox.Show(oException.Message, Application.ProductName + "-" + Application.ProductVersion, MessageBoxButtons.OK, MessageBoxIcon.Error);
                            return false;
                        }

                        break;
                    case 2:
                        try
                        {
                            PROCESO = oCFDiClient.getCfdiTest(EDICOM_USUARIO, EDICOM_PASSWORD, ARCHIVO_Leido);
                            GuardarArchivo(PROCESO, Comprobante.Serie + Comprobante.Folio, xml);
                        }
                        catch (FaultException oException)
                        {

                            CFDiException oCFDiException = new CFDiException();
                            MessageBox.Show(oException.Message, Application.ProductName + "-" + Application.ProductVersion, MessageBoxButtons.OK, MessageBoxIcon.Error);
                            return false;
                        }
                        break;
                    case 3:
                        try
                        {
                            PROCESO = oCFDiClient.getTimbreCfdi(EDICOM_USUARIO, EDICOM_PASSWORD, ARCHIVO_Leido);
                        }
                        catch (Exception oException)
                        {

                            CFDiException oCFDiException = new CFDiException();
                            MessageBox.Show(oException.Message, Application.ProductName + "-" + Application.ProductVersion, MessageBoxButtons.OK, MessageBoxIcon.Error);
                            return false;
                        }
                        break;
                    case 4:
                        try
                        {
                            PROCESO = oCFDiClient.getTimbreCfdiTest(EDICOM_USUARIO, EDICOM_PASSWORD, ARCHIVO_Leido);
                        }
                        catch (Exception oException)
                        {

                            CFDiException oCFDiException = new CFDiException();
                            MessageBox.Show(oException.Message, Application.ProductName + "-" + Application.ProductVersion, MessageBoxButtons.OK, MessageBoxIcon.Error);

                            return false;
                        }
                        break;
                }
                return true;
            }
            catch (Exception oException)
            {
                MessageBox.Show("Generando CFDI " + oException.Message, Application.ProductName + "-" + Application.ProductVersion, MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
        }


        private bool Timbrar(cSERIE ocSERIE, string estado, string xml)
        {
            try
            {
                //Timbrar
                bool usar_edicom = false;
                if (estado.ToUpper().Contains("BORRADOR"))
                {
                    usar_edicom = true;
                }
                try
                {
                    if (usar_edicom)
                    {
                        //GenerarCFDI(string xml, int Opcion, string EDICOM_USUARIO, string EDICOM_PASSWORD
                        usar_edicom = GenerarCFDI(xml,2, Globales.oCFDI_USUARIO.USUARIO, cENCRIPTACION.Decrypt(Globales.oCFDI_USUARIO.PASSWORD));

                    }
                    else
                    {
                        usar_edicom = GenerarCFDI(xml,1, Globales.oCFDI_USUARIO.USUARIO, cENCRIPTACION.Decrypt(Globales.oCFDI_USUARIO.PASSWORD));
                        ocSERIE.incrementar(ocSERIE.ROW_ID);
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Error timbrando en EDICOM" + Environment.NewLine + ex.InnerException.ToString(), Application.ProductVersion, MessageBoxButtons.OK);
                    return false;
                }
                if (!estado.ToUpper().Contains("BORRADOR"))
                {
                    return true;
                }
                return false;
            }
            catch (Exception e)
            {
                ErrorFX.mostrar(e, true, true, "Carta Port Generación - 149 ");
            }
            return true;
        }

        private byte[] ReadBinaryFile(string fileName)
        {
            if (File.Exists(fileName))
            {
                try
                {
                    ///Open and read a file&#12290;
                    FileStream fileStream = File.OpenRead(fileName);
                    byte[] archivo = ConvertStreamToByteBuffer(fileStream);
                    fileStream.Close();
                    return archivo;
                }
                catch (Exception ex)
                {
                    return new byte[0];
                }
            }
            else
            {
                return new byte[0];
            }
        }

        public byte[] ConvertStreamToByteBuffer(System.IO.Stream theStream)
        {
            int b1;
            System.IO.MemoryStream tempStream = new System.IO.MemoryStream();
            while ((b1 = theStream.ReadByte()) != -1)
            {
                tempStream.WriteByte(((byte)b1));
            }
            return tempStream.ToArray();
        }


        public void ReplaceInFile(string filePath, string searchText, string replaceText)
        {
            StreamReader reader = new StreamReader(filePath);
            string content = reader.ReadToEnd();
            reader.Close();
            reader.Dispose();

            content = Regex.Replace(content, searchText, replaceText);

            StreamWriter writer = new StreamWriter(filePath);
            writer.Write(content);
            writer.Close();
            writer.Dispose();
        }

        private void GuardarArchivo(byte[] PROCESO, string INVOICE_ID, string ARCHIVO)
        {
            //Guardar los PROCESO

            File.WriteAllBytes("TEMPORAL.ZIP", PROCESO);
            //Descomprimir
            String TargetDirectory = "TEMPORAL";
            using (Ionic.Zip.ZipFile zip = Ionic.Zip.ZipFile.Read("TEMPORAL.ZIP"))
            {
                zip.ExtractAll(TargetDirectory, Ionic.Zip.ExtractExistingFileAction.OverwriteSilently);
            }

            //Copiar el ARCHIVO Descrompimido por otro
            string ARCHIVO_descomprimido = TargetDirectory + @"\" + "SIGN_" + INVOICE_ID + ".XML";

            File.Copy(ARCHIVO_descomprimido, ARCHIVO, true);
            File.Delete(ARCHIVO_descomprimido);
            File.Delete("TEMPORAL.ZIP");
        }


        private string GuardarArchivo(CFDI33.Comprobante comprobante, cSERIE ocSERIE, string invoiceId
            ,DateTime InvoiceDate, string CustomerId)
        {
            string DIRECTORIO_ARCHIVOS = "";

            DIRECTORIO_ARCHIVOS += ocSERIE.DIRECTORIO + @"\" + ocSERIE.ID + @"\" + Fechammmyy(InvoiceDate.ToString());
            string XML = DIRECTORIO_ARCHIVOS;

            //Rodrigo Escalona: Unificar directorio
            string prefijoPDF = "pdf";
            try
            {
                if (ocSERIE.UnirCarpeta)
                {
                    prefijoPDF = "";
                }
            }
            catch
            {

            }

            string PDF = XML + @"\" + prefijoPDF;
            if (!Directory.Exists(PDF))
            {
                try
                {

                    Directory.CreateDirectory(PDF);
                }
                catch (IOException excep)
                {
                    MessageBox.Show(excep.Message.ToString() + Environment.NewLine + "Creando directorio de PDF"
                        , Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error);
                    throw new Exception(excep.Message.ToString() + Environment.NewLine + "Carta Porte Creando directorio de PDF");
                }
            }
            //Rodrigo Escalona: Unificar directorio
            string prefijoXML = "xml";
            try
            {
                if (ocSERIE.UnirCarpeta)
                {
                    prefijoXML = "";
                }
            }
            catch
            {

            }
            XML += @"\" + prefijoXML;
            if (Directory.Exists(XML) == false)
                Directory.CreateDirectory(XML);

            string XML_Archivo = "";

            string nombre_archivo = invoiceId;
            if (ocSERIE.ARCHIVO_NOMBRE != "")
            {
                nombre_archivo = ocSERIE.ARCHIVO_NOMBRE.Replace("{FACTURA}", invoiceId).Replace("{CLIENTE}", CustomerId)
                    ;
            }

            nombre_archivo += "_CartaPorte";

            if (Directory.Exists(DIRECTORIO_ARCHIVOS))
            {
                XML_Archivo = XML + @"\" + nombre_archivo + ".xml";
            }
            else
            {
                XML_Archivo = @"\" + nombre_archivo + ".xml"; ;
            }

            XML_Archivo = XML + @"\" + nombre_archivo + ".xml";

            return XML_Archivo;
        }

        private string GenerarCadena(string nombreArchivo)
        {
            XslCompiledTransform myXslTrans = new XslCompiledTransform();
            XslCompiledTransform trans = new XslCompiledTransform();
            string sDirectory = AppDomain.CurrentDomain.BaseDirectory;
            //Cargar xslt
            try
            {
                //Version 3
                myXslTrans.Load(sDirectory + "/XSLT/cadenaoriginal_3_3.xslt");
            }
            catch (XmlException errores)
            {
                ErrorFX.mostrar(errores, true, true, "Carta Porte Generación - 226 ", false);
                return null;
            }
            XPathDocument myXPathDoc= new XPathDocument(nombreArchivo);
            string nombre_tmp = Path.ChangeExtension(nombreArchivo, null);
            //Crear Archivo Temporal
            XmlTextWriter myWriter = new XmlTextWriter(nombre_tmp + ".TXT", null);
            //Transformar al XML
            myXslTrans.Transform(myXPathDoc, null, myWriter);
            myWriter.Close();

            //Abrir Archivo TXT
            string CADENA_ORIGINAL = "";
            StreamReader re = File.OpenText(nombre_tmp + ".TXT");
            string input = null;
            while ((input = re.ReadLine()) != null)
            {
                CADENA_ORIGINAL += input;
            }
            re.Close();

            //Eliminar Archivos Temporales
            File.Delete(nombre_tmp + ".TXT");
            File.Delete(nombre_tmp + ".XML");

            //Buscar el &amp; en la Cadena y sustituirlo por &
            CADENA_ORIGINAL = CADENA_ORIGINAL.Replace("&amp;", "&");

            return CADENA_ORIGINAL;
        }

        private string GenerarCFDI(string invoiceId)
        {
            XmlSerializerNamespaces myNamespaces = new XmlSerializerNamespaces();
            myNamespaces.Add("cfdi", "http://www.sat.gob.mx/cfd/3");
            myNamespaces.Add("cartaporte20", "http://www.sat.gob.mx/CartaPorte20");
            string nombre_tmp = invoiceId + "TMP" + DateTime.Now.ToString("ddMMyyyyhhmmss") + "_CartaPorte.xml";
            if (File.Exists(nombre_tmp))
            {
                File.Delete(nombre_tmp);
            }
            XPathDocument myXPathDoc;
            XmlSerializer serializer = new XmlSerializer(typeof(CFDI33.Comprobante));
            try
            {
                TextWriter writer = new StreamWriter(nombre_tmp);
                serializer.Serialize(writer, Comprobante, myNamespaces);
                writer.Close();
                myXPathDoc = new XPathDocument(nombre_tmp);
                XmlUrlResolver resolver = new XmlUrlResolver();
                resolver.Credentials = System.Net.CredentialCache.DefaultCredentials;
                XsltSettings settings = new XsltSettings(true, true);
            }
            catch (XmlException errores)
            {
                ErrorFX.mostrar(errores, true, true, "Carta Porte Generación - 87 ", false);
                throw new Exception("Carta Porte Generación - 87 " + errores.Message);
            }

            return nombre_tmp;
        }

        internal XmlElement Complemento(string invoiceId)
        {
            try
            {
                //Crear el Traslado
                Comprobante.TipoDeComprobante = "T";
                Comprobante.CondicionesDePago = null;
                Comprobante.MetodoPago = null;
                Comprobante.FormaPago = null;
                Comprobante.Moneda = "XXX";
                Comprobante.TipoCambio = 0;
                Comprobante.TipoCambioSpecified = false;
                Comprobante.SubTotal = 0;
                Comprobante.Total = 0;
                if (Comprobante.Conceptos != null)
                {
                    foreach (CFDI33.ComprobanteConcepto concepto in Comprobante.Conceptos)
                    {
                        concepto.ValorUnitario = 0;
                        concepto.Importe = 0;
                        concepto.Impuestos = null;
                    }

                }
                Comprobante.Impuestos = null;

                ModeloCartaPorte.CartaPorteEntities Db = new ModeloCartaPorte.CartaPorteEntities();
                ModeloCartaPorte.CartaPorte OCartaPorteDb = Db.CartaPorteSet
                    .Where(a => a.InvoiceId.Equals(invoiceId))
                    .FirstOrDefault();
                if (OCartaPorteDb == null)
                {
                    throw new Exception("La factura " + invoiceId + " no tiene datos de Carta Porte - 2035");
                }
                CartaPorte OCartaPorte = new CartaPorte();
                OCartaPorte.ViaEntradaSalida = c_CveTransporte.Item01;
                OCartaPorte.TranspInternac = CartaPorteTranspInternac.No;
                OCartaPorte.TotalDistRec = 1;
                OCartaPorte.TotalDistRecSpecified = true;

                OCartaPorte.Ubicaciones = new CartaPorteUbicacion[2];
                OCartaPorte.Ubicaciones[0] = new CartaPorteUbicacion();
                OCartaPorte.Ubicaciones[0].TipoUbicacion = CartaPorteUbicacionTipoUbicacion.Origen;
                OCartaPorte.Ubicaciones[0].RFCRemitenteDestinatario = Comprobante.Emisor.Rfc;
                if (OCartaPorte.Ubicaciones[0].RFCRemitenteDestinatario.Equals("XEXX010101000"))
                {
                    if (Comprobante.Receptor.NumRegIdTrib != null)
                    {
                        OCartaPorte.Ubicaciones[0].NumRegIdTrib = Comprobante.Receptor.NumRegIdTrib;
                    }
                    
                }
                OCartaPorte.Ubicaciones[0].FechaHoraSalidaLlegada = DateTime.Now.ToString("yyyy-MM-ddTHH:mm:ss");


                OCartaPorte.Ubicaciones[1] = new CartaPorteUbicacion();
                OCartaPorte.Ubicaciones[1].TipoUbicacion = CartaPorteUbicacionTipoUbicacion.Destino;
                OCartaPorte.Ubicaciones[1].RFCRemitenteDestinatario = Comprobante.Receptor.Rfc;
                OCartaPorte.Ubicaciones[1].FechaHoraSalidaLlegada = DateTime.Now.AddHours(2).ToString("yyyy-MM-ddTHH:mm:ss");
                OCartaPorte.Ubicaciones[1].DistanciaRecorrida = 1;
                OCartaPorte.Ubicaciones[1].DistanciaRecorridaSpecified = true;

                OCartaPorte.Mercancias = new CartaPorteMercancias();
                OCartaPorte.Mercancias.UnidadPeso = c_ClaveUnidadPeso.KGM;

                //78121601

                var ConceptosMateriales = Comprobante.Conceptos.Where(a => !a.ClaveProdServ.Contains("78121"));

                OCartaPorte.Mercancias.Mercancia = new CartaPorteMercanciasMercancia[ConceptosMateriales.Count()];
                int i = 0;
                decimal PesoBrutoTotal = 0;
                int NumTotalMercancias = 0;
                foreach (CFDI33.ComprobanteConcepto ComprobanteConcepto in ConceptosMateriales)
                {
                    OCartaPorte.Mercancias.Mercancia[i] = new CartaPorteMercanciasMercancia();
                    OCartaPorte.Mercancias.Mercancia[i].Cantidad = ComprobanteConcepto.Cantidad;
                    NumTotalMercancias += Decimal.ToInt32(ComprobanteConcepto.Cantidad);

                    OCartaPorte.Mercancias.Mercancia[i].ClaveUnidad = c_ClaveUnidad.H87;

                    try
                    {
                        c_ClaveProdServCP Oc_ClaveProdServCP = (c_ClaveProdServCP)Enum.Parse(typeof(c_ClaveProdServCP), "Item" + ComprobanteConcepto.ClaveProdServ);
                        OCartaPorte.Mercancias.Mercancia[i].BienesTransp = Oc_ClaveProdServCP;
                    }
                    catch
                    {

                    }


                    //Autotransporte
                    ComprobanteConcepto.ClaveProdServ = "78101800";

                    OCartaPorte.Mercancias.Mercancia[i].Descripcion = ComprobanteConcepto.Descripcion;

                    OCartaPorte.Mercancias.Mercancia[i].PesoEnKg = ComprobanteConcepto.Cantidad;
                    PesoBrutoTotal += OCartaPorte.Mercancias.Mercancia[i].PesoEnKg;

                    OCartaPorte.Mercancias.Mercancia[i].MaterialPeligroso = CartaPorteMercanciasMercanciaMaterialPeligroso.No;
                    OCartaPorte.Mercancias.Mercancia[i].MaterialPeligrosoSpecified = true;

                    OCartaPorte.Mercancias.Mercancia[i].MonedaSpecified = true;
                    OCartaPorte.Mercancias.Mercancia[i].Moneda = c_Moneda.MXN;
                    if (Comprobante.Moneda == "USD")
                    {
                        OCartaPorte.Mercancias.Mercancia[i].Moneda = c_Moneda.USD;
                    }

                    //OCartaPorte.Mercancias.Mercancia[i].ValorMercancia = ComprobanteConcepto.Importe;
                    //OCartaPorte.Mercancias.Mercancia[i].ValorMercanciaSpecified = true;

                    //Agregar los impuestos de cada uno
                    //ComprobanteConcepto.Impuestos = new ComprobanteConceptoImpuestos();
                    //ComprobanteConcepto.Impuestos.Retenciones = new ComprobanteConceptoImpuestosRetencion[1];
                    //ComprobanteConcepto.Impuestos.Retenciones[0] = new ComprobanteConceptoImpuestosRetencion();
                    //ComprobanteConcepto.Impuestos.Retenciones[0].TasaOCuota =

                    //ComprobanteConcepto.Impuestos.Traslados = new ComprobanteConceptoImpuestosTraslado[1];


                    i++;
                }
                OCartaPorte.Mercancias.NumTotalMercancias = OCartaPorte.Mercancias.Mercancia.Length;
                OCartaPorte.Mercancias.PesoBrutoTotal = PesoBrutoTotal;

                OCartaPorte.Mercancias.Autotransporte = new CartaPorteMercanciasAutotransporte();
                OCartaPorte.Mercancias.Autotransporte.PermSCT = c_TipoPermiso.TPAF01;
                OCartaPorte.Mercancias.Autotransporte.NumPermisoSCT = OCartaPorteDb.Transporte.NumeroPermisoSCT;
                OCartaPorte.Mercancias.Autotransporte.IdentificacionVehicular = new CartaPorteMercanciasAutotransporteIdentificacionVehicular();
                OCartaPorte.Mercancias.Autotransporte.IdentificacionVehicular.PlacaVM = OCartaPorteDb.Transporte.PlacaRemolque;
                OCartaPorte.Mercancias.Autotransporte.IdentificacionVehicular.AnioModeloVM = DateTime.Now.Year;
                try
                {
                    OCartaPorte.Mercancias.Autotransporte.IdentificacionVehicular.AnioModeloVM = int.Parse(OCartaPorteDb.Transporte.Anio);
                }
                catch
                {

                }

                OCartaPorte.Mercancias.Autotransporte.Seguros = new CartaPorteMercanciasAutotransporteSeguros();
                OCartaPorte.Mercancias.Autotransporte.Seguros.PolizaRespCivil = OCartaPorteDb.Transporte.PolizaResponsabilidadCivil;
                OCartaPorte.Mercancias.Autotransporte.Seguros.AseguraRespCivil = OCartaPorteDb.Transporte.AseguradoraResponsabilidadCivil;

                OCartaPorte.FiguraTransporte = new CartaPorteTiposFigura[1];
                OCartaPorte.FiguraTransporte[0] = new CartaPorteTiposFigura();
                OCartaPorte.FiguraTransporte[0].TipoFigura = c_FiguraTransporte.Item01;
                OCartaPorte.FiguraTransporte[0].RFCFigura = OCartaPorteDb.Transportista.RFC;
                OCartaPorte.FiguraTransporte[0].NumLicencia = OCartaPorteDb.Transportista.NumeroLicencia;

                XmlDocument Doc = new XmlDocument();
                XmlSerializerNamespaces myNamespaces_comercio = new XmlSerializerNamespaces();
                //Version Carta Porte de llenado
                //namespaces.Add("myns", MyElement.ElementNamespace);
                myNamespaces_comercio.Add("cartaporte20", CartaPorte.ElementNamespace);
                using (XmlWriter writer = Doc.CreateNavigator().AppendChild())
                {
                    new XmlSerializer(OCartaPorte.GetType()).Serialize(writer, OCartaPorte, myNamespaces_comercio);
                }
                return Doc.DocumentElement;
            }
            catch (Exception exCCE)
            {
                ErrorFX.mostrar(exCCE, true, true, "generar33 - Carta Porte - 2010", true);
                return null;
            }
            return null;
        }

        public string SelloRSA(string LLAVE, string PASSWORD, string strData, decimal ANIO)
        {
            try
            {
                StringBuilder sbPassword;
                StringBuilder sbPrivateKey;
                byte[] b;
                byte[] block;
                int keyBytes;



                string strKeyFile = LLAVE;

                sbPassword = new StringBuilder(PASSWORD);

                sbPrivateKey = Rsa.ReadEncPrivateKey(strKeyFile, sbPassword.ToString());

                keyBytes = Rsa.KeyBytes(sbPrivateKey.ToString());

                b = System.Text.Encoding.UTF8.GetBytes(strData);


                block = Rsa.EncodeMsgForSignature(keyBytes, b, HashAlgorithm.Sha256);

                block = Rsa.RawPrivate(block, sbPrivateKey.ToString());

                string resultado = System.Convert.ToBase64String(block);

                Wipe.String(sbPassword);
                Wipe.String(sbPrivateKey);
                Wipe.Data(block);

                return resultado;
            }
            catch (Exception exLeyendaFiscal)
            {
                ErrorFX.mostrar(exLeyendaFiscal, true, true, "Carta Porte Generación - SelloRSA - 1666", true);
                return null;
            }

        }


        internal bool DoCartaPorte(string invoice)
        {
            ModeloCartaPorte.CartaPorteEntities Db = new ModeloCartaPorte.CartaPorteEntities();
            ModeloCartaPorte.CartaPorte OCartaPorte = Db.CartaPorteSet
                .Where(a => a.InvoiceId.Equals(invoice))
                .FirstOrDefault();
            if (OCartaPorte != null)
            {
                return true;
            }
            Db.Dispose();
            return false;
        }

        public string Fechammmyy(string pFecha)
        {
            DateTime Fecha = DateTime.Parse(pFecha.ToString());
            string FechaFormateada = "";
            string[] mMeses = new string[13];

            mMeses[1] = "Ene";
            mMeses[2] = "Feb";
            mMeses[3] = "Mar";
            mMeses[4] = "Abr";
            mMeses[5] = "May";
            mMeses[6] = "Jun";
            mMeses[7] = "Jul";
            mMeses[8] = "Agt";
            mMeses[9] = "Sep";
            mMeses[10] = "Oct";
            mMeses[11] = "Nov";
            mMeses[12] = "Dic";

            int Mes = int.Parse(Fecha.Month.ToString());

            FechaFormateada = mMeses[Mes] + AgregarCero(Fecha.Year.ToString());

            return FechaFormateada;
        }
        public string AgregarCero(string p)
        {

            String regreso = p;

            if (p.Length < 2)

                regreso = "0" + p;

            return regreso;
        }

        #region Imprimir
        internal void ImprimirCFDI()
        {
            ReportDocument oReport = new ReportDocument();
            OpenFileDialog openFileDialog1 = new OpenFileDialog
            {
                InitialDirectory = @"D:\",
                Title = "CFDI ",
                CheckFileExists = true,
                CheckPathExists = true,
                DefaultExt = "xml",
                Filter = "CFDI XML files (*.xml)|*.xml",
                FilterIndex = 2,
                RestoreDirectory = true,
                ReadOnlyChecked = true,
                ShowReadOnly = true
            };

            if (openFileDialog1.ShowDialog() == DialogResult.Cancel)
            {
                return;
            }
            string path = openFileDialog1.FileName;
            string Formato = "CartaPorteTraslado.rpt";
            

            //XmlDocument doc = new XmlDocument();
            //doc.Load(path);
            //XmlNodeList XmlCartaPorte = doc.GetElementsByTagName("cartaporte20:CartaPorte");
            //if (XmlCartaPorte == null)
            //{
            //    return;
            //}

            //MemoryStream stm = new MemoryStream();
            //StreamWriter stw = new StreamWriter(stm);
            //stw.Write(XmlCartaPorte[0].OuterXml);
            //stw.Flush();
            //stm.Position = 0;
            //XmlSerializer Serializer = new XmlSerializer(typeof(CartaPorte));
            //CartaPorte OCartaPorte = (CartaPorte)Serializer.Deserialize(stm);
            
            oReport.Load(Formato);

            DataSet ds1 = new DataSet();
            ds1.ReadXml(path);


            oReport.SetDataSource(ds1);
            frmCFDIImpresion ofrmReporte = new frmCFDIImpresion(oReport);
            ofrmReporte.ShowDialog();
        }

        internal string Imprimir(string pathP, bool verPDF = true)
        {
            ReportDocument oReport = new ReportDocument();
            string path = "";
            if (!String.IsNullOrEmpty(pathP))
            {
                if (File.Exists(pathP))
                {
                    path = pathP;
                }
            }
            else
            {
                OpenFileDialog openFileDialog1 = new OpenFileDialog
                {
                    InitialDirectory = @"D:\",
                    Title = "CFDI ",
                    CheckFileExists = true,
                    CheckPathExists = true,
                    DefaultExt = "xml",
                    Filter = "CFDI XML files (*.xml)|*.xml",
                    FilterIndex = 2,
                    RestoreDirectory = true,
                    ReadOnlyChecked = true,
                    ShowReadOnly = true
                };

                if (openFileDialog1.ShowDialog() == DialogResult.OK)
                {
                    path = openFileDialog1.FileName;
                }

            }

            string FORMATO = "TRASLADOCartaPorteds.rpt";


            if (!File.Exists(FORMATO))
            {
                MessageBox.Show("El Reporte " + FORMATO + " no existe en su directorio. La factura no fue Generada.", Application.ProductName + "-" + Application.ProductVersion, MessageBoxButtons.OK, MessageBoxIcon.Error);
                return string.Empty;
            }

            try
            {

                XmlSerializer serializer_CFD_3 = new XmlSerializer(typeof(CFDI33.Comprobante));
                TextReader reader = new StreamReader(path);
                CFDI33.Comprobante oComprobanteLocal = (CFDI33.Comprobante)serializer_CFD_3.Deserialize(reader);
                reader.Close();
                oReport.Load(FORMATO);

                XmlDocument doc = new XmlDocument();
                doc.Load(path);
                XmlNodeList TimbreFiscalDigital = doc.GetElementsByTagName("tfd:TimbreFiscalDigital");
                if (TimbreFiscalDigital == null)
                {
                    ErrorFX.mostrar("El archivo: " + path + " No esta timbrado.", true, true, false);
                    return string.Empty;
                }
                string UUID = "";
                UUID = TimbreFiscalDigital[0].Attributes["UUID"].Value;
                string FechaTimbrado = "";
                FechaTimbrado = TimbreFiscalDigital[0].Attributes["FechaTimbrado"].Value;
                string noCertificadoSAT = "";
                noCertificadoSAT = TimbreFiscalDigital[0].Attributes["NoCertificadoSAT"].Value;
                string selloSAT = "";
                selloSAT = TimbreFiscalDigital[0].Attributes["SelloSAT"].Value;

                XmlNodeList Comprobante = doc.GetElementsByTagName("cfdi:Comprobante");
                string LugarExpedicion = "";
                if (Comprobante != null)
                {
                    LugarExpedicion = Comprobante[0].Attributes["LugarExpedicion"].Value;
                }

                byte[] image = generarImagen(path);
                string cadenaOriginal = generarCadenaOriginal(path);
                string cadenaTFD = generarCadenaTFD(path);


                XmlNodeList Emisor = doc.GetElementsByTagName("cfdi:Emisor");
                string RegimenFiscal = "";
                if (Emisor != null)
                {
                    RegimenFiscal = Emisor[0].Attributes["RegimenFiscal"].Value;
                }

                XmlNodeList Receptor = doc.GetElementsByTagName("cfdi:Receptor");
                string UsoCFDI = "";
                if (Receptor != null)
                {
                    UsoCFDI = Receptor[0].Attributes["UsoCFDI"].Value;
                }
                string NumRegIdTrib = "";
                if (Receptor != null)
                {
                    try
                    {
                        NumRegIdTrib = Receptor[0].Attributes["NumRegIdTrib"].Value;
                    }
                    catch
                    {

                    }
                }

                cfdiCE ocfdiCE = new cfdiCE();
                ocfdiCE.DATOS.AddDATOSRow(image, cadenaOriginal, cadenaTFD, ""
                    , "", 0
                    , oComprobanteLocal.Emisor.Rfc, oComprobanteLocal.Emisor.Nombre, oComprobanteLocal.Emisor.RegimenFiscal
                    , oComprobanteLocal.LugarExpedicion
                    , oComprobanteLocal.Receptor.Nombre, oComprobanteLocal.Receptor.Rfc
                    , oComprobanteLocal.Fecha.ToString(), oComprobanteLocal.Serie, oComprobanteLocal.Folio.ToString()
                    , oComprobanteLocal.NoCertificado.ToString()
                    , oComprobanteLocal.TipoDeComprobante.ToString(), 0, 0
                    , oComprobanteLocal.Total
                    , UUID, "", FechaTimbrado, noCertificadoSAT, selloSAT
                    , "", RegimenFiscal, UsoCFDI, LugarExpedicion
                    , NumRegIdTrib
                    );

                int Consecutivo = 1;
                foreach (CFDI33.ComprobanteConcepto concepto in oComprobanteLocal.Conceptos)
                {
                    ocfdiCE.conceptos.AddconceptosRow(concepto.Cantidad, concepto.Descripcion.Replace("\n", "").Replace("\r", "")
                        , concepto.NoIdentificacion, concepto.ValorUnitario, concepto.Importe, 0,
                        concepto.Unidad, concepto.ClaveProdServ, concepto.ClaveUnidad, Consecutivo,"");
                    Consecutivo++;
                }


                //Agregar los datos de Carta Porte
                XmlNodeList Autotransporte = doc.GetElementsByTagName("cartaporte20:Autotransporte");
                if (Autotransporte == null)
                {
                    ErrorFX.mostrar("El archivo: " + path + " No tiene cartaporte20:Autotransporte.", true, true, false);
                    return string.Empty;
                }
                string PermSCT = "";
                PermSCT = Autotransporte[0].Attributes["PermSCT"].Value;
                string NumPermisoSCT = "";
                NumPermisoSCT = Autotransporte[0].Attributes["NumPermisoSCT"].Value;

                XmlNodeList IdentificacionVehicular = doc.GetElementsByTagName("cartaporte20:IdentificacionVehicular");
                if (Autotransporte == null)
                {
                    ErrorFX.mostrar("El archivo: " + path + " No tiene cartaporte20:IdentificacionVehicular.", true, true, false);
                    return string.Empty;
                }
                string ConfigVehicular = "";
                ConfigVehicular = IdentificacionVehicular[0].Attributes["ConfigVehicular"].Value;
                string PlacaVM = "";
                PlacaVM = IdentificacionVehicular[0].Attributes["PlacaVM"].Value;
                string AnioModeloVM = "";
                AnioModeloVM = IdentificacionVehicular[0].Attributes["AnioModeloVM"].Value;

                XmlNodeList Seguros = doc.GetElementsByTagName("cartaporte20:Seguros");
                if (Autotransporte == null)
                {
                    ErrorFX.mostrar("El archivo: " + path + " No tiene cartaporte20:Seguros.", true, true, false);
                    return string.Empty;
                }
                string AseguraRespCivil = "";
                AseguraRespCivil = Seguros[0].Attributes["AseguraRespCivil"].Value;
                string PolizaRespCivil = "";
                PolizaRespCivil = Seguros[0].Attributes["PolizaRespCivil"].Value;

                XmlNodeList TiposFigura = doc.GetElementsByTagName("cartaporte20:TiposFigura");
                if (Autotransporte == null)
                {
                    ErrorFX.mostrar("El archivo: " + path + " No tiene cartaporte20:TiposFigura.", true, true, false);
                    return string.Empty;
                }
                string TipoFigura = "";
                TipoFigura = TiposFigura[0].Attributes["TipoFigura"].Value;
                string RFCFigura = "";
                RFCFigura = TiposFigura[0].Attributes["RFCFigura"].Value;
                string NumLicencia = "";
                NumLicencia = TiposFigura[0].Attributes["NumLicencia"].Value;

                ocfdiCE.CartaPorte.AddCartaPorteRow(PermSCT, NumPermisoSCT, ConfigVehicular, PlacaVM, AnioModeloVM, AseguraRespCivil
                    , PolizaRespCivil, TipoFigura, RFCFigura, NumLicencia,"","","");


                oReport.SetDataSource(ocfdiCE);

                foreach (ParameterFieldDefinition definition in oReport.DataDefinition.ParameterFields)
                {
                    ParameterValues myvals = new ParameterValues();
                    ParameterDiscreteValue myDiscrete = new ParameterDiscreteValue();
                    switch (definition.ParameterFieldName)
                    {
                        case "cadenaSAT":
                            //if (!String.IsNullOrEmpty(xml))
                            //{
                            myDiscrete.Value = cadenaTFD;
                            myvals.Add(myDiscrete);
                            definition.CurrentValues.Add(myDiscrete);
                            definition.ApplyCurrentValues(myvals);
                            //}
                            break;                        
                    }
                }
                try
                {
                    ExportOptions CrExportOptions = new ExportOptions();
                    DiskFileDestinationOptions CrDiskFileDestinationOptions = new DiskFileDestinationOptions();
                    PdfRtfWordFormatOptions CrFormatTypeOptions = new PdfRtfWordFormatOptions();
                    string pdftr = path.Replace(".xml", ".pdf");
                    pdftr = pdftr.Replace(@"\xml", @"\pdf");
                    CrDiskFileDestinationOptions.DiskFileName = pdftr;
                    CrExportOptions = oReport.ExportOptions;
                    CrExportOptions.ExportDestinationType = ExportDestinationType.DiskFile;
                    CrExportOptions.ExportFormatType = ExportFormatType.PortableDocFormat;
                    CrExportOptions.DestinationOptions = CrDiskFileDestinationOptions;
                    CrExportOptions.FormatOptions = CrFormatTypeOptions;
                    oReport.Export();
                    if (verPDF)
                    {
                        frmCFDIImpresion ofrmReporte = new frmCFDIImpresion(oReport);
                        ofrmReporte.ShowDialog();
                    }
                    return pdftr;
                }
                catch (Exception error)
                {
                    string mensaje = "Impresion Carta Porte Traslasdo . ";
                    mensaje += error.Message.ToString();
                    if (error.InnerException != null)
                    {
                        mensaje += " " + error.InnerException.Message.ToString();
                    }

                    MessageBox.Show(mensaje);
                }
                return string.Empty;
            }
            catch (Exception error)
            {
                string mensaje = error.Message.ToString();
                if (error.InnerException != null)
                {
                    mensaje += Environment.NewLine + error.InnerException.ToString();
                }
                MessageBox.Show(mensaje);
            }
            return string.Empty;
        }
        private string generarCadenaOriginal(string archivo)
        {
            XPathDocument myXPathDoc = new XPathDocument(archivo);
            //XslTransform myXslTrans = new XslTransform();
            XslCompiledTransform myXslTrans = new XslCompiledTransform();
            XslCompiledTransform trans = new XslCompiledTransform();
            XmlUrlResolver resolver = new XmlUrlResolver();

            resolver.Credentials = System.Net.CredentialCache.DefaultCredentials;
            XsltSettings settings = new XsltSettings(true, true);
            string sDirectory = AppDomain.CurrentDomain.BaseDirectory;
            //Cargar xslt
            try
            {
                //Version 3
                myXslTrans.Load(sDirectory + "/XSLT/cadenaoriginal_3_2.xslt");
            }
            catch (XmlException errores)
            {
                MessageBox.Show("Error. Conectarse en el Portal del SAT al " + errores.InnerException.ToString());
                return "";
            }

            //Crear Archivo Temporal
            string nombre_tmp = "TMP" + DateTime.Now.ToString("ddMMyyyyhhmmss");
            XmlTextWriter myWriter = new XmlTextWriter(nombre_tmp + ".TXT", null);

            //Transformar al XML
            myXslTrans.Transform(myXPathDoc, null, myWriter);

            myWriter.Close();
            string CADENA_ORIGINAL = "";
            //Abrir Archivo TXT
            StreamReader re = File.OpenText(nombre_tmp + ".TXT");
            string input = null;
            while ((input = re.ReadLine()) != null)
            {
                CADENA_ORIGINAL += input;
            }
            re.Close();

            return CADENA_ORIGINAL;
        }

        private string generarCadenaTFD(string archivo)
        {
            string nombre_tmp = "TMP" + DateTime.Now.ToString("ddMMyyyyhhmmss");
            XmlDocument doc = new XmlDocument();
            doc.Load(archivo);
            XslCompiledTransform myXslTrans = new XslCompiledTransform();
            XslCompiledTransform trans = new XslCompiledTransform();
            XmlUrlResolver resolver = new XmlUrlResolver();

            resolver.Credentials = System.Net.CredentialCache.DefaultCredentials;
            XsltSettings settings = new XsltSettings(true, true);

            //Cargar xslt
            try
            {
                myXslTrans.Load(AppDomain.CurrentDomain.BaseDirectory + "/XSLT/cadenaoriginal_TFD_1_1.xslt");
            }
            catch (XmlException errores)
            {
                MessageBox.Show("Error en " + errores.InnerException.ToString());
                return "";
            }

            //Crear Archivo Temporal

            XmlTextWriter myWriter = new XmlTextWriter(nombre_tmp + ".TXT", null);
            XmlNodeList TimbreFiscalDigital = doc.GetElementsByTagName("tfd:TimbreFiscalDigital");
            //Transformar al XML
            myXslTrans.Transform(TimbreFiscalDigital[0], null, myWriter);

            myWriter.Close();

            //Abrir Archivo TXT
            string cadenaoriginal_TFD_1_0 = "";
            StreamReader re = File.OpenText(nombre_tmp + ".TXT");
            string input = null;
            while ((input = re.ReadLine()) != null)
            {
                cadenaoriginal_TFD_1_0 += input;
            }
            re.Close();

            //Eliminar Archivos Temporales
            File.Delete(nombre_tmp + ".TXT");
            return cadenaoriginal_TFD_1_0;

        }

        private byte[] generarImagen(string archivo)
        {
            //?re=XAXX010101000&rr=XAXX010101000&tt=1234567890.123456&id=ad662d33-6934-459c-a128-BDf0393f0f44
            string QR_Code = "";
            XmlSerializer serializer_CFD_3 = new XmlSerializer(typeof(CFDI33.Comprobante));
            TextReader reader = new StreamReader(archivo);
            try
            {
                if (!File.Exists(archivo))
                {
                    MessageBox.Show("El CFDI " + archivo + " no existe");
                    return null;
                }
                //Abrir el archivo y copiar los sellos
                XmlDocument doc = new XmlDocument();
                doc.Load(archivo);

                XmlNodeList TimbreFiscalDigital = doc.GetElementsByTagName("tfd:TimbreFiscalDigital");
                XmlNodeList receptor = doc.GetElementsByTagName("cfdi:Receptor");
                XmlNodeList emisor = doc.GetElementsByTagName("cfdi:Emisor");
                XmlNodeList comprobante = doc.GetElementsByTagName("cfdi:Comprobante");



                string Sello = "";

                try
                {
                    Sello = comprobante[0].Attributes["Sello"].Value;
                }
                catch
                {

                }

                string rfcReceptor = "";

                try
                {
                    rfcReceptor = receptor[0].Attributes["Rfc"].Value;
                }
                catch
                {

                }

                string rfcEmisor = "";

                try
                {
                    rfcEmisor = emisor[0].Attributes["Rfc"].Value;
                }
                catch
                {

                }

                string total = "";

                try
                {
                    total = comprobante[0].Attributes["Total"].Value;
                }
                catch
                {

                }

                string UUID = "";

                try
                {
                    UUID = TimbreFiscalDigital[0].Attributes["UUID"].Value;
                }
                catch
                {

                }
                string FechaTimbrado = "";

                try
                {
                    FechaTimbrado = TimbreFiscalDigital[0].Attributes["FechaTimbrado"].Value;
                }
                catch
                {

                }

                string noCertificadoSAT = "";
                try
                {
                    noCertificadoSAT = TimbreFiscalDigital[0].Attributes["noCertificadoSAT"].Value;
                }
                catch
                {

                }


                try
                {
                    noCertificadoSAT = TimbreFiscalDigital[0].Attributes["NoCertificadoSAT"].Value;
                }
                catch
                {

                }

                string selloSAT = "";
                try
                {
                    selloSAT = TimbreFiscalDigital[0].Attributes["selloSAT"].Value;
                }
                catch
                {

                }

                try
                {
                    selloSAT = TimbreFiscalDigital[0].Attributes["SelloSAT"].Value;
                }
                catch
                {

                }

                string USO_CFDI = "";

                try
                {
                    USO_CFDI = receptor[0].Attributes["UsoCFDI"].Value;
                }
                catch
                {

                }

                try
                {
                    USO_CFDI = receptor[0].Attributes["UsoCFDI"].Value;
                }
                catch
                {

                }


                string VERSION = "";

                try
                {
                    VERSION = comprobante[0].Attributes["version"].Value;
                }
                catch
                {

                }

                try
                {
                    VERSION = comprobante[0].Attributes["Version"].Value;
                }
                catch
                {

                }

                string metodo_pago = "";

                try
                {
                    metodo_pago = comprobante[0].Attributes["MetodoPago"].Value;
                }
                catch
                {

                }

                try
                {
                    metodo_pago = comprobante[0].Attributes["MetodoPago"].Value;
                }
                catch
                {

                }
                string banco = "";
                try
                {
                    banco = comprobante[0].Attributes["NumCtaPago"].Value;
                }
                catch
                {

                }

                try
                {
                    banco = comprobante[0].Attributes["NumCtaPago"].Value;
                }
                catch
                {

                }


                string FormaPago = "";

                try
                {
                    FormaPago = comprobante[0].Attributes["FormaPago"].Value;
                }
                catch
                {

                }

                try
                {
                    FormaPago = comprobante[0].Attributes["formaPago"].Value;
                }
                catch
                {

                }
                string NoCertificado = "";
                try
                {
                    NoCertificado = comprobante[0].Attributes["NoCertificado"].Value;
                }
                catch
                {

                }
                //Abrir Archivo TXT
                string cadenaoriginal_TFD_1_0 = "";
                //Crear Archivo Temporal
                if (TimbreFiscalDigital != null)
                {
                    string sDirectory = AppDomain.CurrentDomain.BaseDirectory;
                    //Generar cadenaoriginal_TFD_1_0.xslt
                    string nombre_tmp = "TMP" + DateTime.Now.ToString("ddMMyyyyhhmmss");
                    //XslTransform myXslTrans = new XslTransform();
                    XslCompiledTransform myXslTrans = new XslCompiledTransform();


                    XslCompiledTransform trans = new XslCompiledTransform();
                    XmlUrlResolver resolver = new XmlUrlResolver();

                    resolver.Credentials = System.Net.CredentialCache.DefaultCredentials;
                    XsltSettings settings = new XsltSettings(true, true);

                    //Cargar xslt
                    try
                    {

                        myXslTrans.Load(sDirectory + "/XSLT/cadenaoriginal_TFD_1_1.xslt");
                    }
                    catch (XmlException errores)
                    {
                        MessageBox.Show("Error en " + errores.InnerException.ToString());
                        return null;
                    }
                    XmlTextWriter myWriter = new XmlTextWriter(nombre_tmp + ".TXT", null);

                    //Transformar al XML
                    myXslTrans.Transform(TimbreFiscalDigital[0], null, myWriter);

                    myWriter.Close();


                    StreamReader re = File.OpenText(nombre_tmp + ".TXT");
                    string input = null;
                    while ((input = re.ReadLine()) != null)
                    {
                        cadenaoriginal_TFD_1_0 += input;
                    }
                    re.Close();
                    re.Dispose();
                    //Eliminar Archivos Temporales
                    File.Delete(nombre_tmp + ".TXT");
                }

                QR_Code = "https://verificacfdi.facturaelectronica.sat.gob.mx/default.aspx"
                    + "?id=" + UUID
                    + "&re=" + rfcEmisor
                    + "&rr=" + rfcReceptor
                    + "&tt=" + total
                    + "&fe=" + Sello.Substring(Sello.Length - 8, 8);

                //XmlDocument doc = new XmlDocument();
                //doc.Load(archivo);


                //XmlNodeList TimbreFiscalDigital = doc.GetElementsByTagName("tfd:TimbreFiscalDigital");

                //string UUID = "";
                //UUID = TimbreFiscalDigital[0].Attributes["UUID"].Value;

                //oComprobanteLocal = (Comprobante)serializer_CFD_3.Deserialize(reader);

                //string QR_Code = "https://verificacfdi.facturaelectronica.sat.gob.mx/default.aspx&";
                //QR_Code = "https://verificacfdi.facturaelectronica.sat.gob.mx/default.aspx"
                //    + "?id=" + UUID
                //    + "&re=" + rfcEmisor
                //    + "&rr=" + rfcReceptor
                //    + "&tt=" + total
                //    + "&fe=" + Sello.Substring(Sello.Length - 8, 8);

                //QR_Code += "?re=" + oComprobanteLocal.Emisor.Rfc;
                //QR_Code += "&rr=" + oComprobanteLocal.Receptor.Rfc;
                //QR_Code += "&tt=" + oComprobanteLocal.Total.ToString();
                //QR_Code += "&id=" + UUID;

            }
            catch (Exception xmlex)
            {
                Bitacora.Log("Errpr :: " + xmlex.ToString());
                return null;
            }
            int size = 320;
            QRCodeWriter writer = new QRCodeWriter();
            com.google.zxing.common.ByteMatrix matrix;
            matrix = writer.encode(QR_Code, com.google.zxing.BarcodeFormat.QR_CODE, size, size, null);
            Bitmap img = new Bitmap(size, size);
            Color Color = Color.FromArgb(0, 0, 0);

            for (int y = 0; y < matrix.Height; ++y)
            {
                for (int x = 0; x < matrix.Width; ++x)
                {
                    Color pixelColor = img.GetPixel(x, y);

                    //Find the colour of the dot
                    if (matrix.get_Renamed(x, y) == -1)
                    {
                        img.SetPixel(x, y, Color.White);
                    }
                    else
                    {
                        img.SetPixel(x, y, Color.Black);
                    }
                }
            }


            string nombre = Path.GetTempPath() + "QRP_TEMP.BMP";
            img.Save(nombre);

            FileStream fs = new FileStream(nombre, System.IO.FileMode.Open, System.IO.FileAccess.Read);
            byte[] Image = new byte[fs.Length];
            fs.Read(Image, 0, Convert.ToInt32(fs.Length));
            int image_32 = Convert.ToInt32(fs.Length);
            fs.Close();
            File.Delete(nombre);

            return Image;

        }

        #endregion
    }
}
