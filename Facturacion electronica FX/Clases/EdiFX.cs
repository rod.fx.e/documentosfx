﻿using System;
using System.Collections.Generic;
using System.Configuration; // Para leer el archivo de configuración
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace FE_FX.Clases
{
    public class EdiFX
    {
        // Configurable properties
        public string Standard { get; set; } = "X";
        public string Version { get; set; } = "004010";
        public string TransactionType { get; set; } = "810";
        public string TestIndicator { get; set; } = "P";
        public string ReceiverId { get; set; } = "812297850"; // Default receiver ID

        public EdiFX()
        {
            // Default constructor
        }

        public string GetLastPartAfterHyphen(string input)
        {
            int lastHyphenIndex = input.LastIndexOf('-');
            if (lastHyphenIndex != -1 && lastHyphenIndex < input.Length - 1)
            {
                return input.Substring(lastHyphenIndex + 1);
            }
            else
            {
                return string.Empty; // o lanzar una excepción dependiendo de tu necesidad
            }
        }

        public void CrearFactura810(cFACTURA factura, string planta, string directorio = "")
        {
            // Leer el directorio desde el archivo de configuración
            //if (string.IsNullOrEmpty(directorio))
            //{
                directorio = ConfigurationManager.AppSettings["directorioEDI"];
            //}

            // Determine the client ID based on the planta
            string senderId = factura.CUSTOMER_ID.Contains("219") ? "812678378": "812821996";

            //(219 – 219A)  à 812678378

            string CodigoProveedor = "02070";
            // StringBuilder to construct the EDI file content
            StringBuilder ediBuilder = new StringBuilder();

            // Header section
            ediBuilder.AppendFormat("CAB,{0},{1},{2},{3},{4},{5}\n",
                senderId.PadRight(30),
                ReceiverId.PadRight(30),
                Standard,
                Version,
                TransactionType,
                TestIndicator);

            // Invoice header
            ediBuilder.AppendFormat("HDR,{0},{1},{2:yyyyMMdd},{3},{4:F0},{5}\n",
                senderId,
                ReceiverId,
                factura.INVOICE_DATE,
                CodigoProveedor,
                factura.TOTAL_AMOUNT,
                factura.INVOICE_ID.PadRight(5)
            );
            // Manifiesto:
            string Manifiesto = "";
            Manifiesto = GetLastPartAfterHyphen(factura.CUSTOMER_PO_REF);

            // Detail lines
            foreach (var linea in factura.LINEAS)
            {
                int QtyTr = (int)decimal.Parse(linea.QTY);
                string UmCode = "EA";
                if (linea.UM_CODE.Equals("PZ"))
                {
                    UmCode = "EA";
                }
                else
                {
                    UmCode = linea.UM_CODE;
                }

                // Obtener CustomerPoRef de la Linea de la factura
                if (!String.IsNullOrEmpty(linea.CustomerPoRef))
                {
                    Manifiesto = GetLastPartAfterHyphen(linea.CustomerPoRef);
                }

                ediBuilder.AppendFormat("LIN,{0},{1},{2},{3},{4},{5},{6},{7},{8:yyyyMMdd}\n",
                    "M390",
                    QtyTr,
                    UmCode,
                    linea.UNIT_PRICE,
                    linea.CUSTOMER_PART_ID.PadRight(12).Replace("-", ""),
                    "N1", // Dock Code Reader
                    Manifiesto, // linea.MANIFEST_NUMBER,
                    factura.INVOICE_ID,
                    factura.INVOICE_DATE);
            }

            // Generate the EDI file name
            string fileName = $"810_{planta.ToUpper()}_{factura.INVOICE_ID}.txt";

            // Check if the directory exists, if not prompt the user to select the directory
            if (string.IsNullOrEmpty(directorio) || !Directory.Exists(directorio))
            {
                using (FolderBrowserDialog folderBrowserDialog = new FolderBrowserDialog())
                {
                    folderBrowserDialog.Description = "Select the directory to save the EDI file";
                    if (folderBrowserDialog.ShowDialog() == DialogResult.OK)
                    {
                        directorio = folderBrowserDialog.SelectedPath;
                    }
                    else
                    {
                        Console.WriteLine("Operation cancelled. No file was created.");
                        return;
                    }
                }
            }

            // Save the EDI content to the specified directory
            string filePath = Path.Combine(directorio, fileName);
            File.WriteAllText(filePath, ediBuilder.ToString());
            Console.WriteLine($"EDI 810 file created: {filePath}");
        }
    }
}
