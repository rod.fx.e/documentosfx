﻿using CFDI32;
using CryptoSysPKI;
using ModeloDocumentosFX;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml;
using System.Xml.Serialization;
using System.Xml.XPath;
using System.Xml.Xsl;
using cce11_v32;
using Generales;
namespace FE_FX.Clases
{
    class generar32
    {
        public bool generar(cFACTURA oFACTURA, cSERIE ocSERIE, Encapsular oEncapsular, cCERTIFICADO ocCERTIFICADO)
        {
            ModeloDocumentosFXContainer dbContext = new ModeloDocumentosFXContainer();
            string conection = dbContext.Database.Connection.ConnectionString;
            cCONEXCION oData = new cCONEXCION(conection);

            XmlSerializer serializer = new XmlSerializer(typeof(CFDI32.Comprobante));

            CFDI32.Comprobante oComprobante = new CFDI32.Comprobante();
            XmlSerializerNamespaces myNamespaces = new XmlSerializerNamespaces();
            myNamespaces.Add("cfdi", "http://www.sat.gob.mx/cfd/3");
            DateTime FECHA_FACTURA = new DateTime();
            try
            {
                //MessageBox.Show("Comprobante2");

                //Extraer el SERIE de INVOICE_ID
                string FOLIO = ExtractNumbers(oFACTURA.INVOICE_ID);
                //Serie
                oComprobante.serie = ocSERIE.ID;
                //Folio
                oComprobante.folio = FOLIO;
                //MessageBox.Show("Comprobante3");

                oComprobante.Moneda = oFACTURA.CURRENCY_ID;

                if (oComprobante.Moneda.Contains("DLLS"))
                {
                    oComprobante.Moneda = "USD";
                }
                if (oComprobante.Moneda.Contains("MXP"))
                {
                    oComprobante.Moneda = "MXN";
                }

                oComprobante.TipoCambio = otroTruncar(oFACTURA.SELL_RATE, oEncapsular.ocEMPRESA).ToString();

                oComprobante.condicionesDePago = oFACTURA.TERMS_NET_DAYS;
                if ((oComprobante.condicionesDePago == "")
                    ||
                    (oComprobante.condicionesDePago == null))
                {
                    oComprobante.condicionesDePago = "PAGO EN UNA SOLA EXHIBICION";
                }
                else
                {
                    if (oFACTURA.TERMS_NET_DAYS != "")
                    {
                        oComprobante.condicionesDePago = oFACTURA.TERMS_NET_DAYS;
                    }
                }
                //CDFI 3.2
                oComprobante.LugarExpedicion = ocSERIE.ESTADO + "," + ocSERIE.MUNICIPIO;
                //Comprobante Comercio Exterior 1.1
                oComprobante.LugarExpedicion = ocSERIE.CP;

                //MessageBox.Show("Comprobante4");
                IFormatProvider culture = new CultureInfo("es-MX", true);
                DateTime INVOICE_DATE = oFACTURA.traer_INVOICE_DATE(oFACTURA.INVOICE_ID); //oFACTURA.INVOICE_DATE;
                DateTime CREATE_DATE = oFACTURA.traer_CREATE_DATE(oFACTURA.INVOICE_ID); //oFACTURA.CREATE_DATE;

                //MessageBox.Show("Comprobante5");
                try
                {
                    //FECHA_FACTURA = DateTime.Parse(INVOICE_DATE.ToString("dd/MM/yyyy") + " " + CREATE_DATE.ToString("HH:mm:ss"));
                    INVOICE_DATE = DateTime.Parse(INVOICE_DATE.Day.ToString() + "/" + INVOICE_DATE.Month.ToString() + "/" + INVOICE_DATE.Year.ToString() + " " + CREATE_DATE.ToString("HH:mm:ss"), culture);
                }
                catch
                {
                    try
                    {
                        FECHA_FACTURA = DateTime.Parse(INVOICE_DATE.Day.ToString() + "/" + INVOICE_DATE.Month.ToString() + "/" + INVOICE_DATE.Year.ToString() + " " + CREATE_DATE.ToString("HH:mm:ss"));
                    }
                    catch
                    {
                        //try
                        //{
                        //    FECHA_FACTURA = DateTime.Parse(INVOICE_DATE.ToString("dd7/M/yyyy") + " " + CREATE_DATE.ToString("HH:mm:ss"));
                        //}
                        //catch (Exception ej)
                        //{
                        //    MessageBox.Show(ej.Message);
                        //}
                    }
                }
                //Obtener la hora de Mexico DF
                //try
                //{
                //    var remoteTimeZone = TimeZoneInfo.FindSystemTimeZoneById("Central Standard Time (Mexico)");
                //    INVOICE_DATE = DateTime.Parse(INVOICE_DATE.Day.ToString() + "/" + INVOICE_DATE.Month.ToString() + "/" + INVOICE_DATE.Year.ToString() + " " + TimeZoneInfo.ConvertTime(DateTime.Now, remoteTimeZone).ToString("HH:mm:ss"));
                //}
                //catch
                //{
                //    Usar la fecha normal
                //    INVOICE_DATE = DateTime.Parse(INVOICE_DATE.Day.ToString() + "/" + INVOICE_DATE.Month.ToString() + "/" + INVOICE_DATE.Year.ToString() + " " + CREATE_DATE.ToString("HH:mm:ss"));
                //}

                //Verificar si se le baja la fecha INVOICE_DATE.AddHours(-1);
                int menos_hora = 0;
                try
                {
                    menos_hora = int.Parse(ConfigurationManager.AppSettings["menos_hora"].ToString());
                }
                catch { }
                //MessageBox.Show("Comprobante6");
                oComprobante.fecha = INVOICE_DATE.AddHours(menos_hora); ;//FECHA_FACTURA;

                //Aprobacion
                //Tipo de Comprobante
                switch (oFACTURA.TYPE)
                {
                    case "I":
                        oComprobante.tipoDeComprobante = ComprobanteTipoDeComprobante.ingreso;
                        break;
                    case "M":
                        oComprobante.tipoDeComprobante = ComprobanteTipoDeComprobante.egreso;
                        oComprobante.condicionesDePago = "Contado";
                        break;
                    default:
                        oComprobante.tipoDeComprobante = ComprobanteTipoDeComprobante.egreso;
                        oComprobante.condicionesDePago = "Contado";
                        break;
                }
                //MessageBox.Show("Comprobante7");
                //Forma de Pago
                //Cambio de Rodrigo Escalona Forma de Pago
                oComprobante.formaDePago = oFACTURA.FORMA_DE_PAGO; //validacion("Pago en una sola exhibicion");

                if (oComprobante.tipoDeComprobante == ComprobanteTipoDeComprobante.egreso)
                {
                    /*
                    oComprobante.NumCtaPago = oFACTURA.CUENTA_BANCARIA.Trim();
                    if (oComprobante.NumCtaPago == "")
                    {
                        oComprobante.NumCtaPago = "NO IDENTIFICADO";
                    }

                    oComprobante.metodoDePago = oFACTURA.PAGO;
                    if (oComprobante.metodoDePago == "")
                    {
                        //oComprobante.metodoDePago = "NO IDENTIFICADO";
                        //Rodrigo Escalona
                        oComprobante.metodoDePago = "NA";
                    }

                    oComprobante.formaDePago = oFACTURA.FORMA_DE_PAGO;
                    if (oComprobante.formaDePago == "")
                    {
                        oComprobante.formaDePago = "NO IDENTIFICADO";
                    }
                    */
                    oComprobante.metodoDePago = "NA";
                    oComprobante.NumCtaPago = "0000";
                    oComprobante.formaDePago = "NA";

                }
                else
                {
                    if (oFACTURA.CUENTA_BANCARIA != "")
                    {
                        oComprobante.NumCtaPago = oFACTURA.CUENTA_BANCARIA;
                    }
                    else
                    {
                        if (oFACTURA.CUENTA_BANCARIA.Trim() == "")
                        {
                            //oComprobante.NumCtaPago = "NO IDENTIFICADO";
                            //Rodrigo Escalona: 20-1-2014 Numero de Cuenta de Pago no Requerida
                            oComprobante.NumCtaPago = "";
                        }
                        else
                        {
                            oComprobante.NumCtaPago = oFACTURA.CUENTA_BANCARIA.Substring(oFACTURA.CUENTA_BANCARIA.Length - 4, 4);
                        }
                    }
                    if (oFACTURA.PAGO != "")
                    {
                        oComprobante.metodoDePago = validacion(oFACTURA.PAGO);
                    }
                    else
                    {
                        if (oFACTURA.PAGO.Trim() == "")
                        {
                            //Reforma de 14-07-2016
                            oComprobante.metodoDePago = "NA";
                        }
                        else
                        {
                            oComprobante.metodoDePago = validacion(oFACTURA.PAGO);
                        }
                    }

                    //Metodo de Pago: Eliminar las letras
                    if (!oComprobante.metodoDePago.Contains("NA"))
                    {
                        oComprobante.metodoDePago = Regex.Replace(oComprobante.metodoDePago, "[^0-9,]", "");
                    }


                    if (oComprobante.metodoDePago.Length == 0)
                    {
                        MessageBox.Show("El metodo de pago esta en blanco, asigne alguno.", Application.ProductName + "-" + Application.ProductVersion, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                        return false;
                        //oComprobante.metodoDePago = "98";
                    }
                    else
                    {
                        if (!cUtilidades.validarMetodopago(oComprobante.metodoDePago))
                        {
                            MessageBox.Show("El metodo de pago " + oComprobante.metodoDePago + " no es valido.", Application.ProductName + "-" + Application.ProductVersion, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                            return false;
                        }
                    }

                }



                //Certificado
                //Para la Version V3
                //MessageBox.Show("Comprobante8");
                X509Certificate cert = X509Certificate.CreateFromCertFile(ocCERTIFICADO.CERTIFICADO);
                string NoSerie = cert.GetSerialNumberString();
                string Certificado64 = System.Convert.ToBase64String(cert.GetRawCertData());
                oComprobante.certificado = Certificado64;
                //MessageBox.Show("Comprobante9");
                //Version 3
                oComprobante.noCertificado = validacion(ocCERTIFICADO.NO_CERTIFICADO);

                bool descuentoTr = false;
                try
                {
                    descuentoTr = bool.Parse(oEncapsular.ocEMPRESA.DESCUENTO_LINEA);
                }
                catch
                {
                }


                //SubTotal
                if (oFACTURA.anticipo)
                {
                    oComprobante.subTotal = Math.Abs(oFACTURA.TOTAL_AMOUNT);
                }
                else
                {
                    oComprobante.subTotal = Math.Abs(oFACTURA.TOTAL_AMOUNT) + Math.Abs(oFACTURA.DESCUENTO_TOTAL);


                    if (descuentoTr)
                    {
                        oComprobante.subTotal = Math.Abs(oFACTURA.TOTAL_AMOUNT);
                    }
                    else
                    {
                        oComprobante.subTotal = Math.Abs(oFACTURA.TOTAL_AMOUNT) + oFACTURA.DESCUENTO_TOTAL;
                    }

                }





                //MessageBox.Show("Comprobante10");
                if (oFACTURA.DESCUENTO_TOTAL != 0)
                {
                    if (!descuentoTr)
                    {
                        oComprobante.descuento = oFACTURA.DESCUENTO_TOTAL;
                        oComprobante.descuentoSpecified = true;
                        //Rodrigo Escalona
                        //Cambio de decimales
                        //10-7-2016
                        //oComprobante.descuento = Math.Round(oComprobante.descuento, valorUnitariodecimales);
                        //oComprobante.descuento = otroTruncar(oFACTURA.DESCUENTO_TOTAL, ocEMPRESA);
                    }

                }
                //Total
                //SubTotal – Descuentos + IVA = Total = Total de la Factura.

                if (oComprobante.tipoDeComprobante == ComprobanteTipoDeComprobante.ingreso)
                {
                    if (oFACTURA.anticipo)
                    {
                        oComprobante.total = Math.Abs(oFACTURA.TOTAL_VAT_AMOUNT);
                    }
                    else
                    {
                        if (descuentoTr)
                        {
                            oComprobante.total = (Math.Abs((oComprobante.subTotal + oFACTURA.TOTAL_VAT_AMOUNT) - Math.Abs(oFACTURA.TOTAL_RETENIDO)));
                        }
                        else
                        {
                            oComprobante.total = (Math.Abs((oComprobante.subTotal + oFACTURA.TOTAL_VAT_AMOUNT) - Math.Abs(oFACTURA.TOTAL_RETENIDO))) - oFACTURA.DESCUENTO_TOTAL;
                        }
                    }
                    //- oFACTURA.DESCUENTO_TOTAL;
                }

                if (oComprobante.tipoDeComprobante == ComprobanteTipoDeComprobante.egreso)
                {
                    if (oFACTURA.anticipo)
                    {
                        oComprobante.total = Math.Abs(oFACTURA.TOTAL_VAT_AMOUNT);
                    }
                    else
                    {
                        if (oFACTURA.DESCUENTO_TOTAL == 0)
                        {
                            oComprobante.total = Math.Abs(oFACTURA.TOTAL_AMOUNT + oFACTURA.TOTAL_VAT_AMOUNT);
                            oComprobante.descuento = 0;
                            oComprobante.descuentoSpecified = true;
                        }
                        else
                        {
                            oComprobante.total = Math.Abs(oFACTURA.TOTAL_AMOUNT);
                        }
                    }
                }
                //Rodrigo Escalona
                //Cambio de decimales
                //10-7-2016
                oComprobante.subTotal = otroTruncar(oComprobante.subTotal, oEncapsular.ocEMPRESA);
                oComprobante.total = otroTruncar(oComprobante.total, oEncapsular.ocEMPRESA);
                //Elemento Emisor
                ComprobanteEmisor oComprobanteEmisor = new ComprobanteEmisor();

                //Cargar los datos de la Compañia apartir del Certificado
                cert = X509Certificate.CreateFromCertFile(ocCERTIFICADO.CERTIFICADO);
                string Dato = cert.GetName();
                //MessageBox.Show("Comprobante11");

                char[] delimiterChars = { ',' };
                string[] TipoCer = Dato.Split(delimiterChars);

                //RFC Posicion 4
                string RFC_Certificado = TipoCer[4];
                char[] RFC_Delimiter = { '=' };
                string[] RFC_Arreglo = RFC_Certificado.Split(RFC_Delimiter);
                string RFC_Arreglo_Certificado = RFC_Arreglo[1];
                RFC_Arreglo_Certificado = RFC_Arreglo_Certificado.Replace("\"", "");
                RFC_Arreglo_Certificado = RFC_Arreglo_Certificado.Replace("/", "");
                RFC_Arreglo_Certificado = RFC_Arreglo_Certificado.Trim();
                //MessageBox.Show("Comprobante12");
                //RFC
                oComprobanteEmisor.rfc = validacion(oEncapsular.ocEMPRESA.RFC);
                //Nombre
                oComprobanteEmisor.nombre = validacion(oEncapsular.ocEMPRESA.ID);
                //Regimen Fiscal
                ComprobanteEmisorRegimenFiscal[] oComprobanteEmisorRegimenFiscal = new ComprobanteEmisorRegimenFiscal[1];
                oComprobanteEmisorRegimenFiscal[0] = new ComprobanteEmisorRegimenFiscal();
                oComprobanteEmisorRegimenFiscal[0].Regimen = oEncapsular.ocEMPRESA.REGIMEN_FISCAL;
                oComprobanteEmisor.RegimenFiscal = oComprobanteEmisorRegimenFiscal;
                //MessageBox.Show("Comprobante13");
                t_UbicacionFiscal ot_UbicacionFiscal = new t_UbicacionFiscal();

                //Domicilio Fiscal
                if (oEncapsular.ocEMPRESA.CALLE != "")
                {
                    ot_UbicacionFiscal.calle = validacion(oEncapsular.ocEMPRESA.CALLE);
                }
                if (oEncapsular.ocEMPRESA.EXTERIOR != "")
                {
                    ot_UbicacionFiscal.noExterior = validacion(oEncapsular.ocEMPRESA.EXTERIOR);
                }
                if (oEncapsular.ocEMPRESA.INTERIOR != "")
                {
                    ot_UbicacionFiscal.noInterior = validacion(oEncapsular.ocEMPRESA.INTERIOR);
                }
                if (oEncapsular.ocEMPRESA.COLONIA != "")
                {
                    ot_UbicacionFiscal.colonia = validacion(oEncapsular.ocEMPRESA.COLONIA);
                }
                if (oEncapsular.ocEMPRESA.LOCALIDAD != "")
                {
                    ot_UbicacionFiscal.localidad = validacion(oEncapsular.ocEMPRESA.LOCALIDAD);
                }
                if (oEncapsular.ocEMPRESA.REFERENCIA != "")
                {
                    ot_UbicacionFiscal.referencia = validacion(oEncapsular.ocEMPRESA.REFERENCIA);
                }
                if (oEncapsular.ocEMPRESA.MUNICIPIO != "")
                {
                    ot_UbicacionFiscal.municipio = validacion(oEncapsular.ocEMPRESA.MUNICIPIO);
                }
                if (oEncapsular.ocEMPRESA.ESTADO != "")
                {
                    ot_UbicacionFiscal.estado = validacion(oEncapsular.ocEMPRESA.ESTADO);
                }
                if (oEncapsular.ocEMPRESA.PAIS != "")
                {
                    ot_UbicacionFiscal.pais = validacion(oEncapsular.ocEMPRESA.PAIS);
                }
                if (oEncapsular.ocEMPRESA.CP != "")
                {
                    ot_UbicacionFiscal.codigoPostal = validacion(oEncapsular.ocEMPRESA.CP);
                }
                //MessageBox.Show("Comprobante14");
                oComprobanteEmisor.DomicilioFiscal = ot_UbicacionFiscal;
                //oFACTURA.ocSERIE.cargar(oSERIE.ROW_ID);

                //Expedido En
                t_Ubicacion ot_Ubicacion = new t_Ubicacion();
                if (ocSERIE.CALLE != "")
                {
                    ot_Ubicacion.calle = validacion(ocSERIE.CALLE);
                }
                if (ocSERIE.EXTERIOR != "")
                {
                    ot_Ubicacion.noExterior = validacion(ocSERIE.EXTERIOR);
                }
                if (ocSERIE.INTERIOR != "")
                {
                    ot_Ubicacion.noInterior = validacion(ocSERIE.INTERIOR);
                }
                if (ocSERIE.COLONIA != "")
                {
                    ot_Ubicacion.colonia = validacion(ocSERIE.COLONIA);
                }
                if (ocSERIE.LOCALIDAD != "")
                {
                    ot_Ubicacion.localidad = validacion(ocSERIE.LOCALIDAD);
                }
                if (ocSERIE.REFERENCIA != "")
                {
                    ot_Ubicacion.referencia = validacion(ocSERIE.REFERENCIA);
                }
                if (ocSERIE.MUNICIPIO != "")
                {
                    ot_Ubicacion.municipio = validacion(ocSERIE.MUNICIPIO);
                }
                if (ocSERIE.ESTADO != "")
                {
                    ot_Ubicacion.estado = validacion(ocSERIE.ESTADO);
                }
                if (ocSERIE.PAIS != "")
                {
                    ot_Ubicacion.pais = validacion(ocSERIE.PAIS);
                }
                if (ocSERIE.CP != "")
                {
                    ot_Ubicacion.codigoPostal = validacion(ocSERIE.CP);
                }

                oComprobanteEmisor.ExpedidoEn = ot_Ubicacion;

                oComprobante.Emisor = oComprobanteEmisor;
                //MessageBox.Show("Comprobante15");
                //Elemento Receptor
                ComprobanteReceptor oComprobanteReceptor = new ComprobanteReceptor();
                oComprobanteReceptor.rfc = validacion(oFACTURA.VAT_REGISTRATION);
                //&amp;
                oComprobanteReceptor.rfc = validacion_AMP(oComprobanteReceptor.rfc);
                oComprobanteReceptor.nombre = validacion(oFACTURA.BILL_TO_NAME);
                //&amp;
                oComprobanteReceptor.nombre = validacion_AMP(oComprobanteReceptor.nombre);
                //MessageBox.Show("Comprobante16");
                //if (bool.Parse(ConfigurationManager.AppSettings["DomicilioReceptor"].ToString()))
                if (true)
                {
                    t_Ubicacion ot_UbicacionReceptor = new t_Ubicacion();
                    //Verificar
                    if (oFACTURA.BILL_TO_ADDR_1.Trim() != "")
                    {
                        ot_UbicacionReceptor.calle = extraeDireccion(validacion(oFACTURA.BILL_TO_ADDR_1));
                        string exterior = extraeExterior(validacion(oFACTURA.BILL_TO_ADDR_1));
                        if (exterior != "")
                        {
                            ot_UbicacionReceptor.noExterior = exterior;
                        }
                        string interior = extraeInterior(validacion(oFACTURA.BILL_TO_ADDR_1));
                        if (interior != "")
                        {
                            ot_UbicacionReceptor.noInterior = interior;
                        }

                    }

                    string localidad = extraeLocalidad(validacion(oFACTURA.BILL_TO_ADDR_3));
                    if (localidad != "")
                    {
                        ot_UbicacionReceptor.localidad = localidad;
                    }

                    if (oFACTURA.BILL_TO_ADDR_2.Trim() != "")
                    {
                        ot_UbicacionReceptor.colonia = validacion(oFACTURA.BILL_TO_ADDR_2);
                    }
                    if (oFACTURA.BILL_TO_STATE.Trim() != "")
                    {
                        //ot_UbicacionReceptor.localidad = validacion(oFACTURA.BILL_TO_STATE);

                    }
                    if (oFACTURA.BILL_TO_STATE.Trim() != "")
                    {
                        ot_UbicacionReceptor.estado = validacion(oFACTURA.BILL_TO_STATE);
                    }
                    //ot_UbicacionReceptor.referencia = "";
                    if (oFACTURA.BILL_TO_CITY.Trim() != "")
                    {
                        ot_UbicacionReceptor.municipio = validacion(oFACTURA.BILL_TO_CITY);
                    }
                    ot_UbicacionReceptor.pais = validacion(oFACTURA.BILL_TO_COUNTRY);
                    //Validar el pais



                    if (oFACTURA.BILL_TO_ZIPCODE.Trim() != "")
                    {
                        ot_UbicacionReceptor.codigoPostal = validacion(oFACTURA.BILL_TO_ZIPCODE);
                    }
                    oComprobanteReceptor.Domicilio = ot_UbicacionReceptor;
                }
                oComprobante.Receptor = oComprobanteReceptor;
                //Agregar direccion de Destino
                //MessageBox.Show("Comprobante17");


                //Elemento Conceptos
                ComprobanteConcepto[] oComprobanteConceptos;
                oComprobanteConceptos = new ComprobanteConcepto[oFACTURA.LINEAS.Length];
                for (int i = 0; i < oFACTURA.LINEAS.Length; i++)
                {
                    if (oFACTURA.LINEAS[i] != null)
                    {
                        //MessageBox.Show("Comprobante18");
                        ComprobanteConcepto oComprobanteConcepto = new ComprobanteConcepto();
                        oComprobanteConcepto.cantidad = Math.Abs(decimal.Parse(oFACTURA.LINEAS[i].QTY));
                        if (oFACTURA.LINEAS[i].STOCK_UM.Trim() != "")
                        {
                            oComprobanteConcepto.unidad = oFACTURA.LINEAS[i].STOCK_UM;
                        }
                        else
                        {
                            oComprobanteConcepto.unidad = "NO APLICA";
                        }
                        if (oFACTURA.anticipo)
                        {
                            oFACTURA.LINEAS[i].AMOUNT = (decimal.Parse(oFACTURA.LINEAS[i].AMOUNT) - oFACTURA.anticipoAMOUNT).ToString();
                            oFACTURA.LINEAS[i].UNIT_PRICE = 
                                (Math.Round(decimal.Parse(oFACTURA.LINEAS[i].AMOUNT) / oComprobanteConcepto.cantidad
                                , 4)).ToString();
                        }

                        //Agregar el producto PART_ID
                        if (oFACTURA.LINEAS[i].PART_ID != "")
                        {
                            oComprobanteConcepto.noIdentificacion = oFACTURA.LINEAS[i].PART_ID;
                        }
                        //MessageBox.Show("Comprobante19");
                        oComprobanteConcepto.descripcion = validacion(oFACTURA.LINEAS[i].DESCRIPCION);

                        decimal unitPrice = 0;
                        try
                        {
                            unitPrice = decimal.Parse(oFACTURA.LINEAS[i].UNIT_PRICE);
                        }
                        catch
                        {
                            unitPrice = Math.Round(decimal.Parse(oFACTURA.LINEAS[i].AMOUNT) / oComprobanteConcepto.cantidad, 2);
                        }
                        oComprobanteConcepto.valorUnitario = Math.Round(Math.Abs(unitPrice), 4); //Math.Abs(Math.Round(decimal.Parse(oFACTURA.LINEAS[i].UNIT_PRICE), valorUnitariodecimales)); //Math.Abs(decimal.Parse(oFACTURA.LINEAS[i].UNIT_PRICE)).ToString();


                        //MessageBox.Show("Comprobante19- Importe");
                        //Verificar que sea anticipo
                        if (oFACTURA.TYPE == "I")
                        {
                            oComprobanteConcepto.importe = decimal.Parse(oFACTURA.LINEAS[i].AMOUNT);
                        }
                        else
                        {
                            //oComprobanteConcepto.importe = Math.Abs(decimal.Parse(oFACTURA.LINEAS[i].AMOUNT));
                            oComprobanteConcepto.importe = decimal.Parse(oFACTURA.LINEAS[i].AMOUNT);
                        }


                        oComprobanteConcepto.importe = Math.Abs(oComprobanteConcepto.importe); //Math.Round(oComprobanteConcepto.importe, 4);
                        oComprobanteConcepto.valorUnitario = Math.Abs(oComprobanteConcepto.valorUnitario);//Math.Round(oComprobanteConcepto.valorUnitario, 4);

                        oComprobanteConcepto.importe = otroTruncar(oComprobanteConcepto.importe, oEncapsular.ocEMPRESA);


                        //MessageBox.Show("Comprobante20");
                        //Informacion del Pedimento
                        cPEDIMENTO ocPEDIMENTO = new cPEDIMENTO(oEncapsular.oData_ERP, oEncapsular.ocEMPRESA);
                        //MessageBox.Show(oData_ERP.sConn);
                        List<cPEDIMENTO> lista = ocPEDIMENTO.obtener_linea(oFACTURA.INVOICE_ID, oFACTURA.LINEAS[i].LINEA, oEncapsular.ocEMPRESA);
                        try
                        {
                            if (lista != null)
                            {
                                if (lista.Count > 0)
                                {
                                    t_InformacionAduanera[] ot_InformacionAduanera;
                                    ot_InformacionAduanera = new t_InformacionAduanera[lista.Count];
                                    for (int t = 0; t < lista.Count; t++)
                                    {
                                        ot_InformacionAduanera[t] = new t_InformacionAduanera();
                                        ot_InformacionAduanera[t].aduana = lista[t].ADUANA;
                                        ot_InformacionAduanera[t].fecha = DateTime.Parse(lista[t].FECHA);
                                        ot_InformacionAduanera[t].numero = lista[t].PEDIMENTO;
                                    }
                                    oComprobanteConcepto.Items = ot_InformacionAduanera;
                                }
                            }
                        }
                        catch (Exception excepPeddmentos)
                        {
                            MessageBox.Show("Error cargar de Pedimentos " + excepPeddmentos.InnerException.ToString()
                                , Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error);
                        }
                        //MessageBox.Show("Comprobante21");
                        oComprobanteConceptos[i] = new ComprobanteConcepto();
                        oComprobanteConceptos[i] = oComprobanteConcepto;
                    }
                }
                //MessageBox.Show("Comprobante22");
                oComprobante.Conceptos = oComprobanteConceptos;

                //MessageBox.Show("Comprobante23");
                //Elemento Impuestos
                ComprobanteImpuestos oImpuesto = new ComprobanteImpuestos();
                //Impuestos  Trasladados

                oImpuesto.totalImpuestosTrasladadosSpecified = true;
                if (oFACTURA.anticipo)
                {
                    oImpuesto.totalImpuestosTrasladados = Math.Abs(oFACTURA.TOTAL_VAT_AMOUNT) - oFACTURA.anticipoVAT_AMOUNT;
                }
                else
                {
                    oImpuesto.totalImpuestosTrasladados = Math.Abs(oFACTURA.TOTAL_VAT_AMOUNT);
                }


                //Impuestos  Trasladados
                ComprobanteImpuestosTraslado[] oComprobanteImpuestosTraslados;
                oComprobanteImpuestosTraslados = new ComprobanteImpuestosTraslado[2];
                //IVA
                ComprobanteImpuestosTraslado oComprobanteImpuestosTraslado = new ComprobanteImpuestosTraslado();
                if (oFACTURA.anticipo)
                {
                    oComprobanteImpuestosTraslado.importe = Math.Abs(oFACTURA.TOTAL_VAT_AMOUNT) - oFACTURA.anticipoVAT_AMOUNT;
                }
                else
                {
                    oComprobanteImpuestosTraslado.importe = Math.Abs(oFACTURA.TOTAL_VAT_AMOUNT);
                }

                oComprobanteImpuestosTraslado.tasa = oFACTURA.VAT_PERCENT;
                oComprobanteImpuestosTraslado.impuesto = ComprobanteImpuestosTrasladoImpuesto.IVA;
                oComprobanteImpuestosTraslados[0] = new ComprobanteImpuestosTraslado();
                oComprobanteImpuestosTraslados[0] = oComprobanteImpuestosTraslado;
                //IEPS
                oComprobanteImpuestosTraslado = new ComprobanteImpuestosTraslado();
                oComprobanteImpuestosTraslado.importe = 0;
                oComprobanteImpuestosTraslado.tasa = 0;
                oComprobanteImpuestosTraslado.impuesto = ComprobanteImpuestosTrasladoImpuesto.IEPS;
                oComprobanteImpuestosTraslados[1] = new ComprobanteImpuestosTraslado();
                oComprobanteImpuestosTraslados[1] = oComprobanteImpuestosTraslado;
                oImpuesto.Traslados = oComprobanteImpuestosTraslados;
                //Retenidos
                if (oFACTURA.TOTAL_RETENIDO != 0)
                {
                    ComprobanteImpuestosRetencion[] oComprobanteImpuestosRetencion = new ComprobanteImpuestosRetencion[1];
                    oComprobanteImpuestosRetencion[0] = new ComprobanteImpuestosRetencion();
                    oComprobanteImpuestosRetencion[0].importe = Math.Abs(oFACTURA.TOTAL_RETENIDO);
                    oComprobanteImpuestosRetencion[0].impuesto = ComprobanteImpuestosRetencionImpuesto.IVA;
                    oImpuesto.Retenciones = oComprobanteImpuestosRetencion;
                    oImpuesto.totalImpuestosRetenidosSpecified = true;
                    oImpuesto.totalImpuestosRetenidos = Math.Abs(oFACTURA.TOTAL_RETENIDO);
                }
                oComprobante.Impuestos = oImpuesto;
                //FIMA REDONDEO

                if (oEncapsular.ocEMPRESA.ENTITY_ID.Contains("FIMA"))
                //if (false)
                {
                    //Recalcular todo el XML con todos los decimales
                    decimal subTotal = 0;
                    int i = 0;
                    foreach (ComprobanteConcepto oComprobanteConcepto in oComprobante.Conceptos)
                    {
                        if (oComprobanteConcepto != null)
                        {
                            subTotal += Math.Abs(oComprobanteConcepto.cantidad * oComprobanteConcepto.valorUnitario);
                            oComprobante.Conceptos[i].importe = Math.Abs(oComprobanteConcepto.cantidad * oComprobanteConcepto.valorUnitario);
                            oComprobante.Conceptos[i].importe = otroTruncar(oComprobante.Conceptos[i].importe, oEncapsular.ocEMPRESA);

                            i++;
                        }
                    }
                    oComprobante.subTotal = otroTruncar(subTotal, oEncapsular.ocEMPRESA);
                    oComprobante.TipoCambio = otroTruncar(decimal.Parse(oComprobante.TipoCambio), oEncapsular.ocEMPRESA).ToString();
                    //Tipo de cambio 6 decimales
                    oComprobante.TipoCambio = otroTruncar(decimal.Parse(oComprobante.TipoCambio), oEncapsular.ocEMPRESA).ToString();


                    if (oComprobante.Impuestos.totalImpuestosTrasladados != 0)
                    {
                        //oComprobante.Impuestos.totalImpuestosTrasladados = Math.Abs(subTotal)*decimal.Parse("0.16");
                        oComprobante.Impuestos.totalImpuestosTrasladados = oComprobante.total - oComprobante.subTotal;
                        if (oComprobante.Impuestos.Traslados.Length != 0)
                        {
                            //Buscar el iva
                            int imp = 0;
                            foreach (ComprobanteImpuestosTraslado impuesto in oComprobante.Impuestos.Traslados)
                            {
                                if (impuesto.impuesto == ComprobanteImpuestosTrasladoImpuesto.IVA)
                                {
                                    oComprobante.Impuestos.Traslados[imp].importe = oComprobante.Impuestos.totalImpuestosTrasladados;
                                    break;
                                }
                                imp++;
                            }
                        }
                        oComprobante.total = otroTruncar(Math.Abs(oComprobante.Impuestos.totalImpuestosTrasladados) - Math.Abs(oComprobante.Impuestos.totalImpuestosRetenidos) - Math.Abs(oComprobante.descuento) + Math.Abs(oComprobante.subTotal), oEncapsular.ocEMPRESA);
                    }
                    else
                    {
                        oComprobante.total = otroTruncar(Math.Abs(oComprobante.subTotal) - Math.Abs(oComprobante.Impuestos.totalImpuestosRetenidos) - Math.Abs(oComprobante.descuento), oEncapsular.ocEMPRESA);
                    }
                    //Fin de recalcular

                    //Formatear todo
                    //oComprobante.subTotal = otroTruncar(oComprobante.subTotal, ocEMPRESA);
                    //oComprobante.descuento = otroTruncar(oComprobante.descuento, ocEMPRESA);
                    //oComprobante.total = otroTruncar(oComprobante.total, ocEMPRESA);
                    //Lineas
                    oComprobante.total = otroTruncar(oComprobante.total, oEncapsular.ocEMPRESA);

                    i = 0;
                    foreach (ComprobanteConcepto oComprobanteConcepto in oComprobante.Conceptos)
                    {
                        if (oComprobanteConcepto != null)
                        {
                            oComprobante.Conceptos[i].cantidad = otroTruncar(oComprobante.Conceptos[i].cantidad, oEncapsular.ocEMPRESA);
                            oComprobante.Conceptos[i].valorUnitario = otroTruncar(oComprobante.Conceptos[i].valorUnitario, oEncapsular.ocEMPRESA);
                            oComprobante.Conceptos[i].importe = otroTruncar(oComprobante.Conceptos[i].importe, oEncapsular.ocEMPRESA);
                            i++;
                        }
                    }
                    if (oComprobante.Impuestos.totalImpuestosTrasladados != 0)
                    {
                        //oComprobante.Impuestos.totalImpuestosTrasladados = otroTruncar(oComprobante.Impuestos.totalImpuestosTrasladados, ocEMPRESA);
                        oComprobante.Impuestos.totalImpuestosTrasladados = oComprobante.total - oComprobante.subTotal;
                        if (oComprobante.Impuestos.Traslados.Length != 0)
                        {
                            int imp = 0;
                            foreach (ComprobanteImpuestosTraslado impuesto in oComprobante.Impuestos.Traslados)
                            {
                                if (impuesto.impuesto == ComprobanteImpuestosTrasladoImpuesto.IVA)
                                {
                                    oComprobante.Impuestos.Traslados[imp].importe = otroTruncar(oComprobante.Impuestos.totalImpuestosTrasladados, oEncapsular.ocEMPRESA); //otroTruncar(oComprobante.Impuestos.Traslados[imp].importe, ocEMPRESA);
                                    break;
                                }
                                imp++;
                            }
                        }
                    }


                }
                //FIMA REDONDEO

                //Rodrigo Cuadrar sub total
                //if (cConfiguracionGeneral.calcular_manualmente)
                //Validar que no tenga complemento
                cCCE ocCCE = new cCCE();
                bool cce = ocCCE.agregarComplemento(oEncapsular.ocEMPRESA, oFACTURA, oComprobante.Impuestos.totalImpuestosTrasladados);

                //oComprobante.total = (oComprobante.Impuestos.totalImpuestosTrasladados + subTotal) - oComprobante.Impuestos.totalImpuestosRetenidos - oComprobante.descuento;


                //Rodrigo Escalona
                //Agregar leyendaFiscal
                try
                {
                    if (oFACTURA.ESPECIFICACIONES == "")
                    {
                        cCLIENTE_CONFIGURACION ocCLIENTE_CONFIGURACION = new cCLIENTE_CONFIGURACION(oData);
                        ocCLIENTE_CONFIGURACION.cargar_CUSTOMER_ID(oFACTURA.CUSTOMER_ID);
                        if (ocCLIENTE_CONFIGURACION.LEYENDA_FISCAL != "")
                        {
                            oFACTURA.ESPECIFICACIONES = ocCLIENTE_CONFIGURACION.LEYENDA_FISCAL;
                            //Eliminar la especificación anterior

                            //Insertar la especificacion en la factura
                            string sSQL_insert = @"INSERT INTO [dbo].[RECEIVABLE_BINARY]  ([INVOICE_ID],[TYPE],[BITS],[BITS_LENGTH]) 
                                VALUES ('" + oFACTURA.INVOICE_ID + "','D',CAST('" + ocCLIENTE_CONFIGURACION.LEYENDA_FISCAL + @"' AS NVARBINARY(MAX)),
                                    @" + ocCLIENTE_CONFIGURACION.LEYENDA_FISCAL.Length.ToString() + ")";

                            string sSQL_sql = " SELECT TOP 1 * FROM  [dbo].[RECEIVABLE_BINARY] WHERE INVOICE_ID='" + oFACTURA.INVOICE_ID + "' AND TYPE='D' ";
                            bool insertar = true;
                            DataTable oDataTable = oEncapsular.oData_ERP.EjecutarConsulta(sSQL_sql);
                            if (oDataTable != null)
                            {
                                foreach (DataRow oDataRow in oDataTable.Rows)
                                {
                                    insertar = false;
                                }
                            }
                            if (insertar)
                            {
                                oEncapsular.oData_ERP.EjecutarConsulta(sSQL_insert);
                            }

                        }
                    }


                    if (oFACTURA.ESPECIFICACIONES != "")
                    {
                        oComprobante.Complemento = new ComprobanteComplemento();
                        LeyendasFiscales oLeyendasFiscales = new LeyendasFiscales();
                        oLeyendasFiscales.Leyenda = new LeyendasFiscalesLeyenda[1];
                        oLeyendasFiscales.Leyenda[0] = new LeyendasFiscalesLeyenda();
                        oLeyendasFiscales.Leyenda[0].textoLeyenda = oFACTURA.ESPECIFICACIONES;
                        oComprobante.Complemento.Any = new XmlElement[1];
                        XmlDocument doc = new XmlDocument();

                        XmlSerializerNamespaces myNamespaces_leyenda = new XmlSerializerNamespaces();
                        myNamespaces_leyenda.Add("leyendasFisc", "http://www.sat.gob.mx/leyendasFiscales");

                        using (XmlWriter writer = doc.CreateNavigator().AppendChild())
                        {
                            new XmlSerializer(oLeyendasFiscales.GetType()).Serialize(writer, oLeyendasFiscales, myNamespaces_leyenda);
                        }

                        int indice = oComprobante.Complemento.Any.Length - 1;
                        oComprobante.Complemento.Any[indice] = doc.DocumentElement;


                    }
                }
                catch (Exception exLeyendaFiscal)
                {
                    string mensaje = exLeyendaFiscal.Message.ToString();
                    if (exLeyendaFiscal.InnerException != null)
                    {
                        mensaje += Environment.NewLine + exLeyendaFiscal.InnerException.Message.ToString();
                    }
                    MessageBox.Show("Error en Leyenda Fiscal. " + mensaje
                        , Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return false;
                }

                //Complemento de Comercio Exterior
                if (oComprobante.tipoDeComprobante == ComprobanteTipoDeComprobante.ingreso)
                {
                    try
                    {
                        if (cce)
                        {
                            //Eliminar Domicilio
                            oComprobante.Emisor.DomicilioFiscal = null;
                            oComprobante.Emisor.ExpedidoEn = null;

                            //Cambia cosas del regiment
                            oComprobante.Emisor.RegimenFiscal[0].Regimen = "601";

                            //Aplicar complemento
                            oComprobante.Complemento = new ComprobanteComplemento();

                            ComercioExterior oComercioExterior = new ComercioExterior();

                            //Pedimento A1
                            //oComercioExterior.MotivoTraslado = c_MotivoTraslado.Item01;
                            //oComercioExterior.MotivoTrasladoSpecified = true;

                            oComercioExterior.ClaveDePedimentoSpecified = true;
                            oComercioExterior.ClaveDePedimento = c_ClavePedimento.A1;
                            oComercioExterior.CertificadoOrigen = 0;
                            oComercioExterior.TipoOperacion = c_TipoOperacion.Item2;
                            oComercioExterior.Subdivision = 0;
                            oComercioExterior.SubdivisionSpecified = true;
                            oComercioExterior.IncotermSpecified = true;
                            oComercioExterior.Incoterm = oFACTURA.FREE_ON_BOARD;

                            oComercioExterior.CertificadoOrigen = 0;
                            oComercioExterior.CertificadoOrigenSpecified = true;
                            oComercioExterior.TipoCambioUSDSpecified = true;
                            oComercioExterior.TipoCambioUSD = oFACTURA.SELL_RATE;


                            //Validar el TC si es MXN
                            //Aplicable a bradford
                            if (oEncapsular.ocEMPRESA.ENTITY_ID.Contains("BDM"))
                            {
                                if (oComprobante.Moneda.Equals("MXN"))
                                {
                                    if (decimal.Parse(oComprobante.TipoCambio) == 1)
                                    {
                                        oComercioExterior.TipoCambioUSD
                                            =
                                            otroTruncar(oFACTURA.tipoCambio("USD", oFACTURA.INVOICE_DATE, oEncapsular.oData_ERP)
                                            , oEncapsular.ocEMPRESA);

                                        oComercioExterior.TipoCambioUSD = 1;
                                    }
                                }
                            }


                            oComercioExterior.TipoCambioUSD = decimal.Round(oComercioExterior.TipoCambioUSD, 4);
                            oComercioExterior.TotalUSD = Math.Round(oComprobante.total, 2);
                            oComercioExterior.TotalUSDSpecified = true;

                            oComercioExterior.Receptor = new ComercioExteriorReceptor();
                            oComercioExterior.Receptor.NumRegIdTrib = oFACTURA.TAX_ID_NUMBER;

                            //Version 1.0
                            //oComercioExterior.Destinatario = new ComercioExteriorDestinatario();
                            //oComercioExterior.Destinatario.NumRegIdTrib = oFACTURA.TAX_ID_NUMBER;
                            //oComercioExterior.Destinatario.Nombre = oComprobante.Receptor.nombre;
                            //oComercioExterior.Destinatario.NumRegIdTrib = oFACTURA.TAX_ID_NUMBER;// oComprobante.Receptor.rfc;
                            //oComercioExterior.Destinatario.Domicilio = new ComercioExteriorDestinatarioDomicilio();
                            //if (oComprobante.Receptor.Domicilio.calle != null)
                            //{
                            //    oComercioExterior.Destinatario.Domicilio.Calle = oComprobante.Receptor.Domicilio.calle;
                            //}
                            //if (oComprobante.Receptor.Domicilio.noExterior != null)
                            //{
                            //    oComercioExterior.Destinatario.Domicilio.NumeroExterior = oComprobante.Receptor.Domicilio.noExterior;
                            //}
                            //if (oComprobante.Receptor.Domicilio.noInterior != null)
                            //{
                            //    oComercioExterior.Destinatario.Domicilio.NumeroInterior = oComprobante.Receptor.Domicilio.noInterior;
                            //}
                            //oComercioExterior.Destinatario.Domicilio.Pais = c_Pais.USA;
                            //if (oFACTURA.COUNTRY.ToUpper().Contains("USA"))
                            //{
                            //    oComercioExterior.Destinatario.Domicilio.Pais = c_Pais.USA;
                            //}
                            //if (oFACTURA.COUNTRY.ToUpper().Contains("CAN"))
                            //{
                            //    oComercioExterior.Destinatario.Domicilio.Pais = c_Pais.CAN;
                            //}

                            //if (oFACTURA.COUNTRY.ToUpper().Contains("GBR"))
                            //{
                            //    oComercioExterior.Destinatario.Domicilio.Pais = c_Pais.GBR;
                            //}

                            //oComercioExterior.Destinatario.Domicilio.CodigoPostal = oFACTURA.BILL_TO_ZIPCODE;//oComprobante.Receptor.Domicilio.codigoPostal;
                            //if (oFACTURA.ZIPCODE!="")
                            //{
                            //    oComercioExterior.Destinatario.Domicilio.CodigoPostal = oFACTURA.ZIPCODE;
                            //}
                            //if (oComprobante.Receptor.Domicilio.estado != null)
                            //{
                            //    oComercioExterior.Destinatario.Domicilio.Estado = oComprobante.Receptor.Domicilio.estado;
                            //}

                            //Version 1.1
                            oComercioExterior.Destinatario = new ComercioExteriorDestinatario[1];
                            oComercioExterior.Destinatario[0] = new ComercioExteriorDestinatario();
                            oComercioExterior.Destinatario[0].Nombre = oComprobante.Receptor.nombre;
                            oComercioExterior.Destinatario[0].NumRegIdTrib = oFACTURA.TAX_ID_NUMBER;// oComprobante.Receptor.rfc;
                                                                                                    //if (oFACTURA.TAX_ID_NUMBER != null)
                                                                                                    //{
                                                                                                    //    oComercioExterior.Destinatario.NumRegIdTrib = oFACTURA.TAX_ID_NUMBER;
                                                                                                    //}
                            oComercioExterior.Destinatario[0].Domicilio = new ComercioExteriorDestinatarioDomicilio[1];
                            oComercioExterior.Destinatario[0].Domicilio[0] = new ComercioExteriorDestinatarioDomicilio();
                            if (oComprobante.Receptor.Domicilio.calle != null)
                            {
                                oComercioExterior.Destinatario[0].Domicilio[0].Calle = oComprobante.Receptor.Domicilio.calle.Replace(".", "");
                            }
                            if (oComprobante.Receptor.Domicilio.noExterior != null)
                            {
                                oComercioExterior.Destinatario[0].Domicilio[0].NumeroExterior = oComprobante.Receptor.Domicilio.noExterior;
                            }
                            if (oComprobante.Receptor.Domicilio.noInterior != null)
                            {
                                oComercioExterior.Destinatario[0].Domicilio[0].NumeroInterior = oComprobante.Receptor.Domicilio.noInterior;
                            }
                            oComercioExterior.Destinatario[0].Domicilio[0].Pais = c_Pais.USA;

                            if (oFACTURA.COUNTRY.ToUpper().Contains("IND"))
                            {
                                oComercioExterior.Destinatario[0].Domicilio[0].Pais = c_Pais.IND;
                            }

                            if (oFACTURA.COUNTRY.ToUpper().Contains("DOM"))
                            {
                                oComercioExterior.Destinatario[0].Domicilio[0].Pais = c_Pais.DOM;
                            }
                            if (oFACTURA.COUNTRY.ToUpper().Contains("USA"))
                            {
                                oComercioExterior.Destinatario[0].Domicilio[0].Pais = c_Pais.USA;
                            }
                            if (oFACTURA.COUNTRY.ToUpper().Contains("CAN"))
                            {
                                oComercioExterior.Destinatario[0].Domicilio[0].Pais = c_Pais.CAN;
                            }

                            if (oFACTURA.COUNTRY.ToUpper().Contains("GBR"))
                            {
                                oComercioExterior.Destinatario[0].Domicilio[0].Pais = c_Pais.GBR;
                            }

                            oComercioExterior.Destinatario[0].Domicilio[0].CodigoPostal = oFACTURA.BILL_TO_ZIPCODE;//oComprobante.Receptor.Domicilio.codigoPostal;
                            oComercioExterior.Destinatario[0].Domicilio[0].Estado = oComprobante.Receptor.Domicilio.estado;

                            bool direccionSLP = false;
                            try
                            {
                                if (bool.Parse(ConfigurationManager.AppSettings["direccionSLP"].ToString()))
                                {
                                    direccionSLP = true;
                                }
                            }
                            catch
                            {

                            }

                            if (!direccionSLP)
                            {
                                if (oFACTURA.ZIPCODE != "")
                                {
                                    oComercioExterior.Destinatario[0].Domicilio[0].Estado = oFACTURA.STATE;
                                    oComercioExterior.Destinatario[0].Domicilio[0].CodigoPostal = oFACTURA.ZIPCODE;


                                    //


                                }

                                oComprobante.Receptor.Domicilio.codigoPostal = oComercioExterior.Destinatario[0].Domicilio[0].CodigoPostal;
                                oComprobante.Receptor.Domicilio.estado = oComercioExterior.Destinatario[0].Domicilio[0].Estado;
                                oComprobante.Receptor.Domicilio.pais = oComercioExterior.Destinatario[0].Domicilio[0].Pais.ToString();
                                //Rodrigo Escalona: 08/06/2016

                                if (oFACTURA.STATE != "")
                                {
                                    //oComercioExterior.Destinatario[0].Domicilio[0].Estado = oFACTURA.STATE;
                                    oComercioExterior.Destinatario[0].Domicilio[0].CodigoPostal = oFACTURA.ZIPCODE;
                                }
                            }



                            //if (oComprobante.Receptor.Domicilio.estado != null)
                            //{
                            //    oComercioExterior.Destinatario[0].Domicilio[0].Estado = oComprobante.Receptor.Domicilio.estado;
                            //}/


                            //}

                            //if (oComprobante.Receptor.Domicilio.localidad != null)
                            //{
                            //    oComercioExterior.Destinatario.Domicilio.Localidad = oComprobante.Receptor.Domicilio.localidad;
                            //}

                            //if (oComprobante.Receptor.Domicilio.municipio != null)
                            //{
                            //    oComercioExterior.Destinatario.Domicilio.Municipio = oComprobante.Receptor.Domicilio.municipio;
                            //}

                            //if (oComprobante.Receptor.Domicilio.referencia != null)
                            //{
                            //    oComercioExterior.Destinatario.Domicilio.Referencia = oComprobante.Receptor.Domicilio.referencia;
                            //}



                            //oComprobante.Receptor.Domicilio = null;
                            //oComprobante.Receptor.ExpedidoEn = null;
                            //

                            oComercioExterior.Mercancias = new ComercioExteriorMercancia[oComprobante.Conceptos.Length];
                            int i = 0;
                            decimal TotalUSD = 0;
                            foreach (ComprobanteConcepto concepto in oComprobante.Conceptos)
                            {
                                //Truncar el importe
                                oComprobante.Conceptos[i].importe = otroTruncar(oComprobante.Conceptos[i].importe, oEncapsular.ocEMPRESA);

                                oComercioExterior.Mercancias[i] = new ComercioExteriorMercancia();
                                oComercioExterior.Mercancias[i].CantidadAduanaSpecified = true;

                                decimal CantidadAduana = decimal.Parse(concepto.cantidad.ToString("#.##"));
                                oComercioExterior.Mercancias[i].CantidadAduana = CantidadAduana;
                                oComercioExterior.Mercancias[i].UnidadAduanaSpecified = true;

                                oComercioExterior.Mercancias[i].NoIdentificacion = concepto.noIdentificacion;

                                ocCCE.cargar(oEncapsular.ocEMPRESA.ROW_ID);

                                string fraccion = ocCCE.obtenerFraccion(concepto.noIdentificacion
                                    , oFACTURA.INVOICE_ID, "1", oEncapsular.oData_ERP, concepto.descripcion);
                                if (fraccion.Length == 0)
                                {
                                    string mensaje = "Error en CCE. Fracción Arancelaría del Producto " + concepto.noIdentificacion + ". No existe";
                                    MessageBox.Show(mensaje, Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error);
                                    return false;
                                }
                                try
                                {
                                    //c_FraccionArancelaria oc_FraccionArancelaria = GetFraccionArancelaria(fraccion);
                                    c_FraccionArancelaria oc_FraccionArancelaria = (c_FraccionArancelaria)System.Enum.Parse(typeof(c_FraccionArancelaria), "Item" + fraccion);

                                    oComercioExterior.Mercancias[i].FraccionArancelaria = oc_FraccionArancelaria;
                                    //Version 1.1
                                    oComercioExterior.Mercancias[i].UnidadAduana = c_UnidadAduana.Item06;
                                    //oComercioExterior.Mercancias[i].UnidadAduana = //cFraccionUnidad11.obtenerUM(fraccion);

                                    //Verson 1.0
                                    //oComercioExterior.Mercancias[i].UnidadAduana = c_UnidadMedidaAduana.Item6;
                                    //oComercioExterior.Mercancias[i].UnidadAduana = cFraccionUnidad10.obtenerUM(fraccion);

                                    //Buscar la Unidad de aduana de la fraccion aranceralaria
                                    //oComercioExterior.Mercancias[i].UnidadAduana = cFraccionUnidad.obtenerUM(fraccion);
                                }
                                catch (Exception exFraccion)
                                {
                                    string mensaje = "Error en CCE. Fracción Arancelaría (Valor: noIdentificacion=" + fraccion + "). " + exFraccion.Message.ToString();
                                    if (exFraccion.InnerException != null)
                                    {
                                        mensaje += Environment.NewLine + exFraccion.InnerException.Message.ToString();
                                    }
                                    MessageBox.Show(mensaje, Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error);
                                    return false;
                                }

                                oComercioExterior.Mercancias[i].NoIdentificacion = concepto.noIdentificacion;
                                //Agergar el id
                                try
                                {
                                    if (bool.Parse(ConfigurationManager.AppSettings["cceMostrarIndicador"].ToString()))
                                    {
                                        oComercioExterior.Mercancias[i].NoIdentificacion = concepto.noIdentificacion + i.ToString();
                                    }
                                }
                                catch
                                {

                                }


                                oComprobante.Conceptos[i].noIdentificacion = oComercioExterior.Mercancias[i].NoIdentificacion;

                                decimal valorUnitario = decimal.Parse(concepto.valorUnitario.ToString("#.##"));
                                oComercioExterior.Mercancias[i].ValorUnitarioAduana = valorUnitario;
                                oComercioExterior.Mercancias[i].ValorUnitarioAduanaSpecified = true;



                                decimal ValorDolares = oComercioExterior.Mercancias[i].ValorUnitarioAduana
                                    * oComercioExterior.Mercancias[i].CantidadAduana;//decimal.Parse(concepto.importe.ToString("#.##"));
                                ValorDolares = decimal.Parse(concepto.importe.ToString("#.##"));

                                oComercioExterior.Mercancias[i].ValorDolares = decimal.Parse(ValorDolares.ToString("#.##")); ;//ValorDolares;
                                oComercioExterior.Mercancias[i].FraccionArancelariaSpecified = true;

                                TotalUSD += oComercioExterior.Mercancias[i].ValorDolares;


                                i++;
                            }
                            oComercioExterior.TotalUSD = Math.Round(decimal.Parse(TotalUSD.ToString("F")), 2, MidpointRounding.AwayFromZero);
                            oComprobante.subTotal = oComercioExterior.TotalUSD;
                            oComprobante.total = oComercioExterior.TotalUSD;
                            XmlDocument doc = new XmlDocument();
                            oComprobante.Complemento.Any = new XmlElement[1];
                            XmlSerializerNamespaces myNamespaces_comercio = new XmlSerializerNamespaces();
                            //Version 10
                            //myNamespaces_comercio.Add("cce", "http://www.sat.gob.mx/ComercioExterior");

                            //Version 11
                            myNamespaces_comercio.Add("cce11", "http://www.sat.gob.mx/ComercioExterior11");

                            using (XmlWriter writer = doc.CreateNavigator().AppendChild())
                            {
                                new XmlSerializer(oComercioExterior.GetType()).Serialize(writer, oComercioExterior, myNamespaces_comercio);
                            }
                            int indice = oComprobante.Complemento.Any.Length - 1;
                            oComprobante.Complemento.Any = new XmlElement[1];
                            oComprobante.Complemento.Any[indice] = doc.DocumentElement;

                        }
                    }
                    catch (Exception exCCE)
                    {
                        string mensaje = "Error en CCE. " + exCCE.Message.ToString();
                        if (exCCE.InnerException != null)
                        {
                            mensaje += Environment.NewLine + exCCE.InnerException.Message.ToString();
                        }
                        MessageBox.Show(mensaje, Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error);

                        return false;
                    }
                }

            }
            catch (Exception errGeneral)
            {
                string mensaje = "Error General. " + errGeneral.Message.ToString();
                if (errGeneral.InnerException != null)
                {
                    mensaje += Environment.NewLine + errGeneral.InnerException.Message.ToString();
                }
                MessageBox.Show(mensaje, Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }

            XslCompiledTransform myXslTrans = new XslCompiledTransform();

            XslCompiledTransform trans = new XslCompiledTransform();
            string nombre_tmp = oFACTURA.INVOICE_ID + "TMP" + DateTime.Now.ToString("ddMMyyyyhhmmss");
            if (File.Exists(nombre_tmp + ".XML"))
            {
                File.Delete(nombre_tmp + ".XML");
            }

            XPathDocument myXPathDoc;
            try
            {

                TextWriter writer = new StreamWriter(nombre_tmp + ".XML");
                serializer.Serialize(writer, oComprobante, myNamespaces);
                writer.Close();

                myXPathDoc = new XPathDocument(nombre_tmp + ".XML");
                //XslTransform myXslTrans = new XslTransform();
                myXslTrans = new XslCompiledTransform();


                trans = new XslCompiledTransform();
                XmlUrlResolver resolver = new XmlUrlResolver();

                resolver.Credentials = System.Net.CredentialCache.DefaultCredentials;
                XsltSettings settings = new XsltSettings(true, true);
            }
            catch (XmlException errores)
            {
                MessageBox.Show("Error en " + errores.InnerException.ToString());
                return false;
            }
            string sDirectory = AppDomain.CurrentDomain.BaseDirectory;
            //Cargar xslt
            try
            {
                //Version 3
                myXslTrans.Load(sDirectory + "/XSLT/cadenaoriginal_3_2.xslt");
            }
            catch (XmlException errores)
            {
                MessageBox.Show("Error en " + errores.InnerException.ToString());
                return false;
            }

            //Crear Archivo Temporal

            XmlTextWriter myWriter = new XmlTextWriter(nombre_tmp + ".TXT", null);

            //Transformar al XML
            myXslTrans.Transform(myXPathDoc, null, myWriter);

            myWriter.Close();

            //Abrir Archivo TXT
            string CADENA_ORIGINAL = "";
            StreamReader re = File.OpenText(nombre_tmp + ".TXT");
            string input = null;
            while ((input = re.ReadLine()) != null)
            {
                CADENA_ORIGINAL += input;
            }
            re.Close();

            //Eliminar Archivos Temporales
            File.Delete(nombre_tmp + ".TXT");
            File.Delete(nombre_tmp + ".XML");

            //Buscar el &amp; en la Cadena y sustituirlo por &
            CADENA_ORIGINAL = CADENA_ORIGINAL.Replace("&amp;", "&");



            //MessageBox.Show("Generar Sello.");
            oComprobante.sello = SelloRSA(ocCERTIFICADO.LLAVE, ocCERTIFICADO.PASSWORD, CADENA_ORIGINAL, FECHA_FACTURA.Year); ;

            string DIRECTORIO_ARCHIVOS = "";
            //if (!Directory.Exists(ocSERIE.DIRECTORIO))
            //{
            //    ocSERIE.DIRECTORIO=Application.StartupPath;
            //}


            DIRECTORIO_ARCHIVOS += ocSERIE.DIRECTORIO + @"\" + ocSERIE.ID + @"\" + Fechammmyy(oFACTURA.INVOICE_DATE.ToString());
            string XML = DIRECTORIO_ARCHIVOS;

            string PDF = XML + @"\" + "pdf";
            if (!Directory.Exists(PDF))
            {
                try
                {

                    Directory.CreateDirectory(PDF);
                }
                catch (IOException excep)
                {
                    MessageBox.Show(excep.Message.ToString() + Environment.NewLine + "Creando directorio de PDF"
                        , Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return false;
                }
            }

            XML += @"\" + "xml";
            if (Directory.Exists(XML) == false)
                Directory.CreateDirectory(XML);

            string XML_Archivo = "";

            string nombre_archivo = oFACTURA.INVOICE_ID;
            if (ocSERIE.ARCHIVO_NOMBRE != "")
            {
                nombre_archivo = ocSERIE.ARCHIVO_NOMBRE.Replace("{FACTURA}", oFACTURA.INVOICE_ID).Replace("{CLIENTE}", oFACTURA.CUSTOMER_ID);
            }


            if (Directory.Exists(DIRECTORIO_ARCHIVOS))
            {
                XML_Archivo = XML + @"\" + nombre_archivo + ".xml";
            }
            else
            {
                XML_Archivo = @"\" + nombre_archivo + ".xml"; ;
            }

            XML_Archivo = XML + @"\" + nombre_archivo + ".xml";

            //MessageBox.Show("Cargado XLT");

            //MessageBox.Show("Cargado XLT " + ocCERTIFICADO.CERTIFICADO);
            if (!File.Exists(ocCERTIFICADO.CERTIFICADO))
            {
                MessageBox.Show("No se tiene acceso al archivo " + ocCERTIFICADO.CERTIFICADO + "", Application.ProductName + "-" + Application.ProductVersion, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);

                return false;
            }
            TextWriter writerXML = new StreamWriter(XML_Archivo);
            serializer.Serialize(writerXML, oComprobante, myNamespaces);
            writerXML.Close();
            parsar_d1p1(XML_Archivo);
            //Guardar Datos
            //MessageBox.Show("Guardar datos en bandeja.");
            string PDF_Archivo = "";
            PDF_Archivo = PDF + @"\" + nombre_archivo + ".pdf";
            cFACTURA ocFACTURA = new cFACTURA(oEncapsular.oData_ERP);
            ocFACTURA.datos(oFACTURA.INVOICE_ID, "", oFACTURA.ADDR_NO, oEncapsular.ocEMPRESA, "");
            cFACTURA_BANDEJA ocFACTURA_BANDEJA = new cFACTURA_BANDEJA(oEncapsular.oData_ERP, oEncapsular.ocEMPRESA);
            ocFACTURA_BANDEJA.INVOICE_ID = oFACTURA.INVOICE_ID;
            ocFACTURA_BANDEJA.XML = XML_Archivo;
            ocFACTURA_BANDEJA.ESTADO = "Creada Facturación Electrónica";
            ocFACTURA_BANDEJA.CADENA = CADENA_ORIGINAL;
            ocFACTURA_BANDEJA.SELLO = oComprobante.sello;
            ocFACTURA_BANDEJA.SERIE = oComprobante.serie;
            ocFACTURA_BANDEJA.PDF = PDF_Archivo;
            ocFACTURA_BANDEJA.FORMA_DE_PAGO = oComprobante.formaDePago.ToString();
            ocFACTURA_BANDEJA.METODO_DE_PAGO = oComprobante.metodoDePago.ToString();
            ocFACTURA_BANDEJA.CTA_BANCO = oComprobante.NumCtaPago.ToString(); //ocFACTURA.CUENTA_BANCARIA;
            ocFACTURA_BANDEJA.ADDR_NO = oFACTURA.ADDR_NO;
            return ocFACTURA_BANDEJA.guardar();

        }
        public string ExtractNumbers(string expr)
        {
            return string.Join(null, System.Text.RegularExpressions.Regex.Split(expr, "[^\\d]"));
        }
        public decimal otroTruncar(decimal valor, cEMPRESA oEMPRESA)
        {
            try
            {

                decimal result = 0;
                string[] xs = valor.ToString().Split('.');

                string decimalparte = xs[1] + "0000000000000000000000";
                if (oEMPRESA.APROXIMACION == "Truncar")
                {
                    result = decimal.Parse(xs[0] + "." + decimalparte.Substring(0, Math.Min(decimalparte.Length, int.Parse(oEMPRESA.VALORES_DECIMALES))));
                }
                else
                {
                    result = Math.Round(valor, int.Parse(oEMPRESA.VALORES_DECIMALES));
                    xs = result.ToString().Split('.');
                    if (xs[1].Length != int.Parse(oEMPRESA.VALORES_DECIMALES))
                    {
                        decimalparte = xs[1] + "0000000000000000000000";
                        result = decimal.Parse(xs[0] + "." + decimalparte.Substring(0, int.Parse(oEMPRESA.VALORES_DECIMALES)));
                    }
                }
                return result;
            }
            catch
            {
                return valor;
            }
        }
        public string validacion(string cadena)
        {
            string devolucion = "";
            devolucion = cadena.ToUpper();

            devolucion = devolucion.Replace("á", "A");
            devolucion = devolucion.Replace("é", "E");
            devolucion = devolucion.Replace("í", "I");
            devolucion = devolucion.Replace("ó", "O");
            devolucion = devolucion.Replace("ú", "U");
            devolucion = devolucion.Replace("Á", "A");
            devolucion = devolucion.Replace("É", "E");
            devolucion = devolucion.Replace("Í", "I");
            devolucion = devolucion.Replace("Ó", "O");
            devolucion = devolucion.Replace("Ú", "U");
            devolucion = devolucion.Replace(">", "");
            devolucion = devolucion.Replace("<", "");
            //devolucion = devolucion.Replace("&", "");
            devolucion = devolucion.Replace("'", "");
            devolucion = devolucion.Replace("#", "");
            devolucion = devolucion.Replace("|", "");
            //devolucion = HtmlEncode(devolucion);

            devolucion = Regex.Replace(devolucion, @"[\u0000-\u001F]", string.Empty);


            return devolucion; //Regex.Replace(cadena, "[á,é,í,ó,ú,Á,É,Í,Ó,Ú,#,|,&,',>,<]", "");
        }
        public void parsar_d1p1(string ARCHIVO)
        {
            ReplaceInFile(ARCHIVO, "d1p1", "xsi");
            ReplaceInFile(ARCHIVO, "xsi:cfdi=\"http://www.sat.gob.mx/cfd/3\"", "");
        }
        static public void ReplaceInFile(string filePath, string searchText, string replaceText)
        {
            StreamReader reader = new StreamReader(filePath);
            string content = reader.ReadToEnd();
            reader.Close();

            content = Regex.Replace(content, searchText, replaceText);

            StreamWriter writer = new StreamWriter(filePath);
            writer.Write(content);
            writer.Close();
        }
        public string validacion_AMP(string cadena)
        {
            string devolucion = cadena;
            //devolucion = devolucion.Replace("&", "&amp;");
            return devolucion;
        }
        public string Fechammmyy(string pFecha)
        {
            DateTime Fecha = DateTime.Parse(pFecha.ToString());
            string FechaFormateada = "";
            string[] mMeses = new string[13];

            mMeses[1] = "Ene";
            mMeses[2] = "Feb";
            mMeses[3] = "Mar";
            mMeses[4] = "Abr";
            mMeses[5] = "May";
            mMeses[6] = "Jun";
            mMeses[7] = "Jul";
            mMeses[8] = "Agt";
            mMeses[9] = "Sep";
            mMeses[10] = "Oct";
            mMeses[11] = "Nov";
            mMeses[12] = "Dic";

            int Mes = int.Parse(Fecha.Month.ToString());

            FechaFormateada = mMeses[Mes] + AgregarCero(Fecha.Year.ToString());

            return FechaFormateada;
        }
        public string AgregarCero(string p)
        {

            String regreso = p;

            if (p.Length < 2)

                regreso = "0" + p;

            return regreso;
        }
        public string SelloRSA(string LLAVE, string PASSWORD, string strData, decimal ANIO)
        {

            StringBuilder sbPassword;
            StringBuilder sbPrivateKey;
            byte[] b;
            byte[] block;
            int keyBytes;



            string strKeyFile = LLAVE;

            sbPassword = new StringBuilder(PASSWORD);

            sbPrivateKey = Rsa.ReadEncPrivateKey(strKeyFile, sbPassword.ToString());

            keyBytes = Rsa.KeyBytes(sbPrivateKey.ToString());

            b = System.Text.Encoding.UTF8.GetBytes(strData);


            block = Rsa.EncodeMsgForSignature(keyBytes, b, HashAlgorithm.Sha1);

            block = Rsa.RawPrivate(block, sbPrivateKey.ToString());

            string resultado = System.Convert.ToBase64String(block);

            Wipe.String(sbPassword);
            Wipe.String(sbPrivateKey);
            Wipe.Data(block);

            return resultado;

        }
        private string extraeExterior(string source)
        {
            string[] stringSeparators = new string[] { "EXT.", "INT." };
            string[] result;
            result = source.Split(stringSeparators, StringSplitOptions.None);
            if (result.Length == 1)
            {
                return "";
            }
            return result[1];
        }
        private string extraeInterior(string source)
        {
            string[] stringSeparators = new string[] { "INT." };
            string[] result;
            result = source.Split(stringSeparators, StringSplitOptions.None);
            if (result.Length == 1)
            {
                return "";
            }
            return result[1];
        }
        private string extraeDireccion(string source)
        {
            string[] stringSeparators = new string[] { "EXT.", "INT." };
            string[] result;
            result = source.Split(stringSeparators, StringSplitOptions.None);
            if (result.Length == 1)
            {
                return source;
            }

            return result[0];
        }
        private string extraeLocalidad(string source)
        {
            string[] stringSeparators = new string[] { "LOC." };
            string[] result;
            result = source.Split(stringSeparators, StringSplitOptions.None);
            if (result.Length == 1)
            {
                return "";
            }
            return result[1];
        }
    }
}
