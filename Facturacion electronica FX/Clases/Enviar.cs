﻿using Generales;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace FE_FX.Clases
{
    class Enviar
    {
        public bool enviar(cFACTURA oFACTURA, bool adjuntar, bool cancelado, bool validar_automatico, Encapsular oEncapsular, cCONEXCION oData
            ,string correoPara=null)
        {
            try
            {
                if (!bool.Parse(oEncapsular.ocEMPRESA.ENVIO_CORREO))
                {
                    ErrorFX.mostrar("La empresa " + oEncapsular.ocEMPRESA.ID + " entidad " + oEncapsular.ocEMPRESA.ENTITY_ID +
                        @"  no esta configurada para envio de correo", true, true, true);
                    return false;
                }

                cSERIE ocSERIE = new cSERIE();
                
                ocSERIE.cargar_serie(oFACTURA.SERIE, oEncapsular.ocEMPRESA.ROW_ID);
                if (String.IsNullOrEmpty(ocSERIE.ROW_ID_CERTIFICADO))
                {
                    ocSERIE.cargar_serie(ocSERIE.obtenerSerie(oFACTURA.INVOICE_ID)
                        , oEncapsular.ocEMPRESA.ROW_ID);
                }

                //Rodrigo Escalona 29/01/2020 Validar si la serie esta vacia salirse
                if (String.IsNullOrEmpty(ocSERIE.ROW_ID))
                {
                    return false;
                }

                if (validar_automatico)
                {
                    if (ocSERIE.ENVIO_AUTOMATICO_MAIL == "")
                    {
                        ocSERIE.ENVIO_AUTOMATICO_MAIL = "False";
                    }
                    if (!bool.Parse(ocSERIE.ENVIO_AUTOMATICO_MAIL))
                    {
                        return true;
                    }
                }

                string FROM_CORREO = oEncapsular.ocEMPRESA.USUARIO;

                //if (isEmail(ocSERIE.DE))
                //{
                //    FROM_CORREO = ocSERIE.DE;
                //}
                //else
                //{
                //    ErrorFX.mostrar("El from de la serie " + ocSERIE.DE + " no es un mail valido.",true,true);
                //    return false;
                //}

                MailAddress from = new MailAddress(FROM_CORREO, ocSERIE.REMITENTE,
                                System.Text.Encoding.UTF8);

                // Specify the message content.
                MailMessage message = new MailMessage();
                message.From = from;
                //Destinatarios.                
                //TO
                string[] rEmail = oFACTURA.CONTACT_EMAIL.Split(';');
                if (String.IsNullOrEmpty(correoPara))
                {
                    

                    rEmail = oFACTURA.CONTACT_EMAIL.Split(';');
                    for (int i = 0; i < rEmail.Length; i++)
                    {
                        if (!String.IsNullOrEmpty(rEmail[i].ToString().Trim()))
                        {
                            if (isEmail(rEmail[i].ToString().Trim()))
                            {
                                message.To.Add(rEmail[i].ToString().Trim());
                            }
                        }
                    }
                }
                //PARA
                if (String.IsNullOrEmpty(correoPara))
                {                   
                    string[] rEmailPARA = ocSERIE.PARA.Split(';');
                    for (int i = 0; i < rEmailPARA.Length; i++)
                    {
                        if (!String.IsNullOrEmpty(rEmailPARA[i].ToString().Trim()))
                        {
                            if (isEmail(rEmailPARA[i].ToString().Trim()))
                            {
                                message.To.Add(rEmailPARA[i].ToString().Trim());
                            }
                        }
                    }
                }
                else
                {
                    if (isEmail(correoPara))
                    {
                        message.To.Add(correoPara);
                    }
                }

                if (message.To.Count == 0)
                {
                    ErrorFX.mostrar("Enviar - 89 - El correo no puede enviarse, no existen destinatarios.",true,true,true);
                    return false;
                }

                //CC
                if (String.IsNullOrEmpty(correoPara))
                {
                    string[] rEmailCC = ocSERIE.CC.Split(new char[] { ',', ';' }, StringSplitOptions.RemoveEmptyEntries);
                    for (int i = 0; i < rEmailCC.Length; i++)
                    {
                        if (!String.IsNullOrEmpty(rEmailCC[i].ToString().Trim()))
                        {
                            if (isEmail(rEmailCC[i].ToString().Trim()))
                            {
                                message.CC.Add(rEmailCC[i].ToString().Trim());
                            }
                        }
                    }

                    //CCO
                    string[] rEmailCCO = ocSERIE.CCO.Split(new char[] { ',', ';' }, StringSplitOptions.RemoveEmptyEntries);
                    for (int i = 0; i < rEmailCCO.Length; i++)
                    {
                        if (!String.IsNullOrEmpty(rEmailCCO[i].ToString().Trim()))
                        {
                            if (isEmail(rEmailCCO[i].ToString().Trim()))
                            {
                                message.Bcc.Add(rEmailCCO[i].ToString().Trim());
                            }
                        }
                    }
                }

                //Configurar Mensaje
                string mensaje_enviar = ocSERIE.MENSAJE.Replace("<<NUMFACTURA>>", oFACTURA.INVOICE_ID);
                string asunto_enviar = ocSERIE.ASUNTO.Replace("<<NUMFACTURA>>", oFACTURA.INVOICE_ID);

                if (adjuntar)
                {
                    mensaje_enviar = ocSERIE.MENSAJE_ADJUNTO;
                    asunto_enviar = ocSERIE.ASUNTO_ADJUNTO.Replace("<<NUMFACTURA>>", oFACTURA.INVOICE_ID);
                }
                if (cancelado)
                {
                    mensaje_enviar = ocSERIE.MENSAJE_CANCELADO;
                    mensaje_enviar = ocSERIE.MENSAJE_CANCELADO.Replace("<<NUMFACTURA>>", oFACTURA.INVOICE_ID);
                }
                //Verificar el Asunto personalizado por cliente
                cCLIENTE_CONFIGURACION ocCLIENTE_CONFIGURACION = new cCLIENTE_CONFIGURACION(oData);
                if (ocCLIENTE_CONFIGURACION.cargar_CUSTOMER_ID(oFACTURA.CUSTOMER_ID))
                {
                    asunto_enviar = ocCLIENTE_CONFIGURACION.ASUNTO;
                    //Cargar Correos
                    if (ocCLIENTE_CONFIGURACION.CORREO != "")
                    {
                        message.To.Clear();
                        if (String.IsNullOrEmpty(correoPara))
                        {
                            rEmail = ocCLIENTE_CONFIGURACION.CORREO.Split(';');
                            for (int i = 0; i < rEmail.Length; i++)
                            {
                                if (!String.IsNullOrEmpty(rEmail[i].ToString().Trim()))
                                {
                                    if (isEmail(rEmail[i].ToString().Trim()))
                                    {
                                        message.To.Add(rEmail[i].ToString().Trim());
                                    }
                                }
                            }
                        }
                    }

                }

                if (mensaje_enviar.Length == 0)
                {
                    mensaje_enviar = ocSERIE.MENSAJE.Replace("<<NUMFACTURA>>", oFACTURA.INVOICE_ID);
                }
                if (asunto_enviar.Length == 0)
                {
                    asunto_enviar = ocSERIE.ASUNTO.Replace("<<NUMFACTURA>>", oFACTURA.INVOICE_ID);
                }

                if (oFACTURA.TYPE != "M")
                {
                    asunto_enviar = asunto_enviar.Replace("<<TIPO>>", "Factura");
                    mensaje_enviar = mensaje_enviar.Replace("<<TIPO>>", "Factura");
                }
                else
                {
                    asunto_enviar = asunto_enviar.Replace("<<TIPO>>", "Nota de Credito");
                    mensaje_enviar = mensaje_enviar.Replace("<<TIPO>>", "Nota de Credito");
                }


                asunto_enviar = asunto_enviar.Replace("<<CUSTOMER_ID>>", oFACTURA.CUSTOMER_ID);
                asunto_enviar = asunto_enviar.Replace("<<NUMFACTURA>>", oFACTURA.INVOICE_ID);
                asunto_enviar = asunto_enviar.Replace("<<CUSTOMER_NAME>>", oFACTURA.BILL_TO_NAME);
                asunto_enviar = asunto_enviar.Replace("<<INVOICE_DATE>>", oFACTURA.INVOICE_DATE.ToString("dd/MM/yyyy"));
                asunto_enviar = asunto_enviar.Replace("<<TOTAL_AMOUNT>>", (Math.Abs((oFACTURA.TOTAL_AMOUNT + oFACTURA.TOTAL_VAT_AMOUNT))).ToString("$###,###,##0.00"));

                mensaje_enviar = mensaje_enviar.Replace("<<NUMFACTURA>>", oFACTURA.INVOICE_ID);
                mensaje_enviar = mensaje_enviar.Replace("<<CUSTOMER_ID>>", oFACTURA.CUSTOMER_ID);
                mensaje_enviar = mensaje_enviar.Replace("<<CUSTOMER_NAME>>", oFACTURA.BILL_TO_NAME);
                mensaje_enviar = mensaje_enviar.Replace("<<INVOICE_DATE@4>>", oFACTURA.INVOICE_DATE.AddMonths(4).ToString("dd/MM/yyyy"));
                mensaje_enviar = mensaje_enviar.Replace("<<INVOICE_DATE>>", oFACTURA.INVOICE_DATE.ToString("dd/MM/yyyy"));
                mensaje_enviar = mensaje_enviar.Replace("<<TOTAL_AMOUNT>>", (Math.Abs((oFACTURA.TOTAL_AMOUNT + oFACTURA.TOTAL_VAT_AMOUNT))).ToString("$###,###,##0.00"));

                mensaje_enviar = Regex.Replace(mensaje_enviar, Environment.NewLine, "<br>", RegexOptions.Multiline);
                mensaje_enviar = Regex.Replace(mensaje_enviar, "\n", "<br>", RegexOptions.Multiline);

                message.Body = mensaje_enviar; //+ "<br> Enviado el Fecha y Hora: " + DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss");
                message.BodyEncoding = System.Text.Encoding.UTF8;



                message.Subject = asunto_enviar;
                message.SubjectEncoding = System.Text.Encoding.UTF8;
                message.IsBodyHtml = true;
                message.Priority = MailPriority.High;

                //Añadir Atachment
                Attachment data;
                cFACTURA_BANDEJA ocFACTURA_BANDEJA = new cFACTURA_BANDEJA(oEncapsular.oData_ERP, oEncapsular.ocEMPRESA);
                ocFACTURA_BANDEJA.cargar_ID(oFACTURA.INVOICE_ID, oEncapsular.ocEMPRESA);

                if (adjuntar)
                {
                    //Adjuntar los Archivos XML y PDF
                    if (!String.IsNullOrEmpty(ocFACTURA_BANDEJA.XML.ToString().Trim()))
                    {
                        //Validar que exista el XML Factura
                        string archivotmp = ocFACTURA_BANDEJA.XML.ToString();
                        
                        string archivoNombreAdenda= Path.GetFileNameWithoutExtension(archivotmp);
                        string directorioTr = archivotmp.Replace(archivoNombreAdenda+".xml", "");
                        string archivoNuevo = directorioTr + "/" + archivoNombreAdenda + "_adenda.xml";

                        if (File.Exists(archivoNuevo))
                        {
                            data = new Attachment(archivoNuevo);
                            data.Name = oFACTURA.INVOICE_ID + ".xml";
                            if (ocCLIENTE_CONFIGURACION.ARCHIVO != null)
                            {
                                if (ocCLIENTE_CONFIGURACION.ARCHIVO != "")
                                {
                                    data.Name = ocCLIENTE_CONFIGURACION.ARCHIVO.Replace("<<NUMFACTURA>>", oFACTURA.INVOICE_ID) + ".xml";
                                }
                            }

                            message.Attachments.Add(data);
                        }
                        else
                        {
                            if (File.Exists(ocFACTURA_BANDEJA.XML.ToString().Trim()))
                            {
                                data = new Attachment(ocFACTURA_BANDEJA.XML.ToString().Trim());
                                data.Name = oFACTURA.INVOICE_ID + ".xml";
                                if (ocCLIENTE_CONFIGURACION.ARCHIVO != null)
                                {
                                    if (ocCLIENTE_CONFIGURACION.ARCHIVO != "")
                                    {
                                        data.Name = ocCLIENTE_CONFIGURACION.ARCHIVO.Replace("<<NUMFACTURA>>", oFACTURA.INVOICE_ID) + ".xml";
                                    }
                                }

                                message.Attachments.Add(data);
                            }
                            else
                            {
                                ErrorFX.mostrar("Enviar - 233 - El Archivo XML " + ocFACTURA_BANDEJA.XML.ToString().Trim() + " no existe, no es posible enviar el mail.", true, true, true);
                                return false;
                            }
                        }
                         
                    }





                    if (!String.IsNullOrEmpty(ocFACTURA_BANDEJA.PDF.ToString().Trim()))
                    {
                        if (File.Exists(ocFACTURA_BANDEJA.PDF.ToString().Trim()))
                        {
                            data = new Attachment(ocFACTURA_BANDEJA.PDF.ToString().Trim());
                            data.Name = oFACTURA.INVOICE_ID + ".pdf";
                            if (ocCLIENTE_CONFIGURACION.ARCHIVO != null)
                            {
                                if (ocCLIENTE_CONFIGURACION.ARCHIVO != "")
                                {
                                    data.Name = ocCLIENTE_CONFIGURACION.ARCHIVO.Replace("<<NUMFACTURA>>", oFACTURA.INVOICE_ID) + ".pdf";
                                }
                            }
                            message.Attachments.Add(data);
                        }
                        else
                        {
                            ErrorFX.mostrar("Enviar - 233 - El Archivo PDF " + ocFACTURA_BANDEJA.PDF.ToString().Trim() + " no existe, no es posible enviar el mail.", true, true,true);
                            return false;
                        }
                    }
                }

                try
                {
                    /*
                    var smtp = new SmtpClient
                    {
                        Host = oEncapsular.ocEMPRESA.SMTP,
                        Port = int.Parse(oEncapsular.ocEMPRESA.PUERTO),
                        EnableSsl = true,
                        DeliveryMethod = SmtpDeliveryMethod.Network,
                        Timeout = 2000000,
                        UseDefaultCredentials = false,
                        Credentials = new System.Net.NetworkCredential(oEncapsular.ocEMPRESA.USUARIO, cENCRIPTACION.Decrypt(oEncapsular.ocEMPRESA.PASSWORD_EMAIL))
                    };
                    smtp.Send(message);
                    */


                    ////MailAddress to = new MailAddress(Objeto.EMAIL);
                    ////// Specify the message content.
                    ////MailMessage message = new MailMessage(from, to);
                    ////message.Body = "Esta es una prueba de envio de Facturación Electrónica. Fecha y Hora: " + DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss");
                    ////message.BodyEncoding = System.Text.Encoding.UTF8;
                    ////message.Subject = "Prueba de Facturación Electrónica";
                    ////message.SubjectEncoding = System.Text.Encoding.UTF8;
                    ////message.IsBodyHtml = true;
                    ////message.Priority = MailPriority.High;

                    //Configurar SMPT
                    SmtpClient client = new SmtpClient(oEncapsular.ocEMPRESA.SMTP, int.Parse(oEncapsular.ocEMPRESA.PUERTO));
                    //Configurar Crendiciales de Salida
                    client.Host = oEncapsular.ocEMPRESA.SMTP;
                    client.Port = int.Parse(oEncapsular.ocEMPRESA.PUERTO);
                    client.DeliveryMethod = SmtpDeliveryMethod.Network;

                    client.UseDefaultCredentials = bool.Parse(oEncapsular.ocEMPRESA.CRENDENCIALES);
                    client.EnableSsl = bool.Parse(oEncapsular.ocEMPRESA.SSL);
                    client.Timeout = 20000;
                    if (oEncapsular.ocEMPRESA.PASSWORD_EMAIL != "")
                    {
                        string passwordMailtr = cENCRIPTACION.Decrypt(oEncapsular.ocEMPRESA.PASSWORD_EMAIL);
                        System.Net.NetworkCredential credenciales = new System.Net.NetworkCredential(oEncapsular.ocEMPRESA.USUARIO, passwordMailtr);
                        client.Credentials = credenciales;
                    }
                    else
                    {
                        System.Net.NetworkCredential credenciales = new System.Net.NetworkCredential(oEncapsular.ocEMPRESA.USUARIO, "");
                        client.Credentials = credenciales;
                    }
                    client.Send(message);
                }
                catch (Exception error)
                {
                    ErrorFX.mostrar(error, false, true, true);
                }

            }
            catch (Exception ex)
            {
                ErrorFX.mostrar(ex, true, true, "Enviar - 290 -", true);
                return false;
            }

            return true;
        }

        public static bool isEmail(string inputEmail)
        {
            if (inputEmail == null || inputEmail.Length == 0)
            {
                return false;
            }

            const string expression = @"^([a-zA-Z0-9_\-\.]+)@((\[[0-9]{1,3}" +
                                      @"\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9\-]+\" +
                                      @".)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$";

            Regex regex = new Regex(expression);
            return regex.IsMatch(inputEmail);
        }
    }
}
