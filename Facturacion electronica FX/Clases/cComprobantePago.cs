﻿using com.google.zxing.qrcode;
using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;
using FE_FX.ComprobantePago;
using FE_FX.EDICOM_Servicio;
using Generales;
using Ionic.Zip;
using ModeloComprobantePago;
using ModeloDocumentosFX;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity.Validation;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net.Mail;
using System.ServiceModel;
using System.Text;
using System.Text.RegularExpressions;
using System.Xml;
using System.Xml.Serialization;
using CFDI33;
using System.Data.Entity.Infrastructure;
using Pago10;
using System.Xml.Linq;
using System.Windows.Forms;

namespace FE_FX.Clases
{
    class cComprobantePago
    {
        //TODO: Reporte de CFDI a Complemento a Excel
        //TODO: Validar el Complemento de Pago con el Valor de Visual
        //TODO: Complemento de Pagos

        public decimal TruncateDecimal(decimal value, int precision)
        {
            decimal step = (decimal)Math.Pow(10, precision);
            decimal tmp = Math.Truncate(step * value);
            return tmp / step;
        }

        internal string mensaje = "";
        internal pago oRegistro = new pago();
        internal bool generar(string checkId, string customerId, Encapsular oEncapsular, cSERIE ocSERIE
            , string estado, string version, pago oPagoTr = null, bool omitirValidacionNDC = false, bool tipoCambioPiguarlarlo = true)
        {
            try
            {
                oRegistro = new pago();
                //Validar
                try
                {
                    if (bool.Parse(ocSERIE.validarComprobantePago))
                    {
                        bool ValidacionFacturasTr = ValidarFacturas(checkId, customerId, oEncapsular);
                    }
                }
                catch (Exception error)
                {
                    ErrorFX.mostrar(error, true, true, "cComprobantePago - 51 - Validar CFDI PPD y 99", false);
                    return false;
                }


                List<ndcAplicadas> aplicacionNDC = new List<ndcAplicadas>();
                if (!omitirValidacionNDC)
                {
                    aplicacionNDC = validarNDCListado(checkId, customerId, oEncapsular);

                    if (!String.IsNullOrEmpty(errorNDC))
                    {
                        informacion = errorNDC;
                        return false;
                    }
                }


                //Cargar el ajuste cuanto esta asi: Posted directly to account
                decimal ajusteMontoTotal = 0;
                string sSQL = @"
                SELECT ISNULL(SUM(crl.AMOUNT),0) AS MontoAplicado
                FROM CASH_RECEIPT AS cr 
                INNER JOIN CASH_RECEIPT_LINE AS crl ON cr.CUSTOMER_ID = crl.CUSTOMER_ID AND cr.CHECK_ID = crl.CHECK_ID 
                WHERE cr.CHECK_ID='" + checkId + "' AND cr.CUSTOMER_ID='" + customerId + "' AND crl.REFERENCE like '%Posted directly to account%'";
                DataTable oDataTable = oEncapsular.oData_ERP.EjecutarConsulta(sSQL);
                BitacoraFX.Log("SQL ajusteMontoTotal " + Environment.NewLine + sSQL);
                if (oDataTable != null)
                {
                    foreach (DataRow oDataRow in oDataTable.Rows)
                    {
                        //Si existe y no esta Timbrado real eliminarlo
                        try
                        {
                            ajusteMontoTotal = decimal.Parse(oDataRow["MontoAplicado"].ToString());
                        }
                        catch (Exception e)
                        {
                            string mensaje = "";
                            if (e.InnerException != null)
                            {
                                mensaje += Environment.NewLine + e.InnerException.Message.ToString();
                                if (e.InnerException.StackTrace != null)
                                {
                                    mensaje += Environment.NewLine + e.InnerException.StackTrace;
                                }
                            }
                            ErrorFX.mostrar(e, true, true, "cComprobantePago - 143 " + mensaje, false);
                        }
                    }
                }
                BitacoraFX.Log("ajusteMontoTotal " + ajusteMontoTotal.ToString() + Environment.NewLine);



                cCERTIFICADO ocCERTIFICADO = new cCERTIFICADO();
                ocCERTIFICADO.cargar(ocSERIE.ROW_ID_CERTIFICADO);

                //Rodrigo Escalona 01/01/2019 
                //Topre: El tipo de cambio debe ser el del día de pago

                sSQL = @"
                SELECT cr.CUSTOMER_ID, c.NAME, cr.CHECK_ID, cr.BANK_ACCOUNT_ID, ba.DESCRIPTION, cr.AMOUNT
                , cr.USER_ID, cr.CHECK_DATE, cr.STATUS, cr.CURRENCY_ID, cr.PAYMENT_METHOD, c.TAX_ID_NUMBER, 
                c.VAT_REGISTRATION,c.BILL_TO_COUNTRY,c.COUNTRY, c.BILL_TO_ZIPCODE, c.ZIPCODE, c._ZIPCODE, crl.INVOICE_ID, crl.AMOUNT AS MontoAplicado
                , r.TOTAL_AMOUNT,cr.SELL_RATE
                , r.INVOICE_DATE, r.CURRENCY_ID AS FacturaMoneda
                , ISNULL(crc.SELL_RATE,ISNULL(crl.SELL_RATE,cr.SELL_RATE)) as TipoCambio
                
                , ISNULL((SELECT SUM(cr2.AMOUNT) FROM CASH_RECEIPT cr2 WHERE cr2.CUSTOMER_ID = cr.CUSTOMER_ID AND cr2.CHECK_ID = cr.CHECK_ID AND cr2.BANK_ACCOUNT_ID = cr.BANK_ACCOUNT_ID),0) as TotalAplicado
--CASO FIMA PONE LAS NDC CON CUENTAS AND GL_ACCOUNT_ID IS NULL
                , ISNULL((SELECT SUM(crl2.AMOUNT) FROM CASH_RECEIPT_LINE crl2 WHERE crl2.CHECK_ID = cr.CHECK_ID AND crl2.CUSTOMER_ID = cr.CUSTOMER_ID AND crl2.AMOUNT > 0 AND GL_ACCOUNT_ID IS NULL),0) as TotalAplicadoPositivo
                , ABS(ISNULL((SELECT SUM(crl2.AMOUNT) FROM CASH_RECEIPT_LINE crl2 WHERE crl2.CHECK_ID = cr.CHECK_ID AND crl2.CUSTOMER_ID = cr.CUSTOMER_ID AND 
                 crl2.APPLIED_TYPE='G' AND 
                (crl2.AMOUNT < 0 OR NOT(GL_ACCOUNT_ID IS NULL))), 0)) as TotalAplicadoNegativo


                , ISNULL((SELECT COUNT(*)+1
                FROM  CASH_RECEIPT_LINE AS crl2
                INNER JOIN CASH_RECEIPT AS cr2 ON crl2.CUSTOMER_ID = cr2.CUSTOMER_ID AND crl2.CHECK_ID = cr2.CHECK_ID
                WHERE (crl2.INVOICE_ID = crl.INVOICE_ID) 
                AND cr2.CHECK_DATE<=cr.CHECK_DATE AND cr2.VOID_DATE IS NULL AND cr2.CUSTOMER_ID=cr.CUSTOMER_ID
                AND (crl2.CUSTOMER_ID<>cr.CUSTOMER_ID AND crl2.CHECK_ID <> cr.CHECK_ID)
                ),1)
                AS NumParcialidad

                , 
                r.TOTAL_AMOUNT-
                ISNULL((SELECT SUM(crl2.AMOUNT)
                FROM  CASH_RECEIPT_LINE AS crl2
                INNER JOIN CASH_RECEIPT AS cr2 ON crl2.CUSTOMER_ID = cr2.CUSTOMER_ID AND crl2.CHECK_ID = cr2.CHECK_ID
                WHERE (crl2.INVOICE_ID = crl.INVOICE_ID) 
                AND cr2.CHECK_DATE<=cr.CHECK_DATE AND cr2.VOID_DATE IS NULL AND cr2.CUSTOMER_ID=cr.CUSTOMER_ID
                AND (crl2.CUSTOMER_ID<>cr.CUSTOMER_ID AND crl2.CHECK_ID <> cr.CHECK_ID)
                AND cr.VOID_DATE IS NULL
                ),0)
                AS SaldoAnterior
                , ISNULL(crl.PCURR_AMOUNT,crl.AMOUNT) as PCURR_AMOUNT
                , r.BUY_RATE
                FROM CASH_RECEIPT AS cr 
                INNER JOIN CUSTOMER AS c ON cr.CUSTOMER_ID = c.ID 
                INNER JOIN BANK_ACCOUNT AS ba ON cr.BANK_ACCOUNT_ID = ba.ID 
                INNER JOIN CASH_RECEIPT_LINE AS crl ON cr.CUSTOMER_ID = crl.CUSTOMER_ID AND cr.CHECK_ID = crl.CHECK_ID 
                INNER JOIN RECEIVABLE AS r ON c.ID = r.CUSTOMER_ID AND crl.INVOICE_ID = r.INVOICE_ID
                INNER JOIN CASH_RECEIPT_CURR AS crc ON cr.CUSTOMER_ID = crc.CUSTOMER_ID AND cr.CHECK_ID = crc.CHECK_ID AND cr.CURRENCY_ID = crc.CURRENCY_ID
                WHERE cr.CHECK_ID='" + checkId + "' AND cr.CUSTOMER_ID='" + customerId + "' AND crl.AMOUNT>0 " +
                " ORDER BY crl.AMOUNT DESC ";

                //-
                //ISNULL((SELECT SUM(ABS(rma.APPLY_AMOUNT)) FROM RECV_MEMO_APPLY as rma WHERE rma.INV_INVOICE_ID = crl.INVOICE_ID AND rma.apply_date <= cr.CHECK_DATE),0)

                BitacoraFX.Log("SQL " + Environment.NewLine + sSQL);

                oDataTable = oEncapsular.oData_ERP.EjecutarConsulta(sSQL);
                decimal totalAplicadoNegativoTr = 0;
                decimal totalAplicadoTr = 0;
                if (oDataTable != null)
                {
                    ModeloComprobantePago.documentosfxEntities dbContext = new documentosfxEntities();
                    bool guardarCabecera = true;
                    foreach (DataRow oDataRow in oDataTable.Rows)
                    {
                        //Caso del que PCURR_AMOUNT no sea la sumatoria
                        decimal PCURR_AMOUNT = 0;
                        try
                        {
                            PCURR_AMOUNT = decimal.Parse(Math.Round(double.Parse(oDataRow["PCURR_AMOUNT"].ToString()), 2).ToString());
                        }
                        catch (Exception e)
                        {
                            string mensaje = "";
                            if (e.InnerException != null)
                            {
                                mensaje += Environment.NewLine + e.InnerException.Message.ToString();
                                if (e.InnerException.StackTrace != null)
                                {
                                    mensaje += Environment.NewLine + e.InnerException.StackTrace;
                                }
                            }
                            ErrorFX.mostrar(e, true, true, "cComprobantePago - 220 " + mensaje, false);
                        }

                        decimal BUY_RATEr = 1;
                        try
                        {
                            BUY_RATEr = decimal.Parse(Math.Round(double.Parse(oDataRow["BUY_RATE"].ToString()), 2).ToString());
                        }
                        catch (Exception e)
                        {
                            string mensaje = "";
                            if (e.InnerException != null)
                            {
                                mensaje += Environment.NewLine + e.InnerException.Message.ToString();
                                if (e.InnerException.StackTrace != null)
                                {
                                    mensaje += Environment.NewLine + e.InnerException.StackTrace;
                                }
                            }
                            ErrorFX.mostrar(e, true, true, "cComprobantePago - 239 " + mensaje, false);
                        }

                        //Si existe y no esta Timbrado real eliminarlo
                        string checkIdTr = oDataRow["CHECK_ID"].ToString();
                        string customerIdTr = oDataRow["CUSTOMER_ID"].ToString();
                        pago oRegistroTr = dbContext.pagoSet.Where(a => a.checkId.Equals(checkIdTr)
                        & a.customerId.Equals(customerIdTr)).FirstOrDefault();
                        if (oRegistroTr != null)
                        {
                            try
                            {
                                if (!oRegistroTr.estado.Contains("Modo REAL"))
                                {
                                    //Rodrigo Escalona 27/05/2020 determinar si es editado por el cliente
                                    if (oRegistroTr.editado)
                                    {
                                        this.oRegistro = oRegistroTr;
                                        return true;
                                    }

                                    if (File.Exists(oRegistroTr.xml))
                                    {
                                        File.Delete(oRegistroTr.xml);
                                    }
                                    if (File.Exists(oRegistroTr.pdf))
                                    {
                                        File.Delete(oRegistroTr.pdf);
                                    }
                                    Eliminar(oRegistro.Id);


                                }
                                else
                                {
                                    //Caso Cancelado
                                    bool cancelado = false;
                                    if (String.IsNullOrEmpty(oRegistroTr.cancelado))
                                    {
                                        if (oRegistroTr.cancelado != null)
                                        {
                                            cancelado = true;
                                            try
                                            {
                                                if (oRegistroTr.cancelado.Contains("Cancelado"))
                                                {
                                                    string xmlCancelado = oRegistroTr.xml + "-Cancelado";
                                                    if (File.Exists(oRegistroTr.xml))
                                                    {
                                                        File.Move(oRegistroTr.xml, xmlCancelado);
                                                    }
                                                    oRegistroTr.xml = xmlCancelado;
                                                    if (!String.IsNullOrEmpty(oRegistroTr.pdf))
                                                    {
                                                        string pdfCancelado = oRegistroTr.pdf + "-Cancelado";
                                                        if (File.Exists(oRegistroTr.pdf))
                                                        {
                                                            File.Move(oRegistroTr.pdf, pdfCancelado);
                                                        }
                                                        oRegistroTr.pdf = pdfCancelado;
                                                    }

                                                    oRegistroTr.checkId = oRegistroTr.checkId + "-Cancelado";
                                                    dbContext.pagoSet.Attach(oRegistroTr);
                                                    dbContext.Entry(oRegistroTr).State = System.Data.Entity.EntityState.Modified;
                                                    dbContext.SaveChanges();
                                                }
                                            }
                                            catch (DbUpdateException e)
                                            {
                                                ErrorFX.mostrar(e, true, true, " DbUpdateException cComprobantePago - 206", false);
                                                return false;
                                            }
                                            catch (Exception e)
                                            {
                                                ErrorFX.mostrar(e, true, true, "cComprobantePago - 211", false);
                                                return false;
                                            }
                                        }
                                    }

                                    if (!cancelado)
                                    {
                                        try
                                        {
                                            string sufijo = "Relacionado";
                                            if (!String.IsNullOrEmpty(oRegistroTr.error))
                                            {
                                                sufijo = "Error" + DateTime.Now.ToString("YYYYmmmddd_HMS");
                                            }
                                            //Relacionado

                                            string xmlCancelado = oRegistroTr.xml + "-" + sufijo;
                                            if (File.Exists(oRegistroTr.xml))
                                            {
                                                File.Move(oRegistroTr.xml, xmlCancelado);
                                            }
                                            oRegistroTr.xml = xmlCancelado;
                                            if (!String.IsNullOrEmpty(oRegistroTr.pdf))
                                            {
                                                string pdfCancelado = oRegistroTr.pdf + "-" + sufijo;
                                                if (File.Exists(oRegistroTr.pdf))
                                                {
                                                    File.Move(oRegistroTr.pdf, pdfCancelado);
                                                }
                                                oRegistroTr.pdf = pdfCancelado;
                                            }


                                            oRegistroTr.checkId = oRegistroTr.checkId + "-" + sufijo + " " + DateTime.Now.ToString("ddMMyyyy");



                                            dbContext.pagoSet.Attach(oRegistroTr);
                                            dbContext.Entry(oRegistroTr).State = System.Data.Entity.EntityState.Modified;
                                            dbContext.SaveChanges();
                                        }
                                        catch (DbUpdateException e)
                                        {
                                            var builder = new StringBuilder("Error actualizando la base de datos (DbUpdateException). ");

                                            try
                                            {
                                                foreach (var result in e.Entries)
                                                {
                                                    builder.AppendFormat("Tipo: {0}. ", result.Entity.GetType().Name + @"
                                                        " + result.Entity.GetType().FullName);
                                                }
                                            }
                                            catch (Exception error)
                                            {
                                                builder.Append("Error parsing DbUpdateException: " + error.ToString());
                                            }

                                            ErrorFX.mostrar(builder + " DbUpdateException cComprobantePago - 247", true, true, false);
                                            return false;
                                        }
                                        catch (DbEntityValidationException e)
                                        {
                                            var builder = new StringBuilder("Error actualizando la base de datos (DbUpdateException). ");

                                            try
                                            {
                                                foreach (var eve in e.EntityValidationErrors)
                                                {
                                                    builder.AppendFormat("Entity of type \"{0}\" in state \"{1}\" has the following validation errors:",
                                                    eve.Entry.Entity.GetType().Name, eve.Entry.State);
                                                    foreach (var ve in eve.ValidationErrors)
                                                    {
                                                        builder.AppendFormat("- Property: \"{0}\", Error: \"{1}\"",
                                                            ve.PropertyName, ve.ErrorMessage);
                                                    }
                                                }
                                            }
                                            catch (Exception error)
                                            {
                                                builder.Append("Error parsing DbEntityValidationException: " + error.ToString());
                                            }
                                            ErrorFX.mostrar(builder + " DbUpdateException cComprobantePago - 247", true, true, false);
                                            return false;
                                        }

                                        catch (Exception e)
                                        {
                                            ErrorFX.mostrar(e, true, true, " Exception cComprobantePago - 252 ", false);
                                            return false;
                                        }


                                    }
                                }
                            }
                            catch (Exception e)
                            {
                                ErrorFX.mostrar(e, true, true, "cComprobantePago - 232", false);
                                return false;
                            }
                        }


                        //Guardar cabecera
                        if (guardarCabecera)
                        {
                            try
                            {
                                oRegistro.version = version;
                                oRegistro.editado = false;
                                oRegistro.checkId = oDataRow["CHECK_ID"].ToString();
                                oRegistro.customerId = oDataRow["CUSTOMER_ID"].ToString();
                                oRegistro.folio = ocSERIE.consecutivo(ocSERIE.ROW_ID);
                                ocSERIE.incrementar(ocSERIE.ROW_ID);
                                oRegistro.serie = ocSERIE.ID;
                                oRegistro.fecha = DateTime.Now;
                                oRegistro.estado = estado;
                                oRegistro.monedaP = traducirMoneda(oDataRow["CURRENCY_ID"].ToString());
                                //Rodrigo Escalona 12/02/2020
                                //totalAplicadoTr= decimal.Parse(Math.Round(double.Parse(oDataRow["AMOUNT"].ToString()), 2).ToString());
                                totalAplicadoNegativoTr = decimal.Parse(oDataRow["TotalAplicadoNegativo"].ToString());
                                totalAplicadoTr = decimal.Parse(Math.Round(double.Parse(oDataRow["TotalAplicado"].ToString()), 2).ToString());

                                //Rodrigo Escalona
                                oRegistro.monto = Math.Round(totalAplicadoTr, 2);


                                DateTime fechaPagoTr = DateTime.Now;
                                try
                                {
                                    fechaPagoTr = DateTime.Parse(oDataRow["CHECK_DATE"].ToString());
                                }
                                catch (Exception e)
                                {
                                    string mensaje = "";
                                    if (e.InnerException != null)
                                    {
                                        mensaje += Environment.NewLine + e.InnerException.Message.ToString();
                                        if (e.InnerException.StackTrace != null)
                                        {
                                            mensaje += Environment.NewLine + e.InnerException.StackTrace;
                                        }
                                    }
                                    ErrorFX.mostrar(e, true, true, "cComprobantePago - 453 " + mensaje, false);
                                }

                                //oRegistro.monto = 0;
                                oRegistro.tipoCambioP = decimal.Parse(oDataRow["TipoCambio"].ToString());
                                //Traer el TC 
                                cFACTURA oFACTURA = new cFACTURA(oEncapsular.oData_ERP);
                                oRegistro.tipoCambioP = oFACTURA.tipoCambio(oDataRow["FacturaMoneda"].ToString(),
                                    fechaPagoTr, oEncapsular.oData_ERP);

                                if (!MonedasDiferentes(oRegistro.checkId, oRegistro.customerId, oEncapsular.oData_ERP))
                                {
                                    oRegistro.tipoCambioP = 1;
                                }



                                BitacoraFX.Log("oRegistro.tipoCambioP " + oRegistro.tipoCambioP.ToString());




                                oRegistro.fechaPago = new DateTime(fechaPagoTr.Year, fechaPagoTr.Month, fechaPagoTr.Day, 12, 0, 0);
                                oRegistro.noCertificado = ocCERTIFICADO.NO_CERTIFICADO;

                                BitacoraFX.Log("cComprobantePago - " + oRegistro.noCertificado + " Serie: " + ocSERIE.ID);


                                oRegistro.lugarExpedicion = ocSERIE.CP;
                                oRegistro.emisor = oEncapsular.ocEMPRESA.RFC;
                                oRegistro.emisorNombre = oEncapsular.ocEMPRESA.ID;
                                oRegistro.emisorRegimenFiscal = oEncapsular.ocEMPRESA.REGIMEN_FISCAL;
                                oRegistro.receptor = oDataRow["VAT_REGISTRATION"].ToString();
                                oRegistro.receptorNombre = oDataRow["NAME"].ToString();

                                if (!String.IsNullOrEmpty(oDataRow["TAX_ID_NUMBER"].ToString()))
                                {
                                    oRegistro.receptorNumRegIdTrib = oDataRow["TAX_ID_NUMBER"].ToString();
                                }
                                try
                                {
                                    oRegistro.receptorCodigoPostal = oDataRow["BILL_TO_ZIPCODE"].ToString();
                                    if (oRegistro.receptorCodigoPostal.Length > 5)
                                    {
                                        oRegistro.receptorCodigoPostal = oRegistro.receptorCodigoPostal.Substring(0, 5);
                                    }
                                    oRegistro.receptorPais = oDataRow["BILL_TO_COUNTRY"].ToString();
                                    if (oRegistro.receptorCodigoPostal.Length == 0)
                                    {
                                        oRegistro.receptorCodigoPostal = oDataRow["ZIPCODE"].ToString();
                                    }
                                    if (oRegistro.receptorPais.Length == 0)
                                    {
                                        oRegistro.receptorPais = oDataRow["COUNTRY"].ToString();
                                    }
                                    //if (oRegistro.receptorNumRegIdTrib.Contains("XEXX010101000"))
                                    //{

                                    //}

                                }
                                catch (Exception error)
                                {
                                    ErrorFX.mostrar(error, true, true, "cComprobantePAgo - 528 - Seccion de Domicilio del Receptor ", true);
                                }

                                if (oPagoTr != null)
                                {
                                    if (!String.IsNullOrEmpty(oPagoTr.formaDePagoP))
                                    {
                                        oRegistro.formaDePagoP = oPagoTr.formaDePagoP;
                                    }
                                    if (!String.IsNullOrEmpty(oPagoTr.NomBancoOrdExt))
                                    {
                                        oRegistro.NomBancoOrdExt = oPagoTr.NomBancoOrdExt;
                                    }
                                    if (!String.IsNullOrEmpty(oPagoTr.NumOperacion))
                                    {
                                        oRegistro.NumOperacion = oPagoTr.NumOperacion;
                                    }
                                    if (!String.IsNullOrEmpty(oPagoTr.CtaBeneficiario))
                                    {
                                        oRegistro.CtaBeneficiario = oPagoTr.CtaBeneficiario;
                                    }
                                    if (!String.IsNullOrEmpty(oPagoTr.CtaOrdenante))
                                    {
                                        oRegistro.CtaOrdenante = oPagoTr.CtaOrdenante;
                                    }
                                    if (!String.IsNullOrEmpty(oPagoTr.RfcEmisorCtaBen))
                                    {
                                        oRegistro.RfcEmisorCtaBen = oPagoTr.RfcEmisorCtaBen;
                                    }
                                    if (!String.IsNullOrEmpty(oPagoTr.RfcEmisorCtaOrd))
                                    {
                                        oRegistro.RfcEmisorCtaOrd = oPagoTr.RfcEmisorCtaOrd;
                                    }
                                    if (oPagoTr.tipoCambioP != null)
                                    {
                                        //oRegistro.tipoCambioP = oPagoTr.tipoCambioP;
                                        //BitacoraFX.Log("oRegistro.tipoCambioP " + oRegistro.tipoCambioP.ToString());
                                    }
                                }

                                guardarCabecera = false;

                            }
                            catch (Exception e)
                            {
                                string mensaje = "";
                                if (e.InnerException != null)
                                {
                                    mensaje += Environment.NewLine + e.InnerException.Message.ToString();
                                    if (e.InnerException.StackTrace != null)
                                    {
                                        mensaje += Environment.NewLine + e.InnerException.StackTrace;
                                    }
                                }
                                ErrorFX.mostrar(e, true, true, "cComprobantePago - 498 " + mensaje, false);
                            }
                        }

                        //Guardar las lineas
                        pagoLinea linea = new pagoLinea();
                        //Cargar el folio fiscal de la factura
                        string uuidTr = obtenerUUID(oDataRow["INVOICE_ID"].ToString(), oEncapsular);
                        //BitacoraFX.Log("Factura " + oDataRow["INVOICE_ID"].ToString() + " uuidTr: " + uuidTr);
                        if (!String.IsNullOrEmpty(uuidTr))
                        {
                            //Validar que la NDC no esta aplicada al 100%
                            //BitacoraFX.Log("Agregar Factura " + oDataRow["INVOICE_ID"].ToString() + " uuidTr: " + uuidTr);

                            linea.UUID = uuidTr;
                            string serieTr = Regex.Replace(oDataRow["INVOICE_ID"].ToString(), @"[\d-]", string.Empty);
                            string folioTr = Regex.Replace(oDataRow["INVOICE_ID"].ToString(), @"^[A-Za-z]+", "");
                            string invoiceIdTr = oDataRow["INVOICE_ID"].ToString();
                            linea.folio = folioTr;
                            linea.serie = serieTr;
                            linea.factura = oDataRow["INVOICE_ID"].ToString();
                            linea.NumParcialidad = oDataRow["NumParcialidad"].ToString();
                            linea.MonedaDR = traducirMoneda(oDataRow["FacturaMoneda"].ToString());

                            decimal totalAplicadoPositivoTr = 0;
                            try
                            {
                                totalAplicadoPositivoTr = decimal.Parse(oDataRow["TotalAplicadoPositivo"].ToString());
                            }
                            catch (Exception e)
                            {
                                string mensaje = "";
                                if (e.InnerException != null)
                                {
                                    mensaje += Environment.NewLine + e.InnerException.Message.ToString();
                                    if (e.InnerException.StackTrace != null)
                                    {
                                        mensaje += Environment.NewLine + e.InnerException.StackTrace;
                                    }
                                }
                                ErrorFX.mostrar(e, true, true, "cComprobantePago - 534 " + mensaje, false);
                            }

                            decimal montoAplicadoTR = 0;
                            try
                            {
                                montoAplicadoTR = Math.Round(decimal.Parse(oDataRow["MontoAplicado"].ToString()), 2); //Linea
                            }
                            catch (Exception e)
                            {
                                string mensaje = "";
                                if (e.InnerException != null)
                                {
                                    mensaje += Environment.NewLine + e.InnerException.Message.ToString();
                                    if (e.InnerException.StackTrace != null)
                                    {
                                        mensaje += Environment.NewLine + e.InnerException.StackTrace;
                                    }
                                }
                                ErrorFX.mostrar(e, true, true, "cComprobantePago - 554 " + mensaje, false);
                            }

                            decimal AMOUNTTR = 0;
                            try
                            {
                                AMOUNTTR = Math.Round(decimal.Parse(oDataRow["AMOUNT"].ToString()), 2); //Linea
                            }
                            catch (Exception e)
                            {
                                string mensaje = "";
                                if (e.InnerException != null)
                                {
                                    mensaje += Environment.NewLine + e.InnerException.Message.ToString();
                                    if (e.InnerException.StackTrace != null)
                                    {
                                        mensaje += Environment.NewLine + e.InnerException.StackTrace;
                                    }
                                }
                                ErrorFX.mostrar(e, true, true, "cComprobantePago - 574 " + mensaje, false);
                            }

                            decimal ImpPagadoTr = Math.Round((decimal.Parse(oDataRow["TOTAL_AMOUNT"].ToString()) * decimal.Parse(oDataRow["TotalAplicado"].ToString()))
                                / decimal.Parse(oDataRow["TotalAplicadoPositivo"].ToString()), 2)
                                ;
                            linea.ImpPagado = ImpPagadoTr;

                            if (
                                    oEncapsular.ocEMPRESA.ENTITY_ID.Equals("TAM")
                                |
                                oEncapsular.ocEMPRESA.ENTITY_ID.Equals("TAMS")

                                )
                            {
                                if (oDataRow["CURRENCY_ID"].ToString() == oDataRow["FacturaMoneda"].ToString())
                                {
                                    linea.ImpPagado = Math.Round(decimal.Parse(oDataRow["MontoAplicado"].ToString()), 2);
                                }
                            }



                            #region tipoCambio
                            decimal tipoCambioDRtr = 1;
                            linea.TipoCambioDR = Math.Round(tipoCambioDRtr, 2);
                            if (linea.MonedaDR != oRegistro.monedaP)
                            {
                                if (oPagoTr != null)
                                {
                                    //BitacoraFX.Log("Guardar tipo de cambio de la linea");
                                    if (oPagoTr.tipoCambioP != null)
                                    {
                                        decimal tipoCambio = totalAplicadoTr / totalAplicadoPositivoTr;
                                        linea.TipoCambioDR = 1 / tipoCambio;
                                    }
                                }
                                else
                                {
                                    if (!tipoCambioPiguarlarlo)
                                    {
                                        tipoCambioDRtr = buscarTC(oRegistro.checkId, oRegistro.customerId, oDataRow["FacturaMoneda"].ToString(), oEncapsular);
                                    }
                                    else
                                    {
                                        if (oDataRow["CURRENCY_ID"].ToString() != oDataRow["FacturaMoneda"].ToString())
                                        {
                                            tipoCambioDRtr = buscarTC(oRegistro.checkId, oRegistro.customerId, oDataRow["FacturaMoneda"].ToString(), oEncapsular);// decimal.Parse(oDataRow["TipoCambio"].ToString());
                                        }
                                    }
                                    linea.TipoCambioDR = Math.Round(tipoCambioDRtr, 2);
                                }
                            }

                            #endregion tipoCambio

                            linea.ImpSaldoAnt = ImpPagadoTr;
                            //Rodrigo Escalona 07/06/2021 Dlls a MXN

                            //if ((oDataRow["CURRENCY_ID"].ToString() == "MXN") & (oDataRow["FacturaMoneda"].ToString() == "USD"))
                            //{

                            //}

                            //Rodrigo Escalona 09/10/2020 corregir saldos de bradford
                            if (oEncapsular.ocEMPRESA.ENTITY_ID.Equals("BDM"))
                            {
                                if ((oDataRow["CURRENCY_ID"].ToString() == "MXP") & (oDataRow["FacturaMoneda"].ToString() == "USD"))
                                {
                                    BitacoraFX.Log("Entre a Bradford " + linea.folio + " uuidTr: " + linea.UUID);
                                    tipoCambioDRtr = Math.Round(decimal.Parse(oDataRow["AMOUNT"].ToString())
                                        / decimal.Parse(oDataRow["TotalAplicadoPositivo"].ToString()), 4);
                                    linea.TipoCambioDR = 1 / tipoCambioDRtr;
                                    ImpPagadoTr = Math.Round((decimal.Parse(oDataRow["MontoAplicado"].ToString())), 2);
                                    BitacoraFX.Log("Entre a Bradford MontoAplicado " + oDataRow["MontoAplicado"].ToString());

                                    linea.ImpPagado = ImpPagadoTr;
                                    linea.ImpSaldoAnt = 0;

                                    BitacoraFX.Log("Entre a Bradford linea.ImpPagado " + linea.ImpPagado.ToString());
                                }
                                else
                                {
                                    BitacoraFX.Log("Entre a Moendas Igualaes Bradford " + linea.folio + " uuidTr: " + linea.UUID);
                                    ImpPagadoTr = Math.Round((decimal.Parse(oDataRow["MontoAplicado"].ToString())), 2);
                                    linea.ImpPagado = ImpPagadoTr;
                                    linea.ImpSaldoAnt = ImpPagadoTr;
                                }
                            }

                            if (oEncapsular.ocEMPRESA.ENTITY_ID.Equals("FIMA")
                                |
                                oEncapsular.ocEMPRESA.ENTITY_ID.Equals("TAM")
                                |
                                oEncapsular.ocEMPRESA.ENTITY_ID.Equals("TAMS")
                                |
                                oEncapsular.ocEMPRESA.ENTITY_ID.Equals("Losifra")
                                )
                            {
                                if (
                                    ((oDataRow["CURRENCY_ID"].ToString() == "MN") & (oDataRow["FacturaMoneda"].ToString() == "DLLS"))
                                    |
                                    ((oDataRow["CURRENCY_ID"].ToString() == "MXN") & (oDataRow["FacturaMoneda"].ToString() == "USD"))
                                    )
                                {
                                    BitacoraFX.Log("Entre a Fima " + linea.folio + " uuidTr: " + linea.UUID);
                                    tipoCambioDRtr = Math.Round(decimal.Parse(oDataRow["AMOUNT"].ToString())
                                        / decimal.Parse(oDataRow["TotalAplicadoPositivo"].ToString()), 4);
                                    linea.TipoCambioDR = Math.Round(1 / tipoCambioDRtr, 4);
                                    ImpPagadoTr = Math.Round((decimal.Parse(oDataRow["MontoAplicado"].ToString())), 2);
                                    BitacoraFX.Log("Entre a Fima MontoAplicado " + oDataRow["MontoAplicado"].ToString());
                                    BitacoraFX.Log("Entre a Fima TipoCambioDR " + linea.TipoCambioDR.ToString());
                                    linea.ImpPagado = ImpPagadoTr;
                                    linea.ImpSaldoAnt = 0;

                                    BitacoraFX.Log("Entre a Fima linea.ImpPagado " + linea.ImpPagado.ToString());
                                }

                                //Rodrigo Escalona: 01/06/2021 Cambio de Moneda de Losifra

                                if ((oDataRow["CURRENCY_ID"].ToString() == "MN") & (oDataRow["FacturaMoneda"].ToString() == "MN")
                                    |
                                    (oDataRow["CURRENCY_ID"].ToString() == "MXN") & (oDataRow["FacturaMoneda"].ToString() == "MXN"))
                                {
                                    ImpPagadoTr =
                                        Math.Round(
                                        decimal.Parse(oDataRow["MontoAplicado"].ToString())
                                        / decimal.Parse(oDataRow["TotalAplicadoPositivo"].ToString())
                                        * decimal.Parse(oDataRow["AMOUNT"].ToString())
                                        , 2);
                                    linea.ImpPagado = ImpPagadoTr;
                                    linea.ImpSaldoAnt = ImpPagadoTr;
                                }

                            }


                            if (linea.ImpPagado >= 0)
                            {
                                BitacoraFX.Log("617 Agregar Factura " + linea.folio + " uuidTr: " + linea.UUID);
                                //Realizar la sumatoria

                                oRegistro.pagoLinea.Add(linea);
                            }
                        }
                        else
                        {
                            this.mensaje = "El folio fiscal para la factura " + oDataRow["INVOICE_ID"].ToString() + " no existe";
                            return false;
                        }
                    }

                    //Rodrigo Escalona 09/10/2020 


                    decimal totalImportaPagadoLinea = 0;

                    foreach (pagoLinea linea in oRegistro.pagoLinea)
                    {
                        decimal tcTr = 1;
                        if (linea.TipoCambioDR != null)
                        {
                            tcTr = (decimal)linea.TipoCambioDR;
                        }

                        decimal ImpPagadoTr = ((decimal)linea.ImpPagado / tcTr);
                        totalImportaPagadoLinea += ImpPagadoTr;
                    }

                    //(oRegistro.pagoLinea.Sum(a => a.ImpPagado).Value);
                    //BitacoraFX.Log("Entre a Bradford totalImportaPagadoLinea " + totalImportaPagadoLinea.ToString() 
                    //    + " oRegistro.monto " + oRegistro.monto.ToString());
                    //decimal diferencia = 0;
                    decimal totalPagado = 0;
                    if (oRegistro.monto != null)
                    {
                        totalPagado = (decimal)oRegistro.monto;
                    }

                    decimal diferencia = totalPagado - totalImportaPagadoLinea;

                    BitacoraFX.Log("Dif totalImportaPagadoLinea " + totalImportaPagadoLinea.ToString()
                        + " oRegistro.monto " + oRegistro.monto.ToString() + " Diferencia; " + diferencia.ToString());


                    bool procesarDiferencia = false;

                    if ((double)Math.Abs(diferencia) > 0.5)
                    {
                        procesarDiferencia = true;
                    }


                    if (procesarDiferencia)
                    {
                        if (oEncapsular.ocEMPRESA.ENTITY_ID.Equals("BDM"))
                        {


                            //diferencia = (decimal)oRegistro.monto - totalImportaPagadoLinea;
                            //Restarles a todas las lineas el % correspondiente
                            foreach (pagoLinea linea in oRegistro.pagoLinea)
                            {
                                if (oRegistro.monedaP != linea.MonedaDR)
                                {
                                    decimal tcTr = (decimal)oRegistro.monto / totalImportaPagadoLinea;
                                    //decimal ImpPagadoTr = (((decimal)linea.ImpPagado * (decimal)oRegistro.monto) / totalImportaPagadoLinea);
                                    linea.TipoCambioDR = TruncateDecimal(tcTr, 2);
                                }
                                else
                                {
                                    //Monedas iguales
                                    decimal porcentaje = (decimal)linea.ImpPagado / totalImportaPagadoLinea;
                                    decimal ImpPagadoTr = porcentaje * totalPagado;
                                    BitacoraFX.Log("Dif Lineas  porcentaje: " + porcentaje.ToString()
                        + " linea.ImpPagado " + linea.ImpPagado.ToString() + " ImpPagadoTr Nuevo ; " + ImpPagadoTr.ToString());

                                    linea.ImpPagado = TruncateDecimal(ImpPagadoTr, 2);
                                    linea.ImpSaldoAnt = TruncateDecimal(ImpPagadoTr, 2);


                                }
                            }
                        }
                        else
                        {
                            //Solucionar una linea
                            if (oRegistro.pagoLinea.Count == 1)
                            {
                                //Moneda es
                                foreach (pagoLinea linea in oRegistro.pagoLinea)
                                {
                                    if (oRegistro.monedaP == linea.MonedaDR)
                                    {
                                        //decimal ImpPagadoTr = (((decimal)linea.ImpPagado * (decimal)oRegistro.monto) / totalImportaPagadoLinea);
                                        linea.ImpPagado = TruncateDecimal(totalPagado, 2);
                                        linea.ImpSaldoAnt = TruncateDecimal(totalPagado, 2);
                                    }
                                    else
                                    {
                                        //decimal tcTr = (decimal)oRegistro.monto / totalImportaPagadoLinea;
                                        //linea.TipoCambioDR = TruncateDecimal(tcTr, 2);
                                    }
                                }
                            }
                            else
                            {
                                //Son muchas lineas la diferencia se pone al final 
                                if (oEncapsular.ocEMPRESA.ENTITY_ID.Equals("FIMA"))
                                {
                                    decimal ImpPagadoTr = (decimal)oRegistro.pagoLinea.LastOrDefault().ImpPagado;
                                    ImpPagadoTr = ImpPagadoTr + diferencia;

                                    oRegistro.pagoLinea.LastOrDefault().ImpPagado = ImpPagadoTr;
                                    oRegistro.pagoLinea.LastOrDefault().ImpSaldoAnt = ImpPagadoTr;
                                }



                            }
                        }
                    }

                    //Guardar el comprobante
                    dbContext.pagoSet.Add(oRegistro);
                    dbContext.SaveChanges();

                    //Calcular Totales
                    if (version.Equals("4.0"))
                    {
                        //Calcular los totales
                        ModeloComprobantePago.pagoTotales OPagosTotales = CalcularTotales(oRegistro, oEncapsular, ocSERIE.ACCOUNT_ID_RETENIDO);
                    }

                    dbContext.Dispose();
                }
                return true;
            }
            catch (DbEntityValidationException e)
            {
                string mensaje = "";
                foreach (var eve in e.EntityValidationErrors)
                {
                    foreach (var ve in eve.ValidationErrors)
                    {
                        mensaje += Environment.NewLine + ve.PropertyName + " Error: " + ve.ErrorMessage;
                    }
                }

                ErrorFX.mostrar("cComprobantePago - 157 " + mensaje, true, true, false);
            }
            catch (Exception e)
            {
                string mensaje = "";
                if (e.InnerException != null)
                {
                    mensaje += Environment.NewLine + e.InnerException.Message.ToString();
                    if (e.InnerException.StackTrace != null)
                    {
                        mensaje += Environment.NewLine + e.InnerException.StackTrace;
                    }
                }
                ErrorFX.mostrar(e, true, true, "cComprobantePago - 20 " + mensaje, false);
            }
            return false;
        }
        public static void EliminarLinea(int id)
        {
            ModeloComprobantePago.documentosfxEntities dbContext = new documentosfxEntities();
            string conection = dbContext.Database.Connection.ConnectionString;
            cCONEXCION oData = new cCONEXCION(conection);
            try
            {
                //Eliminar pagos lineas impuestos
                string sSQL = @"DELETE FROM pagoLineaImpuestoSet
                                        FROM pagoLineaImpuestoSet pli INNER JOIN pagoLineaSet pl ON pli.pagoLinea_Id= pl.Id
                                        INNER JOIN pagoSet p ON pl.pago_Id = p.Id
                                        WHERE (pl.Id = " + id.ToString() + ")";
                oData.EjecutarConsulta(sSQL);

                sSQL = @"DELETE FROM pagoLineaRetencionSet
                                        FROM pagoLineaImpuestoSet pli INNER JOIN pagoLineaSet pl ON pli.pagoLinea_Id= pl.Id
                                        INNER JOIN pagoSet p ON pl.pago_Id = p.Id
                                        WHERE (pl.Id = " + id.ToString() + ")";
                oData.EjecutarConsulta(sSQL);

                //Eliminar pagos lineas
                sSQL = @"DELETE  FROM pagoLineaSet
                                        FROM pagoLineaSet INNER JOIN pagoSet ON pagoLineaSet.pago_Id = pagoSet.Id
                                        WHERE (pagoLineaSet.Id = " + id.ToString() + ")";
                oData.EjecutarConsulta(sSQL);

                oData.DestruirConexion();
            }
            catch (Exception e)
            {
                ErrorFX.mostrar(e, true, true, "cComprobantePago - Eliminar - 982 ", false);
            }
        }

        public static void Eliminar(int id)
        {
            ModeloComprobantePago.documentosfxEntities dbContext = new documentosfxEntities();
            string conection = dbContext.Database.Connection.ConnectionString;
            cCONEXCION oData = new cCONEXCION(conection);
            try
            {
                //Eliminar pagos lineas impuestos
                string sSQL = @"DELETE FROM pagoLineaImpuestoSet
                                        FROM pagoLineaImpuestoSet pli INNER JOIN pagoLineaSet pl ON pli.pagoLinea_Id= pl.Id
                                        INNER JOIN pagoSet p ON pl.pago_Id = p.Id
                                        WHERE (p.Id = " + id.ToString() + ")";
                oData.EjecutarConsulta(sSQL);

                sSQL = @"DELETE FROM pagoLineaRetencionSet
                                        FROM pagoLineaRetencionSet pli INNER JOIN pagoLineaSet pl ON pli.pagoLinea_Id= pl.Id
                                        INNER JOIN pagoSet p ON pl.pago_Id = p.Id
                                        WHERE (p.Id = " + id.ToString() + ")";
                oData.EjecutarConsulta(sSQL);

                sSQL = @"DELETE  FROM pagoRetencionSet 
                                        FROM pagoRetencionSet pi
                                        INNER JOIN pagoSet p ON pi.pago_Id = p.Id
                                        WHERE (p.Id = " + id.ToString() + ")";
                oData.EjecutarConsulta(sSQL);

                sSQL = @"DELETE  FROM pagoImpuestoSet 
                                        FROM pagoImpuestoSet pi
                                        INNER JOIN pagoSet p ON pi.pago_Id = p.Id
                                        WHERE (p.Id = " + id.ToString() + ")";
                oData.EjecutarConsulta(sSQL);

                //Eliminar pagos lineas
                sSQL = @"DELETE  FROM pagoLineaSet
                                        FROM pagoLineaSet INNER JOIN pagoSet ON pagoLineaSet.pago_Id = pagoSet.Id
                                        WHERE (pagoSet.Id = " + id.ToString() + ")";
                oData.EjecutarConsulta(sSQL);

                //Eliminar
                sSQL = @"DELETE  FROM pagoTotalesSet
                                        FROM pagoTotalesSet INNER JOIN pagoSet ON pagoTotalesSet.pago_Id = pagoSet.Id
                                        WHERE (pagoSet.Id = " + id.ToString() + ")";
                oData.EjecutarConsulta(sSQL);

                //Eliminar
                sSQL = @"DELETE
                                        FROM pagoSet
                                        WHERE (pagoSet.Id = " + id.ToString() + ")";
                oData.EjecutarConsulta(sSQL);
                oData.DestruirConexion();
            }
            catch (Exception e)
            {
                ErrorFX.mostrar(e, true, true, "cComprobantePago - Eliminar - 982 ", false);
            }
        }

        internal bool GenerarNuevo(string checkId, string customerId, Encapsular oEncapsular, cSERIE ocSERIE
            , string estado, string version, pago oPagoTr = null, bool omitirValidacionNDC = false, bool tipoCambioPiguarlarlo = true)
        {
            try
            {

                ModeloComprobantePago.documentosfxEntities dbContext = new documentosfxEntities();
                oRegistro = dbContext.pagoSet.Where(a => a.checkId == checkId
                && a.customerId == customerId).FirstOrDefault();
                if (oRegistro != null)
                {
                    if (oRegistro.editado)
                    {
                        return true;
                    }
                }


                //Validar
                try
                {
                    if (bool.Parse(ocSERIE.validarComprobantePago))
                    {
                        bool ValidacionFacturasTr = ValidarFacturas(checkId, customerId, oEncapsular);
                    }
                }
                catch (Exception error)
                {
                    ErrorFX.mostrar(error, true, true, "GenerarNuevo - 973 - Exiten facturas con CFDI PPD y 99", false);
                    return false;
                }
                cCERTIFICADO ocCERTIFICADO = new cCERTIFICADO();
                ocCERTIFICADO.cargar(ocSERIE.ROW_ID_CERTIFICADO);
                pago OPago = GenerarCabeceraPago(checkId, customerId, oEncapsular, ocSERIE, estado, version, ocCERTIFICADO);
                if (OPago == null)
                {
                    ErrorFX.mostrar("GenerarNuevo - 981 - Pago no esta guardado", true, true, false);
                    return false;
                }
                if (OPago.editado)
                {
                    return true;
                }


                bool ErrorLineas = GenerarLineasPago(OPago, checkId, customerId, oEncapsular, ocSERIE.ACCOUNT_ID_RETENIDO);
                if (!ErrorLineas)
                {
                    DialogResult Resultado = MessageBox.Show("Se encontraron estos errores: "
                        + Environment.NewLine + this.mensaje + " Desea agregarlos manualmente?"
                        , Application.ProductName + "-" + Application.ProductVersion, MessageBoxButtons.YesNo);
                    if (Resultado != DialogResult.Yes)
                    {
                        ErrorFX.mostrar("GenerarLineasPago - 1031 - " + this.mensaje, true, true, false);
                        return false;
                    }
                }
                GenerarImpuestosNuevo(OPago);
                //GenerarImpuestos(OPago, oEncapsular);
                GenerarRetencionNuevo(OPago);
                //GenerarRetencion(OPago, oEncapsular, ocSERIE.ACCOUNT_ID_RETENIDO);
                //CalcularTotales(OPago, oEncapsular,ocSERIE.ACCOUNT_ID_RETENIDO);
                CalcularTotalesNuevo(OPago);

                oRegistro = dbContext.pagoSet.Where(a => a.checkId.Equals(checkId)
                                & a.customerId.Equals(customerId)).FirstOrDefault();
                return true;
            }
            catch (DbEntityValidationException e)
            {
                string mensaje = "";
                foreach (var eve in e.EntityValidationErrors)
                {
                    foreach (var ve in eve.ValidationErrors)
                    {
                        mensaje += Environment.NewLine + ve.PropertyName + " Error: " + ve.ErrorMessage;
                    }
                }

                ErrorFX.mostrar("cComprobantePago - GenerarNuevo - 976 " + mensaje, true, true, false);
            }
            catch (Exception e)
            {
                string mensaje = "";
                if (e.InnerException != null)
                {
                    mensaje += Environment.NewLine + e.InnerException.Message.ToString();
                    if (e.InnerException.StackTrace != null)
                    {
                        mensaje += Environment.NewLine + e.InnerException.StackTrace;
                    }
                }
                ErrorFX.mostrar(e, true, true, "cComprobantePago - GenerarNuevo - 989 " + mensaje, false);
            }
            return false;
        }

        private bool GenerarLineasPago(pago oPagoP, string checkId, string customerId, Encapsular oEncapsular, string cuentaRetencion)
        {
            try
            {
                this.mensaje = string.Empty;
                EliminarLineasFacturasNoExistentes(oPagoP, checkId, customerId);
                //Rodrigo Escalona: 27/03/2023 Cargar el Pago
                //Rodrigo Escalona: 27/03/2023 Verificar si existe el registro en pagos
                ModeloComprobantePago.documentosfxEntities dbContext = new documentosfxEntities();
                pago oRegistro = dbContext.pagoSet.Where(a => a.checkId.Equals(checkId)
                        & a.customerId.Equals(customerId)).FirstOrDefault();
                if (oRegistro == null)
                {
                    ErrorFX.mostrar("GenerarLineasPago - 1028 - La cabecera no existe.", true, true, false);
                    return false;
                }
                string Sql = @"
                SELECT 
                crl.INVOICE_ID, crl.AMOUNT AS ImpPagado
                , r.TOTAL_AMOUNT
                , r.INVOICE_DATE, r.CURRENCY_ID AS FacturaMoneda
                , crl.SELL_RATE as TipoCambioLinea
                , ISNULL((SELECT COUNT(*) + 1
                FROM  CASH_RECEIPT_LINE AS crl2
                INNER JOIN CASH_RECEIPT AS cr2 ON crl2.CUSTOMER_ID = cr2.CUSTOMER_ID AND crl2.CHECK_ID = cr2.CHECK_ID
                WHERE (crl2.INVOICE_ID = crl.INVOICE_ID)
                AND cr2.CHECK_DATE <= cr.CHECK_DATE AND cr2.VOID_DATE IS NULL AND cr2.CUSTOMER_ID = crl.CUSTOMER_ID
                AND crl2.CHECK_ID <> crl.CHECK_ID
                
                ),1)
                AS NumParcialidad
                ,
                r.TOTAL_AMOUNT -
                ISNULL((SELECT SUM(crl2.AMOUNT)
                FROM  CASH_RECEIPT_LINE AS crl2
                INNER JOIN CASH_RECEIPT AS cr2 ON crl2.CUSTOMER_ID = cr2.CUSTOMER_ID AND crl2.CHECK_ID = cr2.CHECK_ID
                WHERE (crl2.INVOICE_ID = crl.INVOICE_ID)
                AND cr2.CHECK_DATE <= cr.CHECK_DATE AND cr2.VOID_DATE IS NULL AND cr2.CUSTOMER_ID = crl.CUSTOMER_ID
                AND crl2.CHECK_ID <> crl.CHECK_ID
                
                ),0)
                AS ImpSaldo
                , ISNULL(crl.PCURR_AMOUNT,crl.AMOUNT) as PCURR_AMOUNT
                , r.SELL_RATE as TipoCambioFactura
                ,
                ISNULL((SELECT SUM(cr2.AMOUNT) from CASH_RECEIPT cr2 WHERE cr2.CUSTOMER_ID=crl.CUSTOMER_ID AND cr2.CHECK_ID=crl.CHECK_ID),1)/
                ISNULL((SELECT SUM(crl2.AMOUNT) from CASH_RECEIPT_LINE crl2 WHERE crl2.CUSTOMER_ID=crl.CUSTOMER_ID AND crl2.CHECK_ID=crl.CHECK_ID),1)
                as TCPago
                , ISNULL((SELECT SUM(crl2.AMOUNT) FROM CASH_RECEIPT_LINE crl2 
                WHERE crl2.CHECK_ID = cr.CHECK_ID AND crl2.CUSTOMER_ID = cr.CUSTOMER_ID AND crl2.AMOUNT > 0 AND GL_ACCOUNT_ID IS NULL),0) 
                as TotalAplicadoPositivo
                ,cr.AMOUNT as TotalPago
                FROM CASH_RECEIPT_LINE crl
                INNER JOIN CASH_RECEIPT cr ON cr.CHECK_ID=crl.CHECK_ID AND cr.CUSTOMER_ID=crl.CUSTOMER_ID
                INNER JOIN RECEIVABLE AS r ON crl.INVOICE_ID = r.INVOICE_ID
                WHERE crl.CHECK_ID = '" + checkId + "' AND crl.CUSTOMER_ID = '" + customerId + @"' AND crl.AMOUNT > 0 
                ORDER BY crl.AMOUNT DESC  ";
                BitacoraFX.Log("SQL " + Environment.NewLine + Sql);
                DataTable oDataTable = oEncapsular.oData_ERP.EjecutarConsulta(Sql);
                if (oDataTable != null)
                {
                    foreach (DataRow oDataRow in oDataTable.Rows)
                    {
                        //Guardar las lineas
                        pagoLinea linea = new pagoLinea();
                        //Cargar el folio fiscal de la factura
                        string uuidTr = obtenerUUID(oDataRow["INVOICE_ID"].ToString(), oEncapsular);
                        decimal TotalAplicadoPositivoTr = decimal.Parse(oDataRow["TotalAplicadoPositivo"].ToString());
                        decimal TotalPagado = decimal.Parse(oDataRow["TotalPago"].ToString());
                        //Rodrigo Escalona: Verificar el tipo de cambio 19/07/2023 Sacred no puede hacer el complemento
                        decimal TCPago=decimal.Parse(oDataRow["TCPago"].ToString());
                        if (TCPago != 1)
                        {
                            TotalPagado = TotalPagado / TCPago;
                        }
                        decimal PorcetajeAplicacion = 1;
                        PorcetajeAplicacion = TotalPagado / TotalAplicadoPositivoTr;

                        //BitacoraFX.Log("Factura " + oDataRow["INVOICE_ID"].ToString() + " uuidTr: " + uuidTr);
                        if (!String.IsNullOrEmpty(uuidTr))
                        {
                            linea = dbContext.pagoLineaSet.Where(a => a.pago.Id.Equals(oPagoP.Id) && a.UUID.Equals(uuidTr)).FirstOrDefault();
                            if (linea == null)
                            {
                                linea = new pagoLinea();
                            }
                            linea.UUID = uuidTr;
                            string serieTr = Regex.Replace(oDataRow["INVOICE_ID"].ToString(), @"[\d-]", string.Empty);
                            string folioTr = Regex.Replace(oDataRow["INVOICE_ID"].ToString(), @"^[A-Za-z]+", string.Empty);
                            string invoiceIdTr = oDataRow["INVOICE_ID"].ToString();
                            linea.folio = folioTr;
                            linea.serie = serieTr;
                            linea.factura = oDataRow["INVOICE_ID"].ToString();
                            linea.NumParcialidad = oDataRow["NumParcialidad"].ToString();
                            decimal ImpPagadoTr = Math.Round((decimal.Parse(oDataRow["ImpPagado"].ToString())), 2) * PorcetajeAplicacion;
                            linea.ImpPagado = ImpPagadoTr;
                            linea.MonedaDR = traducirMoneda(oDataRow["FacturaMoneda"].ToString());
                            decimal ImpSaldoTr = Math.Round((decimal.Parse(oDataRow["ImpSaldo"].ToString())), 2) * PorcetajeAplicacion;
                            linea.ImpSaldoAnt = ImpSaldoTr;
                            linea.EquivalenciaDR = 1;
                            if (!oRegistro.monedaP.Equals(linea.MonedaDR))
                            {
                                if (oRegistro.monedaP.Equals("MXN"))
                                {
                                    linea.EquivalenciaDR = 1 / decimal.Parse(oDataRow["TCPago"].ToString());
                                }
                                else
                                {
                                    linea.EquivalenciaDR = 1 / (decimal)oPagoP.tipoCambioP;
                                }
                                linea.EquivalenciaDR = Math.Round(linea.EquivalenciaDR, 10);
                            }

                            if (linea.Id == 0)
                            {
                                linea.pago = oRegistro;
                                dbContext.pagoLineaSet.Add(linea);
                                dbContext.SaveChanges();
                            }
                            else
                            {
                                dbContext.pagoLineaSet.Attach(linea);
                                dbContext.Entry(linea).State = System.Data.Entity.EntityState.Modified;
                                dbContext.SaveChanges();
                            }

                            GenerarLineaImpuestoPago(linea, oEncapsular);
                            GenerarLineaRetencionPago(linea, oEncapsular, cuentaRetencion);

                        }
                        else
                        {
                            this.mensaje = "El folio fiscal para la factura " + oDataRow["INVOICE_ID"].ToString() + " no existe";
                            return false;
                        }
                    }
                }
                if (!String.IsNullOrEmpty(this.mensaje))
                {
                    ErrorFX.mostrar(this.mensaje, true, true, false);
                    return false;
                }
                return true;
            }
            catch (DbEntityValidationException e)
            {
                string mensaje = "";
                foreach (var eve in e.EntityValidationErrors)
                {
                    foreach (var ve in eve.ValidationErrors)
                    {
                        mensaje += Environment.NewLine + ve.PropertyName + " Error: " + ve.ErrorMessage;
                    }
                }

                ErrorFX.mostrar("cComprobantePago - GenerarLineasPago - 1034 " + mensaje, true, true, false);
            }
            catch (Exception e)
            {
                string mensaje = "";
                if (e.InnerException != null)
                {
                    mensaje += Environment.NewLine + e.InnerException.Message.ToString();
                    if (e.InnerException.StackTrace != null)
                    {
                        mensaje += Environment.NewLine + e.InnerException.StackTrace;
                    }
                }
                ErrorFX.mostrar(e, true, true, "cComprobantePago - GenerarLineasPago - 1048 " + mensaje, false);
            }
            return false;
        }
        private decimal SubtractOneFromLastDecimal(decimal number)
        {
            int decimalCount = BitConverter.GetBytes(decimal.GetBits(number)[3])[2];
            decimal powerOfTen = (decimal)Math.Pow(10, decimalCount - 1);
            decimal lastDigit = number % powerOfTen;
            decimal result = number - lastDigit + (lastDigit - 1);
            return result;
        }
        private ModeloComprobantePago.pagoImpuesto GenerarImpuestos(pago oPagoTr, Encapsular oEncapsular)
        {
            try
            {

                ModeloComprobantePago.pagoImpuesto OpagoImpuesto = new pagoImpuesto();
                ModeloComprobantePago.documentosfxEntities dbContext = new documentosfxEntities();
                OpagoImpuesto = dbContext.pagoImpuestoSet.Where(a => a.pago.Id.Equals(oPagoTr.Id)).FirstOrDefault();
                if (OpagoImpuesto == null)
                {
                    OpagoImpuesto = new pagoImpuesto();
                }
                string Sql = @"
                SELECT SUM(tabla.Total) as Total, SUM(tabla.TotalImpuesto) as TotalImpuesto, tabla.Porcentaje
                FROM CASH_RECEIPT AS cr 
                INNER JOIN CASH_RECEIPT_LINE AS crl ON cr.CUSTOMER_ID = crl.CUSTOMER_ID AND cr.CHECK_ID = crl.CHECK_ID 
                INNER JOIN 
                (
                SELECT rl.INVOICE_ID,SUM(rl.AMOUNT) as Total, SUM(rl.VAT_AMOUNT) as TotalImpuesto, MAX(rl.VAT_PERCENT) as Porcentaje
                FROM RECEIVABLE_LINE rl
                GROUP BY rl.INVOICE_ID,rl.VAT_PERCENT
                ) as Tabla
                ON Tabla.INVOICE_ID=crl.INVOICE_ID
                WHERE cr.CHECK_ID='" + oPagoTr.checkId + "' AND cr.CUSTOMER_ID='" + oPagoTr.customerId + @"' AND crl.AMOUNT>0 
                GROUP BY tabla.Porcentaje
                HAVING tabla.Porcentaje>0
                ";
                DataTable oDataTable = oEncapsular.oData_ERP.EjecutarConsulta(Sql);
                if (oDataTable != null)
                {
                    if (oDataTable.Rows.Count == 0)
                    {
                        if (OpagoImpuesto.Id != 0)
                        {

                            dbContext.pagoImpuestoSet.Remove(OpagoImpuesto);
                            dbContext.SaveChanges();

                        }

                    }
                    else
                    {
                        foreach (DataRow oDataRow in oDataTable.Rows)
                        {
                            decimal TasaTr = Math.Truncate(decimal.Parse(oDataRow["Porcentaje"].ToString()));
                            decimal Porcentaje = decimal.Parse(oDataRow["Porcentaje"].ToString());
                            decimal BaseP = decimal.Parse(oDataRow["Total"].ToString());
                            if (BaseP > 0)
                            {
                                OpagoImpuesto.TipoFactorP = "Tasa";
                                OpagoImpuesto.ImpuestoP = "002";
                                OpagoImpuesto.TasaOCuotaP = decimal.Parse("0.16");
                                OpagoImpuesto.BaseP = BaseP;// decimal.Parse(oDataRow["Total"].ToString());
                                OpagoImpuesto.ImporteP = decimal.Parse(oDataRow["TotalImpuesto"].ToString());
                            }
                        }

                        if (OpagoImpuesto.Id == 0)
                        {
                            pago oRegistroTr = dbContext.pagoSet.Where(a => a.Id.Equals(oPagoTr.Id)).FirstOrDefault();
                            OpagoImpuesto.pago = oRegistroTr;
                            dbContext.pagoImpuestoSet.Add(OpagoImpuesto);
                            dbContext.SaveChanges();
                        }
                        else
                        {
                            dbContext.pagoImpuestoSet.Attach(OpagoImpuesto);
                            dbContext.Entry(OpagoImpuesto).State = System.Data.Entity.EntityState.Modified;
                            dbContext.SaveChanges();
                        }
                    }

                }

                return OpagoImpuesto;
            }
            catch (Exception Error)
            {
                string mensaje = "";
                if (Error.InnerException != null)
                {
                    mensaje += Environment.NewLine + Error.InnerException.Message.ToString();
                    if (Error.InnerException.StackTrace != null)
                    {
                        mensaje += Environment.NewLine + Error.InnerException.StackTrace;
                    }
                }
                ErrorFX.mostrar(Error, true, true, "GenerarImpuestos - 1264 - Calculo de Totales " + mensaje, false);
            }
            return null;
        }

        internal ModeloComprobantePago.pagoImpuesto GenerarImpuestosNuevo(pago oPagoTr)
        {
            try
            {

                ModeloComprobantePago.pagoImpuesto OpagoImpuesto = new pagoImpuesto();
                ModeloComprobantePago.documentosfxEntities dbContext = new documentosfxEntities();
                OpagoImpuesto = dbContext.pagoImpuestoSet.Where(a => a.pago.Id.Equals(oPagoTr.Id)).FirstOrDefault();
                if (OpagoImpuesto == null)
                {
                    OpagoImpuesto = new pagoImpuesto();
                }
                else
                {
                    //Rodrigo Escalona: 23/05/2023
                    dbContext.pagoImpuestoSet.Attach(OpagoImpuesto);
                    dbContext.pagoImpuestoSet.Remove(OpagoImpuesto);
                    dbContext.SaveChanges();
                    OpagoImpuesto = new pagoImpuesto();
                }

                //Rodrigo Escalona 04/0/2023 Cargar las lineas guardadas
                //List<ResultLine> Impuestos = dbContext.pagoLineaImpuestoSet.Where(a => a.pagoLinea.pago.Id.Equals(oPagoTr.Id))
                //    .GroupBy(l => l.TasaOCuotaDR)
                //    .Select(cl => new ResultLine
                //    {
                //        TasaOCuotaDR = cl.FirstOrDefault().TasaOCuotaDR,
                //        BaseDR = cl.Sum(c => c.BaseDR),
                //        ImporteDR = cl.Sum(c => c.ImporteDR),
                //    }).ToList<ResultLine>();

                //Rodrigo Escalona 09/05/2023  Calcular Agregnado la EquivaleDR
                var ImpuestosTr = dbContext.pagoLineaImpuestoSet.Where(a => a.pagoLinea.pago.Id.Equals(oPagoTr.Id));
                List<ResultLine> Impuestos = new List<ResultLine>();
                ResultLine ImpuestoTotal = new ResultLine();
                foreach (pagoLineaImpuesto impuestoTr in ImpuestosTr)
                {
                    ImpuestoTotal.TasaOCuotaDR = impuestoTr.TasaOCuotaDR;
                    ImpuestoTotal.BaseDR += SubtractOneFromLastDecimal(impuestoTr.BaseDR * decimal.Parse(impuestoTr.pagoLinea.EquivalenciaDR.ToString()));
                    ImpuestoTotal.ImporteDR += Math.Truncate(impuestoTr.ImporteDR * decimal.Parse(impuestoTr.pagoLinea.EquivalenciaDR.ToString()));
                }
                if (ImpuestoTotal.BaseDR > 0)
                {
                    Impuestos.Add(ImpuestoTotal);

                    decimal TipoCambioTr = (decimal)oPagoTr.tipoCambioP;
                    if (Impuestos.Count > 0)
                    {
                        foreach (ResultLine oDataRow in Impuestos)
                        {
                            OpagoImpuesto.TipoFactorP = "Tasa";
                            OpagoImpuesto.ImpuestoP = "002";
                            OpagoImpuesto.TasaOCuotaP = decimal.Parse("0.16");
                            OpagoImpuesto.BaseP = oDataRow.BaseDR;//* TipoCambioTr;
                            OpagoImpuesto.ImporteP = oDataRow.ImporteDR;// * TipoCambioTr;
                        }

                        if (OpagoImpuesto.Id == 0)
                        {
                            pago oRegistroTr = dbContext.pagoSet.Where(a => a.Id.Equals(oPagoTr.Id)).FirstOrDefault();
                            OpagoImpuesto.pago = oRegistroTr;
                            dbContext.pagoImpuestoSet.Add(OpagoImpuesto);
                            dbContext.SaveChanges();
                        }
                        else
                        {
                            dbContext.pagoImpuestoSet.Attach(OpagoImpuesto);
                            dbContext.Entry(OpagoImpuesto).State = System.Data.Entity.EntityState.Modified;
                            dbContext.SaveChanges();
                        }
                    }
                }
                return OpagoImpuesto;
            }
            catch (Exception Error)
            {
                string mensaje = "";
                if (Error.InnerException != null)
                {
                    mensaje += Environment.NewLine + Error.InnerException.Message.ToString();
                    if (Error.InnerException.StackTrace != null)
                    {
                        mensaje += Environment.NewLine + Error.InnerException.StackTrace;
                    }
                }
                ErrorFX.mostrar(Error, true, true, "GenerarImpuestos - 1264 - Calculo de Totales " + mensaje, false);
            }
            return null;
        }

        internal ModeloComprobantePago.pagoRetencion GenerarRetencionNuevo(pago oPagoTr)
        {
            try
            {

                ModeloComprobantePago.pagoRetencion OpagoRetencion = new pagoRetencion();
                ModeloComprobantePago.documentosfxEntities dbContext = new documentosfxEntities();
                OpagoRetencion = dbContext.pagoRetencionSet.Where(a => a.pago.Id.Equals(oPagoTr.Id)).FirstOrDefault();
                if (OpagoRetencion == null)
                {
                    OpagoRetencion = new pagoRetencion();
                }
                if (OpagoRetencion.Id != 0)
                {
                    dbContext.pagoRetencionSet.Remove(OpagoRetencion);
                    dbContext.SaveChanges();
                    OpagoRetencion = new pagoRetencion();
                }
                //Rodrigo Escalona 04/0/2023 Cargar las lineas guardadas
                //var Impuestos = dbContext.pagoLineaRetencionSet.Where(a => a.pagoLinea.pago.Id.Equals(oPagoTr.Id))
                //    .GroupBy(l => l.TasaOCuotaDR)
                //    .SelectMany(cl => cl.Select(
                //    csLine => new ResultLine
                //    {
                //        TasaOCuotaDR = csLine.TasaOCuotaDR,
                //        BaseDR = cl.Sum(c => c.BaseDR),
                //        ImporteDR = cl.Sum(c => c.ImporteDR),
                //    })).ToList<ResultLine>();
                var ImpuestosTr = dbContext.pagoLineaRetencionSet.Where(a => a.pagoLinea.pago.Id.Equals(oPagoTr.Id));
                List<ResultLine> Impuestos = new List<ResultLine>();
                ResultLine ImpuestoTotal = new ResultLine();
                foreach (pagoLineaRetencion impuestoTr in ImpuestosTr)
                {
                    ImpuestoTotal.TasaOCuotaDR = impuestoTr.TasaOCuotaDR;
                    ImpuestoTotal.BaseDR += impuestoTr.BaseDR / decimal.Parse(impuestoTr.pagoLinea.EquivalenciaDR.ToString());
                    ImpuestoTotal.ImporteDR += impuestoTr.ImporteDR / decimal.Parse(impuestoTr.pagoLinea.EquivalenciaDR.ToString());
                }
                if (ImpuestoTotal.BaseDR > 0)
                {
                    Impuestos.Add(ImpuestoTotal);
                    decimal TipoCambioTr = (decimal)oPagoTr.tipoCambioP;

                    foreach (ResultLine oDataRow in Impuestos)
                    {
                        OpagoRetencion.TipoFactorP = "Tasa";
                        OpagoRetencion.ImpuestoP = "002";
                        OpagoRetencion.TasaOCuotaP = oDataRow.TasaOCuotaDR;
                        OpagoRetencion.BaseP = oDataRow.BaseDR;
                        OpagoRetencion.ImporteP = oDataRow.ImporteDR;
                    }

                    if (OpagoRetencion.Id == 0)
                    {
                        pago oRegistroTr = dbContext.pagoSet.Where(a => a.Id.Equals(oPagoTr.Id)).FirstOrDefault();
                        OpagoRetencion.pago = oRegistroTr;
                        dbContext.pagoRetencionSet.Add(OpagoRetencion);
                        dbContext.SaveChanges();
                    }
                    else
                    {
                        dbContext.pagoRetencionSet.Attach(OpagoRetencion);
                        dbContext.Entry(OpagoRetencion).State = System.Data.Entity.EntityState.Modified;
                        dbContext.SaveChanges();
                    }
                }
                return OpagoRetencion;
            }
            catch (Exception Error)
            {
                string mensaje = "";
                if (Error.InnerException != null)
                {
                    mensaje += Environment.NewLine + Error.InnerException.Message.ToString();
                    if (Error.InnerException.StackTrace != null)
                    {
                        mensaje += Environment.NewLine + Error.InnerException.StackTrace;
                    }
                }
                ErrorFX.mostrar(Error, true, true, "GenerarImpuestos - 1264 - Calculo de Totales " + mensaje, false);
            }
            return null;
        }

        public decimal RoundUp(decimal input, int places)
        {
            decimal multiplier = (decimal)Math.Pow(10, Convert.ToDouble(places));
            return Math.Ceiling(input * multiplier) / multiplier;
        }


        private ModeloComprobantePago.pagoRetencion GenerarRetencion(pago oPagoTr, Encapsular oEncapsular, string cuentaRetenido)
        {
            try
            {
                //Realizar las retenciones
                string ACCOUNT_ID_in = string.Empty;
                if (!String.IsNullOrEmpty(cuentaRetenido))
                {
                    char[] delimiterChars = { ',' };
                    string[] ACCOUNT_ID_Arreglo = cuentaRetenido.Split(delimiterChars);
                    foreach (string account_id in ACCOUNT_ID_Arreglo)
                    {
                        if (ACCOUNT_ID_in == "")
                        {
                            ACCOUNT_ID_in += "'" + account_id + "'";
                        }
                        else
                        {
                            ACCOUNT_ID_in += ",'" + account_id + "'";
                        }
                    }
                }
                else
                {
                    return null;
                }
                ModeloComprobantePago.pagoRetencion OpagoRetencion = new pagoRetencion();
                ModeloComprobantePago.documentosfxEntities dbContext = new documentosfxEntities();
                OpagoRetencion = dbContext.pagoRetencionSet.Where(a => a.pago.Id.Equals(oPagoTr.Id)).FirstOrDefault();
                if (OpagoRetencion == null)
                {
                    OpagoRetencion = new pagoRetencion();
                }
                string Sql = @"
                SELECT SUM(tabla.Total) as Total, SUM(tabla.TotalImpuesto) as TotalImpuesto
                FROM CASH_RECEIPT AS cr 
                INNER JOIN CASH_RECEIPT_LINE AS crl ON cr.CUSTOMER_ID = crl.CUSTOMER_ID AND cr.CHECK_ID = crl.CHECK_ID 
                INNER JOIN 
                (
                    SELECT SUM(tabla2.BaseDR) as Total, SUM(tabla2.ImpuestoDR) as TotalImpuesto, tabla2.INVOICE_ID
                    FROM
                    (
	                    SELECT rl.INVOICE_ID,COALESCE((SELECT SUM(rl2.AMOUNT) FROM RECEIVABLE_LINE rl2 WHERE rl2.INVOICE_ID=rl.INVOICE_ID),0) as BaseDR
	                    , ABS(rl.AMOUNT) as ImpuestoDR
	                    FROM RECEIVABLE_LINE rl
	                    WHERE rl.GL_ACCOUNT_ID IN (" + ACCOUNT_ID_in + @")
                    ) as tabla2  
                    GROUP BY   tabla2.INVOICE_ID
                ) as Tabla
                ON Tabla.INVOICE_ID=crl.INVOICE_ID
                WHERE cr.CHECK_ID='" + oPagoTr.checkId + "' AND cr.CUSTOMER_ID='" + oPagoTr.customerId + @"' AND crl.AMOUNT>0 
                HAVING NOT(SUM(tabla.Total)) IS NULL
                ";
                DataTable oDataTable = oEncapsular.oData_ERP.EjecutarConsulta(Sql);
                if (oDataTable != null)
                {
                    if (oDataTable.Rows.Count == 0)
                    {
                        if (OpagoRetencion.Id != 0)
                        {

                            dbContext.pagoRetencionSet.Remove(OpagoRetencion);
                            dbContext.SaveChanges();

                        }

                    }
                    else
                    {
                        pago oRegistroTr = dbContext.pagoSet.Where(a => a.Id.Equals(oPagoTr.Id)).FirstOrDefault();
                        //Rodrigo Escalona: 29/03/2023 Eliminar los Trasladados
                        if (oRegistroTr.pagoImpuesto.Count > 0)
                        {
                            dbContext.pagoImpuestoSet.RemoveRange(oRegistroTr.pagoImpuesto);
                            dbContext.SaveChanges();
                        }


                        foreach (DataRow oDataRow in oDataTable.Rows)
                        {
                            OpagoRetencion.TipoFactorP = "Tasa";
                            OpagoRetencion.ImpuestoP = "002";
                            OpagoRetencion.TasaOCuotaP = decimal.Parse("0.16");
                            OpagoRetencion.BaseP = decimal.Parse(oDataRow["Total"].ToString());
                            OpagoRetencion.ImporteP = decimal.Parse(oDataRow["TotalImpuesto"].ToString());
                        }

                        if (OpagoRetencion.Id == 0)
                        {

                            OpagoRetencion.pago = oRegistroTr;
                            dbContext.pagoRetencionSet.Add(OpagoRetencion);
                            dbContext.SaveChanges();
                        }
                        else
                        {
                            dbContext.pagoRetencionSet.Attach(OpagoRetencion);
                            dbContext.Entry(OpagoRetencion).State = System.Data.Entity.EntityState.Modified;
                            dbContext.SaveChanges();
                        }
                    }

                }





                return OpagoRetencion;
            }
            catch (Exception Error)
            {
                string mensaje = "";
                if (Error.InnerException != null)
                {
                    mensaje += Environment.NewLine + Error.InnerException.Message.ToString();
                    if (Error.InnerException.StackTrace != null)
                    {
                        mensaje += Environment.NewLine + Error.InnerException.StackTrace;
                    }
                }
                ErrorFX.mostrar(Error, true, true, "GenerarImpuestos - 1264 - Calculo de Totales " + mensaje, false);
            }
            return null;
        }

        private void GenerarLineaImpuestoPago(pagoLinea linea, Encapsular oEncapsular)
        {
            try
            {
                //Buscar la linea para asociarla
                ModeloComprobantePago.pagoLinea Linea = new pagoLinea();
                ModeloComprobantePago.documentosfxEntities dbContext = new documentosfxEntities();
                Linea = dbContext.pagoLineaSet.Where(a => a.Id.Equals(linea.Id)).FirstOrDefault();
                if (Linea == null)
                {
                    Linea = new pagoLinea();
                }
                string InvoiceIdTr = Linea.serie + Linea.folio;
                //Rodrigo Escalona: 14/06/2023 Agregue 
                //--HAVING rl.VAT_PERCENT <> 0
                string Sql = @"
                SELECT SUM(rl.AMOUNT) as BaseDR, SUM(rl.VAT_AMOUNT) as ImpuestoDR, MAX(rl.VAT_PERCENT) as Porcentaje
                FROM RECEIVABLE_LINE rl
                WHERE rl.INVOICE_ID='" + InvoiceIdTr + @"'
                GROUP BY rl.VAT_PERCENT
                ";
                DataTable oDataTable = oEncapsular.oData_ERP.EjecutarConsulta(Sql);
                if (oDataTable != null)
                {
                    if (oDataTable.Rows.Count == 0)
                    {
                        if (Linea.Id != 0)
                        {
                            if (Linea.pagoLineaImpuesto.Count > 0)
                            {
                                dbContext.pagoLineaImpuestoSet.RemoveRange(Linea.pagoLineaImpuesto);
                                dbContext.SaveChanges();
                            }
                        }

                    }
                    else
                    {
                        foreach (DataRow oDataRow in oDataTable.Rows)
                        {
                            decimal Porcentaje = decimal.Parse(oDataRow["Porcentaje"].ToString());

                            pagoLineaImpuesto OpagoLineaImpuesto = new pagoLineaImpuesto();
                            OpagoLineaImpuesto = Linea.pagoLineaImpuesto.Where(a => a.TasaOCuotaDR.Equals(Porcentaje)).FirstOrDefault();
                            if (OpagoLineaImpuesto == null)
                            {
                                OpagoLineaImpuesto = new pagoLineaImpuesto();
                            }



                            //Math.Round(decimal.Parse(oDataRow["BaseDR"].ToString()), 2, MidpointRounding.AwayFromZero);//decimal.Parse(oDataRow["BaseDR"].ToString());
                            OpagoLineaImpuesto.TasaOCuotaDR = Porcentaje;
                            OpagoLineaImpuesto.TipoFactorDR = "Tasa";
                            OpagoLineaImpuesto.ImpuestoDR = "002";

                            decimal ImpPagadoTr = (decimal)linea.ImpPagado;
                            decimal EquivalenciaDRTr = (decimal)linea.EquivalenciaDR;
                            //ImpPagadoTr = Math.Round(ImpPagadoTr / EquivalenciaDRTr, 2, MidpointRounding.AwayFromZero);

                            if (Porcentaje == 0)
                            {
                                OpagoLineaImpuesto.ImporteDR = 0;
                                OpagoLineaImpuesto.BaseDR = ImpPagadoTr;
                            }
                            else
                            {
                                OpagoLineaImpuesto.ImporteDR = Math.Round((decimal)OpagoLineaImpuesto.BaseDR * (decimal)0.16, 4, MidpointRounding.AwayFromZero);

                                OpagoLineaImpuesto.BaseDR = Math.Round(ImpPagadoTr - OpagoLineaImpuesto.ImporteDR, 4, MidpointRounding.AwayFromZero); //Math.Round(ImpPagadoTr / (decimal)1.16, 2, MidpointRounding.AwayFromZero); //RoundUp(decimal.Parse(oDataRow["BaseDR"].ToString()), 2);



                            }

                            //Verificar si la sumatoria de los dos no cuadran

                            //Verificar que el ImporteDR no este en 0
                            if (OpagoLineaImpuesto.ImporteDR == 0)
                            {
                                if (OpagoLineaImpuesto.TasaOCuotaDR==16)
                                {
                                    decimal BaseDRTr = Math.Round(ImpPagadoTr / 1.16M, 4);
                                    decimal ImporteDRTr = ImpPagadoTr - BaseDRTr;
                                    OpagoLineaImpuesto.BaseDR = BaseDRTr;
                                    OpagoLineaImpuesto.ImporteDR = ImporteDRTr;
                                    Generales.BitacoraFX.Log("Calculo ImporteDR " + OpagoLineaImpuesto.ImporteDR.ToString());
                                    Generales.BitacoraFX.Log("Recalculo BaseDR " + OpagoLineaImpuesto.BaseDR.ToString());
                                }
                            }

                            //decimal.Parse(oDataRow["ImpuestoDR"].ToString());
                            if (OpagoLineaImpuesto.Id == 0)
                            {
                                OpagoLineaImpuesto.pagoLinea = Linea;
                                dbContext.pagoLineaImpuestoSet.Add(OpagoLineaImpuesto);
                                dbContext.SaveChanges();
                            }
                            else
                            {
                                dbContext.pagoLineaImpuestoSet.Attach(OpagoLineaImpuesto);
                                dbContext.Entry(OpagoLineaImpuesto).State = System.Data.Entity.EntityState.Modified;
                                dbContext.SaveChanges();
                            }
                        }
                    }
                }
            }
            catch (DbEntityValidationException e)
            {
                string mensaje = "";
                foreach (var eve in e.EntityValidationErrors)
                {
                    foreach (var ve in eve.ValidationErrors)
                    {
                        mensaje += Environment.NewLine + ve.PropertyName + " Error: " + ve.ErrorMessage;
                    }
                }

                ErrorFX.mostrar("cComprobantePago - GenerarLineaImpuestoPago - 1193 " + mensaje, true, true, false);
            }
            catch (Exception e)
            {
                string mensaje = "";
                if (e.InnerException != null)
                {
                    mensaje += Environment.NewLine + e.InnerException.Message.ToString();
                    if (e.InnerException.StackTrace != null)
                    {
                        mensaje += Environment.NewLine + e.InnerException.StackTrace;
                    }
                }
                ErrorFX.mostrar(e, true, true, "cComprobantePago - GenerarLineaImpuestoPago - 1206 " + mensaje, false);
            }
        }
        private void GenerarLineaRetencionPago(pagoLinea linea, Encapsular oEncapsular, string cuentaRetenido)
        {
            try
            {
                //Realizar las retenciones
                string ACCOUNT_ID_in = "";
                if (!String.IsNullOrEmpty(cuentaRetenido))
                {
                    char[] delimiterChars = { ',' };
                    string[] ACCOUNT_ID_Arreglo = cuentaRetenido.Split(delimiterChars);

                    foreach (string account_id in ACCOUNT_ID_Arreglo)
                    {
                        if (ACCOUNT_ID_in == "")
                        {
                            ACCOUNT_ID_in += "'" + account_id + "'";
                        }
                        else
                        {
                            ACCOUNT_ID_in += ",'" + account_id + "'";
                        }
                    }
                }
                else
                {
                    return;
                }

                //Buscar la linea para asociarla
                ModeloComprobantePago.pagoLinea Linea = new pagoLinea();
                ModeloComprobantePago.documentosfxEntities dbContext = new documentosfxEntities();
                Linea = dbContext.pagoLineaSet.Where(a => a.Id.Equals(linea.Id)).FirstOrDefault();
                if (Linea == null)
                {
                    Linea = new pagoLinea();
                }




                string InvoiceIdTr = Linea.serie + Linea.folio;
                string Sql = @"

                SELECT SUM(tabla.BaseDR) as BaseDR, SUM(tabla.ImpuestoDR) as ImpuestoDR
                ,SUM(tabla.BaseDR)+SUM(tabla.ImpuestoDR) as BaseDRAnterior
                FROM
                (
	                SELECT COALESCE((SELECT SUM(rl2.AMOUNT) FROM RECEIVABLE_LINE rl2 WHERE rl2.INVOICE_ID=rl.INVOICE_ID),0) as BaseDR
	                , ABS(rl.AMOUNT) as ImpuestoDR
	                FROM RECEIVABLE_LINE rl
	                WHERE rl.INVOICE_ID='" + InvoiceIdTr + @"'
	                AND rl.GL_ACCOUNT_ID IN (" + ACCOUNT_ID_in + @")
                ) as tabla 
                HAVING NOT(SUM(tabla.BaseDR)) IS NULL
                ";
                DataTable oDataTable = oEncapsular.oData_ERP.EjecutarConsulta(Sql);
                if (oDataTable != null)
                {
                    if (oDataTable.Rows.Count == 0)
                    {
                        if (Linea.Id != 0)
                        {
                            if (Linea.pagoLineaRetencion.Count > 0)
                            {
                                dbContext.pagoLineaRetencionSet.RemoveRange(Linea.pagoLineaRetencion);
                                dbContext.SaveChanges();
                            }
                        }

                    }
                    else
                    {
                        //Rodrigo Escalona: 29/03/2023 Eliminar los trasladados
                        if (Linea.pagoLineaImpuesto.Count > 0)
                        {
                            dbContext.pagoLineaImpuestoSet.RemoveRange(Linea.pagoLineaImpuesto);
                            dbContext.SaveChanges();
                        }

                        foreach (DataRow oDataRow in oDataTable.Rows)
                        {
                            decimal Porcentaje = 16;

                            pagoLineaRetencion OpagoLineaRetencion = new pagoLineaRetencion();
                            OpagoLineaRetencion = Linea.pagoLineaRetencion.Where(a => a.TasaOCuotaDR.Equals(Porcentaje)).FirstOrDefault();
                            if (OpagoLineaRetencion == null)
                            {
                                OpagoLineaRetencion = new pagoLineaRetencion();
                            }
                            OpagoLineaRetencion.BaseDR = decimal.Parse(oDataRow["BaseDR"].ToString());
                            OpagoLineaRetencion.TasaOCuotaDR = Porcentaje;
                            OpagoLineaRetencion.TipoFactorDR = "Tasa";
                            OpagoLineaRetencion.ImpuestoDR = "002";

                            OpagoLineaRetencion.ImporteDR = Math.Round(decimal.Parse(oDataRow["ImpuestoDR"].ToString()), 2, MidpointRounding.AwayFromZero);//decimal.Parse(oDataRow["ImpuestoDR"].ToString());
                            if (OpagoLineaRetencion.Id == 0)
                            {
                                OpagoLineaRetencion.pagoLinea = Linea;
                                dbContext.pagoLineaRetencionSet.Add(OpagoLineaRetencion);
                                dbContext.SaveChanges();
                            }
                            else
                            {
                                dbContext.pagoLineaRetencionSet.Attach(OpagoLineaRetencion);
                                dbContext.Entry(OpagoLineaRetencion).State = System.Data.Entity.EntityState.Modified;
                                dbContext.SaveChanges();
                            }
                        }
                    }
                }
            }
            catch (DbEntityValidationException e)
            {
                string mensaje = "";
                foreach (var eve in e.EntityValidationErrors)
                {
                    foreach (var ve in eve.ValidationErrors)
                    {
                        mensaje += Environment.NewLine + ve.PropertyName + " Error: " + ve.ErrorMessage;
                    }
                }

                ErrorFX.mostrar("cComprobantePago - GenerarLineaImpuestoPago - 1193 " + mensaje, true, true, false);
            }
            catch (Exception e)
            {
                string mensaje = "";
                if (e.InnerException != null)
                {
                    mensaje += Environment.NewLine + e.InnerException.Message.ToString();
                    if (e.InnerException.StackTrace != null)
                    {
                        mensaje += Environment.NewLine + e.InnerException.StackTrace;
                    }
                }
                ErrorFX.mostrar(e, true, true, "cComprobantePago - GenerarLineaImpuestoPago - 1206 " + mensaje, false);
            }
        }

        private void EliminarLineasFacturasNoExistentes(pago oPagoP, string checkId, string customerId)
        {

        }

        private pago GenerarCabeceraPago(string checkId, string customerId, Encapsular oEncapsular, cSERIE ocSERIE
            , string estado, string version, cCERTIFICADO ocCERTIFICADO)
        {
            try
            {
                string Sql = @"
                SELECT cr.CUSTOMER_ID, c.NAME, cr.CHECK_ID, cr.BANK_ACCOUNT_ID, ba.DESCRIPTION, cr.AMOUNT
                , cr.USER_ID, cr.CHECK_DATE, cr.STATUS, cr.CURRENCY_ID, cr.PAYMENT_METHOD, c.TAX_ID_NUMBER
                , c.VAT_REGISTRATION,c.BILL_TO_COUNTRY, c.BILL_TO_ZIPCODE, c.ZIPCODE
                , ISNULL(crc.SELL_RATE,cr.SELL_RATE) as TipoCambio
                , c.ADDR_3, cr.DEPOSIT_ID
                FROM CASH_RECEIPT AS cr 
                INNER JOIN CUSTOMER AS c ON cr.CUSTOMER_ID = c.ID 
                INNER JOIN BANK_ACCOUNT AS ba ON cr.BANK_ACCOUNT_ID = ba.ID 
                INNER JOIN CASH_RECEIPT_CURR AS crc ON cr.CUSTOMER_ID = crc.CUSTOMER_ID AND cr.CHECK_ID = crc.CHECK_ID AND cr.CURRENCY_ID = crc.CURRENCY_ID
                WHERE cr.CHECK_ID='" + checkId + "' AND cr.CUSTOMER_ID='" + customerId + @"' 
                ";
                BitacoraFX.Log("SQL " + Environment.NewLine + Sql);
                DataTable oDataTable = oEncapsular.oData_ERP.EjecutarConsulta(Sql);
                if (oDataTable != null)
                {
                    ModeloComprobantePago.documentosfxEntities dbContext = new documentosfxEntities();
                    foreach (DataRow oDataRow in oDataTable.Rows)
                    {
                        //Rodrigo Escalona: 27/03/2023 Verificar si existe el registro en pagos
                        pago oRegistro = dbContext.pagoSet.Where(a => a.checkId.Equals(checkId)
                                & a.customerId.Equals(customerId)).FirstOrDefault();
                        if (oRegistro == null)
                        {
                            oRegistro = new pago();
                        }

                        oRegistro.version = version;
                        oRegistro.editado = false;
                        oRegistro.deposit = oDataRow["DEPOSIT_ID"].ToString();
                        oRegistro.checkId = oDataRow["CHECK_ID"].ToString();
                        oRegistro.customerId = oDataRow["CUSTOMER_ID"].ToString();
                        oRegistro.folio = ocSERIE.consecutivo(ocSERIE.ROW_ID);
                        ocSERIE.incrementar(ocSERIE.ROW_ID);
                        oRegistro.serie = ocSERIE.ID;
                        oRegistro.fecha = DateTime.Now;
                        oRegistro.estado = estado;
                        oRegistro.monedaP = traducirMoneda(oDataRow["CURRENCY_ID"].ToString());
                        oRegistro.monto = 0;

                        DateTime fechaPagoTr = DateTime.Now;
                        try
                        {
                            fechaPagoTr = DateTime.Parse(oDataRow["CHECK_DATE"].ToString());
                        }
                        catch (Exception e)
                        {
                            string mensaje = "";
                            if (e.InnerException != null)
                            {
                                mensaje += Environment.NewLine + e.InnerException.Message.ToString();
                                if (e.InnerException.StackTrace != null)
                                {
                                    mensaje += Environment.NewLine + e.InnerException.StackTrace;
                                }
                            }
                            ErrorFX.mostrar(e, true, true, "cComprobantePago - 453 - CHECK_DATE en Visual no es fecha" + mensaje, false);
                        }

                        //oRegistro.monto = 0;
                        oRegistro.tipoCambioP = decimal.Parse(oDataRow["TipoCambio"].ToString());

                        if (!MonedasDiferentes(oRegistro.checkId, oRegistro.customerId, oEncapsular.oData_ERP))
                        {
                            if (oRegistro.monedaP.Equals("MXN"))
                            {
                                oRegistro.tipoCambioP = 1;
                            }

                        }

                        oRegistro.fechaPago = new DateTime(fechaPagoTr.Year, fechaPagoTr.Month, fechaPagoTr.Day, 12, 0, 0);
                        oRegistro.noCertificado = ocCERTIFICADO.NO_CERTIFICADO;
                        oRegistro.lugarExpedicion = ocSERIE.CP;
                        oRegistro.emisor = oEncapsular.ocEMPRESA.RFC;
                        oRegistro.emisorNombre = oEncapsular.ocEMPRESA.ID;
                        oRegistro.emisorRegimenFiscal = oEncapsular.ocEMPRESA.REGIMEN_FISCAL;
                        oRegistro.receptor = oDataRow["VAT_REGISTRATION"].ToString();
                        oRegistro.receptorNombre = oDataRow["NAME"].ToString();

                        try
                        {
                            bool USAR_COMPLEMENTO_ADDR3Tr = bool.Parse(oEncapsular.ocEMPRESA.USAR_COMPLEMENTO_ADDR3);
                            if (USAR_COMPLEMENTO_ADDR3Tr)
                            {
                                oRegistro.receptorNombre += oDataRow["ADDR_3"].ToString();
                            }


                        }
                        catch
                        {


                        }
                        oRegistro.receptorNombre = Generar40.LimpiarNombre(oRegistro.receptorNombre);

                        if (!String.IsNullOrEmpty(oDataRow["TAX_ID_NUMBER"].ToString()))
                        {
                            oRegistro.receptorNumRegIdTrib = oDataRow["TAX_ID_NUMBER"].ToString();
                        }

                        try
                        {
                            oRegistro.receptorCodigoPostal = oDataRow["BILL_TO_ZIPCODE"].ToString();
                            if (oRegistro.receptorCodigoPostal.Length > 5)
                            {
                                oRegistro.receptorCodigoPostal = oRegistro.receptorCodigoPostal.Substring(0, 5);
                            }
                        }
                        catch (Exception error)
                        {
                            ErrorFX.mostrar(error, true, true, "cComprobantePago - 528 - Seccion de Domicilio del Receptor el campo BILL_TO_ZIPCODE esta Nulo", true);
                        }


                        if (oRegistro.receptorCodigoPostal.Length == 0)
                        {
                            try
                            {
                                oRegistro.receptorCodigoPostal = oDataRow["ZIPCODE"].ToString();
                                if (oRegistro.receptorCodigoPostal.Length > 5)
                                {
                                    oRegistro.receptorCodigoPostal = oRegistro.receptorCodigoPostal.Substring(0, 5);
                                }
                            }
                            catch (Exception error)
                            {
                                ErrorFX.mostrar(error, true, true, "cComprobantePago - 1907 - Seccion de Domicilio del Receptor el campo ZIPCODE esta Nulo", true);
                            }
                        }

                        try
                        {
                            oRegistro.receptorPais = oDataRow["BILL_TO_COUNTRY"].ToString();
                        }
                        catch (Exception error)
                        {
                            ErrorFX.mostrar(error, true, true, "cComprobantePago - 528 - Seccion de Domicilio del Receptor el campo BILL_TO_COUNTRY esta Nulo", true);
                        }

                        if (oRegistro.Id == 0)
                        {
                            dbContext.pagoSet.Add(oRegistro);
                            dbContext.SaveChanges();
                        }
                        else
                        {
                            dbContext.pagoSet.Attach(oRegistro);
                            dbContext.Entry(oRegistro).State = System.Data.Entity.EntityState.Modified;
                            dbContext.SaveChanges();
                        }
                        return oRegistro;
                    }

                }
                else
                {
                    ErrorFX.mostrar("cComprobantePago - GenerarCabeceraPago - 1199 - Consulta sin datos ", true, true, false);
                }

            }
            catch (DbEntityValidationException e)
            {
                string mensaje = "";
                foreach (var eve in e.EntityValidationErrors)
                {
                    foreach (var ve in eve.ValidationErrors)
                    {
                        mensaje += Environment.NewLine + ve.PropertyName + " Error: " + ve.ErrorMessage;
                    }
                }

                ErrorFX.mostrar("cComprobantePago - GenerarCabeceraPago - 1181 " + mensaje, true, true, false);
            }
            catch (Exception e)
            {
                string mensaje = "";
                if (e.InnerException != null)
                {
                    mensaje += Environment.NewLine + e.InnerException.Message.ToString();
                    if (e.InnerException.StackTrace != null)
                    {
                        mensaje += Environment.NewLine + e.InnerException.StackTrace;
                    }
                }
                ErrorFX.mostrar(e, true, true, "cComprobantePago - GenerarCabeceraPago - 1194 " + mensaje, false);
            }
            return null;
        }

        private bool ValidarFacturas(string checkId, string customerId, Encapsular encapsular)
        {
            try
            {
                string sSQLValidar = @"
                        SELECT crl.INVOICE_ID
                        FROM CASH_RECEIPT AS cr 
                        INNER JOIN CUSTOMER AS c ON cr.CUSTOMER_ID = c.ID 
                        INNER JOIN BANK_ACCOUNT AS ba ON cr.BANK_ACCOUNT_ID = ba.ID 
                        INNER JOIN CASH_RECEIPT_LINE AS crl ON cr.CUSTOMER_ID = crl.CUSTOMER_ID AND cr.CHECK_ID = crl.CHECK_ID 
                        INNER JOIN RECEIVABLE AS r ON c.ID = r.CUSTOMER_ID AND crl.INVOICE_ID = r.INVOICE_ID
                        INNER JOIN CASH_RECEIPT_CURR AS crc ON cr.CUSTOMER_ID = crc.CUSTOMER_ID AND cr.CHECK_ID = crc.CHECK_ID AND cr.CURRENCY_ID = crc.CURRENCY_ID
                        WHERE cr.CHECK_ID='" + checkId + "' AND cr.CUSTOMER_ID='" + customerId + "' AND crl.AMOUNT>0";
                DataTable oDataTableValidar = encapsular.oData_ERP.EjecutarConsulta(sSQLValidar);
                if (oDataTableValidar != null)
                {
                    string resultado = "";
                    foreach (DataRow oDataRow in oDataTableValidar.Rows)
                    {
                        string uuidTr = obtenerUUID(oDataRow["INVOICE_ID"].ToString(), encapsular, true);
                        if (String.IsNullOrEmpty(uuidTr))
                        {
                            resultado += " La factura " + oDataRow["INVOICE_ID"].ToString() + " no cumple con Metodo de pago: PPD y Forma de pago: 99 " + Environment.NewLine;
                        }
                    }

                    if (!String.IsNullOrEmpty(resultado))
                    {
                        ErrorFX.mostrar(resultado, true, true, true);
                        return false;
                    }
                }
            }
            catch (Exception Error)
            {
                string mensaje = "";
                if (Error.InnerException != null)
                {
                    mensaje += Environment.NewLine + Error.InnerException.Message.ToString();
                    if (Error.InnerException.StackTrace != null)
                    {
                        mensaje += Environment.NewLine + Error.InnerException.StackTrace;
                    }
                }
                ErrorFX.mostrar(Error, true, true, "cComprobantePago - ValidarFacturas - 1010 - " + mensaje, false);
            }
            return false;
        }


        private bool MonedasDiferentes(string checkId, string customerId, cCONEXCION oData_ERP)
        {
            String Sql = @"
            SELECT TOP 1 cr.CHECK_ID
            FROM  CASH_RECEIPT_LINE AS crl
            INNER JOIN CASH_RECEIPT AS cr ON crl.CUSTOMER_ID = cr.CUSTOMER_ID AND crl.CHECK_ID = cr.CHECK_ID
            INNER JOIN RECEIVABLE AS r ON cr.CUSTOMER_ID = r.CUSTOMER_ID AND crl.INVOICE_ID = r.INVOICE_ID
            WHERE cr.CHECK_ID='" + checkId + "' AND cr.CUSTOMER_ID='" + customerId + @"'
            AND r.CURRENCY_ID<> cr.CURRENCY_ID
            ";

            DataTable oDataTable = oData_ERP.EjecutarConsulta(Sql);
            if (oDataTable != null)
            {
                foreach (DataRow oDataRow in oDataTable.Rows)
                {
                    return true;
                }
            }
            return false;
        }

        private ModeloComprobantePago.pagoTotales CalcularTotales(pago oPagoTr, Encapsular oEncapsular, string cuentaRetencion)
        {
            try
            {
                ModeloComprobantePago.pagoTotales OPagosTotales = new pagoTotales();
                ModeloComprobantePago.documentosfxEntities dbContext = new documentosfxEntities();
                OPagosTotales = dbContext.pagoTotalesSet.Where(a => a.pago.Id.Equals(oPagoTr.Id)).FirstOrDefault();
                if (OPagosTotales == null)
                {
                    OPagosTotales = new pagoTotales();
                }
                decimal Total = 0;

                string Sql = @"
                SELECT SUM(tabla.Total) as Total, SUM(tabla.TotalImpuesto) as TotalImpuesto, tabla.Porcentaje
                FROM CASH_RECEIPT AS cr 
                INNER JOIN CASH_RECEIPT_LINE AS crl ON cr.CUSTOMER_ID = crl.CUSTOMER_ID AND cr.CHECK_ID = crl.CHECK_ID 
                INNER JOIN 
                (
                SELECT rl.INVOICE_ID,SUM(rl.AMOUNT) as Total, SUM(rl.VAT_AMOUNT) as TotalImpuesto, MAX(rl.VAT_PERCENT) as Porcentaje
                FROM RECEIVABLE_LINE rl
                GROUP BY rl.INVOICE_ID,rl.VAT_PERCENT
                ) as Tabla
                ON Tabla.INVOICE_ID=crl.INVOICE_ID
                WHERE cr.CHECK_ID='" + oPagoTr.checkId + "' AND cr.CUSTOMER_ID='" + oPagoTr.customerId + @"' AND crl.AMOUNT>0 
                GROUP BY tabla.Porcentaje
                ";
                DataTable oDataTable = oEncapsular.oData_ERP.EjecutarConsulta(Sql);
                if (oDataTable != null)
                {
                    OPagosTotales.MontoTotalPagos = 0;
                    foreach (DataRow oDataRow in oDataTable.Rows)
                    {
                        decimal TasaTr = Math.Truncate(decimal.Parse(oDataRow["Porcentaje"].ToString()));
                        switch (TasaTr)
                        {
                            case 16:
                                OPagosTotales.TotalTrasladosBaseIVA16 = decimal.Parse(oDataRow["Total"].ToString());
                                OPagosTotales.TotalTrasladosImpuestoIVA16 = decimal.Parse(oDataRow["TotalImpuesto"].ToString());
                                OPagosTotales.MontoTotalPagos += OPagosTotales.TotalTrasladosBaseIVA16 + OPagosTotales.TotalTrasladosImpuestoIVA16;
                                break;
                            case 0:
                                OPagosTotales.TotalTrasladosBaseIVA0 = decimal.Parse(oDataRow["Total"].ToString());
                                //OPagosTotales.TotalTrasladosImpuestoIVA0 = decimal.Parse(oDataRow["TotalImpuesto"].ToString());
                                OPagosTotales.MontoTotalPagos += decimal.Parse(oDataRow["Total"].ToString());
                                break;
                            case 8:
                                OPagosTotales.TotalTrasladosBaseIVA8 = decimal.Parse(oDataRow["Total"].ToString());
                                OPagosTotales.TotalTrasladosImpuestoIVA8 = decimal.Parse(oDataRow["TotalImpuesto"].ToString());
                                OPagosTotales.MontoTotalPagos += OPagosTotales.TotalTrasladosBaseIVA8 + OPagosTotales.TotalTrasladosImpuestoIVA8;
                                break;
                        }
                        Total += OPagosTotales.MontoTotalPagos;
                    }

                }
                //Cargar los pagos retenidos
                var RetencionesTr = dbContext.pagoRetencionSet.Where(a => a.pago.Id.Equals(oPagoTr.Id));
                OPagosTotales.TotalRetencionesIVA = 0;
                foreach (pagoRetencion RetencionTr in RetencionesTr)
                {
                    OPagosTotales.TotalTrasladosBaseIVA16 = 0;
                    OPagosTotales.TotalTrasladosImpuestoIVA16 = 0;
                    OPagosTotales.TotalRetencionesIVA += RetencionTr.ImporteP;
                    Total += RetencionTr.ImporteP;
                }



                //Guardar Total del Pago
                pago oRegistroTr = dbContext.pagoSet.Where(a => a.Id.Equals(oPagoTr.Id)).FirstOrDefault();
                oRegistroTr.monto = Total;
                dbContext.pagoSet.Attach(oRegistroTr);
                dbContext.Entry(oRegistroTr).State = System.Data.Entity.EntityState.Modified;
                dbContext.SaveChanges();

                if (OPagosTotales.Id == 0)
                {

                    OPagosTotales.pago = oRegistroTr;
                    dbContext.pagoTotalesSet.Add(OPagosTotales);
                    dbContext.SaveChanges();
                }
                else
                {
                    dbContext.pagoTotalesSet.Attach(OPagosTotales);
                    dbContext.Entry(OPagosTotales).State = System.Data.Entity.EntityState.Modified;
                    dbContext.SaveChanges();
                }



                return OPagosTotales;
            }
            catch (Exception Error)
            {
                string mensaje = "";
                if (Error.InnerException != null)
                {
                    mensaje += Environment.NewLine + Error.InnerException.Message.ToString();
                    if (Error.InnerException.StackTrace != null)
                    {
                        mensaje += Environment.NewLine + Error.InnerException.StackTrace;
                    }
                }
                ErrorFX.mostrar(Error, true, true, "cComprobantePago - 1015 - Calculo de Totales " + mensaje, false);
            }
            return null;
        }

        internal ModeloComprobantePago.pagoTotales CalcularTotalesNuevo(pago oPagoTr)
        {
            try
            {
                ModeloComprobantePago.pagoTotales OPagosTotales = new pagoTotales();
                ModeloComprobantePago.documentosfxEntities dbContext = new documentosfxEntities();
                OPagosTotales = dbContext.pagoTotalesSet.Where(a => a.pago.Id.Equals(oPagoTr.Id)).FirstOrDefault();
                if (OPagosTotales == null)
                {
                    OPagosTotales = new pagoTotales();
                }
                //decimal Total = 0;
                decimal TipoCambioP = oPagoTr.tipoCambioP != null ? (decimal)oPagoTr.tipoCambioP : 0;
                decimal MontoPagado = 0;
                var pagoLineas = dbContext.pagoLineaSet.Where(a => a.pago.Id.Equals(oPagoTr.Id));
                foreach (pagoLinea LineaTr in pagoLineas)
                {
                    decimal EquivalenciaDRTr = LineaTr.EquivalenciaDR;
                    MontoPagado += Math.Round((decimal)LineaTr.ImpPagado, 2, MidpointRounding.AwayFromZero); //decimal.Parse(LineaTr.ImpPagado.ToString())/EquivalenciaDRTr;
                }
                decimal MontoPagadoConvertido = Math.Round(MontoPagado * TipoCambioP, 2, MidpointRounding.AwayFromZero);
                OPagosTotales.MontoTotalPagos = MontoPagadoConvertido;


                //Rodrigo Escalona 04/0/2023 Calculo de Impuestos
                var ImpuestosTr = dbContext.pagoLineaImpuestoSet.Where(a => a.pagoLinea.pago.Id.Equals(oPagoTr.Id));
                List<ResultLine> Impuestos = new List<ResultLine>();
                ResultLine ImpuestoTotal = new ResultLine();
                foreach (pagoLineaImpuesto impuestoTr in ImpuestosTr)
                {
                    ImpuestoTotal.TasaOCuotaDR = impuestoTr.TasaOCuotaDR;
                    ImpuestoTotal.BaseDR += impuestoTr.BaseDR / impuestoTr.pagoLinea.EquivalenciaDR;
                    ImpuestoTotal.ImporteDR += impuestoTr.ImporteDR / impuestoTr.pagoLinea.EquivalenciaDR;
                }
                if (ImpuestoTotal.BaseDR > 0)
                {
                    Impuestos.Add(ImpuestoTotal);
                    if (Impuestos.Count > 0)
                    {
                        foreach (ResultLine oDataRow in Impuestos)
                        {
                            switch (oDataRow.TasaOCuotaDR)
                            {
                                case 16:
                                    OPagosTotales.TotalTrasladosBaseIVA16 = Math.Round(oDataRow.BaseDR * TipoCambioP, 2, MidpointRounding.AwayFromZero); //Math.Round(oDataRow.BaseDR * TipoCambioP, 2);//RoundUp(oDataRow.BaseDR* TipoCambioP,2);
                                    OPagosTotales.TotalTrasladosImpuestoIVA16 = Math.Round(oDataRow.ImporteDR * TipoCambioP, 2, MidpointRounding.AwayFromZero);  // Math.Round(oDataRow.ImporteDR * TipoCambioP, 2);//RoundUp(oDataRow.ImporteDR * TipoCambioP,2);
                                    BitacoraFX.Log("Sumo base 16: " + OPagosTotales.MontoTotalPagos.ToString());
                                    break;
                                case 0:
                                    OPagosTotales.TotalTrasladosBaseIVA0 = MontoPagadoConvertido;
                                    break;
                                case 8:
                                    OPagosTotales.TotalTrasladosBaseIVA8 = oDataRow.BaseDR * TipoCambioP;
                                    OPagosTotales.TotalTrasladosImpuestoIVA8 = oDataRow.ImporteDR * TipoCambioP;
                                    //OPagosTotales.MontoTotalPagos += OPagosTotales.TotalTrasladosBaseIVA8 + OPagosTotales.TotalTrasladosImpuestoIVA8;
                                    BitacoraFX.Log("Sumo base 8: " + OPagosTotales.MontoTotalPagos.ToString());
                                    break;
                            }
                        }
                    }
                }

                //Rodrigo Escalona 14/06/2023 Calculo de Retenciones
                var RetencionesTr = dbContext.pagoLineaRetencionSet.Where(a => a.pagoLinea.pago.Id.Equals(oPagoTr.Id));
                List<ResultLine> Retenciones = new List<ResultLine>();
                ResultLine RetencionesTotal = new ResultLine();
                foreach (pagoLineaRetencion impuestoTr in RetencionesTr)
                {
                    RetencionesTotal.TasaOCuotaDR = impuestoTr.TasaOCuotaDR;
                    RetencionesTotal.BaseDR += impuestoTr.BaseDR / impuestoTr.pagoLinea.EquivalenciaDR;
                    RetencionesTotal.ImporteDR += impuestoTr.ImporteDR / impuestoTr.pagoLinea.EquivalenciaDR;
                }
                if (RetencionesTotal.BaseDR > 0)
                {
                    Retenciones.Add(RetencionesTotal);
                    if (Retenciones.Count > 0)
                    {
                        foreach (ResultLine oDataRow in Retenciones)
                        {
                            OPagosTotales.TotalRetencionesIVA = Math.Round(oDataRow.ImporteDR * TipoCambioP, 2, MidpointRounding.AwayFromZero);
                            BitacoraFX.Log("TotalRetencionesIVA: " + OPagosTotales.MontoTotalPagos.ToString());
                        }
                    }
                }

                //Guardar Total del Pago
                pago oRegistroTr = dbContext.pagoSet.Where(a => a.Id.Equals(oPagoTr.Id)).FirstOrDefault();
                oRegistroTr.monto = MontoPagado;
                dbContext.pagoSet.Attach(oRegistroTr);
                dbContext.Entry(oRegistroTr).State = System.Data.Entity.EntityState.Modified;
                dbContext.SaveChanges();

                if (OPagosTotales.Id == 0)
                {

                    OPagosTotales.pago = oRegistroTr;
                    dbContext.pagoTotalesSet.Add(OPagosTotales);
                    dbContext.SaveChanges();
                }
                else
                {
                    dbContext.pagoTotalesSet.Attach(OPagosTotales);
                    dbContext.Entry(OPagosTotales).State = System.Data.Entity.EntityState.Modified;
                    dbContext.SaveChanges();
                }



                return OPagosTotales;
            }
            catch (Exception Error)
            {
                string mensaje = "";
                if (Error.InnerException != null)
                {
                    mensaje += Environment.NewLine + Error.InnerException.Message.ToString();
                    if (Error.InnerException.StackTrace != null)
                    {
                        mensaje += Environment.NewLine + Error.InnerException.StackTrace;
                    }
                }
                ErrorFX.mostrar(Error, true, true, "cComprobantePago - 1015 - Calculo de Totales " + mensaje, false);
            }
            return null;
        }

        internal void relacionar(string uUIDtr, string tipoRelacion, string checkId, string customerId)
        {
            try
            {

                ModeloComprobantePago.documentosfxEntities dbContext = new documentosfxEntities();

                pagoRelacionado opagoRelacionado = dbContext.pagoRelacionadoSet.Where(b => b.checkId == checkId
                & b.customerId == customerId
                & b.UUID == uUIDtr
                ).FirstOrDefault();

                if (opagoRelacionado == null)
                {
                    opagoRelacionado = new pagoRelacionado();
                    opagoRelacionado.linea = 0;
                    opagoRelacionado.tipoRelacion = tipoRelacion;
                    opagoRelacionado.checkId = checkId;
                    opagoRelacionado.customerId = customerId;
                    opagoRelacionado.UUID = uUIDtr;
                    dbContext.pagoRelacionadoSet.Add(opagoRelacionado);
                    dbContext.SaveChanges();
                }
                dbContext.Dispose();
            }
            catch (Exception e)
            {
                ErrorFX.mostrar(e, true, true, "cComprobantePago - 547", false);
            }
        }

        internal void eliminarRelaciones(string checkId, string customerId)
        {
            try
            {
                ModeloComprobantePago.documentosfxEntities dbContext = new documentosfxEntities();
                var pagos = dbContext.pagoRelacionadoSet.Where(b => b.checkId == checkId
                & b.customerId == customerId);
                if (pagos != null)
                {
                    dbContext.pagoRelacionadoSet.RemoveRange(pagos);
                    dbContext.SaveChanges();
                }
                dbContext.Dispose();
            }
            catch (Exception e)
            {
                ErrorFX.mostrar(e, true, true, "cComprobantePago - 535", false);
            }
        }

        private decimal buscarTCFactura(string invoiceId, Encapsular oEncapsular)
        {
            try
            {
                string sSQL = @"
                SELECT TOP 1 BUY_RATE
                FROM RECEIVABLE r
                WHERE r.INVOICE_ID = = '" + invoiceId + @"'";
                DataTable oDataTable = oEncapsular.oData_ERP.EjecutarConsulta(sSQL);
                if (oDataTable != null)
                {
                    foreach (DataRow oDataRow in oDataTable.Rows)
                    {
                        return decimal.Parse(oDataRow["BUY_RATE"].ToString());
                    }
                }
            }
            catch (Exception e)
            {
                ErrorFX.mostrar(e, true, true, "cComprobantePago - 772", false);
            }
            return 1;
        }

        private decimal buscarTC(string checkId, string customerId, string CURRENCY_ID, Encapsular oEncapsular)
        {
            try
            {
                string sSQL = @"
                SELECT TOP 1 SELL_RATE
                FROM CASH_RECEIPT_CURR crc
                WHERE crc.CHECK_ID = '" + checkId + @"' AND crc.CUSTOMER_ID = '" + customerId + @"' AND crc.CURRENCY_ID='" + CURRENCY_ID + @"'";
                DataTable oDataTable = oEncapsular.oData_ERP.EjecutarConsulta(sSQL);
                if (oDataTable != null)
                {
                    foreach (DataRow oDataRow in oDataTable.Rows)
                    {
                        return decimal.Parse(oDataRow["SELL_RATE"].ToString());
                    }
                }
            }
            catch (Exception e)
            {
                ErrorFX.mostrar(e, true, true, "cComprobantePago - 381", false);
            }
            return 1;
        }

        internal void eliminar(string CHECK_ID, string CUSTOMER_ID)
        {
            try
            {
                ModeloComprobantePago.documentosfxEntities dbContext = new documentosfxEntities();
                string conection = dbContext.Database.Connection.ConnectionString;
                cCONEXCION oData = new cCONEXCION(conection);

                string sSQL = @"DELETE FROM pagoLineaSet
                            FROM pagoSet AS p INNER JOIN
                            pagoLineaSet ON p.Id = pagoLineaSet.pago_Id
                            WHERE (p.customerId = N'" + CUSTOMER_ID + @"') AND (p.checkId = N'" + CHECK_ID + @"')";
                oData.EjecutarConsulta(sSQL);

                sSQL = @" DELETE
                            FROM pagoSet
                            WHERE (customerId = N'" + CUSTOMER_ID + @"') AND (checkId = N'" + CHECK_ID + @"')";
                oData.EjecutarConsulta(sSQL);
            }
            catch (Exception e)
            {
                ErrorFX.mostrar(e, true, true, "cComprobantePago - 859", false);
            }
        }

        private decimal montoAplicadoNDC(string CHECK_ID, DateTime dateTime)
        {
            try
            {

            }
            catch (Exception e)
            {
                ErrorFX.mostrar(e, true, true, "cComprobantePago - 246", false);
            }
            return 0;
        }
        internal string errorNDC = "";
        internal List<ndcAplicadas> validarNDCListado(string checkId, string customerId, Encapsular oEncapsular)
        {
            List<ndcAplicadas> lista = new List<ndcAplicadas>();
            try
            {
                errorNDC = "";
                //cEMPRESA ocEMPRESA = new cEMPRESA();
                List<string> noAsociados = new List<string>();
                //foreach (cEMPRESA registro in ocEMPRESA.todos("", true, Globales.automatico))
                //{
                //cCONEXCION oData_ERPp = new cCONEXCION(registro.TIPO, registro.SERVIDOR
                //                , registro.BD, registro.USUARIO_BD, registro.PASSWORD_BD);

                //Crear la conexión por cada una
                string sSQL = @"
                    SELECT crl.INVOICE_ID, crl.AMOUNT AS MontoAplicado
                    FROM CASH_RECEIPT AS cr 
                    INNER JOIN CUSTOMER AS c ON cr.CUSTOMER_ID = c.ID 
                    INNER JOIN BANK_ACCOUNT AS ba ON cr.BANK_ACCOUNT_ID = ba.ID 
                    INNER JOIN CASH_RECEIPT_LINE AS crl ON cr.CUSTOMER_ID = crl.CUSTOMER_ID AND cr.CHECK_ID = crl.CHECK_ID 
                    INNER JOIN RECEIVABLE AS r ON c.ID = r.CUSTOMER_ID AND crl.INVOICE_ID = r.INVOICE_ID
                    INNER JOIN CASH_RECEIPT_CURR AS crc ON cr.CUSTOMER_ID = crc.CUSTOMER_ID AND cr.CHECK_ID = crc.CHECK_ID AND cr.CURRENCY_ID = crc.CURRENCY_ID
                    WHERE cr.CHECK_ID='" + checkId + "' AND cr.CUSTOMER_ID='" + customerId + @"' AND crl.AMOUNT<0 
                    AND NOT(crl.INVOICE_ID is null) 
                    ";
                DataTable oDataTable = oEncapsular.oData_ERP.EjecutarConsulta(sSQL);
                if (oDataTable != null)
                {
                    //BitacoraFX.Log("validarNDCListado: " + sSQL);

                    ModeloComprobantePago.documentosfxEntities dbContext = new documentosfxEntities();
                    foreach (DataRow oDataRow in oDataTable.Rows)
                    {
                        decimal montoNDC = Math.Abs(decimal.Parse(oDataRow["MontoAplicado"].ToString()));
                        string facturas = facturasNDC(oDataRow["INVOICE_ID"].ToString(), oEncapsular);
                        //TODO: Cargar la Factura aplicada y validar que exista
                        //Buscar en los CFDI relacionados
                        if (!String.IsNullOrEmpty(facturas))
                        {
                            sSQL = @"
                                SELECT crl.INVOICE_ID, crl.AMOUNT AS MontoAplicado
                                FROM CASH_RECEIPT AS cr 
                                INNER JOIN CUSTOMER AS c ON cr.CUSTOMER_ID = c.ID 
                                INNER JOIN BANK_ACCOUNT AS ba ON cr.BANK_ACCOUNT_ID = ba.ID 
                                INNER JOIN CASH_RECEIPT_LINE AS crl ON cr.CUSTOMER_ID = crl.CUSTOMER_ID AND cr.CHECK_ID = crl.CHECK_ID 
                                INNER JOIN RECEIVABLE AS r ON c.ID = r.CUSTOMER_ID AND crl.INVOICE_ID = r.INVOICE_ID
                                INNER JOIN CASH_RECEIPT_CURR AS crc ON cr.CUSTOMER_ID = crc.CUSTOMER_ID AND cr.CHECK_ID = crc.CHECK_ID AND cr.CURRENCY_ID = crc.CURRENCY_ID
                                WHERE cr.CHECK_ID='" + checkId + "' AND cr.CUSTOMER_ID='" + customerId + "' AND crl.AMOUNT>0 AND crl.INVOICE_ID IN (" + facturas + ")";
                            //BitacoraFX.Log("validarNDCListado: " + sSQL);
                            DataTable oDataTableI = oEncapsular.oData_ERP.EjecutarConsulta(sSQL);
                            foreach (DataRow oDataRowI in oDataTableI.Rows)
                            {
                                noAsociados.Remove(oDataRow["INVOICE_ID"].ToString());
                                ndcAplicadas item = new ndcAplicadas();
                                item.invoice_id = oDataRowI["INVOICE_ID"].ToString();
                                item.ndc = oDataRow["INVOICE_ID"].ToString();
                                item.monto = Math.Round(Math.Abs(decimal.Parse(oDataRow["MontoAplicado"].ToString())), 2);
                                lista.Add(item);
                                //BitacoraFX.Log("Agregar NDC: " + oDataRow["INVOICE_ID"].ToString() + " INVOICE " + item.invoice_id + " Monto " + item.monto.ToString());
                            }
                        }
                        else
                        {
                            //Agregarla
                            noAsociados.Add(oDataRow["INVOICE_ID"].ToString());
                        }

                    }
                }
                //}

                if (noAsociados.Count != 0)
                {
                    var result = string.Join(",", noAsociados);
                    errorNDC = "La(s) NDC " + result + " no estan asociadas a las facturas del pago. ";
                }
                ////Enviar error 
                //if (String.IsNullOrEmpty(errorNDC))
                //{
                //    
                //}
                //errorNDC += oDataRow["INVOICE_ID"].ToString();

            }
            catch (Exception e)
            {
                ErrorFX.mostrar(e, true, true, "cComprobantePago - 210", false);
            }
            return lista;
        }

        private string facturasNDC(string NDC, Encapsular oEncapsular)
        {
            try
            {
                string retornar = "";
                ModeloDocumentosFXContainer dbContext = new ModeloDocumentosFXContainer();
                string sSQL = @"
                SELECT  
                INVOICE_ID
                FROM ndcFacturaLineaSet
                WHERE invoice_id like '" + NDC + @"'
                ";

                //Buscar en ndcFacturaLineaSet
                var resultLinea = from b in dbContext.ndcFacturaLineaSet
                                                                 .Where(b => b.INVOICE_ID == NDC)
                                  select b;
                List<ndcFacturaLinea> dtLinea = resultLinea.ToList();
                foreach (ndcFacturaLinea registro in dtLinea)
                {
                    if (!retornar.Contains(registro.invoice_id_relacionado))
                    {
                        if (!String.IsNullOrEmpty(retornar))
                        {
                            retornar += ",";
                        }
                        retornar += "'" + registro.invoice_id_relacionado + "'";
                    }

                }

                var result = from b in dbContext.ndcFacturaSet
                                                                 .Where(b => b.INVOICE_ID == NDC)
                             select b;
                List<ndcFactura> dt = result.ToList();
                foreach (ndcFactura registro in dt)
                {
                    if (!retornar.Contains(registro.INVOICE_ID_relacionada))
                    {
                        if (!String.IsNullOrEmpty(retornar))
                        {
                            retornar += ",";
                        }
                        retornar += "'" + registro.INVOICE_ID_relacionada + "'";
                    }

                }

                //Cargar CU
                if (oEncapsular.ocEMPRESA.ENTITY_ID.Contains("CHIME"))
                {
                    //Buscar el servidor de ellos
                    sSQL = @"
                    SELECT  
                    invoice_id_relacionado
                    FROM ndcFacturaLineaSet
                    WHERE invoice_id like '" + NDC + @"'
                    ";
                    cCONEXCION oDateTmp = new cCONEXCION(oEncapsular.ocEMPRESA.SERVIDOR, "documentosfx",
                        oEncapsular.ocEMPRESA.USUARIO_BD, oEncapsular.ocEMPRESA.PASSWORD_BD);
                    DataTable oDataTable = oDateTmp.EjecutarConsulta(sSQL);
                    if (oDataTable != null)
                    {
                        foreach (DataRow oDataRow in oDataTable.Rows)
                        {
                            if (!retornar.Contains(oDataRow["invoice_id_relacionado"].ToString()))
                            {
                                if (!String.IsNullOrEmpty(retornar))
                                {
                                    retornar += ",";
                                }
                                retornar += "'" + oDataRow["invoice_id_relacionado"].ToString() + "'";
                            }
                        }
                    }
                }

                //BitacoraFX.Log("cComprobantePago.cs 692 " + retornar);

                return retornar;
            }
            catch (Exception e)
            {
                ErrorFX.mostrar(e, true, true, "cComprobantePago - 214", false);
            }
            return "";
        }

        private string obtenerUUID(string invoice_id, Encapsular oEncapsular
            , bool validarComprobantePPD99 = false)
        {
            try
            {
                cCONEXCION oData_ERPp = new cCONEXCION(oEncapsular.ocEMPRESA.TIPO, oEncapsular.ocEMPRESA.SERVIDOR
                                    , oEncapsular.ocEMPRESA.BD, oEncapsular.ocEMPRESA.USUARIO_BD, oEncapsular.ocEMPRESA.PASSWORD_BD);
                string BD_Auxiliar = "";
                if (oEncapsular.ocEMPRESA.BD_AUXILIAR != "")
                {
                    BD_Auxiliar = oEncapsular.ocEMPRESA.BD_AUXILIAR + ".dbo.";
                }
                string sSQL = @"SELECT VMX_FE.UUID, VMX_FE.FORMA_DE_PAGO, VMX_FE.METODO_DE_PAGO
                FROM " + BD_Auxiliar + @"VMX_FE VMX_FE  
                WHERE VMX_FE.INVOICE_ID collate SQL_Latin1_General_CP1_CI_AS = '" + invoice_id + @"'
                AND(NOT(ESTADO LIKE '%Test%') AND NOT(ESTADO LIKE '%Creada%'))";
                DataTable oDataTable = oData_ERPp.EjecutarConsulta(sSQL);
                oData_ERPp.DestruirConexion();
                if (oDataTable != null)
                {
                    foreach (DataRow oDataRow in oDataTable.Rows)
                    {
                        if (validarComprobantePPD99)
                        {
                            string formaPago = oDataRow["FORMA_DE_PAGO"].ToString();
                            string metodoPago = oDataRow["METODO_DE_PAGO"].ToString();
                            if (formaPago.Equals("99") & metodoPago.Equals("PPD"))
                            {
                                return oDataRow["UUID"].ToString();
                            }
                            return null;
                        }

                        return oDataRow["UUID"].ToString();
                    }
                }
                return null;
            }
            catch (Exception e)
            {
                ErrorFX.mostrar(e, true, true, "ComprobantePago - 110", false);
            }
            return null;
        }
        private string traducirMoneda(string moneda)
        {
            try
            {
                if (moneda.Contains("MX") | moneda.Contains("PESO") | moneda.Contains("MN") | moneda.Contains("MXP"))
                {
                    return "MXN";
                }
                if (moneda.Contains("US") | moneda.Contains("DOLAR") | moneda.Contains("DLLS"))
                {
                    return "USD";
                }
            }
            catch (Exception e)
            {
                ErrorFX.mostrar(e, true, true, "ComprobantePago - 76", false);
            }
            return null;
        }
        internal string informacion = "";
        public bool timbrar(CFDI33.Comprobante oComprobante, string estado, pago oRegistro, cSERIE ocSERIE)
        {
            try
            {
                //Timbrar
                mensaje = "Timbrando el CFDI " + oComprobante.Serie + oComprobante.Folio;
                bool usar_edicom = false;
                bool resultadoTimbrado = false;
                int opcion = 1;
                if (estado.Contains("BORRADOR"))
                {
                    usar_edicom = true;
                    opcion = 2;
                }
                try
                {
                    //Buscar usuarios de EDICOM
                    bool existe = Globales.cargarTimbrado("EDICOM");
                    if (!existe)
                    {
                        //ErrorFX.mostrar("La configuración para EDICOM no existe ", false, false, false);
                        string factura = oComprobante.Serie + oComprobante.Folio;

                        return timbrarSolucionFactible(opcion, oRegistro.xml, factura);
                    }
                    else
                    {
                        string passwordTr = cENCRIPTACION.Decrypt(Globales.oCFDI_USUARIO.PASSWORD);

                        if (usar_edicom)
                        {
                            resultadoTimbrado = generarCFDI(oComprobante, 2, Globales.oCFDI_USUARIO.USUARIO, passwordTr, oRegistro);
                        }
                        else
                        {
                            resultadoTimbrado = generarCFDI(oComprobante, 1, Globales.oCFDI_USUARIO.USUARIO, passwordTr, oRegistro);
                        }
                    }

                }
                catch (FaultException<ExceptionDetail> declaredFaultEx)
                {
                    // An error on the service, transmitted via declared SOAP
                    // fault (specified in the contract for an operation).
                    mensaje = declaredFaultEx.Detail.Message;
                    ErrorFX.mostrar(mensaje, false, true, true);
                    return false;
                }
                catch (FaultException unknownFaultEx)
                {
                    // An error on the service, transmitted via undeclared SOAP
                    // fault (not specified in the contract for an operation).
                    mensaje = unknownFaultEx.Message;
                    ErrorFX.mostrar(unknownFaultEx, false, true, true);
                    return false;
                }
                catch (CommunicationException commEx)
                {
                    // A communication error in either the service or client application.
                    mensaje = commEx.Message;
                    ErrorFX.mostrar(commEx, false, true, true);
                    return false;
                }
                catch (Exception ex)
                {
                    mensaje = "Error timbrando en EDICOM " + Environment.NewLine + ex.InnerException.ToString();
                    ErrorFX.mostrar(ex, false, true, true);
                    return false;
                }
                string estadoTr = estado.ToUpper();
                if (!estadoTr.Contains("BORRADOR"))
                {
                    //guardarCambios(ocSERIE);
                    return true;
                }

                return resultadoTimbrado;
            }
            catch (Exception e)
            {
                ErrorFX.mostrar(e, true, true, "frmComprobantePago - 84 ");
            }
            return true;
        }
        public bool timbrar40(CFDI40.Comprobante oComprobante, string estado, pago oRegistro, cSERIE ocSERIE)
        {
            try
            {
                //Timbrar
                mensaje = "Timbrando el CFDI Versión 4.0 " + oComprobante.Serie + oComprobante.Folio;
                bool usar_edicom = false;
                bool resultadoTimbrado = false;
                int opcion = 1;
                if (estado.Contains("BORRADOR"))
                {
                    usar_edicom = true;
                    opcion = 2;
                }
                try
                {
                    //Buscar usuarios de EDICOM
                    bool existe = Globales.cargarTimbrado("EDICOM");
                    if (!existe)
                    {
                        //ErrorFX.mostrar("La configuración para EDICOM no existe ", false, false, false);
                        string factura = oComprobante.Serie + oComprobante.Folio;

                        return timbrarSolucionFactible(opcion, oRegistro.xml, factura);
                    }
                    else
                    {
                        string passwordTr = cENCRIPTACION.Decrypt(Globales.oCFDI_USUARIO.PASSWORD);

                        if (usar_edicom)
                        {
                            resultadoTimbrado = generarCFDI40(oComprobante, 2, Globales.oCFDI_USUARIO.USUARIO, passwordTr, oRegistro);
                        }
                        else
                        {
                            resultadoTimbrado = generarCFDI40(oComprobante, 1, Globales.oCFDI_USUARIO.USUARIO, passwordTr, oRegistro);
                        }
                    }

                }
                catch (FaultException<ExceptionDetail> declaredFaultEx)
                {
                    // An error on the service, transmitted via declared SOAP
                    // fault (specified in the contract for an operation).
                    mensaje = declaredFaultEx.Detail.Message;
                    ErrorFX.mostrar(mensaje, false, true, true);
                    return false;
                }
                catch (FaultException unknownFaultEx)
                {
                    // An error on the service, transmitted via undeclared SOAP
                    // fault (not specified in the contract for an operation).
                    mensaje = unknownFaultEx.Message;
                    ErrorFX.mostrar(unknownFaultEx, false, true, true);
                    return false;
                }
                catch (CommunicationException commEx)
                {
                    // A communication error in either the service or client application.
                    mensaje = commEx.Message;
                    ErrorFX.mostrar(commEx, false, true, true);
                    return false;
                }
                catch (Exception ex)
                {
                    mensaje = "Error timbrando en EDICOM " + Environment.NewLine + ex.InnerException.ToString();
                    ErrorFX.mostrar(ex, false, true, true);
                    return false;
                }
                //string estadoTr = estado.ToUpper();
                //if (!estadoTr.Contains("BORRADOR"))
                //{
                //    //guardarCambios(ocSERIE);
                //    return true;
                //}

                return resultadoTimbrado;
            }
            catch (Exception e)
            {
                ErrorFX.mostrar(e, true, true, "frmComprobantePago - 84 ");
            }
            return true;
        }

        private void guardarError(string mensajeError, pago oRegistrop, string estado)
        {
            try
            {
                //Guardar el error
                ModeloComprobantePago.documentosfxEntities dbContext = new documentosfxEntities();
                pago oRegistro = dbContext.pagoSet.Where(a => a.Id == oRegistrop.Id).FirstOrDefault();
                if (oRegistro != null)
                {
                    oRegistro.error = mensajeError + " " + estado;
                    dbContext.pagoSet.Attach(oRegistro);
                    dbContext.Entry(oRegistro).State = System.Data.Entity.EntityState.Modified;
                    dbContext.SaveChanges();
                }
                dbContext.Dispose();

            }
            catch (Exception e)
            {
                ErrorFX.mostrar(e, true, true, "frmComprobantePago - 700 ");
            }
        }

        //private void guardarCambios(cSERIE ocSERIE)
        //{
        //    try
        //    {
        //        //Aumentar la serie
        //        //ocSERIE.incrementar(ocSERIE.ROW_ID);
        //    }
        //    catch (Exception e)
        //    {
        //        ErrorFX.mostrar(e, true, true, "frmComprobantePago - 286 ");
        //    }
        //    return;
        //}


        internal void cargarDetalle(DateTime inicio, DateTime fin)
        {
            try
            {
                ModeloComprobantePago.documentosfxEntities dbContext = new documentosfxEntities();
                string conection = dbContext.Database.Connection.ConnectionString;
                cCONEXCION oData = new cCONEXCION(conection);

                string sSQL = @"
                SELECT p.checkId, p.customerId, p.serie, p.folio, p.fecha, p.noCertificado, p.emisor, p.receptor, p.lugarExpedicion, p.emisorNombre, p.emisorRegimenFiscal, p.receptorResidenciaFiscal, p.receptorNombre, 
                p.receptorNumRegIdTrib, p.fechaPago, p.formaDePagoP, p.monedaP, p.monto, p.NumOperacion, p.RfcEmisorCtaOrd, p.NomBancoOrdExt, p.CtaOrdenante, p.RfcEmisorCtaBen, p.CtaBeneficiario, p.estado, p.xml, p.pdf, p.error, 
                p.uuid, p.cancelado, p.pdfEstado, pR.UUID AS Expr1, pR.tipoRelacion, pL.factura
                , pL.UUID AS FacturaUUID, pL.MonedaDR, pL.NumParcialidad, pL.ImpSaldoAnt
                , pL.ImpPagado, pL.ImpSaldoInsoluto, pL.TipoCambioDR, 
                pL.folio AS FacturaFolio, pL.serie AS FacturaSerie
                FROM pagoSet AS p 
                LEFT OUTER JOIN pagoRelacionadoSet AS pR ON p.checkId = pR.checkId AND p.customerId = pR.customerId 
                LEFT OUTER JOIN pagoLineaSet AS pL ON p.Id = pL.pago_Id
                WHERE 1=1
                ";
                string FECHA_INICIO = oData.convertir_fecha(inicio, "i");
                sSQL += " AND p.fechaPago>=" + FECHA_INICIO + "";
                string FECHA_FINAL = oData.convertir_fecha(fin, "f");
                sSQL += " AND p.fechaPago<=" + FECHA_FINAL + "";
                DataTable oDataTable = oData.EjecutarConsulta(sSQL);

                ExcelFX.exportarDataTable(oDataTable, "Relacion de pagos y facturas Detallado");
            }
            catch (Exception e)
            {
                ErrorFX.mostrar(e, true, true, false);
            }
        }

        public bool generarCFDI(CFDI33.Comprobante oComprobante, int Opcion, string EDICOM_USUARIO, string EDICOM_PASSWORD, pago oRegistro)
        {
            try
            {
                //Comprimir Archivo
                string Archivo_Tmp = oComprobante.Serie + oComprobante.Folio + ".xml";


                File.Copy(oRegistro.xml, Archivo_Tmp, true);
                string ARCHIVO_zip = Archivo_Tmp.ToUpper().Replace("XML", "ZIP");
                using (ZipFile zip = new ZipFile())
                {
                    zip.AddFile(Archivo_Tmp);
                    zip.Save(ARCHIVO_zip);
                }
                //Fin de Compresion de Archivo
                File.Delete(Archivo_Tmp);

                byte[] ARCHIVO_Leido = ReadBinaryFile(ARCHIVO_zip);
                File.Delete(ARCHIVO_zip);
                CFDiClient oCFDiClient = new CFDiClient();

                byte[] PROCESO;

                switch (Opcion)
                {
                    case 1:
                        try
                        {
                            informacion = "Generando CFDI ";
                            PROCESO = oCFDiClient.getCfdi(EDICOM_USUARIO, EDICOM_PASSWORD, ARCHIVO_Leido);
                            guardarArchivo(PROCESO, oComprobante.Serie + oComprobante.Folio, oRegistro);
                        }
                        catch (FaultException oException)
                        {
                            ErrorFX.mostrar(oException, false, true, true);
                            CFDiException oCFDiException = new CFDiException();
                            informacion = oException.Message;
                            guardarError(oRegistro, informacion);
                            return false;
                        }

                        break;
                    case 2:
                        try
                        {
                            informacion = "Generando CFDI BORRADOR ";
                            PROCESO = oCFDiClient.getCfdiTest(EDICOM_USUARIO, EDICOM_PASSWORD, ARCHIVO_Leido);
                            guardarArchivo(PROCESO, oComprobante.Serie + oComprobante.Folio, oRegistro);
                        }
                        catch (FaultException oException)
                        {
                            ErrorFX.mostrar(oException, false, true, true);
                            CFDiException oCFDiException = new CFDiException();
                            informacion = oException.Message;
                            guardarError(oRegistro, informacion);
                            return false;
                        }
                        break;
                    case 3:
                        try
                        {
                            informacion = "Generando Timbre CFDI  ";
                            PROCESO = oCFDiClient.getTimbreCfdi(EDICOM_USUARIO, EDICOM_PASSWORD, ARCHIVO_Leido);
                        }
                        catch (Exception oException)
                        {
                            ErrorFX.mostrar(oException, false, true, true);
                            CFDiException oCFDiException = new CFDiException();
                            informacion = oException.Message;
                            guardarError(oRegistro, informacion);
                            return false;
                        }
                        break;
                    case 4:
                        try
                        {
                            informacion = "Generando Timbre CFDI Test ";
                            PROCESO = oCFDiClient.getTimbreCfdiTest(EDICOM_USUARIO, EDICOM_PASSWORD, ARCHIVO_Leido);
                        }
                        catch (Exception oException)
                        {
                            ErrorFX.mostrar(oException, false, true, true);
                            CFDiException oCFDiException = new CFDiException();
                            informacion = oException.Message;
                            guardarError(oRegistro, informacion);
                            return false;
                        }
                        break;
                }


                informacion = "Generado y Sellado en Modo BORRADOR " + DateTime.Now.Day.ToString() + "/" + DateTime.Now.Month.ToString() + "/" + DateTime.Now.Year.ToString() + " " + DateTime.Now.ToString("HH:mm:ss");
                if (Opcion == 1)
                {
                    informacion = informacion.Replace("BORRADOR", " REAL ");

                }

                actualizarPago(oRegistro);


                return true;
            }
            catch (Exception oException)
            {
                informacion = "Generando CFDI " + oException.Message;
                ErrorFX.mostrar(oException, false, true, true);
                return false;
            }
        }


        public bool generarCFDI40(CFDI40.Comprobante oComprobante, int Opcion, string EDICOM_USUARIO, string EDICOM_PASSWORD, pago oRegistro)
        {
            try
            {
                //Comprimir Archivo
                string Archivo_Tmp = oComprobante.Serie + oComprobante.Folio + ".xml";

                File.Copy(oRegistro.xml, Archivo_Tmp, true);
                string ARCHIVO_zip = Archivo_Tmp.ToUpper().Replace("XML", "ZIP");
                using (ZipFile zip = new ZipFile())
                {
                    zip.AddFile(Archivo_Tmp);
                    zip.Save(ARCHIVO_zip);
                }
                //Fin de Compresion de Archivo
                File.Delete(Archivo_Tmp);

                byte[] ARCHIVO_Leido = ReadBinaryFile(ARCHIVO_zip);
                File.Delete(ARCHIVO_zip);
                CFDiClient oCFDiClient = new CFDiClient();

                byte[] PROCESO;

                switch (Opcion)
                {
                    case 1:
                        try
                        {
                            informacion = "Generando CFDI ";
                            PROCESO = oCFDiClient.getCfdi(EDICOM_USUARIO, EDICOM_PASSWORD, ARCHIVO_Leido);
                            guardarArchivo(PROCESO, oComprobante.Serie + oComprobante.Folio, oRegistro);
                        }
                        catch (FaultException oException)
                        {
                            ErrorFX.mostrar(oException, false, true, true);
                            CFDiException oCFDiException = new CFDiException();
                            informacion = oException.Message;
                            guardarError(oRegistro, informacion);
                            return false;
                        }

                        break;
                    case 2:
                        try
                        {
                            informacion = "Generando CFDI BORRADOR ";
                            PROCESO = oCFDiClient.getCfdiTest(EDICOM_USUARIO, EDICOM_PASSWORD, ARCHIVO_Leido);
                            guardarArchivo(PROCESO, oComprobante.Serie + oComprobante.Folio, oRegistro);
                        }
                        catch (FaultException oException)
                        {
                            ErrorFX.mostrar(oException, false, true, true);
                            CFDiException oCFDiException = new CFDiException();
                            informacion = oException.Message;
                            guardarError(oRegistro, informacion);
                            return false;
                        }
                        break;
                    case 3:
                        try
                        {
                            informacion = "Generando Timbre CFDI  ";
                            PROCESO = oCFDiClient.getTimbreCfdi(EDICOM_USUARIO, EDICOM_PASSWORD, ARCHIVO_Leido);
                        }
                        catch (Exception oException)
                        {
                            ErrorFX.mostrar(oException, false, true, true);
                            CFDiException oCFDiException = new CFDiException();
                            informacion = oException.Message;
                            guardarError(oRegistro, informacion);
                            return false;
                        }
                        break;
                    case 4:
                        try
                        {
                            informacion = "Generando Timbre CFDI Test ";
                            PROCESO = oCFDiClient.getTimbreCfdiTest(EDICOM_USUARIO, EDICOM_PASSWORD, ARCHIVO_Leido);
                        }
                        catch (Exception oException)
                        {
                            ErrorFX.mostrar(oException, false, true, true);
                            CFDiException oCFDiException = new CFDiException();
                            informacion = oException.Message;
                            guardarError(oRegistro, informacion);
                            return false;
                        }
                        break;
                }


                informacion = "Generado y Sellado en Modo BORRADOR " + DateTime.Now.Day.ToString() + "/" + DateTime.Now.Month.ToString() + "/" + DateTime.Now.Year.ToString() + " " + DateTime.Now.ToString("HH:mm:ss");
                if (Opcion == 1)
                {
                    informacion = informacion.Replace("BORRADOR", " REAL ");

                }

                actualizarPago(oRegistro);


                return true;
            }
            catch (Exception oException)
            {
                informacion = "Generando CFDI " + oException.Message;
                ErrorFX.mostrar(oException, false, true, true);
                return false;
            }
        }



        private void actualizarPago(pago oRegistrop)
        {
            try
            {
                ModeloComprobantePago.documentosfxEntities dbContext = new documentosfxEntities();
                pago oRegistro = dbContext.pagoSet.Where(a => a.Id == oRegistrop.Id).FirstOrDefault();
                oRegistro.estado = informacion;

                XmlDocument docXML = new XmlDocument();
                docXML.Load(oRegistro.xml);
                XmlNodeList TimbreFiscalDigital = docXML.GetElementsByTagName("tfd:TimbreFiscalDigital");
                string uuidTr = "";
                if (TimbreFiscalDigital != null)
                {
                    if (TimbreFiscalDigital[0] != null)
                    {
                        uuidTr = TimbreFiscalDigital[0].Attributes["UUID"].Value;
                    }
                }

                oRegistro.uuid = uuidTr;

                //<pago10:Pago FechaPago="2018-08-17T12:00:00" FormaDePagoP="02"
                string formaDePagoPTr = "";
                XmlNodeList Pago = docXML.GetElementsByTagName("pago10:Pago");
                if (Pago != null)
                {
                    if (Pago[0] != null)
                    {
                        formaDePagoPTr = Pago[0].Attributes["FormaDePagoP"].Value;
                    }
                }


                oRegistro.formaDePagoP = formaDePagoPTr;

                dbContext.pagoSet.Attach(oRegistro);
                dbContext.Entry(oRegistro).State = System.Data.Entity.EntityState.Modified;
                dbContext.SaveChanges();
                dbContext.Dispose();
            }
            catch (Exception e)
            {
                ErrorFX.mostrar(e, true, true, "frmComprobantePago - 326 ");
            }
        }

        public void guardarError(pago oRegistrop, string error)
        {
            try
            {
                //Valor en Nulo
                if (oRegistrop.Id == 0)
                {
                    return;
                }
                ModeloComprobantePago.documentosfxEntities dbContext = new documentosfxEntities();
                pago oRegistro = dbContext.pagoSet.Where(a => a.Id == oRegistrop.Id).FirstOrDefault();
                if (oRegistro != null)
                {
                    oRegistro.estado = "Error de Timbrado " + error;
                    oRegistro.error = error;
                    if (File.Exists(oRegistro.xml))
                    {
                        string archivoDestino = oRegistro.xml + "_error";
                        if (File.Exists(archivoDestino))
                        {
                            System.IO.File.Delete(archivoDestino);
                        }

                        System.IO.File.Move(oRegistro.xml, oRegistro.xml + "_error");

                    }
                    oRegistro.xml = oRegistro.xml + "_error";
                    dbContext.pagoSet.Attach(oRegistro);
                    dbContext.Entry(oRegistro).State = System.Data.Entity.EntityState.Modified;
                    dbContext.SaveChanges();
                }
                dbContext.Dispose();
            }
            catch (Exception e)
            {
                ErrorFX.mostrar(e, true, true, "frmComprobantePago - 314 ");
            }
        }

        public byte[] ReadBinaryFile(string fileName)
        {
            if (File.Exists(fileName))
            {
                try
                {
                    ///Open and read a file&#12290;
                    FileStream fileStream = File.OpenRead(fileName);
                    byte[] archivo = ConvertStreamToByteBuffer(fileStream);
                    fileStream.Close();
                    return archivo;
                }
                catch (Exception ex)
                {
                    return new byte[0];
                }
            }
            else
            {
                return new byte[0];
            }
        }
        public byte[] ConvertStreamToByteBuffer(System.IO.Stream theStream)
        {
            int b1;
            System.IO.MemoryStream tempStream = new System.IO.MemoryStream();
            while ((b1 = theStream.ReadByte()) != -1)
            {
                tempStream.WriteByte(((byte)b1));
            }
            return tempStream.ToArray();
        }
        private void guardarArchivo(byte[] PROCESO, string INVOICE_ID, pago oRegistro)
        {
            //Guardar los PROCESO
            File.WriteAllBytes("TEMPORAL.ZIP", PROCESO);
            //Descomprimir
            String TargetDirectory = "TEMPORAL";
            using (Ionic.Zip.ZipFile zip = Ionic.Zip.ZipFile.Read("TEMPORAL.ZIP"))
            {
                zip.ExtractAll(TargetDirectory, Ionic.Zip.ExtractExistingFileAction.OverwriteSilently);
            }

            //Copiar el ARCHIVO Descrompimido por otro
            string ARCHIVO_descomprimido = TargetDirectory + @"\" + "SIGN_" + INVOICE_ID + ".XML";

            File.Copy(ARCHIVO_descomprimido, oRegistro.xml, true);

            File.Delete(ARCHIVO_descomprimido);
            File.Delete("TEMPORAL.ZIP");
        }

        internal decimal cargarComplementoPago(string xml)
        {
            CFDI33.Comprobante oComprobanteTr;
            //Pasear el comprobante

            try
            {
                XmlSerializer serializer_CFD_3 = new XmlSerializer(typeof(CFDI33.Comprobante));
                TextReader reader = new StreamReader(xml);
                oComprobanteTr = (CFDI33.Comprobante)serializer_CFD_3.Deserialize(reader);
                reader.Dispose();
                reader.Close();
                //Buscar el Comprobante de Pago
                foreach (CFDI33.ComprobanteComplemento complemento in oComprobanteTr.Complemento)
                {
                    foreach (XmlElement elemento in complemento.Any)
                    {
                        //Si es nomina buscar los elementos con Linq
                        if (elemento.OuterXml.Contains("pago10:Pagos"))
                        {
                            using (StringReader readerPagos = new StringReader(elemento.OuterXml))
                            {
                                XmlSerializer valueSerializer = new XmlSerializer(typeof(Pagos));
                                Pagos oPagos = (Pagos)valueSerializer.Deserialize(readerPagos);
                                foreach (Pago10.PagosPago pago in oPagos.Pago)
                                {
                                    //Rodrigo 16/06/2022  Cambio a Decimal de 2
                                    return decimal.Parse(pago.Monto);
                                    //return pago.Monto;
                                }
                            }
                        }
                    }
                }
                return 0;
            }
            catch (Exception error)
            {
                ErrorFX.mostrar(error, true, true, true);
            }
            return 0;
        }

        internal void autorelacionar(pago oRegistrop)
        {
            try
            {
                ModeloComprobantePago.documentosfxEntities dbContext = new documentosfxEntities();
                pago oRegistro = dbContext.pagoSet.Where(a => a.Id == oRegistrop.Id
                    & !a.estado.Contains("BORRADOR"))
                    .OrderByDescending(b => b.Id)
                    .FirstOrDefault();
                if (oRegistro != null)
                {
                    if (!String.IsNullOrEmpty(oRegistro.uuid))
                    {
                        cComprobantePago ocComprobantePago = new cComprobantePago();
                        ocComprobantePago.relacionar(oRegistro.uuid, "04", oRegistro.checkId, oRegistro.customerId);
                    }

                }
                dbContext.Dispose();
            }
            catch (Exception e)
            {
                ErrorFX.mostrar(e, true, true, "frmComprobantePago - 1292");
            }
        }

        //private dsComprobantePago.dtCPdetalleDataTable informacionPago(string filename)
        //{
        //    try
        //    {
        //        CFDI33.Comprobante oComprobante;
        //        //Pasear el comprobante
        //        dsComprobantePago.dtCPdetalleDataTable oRegistro = new dsComprobantePago.dtCPdetalleDataTable();

        //        try
        //        {
        //            XmlSerializer serializer_CFD_3 = new XmlSerializer(typeof(CFDI33.Comprobante));
        //            TextReader reader = new StreamReader(filename);
        //            oComprobante = (CFDI33.Comprobante)serializer_CFD_3.Deserialize(reader);
        //            reader.Dispose();
        //            reader.Close();

        //            if (!oComprobante.TipoDeComprobante.Equals("P"))
        //            {
        //                return null;
        //            }

        //            dsComprobantePago.dtCPdetalleRow row = new dsComprobantePago.dtCPdetalleRow();
        //            oRegistro.AdddtCPdetalleRow();


        //            //oComprobante.Emisor.Rfc
        //            //                        , oComprobante.Emisor.Nombre, oComprobante.Receptor.Rfc
        //            //                        , oComprobante.Receptor.Nombre, oComprobante.Serie, oComprobante.Folio


        //            //Determinar si es Timbrado
        //            foreach (ComprobanteComplemento complemento in oComprobante.Complemento)
        //            {
        //                foreach (XmlElement elemento in complemento.Any)
        //                {
        //                    //Timbrado
        //                    if (elemento.OuterXml.Contains("tfd:TimbreFiscalDigital"))
        //                    {
        //                        var doc = XDocument.Parse(elemento.OuterXml);
        //                        string uuidtr = doc.Root.Attributes("UUID").First().Value.ToString();
        //                        dtg.Rows[n].Cells["uuid"].Value = uuidtr;
        //                        string FechaTimbradotr = doc.Root.Attributes("FechaTimbrado").First().Value.ToString();
        //                        dtg.Rows[n].Cells["timbrado"].Value = FechaTimbradotr;
        //                        ModeloComprobantePago.documentosfxEntities dbContext = new documentosfxEntities();
        //                        //Cargar el Check y el Customer
        //                        pago oRegistroTr = dbContext.pagoSet.Where(a => a.uuid.Equals(uuidtr)).FirstOrDefault();
        //                        if (oRegistroTr != null)
        //                        {
        //                            dtg.Rows[n].Cells["CHECK_ID"].Value = oRegistroTr.checkId;
        //                            dtg.Rows[n].Cells["CUSTOMER_ID"].Value = oRegistroTr.customerId;
        //                        }
        //                    }
        //                }
        //            }

        //            //Buscar el Comprobante de Pago
        //            foreach (ComprobanteComplemento complemento in oComprobante.Complemento)
        //            {
        //                foreach (XmlElement elemento in complemento.Any)
        //                {
        //                    //Si es nomina buscar los elementos con Linq
        //                    if (elemento.OuterXml.Contains("pago10:Pagos"))
        //                    {
        //                        using (StringReader readerPagos = new StringReader(elemento.OuterXml))
        //                        {
        //                            XmlSerializer valueSerializer = new XmlSerializer(typeof(Pagos));
        //                            Pagos oPagos = (Pagos)valueSerializer.Deserialize(readerPagos);
        //                            foreach (Pago10.PagosPago pago in oPagos.Pago)
        //                            {
        //                                dtg.Rows[n].Cells["Monto"].Value = pago.Monto.ToString("C");
        //                                dtg.Rows[n].Cells["Moneda"].Value = pago.MonedaP;
        //                                bool duplicar = false;
        //                                foreach (Pago10.PagosPagoDoctoRelacionado cfdi in pago.DoctoRelacionado)
        //                                {
        //                                    if (duplicar)
        //                                    {
        //                                        int nAnterior = n;
        //                                        n = dtg.Rows.Add();
        //                                        dtg.Rows[n].Tag = dtg.Rows[nAnterior].Tag;

        //                                        foreach (DataGridViewColumn column in dtg.Columns)
        //                                        {
        //                                            if (dtg.Rows[nAnterior].Cells[column.Index].Value != null)
        //                                            {
        //                                                dtg.Rows[n].Cells[column.Index].Value = dtg.Rows[nAnterior].Cells[column.Index].Value;
        //                                            }
        //                                        }

        //                                    }
        //                                    dtg.Rows[n].Cells["cfdiSerie"].Value = cfdi.Serie;
        //                                    dtg.Rows[n].Cells["cfdiFolio"].Value = cfdi.Folio;
        //                                    dtg.Rows[n].Cells["NumParcialidad"].Value = cfdi.NumParcialidad;
        //                                    dtg.Rows[n].Cells["ImpPagado"].Value = cfdi.ImpPagado.ToString("C");
        //                                    dtg.Rows[n].Cells["cfdiUUID"].Value = cfdi.IdDocumento;
        //                                    duplicar = true;
        //                                }
        //                            }
        //                        }
        //                    }
        //                }
        //            }


        //        }
        //        catch (Exception error)
        //        {
        //            ErrorFX.mostrar(e, true, true, "cComprobantePago - 1409");
        //        }

        //    }
        //    catch (Exception e)
        //    {
        //        ErrorFX.mostrar(e, true, true, "cComprobantePago - 1415");
        //    }
        //    return null;
        //}

        internal bool validarRelacion(string checkId, string customerId)
        {
            try
            {
                ModeloComprobantePago.documentosfxEntities dbContext = new documentosfxEntities();
                var pagos = dbContext.pagoRelacionadoSet.Where(b => b.checkId == checkId
                                    & b.customerId == customerId);

                if (pagos != null)
                {
                    if (pagos.LongCount() != 0)
                    {
                        dbContext.Dispose();
                        return true;
                    }
                }
                dbContext.Dispose();
                return false;
            }
            catch (Exception e)
            {
                ErrorFX.mostrar(e, true, true, "cComprobantePago - 980");
            }
            return false;
        }

        public void imprimir(pago oPago, Encapsular oEncapsular, bool verPdf = true)
        {
            string complementoPago = "complementoPago.rpt";
            try
            {
                if (oPago == null)
                {
                    return;
                }
                ModeloComprobantePago.documentosfxEntities dbContext = new documentosfxEntities();
                int IdTtr = oPago.Id;
                pago oRegistro = dbContext.pagoSet.Where(a => a.Id == IdTtr).FirstOrDefault();
                string xml = oRegistro.xml;
                if (!File.Exists(xml))
                {
                    ErrorFX.mostrar("cComprobantePago - imprimir - 1736 - El archivo : " + xml + " no existe", false, false, false);
                    return;
                }

                //Cargar XMl a Bandeja
                if (oPago.version.Equals("3.3"))
                {
                    cargarXML(oPago);
                }
                else
                {
                    CargarXML40(oPago);
                }




                string UUID = oRegistro.uuid;
                string observaciones = "";
                if (!String.IsNullOrEmpty(oRegistro.observaciones))
                {
                    observaciones = oRegistro.observaciones;
                }
                dbContext.Dispose();

                ReportDocument oReport = new ReportDocument();

                oReport.Load(complementoPago);

                DataSet ds = new DataSet();
                ds.ReadXml(xml);

                if (String.IsNullOrEmpty(UUID))
                {
                    UUID = "Temporal";
                }


                //Generación de Imagen
                string urlTr = "https://verificacfdi.facturaelectronica.sat.gob.mx/default.aspx?id=" + oRegistro.uuid
                + "&re=" + oRegistro.emisor + "&rr=" + oRegistro.receptor
                + "&tt=0" + "&fe=" + oRegistro.sello.Substring(oRegistro.sello.Length - 8, 8);
                byte[] imagen = generarQR(urlTr);
                //Fin
                dsComprobantePago.dtComprobantePagoDataTable oDtComprobantePago = new dsComprobantePago.dtComprobantePagoDataTable();
                oDtComprobantePago.AdddtComprobantePagoRow(imagen);

                if (oEncapsular != null)
                {
                    try
                    {
                        string Sql = @"
                                SELECT crl.INVOICE_ID, crl.AMOUNT
                                FROM CASH_RECEIPT AS cr 
                                INNER JOIN CUSTOMER AS c ON cr.CUSTOMER_ID = c.ID 
                                INNER JOIN BANK_ACCOUNT AS ba ON cr.BANK_ACCOUNT_ID = ba.ID 
                                INNER JOIN CASH_RECEIPT_LINE AS crl ON cr.CUSTOMER_ID = crl.CUSTOMER_ID AND cr.CHECK_ID = crl.CHECK_ID 
                                INNER JOIN RECEIVABLE AS r ON c.ID = r.CUSTOMER_ID AND crl.INVOICE_ID = r.INVOICE_ID
                                INNER JOIN CASH_RECEIPT_CURR AS crc ON cr.CUSTOMER_ID = crc.CUSTOMER_ID AND cr.CHECK_ID = crc.CHECK_ID AND cr.CURRENCY_ID = crc.CURRENCY_ID
                                WHERE cr.CHECK_ID='" + oPago.checkId + "' AND cr.CUSTOMER_ID='" + oPago.customerId + "' AND crl.AMOUNT<0 " +
                        " ORDER BY crl.AMOUNT DESC ";
                        DataTable oDataTable = oEncapsular.oData_ERP.EjecutarConsulta(Sql);
                        dsComprobantePago.dtCPpagoDataTable TablaNDC = new dsComprobantePago.dtCPpagoDataTable();
                        if (oDataTable != null)
                        {
                            foreach (DataRow oDataRow in oDataTable.Rows)
                            {
                                cFACTURA_BANDEJA ocFACTURA_BANDEJA = new cFACTURA_BANDEJA();
                                ocFACTURA_BANDEJA.cargar_ID(oDataRow["INVOICE_ID"].ToString(), oEncapsular.ocEMPRESA);
                                //TablaNDC.AdddtCPpagoRow(ocFACTURA_BANDEJA.SERIE, oDataRow["INVOICE_ID"].ToString()
                                //    ,"1", oDataRow["AMOUNT"].ToString(), ocFACTURA_BANDEJA.UUID);
                            }
                        }
                    }
                    catch (Exception Error)
                    {
                        throw Error;
                    }
                }


                foreach (CrystalDecisions.CrystalReports.Engine.Table tabla in oReport.Database.Tables)
                {
                    if (tabla.Name.ToString() == "TimbreFiscalDigital")
                    {
                        oReport.Database.Tables["TimbreFiscalDigital"].SetDataSource(ds.Tables["TimbreFiscalDigital"]);
                    }
                    if (tabla.Name.ToString() == "CfdiRelacionados")
                    {
                        if (ds.Tables["CfdiRelacionados"] != null)
                        {
                            oReport.Database.Tables["CfdiRelacionados"].SetDataSource(ds.Tables["CfdiRelacionados"]);
                        }
                        else
                        {
                            DataTable oDataTableTmP = new DataTable();
                            oReport.Database.Tables["CfdiRelacionados"].SetDataSource(oDataTableTmP);
                        }
                    }


                    if (tabla.Name.ToString() == "CfdiRelacionado")
                    {
                        if (ds.Tables["CfdiRelacionado"] != null)
                        {
                            oReport.Database.Tables["CfdiRelacionado"].SetDataSource(ds.Tables["CfdiRelacionado"]);
                        }
                        else
                        {
                            DataTable oDataTableTmP = new DataTable();
                            oReport.Database.Tables["CfdiRelacionado"].SetDataSource(oDataTableTmP);
                        }
                    }

                    if (tabla.Name.ToString() == "dtComprobantePago")
                    {
                        oReport.Database.Tables["dtComprobantePago"].SetDataSource((System.Data.DataTable)oDtComprobantePago);
                    }

                    if (tabla.Name.ToString() == "Comprobante")
                    {
                        oReport.Database.Tables["Comprobante"].SetDataSource(ds.Tables["Comprobante"]);
                    }
                    if (tabla.Name.ToString() == "Conceptos")
                    {
                        oReport.Database.Tables["Conceptos"].SetDataSource(ds.Tables["Conceptos"]);
                    }
                    if (tabla.Name.ToString() == "Concepto")
                    {
                        oReport.Database.Tables["Concepto"].SetDataSource(ds.Tables["Concepto"]);
                    }
                    if (tabla.Name.ToString() == "Emisor")
                    {
                        oReport.Database.Tables["Emisor"].SetDataSource(ds.Tables["Emisor"]);
                    }
                    if (tabla.Name.ToString() == "InformacionAduanera")
                    {
                        oReport.Database.Tables["InformacionAduanera"].SetDataSource(ds.Tables["InformacionAduanera"]);
                    }
                    if (tabla.Name.ToString() == "Receptor")
                    {
                        oReport.Database.Tables["Receptor"].SetDataSource(ds.Tables["Receptor"]);
                    }
                    if (tabla.Name.ToString() == "Complemento")
                    {
                        oReport.Database.Tables["Complemento"].SetDataSource(ds.Tables["Complemento"]);
                    }
                    if (tabla.Name.ToString() == "DoctoRelacionado")
                    {
                        DataTable DoctoRelacionadoDS = ds.Tables["DoctoRelacionado"];                        
                        oReport.Database.Tables["DoctoRelacionado"].SetDataSource(DoctoRelacionadoDS);
                    }
                    if (tabla.Name.ToString() == "Pago")
                    {
                        oReport.Database.Tables["Pago"].SetDataSource(ds.Tables["Pago"]);
                    }
                    if (tabla.Name.ToString() == "Pagos")
                    {
                        oReport.Database.Tables["Pagos"].SetDataSource(ds.Tables["Pagos"]);
                    }
                }





                foreach (ParameterFieldDefinition definition in oReport.DataDefinition.ParameterFields)
                {
                    ParameterValues myvals = new ParameterValues();
                    ParameterDiscreteValue myDiscrete = new ParameterDiscreteValue();
                    switch (definition.ParameterFieldName)
                    {
                        case "observaciones":
                            //if (!String.IsNullOrEmpty(observaciones))
                            //{
                            myDiscrete.Value = observaciones;
                            myvals.Add(myDiscrete);
                            definition.CurrentValues.Add(myDiscrete);
                            definition.ApplyCurrentValues(myvals);
                            //}

                            break;
                        case "cadenaSAT":
                            //if (!String.IsNullOrEmpty(xml))
                            //{
                            myDiscrete.Value = timbrado33.generarCadenaTFD(xml);
                            myvals.Add(myDiscrete);
                            definition.CurrentValues.Add(myDiscrete);
                            definition.ApplyCurrentValues(myvals);
                            //}
                            break;
                        case "estado":
                            //if (!String.IsNullOrEmpty(oRegistro.estado))
                            //{
                            myDiscrete.Value = oRegistro.estado;
                            myvals.Add(myDiscrete);
                            definition.CurrentValues.Add(myDiscrete);
                            definition.ApplyCurrentValues(myvals);
                            //}
                            break;
                    }
                }

                exportarPDF(oReport, oPago, verPdf);
                oReport.Close();
                oReport.Dispose();
                dbContext.Dispose();
            }
            catch (Exception e)
            {
                ErrorFX.mostrar(e, true, true, "cComprobantePago - 101 - imprimir RPT: " + complementoPago);
            }
        }

        private void cargarXML(pago oPago)
        {
            try
            {
                if (oPago == null)
                {
                    return;
                }
                XmlSerializer serializer_CFD_3 = new XmlSerializer(typeof(CFDI33.Comprobante));
                TextReader reader = new StreamReader(oPago.xml);
                CFDI33.Comprobante oComprobante = (CFDI33.Comprobante)serializer_CFD_3.Deserialize(reader);
                reader.Dispose();
                reader.Close();

                ModeloComprobantePago.documentosfxEntities dbContext = new documentosfxEntities();
                int IdTtr = oPago.Id;
                pago oRegistro = dbContext.pagoSet.Where(a => a.Id == IdTtr).FirstOrDefault();
                oRegistro.sello = oComprobante.Sello;


                dbContext.pagoSet.Attach(oRegistro);
                dbContext.Entry(oRegistro).State = System.Data.Entity.EntityState.Modified;
                dbContext.SaveChanges();
                dbContext.Dispose();


            }
            catch (Exception error)
            {
                ErrorFX.mostrar(error, true, true, true);
            }
            return;
        }

        private void CargarXML40(pago oPago)
        {
            try
            {
                if (oPago == null)
                {
                    return;
                }
                XmlSerializer serializer_CFD = new XmlSerializer(typeof(CFDI40.Comprobante));
                TextReader reader = new StreamReader(oPago.xml);
                CFDI40.Comprobante oComprobante = (CFDI40.Comprobante)serializer_CFD.Deserialize(reader);
                reader.Dispose();
                reader.Close();

                ModeloComprobantePago.documentosfxEntities dbContext = new documentosfxEntities();
                int IdTtr = oPago.Id;
                pago oRegistro = dbContext.pagoSet.Where(a => a.Id == IdTtr).FirstOrDefault();
                oRegistro.sello = oComprobante.Sello;


                dbContext.pagoSet.Attach(oRegistro);
                dbContext.Entry(oRegistro).State = System.Data.Entity.EntityState.Modified;
                dbContext.SaveChanges();
                dbContext.Dispose();


            }
            catch (Exception error)
            {
                ErrorFX.mostrar(error, true, true, "cComprobantePago-CargarXML40-", true);
            }
            return;
        }


        private byte[] generarQR(string urlTr)
        {
            try
            {
                int size = 320;
                QRCodeWriter writer = new QRCodeWriter();
                com.google.zxing.common.ByteMatrix matrix;
                matrix = writer.encode(urlTr, com.google.zxing.BarcodeFormat.QR_CODE, size, size, null);
                Bitmap img = new Bitmap(size, size);
                Color Color = Color.FromArgb(0, 0, 0);
                for (int y = 0; y < matrix.Height; ++y)
                {
                    for (int x = 0; x < matrix.Width; ++x)
                    {
                        Color pixelColor = img.GetPixel(x, y);

                        //Find the colour of the dot
                        if (matrix.get_Renamed(x, y) == -1)
                        {
                            img.SetPixel(x, y, Color.White);
                        }
                        else
                        {
                            img.SetPixel(x, y, Color.Black);
                        }
                    }
                }
                string nombre = Path.GetTempPath() + "QRP_TEMP.BMP";
                if (File.Exists(nombre))
                {
                    File.Delete(nombre);
                }
                img.Save(nombre);
                FileStream fs = new FileStream(nombre, System.IO.FileMode.Open, System.IO.FileAccess.Read);
                byte[] Image = new byte[fs.Length];
                fs.Read(Image, 0, Convert.ToInt32(fs.Length));
                int image_32 = Convert.ToInt32(fs.Length);
                fs.Close();
                File.Delete(nombre);
                return Image;
            }
            catch (Exception error)
            {
                ErrorFX.mostrar(error, true, true, true);
            }
            return null;
        }
        public static decimal RedondeoSAT(decimal importe, decimal tipoCambio)
        {
            try
            {
                int NumDecimalesImportePagado = BitConverter.GetBytes(decimal.GetBits(importe)[3])[2];
                int NumDecimalesTipoCambioDR = BitConverter.GetBytes(decimal.GetBits(tipoCambio)[3])[2];
                //Calcular el límite inferior como:
                //(ImportePagado - (10 - NumDecimalesImportePagado) / 2) / (TipoCambioDR + (10 - NumDecimalesTipoCambioDR) / 2 - 0.0000000001).
                //Calcular el límite superior como:
                //(ImportePagado + (10 - NumDecimalesImportePagado) / 2 - 0.0000000001) / (TipoCambioDR - (10 - NumDecimalesTipoCambioDR) / 2).
                double resultadoDouble = Math.Pow(10, NumDecimalesImportePagado);
                decimal Potencia10importe = (decimal)resultadoDouble;
                resultadoDouble = Math.Pow(10, NumDecimalesTipoCambioDR);
                decimal Potencia10tipoCambio = (decimal)resultadoDouble;

                decimal LimiteInferior = (importe - (Potencia10importe / 2)) / (tipoCambio + (Potencia10tipoCambio / 2) - (decimal)(0.0000000001));
                decimal LimiteSuperior = (importe + (Potencia10importe / 2) - (decimal)(0.0000000001)) / (tipoCambio - (Potencia10tipoCambio / 2));
                List<decimal> arr = new List<decimal>();
                arr.Add(LimiteInferior);
                arr.Add(LimiteSuperior);
                decimal avg = Queryable.Average(arr.AsQueryable());
                return avg;

            }
            catch (Exception error)
            {
                ErrorFX.mostrar(error, true, true, "RedondeoSAT - 3829 - ", true);
            }
            return importe;



        }
        private void exportarPDF(ReportDocument oReport, pago oPago, bool verPdf = true)
        {
            //Guardar el PDF
            ModeloComprobantePago.documentosfxEntities dbContext = new documentosfxEntities();
            pago oRegistro = dbContext.pagoSet.Where(a => a.Id == oPago.Id).FirstOrDefault();

            try
            {
                ExportOptions CrExportOptions = new ExportOptions();
                DiskFileDestinationOptions CrDiskFileDestinationOptions = new DiskFileDestinationOptions();
                PdfRtfWordFormatOptions CrFormatTypeOptions = new PdfRtfWordFormatOptions();
                string archivo = oPago.xml.Replace("xml", "pdf");
                if (File.Exists(archivo))
                {
                    File.Delete(archivo);
                }
                CrDiskFileDestinationOptions.DiskFileName = archivo;
                CrExportOptions = oReport.ExportOptions;
                CrExportOptions.ExportDestinationType = ExportDestinationType.DiskFile;
                CrExportOptions.ExportFormatType = ExportFormatType.PortableDocFormat;
                CrExportOptions.DestinationOptions = CrDiskFileDestinationOptions;
                CrExportOptions.FormatOptions = CrFormatTypeOptions;
                oReport.Export();
                if (oRegistro != null)
                {
                    oRegistro.pdf = archivo;
                    oRegistro.pdfEstado = "Generado el " + DateTime.Now.ToString("yyyy-MM-dd HH:mm");

                }


                if (!Globales.automatico)
                {
                    if (verPdf)
                    {
                        frmPDF ofrmPDF = new frmPDF(archivo);
                        ofrmPDF.Show();
                    }

                }

            }
            catch (CrystalReportsException eCrystal)
            {
                ErrorFX.mostrar(eCrystal, true, true, "cComprobantePago - 462 ");
                if (oRegistro != null)
                {
                    oRegistro.pdfEstado = eCrystal.Message.ToString() + " " + DateTime.Now.ToString("yyyy-MM-dd HH:mm");
                }
            }
            catch (Exception error)
            {
                ErrorFX.mostrar(error, true, true, "cComprobantePago - 1159 ");
                if (oRegistro != null)
                {
                    oRegistro.pdfEstado = error.Message.ToString() + " " + DateTime.Now.ToString("yyyy-MM-dd HH:mm");
                }
            }
            finally
            {
                if (oRegistro != null)
                {
                    dbContext.pagoSet.Attach(oRegistro);
                    dbContext.Entry(oRegistro).State = System.Data.Entity.EntityState.Modified;
                    dbContext.SaveChanges();
                    dbContext.Dispose();
                }

            }
        }

        public bool enviar(pago oPago, bool adjuntar, bool cancelado, bool validar_automatico, Encapsular oEncapsular
        , string correoPara = null)
        {
            try
            {
                if (!bool.Parse(oEncapsular.ocEMPRESA.ENVIO_CORREO))
                {
                    ErrorFX.mostrar("La empresa " + oEncapsular.ocEMPRESA.ID + " entidad " + oEncapsular.ocEMPRESA.ENTITY_ID +
                        @"  no esta configurada para envio de correo", true, true, true);
                    return false;
                }

                cSERIE ocSERIE = new cSERIE();
                ocSERIE.cargar_serie(oPago.serie);

                if (validar_automatico)
                {
                    if (ocSERIE.ENVIO_AUTOMATICO_MAIL == "")
                    {
                        ocSERIE.ENVIO_AUTOMATICO_MAIL = "False";
                    }
                    if (!bool.Parse(ocSERIE.ENVIO_AUTOMATICO_MAIL))
                    {
                        return true;
                    }
                }

                string FROM_CORREO = oEncapsular.ocEMPRESA.USUARIO;
                MailAddress from = new MailAddress(FROM_CORREO, ocSERIE.REMITENTE,
                                System.Text.Encoding.UTF8);

                // Specify the message content.
                MailMessage message = new MailMessage();
                message.From = from;
                //Destinatarios.                
                //TO
                string contactos = buscarContactos(oPago.customerId, oEncapsular);
                if (!String.IsNullOrEmpty(contactos))
                {
                    string[] rEmail = contactos.Split(';');
                    if (rEmail.Length > 0)
                    {
                        for (int i = 0; i < rEmail.Length; i++)
                        {
                            if (!String.IsNullOrEmpty(rEmail[i].ToString().Trim()))
                            {
                                if (isEmail(rEmail[i].ToString().Trim()))
                                {
                                    message.To.Add(rEmail[i].ToString().Trim());
                                }
                            }
                        }
                    }
                }

                //PARA
                if (String.IsNullOrEmpty(correoPara))
                {
                    string[] rEmailPARA = ocSERIE.PARA.Split(';');
                    for (int i = 0; i < rEmailPARA.Length; i++)
                    {
                        if (!String.IsNullOrEmpty(rEmailPARA[i].ToString().Trim()))
                        {
                            if (isEmail(rEmailPARA[i].ToString().Trim()))
                            {
                                message.To.Add(rEmailPARA[i].ToString().Trim());
                            }
                        }
                    }
                }
                else
                {
                    if (isEmail(correoPara))
                    {
                        message.To.Add(correoPara);
                    }
                }

                if (message.To.Count == 0)
                {
                    ErrorFX.mostrar("cComprobantePago - 1135 - El correo no puede enviarse, no existen destinatarios.", true, true, true);
                    return false;
                }

                //CC
                if (String.IsNullOrEmpty(correoPara))
                {
                    string[] rEmailCC = ocSERIE.CC.Split(new char[] { ',', ';' }, StringSplitOptions.RemoveEmptyEntries);
                    for (int i = 0; i < rEmailCC.Length; i++)
                    {
                        if (!String.IsNullOrEmpty(rEmailCC[i].ToString().Trim()))
                        {
                            if (isEmail(rEmailCC[i].ToString().Trim()))
                            {
                                message.CC.Add(rEmailCC[i].ToString().Trim());
                            }
                        }
                    }

                    //CCO
                    string[] rEmailCCO = ocSERIE.CCO.Split(new char[] { ',', ';' }, StringSplitOptions.RemoveEmptyEntries);
                    for (int i = 0; i < rEmailCCO.Length; i++)
                    {
                        if (!String.IsNullOrEmpty(rEmailCCO[i].ToString().Trim()))
                        {
                            if (isEmail(rEmailCCO[i].ToString().Trim()))
                            {
                                message.Bcc.Add(rEmailCCO[i].ToString().Trim());
                            }
                        }
                    }
                }

                //Configurar Mensaje
                string mensaje_enviar = ocSERIE.MENSAJE.Replace("<<NUMFACTURA>>", oPago.serie + oPago.folio);
                string asunto_enviar = ocSERIE.ASUNTO.Replace("<<NUMFACTURA>>", oPago.serie + oPago.folio);

                if (adjuntar)
                {
                    mensaje_enviar = ocSERIE.MENSAJE_ADJUNTO;
                    asunto_enviar = ocSERIE.ASUNTO_ADJUNTO.Replace("<<NUMFACTURA>>", oPago.serie + oPago.folio);
                }
                if (cancelado)
                {
                    mensaje_enviar = ocSERIE.MENSAJE_CANCELADO;
                    mensaje_enviar = ocSERIE.MENSAJE_CANCELADO.Replace("<<NUMFACTURA>>", oPago.serie + oPago.folio);
                }
                //Verificar el Asunto personalizado por cliente
                //cCLIENTE_CONFIGURACION ocCLIENTE_CONFIGURACION = new cCLIENTE_CONFIGURACION(oData);
                //if (ocCLIENTE_CONFIGURACION.cargar_CUSTOMER_ID(oPago.customerId))
                //{
                //    asunto_enviar = ocCLIENTE_CONFIGURACION.ASUNTO;
                //    //Cargar Correos
                //    if (ocCLIENTE_CONFIGURACION.CORREO != "")
                //    {
                //        message.To.Clear();
                //        if (String.IsNullOrEmpty(correoPara))
                //        {
                //            rEmail = ocCLIENTE_CONFIGURACION.CORREO.Split(';');
                //            for (int i = 0; i < rEmail.Length; i++)
                //            {
                //                if (!String.IsNullOrEmpty(rEmail[i].ToString().Trim()))
                //                {
                //                    if (isEmail(rEmail[i].ToString().Trim()))
                //                    {
                //                        message.To.Add(rEmail[i].ToString().Trim());
                //                    }
                //                }
                //            }
                //        }
                //    }

                //}

                if (mensaje_enviar.Length == 0)
                {
                    mensaje_enviar = ocSERIE.MENSAJE.Replace("<<NUMFACTURA>>", oPago.serie + oPago.folio);
                }
                if (asunto_enviar.Length == 0)
                {
                    asunto_enviar = ocSERIE.ASUNTO.Replace("<<NUMFACTURA>>", oPago.serie + oPago.folio);
                }

                asunto_enviar = asunto_enviar.Replace("<<CUSTOMER_ID>>", oPago.customerId);
                asunto_enviar = asunto_enviar.Replace("<<NUMFACTURA>>", oPago.serie + oPago.folio);
                asunto_enviar = asunto_enviar.Replace("<<INVOICE_DATE>>", oPago.fechaPago.ToString("dd/MM/yyyy"));
                asunto_enviar = asunto_enviar.Replace("<<TOTAL_AMOUNT>>", (Math.Abs((decimal.Parse(oPago.monto.ToString())))).ToString("$###,###,##0.00"));

                mensaje_enviar = mensaje_enviar.Replace("<<NUMFACTURA>>", oPago.serie + oPago.folio);
                mensaje_enviar = mensaje_enviar.Replace("<<CUSTOMER_ID>>", oPago.customerId);
                mensaje_enviar = mensaje_enviar.Replace("<<INVOICE_DATE@4>>", oPago.fechaPago.AddMonths(4).ToString("dd/MM/yyyy"));
                mensaje_enviar = mensaje_enviar.Replace("<<INVOICE_DATE>>", oPago.fechaPago.ToString("dd/MM/yyyy"));
                mensaje_enviar = mensaje_enviar.Replace("<<TOTAL_AMOUNT>>", (Math.Abs((decimal.Parse(oPago.monto.ToString())))).ToString("$###,###,##0.00"));

                mensaje_enviar = Regex.Replace(mensaje_enviar, Environment.NewLine, "<br>", RegexOptions.Multiline);
                mensaje_enviar = Regex.Replace(mensaje_enviar, "\n", "<br>", RegexOptions.Multiline);

                message.Body = mensaje_enviar; //+ "<br> Enviado el Fecha y Hora: " + DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss");
                message.BodyEncoding = System.Text.Encoding.UTF8;



                message.Subject = asunto_enviar;
                message.SubjectEncoding = System.Text.Encoding.UTF8;
                message.IsBodyHtml = true;
                message.Priority = MailPriority.High;

                //Añadir Atachment
                Attachment data;

                if (adjuntar)
                {
                    //Adjuntar los Archivos XML y PDF
                    if (!String.IsNullOrEmpty(oPago.xml.ToString().Trim()))
                    {
                        //Validar que exista el XML Factura
                        string archivotmp = oPago.xml.ToString();
                        if (File.Exists(archivotmp))
                        {
                            data = new Attachment(archivotmp);
                            data.Name = oPago.serie + oPago.folio + ".xml";
                            message.Attachments.Add(data);
                        }

                    }


                    if (!String.IsNullOrEmpty(oPago.pdf.ToString().Trim()))
                    {
                        //Validar que exista el XML Factura
                        string archivotmp = oPago.pdf.ToString();
                        if (File.Exists(archivotmp))
                        {
                            data = new Attachment(archivotmp);
                            data.Name = oPago.serie + oPago.folio + ".pdf";
                            message.Attachments.Add(data);
                        }

                    }

                }

                try
                {
                    //Configurar SMPT
                    SmtpClient client = new SmtpClient(oEncapsular.ocEMPRESA.SMTP, int.Parse(oEncapsular.ocEMPRESA.PUERTO));
                    //Configurar Crendiciales de Salida
                    client.Host = oEncapsular.ocEMPRESA.SMTP;
                    client.Port = int.Parse(oEncapsular.ocEMPRESA.PUERTO);
                    client.DeliveryMethod = SmtpDeliveryMethod.Network;

                    client.UseDefaultCredentials = bool.Parse(oEncapsular.ocEMPRESA.CRENDENCIALES);
                    client.EnableSsl = bool.Parse(oEncapsular.ocEMPRESA.SSL);
                    client.Timeout = 20000;
                    if (oEncapsular.ocEMPRESA.PASSWORD_EMAIL != "")
                    {
                        string passwordMailtr = cENCRIPTACION.Decrypt(oEncapsular.ocEMPRESA.PASSWORD_EMAIL);
                        System.Net.NetworkCredential credenciales = new System.Net.NetworkCredential(oEncapsular.ocEMPRESA.USUARIO, passwordMailtr);
                        client.Credentials = credenciales;
                    }
                    else
                    {
                        System.Net.NetworkCredential credenciales = new System.Net.NetworkCredential(oEncapsular.ocEMPRESA.USUARIO, "");
                        client.Credentials = credenciales;
                    }
                    client.Send(message);
                }
                catch (Exception error)
                {
                    ErrorFX.mostrar(error, false, true, true);
                }

            }
            catch (Exception ex)
            {
                ErrorFX.mostrar(ex, true, true, "cComprobantePago Enviar - 1310 -", true);
                return false;
            }

            return true;
        }

        private string buscarContactos(string customerId, Encapsular oEncapsular)
        {
            try
            {
                string devolver = "";
                string sSQL = @"
                SELECT CONTACT_EMAIL
                FROM CUSTOMER_CONTACT
                WHERE CUSTOMER_ID = '" + customerId + @"' AND CONTACT_EMAIL LIKE ';%'
                ";
                DataTable oDataTable = oEncapsular.oData_ERP.EjecutarConsulta(sSQL);
                if (oDataTable != null)
                {
                    foreach (DataRow oDataRow in oDataTable.Rows)
                    {
                        if (!String.IsNullOrEmpty(devolver))
                        {
                            devolver += ";";
                        }
                        devolver += oDataRow["CONTACT_EMAIL"].ToString().Replace(";", "");
                    }
                }
                return devolver;
            }
            catch (Exception error)
            {
                ErrorFX.mostrar(error, true, true, "cComprobantePago Enviar - 1322 -", true);
                return null;
            }
        }

        public static bool isEmail(string inputEmail)
        {
            if (inputEmail == null || inputEmail.Length == 0)
            {
                return false;
            }

            const string expression = @"^([a-zA-Z0-9_\-\.]+)@((\[[0-9]{1,3}" +
                                      @"\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9\-]+\" +
                                      @".)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$";

            Regex regex = new Regex(expression);
            return regex.IsMatch(inputEmail);
        }

        private bool timbrarSolucionFactible(int opcion, string archivo, string factura)
        {
            string prod_endpoint = "TimbradoEndpoint_PRODUCCION";
            string test_endpoint = "TimbradoEndpoint_TESTING";

            //Si recibe error 417 deberá descomentar la linea a continuación
            System.Net.ServicePointManager.Expect100Continue = false;

            //El paquete o namespace en el que se encuentran las clases
            //será el que se define al agregar la referencia al WebService,
            //en este ejemplo es: com.sf.ws.Timbrado

            Solucion_Factible.TimbradoPortTypeClient portClient = null;
            portClient = (opcion == 1)
                ? new Solucion_Factible.TimbradoPortTypeClient(prod_endpoint)
                : portClient = new Solucion_Factible.TimbradoPortTypeClient(test_endpoint);

            try
            {
                System.Console.WriteLine("Sending request...");
                System.Console.WriteLine("EndPoint = " + portClient.Endpoint.Address);

                //Comprimir Archivo
                string Archivo_Tmp = factura + ".xml";
                File.Copy(archivo, Archivo_Tmp, true);
                string ARCHIVO_zip = Archivo_Tmp.ToUpper().Replace("XML", "ZIP");
                using (ZipFile zip = new ZipFile())
                {
                    zip.AddFile(Archivo_Tmp);
                    zip.Save(ARCHIVO_zip);
                }
                //Fin de Compresion de Archivo
                File.Delete(Archivo_Tmp);

                byte[] ARCHIVO_Leido = ReadBinaryFile(ARCHIVO_zip);
                File.Delete(ARCHIVO_zip);
                Solucion_Factible.CFDICertificacion response;
                if (opcion == 2)
                {
                    response = portClient.timbrar("testing@solucionfactible.com", "timbrado.SF.16672", ARCHIVO_Leido, true);
                }
                else
                {
                    //Cargar los datos del SL
                    Globales.cargarUsuarioSL();
                    response = portClient.timbrar(Globales.oCFDI_USUARIO.USUARIO, cENCRIPTACION.Decrypt(Globales.oCFDI_USUARIO.PASSWORD), ARCHIVO_Leido, true);

                }

                System.Console.WriteLine("Información de la transacción");
                System.Console.WriteLine(response.status);
                System.Console.WriteLine(response.mensaje);
                System.Console.WriteLine("Resultados recibidos" + response.resultados.Length);

                Solucion_Factible.CFDIResultadoCertificacion[] resultados = response.resultados;
                if (resultados[0] != null)
                {
                    if (resultados[0].status == 200)
                    {
                        guardarArchivoSolucionFactible(resultados[0].cfdiTimbrado, archivo, factura);
                    }
                    else
                    {
                        Exception oException = new Exception(resultados[0].mensaje);
                        cargarErrorCompleto(oException);
                        ErrorFX.mostrar(oException, true, true, "Factura: " + factura, true);
                        return false;
                    }
                }
                return true;
            }
            catch (Exception oException)
            {
                cargarErrorCompleto(oException);
                ErrorFX.mostrar(oException, true, true, "cComprobantePago.cs - 2231 - Timbrado Solución Factible", true);
                return false;
            }
        }
        string error = "";
        private void cargarErrorCompleto(Exception error)
        {
            string mensaje = error.Message.ToString();
            if (error.InnerException != null)
            {
                mensaje += Environment.NewLine + error.InnerException.Message.ToString();
            }
            mensaje = mensaje.Replace("'", "");
            this.error = mensaje;
        }
        private void guardarArchivoSolucionFactible(byte[] PROCESO, string archivo, string factura)
        {
            try
            {
                File.WriteAllBytes(archivo, PROCESO);

            }
            catch (Exception oException)
            {
                ErrorFX.mostrar(oException, true, true, "Timbrar.cs - 306 - Solucion Factible", true);
            }

        }

        public void sendSMTPMail(cSERIE ocSERIE, cEMPRESA ocEMPRESA)
        {

            string mail_user = ocEMPRESA.USUARIO;
            string mail_smtp = ocEMPRESA.SMTP;
            string mail_pass = ocEMPRESA.PASSWORD_EMAIL;


            MailMessage mm = new MailMessage();
            mm.IsBodyHtml = true;
            mm.From = new MailAddress(mail_user, "Usage", Encoding.UTF8);
            mm.Subject = ocSERIE.ASUNTO;
            mm.Body = mensaje;
            mm.SubjectEncoding = System.Text.Encoding.UTF8;
            mm.IsBodyHtml = true;
            mm.Priority = MailPriority.High;

            string[] rEmailCC = ocSERIE.PARA.ToString().Split(';');
            for (int i = 0; i < rEmailCC.Length; i++)
            {
                if (!String.IsNullOrEmpty(rEmailCC[i].ToString().Trim()))
                {
                    if (Globales.isEmail(rEmailCC[i].ToString().Trim()))
                    {
                        mm.To.Add(rEmailCC[i].ToString().Trim());
                    }
                }
            }
            try
            {
                var smtp = new SmtpClient
                {
                    Host = mail_smtp,
                    Port = 587,
                    EnableSsl = true,
                    DeliveryMethod = SmtpDeliveryMethod.Network,
                    UseDefaultCredentials = false,
                    Credentials = new System.Net.NetworkCredential(mail_user, mail_pass)
                };
                smtp.Send(mm);
            }
            catch (Exception error)
            {
                string mensajetr = error.Message.ToString();
                if (error.InnerException != null)
                {
                    mensajetr += Environment.NewLine + error.InnerException.Message.ToString();
                }
                BitacoraFX.Log("Envio de correo ErrorFX: " + mensajetr);
            }

        }
    }

}
