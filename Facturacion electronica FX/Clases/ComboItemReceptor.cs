﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FE_FX.Clases
{
    public class ComboItemReceptor
    {
        public string Name { get; set; }
        public string Value { get; set; }
        public string OtroValor { get; set; }
        public override string ToString()
        {
            if (String.IsNullOrEmpty(Name) & String.IsNullOrEmpty(Value))
            {
                return "";
            }
            else
            {
                return this.Value;
            }

        }
    }
}
