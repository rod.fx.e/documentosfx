﻿using Generales;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FE_FX.Clases
{
    class cRMA
    {
        public bool FacturaRMACompleto(string invoiceid,Encapsular oEncapsular)
        {
            try
            {
                cCONEXCION oData_ERPp = new cCONEXCION(oEncapsular.ocEMPRESA.TIPO, oEncapsular.ocEMPRESA.SERVIDOR
                                    , oEncapsular.ocEMPRESA.BD, oEncapsular.ocEMPRESA.USUARIO_BD, oEncapsular.ocEMPRESA.PASSWORD_BD);

                string Sql = @"
                SELECT TOP 100 rl.INVOICE_ID,sl.PACKLIST_ID,sl.RMA_ID, sl.RMA_LINE_NO, sl.SHIPPED_QTY, TablaRMA.PACKLIST_ID as PACKLIST_ID_ORIGEN
                , TablaRMA.SHIPPED_QTY
                ,sl.SHIPPED_QTY+TablaRMA.SHIPPED_QTY as CantidadRestante
                ,TablaRMA.INVOICE_ID,TablaRMA.UUID,TablaRMA.INVOICE_DATE
                FROM RECEIVABLE_LINE rl 
                INNER JOIN SHIPPER_LINE sl ON sl.PACKLIST_ID=rl.PACKLIST_ID
                INNER JOIN 
                (
                SELECT r.ID, r.PACKLIST_ID, sl.SHIPPED_QTY, rl2.INVOICE_ID
                , v.UUID, r2.INVOICE_DATE
                FROM RMA r 
                INNER JOIN RMA_LINE rl ON rl.RMA_ID=r.ID
                INNER JOIN SHIPPER_LINE sl ON sl.PACKLIST_ID=r.PACKLIST_ID
                INNER JOIN RECEIVABLE_LINE rl2 ON rl2.PACKLIST_ID=r.PACKLIST_ID
                INNER JOIN RECEIVABLE r2 ON r2.INVOICE_ID=rl2.INVOICE_ID
                INNER JOIN SRT..VMX_FE v ON v.INVOICE_ID=rl2.INVOICE_ID
                ) as TablaRMA on TablaRMA.ID=sl.RMA_ID
                WHERE (sl.SHIPPED_QTY+TablaRMA.SHIPPED_QTY)<>0
                AND rl.INVOICE_ID='" + invoiceid + @"'
                ";

                DataTable oDataTable = oData_ERPp.EjecutarConsulta(Sql);
                oData_ERPp.DestruirConexion();
                if (oDataTable != null)
                {
                    foreach (DataRow oDataRow in oDataTable.Rows)
                    {
                    }
                }
                    
            }
            catch (Exception errores)
            {
                ErrorFX.mostrar(errores, true, true, "generar40 - 1432 - generarComprobantePago ", false);
            }
            return false;
        }
        public bool FacturaTieneRMA(string invoiceid, Encapsular oEncapsular)
        {
            try
            {
                cCONEXCION oData_ERPp = new cCONEXCION(oEncapsular.ocEMPRESA.TIPO, oEncapsular.ocEMPRESA.SERVIDOR
                                    , oEncapsular.ocEMPRESA.BD, oEncapsular.ocEMPRESA.USUARIO_BD, oEncapsular.ocEMPRESA.PASSWORD_BD);

                string Sql = @"
                SELECT TOP 100 rl.INVOICE_ID,sl.PACKLIST_ID,sl.RMA_ID, sl.RMA_LINE_NO, sl.SHIPPED_QTY, TablaRMA.PACKLIST_ID as PACKLIST_ID_ORIGEN
                , TablaRMA.SHIPPED_QTY
                ,sl.SHIPPED_QTY+TablaRMA.SHIPPED_QTY as CantidadRestante
                ,TablaRMA.INVOICE_ID,TablaRMA.UUID,TablaRMA.INVOICE_DATE
                FROM RECEIVABLE_LINE rl 
                INNER JOIN SHIPPER_LINE sl ON sl.PACKLIST_ID=rl.PACKLIST_ID
                INNER JOIN 
                (
                SELECT r.ID, r.PACKLIST_ID, sl.SHIPPED_QTY, rl2.INVOICE_ID
                , v.UUID, r2.INVOICE_DATE
                FROM RMA r 
                INNER JOIN RMA_LINE rl ON rl.RMA_ID=r.ID
                INNER JOIN SHIPPER_LINE sl ON sl.PACKLIST_ID=r.PACKLIST_ID
                INNER JOIN RECEIVABLE_LINE rl2 ON rl2.PACKLIST_ID=r.PACKLIST_ID
                INNER JOIN RECEIVABLE r2 ON r2.INVOICE_ID=rl2.INVOICE_ID
                INNER JOIN SRT..VMX_FE v ON v.INVOICE_ID=rl2.INVOICE_ID
                ) as TablaRMA on TablaRMA.ID=sl.RMA_ID
                WHERE (sl.SHIPPED_QTY+TablaRMA.SHIPPED_QTY)<>0
                AND rl.INVOICE_ID='" + invoiceid + @"'
                ";

                DataTable oDataTable = oData_ERPp.EjecutarConsulta(Sql);
                oData_ERPp.DestruirConexion();
                if (oDataTable != null)
                {
                    foreach (DataRow oDataRow in oDataTable.Rows)
                    {
                    }
                }

            }
            catch (Exception errores)
            {
                ErrorFX.mostrar(errores, true, true, "generar40 - 1432 - generarComprobantePago ", false);
            }
            return false;
        }

    }
}
