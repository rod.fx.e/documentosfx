﻿using com.google.zxing.qrcode;
using CrystalDecisions.CrystalReports.Engine;
using DevComponents.DotNetBar;
using FE_FX.CFDI;
using FE_FX.Retencion;
using Generales;
using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml;
using System.Xml.Serialization;
using System.Xml.XPath;
using System.Xml.Xsl;

namespace FE_FX.Clases
{
    class cRetencion
    {
        public void imprimir(string pathXML="")
        {
            if (String.IsNullOrEmpty(pathXML))
            {
                OpenFileDialog openFileDialog1 = new OpenFileDialog
                {
                    InitialDirectory = @"D:\",
                    Title = "CFDI ",
                    CheckFileExists = true,
                    CheckPathExists = true,
                    DefaultExt = "xml",
                    Filter = "CFDI XML files (*.xml)|*.xml",
                    FilterIndex = 2,
                    RestoreDirectory = true,
                    ReadOnlyChecked = true,
                    ShowReadOnly = true
                };

                if (openFileDialog1.ShowDialog() == DialogResult.OK)
                {
                    pathXML = openFileDialog1.FileName;
                }

            }

            if (String.IsNullOrEmpty(pathXML))
            {
                return;
            }


            ReportDocument oReport = new ReportDocument();
            string FORMATO = "RETENCIONds.RPT";
            if (!File.Exists(FORMATO))
            {
                MessageBoxEx.Show("El Reporte " + FORMATO + " no existe en su directorio. La retencion no fue Generada."
                    , Application.ProductName + "-" + Application.ProductVersion, MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            if (!File.Exists(pathXML))
            {
                MessageBoxEx.Show("La Retención no existe en su directorio. La retencion no fue Generada."
                    , Application.ProductName + "-" + Application.ProductVersion, MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            oReport.Load(FORMATO);
            //DataSet oDataSet = new DataSet();
            
            //oDataSet.ReadXml(pathXML);
            //XmlDocument doc = new XmlDocument();
            //doc.Load(pathXML);

            //XmlNodeList TimbreFiscalDigital = doc.GetElementsByTagName("tfd:TimbreFiscalDigital");
            //if (TimbreFiscalDigital == null)
            //{
            //    MessageBoxEx.Show("El archivo " + pathXML + " no esta timbrado."
            //    , Application.ProductName + "-" + Application.ProductVersion, MessageBoxButtons.OK, MessageBoxIcon.Error);
            //    return;
            //}

            //XmlNodeList Emisor = doc.GetElementsByTagName("retenciones:Emisor");
            //string RegimenFiscal = "";
            //if (Emisor != null)
            //{
            //    RegimenFiscal = Emisor[0].Attributes["RegimenFiscal"].Value;

            //    MessageBoxEx.Show("El archivo " + pathXML + " no tiene retenciones:Emisor."
            //    , Application.ProductName + "-" + Application.ProductVersion, MessageBoxButtons.OK, MessageBoxIcon.Error);
            //    return;
            //}

            //XmlNodeList Receptor = doc.GetElementsByTagName("retenciones:Receptor");
            //if (Receptor == null)
            //{
            //    MessageBoxEx.Show("El archivo " + pathXML + " no tiene retenciones:Receptor."
            //    , Application.ProductName + "-" + Application.ProductVersion, MessageBoxButtons.OK, MessageBoxIcon.Error);
            //    return;
            //}

            //byte[] image = generarImagen(pathXML, TimbreFiscalDigital, Emisor, Receptor);
            //dbImageb.dtImagenDataTable oDATOS = new dbImageb.dtImagenDataTable();
            //oDATOS.AdddtImagenRow(image);

            //oReport.SetDataSource(oDataSet);
            //if (oReport.Database.Tables.Count > 0)
            //{
            //    //oReport.SetDataSource(oDataSet);
            //    foreach (CrystalDecisions.CrystalReports.Engine.Table tabla in oReport.Database.Tables)
            //    {
            //        if (tabla.Name.ToString() == "dtImagen")
            //        {
            //            oReport.Database.Tables["dtImagen"].SetDataSource((System.Data.DataTable)oDATOS);
            //        }
            //        if (tabla.Name.ToString() == "Emisor")
            //        {
            //            oReport.Database.Tables["Emisor"].SetDataSource(oDataSet.Tables["retenciones:Emisor"]);
            //        }
            //        if (tabla.Name.ToString() == "ImpRetenidos")
            //        {
            //            oReport.Database.Tables["ImpRetenidos"].SetDataSource(oDataSet.Tables["retenciones:ImpRetenidos"]);
            //        }
            //        if (tabla.Name.ToString() == "NoBeneficiario")
            //        {
            //            oReport.Database.Tables["NoBeneficiario"].SetDataSource(oDataSet.Tables["retenciones:NoBeneficiario"]);
            //        }
            //        if (tabla.Name.ToString() == "Periodo")
            //        {
            //            oReport.Database.Tables["Periodo"].SetDataSource(oDataSet.Tables["retenciones:Periodo"]);
            //        }
            //        if (tabla.Name.ToString() == "Retenciones")
            //        {
            //            oReport.Database.Tables["Retenciones"].SetDataSource(oDataSet.Tables["retenciones:Retenciones"]);
            //        }
            //        if (tabla.Name.ToString() == "TimbreFiscal")
            //        {
            //            oReport.Database.Tables["TimbreFiscal"].SetDataSource(oDataSet.Tables["retenciones:TimbreFiscal"]);
            //        }
            //        if (tabla.Name.ToString() == "Totales")
            //        {
            //            oReport.Database.Tables["Totales"].SetDataSource(oDataSet.Tables["retenciones:Totales"]);
            //        }
            //    }
            //}

            

            frmCFDIImpresion ofrmReporte = new frmCFDIImpresion(oReport);
            ofrmReporte.ShowDialog();
        }

        public void imprimirXML(string pathXML = "")
        {
            if (String.IsNullOrEmpty(pathXML))
            {
                OpenFileDialog openFileDialog1 = new OpenFileDialog
                {
                    InitialDirectory = @"D:\",
                    Title = "CFDI ",
                    CheckFileExists = true,
                    CheckPathExists = true,
                    DefaultExt = "xml",
                    Filter = "CFDI XML files (*.xml)|*.xml",
                    FilterIndex = 2,
                    RestoreDirectory = true,
                    ReadOnlyChecked = true,
                    ShowReadOnly = true
                };

                if (openFileDialog1.ShowDialog() == DialogResult.OK)
                {
                    pathXML = openFileDialog1.FileName;
                }

            }

            if (String.IsNullOrEmpty(pathXML))
            {
                return;
            }


            ReportDocument oReport = new ReportDocument();
            string FORMATO = "RETENCION.RPT";
            if (!File.Exists(FORMATO))
            {
                MessageBoxEx.Show("El Reporte " + FORMATO + " no existe en su directorio. La retencion no fue Generada."
                    , Application.ProductName + "-" + Application.ProductVersion, MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            if (!File.Exists(pathXML))
            {
                MessageBoxEx.Show("La Retención no existe en su directorio. La retencion no fue Generada."
                    , Application.ProductName + "-" + Application.ProductVersion, MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            string EmisorNombre = "Nombre del Emisor";
            string EmisorRFC = "RFC del Emisor";
            string Clave = "Clave";
            string Pais = "País";
            string Descripcion = "Descripción";

            // Suponiendo que Beneficiario es una cadena
            string Beneficiario = "Nombre del Beneficiario";
            string ReceptoNombre = "Nombre del Receptor";


            string SelloSAT = "Sello del SAT";

            string TipoPagoRet = "Tipo de Pago de Retención";

            string TipoPago = "Tipo de Pago";

            string ReceptorRFC = "RFC del Receptor";

            oReport.Load(FORMATO);
            DataSet oDataSet = new DataSet();

            XmlSerializer serializerXML = new XmlSerializer(typeof(Retenciones));
            TextReader reader = new StreamReader(pathXML);
            Retenciones ORetenciones = (Retenciones)serializerXML.Deserialize(reader);
            reader.Close();

            XmlDocument doc = new XmlDocument();
            doc.Load(pathXML);
            XmlNodeList TimbreFiscalDigital = doc.GetElementsByTagName("tfd:TimbreFiscalDigital");
            if (TimbreFiscalDigital == null)
            {
                ErrorFX.mostrar("El archivo: " + pathXML + " No esta timbrado.", true, true, false);
                return;
            }
            string UUID = "";
            UUID = TimbreFiscalDigital[0].Attributes["UUID"].Value;
            string FechaTimbrado = "";
            FechaTimbrado = TimbreFiscalDigital[0].Attributes["FechaTimbrado"].Value;
            string noCertificadoSAT = "";
            noCertificadoSAT = TimbreFiscalDigital[0].Attributes["NoCertificadoSAT"].Value;
            string selloSAT = "";
            selloSAT = TimbreFiscalDigital[0].Attributes["SelloSAT"].Value;
            
            XmlNodeList Emisor = doc.GetElementsByTagName("retenciones:Emisor");
            if (Emisor == null)
            {
                //RegimenFiscal = Emisor[0].Attributes["RegimenFiscal"].Value;

                MessageBoxEx.Show("El archivo " + pathXML + " no tiene retenciones:Emisor."
                , Application.ProductName + "-" + Application.ProductVersion, MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            else
            {
                EmisorNombre = Emisor[0].Attributes["NomDenRazSocE"].Value;
                EmisorRFC = Emisor[0].Attributes["RfcE"].Value;
            }
            XmlNodeList Receptor = doc.GetElementsByTagName("retenciones:Receptor");
            if (Receptor == null)
            {
                MessageBoxEx.Show("El archivo " + pathXML + " no tiene retenciones:Receptor."
                , Application.ProductName + "-" + Application.ProductVersion, MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            XmlNodeList Extranjero = doc.GetElementsByTagName("retenciones:Extranjero");
            if (Extranjero != null)
            {
                ReceptoNombre = Extranjero[0].Attributes["NomDenRazSocR"].Value;
                ReceptorRFC = Extranjero[0].Attributes["NumRegIdTribR"].Value;
            }

            string Ejercicio = string.Empty;
            string MesFin = string.Empty;
            string MesIni = string.Empty;
            XmlNodeList Periodo = doc.GetElementsByTagName("retenciones:Periodo");
            if (Periodo != null)
            {
                Ejercicio = Periodo[0].Attributes["Ejercicio"].Value;
                MesFin = Periodo[0].Attributes["MesFin"].Value;
                MesIni = Periodo[0].Attributes["MesIni"].Value;
            }

            string CadenaOriginal = CadenaRetencion(pathXML);
            
            byte[] image = generarImagen(pathXML, TimbreFiscalDigital
                , Emisor
                , Receptor);

            dtsRetencion2 OdtsRetencion2 = new dtsRetencion2();


            int Consecutivo = 1;
            foreach (var impuestoretenido in ORetenciones.Totales.ImpRetenidos)
            {
                OdtsRetencion2.RetencionCompleta.AddRetencionCompletaRow(EmisorNombre, EmisorRFC, Clave, Pais, Descripcion,
                Beneficiario, ReceptoNombre
                , ORetenciones.Totales.MontoTotExent, ORetenciones.Totales.MontoTotRet, ORetenciones.Totales.MontoTotGrav
                , UUID, FechaTimbrado, SelloSAT, ORetenciones.Totales.MontoTotOperacion, TipoPagoRet.ToString()
                , impuestoretenido.MontoRet, impuestoretenido.ImpuestoRet
                , impuestoretenido.BaseRet.ToString(), Ejercicio.ToString(), MesIni.ToString(), MesFin.ToString()
                , impuestoretenido.TipoPagoRet.ToString().Replace("Item", ""), image, ReceptorRFC
                , ORetenciones.FolioInt, ORetenciones.Version, ORetenciones.CveRetenc.ToString().Replace("Item","")
                , ORetenciones.DescRetenc, ORetenciones.LugarExpRetenc
                , ORetenciones.FechaExp
                , CadenaOriginal, noCertificadoSAT, selloSAT
                );

                Consecutivo++;
            }


            oReport.SetDataSource(OdtsRetencion2);

            frmCFDIImpresion ofrmReporte = new frmCFDIImpresion(oReport);
            ofrmReporte.ShowDialog();
        }

        private string CadenaRetencion(string ARCHIVO)
        {
            //Cargar el XML
            StreamReader reader = new StreamReader(ARCHIVO);
            XPathDocument myXPathDoc = new XPathDocument(reader);

            //Cargando el XSLT
            string sDirectory = AppDomain.CurrentDomain.BaseDirectory;
            XslCompiledTransform myXslTrans = new XslCompiledTransform();
            myXslTrans.Load(sDirectory + "/XSLT/retenciones.xslt");

            StringWriter str = new StringWriter();
            XmlTextWriter myWriter = new XmlTextWriter(str);

            //Aplicando transformacion
            myXslTrans.Transform(myXPathDoc, null, myWriter);

            //Resultado
            string result = str.ToString();

            return result;
        }

        private byte[] generarImagen(string archivo, XmlNodeList TimbreFiscalDigital, XmlNodeList Emisor, XmlNodeList Receptor)
        {
            //?re=XAXX010101000&rr=XAXX010101000&tt=1234567890.123456&id=ad662d33-6934-459c-a128-BDf0393f0f44
            string QR_Code = "";
            XmlSerializer serializer_CFD_3 = new XmlSerializer(typeof(Retenciones));
            TextReader reader = new StreamReader(archivo);
            try
            {

                

                string UUID = "";
                UUID = TimbreFiscalDigital[0].Attributes["UUID"].Value;

                string rfcEmisor = "";
                rfcEmisor = Emisor[0].Attributes["RfcE"].Value;

                string rfcReceptor = "";
                //if (Receptor[0].Attributes["RFC"].Value != null)
                //{
                //    rfcReceptor = Receptor[0].Attributes["RFC"].Value
                //}
                string montoTotRet = "0";


                QR_Code += "?re=" + rfcEmisor;
                QR_Code += "&rr=" + rfcReceptor;
                QR_Code += "&tt=" + montoTotRet;
                QR_Code += "&id=" + UUID;

            }
            catch (Exception xmlex)
            {
                Bitacora.Log("Errpr :: " + xmlex.ToString());
                return null;
            }
            int size = 320;
            QRCodeWriter writer = new QRCodeWriter();
            com.google.zxing.common.ByteMatrix matrix;
            matrix = writer.encode(QR_Code, com.google.zxing.BarcodeFormat.QR_CODE, size, size, null);
            Bitmap img = new Bitmap(size, size);
            Color Color = Color.FromArgb(0, 0, 0);

            for (int y = 0; y < matrix.Height; ++y)
            {
                for (int x = 0; x < matrix.Width; ++x)
                {
                    Color pixelColor = img.GetPixel(x, y);

                    //Find the colour of the dot
                    if (matrix.get_Renamed(x, y) == -1)
                    {
                        img.SetPixel(x, y, Color.White);
                    }
                    else
                    {
                        img.SetPixel(x, y, Color.Black);
                    }
                }
            }


            string nombre = Path.GetTempPath() + "QRP_TEMP.BMP";
            img.Save(nombre);

            FileStream fs = new FileStream(nombre, System.IO.FileMode.Open, System.IO.FileAccess.Read);
            byte[] Image = new byte[fs.Length];
            fs.Read(Image, 0, Convert.ToInt32(fs.Length));
            int image_32 = Convert.ToInt32(fs.Length);
            fs.Close();
            File.Delete(nombre);

            return Image;

        }

    }
}
