﻿using com.google.zxing.qrcode;
using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;
using DevComponents.DotNetBar;
using FE_FX.CFDI;
using Generales;
using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml;
using System.Xml.Serialization;
using System.Xml.XPath;
using System.Xml.Xsl;

namespace FE_FX.Clases
{
    public class cTraslado
    {
        #region Imprimir

        internal void imprimir(string pathP, bool verPDF = true
        , string embarcadoTr = ""
        ,string permisoTr = "", string entregarTr="", string leyendaFiscal="", List<string> listaEspecies=null)
        {
            ReportDocument oReport = new ReportDocument();
            string path = "";
            if (!String.IsNullOrEmpty(pathP))
            {
                if (File.Exists(pathP))
                {
                    path = pathP;
                }
            }
            else
            {
                OpenFileDialog openFileDialog1 = new OpenFileDialog
                {
                    InitialDirectory = @"D:\",
                    Title = "CFDI ",
                    CheckFileExists = true,
                    CheckPathExists = true,
                    DefaultExt = "xml",
                    Filter = "CFDI XML files (*.xml)|*.xml",
                    FilterIndex = 2,
                    RestoreDirectory = true,
                    ReadOnlyChecked = true,
                    ShowReadOnly = true
                };

                if (openFileDialog1.ShowDialog() == DialogResult.OK)
                {
                    path = openFileDialog1.FileName;
                }

            }

            string FORMATO = "TRASLADOds.rpt";

            
            if (!File.Exists(FORMATO))
            {
                MessageBoxEx.Show("El Reporte " + FORMATO + " no existe en su directorio. La factura no fue Generada.", Application.ProductName + "-" + Application.ProductVersion, MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            try
            {

                XmlSerializer serializer_CFD_3 = new XmlSerializer(typeof(CFDI40.Comprobante));
                TextReader reader = new StreamReader(path);
                CFDI40.Comprobante oComprobanteLocal = (CFDI40.Comprobante)serializer_CFD_3.Deserialize(reader);
                reader.Close();
                oReport.Load(FORMATO);

                XmlDocument doc = new XmlDocument();
                doc.Load(path);
                XmlNodeList TimbreFiscalDigital = doc.GetElementsByTagName("tfd:TimbreFiscalDigital");
                if (TimbreFiscalDigital == null)
                {
                    ErrorFX.mostrar("86 - cTraslado -El archivo: " + path + " No esta timbrado.", true, true, false);
                    return;
                }
                if (TimbreFiscalDigital.Count==0)
                {
                    ErrorFX.mostrar("91 - cTraslado - El archivo: " + path + " No esta timbrado.", true, true, false);
                    return;
                }
                string UUID = "";
                UUID = TimbreFiscalDigital[0].Attributes["UUID"].Value;
                string FechaTimbrado = "";
                FechaTimbrado = TimbreFiscalDigital[0].Attributes["FechaTimbrado"].Value;
                string noCertificadoSAT = "";
                noCertificadoSAT = TimbreFiscalDigital[0].Attributes["NoCertificadoSAT"].Value;
                string selloSAT = "";
                selloSAT = TimbreFiscalDigital[0].Attributes["SelloSAT"].Value;

                XmlNodeList Comprobante = doc.GetElementsByTagName("cfdi:Comprobante");
                string LugarExpedicion = "";
                if (Comprobante != null)
                {
                    LugarExpedicion = Comprobante[0].Attributes["LugarExpedicion"].Value;
                }

                byte[] image = generarImagen(path);
                string cadenaOriginal = generarCadenaOriginal(path);
                string cadenaTFD = generarCadenaTFD(path);


                XmlNodeList Emisor = doc.GetElementsByTagName("cfdi:Emisor");
                string RegimenFiscal = "";
                if (Emisor != null)
                {
                    RegimenFiscal = Emisor[0].Attributes["RegimenFiscal"].Value;
                }

                XmlNodeList Receptor = doc.GetElementsByTagName("cfdi:Receptor");
                string UsoCFDI = "";
                if (Receptor != null)
                {
                    UsoCFDI = Receptor[0].Attributes["UsoCFDI"].Value;
                }
                string NumRegIdTrib = "";
                if (Receptor != null)
                {
                    try
                    {
                        NumRegIdTrib = Receptor[0].Attributes["NumRegIdTrib"].Value;
                    }
                    catch
                    {

                    }
                }

                cfdiCE ocfdiCE = new cfdiCE();
                ocfdiCE.DATOS.AddDATOSRow(image, cadenaOriginal, cadenaTFD, ""
                    , "", 0
                    , oComprobanteLocal.Emisor.Rfc, oComprobanteLocal.Emisor.Nombre, oComprobanteLocal.Emisor.RegimenFiscal.ToString()
                    , oComprobanteLocal.LugarExpedicion
                    , oComprobanteLocal.Receptor.Nombre, oComprobanteLocal.Receptor.Rfc
                    , oComprobanteLocal.Fecha.ToString(), oComprobanteLocal.Serie, oComprobanteLocal.Folio.ToString()
                    , oComprobanteLocal.NoCertificado.ToString()
                    , oComprobanteLocal.TipoDeComprobante.ToString(), 0, 0
                    , oComprobanteLocal.Total
                    , UUID, "", FechaTimbrado, noCertificadoSAT, selloSAT
                    , "", RegimenFiscal, UsoCFDI, LugarExpedicion
                    , NumRegIdTrib
                    );

                int Consecutivo = 1;
                foreach (CFDI40.ComprobanteConcepto concepto in oComprobanteLocal.Conceptos)
                {
                    string EspecieTr = ObtnerEspecie(concepto.NoIdentificacion);
                    try
                    {
                        if (String.IsNullOrEmpty(EspecieTr) && listaEspecies != null)
                        {
                            // Restar 1 a Consecutivo para que funcione con índices que empiezan en 0
                            if (Consecutivo - 1 < listaEspecies.Count)
                            {
                                EspecieTr = listaEspecies[Consecutivo - 1];
                            }
                        }
                    }
                    catch
                    {

                    }


                    ocfdiCE.conceptos.AddconceptosRow(concepto.Cantidad, concepto.Descripcion.Replace("\n", "").Replace("\r", "")
                        , concepto.NoIdentificacion, concepto.ValorUnitario, concepto.Importe, 0,
                        concepto.Unidad, concepto.ClaveProdServ, concepto.ClaveUnidad, Consecutivo
                        , EspecieTr);
                    Consecutivo++;
                }



                oReport.SetDataSource(ocfdiCE);

                foreach (ParameterFieldDefinition definition in oReport.DataDefinition.ParameterFields)
                {
                    ParameterValues myvals = new ParameterValues();
                    ParameterDiscreteValue myDiscrete = new ParameterDiscreteValue();
                    switch (definition.ParameterFieldName)
                    {
                        case "cadenaSAT":
                            //if (!String.IsNullOrEmpty(xml))
                            //{
                            myDiscrete.Value = cadenaTFD;
                            myvals.Add(myDiscrete);
                            definition.CurrentValues.Add(myDiscrete);
                            definition.ApplyCurrentValues(myvals);
                            //}
                            break;
                        case "Embarcado":
                            //if (!String.IsNullOrEmpty(xml))
                            //{
                            myDiscrete.Value = embarcadoTr;
                            myvals.Add(myDiscrete);
                            definition.CurrentValues.Add(myDiscrete);
                            definition.ApplyCurrentValues(myvals);
                            //}
                            break;
                        case "Entregar":
                            //if (!String.IsNullOrEmpty(xml))
                            //{
                            myDiscrete.Value = entregarTr;
                            myvals.Add(myDiscrete);
                            definition.CurrentValues.Add(myDiscrete);
                            definition.ApplyCurrentValues(myvals);
                            //}
                            break;
                        case "Permiso":
                            //if (!String.IsNullOrEmpty(xml))
                            //{
                            myDiscrete.Value = permisoTr;
                            myvals.Add(myDiscrete);
                            definition.CurrentValues.Add(myDiscrete);
                            definition.ApplyCurrentValues(myvals);
                            //}
                            break;
                        case "LeyendaFiscal":
                            //if (!String.IsNullOrEmpty(xml))
                            //{
                            myDiscrete.Value = leyendaFiscal;
                            myvals.Add(myDiscrete);
                            definition.CurrentValues.Add(myDiscrete);
                            definition.ApplyCurrentValues(myvals);
                            //}
                            break;
                    }
                }
                try
                {
                    ExportOptions CrExportOptions = new ExportOptions();
                    DiskFileDestinationOptions CrDiskFileDestinationOptions = new DiskFileDestinationOptions();
                    PdfRtfWordFormatOptions CrFormatTypeOptions = new PdfRtfWordFormatOptions();
                    string pdftr = path.Replace(".xml", ".pdf");
                    pdftr = pdftr.Replace(@"\xml", @"\pdf");
                    CrDiskFileDestinationOptions.DiskFileName = pdftr;
                    CrExportOptions = oReport.ExportOptions;
                    CrExportOptions.ExportDestinationType = ExportDestinationType.DiskFile;
                    CrExportOptions.ExportFormatType = ExportFormatType.PortableDocFormat;
                    CrExportOptions.DestinationOptions = CrDiskFileDestinationOptions;
                    CrExportOptions.FormatOptions = CrFormatTypeOptions;
                    oReport.Export();
                }
                catch (Exception error)
                {
                    string mensaje = "Impresion . ";
                    mensaje += error.Message.ToString();
                    if (error.InnerException != null)
                    {
                        mensaje += " " + error.InnerException.Message.ToString();
                    }

                    MessageBox.Show(mensaje);
                }
                if (verPDF)
                {
                    frmCFDIImpresion ofrmReporte = new frmCFDIImpresion(oReport);
                    ofrmReporte.ShowDialog();
                }
            }
            catch (Exception error)
            {
                string mensaje = error.Message.ToString();
                if (error.InnerException != null)
                {
                    mensaje += Environment.NewLine + error.InnerException.ToString();
                }
                MessageBox.Show(mensaje);
            }

        }

        private string ObtnerEspecie(string noIdentificacion)
        {
            try
            {
                cCONEXCION oData_ERP = new cCONEXCION();
                oData_ERP=oData_ERP.PrimeraEmpresa();

                string Sql = @"
                SELECT TOP 1 t.[APROPERTY_5]      
                FROM [dbo].[TRACE] t 
                WHERE t.PART_ID='" + noIdentificacion + @"'
                ORDER BY ROWID DESC
                ";
                DataTable oDataTable = oData_ERP.EjecutarConsulta(Sql);
                if (oDataTable != null)
                {
                    foreach (DataRow oDataRow in oDataTable.Rows)
                    {
                        return oDataRow["APROPERTY_5"].ToString();
                    }
                }
            }
            catch
            {

            }

            return string.Empty;
        }

        private string generarCadenaOriginal(string archivo)
        {
            XPathDocument myXPathDoc = new XPathDocument(archivo);
            //XslTransform myXslTrans = new XslTransform();
            XslCompiledTransform myXslTrans = new XslCompiledTransform();
            XslCompiledTransform trans = new XslCompiledTransform();
            XmlUrlResolver resolver = new XmlUrlResolver();

            resolver.Credentials = System.Net.CredentialCache.DefaultCredentials;
            XsltSettings settings = new XsltSettings(true, true);
            string sDirectory = AppDomain.CurrentDomain.BaseDirectory;
            //Cargar xslt
            try
            {
                //Version 3
                myXslTrans.Load(sDirectory + "/XSLT/cadenaoriginal_3_2.xslt");
            }
            catch (XmlException errores)
            {
                MessageBox.Show("Error. Conectarse en el Portal del SAT al " + errores.InnerException.ToString());
                return "";
            }

            //Crear Archivo Temporal
            string nombre_tmp = "TMP" + DateTime.Now.ToString("ddMMyyyyhhmmss");
            XmlTextWriter myWriter = new XmlTextWriter(nombre_tmp + ".TXT", null);

            //Transformar al XML
            myXslTrans.Transform(myXPathDoc, null, myWriter);

            myWriter.Close();
            string CADENA_ORIGINAL = "";
            //Abrir Archivo TXT
            StreamReader re = File.OpenText(nombre_tmp + ".TXT");
            string input = null;
            while ((input = re.ReadLine()) != null)
            {
                CADENA_ORIGINAL += input;
            }
            re.Close();

            return CADENA_ORIGINAL;
        }

        private string generarCadenaTFD(string archivo)
        {
            string nombre_tmp = "TMP" + DateTime.Now.ToString("ddMMyyyyhhmmss");
            XmlDocument doc = new XmlDocument();
            doc.Load(archivo);
            XslCompiledTransform myXslTrans = new XslCompiledTransform();
            XslCompiledTransform trans = new XslCompiledTransform();
            XmlUrlResolver resolver = new XmlUrlResolver();

            resolver.Credentials = System.Net.CredentialCache.DefaultCredentials;
            XsltSettings settings = new XsltSettings(true, true);

            //Cargar xslt
            try
            {
                myXslTrans.Load(AppDomain.CurrentDomain.BaseDirectory + "/XSLT/cadenaoriginal_TFD_1_1.xslt");
            }
            catch (XmlException errores)
            {
                MessageBox.Show("Error en " + errores.InnerException.ToString());
                return "";
            }

            //Crear Archivo Temporal

            XmlTextWriter myWriter = new XmlTextWriter(nombre_tmp + ".TXT", null);
            XmlNodeList TimbreFiscalDigital = doc.GetElementsByTagName("tfd:TimbreFiscalDigital");
            //Transformar al XML
            myXslTrans.Transform(TimbreFiscalDigital[0], null, myWriter);

            myWriter.Close();

            //Abrir Archivo TXT
            string cadenaoriginal_TFD_1_0 = "";
            StreamReader re = File.OpenText(nombre_tmp + ".TXT");
            string input = null;
            while ((input = re.ReadLine()) != null)
            {
                cadenaoriginal_TFD_1_0 += input;
            }
            re.Close();

            //Eliminar Archivos Temporales
            File.Delete(nombre_tmp + ".TXT");
            return cadenaoriginal_TFD_1_0;

        }

        private byte[] generarImagen(string archivo)
        {
            //?re=XAXX010101000&rr=XAXX010101000&tt=1234567890.123456&id=ad662d33-6934-459c-a128-BDf0393f0f44
            string QR_Code = "";
            XmlSerializer serializer_CFD_3 = new XmlSerializer(typeof(CFDI33.Comprobante));
            TextReader reader = new StreamReader(archivo);
            try
            {
                if (!File.Exists(archivo))
                {
                    MessageBox.Show("El CFDI " + archivo + " no existe");
                    return null;
                }
                //Abrir el archivo y copiar los sellos
                XmlDocument doc = new XmlDocument();
                doc.Load(archivo);

                XmlNodeList TimbreFiscalDigital = doc.GetElementsByTagName("tfd:TimbreFiscalDigital");
                XmlNodeList receptor = doc.GetElementsByTagName("cfdi:Receptor");
                XmlNodeList emisor = doc.GetElementsByTagName("cfdi:Emisor");
                XmlNodeList comprobante = doc.GetElementsByTagName("cfdi:Comprobante");



                string Sello = "";

                try
                {
                    Sello = comprobante[0].Attributes["Sello"].Value;
                }
                catch
                {

                }

                string rfcReceptor = "";

                try
                {
                    rfcReceptor = receptor[0].Attributes["Rfc"].Value;
                }
                catch
                {

                }

                string rfcEmisor = "";

                try
                {
                    rfcEmisor = emisor[0].Attributes["Rfc"].Value;
                }
                catch
                {

                }

                string total = "";

                try
                {
                    total = comprobante[0].Attributes["Total"].Value;
                }
                catch
                {

                }

                string UUID = "";

                try
                {
                    UUID = TimbreFiscalDigital[0].Attributes["UUID"].Value;
                }
                catch
                {

                }
                string FechaTimbrado = "";

                try
                {
                    FechaTimbrado = TimbreFiscalDigital[0].Attributes["FechaTimbrado"].Value;
                }
                catch
                {

                }

                string noCertificadoSAT = "";
                try
                {
                    noCertificadoSAT = TimbreFiscalDigital[0].Attributes["noCertificadoSAT"].Value;
                }
                catch
                {

                }


                try
                {
                    noCertificadoSAT = TimbreFiscalDigital[0].Attributes["NoCertificadoSAT"].Value;
                }
                catch
                {

                }

                string selloSAT = "";
                try
                {
                    selloSAT = TimbreFiscalDigital[0].Attributes["selloSAT"].Value;
                }
                catch
                {

                }

                try
                {
                    selloSAT = TimbreFiscalDigital[0].Attributes["SelloSAT"].Value;
                }
                catch
                {

                }

                string USO_CFDI = "";

                try
                {
                    USO_CFDI = receptor[0].Attributes["UsoCFDI"].Value;
                }
                catch
                {

                }

                try
                {
                    USO_CFDI = receptor[0].Attributes["UsoCFDI"].Value;
                }
                catch
                {

                }


                string VERSION = "";

                try
                {
                    VERSION = comprobante[0].Attributes["version"].Value;
                }
                catch
                {

                }

                try
                {
                    VERSION = comprobante[0].Attributes["Version"].Value;
                }
                catch
                {

                }

                string metodo_pago = "";

                try
                {
                    metodo_pago = comprobante[0].Attributes["MetodoPago"].Value;
                }
                catch
                {

                }

                try
                {
                    metodo_pago = comprobante[0].Attributes["MetodoPago"].Value;
                }
                catch
                {

                }
                string banco = "";
                try
                {
                    banco = comprobante[0].Attributes["NumCtaPago"].Value;
                }
                catch
                {

                }

                try
                {
                    banco = comprobante[0].Attributes["NumCtaPago"].Value;
                }
                catch
                {

                }


                string FormaPago = "";

                try
                {
                    FormaPago = comprobante[0].Attributes["FormaPago"].Value;
                }
                catch
                {

                }

                try
                {
                    FormaPago = comprobante[0].Attributes["formaPago"].Value;
                }
                catch
                {

                }
                string NoCertificado = "";
                try
                {
                    NoCertificado = comprobante[0].Attributes["NoCertificado"].Value;
                }
                catch
                {

                }
                //Abrir Archivo TXT
                string cadenaoriginal_TFD_1_0 = "";
                //Crear Archivo Temporal
                if (TimbreFiscalDigital != null)
                {
                    string sDirectory = AppDomain.CurrentDomain.BaseDirectory;
                    //Generar cadenaoriginal_TFD_1_0.xslt
                    string nombre_tmp = "TMP" + DateTime.Now.ToString("ddMMyyyyhhmmss");
                    //XslTransform myXslTrans = new XslTransform();
                    XslCompiledTransform myXslTrans = new XslCompiledTransform();


                    XslCompiledTransform trans = new XslCompiledTransform();
                    XmlUrlResolver resolver = new XmlUrlResolver();

                    resolver.Credentials = System.Net.CredentialCache.DefaultCredentials;
                    XsltSettings settings = new XsltSettings(true, true);

                    //Cargar xslt
                    try
                    {

                        myXslTrans.Load(sDirectory + "/XSLT/cadenaoriginal_TFD_1_1.xslt");
                    }
                    catch (XmlException errores)
                    {
                        MessageBox.Show("Error en " + errores.InnerException.ToString());
                        return null;
                    }
                    XmlTextWriter myWriter = new XmlTextWriter(nombre_tmp + ".TXT", null);

                    //Transformar al XML
                    myXslTrans.Transform(TimbreFiscalDigital[0], null, myWriter);

                    myWriter.Close();


                    StreamReader re = File.OpenText(nombre_tmp + ".TXT");
                    string input = null;
                    while ((input = re.ReadLine()) != null)
                    {
                        cadenaoriginal_TFD_1_0 += input;
                    }
                    re.Close();
                    re.Dispose();
                    //Eliminar Archivos Temporales
                    File.Delete(nombre_tmp + ".TXT");
                }

                QR_Code = "https://verificacfdi.facturaelectronica.sat.gob.mx/default.aspx"
                    + "?id=" + UUID
                    + "&re=" + rfcEmisor
                    + "&rr=" + rfcReceptor
                    + "&tt=" + total
                    + "&fe=" + Sello.Substring(Sello.Length - 8, 8);

                //XmlDocument doc = new XmlDocument();
                //doc.Load(archivo);


                //XmlNodeList TimbreFiscalDigital = doc.GetElementsByTagName("tfd:TimbreFiscalDigital");

                //string UUID = "";
                //UUID = TimbreFiscalDigital[0].Attributes["UUID"].Value;

                //oComprobanteLocal = (Comprobante)serializer_CFD_3.Deserialize(reader);

                //string QR_Code = "https://verificacfdi.facturaelectronica.sat.gob.mx/default.aspx&";
                //QR_Code = "https://verificacfdi.facturaelectronica.sat.gob.mx/default.aspx"
                //    + "?id=" + UUID
                //    + "&re=" + rfcEmisor
                //    + "&rr=" + rfcReceptor
                //    + "&tt=" + total
                //    + "&fe=" + Sello.Substring(Sello.Length - 8, 8);

                //QR_Code += "?re=" + oComprobanteLocal.Emisor.Rfc;
                //QR_Code += "&rr=" + oComprobanteLocal.Receptor.Rfc;
                //QR_Code += "&tt=" + oComprobanteLocal.Total.ToString();
                //QR_Code += "&id=" + UUID;

            }
            catch (Exception xmlex)
            {
                Bitacora.Log("Errpr :: " + xmlex.ToString());
                return null;
            }
            int size = 320;
            QRCodeWriter writer = new QRCodeWriter();
            com.google.zxing.common.ByteMatrix matrix;
            matrix = writer.encode(QR_Code, com.google.zxing.BarcodeFormat.QR_CODE, size, size, null);
            Bitmap img = new Bitmap(size, size);
            Color Color = Color.FromArgb(0, 0, 0);

            for (int y = 0; y < matrix.Height; ++y)
            {
                for (int x = 0; x < matrix.Width; ++x)
                {
                    Color pixelColor = img.GetPixel(x, y);

                    //Find the colour of the dot
                    if (matrix.get_Renamed(x, y) == -1)
                    {
                        img.SetPixel(x, y, Color.White);
                    }
                    else
                    {
                        img.SetPixel(x, y, Color.Black);
                    }
                }
            }


            string nombre = Path.GetTempPath() + "QRP_TEMP.BMP";
            img.Save(nombre);

            FileStream fs = new FileStream(nombre, System.IO.FileMode.Open, System.IO.FileAccess.Read);
            byte[] Image = new byte[fs.Length];
            fs.Read(Image, 0, Convert.ToInt32(fs.Length));
            int image_32 = Convert.ToInt32(fs.Length);
            fs.Close();
            File.Delete(nombre);

            return Image;

        }

        #endregion
    }
}
