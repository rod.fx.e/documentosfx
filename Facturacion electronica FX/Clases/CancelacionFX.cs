﻿using ModeloCancelarFactura;
using System;
using System.Collections.Generic;
using System.Data.Entity.Validation;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace FE_FX.Clases
{
    class CancelacionFX
    {
        public bool GuardarAcuse(cFACTURA ocFACTURA, Encapsular oEncapsular, EDICOM_Servicio.CancelData respuesta)
        {
            try
            {
                ModeloCancelarFactura.ModeloCancelarFacturaContainer dbContextCancelada = new ModeloCancelarFactura.ModeloCancelarFacturaContainer();
                FacturaCancelada oFacturaCancelada = new FacturaCancelada();
                oFacturaCancelada.factura = ocFACTURA.INVOICE_ID;
                oFacturaCancelada.rfcE = oEncapsular.ocEMPRESA.RFC;
                oFacturaCancelada.status = respuesta.status;
                oFacturaCancelada.statusCode = respuesta.statusCode;
                oFacturaCancelada.uuid = respuesta.uuid;
                oFacturaCancelada.ack = respuesta.ack;
                oFacturaCancelada.fechaCreacion = DateTime.Now;
                dbContextCancelada.FacturaCanceladaSet.Add(oFacturaCancelada);
                dbContextCancelada.SaveChanges();
                dbContextCancelada.Dispose();

                return true;
            }
            catch (DbEntityValidationException error)
            {
                foreach (var eve in error.EntityValidationErrors)
                {
                    foreach (var ve in eve.ValidationErrors)
                    {
                        MessageBox.Show(ve.PropertyName + " Error: " + ve.ErrorMessage, Application.ProductName
                            , MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
                return false;
            }
            catch (Exception oException)
            {
                MessageBox.Show(oException.Message, Application.ProductName + "-" + Application.ProductVersion, MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
        }
    }
}
