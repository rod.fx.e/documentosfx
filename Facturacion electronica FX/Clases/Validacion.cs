﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FE_FX.Clases
{
    class Validacion
    {
        public void FacturasPendientesComplementoPago()
        {
            string Sql = @"
            select p.checkId, p.customerId, p.uuid, p.xml, p.pdf, p.estado
            , pl.factura, pl.UUID as FolioFiscalFactura
            from  pagoLineaSet pl 
            inner join pagoSet p on p.Id=pl.pago_Id
            WHERE 1=1
            AND p.uuid is null
            AND YEAR(r.INVOICE_DATE) IN (" + GetLastTwoYears() + @")
            ";

            ModeloComprobantePago.documentosfxEntities Db = new ModeloComprobantePago.documentosfxEntities();
            

        }

        private string GetLastTwoYears()
        {
            int currentYear = DateTime.Now.Year;
            string lastTwoYears = (currentYear - 1).ToString() + "," + currentYear.ToString();
            return lastTwoYears;
        }
    }
}
