﻿using Generales;
using Ionic.Zip;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Xsl;

namespace FE_FX.Clases
{
    class timbrado
    {
        public string VMX_FE_Tabla = "VMX_FE";

        public byte[] ReadBinaryFile(string fileName)
        {
            if (File.Exists(fileName))
            {
                try
                {
                    ///Open and read a file&#12290;
                    FileStream fileStream = File.OpenRead(fileName);
                    byte[] archivo = ConvertStreamToByteBuffer(fileStream);
                    fileStream.Close();
                    return archivo;
                }
                catch (Exception ex)
                {
                    return new byte[0];
                }
            }
            else
            {
                return new byte[0];
            }
        }

        public byte[] ConvertStreamToByteBuffer(System.IO.Stream theStream)
        {
            int b1;
            System.IO.MemoryStream tempStream = new System.IO.MemoryStream();
            while ((b1 = theStream.ReadByte()) != -1)
            {
                tempStream.WriteByte(((byte)b1));
            }
            return tempStream.ToArray();
        }

        public string CreateMD5(string input)
        {
            // Use input string to calculate MD5 hash
            using (System.Security.Cryptography.MD5 md5 = System.Security.Cryptography.MD5.Create())
            {
                byte[] inputBytes = System.Text.Encoding.ASCII.GetBytes(input);
                byte[] hashBytes = md5.ComputeHash(inputBytes);

                // Convert the byte array to hexadecimal string
                StringBuilder sb = new StringBuilder();
                for (int i = 0; i < hashBytes.Length; i++)
                {
                    sb.Append(hashBytes[i].ToString("X2"));
                }
                return sb.ToString().ToLower();
            }
        }

        private bool STOguardar_archivo(byte[] PROCESO, cFACTURA ocFACTURA
            , cFACTURA_BANDEJA ocFACTURA_BANDEJA, cCONEXCION oData_ERP, cEMPRESA ocEMPRESA
            , cCERTIFICADO ocCERTIFICADO)
        {
            //Guardar los PROCESO

            File.WriteAllBytes("TEMPORAL.ZIP", PROCESO);
            //Descomprimir
            String TargetDirectory = "TEMPORAL";
            using (Ionic.Zip.ZipFile zip = Ionic.Zip.ZipFile.Read("TEMPORAL.ZIP"))
            {
                zip.ExtractAll(TargetDirectory, Ionic.Zip.ExtractExistingFileAction.OverwriteSilently);
            }

            //Copiar el ARCHIVO Descrompimido por otro
            string ARCHIVO_descomprimido = TargetDirectory + @"\" + ocFACTURA.INVOICE_ID + ".XML";
            //Validar que sea un archivo valido CFDI si no es mensaje de error
            XmlDocument doctmp = new XmlDocument();
            try
            {
                doctmp.Load(ARCHIVO_descomprimido);
                XmlNodeList TimbreFiscalDigitalTMP = doctmp.GetElementsByTagName("tfd:TimbreFiscalDigital");
                string UUIDtmp = TimbreFiscalDigitalTMP[0].Attributes["UUID"].Value;
                //Validar que no sea el error para enviar a EDICOM
                if (UUIDtmp == "")
                {
                    return false;
                }
            }
            catch
            {
                File.Delete(ARCHIVO_descomprimido);
                return false;
            }



            File.Copy(ARCHIVO_descomprimido, ocFACTURA_BANDEJA.XML, true);

            //Abrir el archivo y copiar los sellos
            XmlDocument doc = new XmlDocument();
            doc.Load(ocFACTURA_BANDEJA.XML);
            if (ocFACTURA.INVOICE_ID != "")
            {
                //cFACTURA_BANDEJA ocFACTURA_BANDEJA = new cFACTURA_BANDEJA(oData_ERP, ocEMPRESA);
                //ocFACTURA_BANDEJA.cargar_ID(ocFACTURA.INVOICE_ID, ocEMPRESA);
                doc.Save(ocFACTURA_BANDEJA.XML);
            }
            XmlNodeList TimbreFiscalDigital = doc.GetElementsByTagName("tfd:TimbreFiscalDigital");

            string UUID = "";
            UUID = TimbreFiscalDigital[0].Attributes["UUID"].Value;
            //Validar que no sea el error para enviar a EDICOM
            if (UUID == "")
            {
                return false;
            }

            string FechaTimbrado = "";
            FechaTimbrado = TimbreFiscalDigital[0].Attributes["FechaTimbrado"].Value;
            string noCertificadoSAT = "";
            noCertificadoSAT = TimbreFiscalDigital[0].Attributes["noCertificadoSAT"].Value;
            string selloSAT = "";
            selloSAT = TimbreFiscalDigital[0].Attributes["selloSAT"].Value;
            string sDirectory = AppDomain.CurrentDomain.BaseDirectory;
            //Generar cadenaoriginal_TFD_1_0.xslt
            string nombre_tmp = ocFACTURA.INVOICE_ID + "TMP" + DateTime.Now.ToString("ddMMyyyyhhmmss");
            //XslTransform myXslTrans = new XslTransform();
            XslCompiledTransform myXslTrans = new XslCompiledTransform();


            XslCompiledTransform trans = new XslCompiledTransform();
            XmlUrlResolver resolver = new XmlUrlResolver();

            resolver.Credentials = System.Net.CredentialCache.DefaultCredentials;
            XsltSettings settings = new XsltSettings(true, true);

            //Cargar xslt
            try
            {
                myXslTrans.Load(sDirectory + "/XSLT/cadenaoriginal_TFD_1_1.xslt");
            }
            catch (XmlException errores)
            {
                ErrorFX.mostrar(errores, true, false, "timbrado - 155 - ",true);
                return false;
            }

            //Crear Archivo Temporal

            XmlTextWriter myWriter = new XmlTextWriter(nombre_tmp + ".TXT", null);

            //Transformar al XML
            myXslTrans.Transform(TimbreFiscalDigital[0], null, myWriter);

            myWriter.Close();

            //Abrir Archivo TXT
            string cadenaoriginal_TFD_1_0 = "";
            StreamReader re = File.OpenText(nombre_tmp + ".TXT");
            string input = null;
            while ((input = re.ReadLine()) != null)
            {
                cadenaoriginal_TFD_1_0 += input;
            }
            re.Close();

            //Eliminar Archivos Temporales
            File.Delete(nombre_tmp + ".TXT");

            string QR_Code = "";


            QR_Code = "?re=" + ocEMPRESA.RFC
                + "&rr=" + ocFACTURA.VAT_REGISTRATION
                + "&tt=" + (Math.Abs((ocFACTURA.TOTAL_AMOUNT + ocFACTURA.TOTAL_VAT_AMOUNT) - Math.Abs(ocFACTURA.TOTAL_RETENIDO))).ToString()
                + "&id=" + UUID;

            string BD_Auxiliar = ocEMPRESA.BD_AUXILIAR + ".dbo.";
            if (ocEMPRESA.BD_AUXILIAR == "")
            {
                BD_Auxiliar = "";
            }
            string sSQL = " UPDATE " + BD_Auxiliar + VMX_FE_Tabla + " ";
            sSQL += " SET UUID='" + UUID + "',FechaTimbrado='" + FechaTimbrado + "'";
            sSQL += ",noCertificadoSAT='" + noCertificadoSAT + "',selloSAT='" + selloSAT + "'";
            sSQL += ",Cadena_TFD='" + cadenaoriginal_TFD_1_0 + "',QR_Code='" + QR_Code + "',noCertificado='" + ocCERTIFICADO.NO_CERTIFICADO + "' ";
            sSQL += " WHERE INVOICE_ID='" + ocFACTURA.INVOICE_ID + "' ";
            //sSQL = actualizar_invoice_id(sSQL);
            oData_ERP.EjecutarConsulta(sSQL);

            File.Delete(ARCHIVO_descomprimido);
            File.Delete("TEMPORAL.ZIP");
            return true;
        }

        public bool timbrar(Encapsular oEncapsular, cFACTURA ocFACTURA, cSERIE ocSERIE, bool prueba)
        {
            try
            {
                bool resultado = false;
                cFACTURA_BANDEJA ocFACTURA_BANDEJA = new cFACTURA_BANDEJA(oEncapsular.oData_ERP, oEncapsular.ocEMPRESA);
                ocFACTURA_BANDEJA.cargar_ID(ocFACTURA.INVOICE_ID, oEncapsular.ocEMPRESA);

                //Cargar Certificado
                cCERTIFICADO ocCERTIFICADO = new cCERTIFICADO(); //cCERTIFICADO(oEncapsular.oData_ERP);
                ocCERTIFICADO.cargar(ocSERIE.ROW_ID_CERTIFICADO);

                //ocFACTURA = new cFACTURA(oEncapsular.oData_ERP);
                //arrSelectedRows[n].Cells["ESTADO"].Value = "Obtener datos de la Factura";
                //ocFACTURA.datos(INVOICE_ID, ocSERIE.ACCOUNT_ID_RETENIDO, ADDR_NO, oEncapsular.ocEMPRESA, ocSERIE.ACCOUNT_ID_ANTICIPO);


                if (Globales.oCFDI_USUARIO.USUARIO != "")
                {
                    //ocFACTURA_general = new cFACTURA(oEncapsular.oData_ERP);
                    //ocFACTURA_general = ocFACTURA;
                    bool timbrarEDICOM = false;
                    //Tiene creado el usuario STOUsuario si es asi timbrar por STO si da error ir a EDICOM
                    try
                    {
                        string STOUsuario = ConfigurationManager.AppSettings["STOUsuario"].ToString();
                        string STOPassword = ConfigurationManager.AppSettings["STOPassword"].ToString();
                        string STOUsuarioPrueba = ConfigurationManager.AppSettings["STOUsuarioPrueba"].ToString();
                        string STOPasswordPrueba = ConfigurationManager.AppSettings["STOPasswordPrueba"].ToString();
                        //Tibrar con STO
                        try
                        {
                            if (prueba)
                            {
                                resultado = STOgenerar_CFDI(2, STOUsuarioPrueba, STOPasswordPrueba, oEncapsular.oData_ERP, oEncapsular.ocEMPRESA
                                    , ocFACTURA, ocFACTURA_BANDEJA, ocCERTIFICADO);
                            }
                            else
                            {
                                resultado = STOgenerar_CFDI(1, STOUsuario, STOPassword, oEncapsular.oData_ERP, oEncapsular.ocEMPRESA
                                    , ocFACTURA, ocFACTURA_BANDEJA, ocCERTIFICADO);
                            }
                        }
                        catch (Exception ex)
                        {
                            ErrorFX.mostrar("Modulo Timbrado: Proveedor STO " + Environment.NewLine +
                                ex.Message.ToString(), false, true,true);
                            timbrarEDICOM = true;

                        }
                    }
                    catch
                    {
                        timbrarEDICOM = true;
                    }

                    if (timbrarEDICOM)
                    {

                        //Tibrar con Edicom
                        try
                        {
                            if (prueba)
                            {
                                // resultado = generar_CFDI(2, ocCFDI_USUARIO.USUARIO, ocCFDI_USUARIO.PASSWORD, oEncapsular.oData_ERP, oEncapsular.ocEMPRESA
                                //    , ocFACTURA, ocFACTURA_BANDEJA, ocCERTIFICADO);
                            }
                            else
                            {
                                //resultado = generar_CFDI(1, ocCFDI_USUARIO.USUARIO, ocCFDI_USUARIO.PASSWORD, oEncapsular.oData_ERP, oEncapsular.ocEMPRESA
                                //    , ocFACTURA, ocFACTURA_BANDEJA, ocCERTIFICADO);
                            }
                        }
                        catch (Exception ex)
                        {
                            ErrorFX.mostrar("Modulo Timbrado: Proveedor EDICOM " + Environment.NewLine +
                            ex.Message.ToString(), false, true,true);
                        }
                    }
                }
                return resultado;

            }
            catch (Exception e)
            {
                ErrorFX.mostrar(e, true, true, "timbrado - 294 - ", true);
                return false;
            }
        }

        private bool STOgenerar_CFDI(int Opcion, string usuario, string password, cCONEXCION oData_ERP, cEMPRESA ocEMPRESA
            , cFACTURA ocFACTURA, cFACTURA_BANDEJA ocFACTURA_BANDEJA, cCERTIFICADO ocCERTIFICADO)
        {
            try
            {

                if (!File.Exists(ocFACTURA_BANDEJA.XML))
                {
                    return false;
                }
                //Comprimir Archivo

                string Archivo_Tmp = ocFACTURA_BANDEJA.INVOICE_ID + ".xml";

                File.Copy(ocFACTURA_BANDEJA.XML, Archivo_Tmp, true);

                string ARCHIVO_zip = Archivo_Tmp.ToUpper().Replace("XML", "ZIP");


                using (ZipFile zip = new ZipFile())
                {
                    zip.AddFile(Archivo_Tmp);
                    zip.Save(ARCHIVO_zip);
                }
                //Fin de Compresion de Archivo
                File.Delete(Archivo_Tmp);

                byte[] ARCHIVO_Leido = ReadBinaryFile(ARCHIVO_zip);
                File.Delete(ARCHIVO_zip);

                byte[] PROCESO;
                string MENSAJE = "";

                MENSAJE = "";
                switch (Opcion)
                {
                    case 1:
                        try
                        {
                            STOProduccion.TimbradoClient cliente = new STOProduccion.TimbradoClient();
                            STOProduccion.timbrarCfdiRequest otimbrarCfdiRequest = new STOProduccion.timbrarCfdiRequest();

                            PROCESO = cliente.timbrarCfdi(usuario, CreateMD5(password), ARCHIVO_Leido);


                            if (!STOguardar_archivo(PROCESO, ocFACTURA, ocFACTURA_BANDEJA, oData_ERP, ocEMPRESA
                                , ocCERTIFICADO))
                            {
                                return false;
                            }
                        }
                        catch (Exception pe)
                        {
                            ErrorFX.mostrar("Error STO Modo Real: " + pe.Message.ToString(), false, true,true);
                            return false;
                        }
                        break;
                    case 2:
                        try
                        {
                            //STOPrueba.MassiveReceptionControllerImplClient cliente = new STOPrueba.MassiveReceptionControllerImplClient();
                            //PROCESO = cliente.StampCFD(ARCHIVO_Leido, usuario, CreateMD5(password));
                            STOPrueba.TimbradoClient cliente = new STOPrueba.TimbradoClient();
                            STOPrueba.timbrarCfdiRequest otimbrarCfdiRequest = new STOPrueba.timbrarCfdiRequest();

                            PROCESO = cliente.timbrarCfdi(usuario, CreateMD5(password), ARCHIVO_Leido);
                            MENSAJE = "PRUEBA";
                            if (!STOguardar_archivo(PROCESO, ocFACTURA, ocFACTURA_BANDEJA, oData_ERP, ocEMPRESA
                                , ocCERTIFICADO))
                            {
                                return false;
                            }
                        }
                        catch (Exception pe)
                        {
                            ErrorFX.mostrar("Error STO Modo Prueba: " + pe.Message.ToString(), false, true,true);
                            return false;
                        }
                        break;
                }

                if (MENSAJE.Trim() == "")
                {
                    ocFACTURA_BANDEJA.ESTADO = "Sellada STO " + DateTime.Now.ToShortDateString() + " " + DateTime.Now.ToLongTimeString();
                }
                else
                {
                    ocFACTURA_BANDEJA.ESTADO = "Test STO " + DateTime.Now.ToShortDateString() + " " + DateTime.Now.ToLongTimeString();
                }
                ocFACTURA_BANDEJA.guardar();
                return true;
            }
            catch (Exception oException)
            {
                string mensaje = oException.Message.ToString();
                if (oException.InnerException != null)
                {
                    mensaje += Environment.NewLine + "Inner: " + oException.InnerException.Message;
                }
                mensaje += Environment.NewLine + "Clase: Timbrado";

                ErrorFX.mostrar(mensaje, true, true, true);
                return false;
            }
        }


    }
}
