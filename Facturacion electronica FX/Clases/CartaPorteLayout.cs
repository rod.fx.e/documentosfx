﻿using OfficeOpenXml;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace FE_FX.Clases
{
    public enum TipoCartaPorte
    {
        Embarque,
        OC
    }
    class CartaPorteLayout
    {

        public bool Generar(TipoCartaPorte tipoCartaPorte, List<string> facturas)
        {
            try
            {
                string pathPlantilla = AppDomain.CurrentDomain.BaseDirectory + "LayoutCartaPorte.xlsx";
                var tempfilePlantilla = new FileInfo(pathPlantilla);
                if (!tempfilePlantilla.Exists)
                {
                    MessageBox.Show("No existe la plantilla", Application.ProductName
                    , MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return false;
                }
                string fileName = Guid.NewGuid().ToString() + ".xlsx";
                var path = Path.Combine(System.IO.Path.GetTempPath(), fileName);
                File.Copy(pathPlantilla, path);


                FileInfo tempfile = new FileInfo(path);
                if (!tempfile.Exists)
                {
                    MessageBox.Show("No existe el directorio " + path, Application.ProductName
                    , MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return false;
                }

                //Save the file
                using (var pck = new ExcelPackage(tempfile))
                {
                    var ws = pck.Workbook.Worksheets[1];
                    ws.Cells.Style.Font.Name = "Arial";
                    ws.Cells.Style.Font.Size = 10;
                    //Editar la cabecera
                    string dia = DateTime.Now.Day.ToString().PadLeft(2, '0');
                    ws.Cells["F3"].Value = dia + DateTime.Now.ToString("MMyyyy");
                    IngresoImportacionLineaExportacion O = new IngresoImportacionLineaExportacion();
                    ws.Cells["A2"].LoadFromCollection(O.Obtener(tipoCartaPorte, facturas));
                    ws.Cells.Style.Font.Name = "Arial";
                    ws.Cells.Style.Font.Size = 10;

                    pck.Save();
                }
                DialogResult Resultado = MessageBox.Show("Exportación del archivo " + Environment.NewLine + fileName + " finalizada." + Environment.NewLine + "¿Desea abrirlo?", Application.ProductName + "-" + Application.ProductVersion, MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                if (Resultado.ToString() != "Yes")
                {
                    return true;
                }

                if (tempfile.Exists)
                {
                    System.Diagnostics.Process.Start(path);
                }
                return true;
            }
            catch (Exception error)
            {
                string mensaje = "Carta Porte 60 ";
                mensaje = error.Message.ToString();
                if (error.InnerException != null)
                {
                    mensaje += " " + error.InnerException.Message.ToString();
                }
                throw new Exception(mensaje);
            }
        }

    }
}
