﻿using CFDI33;
using ClasificadorProductos.formularios;
using CryptoSysPKI;
using Generales;
using ModeloComprobantePago;
using ModeloDocumentosFX;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml;
using System.Xml.Serialization;
using System.Xml.XPath;
using System.Xml.Xsl;
//https://www.tektutorialshub.com/crystal-reports/how-to-download-and-install-crystal-report-runtime/
namespace FE_FX.Clases
{
    class generar33
    {
        public bool generar(cFACTURA oFACTURA, cSERIE ocSERIE, Encapsular oEncapsular, cCERTIFICADO ocCERTIFICADO, bool complementoComercioExterior = false
            , bool sinRelacion = false, bool sinCEE = false)
        {
            if (FacturaTimbradaReal(oFACTURA.INVOICE_ID, oEncapsular))
            {
                ErrorFX.mostrar("Generar 33 -La factura ya se encuentra timbrada", true, true,false);
                return false;
            }
            //Leyenda Fiscal
            bool leyendaFiscal = false;

            ModeloDocumentosFXContainer dbContext = new ModeloDocumentosFXContainer();
            string conection = dbContext.Database.Connection.ConnectionString;
            cCONEXCION oData = new cCONEXCION(conection);

            CFDI32.t_Ubicacion ot_UbicacionReceptor = new CFDI32.t_Ubicacion();

            CFDI33.Comprobante oComprobante = new CFDI33.Comprobante();
            XmlSerializerNamespaces myNamespaces = new XmlSerializerNamespaces();
            myNamespaces.Add("cfdi", "http://www.sat.gob.mx/cfd/3");
            DateTime FECHA_FACTURA = new DateTime();
            try
            {
                cFACTURA_BANDEJA ocFACTURA_BANDEJAtmp = new cFACTURA_BANDEJA(oData);
                ocFACTURA_BANDEJAtmp.cargar_ID(oFACTURA.INVOICE_ID, oEncapsular.ocEMPRESA);

                try
                {
                    try
                    {
                        //Extraer el SERIE de INVOICE_ID
                        string FOLIO = ExtractNumbers(oFACTURA.INVOICE_ID);
                        //Serie
                        oComprobante.Serie = ocSERIE.ID;
                        //Folio
                        oComprobante.Folio = FOLIO;
                        //MessageBox.Show("Comprobante3");

                        oComprobante.Moneda = "USD";// c_Moneda.USD;

                        if (oFACTURA.CURRENCY_ID.Contains("DLLS") |
                            oFACTURA.CURRENCY_ID.Contains("USD")
                            )
                        {
                            oComprobante.Moneda = "USD";//c_Moneda.USD;
                        }
                        if (oFACTURA.CURRENCY_ID.Contains("MXP")
                            |
                            oFACTURA.CURRENCY_ID.Contains("PESOS")
                            |
                            oFACTURA.CURRENCY_ID.Contains("MXN")
                            |
                            oFACTURA.CURRENCY_ID.Contains("MN")
                            )
                        {
                            oComprobante.Moneda = "MXN";//c_Moneda.MXN;
                        }

                        if (oComprobante.Moneda != "MXN")
                        {
                            oComprobante.TipoCambio = otroTruncar(oFACTURA.SELL_RATE, 4);
                            oComprobante.TipoCambioSpecified = true;

                        }

                        oComprobante.CondicionesDePago = oFACTURA.TERMS_NET_DAYS;
                        if ((oComprobante.CondicionesDePago == "")
                            ||
                            (oComprobante.CondicionesDePago == null))
                        {
                            oComprobante.CondicionesDePago = "PAGO EN UNA SOLA EXHIBICION";
                        }
                        else
                        {
                            if (oFACTURA.TERMS_NET_DAYS != "")
                            {
                                oComprobante.CondicionesDePago = oFACTURA.TERMS_NET_DAYS;
                            }
                        }
                        //CDFI 3.3
                        //CFDI33.c_CodigoPostal oc_CodigoPostal = (CFDI33.c_CodigoPostal)System.Enum.Parse(typeof(CFDI33.c_CodigoPostal), "Item" + ocSERIE.CP);

                        oComprobante.LugarExpedicion = ocSERIE.CP;// oc_CodigoPostal;


                        //MessageBox.Show("Comprobante4");
                        IFormatProvider culture = new CultureInfo("es-MX", true);
                        DateTime INVOICE_DATE = oFACTURA.traer_INVOICE_DATE(oFACTURA.INVOICE_ID); //oFACTURA.INVOICE_DATE;
                        DateTime CREATE_DATE = oFACTURA.traer_CREATE_DATE(oFACTURA.INVOICE_ID); //oFACTURA.CREATE_DATE;

                        //MessageBox.Show("Comprobante5");
                        try
                        {
                            //FECHA_FACTURA = DateTime.Parse(INVOICE_DATE.ToString("dd/MM/yyyy") + " " + CREATE_DATE.ToString("HH:mm:ss"));
                            //INVOICE_DATE = DateTime.Parse(INVOICE_DATE.Day.ToString() + "/" + INVOICE_DATE.Month.ToString() + "/" + INVOICE_DATE.Year.ToString() + " " + CREATE_DATE.ToString("HH:mm:ss"), culture);
                            CREATE_DATE = DateTime.Now;
                            INVOICE_DATE = DateTime.Parse(INVOICE_DATE.Day.ToString() + "/" + INVOICE_DATE.Month.ToString() + "/" + INVOICE_DATE.Year.ToString() + " " + CREATE_DATE.ToString("HH:mm:ss"));
                        }
                        catch
                        {
                            try
                            {
                                CREATE_DATE = DateTime.Now;
                                FECHA_FACTURA = DateTime.Parse(INVOICE_DATE.Day.ToString() + "/" + INVOICE_DATE.Month.ToString() + "/" + INVOICE_DATE.Year.ToString() + " " + CREATE_DATE.ToString("HH:mm:ss"));
                            }
                            catch
                            {

                            }
                        }
                        //Verificar si se le baja la fecha INVOICE_DATE.AddHours(-1);
                        int menos_hora = 0;
                        try
                        {
                            menos_hora = int.Parse(ConfigurationManager.AppSettings["menos_hora"].ToString());
                            INVOICE_DATE = DateTime.Now;
                            INVOICE_DATE = INVOICE_DATE.AddHours(menos_hora);
                            INVOICE_DATE = new DateTime(INVOICE_DATE.Year, INVOICE_DATE.Month, INVOICE_DATE.Day, 0, 0, 0);
                        }
                        catch { }
                        //MessageBox.Show("Comprobante6");
                        oComprobante.Fecha = INVOICE_DATE;//INVOICE_DATE.AddHours(menos_hora); ;//FECHA_FACTURA;




                        //Aprobacion
                        //Tipo de Comprobante
                        switch (oFACTURA.TYPE)
                        {
                            case "I":
                                oComprobante.TipoDeComprobante = "I";//c_TipoDeComprobante.I;
                                break;
                            case "M":
                                oComprobante.TipoDeComprobante = "E";//c_TipoDeComprobante.E;
                                oComprobante.CondicionesDePago = "Contado";
                                break;
                            default:
                                oComprobante.TipoDeComprobante = "I";//c_TipoDeComprobante.E;
                                oComprobante.CondicionesDePago = "Contado";
                                break;
                        }


                        //MessageBox.Show("Comprobante7");
                        //Forma de Pago
                        //Cambio de Rodrigo Escalona Forma de Pago
                        //oComprobante.FormaPago = "99";// c_FormaPago.Item01;
                        //oComprobante.FormaPagoSpecified = true;
                        //if (oComprobante.TipoDeComprobante.ToUpper().Equals("E"))//c_TipoDeComprobante.E)

                        if (!String.IsNullOrEmpty(ocFACTURA_BANDEJAtmp.METODO_DE_PAGO))
                        {
                            oComprobante.MetodoPago = ocFACTURA_BANDEJAtmp.METODO_DE_PAGO;
                        }
                        else
                        {
                            oComprobante.MetodoPago = "PPD";
                        }
                        if (!String.IsNullOrEmpty(ocFACTURA_BANDEJAtmp.FORMA_DE_PAGO))
                        {
                            oComprobante.FormaPago = ocFACTURA_BANDEJAtmp.FORMA_DE_PAGO;
                        }
                        else
                        {
                            oComprobante.FormaPago = "99";
                        }

                        //{
                        //oComprobante.MetodoPago = "PUE";
                        //    oComprobante.FormaPago = "99";//Cargar los tipos de datos relacionados
                        ComprobanteCfdiRelacionados oComprobanteCfdiRelacionados = null;
                        //if (oComprobante.TipoDeComprobante.Contains("E"))
                        //{
                        oComprobanteCfdiRelacionados = new CFDI33.ComprobanteCfdiRelacionados();
                        oComprobanteCfdiRelacionados = cargarComprobanteCfdiRelacionados(oFACTURA.INVOICE_ID, oEncapsular
                            , oFACTURA);
                        //}

                        //if (oComprobanteCfdiRelacionados == null)
                        //{
                        //    ErrorFX.mostrar("La Nota de crédito " + oFACTURA.INVOICE_ID + " debe tener CFDI relacionados."
                        //        + Environment.NewLine + "generar33 - 169"
                        //        , true, true, true);
                        //    return false;
                        //}
                        if (!sinRelacion)
                        {
                            if (oComprobanteCfdiRelacionados != null)
                            {
                                oComprobante.CfdiRelacionados = new CFDI33.ComprobanteCfdiRelacionados();
                                oComprobante.CfdiRelacionados = oComprobanteCfdiRelacionados;
                            }
                            else
                            {
                                //FIMA 
                                //Validar que no sea OPERADORA
                                if (oFACTURA.CUSTOMER_ID != "305")
                                {
                                    //if (oComprobante.TipoDeComprobante.Equals("E"))
                                    //{
                                    //    ErrorFX.mostrar("La Nota de crédito " + oFACTURA.INVOICE_ID + " debe tener CFDI relacionados."
                                    //    + Environment.NewLine + "generar33 - 169"
                                    //    , true, true, true);
                                    //    return false;
                                    //}
                                }

                            }
                        }



                        //}
                        //else
                        //{
                        //    if (!String.IsNullOrEmpty(ocFACTURA_BANDEJAtmp.METODO_DE_PAGO))
                        //    {
                        //        oComprobante.MetodoPago = ocFACTURA_BANDEJAtmp.METODO_DE_PAGO;
                        //    }
                        //    else
                        //    {
                        //        oComprobante.MetodoPago = "PPD";
                        //    }
                        //    if (!String.IsNullOrEmpty(ocFACTURA_BANDEJAtmp.FORMA_DE_PAGO))
                        //    {
                        //        oComprobante.FormaPago = ocFACTURA_BANDEJAtmp.FORMA_DE_PAGO;
                        //    }
                        //    else
                        //    {
                        //        oComprobante.FormaPago = "99";
                        //    }
                        //}

                        //Tighitco siempre PPD
                        if (oEncapsular.ocEMPRESA.ENTITY_ID.Equals("TIGMEX"))
                        {
                            oComprobante.MetodoPago = "PPD";
                        }
                        else
                        {
                            if (oEncapsular.ocEMPRESA.ENTITY_ID.Equals("CHIMEX"))
                            {
                                oComprobante.MetodoPago = "PPD";
                            }
                        }

                    }
                    catch (Exception error1)
                    {
                        ErrorFX.mostrar(error1, true, true, "generar33 - 190 - Cabecera ", true);
                    }
                    X509Certificate cert = X509Certificate.CreateFromCertFile(ocCERTIFICADO.CERTIFICADO);
                    try
                    {
                        //Certificado
                        //Para la Version V3
                        //MessageBox.Show("Comprobante8");

                        string NoSerie = cert.GetSerialNumberString();
                        string Certificado64 = System.Convert.ToBase64String(cert.GetRawCertData());
                        oComprobante.Certificado = Certificado64;
                        //MessageBox.Show("Comprobante9");
                        //Version 3
                        oComprobante.NoCertificado = validacion(ocCERTIFICADO.NO_CERTIFICADO);

                        bool descuentoTr = false;
                        try
                        {
                            descuentoTr = bool.Parse(oEncapsular.ocEMPRESA.DESCUENTO_LINEA);
                        }
                        catch
                        {
                        }


                        //SubTotal
                        if (oFACTURA.anticipo)
                        {
                            oComprobante.SubTotal = Math.Abs(oFACTURA.TOTAL_AMOUNT);
                        }
                        else
                        {
                            oComprobante.SubTotal = Math.Abs(oFACTURA.TOTAL_AMOUNT) + Math.Abs(oFACTURA.DESCUENTO_TOTAL);


                            if (descuentoTr)
                            {
                                oComprobante.SubTotal = Math.Abs(oFACTURA.TOTAL_AMOUNT);
                            }
                            else
                            {
                                oComprobante.SubTotal = Math.Abs(oFACTURA.TOTAL_AMOUNT) + oFACTURA.DESCUENTO_TOTAL;
                            }

                        }

                        //MessageBox.Show("Comprobante10");
                        if (oFACTURA.DESCUENTO_TOTAL != 0)
                        {
                            if (!descuentoTr)
                            {
                                oComprobante.Descuento = oFACTURA.DESCUENTO_TOTAL;
                                oComprobante.DescuentoSpecified = true;
                                //Rodrigo Escalona
                                //Cambio de decimales
                                //10-7-2016
                                //oComprobante.descuento = Math.Round(oComprobante.descuento, valorUnitariodecimales);
                                //oComprobante.descuento = otroTruncar(oFACTURA.DESCUENTO_TOTAL, ocEMPRESA);
                            }

                        }
                        //Total
                        //SubTotal – Descuentos + IVA = Total = Total de la Factura.

                        if (oComprobante.TipoDeComprobante.ToUpper().Equals("I"))//c_TipoDeComprobante.I)
                        {
                            if (oFACTURA.anticipo)
                            {
                                oComprobante.Total = Math.Abs(oFACTURA.TOTAL_VAT_AMOUNT);
                            }
                            else
                            {
                                if (descuentoTr)
                                {
                                    oComprobante.Total = (Math.Abs((oComprobante.SubTotal + oFACTURA.TOTAL_VAT_AMOUNT) - Math.Abs(oFACTURA.TOTAL_RETENIDO)));
                                }
                                else
                                {
                                    oComprobante.Total = (Math.Abs((oComprobante.SubTotal + oFACTURA.TOTAL_VAT_AMOUNT) - Math.Abs(oFACTURA.TOTAL_RETENIDO))) - oFACTURA.DESCUENTO_TOTAL;
                                }
                            }
                            //- oFACTURA.DESCUENTO_TOTAL;
                        }

                        if (oComprobante.TipoDeComprobante.ToUpper().Equals("E"))// c_TipoDeComprobante.E)
                        {
                            if (oFACTURA.anticipo)
                            {
                                oComprobante.Total = Math.Abs(oFACTURA.TOTAL_VAT_AMOUNT);
                            }
                            else
                            {
                                if (oFACTURA.DESCUENTO_TOTAL == 0)
                                {
                                    oComprobante.Total = Math.Abs(oFACTURA.TOTAL_AMOUNT + oFACTURA.TOTAL_VAT_AMOUNT);
                                    oComprobante.Descuento = 0;
                                    oComprobante.DescuentoSpecified = false;
                                }
                                else
                                {
                                    oComprobante.Total = Math.Abs(oFACTURA.TOTAL_AMOUNT + oFACTURA.TOTAL_VAT_AMOUNT);
                                }
                            }
                        }
                        //Rodrigo Escalona
                        //Cambio de decimales
                        //10-7-2016
                        oComprobante.SubTotal = otroTruncar(oComprobante.SubTotal, oEncapsular.ocEMPRESA);
                        oComprobante.Total = otroTruncar(oComprobante.Total, oEncapsular.ocEMPRESA);
                    }
                    catch (Exception error1)
                    {
                        ErrorFX.mostrar(error1, true, true, "generar33 - 312 - Cabecera", true);
                    }
                    //Elemento Emisor
                    CFDI33.ComprobanteEmisor oComprobanteEmisor = new CFDI33.ComprobanteEmisor();

                    //Cargar los datos de la Compañia apartir del Certificado
                    cert = X509Certificate.CreateFromCertFile(ocCERTIFICADO.CERTIFICADO);
                    string Dato = cert.GetName();
                    //MessageBox.Show("Comprobante11");

                    char[] delimiterChars = { ',' };
                    string[] TipoCer = Dato.Split(delimiterChars);

                    //RFC Posicion 4
                    string RFC_Certificado = TipoCer[4];
                    char[] RFC_Delimiter = { '=' };
                    string[] RFC_Arreglo = RFC_Certificado.Split(RFC_Delimiter);
                    string RFC_Arreglo_Certificado = RFC_Arreglo[1];
                    RFC_Arreglo_Certificado = RFC_Arreglo_Certificado.Replace("\"", "");
                    RFC_Arreglo_Certificado = RFC_Arreglo_Certificado.Replace("/", "");
                    RFC_Arreglo_Certificado = RFC_Arreglo_Certificado.Trim();
                    //MessageBox.Show("Comprobante12");
                    //RFC
                    oComprobanteEmisor.Rfc = validacion(oEncapsular.ocEMPRESA.RFC);
                    //Nombre
                    oComprobanteEmisor.Nombre = validacion(oEncapsular.ocEMPRESA.ID);
                    oComprobanteEmisor.RegimenFiscal = oEncapsular.ocEMPRESA.REGIMEN_FISCAL; ;//c_RegimenFiscal.Item609;
                                                                                              //Regimen Fiscal
                                                                                              //ComprobanteEmisorRegimenFiscal[] oComprobanteEmisorRegimenFiscal = new ComprobanteEmisorRegimenFiscal[1];
                                                                                              //oComprobanteEmisorRegimenFiscal[0] = new ComprobanteEmisorRegimenFiscal();
                                                                                              //oComprobanteEmisorRegimenFiscal[0].Regimen = oEncapsular.ocEMPRESA.REGIMEN_FISCAL;
                                                                                              //oComprobanteEmisor.RegimenFiscal = oComprobanteEmisorRegimenFiscal;
                                                                                              //t_UbicacionFiscal ot_UbicacionFiscal = new t_UbicacionFiscal();

                    ////Domicilio Fiscal
                    //if (oEncapsular.ocEMPRESA.CALLE != "")
                    //{
                    //    ot_UbicacionFiscal.calle = validacion(oEncapsular.ocEMPRESA.CALLE);
                    //}
                    //if (oEncapsular.ocEMPRESA.EXTERIOR != "")
                    //{
                    //    ot_UbicacionFiscal.noExterior = validacion(oEncapsular.ocEMPRESA.EXTERIOR);
                    //}
                    //if (oEncapsular.ocEMPRESA.INTERIOR != "")
                    //{
                    //    ot_UbicacionFiscal.noInterior = validacion(oEncapsular.ocEMPRESA.INTERIOR);
                    //}
                    //if (oEncapsular.ocEMPRESA.COLONIA != "")
                    //{
                    //    ot_UbicacionFiscal.colonia = validacion(oEncapsular.ocEMPRESA.COLONIA);
                    //}
                    //if (oEncapsular.ocEMPRESA.LOCALIDAD != "")
                    //{
                    //    ot_UbicacionFiscal.localidad = validacion(oEncapsular.ocEMPRESA.LOCALIDAD);
                    //}
                    //if (oEncapsular.ocEMPRESA.REFERENCIA != "")
                    //{
                    //    ot_UbicacionFiscal.referencia = validacion(oEncapsular.ocEMPRESA.REFERENCIA);
                    //}
                    //if (oEncapsular.ocEMPRESA.MUNICIPIO != "")
                    //{
                    //    ot_UbicacionFiscal.municipio = validacion(oEncapsular.ocEMPRESA.MUNICIPIO);
                    //}
                    //if (oEncapsular.ocEMPRESA.ESTADO != "")
                    //{
                    //    ot_UbicacionFiscal.estado = validacion(oEncapsular.ocEMPRESA.ESTADO);
                    //}
                    //if (oEncapsular.ocEMPRESA.PAIS != "")
                    //{
                    //    ot_UbicacionFiscal.pais = validacion(oEncapsular.ocEMPRESA.PAIS);
                    //}
                    //if (oEncapsular.ocEMPRESA.CP != "")
                    //{
                    //    ot_UbicacionFiscal.codigoPostal = validacion(oEncapsular.ocEMPRESA.CP);
                    //}
                    ////MessageBox.Show("Comprobante14");
                    //oComprobanteEmisor.DomicilioFiscal = ot_UbicacionFiscal;
                    ////oFACTURA.ocSERIE.cargar(oSERIE.ROW_ID);

                    ////Expedido En
                    //t_Ubicacion ot_Ubicacion = new t_Ubicacion();
                    //if (ocSERIE.CALLE != "")
                    //{
                    //    ot_Ubicacion.calle = validacion(ocSERIE.CALLE);
                    //}
                    //if (ocSERIE.EXTERIOR != "")
                    //{
                    //    ot_Ubicacion.noExterior = validacion(ocSERIE.EXTERIOR);
                    //}
                    //if (ocSERIE.INTERIOR != "")
                    //{
                    //    ot_Ubicacion.noInterior = validacion(ocSERIE.INTERIOR);
                    //}
                    //if (ocSERIE.COLONIA != "")
                    //{
                    //    ot_Ubicacion.colonia = validacion(ocSERIE.COLONIA);
                    //}
                    //if (ocSERIE.LOCALIDAD != "")
                    //{
                    //    ot_Ubicacion.localidad = validacion(ocSERIE.LOCALIDAD);
                    //}
                    //if (ocSERIE.REFERENCIA != "")
                    //{
                    //    ot_Ubicacion.referencia = validacion(ocSERIE.REFERENCIA);
                    //}
                    //if (ocSERIE.MUNICIPIO != "")
                    //{
                    //    ot_Ubicacion.municipio = validacion(ocSERIE.MUNICIPIO);
                    //}
                    //if (ocSERIE.ESTADO != "")
                    //{
                    //    ot_Ubicacion.estado = validacion(ocSERIE.ESTADO);
                    //}
                    //if (ocSERIE.PAIS != "")
                    //{
                    //    ot_Ubicacion.pais = validacion(ocSERIE.PAIS);
                    //}
                    //if (ocSERIE.CP != "")
                    //{
                    //    ot_Ubicacion.codigoPostal = validacion(ocSERIE.CP);
                    //}

                    //oComprobanteEmisor.ExpedidoEn = ot_Ubicacion;

                    oComprobante.Emisor = oComprobanteEmisor;
                    //MessageBox.Show("Comprobante15");
                    //Elemento Receptor
                    CFDI33.ComprobanteReceptor oComprobanteReceptor = new CFDI33.ComprobanteReceptor();
                    oComprobanteReceptor.Rfc = validacion(oFACTURA.VAT_REGISTRATION);
                    //&amp;
                    oComprobanteReceptor.Rfc = validacion_AMP(oComprobanteReceptor.Rfc);
                    oComprobanteReceptor.Nombre = validacion(oFACTURA.BILL_TO_NAME);

                    try
                    {
                        if (!String.IsNullOrEmpty(oEncapsular.ocEMPRESA.CUSTOM_NOMBRE_CFDI))
                        {
                            string customtr = oEncapsular.ocEMPRESA.CUSTOM_NOMBRE_CFDI.Replace("NAME", oFACTURA.NAME)
                                .Replace("ADDR_1", oFACTURA.ADDR_1)
                                .Replace("ADDR_2", oFACTURA.ADDR_2)
                                .Replace("ADDR_3", oFACTURA.ADDR_3);
                            oComprobanteReceptor.Nombre = customtr;
                        }

                        /*
                        if (!String.IsNullOrEmpty(oEncapsular.ocEMPRESA.USAR_COMPLEMENTO_ADDR3))
                        {
                            bool USAR_COMPLEMENTO_ADDR3Tr = bool.Parse(oEncapsular.ocEMPRESA.USAR_COMPLEMENTO_ADDR3);
                            if (USAR_COMPLEMENTO_ADDR3Tr)
                            {
                                oComprobanteReceptor.Nombre = validacion(oFACTURA.BILL_TO_NAME);

                                string addr3Tr = oFACTURA.BILL_TO_ADDR_3.Trim();
                                if (!String.IsNullOrEmpty(addr3Tr))
                                {
                                    oComprobanteReceptor.Nombre +=" "+addr3Tr;
                                }
                                else
                                {
                                    oComprobanteReceptor.Nombre += validacion(" " + oFACTURA.ADDR_3);
                                }


                                string addr1Tr = oFACTURA.BILL_TO_ADDR_1.Trim();
                                if (!String.IsNullOrEmpty(addr1Tr))
                                {
                                    oComprobanteReceptor.Nombre += addr1Tr;
                                }
                                else
                                {
                                    oComprobanteReceptor.Nombre += validacion(oFACTURA.ADDR_1);
                                }


                            }
                        }
                        */
                    }
                    catch (Exception e)
                    {

                    }

                    if (!String.IsNullOrEmpty(ocFACTURA_BANDEJAtmp.USO_CFDI))
                    {
                        oComprobanteReceptor.UsoCFDI = ocFACTURA_BANDEJAtmp.USO_CFDI;
                    }
                    else
                    {
                        oComprobanteReceptor.UsoCFDI = "P01";
                    }
                    //Rodrigo Escalona: 31/05/2018
                    cCLIENTE_CONFIGURACION ocCLIENTE_CONFIGURACION = new cCLIENTE_CONFIGURACION(oData);
                    ocCLIENTE_CONFIGURACION.cargar_CUSTOMER_ID(oFACTURA.CUSTOMER_ID);
                    if (!String.IsNullOrEmpty(ocCLIENTE_CONFIGURACION.usoCFDI))
                    {
                        oComprobanteReceptor.UsoCFDI = ocCLIENTE_CONFIGURACION.usoCFDI;
                    }


                    //&amp;
                    oComprobanteReceptor.Nombre = validacion_AMP(oComprobanteReceptor.Nombre);

                    //MessageBox.Show("Comprobante16");
                    //if (bool.Parse(ConfigurationManager.AppSettings["DomicilioReceptor"].ToString()))
                    ////if (true)
                    ////{

                    //Verificar
                    if (oFACTURA.BILL_TO_ADDR_1.Trim() != "")
                    {
                        ot_UbicacionReceptor.calle = extraeDireccion(validacion(oFACTURA.BILL_TO_ADDR_1));
                        string exterior = extraeExterior(validacion(oFACTURA.BILL_TO_ADDR_1));
                        if (exterior != "")
                        {
                            ot_UbicacionReceptor.noExterior = exterior;
                        }
                        string interior = extraeInterior(validacion(oFACTURA.BILL_TO_ADDR_1));
                        if (interior != "")
                        {
                            ot_UbicacionReceptor.noInterior = interior;
                        }

                    }

                    string localidad = extraeLocalidad(validacion(oFACTURA.BILL_TO_ADDR_3));
                    if (localidad != "")
                    {
                        ot_UbicacionReceptor.localidad = localidad;
                    }

                    if (oFACTURA.BILL_TO_ADDR_2.Trim() != "")
                    {
                        ot_UbicacionReceptor.colonia = validacion(oFACTURA.BILL_TO_ADDR_2);
                    }
                    if (oFACTURA.BILL_TO_STATE.Trim() != "")
                    {
                        ot_UbicacionReceptor.localidad = validacion(oFACTURA.BILL_TO_STATE);

                    }
                    if (oFACTURA.BILL_TO_STATE.Trim() != "")
                    {
                        ot_UbicacionReceptor.estado = validacion(oFACTURA.BILL_TO_STATE);
                    }
                    ot_UbicacionReceptor.referencia = "";
                    if (oFACTURA.BILL_TO_CITY.Trim() != "")
                    {
                        ot_UbicacionReceptor.municipio = validacion(oFACTURA.BILL_TO_CITY);
                    }
                    ot_UbicacionReceptor.pais = validacion(oFACTURA.BILL_TO_COUNTRY);
                    //Validar el pais
                    ////ot_UbicacionReceptor.pais = c_Pais.USA.ToString();

                    ////if (oFACTURA.COUNTRY.ToUpper().Contains("IND"))
                    ////{
                    ////    ot_UbicacionReceptor.pais = c_Pais.IND.ToString();
                    ////}

                    ////if (oFACTURA.COUNTRY.ToUpper().Contains("DOM"))
                    ////{
                    ////    ot_UbicacionReceptor.pais = c_Pais.DOM.ToString();
                    ////}
                    ////if (oFACTURA.COUNTRY.ToUpper().Contains("USA"))
                    ////{
                    ////    ot_UbicacionReceptor.pais = c_Pais.USA.ToString();
                    ////}
                    ////if (oFACTURA.COUNTRY.ToUpper().Contains("CAN"))
                    ////{
                    ////    ot_UbicacionReceptor.pais = c_Pais.CAN.ToString();
                    ////}

                    ////if (oFACTURA.COUNTRY.ToUpper().Contains("GBR"))
                    ////{
                    ////    ot_UbicacionReceptor.pais = c_Pais.GBR.ToString();
                    ////}


                    if (oFACTURA.BILL_TO_ZIPCODE.Trim() != "")
                    {
                        ot_UbicacionReceptor.codigoPostal = validacion(oFACTURA.BILL_TO_ZIPCODE);
                    }
                    ////    oComprobanteReceptor.Domicilio = ot_UbicacionReceptor;
                    ////}

                    oComprobante.Receptor = oComprobanteReceptor;
                }
                catch (Exception errGeneral)
                {
                    ErrorFX.mostrar(errGeneral, true, true, "generar33 - 541 - Seccion de cabecera ", true);
                    return false;
                }
                //Rodrigo Escalona: 26/07/2018 NDC Activar
                bool ndcUsarPropio = false;
                try
                {
                    if (oComprobante.TipoDeComprobante.Equals("E"))
                    {
                        if (!String.IsNullOrEmpty(ocSERIE.ndcUsarPropio))
                        {
                            if (bool.Parse(ocSERIE.ndcUsarPropio))
                            {
                                ndcUsarPropio = true;
                                oComprobante.MetodoPago = ocSERIE.ndcMetodoPago;
                                oComprobante.Receptor.UsoCFDI = ocSERIE.ndcUsoCfdi;
                            }
                        }
                    }
                }
                catch (Exception errGeneral)
                {
                    ErrorFX.mostrar(errGeneral, true, true, "generar33 - 620 - Seccion de Cabecera NDC automatizada ", true);
                    return false;
                }


                //Elemento Conceptos
                try
                {
                    CFDI33.ComprobanteConcepto[] oComprobanteConceptos;
                    oComprobanteConceptos = new CFDI33.ComprobanteConcepto[oFACTURA.LINEAS.Length];
                    for (int i = 0; i < oFACTURA.LINEAS.Length; i++)
                    {
                        if (oFACTURA.LINEAS[i] != null)
                        {
                            //if (decimal.Parse(oFACTURA.LINEAS[i].AMOUNT) != 0)
                            if (true)
                            {
                                CFDI33.ComprobanteConcepto oComprobanteConcepto = new CFDI33.ComprobanteConcepto();
                                //MessageBox.Show("Comprobante18");
                                try
                                {

                                    oComprobanteConcepto.Cantidad = Math.Abs(decimal.Parse(oFACTURA.LINEAS[i].QTY));
                                    oComprobanteConcepto.Cantidad = Math.Round(oComprobanteConcepto.Cantidad, 2);
                                    //Rodrigo Escalona 31/01/2020 Si el producto tiene valor
                                    if (!String.IsNullOrEmpty(oFACTURA.LINEAS[i].PART_ID))
                                    {
                                        if (!String.IsNullOrEmpty(oFACTURA.LINEAS[i].STOCK_UM))
                                        {
                                            configuracionUM busquedatr = buscarClaveUnidad(oFACTURA.LINEAS[i].STOCK_UM);
                                            if (busquedatr != null)
                                            {
                                                oComprobanteConcepto.ClaveUnidad = busquedatr.SAT.ToUpper();
                                                if (oComprobanteConcepto.ClaveUnidad.Equals("KGM"))
                                                {
                                                    //Rodrigo Escalona 29/04/2021 Fima no quiere KF
                                                    if (!oEncapsular.ocEMPRESA.ENTITY_ID.Contains("FIMA"))
                                                    {
                                                        oComprobanteConcepto.Unidad = "KG";
                                                    }
                                                }
                                            }
                                            else
                                            {
                                                oComprobanteConcepto.ClaveUnidad = oFACTURA.LINEAS[i].STOCK_UM.ToUpper();
                                                if (oComprobanteConcepto.ClaveUnidad.Equals("KGM"))
                                                {
                                                    //Rodrigo Escalona 29/04/2021 Fima no quiere KF
                                                    if (!oEncapsular.ocEMPRESA.ENTITY_ID.Contains("FIMA"))
                                                    {
                                                        oComprobanteConcepto.Unidad = "KG";
                                                    }
                                                }
                                            }
                                        }
                                    }

                                }
                                catch (Exception errGeneral)
                                {
                                    ErrorFX.mostrar(errGeneral, true, true, "generar33 - 577 - Seccion de lineas - Cantidad", true);
                                    return false;
                                }

                                //else
                                //{
                                //    //Cargar la unidad por defecto
                                //    oComprobanteConcepto.Unidad = "99";
                                //}

                                try
                                {
                                    if (oFACTURA.anticipo)
                                    {
                                        oFACTURA.LINEAS[i].AMOUNT = (decimal.Parse(oFACTURA.LINEAS[i].AMOUNT) - oFACTURA.anticipoAMOUNT).ToString();
                                        oFACTURA.LINEAS[i].UNIT_PRICE =
                                            (Math.Round(decimal.Parse(oFACTURA.LINEAS[i].AMOUNT) / oComprobanteConcepto.Cantidad
                                            , 4)).ToString();
                                    }
                                }
                                catch (Exception errGeneral)
                                {
                                    ErrorFX.mostrar(errGeneral, true, true, "generar33 - 600 - Seccion de lineas - Anticipo", true);
                                    return false;
                                }
                                //Agregar el producto PART_ID
                                try
                                {
                                    //Rodrigo Escalona 17/12/2020 Validar si la prioridad es el producto del cliente
                                    bool pegarPart = true;
                                    if (!String.IsNullOrEmpty(oFACTURA.LINEAS[i].PART_ID))
                                    {
                                        oComprobanteConcepto.NoIdentificacion = oFACTURA.LINEAS[i].PART_ID;
                                    }
                                    if (!String.IsNullOrEmpty(oFACTURA.LINEAS[i].CUSTOMER_PART_ID))
                                    {
                                        try
                                        {
                                            if (!String.IsNullOrEmpty(oEncapsular.ocEMPRESA.PRIORIDAD_PART))
                                            {
                                                bool PRIORIDAD_PARTTr = bool.Parse(oEncapsular.ocEMPRESA.PRIORIDAD_PART);
                                                if (PRIORIDAD_PARTTr)
                                                {
                                                    //Rodrigo Escalona: 12/10/2021 Validar si el filtro es por Cliente  oGeneral
                                                    //bool NoIdentificacion = true;
                                                    if (!String.IsNullOrEmpty(oEncapsular.ocEMPRESA.PrioridadPartFiltro))
                                                    {
                                                        //NoIdentificacion = false;
                                                        //Validar si es el mismo customer
                                                        if (oEncapsular.ocEMPRESA.PrioridadPartFiltro.Equals(oFACTURA.CUSTOMER_ID))
                                                        {
                                                            //NoIdentificacion = true;
                                                            oComprobanteConcepto.NoIdentificacion = oFACTURA.LINEAS[i].CUSTOMER_PART_ID;
                                                            pegarPart = false;
                                                        }
                                                    }
                                                    //if (NoIdentificacion)
                                                    //{

                                                    //}

                                                }
                                            }
                                        }
                                        catch (Exception e)
                                        {
                                            ErrorFX.mostrar(e, true, true, "generar33 - 740 - errorPrioridad", false);
                                        }
                                    }

                                    if (pegarPart)
                                    {
                                        if (!String.IsNullOrEmpty(oFACTURA.LINEAS[i].PART_ID))
                                        {
                                            oComprobanteConcepto.NoIdentificacion = oFACTURA.LINEAS[i].PART_ID;
                                        }
                                    }
                                    //Cargar la Linea relacionada
                                    bool relacionado = false;
                                    try
                                    {
                                        string lineatr = oFACTURA.LINEAS[i].LINEA;

                                        ndcFacturaLinea linea = (from b in dbContext.ndcFacturaLineaSet
                                        .Where(a => a.INVOICE_ID == oFACTURA.INVOICE_ID & a.LINE_NO == lineatr)
                                                                 select b).FirstOrDefault();
                                        if (linea != null)
                                        {
                                            relacionado = true;
                                            //Rodrigo Escalona: 29/12/2021 Error que da a Tghitco
                                            //oComprobanteConcepto.ClaveProdServ = linea.clasificacion;
                                            //oComprobanteConcepto.NoIdentificacion = linea.PART_ID;
                                        }

                                    }
                                    catch (Exception e)
                                    {
                                        ErrorFX.mostrar(e, true, true, "generar33 - 727", false);
                                    }


                                    //Buscar el numero de Producto para ser migrado


                                    if (!String.IsNullOrEmpty(oFACTURA.LINEAS[i].ClaveProdServ) & !String.IsNullOrEmpty(oFACTURA.LINEAS[i].PART_ID))
                                    {
                                        BitacoraFX.Log("863 - Buscar el numero de Producto para ser migrado "
                                                            + oFACTURA.LINEAS[i].ClaveProdServ);

                                        if (!relacionado)
                                        {
                                            oComprobanteConcepto.ClaveProdServ = oFACTURA.LINEAS[i].ClaveProdServ;
                                        }
                                    }
                                    else
                                    {
                                        //Validar si la linea de la Factura tiene CFDI relacionados
                                        bool cargarLinea = true;
                                        try
                                        {
                                            string lineatr = oFACTURA.LINEAS[i].LINEA;

                                            ndcFacturaLinea linea = (from b in dbContext.ndcFacturaLineaSet
                                            .Where(a => a.INVOICE_ID == oFACTURA.INVOICE_ID & a.LINE_NO == lineatr)
                                                                     select b).FirstOrDefault();
                                            if (linea != null)
                                            {
                                                cargarLinea = false;
                                                oComprobanteConcepto.ClaveProdServ = linea.clasificacion;
                                                oComprobanteConcepto.NoIdentificacion = linea.PART_ID;
                                            }

                                        }
                                        catch (Exception e)
                                        {
                                            ErrorFX.mostrar(e, true, true, "generar33 - 617", false);
                                        }
                                        if (cargarLinea)
                                        {
                                            try
                                            {
                                                BitacoraFX.Log("Buscar la ClaveProdServ - 902 ");
                                                //Cargar desde aqui 
                                                if (!String.IsNullOrEmpty(oFACTURA.LINEAS[i].ClaveProdServ) & !String.IsNullOrEmpty(oFACTURA.LINEAS[i].PART_ID))
                                                {
                                                    oComprobanteConcepto.ClaveProdServ = oFACTURA.LINEAS[i].ClaveProdServ.Trim();
                                                    configuracionUM busquedatr = buscarClaveUnidad(oFACTURA.LINEAS[i].STOCK_UM.Trim());

                                                    if (busquedatr != null)
                                                    {
                                                        oComprobanteConcepto.ClaveUnidad = busquedatr.SAT.ToUpper();
                                                        if (oComprobanteConcepto.ClaveUnidad.Equals("KGM"))
                                                        {
                                                            //Rodrigo Escalona 28/04/2021
                                                            //Rodrigo Escalona 29/04/2021 Fima no quiere KF
                                                            if (!oEncapsular.ocEMPRESA.ENTITY_ID.Contains("FIMA"))
                                                            {
                                                                oComprobanteConcepto.Unidad = "KG";
                                                            }
                                                        }
                                                    }
                                                    BitacoraFX.Log("915 Buscar el numero de Producto para ser migrado "
                                                            + oFACTURA.LINEAS[i].ClaveProdServ);
                                                }
                                                else
                                                {
                                                    //Validar que tengas los valores para buscar 
                                                    bool buscarClasificador = false;
                                                    BitacoraFX.Log("Busqueda de Clasificador por Product Code " + oFACTURA.LINEAS[i].PRODUCT_CODE);

                                                    if (!String.IsNullOrEmpty(oFACTURA.LINEAS[i].PRODUCT_CODE))
                                                    {
                                                        buscarClasificador = true;
                                                    }
                                                    BitacoraFX.Log("Busqueda de Clasificador por GL_ACCOUNT_ID " + oFACTURA.LINEAS[i].GL_ACCOUNT_ID);
                                                    if (!String.IsNullOrEmpty(oFACTURA.LINEAS[i].GL_ACCOUNT_ID))
                                                    {
                                                        buscarClasificador = true;
                                                    }
                                                    BitacoraFX.Log("Busqueda de Clasificador por GL_ACCOUNT_ID " + oFACTURA.LINEAS[i].SERVICE_CHARGE_ID);
                                                    if (!String.IsNullOrEmpty(oFACTURA.LINEAS[i].SERVICE_CHARGE_ID))
                                                    {
                                                        buscarClasificador = true;
                                                    }
                                                    if (buscarClasificador)
                                                    {
                                                        configuracionProducto oConfiguracionProducto = oFACTURA.buscarClave(oFACTURA.LINEAS[i].PART_ID
                                                            , oFACTURA.LINEAS[i].PRODUCT_CODE, oFACTURA.LINEAS[i].GL_ACCOUNT_ID
                                                            , oFACTURA.LINEAS[i].SERVICE_CHARGE_ID);
                                                        bool buscarProducto = false;
                                                        if (oConfiguracionProducto == null)
                                                        {
                                                            frmProductoClasificacion oFrmProductoClasificacion = new frmProductoClasificacion(oFACTURA.LINEAS[i].PART_ID
                                                            , oFACTURA.LINEAS[i].PRODUCT_CODE, oFACTURA.LINEAS[i].GL_ACCOUNT_ID
                                                            , oFACTURA.LINEAS[i].SERVICE_CHARGE_ID);
                                                            if (oFrmProductoClasificacion.ShowDialog() == DialogResult.OK)
                                                            {
                                                                oConfiguracionProducto = oFACTURA.buscarClave(oFACTURA.LINEAS[i].PART_ID
                                                                , oFACTURA.LINEAS[i].PRODUCT_CODE, oFACTURA.LINEAS[i].GL_ACCOUNT_ID
                                                                , oFACTURA.LINEAS[i].SERVICE_CHARGE_ID);
                                                            }
                                                            else
                                                            {
                                                                return false;
                                                            }
                                                        }

                                                        BitacoraFX.Log("Busqueda de Clasificador encontrador oConfiguracionProducto clasificacionSAT "
                                                            + oConfiguracionProducto.clasificacionSAT);

                                                        if (!buscarProducto)
                                                        {
                                                            if (String.IsNullOrEmpty(oConfiguracionProducto.clasificacionSAT))
                                                            {
                                                                oConfiguracionProducto = buscarConfiguracionProducto(oFACTURA.INVOICE_ID, oFACTURA.LINEAS[i].PART_ID);
                                                                if (String.IsNullOrEmpty(oConfiguracionProducto.clasificacionSAT))
                                                                {
                                                                    if (!ndcUsarPropio)
                                                                    {
                                                                        ErrorFX.mostrar("GENERAR33 - 640 - Factura " + oFACTURA.INVOICE_ID + "  El producto " + oFACTURA.LINEAS[i].PART_ID
                                                                    + " no tiene su clave del SAT " + Environment.NewLine + " Configure el product code: ", true, true, true);


                                                                        return false;
                                                                    }
                                                                }
                                                            }
                                                            oComprobanteConcepto.ClaveProdServ = oConfiguracionProducto.clasificacionSAT.Trim();
                                                            if (!String.IsNullOrEmpty(oConfiguracionProducto.unidadSAT))
                                                            {
                                                                oComprobanteConcepto.ClaveUnidad = oConfiguracionProducto.unidadSAT.Trim().ToUpper();
                                                            }
                                                        }
                                                        else
                                                        {
                                                            if (!ndcUsarPropio)
                                                            {
                                                                ErrorFX.mostrar("GENERAR33 - 640 - Factura " + oFACTURA.INVOICE_ID + "  El producto " + oFACTURA.LINEAS[i].PART_ID
                                                            + " no tiene su clave del SAT", true, true, true);

                                                                if (Globales.automatico)
                                                                {
                                                                    return false;
                                                                }

                                                                oConfiguracionProducto = buscarConfiguracionProducto(oFACTURA.INVOICE_ID, oFACTURA.LINEAS[i].PART_ID);

                                                                oComprobanteConcepto.ClaveProdServ = oConfiguracionProducto.clasificacionSAT.Trim();
                                                                if (!String.IsNullOrEmpty(oConfiguracionProducto.unidadSAT))
                                                                {
                                                                    oComprobanteConcepto.ClaveUnidad = oConfiguracionProducto.unidadSAT.Trim().ToUpper();
                                                                }

                                                            }

                                                        }

                                                    }
                                                    else
                                                    {
                                                        if (!ndcUsarPropio)
                                                        {
                                                            ErrorFX.mostrar("GENERAR33 - 608 - "
                                                        + Environment.NewLine
                                                        + " Factura " + oFACTURA.INVOICE_ID + "  La cuenta contable " + oFACTURA.LINEAS[i].GL_ACCOUNT_ID
                                                        + " no tiene su clave del SAT", true, true, true);
                                                            if (Globales.automatico)
                                                            {
                                                                return false;
                                                            }
                                                            ClasificadorProductos.formularios.frmProductoClasificacion oProducto
                                                                = new ClasificadorProductos.formularios.frmProductoClasificacion(oFACTURA.LINEAS[i].GL_ACCOUNT_ID);
                                                            oProducto.ShowDialog();
                                                            if (oProducto.oRegistro == null)
                                                            {
                                                                ErrorFX.mostrar(" Factura " + oFACTURA.INVOICE_ID + " Acción cancelada por el usuario la cuenta contable " + oFACTURA.LINEAS[i].PART_ID
                                                                + " no tiene su clave del SAT", true, true, true);
                                                                return false;
                                                            }
                                                            oComprobanteConcepto.ClaveProdServ = oProducto.oRegistro.clasificacionSAT.Trim();
                                                            if (!String.IsNullOrEmpty(oProducto.oRegistro.unidadSAT))
                                                            {
                                                                oComprobanteConcepto.ClaveUnidad = oProducto.oRegistro.unidadSAT.Trim().ToUpper();
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                            catch (Exception errGeneral)
                                            {
                                                ErrorFX.mostrar(errGeneral, true, true, "generar33 - 734 - Seccion de lineas - Asignación de Clave de Producto SAT", true);
                                                return false;
                                            }
                                        }


                                    }
                                }
                                catch (Exception errGeneral)
                                {
                                    ErrorFX.mostrar(errGeneral, true, true, "generar33 - 734 - Seccion de lineas - Clave de Producto", true);
                                    return false;
                                }
                                oComprobanteConcepto.Descripcion = validacion(oFACTURA.LINEAS[i].DESCRIPCION);

                                decimal unitPrice = 0;
                                try
                                {
                                    unitPrice = decimal.Parse(oFACTURA.LINEAS[i].UNIT_PRICE);
                                }
                                catch
                                {
                                    //23/01/2018: Precio unitario FIMAS
                                    //Precio unitario de FIMA
                                    if (oEncapsular.ocEMPRESA.ENTITY_ID.Contains("FIMA"))
                                    {
                                        unitPrice = Math.Round(decimal.Parse(oFACTURA.LINEAS[i].AMOUNT) / oComprobanteConcepto.Cantidad, 4);
                                    }
                                    else
                                    {
                                        unitPrice = Math.Round(decimal.Parse(oFACTURA.LINEAS[i].AMOUNT) / oComprobanteConcepto.Cantidad, 2);
                                    }

                                }
                                oComprobanteConcepto.ValorUnitario = Math.Round(Math.Abs(unitPrice), 4); //Math.Abs(Math.Round(decimal.Parse(oFACTURA.LINEAS[i].UNIT_PRICE), valorUnitariodecimales)); //Math.Abs(decimal.Parse(oFACTURA.LINEAS[i].UNIT_PRICE)).ToString();

                                //MessageBox.Show("Comprobante19- Importe");
                                //Verificar que sea anticipo
                                //if (oFACTURA.TYPE == "I")
                                //{
                                //    oComprobanteConcepto.Importe = Math.Round(decimal.Parse(oFACTURA.LINEAS[i].AMOUNT), 4);
                                //}
                                //else
                                //{
                                //    //oComprobanteConcepto.importe = Math.Abs(decimal.Parse(oFACTURA.LINEAS[i].AMOUNT));
                                //oComprobanteConcepto.Importe = Math.Round(decimal.Parse(oFACTURA.LINEAS[i].AMOUNT), 2);
                                oComprobanteConcepto.Importe = otroTruncar(Math.Abs(decimal.Parse(oFACTURA.LINEAS[i].AMOUNT)), oEncapsular.ocEMPRESA);

                                //}
                                //oComprobanteConcepto.Importe = oComprobanteConcepto.ValorUnitario * oComprobanteConcepto.Cantidad;
                                oComprobanteConcepto.Importe = Math.Abs(oComprobanteConcepto.Importe); //Math.Round(oComprobanteConcepto.importe, 4);
                                oComprobanteConcepto.ValorUnitario = Math.Abs(oComprobanteConcepto.ValorUnitario);//Math.Round(oComprobanteConcepto.valorUnitario, 4);

                                //Buscar la conversión
                                configuracionUM oconfiguracionUM = buscarClaveUnidad(oFACTURA.LINEAS[i].STOCK_UM);
                                if (oconfiguracionUM != null)
                                {
                                    if (!String.IsNullOrEmpty(oconfiguracionUM.UnidadMedida))
                                    {
                                        oComprobanteConcepto.ClaveUnidad = oconfiguracionUM.SAT.ToUpper();
                                        //oComprobanteConcepto.Unidad = oComprobanteConcepto.ClaveUnidad;
                                    }
                                }


                                //oComprobanteConcepto.Importe = otroTruncar(oComprobanteConcepto.Importe, oEncapsular.ocEMPRESA);

                                //Rodrigo Escalona 30/06/2022 Validar impuesto en 0 el impuesto es 0
                                if (oComprobanteConcepto.Importe != 0)
                                {
                                    oComprobanteConcepto.Impuestos = new CFDI33.ComprobanteConceptoImpuestos();
                                    oComprobanteConcepto.Impuestos.Traslados = new CFDI33.ComprobanteConceptoImpuestosTraslado[1];
                                    oComprobanteConcepto.Impuestos.Traslados[0] = new CFDI33.ComprobanteConceptoImpuestosTraslado();
                                    oComprobanteConcepto.Impuestos.Traslados[0].TasaOCuota = decimal.Parse("0.160000");
                                    oComprobanteConcepto.Impuestos.Traslados[0].TasaOCuotaSpecified = true;
                                    oComprobanteConcepto.Impuestos.Traslados[0].TipoFactor = "Tasa";
                                    oComprobanteConcepto.Impuestos.Traslados[0].Importe = otroTruncar(Math.Abs(decimal.Parse(oFACTURA.LINEAS[i].VAT_AMOUNT)), oEncapsular.ocEMPRESA);
                                    oComprobanteConcepto.Impuestos.Traslados[0].Base = otroTruncar(Math.Abs(decimal.Parse(oFACTURA.LINEAS[i].AMOUNT)), oEncapsular.ocEMPRESA);
                                    oComprobanteConcepto.Impuestos.Traslados[0].ImporteSpecified = true;

                                    decimal porcentajeTasaTr = 0;
                                    if (decimal.Parse(oFACTURA.LINEAS[i].VAT_AMOUNT) != 0)
                                    {
                                        porcentajeTasaTr = Math.Abs(((decimal.Parse(oFACTURA.LINEAS[i].AMOUNT) * 100) / decimal.Parse(oFACTURA.LINEAS[i].VAT_AMOUNT)) / 100);
                                    }
                                    oComprobanteConcepto.Impuestos.Traslados[0].TasaOCuota = Math.Round(porcentajeTasaTr, 2);
                                    oComprobanteConcepto.Impuestos.Traslados[0].Impuesto = "002";

                                    if (oComprobanteConcepto.Impuestos.Traslados[0].Importe == 0)
                                    {
                                        oComprobanteConcepto.Impuestos.Traslados[0].TasaOCuota = decimal.Parse("0.000000");
                                    }
                                    else
                                    {
                                        oComprobanteConcepto.Impuestos.Traslados[0].TasaOCuota = decimal.Parse("0.160000");
                                    }
                                }

                                
                                //- decimal.Parse(oFACTURA.LINEAS[i].VAT_AMOUNT);

                                //< cfdi:Traslado Base = "17000" Impuesto = "002" TipoFactor = "Tasa" TasaOCuota = "0.160000" Importe = "2720" />
                                //         </ cfdi:Traslados >

                                //Agregar la retención
                                if (oFACTURA.TOTAL_RETENIDO != 0)
                                {
                                    if (oFACTURA.LINEAS[i].retencion != null)
                                    {

                                        if (oFACTURA.LINEAS[i].retencion.Count > 0)
                                        {
                                            oComprobanteConcepto.Impuestos.Retenciones = new CFDI33.ComprobanteConceptoImpuestosRetencion[oFACTURA.LINEAS[i].retencion.Count];
                                            int contadorRetencion = 0;
                                            foreach (cLINEARETENCION LineaRetencion in oFACTURA.LINEAS[i].retencion)
                                            {
                                                oComprobanteConcepto.Impuestos.Retenciones[contadorRetencion] = new CFDI33.ComprobanteConceptoImpuestosRetencion();
                                                oComprobanteConcepto.Impuestos.Retenciones[contadorRetencion].TipoFactor = "Tasa";
                                                decimal porcentajeTasa = 0;
                                                if (decimal.Parse(oFACTURA.LINEAS[i].AMOUNT) != 0)
                                                {
                                                    porcentajeTasa = Math.Abs(((decimal.Parse(LineaRetencion.AMOUNT) * 100) / decimal.Parse(oFACTURA.LINEAS[i].AMOUNT)) / 100);
                                                }
                                                oComprobanteConcepto.Impuestos.Retenciones[contadorRetencion].TasaOCuota = Math.Round(porcentajeTasa, 3);
                                                oComprobanteConcepto.Impuestos.Retenciones[contadorRetencion].Impuesto = "002";
                                                oComprobanteConcepto.Impuestos.Retenciones[contadorRetencion].Base = otroTruncar(Math.Abs(decimal.Parse(oFACTURA.LINEAS[i].AMOUNT)), oEncapsular.ocEMPRESA);
                                                oComprobanteConcepto.Impuestos.Retenciones[contadorRetencion].Importe = otroTruncar(Math.Abs(decimal.Parse(LineaRetencion.AMOUNT)), oEncapsular.ocEMPRESA);

                                                BitacoraFX.Log("Retencion " + contadorRetencion.ToString() + " oFACTURA.LINEAS[i].retencion = " + oFACTURA.LINEAS[i].retencion.ToString()
                                                    + "  porcentajeTasa=" + porcentajeTasa.ToString()
                                                    + "  GL_ACCOUNT_ID=" + LineaRetencion.GL_ACCOUNT_ID.ToString()
                                                    + "  porcentajeTasa=" + porcentajeTasa.ToString()
                                                    );

                                                contadorRetencion++;
                                            }

                                        }


                                    }
                                    else
                                    {
                                        //Agregar la rentecion GLOBAL GENERAL
                                        oComprobanteConcepto.Impuestos.Retenciones = new CFDI33.ComprobanteConceptoImpuestosRetencion[1];
                                        oComprobanteConcepto.Impuestos.Retenciones[0] = new CFDI33.ComprobanteConceptoImpuestosRetencion();
                                        oComprobanteConcepto.Impuestos.Retenciones[0].TipoFactor = "Tasa";

                                        //oComprobanteConcepto.Impuestos.Retenciones[0].TasaOCuota = decimal.Parse("0.160000");
                                        //if (oComprobanteConcepto.Impuestos.Retenciones[0].Importe == 0)
                                        //{
                                        //    oComprobanteConcepto.Impuestos.Retenciones[0].TasaOCuota = decimal.Parse("0.000000");
                                        //}
                                        //Determinar la Tasa
                                        decimal porcentajeTasa = 0;
                                        if (decimal.Parse(oFACTURA.LINEAS[i].AMOUNT) != 0)
                                        {
                                            porcentajeTasa = Math.Abs(((oFACTURA.TOTAL_RETENIDO * 100) / decimal.Parse(oFACTURA.LINEAS[i].AMOUNT)) / 100);
                                        }
                                        oComprobanteConcepto.Impuestos.Retenciones[0].TasaOCuota = Math.Round(porcentajeTasa, 3);


                                        oComprobanteConcepto.Impuestos.Retenciones[0].Impuesto = "002";
                                        oComprobanteConcepto.Impuestos.Retenciones[0].Base = otroTruncar(Math.Abs(decimal.Parse(oFACTURA.LINEAS[i].AMOUNT)), oEncapsular.ocEMPRESA);
                                        oComprobanteConcepto.Impuestos.Retenciones[0].Importe = otroTruncar(Math.Abs(oFACTURA.TOTAL_RETENIDO), oEncapsular.ocEMPRESA);


                                    }
                                }

                                if (oEncapsular.ocEMPRESA.ENTITY_ID.Contains("FIMA"))
                                {
                                    if (oComprobanteConcepto.Descripcion.Contains("SERVICIO"))
                                    {
                                        oComprobanteConcepto.ClaveUnidad = "E48";
                                    }
                                    if (String.IsNullOrEmpty(oComprobanteConcepto.ClaveUnidad))
                                    {
                                        oComprobanteConcepto.ClaveUnidad = "H87";
                                        if (oComprobanteConcepto.Descripcion.Contains("SERVICIO"))
                                        {
                                            oComprobanteConcepto.ClaveUnidad = "E48";
                                        }
                                    }

                                }

                                //Cuando es factura de anticipo
                                //if (oComprobante.TipoDeComprobante.ToUpper().Equals("E"))//c_TipoDeComprobante.E)
                                //{
                                //    oComprobanteConcepto.ClaveProdServ = "84111506";
                                //    oComprobanteConcepto.ClaveUnidad = "ACT";
                                //}

                                //Rodrigo Escalona: NDC si es automatizada 26/07/2018

                                try
                                {
                                    if (oComprobante.TipoDeComprobante.Equals("E"))
                                    {
                                        if (bool.Parse(ocSERIE.ndcUsarPropio))
                                        {
                                            ndcUsarPropio = true;
                                            if (String.IsNullOrEmpty(oComprobanteConcepto.ClaveProdServ))
                                            {
                                                oComprobanteConcepto.ClaveProdServ = ocSERIE.ndcClaveProd;
                                            }
                                            if (String.IsNullOrEmpty(oComprobanteConcepto.ClaveUnidad))
                                            {
                                                oComprobanteConcepto.ClaveUnidad = ocSERIE.ndcUnidad.ToUpper();
                                            }


                                        }
                                    }


                                }
                                catch (Exception errGeneral)
                                {
                                    ErrorFX.mostrar(errGeneral, true, true, "generar33 - 620 - Seccion de Cabecera NDC automatizada ", true);
                                    return false;
                                }

                                oComprobanteConceptos[i] = new CFDI33.ComprobanteConcepto();
                                try
                                {
                                    //Validar que la ClaveProdServ no este en blanco
                                    try
                                    {
                                        if (!String.IsNullOrEmpty(oComprobanteConcepto.ClaveProdServ))
                                        {
                                            //Cargar ClaveProdServ del NoIdentificacion
                                            if (!String.IsNullOrEmpty(oComprobanteConcepto.NoIdentificacion))
                                            {
                                                //Buscar el numero de parte el clasificador
                                                ndcFacturaLinea lineaTmp = dbContext.ndcFacturaLineaSet.Where(a => a.PART_ID == oComprobanteConcepto.NoIdentificacion
                                                & a.INVOICE_ID != oFACTURA.INVOICE_ID).FirstOrDefault();
                                                if (lineaTmp != null)
                                                {
                                                    oComprobanteConcepto.ClaveProdServ = lineaTmp.clasificacion;

                                                }

                                                //Actualizarlo en la tabla

                                            }
                                            else
                                            {
                                                oComprobanteConcepto.NoIdentificacion = oComprobanteConcepto.ClaveProdServ;
                                            }
                                        }
                                    }
                                    catch (Exception errGeneral)
                                    {
                                        ErrorFX.mostrar(errGeneral, true, true, "generar33 - 1293 - Validacion de  ClaveProdServ", true);
                                        return false;
                                    }
                                    try
                                    {
                                        //Limpiar la ClaveProdSer
                                        if (!String.IsNullOrEmpty(oComprobanteConcepto.ClaveProdServ))
                                        {
                                            oComprobanteConcepto.ClaveProdServ = Regex.Replace(oComprobanteConcepto.ClaveProdServ, @"^[A-Za-z]+", "");
                                            oComprobanteConcepto.ClaveProdServ = oComprobanteConcepto.ClaveProdServ.Trim().TrimEnd().TrimStart();
                                            string ClaveProdServTr = "";
                                            foreach (char c in oComprobanteConcepto.ClaveProdServ)
                                            {
                                                if (!(c < '0' || c > '9'))
                                                {
                                                    ClaveProdServTr += c;
                                                }
                                            }
                                            oComprobanteConcepto.ClaveProdServ = ClaveProdServTr;
                                        }
                                    }
                                    catch (Exception errGeneral)
                                    {
                                        ErrorFX.mostrar(errGeneral, true, true, "generar33 - 1318 - Limpiar caracteres Invalidos de  ClaveProdServ", true);
                                        return false;
                                    }

                                    try
                                    {
                                        //Rodrigo Escalona 05/10/2018
                                        //NoIdentificacion
                                        //Si esta nulo agregar la misma 
                                        if (String.IsNullOrEmpty(oComprobanteConcepto.NoIdentificacion))
                                        {
                                            if (oComprobanteConcepto.Descripcion.Length > 0)
                                            {
                                                //Agregar la primera palabra de la descripción
                                                string[] arregloDescripcion = oComprobanteConcepto.Descripcion.Split(' ');
                                                oComprobanteConcepto.NoIdentificacion = arregloDescripcion[0];
                                            }
                                            else
                                            {
                                                //Agregar la clave del producto
                                                oComprobanteConcepto.NoIdentificacion = oComprobanteConcepto.ClaveProdServ;
                                            }
                                        }
                                    }
                                    catch (Exception errGeneral)
                                    {
                                        ErrorFX.mostrar(errGeneral, true, true, "generar33 - 1344 - Verificar el NoIdentificacion", true);
                                        return false;
                                    }

                                    try
                                    {
                                        //Rodrigo Escalona 11/07/2019 Si esta en nulo
                                        if (String.IsNullOrEmpty(oFACTURA.LINEAS[i].ClaveProdServ))
                                        {
                                            //oFACTURA.LINEAS[i].ClaveProdServ

                                            oComprobanteConcepto.ClaveProdServ = "84111506";

                                            BitacoraFX.Log("Agregar el 84111506 Numero de identificacion: "
                                                + oComprobanteConcepto.NoIdentificacion


                                                );
                                            BitacoraFX.Log("1334 - Buscar el numero de Producto para ser migrado "
                                                                    + oFACTURA.LINEAS[i].ClaveProdServ);
                                            if (oFACTURA.LINEAS[i].ClaveProdServ != "")
                                            {
                                                oComprobanteConcepto.ClaveProdServ = oFACTURA.LINEAS[i].ClaveProdServ;
                                            }

                                            oComprobanteConcepto.Unidad = "ACT";
                                        }
                                    }
                                    catch (Exception errGeneral)
                                    {
                                        ErrorFX.mostrar(errGeneral, true, true, "generar33 - 1375 - Verificar ClaveProdServ", true);
                                        return false;
                                    }

                                    try
                                    {
                                        //Agregar informacion Aduanera
                                        //Rodrigo Escalona: 24/11/2019 
                                        cPEDIMENTO ocPEDIMENTO = new cPEDIMENTO(oData, oEncapsular.ocEMPRESA);
                                        List<cPEDIMENTO> oPedimentos = new List<cPEDIMENTO>();
                                        oPedimentos = ocPEDIMENTO.obtener_linea(oFACTURA.INVOICE_ID, oFACTURA.LINEAS[i].LINEA, oEncapsular.ocEMPRESA);
                                        if (oPedimentos.Count > 0)
                                        {
                                            oComprobanteConcepto.InformacionAduanera = new ComprobanteConceptoInformacionAduanera[oPedimentos.Count];
                                            int contadorI = 0;
                                            foreach (cPEDIMENTO pedimento in oPedimentos)
                                            {
                                                oComprobanteConcepto.InformacionAduanera[contadorI] = new ComprobanteConceptoInformacionAduanera();
                                                oComprobanteConcepto.InformacionAduanera[contadorI].NumeroPedimento = pedimento.PEDIMENTO;
                                                contadorI++;
                                            }
                                        }
                                    }
                                    catch (Exception errGeneral)
                                    {
                                        ErrorFX.mostrar(errGeneral, true, true, "generar33 - 1391 - Verificar  Pedimentos ", true);
                                        return false;
                                    }

                                    try
                                    {
                                        if (!String.IsNullOrEmpty(oComprobanteConcepto.ClaveUnidad))
                                        {
                                            if (oComprobanteConcepto.ClaveUnidad.Equals("KGM"))
                                            {
                                                //Rodrigo Escalona 29/04/2021 Fima no quiere KF
                                                if (!oEncapsular.ocEMPRESA.ENTITY_ID.Contains("FIMA"))
                                                {
                                                    oComprobanteConcepto.Unidad = "KG";
                                                }
                                            }

                                            //Rodrigo Escalona 24/06/2021
                                            if (oComprobanteConcepto.ClaveUnidad.Equals("H87"))
                                            {
                                                //Rodrigo Escalona 29/04/2021 Fima no quiere KF
                                                oComprobanteConcepto.Unidad = "PZA";

                                            }
                                        }
                                        else
                                        {
                                            try
                                            {
                                                if (oComprobanteConcepto.Unidad.ToUpper() == "PZA")
                                                {
                                                    oComprobanteConcepto.ClaveUnidad = "H87";
                                                }
                                            }
                                            catch (Exception errGeneral)
                                            {
                                                ErrorFX.mostrar(errGeneral,false, true, "generar33 - 1446 - oComprobanteConcepto.Unidad Nulo", true);
                                                //return false;
                                                oComprobanteConcepto.Unidad = oComprobanteConcepto.ClaveUnidad;
                                            }

                                        }
                                    }
                                    catch (Exception errGeneral)
                                    {
                                        ErrorFX.mostrar(errGeneral, true, true, "generar33 - 1408 - Verificacion de Clave de Unidad", true);
                                        return false;
                                    }
                                    try
                                    {

                                        //Rodrigo Escalona 13/04/2021: Agregar la parte
                                        if (esKit(oFACTURA.LINEAS[i].PART_ID))
                                        {
                                            List<CFDI33.ComprobanteConceptoParte> oComprobanteConceptoPartes = new List<CFDI33.ComprobanteConceptoParte>();
                                            oComprobanteConceptoPartes = obtenerKit(oFACTURA.LINEAS[i].PART_ID, decimal.Parse(oFACTURA.LINEAS[i].QTY));
                                            if (oComprobanteConceptoPartes != null)
                                            {
                                                if (oComprobanteConceptoPartes.Count > 0)
                                                {
                                                    oComprobanteConcepto.Parte = new CFDI33.ComprobanteConceptoParte[oComprobanteConceptoPartes.Count];
                                                    oComprobanteConcepto.Parte = oComprobanteConceptoPartes.ToArray();
                                                }

                                            }
                                        }
                                    }
                                    catch (Exception errGeneral)
                                    {
                                        ErrorFX.mostrar(errGeneral, true, true, "generar33 - 1425 - Verificar la nulidad ClaveProdServ", true);
                                        return false;
                                    }
                                    //Rodrigo Escalona 26/04/2021 UM Actualizarlas Buscar el codigo en el maestro de Articulos
                                    try
                                    {
                                        if (!String.IsNullOrEmpty(oFACTURA.LINEAS[i].UM_CODE))
                                        {
                                            configuracionUM busquedatr = buscarClaveUnidad(oFACTURA.LINEAS[i].UM_CODE.Trim());
                                            if (busquedatr != null)
                                            {

                                                if (!oFACTURA.LINEAS[i].UM_CODE.Trim().Equals(busquedatr.SAT.Trim()))
                                                {
                                                    oComprobanteConcepto.ClaveUnidad = busquedatr.SAT.Trim();
                                                }
                                            }
                                        }
                                    }
                                    catch (Exception errGeneral)
                                    {
                                        ErrorFX.mostrar(errGeneral, true, true, "generar33 - 1439 - UM Actualizarlas Buscar el codigo en el maestro de Articulos", true);
                                        return false;
                                    }

                                    //Rodrigo Escalona 29/04/2021 Conversión a Pallets


                                    //Rodrigo Escalona 31/11/2021 Fima dice que no debe tener unidad 
                                    if (oEncapsular.ocEMPRESA.ENTITY_ID.Contains("FIMA"))
                                    {
                                        oComprobanteConcepto.Unidad = null;
                                    }

                                    //Rodrigo Escalona 31/11/2021 Losifra dice que los servicios deben buscar en la tabla
                                    if (oEncapsular.ocEMPRESA.SITE_ID.Contains("Losifra"))
                                    {
                                        if (!String.IsNullOrEmpty(oFACTURA.LINEAS[i].SERVICE_CHARGE_ID))
                                        {
                                            configuracionProducto oConfiguracionProducto = oFACTURA.buscarClave(string.Empty
                                                                , string.Empty, string.Empty
                                                                , oFACTURA.LINEAS[i].SERVICE_CHARGE_ID);
                                            oComprobanteConcepto.ClaveUnidad = oConfiguracionProducto.unidadSAT;
                                        }
                                    }

                                    //Rodrigo Escalona 31/11/2021 Verificar ue no este en blano la lae
                                    try
                                    {
                                        if (String.IsNullOrEmpty(oComprobanteConcepto.ClaveProdServ))
                                        {
                                            if (!String.IsNullOrEmpty(oFACTURA.LINEAS[i].PRODUCT_CODE))
                                            {
                                                configuracionProducto oConfiguracionProducto = oFACTURA.buscarClave(string.Empty
                                                                    , oFACTURA.LINEAS[i].PRODUCT_CODE, string.Empty
                                                                    , string.Empty);
                                                oComprobanteConcepto.ClaveProdServ = oConfiguracionProducto.clasificacionSAT;
                                                oComprobanteConcepto.ClaveUnidad = oConfiguracionProducto.unidadSAT;
                                            }
                                            
                                        }
                                    }
                                    catch (Exception errGeneral)
                                    {
                                        ErrorFX.mostrar(errGeneral, true, true, "generar33 - 1470 - Verificar ue no este en blanco ClaveProdServ", true);
                                        return false;
                                    }

                                    try
                                    {
                                        if (String.IsNullOrEmpty(oComprobanteConcepto.ClaveUnidad))
                                        {
                                            if (!String.IsNullOrEmpty(oFACTURA.LINEAS[i].PRODUCT_CODE))
                                            {
                                                configuracionProducto oConfiguracionProducto = oFACTURA.buscarClave(string.Empty
                                                                    , oFACTURA.LINEAS[i].PRODUCT_CODE, string.Empty
                                                                    , string.Empty);
                                                oComprobanteConcepto.ClaveProdServ = oConfiguracionProducto.clasificacionSAT;
                                                oComprobanteConcepto.ClaveUnidad = oConfiguracionProducto.unidadSAT;
                                            }
                                            else
                                            {
                                                if (!String.IsNullOrEmpty(oFACTURA.LINEAS[i].GL_ACCOUNT_ID))
                                                {
                                                    configuracionProducto oConfiguracionProducto = oFACTURA.buscarClave(string.Empty
                                                                        , string.Empty, oFACTURA.LINEAS[i].GL_ACCOUNT_ID
                                                                        , string.Empty);
                                                    oComprobanteConcepto.ClaveProdServ = oConfiguracionProducto.clasificacionSAT;
                                                    oComprobanteConcepto.ClaveUnidad = oConfiguracionProducto.unidadSAT;
                                                }
                                            }
                                            

                                        }
                                    }
                                    catch (Exception errGeneral)
                                    {
                                        ErrorFX.mostrar(errGeneral, true, true, "generar33 - 1536 - Verificar ue no este en blanco ClaveUnidad", true);
                                        return false;
                                    }

                                }
                                catch (Exception errGeneral)
                                {
                                    ErrorFX.mostrar(errGeneral, true, true, "generar33 - 1270 - Sección de validacion de  ClaveProdServ", true);
                                    return false;
                                }


                                //Verificar que si no tiene Clave de producto del SAT 
                                if (String.IsNullOrEmpty(oComprobanteConcepto.ClaveProdServ))
                                {
                                    oComprobanteConcepto.ClaveProdServ = "25201500";
                                }

                                if (String.IsNullOrEmpty(oComprobanteConcepto.ClaveUnidad))
                                {
                                    oComprobanteConcepto.ClaveUnidad = "H87";
                                }

                                oComprobanteConceptos[i] = oComprobanteConcepto;
                            }


                        }
                    }


                    oComprobante.Conceptos = oComprobanteConceptos;
                }
                catch (Exception errGeneral)
                {
                    ErrorFX.mostrar(errGeneral, true, true, "generar33 - 842 - Seccion de lineas ", true);
                    return false;
                }
                //MessageBox.Show("Comprobante22");

                try
                {
                    //MessageBox.Show("Comprobante23");
                    //Elemento Impuestos
                    CFDI33.ComprobanteImpuestos oImpuesto = new CFDI33.ComprobanteImpuestos();
                    //Impuestos  Trasladados

                    oImpuesto.TotalImpuestosTrasladadosSpecified = true;
                    if (oFACTURA.anticipo)
                    {
                        oImpuesto.TotalImpuestosTrasladados = Math.Abs(oFACTURA.TOTAL_VAT_AMOUNT) - oFACTURA.anticipoVAT_AMOUNT;
                        oImpuesto.TotalImpuestosTrasladados = Math.Round(oImpuesto.TotalImpuestosTrasladados, 2);
                    }
                    else
                    {
                        oImpuesto.TotalImpuestosTrasladados = Math.Abs(oFACTURA.TOTAL_VAT_AMOUNT);
                        oImpuesto.TotalImpuestosTrasladados = Math.Round(oImpuesto.TotalImpuestosTrasladados, 2);
                    }
                    //Rodrigo Escalona: 26/07/2018 Importe 2
                    oImpuesto.TotalImpuestosTrasladados = Math.Round(oImpuesto.TotalImpuestosTrasladados, 2);

                    //Impuestos  Trasladados
                    CFDI33.ComprobanteImpuestosTraslado[] oComprobanteImpuestosTraslados;
                    oComprobanteImpuestosTraslados = new CFDI33.ComprobanteImpuestosTraslado[1];
                    //IVA
                    CFDI33.ComprobanteImpuestosTraslado oComprobanteImpuestosTraslado = new CFDI33.ComprobanteImpuestosTraslado();
                    if (oFACTURA.anticipo)
                    {
                        oComprobanteImpuestosTraslado.Importe = Math.Abs(oFACTURA.TOTAL_VAT_AMOUNT) - oFACTURA.anticipoVAT_AMOUNT;
                        oComprobanteImpuestosTraslado.Importe = Math.Round(oComprobanteImpuestosTraslado.Importe, 2);
                    }
                    else
                    {
                        oComprobanteImpuestosTraslado.Importe = Math.Abs(oFACTURA.TOTAL_VAT_AMOUNT);
                        oComprobanteImpuestosTraslado.Importe = Math.Round(oComprobanteImpuestosTraslado.Importe, 2);
                    }
                    //Rodrigo Escalona: 26/07/2018 Importe 2
                    oComprobanteImpuestosTraslado.Importe = Math.Round(oComprobanteImpuestosTraslado.Importe, 2);

                    oComprobanteImpuestosTraslado.TasaOCuota = decimal.Parse("0.160000");//oFACTURA.VAT_PERCENT;
                    if (oComprobanteImpuestosTraslado.Importe == 0)
                    {
                        oComprobanteImpuestosTraslado.TasaOCuota = decimal.Parse("0.000000");//oFACTURA.VAT_PERCENT;
                    }
                    oComprobanteImpuestosTraslado.Impuesto = "002";//c_Impuesto.Item001;
                    oComprobanteImpuestosTraslado.TipoFactor = "Tasa";
                    oComprobanteImpuestosTraslados[0] = new CFDI33.ComprobanteImpuestosTraslado();
                    oComprobanteImpuestosTraslados[0] = oComprobanteImpuestosTraslado;
                    //IEPS
                    //oComprobanteImpuestosTraslado = new ComprobanteImpuestosTraslado();
                    //oComprobanteImpuestosTraslado.Importe = 0;
                    //oComprobanteImpuestosTraslado.TasaOCuota = 0;
                    //oComprobanteImpuestosTraslado.TipoFactor = "Tasa";
                    //oComprobanteImpuestosTraslado.Impuesto = "001";//c_Impuesto.Item003;
                    //oComprobanteImpuestosTraslados[1] = new ComprobanteImpuestosTraslado();
                    //oComprobanteImpuestosTraslados[1] = oComprobanteImpuestosTraslado;
                    oImpuesto.Traslados = oComprobanteImpuestosTraslados;
                    //Retenidos
                    if (oFACTURA.TOTAL_RETENIDO != 0)
                    {
                        CFDI33.ComprobanteImpuestosRetencion[] oComprobanteImpuestosRetencion = new CFDI33.ComprobanteImpuestosRetencion[1];
                        oComprobanteImpuestosRetencion[0] = new CFDI33.ComprobanteImpuestosRetencion();
                        oComprobanteImpuestosRetencion[0].Importe = Math.Abs(oFACTURA.TOTAL_RETENIDO);
                        oComprobanteImpuestosRetencion[0].Importe = Math.Round(oComprobanteImpuestosRetencion[0].Importe, 2);
                        oComprobanteImpuestosRetencion[0].Impuesto = "002";//c_Impuesto.Item001;
                        oImpuesto.Retenciones = oComprobanteImpuestosRetencion;
                        oImpuesto.TotalImpuestosRetenidosSpecified = true;
                        oImpuesto.TotalImpuestosRetenidos = Math.Abs(oFACTURA.TOTAL_RETENIDO);
                        oImpuesto.TotalImpuestosRetenidos = Math.Round(oImpuesto.TotalImpuestosRetenidos, 2);
                    }
                    oComprobante.Impuestos = oImpuesto;
                }
                catch (Exception errGeneral)
                {
                    ErrorFX.mostrar(errGeneral, true, true, "generar33 - 895 - Seccion de impuestos ", true);
                    return false;
                }


                //FIMA REDONDEO

                if (oEncapsular.ocEMPRESA.ENTITY_ID.Contains("FIMA"))
                {
                    //if (oComprobante.TipoDeComprobante.Equals("I"))
                    {
                        //Recalcular todo el XML con todos los decimales
                        decimal subTotal = 0;
                        decimal TotalImpuestosTrasladadosTr = 0;
                        int i = 0;
                        foreach (CFDI33.ComprobanteConcepto oComprobanteConcepto in oComprobante.Conceptos)
                        {
                            if (oComprobanteConcepto != null)
                            {
                                //subTotal += Math.Abs(oComprobanteConcepto.Cantidad * oComprobanteConcepto.ValorUnitario);
                                //oComprobante.Conceptos[i].Importe = Math.Abs(oComprobanteConcepto.Cantidad * oComprobanteConcepto.ValorUnitario);

                                subTotal += oComprobante.Conceptos[i].Importe;
                                //oComprobante.Conceptos[i].Importe = otroTruncar(oComprobante.Conceptos[i].Importe, oEncapsular.ocEMPRESA);
                                oComprobante.Conceptos[i].Impuestos.Traslados[0].Base = oComprobante.Conceptos[i].Importe;
                                if (oComprobante.Conceptos[i].Impuestos.Traslados[0].TasaOCuota != 0)
                                {
                                    oComprobante.Conceptos[i].Impuestos.Traslados[0].Importe = otroTruncar(oComprobante.Conceptos[i].Importe * oComprobante.Conceptos[i].Impuestos.Traslados[0].TasaOCuota, oEncapsular.ocEMPRESA);
                                    TotalImpuestosTrasladadosTr += oComprobante.Conceptos[i].Impuestos.Traslados[0].Importe;
                                }
                                else
                                {
                                    oComprobante.Conceptos[i].Impuestos.Traslados[0].Importe = 0;
                                }
                                i++;
                            }
                        }
                        //Modifico: 19/12/2017
                        oComprobante.SubTotal = otroTruncar(subTotal, oEncapsular.ocEMPRESA);

                        //Version 3.3
                        //oComprobante.SubTotal = Math.Truncate(100 * oComprobante.SubTotal) / 100;

                        oComprobante.TipoCambio = otroTruncar(oComprobante.TipoCambio, oEncapsular.ocEMPRESA);
                        //Tipo de cambio 6 decimales
                        //oComprobante.TipoCambio = otroTruncar(oComprobante.TipoCambio, oEncapsular.ocEMPRESA);


                        if (oComprobante.Impuestos.TotalImpuestosTrasladados != 0)
                        {
                            //oComprobante.Impuestos.totalImpuestosTrasladados = Math.Abs(subTotal)*decimal.Parse("0.16");
                            oComprobante.Impuestos.TotalImpuestosTrasladados = oComprobante.Total - oComprobante.SubTotal;
                            if (oComprobante.Impuestos.Traslados.Length != 0)
                            {
                                //Buscar el iva
                                int imp = 0;
                                decimal TotalImpuestosTrasladadosTrTotal = 0;
                                foreach (CFDI33.ComprobanteImpuestosTraslado impuesto in oComprobante.Impuestos.Traslados)
                                {
                                    if (impuesto.Impuesto == "001")//c_Impuesto.Item001)
                                    {
                                        oComprobante.Impuestos.Traslados[imp].Importe = TotalImpuestosTrasladadosTr;//oComprobante.Impuestos.TotalImpuestosTrasladados;
                                        TotalImpuestosTrasladadosTrTotal += oComprobante.Impuestos.Traslados[imp].Importe;
                                        break;
                                    }
                                    if (impuesto.Impuesto == "002")//c_Impuesto.Item001)
                                    {
                                        oComprobante.Impuestos.Traslados[imp].Importe = TotalImpuestosTrasladadosTr; //oComprobante.Impuestos.TotalImpuestosTrasladados;
                                        TotalImpuestosTrasladadosTrTotal += oComprobante.Impuestos.Traslados[imp].Importe;
                                        break;
                                    }
                                    imp++;
                                }

                                oComprobante.Impuestos.TotalImpuestosTrasladados = TotalImpuestosTrasladadosTrTotal;
                            }
                            oComprobante.Total = otroTruncar(Math.Abs(oComprobante.Impuestos.TotalImpuestosTrasladados) - Math.Abs(oComprobante.Impuestos.TotalImpuestosRetenidos) - Math.Abs(oComprobante.Descuento) + Math.Abs(oComprobante.SubTotal), oEncapsular.ocEMPRESA);
                        }
                        else
                        {
                            oComprobante.Total = otroTruncar(Math.Abs(oComprobante.SubTotal) - Math.Abs(oComprobante.Impuestos.TotalImpuestosRetenidos) - Math.Abs(oComprobante.Descuento), oEncapsular.ocEMPRESA);
                        }
                        //Fin de recalcular

                        //Formatear todo
                        //oComprobante.subTotal = otroTruncar(oComprobante.subTotal, ocEMPRESA);
                        //oComprobante.descuento = otroTruncar(oComprobante.descuento, ocEMPRESA);
                        //oComprobante.total = otroTruncar(oComprobante.total, ocEMPRESA);
                        //Lineas
                        oComprobante.Total = otroTruncar(oComprobante.Total, oEncapsular.ocEMPRESA);

                        i = 0;
                        foreach (CFDI33.ComprobanteConcepto oComprobanteConcepto in oComprobante.Conceptos)
                        {
                            if (oComprobanteConcepto != null)
                            {
                                oComprobante.Conceptos[i].Cantidad = otroTruncar(oComprobante.Conceptos[i].Cantidad, oEncapsular.ocEMPRESA);
                                //oComprobante.Conceptos[i].ValorUnitario = otroTruncar(oComprobante.Conceptos[i].ValorUnitario, oEncapsular.ocEMPRESA);
                                oComprobante.Conceptos[i].Importe = otroTruncar(oComprobante.Conceptos[i].Importe, oEncapsular.ocEMPRESA);
                                i++;
                            }
                        }
                        if (oComprobante.Impuestos.TotalImpuestosTrasladados != 0)
                        {
                            //oComprobante.Impuestos.totalImpuestosTrasladados = otroTruncar(oComprobante.Impuestos.totalImpuestosTrasladados, ocEMPRESA);
                            //oComprobante.Impuestos.TotalImpuestosTrasladados = oComprobante.Total - oComprobante.SubTotal;
                            ////CFDI 3.3
                            //oComprobante.Impuestos.TotalImpuestosTrasladados = Math.Truncate(100 * oComprobante.Impuestos.TotalImpuestosTrasladados) / 100;
                            decimal TotalImpuestosTrasladadostr = 0;
                            if (oComprobante.Impuestos.Traslados.Length != 0)
                            {
                                int imp = 0;
                                foreach (CFDI33.ComprobanteImpuestosTraslado impuesto in oComprobante.Impuestos.Traslados)
                                {
                                    if (impuesto.Impuesto == "001")//c_Impuesto.Item001)
                                    {
                                        oComprobante.Impuestos.Traslados[imp].Importe = otroTruncar(oComprobante.Impuestos.TotalImpuestosTrasladados, oEncapsular.ocEMPRESA); //otroTruncar(oComprobante.Impuestos.Traslados[imp].importe, ocEMPRESA);
                                        break;
                                    }
                                    if (impuesto.Impuesto == "002")//c_Impuesto.Item001)
                                    {
                                        TotalImpuestosTrasladadostr += otroTruncar(impuesto.Importe, oEncapsular.ocEMPRESA); //otroTruncar(oComprobante.Impuestos.Traslados[imp].importe, ocEMPRESA);
                                    }
                                    imp++;
                                }
                            }
                            oComprobante.Impuestos.TotalImpuestosTrasladados = Math.Truncate(100 * TotalImpuestosTrasladadostr) / 100;
                        }
                    }
                }

                //FIMA REDONDEO

                //Autocuadrar subtotal
                //23/04/2018 Rodrigo Escalona: 
                try
                {
                    if (bool.Parse(ConfigurationManager.AppSettings["recalcularSubTotal"].ToString()))
                    {
                        //Sumar las lineas 
                        decimal subtotal = 0;
                        decimal impuesto = 0;
                        decimal retencion = 0;
                        foreach (CFDI33.ComprobanteConcepto oComprobanteConcepto in oComprobante.Conceptos)
                        {
                            subtotal += oComprobanteConcepto.Importe;
                            if (oComprobanteConcepto.Impuestos != null)
                            {
                                if (oComprobanteConcepto.Impuestos.Traslados != null)
                                {
                                    foreach (ComprobanteConceptoImpuestosTraslado oImpuesto in oComprobanteConcepto.Impuestos.Traslados)
                                    {
                                        impuesto += oImpuesto.Importe;
                                    }
                                }
                                if (oComprobanteConcepto.Impuestos.Retenciones != null)
                                {
                                    foreach (ComprobanteConceptoImpuestosRetencion oImpuesto in oComprobanteConcepto.Impuestos.Retenciones)
                                    {
                                        retencion += Math.Abs(oImpuesto.Importe);
                                    }
                                }
                            }
                        }

                        //Cambiar los Totales y Subtotales
                        oComprobante.SubTotal = subtotal;
                        oComprobante.Total = subtotal + impuesto - retencion;
                    }
                }
                catch
                {

                }

                //Buscar complementos:
                int totalComplementos = 0;

                XmlElement oLeyenda = complementoLeyenda(oFACTURA, oEncapsular, oData);


                if (oLeyenda != null)
                {
                    leyendaFiscal = true;
                    totalComplementos++;
                }
                //Comercio Exterior
                cCCE ocCCE = new cCCE();
                XmlElement oCCE = null;
                bool cce = false;
                if (sinCEE)
                {
                    //Timbrar sin Complemento de CEE
                    cce = false;
                }
                else
                {
                    //Agregar el complemento
                    cce = ocCCE.agregarComplemento(oEncapsular.ocEMPRESA, oFACTURA, oComprobante.Impuestos.TotalImpuestosTrasladados);
                    if (complementoComercioExterior)
                    {
                        cce = complementoComercioExterior;
                    }
                    BitacoraFX.Log("Factura: " + oFACTURA.INVOICE_ID + " CCE:" + cce.ToString());
                    if (cce)
                    {
                        oCCE = complementoCCE(oComprobante, cce, oFACTURA, oEncapsular, ocCCE, ot_UbicacionReceptor, ocFACTURA_BANDEJAtmp);
                        if (oCCE != null)
                        {
                            oComprobante.Receptor.NumRegIdTrib = oFACTURA.TAX_ID_NUMBER;
                            oComprobante.Receptor.ResidenciaFiscal = ot_UbicacionReceptor.pais;

                            totalComplementos++;
                        }
                        else
                        {
                            //La factura requiere Complemento de Comercio Exterior dio un error su generación
                            ErrorFX.mostrar(" Factura " + oFACTURA.INVOICE_ID + " La factura " + oFACTURA.INVOICE_ID
                                + " requiere Complemento de Comercio Exterior dio un error su generación", !Globales.automatico, true, true);
                            return false;
                        }
                    }
                }

                //////////Complemento de Carta Porte de Traslado
                ////////XmlElement OCartePorte = null;
                ////////if (DoCartaPorte(oFACTURA.INVOICE_ID))
                ////////{
                ////////    OCartePorte = ComplementoCartaPorte(oFACTURA, oComprobante);
                ////////    if (OCartePorte != null)
                ////////    {
                ////////        //AGregar el namespace
                ////////        myNamespaces.Add("cartaporte20", "http://www.sat.gob.mx/CartaPorte20");
                ////////        totalComplementos++;
                ////////    }
                ////////}


                if (totalComplementos != 0)
                {
                    oComprobante.Complemento = new CFDI33.ComprobanteComplemento[1];
                    oComprobante.Complemento[0] = new CFDI33.ComprobanteComplemento();
                    oComprobante.Complemento[0].Any = new XmlElement[totalComplementos];
                    totalComplementos = 0;
                    if (oLeyenda != null)
                    {
                        oComprobante.Complemento[0].Any[totalComplementos] = oLeyenda;
                        totalComplementos++;

                    }
                    if (oCCE != null)
                    {
                        oComprobante.Complemento[0].Any[totalComplementos] = oCCE;
                        totalComplementos++;
                    }
                    ////if (OCartePorte != null)
                    ////{
                    ////    oComprobante.Complemento[0].Any[totalComplementos] = OCartePorte;
                    ////    totalComplementos++;
                    ////}
                }



            }
            catch (Exception errGeneral)
            {
                ErrorFX.mostrar(errGeneral, true, true, "generar33 - 913 ", true);
                return false;
            }

            try
            {
                if (!String.IsNullOrEmpty(ocSERIE.CLIENTE_TRASLADO))
                {
                    if (oFACTURA.CUSTOMER_ID.Contains(ocSERIE.CLIENTE_TRASLADO))
                    {
                        try
                        {
                            oComprobante.TipoDeComprobante = "T";
                            oComprobante.CondicionesDePago = null;
                            oComprobante.MetodoPago = null;
                            oComprobante.FormaPago = null;
                            oComprobante.SubTotal = 0;
                            oComprobante.Total = 0;
                            if (oComprobante.Conceptos != null)
                            {
                                foreach (CFDI33.ComprobanteConcepto concepto in oComprobante.Conceptos)
                                {
                                    concepto.ValorUnitario = 0;
                                    concepto.Importe = 0;
                                    concepto.Impuestos = null;
                                }

                            }
                            oComprobante.Impuestos = null;
                        }
                        catch (Exception errTraslado)
                        {
                            ErrorFX.mostrar(errTraslado, true, true, "generar33 - Lineas Traslado - 1648  ");
                            return false;
                        }
                    }
                }
            }
            catch (Exception errTraslado)
            {
                ErrorFX.mostrar(errTraslado, true, true, "generar33 - Traslado - 1634  ");
                return false;
            }


            XslCompiledTransform myXslTrans = new XslCompiledTransform();

            XslCompiledTransform trans = new XslCompiledTransform();
            string nombre_tmp = oFACTURA.INVOICE_ID + "TMP" + DateTime.Now.ToString("ddMMyyyyhhmmss");
            if (File.Exists(nombre_tmp + ".XML"))
            {
                File.Delete(nombre_tmp + ".XML");
            }

            XPathDocument myXPathDoc;
            XmlSerializer serializer = new XmlSerializer(typeof(CFDI33.Comprobante));
            try
            {


                TextWriter writer = new StreamWriter(nombre_tmp + ".XML");
                serializer.Serialize(writer, oComprobante, myNamespaces);
                writer.Close();

                myXPathDoc = new XPathDocument(nombre_tmp + ".XML");
                //XslTransform myXslTrans = new XslTransform();
                myXslTrans = new XslCompiledTransform();


                trans = new XslCompiledTransform();
                XmlUrlResolver resolver = new XmlUrlResolver();

                resolver.Credentials = System.Net.CredentialCache.DefaultCredentials;
                XsltSettings settings = new XsltSettings(true, true);
            }
            catch (XmlException errores)
            {
                ErrorFX.mostrar(errores, true, true, "generar33 - 949 ", false);
                return false;
            }

            string sDirectory = AppDomain.CurrentDomain.BaseDirectory;
            //Cargar xslt
            try
            {
                //Version 3
                myXslTrans.Load(sDirectory + "/XSLT/cadenaoriginal_3_3.xslt");
            }
            catch (XmlException errores)
            {
                ErrorFX.mostrar(errores, true, true, "generar33 - 961 ", false);
                return false;
            }

            //Crear Archivo Temporal

            XmlTextWriter myWriter = new XmlTextWriter(nombre_tmp + ".TXT", null);

            //Transformar al XML
            myXslTrans.Transform(myXPathDoc, null, myWriter);

            myWriter.Close();

            //Abrir Archivo TXT
            string CADENA_ORIGINAL = "";
            StreamReader re = File.OpenText(nombre_tmp + ".TXT");
            string input = null;
            while ((input = re.ReadLine()) != null)
            {
                CADENA_ORIGINAL += input;
            }
            re.Close();

            //Eliminar Archivos Temporales
            File.Delete(nombre_tmp + ".TXT");
            File.Delete(nombre_tmp + ".XML");

            //Buscar el &amp; en la Cadena y sustituirlo por &
            CADENA_ORIGINAL = CADENA_ORIGINAL.Replace("&amp;", "&");



            //MessageBox.Show("Generar Sello.");
            oComprobante.Sello = SelloRSA(ocCERTIFICADO.LLAVE, ocCERTIFICADO.PASSWORD, CADENA_ORIGINAL, FECHA_FACTURA.Year); ;

            string DIRECTORIO_ARCHIVOS = "";
            //if (!Directory.Exists(ocSERIE.DIRECTORIO))
            //{
            //    ocSERIE.DIRECTORIO=Application.StartupPath;
            //}


            DIRECTORIO_ARCHIVOS += ocSERIE.DIRECTORIO + @"\" + ocSERIE.ID + @"\" + Fechammmyy(oFACTURA.INVOICE_DATE.ToString());
            string XML = DIRECTORIO_ARCHIVOS;

            //Rodrigo Escalona: Unificar directorio
            string prefijoPDF = "pdf";
            try
            {
                if (ocSERIE.UnirCarpeta)
                {
                    prefijoPDF = "";
                }
            }
            catch
            {

            }

            string PDF = XML + @"\" + prefijoPDF;
            if (!Directory.Exists(PDF))
            {
                try
                {

                    Directory.CreateDirectory(PDF);
                }
                catch (IOException excep)
                {
                    MessageBox.Show(excep.Message.ToString() + Environment.NewLine + "Creando directorio de PDF"
                        , Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return false;
                }
            }
            //Rodrigo Escalona: Unificar directorio
            string prefijoXML = "xml";
            try
            {
                if (ocSERIE.UnirCarpeta)
                {
                    prefijoXML = "";
                }
            }
            catch
            {

            }
            XML += @"\" + prefijoXML;
            if (Directory.Exists(XML) == false)
                Directory.CreateDirectory(XML);

            string XML_Archivo = "";

            string nombre_archivo = oFACTURA.INVOICE_ID;
            if (ocSERIE.ARCHIVO_NOMBRE != "")
            {
                nombre_archivo = ocSERIE.ARCHIVO_NOMBRE.Replace("{FACTURA}", oFACTURA.INVOICE_ID).Replace("{CLIENTE}", oFACTURA.CUSTOMER_ID)
                    ;
            }


            if (Directory.Exists(DIRECTORIO_ARCHIVOS))
            {
                XML_Archivo = XML + @"\" + nombre_archivo + ".xml";
            }
            else
            {
                XML_Archivo = @"\" + nombre_archivo + ".xml"; ;
            }

            XML_Archivo = XML + @"\" + nombre_archivo + ".xml";

            //MessageBox.Show("Cargado XLT");

            //MessageBox.Show("Cargado XLT " + ocCERTIFICADO.CERTIFICADO);
            if (!File.Exists(ocCERTIFICADO.CERTIFICADO))
            {
                MessageBox.Show("No se tiene acceso al archivo " + ocCERTIFICADO.CERTIFICADO + "", Application.ProductName + "-" + Application.ProductVersion, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);

                return false;
            }
            TextWriter writerXML = new StreamWriter(XML_Archivo);
            serializer.Serialize(writerXML, oComprobante, myNamespaces);
            writerXML.Close();
            writerXML.Dispose();
            parsar_d1p1(XML_Archivo, leyendaFiscal);
            //Guardar Datos
            //MessageBox.Show("Guardar datos en bandeja.");
            string PDF_Archivo = "";
            PDF_Archivo = PDF + @"\" + nombre_archivo + ".pdf";
            cFACTURA ocFACTURA = new cFACTURA(oEncapsular.oData_ERP);
            ocFACTURA.datos(oFACTURA.INVOICE_ID, "", oFACTURA.ADDR_NO, oEncapsular.ocEMPRESA, "");
            cFACTURA_BANDEJA ocFACTURA_BANDEJA = new cFACTURA_BANDEJA(oEncapsular.oData_ERP, oEncapsular.ocEMPRESA);
            ocFACTURA_BANDEJA.INVOICE_ID = oFACTURA.INVOICE_ID;
            ocFACTURA_BANDEJA.XML = XML_Archivo;
            ocFACTURA_BANDEJA.ESTADO = "Creada Facturación Electrónica";
            ocFACTURA_BANDEJA.CADENA = CADENA_ORIGINAL;
            ocFACTURA_BANDEJA.SELLO = oComprobante.Sello;
            ocFACTURA_BANDEJA.SERIE = oComprobante.Serie;
            ocFACTURA_BANDEJA.USO_CFDI = oComprobante.Receptor.UsoCFDI;
            ocFACTURA_BANDEJA.PDF = PDF_Archivo;
            ocFACTURA_BANDEJA.VERSION = oComprobante.Version;

            ocFACTURA_BANDEJA.FORMA_DE_PAGO = string.Empty;
            if (!string.IsNullOrEmpty(oComprobante.FormaPago))
            {
                ocFACTURA_BANDEJA.FORMA_DE_PAGO = oComprobante.FormaPago.ToString();
            }

            ocFACTURA_BANDEJA.METODO_DE_PAGO = string.Empty;
            if (!string.IsNullOrEmpty(oComprobante.MetodoPago))
            {
                ocFACTURA_BANDEJA.METODO_DE_PAGO = oComprobante.MetodoPago.ToString();
            }

            ocFACTURA_BANDEJA.CTA_BANCO = "";// oComprobante.NumCtaPago.ToString(); //ocFACTURA.CUENTA_BANCARIA;
            ocFACTURA_BANDEJA.ADDR_NO = oFACTURA.ADDR_NO;
            return ocFACTURA_BANDEJA.guardar();

        }

        private bool DoCartaPorte(string invoice)
        {

            ModeloCartaPorte.CartaPorteEntities Db = new ModeloCartaPorte.CartaPorteEntities();
            ModeloCartaPorte.CartaPorte OCartaPorte = Db.CartaPorteSet
                .Where(a => a.InvoiceId.Equals(invoice))
                .FirstOrDefault();
            if (OCartaPorte != null)
            {
                return true;
            }
            Db.Dispose();
            return false;
        }

        private XmlElement ComplementoCartaPorte(cFACTURA oFACTURA, CFDI33.Comprobante oComprobante)
        {
            try
            {
                //Crear el Traslado
                oComprobante.TipoDeComprobante = "T";
                oComprobante.CondicionesDePago = null;
                oComprobante.MetodoPago = null;
                oComprobante.FormaPago = null;
                oComprobante.Moneda = "XXX";
                oComprobante.TipoCambio = 0;
                oComprobante.TipoCambioSpecified = false;
                oComprobante.SubTotal = 0;
                oComprobante.Total = 0;
                if (oComprobante.Conceptos != null)
                {
                    foreach (CFDI33.ComprobanteConcepto concepto in oComprobante.Conceptos)
                    {
                        concepto.ValorUnitario = 0;
                        concepto.Importe = 0;
                        concepto.Impuestos = null;
                    }

                }
                oComprobante.Impuestos = null;

                ModeloCartaPorte.CartaPorteEntities Db = new ModeloCartaPorte.CartaPorteEntities();
                ModeloCartaPorte.CartaPorte OCartaPorteDb = Db.CartaPorteSet
                    .Where(a => a.InvoiceId.Equals(oFACTURA.INVOICE_ID))
                    .FirstOrDefault();
                if (OCartaPorteDb == null)
                {
                    throw new Exception("La factura " + oFACTURA.INVOICE_ID + " no tiene datos de Carta Porte - 2035");
                }
                CartaPorte OCartaPorte = new CartaPorte();
                OCartaPorte.ViaEntradaSalida = c_CveTransporte.Item01;
                OCartaPorte.TranspInternac = CartaPorteTranspInternac.No;
                OCartaPorte.TotalDistRec = 1;
                OCartaPorte.TotalDistRecSpecified = true;

                OCartaPorte.Ubicaciones = new CartaPorteUbicacion[2];
                OCartaPorte.Ubicaciones[0] = new CartaPorteUbicacion();
                OCartaPorte.Ubicaciones[0].TipoUbicacion = CartaPorteUbicacionTipoUbicacion.Origen;
                OCartaPorte.Ubicaciones[0].RFCRemitenteDestinatario = oComprobante.Emisor.Rfc;
                OCartaPorte.Ubicaciones[0].FechaHoraSalidaLlegada = DateTime.Now.ToString("yyyy-MM-ddTHH:mm:ss");


                OCartaPorte.Ubicaciones[1] = new CartaPorteUbicacion();
                OCartaPorte.Ubicaciones[1].TipoUbicacion = CartaPorteUbicacionTipoUbicacion.Destino;
                OCartaPorte.Ubicaciones[1].RFCRemitenteDestinatario = oComprobante.Receptor.Rfc;
                OCartaPorte.Ubicaciones[1].FechaHoraSalidaLlegada = DateTime.Now.AddHours(2).ToString("yyyy-MM-ddTHH:mm:ss");
                OCartaPorte.Ubicaciones[1].DistanciaRecorrida = 1;
                OCartaPorte.Ubicaciones[1].DistanciaRecorridaSpecified = true;

                OCartaPorte.Mercancias = new CartaPorteMercancias();
                OCartaPorte.Mercancias.UnidadPeso = c_ClaveUnidadPeso.KGM;
                OCartaPorte.Mercancias.Mercancia = new CartaPorteMercanciasMercancia[oComprobante.Conceptos.Length];
                int i = 0;
                decimal PesoBrutoTotal = 0;
                int NumTotalMercancias = 0;
                foreach (CFDI33.ComprobanteConcepto oComprobanteConcepto in oComprobante.Conceptos)
                {
                    OCartaPorte.Mercancias.Mercancia[i] = new CartaPorteMercanciasMercancia();
                    OCartaPorte.Mercancias.Mercancia[i].Cantidad = oComprobanteConcepto.Cantidad;
                    NumTotalMercancias += Decimal.ToInt32(oComprobanteConcepto.Cantidad);

                    OCartaPorte.Mercancias.Mercancia[i].ClaveUnidad = c_ClaveUnidad.H87;

                    c_ClaveProdServCP Oc_ClaveProdServCP = (c_ClaveProdServCP)Enum.Parse(typeof(c_ClaveProdServCP), "Item" + oComprobanteConcepto.ClaveProdServ);
                    OCartaPorte.Mercancias.Mercancia[i].BienesTransp = Oc_ClaveProdServCP;
                    //Autotransporte
                    oComprobanteConcepto.ClaveProdServ = "78101800";

                    OCartaPorte.Mercancias.Mercancia[i].Descripcion = oComprobanteConcepto.Descripcion;

                    OCartaPorte.Mercancias.Mercancia[i].PesoEnKg = oComprobanteConcepto.Cantidad;
                    PesoBrutoTotal += OCartaPorte.Mercancias.Mercancia[i].PesoEnKg;

                    //OCartaPorte.Mercancias.Mercancia[i].MaterialPeligroso = 0;
                    //OCartaPorte.Mercancias.Mercancia[i].MaterialPeligrosoSpecified = true;

                    OCartaPorte.Mercancias.Mercancia[i].MonedaSpecified = true;
                    OCartaPorte.Mercancias.Mercancia[i].Moneda = c_Moneda.MXN;
                    if (oComprobante.Moneda == "USD")
                    {
                        OCartaPorte.Mercancias.Mercancia[i].Moneda = c_Moneda.USD;
                    }

                    //OCartaPorte.Mercancias.Mercancia[i].ValorMercancia = oComprobanteConcepto.Importe;
                    //OCartaPorte.Mercancias.Mercancia[i].ValorMercanciaSpecified = true;

                    //Agregar los impuestos de cada uno
                    //oComprobanteConcepto.Impuestos = new ComprobanteConceptoImpuestos();
                    //oComprobanteConcepto.Impuestos.Retenciones = new ComprobanteConceptoImpuestosRetencion[1];
                    //oComprobanteConcepto.Impuestos.Retenciones[0] = new ComprobanteConceptoImpuestosRetencion();
                    //oComprobanteConcepto.Impuestos.Retenciones[0].TasaOCuota =

                    //oComprobanteConcepto.Impuestos.Traslados = new ComprobanteConceptoImpuestosTraslado[1];


                    i++;
                }
                OCartaPorte.Mercancias.NumTotalMercancias = OCartaPorte.Mercancias.Mercancia.Length;
                OCartaPorte.Mercancias.PesoBrutoTotal = PesoBrutoTotal;

                OCartaPorte.Mercancias.Autotransporte = new CartaPorteMercanciasAutotransporte();
                OCartaPorte.Mercancias.Autotransporte.PermSCT = c_TipoPermiso.TPAF01;
                OCartaPorte.Mercancias.Autotransporte.NumPermisoSCT = OCartaPorteDb.Transporte.NumeroPermisoSCT;
                OCartaPorte.Mercancias.Autotransporte.IdentificacionVehicular = new CartaPorteMercanciasAutotransporteIdentificacionVehicular();
                OCartaPorte.Mercancias.Autotransporte.IdentificacionVehicular.PlacaVM = OCartaPorteDb.Transporte.PlacaRemolque;
                OCartaPorte.Mercancias.Autotransporte.IdentificacionVehicular.AnioModeloVM = DateTime.Now.Year;
                try
                {
                    OCartaPorte.Mercancias.Autotransporte.IdentificacionVehicular.AnioModeloVM = int.Parse(OCartaPorteDb.Transporte.Anio);
                }
                catch
                {

                }

                OCartaPorte.Mercancias.Autotransporte.Seguros = new CartaPorteMercanciasAutotransporteSeguros();
                OCartaPorte.Mercancias.Autotransporte.Seguros.PolizaRespCivil = OCartaPorteDb.Transporte.PolizaResponsabilidadCivil;
                OCartaPorte.Mercancias.Autotransporte.Seguros.AseguraRespCivil = OCartaPorteDb.Transporte.AseguradoraResponsabilidadCivil;

                OCartaPorte.FiguraTransporte = new CartaPorteTiposFigura[1];
                OCartaPorte.FiguraTransporte[0] = new CartaPorteTiposFigura();
                OCartaPorte.FiguraTransporte[0].TipoFigura = c_FiguraTransporte.Item01;
                OCartaPorte.FiguraTransporte[0].RFCFigura = OCartaPorteDb.Transportista.RFC;
                OCartaPorte.FiguraTransporte[0].NumLicencia = OCartaPorteDb.Transportista.NumeroLicencia;

                XmlDocument Doc = new XmlDocument();
                XmlSerializerNamespaces myNamespaces_comercio = new XmlSerializerNamespaces();
                //Version Carta Porte de llenado
                //namespaces.Add("myns", MyElement.ElementNamespace);
                myNamespaces_comercio.Add("cartaporte20", CartaPorte.ElementNamespace);
                using (XmlWriter writer = Doc.CreateNavigator().AppendChild())
                {
                    new XmlSerializer(OCartaPorte.GetType()).Serialize(writer, OCartaPorte, myNamespaces_comercio);
                }
                return Doc.DocumentElement;
            }
            catch (Exception exCCE)
            {
                ErrorFX.mostrar(exCCE, true, true, "generar33 - Carta Porte - 2010", true);
                return null;
            }
            return null;
        }

        private List<CFDI33.ComprobanteConceptoParte> obtenerKit(string pART_ID, decimal qTY)
        {
            List<CFDI33.ComprobanteConceptoParte> lista = new List<CFDI33.ComprobanteConceptoParte>();
            ModeloDocumentosFX.ModeloDocumentosFXContainer db = new ModeloDocumentosFX.ModeloDocumentosFXContainer();
            var oConfiguracionPartes = db.configuracionPartesSet.Where(a => a.producto.Equals(pART_ID));
            if (oConfiguracionPartes != null)
            {
                foreach (configuracionPartes registro in oConfiguracionPartes)
                {
                    CFDI33.ComprobanteConceptoParte oParte = new CFDI33.ComprobanteConceptoParte();
                    oParte.Cantidad = otroTruncar(registro.Cantidad * qTY);
                    oParte.ClaveProdServ = registro.ClaveProdServ;
                    oParte.Descripcion = registro.Descripcion;
                    oParte.Importe = Math.Round(oParte.Cantidad * registro.ValorUnitario,2);
                    oParte.ImporteSpecified = true;
                    oParte.ValorUnitarioSpecified = true;
                    oParte.ValorUnitario = registro.ValorUnitario;//otroTruncar(registro.ValorUnitario);
                    oParte.NoIdentificacion = registro.NoIdentificacion;
                    oParte.Unidad = registro.Unidad;
                    lista.Add(oParte);
                }
            }
            return lista;
        }

        private bool esKit(string pART_ID)
        {
            ModeloDocumentosFX.ModeloDocumentosFXContainer db = new ModeloDocumentosFX.ModeloDocumentosFXContainer();
            configuracionPartes oConfiguracionPartes = db.configuracionPartesSet.Where(a => a.producto.Equals(pART_ID)).FirstOrDefault();
            if (oConfiguracionPartes == null)
            {
                return false;
            }
            return true;
        }


        public CFDI33.Comprobante generarComprobantePago(ModeloComprobantePago.pago oPago, Encapsular oEncapsular, bool autoAjustar = false)
        {
            try
            {
                ModeloComprobantePago.documentosfxEntities dbContext = new ModeloComprobantePago.documentosfxEntities();
                string conection = dbContext.Database.Connection.ConnectionString;
                cCONEXCION oData = new cCONEXCION(conection);

                CFDI33.Comprobante oComprobante = new CFDI33.Comprobante();
                XmlSerializerNamespaces myNamespaces = new XmlSerializerNamespaces();
                myNamespaces.Add("cfdi", "http://www.sat.gob.mx/cfd/3");

                try
                {
                    cCERTIFICADO ocCERTIFICADO = new cCERTIFICADO();
                    ocCERTIFICADO.cargar_NumCert(oPago.noCertificado);

                    //BitacoraFX.Log("generar33 - 1533 " + oPago.noCertificado);

                    cSERIE ocSERIE = new cSERIE();
                    try
                    {
                        //BitacoraFX.Log("generar33 - 1541 " + ocCERTIFICADO.NO_CERTIFICADO);
                        X509Certificate cert = X509Certificate.CreateFromCertFile(ocCERTIFICADO.CERTIFICADO);
                        string NoSerie = cert.GetSerialNumberString();
                        string Certificado64 = System.Convert.ToBase64String(cert.GetRawCertData());
                        oComprobante.Certificado = Certificado64;
                        oComprobante.NoCertificado = timbrado33.validacion(oPago.noCertificado);


                    }
                    catch (Exception e)
                    {
                        ErrorFX.mostrar(e, true, true, "generar33 - Busqueda o creación de Certificado - 1517 ");
                        return null;
                    }

                    oComprobante.TipoDeComprobante = "P";
                    oComprobante.Folio = oPago.folio;
                    oComprobante.Serie = oPago.serie;

                    DateTime oFechatr = new DateTime(oPago.fecha.Year, oPago.fecha.Month, oPago.fecha.Day, oPago.fecha.Hour, oPago.fecha.Minute, 0);

                    oComprobante.Fecha = oFechatr;

                    DateTime utcTime = DateTime.Now.ToUniversalTime();
                    string nzTimeZoneKey = "Pacific Standard Time (Mexico)";
                    TimeZoneInfo nzTimeZone = TimeZoneInfo.FindSystemTimeZoneById(nzTimeZoneKey);
                    DateTime nzDateTime = TimeZoneInfo.ConvertTimeFromUtc(utcTime, nzTimeZone);



                    oComprobante.Fecha = new DateTime(nzDateTime.Year, nzDateTime.Month, nzDateTime.Day, nzDateTime.Hour, nzDateTime.Minute, 0);


                    oComprobante.NoCertificado = oPago.noCertificado;
                    oComprobante.SubTotal = 0;
                    oComprobante.Moneda = "XXX";
                    oComprobante.Total = 0;
                    oComprobante.LugarExpedicion = oPago.lugarExpedicion;

                    oComprobante.Emisor = new CFDI33.ComprobanteEmisor();
                    oComprobante.Emisor.Rfc = oPago.emisor;
                    oComprobante.Emisor.Nombre = oPago.emisorNombre;
                    oComprobante.Emisor.RegimenFiscal = oPago.emisorRegimenFiscal;

                    oComprobante.Receptor = new CFDI33.ComprobanteReceptor();
                    oComprobante.Receptor.Rfc = oPago.receptor;
                    oComprobante.Receptor.Nombre = oPago.receptorNombre;

                    if (!String.IsNullOrEmpty(oPago.receptorNumRegIdTrib))
                    {
                        //oComprobante.Receptor.NumRegIdTrib = oPago.receptorNumRegIdTrib;

                    }

                    //Agregar complementos relacionados
                    var pagos = dbContext.pagoRelacionadoSet.Where(b => b.checkId == oPago.checkId
                                    & b.customerId == oPago.customerId);
                    if (pagos != null)
                    {
                        if (pagos.LongCount() != 0)
                        {
                            oComprobante.CfdiRelacionados = new CFDI33.ComprobanteCfdiRelacionados();

                            ComprobanteCfdiRelacionados devolver = new CFDI33.ComprobanteCfdiRelacionados();
                            devolver.CfdiRelacionado = new CFDI33.ComprobanteCfdiRelacionadosCfdiRelacionado[pagos.LongCount()];
                            int i = 0;
                            foreach (pagoRelacionado pagoR in pagos)
                            {
                                devolver.TipoRelacion = pagoR.tipoRelacion;
                                devolver.CfdiRelacionado[i] = new CFDI33.ComprobanteCfdiRelacionadosCfdiRelacionado();
                                devolver.CfdiRelacionado[i].UUID = pagoR.UUID;
                                i++;
                            }

                            oComprobante.CfdiRelacionados = devolver;
                        }


                    }


                    oComprobante.Receptor.UsoCFDI = "P01";
                    //Agregar el concepto
                    oComprobante.Conceptos = new CFDI33.ComprobanteConcepto[1];
                    oComprobante.Conceptos[0] = new CFDI33.ComprobanteConcepto();
                    oComprobante.Conceptos[0].Cantidad = 1;
                    oComprobante.Conceptos[0].ClaveUnidad = "ACT";
                    oComprobante.Conceptos[0].Descripcion = "Pago";
                    oComprobante.Conceptos[0].ValorUnitario = 0;
                    oComprobante.Conceptos[0].ClaveProdServ = "84111506";
                    oComprobante.Conceptos[0].Importe = 0;

                    //Agregar el complemento de pago

                    XmlElement oPagoComplemento = null;
                    oPagoComplemento = complementoPago10(oPago, oEncapsular, autoAjustar);



                    oComprobante.Complemento = new CFDI33.ComprobanteComplemento[1];
                    oComprobante.Complemento[0] = new CFDI33.ComprobanteComplemento();
                    oComprobante.Complemento[0].Any = new XmlElement[1];
                    oComprobante.Complemento[0].Any[0] = oPagoComplemento;

                    string cadena = timbrado33.cadena(oComprobante);


                    ocSERIE.cargar_serie(oComprobante.Serie, ocCERTIFICADO.ROW_ID_EMPRESA);
                    if (String.IsNullOrEmpty(ocSERIE.ID))
                    {
                        ErrorFX.mostrar("generar33 - 1613 - La serie " + oComprobante.Serie + " del Certificado " + ocCERTIFICADO.NO_CERTIFICADO + " Empresa Id: " + ocCERTIFICADO.ROW_ID_EMPRESA
                            + " no existe", false, false, false);

                        ocSERIE.cargar_serie(oComprobante.Serie);
                        if (String.IsNullOrEmpty(ocSERIE.ID))
                        {
                            ErrorFX.mostrar("generar33 - 1613 - La serie " + oComprobante.Serie + " del Certificado " + ocCERTIFICADO.NO_CERTIFICADO + " Empresa Id: " + ocCERTIFICADO.ROW_ID_EMPRESA
                            + " no existe", true, false, false);
                            return null;
                        }
                    }
                    BitacoraFX.Log("generar33 - 1541 " + ocCERTIFICADO.NO_CERTIFICADO + " ocCERTIFICADO.PASSWORD=" + ocCERTIFICADO.PASSWORD);

                    oComprobante.Sello = timbrado33.SelloRSA(ocCERTIFICADO.LLAVE, ocCERTIFICADO.PASSWORD, cadena, oComprobante.Fecha.Year);

                    string archivotr = timbrado33.guardarComprobante(oComprobante, oPago, ocSERIE, oPago.estado, oPago.fechaPago);
                    if (!String.IsNullOrEmpty(archivotr))
                    {
                        oPago.xml = archivotr;

                        //Guardar
                        ModeloComprobantePago.pago oRegistro = dbContext.pagoSet.Where(a => a.Id == oPago.Id).FirstOrDefault();
                        oRegistro.xml = archivotr;
                        oRegistro.sello = oComprobante.Sello;
                        dbContext.pagoSet.Attach(oRegistro);
                        dbContext.Entry(oRegistro).State = System.Data.Entity.EntityState.Modified;
                        dbContext.SaveChanges();
                    }

                    return oComprobante;
                }
                catch (Exception errores)
                {
                    ErrorFX.mostrar(errores, true, true, "generar33 - 1432 - generarComprobantePago ", false);
                    return null;
                }


            }
            catch (Exception errores)
            {
                ErrorFX.mostrar(errores, true, true, "generar33 - 1436 - generarComprobantePago ", false);
                return null;
            }
        }


        private ComprobanteCfdiRelacionados cargarComprobanteCfdiRelacionados(string INVOICE_ID
            , Encapsular oEncapsular, cFACTURA ocFACTURA)
        {
            ModeloDocumentosFXContainer dbContext = new ModeloDocumentosFXContainer();
            try
            {
                bool repetir = true;
                do
                {
                    AgregarRMA(INVOICE_ID, oEncapsular);

                    var result = from b in dbContext.ndcFacturaSet
                    .Where(a => a.INVOICE_ID == INVOICE_ID)
                                 select b;
                    List<ndcFactura> dt = result.ToList();
                    if (dt.Count == 0)
                    {
                        if (Globales.automatico)
                        {
                            return null;
                        }
                        if (!Globales.preguntarRelacionados)
                        {
                            return null;
                        }
                        DialogResult Resultado = MessageBox.Show("¿Desea cargar los CFDI relacionados?", Application.ProductName + "-" + Application.ProductVersion, MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                        if (Resultado.ToString() != "Yes")
                        {
                            return null;
                        }

                        //if (!ocFACTURA.TYPE.Equals("I"))
                        //{
                        //    DialogResult Resultado = MessageBox.Show("¿Desea cargar los CFDI relacionados?", Application.ProductName + "-" + Application.ProductVersion, MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                        //    if (Resultado.ToString() != "Yes")
                        //    {
                        //        return null;
                        //    }
                        //}
                        //else
                        //{
                        //    return null;
                        //}
                        //Cargar la lista
                        if (oEncapsular.ocEMPRESA.relacionLinea)
                        {
                            ClasificadorProductos.formularios.frmTipoRelacionNDCnormalSimplificado ofrmTipoRelacionNDC =
                            new ClasificadorProductos.formularios.frmTipoRelacionNDCnormalSimplificado(oEncapsular.ocEMPRESA.ENTITY_ID
                                , ocFACTURA.INVOICE_ID, ocFACTURA.CUSTOMER_ID);
                            ofrmTipoRelacionNDC.ShowDialog();
                        }
                        else
                        {
                            ClasificadorProductos.formularios.frmTipoRelacionNDCfactura ofrmTipoRelacionNDC =
                                new ClasificadorProductos.formularios.frmTipoRelacionNDCfactura(oEncapsular.ocEMPRESA.ENTITY_ID
                                    , ocFACTURA.INVOICE_ID, ocFACTURA.CUSTOMER_ID);
                            ofrmTipoRelacionNDC.ShowDialog();
                        }

                    }
                    else
                    {
                        ComprobanteCfdiRelacionados devolver = new CFDI33.ComprobanteCfdiRelacionados();
                        devolver.CfdiRelacionado = new CFDI33.ComprobanteCfdiRelacionadosCfdiRelacionado[dt.Count];
                        int i = 0;
                        foreach (ndcFactura registro in dt)
                        {
                            devolver.TipoRelacion = registro.tipoRelacion;
                            devolver.CfdiRelacionado[i] = new CFDI33.ComprobanteCfdiRelacionadosCfdiRelacionado();
                            devolver.CfdiRelacionado[i].UUID = registro.UUID;

                            i++;
                        }
                        return devolver;
                    }
                } while (repetir);
            }
            catch (Exception e)
            {
                ErrorFX.mostrar(e, true, true, "generar33 - 1094", false);
            }
            return null;
        }

        //Rodrigo Escalona: 23/12/2022 Agregar RMA Automaticamente
        private void AgregarRMA(string invoice_id,Encapsular oEncapsular)
        {
            string Sql = @"
                select distinct s.invoice_id
                from shipper s 
                inner join rma r on r.packlist_id=s.packlist_id
                inner join SHIPPER_LINE sl ON sl.RMA_ID=r.ID
                inner join SHIPPER s2 ON s2.packlist_id=sl.packlist_id
                WHERE s2.invoice_id='" + invoice_id + "'";

            cCONEXCION oData = new cCONEXCION();
            oData = oEncapsular.oData_ERP;
            DataTable oDatTable = oData.EjecutarConsulta(Sql);
            if (oDatTable != null)
            {
                foreach (DataRow ODataRow in oDatTable.Rows)
                {
                    cFACTURA_BANDEJA ocFACTURA_BANDEJATemp = new cFACTURA_BANDEJA();
                    if(ocFACTURA_BANDEJATemp.cargar_ID(ODataRow["invoice_id"].ToString(), oEncapsular.ocEMPRESA))
                    {
                        //Verificar si puede guardarse
                        ModeloDocumentosFX.ModeloDocumentosFXContainer dbContext = new ModeloDocumentosFXContainer();

                        string conection = dbContext.Database.Connection.ConnectionString;
                        cCONEXCION oDataFX = new cCONEXCION(conection);
                        Sql = @"DELETE FROM ndcFacturaSet
                        WHERE tipoRelacion='04' 
                        AND INVOICE_ID='" + invoice_id + @"' 
                        AND INVOICE_ID_relacionada='" + ODataRow["invoice_id"].ToString() + "'";
                        oData.EjecutarConsulta(Sql);


                        Sql = @"INSERT INTO ndcFacturaSet (tipoRelacion,INVOICE_ID,INVOICE_ID_relacionada,UUID) VALUES 
                        ('04','" + invoice_id + "','" + ODataRow["invoice_id"].ToString() + "','" + ocFACTURA_BANDEJATemp.UUID + @"')";
                        oData.EjecutarConsulta(Sql);

                        
                    }

                }
            }
        }

        internal configuracionUM buscarClaveUnidad(string STOCK_UM)
        {
            try
            {
                ModeloDocumentosFX.ModeloDocumentosFXContainer dbContext = new ModeloDocumentosFXContainer();
                configuracionUM registro = new configuracionUM();
                registro = (from r in dbContext.configuracionUMSet.Where
                                        (a => a.UnidadMedida == STOCK_UM)
                            select r).FirstOrDefault();
                if (registro != null)
                {
                    if (registro.Id != 0)
                    {
                        return registro;
                    }
                }
                //Buscar 
                registro = new configuracionUM();
                registro = (from r in dbContext.configuracionUMSet.Where
                                        (a => a.SAT == STOCK_UM)
                            select r).FirstOrDefault();
                if (registro != null)
                {
                    if (registro.Id != 0)
                    {
                        return registro;
                    }
                }
                return null;
            }
            catch (Exception e)
            {
                ErrorFX.mostrar(e, true, true, "generar33 - 1080 - Buscando Conversión " + STOCK_UM, true);
            }
            return null;
        }

        public XmlElement complementoPago10(ModeloComprobantePago.pago oPago, Encapsular oEncapsular, bool autoAjustar = false)
        {
            try
            {
                Pago10.Pagos oPagos = new Pago10.Pagos();

                //Version 11
                oPagos.Pago = new Pago10.PagosPago[1];
                try
                {
                    oPagos.Pago[0] = new Pago10.PagosPago();


                    DateTime oFechaPagoTr = new DateTime(oPago.fechaPago.Year, oPago.fechaPago.Month, oPago.fechaPago.Day
                        , oPago.fechaPago.Hour, oPago.fechaPago.Minute, 0);


                    oPagos.Pago[0].FechaPago = oFechaPagoTr;
                    //Rodrigo Escalona
                    //Aqui debemos llenar la forma de pago
                    oPagos.Pago[0].FormaDePagoP = Pago10.c_FormaPago.Item99;




                    if (!String.IsNullOrEmpty(oPago.monedaP))
                    {
                        if (oPago.monedaP.Contains("MXN"))
                        {
                            oPagos.Pago[0].MonedaP = Pago10.c_Moneda.MXN;
                        }
                        if (oPago.monedaP.Contains("USD"))
                        {
                            oPagos.Pago[0].MonedaP = Pago10.c_Moneda.USD;
                        }
                    }

                    oPagos.Pago[0].TipoCambioPSpecified = false;
                    if (!oPago.monedaP.Contains("MX"))
                    {
                        decimal tipoCambioPtr = 1;
                        oPagos.Pago[0].TipoCambioPSpecified = true;
                        oPagos.Pago[0].TipoCambioP = tipoCambioPtr;
                        if (oPago.tipoCambioP != null)
                        {
                            tipoCambioPtr = decimal.Parse(oPago.tipoCambioP.ToString());
                            oPagos.Pago[0].TipoCambioP = tipoCambioPtr;
                            //BitacoraFX.Log(" oPagos.Pago[0].TipoCambioP " + tipoCambioPtr.ToString());
                        }
                    }

                    decimal montotr = 1;
                    if (oPago.monto != null)
                    {
                        montotr = decimal.Parse(oPago.monto.ToString());
                        montotr = Math.Round(montotr, 2);
                    }

                    //Rodrigo 16/06/2022  Cambio a Decimal de 2
                    //oPagos.Pago[0].Monto = Math.Round(montotr, 2);//.ToString("#.##");
                    oPagos.Pago[0].Monto = Math.Round(montotr, 2).ToString("#.##");

                    if (!String.IsNullOrEmpty(oPago.NumOperacion))
                    {
                        oPagos.Pago[0].NumOperacion = oPago.NumOperacion;
                    }
                    if (!String.IsNullOrEmpty(oPago.RfcEmisorCtaOrd))
                    {
                        oPagos.Pago[0].RfcEmisorCtaOrd = oPago.RfcEmisorCtaOrd;
                    }

                    oPagos.Pago[0].FormaDePagoP = Pago10.c_FormaPago.Item03;
                    //Cargar el ya guardado
                    if (!String.IsNullOrEmpty(oPago.formaDePagoP))
                    {
                        //Pago10.c_FormaPago oc_FormaPago = (Pago10.c_FormaPago)System.Enum.Parse(typeof(Pago10.c_FormaPago), "Item" + oPago.formaDePagoP);
                        //oPagos.Pago[0].FormaDePagoP = oc_FormaPago;
                        switch (oPago.formaDePagoP)
                        {
                            case "01":
                                oPagos.Pago[0].FormaDePagoP = Pago10.c_FormaPago.Item01;
                                break;
                            case "02":
                                oPagos.Pago[0].FormaDePagoP = Pago10.c_FormaPago.Item02;
                                break;
                            case "03":
                                oPagos.Pago[0].FormaDePagoP = Pago10.c_FormaPago.Item03;
                                break;
                            case "04":
                                oPagos.Pago[0].FormaDePagoP = Pago10.c_FormaPago.Item04;
                                break;
                            case "05":
                                oPagos.Pago[0].FormaDePagoP = Pago10.c_FormaPago.Item05;
                                break;
                            case "06":
                                oPagos.Pago[0].FormaDePagoP = Pago10.c_FormaPago.Item06;
                                break;
                            case "07":
                                oPagos.Pago[0].FormaDePagoP = Pago10.c_FormaPago.Item01;
                                break;
                            case "08":
                                oPagos.Pago[0].FormaDePagoP = Pago10.c_FormaPago.Item08;
                                break;
                            case "12":
                                oPagos.Pago[0].FormaDePagoP = Pago10.c_FormaPago.Item12;
                                break;
                            case "13":
                                oPagos.Pago[0].FormaDePagoP = Pago10.c_FormaPago.Item13;
                                break;
                            case "14":
                                oPagos.Pago[0].FormaDePagoP = Pago10.c_FormaPago.Item14;
                                break;
                            case "15":
                                oPagos.Pago[0].FormaDePagoP = Pago10.c_FormaPago.Item15;
                                break;
                        }
                    }
                }
                catch (Exception exCCE)
                {
                    ErrorFX.mostrar(exCCE, true, true, "generar33 - 1777 - complementoPago10 Cabecera ", true);
                    return null;
                }

                if (!String.IsNullOrEmpty(oPago.NomBancoOrdExt))
                {
                    oPagos.Pago[0].NomBancoOrdExt = oPago.NomBancoOrdExt;
                }
                if (!String.IsNullOrEmpty(oPago.NumOperacion))
                {
                    oPagos.Pago[0].NumOperacion = oPago.NumOperacion;
                }
                if (!String.IsNullOrEmpty(oPago.CtaBeneficiario))
                {
                    oPagos.Pago[0].CtaBeneficiario = oPago.CtaBeneficiario;
                }
                if (!String.IsNullOrEmpty(oPago.CtaOrdenante))
                {
                    oPagos.Pago[0].CtaOrdenante = oPago.CtaOrdenante;
                }
                if (!String.IsNullOrEmpty(oPago.RfcEmisorCtaBen))
                {
                    oPagos.Pago[0].RfcEmisorCtaBen = oPago.RfcEmisorCtaBen;
                }
                if (!String.IsNullOrEmpty(oPago.RfcEmisorCtaOrd))
                {
                    oPagos.Pago[0].RfcEmisorCtaOrd = oPago.RfcEmisorCtaOrd;
                }


                oPagos.Pago[0].DoctoRelacionado = new Pago10.PagosPagoDoctoRelacionado[oPago.pagoLinea.Count];
                int i = 0;
                decimal montoTr = 0;
                foreach (pagoLinea linea in oPago.pagoLinea)
                {
                    try
                    {


                        oPagos.Pago[0].DoctoRelacionado[i] = new Pago10.PagosPagoDoctoRelacionado();
                        try
                        {
                            oPagos.Pago[0].DoctoRelacionado[i].IdDocumento = linea.UUID;
                        }
                        catch (Exception exCCE)
                        {
                            ErrorFX.mostrar(exCCE, true, true, "generar33 - 1796 - complementoPago10 Linea linea.UUID " + i.ToString(), true);
                            return null;
                        }
                        try
                        {
                            oPagos.Pago[0].DoctoRelacionado[i].Serie = linea.serie;
                        }
                        catch (Exception exCCE)
                        {
                            ErrorFX.mostrar(exCCE, true, true, "generar33 - 1806 - complementoPago10 Linea linea.serie " + i.ToString(), true);
                            return null;
                        }

                        try
                        {
                            oPagos.Pago[0].DoctoRelacionado[i].Folio = linea.folio;
                        }
                        catch (Exception exCCE)
                        {
                            ErrorFX.mostrar(exCCE, true, true, "generar33 - 1816 - complementoPago10 Linea linea.folio " + i.ToString(), true);
                            return null;
                        }

                        if (linea.MonedaDR.Contains("MX"))
                        {
                            oPagos.Pago[0].DoctoRelacionado[i].MonedaDR = Pago10.c_Moneda.MXN;
                        }
                        if (linea.MonedaDR.Contains("USD"))
                        {
                            oPagos.Pago[0].DoctoRelacionado[i].MonedaDR = Pago10.c_Moneda.USD;
                        }


                        //Si el valor de este campo es diferente al valor registrado en el campo MonedaP, se debe
                        //registrar información en el campoTipoCambioDR

                        if (oPagos.Pago[0].MonedaP != oPagos.Pago[0].DoctoRelacionado[i].MonedaDR)
                        {
                            if (!linea.MonedaDR.Contains("MX"))
                            {
                                decimal tipoCambioDRtr = 1;
                                if (linea.TipoCambioDR != null)
                                {
                                    try
                                    {
                                        tipoCambioDRtr = decimal.Parse(linea.TipoCambioDR.ToString());
                                    }
                                    catch (Exception exCCE)
                                    {
                                        ErrorFX.mostrar(exCCE, true, true, "generar33 - 1859 - complementoPago10 Linea tipoCambioDRtr " + i.ToString(), true);
                                        return null;
                                    }

                                }
                                oPagos.Pago[0].DoctoRelacionado[i].TipoCambioDRSpecified = true;
                                oPagos.Pago[0].DoctoRelacionado[i].TipoCambioDR = Math.Round(tipoCambioDRtr, 6);

                                //Rodrigo Escalona:
                                //26/12/2018
                                //Caso bradford 1/x;
                                if (oPago.emisor.Contains("BME980"))
                                {
                                    decimal tipoCambioDRBrd = cargarTCUSD(oPago, oEncapsular);
                                    oPagos.Pago[0].DoctoRelacionado[i].TipoCambioDR = Math.Round((decimal)linea.TipoCambioDR, 6);// : Math.Round(tipoCambioDRBrd, 6);
                                    BitacoraFX.Log("2163- Tipo Bradford " + oPagos.Pago[0].DoctoRelacionado[i].TipoCambioDR.ToString());
                                }
                            }
                            else
                            {


                                oPagos.Pago[0].DoctoRelacionado[i].TipoCambioDRSpecified = true;
                                oPagos.Pago[0].DoctoRelacionado[i].TipoCambioDR = 1;
                            }
                        }


                        oPagos.Pago[0].DoctoRelacionado[i].MetodoDePagoDR = Pago10.c_MetodoPago.PPD;
                        string parcialidad = "1";
                        if (!String.IsNullOrEmpty(linea.NumParcialidad))
                        {
                            parcialidad = linea.NumParcialidad;
                        }
                        oPagos.Pago[0].DoctoRelacionado[i].NumParcialidad = parcialidad;
                        decimal ImpPagadotr = 0;
                        if (linea.ImpPagado != null)
                        {
                            try
                            {
                                ImpPagadotr = decimal.Parse(linea.ImpPagado.ToString());
                            }
                            catch (Exception exCCE)
                            {
                                ErrorFX.mostrar(exCCE, true, true, "generar33 - 1859 - complementoPago10 Linea ImpPagadotr " + i.ToString(), true);
                                return null;
                            }
                        }
                        //pago10: DoctoRelacionado IdDocumento = "FB0860BC-E239-45E0-8377-12E3981E9619" 
                        //Serie = "A" Folio = "7647" MonedaDR = "USD" MetodoDePagoDR = "PPD" NumParcialidad = "1" 
                        //    ImpSaldoAnt = "77377.300000" ImpPagado = "77377.300000" 
                        //    ImpSaldoInsoluto = "0.000000" />

                        oPagos.Pago[0].DoctoRelacionado[i].ImpPagadoSpecified = true;
                        oPagos.Pago[0].DoctoRelacionado[i].ImpPagado = Math.Round(ImpPagadotr, 2);// linea.ImpPagado;

                        if (oPagos.Pago[0].MonedaP != Pago10.c_Moneda.MXN)
                        {
                            oPagos.Pago[0].DoctoRelacionado[i].ImpPagado = Math.Round(oPagos.Pago[0].DoctoRelacionado[i].ImpPagado, 2);
                        }


                        decimal impSaldoAntTr = 0;
                        if (linea.ImpPagado != null)
                        {
                            oPagos.Pago[0].DoctoRelacionado[i].ImpSaldoAntSpecified = true;

                            try
                            {
                                impSaldoAntTr = decimal.Parse(linea.ImpSaldoAnt.ToString());
                            }
                            catch (Exception exCCE)
                            {
                                ErrorFX.mostrar(exCCE, true, true, "generar33 - 1859 - complementoPago10 Linea impSaldoAntTr " + i.ToString(), true);
                                return null;
                            }
                            oPagos.Pago[0].DoctoRelacionado[i].ImpSaldoAnt = Math.Round(impSaldoAntTr, 2);// linea.ImpSaldoAnt;
                            if (oPagos.Pago[0].DoctoRelacionado[i].ImpSaldoAnt == 0)
                            {
                                oPagos.Pago[0].DoctoRelacionado[i].ImpSaldoAnt = Math.Round(ImpPagadotr, 2);
                            }

                            if (oPagos.Pago[0].MonedaP != Pago10.c_Moneda.MXN)
                            {
                                oPagos.Pago[0].DoctoRelacionado[i].ImpSaldoAnt = Math.Round(oPagos.Pago[0].DoctoRelacionado[i].ImpSaldoAnt, 2);
                            }
                        }



                        oPagos.Pago[0].DoctoRelacionado[i].ImpSaldoInsolutoSpecified = true;
                        oPagos.Pago[0].DoctoRelacionado[i].ImpSaldoInsoluto = Math.Round(Math.Abs(oPagos.Pago[0].DoctoRelacionado[i].ImpPagado - oPagos.Pago[0].DoctoRelacionado[i].ImpSaldoAnt), 2);
                        if (oPagos.Pago[0].MonedaP != Pago10.c_Moneda.MXN)
                        {
                            oPagos.Pago[0].DoctoRelacionado[i].ImpSaldoInsoluto = Math.Round(oPagos.Pago[0].DoctoRelacionado[i].ImpSaldoInsoluto, 2);
                        }

                        montoTr += ImpPagadotr;

                        i++;
                    }

                    catch (Exception exCCE)
                    {
                        ErrorFX.mostrar(exCCE, true, true, "generar33 - 1859 - complementoPago10 Linea " + i.ToString(), true);
                        return null;
                    }
                }

                try
                {
                    if (autoAjustar)
                    {
                        //Rodrigo 16/06/2022  Cambio a Decimal de 2
                        //oPagos.Pago[0].Monto = Math.Round(montotr, 2);//.ToString("#.##");
                        oPagos.Pago[0].Monto = Math.Round(montoTr, 2).ToString("#.##");

                        //oPagos.Pago[0].Monto = Math.Round(montoTr, 2);//.ToString("#.##");
                    }
                    else
                    {
                        bool montoPagoAutomatico = bool.Parse(ConfigurationManager.AppSettings["montoPagoAutomatico"].ToString());
                        if (montoPagoAutomatico)
                        {
                            //Rodrigo 16/06/2022  Cambio a Decimal de 2
                            //oPagos.Pago[0].Monto = Math.Round(montotr, 2);//.ToString("#.##");
                            oPagos.Pago[0].Monto = Math.Round(montoTr, 2).ToString("#.##");

                            //oPagos.Pago[0].Monto = Math.Round(montoTr, 2);//.ToString("#.##"); ;
                        }
                    }
                }
                catch
                {

                }

                try
                {
                    XmlDocument doc = new XmlDocument();
                    XmlSerializerNamespaces myNamespaces_comercio = new XmlSerializerNamespaces();
                    myNamespaces_comercio.Add("pago10", "http://www.sat.gob.mx/Pagos");


                    using (XmlWriter writer = doc.CreateNavigator().AppendChild())
                    {
                        new XmlSerializer(oPagos.GetType()).Serialize(writer, oPagos, myNamespaces_comercio);
                    }
                    return doc.DocumentElement;
                }
                catch (Exception exCCE)
                {
                    ErrorFX.mostrar(exCCE, true, true, "generar33 - 2638 - complementoPago10", true);
                    return null;
                }
            }
            catch (Exception exCCE)
            {
                ErrorFX.mostrar(exCCE, true, true, "generar33 - 1602 - complementoPago10", true);
                return null;
            }
        }

        private decimal cargarTCUSD(pago oPago, Encapsular oEncapsular)
        {
            try
            {
                if (oEncapsular != null)
                {
                    cCONEXCION oData = new cCONEXCION();
                    oData = oEncapsular.oData_ERP;
                    //Cargar Braford
                    //Cargar el By rate de USD del cliente
                    string sSQL = @" SELECT TOP (1) BUY_RATE, EFFECTIVE_DATE 
                    FROM CURRENCY_EXCHANGE 
                     WHERE (CURRENCY_ID = 'USD') 
                     AND (EFFECTIVE_DATE <= " + oData.convertir_fecha(oPago.fechaPago) + @") 
                      ORDER BY EFFECTIVE_DATE DESC ";
                    DataTable oDataTableValidar = oData.EjecutarConsulta(sSQL);
                    if (oDataTableValidar != null)
                    {
                        foreach (DataRow oDataRow in oDataTableValidar.Rows)
                        {
                            return decimal.Parse(oDataRow["BUY_RATE"].ToString());
                        }
                    }
                }

                return 1;
            }
            catch (Exception exCCE)
            {
                ErrorFX.mostrar(exCCE, true, true, "generar33 - 1602 - cargarTCUSD", true);
            }
            return 1;
        }

        private XmlElement complementoCCE(CFDI33.Comprobante oComprobante, bool cce,
            cFACTURA oFACTURA, Encapsular oEncapsular, cCCE ocCCE, CFDI32.t_Ubicacion ot_UbicacionReceptor
            , cFACTURA_BANDEJA ocFACTURA_BANDEJA)
        {
            //Complemento de Comercio Exterior
            if (oComprobante.TipoDeComprobante.ToUpper().Equals("I")) //c_TipoDeComprobante.I)
            {
                try
                {
                    if (cce)
                    {
                        ComercioExterior oComercioExterior = new ComercioExterior();
                        //oComercioExterior.ClaveDePedimentoSpecified = true;
                        //oComercioExterior.ClaveDePedimento = c_ClavePedimento.A1;
                        try
                        {
                            oComercioExterior.ClaveDePedimento = "A1";
                            oComercioExterior.CertificadoOrigen = 0;
                            //oComercioExterior.TipoOperacion = "2";// c_TipoOperacion.Item2;
                            //oComercioExterior.Subdivision = 0;
                            if (!String.IsNullOrEmpty(ocFACTURA_BANDEJA.subdivision))
                            {
                                int subdivisiontr = int.Parse(ocFACTURA_BANDEJA.subdivision);
                                //oComercioExterior.Subdivision = subdivisiontr;
                            }

                            //oComercioExterior.SubdivisionSpecified = true;
                            //oComercioExterior.Incoterm = "EXW";

                            //oComercioExterior.IncotermSpecified = true;
                            //oComercioExterior.Incoterm = c_INCOTERM.EXW;
                            if (oFACTURA.FREE_ON_BOARD.ToString().Contains("EXW"))
                            {
                                oComercioExterior.Incoterm = "EXW";//c_INCOTERM.EXW;
                            }
                            if (oFACTURA.FREE_ON_BOARD.ToString().Contains("FOB"))
                            {
                                oComercioExterior.Incoterm = "FOB"; //c_INCOTERM.FOB;
                            }
                            if (oFACTURA.FREE_ON_BOARD.ToString().Contains("FCA"))
                            {
                                oComercioExterior.Incoterm = "FCA"; //c_INCOTERM.FCA;
                            }
                            if (oFACTURA.FREE_ON_BOARD.ToString().Contains("FAS"))
                            {
                                oComercioExterior.Incoterm = "FAS"; // c_INCOTERM.FAS;
                            }
                            if (oFACTURA.FREE_ON_BOARD.ToString().Contains("DES"))
                            {
                                oComercioExterior.Incoterm = "DES";// c_INCOTERM.DES;
                            }
                            if (oFACTURA.FREE_ON_BOARD.ToString().Contains("DEQ"))
                            {
                                oComercioExterior.Incoterm = "DEQ";// c_INCOTERM.DEQ;
                            }
                            if (oFACTURA.FREE_ON_BOARD.ToString().Contains("DDU"))
                            {
                                oComercioExterior.Incoterm = "DDU";//  c_INCOTERM.DDU;
                            }
                            if (oFACTURA.FREE_ON_BOARD.ToString().Contains("DDP"))
                            {
                                oComercioExterior.Incoterm = "DDP";//  c_INCOTERM.DDP;
                            }
                            if (oFACTURA.FREE_ON_BOARD.ToString().Contains("DAT"))
                            {
                                oComercioExterior.Incoterm = "DAT";//  c_INCOTERM.DAT;
                            }
                            if (oFACTURA.FREE_ON_BOARD.ToString().Contains("DAP"))
                            {
                                oComercioExterior.Incoterm = "DAP";//  c_INCOTERM.DAP;
                            }
                            if (oFACTURA.FREE_ON_BOARD.ToString().Contains("DAF"))
                            {
                                oComercioExterior.Incoterm = "DAF";//  c_INCOTERM.DAF;
                            }
                            if (oFACTURA.FREE_ON_BOARD.ToString().Contains("CPT"))
                            {
                                oComercioExterior.Incoterm = "CPT";// c_INCOTERM.CPT;
                            }
                            if (oFACTURA.FREE_ON_BOARD.ToString().Contains("CIP"))
                            {
                                oComercioExterior.Incoterm = "CIP";//c_INCOTERM.CIP;
                            }
                            if (oFACTURA.FREE_ON_BOARD.ToString().Contains("CIF"))
                            {
                                oComercioExterior.Incoterm = "CIF";//c_INCOTERM.CIF;
                            }
                            if (oFACTURA.FREE_ON_BOARD.ToString().Contains("CFR"))
                            {
                                oComercioExterior.Incoterm = "CFR";//c_INCOTERM.CFR;
                            }

                            bool errorIncoterm = false;
                            if (oComercioExterior.Incoterm == null)
                            {
                                errorIncoterm = true;
                            }
                            else
                            {
                                if (String.IsNullOrEmpty(oComercioExterior.Incoterm))
                                {
                                    errorIncoterm = true;
                                }
                            }
                            if (errorIncoterm)
                            {
                                ErrorFX.mostrar("generar33 - 2270 - CEE Incoterm no existe.", true, true, true);
                                return null;

                            }



                            oComercioExterior.CertificadoOrigen = 0;
                            //oComercioExterior.CertificadoOrigenSpecified = true;
                            //oComercioExterior.TipoCambioUSDSpecified = true;
                            oComercioExterior.TipoCambioUSD = oFACTURA.SELL_RATE;
                            oComercioExterior.TotalUSD = Math.Round(oComprobante.Total, 2).ToString();
                            //oComercioExterior.TotalUSDSpecified = true;

                            oComercioExterior.Emisor = new ComercioExteriorEmisor();
                            oComercioExterior.Emisor.Domicilio = new ComercioExteriorEmisorDomicilio();
                            //Domicilio Fiscal
                            if (oEncapsular.ocEMPRESA.CALLE != "")
                            {
                                oComercioExterior.Emisor.Domicilio.Calle = validacion(oEncapsular.ocEMPRESA.CALLE);
                            }
                            if (oEncapsular.ocEMPRESA.EXTERIOR != "")
                            {
                                oComercioExterior.Emisor.Domicilio.NumeroExterior = validacion(oEncapsular.ocEMPRESA.EXTERIOR);
                            }
                            if (oEncapsular.ocEMPRESA.INTERIOR != "")
                            {
                                oComercioExterior.Emisor.Domicilio.NumeroInterior = validacion(oEncapsular.ocEMPRESA.INTERIOR);
                            }
                            if (oEncapsular.ocEMPRESA.COLONIA != "")
                            {
                                //c_Colonia oc_Colonia = (c_Colonia)System.Enum.Parse(typeof(c_Colonia), "Item" + oEncapsular.ocEMPRESA.COLONIA);

                                //oComercioExterior.Emisor.Domicilio.Colonia = oc_Colonia;
                                //oComercioExterior.Emisor.Domicilio.ColoniaSpecified = true;
                                oComercioExterior.Emisor.Domicilio.Colonia = oEncapsular.ocEMPRESA.COLONIA;
                            }
                            if (oEncapsular.ocEMPRESA.LOCALIDAD != "")
                            {
                                //c_Localidad oc_Localidad = (c_Localidad)System.Enum.Parse(typeof(c_Localidad), "Item" + oEncapsular.ocEMPRESA.LOCALIDAD);
                                //oComercioExterior.Emisor.Domicilio.Localidad = oc_Localidad;
                                //oComercioExterior.Emisor.Domicilio.LocalidadSpecified = true;
                                oComercioExterior.Emisor.Domicilio.Localidad = oEncapsular.ocEMPRESA.LOCALIDAD;
                            }
                            if (oEncapsular.ocEMPRESA.REFERENCIA != "")
                            {
                                oComercioExterior.Emisor.Domicilio.Referencia = validacion(oEncapsular.ocEMPRESA.REFERENCIA);
                            }
                            if (oEncapsular.ocEMPRESA.MUNICIPIO != "")
                            {

                                //c_Municipio oc_Municipio = (c_Municipio)System.Enum.Parse(typeof(c_Municipio), "Item" + oEncapsular.ocEMPRESA.MUNICIPIO);
                                //oComercioExterior.Emisor.Domicilio.Municipio = oc_Municipio;
                                //oComercioExterior.Emisor.Domicilio.MunicipioSpecified = true;
                                oComercioExterior.Emisor.Domicilio.Municipio = oEncapsular.ocEMPRESA.MUNICIPIO;
                            }
                            if (oEncapsular.ocEMPRESA.ESTADO != "")
                            {

                                //c_Estado oc_Estado = (c_Estado)System.Enum.Parse(typeof(c_Estado), oEncapsular.ocEMPRESA.ESTADO);
                                //oComercioExterior.Emisor.Domicilio.Estado = oc_Estado;
                                oComercioExterior.Emisor.Domicilio.Estado = oEncapsular.ocEMPRESA.ESTADO;

                            }
                            if (oEncapsular.ocEMPRESA.PAIS != "")
                            {
                                //ot_UbicacionFiscal.pais = validacion(oEncapsular.ocEMPRESA.PAIS);
                                //oComercioExterior.Emisor.Domicilio.Pais = c_Pais.MEX;
                                oComercioExterior.Emisor.Domicilio.Pais = "MEX";
                            }
                            if (oEncapsular.ocEMPRESA.CP != "")
                            {
                                //ot_UbicacionFiscal.codigoPostal = validacion(oEncapsular.ocEMPRESA.CP);
                                //c_CodigoPostal oc_CodigoPostal = (c_CodigoPostal)System.Enum.Parse(typeof(c_CodigoPostal), "Item" + oEncapsular.ocEMPRESA.CP);
                                //oComercioExterior.Emisor.Domicilio.CodigoPostal = oc_CodigoPostal;
                                oComercioExterior.Emisor.Domicilio.CodigoPostal = oEncapsular.ocEMPRESA.CP;
                                if (oEncapsular.ocEMPRESA.ENTITY_ID.Equals("FIMA"))
                                {
                                    //Rodrigo Escalona 07/01/2020
                                    oComercioExterior.Emisor.Domicilio.CodigoPostal = "67144";
                                }
                            }
                            oComercioExterior.TipoCambioUSD = decimal.Round(oComercioExterior.TipoCambioUSD, 4);



                            //Receptor
                            oComercioExterior.Receptor = new ComercioExteriorReceptor();
                            //oComercioExterior.Receptor.NumRegIdTrib = oFACTURA.TAX_ID_NUMBER;
                            oComercioExterior.Receptor.Domicilio = new ComercioExteriorReceptorDomicilio();
                            if (oFACTURA.BILL_TO_ADDR_1 != null)
                            {
                                oComercioExterior.Receptor.Domicilio.Calle = oFACTURA.BILL_TO_ADDR_1 + oFACTURA.BILL_TO_ADDR_2;
                            }
                            if (ot_UbicacionReceptor.noExterior != null)
                            {
                                oComercioExterior.Receptor.Domicilio.NumeroExterior = ot_UbicacionReceptor.noExterior;

                            }
                            if (ot_UbicacionReceptor.noInterior != null)
                            {
                                oComercioExterior.Receptor.Domicilio.NumeroInterior = ot_UbicacionReceptor.noInterior;
                            }
                            oComercioExterior.Receptor.Domicilio.Pais = oFACTURA.BILL_TO_COUNTRY.ToUpper();//c_Pais.USA;

                            if (oFACTURA.BILL_TO_COUNTRY.ToUpper().Contains("IND"))
                            {
                                oComercioExterior.Receptor.Domicilio.Pais = "IND";//c_Pais.IND;
                            }

                            if (oFACTURA.BILL_TO_COUNTRY.ToUpper().Contains("IRL"))
                            {
                                oComercioExterior.Receptor.Domicilio.Pais = "IRL";//c_Pais.IND;
                            }

                            if (oFACTURA.BILL_TO_COUNTRY.ToUpper().Contains("JPN"))
                            {
                                oComercioExterior.Receptor.Domicilio.Pais = "JPN";//c_Pais.IND;
                            }

                            if (oFACTURA.BILL_TO_COUNTRY.ToUpper().Contains("CHINA"))
                            {
                                oComercioExterior.Receptor.Domicilio.Pais = "CHN";//c_Pais.IND;
                            }
                            if (oFACTURA.BILL_TO_COUNTRY.ToUpper().Contains("CHN"))
                            {
                                oComercioExterior.Receptor.Domicilio.Pais = "CHN";//c_Pais.IND;
                            }


                            if (oFACTURA.BILL_TO_COUNTRY.ToUpper().Contains("THA"))
                            {
                                oComercioExterior.Receptor.Domicilio.Pais = "THA";//c_Pais.IND;
                            }

                            if (oFACTURA.BILL_TO_COUNTRY.ToUpper().Contains("JAPON"))
                            {
                                oComercioExterior.Receptor.Domicilio.Pais = "JPN";//c_Pais.IND;
                            }
                            if (oFACTURA.BILL_TO_COUNTRY.ToUpper().Contains("CHINA"))
                            {
                                oComercioExterior.Receptor.Domicilio.Pais = "CHN";//c_Pais.IND;
                            }
                            if (oFACTURA.BILL_TO_COUNTRY.ToUpper().Contains("CHN"))
                            {
                                oComercioExterior.Receptor.Domicilio.Pais = "CHN";//c_Pais.IND;
                            }


                            if (oFACTURA.BILL_TO_COUNTRY.ToUpper().Contains("DOM"))
                            {
                                oComercioExterior.Receptor.Domicilio.Pais = "DOM";//c_Pais.DOM;
                            }
                            if (oFACTURA.BILL_TO_COUNTRY.ToUpper().Contains("USA"))
                            {
                                oComercioExterior.Receptor.Domicilio.Pais = "USA";// c_Pais.USA;
                            }
                            if (oFACTURA.BILL_TO_COUNTRY.ToUpper().Contains("CAN"))
                            {
                                oComercioExterior.Receptor.Domicilio.Pais = "CAN";// c_Pais.CAN;
                            }

                            if (oFACTURA.BILL_TO_COUNTRY.ToUpper().Contains("GBR"))
                            {
                                oComercioExterior.Receptor.Domicilio.Pais = "GBR";//c_Pais.GBR;
                            }

                            if (oFACTURA.BILL_TO_COUNTRY.ToUpper().Contains("BOL"))
                            {
                                oComercioExterior.Receptor.Domicilio.Pais = "BOL";//c_Pais.GBR;
                            }

                            if (oFACTURA.BILL_TO_COUNTRY.ToUpper().Contains("POL"))
                            {
                                oComercioExterior.Receptor.Domicilio.Pais = "POL";//c_Pais.DOM;
                            }

                            if (oFACTURA.BILL_TO_COUNTRY.ToUpper().Contains("GTM"))
                            {
                                oComercioExterior.Receptor.Domicilio.Pais = "GTM";//c_Pais.DOM;
                            }

                            if (oFACTURA.BILL_TO_COUNTRY.ToUpper().Contains("GUATEMALA"))
                            {
                                oComercioExterior.Receptor.Domicilio.Pais = "GTM";//c_Pais.DOM;
                            }

                            if (oFACTURA.BILL_TO_COUNTRY.ToUpper().Contains("SUIZA"))
                            {
                                oComercioExterior.Receptor.Domicilio.Pais = "CHE";//c_Pais.DOM;
                            }

                            if (oFACTURA.BILL_TO_COUNTRY.ToUpper().Contains("CHE"))
                            {
                                oComercioExterior.Receptor.Domicilio.Pais = "CHE";//c_Pais.DOM;
                            }


                            if (oFACTURA.BILL_TO_COUNTRY.ToUpper().Contains("FRA"))
                            {
                                oComercioExterior.Receptor.Domicilio.Pais = "FRA";//c_Pais.DOM;
                            }

                            if (oFACTURA.BILL_TO_COUNTRY.ToUpper().Contains("FRANCIA"))
                            {
                                oComercioExterior.Receptor.Domicilio.Pais = "FRA";//c_Pais.DOM;
                            }


                            if (oFACTURA.BILL_TO_COUNTRY.ToUpper().Contains("COL"))
                            {
                                oComercioExterior.Receptor.Domicilio.Pais = "COL";//c_Pais.DOM;
                            }

                            if (oFACTURA.BILL_TO_COUNTRY.ToUpper().Contains("IDN"))
                            {
                                oComercioExterior.Receptor.Domicilio.Pais = "IDN";//c_Pais.DOM;
                            }


                            if (!String.IsNullOrEmpty(oFACTURA.BILL_TO_CITY))
                            {
                                oComercioExterior.Receptor.Domicilio.Localidad = oFACTURA.BILL_TO_CITY;
                            }

                            oComercioExterior.Receptor.Domicilio.CodigoPostal = ot_UbicacionReceptor.codigoPostal;
                            oComercioExterior.Receptor.Domicilio.Estado = ot_UbicacionReceptor.estado;
                            //Fin de receptor

                            //Destinatario
                            //Version 1.1
                            oComercioExterior.Destinatario = new ComercioExteriorDestinatario[1];
                            oComercioExterior.Destinatario[0] = new ComercioExteriorDestinatario();
                            oComercioExterior.Destinatario[0].Nombre = oFACTURA.NAME; //oComprobante.Receptor.Nombre;

                            if (!String.IsNullOrEmpty(oFACTURA.VAT_REGISTRATION_DESTINO))
                            {
                                oComercioExterior.Destinatario[0].NumRegIdTrib = oFACTURA.VAT_REGISTRATION_DESTINO;
                            }
                            else
                            {
                                oComercioExterior.Destinatario[0].NumRegIdTrib = oFACTURA.TAX_ID_NUMBER;
                            }

                            oComercioExterior.Destinatario[0].Domicilio = new ComercioExteriorDestinatarioDomicilio[1];
                            oComercioExterior.Destinatario[0].Domicilio[0] = new ComercioExteriorDestinatarioDomicilio();
                            if (oFACTURA.ADDR_1 != "")
                            {
                                oComercioExterior.Destinatario[0].Domicilio[0].Calle = oFACTURA.ADDR_1 + oFACTURA.ADDR_2;
                            }
                            if (oFACTURA.COUNTRY != "")
                            {
                                oComercioExterior.Destinatario[0].Domicilio[0].Pais = oFACTURA.COUNTRY;
                            }
                            if (!String.IsNullOrEmpty(oFACTURA.CITY))
                            {
                                oComercioExterior.Destinatario[0].Domicilio[0].Localidad = oFACTURA.CITY;
                            }
                            if (oFACTURA.STATE != "")
                            {
                                oComercioExterior.Destinatario[0].Domicilio[0].Estado = oFACTURA.STATE;
                            }
                            if (oFACTURA.ZIPCODE != "")
                            {
                                oComercioExterior.Destinatario[0].Domicilio[0].CodigoPostal = oFACTURA.ZIPCODE;
                            }

                            //Validar el TC si es MXN
                            //Aplicable a bradford
                            if (oEncapsular.ocEMPRESA.ENTITY_ID.Contains("BDM"))
                            {
                                if (oComprobante.Moneda.Equals("MXN"))
                                {
                                    //if (oComprobante.TipoCambio == 1)
                                    //{
                                    oComercioExterior.TipoCambioUSD
                                        =
                                        otroTruncar(oFACTURA.tipoCambio("USD", oFACTURA.INVOICE_DATE, oEncapsular.oData_ERP)
                                        , oEncapsular.ocEMPRESA);
                                    ErrorFX.mostrar("tipo de cambio - " + oComercioExterior.TipoCambioUSD.ToString(), false, false, false);
                                    //oComercioExterior.TipoCambioUSD = 1;
                                    //}
                                }
                            }
                            //Rodrigo Escalona: 04/11/2019
                            if (String.IsNullOrEmpty(oComercioExterior.Receptor.Domicilio.Calle))
                            {
                                oComercioExterior.Receptor = null;
                            }

                            ////Rodrigo Escalona: 04/010/2019
                            ////Caso Bradford 
                            //if (oEncapsular.ocEMPRESA.ENTITY_ID.Contains("BDM"))
                            //{
                            //    //if (oComercioExterior.Receptor.Domicilio.Pais.Contains("MEX"))
                            //    //{

                            //        oComercioExterior.Receptor.Domicilio.Calle = oComercioExterior.Destinatario[0].Domicilio[0].Calle;
                            //        oComercioExterior.Receptor.Domicilio.Pais = oComercioExterior.Destinatario[0].Domicilio[0].Pais;
                            //        oComercioExterior.Receptor.Domicilio.Estado = oComercioExterior.Destinatario[0].Domicilio[0].Estado;
                            //        oComercioExterior.Receptor.Domicilio.Localidad = oComercioExterior.Destinatario[0].Domicilio[0].Localidad;
                            //        oComercioExterior.Receptor.Domicilio.CodigoPostal = oComercioExterior.Destinatario[0].Domicilio[0].CodigoPostal;
                            //    //}
                            //}

                            //Sumar las lineas
                            //var results = from p in oComprobante.Conceptos
                            //              group p.NoIdentificacion by p.NoIdentificacion into g
                            //              select new { NoIdentificacion = g.Key, Cars = g.ToList() };
                        }
                        catch (Exception exCCE)
                        {
                            ErrorFX.mostrar(exCCE, true, true, "generar33 - 1700 - Datos de la cabecera de CEE", true);
                            return null;
                        }
                        if (oComprobante.Conceptos == null)
                        {
                            ErrorFX.mostrar("generar33 - 1706 - CFI sin conceptos", true, true, true);
                            return null;
                        }

                        //ROdrigo esalona
                        oComercioExterior.Mercancias = new ComercioExteriorMercancia[oComprobante.Conceptos.Length + 1];


                        int i = 0;
                        try
                        {


                            decimal TotalUSD = 0;
                            int Consecutivo_Linea = 0;
                            foreach (CFDI33.ComprobanteConcepto concepto in oComprobante.Conceptos)
                            {
                                if (concepto != null)
                                {


                                    if (!String.IsNullOrEmpty(concepto.Importe.ToString()))
                                    {
                                        //MessageBox.Show("Importe:" + concepto.Importe.ToString() + " ");

                                        decimal importetr = concepto.Importe;
                                        try
                                        {
                                            if (oEncapsular.ocEMPRESA.ENTITY_ID.Contains("BDM"))
                                            {
                                                if (oComprobante.Moneda.Contains("MXN"))
                                                {
                                                    decimal TipoCambiotr = oComercioExterior.TipoCambioUSD;//oComprobante.TipoCambio;
                                                    importetr = importetr / TipoCambiotr;

                                                }
                                            }
                                        }
                                        catch (Exception exCCE)
                                        {
                                            ErrorFX.mostrar(exCCE, true, true, "generar33 - 1975 - Datos de las líneas del CEE linea: " + i.ToString(), true);
                                            return null;
                                        }

                                        if (!String.IsNullOrEmpty(concepto.NoIdentificacion))
                                        {
                                            string fraccion = "";
                                            try
                                            {


                                                //oComprobante.Conceptos[i].Importe = otroTruncar(oComprobante.Conceptos[i].Importe, oEncapsular.ocEMPRESA);
                                                if (oEncapsular.ocEMPRESA.ENTITY_ID.Contains("BDM"))
                                                {
                                                    //oComprobante.Conceptos[i].Importe = importetr;
                                                }
                                                oComercioExterior.Mercancias[i] = new ComercioExteriorMercancia();
                                                oComercioExterior.Mercancias[i].CantidadAduanaSpecified = true;

                                                decimal CantidadAduana = decimal.Parse(concepto.Cantidad.ToString("#.##"));
                                                oComercioExterior.Mercancias[i].CantidadAduana = CantidadAduana;
                                                //oComercioExterior.Mercancias[i].UnidadAduanaSpecified = true;
                                                //MessageBox.Show("1997");
                                                oComercioExterior.Mercancias[i].NoIdentificacion = concepto.NoIdentificacion;

                                                if (String.IsNullOrEmpty(concepto.Descripcion))
                                                {
                                                    string mensaje = "generar33 - 1736 " + Environment.NewLine + "Factura: " + oFACTURA.INVOICE_ID + "  Error en CCE. " + concepto.NoIdentificacion + ". No existe";
                                                    ErrorFX.mostrar(mensaje, true, true, true);
                                                    return null;
                                                }

                                                ocCCE.cargar(oEncapsular.ocEMPRESA.ROW_ID);
                                                //MessageBox.Show("2008");

                                                fraccion = ocCCE.obtenerFraccion(concepto.NoIdentificacion
                                                    , oFACTURA.INVOICE_ID, "1", oEncapsular.oData_ERP, concepto.Descripcion);
                                                if (String.IsNullOrEmpty(fraccion))
                                                {
                                                    string mensaje = "generar33 - 1467 " + Environment.NewLine + "Factura: " + oFACTURA.INVOICE_ID + "  Error en CCE. Fracción Arancelaría del Producto " + concepto.NoIdentificacion + ". No existe";
                                                    try
                                                    {
                                                        ErrorFX.mostrar(mensaje, true, true, true);
                                                    }
                                                    catch
                                                    {

                                                    }

                                                    return null;
                                                }
                                            }
                                            catch (Exception exCCE)
                                            {
                                                ErrorFX.mostrar(exCCE, true, true, "generar33 - 1756 - Datos de las líneas del CEE linea: " + i.ToString(), true);
                                                return null;
                                            }
                                            //MessageBox.Show("2023");
                                            try
                                            {
                                                //c_FraccionArancelaria oc_FraccionArancelaria = GetFraccionArancelaria(fraccion);
                                                //c_FraccionArancelaria oc_FraccionArancelaria = (c_FraccionArancelaria)System.Enum.Parse(typeof(c_FraccionArancelaria), "Item" + fraccion);

                                                oComercioExterior.Mercancias[i].FraccionArancelaria = fraccion;
                                                //oComercioExterior.Mercancias[i].FraccionArancelariaSpecified = true;
                                                //Version 1.1
                                                //oComercioExterior.Mercancias[i].UnidadAduana = cFraccionUnidad11.obtenerUM(fraccion);
                                                

                                                string fracciontr = "00" + cUMFracciones.obtenerUM(fraccion);
                                                BitacoraFX.Log("Fraccion arancelaria " + fraccion + " UM Cargaado " + fracciontr);

                                                oComercioExterior.Mercancias[i].UnidadAduana = (fracciontr).Substring(fracciontr.Length - 2, 2);                                                
                                                if (oComercioExterior.Mercancias[i].UnidadAduana.Contains("06"))
                                                {
                                                    oComercioExterior.Mercancias[i].UnidadAduana = "06";
                                                }
                                                else
                                                {
                                                    if (oComercioExterior.Mercancias[i].UnidadAduana.Contains("01"))
                                                    {
                                                        oComercioExterior.Mercancias[i].UnidadAduana = "01";
                                                    }
                                                    else
                                                    {
                                                        oComercioExterior.Mercancias[i].UnidadAduana = "06";
                                                    }
                                                }
                                            }
                                            catch (Exception exFraccion)
                                            {
                                                ErrorFX.mostrar(exFraccion, true, true
                                               , "Error en CCE. Fracción Arancelaría (Valor: noIdentificacion=" + fraccion + "). generar33 - 1415", true);
                                                return null;

                                            }
                                            //MessageBox.Show("2043");
                                            if (!String.IsNullOrEmpty(concepto.NoIdentificacion))
                                            {
                                                oComercioExterior.Mercancias[i].NoIdentificacion = concepto.NoIdentificacion;// + Consecutivo_Linea.ToString();
                                                Consecutivo_Linea++; 
                                            }
                                            else
                                            {
                                                oComercioExterior.Mercancias[i].NoIdentificacion = DateTime.Now.ToShortDateString();
                                            }
                                            //MessageBox.Show("2052");
                                            //Agergar el id
                                            try
                                            {
                                                if (bool.Parse(ConfigurationManager.AppSettings["cceMostrarIndicador"].ToString()))
                                                {
                                                    oComercioExterior.Mercancias[i].NoIdentificacion = concepto.NoIdentificacion + i.ToString();
                                                }
                                            }
                                            catch
                                            {

                                            }
                                            //MessageBox.Show("2065");
                                            string mensajetr = "";
                                            try
                                            {

                                                //MessageBox.Show(oComprobante.Conceptos[i].NoIdentificacion);
                                                if (oComprobante.Conceptos[i] != null)
                                                {
                                                    oComprobante.Conceptos[i].NoIdentificacion = oComercioExterior.Mercancias[i].NoIdentificacion;
                                                }

                                                //MessageBox.Show(oComprobante.Conceptos[i].NoIdentificacion);

                                                decimal valorUnitario = decimal.Parse(concepto.ValorUnitario.ToString("#.##"));
                                                mensajetr += " Valor unitario: " + valorUnitario.ToString();

                                                oComercioExterior.Mercancias[i].ValorUnitarioAduana = valorUnitario;
                                                oComercioExterior.Mercancias[i].ValorUnitarioAduanaSpecified = true;



                                                decimal ValorDolares = oComercioExterior.Mercancias[i].ValorUnitarioAduana
                                                    * oComercioExterior.Mercancias[i].CantidadAduana;//decimal.Parse(concepto.importe.ToString("#.##"));
                                                mensajetr += " CantidadAduana: " + oComercioExterior.Mercancias[i].CantidadAduana.ToString();
                                                ValorDolares = decimal.Parse(concepto.Importe.ToString("#.##"));
                                                mensajetr += " ValorDolares: " + ValorDolares.ToString();

                                                if (oEncapsular.ocEMPRESA.ENTITY_ID.Contains("BDM"))
                                                {
                                                    if (oComprobante.Moneda.Contains("MXN"))
                                                    {
                                                        ValorDolares = ValorDolares / oComercioExterior.TipoCambioUSD;
                                                    }
                                                }
                                                mensajetr += "ValorDolares: Dos " + ValorDolares.ToString();
                                                oComercioExterior.Mercancias[i].ValorDolares = decimal.Parse(ValorDolares.ToString("#.##")); ;//ValorDolares;
                                                                                                                                              //oComercioExterior.Mercancias[i].FraccionArancelariaSpecified = true;

                                                TotalUSD += oComercioExterior.Mercancias[i].ValorDolares;

                                            }
                                            catch (Exception exCCE)
                                            {
                                                ErrorFX.mostrar(exCCE, true, true, "generar33 - 2628 - Datos de las líneas del CEE linea: " + i.ToString(), true);
                                                return null;
                                            }
                                            //MessageBox.Show("2101");
                                            if (oEncapsular.ocEMPRESA.ENTITY_ID.Equals("FIMA"))
                                            {
                                                //22/01/2018: Cargar la conversión explicita que tiene FIMA
                                                //Cargar la unidad de CE 
                                                if (oComercioExterior.Mercancias[i].UnidadAduana.Equals("01"))
                                                {
                                                    decimal WEIGHT = oFACTURA.cargarPartWEIGHT(oComercioExterior.Mercancias[i].NoIdentificacion);
                                                    decimal cantidadAduanatr = oComercioExterior.Mercancias[i].CantidadAduana;

                                                    oComercioExterior.Mercancias[i].CantidadAduana = Math.Round(oComercioExterior.Mercancias[i].CantidadAduana * WEIGHT, 2);

                                                    //Rodrigo Escalona: 04/10/2021 Si es PC y WUEHT UM LB debe ser divido entre 2.2046
                                                    //decimal CantidadAduana = decimal.Parse(concepto.Cantidad.ToString("#.##"));
                                                    //if (concepto.ClaveUnidad.Equals("H87")
                                                    //    & oFACTURA.WEIGHT_UM.Equals("LB")
                                                    //    )
                                                    //{
                                                    //    decimal calculo = oComercioExterior.Mercancias[i].CantidadAduana
                                                    //        /
                                                    //       decimal.Parse("2.2046");

                                                    //    oComercioExterior.Mercancias[i].CantidadAduana = calculo;
                                                    //}


                                                    decimal ValorUnitarioAduanatr = oComercioExterior.Mercancias[i].ValorDolares / oComercioExterior.Mercancias[i].CantidadAduana;

                                                    oComercioExterior.Mercancias[i].ValorUnitarioAduana = Math.Round(ValorUnitarioAduanatr, 2);
                                                }
                                                else
                                                {
                                                    try
                                                    {
                                                        ModeloDocumentosFX.ModeloDocumentosFXContainer dbContext = new ModeloDocumentosFXContainer();
                                                        configuracionUMproducto registroUMproducto = new configuracionUMproducto();
                                                        registroUMproducto = (from r in dbContext.configuracionUMproductoSet.Where
                                                                                (a => a.producto == concepto.NoIdentificacion)
                                                                              select r).FirstOrDefault();
                                                        if (registroUMproducto != null)
                                                        {
                                                            try
                                                            {
                                                                //Calcular con el factor de conversión
                                                                oComercioExterior.Mercancias[i].CantidadAduana = Math.Round(oComercioExterior.Mercancias[i].CantidadAduana * registroUMproducto.FactorCE, 2);
                                                                decimal ValorUnitarioAduanatr = oComercioExterior.Mercancias[i].ValorDolares / oComercioExterior.Mercancias[i].CantidadAduana;
                                                                oComercioExterior.Mercancias[i].ValorUnitarioAduana = Math.Round(ValorUnitarioAduanatr, 2);

                                                            }
                                                            catch (Exception exFraccion)
                                                            {

                                                                ErrorFX.mostrar(exFraccion, true, true, "Error en CCE (Complemento de Comercio Exterior) . generar33 - 2133");
                                                                return null;
                                                            }
                                                        }
                                                        else
                                                        {
                                                            ErrorFX.mostrar("generar33 CCE (Complemento de Comercio Exterior) - 2139 - Buscando Conversión , no existe para la Producto: " + concepto.NoIdentificacion, true, true, true);
                                                        }
                                                    }
                                                    catch (Exception e)
                                                    {
                                                        ErrorFX.mostrar(e, true, true, "generar33 CCE (Complemento de Comercio Exterior) - 2144 - Buscando Conversión " + concepto.ClaveUnidad + " Producto: " + concepto.NoIdentificacion);
                                                    }
                                                }


                                                //FIMA dice siempre no 09/01/2018
                                                //Verificar la cantidad en Aduana
                                                //if (oComercioExterior.Mercancias[i].UnidadAduana.Equals("01"))
                                                //{
                                                //    decimal WEIGHT = oFACTURA.cargarPartWEIGHT(oComercioExterior.Mercancias[i].NoIdentificacion);
                                                //    decimal cantidadAduanatr = oComercioExterior.Mercancias[i].CantidadAduana;

                                                //    oComercioExterior.Mercancias[i].CantidadAduana = Math.Round(oComercioExterior.Mercancias[i].CantidadAduana * WEIGHT, 2);

                                                //    decimal ValorUnitarioAduanatr = oComercioExterior.Mercancias[i].ValorDolares / oComercioExterior.Mercancias[i].CantidadAduana;

                                                //    oComercioExterior.Mercancias[i].ValorUnitarioAduana = Math.Round(ValorUnitarioAduanatr, 2);
                                                //}

                                                //if (oComercioExterior.Mercancias[i].UnidadAduana.Equals("04"))
                                                //{
                                                //    decimal WEIGHT = oFACTURA.cargarPartWEIGHT(oComercioExterior.Mercancias[i].NoIdentificacion, false);
                                                //    decimal cantidadAduanatr = oComercioExterior.Mercancias[i].CantidadAduana;

                                                //    oComercioExterior.Mercancias[i].CantidadAduana = Math.Round(oComercioExterior.Mercancias[i].CantidadAduana * WEIGHT, 2);

                                                //    decimal ValorUnitarioAduanatr = oComercioExterior.Mercancias[i].ValorDolares / oComercioExterior.Mercancias[i].CantidadAduana;

                                                //    oComercioExterior.Mercancias[i].ValorUnitarioAduana = Math.Round(ValorUnitarioAduanatr, 2);
                                                //}
                                            }
                                            else
                                            {
                                                //MessageBox.Show("2183");
                                                try
                                                {
                                                    if (oComercioExterior.Mercancias[i].UnidadAduana.Equals("04"))
                                                    {
                                                        decimal WEIGHT = oFACTURA.cargarPartWEIGHT(oComercioExterior.Mercancias[i].NoIdentificacion, false);
                                                        decimal cantidadAduanatr = oComercioExterior.Mercancias[i].CantidadAduana;

                                                        oComercioExterior.Mercancias[i].CantidadAduana = Math.Round(oComercioExterior.Mercancias[i].CantidadAduana * WEIGHT, 2);

                                                        decimal ValorUnitarioAduanatr = oComercioExterior.Mercancias[i].ValorDolares / oComercioExterior.Mercancias[i].CantidadAduana;

                                                        oComercioExterior.Mercancias[i].ValorUnitarioAduana = Math.Round(ValorUnitarioAduanatr, 2);
                                                    }
                                                    //MessageBox.Show("2197");
                                                    //Para todos los demas clientes
                                                    if (oComercioExterior.Mercancias[i].UnidadAduana.Equals("01"))
                                                    {
                                                        decimal WEIGHT = oFACTURA.cargarPartWEIGHT(oComercioExterior.Mercancias[i].NoIdentificacion);
                                                        decimal cantidadAduanatr = oComercioExterior.Mercancias[i].CantidadAduana;
                                                        oComercioExterior.Mercancias[i].CantidadAduana = Math.Round(oComercioExterior.Mercancias[i].CantidadAduana * WEIGHT, 3);
                                                        if (oComercioExterior.Mercancias[i].CantidadAduana == 0)
                                                        {
                                                            throw new Exception("Error CCE: Factura " + oFACTURA.INVOICE_ID + " error weight Producto: " + oComercioExterior.Mercancias[i].NoIdentificacion);
                                                        }

                                                        decimal ValorUnitarioAduanatr = oComercioExterior.Mercancias[i].ValorDolares / oComercioExterior.Mercancias[i].CantidadAduana;
                                                        oComercioExterior.Mercancias[i].ValorUnitarioAduana = Math.Round(ValorUnitarioAduanatr, 2);
                                                        if (oComercioExterior.Mercancias[i].ValorUnitarioAduana == 0)
                                                        {

                                                        }



                                                    }
                                                    else
                                                    {
                                                        //Conversión Pieza a Pieza
                                                        if (oComercioExterior.Mercancias[i].UnidadAduana.Equals("06"))
                                                        {
                                                            oComercioExterior.Mercancias[i].CantidadAduana = Math.Round(oComercioExterior.Mercancias[i].CantidadAduana, 3);
                                                            if (oComercioExterior.Mercancias[i].CantidadAduana == 0)
                                                            {
                                                                throw new Exception("Error CCE: Factura " + oFACTURA.INVOICE_ID + " error Unidad Aduana 06 Producto: " + oComercioExterior.Mercancias[i].NoIdentificacion);
                                                            }
                                                            decimal ValorUnitarioAduanatr = oComercioExterior.Mercancias[i].ValorDolares / oComercioExterior.Mercancias[i].CantidadAduana;
                                                            oComercioExterior.Mercancias[i].ValorUnitarioAduana = Math.Round(ValorUnitarioAduanatr, 2);
                                                        }
                                                        else
                                                        {
                                                            //Otras unidades de medidas
                                                            oComercioExterior.Mercancias[i].CantidadAduana = Math.Round(oComercioExterior.Mercancias[i].CantidadAduana, 3);
                                                            if (oComercioExterior.Mercancias[i].CantidadAduana == 0)
                                                            {
                                                                throw new Exception("Error CCE: Factura " + oFACTURA.INVOICE_ID + " error weight Producto: " + oComercioExterior.Mercancias[i].NoIdentificacion);
                                                            }
                                                            decimal ValorUnitarioAduanatr = oComercioExterior.Mercancias[i].ValorDolares / oComercioExterior.Mercancias[i].CantidadAduana;
                                                            oComercioExterior.Mercancias[i].ValorUnitarioAduana = Math.Round(ValorUnitarioAduanatr, 2);
                                                        }

                                                    }
                                                }

                                                catch (Exception exCCE)
                                                {
                                                    ErrorFX.mostrar(exCCE, true, true, "generar33 - 2235 - Busqueda de peso Datos de las líneas del CEE linea: " + i.ToString(), true);
                                                    return null;
                                                }
                                            }
                                            //Rodrigo Escalona. VAlidar si agregamos otra fraccione
                                            //if (oComercioExterior.Mercancias[i].NoIdentificacion=="05112233")
                                            //{
                                            //    //oComercioExterior.Mercancias[i].FraccionArancelaria = "";
                                            //    ComercioExteriorMercancia oComercioExteriorMercancia = new ComercioExteriorMercancia();
                                            //    oComercioExteriorMercancia = oComercioExterior.Mercancias[i];
                                            //    i++;
                                            //    //oComercioExteriorMercancia.FraccionArancelaria = "6307909900";
                                            //    oComercioExterior.Mercancias[i] = new ComercioExteriorMercancia();
                                            //    oComercioExterior.Mercancias[i].CantidadAduana= oComercioExteriorMercancia.CantidadAduana;
                                            //    oComercioExterior.Mercancias[i].NoIdentificacion = oComercioExteriorMercancia.NoIdentificacion;
                                            //    oComercioExterior.Mercancias[i].UnidadAduana = oComercioExteriorMercancia.UnidadAduana;
                                            //    oComercioExterior.Mercancias[i].ValorUnitarioAduanaSpecified = true;
                                            //    oComercioExterior.Mercancias[0].ValorDolares = decimal.Parse("2416.05"); //oComercioExteriorMercancia.ValorDolares; //decimal.Parse("2416.0");                                    ");
                                            //    oComercioExterior.Mercancias[i].ValorDolares = decimal.Parse("2416.05");

                                            //    oComercioExterior.Mercancias[i].CantidadAduanaSpecified = true;

                                            //    oComercioExterior.Mercancias[0].ValorUnitarioAduana = decimal.Parse("1.88");
                                            //    oComercioExterior.Mercancias[i].ValorUnitarioAduana = decimal.Parse("1.88");
                                            //    //oComercioExteriorMercancia.ValorUnitarioAduana; //decimal.Parse("9.5875");
                                            //    //oComercioExterior.Mercancias[0].ValorDolares = decimal.Parse("9.5875");

                                            //    oComercioExterior.Mercancias[i].FraccionArancelaria = "6307909900";

                                            //}

                                            i++;
                                            //MessageBox.Show("2246");
                                        }
                                    }
                                }
                            }
                            try
                            {
                                //MessageBox.Show("2252");
                                oComercioExterior.TotalUSD = Math.Round(decimal.Parse(TotalUSD.ToString("F")), 2, MidpointRounding.AwayFromZero).ToString();
                                if (oEncapsular.ocEMPRESA.ENTITY_ID.Contains("BDM"))
                                {

                                }
                                else
                                {
                                    oComprobante.SubTotal = decimal.Parse(oComercioExterior.TotalUSD);
                                    oComprobante.Total = decimal.Parse(oComercioExterior.TotalUSD);
                                }
                                //MessageBox.Show("2263");

                                XmlDocument doc = new XmlDocument();
                                XmlSerializerNamespaces myNamespaces_comercio = new XmlSerializerNamespaces();
                                //Version 11
                                myNamespaces_comercio.Add("cce11", "http://www.sat.gob.mx/ComercioExterior11");
                                using (XmlWriter writer = doc.CreateNavigator().AppendChild())
                                {
                                    new XmlSerializer(oComercioExterior.GetType()).Serialize(writer, oComercioExterior, myNamespaces_comercio);
                                }
                                return doc.DocumentElement;
                            }
                            catch (Exception exCCE)
                            {
                                ErrorFX.mostrar(exCCE, true, true, "generar33 - 2268 - Datos de las líneas del CEE linea: " + i.ToString(), true);
                                return null;
                            }

                        }
                        catch (Exception exCCE)
                        {
                            ErrorFX.mostrar(exCCE, true, true, "generar33 - 2275 - Datos de las líneas del CEE linea: " + i.ToString(), true);
                            return null;
                        }

                    }
                }
                catch (Exception exCCE)
                {
                    ErrorFX.mostrar(exCCE, true, true, "generar33 - 1446", true);
                    return null;
                }
            }
            return null;
        }


        private XmlElement complementoLeyenda(cFACTURA oFACTURA, Encapsular oEncapsular, cCONEXCION oData)
        {
            try
            {
                bool insertar = false;
                BitacoraFX.Log("Cargar LEyenda "+ oFACTURA.CUSTOMER_ID + " " + oFACTURA.COUNTRY+ " "+ oFACTURA.ADDR_NO);
                cCLIENTE_CONFIGURACION ocCLIENTE_CONFIGURACION = new cCLIENTE_CONFIGURACION(oData);
                ocCLIENTE_CONFIGURACION.cargar_CUSTOMER_ID(oFACTURA.CUSTOMER_ID, oFACTURA.COUNTRY,oFACTURA.ADDR_NO);
                if (ocCLIENTE_CONFIGURACION.LEYENDA_FISCAL != "")
                {
                    oFACTURA.ESPECIFICACIONES = ocCLIENTE_CONFIGURACION.LEYENDA_FISCAL;
                    //Insertar la especificacion en la factura
                    string sSQL_insert = @"INSERT INTO [dbo].[RECEIVABLE_BINARY]  ([INVOICE_ID],[TYPE],[BITS],[BITS_LENGTH]) 
                                VALUES ('" + oFACTURA.INVOICE_ID + "','D',CAST('" + ocCLIENTE_CONFIGURACION.LEYENDA_FISCAL + @"' AS VARBINARY(MAX)),
                                    " + (ocCLIENTE_CONFIGURACION.LEYENDA_FISCAL.Length + 10).ToString() + ")";

                    string sSQL_sql = " SELECT TOP 1 * FROM  [dbo].[RECEIVABLE_BINARY] WHERE INVOICE_ID='" + oFACTURA.INVOICE_ID + "' AND TYPE='D' ";
                    insertar = true;
                    DataTable oDataTable = oEncapsular.oData_ERP.EjecutarConsulta(sSQL_sql);
                    if (oDataTable != null)
                    {
                        foreach (DataRow oDataRow in oDataTable.Rows)
                        {
                            //Eliminar la especifcación actual
                            sSQL_sql = "DELETE FROM  [dbo].[RECEIVABLE_BINARY] WHERE INVOICE_ID='" + oFACTURA.INVOICE_ID + "' AND TYPE='D' ";
                            oEncapsular.oData_ERP.EjecutarConsulta(sSQL_sql);
                            insertar = true;
                        }
                    }
                    if (insertar)
                    {
                        oEncapsular.oData_ERP.EjecutarConsulta(sSQL_insert);
                    }
                }

                if (oFACTURA.ESPECIFICACIONES != "")
                {
                    CFDI33.ComprobanteComplemento o = new CFDI33.ComprobanteComplemento();
                    CFDI33.ComprobanteImpuestosRetencion[] oComprobanteImpuestosRetencion = new CFDI33.ComprobanteImpuestosRetencion[1];
                    LeyendasFiscales oLeyendasFiscales = new LeyendasFiscales();
                    oLeyendasFiscales.Leyenda = new LeyendasFiscalesLeyenda[1];
                    oLeyendasFiscales.Leyenda[0] = new LeyendasFiscalesLeyenda();
                    oLeyendasFiscales.Leyenda[0].textoLeyenda = oFACTURA.ESPECIFICACIONES;

                    XmlDocument doc = new XmlDocument();
                    XmlSerializerNamespaces myNamespaces_leyenda = new XmlSerializerNamespaces();
                    myNamespaces_leyenda.Add("leyendasFisc", "http://www.sat.gob.mx/leyendasFiscales");

                    using (XmlWriter writer = doc.CreateNavigator().AppendChild())
                    {
                        new XmlSerializer(oLeyendasFiscales.GetType()).Serialize(writer, oLeyendasFiscales, myNamespaces_leyenda);
                    }

                    return doc.DocumentElement;
                }
            }
            catch (Exception exLeyendaFiscal)
            {
                ErrorFX.mostrar(exLeyendaFiscal, true, true, "generar33 - Leyenda Fiscal - 1509", true);
                return null;
            }
            return null;
        }

        public string ExtractNumbers(string expr)
        {
            return string.Join(null, System.Text.RegularExpressions.Regex.Split(expr, "[^\\d]"));
        }
        public decimal otroTruncar(decimal valor, cEMPRESA oEMPRESA)
        {
            try
            {

                decimal result = 0;
                string[] xs = valor.ToString().Split('.');

                string decimalparte = xs[1] + "0000000000000000000000";
                if (oEMPRESA.APROXIMACION == "Truncar")
                {
                    result = decimal.Parse(xs[0] + "." + decimalparte.Substring(0, Math.Min(decimalparte.Length, int.Parse(oEMPRESA.VALORES_DECIMALES))));
                }
                else
                {
                    result = Math.Round(valor, int.Parse(oEMPRESA.VALORES_DECIMALES), MidpointRounding.AwayFromZero);
                    xs = result.ToString().Split('.');
                    if (xs[1].Length != int.Parse(oEMPRESA.VALORES_DECIMALES))
                    {
                        decimalparte = xs[1] + "0000000000000000000000";
                        result = decimal.Parse(xs[0] + "." + decimalparte.Substring(0, int.Parse(oEMPRESA.VALORES_DECIMALES)));
                    }
                }
                return result;
            }
            catch
            {
                return valor;
            }
        }

        public decimal otroTruncar(decimal valor, int decimales = 4)
        {
            try
            {

                decimal result = 0;
                string[] xs = valor.ToString().Split('.');

                string decimalparte = xs[1] + "0000000000000000000000";
                result = decimal.Parse(xs[0] + "." + decimalparte.Substring(0, Math.Min(decimalparte.Length, decimales)));

                return result;
            }
            catch
            {
                return valor;
            }
        }

        public string validacion(string cadena)
        {
            string devolucion = "";
            devolucion = cadena.ToUpper();

            devolucion = devolucion.Replace("á", "A");
            devolucion = devolucion.Replace("é", "E");
            devolucion = devolucion.Replace("í", "I");
            devolucion = devolucion.Replace("ó", "O");
            devolucion = devolucion.Replace("ú", "U");
            devolucion = devolucion.Replace("Á", "A");
            devolucion = devolucion.Replace("É", "E");
            devolucion = devolucion.Replace("Í", "I");
            devolucion = devolucion.Replace("Ó", "O");
            devolucion = devolucion.Replace("Ú", "U");
            devolucion = devolucion.Replace(">", "");
            devolucion = devolucion.Replace("<", "");
            //devolucion = devolucion.Replace("&", "");
            devolucion = devolucion.Replace("'", "");
            devolucion = devolucion.Replace("#", "");
            devolucion = devolucion.Replace("|", "");
            //devolucion = HtmlEncode(devolucion);

            devolucion = Regex.Replace(devolucion, @"[\u0000-\u001F]", string.Empty);


            return devolucion; //Regex.Replace(cadena, "[á,é,í,ó,ú,Á,É,Í,Ó,Ú,#,|,&,',>,<]", "");
        }
        public void parsar_d1p1(string ARCHIVO, bool leyendaFiscal)
        {
            ReplaceInFile(ARCHIVO, "d1p1", "xsi");
            ReplaceInFile(ARCHIVO, "xsi:cfdi=\"http://www.sat.gob.mx/cfd/3\"", "");
            ReplaceInFile(ARCHIVO, " cartaporte20=\"http://www.sat.gob.mx/CartaPorte20\"", "");
            //cartaporte20="http://www.sat.gob.mx/CartaPorte20"
            if (leyendaFiscal)
            {
                ReplaceInFile(ARCHIVO, "xsi:schemaLocation=\"http://www.sat.gob.mx/cfd/3 http://www.sat.gob.mx/sitio_internet/cfd/3/cfdv33.xsd\"", "xsi:schemaLocation=\"http://www.sat.gob.mx/cfd/3 http://www.sat.gob.mx/sitio_internet/cfd/3/cfdv33.xsd http://www.sat.gob.mx/leyendasFiscales http://www.sat.gob.mx/sitio_internet/cfd/leyendasFiscales/leyendasFisc.xsd\"");
            }
        }
        static public void ReplaceInFile(string filePath, string searchText, string replaceText)
        {
            StreamReader reader = new StreamReader(filePath);
            string content = reader.ReadToEnd();
            reader.Close();
            reader.Dispose();

            content = Regex.Replace(content, searchText, replaceText);

            StreamWriter writer = new StreamWriter(filePath);
            writer.Write(content);
            writer.Close();
            writer.Dispose();
        }
        public string validacion_AMP(string cadena)
        {
            string devolucion = cadena;
            //devolucion = devolucion.Replace("&", "&amp;");
            return devolucion;
        }
        public string Fechammmyy(string pFecha)
        {
            DateTime Fecha = DateTime.Parse(pFecha.ToString());
            string FechaFormateada = "";
            string[] mMeses = new string[13];

            mMeses[1] = "Ene";
            mMeses[2] = "Feb";
            mMeses[3] = "Mar";
            mMeses[4] = "Abr";
            mMeses[5] = "May";
            mMeses[6] = "Jun";
            mMeses[7] = "Jul";
            mMeses[8] = "Agt";
            mMeses[9] = "Sep";
            mMeses[10] = "Oct";
            mMeses[11] = "Nov";
            mMeses[12] = "Dic";

            int Mes = int.Parse(Fecha.Month.ToString());

            FechaFormateada = mMeses[Mes] + AgregarCero(Fecha.Year.ToString());

            return FechaFormateada;
        }
        public string AgregarCero(string p)
        {

            String regreso = p;

            if (p.Length < 2)

                regreso = "0" + p;

            return regreso;
        }

        public string SelloRSA(string LLAVE, string PASSWORD, string strData, decimal ANIO)
        {
            try
            {
                StringBuilder sbPassword;
                StringBuilder sbPrivateKey;
                byte[] b;
                byte[] block;
                int keyBytes;



                string strKeyFile = LLAVE;

                sbPassword = new StringBuilder(PASSWORD);

                sbPrivateKey = Rsa.ReadEncPrivateKey(strKeyFile, sbPassword.ToString());

                keyBytes = Rsa.KeyBytes(sbPrivateKey.ToString());

                b = System.Text.Encoding.UTF8.GetBytes(strData);


                block = Rsa.EncodeMsgForSignature(keyBytes, b, HashAlgorithm.Sha256);

                block = Rsa.RawPrivate(block, sbPrivateKey.ToString());

                string resultado = System.Convert.ToBase64String(block);

                Wipe.String(sbPassword);
                Wipe.String(sbPrivateKey);
                Wipe.Data(block);

                return resultado;
            }
            catch (Exception exLeyendaFiscal)
            {
                ErrorFX.mostrar(exLeyendaFiscal, true, true, "generar33 - SelloRSA - 1666", true);
                return null;
            }

        }

        private string extraeExterior(string source)
        {
            string[] stringSeparators = new string[] { "EXT.", "INT." };
            string[] result;
            result = source.Split(stringSeparators, StringSplitOptions.None);
            if (result.Length == 1)
            {
                return "";
            }
            return result[1];
        }
        private string extraeInterior(string source)
        {
            string[] stringSeparators = new string[] { "INT." };
            string[] result;
            result = source.Split(stringSeparators, StringSplitOptions.None);
            if (result.Length == 1)
            {
                return "";
            }
            return result[1];
        }
        private string extraeDireccion(string source)
        {
            string[] stringSeparators = new string[] { "EXT.", "INT." };
            string[] result;
            result = source.Split(stringSeparators, StringSplitOptions.None);
            if (result.Length == 1)
            {
                return source;
            }

            return result[0];
        }

        private configuracionProducto buscarConfiguracionProducto(string invoice_id, string partId)
        {
            ClasificadorProductos.formularios.frmProductoClasificacion oProducto = new ClasificadorProductos.formularios.frmProductoClasificacion(partId);
            oProducto.ShowDialog();
            if (oProducto.oRegistro == null)
            {
                ErrorFX.mostrar(
                "GENERAR33 - 602 - "
                + Environment.NewLine
                + " Factura " + invoice_id + " Acción cancelada por el usuario el producto " + partId
                + " no tiene su clave del SAT", true, true, true);
                return null;
            }
            return oProducto.oRegistro;
        }

        private bool FacturaTimbradaReal(string invoiceId, Encapsular oEncapsular)
        {
            cFACTURA_BANDEJA ocFACTURA_BANDEJA = new cFACTURA_BANDEJA(oEncapsular.oData_ERP, oEncapsular.ocEMPRESA);
            return ocFACTURA_BANDEJA.ExisteTimbrada(invoiceId);
        }
        private string extraeLocalidad(string source)
        {
            string[] stringSeparators = new string[] { "LOC." };
            string[] result;
            result = source.Split(stringSeparators, StringSplitOptions.None);
            if (result.Length == 1)
            {
                return "";
            }
            return result[1];
        }

    }
}
