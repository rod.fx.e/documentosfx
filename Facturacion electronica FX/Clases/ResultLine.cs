﻿namespace FE_FX.Clases
{
    internal class ResultLine
    {
        public decimal TasaOCuotaDR { get; set; }
        public decimal BaseDR { get; set; }
        public decimal ImporteDR { get; set; }
    }
}