﻿using CFDI40;
using ClasificadorProductos.formularios;
using CryptoSysPKI;
using Generales;
using ModeloComprobantePago;
using ModeloDocumentosFX;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml;
using System.Xml.Serialization;
using System.Xml.XPath;
using System.Xml.Xsl;

namespace FE_FX.Clases
{
    class Generar40
    {
        public bool Generar(cFACTURA oFACTURA, cSERIE ocSERIE
        , Encapsular oEncapsular, cCERTIFICADO ocCERTIFICADO, bool complementoComercioExterior = false
        , bool sinRelacion = false, bool sinCEE = false, bool fechaActual=false)
        {
            if (FacturaTimbradaReal(oFACTURA.INVOICE_ID, oEncapsular))
            {
                ErrorFX.mostrar("Generar 40 -La factura ya se encuentra timbrada", true, true, false);
                return false;
            }
            //Leyenda Fiscal
            bool leyendaFiscal = false;

            ModeloDocumentosFXContainer dbContext = new ModeloDocumentosFXContainer();
            string conection = dbContext.Database.Connection.ConnectionString;
            cCONEXCION oData = new cCONEXCION(conection);

            CFDI32.t_Ubicacion ot_UbicacionReceptor = new CFDI32.t_Ubicacion();

            CFDI40.Comprobante oComprobante = new CFDI40.Comprobante();
            XmlSerializerNamespaces myNamespaces = new XmlSerializerNamespaces();
            myNamespaces.Add("cfdi", "http://www.sat.gob.mx/cfd/4");
            DateTime FECHA_FACTURA = new DateTime();
            try
            {
                cFACTURA_BANDEJA ocFACTURA_BANDEJAtmp = new cFACTURA_BANDEJA(oData);
                ocFACTURA_BANDEJAtmp.cargar_ID(oFACTURA.INVOICE_ID, oEncapsular.ocEMPRESA);
                //Rodrigo Escalona: 06-07-2023 Verificar datos del regimen fiscal

                try
                {
                    cFACTURA_BANDEJA ocFACTURA_BANDEJAtmpERP = new cFACTURA_BANDEJA(oEncapsular.oData_ERP);
                    if(ocFACTURA_BANDEJAtmpERP.cargar_ID(oFACTURA.INVOICE_ID, oEncapsular.ocEMPRESA))
                    {
                        ocFACTURA_BANDEJAtmp = ocFACTURA_BANDEJAtmpERP;
                    }

                }
                catch
                {

                }
                

                try
                {
                    try
                    {
                        //Extraer el SERIE de INVOICE_ID
                        string FOLIO = ExtractNumbers(oFACTURA.INVOICE_ID);
                        //Serie
                        oComprobante.Serie = ocSERIE.ID;
                        //Folio
                        oComprobante.Folio = FOLIO;
                        oComprobante.Moneda = "USD";//CFDI40.c_Moneda.USD;
                        oComprobante.Exportacion = "01";
                        
                        try
                        {
                            switch (ocFACTURA_BANDEJAtmp.Exportacion)
                            {
                                case "01":
                                    oComprobante.Exportacion = "01";//c_Exportacion.Item01;
                                    break;
                                case "02":
                                    oComprobante.Exportacion = "02";//c_Exportacion.Item02;
                                    break;
                                case "03":
                                    oComprobante.Exportacion = "03";//c_Exportacion.Item03;
                                    break;
                                case "04":
                                    oComprobante.Exportacion = "04";//c_Exportacion.Item03;
                                    break;
                            }
                        }
                        catch (Exception Error)
                        {
                            BitacoraFX.Log("Generar-102 " + Error.Message.ToString());
                        }
                        
                        if (oFACTURA.CURRENCY_ID.Contains("DLLS") |
                            oFACTURA.CURRENCY_ID.Contains("USD")
                            )
                        {
                            oComprobante.Moneda = "USD"; //CFDI40.c_Moneda.USD;
                        }
                        if (oFACTURA.CURRENCY_ID.Contains("MXP")
                            |
                            oFACTURA.CURRENCY_ID.Contains("PESOS")
                            |
                            oFACTURA.CURRENCY_ID.Contains("MXN")
                            |
                            oFACTURA.CURRENCY_ID.Contains("MN")
                            )
                        {
                            oComprobante.Moneda = "MXN"; //CFDI40.c_Moneda.MXN;
                        }

                        if (oComprobante.Moneda != "MXN")//CFDI40.c_Moneda.MXN)
                        {
                            oComprobante.TipoCambio = OtroTruncar(oFACTURA.SELL_RATE, 4);
                            oComprobante.TipoCambioSpecified = true;

                            if (fechaActual)
                            {
                                oComprobante.TipoCambio = ObtenerTipoCambio(oEncapsular.oData_ERP, DateTime.Now);
                            }

                        }

                        oComprobante.CondicionesDePago = oFACTURA.TERMS_NET_DAYS;
                        if ((oComprobante.CondicionesDePago == "")
                            ||
                            (oComprobante.CondicionesDePago == null))
                        {
                            oComprobante.CondicionesDePago = "PAGO EN UNA SOLA EXHIBICION";
                        }
                        else
                        {
                            if (oFACTURA.TERMS_NET_DAYS != "")
                            {
                                oComprobante.CondicionesDePago = oFACTURA.TERMS_NET_DAYS;
                            }
                        }

                        //CDFI
                        //CFDI40.c_CodigoPostal CodigoPostal = (CFDI40.c_CodigoPostal)System.Enum.Parse(typeof(CFDI40.c_CodigoPostal), "Item" + ocSERIE.CP);
                        oComprobante.LugarExpedicion = ocSERIE.CP;//CodigoPostal;

                        IFormatProvider culture = new CultureInfo("es-MX", true);
                        DateTime INVOICE_DATE = oFACTURA.traer_INVOICE_DATE(oFACTURA.INVOICE_ID); //oFACTURA.INVOICE_DATE;
                        DateTime CREATE_DATE = oFACTURA.traer_CREATE_DATE(oFACTURA.INVOICE_ID); //oFACTURA.CREATE_DATE;

                        try
                        {
                            CREATE_DATE = DateTime.Now;
                            INVOICE_DATE = DateTime.Parse(INVOICE_DATE.Day.ToString() + "/" + INVOICE_DATE.Month.ToString() + "/" + INVOICE_DATE.Year.ToString() + " " + CREATE_DATE.ToString("HH:mm:ss"));
                        }
                        catch
                        {
                            try
                            {
                                CREATE_DATE = DateTime.Now;
                                FECHA_FACTURA = DateTime.Parse(INVOICE_DATE.Day.ToString() + "/" + INVOICE_DATE.Month.ToString() + "/" + INVOICE_DATE.Year.ToString() + " " + CREATE_DATE.ToString("HH:mm:ss"));
                            }
                            catch
                            {

                            }
                        }
                        //Verificar si se le baja la fecha INVOICE_DATE.AddHours(-1);
                        int menos_hora = 0;
                        try
                        {
                            menos_hora = int.Parse(ConfigurationManager.AppSettings["menos_hora"].ToString());
                            //INVOICE_DATE = DateTime.Now;
                            INVOICE_DATE = INVOICE_DATE.AddHours(menos_hora);
                            INVOICE_DATE = new DateTime(INVOICE_DATE.Year, INVOICE_DATE.Month, INVOICE_DATE.Day, 0, 0, 0);
                        }
                        catch
                        {

                        }

                        oComprobante.Fecha = INVOICE_DATE;
                        if (fechaActual)
                        {
                            DateTime oFechatr = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day
                                , DateTime.Now.Hour, DateTime.Now.Minute, 0);

                            oComprobante.Fecha = oFechatr;
                        }
                        //Tipo de Comprobante
                        switch (oFACTURA.TYPE)
                        {
                            case "I":
                                oComprobante.TipoDeComprobante = CFDI40.c_TipoDeComprobante.I;
                                break;
                            case "M":
                                oComprobante.TipoDeComprobante = CFDI40.c_TipoDeComprobante.E;
                                oComprobante.CondicionesDePago = "Contado";
                                break;
                            default:
                                oComprobante.TipoDeComprobante = CFDI40.c_TipoDeComprobante.I;
                                oComprobante.CondicionesDePago = "Contado";
                                break;
                        }

                        try
                        {
                            //CFDI40.c_MetodoPago MetodoPago = CFDI40.c_MetodoPago.PPD;
                            //oComprobante.MetodoPago = CFDI40.c_MetodoPago.PPD;
                            //oComprobante.MetodoPagoSpecified = true;
                            //oComprobante.FormaPagoSpecified = true;
                            //if (!String.IsNullOrEmpty(ocFACTURA_BANDEJAtmp.METODO_DE_PAGO))
                            //{
                            //    oComprobante.MetodoPago = (CFDI40.c_MetodoPago)System.Enum.Parse(typeof(CFDI40.c_MetodoPago)
                            //        , ocFACTURA_BANDEJAtmp.METODO_DE_PAGO);
                            //}
                            //else
                            //{
                            //    oComprobante.MetodoPago = MetodoPago;
                            //}
                            oComprobante.MetodoPago = "PPD";
                            if (!String.IsNullOrEmpty(ocFACTURA_BANDEJAtmp.METODO_DE_PAGO))
                            {
                                oComprobante.MetodoPago = ocFACTURA_BANDEJAtmp.METODO_DE_PAGO;
                            }

                            BitacoraFX.Log(" Genera40 - FORMA_DE_PAGO - 179 " + ocFACTURA_BANDEJAtmp.FORMA_DE_PAGO);

                            //oComprobante.FormaPagoSpecified = true;
                            if (!String.IsNullOrEmpty(ocFACTURA_BANDEJAtmp.FORMA_DE_PAGO))
                            {
                                oComprobante.FormaPago = (CFDI40.c_FormaPago)System.Enum.Parse(typeof(CFDI40.c_FormaPago)
                                    , "Item" + ocFACTURA_BANDEJAtmp.FORMA_DE_PAGO.Replace("Item", string.Empty));
                                oComprobante.FormaPagoSpecified = true;

                                //El Item 01 ya quyitarlo Rodrigo Escalona: 06/11/2023
                                //Quitado 
                                ////if(oComprobante.FormaPago== c_FormaPago.Item01)
                                ////{
                                ////    oComprobante.FormaPago = c_FormaPago.Item99;
                                ////}

                            }
                            else
                            {
                                oComprobante.FormaPago = CFDI40.c_FormaPago.Item03;
                            }
                        }
                        catch (Exception error)
                        {
                            ErrorFX.mostrar(error, true, true, "Generar40 - 190 - Metodod de Pago y Forma de Pago ", true);
                        }

                        //Rodrigo Escalona: 26/04/2023
                        if (oEncapsular.ocEMPRESA.DetenerRelacionRMA)
                        {
                            //Autorelacionar al RMA
                            AutorelacionarRMA(oFACTURA.INVOICE_ID, oEncapsular);
                        }

                        CFDI40.ComprobanteCfdiRelacionados oComprobanteCfdiRelacionados = null;
                        oComprobanteCfdiRelacionados = new CFDI40.ComprobanteCfdiRelacionados();
                        oComprobanteCfdiRelacionados = CargarComprobanteCfdiRelacionados(oFACTURA.INVOICE_ID, oEncapsular
                            , oFACTURA);

                        if (!sinRelacion)
                        {
                            if (oComprobanteCfdiRelacionados != null)
                            {
                                //oComprobante.CfdiRelacionados = new CFDI40.ComprobanteCfdiRelacionados();
                                //oComprobante.CfdiRelacionados = oComprobanteCfdiRelacionados;
                                oComprobante.CfdiRelacionados = new ComprobanteCfdiRelacionados[1];
                                oComprobante.CfdiRelacionados[0] = new ComprobanteCfdiRelacionados();
                                oComprobante.CfdiRelacionados[0] = oComprobanteCfdiRelacionados;
                            }
                        }

                        //Tighitco siempre PPD
                        if (oEncapsular.ocEMPRESA.ENTITY_ID.Equals("TIGMEX"))
                        {
                            oComprobante.MetodoPago = "PPD";//CFDI40.c_MetodoPago.PPD;
                        }
                        else
                        {
                            if (oEncapsular.ocEMPRESA.ENTITY_ID.Equals("CHIMEX"))
                            {
                                oComprobante.MetodoPago = "PPD";//CFDI40.c_MetodoPago.PPD;
                            }
                        }

                    }
                    catch (Exception error)
                    {
                        ErrorFX.mostrar(error, true, true, "Generar40 - 190 - Cabecera ", true);
                    }
                    X509Certificate cert = X509Certificate.CreateFromCertFile(ocCERTIFICADO.CERTIFICADO);
                    try
                    {
                        string NoSerie = cert.GetSerialNumberString();
                        string Certificado64 = System.Convert.ToBase64String(cert.GetRawCertData());
                        oComprobante.Certificado = Certificado64;
                        oComprobante.NoCertificado = Validacion(ocCERTIFICADO.NO_CERTIFICADO);
                        bool descuentoTr = false;
                        try
                        {
                            descuentoTr = bool.Parse(oEncapsular.ocEMPRESA.DESCUENTO_LINEA);
                        }
                        catch
                        {
                        }

                        //SubTotal
                        if (oFACTURA.anticipo)
                        {
                            oComprobante.SubTotal = Math.Abs(oFACTURA.TOTAL_AMOUNT);
                        }
                        else
                        {
                            oComprobante.SubTotal = Math.Abs(oFACTURA.TOTAL_AMOUNT) + Math.Abs(oFACTURA.DESCUENTO_TOTAL);


                            if (descuentoTr)
                            {
                                oComprobante.SubTotal = Math.Abs(oFACTURA.TOTAL_AMOUNT);
                            }
                            else
                            {
                                oComprobante.SubTotal = Math.Abs(oFACTURA.TOTAL_AMOUNT) + oFACTURA.DESCUENTO_TOTAL;
                            }

                        }

                        //MessageBox.Show("Comprobante10");
                        if (oFACTURA.DESCUENTO_TOTAL != 0)
                        {
                            if (!descuentoTr)
                            {
                                oComprobante.Descuento = OtroTruncar(oFACTURA.DESCUENTO_TOTAL, oEncapsular.ocEMPRESA);
                                oComprobante.DescuentoSpecified = true;
                            }
                        }

                        if (oComprobante.TipoDeComprobante == CFDI40.c_TipoDeComprobante.I)
                        {
                            if (oFACTURA.anticipo)
                            {
                                oComprobante.Total = Math.Abs(oFACTURA.TOTAL_VAT_AMOUNT);
                            }
                            else
                            {
                                if (descuentoTr)
                                {
                                    oComprobante.Total = (Math.Abs((oComprobante.SubTotal + oFACTURA.TOTAL_VAT_AMOUNT) - Math.Abs(oFACTURA.TOTAL_RETENIDO)));
                                }
                                else
                                {
                                    oComprobante.Total = (Math.Abs((oComprobante.SubTotal + oFACTURA.TOTAL_VAT_AMOUNT) - Math.Abs(oFACTURA.TOTAL_RETENIDO))) - oFACTURA.DESCUENTO_TOTAL;
                                }
                            }
                        }

                        if (oComprobante.TipoDeComprobante == CFDI40.c_TipoDeComprobante.E)
                        {
                            if (oFACTURA.anticipo)
                            {
                                oComprobante.Total = Math.Abs(oFACTURA.TOTAL_VAT_AMOUNT);
                            }
                            else
                            {
                                if (oFACTURA.DESCUENTO_TOTAL == 0)
                                {
                                    oComprobante.Total = Math.Abs(oFACTURA.TOTAL_AMOUNT + oFACTURA.TOTAL_VAT_AMOUNT);
                                    oComprobante.Descuento = 0;
                                    oComprobante.DescuentoSpecified = false;
                                }
                                else
                                {
                                    oComprobante.Total = Math.Abs(oFACTURA.TOTAL_AMOUNT + oFACTURA.TOTAL_VAT_AMOUNT);
                                }
                            }
                        }
                        oComprobante.SubTotal = OtroTruncar(oComprobante.SubTotal, oEncapsular.ocEMPRESA);
                        oComprobante.Total = OtroTruncar(oComprobante.Total, oEncapsular.ocEMPRESA);
                    }
                    catch (Exception error)
                    {
                        ErrorFX.mostrar(error, true, true, "Generar40 - 312 - Cabecera del Comprobante", true);
                    }
                    //Elemento Emisor
                    CFDI40.ComprobanteEmisor oComprobanteEmisor = new CFDI40.ComprobanteEmisor();

                    //Cargar los datos de la Compañia apartir del Certificado
                    cert = X509Certificate.CreateFromCertFile(ocCERTIFICADO.CERTIFICADO);
                    string Dato = cert.GetName();
                    //MessageBox.Show("Comprobante11");

                    char[] delimiterChars = { ',' };
                    string[] TipoCer = Dato.Split(delimiterChars);

                    //RFC Posicion 4
                    string RFC_Certificado = TipoCer[4];
                    char[] RFC_Delimiter = { '=' };
                    string[] RFC_Arreglo = RFC_Certificado.Split(RFC_Delimiter);
                    string RFC_Arreglo_Certificado = RFC_Arreglo[1];
                    RFC_Arreglo_Certificado = RFC_Arreglo_Certificado.Replace("\"", "");
                    RFC_Arreglo_Certificado = RFC_Arreglo_Certificado.Replace("/", "");
                    RFC_Arreglo_Certificado = RFC_Arreglo_Certificado.Trim();
                    //MessageBox.Show("Comprobante12");
                    //RFC
                    oComprobanteEmisor.Rfc = Validacion(oEncapsular.ocEMPRESA.RFC);
                    //Nombre
                    oComprobanteEmisor.Nombre = Validacion(oEncapsular.ocEMPRESA.ID);
                    oComprobanteEmisor.Nombre = LimpiarNombre(oComprobanteEmisor.Nombre);

                    oComprobanteEmisor.RegimenFiscal = CFDI40.c_RegimenFiscal.Item601;


                    oComprobante.Emisor = oComprobanteEmisor;
                    //MessageBox.Show("Comprobante15");
                    //Elemento Receptor
                    CFDI40.ComprobanteReceptor oComprobanteReceptor = new CFDI40.ComprobanteReceptor();
                    oComprobanteReceptor.Rfc = Validacion(oFACTURA.VAT_REGISTRATION);
                    //&amp;
                    oComprobanteReceptor.Rfc = Validacion(oComprobanteReceptor.Rfc);
                    oComprobanteReceptor.Nombre = LimpiarNombre(Validacion(oFACTURA.BILL_TO_NAME));
                    //oComprobanteReceptor.RegimenFiscalReceptor = c_RegimenFiscal.Item601;
                    //if (oFACTURA.BILL_TO_NAME.Contains("SA DE CV")
                    //    ||
                    //    oFACTURA.BILL_TO_NAME.Contains("S.A. DE C.V.")
                    //    ||
                    //    oFACTURA.BILL_TO_NAME.Contains("SA.DE CV.")
                    //    ||
                    //    oFACTURA.BILL_TO_NAME.Contains("SA.DE CV.")

                    //)
                    //{
                    //    oComprobanteReceptor.RegimenFiscalReceptor = c_RegimenFiscal.Item601;
                    //}
                    //else
                    //{
                    //    oComprobanteReceptor.RegimenFiscalReceptor = c_RegimenFiscal.Item601;
                    //}
                    BitacoraFX.Log("Regimen Fiscal " + ocFACTURA_BANDEJAtmp.RegimenFiscal.Trim()
                                                    );
                    if (!String.IsNullOrEmpty(ocFACTURA_BANDEJAtmp.RegimenFiscal))
                    {
                        oComprobanteReceptor.RegimenFiscalReceptor = ObtenerRegimenFiscal(ocFACTURA_BANDEJAtmp.RegimenFiscal);

                        //switch (ocFACTURA_BANDEJAtmp.RegimenFiscal)
                        //{
                        //    case "601":
                        //        oComprobanteReceptor.RegimenFiscalReceptor = c_RegimenFiscal.Item601;
                        //        break;
                        //    case "612":
                        //        oComprobanteReceptor.RegimenFiscalReceptor = c_RegimenFiscal.Item612;
                        //        break;
                        //    case "605":
                        //        oComprobanteReceptor.RegimenFiscalReceptor = c_RegimenFiscal.Item605;
                        //        break;
                        //    default:
                        //        oComprobanteReceptor.RegimenFiscalReceptor = c_RegimenFiscal.Item601;
                        //        break;
                        //}
                    }
                    try
                    {
                        try
                        {
                            c_RegimenFiscal RegimenFiscalTr = (c_RegimenFiscal)Enum.Parse(typeof(c_RegimenFiscal)
                            , "Item" + ocFACTURA_BANDEJAtmp.RegimenFiscal.Trim());
                            oComprobanteReceptor.RegimenFiscalReceptor = RegimenFiscalTr;

                            BitacoraFX.Log("Regimen Fiscal " + ocFACTURA_BANDEJAtmp.RegimenFiscal.Trim()
                                                    );
                        }
                        catch
                        {

                        }
                    }
                    catch (Exception error)
                    {
                        ErrorFX.mostrar(error, true, true, "Generar40 - 190 - Dirección ", true);
                    }
                    try
                    {
                        if (!String.IsNullOrEmpty(oEncapsular.ocEMPRESA.CUSTOM_NOMBRE_CFDI))
                        {
                            string customtr = oEncapsular.ocEMPRESA.CUSTOM_NOMBRE_CFDI.Replace("NAME", oFACTURA.NAME)
                                .Replace("ADDR_1", oFACTURA.ADDR_1)
                                .Replace("ADDR_2", oFACTURA.ADDR_2)
                                .Replace("ADDR_3", oFACTURA.ADDR_3);
                            
                            if (!String.IsNullOrEmpty(customtr))
                            {
                                oComprobanteReceptor.Nombre = customtr;
                            }
                        }
                    }
                    catch (Exception error)
                    {
                        ErrorFX.mostrar(error, true, true, "Generar40 - 190 - Dirección ", true);
                    }
                    oComprobanteReceptor.UsoCFDI = c_UsoCFDI.G03;

                    try
                    {
                        oComprobanteReceptor.UsoCFDI = CFDI40.c_UsoCFDI.G01;
                        if (!String.IsNullOrEmpty(ocFACTURA_BANDEJAtmp.USO_CFDI))
                        {
                            oComprobanteReceptor.UsoCFDI = (CFDI40.c_UsoCFDI)System.Enum.Parse(typeof(CFDI40.c_UsoCFDI)
                                    , ocFACTURA_BANDEJAtmp.USO_CFDI);
                        }
                    }
                    catch (Exception error)
                    {
                        ErrorFX.mostrar(error, true, true, "Generar40 - 190 - UsoCFDI ", true);
                    }
                    try
                    {
                        //Rodrigo Escalona: 31/05/2018
                        cCLIENTE_CONFIGURACION ocCLIENTE_CONFIGURACION = new cCLIENTE_CONFIGURACION(oData);
                        ocCLIENTE_CONFIGURACION.cargar_CUSTOMER_ID(oFACTURA.CUSTOMER_ID);
                        if (!String.IsNullOrEmpty(ocCLIENTE_CONFIGURACION.usoCFDI))
                        {
                            oComprobanteReceptor.UsoCFDI = (CFDI40.c_UsoCFDI)System.Enum.Parse(typeof(CFDI40.c_UsoCFDI)
                                    , ocCLIENTE_CONFIGURACION.usoCFDI);
                        }
                    }
                    catch (Exception error)
                    {
                        ErrorFX.mostrar(error, true, true, "Generar40 - 190 - UsoCFDI ", true);
                    }

                    //Rodrigo Escalona 20/01/2023
                    //Si el regimen es P01 debe ser G01
                    if (oComprobanteReceptor.UsoCFDI == c_UsoCFDI.P01)
                    {
                        oComprobanteReceptor.UsoCFDI = c_UsoCFDI.G01;
                    }
                    oComprobanteReceptor.Nombre = Validacion(oComprobanteReceptor.Nombre);

                    //CFDI 4.0 Limpiar el Nombre
                    oComprobanteReceptor.Nombre = LimpiarNombre(oComprobanteReceptor.Nombre);

                    //Verificar
                    if (oFACTURA.BILL_TO_ADDR_1.Trim() != "")
                    {
                        ot_UbicacionReceptor.calle = ExtraeDireccion(Validacion(oFACTURA.BILL_TO_ADDR_1));
                        string exterior = ExtraeExterior(Validacion(oFACTURA.BILL_TO_ADDR_1));
                        if (exterior != "")
                        {
                            ot_UbicacionReceptor.noExterior = exterior;
                        }
                        string interior = ExtraeInterior(Validacion(oFACTURA.BILL_TO_ADDR_1));
                        if (interior != "")
                        {
                            ot_UbicacionReceptor.noInterior = interior;
                        }

                    }

                    string localidad = ExtraeLocalidad(Validacion(oFACTURA.BILL_TO_ADDR_3));
                    if (localidad != "")
                    {
                        ot_UbicacionReceptor.localidad = localidad;
                    }

                    if (oFACTURA.BILL_TO_ADDR_2.Trim() != "")
                    {
                        ot_UbicacionReceptor.colonia = Validacion(oFACTURA.BILL_TO_ADDR_2);
                    }
                    if (oFACTURA.BILL_TO_STATE.Trim() != "")
                    {
                        ot_UbicacionReceptor.localidad = Validacion(oFACTURA.BILL_TO_STATE);

                    }
                    if (oFACTURA.BILL_TO_STATE.Trim() != "")
                    {
                        ot_UbicacionReceptor.estado = Validacion(oFACTURA.BILL_TO_STATE);
                    }
                    ot_UbicacionReceptor.referencia = "";
                    if (oFACTURA.BILL_TO_CITY.Trim() != "")
                    {
                        ot_UbicacionReceptor.municipio = Validacion(oFACTURA.BILL_TO_CITY);
                    }

                    ot_UbicacionReceptor.pais = Validacion(oFACTURA.BILL_TO_COUNTRY);
                    try
                    {
                        if (ot_UbicacionReceptor.pais.ToUpper().Contains("MEX"))
                        {
                            oComprobanteReceptor.ResidenciaFiscalSpecified = false;
                        }
                        else
                        {
                            oComprobanteReceptor.ResidenciaFiscal = (CFDI40.c_Pais)System.Enum.Parse(typeof(CFDI40.c_Pais)
                                    , ot_UbicacionReceptor.pais);
                            oComprobanteReceptor.ResidenciaFiscalSpecified = true;
                        }

                    }
                    catch (Exception error)
                    {
                        ErrorFX.mostrar(error, true, true, "Generar40 - 548 - Seccion de Domicilio del Receptor ", true);
                        return false;
                    }

                    if (oFACTURA.BILL_TO_ZIPCODE.Trim() != "")
                    {
                        ot_UbicacionReceptor.codigoPostal = Validacion(oFACTURA.BILL_TO_ZIPCODE);
                        //if (!System.Text.RegularExpressions.Regex.IsMatch("^[0-9]{5}", ot_UbicacionReceptor.codigoPostal))
                        //{
                        //    ErrorFX.mostrar("Generar40 - El Código postal del Cliente " + oFACTURA.CUSTOMER_ID + " " + oFACTURA.NAME + @" no tiene 5 numeros ",true,true,true);
                        //    return false;
                        //}


                    }
                    else
                    {
                        ErrorFX.mostrar("Generar40 - 541 - Receptor debe agregarle el Codigo Postal. Debe hacerlo en Visual en Bill ZipCode", true, true, true);
                        return false;
                    }

                    oComprobanteReceptor.DomicilioFiscalReceptor = ot_UbicacionReceptor.codigoPostal;

                    //Rodrigo Escalona:
                    ////Validacion CFDI40149	El campo DomicilioFiscalReceptor, no es igual al valor del campo LugarExpedicion. 
                    ///
                    if (oComprobanteReceptor.Rfc.Equals("XAXX010101000")
                        || oComprobanteReceptor.Rfc.Equals("XEXX010101000"))
                    {
                        oComprobanteReceptor.DomicilioFiscalReceptor = oComprobante.LugarExpedicion;
                        oComprobanteReceptor.RegimenFiscalReceptor = c_RegimenFiscal.Item616;
                        oComprobanteReceptor.UsoCFDI = c_UsoCFDI.S01;

                    }

                    if (oComprobanteReceptor.Rfc.Equals("XAXX010101000"))
                    {
                        oComprobanteReceptor.Nombre = "PUBLICO EN GENERAL";
                        oComprobanteReceptor.RegimenFiscalReceptor = c_RegimenFiscal.Item616;
                        oComprobanteReceptor.UsoCFDI = c_UsoCFDI.S01;
                        oComprobanteReceptor.DomicilioFiscalReceptor = oComprobante.LugarExpedicion;
                        //Rodrigo Escalona: Agregar la Informacion Global
                        oComprobante.InformacionGlobal = new ComprobanteInformacionGlobal();
                        oComprobante.InformacionGlobal.Año = short.Parse(oFACTURA.INVOICE_DATE.Year.ToString());
                        switch (oFACTURA.INVOICE_DATE.Month)
                        {
                            case 1:
                                oComprobante.InformacionGlobal.Meses = c_Meses.Item01;
                                break;
                            case 2:
                                oComprobante.InformacionGlobal.Meses = c_Meses.Item02;
                                break;
                            case 3:
                                oComprobante.InformacionGlobal.Meses = c_Meses.Item03;
                                break;
                            case 4:
                                oComprobante.InformacionGlobal.Meses = c_Meses.Item04;
                                break;
                            case 5:
                                oComprobante.InformacionGlobal.Meses = c_Meses.Item05;
                                break;
                            case 6:
                                oComprobante.InformacionGlobal.Meses = c_Meses.Item06;
                                break;
                            case 7:
                                oComprobante.InformacionGlobal.Meses = c_Meses.Item07;
                                break;
                            case 8:
                                oComprobante.InformacionGlobal.Meses = c_Meses.Item08;
                                break;
                            case 9:
                                oComprobante.InformacionGlobal.Meses = c_Meses.Item09;
                                break;
                            case 10:
                                oComprobante.InformacionGlobal.Meses = c_Meses.Item10;
                                break;
                            case 11:
                                oComprobante.InformacionGlobal.Meses = c_Meses.Item11;
                                break;
                            case 12:
                                oComprobante.InformacionGlobal.Meses = c_Meses.Item12;
                                break;
                        }

                        oComprobante.InformacionGlobal.Periodicidad = c_Periodicidad.Item01;


                        try
                        {
                            ModeloDocumentosFX.ModeloDocumentosFXContainer Db = new ModeloDocumentosFX.ModeloDocumentosFXContainer();
                            ModeloDocumentosFX.InformacionGlobal OInformacionGlobal = Db.InformacionGlobalSet.Where(a => a.INVOICE_ID.Equals(oFACTURA.INVOICE_ID))
                                .FirstOrDefault();
                            if (OInformacionGlobal != null)
                            {
                                oComprobante.InformacionGlobal.Año = short.Parse(OInformacionGlobal.InformacionGlobalAnio);

                                oComprobante.InformacionGlobal.Meses = c_Meses.Item01;
                                switch (OInformacionGlobal.InformacionGlobalMes)
                                {
                                    case "01":
                                        oComprobante.InformacionGlobal.Meses = c_Meses.Item01;
                                        break;
                                    case "02":
                                        oComprobante.InformacionGlobal.Meses = c_Meses.Item02;
                                        break;
                                    case "03":
                                        oComprobante.InformacionGlobal.Meses = c_Meses.Item03;
                                        break;
                                    case "04":
                                        oComprobante.InformacionGlobal.Meses = c_Meses.Item04;
                                        break;
                                    case "05":
                                        oComprobante.InformacionGlobal.Meses = c_Meses.Item05;
                                        break;
                                    case "06":
                                        oComprobante.InformacionGlobal.Meses = c_Meses.Item06;
                                        break;
                                    case "07":
                                        oComprobante.InformacionGlobal.Meses = c_Meses.Item07;
                                        break;
                                    case "08":
                                        oComprobante.InformacionGlobal.Meses = c_Meses.Item08;
                                        break;
                                    case "09":
                                        oComprobante.InformacionGlobal.Meses = c_Meses.Item09;
                                        break;
                                    case "10":
                                        oComprobante.InformacionGlobal.Meses = c_Meses.Item10;
                                        break;
                                    case "11":
                                        oComprobante.InformacionGlobal.Meses = c_Meses.Item11;
                                        break;
                                    case "12":
                                        oComprobante.InformacionGlobal.Meses = c_Meses.Item12;
                                        break;
                                }

                                switch (OInformacionGlobal.InformacionGlobalPeriodicidad)
                                {
                                    case "01":
                                        oComprobante.InformacionGlobal.Periodicidad = c_Periodicidad.Item01;
                                        break;
                                    case "02":
                                        oComprobante.InformacionGlobal.Periodicidad = c_Periodicidad.Item02;
                                        break;
                                    case "03":
                                        oComprobante.InformacionGlobal.Periodicidad = c_Periodicidad.Item03;
                                        break;
                                    case "04":
                                        oComprobante.InformacionGlobal.Periodicidad = c_Periodicidad.Item04;
                                        break;
                                    case "05":
                                        oComprobante.InformacionGlobal.Periodicidad = c_Periodicidad.Item05;
                                        break;
                                }

                            }
                            Db.Dispose();
                        }
                        catch (Exception error)
                        {
                            MessageBox.Show(error.Message, Application.ProductName
                                        , MessageBoxButtons.OK, MessageBoxIcon.Error);
                        }
                    }

                    oComprobante.Receptor = oComprobanteReceptor;
                }
                catch (Exception error)
                {
                    ErrorFX.mostrar(error, true, true, "Generar40 - 541 - Seccion de cabecera ", true);
                    return false;
                }

                //Rodrigo Escalona: 26/07/2018 NDC Activar
                bool ndcUsarPropio = false;
                try
                {
                    if (oComprobante.TipoDeComprobante.Equals("E"))
                    {
                        if (!String.IsNullOrEmpty(ocSERIE.ndcUsarPropio))
                        {
                            if (bool.Parse(ocSERIE.ndcUsarPropio))
                            {
                                ndcUsarPropio = true;
                                oComprobante.MetodoPago = ocSERIE.ndcMetodoPago;
                                //(CFDI40.c_MetodoPago)System.Enum.Parse(typeof(CFDI40.c_MetodoPago)
                                //, ocSERIE.ndcMetodoPago);
                                oComprobante.Receptor.UsoCFDI =
                                    (CFDI40.c_UsoCFDI)System.Enum.Parse(typeof(CFDI40.c_UsoCFDI)
                                    , ocSERIE.ndcUsoCfdi);
                            }
                        }
                    }
                }
                catch (Exception error)
                {
                    ErrorFX.mostrar(error, true, true, "Generar40 - 620 - Seccion de Cabecera NDC automatizada ", true);
                    return false;
                }

                decimal TotalImpuestoBase = 0;
                decimal TotalImpuestoImporte = 0;
                bool AgregarImpuestos = false;
                //Elemento Conceptos
                try
                {
                    CFDI40.ComprobanteConcepto[] oComprobanteConceptos;
                    oComprobanteConceptos = new CFDI40.ComprobanteConcepto[oFACTURA.LINEAS.Length];
                    for (int i = 0; i < oFACTURA.LINEAS.Length; i++)
                    {
                        if (oFACTURA.LINEAS[i] != null)
                        {
                            //if (decimal.Parse(oFACTURA.LINEAS[i].AMOUNT) != 0)
                            if (true)
                            {
                                CFDI40.ComprobanteConcepto oComprobanteConcepto = new CFDI40.ComprobanteConcepto();


                                
                                //MessageBox.Show("Comprobante18");
                                try
                                {
                                    



                                    oComprobanteConcepto.Cantidad = Math.Abs(decimal.Parse(oFACTURA.LINEAS[i].QTY));
                                    oComprobanteConcepto.Cantidad = Math.Round(oComprobanteConcepto.Cantidad, 2);


                                    if (!String.IsNullOrEmpty(oFACTURA.LINEAS[i].Descuento))
                                    {
                                        try
                                        {
                                            oComprobanteConcepto.Descuento = OtroTruncar(decimal.Parse(oFACTURA.LINEAS[i].Descuento)* oComprobanteConcepto.Cantidad
                                                , oEncapsular.ocEMPRESA);
                                            oComprobanteConcepto.DescuentoSpecified = false;
                                            if (oComprobanteConcepto.Descuento != 0)
                                            {
                                                oComprobanteConcepto.DescuentoSpecified = true;
                                            }
                                            
                                        }
                                        catch
                                        {

                                        }
                                    }

                                    //Rodrigo Escalona 31/01/2020 Si el producto tiene valor
                                    if (!String.IsNullOrEmpty(oFACTURA.LINEAS[i].PART_ID))
                                    {
                                        if (!String.IsNullOrEmpty(oFACTURA.LINEAS[i].STOCK_UM))
                                        {
                                            configuracionUM busquedatr = BuscarClaveUnidad(oFACTURA.LINEAS[i].STOCK_UM);
                                            if (busquedatr != null)
                                            {
                                                oComprobanteConcepto.ClaveUnidad = ObtenerClaveUnidad(busquedatr.SAT.ToUpper());
                                                if (oComprobanteConcepto.ClaveUnidad.Equals("KGM"))
                                                {
                                                    //Rodrigo Escalona 29/04/2021 Fima no quiere KF
                                                    if (!oEncapsular.ocEMPRESA.ENTITY_ID.Contains("FIMA"))
                                                    {
                                                        oComprobanteConcepto.Unidad = "KG";
                                                    }
                                                }
                                            }
                                            else
                                            {
                                                oComprobanteConcepto.ClaveUnidad = ObtenerClaveUnidad(oFACTURA.LINEAS[i].STOCK_UM.ToUpper());
                                                if (oComprobanteConcepto.ClaveUnidad.Equals("KGM"))
                                                {
                                                    //Rodrigo Escalona 29/04/2021 Fima no quiere KF
                                                    if (!oEncapsular.ocEMPRESA.ENTITY_ID.Contains("FIMA"))
                                                    {
                                                        oComprobanteConcepto.Unidad = "KG";
                                                    }
                                                }
                                            }
                                        }
                                    }

                                }
                                catch (Exception errGeneral)
                                {
                                    ErrorFX.mostrar(errGeneral, true, true, "Generar40 - 577 - Seccion de lineas - Cantidad", true);
                                    return false;
                                }

                                try
                                {
                                    if (oFACTURA.anticipo)
                                    {
                                        oFACTURA.LINEAS[i].AMOUNT = (decimal.Parse(oFACTURA.LINEAS[i].AMOUNT) - oFACTURA.anticipoAMOUNT).ToString();
                                        oFACTURA.LINEAS[i].UNIT_PRICE =
                                            (Math.Round(decimal.Parse(oFACTURA.LINEAS[i].AMOUNT) / oComprobanteConcepto.Cantidad
                                            , 4)).ToString();
                                    }
                                }
                                catch (Exception errGeneral)
                                {
                                    ErrorFX.mostrar(errGeneral, true, true, "Generar40 - 600 - Seccion de lineas - Anticipo", true);
                                    return false;
                                }
                                //Agregar el producto PART_ID
                                try
                                {
                                    //Rodrigo Escalona 17/12/2020 Validar si la prioridad es el producto del cliente
                                    bool pegarPart = true;
                                    if (!String.IsNullOrEmpty(oFACTURA.LINEAS[i].PART_ID))
                                    {
                                        oComprobanteConcepto.NoIdentificacion = oFACTURA.LINEAS[i].PART_ID;
                                    }
                                    if (!String.IsNullOrEmpty(oFACTURA.LINEAS[i].CUSTOMER_PART_ID))
                                    {
                                        try
                                        {
                                            if (!String.IsNullOrEmpty(oEncapsular.ocEMPRESA.PRIORIDAD_PART))
                                            {
                                                bool PRIORIDAD_PARTTr = bool.Parse(oEncapsular.ocEMPRESA.PRIORIDAD_PART);
                                                if (PRIORIDAD_PARTTr)
                                                {
                                                    //Rodrigo Escalona: 12/10/2021 Validar si el filtro es por Cliente  oGeneral
                                                    //bool NoIdentificacion = true;
                                                    if (!String.IsNullOrEmpty(oEncapsular.ocEMPRESA.PrioridadPartFiltro))
                                                    {
                                                        //NoIdentificacion = false;
                                                        //Validar si es el mismo customer
                                                        if (oEncapsular.ocEMPRESA.PrioridadPartFiltro.Equals(oFACTURA.CUSTOMER_ID))
                                                        {
                                                            //NoIdentificacion = true;
                                                            oComprobanteConcepto.NoIdentificacion = oFACTURA.LINEAS[i].CUSTOMER_PART_ID;
                                                            pegarPart = false;
                                                        }
                                                    }
                                                    //if (NoIdentificacion)
                                                    //{

                                                    //}

                                                }
                                            }
                                        }
                                        catch (Exception e)
                                        {
                                            ErrorFX.mostrar(e, true, true, "Generar40 - 740 - errorPrioridad", false);
                                        }
                                    }

                                    if (pegarPart)
                                    {
                                        if (!String.IsNullOrEmpty(oFACTURA.LINEAS[i].PART_ID))
                                        {
                                            oComprobanteConcepto.NoIdentificacion = oFACTURA.LINEAS[i].PART_ID;
                                        }
                                    }
                                    //Cargar la Linea relacionada
                                    bool relacionado = false;
                                    try
                                    {
                                        string lineatr = oFACTURA.LINEAS[i].LINEA;

                                        ndcFacturaLinea linea = (from b in dbContext.ndcFacturaLineaSet
                                        .Where(a => a.INVOICE_ID == oFACTURA.INVOICE_ID & a.LINE_NO == lineatr)
                                                                 select b).FirstOrDefault();
                                        if (linea != null)
                                        {
                                            relacionado = true;
                                            //Rodrigo Escalona: 29/12/2021 Error que da a Tghitco
                                            //oComprobanteConcepto.ClaveProdServ = linea.clasificacion;
                                            //oComprobanteConcepto.NoIdentificacion = linea.PART_ID;
                                        }

                                    }
                                    catch (Exception e)
                                    {
                                        ErrorFX.mostrar(e, true, true, "Generar40 - 727", false);
                                    }


                                    //Buscar el numero de Producto para ser migrado
                                    if (!String.IsNullOrEmpty(oFACTURA.LINEAS[i].ClaveProdServ) & !String.IsNullOrEmpty(oFACTURA.LINEAS[i].PART_ID))
                                    {
                                        if (!relacionado)
                                        {
                                            oComprobanteConcepto.ClaveProdServ = ObtenerClaveProdServ(oFACTURA.LINEAS[i].ClaveProdServ);
                                        }
                                    }
                                    else
                                    {
                                        //Validar si la linea de la Factura tiene CFDI relacionados
                                        bool cargarLinea = true;
                                        try
                                        {
                                            string lineatr = oFACTURA.LINEAS[i].LINEA;

                                            ndcFacturaLinea linea = (from b in dbContext.ndcFacturaLineaSet
                                            .Where(a => a.INVOICE_ID == oFACTURA.INVOICE_ID & a.LINE_NO == lineatr)
                                                                     select b).FirstOrDefault();
                                            if (linea != null)
                                            {
                                                cargarLinea = false;
                                                oComprobanteConcepto.ClaveProdServ = ObtenerClaveProdServ(linea.clasificacion);
                                                oComprobanteConcepto.NoIdentificacion = linea.PART_ID;
                                            }

                                        }
                                        catch (Exception e)
                                        {
                                            ErrorFX.mostrar(e, true, true, "Generar40 - 617", false);
                                        }
                                        if (cargarLinea)
                                        {
                                            try
                                            {
                                                //Cargar desde aqui 
                                                if (!String.IsNullOrEmpty(oFACTURA.LINEAS[i].ClaveProdServ) & !String.IsNullOrEmpty(oFACTURA.LINEAS[i].PART_ID))
                                                {
                                                    oComprobanteConcepto.ClaveProdServ = ObtenerClaveProdServ(oFACTURA.LINEAS[i].ClaveProdServ.Trim());

                                                    configuracionUM busquedatr = BuscarClaveUnidad(oFACTURA.LINEAS[i].STOCK_UM.Trim());
                                                    if (busquedatr != null)
                                                    {
                                                        oComprobanteConcepto.ClaveUnidad = ObtenerClaveUnidad(busquedatr.SAT.ToUpper());
                                                        if (oComprobanteConcepto.ClaveUnidad.Equals("KGM"))
                                                        {
                                                            //Rodrigo Escalona 28/04/2021
                                                            //Rodrigo Escalona 29/04/2021 Fima no quiere KF
                                                            if (!oEncapsular.ocEMPRESA.ENTITY_ID.Contains("FIMA"))
                                                            {
                                                                oComprobanteConcepto.Unidad = "KG";
                                                            }
                                                        }
                                                    }
                                                }
                                                else
                                                {
                                                    //Validar que tengas los valores para buscar 
                                                    bool buscarClasificador = false;
                                                    if (!String.IsNullOrEmpty(oFACTURA.LINEAS[i].PRODUCT_CODE))
                                                    {
                                                        buscarClasificador = true;
                                                    }
                                                    if (!String.IsNullOrEmpty(oFACTURA.LINEAS[i].GL_ACCOUNT_ID))
                                                    {
                                                        buscarClasificador = true;
                                                    }
                                                    if (!String.IsNullOrEmpty(oFACTURA.LINEAS[i].SERVICE_CHARGE_ID))
                                                    {
                                                        buscarClasificador = true;
                                                    }
                                                    if (buscarClasificador)
                                                    {
                                                        configuracionProducto oConfiguracionProducto = oFACTURA.buscarClave(oFACTURA.LINEAS[i].PART_ID
                                                            , oFACTURA.LINEAS[i].PRODUCT_CODE, oFACTURA.LINEAS[i].GL_ACCOUNT_ID
                                                            , oFACTURA.LINEAS[i].SERVICE_CHARGE_ID);
                                                        bool buscarProducto = false;
                                                        if (oConfiguracionProducto == null)
                                                        {
                                                            frmProductoClasificacion oFrmProductoClasificacion = new frmProductoClasificacion(oFACTURA.LINEAS[i].PART_ID
                                                            , oFACTURA.LINEAS[i].PRODUCT_CODE, oFACTURA.LINEAS[i].GL_ACCOUNT_ID
                                                            , oFACTURA.LINEAS[i].SERVICE_CHARGE_ID);
                                                            if (oFrmProductoClasificacion.ShowDialog() == DialogResult.OK)
                                                            {
                                                                oConfiguracionProducto = oFACTURA.buscarClave(oFACTURA.LINEAS[i].PART_ID
                                                                , oFACTURA.LINEAS[i].PRODUCT_CODE, oFACTURA.LINEAS[i].GL_ACCOUNT_ID
                                                                , oFACTURA.LINEAS[i].SERVICE_CHARGE_ID);
                                                            }
                                                            else
                                                            {
                                                                return false;
                                                            }
                                                        }


                                                        if (!buscarProducto)
                                                        {
                                                            if (String.IsNullOrEmpty(oConfiguracionProducto.clasificacionSAT))
                                                            {
                                                                oConfiguracionProducto = BuscarConfiguracionProducto(oFACTURA.INVOICE_ID, oFACTURA.LINEAS[i].PART_ID);
                                                                if (String.IsNullOrEmpty(oConfiguracionProducto.clasificacionSAT))
                                                                {
                                                                    if (!ndcUsarPropio)
                                                                    {
                                                                        ErrorFX.mostrar("Generar40 - 640 - Factura " + oFACTURA.INVOICE_ID + "  El producto " + oFACTURA.LINEAS[i].PART_ID
                                                                    + " no tiene su clave del SAT " + Environment.NewLine + " Configure el product code: ", true, true, true);


                                                                        return false;
                                                                    }
                                                                }
                                                            }
                                                            oComprobanteConcepto.ClaveProdServ = ObtenerClaveProdServ(oConfiguracionProducto.clasificacionSAT.Trim());
                                                            if (!String.IsNullOrEmpty(oConfiguracionProducto.unidadSAT))
                                                            {
                                                                oComprobanteConcepto.ClaveUnidad = ObtenerClaveUnidad(oConfiguracionProducto.unidadSAT.Trim().ToUpper());
                                                            }
                                                        }
                                                        else
                                                        {
                                                            if (!ndcUsarPropio)
                                                            {
                                                                ErrorFX.mostrar("Generar40 - 640 - Factura " + oFACTURA.INVOICE_ID + "  El producto " + oFACTURA.LINEAS[i].PART_ID
                                                            + " no tiene su clave del SAT", true, true, true);

                                                                if (Globales.automatico)
                                                                {
                                                                    return false;
                                                                }

                                                                oConfiguracionProducto = BuscarConfiguracionProducto(oFACTURA.INVOICE_ID, oFACTURA.LINEAS[i].PART_ID);

                                                                oComprobanteConcepto.ClaveProdServ = ObtenerClaveProdServ(oConfiguracionProducto.clasificacionSAT.Trim());
                                                                if (!String.IsNullOrEmpty(oConfiguracionProducto.unidadSAT))
                                                                {
                                                                    oComprobanteConcepto.ClaveUnidad = ObtenerClaveUnidad(oConfiguracionProducto.unidadSAT.Trim().ToUpper());
                                                                }

                                                            }

                                                        }

                                                    }
                                                    else
                                                    {
                                                        if (!ndcUsarPropio)
                                                        {
                                                            ErrorFX.mostrar("Generar40 - 608 - "
                                                        + Environment.NewLine
                                                        + " Factura " + oFACTURA.INVOICE_ID + "  La cuenta contable " + oFACTURA.LINEAS[i].GL_ACCOUNT_ID
                                                        + " no tiene su clave del SAT", true, true, true);
                                                            if (Globales.automatico)
                                                            {
                                                                return false;
                                                            }
                                                            ClasificadorProductos.formularios.frmProductoClasificacion oProducto
                                                                = new ClasificadorProductos.formularios.frmProductoClasificacion(oFACTURA.LINEAS[i].GL_ACCOUNT_ID);
                                                            oProducto.ShowDialog();
                                                            if (oProducto.oRegistro == null)
                                                            {
                                                                ErrorFX.mostrar(" Factura " + oFACTURA.INVOICE_ID + " Acción cancelada por el usuario la cuenta contable " + oFACTURA.LINEAS[i].PART_ID
                                                                + " no tiene su clave del SAT", true, true, true);
                                                                return false;
                                                            }
                                                            oComprobanteConcepto.ClaveProdServ = ObtenerClaveProdServ(oProducto.oRegistro.clasificacionSAT.Trim());
                                                            if (!String.IsNullOrEmpty(oProducto.oRegistro.unidadSAT))
                                                            {
                                                                oComprobanteConcepto.ClaveUnidad = ObtenerClaveUnidad(oProducto.oRegistro.unidadSAT.Trim().ToUpper());
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                            catch (Exception errGeneral)
                                            {
                                                ErrorFX.mostrar(errGeneral, true, true, "Generar40 - 734 - Seccion de lineas - Asignación de Clave de Producto SAT", true);
                                                return false;
                                            }
                                        }


                                    }
                                }
                                catch (Exception errGeneral)
                                {
                                    ErrorFX.mostrar(errGeneral, true, true, "Generar40 - 734 - Seccion de lineas - Clave de Producto", true);
                                    return false;
                                }
                                oComprobanteConcepto.Descripcion = Validacion(oFACTURA.LINEAS[i].DESCRIPCION);

                                decimal unitPrice = 0;
                                try
                                {
                                    unitPrice = decimal.Parse(oFACTURA.LINEAS[i].UNIT_PRICE);
                                }
                                catch
                                {
                                    //23/01/2018: Precio unitario FIMAS
                                    //Precio unitario de FIMA
                                    if (oEncapsular.ocEMPRESA.ENTITY_ID.Contains("FIMA"))
                                    {
                                        unitPrice = Math.Round(decimal.Parse(oFACTURA.LINEAS[i].AMOUNT) / oComprobanteConcepto.Cantidad, 4);
                                    }
                                    else
                                    {
                                        unitPrice = Math.Round(decimal.Parse(oFACTURA.LINEAS[i].AMOUNT) / oComprobanteConcepto.Cantidad, 2);
                                    }

                                }
                                oComprobanteConcepto.ValorUnitario = Math.Round(Math.Abs(unitPrice), 4);
                                oComprobanteConcepto.Importe = OtroTruncar(Math.Abs(decimal.Parse(oFACTURA.LINEAS[i].AMOUNT))
                                    , oEncapsular.ocEMPRESA);

                                oComprobanteConcepto.Importe = Math.Abs(oComprobanteConcepto.Importe);
                                oComprobanteConcepto.ValorUnitario = Math.Abs(oComprobanteConcepto.ValorUnitario);

                                //Buscar la conversión
                                configuracionUM oconfiguracionUM = BuscarClaveUnidad(oFACTURA.LINEAS[i].STOCK_UM);
                                if (oconfiguracionUM != null)
                                {
                                    if (!String.IsNullOrEmpty(oconfiguracionUM.UnidadMedida))
                                    {
                                        oComprobanteConcepto.ClaveUnidad = ObtenerClaveUnidad(oconfiguracionUM.SAT.ToUpper());
                                        //oComprobanteConcepto.Unidad = oComprobanteConcepto.ClaveUnidad;
                                    }
                                }
                                //if (decimal.Parse(oFACTURA.LINEAS[i].VAT_AMOUNT) == 0)
                                //{
                                //    //Rodrigo Escalona: 12/06/2023 Si es Tasa 0                                    
                                //    oComprobanteConcepto.ObjetoImp = "01";// c_ObjetoImp.Item01;
                                //}
                                //else
                                //{
                                    //oComprobanteConcepto.Importe = OtroTruncar(oComprobanteConcepto.Importe, oEncapsular.ocEMPRESA);
                                    oComprobanteConcepto.ObjetoImp = "02";// c_ObjetoImp.Item02;
                                    AgregarImpuestos = true;
                                    oComprobanteConcepto.Impuestos = new CFDI40.ComprobanteConceptoImpuestos();
                                    oComprobanteConcepto.Impuestos.Traslados = new CFDI40.ComprobanteConceptoImpuestosTraslado[1];
                                    oComprobanteConcepto.Impuestos.Traslados[0] = new CFDI40.ComprobanteConceptoImpuestosTraslado();
                                    oComprobanteConcepto.Impuestos.Traslados[0].TasaOCuota = decimal.Parse("0.160000");
                                    if (decimal.Parse(oFACTURA.LINEAS[i].VAT_AMOUNT) == 0)
                                        {
                                        oComprobanteConcepto.Impuestos.Traslados[0].TasaOCuota = decimal.Parse("0.00000");
                                    }
                                    
                                    oComprobanteConcepto.Impuestos.Traslados[0].TasaOCuotaSpecified = true;
                                    oComprobanteConcepto.Impuestos.Traslados[0].TipoFactor = CFDI40.c_TipoFactor.Tasa;
                                    oComprobanteConcepto.Impuestos.Traslados[0].Importe = OtroTruncar(Math.Abs(decimal.Parse(oFACTURA.LINEAS[i].VAT_AMOUNT)), oEncapsular.ocEMPRESA);

                                decimal BaseTr = Math.Abs(decimal.Parse(oFACTURA.LINEAS[i].AMOUNT));
                                if (!String.IsNullOrEmpty(oFACTURA.LINEAS[i].Descuento))
                                {
                                    decimal DescuentoTr = OtroTruncar(decimal.Parse(oFACTURA.LINEAS[i].Descuento) * oComprobanteConcepto.Cantidad
                                                , oEncapsular.ocEMPRESA);
                                    BaseTr -= DescuentoTr;
                                }

                                oComprobanteConcepto.Impuestos.Traslados[0].Base = OtroTruncar(BaseTr, oEncapsular.ocEMPRESA);

                                //Rodrigo Escalona: 14/09/2023 Verificar la base
                                if (oComprobanteConcepto.Impuestos.Traslados[0].TasaOCuota== decimal.Parse("0.160000"))
                                {

                                }

                                   
                                    oComprobanteConcepto.Impuestos.Traslados[0].ImporteSpecified = true;

                                    decimal porcentajeTasaTr = 0;
                                    if (decimal.Parse(oFACTURA.LINEAS[i].VAT_AMOUNT) != 0)
                                    {
                                        porcentajeTasaTr = Math.Abs(((decimal.Parse(oFACTURA.LINEAS[i].AMOUNT) * 100) / decimal.Parse(oFACTURA.LINEAS[i].VAT_AMOUNT)) / 100);
                                    }
                                    oComprobanteConcepto.Impuestos.Traslados[0].TasaOCuota = Math.Round(porcentajeTasaTr, 2);
                                    oComprobanteConcepto.Impuestos.Traslados[0].Impuesto = CFDI40.c_Impuesto.Item002;

                                    if (oComprobanteConcepto.Impuestos.Traslados[0].Importe == 0)
                                    {
                                        oComprobanteConcepto.Impuestos.Traslados[0].TasaOCuota = decimal.Parse("0.000000");
                                    }
                                    else
                                    {
                                        oComprobanteConcepto.Impuestos.Traslados[0].TasaOCuota = decimal.Parse("0.160000");

                                        // Calcular el impuesto y forzar el redondeo hacia abajo
                                        //decimal baseImpuesto = oComprobanteConcepto.Impuestos.Traslados[0].Base;
                                        //decimal tasa = 0.16M;

                                        //decimal impuesto = Math.Floor(baseImpuesto * tasa * 100) / 100; // Asegura que el resultado sea .58

                                        //oComprobanteConcepto.Impuestos.Traslados[0].Importe = impuesto;


                                        TotalImpuestoImporte += oComprobanteConcepto.Impuestos.Traslados[0].Importe;
                                        TotalImpuestoBase += oComprobanteConcepto.Impuestos.Traslados[0].Base;
                                    }
                                //}


                                //Agregar la retención
                                if (oFACTURA.TOTAL_RETENIDO != 0)
                                {
                                    if (oFACTURA.LINEAS[i].retencion != null)
                                    {

                                        if (oFACTURA.LINEAS[i].retencion.Count > 0)
                                        {
                                            oComprobanteConcepto.Impuestos.Retenciones = new CFDI40.ComprobanteConceptoImpuestosRetencion[oFACTURA.LINEAS[i].retencion.Count];
                                            int contadorRetencion = 0;
                                            foreach (cLINEARETENCION LineaRetencion in oFACTURA.LINEAS[i].retencion)
                                            {
                                                oComprobanteConcepto.Impuestos.Retenciones[contadorRetencion] = new CFDI40.ComprobanteConceptoImpuestosRetencion();
                                                oComprobanteConcepto.Impuestos.Retenciones[contadorRetencion].TipoFactor = CFDI40.c_TipoFactor.Tasa;
                                                decimal porcentajeTasa = 0;
                                                if (decimal.Parse(oFACTURA.LINEAS[i].AMOUNT) != 0)
                                                {
                                                    porcentajeTasa = Math.Abs(((decimal.Parse(LineaRetencion.AMOUNT) * 100) / decimal.Parse(oFACTURA.LINEAS[i].AMOUNT)) / 100);
                                                }
                                                oComprobanteConcepto.Impuestos.Retenciones[contadorRetencion].TasaOCuota = Math.Round(porcentajeTasa, 3);
                                                if (porcentajeTasa > (decimal)0.16)
                                                {
                                                    oComprobanteConcepto.Impuestos.Retenciones[contadorRetencion].TasaOCuota = (decimal)0.16;
                                                }
                                                oComprobanteConcepto.Impuestos.Retenciones[contadorRetencion].Impuesto = c_Impuesto.Item002;
                                                oComprobanteConcepto.Impuestos.Retenciones[contadorRetencion].Base = OtroTruncar(Math.Abs(decimal.Parse(oFACTURA.LINEAS[i].AMOUNT)), oEncapsular.ocEMPRESA);
                                                oComprobanteConcepto.Impuestos.Retenciones[contadorRetencion].Importe = OtroTruncar(Math.Abs(decimal.Parse(LineaRetencion.AMOUNT)), oEncapsular.ocEMPRESA);

                                                BitacoraFX.Log("Retencion " + contadorRetencion.ToString() + " oFACTURA.LINEAS[i].retencion = " + oFACTURA.LINEAS[i].retencion.ToString()
                                                    + "  porcentajeTasa=" + porcentajeTasa.ToString()
                                                    + "  GL_ACCOUNT_ID=" + LineaRetencion.GL_ACCOUNT_ID.ToString()
                                                    + "  porcentajeTasa=" + porcentajeTasa.ToString()
                                                    );

                                                contadorRetencion++;
                                            }

                                        }


                                    }
                                    else
                                    {
                                        //Agregar la rentecion GLOBAL GENERAL
                                        oComprobanteConcepto.Impuestos.Retenciones = new CFDI40.ComprobanteConceptoImpuestosRetencion[1];
                                        oComprobanteConcepto.Impuestos.Retenciones[0] = new CFDI40.ComprobanteConceptoImpuestosRetencion();
                                        oComprobanteConcepto.Impuestos.Retenciones[0].TipoFactor = c_TipoFactor.Tasa;

                                        //oComprobanteConcepto.Impuestos.Retenciones[0].TasaOCuota = decimal.Parse("0.160000");
                                        //if (oComprobanteConcepto.Impuestos.Retenciones[0].Importe == 0)
                                        //{
                                        //    oComprobanteConcepto.Impuestos.Retenciones[0].TasaOCuota = decimal.Parse("0.000000");
                                        //}
                                        //Determinar la Tasa
                                        decimal porcentajeTasa = 0;
                                        if (decimal.Parse(oFACTURA.LINEAS[i].AMOUNT) != 0)
                                        {
                                            porcentajeTasa = Math.Abs(((oFACTURA.TOTAL_RETENIDO * 100) / decimal.Parse(oFACTURA.LINEAS[i].AMOUNT)) / 100);
                                        }
                                        oComprobanteConcepto.Impuestos.Retenciones[0].TasaOCuota = Math.Round(porcentajeTasa, 3);


                                        oComprobanteConcepto.Impuestos.Retenciones[0].Impuesto = c_Impuesto.Item002;
                                        oComprobanteConcepto.Impuestos.Retenciones[0].Base = OtroTruncar(Math.Abs(decimal.Parse(oFACTURA.LINEAS[i].AMOUNT)), oEncapsular.ocEMPRESA);
                                        oComprobanteConcepto.Impuestos.Retenciones[0].Importe = OtroTruncar(Math.Abs(oFACTURA.TOTAL_RETENIDO), oEncapsular.ocEMPRESA);


                                    }
                                }

                                if (oEncapsular.ocEMPRESA.ENTITY_ID.Contains("FIMA"))
                                {
                                    if (oComprobanteConcepto.Descripcion.Contains("SERVICIO"))
                                    {
                                        oComprobanteConcepto.ClaveUnidad = "E48";//;CFDI40.c_ClaveUnidad.E48;
                                    }
                                    //if (String.IsNullOrEmpty(oComprobanteConcepto.ClaveUnidad))
                                    //{
                                    //    oComprobanteConcepto.ClaveUnidad = CFDI40.c_ClaveUnidad.H87;
                                    //    if (oComprobanteConcepto.Descripcion.Contains("SERVICIO"))
                                    //    {
                                    //        oComprobanteConcepto.ClaveUnidad = CFDI40.c_ClaveUnidad.E48;
                                    //    }
                                    //}

                                }

                                //Cuando es factura de anticipo
                                //if (oComprobante.TipoDeComprobante.ToUpper().Equals("E"))//c_TipoDeComprobante.E)
                                //{
                                //    oComprobanteConcepto.ClaveProdServ = "84111506";
                                //    oComprobanteConcepto.ClaveUnidad = "ACT";
                                //}

                                //Rodrigo Escalona: NDC si es automatizada 26/07/2018

                                try
                                {
                                    if (oComprobante.TipoDeComprobante.Equals("E"))
                                    {
                                        if (bool.Parse(ocSERIE.ndcUsarPropio))
                                        {
                                            ndcUsarPropio = true;
                                            oComprobanteConcepto.ClaveProdServ = ObtenerClaveProdServ(ocSERIE.ndcClaveProd);
                                            oComprobanteConcepto.ClaveUnidad = ObtenerClaveUnidad(ocSERIE.ndcUnidad.ToUpper());
                                        }
                                    }


                                }
                                catch (Exception errGeneral)
                                {
                                    ErrorFX.mostrar(errGeneral, true, true, "Generar40 - 620 - Seccion de Cabecera NDC automatizada ", true);
                                    return false;
                                }

                                oComprobanteConceptos[i] = new CFDI40.ComprobanteConcepto();

                                //Validar que la ClaveProdServ no este en blanco
                                //if (!String.IsNullOrEmpty(oComprobanteConcepto.ClaveProdServ))
                                //{
                                //    //Cargar ClaveProdServ del NoIdentificacion
                                //    if (!String.IsNullOrEmpty(oComprobanteConcepto.NoIdentificacion))
                                //    {
                                //        //Buscar el numero de parte el clasificador
                                //        ndcFacturaLinea lineaTmp = dbContext.ndcFacturaLineaSet.Where(a => a.PART_ID == oComprobanteConcepto.NoIdentificacion
                                //        & a.INVOICE_ID != oFACTURA.INVOICE_ID).FirstOrDefault();
                                //        if (lineaTmp != null)
                                //        {
                                //            oComprobanteConcepto.ClaveProdServ = lineaTmp.clasificacion;

                                //        }

                                //        //Actualizarlo en la tabla

                                //    }
                                //    else
                                //    {
                                //        oComprobanteConcepto.NoIdentificacion = oComprobanteConcepto.ClaveProdServ;
                                //    }
                                //}
                                //Limpiar la ClaveProdSer
                                //if (!String.IsNullOrEmpty(oComprobanteConcepto.ClaveProdServ))
                                //{
                                //    oComprobanteConcepto.ClaveProdServ = Regex.Replace(oComprobanteConcepto.ClaveProdServ, @"^[A-Za-z]+", "");
                                //    oComprobanteConcepto.ClaveProdServ = oComprobanteConcepto.ClaveProdServ.Trim().TrimEnd().TrimStart();
                                //    string ClaveProdServTr = "";
                                //    foreach (char c in oComprobanteConcepto.ClaveProdServ)
                                //    {
                                //        if (!(c < '0' || c > '9'))
                                //        {
                                //            ClaveProdServTr += c;
                                //        }
                                //    }
                                //    oComprobanteConcepto.ClaveProdServ = ClaveProdServTr;
                                //}

                                //Rodrigo Escalona 05/10/2018
                                //NoIdentificacion
                                //Si esta nulo agregar la misma 
                                if (String.IsNullOrEmpty(oComprobanteConcepto.NoIdentificacion))
                                {
                                    if (oComprobanteConcepto.Descripcion.Length > 0)
                                    {
                                        //Agregar la primera palabra de la descripción
                                        string[] arregloDescripcion = oComprobanteConcepto.Descripcion.Trim().Split(' ');
                                        if (arregloDescripcion.Length > 0)
                                        {
                                            int arreglo = 0;
                                            if (String.IsNullOrEmpty(arregloDescripcion[arreglo]))
                                            {
                                                arreglo++;
                                            }
                                            oComprobanteConcepto.NoIdentificacion = arregloDescripcion[arreglo];
                                        }


                                    }
                                    else
                                    {
                                        //Agregar la clave del producto
                                        oComprobanteConcepto.NoIdentificacion = oComprobanteConcepto.ClaveProdServ.ToString();
                                    }
                                }

                                //Rodrigo Escalona 11/07/2019 Si esta en nulo
                                //if (String.IsNullOrEmpty(oComprobanteConcepto.ClaveProdServ))
                                //{

                                //    oComprobanteConcepto.ClaveProdServ = "84111506";
                                //    oComprobanteConcepto.Unidad = "ACT";
                                //}

                                //Agregar informacion Aduanera
                                //Rodrigo Escalona: 24/11/2019 
                                cPEDIMENTO ocPEDIMENTO = new cPEDIMENTO(oData, oEncapsular.ocEMPRESA);
                                List<cPEDIMENTO> oPedimentos = new List<cPEDIMENTO>();
                                oPedimentos = ocPEDIMENTO.obtener_linea(oFACTURA.INVOICE_ID, oFACTURA.LINEAS[i].LINEA, oEncapsular.ocEMPRESA);
                                if (oPedimentos.Count > 0)
                                {
                                    oComprobanteConcepto.InformacionAduanera = new ComprobanteConceptoInformacionAduanera[oPedimentos.Count];
                                    int contadorI = 0;
                                    foreach (cPEDIMENTO pedimento in oPedimentos)
                                    {
                                        oComprobanteConcepto.InformacionAduanera[contadorI] = new ComprobanteConceptoInformacionAduanera();
                                        oComprobanteConcepto.InformacionAduanera[contadorI].NumeroPedimento = pedimento.PEDIMENTO;
                                        contadorI++;
                                    }
                                }
                                if (oComprobanteConcepto.ClaveUnidad != null)
                                {
                                    if (oComprobanteConcepto.ClaveUnidad.Equals("KGM"))
                                    {
                                        //Rodrigo Escalona 29/04/2021 Fima no quiere KF
                                        if (!oEncapsular.ocEMPRESA.ENTITY_ID.Contains("FIMA"))
                                        {
                                            oComprobanteConcepto.Unidad = "KG";
                                        }
                                    }

                                    //Rodrigo Escalona 24/06/2021
                                    if (oComprobanteConcepto.ClaveUnidad.Equals("H87"))
                                    {
                                        //Rodrigo Escalona 29/04/2021 Fima no quiere KF
                                        oComprobanteConcepto.Unidad = "PZA";

                                    }

                                }
                                else
                                {
                                    oComprobanteConcepto.ClaveUnidad = "ACT";
                                }

                                //Rodrigo Escalona 13/04/2021: Agregar la parte
                                if (EsKit(oFACTURA.LINEAS[i].PART_ID))
                                {
                                    List<CFDI40.ComprobanteConceptoParte> oComprobanteConceptoPartes = new List<CFDI40.ComprobanteConceptoParte>();
                                    oComprobanteConceptoPartes = ObtenerKit(oFACTURA.LINEAS[i].PART_ID, decimal.Parse(oFACTURA.LINEAS[i].QTY));
                                    if (oComprobanteConceptoPartes != null)
                                    {
                                        if (oComprobanteConceptoPartes.Count > 0)
                                        {
                                            oComprobanteConcepto.Parte = new CFDI40.ComprobanteConceptoParte[oComprobanteConceptoPartes.Count];
                                            oComprobanteConcepto.Parte = oComprobanteConceptoPartes.ToArray();
                                        }

                                    }


                                }

                                //Rodrigo Escalona 26/04/2021 UM Actualizarlas Buscar el codigo en el maestro de Articulos
                                if (!String.IsNullOrEmpty(oFACTURA.LINEAS[i].UM_CODE))
                                {
                                    configuracionUM busquedatr = BuscarClaveUnidad(oFACTURA.LINEAS[i].UM_CODE.Trim());
                                    if (busquedatr != null)
                                    {

                                        if (!oFACTURA.LINEAS[i].UM_CODE.Trim().Equals(busquedatr.SAT.Trim()))
                                        {
                                            oComprobanteConcepto.ClaveUnidad = ObtenerClaveUnidad(busquedatr.SAT.Trim());
                                        }
                                    }
                                }

                                //Rodrigo Escalona 29/04/2021 Conversión a Pallets


                                //Rodrigo Escalona 31/11/2021 Fima dice que no debe tener unidad 
                                if (oEncapsular.ocEMPRESA.ENTITY_ID.Contains("FIMA"))
                                {
                                    oComprobanteConcepto.Unidad = null;
                                }

                                //Rodrigo Escalona 31/11/2021 Losifra dice que los servicios deben buscar en la tabla
                                if (oEncapsular.ocEMPRESA.SITE_ID.Contains("Losifra"))
                                {
                                    if (!String.IsNullOrEmpty(oFACTURA.LINEAS[i].SERVICE_CHARGE_ID))
                                    {
                                        configuracionProducto oConfiguracionProducto = oFACTURA.buscarClave(string.Empty
                                                            , string.Empty, string.Empty
                                                            , oFACTURA.LINEAS[i].SERVICE_CHARGE_ID);
                                        oComprobanteConcepto.ClaveUnidad = ObtenerClaveUnidad(oConfiguracionProducto.unidadSAT);
                                    }
                                }

                                if (String.IsNullOrEmpty(oComprobanteConcepto.ClaveProdServ))
                                {
                                    configuracionProducto oConfiguracionProducto = oFACTURA.buscarClave(oFACTURA.LINEAS[i].PART_ID
                                                            , oFACTURA.LINEAS[i].CUSTOMER_PART_ID, oFACTURA.LINEAS[i].GL_ACCOUNT_ID
                                                            , oFACTURA.LINEAS[i].SERVICE_CHARGE_ID);
                                    oComprobanteConcepto.ClaveProdServ = oConfiguracionProducto.clasificacionSAT;
                                }

                                //Rodrigo Escalona 23/03/2023 Tigthico Validar Kit
                                //Rodrigo Escalona: 23032023 Para Tighitco que sea KIT

                                if (oComprobanteConcepto.Parte != null)
                                {
                                    if (oComprobanteConcepto.Parte.Length > 0)
                                    {
                                        if (oEncapsular.ocEMPRESA.ENTITY_ID.Equals("TIGLA"))
                                        {
                                            oComprobanteConcepto.ClaveUnidad = "XKI";
                                            oComprobanteConcepto.Unidad = "KIT";
                                        }
                                        else
                                        {
                                            if (oEncapsular.ocEMPRESA.ENTITY_ID.Equals("CHIME"))
                                            {
                                                oComprobanteConcepto.ClaveUnidad = "XKI";
                                                oComprobanteConcepto.Unidad = "KIT";
                                            }
                                        }
                                    }
                                }
                                oComprobanteConceptos[i] = oComprobanteConcepto;
                            }


                        }
                    }




                    oComprobante.Conceptos = oComprobanteConceptos;
                }
                catch (Exception errGeneral)
                {
                    ErrorFX.mostrar(errGeneral, true, true, "Generar40 - 842 - Seccion de lineas ", true);
                    return false;
                }
                //MessageBox.Show("Comprobante22");

                try
                {
                    //Rodrigo Escalona: 28/10/2022 Validar si hay impuestos
                    if (AgregarImpuestos)
                    {
                        //MessageBox.Show("Comprobante23");
                        //Elemento Impuestos
                        CFDI40.ComprobanteImpuestos oImpuesto = new CFDI40.ComprobanteImpuestos();
                        //Impuestos  Trasladados

                        oImpuesto.TotalImpuestosTrasladadosSpecified = true;
                        if (oFACTURA.anticipo)
                        {
                            oImpuesto.TotalImpuestosTrasladados = Math.Abs(oFACTURA.TOTAL_VAT_AMOUNT) - oFACTURA.anticipoVAT_AMOUNT;
                            oImpuesto.TotalImpuestosTrasladados = Math.Round(oImpuesto.TotalImpuestosTrasladados, 2);
                        }
                        else
                        {
                            oImpuesto.TotalImpuestosTrasladados = Math.Abs(oFACTURA.TOTAL_VAT_AMOUNT);
                            oImpuesto.TotalImpuestosTrasladados = Math.Round(oImpuesto.TotalImpuestosTrasladados, 2);
                        }
                        //Rodrigo Escalona: 26/07/2018 Importe 2
                        oImpuesto.TotalImpuestosTrasladados = Math.Round(oImpuesto.TotalImpuestosTrasladados, 2); //TotalImpuestoImporte;//Math.Round(oImpuesto.TotalImpuestosTrasladados, 2);

                        //Impuestos  Trasladados
                        CFDI40.ComprobanteImpuestosTraslado[] oComprobanteImpuestosTraslados;
                        oComprobanteImpuestosTraslados = new CFDI40.ComprobanteImpuestosTraslado[1];
                        //IVA
                        CFDI40.ComprobanteImpuestosTraslado oComprobanteImpuestosTraslado = new CFDI40.ComprobanteImpuestosTraslado();
                        oComprobanteImpuestosTraslado.ImporteSpecified = true;
                        oComprobanteImpuestosTraslado.Base = TotalImpuestoBase;//oImpuesto.TotalImpuestosTrasladados;

                        if (oFACTURA.anticipo)
                        {
                            oComprobanteImpuestosTraslado.Importe = Math.Abs(oFACTURA.TOTAL_VAT_AMOUNT) - oFACTURA.anticipoVAT_AMOUNT;
                            oComprobanteImpuestosTraslado.Importe = Math.Round(oComprobanteImpuestosTraslado.Importe, 2);
                        }
                        else
                        {
                            oComprobanteImpuestosTraslado.Importe = Math.Abs(oFACTURA.TOTAL_VAT_AMOUNT);
                            oComprobanteImpuestosTraslado.Importe = Math.Round(oComprobanteImpuestosTraslado.Importe, 2);
                        }
                        //Rodrigo Escalona: 26/07/2018 Importe 2
                        oComprobanteImpuestosTraslado.Importe = Math.Round(oComprobanteImpuestosTraslado.Importe, 2); //TotalImpuestoImporte;//Math.Round(oComprobanteImpuestosTraslado.Importe, 2);
                        oComprobanteImpuestosTraslado.TasaOCuotaSpecified = true;
                        oComprobanteImpuestosTraslado.TasaOCuota = decimal.Parse("0.160000");//oFACTURA.VAT_PERCENT;
                        if (oComprobanteImpuestosTraslado.Importe == 0)
                        {
                            oComprobanteImpuestosTraslado.TasaOCuota = decimal.Parse("0.000000");//oFACTURA.VAT_PERCENT;
                        }
                        oComprobanteImpuestosTraslado.Impuesto = c_Impuesto.Item002;
                        oComprobanteImpuestosTraslado.TipoFactor = c_TipoFactor.Tasa;
                        oComprobanteImpuestosTraslados[0] = new CFDI40.ComprobanteImpuestosTraslado();
                        oComprobanteImpuestosTraslados[0] = oComprobanteImpuestosTraslado;
                        //IEPS
                        //oComprobanteImpuestosTraslado = new ComprobanteImpuestosTraslado();
                        //oComprobanteImpuestosTraslado.Importe = 0;
                        //oComprobanteImpuestosTraslado.TasaOCuota = 0;
                        //oComprobanteImpuestosTraslado.TipoFactor = "Tasa";
                        //oComprobanteImpuestosTraslado.Impuesto = "001";//c_Impuesto.Item003;
                        //oComprobanteImpuestosTraslados[1] = new ComprobanteImpuestosTraslado();
                        //oComprobanteImpuestosTraslados[1] = oComprobanteImpuestosTraslado;
                        oImpuesto.Traslados = oComprobanteImpuestosTraslados;
                        //Retenidos
                        if (oFACTURA.TOTAL_RETENIDO != 0)
                        {
                            CFDI40.ComprobanteImpuestosRetencion[] oComprobanteImpuestosRetencion = new CFDI40.ComprobanteImpuestosRetencion[1];
                            oComprobanteImpuestosRetencion[0] = new CFDI40.ComprobanteImpuestosRetencion();
                            oComprobanteImpuestosRetencion[0].Importe = Math.Abs(oFACTURA.TOTAL_RETENIDO);
                            oComprobanteImpuestosRetencion[0].Importe = Math.Round(oComprobanteImpuestosRetencion[0].Importe, 2);
                            oComprobanteImpuestosRetencion[0].Impuesto = "002";//c_Impuesto.Item002;
                            oImpuesto.Retenciones = oComprobanteImpuestosRetencion;
                            oImpuesto.TotalImpuestosRetenidosSpecified = true;
                            oImpuesto.TotalImpuestosRetenidos = Math.Abs(oFACTURA.TOTAL_RETENIDO);
                            oImpuesto.TotalImpuestosRetenidos = Math.Round(oImpuesto.TotalImpuestosRetenidos, 2);
                        }
                        oComprobante.Impuestos = oImpuesto;
                    }

                }
                catch (Exception errGeneral)
                {
                    ErrorFX.mostrar(errGeneral, true, true, "Generar40 - 895 - Seccion de impuestos ", true);
                    return false;
                }
                /*
                try
                {
                    oComprobante.FormaPagoSpecified = true;
                    if (String.IsNullOrEmpty(ocFACTURA_BANDEJAtmp.FORMA_DE_PAGO))
                    {
                        oComprobante.FormaPago = c_FormaPago.Item99;
                        oComprobante.FormaPagoSpecified = true;
                    }
                    else
                    {
                        oComprobante.FormaPago = CFDI40.c_FormaPago.Item03;
                    }

                }
                catch (Exception errGeneral)
                {
                    ErrorFX.mostrar(errGeneral, true, true, "Generar40 - 1422 - Seccion de forma de pago ", true);
                    return false;
                }
                */
                //FIMA REDONDEO

                if (oEncapsular.ocEMPRESA.ENTITY_ID.Contains("FIMA"))
                {
                    //if (oComprobante.TipoDeComprobante.Equals("I"))
                    {
                        //Recalcular todo el XML con todos los decimales
                        decimal subTotal = 0;
                        decimal descuentoTr = 0;
                        decimal TotalImpuestosTrasladadosTr = 0;
                        int i = 0;
                        foreach (CFDI40.ComprobanteConcepto oComprobanteConcepto in oComprobante.Conceptos)
                        {
                            if (oComprobanteConcepto != null)
                            {
                                //subTotal += Math.Abs(oComprobanteConcepto.Cantidad * oComprobanteConcepto.ValorUnitario);
                                //oComprobante.Conceptos[i].Importe = Math.Abs(oComprobanteConcepto.Cantidad * oComprobanteConcepto.ValorUnitario);

                                subTotal += oComprobante.Conceptos[i].Importe;
                                if (oComprobante.Conceptos[i].DescuentoSpecified)
                                {
                                    descuentoTr += oComprobante.Conceptos[i].Descuento;
                                }

                                //oComprobante.Conceptos[i].Importe = OtroTruncar(oComprobante.Conceptos[i].Importe, oEncapsular.ocEMPRESA);
                                if (oComprobante.Conceptos[i].Impuestos != null)
                                {
                                    decimal importeLinea = oComprobante.Conceptos[i].Importe;
                                    if (oComprobante.Conceptos[i].DescuentoSpecified)
                                    {
                                        importeLinea -= oComprobante.Conceptos[i].Descuento;
                                    }


                                    oComprobante.Conceptos[i].Impuestos.Traslados[0].Base = importeLinea;
                                    if (oComprobante.Conceptos[i].Impuestos.Traslados[0].TasaOCuota != 0)
                                    {
                                        oComprobante.Conceptos[i].Impuestos.Traslados[0].Importe = OtroTruncar(importeLinea * oComprobante.Conceptos[i].Impuestos.Traslados[0].TasaOCuota, oEncapsular.ocEMPRESA);
                                        TotalImpuestosTrasladadosTr += oComprobante.Conceptos[i].Impuestos.Traslados[0].Importe;
                                    }
                                    else
                                    {
                                        oComprobante.Conceptos[i].Impuestos.Traslados[0].Importe = 0;
                                    }
                                }

                                i++;
                            }
                        }
                        //Modifico: 19/12/2017
                        oComprobante.SubTotal = OtroTruncar(subTotal, oEncapsular.ocEMPRESA);
                        if (descuentoTr != 0)
                        {
                            oComprobante.Descuento = OtroTruncar(descuentoTr, oEncapsular.ocEMPRESA);
                            oComprobante.DescuentoSpecified = true;
                        }
                        
                        //Version 3.3
                        //oComprobante.SubTotal = Math.Truncate(100 * oComprobante.SubTotal) / 100;

                        oComprobante.TipoCambio = OtroTruncar(oComprobante.TipoCambio, oEncapsular.ocEMPRESA);
                        //Tipo de cambio 6 decimales
                        //oComprobante.TipoCambio = OtroTruncar(oComprobante.TipoCambio, oEncapsular.ocEMPRESA);
                        try
                        {
                            if (oComprobante.Impuestos != null)
                            {
                                if (oComprobante.Impuestos.TotalImpuestosTrasladados != 0)
                                {
                                    //oComprobante.Impuestos.totalImpuestosTrasladados = Math.Abs(subTotal)*decimal.Parse("0.16");
                                    oComprobante.Impuestos.TotalImpuestosTrasladados = oComprobante.Total - oComprobante.SubTotal;
                                    if (oComprobante.Impuestos.Traslados.Length != 0)
                                    {
                                        //Buscar el iva
                                        int imp = 0;
                                        decimal TotalImpuestosTrasladadosTrTotal = 0;
                                        foreach (CFDI40.ComprobanteImpuestosTraslado impuesto in oComprobante.Impuestos.Traslados)
                                        {
                                            if (impuesto.Impuesto == c_Impuesto.Item001)//c_Impuesto.Item001)
                                            {
                                                oComprobante.Impuestos.Traslados[imp].Base = oComprobante.SubTotal;
                                                oComprobante.Impuestos.Traslados[imp].Importe = TotalImpuestosTrasladadosTr;//oComprobante.Impuestos.TotalImpuestosTrasladados;
                                                TotalImpuestosTrasladadosTrTotal += oComprobante.Impuestos.Traslados[imp].Importe;
                                                break;
                                            }
                                            if (impuesto.Impuesto == c_Impuesto.Item002)//c_Impuesto.Item001)
                                            {
                                                oComprobante.Impuestos.Traslados[imp].Base = oComprobante.SubTotal;
                                                oComprobante.Impuestos.Traslados[imp].Importe = TotalImpuestosTrasladadosTr; //oComprobante.Impuestos.TotalImpuestosTrasladados;
                                                TotalImpuestosTrasladadosTrTotal += oComprobante.Impuestos.Traslados[imp].Importe;
                                                break;
                                            }
                                            imp++;
                                        }

                                        oComprobante.Impuestos.TotalImpuestosTrasladados = TotalImpuestosTrasladadosTrTotal;
                                    }
                                    oComprobante.Total = OtroTruncar(Math.Abs(oComprobante.Impuestos.TotalImpuestosTrasladados) - Math.Abs(oComprobante.Impuestos.TotalImpuestosRetenidos) - Math.Abs(oComprobante.Descuento) + Math.Abs(oComprobante.SubTotal), oEncapsular.ocEMPRESA);
                                }
                                else
                                {
                                    oComprobante.Total = OtroTruncar(Math.Abs(oComprobante.SubTotal) - Math.Abs(oComprobante.Impuestos.TotalImpuestosRetenidos) - Math.Abs(oComprobante.Descuento), oEncapsular.ocEMPRESA);
                                }
                                //Fin de recalcular
                            }
                        }
                        catch //(Exception errGeneral)
                        {
                            //ErrorFX.mostrar(errGeneral, true, true, "Generar40 - Impuestos - 1385 ", true);
                            //return false;
                        }


                        //Formatear todo
                        //oComprobante.subTotal = OtroTruncar(oComprobante.subTotal, ocEMPRESA);
                        //oComprobante.descuento = OtroTruncar(oComprobante.descuento, ocEMPRESA);
                        //oComprobante.total = OtroTruncar(oComprobante.total, ocEMPRESA);
                        //Lineas
                        oComprobante.Total = OtroTruncar(oComprobante.Total, oEncapsular.ocEMPRESA);

                        i = 0;
                        foreach (CFDI40.ComprobanteConcepto oComprobanteConcepto in oComprobante.Conceptos)
                        {
                            if (oComprobanteConcepto != null)
                            {
                                oComprobante.Conceptos[i].Cantidad = OtroTruncar(oComprobante.Conceptos[i].Cantidad, oEncapsular.ocEMPRESA);
                                //oComprobante.Conceptos[i].ValorUnitario = OtroTruncar(oComprobante.Conceptos[i].ValorUnitario, oEncapsular.ocEMPRESA);
                                oComprobante.Conceptos[i].Importe = OtroTruncar(oComprobante.Conceptos[i].Importe, oEncapsular.ocEMPRESA);
                                i++;
                            }
                        }

                        try
                        {
                            if (oComprobante.Impuestos != null)
                            {
                                if (oComprobante.Impuestos.TotalImpuestosTrasladados != 0)
                                {
                                    //oComprobante.Impuestos.totalImpuestosTrasladados = OtroTruncar(oComprobante.Impuestos.totalImpuestosTrasladados, ocEMPRESA);
                                    //oComprobante.Impuestos.TotalImpuestosTrasladados = oComprobante.Total - oComprobante.SubTotal;
                                    ////CFDI 3.3
                                    //oComprobante.Impuestos.TotalImpuestosTrasladados = Math.Truncate(100 * oComprobante.Impuestos.TotalImpuestosTrasladados) / 100;
                                    decimal TotalImpuestosTrasladadostr = 0;
                                    if (oComprobante.Impuestos.Traslados.Length != 0)
                                    {
                                        int imp = 0;
                                        foreach (CFDI40.ComprobanteImpuestosTraslado impuesto in oComprobante.Impuestos.Traslados)
                                        {
                                            if (impuesto.Impuesto == c_Impuesto.Item001)//c_Impuesto.Item001)
                                            {
                                                oComprobante.Impuestos.Traslados[imp].Importe = OtroTruncar(oComprobante.Impuestos.TotalImpuestosTrasladados, oEncapsular.ocEMPRESA); //OtroTruncar(oComprobante.Impuestos.Traslados[imp].importe, ocEMPRESA);
                                                break;
                                            }
                                            if (impuesto.Impuesto == c_Impuesto.Item002)//c_Impuesto.Item001)
                                            {
                                                TotalImpuestosTrasladadostr += OtroTruncar(impuesto.Importe, oEncapsular.ocEMPRESA); //OtroTruncar(oComprobante.Impuestos.Traslados[imp].importe, ocEMPRESA);
                                            }
                                            imp++;
                                        }
                                    }
                                    oComprobante.Impuestos.TotalImpuestosTrasladados = Math.Truncate(100 * TotalImpuestosTrasladadostr) / 100;
                                }
                            }
                        }
                        catch (Exception errGeneral)
                        {
                            ErrorFX.mostrar(errGeneral, true, true, "Generar40 - Impuestos Redondeo - 1456 ", true);
                            return false;
                        }

                    }
                }

                //FIMA REDONDEO

                //Autocuadrar subtotal
                //BitacoraFX.Log("Recalcular: recalcularSubTotal");
                //23/04/2018 Rodrigo Escalona: 
                try
                {
                    //BitacoraFX.Log("Recalcular: recalcularSubTotal");
                    if (bool.Parse(ConfigurationManager.AppSettings["recalcularSubTotal"].ToString()))
                    {
                        //Sumar las lineas 
                        decimal subtotal = 0;
                        decimal impuesto = 0;
                        decimal retencion = 0;
                        decimal descuento = 0;
                        foreach (CFDI40.ComprobanteConcepto oComprobanteConcepto in oComprobante.Conceptos)
                        {
                            subtotal += oComprobanteConcepto.Importe;
                            if (oComprobanteConcepto.DescuentoSpecified)
                            {
                                descuento += oComprobanteConcepto.Descuento;
                            }
                            if (oComprobanteConcepto.Impuestos != null)
                            {
                                if (oComprobanteConcepto.Impuestos.Traslados != null)
                                {
                                    foreach (ComprobanteConceptoImpuestosTraslado oImpuesto in oComprobanteConcepto.Impuestos.Traslados)
                                    {
                                        impuesto += oImpuesto.Importe;
                                    }
                                }
                                if (oComprobanteConcepto.Impuestos.Retenciones != null)
                                {
                                    foreach (ComprobanteConceptoImpuestosRetencion oImpuesto in oComprobanteConcepto.Impuestos.Retenciones)
                                    {
                                        retencion += Math.Abs(oImpuesto.Importe);
                                    }
                                }
                            }
                        }
                        BitacoraFX.Log("Recalcular: subtotal:" + subtotal.ToString() + " impuesto: " + impuesto.ToString()
                            + " Total: " + (subtotal + impuesto - retencion - descuento).ToString());
                        //Cambiar los Totales y Subtotales
                        oComprobante.SubTotal = subtotal;
                        if (descuento > 0)
                        {
                            oComprobante.Descuento = Math.Round(descuento,2);
                            oComprobante.DescuentoSpecified = true;
                        }

                        oComprobante.Total = subtotal + impuesto - retencion- descuento;
                    }
                }
                catch (Exception Error)
                {
                    //ErrorFX.mostrar(Error, true, true, "Generar40 - recalcularSubTotal - CCE ", true);
                }

                //Agregar Exportacion
                string ExportacionTr = "";
                try
                {
                    ModeloDocumentosFX.ModeloDocumentosFXContainer Db =
                        new ModeloDocumentosFX.ModeloDocumentosFXContainer();
                    var resultTr = from b in Db.cceClienteSet
                                                 .Where(a => a.CustomerId.Contains(oFACTURA.CUSTOMER_ID)
                                                 )
                                                 .OrderBy(a => a.CustomerId)
                                   select b;
                    foreach (ModeloDocumentosFX.cceCliente registrotr in resultTr)
                    {
                        if (registrotr != null)
                        {
                            if (!String.IsNullOrEmpty(registrotr.Exportacion))
                            {
                                ExportacionTr = registrotr.Exportacion;
                                switch (ExportacionTr)
                                {
                                    case "01":
                                        oComprobante.Exportacion = "01";//c_Exportacion.Item01;
                                        break;
                                    case "02":
                                        oComprobante.Exportacion = "02";//c_Exportacion.Item02;
                                        break;
                                    case "03":
                                        oComprobante.Exportacion = "03";//c_Exportacion.Item03;
                                        break;
                                    case "04":
                                        oComprobante.Exportacion = "04";//c_Exportacion.Item04;
                                        break;
                                }

                            }

                        }
                    }
                    Db.Dispose();
                }
                catch (Exception Error)
                {
                    BitacoraFX.Log("Generar-Complemento-CCE-1909 " + Error.Message.ToString());
                }




                //Buscar complementos:
                int totalComplementos = 0;
                BitacoraFX.Log("Generar-LeyendaFiscal-1926 ");
                XmlElement oLeyenda = ComplementoLeyenda(oFACTURA, oEncapsular, oData);
                if (oLeyenda != null)
                {
                    BitacoraFX.Log("Generar-LeyendaFiscal-No se proceso ");
                    leyendaFiscal = true;
                    totalComplementos++;
                }
                //Comercio Exterior
                cCCE ocCCE = new cCCE();
                XmlElement oCCE = null;
                bool cce = false;
                if (sinCEE)
                {
                    //Timbrar sin Complemento de CEE
                    cce = false;
                }
                else
                {
                    try
                    {
                        //Agregar el complemento
                        decimal TotalImpuestosTrasladados = 0;
                        if (oComprobante.Impuestos != null)
                        {
                            TotalImpuestosTrasladados = oComprobante.Impuestos.TotalImpuestosTrasladados;
                        }
                        cce = ocCCE.agregarComplemento(oEncapsular.ocEMPRESA, oFACTURA, TotalImpuestosTrasladados);
                        if (complementoComercioExterior)
                        {
                            cce = complementoComercioExterior;
                        }

                        
                        BitacoraFX.Log("Factura: " + oFACTURA.INVOICE_ID + " CCE:" + cce.ToString());
                        if (cce)
                        {


                            oCCE = ComplementoCCE(oComprobante, cce, oFACTURA, oEncapsular, ocCCE, ot_UbicacionReceptor, ocFACTURA_BANDEJAtmp);
                            if (oCCE != null)
                            {
                                oComprobante.Receptor.NumRegIdTrib = oFACTURA.TAX_ID_NUMBER;
                                oComprobante.Receptor.ResidenciaFiscal = ObtenerResidenciaFiscal(ot_UbicacionReceptor.pais);
                                oComprobante.Receptor.ResidenciaFiscalSpecified = true;
                                oComprobante.Receptor.RegimenFiscalReceptor = c_RegimenFiscal.Item616;
                                oComprobante.Receptor.UsoCFDI = c_UsoCFDI.S01;
                                oComprobante.Exportacion = "02";//c_Exportacion.Item02;


                                if (String.IsNullOrEmpty(ExportacionTr))
                                {
                                    try
                                    {
                                        switch (ocFACTURA_BANDEJAtmp.Exportacion)
                                        {
                                            case "01":
                                                oComprobante.Exportacion = "01";//c_Exportacion.Item01;
                                                break;
                                            case "02":
                                                oComprobante.Exportacion = "02";//c_Exportacion.Item02;
                                                break;
                                            case "03":
                                                oComprobante.Exportacion = "03";//c_Exportacion.Item03;
                                                break;
                                        }
                                    }
                                    catch (Exception Error)
                                    {
                                        BitacoraFX.Log("Generar-Complemento-CCE-1929 " + Error.Message.ToString());
                                    }
                                }
                                oComprobante.TipoCambio = OtroTruncar(oFACTURA.SELL_RATE, 4);
                                oComprobante.TipoCambioSpecified = true;
                                totalComplementos++;
                            }
                            else
                            {
                                //La factura requiere Complemento de Comercio Exterior dio un error su generación
                                ErrorFX.mostrar(" Factura " + oFACTURA.INVOICE_ID + " La factura " + oFACTURA.INVOICE_ID
                                    + " requiere Complemento de Comercio Exterior dio un error su generación", !Globales.automatico, true, true);
                                return false;
                            }
                        }
                    }
                    catch (Exception errGeneral)
                    {
                        ErrorFX.mostrar(errGeneral, true, true, "Generar40 - 1559 - CCE ", true);
                        return false;
                    }
                }

                //////////Complemento de Carta Porte de Traslado
                ////////XmlElement OCartePorte = null;
                ////////if (DoCartaPorte(oFACTURA.INVOICE_ID))
                ////////{
                ////////    OCartePorte = ComplementoCartaPorte(oFACTURA, oComprobante);
                ////////    if (OCartePorte != null)
                ////////    {
                ////////        //AGregar el namespace
                ////////        myNamespaces.Add("cartaporte20", "http://www.sat.gob.mx/CartaPorte20");
                ////////        totalComplementos++;
                ////////    }
                ////////}


                if (totalComplementos != 0)
                {
                    oComprobante.Complemento = new CFDI40.ComprobanteComplemento();
                    //oComprobante.Complemento[0] = new CFDI40.ComprobanteComplemento();
                    oComprobante.Complemento.Any = new XmlElement[totalComplementos];
                    totalComplementos = 0;
                    if (oLeyenda != null)
                    {
                        oComprobante.Complemento.Any[totalComplementos] = oLeyenda;
                        totalComplementos++;

                    }
                    if (oCCE != null)
                    {
                        oComprobante.Complemento.Any[totalComplementos] = oCCE;
                        totalComplementos++;
                    }
                }



            }
            catch (Exception errGeneral)
            {
                ErrorFX.mostrar(errGeneral, true, true, "Generar40 - 913 ", true);
                return false;
            }

            try
            {
                if (!String.IsNullOrEmpty(ocSERIE.CLIENTE_TRASLADO))
                {
                    if (oFACTURA.CUSTOMER_ID.Contains(ocSERIE.CLIENTE_TRASLADO))
                    {
                        try
                        {
                            oComprobante.TipoDeComprobante = c_TipoDeComprobante.T;
                            oComprobante.CondicionesDePago = null;
                            oComprobante.FormaPagoSpecified = false;
                            oComprobante.MetodoPago = null;
                            //oComprobante.MetodoPago = null;
                            //oComprobante.FormaPago = null;
                            oComprobante.SubTotal = 0;
                            oComprobante.Total = 0;
                            if (oComprobante.Conceptos != null)
                            {
                                foreach (CFDI40.ComprobanteConcepto concepto in oComprobante.Conceptos)
                                {
                                    concepto.ValorUnitario = 0;
                                    concepto.Importe = 0;
                                    concepto.ObjetoImp = "01"; 
                                    concepto.Impuestos = null;
                                }

                            }
                            oComprobante.Impuestos = null;
                        }
                        catch (Exception errTraslado)
                        {
                            ErrorFX.mostrar(errTraslado, true, true, "Generar40 - Lineas Traslado - 1648  ");
                            return false;
                        }
                    }
                }
            }
            catch (Exception errTraslado)
            {
                ErrorFX.mostrar(errTraslado, true, true, "Generar40 - Traslado - 1634  ");
                return false;
            }


            XslCompiledTransform myXslTrans = new XslCompiledTransform();

            XslCompiledTransform trans = new XslCompiledTransform();
            string nombre_tmp = oFACTURA.INVOICE_ID + "TMP" + DateTime.Now.ToString("ddMMyyyyhhmmss");
            if (File.Exists(nombre_tmp + ".XML"))
            {
                File.Delete(nombre_tmp + ".XML");
            }

            XPathDocument myXPathDoc;
            XmlSerializer serializer = new XmlSerializer(typeof(CFDI40.Comprobante));
            try
            {


                TextWriter writer = new StreamWriter(nombre_tmp + ".XML");
                serializer.Serialize(writer, oComprobante, myNamespaces);
                writer.Close();

                myXPathDoc = new XPathDocument(nombre_tmp + ".XML");
                //XslTransform myXslTrans = new XslTransform();
                myXslTrans = new XslCompiledTransform();


                trans = new XslCompiledTransform();
                XmlUrlResolver resolver = new XmlUrlResolver();

                resolver.Credentials = System.Net.CredentialCache.DefaultCredentials;
                XsltSettings settings = new XsltSettings(true, true);
            }
            catch (XmlException errores)
            {
                ErrorFX.mostrar(errores, true, true, "Generar40 - 949 ", false);
                return false;
            }
            string sDirectory = AppDomain.CurrentDomain.BaseDirectory;
            //Cargar xslt
            try
            {
                //Version 3
                myXslTrans.Load(sDirectory + "/XSLT/cadenaoriginal_4_0.xslt");
            }
            catch (XmlException errores)
            {
                ErrorFX.mostrar(errores, true, true, "Generar40 - 961 ", false);
                return false;
            }

            //Crear Archivo Temporal

            XmlTextWriter myWriter = new XmlTextWriter(nombre_tmp + ".TXT", null);

            //Transformar al XML
            myXslTrans.Transform(myXPathDoc, null, myWriter);

            myWriter.Close();

            //Abrir Archivo TXT
            string CADENA_ORIGINAL = "";
            StreamReader re = File.OpenText(nombre_tmp + ".TXT");
            string input = null;
            while ((input = re.ReadLine()) != null)
            {
                CADENA_ORIGINAL += input;
            }
            re.Close();

            //Eliminar Archivos Temporales
            File.Delete(nombre_tmp + ".TXT");
            File.Delete(nombre_tmp + ".XML");

            //Buscar el &amp; en la Cadena y sustituirlo por &
            CADENA_ORIGINAL = CADENA_ORIGINAL.Replace("&amp;", "&");



            //MessageBox.Show("Generar Sello.");
            oComprobante.Sello = SelloRSA(ocCERTIFICADO.LLAVE, ocCERTIFICADO.PASSWORD, CADENA_ORIGINAL, FECHA_FACTURA.Year); ;

            string DIRECTORIO_ARCHIVOS = "";
            //if (!Directory.Exists(ocSERIE.DIRECTORIO))
            //{
            //    ocSERIE.DIRECTORIO=Application.StartupPath;
            //}


            DIRECTORIO_ARCHIVOS += ocSERIE.DIRECTORIO + @"\" + ocSERIE.ID + @"\" + Fechammmyy(oFACTURA.INVOICE_DATE.ToString());
            string XML = DIRECTORIO_ARCHIVOS;

            //Rodrigo Escalona: Unificar directorio
            string prefijoPDF = "pdf";
            try
            {
                if (ocSERIE.UnirCarpeta)
                {
                    prefijoPDF = "";
                }
            }
            catch
            {

            }

            string PDF = XML + @"\" + prefijoPDF;
            if (!Directory.Exists(PDF))
            {
                try
                {

                    Directory.CreateDirectory(PDF);
                }
                catch (IOException excep)
                {
                    MessageBox.Show(excep.Message.ToString() + Environment.NewLine + "Creando directorio de PDF"
                        , Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return false;
                }
            }
            //Rodrigo Escalona: Unificar directorio
            string prefijoXML = "xml";
            try
            {
                if (ocSERIE.UnirCarpeta)
                {
                    prefijoXML = "";
                }
            }
            catch
            {

            }
            XML += @"\" + prefijoXML;
            if (Directory.Exists(XML) == false)
                Directory.CreateDirectory(XML);

            string XML_Archivo = "";

            string nombre_archivo = oFACTURA.INVOICE_ID;
            if (ocSERIE.ARCHIVO_NOMBRE != "")
            {
                nombre_archivo = ocSERIE.ARCHIVO_NOMBRE.Replace("{FACTURA}", oFACTURA.INVOICE_ID).Replace("{CLIENTE}", oFACTURA.CUSTOMER_ID)
                    ;
            }


            if (Directory.Exists(DIRECTORIO_ARCHIVOS))
            {
                XML_Archivo = XML + @"\" + nombre_archivo + ".xml";
            }
            else
            {
                XML_Archivo = @"\" + nombre_archivo + ".xml"; ;
            }

            XML_Archivo = XML + @"\" + nombre_archivo + ".xml";

            if(oEncapsular.ocEMPRESA.ENTITY_ID.ToUpper().Equals("TOPRE"))
            {
                string DirectorioEDI = DIRECTORIO_ARCHIVOS + @"\EDI\";
                if (!Directory.Exists(DirectorioEDI))
                {
                    //Crear directorio
                    Directory.CreateDirectory(DirectorioEDI);
                }
                EdiFX OEdiFX = new EdiFX();
                OEdiFX.CrearFactura810(oFACTURA, "", DirectorioEDI);
            }

            //MessageBox.Show("Cargado XLT");

            //MessageBox.Show("Cargado XLT " + ocCERTIFICADO.CERTIFICADO);
            if (!File.Exists(ocCERTIFICADO.CERTIFICADO))
            {
                MessageBox.Show("No se tiene acceso al archivo " + ocCERTIFICADO.CERTIFICADO + "", Application.ProductName + "-" + Application.ProductVersion, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);

                return false;
            }
            TextWriter writerXML = new StreamWriter(XML_Archivo);
            serializer.Serialize(writerXML, oComprobante, myNamespaces);
            writerXML.Close();
            writerXML.Dispose();
            Parsar_d1p1(XML_Archivo, leyendaFiscal);
            //Guardar Datos
            //MessageBox.Show("Guardar datos en bandeja.");
            string PDF_Archivo = "";
            PDF_Archivo = PDF + @"\" + nombre_archivo + ".pdf";
            cFACTURA ocFACTURA = new cFACTURA(oEncapsular.oData_ERP);
            ocFACTURA.datos(oFACTURA.INVOICE_ID, "", oFACTURA.ADDR_NO, oEncapsular.ocEMPRESA, "");
            cFACTURA_BANDEJA ocFACTURA_BANDEJA = new cFACTURA_BANDEJA(oEncapsular.oData_ERP, oEncapsular.ocEMPRESA);
            ocFACTURA_BANDEJA.INVOICE_ID = oFACTURA.INVOICE_ID;
            ocFACTURA_BANDEJA.XML = XML_Archivo;
            ocFACTURA_BANDEJA.ESTADO = "Creada Facturación Electrónica";
            ocFACTURA_BANDEJA.CADENA = CADENA_ORIGINAL;
            ocFACTURA_BANDEJA.SELLO = oComprobante.Sello;
            ocFACTURA_BANDEJA.SERIE = oComprobante.Serie;
            ocFACTURA_BANDEJA.USO_CFDI = oComprobante.Receptor.UsoCFDI.ToString();
            ocFACTURA_BANDEJA.PDF = PDF_Archivo;
            ocFACTURA_BANDEJA.VERSION = oComprobante.Version;

            ocFACTURA_BANDEJA.FORMA_DE_PAGO = string.Empty;
            //if (!string.IsNullOrEmpty(oComprobante.FormaPago))
            //{
            ocFACTURA_BANDEJA.FORMA_DE_PAGO = oComprobante.FormaPago.ToString().Replace("Item","");
            //}

            ocFACTURA_BANDEJA.METODO_DE_PAGO = string.Empty;
            //if (!string.IsNullOrEmpty(oComprobante.MetodoPago))
            //{
            if (oComprobante.MetodoPago != null)
            {
                ocFACTURA_BANDEJA.METODO_DE_PAGO = oComprobante.MetodoPago.ToString();
            }

            //}

            ocFACTURA_BANDEJA.CTA_BANCO = "";// oComprobante.NumCtaPago.ToString(); //ocFACTURA.CUENTA_BANCARIA;
            ocFACTURA_BANDEJA.ADDR_NO = oFACTURA.ADDR_NO;
            return ocFACTURA_BANDEJA.guardar();

        }

        private decimal ObtenerTipoCambio(cCONEXCION oData_ERP, DateTime fecha)
        {
            string sSQL = "";
            sSQL = @"SELECT TOP 1 *
                        FROM CURRENCY_EXCHANGE
                        WHERE (EFFECTIVE_DATE <= " + oData_ERP.convertir_fecha(fecha) + @")
                        AND CURRENCY_ID='USD' 
                        ORDER BY EFFECTIVE_DATE DESC";
            DataTable oDataTable = oData_ERP.EjecutarConsulta(sSQL);
            if (oDataTable != null)
            {
                foreach (DataRow oDataRow in oDataTable.Rows)
                {
                    //Añadir el Registro
                    return Math.Round(decimal.Parse(oDataRow["SELL_RATE"].ToString()),4);
                }
            }
            return 1;
        }

        private void AutorelacionarRMA(string invoiceId, Encapsular oEncapsular)
        {
            
        }

        private bool FacturaTimbradaReal(string invoiceId, Encapsular oEncapsular)
        {
            cFACTURA_BANDEJA ocFACTURA_BANDEJA = new cFACTURA_BANDEJA(oEncapsular.oData_ERP, oEncapsular.ocEMPRESA);
            return ocFACTURA_BANDEJA.ExisteTimbrada(invoiceId);
        }

        internal static string LimpiarNombre(string nombre)
        {
            if (nombre.Contains("BANREGIO"))
            {
                return nombre;
            }
            return (nombre.Replace(" SA DE CV", "").Replace(" S.A. DE C.V.", "").Replace(" SA. DE CV.", "")
                .Replace(" SA. DE CV.", "").Replace("S.C.", "").Replace("SRL", "").Replace(" S DE RL DE CV", "")
                .Replace(" S A DE C V", "")
                .Replace("S A DE C V", "")
                .Replace(" SA de CV", "")
                .Replace("S de RL de CV",string.Empty)
                .Replace("S de RL de CV", string.Empty)
                .Replace(" SA D", "")
                .Replace(",SA", "")
                .Replace(",S.A. DE C.V.", "")
                .Replace(" AC", "")
                .Replace(" S DE RL DE CV", "")
                .Replace(" S. DE RL. DE C.V.", "")
                .Replace(" S.A.B. DE C.V.", "")
                .Replace(", S.A.DE C.V.", "")

                ).Trim();
        }

        public CFDI40.c_Pais ObtenerResidenciaFiscal(string pais)
        {
            try
            {
                return (CFDI40.c_Pais)System.Enum.Parse(typeof(CFDI40.c_Pais)
                                    , pais.Trim().Normalize());
            }
            catch (Exception e)
            {
                ErrorFX.mostrar(e, true, true, "Generar40 - ObtenerResidenciaFiscal - No existe la pais "
                    + pais.Trim().Normalize(), false);
                throw e;
            }

        }

        public decimal OtroTruncar(decimal valor, int decimales = 4)
        {
            try
            {

                decimal result = 0;
                string[] xs = valor.ToString().Split('.');

                string decimalparte = xs[1] + "0000000000000000000000";
                result = decimal.Parse(xs[0] + "." + decimalparte.Substring(0, Math.Min(decimalparte.Length, decimales)));

                return result;
            }
            catch
            {
                return valor;
            }
        }

        public string ExtractNumbers(string expr)
        {
            return string.Join(null, System.Text.RegularExpressions.Regex.Split(expr, "[^\\d]"));
        }

        public static string Validacion(string cadena)
        {
            string Devolucion = cadena.Normalize().ToUpper();
            Devolucion = Devolucion.Replace("á", "A");
            Devolucion = Devolucion.Replace("é", "E");
            Devolucion = Devolucion.Replace("í", "I");
            Devolucion = Devolucion.Replace("ó", "O");
            Devolucion = Devolucion.Replace("ú", "U");
            Devolucion = Devolucion.Replace("Á", "A");
            Devolucion = Devolucion.Replace("É", "E");
            Devolucion = Devolucion.Replace("Í", "I");
            Devolucion = Devolucion.Replace("Ó", "O");
            Devolucion = Devolucion.Replace("Ú", "U");
            Devolucion = Devolucion.Replace(">", "");
            Devolucion = Devolucion.Replace("<", "");
            //Devolucion = Devolucion.Replace("&", "");
            Devolucion = Devolucion.Replace("'", "");
            Devolucion = Devolucion.Replace("#", "");
            Devolucion = Devolucion.Replace("|", "");
            //Devolucion = HtmlEncode(Devolucion);
            Devolucion = Regex.Replace(Devolucion, @"[\u0000-\u001F]", string.Empty);
            return Devolucion; //Regex.Replace(cadena, "[á,é,í,ó,ú,Á,É,Í,Ó,Ú,#,|,&,',>,<]", "");
        }

        public decimal OtroTruncar(decimal valor, cEMPRESA oEMPRESA)
        {
            try
            {

                decimal result = 0;
                string[] xs = valor.ToString().Split('.');

                string decimalparte = xs[1] + "0000000000000000000000";
                if (oEMPRESA.APROXIMACION == "Truncar")
                {
                    result = decimal.Parse(xs[0] + "." + decimalparte.Substring(0, Math.Min(decimalparte.Length, int.Parse(oEMPRESA.VALORES_DECIMALES))));
                }
                else
                {
                    result = Math.Round(valor, int.Parse(oEMPRESA.VALORES_DECIMALES), MidpointRounding.AwayFromZero);
                    xs = result.ToString().Split('.');
                    if (xs[1].Length != int.Parse(oEMPRESA.VALORES_DECIMALES))
                    {
                        decimalparte = xs[1] + "0000000000000000000000";
                        result = decimal.Parse(xs[0] + "." + decimalparte.Substring(0, int.Parse(oEMPRESA.VALORES_DECIMALES)));
                    }
                }
                return result;
            }
            catch
            {
                return valor;
            }
        }

        private string ExtraeExterior(string source)
        {
            string[] stringSeparators = new string[] { "EXT.", "INT." };
            string[] result;
            result = source.Split(stringSeparators, StringSplitOptions.None);
            if (result.Length == 1)
            {
                return "";
            }
            return result[1];
        }
        private string ExtraeInterior(string source)
        {
            string[] stringSeparators = new string[] { "INT." };
            string[] result;
            result = source.Split(stringSeparators, StringSplitOptions.None);
            if (result.Length == 1)
            {
                return "";
            }
            return result[1];
        }
        private string ExtraeDireccion(string source)
        {
            string[] stringSeparators = new string[] { "EXT.", "INT." };
            string[] result;
            result = source.Split(stringSeparators, StringSplitOptions.None);
            if (result.Length == 1)
            {
                return source;
            }

            return result[0];
        }
        private string ExtraeLocalidad(string source)
        {
            string[] stringSeparators = new string[] { "LOC." };
            string[] result;
            result = source.Split(stringSeparators, StringSplitOptions.None);
            if (result.Length == 1)
            {
                return "";
            }
            return result[1];
        }

        private ComprobanteCfdiRelacionados CargarComprobanteCfdiRelacionados(string INVOICE_ID
    , Encapsular oEncapsular, cFACTURA ocFACTURA)
        {
            ModeloDocumentosFXContainer dbContext = new ModeloDocumentosFXContainer();
            try
            {
                bool repetir = true;
                do
                {
                    var result = from b in dbContext.ndcFacturaSet
                    .Where(a => a.INVOICE_ID == INVOICE_ID)
                                 select b;
                    List<ndcFactura> dt = result.ToList();
                    if (dt.Count == 0)
                    {
                        if (Globales.automatico)
                        {
                            return null;
                        }
                        if (!Globales.preguntarRelacionados)
                        {
                            return null;
                        }
                        DialogResult Resultado = MessageBox.Show("¿Desea cargar los CFDI relacionados?", Application.ProductName + "-" + Application.ProductVersion, MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                        if (Resultado.ToString() != "Yes")
                        {
                            return null;
                        }

                        //if (!ocFACTURA.TYPE.Equals("I"))
                        //{
                        //    DialogResult Resultado = MessageBox.Show("¿Desea cargar los CFDI relacionados?", Application.ProductName + "-" + Application.ProductVersion, MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                        //    if (Resultado.ToString() != "Yes")
                        //    {
                        //        return null;
                        //    }
                        //}
                        //else
                        //{
                        //    return null;
                        //}
                        //Cargar la lista
                        if (oEncapsular.ocEMPRESA.relacionLinea)
                        {
                            ClasificadorProductos.formularios.frmTipoRelacionNDCnormalSimplificado ofrmTipoRelacionNDC =
                            new ClasificadorProductos.formularios.frmTipoRelacionNDCnormalSimplificado(oEncapsular.ocEMPRESA.ENTITY_ID
                                , ocFACTURA.INVOICE_ID, ocFACTURA.CUSTOMER_ID);
                            ofrmTipoRelacionNDC.ShowDialog();
                        }
                        else
                        {
                            ClasificadorProductos.formularios.frmTipoRelacionNDCfactura ofrmTipoRelacionNDC =
                                new ClasificadorProductos.formularios.frmTipoRelacionNDCfactura(oEncapsular.ocEMPRESA.ENTITY_ID
                                    , ocFACTURA.INVOICE_ID, ocFACTURA.CUSTOMER_ID);
                            ofrmTipoRelacionNDC.ShowDialog();
                        }

                    }
                    else
                    {
                        ComprobanteCfdiRelacionados devolver = new CFDI40.ComprobanteCfdiRelacionados();
                        devolver.CfdiRelacionado = new CFDI40.ComprobanteCfdiRelacionadosCfdiRelacionado[dt.Count];
                        int i = 0;
                        foreach (ndcFactura registro in dt)
                        {
                            //devolver.TipoRelacion = ObtenerTipoRelacion(registro.tipoRelacion);
                            switch (registro.tipoRelacion)
                            {
                                case "01":
                                    devolver.TipoRelacion = c_TipoRelacion.Item01;
                                    break;
                                case "02":
                                    devolver.TipoRelacion = c_TipoRelacion.Item02;
                                    break;
                                case "03":
                                    devolver.TipoRelacion = c_TipoRelacion.Item03;
                                    break;
                                case "04":
                                    devolver.TipoRelacion = c_TipoRelacion.Item04;
                                    break;
                                case "05":
                                    devolver.TipoRelacion = c_TipoRelacion.Item05;
                                    break;
                                case "06":
                                    devolver.TipoRelacion = c_TipoRelacion.Item06;
                                    break;
                                case "07":
                                    devolver.TipoRelacion = c_TipoRelacion.Item07;
                                    break;
                                case "08":
                                    devolver.TipoRelacion = c_TipoRelacion.Item08;
                                    break;
                                case "09":
                                    devolver.TipoRelacion = c_TipoRelacion.Item09;
                                    break;
                            }
                            devolver.CfdiRelacionado[i] = new CFDI40.ComprobanteCfdiRelacionadosCfdiRelacionado();
                            devolver.CfdiRelacionado[i].UUID = registro.UUID;

                            i++;
                        }
                        return devolver;
                    }
                } while (repetir);
            }
            catch (Exception e)
            {
                ErrorFX.mostrar(e, true, true, "Generar40 - 1094", false);
            }
            return null;
        }

        private c_TipoRelacion ObtenerTipoRelacion(string tipoRelacion)
        {
            try
            {
                return (c_TipoRelacion)System.Enum.Parse(typeof(c_TipoRelacion)
                                    , tipoRelacion.Trim().Normalize());
            }
            catch (Exception e)
            {
                ErrorFX.mostrar(e, true, true, "Generar40 - ObtenerTipoRelacion - No existe la TipoRelacion "
                    + tipoRelacion.Trim().Normalize(), false);
                throw e;
            }
        }
        //public CFDI40.c_ClaveProdServ ObtenerClaveProdServ(string claveProdServ)
        public string ObtenerClaveProdServ(string claveProdServ)
        {
            try
            {
                //return (CFDI40.c_ClaveProdServ)System.Enum.Parse(typeof(CFDI40.c_ClaveProdServ)
                //                    , "Item" + claveProdServ.Trim().Normalize());
                return claveProdServ.Trim().Normalize();
            }
            catch (Exception e)
            {
                ErrorFX.mostrar(e, true, true, "Generar40 - ObtenerClaveProdServ - No existe la claveProdServ "
                    + claveProdServ.Trim().Normalize(), false);
                throw e;
            }
        }

        //public CFDI40.c_ClaveUnidad ObtenerClaveUnidad(string claveUnidad)
        public string ObtenerClaveUnidad(string claveUnidad)
        {
            try
            {
                //return (CFDI40.c_ClaveUnidad)System.Enum.Parse(typeof(CFDI40.c_ClaveUnidad)
                //                    , claveUnidad.Trim().Normalize());
                return claveUnidad.Trim().Normalize();
            }
            catch (Exception e)
            {
                ErrorFX.mostrar(e, true, true, "Generar40 - ObtenerClaveUnidad - No existe la ClaveUnidad "
                    + claveUnidad.Trim().Normalize(), false);
                throw e;
            }
        }

        internal configuracionUM BuscarClaveUnidad(string STOCK_UM)
        {
            try
            {
                ModeloDocumentosFX.ModeloDocumentosFXContainer dbContext = new ModeloDocumentosFXContainer();
                configuracionUM registro = new configuracionUM();
                registro = (from r in dbContext.configuracionUMSet.Where
                                        (a => a.UnidadMedida == STOCK_UM)
                            select r).FirstOrDefault();
                if (registro != null)
                {
                    if (registro.Id != 0)
                    {
                        return registro;
                    }
                }
                //Buscar 
                registro = new configuracionUM();
                registro = (from r in dbContext.configuracionUMSet.Where
                                        (a => a.SAT == STOCK_UM)
                            select r).FirstOrDefault();
                if (registro != null)
                {
                    if (registro.Id != 0)
                    {
                        return registro;
                    }
                }
                return null;
            }
            catch (Exception e)
            {
                ErrorFX.mostrar(e, true, true, "Generar40 - 1080 - Buscando Conversión " + STOCK_UM, true);
            }
            return null;
        }

        private bool EsKit(string pART_ID)
        {
            ModeloDocumentosFX.ModeloDocumentosFXContainer db = new ModeloDocumentosFX.ModeloDocumentosFXContainer();
            configuracionPartes oConfiguracionPartes = db.configuracionPartesSet.Where(a => a.producto.Equals(pART_ID)).FirstOrDefault();
            if (oConfiguracionPartes == null)
            {
                return false;
            }
            return true;
        }

        private List<CFDI40.ComprobanteConceptoParte> ObtenerKit(string pART_ID, decimal qTY)
        {
            List<CFDI40.ComprobanteConceptoParte> lista = new List<CFDI40.ComprobanteConceptoParte>();
            ModeloDocumentosFX.ModeloDocumentosFXContainer db = new ModeloDocumentosFX.ModeloDocumentosFXContainer();
            var oConfiguracionPartes = db.configuracionPartesSet.Where(a => a.producto.Equals(pART_ID));
            if (oConfiguracionPartes != null)
            {
                foreach (configuracionPartes registro in oConfiguracionPartes)
                {
                    CFDI40.ComprobanteConceptoParte oParte = new CFDI40.ComprobanteConceptoParte();
                    oParte.Cantidad = OtroTruncar(registro.Cantidad * qTY);
                    oParte.ClaveProdServ = ObtenerClaveProdServ(registro.ClaveProdServ);
                    oParte.Descripcion = registro.Descripcion;
                    oParte.Importe = OtroTruncar(oParte.Cantidad * registro.ValorUnitario);
                    oParte.ImporteSpecified = true;
                    oParte.ValorUnitarioSpecified = true;
                    oParte.ValorUnitario = OtroTruncar(registro.ValorUnitario);
                    oParte.NoIdentificacion = registro.NoIdentificacion;
                    oParte.Unidad = registro.Unidad;
                    lista.Add(oParte);
                }
            }
            return lista;
        }

        public void Parsar_d1p1(string ARCHIVO, bool leyendaFiscal)
        {
            ReplaceInFile(ARCHIVO, "d1p1", "xsi");
            ReplaceInFile(ARCHIVO, "xsi:cfdi=\"http://www.sat.gob.mx/cfd/4\"", "");
            ReplaceInFile(ARCHIVO, " cartaporte20=\"http://www.sat.gob.mx/CartaPorte20\"", "");
            //cartaporte20="http://www.sat.gob.mx/CartaPorte20"
            if (leyendaFiscal)
            {
                ReplaceInFile(ARCHIVO, "xsi:schemaLocation=\"http://www.sat.gob.mx/cfd/4 http://www.sat.gob.mx/sitio_internet/cfd/4/cfdv40.xsd\"", "xsi:schemaLocation=\"http://www.sat.gob.mx/cfd/4 http://www.sat.gob.mx/sitio_internet/cfd/4/cfdv40.xsd http://www.sat.gob.mx/leyendasFiscales http://www.sat.gob.mx/sitio_internet/cfd/leyendasFiscales/leyendasFisc.xsd\"");
            }
        }
        static public void ReplaceInFile(string filePath, string searchText, string replaceText)
        {
            StreamReader reader = new StreamReader(filePath);
            string content = reader.ReadToEnd();
            reader.Close();
            reader.Dispose();

            content = Regex.Replace(content, searchText, replaceText);

            StreamWriter writer = new StreamWriter(filePath);
            writer.Write(content);
            writer.Close();
            writer.Dispose();
        }

        private configuracionProducto BuscarConfiguracionProducto(string invoice_id, string partId)
        {
            ClasificadorProductos.formularios.frmProductoClasificacion oProducto = new ClasificadorProductos.formularios.frmProductoClasificacion(partId);
            oProducto.ShowDialog();
            if (oProducto.oRegistro == null)
            {
                ErrorFX.mostrar(
                "Generar40 - 602 - "
                + Environment.NewLine
                + " Factura " + invoice_id + " Acción cancelada por el usuario el producto " + partId
                + " no tiene su clave del SAT", true, true, true);
                return null;
            }
            return oProducto.oRegistro;
        }

        private XmlElement ComplementoLeyenda(cFACTURA oFACTURA, Encapsular oEncapsular, cCONEXCION oData)
        {
            try
            {
                bool insertar = false;

                cCLIENTE_CONFIGURACION ocCLIENTE_CONFIGURACION = new cCLIENTE_CONFIGURACION(oData);
                ocCLIENTE_CONFIGURACION.cargar_CUSTOMER_ID(oFACTURA.CUSTOMER_ID, oFACTURA.COUNTRY, oFACTURA.ADDR_NO);

                
                    BitacoraFX.Log("Generar-ComplementoLeyenda-2795 " + ocCLIENTE_CONFIGURACION.LEYENDA_FISCAL);
                if (ocCLIENTE_CONFIGURACION.LEYENDA_FISCAL != "")
                {
                    oFACTURA.ESPECIFICACIONES = ocCLIENTE_CONFIGURACION.LEYENDA_FISCAL;
                    //Insertar la especificacion en la factura
                    string sSQL_insert = @"INSERT INTO [dbo].[RECEIVABLE_BINARY]  ([INVOICE_ID],[TYPE],[BITS],[BITS_LENGTH]) 
                                VALUES ('" + oFACTURA.INVOICE_ID + "','D',CAST('" + ocCLIENTE_CONFIGURACION.LEYENDA_FISCAL + @"' AS VARBINARY(MAX)),
                                    " + (ocCLIENTE_CONFIGURACION.LEYENDA_FISCAL.Length + 10).ToString() + ")";

                    string sSQL_sql = " SELECT TOP 1 * FROM  [dbo].[RECEIVABLE_BINARY] WHERE INVOICE_ID='" + oFACTURA.INVOICE_ID + "' AND TYPE='D' ";
                    insertar = true;
                    DataTable oDataTable = oEncapsular.oData_ERP.EjecutarConsulta(sSQL_sql);
                    if (oDataTable != null)
                    {
                        foreach (DataRow oDataRow in oDataTable.Rows)
                        {
                            //Eliminar la especifcación actual
                            sSQL_sql = "DELETE FROM  [dbo].[RECEIVABLE_BINARY] WHERE INVOICE_ID='" + oFACTURA.INVOICE_ID + "' AND TYPE='D' ";
                            oEncapsular.oData_ERP.EjecutarConsulta(sSQL_sql);
                            insertar = true;
                        }
                    }
                    if (insertar)
                    {
                        oEncapsular.oData_ERP.EjecutarConsulta(sSQL_insert);
                    }
                }
                else
                {
                    BitacoraFX.Log("Generar-ComplementoLeyenda-2795 + ocCLIENTE_CONFIGURACION.LEYENDA_FISCAL Vacia ");
                }

                if (oFACTURA.ESPECIFICACIONES != "")
                {
                    CFDI40.ComprobanteComplemento o = new CFDI40.ComprobanteComplemento();
                    CFDI40.ComprobanteImpuestosRetencion[] oComprobanteImpuestosRetencion = new CFDI40.ComprobanteImpuestosRetencion[1];
                    LeyendasFiscales oLeyendasFiscales = new LeyendasFiscales();
                    oLeyendasFiscales.Leyenda = new LeyendasFiscalesLeyenda[1];
                    oLeyendasFiscales.Leyenda[0] = new LeyendasFiscalesLeyenda();
                    oLeyendasFiscales.Leyenda[0].textoLeyenda = oFACTURA.ESPECIFICACIONES;

                    XmlDocument doc = new XmlDocument();
                    XmlSerializerNamespaces myNamespaces_leyenda = new XmlSerializerNamespaces();
                    myNamespaces_leyenda.Add("leyendasFisc", "http://www.sat.gob.mx/leyendasFiscales");

                    using (XmlWriter writer = doc.CreateNavigator().AppendChild())
                    {
                        new XmlSerializer(oLeyendasFiscales.GetType()).Serialize(writer, oLeyendasFiscales, myNamespaces_leyenda);
                    }

                    return doc.DocumentElement;
                }
            }
            catch (Exception exLeyendaFiscal)
            {
                ErrorFX.mostrar(exLeyendaFiscal, true, true, "Generar40 - Leyenda Fiscal - 1509", true);
                return null;
            }
            return null;
        }

        private XmlElement ComplementoCCE(CFDI40.Comprobante oComprobante, bool cce,
        cFACTURA oFACTURA, Encapsular oEncapsular, cCCE ocCCE, CFDI32.t_Ubicacion ot_UbicacionReceptor
        , cFACTURA_BANDEJA ocFACTURA_BANDEJA)
        {
            //Complemento de Comercio Exterior
            if (oComprobante.TipoDeComprobante == c_TipoDeComprobante.I) //c_TipoDeComprobante.I)
            {
                try
                {
                    if (cce)
                    {
                        ComercioExterior oComercioExterior = new ComercioExterior();
                        //oComercioExterior.ClaveDePedimentoSpecified = true;
                        //oComercioExterior.ClaveDePedimento = c_ClavePedimento.A1;
                        try
                        {
                            oComercioExterior.ClaveDePedimento = "A1";
                            oComercioExterior.CertificadoOrigen = 0;
                            //oComercioExterior.TipoOperacion = "2";// c_TipoOperacion.Item2;
                            //oComercioExterior.Subdivision = 0;
                            if (!String.IsNullOrEmpty(ocFACTURA_BANDEJA.subdivision))
                            {
                                int subdivisiontr = int.Parse(ocFACTURA_BANDEJA.subdivision);
                                //oComercioExterior.Subdivision = subdivisiontr;
                            }

                            //oComercioExterior.SubdivisionSpecified = true;
                            //oComercioExterior.Incoterm = "EXW";

                            //oComercioExterior.IncotermSpecified = true;
                            //oComercioExterior.Incoterm = c_INCOTERM.EXW;
                            if (oFACTURA.FREE_ON_BOARD.ToString().Contains("EXW"))
                            {
                                oComercioExterior.Incoterm = "EXW";//c_INCOTERM.EXW;
                            }
                            if (oFACTURA.FREE_ON_BOARD.ToString().Contains("FOB"))
                            {
                                oComercioExterior.Incoterm = "FOB"; //c_INCOTERM.FOB;
                            }
                            if (oFACTURA.FREE_ON_BOARD.ToString().Contains("FCA"))
                            {
                                oComercioExterior.Incoterm = "FCA"; //c_INCOTERM.FCA;
                            }
                            if (oFACTURA.FREE_ON_BOARD.ToString().Contains("FAS"))
                            {
                                oComercioExterior.Incoterm = "FAS"; // c_INCOTERM.FAS;
                            }
                            if (oFACTURA.FREE_ON_BOARD.ToString().Contains("DES"))
                            {
                                oComercioExterior.Incoterm = "DES";// c_INCOTERM.DES;
                            }
                            if (oFACTURA.FREE_ON_BOARD.ToString().Contains("DEQ"))
                            {
                                oComercioExterior.Incoterm = "DEQ";// c_INCOTERM.DEQ;
                            }
                            if (oFACTURA.FREE_ON_BOARD.ToString().Contains("DDU"))
                            {
                                oComercioExterior.Incoterm = "DDU";//  c_INCOTERM.DDU;
                            }
                            if (oFACTURA.FREE_ON_BOARD.ToString().Contains("DDP"))
                            {
                                oComercioExterior.Incoterm = "DDP";//  c_INCOTERM.DDP;
                            }
                            if (oFACTURA.FREE_ON_BOARD.ToString().Contains("DAT"))
                            {
                                oComercioExterior.Incoterm = "DAT";//  c_INCOTERM.DAT;
                            }
                            if (oFACTURA.FREE_ON_BOARD.ToString().Contains("DAP"))
                            {
                                oComercioExterior.Incoterm = "DAP";//  c_INCOTERM.DAP;
                            }
                            if (oFACTURA.FREE_ON_BOARD.ToString().Contains("DAF"))
                            {
                                oComercioExterior.Incoterm = "DAF";//  c_INCOTERM.DAF;
                            }
                            if (oFACTURA.FREE_ON_BOARD.ToString().Contains("CPT"))
                            {
                                oComercioExterior.Incoterm = "CPT";// c_INCOTERM.CPT;
                            }
                            if (oFACTURA.FREE_ON_BOARD.ToString().Contains("CIP"))
                            {
                                oComercioExterior.Incoterm = "CIP";//c_INCOTERM.CIP;
                            }
                            if (oFACTURA.FREE_ON_BOARD.ToString().Contains("CIF"))
                            {
                                oComercioExterior.Incoterm = "CIF";//c_INCOTERM.CIF;
                            }
                            if (oFACTURA.FREE_ON_BOARD.ToString().Contains("CFR"))
                            {
                                oComercioExterior.Incoterm = "CFR";//c_INCOTERM.CFR;
                            }

                            bool errorIncoterm = false;
                            if (oComercioExterior.Incoterm == null)
                            {
                                errorIncoterm = true;
                            }
                            else
                            {
                                if (String.IsNullOrEmpty(oComercioExterior.Incoterm))
                                {
                                    errorIncoterm = true;
                                }
                            }
                            if (errorIncoterm)
                            {
                                ErrorFX.mostrar("Generar40 - 2270 - CEE Incoterm no existe.", true, true, true);
                                return null;

                            }



                            oComercioExterior.CertificadoOrigen = 0;
                            //oComercioExterior.CertificadoOrigenSpecified = true;
                            //oComercioExterior.TipoCambioUSDSpecified = true;
                            oComercioExterior.TipoCambioUSD = oFACTURA.SELL_RATE;
                            oComercioExterior.TotalUSD = Math.Round(oComprobante.Total, 2).ToString();
                            //oComercioExterior.TotalUSDSpecified = true;

                            oComercioExterior.Emisor = new ComercioExteriorEmisor();
                            oComercioExterior.Emisor.Domicilio = new ComercioExteriorEmisorDomicilio();
                            //Domicilio Fiscal
                            if (oEncapsular.ocEMPRESA.CALLE != "")
                            {
                                oComercioExterior.Emisor.Domicilio.Calle = Validacion(oEncapsular.ocEMPRESA.CALLE);
                            }
                            if (oEncapsular.ocEMPRESA.EXTERIOR != "")
                            {
                                oComercioExterior.Emisor.Domicilio.NumeroExterior = Validacion(oEncapsular.ocEMPRESA.EXTERIOR);
                            }
                            if (oEncapsular.ocEMPRESA.INTERIOR != "")
                            {
                                oComercioExterior.Emisor.Domicilio.NumeroInterior = Validacion(oEncapsular.ocEMPRESA.INTERIOR);
                            }
                            if (oEncapsular.ocEMPRESA.COLONIA != "")
                            {
                                //c_Colonia oc_Colonia = (c_Colonia)System.Enum.Parse(typeof(c_Colonia), "Item" + oEncapsular.ocEMPRESA.COLONIA);

                                //oComercioExterior.Emisor.Domicilio.Colonia = oc_Colonia;
                                //oComercioExterior.Emisor.Domicilio.ColoniaSpecified = true;
                                oComercioExterior.Emisor.Domicilio.Colonia = oEncapsular.ocEMPRESA.COLONIA;
                            }
                            if (oEncapsular.ocEMPRESA.LOCALIDAD != "")
                            {
                                //c_Localidad oc_Localidad = (c_Localidad)System.Enum.Parse(typeof(c_Localidad), "Item" + oEncapsular.ocEMPRESA.LOCALIDAD);
                                //oComercioExterior.Emisor.Domicilio.Localidad = oc_Localidad;
                                //oComercioExterior.Emisor.Domicilio.LocalidadSpecified = true;
                                oComercioExterior.Emisor.Domicilio.Localidad = oEncapsular.ocEMPRESA.LOCALIDAD;
                            }
                            if (oEncapsular.ocEMPRESA.REFERENCIA != "")
                            {
                                oComercioExterior.Emisor.Domicilio.Referencia = Validacion(oEncapsular.ocEMPRESA.REFERENCIA);
                            }
                            if (oEncapsular.ocEMPRESA.MUNICIPIO != "")
                            {

                                //c_Municipio oc_Municipio = (c_Municipio)System.Enum.Parse(typeof(c_Municipio), "Item" + oEncapsular.ocEMPRESA.MUNICIPIO);
                                //oComercioExterior.Emisor.Domicilio.Municipio = oc_Municipio;
                                //oComercioExterior.Emisor.Domicilio.MunicipioSpecified = true;
                                oComercioExterior.Emisor.Domicilio.Municipio = oEncapsular.ocEMPRESA.MUNICIPIO;
                            }
                            if (oEncapsular.ocEMPRESA.ESTADO != "")
                            {

                                //c_Estado oc_Estado = (c_Estado)System.Enum.Parse(typeof(c_Estado), oEncapsular.ocEMPRESA.ESTADO);
                                //oComercioExterior.Emisor.Domicilio.Estado = oc_Estado;
                                oComercioExterior.Emisor.Domicilio.Estado = oEncapsular.ocEMPRESA.ESTADO;

                            }
                            if (oEncapsular.ocEMPRESA.PAIS != "")
                            {
                                //ot_UbicacionFiscal.pais = validacion(oEncapsular.ocEMPRESA.PAIS);
                                //oComercioExterior.Emisor.Domicilio.Pais = c_Pais.MEX;
                                oComercioExterior.Emisor.Domicilio.Pais = "MEX";
                            }
                            if (oEncapsular.ocEMPRESA.CP != "")
                            {
                                //ot_UbicacionFiscal.codigoPostal = validacion(oEncapsular.ocEMPRESA.CP);
                                //c_CodigoPostal oc_CodigoPostal = (c_CodigoPostal)System.Enum.Parse(typeof(c_CodigoPostal), "Item" + oEncapsular.ocEMPRESA.CP);
                                //oComercioExterior.Emisor.Domicilio.CodigoPostal = oc_CodigoPostal;
                                oComercioExterior.Emisor.Domicilio.CodigoPostal = oEncapsular.ocEMPRESA.CP;
                                //if (oEncapsular.ocEMPRESA.ENTITY_ID.Equals("FIMA"))
                                //{
                                //    //Rodrigo Escalona 07/01/2020
                                //    oComercioExterior.Emisor.Domicilio.CodigoPostal = "67144";
                                //}
                            }
                            oComercioExterior.TipoCambioUSD = decimal.Round(oComercioExterior.TipoCambioUSD, 4);



                            //Receptor
                            oComercioExterior.Receptor = new ComercioExteriorReceptor();
                            oComercioExterior.Receptor.NumRegIdTrib = oFACTURA.TAX_ID_NUMBER;
                            oComercioExterior.Receptor.Domicilio = new ComercioExteriorReceptorDomicilio();
                            if (oFACTURA.BILL_TO_ADDR_1 != null)
                            {
                                oComercioExterior.Receptor.Domicilio.Calle = oFACTURA.BILL_TO_ADDR_1 + oFACTURA.BILL_TO_ADDR_2;
                            }
                            if (ot_UbicacionReceptor.noExterior != null)
                            {
                                oComercioExterior.Receptor.Domicilio.NumeroExterior = ot_UbicacionReceptor.noExterior;

                            }
                            if (ot_UbicacionReceptor.noInterior != null)
                            {
                                oComercioExterior.Receptor.Domicilio.NumeroInterior = ot_UbicacionReceptor.noInterior;
                            }
                            oComercioExterior.Receptor.Domicilio.Pais = oFACTURA.BILL_TO_COUNTRY.ToUpper();//c_Pais.USA;

                            if (oFACTURA.BILL_TO_COUNTRY.ToUpper().Contains("IND"))
                            {
                                oComercioExterior.Receptor.Domicilio.Pais = "IND";//c_Pais.IND;
                            }

                            if (oFACTURA.BILL_TO_COUNTRY.ToUpper().Contains("IRL"))
                            {
                                oComercioExterior.Receptor.Domicilio.Pais = "IRL";//c_Pais.IND;
                            }

                            if (oFACTURA.BILL_TO_COUNTRY.ToUpper().Contains("JPN"))
                            {
                                oComercioExterior.Receptor.Domicilio.Pais = "JPN";//c_Pais.IND;
                            }

                            if (oFACTURA.BILL_TO_COUNTRY.ToUpper().Contains("CHINA"))
                            {
                                oComercioExterior.Receptor.Domicilio.Pais = "CHN";//c_Pais.IND;
                            }
                            if (oFACTURA.BILL_TO_COUNTRY.ToUpper().Contains("CHN"))
                            {
                                oComercioExterior.Receptor.Domicilio.Pais = "CHN";//c_Pais.IND;
                            }


                            if (oFACTURA.BILL_TO_COUNTRY.ToUpper().Contains("THA"))
                            {
                                oComercioExterior.Receptor.Domicilio.Pais = "THA";//c_Pais.IND;
                            }

                            if (oFACTURA.BILL_TO_COUNTRY.ToUpper().Contains("JAPON"))
                            {
                                oComercioExterior.Receptor.Domicilio.Pais = "JPN";//c_Pais.IND;
                            }
                            if (oFACTURA.BILL_TO_COUNTRY.ToUpper().Contains("CHINA"))
                            {
                                oComercioExterior.Receptor.Domicilio.Pais = "CHN";//c_Pais.IND;
                            }
                            if (oFACTURA.BILL_TO_COUNTRY.ToUpper().Contains("CHN"))
                            {
                                oComercioExterior.Receptor.Domicilio.Pais = "CHN";//c_Pais.IND;
                            }


                            if (oFACTURA.BILL_TO_COUNTRY.ToUpper().Contains("DOM"))
                            {
                                oComercioExterior.Receptor.Domicilio.Pais = "DOM";//c_Pais.DOM;
                            }
                            if (oFACTURA.BILL_TO_COUNTRY.ToUpper().Contains("USA"))
                            {
                                oComercioExterior.Receptor.Domicilio.Pais = "USA";// c_Pais.USA;
                            }
                            if (oFACTURA.BILL_TO_COUNTRY.ToUpper().Contains("CAN"))
                            {
                                oComercioExterior.Receptor.Domicilio.Pais = "CAN";// c_Pais.CAN;
                            }

                            if (oFACTURA.BILL_TO_COUNTRY.ToUpper().Contains("GBR"))
                            {
                                oComercioExterior.Receptor.Domicilio.Pais = "GBR";//c_Pais.GBR;
                            }

                            if (oFACTURA.BILL_TO_COUNTRY.ToUpper().Contains("BOL"))
                            {
                                oComercioExterior.Receptor.Domicilio.Pais = "BOL";//c_Pais.GBR;
                            }

                            if (oFACTURA.BILL_TO_COUNTRY.ToUpper().Contains("POL"))
                            {
                                oComercioExterior.Receptor.Domicilio.Pais = "POL";//c_Pais.DOM;
                            }

                            if (oFACTURA.BILL_TO_COUNTRY.ToUpper().Contains("GTM"))
                            {
                                oComercioExterior.Receptor.Domicilio.Pais = "GTM";//c_Pais.DOM;
                            }

                            if (oFACTURA.BILL_TO_COUNTRY.ToUpper().Contains("GUATEMALA"))
                            {
                                oComercioExterior.Receptor.Domicilio.Pais = "GTM";//c_Pais.DOM;
                            }

                            if (oFACTURA.BILL_TO_COUNTRY.ToUpper().Contains("SUIZA"))
                            {
                                oComercioExterior.Receptor.Domicilio.Pais = "CHE";//c_Pais.DOM;
                            }

                            if (oFACTURA.BILL_TO_COUNTRY.ToUpper().Contains("CHE"))
                            {
                                oComercioExterior.Receptor.Domicilio.Pais = "CHE";//c_Pais.DOM;
                            }


                            if (oFACTURA.BILL_TO_COUNTRY.ToUpper().Contains("FRA"))
                            {
                                oComercioExterior.Receptor.Domicilio.Pais = "FRA";//c_Pais.DOM;
                            }

                            if (oFACTURA.BILL_TO_COUNTRY.ToUpper().Contains("FRANCIA"))
                            {
                                oComercioExterior.Receptor.Domicilio.Pais = "FRA";//c_Pais.DOM;
                            }


                            if (oFACTURA.BILL_TO_COUNTRY.ToUpper().Contains("COL"))
                            {
                                oComercioExterior.Receptor.Domicilio.Pais = "COL";//c_Pais.DOM;
                            }

                            if (oFACTURA.BILL_TO_COUNTRY.ToUpper().Contains("IDN"))
                            {
                                oComercioExterior.Receptor.Domicilio.Pais = "IDN";//c_Pais.DOM;
                            }

                            if (oFACTURA.BILL_TO_COUNTRY.ToUpper().Contains("MEXICO"))
                            {
                                oComercioExterior.Receptor.Domicilio.Pais = "MEX";//c_Pais.DOM;
                            }

                            if (!String.IsNullOrEmpty(oFACTURA.BILL_TO_CITY))
                            {
                                oComercioExterior.Receptor.Domicilio.Localidad = oFACTURA.BILL_TO_CITY;
                            }

                            oComercioExterior.Receptor.Domicilio.CodigoPostal = ot_UbicacionReceptor.codigoPostal;
                            oComercioExterior.Receptor.Domicilio.Estado = ot_UbicacionReceptor.estado;
                            
                            
                            //Fin de receptor

                            //Destinatario
                            //Version 1.1
                            oComercioExterior.Destinatario = new ComercioExteriorDestinatario[1];
                            oComercioExterior.Destinatario[0] = new ComercioExteriorDestinatario();
                            oComercioExterior.Destinatario[0].Nombre = oFACTURA.NAME; //oComprobante.Receptor.Nombre;

                            if (!String.IsNullOrEmpty(oFACTURA.VAT_REGISTRATION_DESTINO))
                            {
                                oComercioExterior.Destinatario[0].NumRegIdTrib = oFACTURA.VAT_REGISTRATION_DESTINO;
                            }
                            else
                            {
                                oComercioExterior.Destinatario[0].NumRegIdTrib = oFACTURA.TAX_ID_NUMBER;
                            }

                            oComercioExterior.Destinatario[0].Domicilio = new ComercioExteriorDestinatarioDomicilio[1];
                            oComercioExterior.Destinatario[0].Domicilio[0] = new ComercioExteriorDestinatarioDomicilio();
                            if (oFACTURA.ADDR_1 != "")
                            {
                                oComercioExterior.Destinatario[0].Domicilio[0].Calle = oFACTURA.ADDR_1 + oFACTURA.ADDR_2;
                            }
                            if (oFACTURA.COUNTRY != "")
                            {
                                oComercioExterior.Destinatario[0].Domicilio[0].Pais = oFACTURA.COUNTRY;
                                if (oFACTURA.COUNTRY.ToUpper().Contains("MEXICO"))
                                {
                                    oComercioExterior.Destinatario[0].Domicilio[0].Pais = "MEX";//c_Pais.DOM;
                                }
                            }
                            if (!String.IsNullOrEmpty(oFACTURA.CITY))
                            {
                                oComercioExterior.Destinatario[0].Domicilio[0].Localidad = oFACTURA.CITY;
                            }
                            if (oFACTURA.STATE != "")
                            {
                                oComercioExterior.Destinatario[0].Domicilio[0].Estado = oFACTURA.STATE;
                            }
                            if (oFACTURA.ZIPCODE != "")
                            {
                                oComercioExterior.Destinatario[0].Domicilio[0].CodigoPostal = oFACTURA.ZIPCODE;
                            }

                            //Validar el TC si es MXN
                            //Aplicable a bradford
                            if (oEncapsular.ocEMPRESA.ENTITY_ID.Contains("BDM"))
                            {
                                if (oComprobante.Moneda.Equals("MXN"))
                                {
                                    //if (oComprobante.TipoCambio == 1)
                                    //{
                                    oComercioExterior.TipoCambioUSD
                                        =
                                        OtroTruncar(oFACTURA.tipoCambio("USD", oFACTURA.INVOICE_DATE, oEncapsular.oData_ERP)
                                        , oEncapsular.ocEMPRESA);
                                    ErrorFX.mostrar("tipo de cambio - " + oComercioExterior.TipoCambioUSD.ToString(), false, false, false);
                                    //oComercioExterior.TipoCambioUSD = 1;
                                    //}
                                }
                            }
                            //Rodrigo Escalona: 04/11/2019
                            if (String.IsNullOrEmpty(oComercioExterior.Receptor.Domicilio.Calle))
                            {
                                oComercioExterior.Receptor = null;
                            }

                            ////Rodrigo Escalona: 04/010/2019
                            ////Caso Bradford 
                            //if (oEncapsular.ocEMPRESA.ENTITY_ID.Contains("BDM"))
                            //{
                            //    //if (oComercioExterior.Receptor.Domicilio.Pais.Contains("MEX"))
                            //    //{

                            //        oComercioExterior.Receptor.Domicilio.Calle = oComercioExterior.Destinatario[0].Domicilio[0].Calle;
                            //        oComercioExterior.Receptor.Domicilio.Pais = oComercioExterior.Destinatario[0].Domicilio[0].Pais;
                            //        oComercioExterior.Receptor.Domicilio.Estado = oComercioExterior.Destinatario[0].Domicilio[0].Estado;
                            //        oComercioExterior.Receptor.Domicilio.Localidad = oComercioExterior.Destinatario[0].Domicilio[0].Localidad;
                            //        oComercioExterior.Receptor.Domicilio.CodigoPostal = oComercioExterior.Destinatario[0].Domicilio[0].CodigoPostal;
                            //    //}
                            //}

                            //Sumar las lineas
                            //var results = from p in oComprobante.Conceptos
                            //              group p.NoIdentificacion by p.NoIdentificacion into g
                            //              select new { NoIdentificacion = g.Key, Cars = g.ToList() };
                        }
                        catch (Exception exCCE)
                        {
                            ErrorFX.mostrar(exCCE, true, true, "Generar40 - 1700 - Datos de la cabecera de CEE", true);
                            return null;
                        }
                        if (oComprobante.Conceptos == null)
                        {
                            ErrorFX.mostrar("Generar40 - 1706 - CFI sin conceptos", true, true, true);
                            return null;
                        }

                        //ROdrigo esalona
                        oComercioExterior.Mercancias = new ComercioExteriorMercancia[oComprobante.Conceptos.Length + 1];


                        int i = 0;
                        try
                        {


                            decimal TotalUSD = 0;
                            foreach (CFDI40.ComprobanteConcepto concepto in oComprobante.Conceptos)
                            {
                                if (concepto != null)
                                {


                                    if (!String.IsNullOrEmpty(concepto.Importe.ToString()))
                                    {
                                        //MessageBox.Show("Importe:" + concepto.Importe.ToString() + " ");

                                        decimal importetr = concepto.Importe;
                                        try
                                        {
                                            if (oEncapsular.ocEMPRESA.ENTITY_ID.Contains("BDM"))
                                            {
                                                if (oComprobante.Moneda == "MXN")//CFDI40.c_Moneda.MXN)
                                                {
                                                    decimal TipoCambiotr = oComercioExterior.TipoCambioUSD;//oComprobante.TipoCambio;
                                                    importetr = importetr / TipoCambiotr;

                                                }
                                            }
                                        }
                                        catch (Exception exCCE)
                                        {
                                            ErrorFX.mostrar(exCCE, true, true, "Generar40 - 1975 - Datos de las líneas del CEE linea: " + i.ToString(), true);
                                            return null;
                                        }

                                        if (!String.IsNullOrEmpty(concepto.NoIdentificacion))
                                        {
                                            string fraccion = "";
                                            try
                                            {


                                                //oComprobante.Conceptos[i].Importe = otroTruncar(oComprobante.Conceptos[i].Importe, oEncapsular.ocEMPRESA);
                                                if (oEncapsular.ocEMPRESA.ENTITY_ID.Contains("BDM"))
                                                {
                                                    //oComprobante.Conceptos[i].Importe = importetr;
                                                }
                                                oComercioExterior.Mercancias[i] = new ComercioExteriorMercancia();
                                                oComercioExterior.Mercancias[i].CantidadAduanaSpecified = true;

                                                decimal CantidadAduana = decimal.Parse(concepto.Cantidad.ToString("#.##"));
                                                oComercioExterior.Mercancias[i].CantidadAduana = CantidadAduana;
                                                //oComercioExterior.Mercancias[i].UnidadAduanaSpecified = true;
                                                //MessageBox.Show("1997");
                                                oComercioExterior.Mercancias[i].NoIdentificacion = concepto.NoIdentificacion;

                                                if (String.IsNullOrEmpty(concepto.Descripcion))
                                                {
                                                    string mensaje = "Generar40 - 1736 " + Environment.NewLine + "Factura: " + oFACTURA.INVOICE_ID + "  Error en CCE. " + concepto.NoIdentificacion + ". No existe";
                                                    ErrorFX.mostrar(mensaje, true, true, true);
                                                    return null;
                                                }

                                                ocCCE.cargar(oEncapsular.ocEMPRESA.ROW_ID);
                                                //MessageBox.Show("2008");

                                                fraccion = ocCCE.obtenerFraccion(concepto.NoIdentificacion
                                                    , oFACTURA.INVOICE_ID, "1", oEncapsular.oData_ERP, concepto.Descripcion);
                                                if (String.IsNullOrEmpty(fraccion))
                                                {
                                                    string mensaje = "Generar40 - 1467 " + Environment.NewLine + "Factura: " + oFACTURA.INVOICE_ID + "  Error en CCE. Fracción Arancelaría del Producto " + concepto.NoIdentificacion + ". No existe";
                                                    try
                                                    {
                                                        ErrorFX.mostrar(mensaje, true, true, true);
                                                    }
                                                    catch
                                                    {

                                                    }

                                                    return null;
                                                }
                                            }
                                            catch (Exception exCCE)
                                            {
                                                ErrorFX.mostrar(exCCE, true, true, "Generar40 - 1756 - Datos de las líneas del CEE linea: " + i.ToString(), true);
                                                return null;
                                            }
                                            //MessageBox.Show("2023");
                                            try
                                            {
                                                //c_FraccionArancelaria oc_FraccionArancelaria = GetFraccionArancelaria(fraccion);
                                                //c_FraccionArancelaria oc_FraccionArancelaria = (c_FraccionArancelaria)System.Enum.Parse(typeof(c_FraccionArancelaria), "Item" + fraccion);

                                                oComercioExterior.Mercancias[i].FraccionArancelaria = fraccion;
                                                //oComercioExterior.Mercancias[i].FraccionArancelariaSpecified = true;
                                                //Version 1.1
                                                //oComercioExterior.Mercancias[i].UnidadAduana = cFraccionUnidad11.obtenerUM(fraccion);
                                                string fracciontr = "00" + cUMFracciones.obtenerUM(fraccion);
                                                oComercioExterior.Mercancias[i].UnidadAduana = (fracciontr).Substring(fracciontr.Length - 2, 2);
                                                if (oComercioExterior.Mercancias[i].UnidadAduana.Contains("00"))
                                                {
                                                    oComercioExterior.Mercancias[i].UnidadAduana = "01";
                                                }
                                            }
                                            catch (Exception exFraccion)
                                            {
                                                ErrorFX.mostrar(exFraccion, true, true
                                               , "Error en CCE. Fracción Arancelaría (Valor: noIdentificacion=" + fraccion + "). Generar40 - 1415", true);
                                                return null;

                                            }
                                            //MessageBox.Show("2043");
                                            if (!String.IsNullOrEmpty(concepto.NoIdentificacion))
                                            {
                                                oComercioExterior.Mercancias[i].NoIdentificacion = concepto.NoIdentificacion;
                                            }
                                            else
                                            {
                                                oComercioExterior.Mercancias[i].NoIdentificacion = DateTime.Now.ToShortDateString();
                                            }
                                            //MessageBox.Show("2052");
                                            //Agergar el id
                                            try
                                            {
                                                if (bool.Parse(ConfigurationManager.AppSettings["cceMostrarIndicador"].ToString()))
                                                {
                                                    oComercioExterior.Mercancias[i].NoIdentificacion = concepto.NoIdentificacion + i.ToString();
                                                }
                                            }
                                            catch
                                            {

                                            }
                                            //MessageBox.Show("2065");
                                            string mensajetr = "";
                                            try
                                            {

                                                //MessageBox.Show(oComprobante.Conceptos[i].NoIdentificacion);
                                                if (oComprobante.Conceptos[i] != null)
                                                {
                                                    oComprobante.Conceptos[i].NoIdentificacion = oComercioExterior.Mercancias[i].NoIdentificacion;
                                                }

                                                //MessageBox.Show(oComprobante.Conceptos[i].NoIdentificacion);

                                                decimal valorUnitario = decimal.Parse(concepto.ValorUnitario.ToString("#.######"));
                                                mensajetr += " Valor unitario: " + valorUnitario.ToString();

                                                //oComercioExterior.Mercancias[i].ValorUnitarioAduana = valorUnitario;
                                                decimal valorUnitario2 = decimal.Parse(concepto.ValorUnitario.ToString("#.##"));
                                                oComercioExterior.Mercancias[i].ValorUnitarioAduana = valorUnitario2;

                                                oComercioExterior.Mercancias[i].ValorUnitarioAduanaSpecified = true;
                                                /*
                                                oComercioExterior.Mercancias[i].ValorDolares = oComprobante.Conceptos[i].Importe;
                                                //Rodrigo Escalona: 2023/04/11 Calculo de ValorUnitarioAduana
                                                decimal ValorUnitarioAduana = Math.Round((oComercioExterior.Mercancias[i].CantidadAduana / oComercioExterior.Mercancias[i].ValorDolares), 2);
                                                oComercioExterior.Mercancias[i].ValorUnitarioAduana = ValorUnitarioAduana;
                                                */
                                                
                                                decimal ValorDolares = oComercioExterior.Mercancias[i].ValorUnitarioAduana
                                                    * oComercioExterior.Mercancias[i].CantidadAduana;//decimal.Parse(concepto.importe.ToString("#.##"));
                                                mensajetr += " CantidadAduana: " + oComercioExterior.Mercancias[i].CantidadAduana.ToString();
                                                ValorDolares = decimal.Parse(concepto.Importe.ToString("#.####"));
                                                mensajetr += " ValorDolares: " + ValorDolares.ToString();

                                                if (oEncapsular.ocEMPRESA.ENTITY_ID.Contains("BDM"))
                                                {
                                                    if (oComprobante.Moneda == "MXN")//CFDI40.c_Moneda.MXN)
                                                    {
                                                        ValorDolares = ValorDolares / oComercioExterior.TipoCambioUSD;
                                                    }
                                                }
                                                mensajetr += "ValorDolares: Dos " + ValorDolares.ToString();
                                                oComercioExterior.Mercancias[i].ValorDolares = decimal.Parse(ValorDolares.ToString("#.####")); ;//ValorDolares;
                                                                                                                                           //oComercioExterior.Mercancias[i].FraccionArancelariaSpecified = true;

                                                TotalUSD += oComercioExterior.Mercancias[i].ValorDolares;

                                            }
                                            catch (Exception exCCE)
                                            {
                                                ErrorFX.mostrar(exCCE, true, true, "Generar40 - 2628 - Datos de las líneas del CEE linea: " + i.ToString(), true);
                                                return null;
                                            }
                                            //MessageBox.Show("2101");
                                            if (oEncapsular.ocEMPRESA.ENTITY_ID.Equals("FIMA"))
                                            {
                                                //22/01/2018: Cargar la conversión explicita que tiene FIMA
                                                BitacoraFX.Log("Caso FIMA  UNidadAduana " + oComercioExterior.Mercancias[i].UnidadAduana);
                                                //Cargar la unidad de CE 
                                                if (oComercioExterior.Mercancias[i].UnidadAduana.Equals("01"))
                                                {
                                                    decimal WEIGHT = oFACTURA.cargarPartWEIGHT(oComercioExterior.Mercancias[i].NoIdentificacion);
                                                    BitacoraFX.Log("Caso FIMA  WEIGHT " + WEIGHT.ToString());
                                                    decimal CantidadAduanatr = Math.Round(concepto.Cantidad * WEIGHT);
                                                    BitacoraFX.Log("Caso FIMA  CantidadAduanatr " + CantidadAduanatr.ToString());
                                                    oComercioExterior.Mercancias[i].CantidadAduana = CantidadAduanatr;
                                                    //Math.Round(oComercioExterior.Mercancias[i].CantidadAduana * WEIGHT, 2);

                                                    //Rodrigo Escalona: 04/10/2021 Si es PC y WUEHT UM LB debe ser divido entre 2.2046
                                                    //decimal CantidadAduana = decimal.Parse(concepto.Cantidad.ToString("#.##"));
                                                    //if (concepto.ClaveUnidad.Equals("H87")
                                                    //    & oFACTURA.WEIGHT_UM.Equals("LB")
                                                    //    )
                                                    //{
                                                    //    decimal calculo = oComercioExterior.Mercancias[i].CantidadAduana
                                                    //        /
                                                    //       decimal.Parse("2.2046");

                                                    //    oComercioExterior.Mercancias[i].CantidadAduana = calculo;
                                                    //}


                                                    decimal ValorUnitarioAduanatr = oComercioExterior.Mercancias[i].ValorDolares / oComercioExterior.Mercancias[i].CantidadAduana;
                                                    BitacoraFX.Log("Caso FIMA  ValorUnitarioAduanatr " + ValorUnitarioAduanatr.ToString());
                                                    oComercioExterior.Mercancias[i].ValorUnitarioAduana = Math.Round(ValorUnitarioAduanatr, 6);

                                                }
                                                else
                                                {
                                                    try
                                                    {
                                                        ModeloDocumentosFX.ModeloDocumentosFXContainer dbContext = new ModeloDocumentosFXContainer();
                                                        configuracionUMproducto registroUMproducto = new configuracionUMproducto();
                                                        registroUMproducto = (from r in dbContext.configuracionUMproductoSet.Where
                                                                                (a => a.producto == concepto.NoIdentificacion)
                                                                              select r).FirstOrDefault();
                                                        if (registroUMproducto != null)
                                                        {
                                                            try
                                                            {
                                                                //Calcular con el factor de conversión
                                                                oComercioExterior.Mercancias[i].CantidadAduana = Math.Round(oComercioExterior.Mercancias[i].CantidadAduana * registroUMproducto.FactorCE, 2);
                                                                decimal ValorUnitarioAduanatr = oComercioExterior.Mercancias[i].ValorDolares / oComercioExterior.Mercancias[i].CantidadAduana;
                                                                oComercioExterior.Mercancias[i].ValorUnitarioAduana = Math.Round(ValorUnitarioAduanatr, 6);

                                                            }
                                                            catch (Exception exFraccion)
                                                            {

                                                                ErrorFX.mostrar(exFraccion, true, true, "Error en CCE (Complemento de Comercio Exterior) . Generar40 - 2133");
                                                                return null;
                                                            }
                                                        }
                                                        else
                                                        {
                                                            ErrorFX.mostrar("Generar40 CCE (Complemento de Comercio Exterior) - 2139 - Buscando Conversión , no existe para la Producto: " + concepto.NoIdentificacion
                                                                + Environment.NewLine
                                                                + "Debe agregarlo en configuración de UM de Producto"
                                                                , true, true, true);
                                                        }
                                                    }
                                                    catch (Exception e)
                                                    {
                                                        ErrorFX.mostrar(e, true, true, "Generar40 CCE (Complemento de Comercio Exterior) - 2144 - Buscando Conversión " + concepto.ClaveUnidad + " Producto: " + concepto.NoIdentificacion);
                                                    }
                                                }


                                                //FIMA dice siempre no 09/01/2018
                                                //Verificar la cantidad en Aduana
                                                //if (oComercioExterior.Mercancias[i].UnidadAduana.Equals("01"))
                                                //{
                                                //    decimal WEIGHT = oFACTURA.cargarPartWEIGHT(oComercioExterior.Mercancias[i].NoIdentificacion);
                                                //    decimal cantidadAduanatr = oComercioExterior.Mercancias[i].CantidadAduana;

                                                //    oComercioExterior.Mercancias[i].CantidadAduana = Math.Round(oComercioExterior.Mercancias[i].CantidadAduana * WEIGHT, 2);

                                                //    decimal ValorUnitarioAduanatr = oComercioExterior.Mercancias[i].ValorDolares / oComercioExterior.Mercancias[i].CantidadAduana;

                                                //    oComercioExterior.Mercancias[i].ValorUnitarioAduana = Math.Round(ValorUnitarioAduanatr, 2);
                                                //}

                                                //if (oComercioExterior.Mercancias[i].UnidadAduana.Equals("04"))
                                                //{
                                                //    decimal WEIGHT = oFACTURA.cargarPartWEIGHT(oComercioExterior.Mercancias[i].NoIdentificacion, false);
                                                //    decimal cantidadAduanatr = oComercioExterior.Mercancias[i].CantidadAduana;

                                                //    oComercioExterior.Mercancias[i].CantidadAduana = Math.Round(oComercioExterior.Mercancias[i].CantidadAduana * WEIGHT, 2);

                                                //    decimal ValorUnitarioAduanatr = oComercioExterior.Mercancias[i].ValorDolares / oComercioExterior.Mercancias[i].CantidadAduana;

                                                //    oComercioExterior.Mercancias[i].ValorUnitarioAduana = Math.Round(ValorUnitarioAduanatr, 2);
                                                //}
                                            }
                                            else
                                            {
                                                //MessageBox.Show("2183");
                                                try
                                                {
                                                    if (oComercioExterior.Mercancias[i].UnidadAduana.Equals("04"))
                                                    {
                                                        decimal WEIGHT = oFACTURA.cargarPartWEIGHT(oComercioExterior.Mercancias[i].NoIdentificacion, false);
                                                        decimal cantidadAduanatr = oComercioExterior.Mercancias[i].CantidadAduana;

                                                        oComercioExterior.Mercancias[i].CantidadAduana = Math.Round(oComercioExterior.Mercancias[i].CantidadAduana * WEIGHT, 2);

                                                        decimal ValorUnitarioAduanatr = oComercioExterior.Mercancias[i].ValorDolares / oComercioExterior.Mercancias[i].CantidadAduana;

                                                        oComercioExterior.Mercancias[i].ValorUnitarioAduana = Math.Round(ValorUnitarioAduanatr, 6);
                                                    }
                                                    //MessageBox.Show("2197");
                                                    //Para todos los demas clientes
                                                    if (oComercioExterior.Mercancias[i].UnidadAduana.Equals("01"))
                                                    {
                                                        decimal WEIGHT = oFACTURA.cargarPartWEIGHT(oComercioExterior.Mercancias[i].NoIdentificacion);
                                                        decimal cantidadAduanatr = oComercioExterior.Mercancias[i].CantidadAduana;
                                                        oComercioExterior.Mercancias[i].CantidadAduana = Math.Round(oComercioExterior.Mercancias[i].CantidadAduana * WEIGHT, 3);
                                                        if (oComercioExterior.Mercancias[i].CantidadAduana == 0)
                                                        {
                                                            throw new Exception("Error CCE: Factura " + oFACTURA.INVOICE_ID + " error weight Producto: " + oComercioExterior.Mercancias[i].NoIdentificacion);
                                                        }

                                                        decimal ValorUnitarioAduanatr = oComercioExterior.Mercancias[i].ValorDolares / oComercioExterior.Mercancias[i].CantidadAduana;
                                                        oComercioExterior.Mercancias[i].ValorUnitarioAduana = Math.Round(ValorUnitarioAduanatr, 6);
                                                        if (oComercioExterior.Mercancias[i].ValorUnitarioAduana == 0)
                                                        {

                                                        }



                                                    }
                                                    else
                                                    {
                                                        //Conversión Pieza a Pieza
                                                        if (oComercioExterior.Mercancias[i].UnidadAduana.Equals("06"))
                                                        {
                                                            oComercioExterior.Mercancias[i].CantidadAduana = Math.Round(oComercioExterior.Mercancias[i].CantidadAduana, 3);
                                                            if (oComercioExterior.Mercancias[i].CantidadAduana == 0)
                                                            {
                                                                throw new Exception("Error CCE: Factura " + oFACTURA.INVOICE_ID + " error Unidad Aduana 06 Producto: " + oComercioExterior.Mercancias[i].NoIdentificacion);
                                                            }
                                                            decimal ValorUnitarioAduanatr = oComercioExterior.Mercancias[i].ValorDolares / oComercioExterior.Mercancias[i].CantidadAduana;
                                                            oComercioExterior.Mercancias[i].ValorUnitarioAduana = Math.Round(ValorUnitarioAduanatr, 6);
                                                        }
                                                        else
                                                        {
                                                            //Otras unidades de medidas
                                                            oComercioExterior.Mercancias[i].CantidadAduana = Math.Round(oComercioExterior.Mercancias[i].CantidadAduana, 3);
                                                            if (oComercioExterior.Mercancias[i].CantidadAduana == 0)
                                                            {
                                                                throw new Exception("Error CCE: Factura " + oFACTURA.INVOICE_ID + " error weight Producto: " + oComercioExterior.Mercancias[i].NoIdentificacion);
                                                            }
                                                            decimal ValorUnitarioAduanatr = oComercioExterior.Mercancias[i].ValorDolares / oComercioExterior.Mercancias[i].CantidadAduana;
                                                            oComercioExterior.Mercancias[i].ValorUnitarioAduana = Math.Round(ValorUnitarioAduanatr, 6);
                                                        }

                                                    }
                                                }

                                                catch (Exception exCCE)
                                                {
                                                    ErrorFX.mostrar(exCCE, true, true, "Generar40 - 2235 - Busqueda de peso Datos de las líneas del CEE linea: " + i.ToString(), true);
                                                    return null;
                                                }
                                            }
                                            //Rodrigo Escalona. VAlidar si agregamos otra fraccione
                                            //if (oComercioExterior.Mercancias[i].NoIdentificacion=="05112233")
                                            //{
                                            //    //oComercioExterior.Mercancias[i].FraccionArancelaria = "";
                                            //    ComercioExteriorMercancia oComercioExteriorMercancia = new ComercioExteriorMercancia();
                                            //    oComercioExteriorMercancia = oComercioExterior.Mercancias[i];
                                            //    i++;
                                            //    //oComercioExteriorMercancia.FraccionArancelaria = "6307909900";
                                            //    oComercioExterior.Mercancias[i] = new ComercioExteriorMercancia();
                                            //    oComercioExterior.Mercancias[i].CantidadAduana= oComercioExteriorMercancia.CantidadAduana;
                                            //    oComercioExterior.Mercancias[i].NoIdentificacion = oComercioExteriorMercancia.NoIdentificacion;
                                            //    oComercioExterior.Mercancias[i].UnidadAduana = oComercioExteriorMercancia.UnidadAduana;
                                            //    oComercioExterior.Mercancias[i].ValorUnitarioAduanaSpecified = true;
                                            //    oComercioExterior.Mercancias[0].ValorDolares = decimal.Parse("2416.05"); //oComercioExteriorMercancia.ValorDolares; //decimal.Parse("2416.0");                                    ");
                                            //    oComercioExterior.Mercancias[i].ValorDolares = decimal.Parse("2416.05");

                                            //    oComercioExterior.Mercancias[i].CantidadAduanaSpecified = true;

                                            //    oComercioExterior.Mercancias[0].ValorUnitarioAduana = decimal.Parse("1.88");
                                            //    oComercioExterior.Mercancias[i].ValorUnitarioAduana = decimal.Parse("1.88");
                                            //    //oComercioExteriorMercancia.ValorUnitarioAduana; //decimal.Parse("9.5875");
                                            //    //oComercioExterior.Mercancias[0].ValorDolares = decimal.Parse("9.5875");

                                            //    oComercioExterior.Mercancias[i].FraccionArancelaria = "6307909900";

                                            //}

                                            i++;
                                            //MessageBox.Show("2246");
                                        }
                                    }
                                }
                            }
                            try
                            {
                                //MessageBox.Show("2252");
                                oComercioExterior.TotalUSD = Math.Round(decimal.Parse(TotalUSD.ToString("F")), 2, MidpointRounding.AwayFromZero).ToString();
                                if (oEncapsular.ocEMPRESA.ENTITY_ID.Contains("BDM"))
                                {
                                    oComprobante.SubTotal = decimal.Parse(oComercioExterior.TotalUSD);
                                    oComprobante.Total = decimal.Parse(oComercioExterior.TotalUSD);
                                }
                                else
                                {

                                }
                                //MessageBox.Show("2263");

                                XmlDocument doc = new XmlDocument();
                                XmlSerializerNamespaces myNamespaces_comercio = new XmlSerializerNamespaces();
                                ////Version 11
                                //myNamespaces_comercio.Add("cce11", "http://www.sat.gob.mx/ComercioExterior11");

                                //Version 20
                                myNamespaces_comercio.Add("cce20", "http://www.sat.gob.mx/ComercioExterior20");
                                using (XmlWriter writer = doc.CreateNavigator().AppendChild())
                                {
                                    new XmlSerializer(oComercioExterior.GetType()).Serialize(writer, oComercioExterior, myNamespaces_comercio);
                                }
                                return doc.DocumentElement;
                            }
                            catch (Exception exCCE)
                            {
                                ErrorFX.mostrar(exCCE, true, true, "Generar40 - 2268 - Datos de las líneas del CEE linea: " + i.ToString(), true);
                                return null;
                            }

                        }
                        catch (Exception exCCE)
                        {
                            ErrorFX.mostrar(exCCE, true, true, "Generar40 - 2275 - Datos de las líneas del CEE linea: " + i.ToString(), true);
                            return null;
                        }

                    }
                }
                catch (Exception exCCE)
                {
                    ErrorFX.mostrar(exCCE, true, true, "Generar40 - 1446", true);
                    return null;
                }
            }
            return null;
        }

        public string SelloRSA(string LLAVE, string PASSWORD, string strData, decimal ANIO)
        {
            try
            {
                StringBuilder sbPassword;
                StringBuilder sbPrivateKey;
                byte[] b;
                byte[] block;
                int keyBytes;



                string strKeyFile = LLAVE;

                sbPassword = new StringBuilder(PASSWORD);

                sbPrivateKey = Rsa.ReadEncPrivateKey(strKeyFile, sbPassword.ToString());

                keyBytes = Rsa.KeyBytes(sbPrivateKey.ToString());

                b = System.Text.Encoding.UTF8.GetBytes(strData);


                block = Rsa.EncodeMsgForSignature(keyBytes, b, HashAlgorithm.Sha256);

                block = Rsa.RawPrivate(block, sbPrivateKey.ToString());

                string resultado = System.Convert.ToBase64String(block);

                Wipe.String(sbPassword);
                Wipe.String(sbPrivateKey);
                Wipe.Data(block);

                return resultado;
            }
            catch (Exception exLeyendaFiscal)
            {
                ErrorFX.mostrar(exLeyendaFiscal, true, true, "Generar40 - SelloRSA - 1666", true);
                return null;
            }

        }


        public string Fechammmyy(string pFecha)
        {
            DateTime Fecha = DateTime.Parse(pFecha.ToString());
            string FechaFormateada = "";
            string[] mMeses = new string[13];

            mMeses[1] = "Ene";
            mMeses[2] = "Feb";
            mMeses[3] = "Mar";
            mMeses[4] = "Abr";
            mMeses[5] = "May";
            mMeses[6] = "Jun";
            mMeses[7] = "Jul";
            mMeses[8] = "Agt";
            mMeses[9] = "Sep";
            mMeses[10] = "Oct";
            mMeses[11] = "Nov";
            mMeses[12] = "Dic";

            int Mes = int.Parse(Fecha.Month.ToString());

            FechaFormateada = mMeses[Mes] + AgregarCero(Fecha.Year.ToString());

            return FechaFormateada;
        }
        public string AgregarCero(string p)
        {

            String regreso = p;

            if (p.Length < 2)

                regreso = "0" + p;

            return regreso;
        }

        public CFDI40.Comprobante generarComprobantePago(ModeloComprobantePago.pago oPago, Encapsular oEncapsular
            , bool autoAjustar = false, bool timbradoReal=true)
        {
            try
            {
                ModeloComprobantePago.documentosfxEntities dbContext = new ModeloComprobantePago.documentosfxEntities();
                string conection = dbContext.Database.Connection.ConnectionString;
                cCONEXCION oData = new cCONEXCION(conection);

                CFDI40.Comprobante oComprobante = new CFDI40.Comprobante();
                XmlSerializerNamespaces myNamespaces = new XmlSerializerNamespaces();
                myNamespaces.Add("cfdi", "http://www.sat.gob.mx/cfd/4");

                try
                {
                    cCERTIFICADO ocCERTIFICADO = new cCERTIFICADO();
                    ocCERTIFICADO.cargar_NumCert(oPago.noCertificado);

                    //BitacoraFX.Log("Generar40 - 1533 " + oPago.noCertificado);

                    cSERIE ocSERIE = new cSERIE();
                    try
                    {
                        //BitacoraFX.Log("Generar40 - 1541 " + ocCERTIFICADO.NO_CERTIFICADO);
                        X509Certificate cert = X509Certificate.CreateFromCertFile(ocCERTIFICADO.CERTIFICADO);
                        string NoSerie = cert.GetSerialNumberString();
                        string Certificado64 = System.Convert.ToBase64String(cert.GetRawCertData());
                        oComprobante.Certificado = Certificado64;
                        oComprobante.NoCertificado = timbrado40.validacion(oPago.noCertificado);


                    }
                    catch (Exception e)
                    {
                        ErrorFX.mostrar(e, true, true, "Generar40 - Busqueda o creación de Certificado - 1517 ");
                        return null;
                    }

                    oComprobante.TipoDeComprobante = c_TipoDeComprobante.P;
                    oComprobante.Folio = oPago.folio;
                    oComprobante.Serie = oPago.serie;
                    oComprobante.Exportacion = "01";
                    DateTime oFechatr = new DateTime(oPago.fecha.Year, oPago.fecha.Month, oPago.fecha.Day, oPago.fecha.Hour, oPago.fecha.Minute, 0);
                    oFechatr = new DateTime(oPago.fecha.Year, oPago.fecha.Month, oPago.fecha.Day, 0, 0, 0);
                    oComprobante.Fecha = oFechatr;

                    DateTime utcTime = DateTime.Now.ToUniversalTime();
                    string nzTimeZoneKey = "Pacific Standard Time (Mexico)";
                    TimeZoneInfo nzTimeZone = TimeZoneInfo.FindSystemTimeZoneById(nzTimeZoneKey);
                    DateTime nzDateTime = TimeZoneInfo.ConvertTimeFromUtc(utcTime, nzTimeZone);


                    oFechatr= new DateTime(nzDateTime.Year, nzDateTime.Month, nzDateTime.Day, 0, 0, 0);
                    oComprobante.Fecha = oFechatr;// new DateTime(nzDateTime.Year, nzDateTime.Month, nzDateTime.Day, nzDateTime.Hour, nzDateTime.Minute, 0);


                    oComprobante.NoCertificado = oPago.noCertificado;
                    oComprobante.SubTotal = 0;
                    oComprobante.Moneda = "XXX";
                    oComprobante.Total = 0;
                    oComprobante.LugarExpedicion = oPago.lugarExpedicion;

                    oComprobante.Emisor = new CFDI40.ComprobanteEmisor();
                    oComprobante.Emisor.Rfc = oPago.emisor;
                    oComprobante.Emisor.Nombre = LimpiarNombre(oPago.emisorNombre);
                    oComprobante.Emisor.RegimenFiscal = c_RegimenFiscal.Item601;// oPago.emisorRegimenFiscal;

                    oComprobante.Receptor = new CFDI40.ComprobanteReceptor();
                    oComprobante.Receptor.Rfc = oPago.receptor;
                    oComprobante.Receptor.Nombre = LimpiarNombre(oPago.receptorNombre);
                    oComprobante.Receptor.DomicilioFiscalReceptor = oPago.receptorCodigoPostal;
                    if (!String.IsNullOrEmpty(oPago.DomicilioFiscalReceptor))
                    {
                        oComprobante.Receptor.DomicilioFiscalReceptor = oPago.DomicilioFiscalReceptor;
                    }
                    
                    try
                    {
                        if (oComprobante.Receptor.Rfc.Contains("XEXX010101000"))
                        {
                            if (oPago.receptorPais == null)
                            {
                                oComprobante.Receptor.ResidenciaFiscal = CFDI40.c_Pais.USA;
                            }
                            else
                            {                                
                                oComprobante.Receptor.ResidenciaFiscal = (CFDI40.c_Pais)System.Enum.Parse(typeof(CFDI40.c_Pais)
                                    , oPago.receptorPais);
                            }
                            oComprobante.Receptor.ResidenciaFiscalSpecified = true;
                            oComprobante.Receptor.DomicilioFiscalReceptor = oComprobante.LugarExpedicion;
                            oComprobante.Receptor.RegimenFiscalReceptor = c_RegimenFiscal.Item616;
                        }

                    }
                    catch (Exception error)
                    {
                        ErrorFX.mostrar(error, true, true, "Generar40 - 548 - Seccion de Domicilio del Receptor " + Environment.NewLine
                            + "El país del cliente " + oComprobante.Receptor.Nombre + " no existe o no esta bien configurado. Debe tener los valores del Catalogo del SAT de paises"
                            
                            , true);
                        return null;
                    }


                    if (oComprobante.Receptor.Rfc.Equals("XEXX010101000"))
                    {
                        oComprobante.Receptor.NumRegIdTrib = oPago.receptorNumRegIdTrib;
                    }
                    if (oComprobante.Receptor.Rfc.Equals("XAXX010101000"))
                    {
                        oComprobante.Receptor.DomicilioFiscalReceptor = oComprobante.LugarExpedicion;
                        oComprobante.Receptor.RegimenFiscalReceptor = c_RegimenFiscal.Item616;
                        oComprobante.Receptor.UsoCFDI = c_UsoCFDI.S01;
                        oComprobante.Receptor.DomicilioFiscalReceptor = oComprobante.LugarExpedicion;
                        //Rodrigo Escalona: Agregar la Informacion Global
                        oComprobante.InformacionGlobal = new ComprobanteInformacionGlobal();
                        oComprobante.InformacionGlobal.Año = short.Parse(DateTime.Now.Year.ToString());
                        oComprobante.InformacionGlobal.Meses = c_Meses.Item01;
                        oComprobante.InformacionGlobal.Periodicidad = c_Periodicidad.Item01;

                    }

                    if (!String.IsNullOrEmpty(oPago.RegimenFiscalReceptor))
                    {
                        oComprobante.Receptor.RegimenFiscalReceptor = ObtenerRegimenFiscal(oPago.RegimenFiscalReceptor);
                    }

                    //Agregar complementos relacionados
                    var pagos = dbContext.pagoRelacionadoSet.Where(b => b.checkId == oPago.checkId
                                    & b.customerId == oPago.customerId);
                    if (pagos != null)
                    {
                        if (pagos.LongCount() != 0)
                        {
                            oComprobante.CfdiRelacionados = new ComprobanteCfdiRelacionados[1]; //new ComprobanteCfdiRelacionados();

                            ComprobanteCfdiRelacionados devolver = new CFDI40.ComprobanteCfdiRelacionados();
                            devolver.CfdiRelacionado = new CFDI40.ComprobanteCfdiRelacionadosCfdiRelacionado[pagos.LongCount()];
                            int i = 0;
                            foreach (pagoRelacionado pagoR in pagos)
                            {
                                devolver.TipoRelacion = c_TipoRelacion.Item01;
                                switch (pagoR.tipoRelacion)
                                {
                                    case "01":
                                        devolver.TipoRelacion = c_TipoRelacion.Item01;
                                        break;
                                    case "02":
                                        devolver.TipoRelacion = c_TipoRelacion.Item02;
                                        break;
                                    case "03":
                                        devolver.TipoRelacion = c_TipoRelacion.Item03;
                                        break;
                                    case "04":
                                        devolver.TipoRelacion = c_TipoRelacion.Item04;
                                        break;
                                }

                                devolver.CfdiRelacionado[i] = new CFDI40.ComprobanteCfdiRelacionadosCfdiRelacionado();
                                devolver.CfdiRelacionado[i].UUID = pagoR.UUID;
                                i++;
                            }
                            oComprobante.CfdiRelacionados[0] = new ComprobanteCfdiRelacionados();
                            oComprobante.CfdiRelacionados[0] = devolver;
                        }


                    }
                    oComprobante.Receptor.Nombre = oComprobante.Receptor.Nombre.ToUpper();

                    oComprobante.Receptor.UsoCFDI = c_UsoCFDI.CP01;  //"P01";
                    //Agregar el concepto
                    oComprobante.Conceptos = new CFDI40.ComprobanteConcepto[1];
                    oComprobante.Conceptos[0] = new CFDI40.ComprobanteConcepto();
                    oComprobante.Conceptos[0].Cantidad = 1;
                    oComprobante.Conceptos[0].ClaveUnidad = "ACT";
                    oComprobante.Conceptos[0].Descripcion = "Pago";
                    oComprobante.Conceptos[0].ValorUnitario = 0;
                    oComprobante.Conceptos[0].ClaveProdServ = "84111506";
                    oComprobante.Conceptos[0].Importe = 0;
                    oComprobante.Conceptos[0].ObjetoImp = "01";

                    //Agregar el complemento de pago
                    XmlElement oPagoComplemento = null;
                    //Generar40 OGenerar40 = new Generar40();
                    oPagoComplemento = ComplementoPago20(oPago, oEncapsular, autoAjustar);
                    oComprobante.Complemento = new CFDI40.ComprobanteComplemento();
                    oComprobante.Complemento.Any = new XmlElement[1];
                    oComprobante.Complemento.Any[0] = oPagoComplemento;
                    string cadena = timbrado40.cadena(oComprobante);


                    ocSERIE.cargar_serie(oComprobante.Serie, ocCERTIFICADO.ROW_ID_EMPRESA);
                    if (String.IsNullOrEmpty(ocSERIE.ID))
                    {
                        ErrorFX.mostrar("Generar40 - 1613 - La serie " + oComprobante.Serie + " del Certificado "
                            + ocCERTIFICADO.NO_CERTIFICADO + " Empresa Id: " + ocCERTIFICADO.ROW_ID_EMPRESA
                            + " no existe", false, false, false);

                        ocSERIE.cargar_serie(oComprobante.Serie);
                        if (String.IsNullOrEmpty(ocSERIE.ID))
                        {
                            ErrorFX.mostrar("Generar40 - 1613 - La serie " + oComprobante.Serie + " del Certificado "
                                + ocCERTIFICADO.NO_CERTIFICADO + " Empresa Id: " + ocCERTIFICADO.ROW_ID_EMPRESA
                            + " no existe", true, false, false);
                            return null;
                        }
                    }
                    BitacoraFX.Log("Generar40 - 1541 " + ocCERTIFICADO.NO_CERTIFICADO + " ocCERTIFICADO.PASSWORD=" + ocCERTIFICADO.PASSWORD);

                    oComprobante.Sello = timbrado40.SelloRSA(ocCERTIFICADO.LLAVE, ocCERTIFICADO.PASSWORD, cadena, oComprobante.Fecha.Year);

                    string archivotr = timbrado40.guardarComprobante(oComprobante, oPago, ocSERIE, oPago.estado, timbradoReal, oPago.fechaPago);
                    if (!String.IsNullOrEmpty(archivotr))
                    {
                        oPago.xml = archivotr;

                        //Guardar
                        ModeloComprobantePago.pago oRegistro = dbContext.pagoSet.Where(a => a.Id == oPago.Id).FirstOrDefault();
                        oRegistro.xml = archivotr;
                        oRegistro.sello = oComprobante.Sello;
                        dbContext.pagoSet.Attach(oRegistro);
                        dbContext.Entry(oRegistro).State = System.Data.Entity.EntityState.Modified;
                        dbContext.SaveChanges();
                    }

                    return oComprobante;
                }
                catch (Exception errores)
                {
                    ErrorFX.mostrar(errores, true, true, "generar40 - 1432 - generarComprobantePago ", false);
                    return null;
                }


            }
            catch (Exception errores)
            {
                ErrorFX.mostrar(errores, true, true, "Generar40 - 1436 - generarComprobantePago ", false);
                return null;
            }
        }

        private c_RegimenFiscal ObtenerRegimenFiscal(string regimenFiscalReceptor)
        {
            switch (regimenFiscalReceptor)
            {
                case "601":
                    return  c_RegimenFiscal.Item601;
                    break;
                case "603":
                    return  c_RegimenFiscal.Item603;
                    break;
                case "605":
                    return  c_RegimenFiscal.Item605;
                    break;
                case "606":
                    return  c_RegimenFiscal.Item606;
                    break;
                case "608":
                    return  c_RegimenFiscal.Item608;
                    break;
                case "609":
                    return  c_RegimenFiscal.Item609;
                    break;
                case "610":
                    return  c_RegimenFiscal.Item610;
                    break;
                case "611":
                    return  c_RegimenFiscal.Item611;
                    break;
                case "612":
                    return  c_RegimenFiscal.Item612;
                    break;
                case "614":
                    return  c_RegimenFiscal.Item614;
                    break;
                case "616":
                    return  c_RegimenFiscal.Item616;
                    break;
                case "620":
                    return  c_RegimenFiscal.Item620;
                    break;

                case "621":
                    return  c_RegimenFiscal.Item621;
                    break;

                case "622":
                    return  c_RegimenFiscal.Item622;
                    break;

                case "623":
                    return  c_RegimenFiscal.Item623;
                    break;

                case "624":
                    return  c_RegimenFiscal.Item624;
                    break;

                case "628":
                    return  c_RegimenFiscal.Item628;
                    break;

                case "607":
                    return  c_RegimenFiscal.Item607;
                    break;

                case "629":
                    return  c_RegimenFiscal.Item629;
                    break;
                case "626":
                    return c_RegimenFiscal.Item626;
                    break;
                case "630":
                    return  c_RegimenFiscal.Item630;
                    break;

                case "615":
                    return  c_RegimenFiscal.Item615;
                    break;

                case "625":
                    return  c_RegimenFiscal.Item625;
                    break;
                default:
                    return c_RegimenFiscal.Item601;
                    break;
            }
        }

        public XmlElement ComplementoPago20(ModeloComprobantePago.pago oPago, Encapsular oEncapsular, bool autoAjustar = false)
        {
            try
            {

                Pago20.Pagos oPagos = new Pago20.Pagos();

                //Version 11
                oPagos.Pago = new Pago20.PagosPago[1];
                try
                {
                    oPagos.Pago[0] = new Pago20.PagosPago();


                    DateTime oFechaPagoTr = new DateTime(oPago.fechaPago.Year, oPago.fechaPago.Month, oPago.fechaPago.Day
                        , oPago.fechaPago.Hour, oPago.fechaPago.Minute, 0);


                    oPagos.Pago[0].FechaPago = oFechaPagoTr;
                    //Rodrigo Escalona
                    //Aqui debemos llenar la forma de pago
                    oPagos.Pago[0].FormaDePagoP = Pago20.c_FormaPago.Item99;




                    if (!String.IsNullOrEmpty(oPago.monedaP))
                    {
                        if (oPago.monedaP.Contains("MXN"))
                        {
                            oPagos.Pago[0].MonedaP = Pago20.c_Moneda.MXN;
                        }
                        if (oPago.monedaP.Contains("USD"))
                        {
                            oPagos.Pago[0].MonedaP = Pago20.c_Moneda.USD;
                        }
                    }

                    oPagos.Pago[0].TipoCambioPSpecified = true;
                    decimal tipoCambioPtr = 1;
                    oPagos.Pago[0].TipoCambioP = tipoCambioPtr;

                    if (!oPago.monedaP.Contains("MX"))
                    {
                        if (oPago.tipoCambioP != null)
                        {
                            tipoCambioPtr = decimal.Parse(oPago.tipoCambioP.ToString());
                            oPagos.Pago[0].TipoCambioP = Math.Round(tipoCambioPtr, 4);
                        }
                    }

                    decimal montotr = 1;
                    if (oPago.monto != null)
                    {
                        montotr = decimal.Parse(oPago.monto.ToString());
                        montotr = Math.Round(montotr, 2);
                    }

                    //Rodrigo 16/06/2022  Cambio a Decimal de 2
                    //oPagos.Pago[0].Monto = Math.Round(montotr, 2);//.ToString("#.##");
                    oPagos.Pago[0].Monto = Math.Round(montotr, 2);

                    if (!String.IsNullOrEmpty(oPago.NumOperacion))
                    {
                        oPagos.Pago[0].NumOperacion = oPago.NumOperacion;
                    }
                    if (!String.IsNullOrEmpty(oPago.RfcEmisorCtaOrd))
                    {
                        oPagos.Pago[0].RfcEmisorCtaOrd = oPago.RfcEmisorCtaOrd;
                    }

                    oPagos.Pago[0].FormaDePagoP = Pago20.c_FormaPago.Item03;
                    //Cargar el ya guardado
                    if (!String.IsNullOrEmpty(oPago.formaDePagoP))
                    {
                        //Pago20.c_FormaPago oc_FormaPago = (Pago20.c_FormaPago)System.Enum.Parse(typeof(Pago20.c_FormaPago), "Item" + oPago.formaDePagoP);
                        //oPagos.Pago[0].FormaDePagoP = oc_FormaPago;
                        switch (oPago.formaDePagoP)
                        {
                            case "01":
                                oPagos.Pago[0].FormaDePagoP = Pago20.c_FormaPago.Item01;
                                break;
                            case "02":
                                oPagos.Pago[0].FormaDePagoP = Pago20.c_FormaPago.Item02;
                                break;
                            case "03":
                                oPagos.Pago[0].FormaDePagoP = Pago20.c_FormaPago.Item03;
                                break;
                            case "04":
                                oPagos.Pago[0].FormaDePagoP = Pago20.c_FormaPago.Item04;
                                break;
                            case "05":
                                oPagos.Pago[0].FormaDePagoP = Pago20.c_FormaPago.Item05;
                                break;
                            case "06":
                                oPagos.Pago[0].FormaDePagoP = Pago20.c_FormaPago.Item06;
                                break;
                            case "07":
                                oPagos.Pago[0].FormaDePagoP = Pago20.c_FormaPago.Item01;
                                break;
                            case "08":
                                oPagos.Pago[0].FormaDePagoP = Pago20.c_FormaPago.Item08;
                                break;
                            case "12":
                                oPagos.Pago[0].FormaDePagoP = Pago20.c_FormaPago.Item12;
                                break;
                            case "13":
                                oPagos.Pago[0].FormaDePagoP = Pago20.c_FormaPago.Item13;
                                break;
                            case "14":
                                oPagos.Pago[0].FormaDePagoP = Pago20.c_FormaPago.Item14;
                                break;
                            case "15":
                                oPagos.Pago[0].FormaDePagoP = Pago20.c_FormaPago.Item15;
                                break;
                        }
                    }
                }
                catch (Exception exCCE)
                {
                    ErrorFX.mostrar(exCCE, true, true, "Generar40 - 1777 - complementoPago20 Cabecera ", true);
                    return null;
                }

                if (!String.IsNullOrEmpty(oPago.NomBancoOrdExt))
                {
                    oPagos.Pago[0].NomBancoOrdExt = oPago.NomBancoOrdExt;
                }
                if (!String.IsNullOrEmpty(oPago.NumOperacion))
                {
                    oPagos.Pago[0].NumOperacion = oPago.NumOperacion;
                }
                if (!String.IsNullOrEmpty(oPago.CtaBeneficiario))
                {
                    oPagos.Pago[0].CtaBeneficiario = oPago.CtaBeneficiario;
                }
                if (!String.IsNullOrEmpty(oPago.CtaOrdenante))
                {
                    oPagos.Pago[0].CtaOrdenante = oPago.CtaOrdenante;
                }
                if (!String.IsNullOrEmpty(oPago.RfcEmisorCtaBen))
                {
                    oPagos.Pago[0].RfcEmisorCtaBen = oPago.RfcEmisorCtaBen;
                }
                if (!String.IsNullOrEmpty(oPago.RfcEmisorCtaOrd))
                {
                    oPagos.Pago[0].RfcEmisorCtaOrd = oPago.RfcEmisorCtaOrd;
                }
                //Agregar los totales
                oPagos.Totales = new Pago20.PagosTotales();
                foreach (pagoTotales total in oPago.pagoTotales)
                {
                    //oPagos.Totales.TotalTrasladosBaseIVA0Specified = true;
                    //oPagos.Totales.TotalTrasladosBaseIVA0 = 0;
                    if (total.TotalRetencionesIEPS != 0)
                    {
                        oPagos.Totales.TotalRetencionesIEPSSpecified = true;
                        oPagos.Totales.TotalRetencionesIEPS = Math.Round(total.TotalRetencionesIEPS, 2);

                    }
                    if (total.TotalRetencionesISR != 0)
                    {
                        oPagos.Totales.TotalRetencionesISRSpecified = true;
                        oPagos.Totales.TotalRetencionesISR = Math.Round(total.TotalRetencionesISR, 2);
                    }
                    if (total.TotalRetencionesIVA != 0)
                    {
                        oPagos.Totales.TotalRetencionesIVASpecified = true;
                        oPagos.Totales.TotalRetencionesIVA = Math.Round(total.TotalRetencionesIVA, 2);
                    }
                    if (total.TotalTrasladosBaseIVA0 != 0)
                    {
                        oPagos.Totales.TotalTrasladosBaseIVA0Specified = true;
                        oPagos.Totales.TotalTrasladosBaseIVA0 = Math.Round(total.TotalTrasladosBaseIVA0, 2);
                        oPagos.Totales.TotalTrasladosImpuestoIVA0Specified = true;
                        oPagos.Totales.TotalTrasladosImpuestoIVA0 = 0.00M;
                    }
                    if (total.TotalTrasladosBaseIVA16 != 0)
                    {
                        oPagos.Totales.TotalTrasladosBaseIVA16Specified = true;
                        oPagos.Totales.TotalTrasladosBaseIVA16 = Math.Round(total.TotalTrasladosBaseIVA16, 2);
                    }
                    if (total.TotalTrasladosBaseIVA8 != 0)
                    {
                        oPagos.Totales.TotalTrasladosBaseIVA8Specified = true;
                        oPagos.Totales.TotalTrasladosBaseIVA8 = Math.Round(total.TotalTrasladosBaseIVA8, 2);
                    }
                    if (total.TotalTrasladosBaseIVAExento != 0)
                    {
                        oPagos.Totales.TotalTrasladosBaseIVAExentoSpecified = true;
                        oPagos.Totales.TotalTrasladosBaseIVAExento = Math.Round(total.TotalTrasladosBaseIVAExento, 2);
                    }
                    if (total.TotalTrasladosImpuestoIVA0 != 0)
                    {
                        oPagos.Totales.TotalTrasladosImpuestoIVA0Specified = true;
                        oPagos.Totales.TotalTrasladosImpuestoIVA0 = Math.Round(total.TotalTrasladosImpuestoIVA0, 2);
                    }
                    if (total.TotalTrasladosImpuestoIVA16 != 0)
                    {
                        oPagos.Totales.TotalTrasladosImpuestoIVA16Specified = true;
                        oPagos.Totales.TotalTrasladosImpuestoIVA16 = Math.Round(total.TotalTrasladosImpuestoIVA16, 2);
                    }

                    if (total.TotalTrasladosImpuestoIVA8 != 0)
                    {
                        oPagos.Totales.TotalTrasladosImpuestoIVA8Specified = true;
                        oPagos.Totales.TotalTrasladosImpuestoIVA8 = Math.Round(total.TotalTrasladosImpuestoIVA8, 2);
                    }

                    oPagos.Totales.MontoTotalPagos = Math.Round(total.MontoTotalPagos, 2);

                    Bitacora.Log("oPagos.Totales.MontoTotalPagos " + oPagos.Totales.MontoTotalPagos.ToString());
                }

                oPagos.Pago[0].DoctoRelacionado = new Pago20.PagosPagoDoctoRelacionado[oPago.pagoLinea.Count];
                int i = 0;
                decimal montoTr = 0;
                foreach (pagoLinea linea in oPago.pagoLinea)
                {
                    try
                    {


                        oPagos.Pago[0].DoctoRelacionado[i] = new Pago20.PagosPagoDoctoRelacionado();
                        oPagos.Pago[0].DoctoRelacionado[i].EquivalenciaDRSpecified = true;
                        oPagos.Pago[0].DoctoRelacionado[i].EquivalenciaDR = 1;
                        if (linea.EquivalenciaDR != null)
                        {
                            oPagos.Pago[0].DoctoRelacionado[i].EquivalenciaDR = (decimal)linea.EquivalenciaDR;
                            if (oPagos.Pago[0].DoctoRelacionado[i].EquivalenciaDR == 1)
                            {
                                oPagos.Pago[0].DoctoRelacionado[i].EquivalenciaDR = 1;
                            }
                        }
                        
                        try
                        {
                            oPagos.Pago[0].DoctoRelacionado[i].IdDocumento = linea.UUID;
                        }
                        catch (Exception exCCE)
                        {
                            ErrorFX.mostrar(exCCE, true, true, "Generar40 - 1796 - complementoPago20 Linea linea.UUID " + i.ToString(), true);
                            return null;
                        }
                        try
                        {
                            oPagos.Pago[0].DoctoRelacionado[i].Serie = linea.serie;
                        }
                        catch (Exception exCCE)
                        {
                            ErrorFX.mostrar(exCCE, true, true, "Generar40 - 1806 - complementoPago20 Linea linea.serie " + i.ToString(), true);
                            return null;
                        }

                        try
                        {
                            oPagos.Pago[0].DoctoRelacionado[i].Folio = linea.folio;
                        }
                        catch (Exception exCCE)
                        {
                            ErrorFX.mostrar(exCCE, true, true, "Generar40 - 1816 - complementoPago20 Linea linea.folio " + i.ToString(), true);
                            return null;
                        }

                        if (linea.MonedaDR.Contains("MX"))
                        {
                            oPagos.Pago[0].DoctoRelacionado[i].MonedaDR = Pago20.c_Moneda.MXN;
                        }
                        if (linea.MonedaDR.Contains("USD"))
                        {
                            oPagos.Pago[0].DoctoRelacionado[i].MonedaDR = Pago20.c_Moneda.USD;
                        }


                        //Si el valor de este campo es diferente al valor registrado en el campo MonedaP, se debe
                        //registrar información en el campoTipoCambioDR

                        if (oPagos.Pago[0].MonedaP != oPagos.Pago[0].DoctoRelacionado[i].MonedaDR)
                        {
                            if (!linea.MonedaDR.Contains("MX"))
                            {
                                decimal tipoCambioDRtr = 1;
                                if (linea.TipoCambioDR != null)
                                {
                                    try
                                    {
                                        tipoCambioDRtr = decimal.Parse(linea.TipoCambioDR.ToString());
                                    }
                                    catch (Exception exCCE)
                                    {
                                        ErrorFX.mostrar(exCCE, true, true, "Generar40 - 4355 - complementoPago20 Linea tipoCambioDRtr " + i.ToString(), true);
                                        return null;
                                    }

                                }
                            }
                        }


                        //oPagos.Pago[0].DoctoRelacionado[i].MetodoDePagoDR = Pago20.c_MetodoPago.PPD;
                        string parcialidad = "1";
                        if (!String.IsNullOrEmpty(linea.NumParcialidad))
                        {
                            parcialidad = linea.NumParcialidad;
                        }
                        oPagos.Pago[0].DoctoRelacionado[i].NumParcialidad = parcialidad;
                        decimal ImpPagadotr = (decimal)linea.ImpPagado;
                        decimal ImpSaldoAnttr = (decimal)linea.ImpSaldoAnt;
                        decimal ImpSaldoInsolutotr = ImpPagadotr;
                        if (linea.ImpSaldoInsoluto != null)
                        {
                            ImpSaldoInsolutotr = (decimal)linea.ImpSaldoInsoluto;
                        }



                        //if (linea.ImpPagado != null)
                        //{
                        //    try
                        //    {
                        //        //ImpPagadotr = decimal.Parse(linea.ImpPagado.ToString());
                        //    }
                        //    catch (Exception exCCE)
                        //    {
                        //        ErrorFX.mostrar(exCCE, true, true, "Generar40 - 1859 - complementoPago20 Linea ImpPagadotr " + i.ToString(), true);
                        //        return null;
                        //    }
                        //}
                        //Pago20: DoctoRelacionado IdDocumento = "FB0860BC-E239-45E0-8377-12E3981E9619" 
                        //Serie = "A" Folio = "7647" MonedaDR = "USD" MetodoDePagoDR = "PPD" NumParcialidad = "1" 
                        //    ImpSaldoAnt = "77377.300000" ImpPagado = "77377.300000" 
                        //    ImpSaldoInsoluto = "0.000000" />

                        //oPagos.Pago[0].DoctoRelacionado[i].ImpPagadoSpecified = true;


                        oPagos.Pago[0].DoctoRelacionado[i].ImpPagado = Math.Round(ImpPagadotr, 2, MidpointRounding.AwayFromZero); //Math.Round(ImpPagadotr, 2);// linea.ImpPagado;
                        ImpPagadotr = oPagos.Pago[0].DoctoRelacionado[i].ImpPagado;

                        if (oPagos.Pago[0].MonedaP != Pago20.c_Moneda.MXN)
                        {
                            oPagos.Pago[0].DoctoRelacionado[i].ImpPagado = Math.Round(oPagos.Pago[0].DoctoRelacionado[i].ImpPagado, 2);
                        }

                        oPagos.Pago[0].DoctoRelacionado[i].ImpSaldoAnt = Math.Round(ImpSaldoAnttr, 2);//oPagos.Pago[0].DoctoRelacionado[i].ImpPagado;
                        oPagos.Pago[0].DoctoRelacionado[i].ImpSaldoInsoluto = oPagos.Pago[0].DoctoRelacionado[i].ImpSaldoAnt -
                           oPagos.Pago[0].DoctoRelacionado[i].ImpPagado; //Math.Round(ImpSaldoInsolutotr, 2);// 0.00M;
                        if (oPagos.Pago[0].DoctoRelacionado[i].ImpSaldoInsoluto < 0)
                        {
                            oPagos.Pago[0].DoctoRelacionado[i].ImpSaldoInsoluto=0;

                            oPagos.Pago[0].DoctoRelacionado[i].ImpSaldoAnt = oPagos.Pago[0].DoctoRelacionado[i].ImpPagado;
                        }

                            /*
                            decimal impSaldoAntTr = 0;
                            if (linea.ImpPagado != null)
                            {
                                //oPagos.Pago[0].DoctoRelacionado[i].ImpSaldoAntSpecified = true;

                                try
                                {
                                    impSaldoAntTr = decimal.Parse(linea.ImpSaldoAnt.ToString());
                                }
                                catch (Exception exCCE)
                                {
                                    ErrorFX.mostrar(exCCE, true, true, "Generar40 - 1859 - complementoPago20 Linea impSaldoAntTr " + i.ToString(), true);
                                    return null;
                                }
                                oPagos.Pago[0].DoctoRelacionado[i].ImpSaldoAnt = Math.Round(impSaldoAntTr, 2);// linea.ImpSaldoAnt;
                                if (oPagos.Pago[0].DoctoRelacionado[i].ImpSaldoAnt == 0)
                                {
                                    oPagos.Pago[0].DoctoRelacionado[i].ImpSaldoAnt = Math.Round(ImpPagadotr, 2);
                                }

                                if (oPagos.Pago[0].MonedaP != Pago20.c_Moneda.MXN)
                                {
                                    oPagos.Pago[0].DoctoRelacionado[i].ImpSaldoAnt = Math.Round(oPagos.Pago[0].DoctoRelacionado[i].ImpSaldoAnt, 2);
                                }
                            }



                            //oPagos.Pago[0].DoctoRelacionado[i].ImpSaldoInsolutoSpecified = true;
                            oPagos.Pago[0].DoctoRelacionado[i].ImpSaldoInsoluto = Math.Round(Math.Abs(oPagos.Pago[0].DoctoRelacionado[i].ImpPagado - oPagos.Pago[0].DoctoRelacionado[i].ImpSaldoAnt), 2);
                            if (oPagos.Pago[0].MonedaP != Pago20.c_Moneda.MXN)
                            {
                                oPagos.Pago[0].DoctoRelacionado[i].ImpSaldoInsoluto = Math.Round(oPagos.Pago[0].DoctoRelacionado[i].ImpSaldoInsoluto, 2);
                            }
                            */

                            oPagos.Pago[0].DoctoRelacionado[i].ObjetoImpDR = Pago20.c_ObjetoImp.Item01;
                        //Rodrigo Escalona 22/03/2022 Validar el impuesto
                        if (linea.pagoLineaImpuesto.Count > 0)
                        {
                            oPagos.Pago[0].DoctoRelacionado[i].ObjetoImpDR = Pago20.c_ObjetoImp.Item02;
                            oPagos.Pago[0].DoctoRelacionado[i].ImpuestosDR = new Pago20.PagosPagoDoctoRelacionadoImpuestosDR();
                            oPagos.Pago[0].DoctoRelacionado[i].ImpuestosDR.TrasladosDR = new Pago20.PagosPagoDoctoRelacionadoImpuestosDRTrasladoDR[1];
                            oPagos.Pago[0].DoctoRelacionado[i].ImpuestosDR.TrasladosDR[0] = new Pago20.PagosPagoDoctoRelacionadoImpuestosDRTrasladoDR();
                            foreach (pagoLineaImpuesto ImpuestoTr in linea.pagoLineaImpuesto)
                            {
                                switch (ImpuestoTr.TipoFactorDR)
                                {
                                    case "Cuota":
                                        oPagos.Pago[0].DoctoRelacionado[i].ImpuestosDR.TrasladosDR[0].TipoFactorDR = Pago20.c_TipoFactor.Cuota;
                                        break;
                                    case "Tasa":
                                        oPagos.Pago[0].DoctoRelacionado[i].ImpuestosDR.TrasladosDR[0].TipoFactorDR = Pago20.c_TipoFactor.Tasa;
                                        break;
                                    case "Exento":
                                        oPagos.Pago[0].DoctoRelacionado[i].ImpuestosDR.TrasladosDR[0].TipoFactorDR = Pago20.c_TipoFactor.Exento;
                                        break;
                                }
                                oPagos.Pago[0].DoctoRelacionado[i].ImpuestosDR.TrasladosDR[0].ImporteDRSpecified = true;
                                oPagos.Pago[0].DoctoRelacionado[i].ImpuestosDR.TrasladosDR[0].TasaOCuotaDRSpecified = true;
                                oPagos.Pago[0].DoctoRelacionado[i].ImpuestosDR.TrasladosDR[0].BaseDR = Math.Round(ImpuestoTr.BaseDR, 2);
                                oPagos.Pago[0].DoctoRelacionado[i].ImpuestosDR.TrasladosDR[0].ImporteDR = Math.Round(ImpuestoTr.ImporteDR, 2);
                                oPagos.Pago[0].DoctoRelacionado[i].ImpuestosDR.TrasladosDR[0].ImpuestoDR = Pago20.c_Impuesto.Item002;
                                if (ImpuestoTr.ImporteDR == 0)
                                {
                                    oPagos.Pago[0].DoctoRelacionado[i].ImpuestosDR.TrasladosDR[0].TasaOCuotaDR = "0.000000";//ImpuestoTr.TasaOCuotaDR;
                                    if (oPagos.Pago[0].DoctoRelacionado[i].ImpuestosDR.TrasladosDR[0].BaseDR!=
                                        oPagos.Pago[0].DoctoRelacionado[i].ImpPagado
                                        )
                                    {
                                        oPagos.Pago[0].DoctoRelacionado[i].ImpuestosDR.TrasladosDR[0].BaseDR = oPagos.Pago[0].DoctoRelacionado[i].ImpPagado;
                                    }
                                }
                                else
                                {
                                    oPagos.Pago[0].DoctoRelacionado[i].ImpuestosDR.TrasladosDR[0].TasaOCuotaDR = "0.160000";//ImpuestoTr.TasaOCuotaDR;
                                }

                            }
                        }
                        //Validar Retenciones
                        if (linea.pagoLineaRetencion.Count > 0)
                        {
                            oPagos.Pago[0].DoctoRelacionado[i].ObjetoImpDR = Pago20.c_ObjetoImp.Item02;
                            if (oPagos.Pago[0].DoctoRelacionado[i].ImpuestosDR == null)
                            {
                                oPagos.Pago[0].DoctoRelacionado[i].ImpuestosDR = new Pago20.PagosPagoDoctoRelacionadoImpuestosDR();
                            }
                            foreach (pagoLineaRetencion RetencionTr in linea.pagoLineaRetencion)
                            {
                                oPagos.Pago[0].DoctoRelacionado[i].ImpuestosDR.RetencionesDR = new Pago20.PagosPagoDoctoRelacionadoImpuestosDRRetencionDR[1];
                                oPagos.Pago[0].DoctoRelacionado[i].ImpuestosDR.RetencionesDR[0] = new Pago20.PagosPagoDoctoRelacionadoImpuestosDRRetencionDR();
                                oPagos.Pago[0].DoctoRelacionado[i].ImpuestosDR.RetencionesDR[0].TipoFactorDR = Pago20.c_TipoFactor.Tasa;
                                oPagos.Pago[0].DoctoRelacionado[i].ImpuestosDR.RetencionesDR[0].BaseDR = Math.Round(RetencionTr.BaseDR + RetencionTr.ImporteDR, 2);
                                oPagos.Pago[0].DoctoRelacionado[i].ImpuestosDR.RetencionesDR[0].ImporteDR = Math.Round(RetencionTr.ImporteDR, 2);
                                oPagos.Pago[0].DoctoRelacionado[i].ImpuestosDR.RetencionesDR[0].ImpuestoDR = Pago20.c_Impuesto.Item002;
                                oPagos.Pago[0].DoctoRelacionado[i].ImpuestosDR.RetencionesDR[0].TasaOCuotaDR = "0.160000";//ImpuestoTr.TasaOCuotaDR;
                            }

                        }

                        montoTr += ImpPagadotr;

                        i++;
                    }

                    catch (Exception exCCE)
                    {
                        ErrorFX.mostrar(exCCE, true, true, "Generar40 - 4516 - complementoPago20 Lineas " + i.ToString(), true);
                        return null;
                    }
                }

                //Rodrigo Escalona: 27/03/2023 - Agregar los impuestos
                oPagos.Pago[0].ImpuestosP = new Pago20.PagosPagoImpuestosP();
                if (oPago.pagoImpuesto.Count > 0)
                {

                    oPagos.Pago[0].ImpuestosP.TrasladosP = new Pago20.PagosPagoImpuestosPTrasladoP[oPago.pagoImpuesto.Count];
                    foreach (pagoImpuesto ImpuestoPTr in oPago.pagoImpuesto)
                    {
                        oPagos.Pago[0].ImpuestosP.TrasladosP[0] = new Pago20.PagosPagoImpuestosPTrasladoP();
                        oPagos.Pago[0].ImpuestosP.TrasladosP[0].BaseP = Math.Round(ImpuestoPTr.BaseP, 2);
                        oPagos.Pago[0].ImpuestosP.TrasladosP[0].ImportePSpecified = true;
                        oPagos.Pago[0].ImpuestosP.TrasladosP[0].ImporteP = Math.Round(ImpuestoPTr.ImporteP, 2);
                        oPagos.Pago[0].ImpuestosP.TrasladosP[0].ImpuestoP = Pago20.c_Impuesto.Item002;
                        oPagos.Pago[0].ImpuestosP.TrasladosP[0].TipoFactorP = Pago20.c_TipoFactor.Tasa;
                        oPagos.Pago[0].ImpuestosP.TrasladosP[0].TasaOCuotaPSpecified = true;
                        if (oPagos.Pago[0].ImpuestosP.TrasladosP[0].ImporteP == 0)
                        {
                            oPagos.Pago[0].ImpuestosP.TrasladosP[0].TasaOCuotaP = "0.000000";
                        }
                        else
                        {
                            oPagos.Pago[0].ImpuestosP.TrasladosP[0].TasaOCuotaP = "0.160000";
                        }
                    }
                }

                //Verificar las Retenciones
                if (oPago.pagoRetencion.Count > 0)
                {
                    oPagos.Pago[0].ImpuestosP.RetencionesP = new Pago20.PagosPagoImpuestosPRetencionP[oPago.pagoRetencion.Count];
                    foreach (pagoRetencion RetencionPTr in oPago.pagoRetencion)
                    {
                        oPagos.Pago[0].ImpuestosP.RetencionesP[0] = new Pago20.PagosPagoImpuestosPRetencionP();
                        oPagos.Pago[0].ImpuestosP.RetencionesP[0].ImpuestoP = Pago20.c_Impuesto.Item002;
                        oPagos.Pago[0].ImpuestosP.RetencionesP[0].ImporteP = Math.Round(RetencionPTr.ImporteP, 2);

                    }
                }

                CorrecionDecimales(oPagos);

                try
                {
                    XmlDocument doc = new XmlDocument();
                    XmlSerializerNamespaces myNamespaces_comercio = new XmlSerializerNamespaces();
                    myNamespaces_comercio.Add("pago20", "http://www.sat.gob.mx/Pagos20");


                    using (XmlWriter writer = doc.CreateNavigator().AppendChild())
                    {
                        new XmlSerializer(oPagos.GetType()).Serialize(writer, oPagos, myNamespaces_comercio);
                    }
                    return doc.DocumentElement;
                }
                catch (Exception exCCE)
                {
                    ErrorFX.mostrar(exCCE, true, true, "Generar40 - 2638 - complementoPago20", true);
                    return null;
                }
            }
            catch (Exception exCCE)
            {
                ErrorFX.mostrar(exCCE, true, true, "Generar40 - 1602 - complementoPago20", true);
                return null;
            }
        }

        private void CorrecionDecimalesV2(Pago20.Pagos oPagos)
        {
            try
            {
                decimal sumaBaseDR = 0;
                decimal sumaImporteDR = 0;

                foreach (var documentoRelacionado in oPagos.Pago[0].DoctoRelacionado)
                {
                    try
                    {
                        decimal baseDRSum = documentoRelacionado.ImpuestosDR.TrasladosDR.Sum(t => t.BaseDR);
                        decimal importeDRSum = documentoRelacionado.ImpuestosDR.TrasladosDR.Sum(t => t.ImporteDR);

                        if (documentoRelacionado.EquivalenciaDRSpecified)
                        {
                            baseDRSum = Math.Round(baseDRSum / documentoRelacionado.EquivalenciaDR, 6, MidpointRounding.AwayFromZero);
                            importeDRSum = Math.Round(importeDRSum / documentoRelacionado.EquivalenciaDR, 6, MidpointRounding.AwayFromZero);
                        }

                        sumaBaseDR += baseDRSum;
                        sumaImporteDR += importeDRSum;
                    }
                    catch
                    {
                        // Log error or handle exception as needed
                    }
                }

                if (oPagos.Pago[0].ImpuestosP.TrasladosP.Length > 0)
                {
                    oPagos.Pago[0].ImpuestosP.TrasladosP[0].BaseP = Math.Round(sumaBaseDR, 2, MidpointRounding.AwayFromZero);
                    oPagos.Pago[0].ImpuestosP.TrasladosP[0].ImporteP = Math.Round(sumaImporteDR, 2, MidpointRounding.AwayFromZero);
                }

                decimal montoTotal = sumaBaseDR + sumaImporteDR;
                oPagos.Pago[0].Monto = montoTotal;

                if (sumaImporteDR == 0)
                {
                    decimal montoTotalPagos = Math.Round(sumaBaseDR * oPagos.Pago[0].TipoCambioP, 2, MidpointRounding.AwayFromZero);
                    oPagos.Totales.TotalTrasladosBaseIVA0 = montoTotalPagos;
                    oPagos.Totales.MontoTotalPagos = montoTotalPagos;
                    oPagos.Totales.TotalTrasladosImpuestoIVA16Specified = false;
                    oPagos.Totales.TotalTrasladosBaseIVA16Specified = false;
                }
                else
                {
                    decimal montoTotalPagos = Math.Round(montoTotal * oPagos.Pago[0].TipoCambioP, 2, MidpointRounding.AwayFromZero);
                    decimal totalTrasladosBaseIVA16 = Math.Round(sumaBaseDR * oPagos.Pago[0].TipoCambioP, 2, MidpointRounding.AwayFromZero);
                    decimal totalTrasladosImpuestoIVA16 = Math.Round(sumaImporteDR * oPagos.Pago[0].TipoCambioP, 2, MidpointRounding.AwayFromZero);

                    oPagos.Totales.TotalTrasladosBaseIVA16 = totalTrasladosBaseIVA16;
                    oPagos.Totales.TotalTrasladosImpuestoIVA16 = totalTrasladosImpuestoIVA16;
                    oPagos.Totales.MontoTotalPagos = montoTotalPagos;

                    oPagos.Totales.TotalTrasladosImpuestoIVA16Specified = true;
                    oPagos.Totales.TotalTrasladosBaseIVA16Specified = true;
                    oPagos.Totales.TotalTrasladosBaseIVA0Specified = false;
                    oPagos.Totales.TotalTrasladosImpuestoIVA0Specified = false;
                }
            }
            catch (Exception ex)
            {
                // Log the exception or handle it as needed
            }
        }


        private void CorrecionDecimales(Pago20.Pagos oPagos)
        {
            try
            {
                //Rodrigo Escalona: 22/06/2023 Corregir BaseP
                //Sumar todos BaseDR
                decimal SumaBaseDR = 0;
                decimal SumaImporteDR = 0;
                foreach (Pago20.PagosPagoDoctoRelacionado documentoRelacionado in oPagos.Pago[0].DoctoRelacionado)
                {
                    try
                    {
                        decimal TrasladosDRTr = documentoRelacionado.ImpuestosDR.TrasladosDR.Sum(a => a.BaseDR);
                        if (documentoRelacionado.EquivalenciaDRSpecified)
                        {
                            TrasladosDRTr = Math.Round(documentoRelacionado.ImpuestosDR.TrasladosDR.Sum(a => a.BaseDR) / documentoRelacionado.EquivalenciaDR
                                , 6, MidpointRounding.AwayFromZero);
                        }
                        SumaBaseDR += TrasladosDRTr;
                    }
                    catch
                    {

                    }

                    try
                    {
                        decimal TrasladosDRTr = documentoRelacionado.ImpuestosDR.TrasladosDR.Sum(a => a.ImporteDR);
                        if (documentoRelacionado.EquivalenciaDRSpecified)
                        {
                            TrasladosDRTr = Math.Round(documentoRelacionado.ImpuestosDR.TrasladosDR.Sum(a => a.ImporteDR) / documentoRelacionado.EquivalenciaDR
                                , 6, MidpointRounding.AwayFromZero);
                        }
                        SumaImporteDR += TrasladosDRTr;
                    }
                    catch
                    {

                    }

                }
                if (oPagos.Pago[0].ImpuestosP.TrasladosP.Length > 0)
                {
                    //Rodrigo EScalona: 14/09/2023 Solucoin es ponerlo en 2 decimales estos totales
                    oPagos.Pago[0].ImpuestosP.TrasladosP[0].BaseP = Math.Round(SumaBaseDR, 2, MidpointRounding.AwayFromZero); //SumaBaseDR;
                    oPagos.Pago[0].ImpuestosP.TrasladosP[0].ImporteP = Math.Round(SumaImporteDR, 2, MidpointRounding.AwayFromZero); //SumaImporteDR;

                }



                //if(oPagos.Pago[0].Monto!= SumaBaseDR)
                //{
                    
                    decimal Monto = Math.Round(SumaBaseDR, 2, MidpointRounding.AwayFromZero) +
                    Math.Round(SumaImporteDR, 2, MidpointRounding.AwayFromZero);
                    oPagos.Pago[0].Monto = Monto;
                    //MessageBox.Show(SumaImporteDR.ToString());
                    if (SumaImporteDR == 0)
                    {
                        //if (oPagos.Totales.TotalTrasladosBaseIVA0 != 0)
                        //{
                            decimal MontoTotalPagos = Math.Round(SumaBaseDR * oPagos.Pago[0].TipoCambioP, 2, MidpointRounding.AwayFromZero);
                            oPagos.Totales.TotalTrasladosBaseIVA0 = MontoTotalPagos;
                            oPagos.Totales.MontoTotalPagos = MontoTotalPagos;

                            oPagos.Totales.TotalTrasladosImpuestoIVA16Specified = false;
                            oPagos.Totales.TotalTrasladosBaseIVA16Specified = false;
                        //}
                    }
                    else
                    {
                        //if (oPagos.Totales.TotalTrasladosBaseIVA16 != 0)
                        //{
                            oPagos.Totales.TotalTrasladosImpuestoIVA0Specified = false;
                            oPagos.Totales.TotalTrasladosBaseIVA0Specified = false;
                            oPagos.Totales.TotalTrasladosBaseIVA0 = 0;
                            oPagos.Totales.TotalTrasladosImpuestoIVA0 = 0;


                            oPagos.Pago[0].Monto = Math.Round(Monto, 2, MidpointRounding.AwayFromZero);
                            decimal MontoTotalPagos = Math.Round(Monto * oPagos.Pago[0].TipoCambioP, 2, MidpointRounding.AwayFromZero);
                            decimal TotalTrasladosBaseIVA16 = Math.Round(SumaBaseDR * oPagos.Pago[0].TipoCambioP, 2, MidpointRounding.ToEven);
                            decimal TotalTrasladosImpuestoIVA16 = Math.Round(SumaImporteDR * oPagos.Pago[0].TipoCambioP, 2, MidpointRounding.AwayFromZero);
                            oPagos.Totales.TotalTrasladosBaseIVA16 = TotalTrasladosBaseIVA16;
                            oPagos.Totales.TotalTrasladosImpuestoIVA16 = TotalTrasladosImpuestoIVA16;
                            oPagos.Totales.MontoTotalPagos = MontoTotalPagos;

                            oPagos.Totales.TotalTrasladosImpuestoIVA16Specified = true;
                            oPagos.Totales.TotalTrasladosBaseIVA16Specified = true;
                        //}
                    }

                    
                //}


            }
            catch(Exception Error)
            {

            }
            


        }
    }


}
