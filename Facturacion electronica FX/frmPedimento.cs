﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;

using System.Text;
using System.Windows.Forms;
using Generales;

namespace FE_FX
{
    public partial class frmPedimento : Form
    {
        private cCONEXCION oData = new cCONEXCION("");
        private cEMPRESA ocEMPRESA;
        private cPEDIMENTO oObjeto;
        private string INVOICE_ID;
        private bool modificado;

        public frmPedimento(string sConn,string INVOICE_IDp,string Dummy,Object ocEMPRESAp)
        {
            ocEMPRESA = (cEMPRESA)ocEMPRESAp;
            oData.sConn = sConn;
            INVOICE_ID = INVOICE_IDp;
            oObjeto = new cPEDIMENTO(oData, ocEMPRESA);
            InitializeComponent();
            ajax_loader.Visible = false;
            cargar_lineas();
        }
        public frmPedimento(string sConn, string ROW_IDp, Object ocEMPRESAp)
        {
            cEMPRESA oEMPRESA = (cEMPRESA)ocEMPRESAp;
            modificado = false;
            oData.sConn = sConn;
            oObjeto = new cPEDIMENTO(oData, oEMPRESA);

            InitializeComponent();

            limpiar();
            oObjeto.ROW_ID = ROW_IDp;
            cargar();
        }
        public void cargar_lineas()
        {
            string sSQL = "SELECT LINE_NO FROM RECEIVABLE_LINE WHERE INVOICE_ID='" +  INVOICE_ID + "'";
            DataTable oDataTable = oData.EjecutarConsulta(sSQL);
            foreach (DataRow oDataRow in oDataTable.Rows)
            {
                LINE_NO.Items.Add(oDataRow["LINE_NO"].ToString());
            }
            if (LINE_NO.SelectedIndex > 0)
            {
                LINE_NO.SelectedIndex = 0;
            }
        }
        public void limpiar()
        {
            if (modificado)
            {

                DialogResult Resultado = MessageBox.Show("¿Desea guardar los cambios?", Application.ProductName + "-" + Application.ProductVersion, MessageBoxButtons.YesNo);
                if (Resultado.ToString() == "Yes")
                {
                    guardar();
                }


            }
            modificado = false;
            oObjeto = new cPEDIMENTO(oData, ocEMPRESA);
            PEDIMENTO.Text = "";
            ADUANA.Text = "";
        }

        private void guardar()
        {
            ajax_loader.Visible = true;
            oObjeto.INVOICE_ID = INVOICE_ID;
            oObjeto.LINE_NO = LINE_NO.Text;
            oObjeto.PEDIMENTO = PEDIMENTO.Text;
            oObjeto.ADUANA = ADUANA.Text;
            oObjeto.FECHA = FECHA.Text;
            if (oObjeto.guardar())
            {
                toolStripStatusLabel1.Text = "Datos Actualizados " + DateTime.Now.ToShortDateString() + " " + DateTime.Now.ToLongTimeString();
                modificado = false;
            }
            ajax_loader.Visible = false;
        }

        private void cargar()
        {
            ajax_loader.Visible = true;
            if (oObjeto.cargar(oObjeto.ROW_ID))
            {
                INVOICE_ID = oObjeto.INVOICE_ID;
                PEDIMENTO.Text = oObjeto.PEDIMENTO;
                ADUANA.Text = oObjeto.ADUANA;
                FECHA.Text = oObjeto.FECHA;
                LINE_NO.Text = oObjeto.LINE_NO;
                toolStripStatusLabel1.Text  = "Datos Cargados " + DateTime.Now.ToShortDateString() + " " + DateTime.Now.ToLongTimeString();
                modificado = false;

            }
            ajax_loader.Visible = false;
        }

        private void button3_Click(object sender, EventArgs e)
        {
            guardar();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            limpiar();
        }

        private void frmUsuario_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (modificado)
            {
                DialogResult Resultado = MessageBox.Show("¿Desea guardar los cambios?", Application.ProductName + "-" + Application.ProductVersion, MessageBoxButtons.YesNo);
                if (Resultado.ToString() == "Yes")
                {
                    guardar();
                }
            }
        }

        private void frmUsuario_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)27)
            {
                this.Close();
            }
            else
            {
                modificado = true;
                if ((e.KeyChar.ToString() == "<")
                                | (e.KeyChar.ToString() == ">")
                                
                                | (e.KeyChar.ToString() == "'")
                                | (e.KeyChar.ToString() == "#")
                                )
                {
                    e.Handled = true;
                }
            }
        }

        private void button7_Click(object sender, EventArgs e)
        {
            eliminar();
        }

        private void eliminar()
        {
            DialogResult Resultado = MessageBox.Show("¿Esta seguro de eliminar?", Application.ProductName + "-" + Application.ProductVersion, MessageBoxButtons.YesNo);
            if (Resultado.ToString() != "Yes")
            {
                return;
            }
            if (oObjeto.eliminar(oObjeto.ROW_ID))
            {
                limpiar();
            }
        }

    }
}
