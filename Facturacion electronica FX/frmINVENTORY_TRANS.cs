﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;

using System.Text;
using System.Windows.Forms;
using Generales;

namespace FE_FX
{
    public partial class frmINVENTORY_TRANS : Form
    {
        private cCONEXCION oData = new cCONEXCION("");
        string devolver = "";
        public DataGridViewSelectedRowCollection selecionados;

        public frmINVENTORY_TRANS(string sConn, string devolverp = "")
        {
            oData.sConn = sConn;
            devolver = devolverp;
            InitializeComponent();
            ajax_loader.Visible = false;
        }

        private void cargar()
        {
            ajax_loader.Visible = true;            
            toolStripStatusLabel1.Text = "Cargando ";

            string sSQL = @"SELECT TOP 20 
            i.TRANSACTION_ID as 'Transacción',i.PART_ID as 'Producto',p.DESCRIPTION as 'Descripción',i.QTY as 'Cantidad',i.TRANSACTION_DATE as 'Fecha'
            ,i.TYPE as 'Tipo',i.CLASS as 'Clase'
            ,p.STOCK_UM as 'UM', p.PRODUCT_CODE 
            ,i.WAREHOUSE_ID, i.LOCATION_ID
            , p.USER_6
            , p.USER_1 as 'ClaveServProd'
            FROM inventory_trans i
            INNER JOIN part p ON i.PART_ID=p.ID
            WHERE i.TRANSACTION_ID LIKE '%" + BUSCAR.Text + "%' OR i.PART_ID LIKE '%" + BUSCAR.Text + @"%' OR p.DESCRIPTION LIKE '%" + BUSCAR.Text + @"%'
            ORDER BY TRANSACTION_DATE DESC";
            DataTable oDataTable = oData.EjecutarConsulta(sSQL);
            dtgrdGeneral.DataSource = oDataTable;
            toolStripStatusLabel1.Text = "Total " + dtgrdGeneral.Rows.Count.ToString();
            ajax_loader.Visible = false;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            cargar();
        }

        private void BUSCAR_KeyPress(object sender, KeyPressEventArgs e)
        {
            //Precionar Enter buscar
            if (e.KeyChar.ToString() == "\r")
            {
                cargar();
            }
            
        }

        private void frmUsuarios_Load(object sender, EventArgs e)
        {
            cargar();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            frmEmpresa oObjeto = new frmEmpresa(oData.sConn);
            oObjeto.ShowDialog();
            cargar();
        }

        private void dtgrdGeneral_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            modificar();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            if (dtgrdGeneral.SelectedRows.Count > 0)
            {
                DataGridViewSelectedRowCollection arrSelectedRows = dtgrdGeneral.SelectedRows;
                string ROW_ID = (string)arrSelectedRows[0].Cells[0].Value;
                frmEmpresa oObjeto = new frmEmpresa(oData.sConn, ROW_ID);
                oObjeto.ShowDialog();
                cargar();
            }
        }

        public void modificar()
        {
            if (dtgrdGeneral.SelectedRows.Count > 0)
            {
                DataGridViewSelectedRowCollection arrSelectedRows = dtgrdGeneral.SelectedRows;
                string ROW_ID = (string)arrSelectedRows[0].Cells[0].Value;
                if (devolver == "")
                {

                    frmEmpresa oObjeto = new frmEmpresa(oData.sConn, ROW_ID);
                    oObjeto.ShowDialog();
                    cargar();
                }
                else
                {
                    selecionados = arrSelectedRows;
                    this.DialogResult = DialogResult.OK;
                }
            }
        }

        private void frmEmpresas_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)27)
            {
                this.Close();
            }
            else
            {
                if ((e.KeyChar.ToString() == "<")
                                | (e.KeyChar.ToString() == ">")
                                
                                | (e.KeyChar.ToString() == "'")
                                | (e.KeyChar.ToString() == "#")
                                )
                {
                    e.Handled = true;
                }
            }
        }

        private void button2_Click_1(object sender, EventArgs e)
        {
            seleccionar();
        }

        public DataGridViewSelectedRowCollection seleccionados;
        private void seleccionar()
        {
            if (dtgrdGeneral.SelectedRows.Count > 0)
            {
                
                seleccionados = dtgrdGeneral.SelectedRows;
                this.DialogResult = DialogResult.OK;
            }
            else
            {
                MessageBox.Show("Seleccione las transacciones a realizar el traspaso.");
            }
            
        }

        private void dtgrdGeneral_CellDoubleClick_1(object sender, DataGridViewCellEventArgs e)
        {
            seleccionar();
        }
    }
}
