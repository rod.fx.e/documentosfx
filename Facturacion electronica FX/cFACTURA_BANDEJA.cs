﻿using System;
using System.Collections.Generic;

using System.Text;
using System.Data;
using System.Windows;
using System.Windows.Forms;
using System.Configuration;
using Generales;
using System.Xml;
using System.IO;
using FE_FX.Clases;
using System.Xml.Xsl;
using ModeloDocumentosFX;
using System.Xml.Linq;

namespace FE_FX
{
    class cFACTURA_BANDEJA
    {
        private string sSQL = "";
        public string INVOICE_ID { get; set; }
        public string XML { get; set; }
        public string ESTADO { get; set; }
        public string PDF { get; set; }
        public string CREATE_DATE { get; set; }
        public string CADENA { get; set; }
        public string SELLO { get; set; }
        public string USO_CFDI { get; set; }
        public string VERSION { get; set; }

        public string SERIE { get; set; }

        public string UUID { get; set; }
        public string FechaTimbrado { get; set; }
        public string Fecha { get; set; }
        public string noCertificadoSAT { get; set; }
        public string selloSAT { get; set; }
        public string Cadena_TFD { get; set; }
        public string QR_Code { get; set; }

        public string FORMA_DE_PAGO { get; set; }
        public string CTA_BANCO { get; set; }
        public string METODO_DE_PAGO { get; set; }
        public string ADDR_NO { get; set; }
        public string PDF_ESTADO { get; set; }
        public string noCertificado { get; set; }

        private cCONEXCION oData = new cCONEXCION("");
        public string subdivision { get; set; }
        public string RegimenFiscal { get; internal set; }
        public string TotalDistRec { get; internal set; }
        public string Exportacion { get; internal set; }
        public Encapsular oEncapsular;

        public cFACTURA_BANDEJA()
        {
            ModeloDocumentosFXContainer dbContext = new ModeloDocumentosFXContainer();
            string conection = dbContext.Database.Connection.ConnectionString;
            oData = new cCONEXCION(conection);
            limpiar();
        }
        public cFACTURA_BANDEJA(cCONEXCION oDatap)
        {
            oData = oDatap;
            limpiar();
        }

        public cFACTURA_BANDEJA(cEMPRESA oEMPRESAp)
        {
            limpiar();
            string BD_Auxiliar = "";
            if (oEMPRESAp.BD_AUXILIAR != "")
            {
                BD_Auxiliar = oEMPRESAp.BD_AUXILIAR + ".dbo.";
            }

            Globales.VMX_FE_Tabla = BD_Auxiliar + "VMX_FE";
        }

        public cFACTURA_BANDEJA(cCONEXCION pData, cEMPRESA oEMPRESAp)
        {
            oData = pData;
            string BD_Auxiliar = "";
            if (oEMPRESAp.BD_AUXILIAR != "")
            {
                BD_Auxiliar = oEMPRESAp.BD_AUXILIAR + ".dbo.";
            }

            Globales.VMX_FE_Tabla = BD_Auxiliar + "VMX_FE";

            limpiar();
        }

        public void limpiar()
        {
            INVOICE_ID = "";
            XML = "";
            CREATE_DATE = "";
            ESTADO = "";
            PDF_ESTADO = "";
        }

        public List<cFACTURA_BANDEJA> todos(string BUSQUEDA, bool POR_PROCESAR, bool PROCESADOS)
        {
            string sSQL_Where = "";
            ////if (POR_PROCESAR)
            ////{
            ////    sSQL_Where += " AND (ESTADO='' OR ESTADO IS NULL) ";
            ////}
            ////if (PROCESADOS)
            ////{
            ////    sSQL_Where += " AND (ESTADO<>'' OR NOT(ESTADO IS NULL)) ";
            ////}
            List<cFACTURA_BANDEJA> lista = new List<cFACTURA_BANDEJA>();

            sSQL = " SELECT * ";
            sSQL += " FROM " + Globales.VMX_FE_Tabla + " ";
            sSQL += " WHERE " + Globales.VMX_FE_Tabla + ".INVOICE_ID LIKE '%" + BUSQUEDA + "%' ";
            sSQL += sSQL_Where;
            sSQL = Globales.actualizar_invoice_id(sSQL);

            DataTable oDataTable = oData.EjecutarConsulta(sSQL);
            foreach (DataRow oDataRow in oDataTable.Rows)
            {
                lista.Add(new cFACTURA_BANDEJA()
                {
                    INVOICE_ID = oDataRow["INVOICE_ID"].ToString()
                   ,
                    XML = oDataRow["XML"].ToString()
                   ,
                    ESTADO = oDataRow["ESTADO"].ToString()
                    ,
                    PDF = oDataRow["PDF"].ToString()
                    ,
                    CREATE_DATE = oDataRow["CREATE_DATE"].ToString()
                    ,
                    CADENA = oDataRow["CADENA"].ToString()
                    ,
                    SELLO = oDataRow["SELLO"].ToString()
                    ,
                    FORMA_DE_PAGO = oDataRow["FORMA_DE_PAGO"].ToString()
                    ,
                    ADDR_NO = oDataRow["ADDR_NO"].ToString()
                    ,
                    PDF_ESTADO = oDataRow["PDF_ESTADO"].ToString()
                    ,
                    Exportacion = oDataRow["Exportacion"].ToString()
                });
            }
            return lista;
        }

        public bool cargar(string ROW_ID)
        {
            sSQL = " SELECT * ";
            sSQL += " FROM " + Globales.VMX_FE_Tabla + " ";
            sSQL += " WHERE ROW_ID=" + ROW_ID + " ";
            DataTable oDataTable = oData.EjecutarConsulta(sSQL);
            if (oDataTable != null)
            {
                foreach (DataRow oDataRow in oDataTable.Rows)
                {
                    //Añadir el Registro
                    ROW_ID = "";
                    if (oData.sTipo == "SQLSERVER")
                    {
                        INVOICE_ID = oDataRow["INVOICE_ID"].ToString();
                        ROW_ID = oDataRow["ROW_ID"].ToString();
                    }
                    if (oData.sTipo == "ORACLE")
                    {
                        INVOICE_ID = oDataRow["ID"].ToString();
                    }
                    XML = oDataRow["XML"].ToString();
                    ESTADO = oDataRow["ESTADO"].ToString();
                    PDF = oDataRow["PDF"].ToString();
                    CREATE_DATE = oDataRow["CREATE_DATE"].ToString();
                    CADENA = oDataRow["CADENA"].ToString();
                    SELLO = oDataRow["SELLO"].ToString();
                    FORMA_DE_PAGO = oDataRow["FORMA_DE_PAGO"].ToString();
                    ADDR_NO = oDataRow["ADDR_NO"].ToString();
                    PDF_ESTADO = oDataRow["PDF_ESTADO"].ToString();
                    noCertificado = oDataRow["noCertificado"].ToString();
                    Exportacion = oDataRow["Exportacion"].ToString();
                    //Datos Bancarios 
                    cargar_banco();
                    return true;
                }
            }
            return false;
        }

        public bool cargar_banco()
        {
            //Buscar el Primer Pedido de la Factura
            //Del Pedido Buscar el BANK_ID
            sSQL = " SELECT * ";
            sSQL += " FROM " + Globales.VMX_FE_Tabla + "";
            sSQL += " WHERE " + Globales.VMX_FE_Tabla + ".INVOICE_ID = '" + INVOICE_ID + "'";
            sSQL = Globales.actualizar_invoice_id(sSQL);
            DataTable oDataTableR = oData.EjecutarConsulta(sSQL);
            if (oDataTableR != null)
            {
                foreach (DataRow oDataRow1 in oDataTableR.Rows)
                {
                    METODO_DE_PAGO = oDataRow1["METODO_DE_PAGO"].ToString();
                    CTA_BANCO = oDataRow1["CTA_BANCO"].ToString();
                    FORMA_DE_PAGO = oDataRow1["FORMA_DE_PAGO"].ToString();
                    USO_CFDI = oDataRow1["USO_CFDI"].ToString();
                    ADDR_NO = oDataRow1["ADDR_NO"].ToString();

                    return true;
                }
            }
            return false;
        }

        public bool guardar()
        {
            if (INVOICE_ID.Trim() == "")
            {
                MessageBox.Show("El Nombre es un campo requerido.", Application.ProductName + "-" + Application.ProductVersion, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return false;
            }

            if (XML.Trim() == "")
            {
                MessageBox.Show("El Archivo es un campo requerido.", Application.ProductName + "-" + Application.ProductVersion, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return false;
            }
            bool existe = false;
            sSQL = " SELECT * ";
            sSQL += " FROM " + Globales.VMX_FE_Tabla + " ";
            sSQL += " WHERE " + Globales.VMX_FE_Tabla + ".INVOICE_ID='" + INVOICE_ID + "' ";
            sSQL = Globales.actualizar_invoice_id(sSQL);
            DataTable oDataTable = oData.EjecutarConsulta(sSQL);
            if (oDataTable != null)
            {
                foreach (DataRow oDataRow in oDataTable.Rows)
                {
                    existe = true;
                }
            }
            
            if (!existe)
            {

                sSQL = " INSERT INTO " + Globales.VMX_FE_Tabla + " ";
                sSQL += " ( ";
                sSQL += " INVOICE_ID, XML,ESTADO, SERIE";
                sSQL += ",PDF, CADENA,SELLO";
                sSQL += ",METODO_DE_PAGO, CTA_BANCO,FORMA_DE_PAGO";
                sSQL += ",ADDR_NO,PDF_ESTADO, USO_CFDI, VERSION,Exportacion )";
                sSQL += " VALUES ";
                sSQL += "(";
                sSQL += " '" + INVOICE_ID + "','" + XML + "','" + ESTADO + "','" + SERIE + "'";
                sSQL += ",'" + PDF + "','" + CADENA.Replace("'","'''") + "','" + SELLO + "'";
                sSQL += ",'" + METODO_DE_PAGO + "', '" + CTA_BANCO + "', '" + FORMA_DE_PAGO + "'";
                sSQL += ", '" + ADDR_NO + "','" + PDF_ESTADO + "', '" + USO_CFDI + "','" + VERSION + "','" + Exportacion + "')";
                sSQL = Globales.actualizar_invoice_id(sSQL);
                oDataTable = oData.EjecutarConsulta(sSQL);


            }
            else
            {
                sSQL = " UPDATE " + Globales.VMX_FE_Tabla + " ";
                sSQL += " SET ";
                sSQL += " XML='" + XML + "',ESTADO='" + ESTADO + "',SERIE='" + SERIE + "'";
                sSQL += ",PDF='" + PDF + "',CADENA='" + CADENA.Replace("'", "'''") + "',SELLO='" + SELLO + "'";
                sSQL += ",METODO_DE_PAGO='" + METODO_DE_PAGO + "', CTA_BANCO='" + CTA_BANCO + "'";
                sSQL += ", FORMA_DE_PAGO='" + FORMA_DE_PAGO + "',ADDR_NO= '" + ADDR_NO + "',PDF_ESTADO='" + PDF_ESTADO + @"'
                ,USO_CFDI='" + USO_CFDI + "',VERSION= '" + VERSION + @"',Exportacion= '" + Exportacion + @"'
                 WHERE INVOICE_ID='" + INVOICE_ID + "' ";
                sSQL = Globales.actualizar_invoice_id(sSQL);
                oData.EjecutarConsulta(sSQL);

            }
            return true;

        }

        public void obtene_metodo_pago(string INVOICE_IDp)
        {
            string sSQL = "";
            sSQL = " SELECT * ";
            sSQL += " FROM " + Globales.VMX_FE_Tabla + " ";
            sSQL += " WHERE INVOICE_ID='" + INVOICE_IDp + "' ";
            sSQL = Globales.actualizar_invoice_id(sSQL);
            DataTable oDataTable = oData.EjecutarConsulta(sSQL);
            if (oDataTable != null)
            {
                foreach (DataRow oDataRow in oDataTable.Rows)
                {
                    CTA_BANCO = oDataRow["CTA_BANCO"].ToString();
                    METODO_DE_PAGO = oDataRow["METODO_DE_PAGO"].ToString();
                    FORMA_DE_PAGO = oDataRow["FORMA_DE_PAGO"].ToString();
                }
            }


        }

        public bool guardar_metodo()
        {

            if (INVOICE_ID != "")
            {
                sSQL = " SELECT  * ";
                sSQL += " FROM " + Globales.VMX_FE_Tabla + " ";
                sSQL += " WHERE " + Globales.VMX_FE_Tabla + ".INVOICE_ID='" + INVOICE_ID + "' ";
                sSQL = Globales.actualizar_invoice_id(sSQL);
                DataTable oDataTable = oData.EjecutarConsulta(sSQL);
                if (oDataTable != null)
                {
                    if (oDataTable.Rows.Count > 0)
                    {
                        sSQL = " UPDATE " + Globales.VMX_FE_Tabla + " ";
                        sSQL += " SET METODO_DE_PAGO='" + METODO_DE_PAGO + "',FORMA_DE_PAGO='" + FORMA_DE_PAGO + "',USO_CFDI='" + USO_CFDI + "' ";
                        sSQL += ", subdivision='" + subdivision + "' ";
                        sSQL += ", RegimenFiscal='" + RegimenFiscal + "', Exportacion='" + Exportacion + "'  ";                        
                        sSQL += " WHERE INVOICE_ID='" + INVOICE_ID + "' ";
                    }
                    else
                    {
                        if (String.IsNullOrEmpty(RegimenFiscal))
                        {
                            RegimenFiscal=ObtnerUltimoRegimenFiscal(INVOICE_ID, oEncapsular);
                        }
                        sSQL = " INSERT INTO " + Globales.VMX_FE_Tabla + " (" + Globales.VMX_FE_Tabla + ".INVOICE_ID, METODO_DE_PAGO,FORMA_DE_PAGO,CREATE_DATE,USO_CFDI,subdivision,RegimenFiscal,Exportacion) VALUES ";
                        sSQL += " ('" + INVOICE_ID + "', '" + METODO_DE_PAGO + "','" + FORMA_DE_PAGO + "','" + CREATE_DATE.Replace("-", "") + "','" + USO_CFDI + "','" + subdivision + "','" + RegimenFiscal + "','" + Exportacion + "') ";
                    }
                    sSQL = Globales.actualizar_invoice_id(sSQL);
                    oData.EjecutarConsulta(sSQL);
                }

                if (!String.IsNullOrEmpty(CTA_BANCO))
                {

                    sSQL = " UPDATE " + Globales.VMX_FE_Tabla + " ";
                    sSQL += " SET CTA_BANCO='" + CTA_BANCO + "' ";
                    sSQL += " WHERE INVOICE_ID='" + INVOICE_ID + "' ";
                    sSQL = Globales.actualizar_invoice_id(sSQL);
                    oData.EjecutarConsulta(sSQL);
                }

            }
            return true;

        }

        private string ObtnerUltimoRegimenFiscal(string iNVOICE_ID, Encapsular oEncapsular)
        {
            //Obtener Ultima factura que no sea la del paremotro del mismo cliente
            string RegimenFiscalTr = string.Empty;

            try
            {
                //MessageBox.Show("Generar PDF");
                //Generar el PDF
                cFACTURA oFACTURA = new cFACTURA(oEncapsular.oData_ERP);
                //MessageBox.Show("Generar Factura");
                oFACTURA.datos(INVOICE_ID, "", "", oEncapsular.ocEMPRESA, "");
                if (oFACTURA != null)
                {
                    if (oFACTURA.CUSTOMER_ID != null)
                    {
                        string Sql = "SELECT TOP 1 INVOICE_ID FROM RECEIVABLE WHERE CUSTOMER_ID='" + oFACTURA.CUSTOMER_ID + "' AND INVOICE_ID<>'" + iNVOICE_ID + "' ORDER BY INVOICE_DATE DESC";
                        DataTable oDataTableRegimen = oEncapsular.oData_ERP.EjecutarConsulta(Sql);
                        if (oDataTableRegimen != null)
                        {
                            foreach (DataRow oDataRowRegimen in oDataTableRegimen.Rows)
                            {
                                string INVOICE_IDAnterior = oDataRowRegimen["INVOICE_ID"].ToString();
                                cFACTURA_BANDEJA OcFACTURA_BANDEJARegimen = new cFACTURA_BANDEJA();
                                OcFACTURA_BANDEJARegimen.cargar_ID(INVOICE_IDAnterior, oEncapsular.ocEMPRESA);
                                RegimenFiscalTr = OcFACTURA_BANDEJARegimen.RegimenFiscal;
                            }
                        }
                    }
                }
                return "601";                
            }
            catch (Exception error)
            {
                ErrorFX.mostrar(error, true, true, "cFACTURA_BANDEJA - ObtnerUltimoRegimenFiscal ", false);
            }
            return RegimenFiscalTr;
        }

        public bool actualizar_estado(string IDp, string ESTADOp)
        {

            if (IDp != "")
            {
                sSQL = " UPDATE " + Globales.VMX_FE_Tabla + " ";
                sSQL += " SET ESTADO='" + ESTADOp + "' ";
                sSQL += " WHERE INVOICE_ID='" + IDp + "' ";
                sSQL = Globales.actualizar_invoice_id(sSQL);
                oData.EjecutarConsulta(sSQL);

            }
            return true;

        }
        public bool actualizar_PDF_ESTADO(string IDp, string PDF_ESTADOp)
        {

            if (IDp != "")
            {
                sSQL = " UPDATE " + Globales.VMX_FE_Tabla + " ";
                sSQL += " SET PDF_ESTADO='" + PDF_ESTADOp + "' ";
                sSQL += " WHERE INVOICE_ID='" + IDp + "' ";
                sSQL = Globales.actualizar_invoice_id(sSQL);
                oData.EjecutarConsulta(sSQL);

            }
            return true;

        }
        public bool actualizar_archivo_destino(string IDp, string ARCHIVO_DESTINOp)
        {

            if (IDp != "")
            {
                sSQL = " INSERT INTO " + Globales.VMX_FE_Tabla + " ";
                sSQL += " SET ARCHIVO_DESTINO='" + ARCHIVO_DESTINOp + "' ";
                sSQL += " WHERE INVOICE_ID='" + IDp + "' ";
                sSQL = Globales.actualizar_invoice_id(sSQL);
                oData.EjecutarConsulta(sSQL);
            }
            return true;

        }

        public bool cargar_ID(string IDp, cEMPRESA ocEMPRESAp)
        {

            string BD_Auxiliar = "";
            if (ocEMPRESAp.BD_AUXILIAR != "")
            {
                BD_Auxiliar = ocEMPRESAp.BD_AUXILIAR + ".dbo.";
            }

            Globales.VMX_FE_Tabla = BD_Auxiliar + "VMX_FE";

            sSQL = " SELECT  * ";
            sSQL += " FROM " + Globales.VMX_FE_Tabla + " ";
            sSQL += " WHERE INVOICE_ID='" + IDp + "' ";


            sSQL = Globales.actualizar_invoice_id(sSQL);
            DataTable oDataTable = oData.EjecutarConsulta(sSQL);
            if (oDataTable != null)
            {
                foreach (DataRow oDataRow in oDataTable.Rows)
                {
                    //Añadir el Registro
                    if (oData.sTipo == "SQLSERVER")
                    {
                        INVOICE_ID = oDataRow["INVOICE_ID"].ToString();
                    }
                    if (oData.sTipo == "ORACLE")
                    {
                        INVOICE_ID = oDataRow["ID"].ToString();
                    }

                    XML = oDataRow["XML"].ToString();
                    ESTADO = oDataRow["ESTADO"].ToString();
                    PDF = oDataRow["PDF"].ToString();
                    CREATE_DATE = oDataRow["CREATE_DATE"].ToString();
                    CADENA = oDataRow["CADENA"].ToString();
                    SELLO = oDataRow["SELLO"].ToString();
                    UUID = oDataRow["UUID"].ToString();
                    SERIE = oDataRow["SERIE"].ToString();
                    VERSION = oDataRow["VERSION"].ToString();
                   
                    noCertificado = oDataRow["noCertificado"].ToString();
                    cargar_banco();

                    USO_CFDI = oDataRow["USO_CFDI"].ToString();
                    METODO_DE_PAGO = oDataRow["METODO_DE_PAGO"].ToString();
                    FORMA_DE_PAGO = oDataRow["FORMA_DE_PAGO"].ToString();
                    try
                    {
                        RegimenFiscal = oDataRow["RegimenFiscal"].ToString();
                    }
                    catch
                    {

                    }

                    try
                    {
                        Exportacion = oDataRow["Exportacion"].ToString();
                    }
                    catch
                    {

                    }
                    return true;
                }
            }
            return false;
        }

        bool Recordar = false;
       
        public void actualizar_datos_xml(string xml, cFACTURA oFACTURAp, Encapsular oEncapsular)
        {
            try
            {
                if (!File.Exists(xml))
                {
                    if (Recordar)
                    {
                        return;
                    }

                    MessageBox.Show("El XML " + xml + " no existe");
                    //Desea 
                    DialogResult Resultado = MessageBox.Show("¿Esta aplicar lo mismo para las demas preguntas del XML?"
                        , Application.ProductName + "-" + Application.ProductVersion, MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                    if (Resultado== DialogResult.Yes)
                    {
                        Recordar = true;
                    }
                    return;
                }
                //Abrir el archivo y copiar los sellos
                XmlDocument doc = new XmlDocument();
                doc.Load(xml);
                if (oFACTURAp.INVOICE_ID != "")
                {
                    cFACTURA_BANDEJA ocFACTURA_BANDEJA = new cFACTURA_BANDEJA(oEncapsular.oData_ERP, oEncapsular.ocEMPRESA);
                    ocFACTURA_BANDEJA.cargar_ID(oFACTURAp.INVOICE_ID, oEncapsular.ocEMPRESA);
                    doc.Save(ocFACTURA_BANDEJA.XML);
                }

                XmlNodeList TimbreFiscalDigital = doc.GetElementsByTagName("tfd:TimbreFiscalDigital");
                XmlNodeList receptor = doc.GetElementsByTagName("cfdi:Receptor");
                XmlNodeList emisor = doc.GetElementsByTagName("cfdi:Emisor");
                XmlNodeList comprobante = doc.GetElementsByTagName("cfdi:Comprobante");

                if (TimbreFiscalDigital==null)
                {
                    MessageBox.Show("cFacturaBandeja - El XML no esta timbrado");
                    return;
                }

                string Sello = "";

                try
                {
                    Sello = comprobante[0].Attributes["Sello"].Value;
                }
                catch
                {

                }

                string Fecha = "";

                try
                {
                    Fecha = comprobante[0].Attributes["Fecha"].Value;
                }
                catch
                {

                }

                string rfcReceptor = "";

                try
                {
                    rfcReceptor = receptor[0].Attributes["Rfc"].Value;
                }
                catch
                {

                }

                string rfcEmisor = "";

                try
                {
                    rfcEmisor = emisor[0].Attributes["Rfc"].Value;
                }
                catch
                {

                }

                string total = "";

                try
                {
                    total = comprobante[0].Attributes["Total"].Value;
                }
                catch
                {

                }

                string UUID = "";

                try
                {
                    UUID = TimbreFiscalDigital[0].Attributes["UUID"].Value;
                }
                catch
                {

                }
                string FechaTimbrado = "";

                try
                {
                    FechaTimbrado = TimbreFiscalDigital[0].Attributes["FechaTimbrado"].Value;
                }
                catch
                {

                }

                string noCertificadoSAT = "";
                try
                {
                    noCertificadoSAT = TimbreFiscalDigital[0].Attributes["noCertificadoSAT"].Value;
                }
                catch
                {

                }


                try
                {
                    noCertificadoSAT = TimbreFiscalDigital[0].Attributes["NoCertificadoSAT"].Value;
                }
                catch
                {

                }

                string selloSAT = "";
                try
                {
                    selloSAT = TimbreFiscalDigital[0].Attributes["selloSAT"].Value;
                }
                catch
                {

                }

                try
                {
                    selloSAT = TimbreFiscalDigital[0].Attributes["SelloSAT"].Value;
                }
                catch
                {

                }

                string USO_CFDI = "";

                try
                {
                    USO_CFDI = receptor[0].Attributes["UsoCFDI"].Value;
                }
                catch
                {

                }

                try
                {
                    USO_CFDI = receptor[0].Attributes["UsoCFDI"].Value;
                }
                catch
                {

                }


                string VERSION = "";

                try
                {
                    VERSION = comprobante[0].Attributes["version"].Value;
                }
                catch
                {

                }

                try
                {
                    VERSION = comprobante[0].Attributes["Version"].Value;
                }
                catch
                {

                }

                string metodo_pago = "";

                try
                {
                    metodo_pago = comprobante[0].Attributes["MetodoPago"].Value;
                }
                catch
                {

                }

                try
                {
                    metodo_pago = comprobante[0].Attributes["MetodoPago"].Value;
                }
                catch
                {

                }
                string banco = "";
                try
                {
                    banco = comprobante[0].Attributes["NumCtaPago"].Value;
                }
                catch
                {

                }

                try
                {
                    banco = comprobante[0].Attributes["NumCtaPago"].Value;
                }
                catch
                {

                }


                string FormaPago = "";

                try
                {
                    FormaPago = comprobante[0].Attributes["FormaPago"].Value;
                }
                catch
                {

                }

                try
                {
                    FormaPago = comprobante[0].Attributes["formaPago"].Value;
                }
                catch
                {

                }
                string NoCertificado = "";
                try
                {
                    NoCertificado = comprobante[0].Attributes["NoCertificado"].Value;
                }
                catch
                {

                }
                //Abrir Archivo TXT
                string cadenaoriginal_TFD_1_0 = "";
                //Crear Archivo Temporal
                if (TimbreFiscalDigital != null)
                {
                    string sDirectory = AppDomain.CurrentDomain.BaseDirectory;
                    //Generar cadenaoriginal_TFD_1_0.xslt
                    string nombre_tmp = oFACTURAp.INVOICE_ID + "TMP" + DateTime.Now.ToString("ddMMyyyyhhmmss");
                    //XslTransform myXslTrans = new XslTransform();
                    XslCompiledTransform myXslTrans = new XslCompiledTransform();


                    XslCompiledTransform trans = new XslCompiledTransform();
                    XmlUrlResolver resolver = new XmlUrlResolver();

                    resolver.Credentials = System.Net.CredentialCache.DefaultCredentials;
                    XsltSettings settings = new XsltSettings(true, true);

                    //Cargar xslt
                    try
                    {

                        myXslTrans.Load(sDirectory + "/XSLT/cadenaoriginal_TFD_1_1.xslt");
                    }
                    catch (XmlException errores)
                    {
                        MessageBox.Show("Error en " + errores.InnerException.ToString());
                        return;
                    }
                    XmlTextWriter myWriter = new XmlTextWriter(nombre_tmp + ".TXT", null);

                    //Transformar al XML
                    myXslTrans.Transform(TimbreFiscalDigital[0], null, myWriter);

                    myWriter.Close();


                    StreamReader re = File.OpenText(nombre_tmp + ".TXT");
                    string input = null;
                    while ((input = re.ReadLine()) != null)
                    {
                        cadenaoriginal_TFD_1_0 += input;
                    }
                    re.Close();
                    re.Dispose();
                    //Eliminar Archivos Temporales
                    File.Delete(nombre_tmp + ".TXT");
                }
                else
                {
                    ErrorFX.mostrar("El documento " + INVOICE_ID + " no esta timbrado, la cadena no fue generada.",
                        true, true, true);
                }



                //http://www.facturando.mx/blog/index.php/2017/01/26/nuevo-codigo-de-barras-bidimensional-cbb-para-el-cfdi-version-3-3/
                //https://verificacfdi.facturaelectronica.sat.gob.mx/default.aspx&id=F7C0E3BC-B09D-482F-881E-3F6B063DED31&re=AAA010101AAA&rr=XXX010101XXA&tt=125.6&fe=A1345678

                string QR_Code = "https://verificacfdi.facturaelectronica.sat.gob.mx/default.aspx&";
                QR_Code = "https://verificacfdi.facturaelectronica.sat.gob.mx/default.aspx"
                    + "?id=" + UUID
                    + "&re=" + rfcEmisor
                    + "&rr=" + rfcReceptor
                    + "&tt=" + total
                    + "&fe=" + Sello.Substring(Sello.Length - 8, 8);

                string BD_Auxiliar = oEncapsular.ocEMPRESA.BD_AUXILIAR + ".dbo.";
                if (oEncapsular.ocEMPRESA.BD_AUXILIAR == "")
                {
                    BD_Auxiliar = "";
                }
                string sSQL = " UPDATE " + Globales.VMX_FE_Tabla + " ";
                sSQL += " SET UUID='" + UUID + "',FechaTimbrado='" + FechaTimbrado + "'";
                sSQL += ",noCertificadoSAT='" + noCertificadoSAT + "',selloSAT='" + selloSAT + "'";
                sSQL += ",Cadena_TFD='" + cadenaoriginal_TFD_1_0 + "',QR_Code='" + QR_Code +
                @"',METODO_DE_PAGO='" + metodo_pago + "',FORMA_DE_PAGO='" + FormaPago + "',CTA_BANCO='" + banco +
                @"',noCertificado='" + NoCertificado + "', USO_CFDI='" + USO_CFDI + @"', VERSION='" + VERSION + @"' 
                , Fecha='" + Fecha + "'";
                sSQL += " WHERE INVOICE_ID='" + oFACTURAp.INVOICE_ID + "' ";
                sSQL = Globales.actualizar_invoice_id(sSQL);
                oEncapsular.oData_ERP.EjecutarConsulta(sSQL);


            }
            catch (XmlException error)
            {
                ErrorFX.mostrar(error, true, true, "cFACTURA_BANDEJA - 744 ", false);
            }

        }

        public List<string> Errores = new List<string>();
        public void Migrar(string xml, Encapsular OEncapsular, string formato)
        {
            try
            {
                //oData = OEncapsular.oData_ERP;
                Errores = new List<string>();
                if (!File.Exists(xml))
                {
                    return;
                }
                //Abrir el archivo y copiar los sellos
                XmlDocument doc = new XmlDocument();
                doc.Load(xml);
                
                XmlNodeList TimbreFiscalDigital = doc.GetElementsByTagName("tfd:TimbreFiscalDigital");
                XmlNodeList receptor = doc.GetElementsByTagName("cfdi:Receptor");
                XmlNodeList emisor = doc.GetElementsByTagName("cfdi:Emisor");
                XmlNodeList comprobante = doc.GetElementsByTagName("cfdi:Comprobante");

                if (TimbreFiscalDigital == null)
                {
                    Errores.Add("830 - Migrar - cFacturaBandeja - El TimbreFiscalDigital esta en nulo");
                    return;
                }

                if (receptor == null)
                {
                    Errores.Add("836 - Migrar - cFacturaBandeja - Receptor esta en nulo");
                    return;
                }

                if (emisor == null)
                {
                    Errores.Add("844 - Migrar - cFacturaBandeja - Emisor esta en nulo");
                    return;
                }

                if (comprobante == null)
                {
                    Errores.Add("849 - Migrar - cFacturaBandeja - Comprobante esta en nulo");
                    return;
                }

                string Sello = "";

                try
                {
                    Sello = comprobante[0].Attributes["Sello"].Value;
                }
                catch
                {

                }

                string Fecha = "";

                try
                {
                    Fecha = comprobante[0].Attributes["Fecha"].Value;
                }
                catch
                {

                }

                string rfcReceptor = "";

                try
                {
                    rfcReceptor = receptor[0].Attributes["Rfc"].Value;
                }
                catch
                {

                }

                string rfcEmisor = "";

                try
                {
                    rfcEmisor = emisor[0].Attributes["Rfc"].Value;
                }
                catch
                {

                }

                string total = "";

                try
                {
                    total = comprobante[0].Attributes["Total"].Value;
                }
                catch
                {

                }

                string UUID = "";

                try
                {
                    UUID = TimbreFiscalDigital[0].Attributes["UUID"].Value;
                }
                catch
                {

                }
                string FechaTimbrado = "";

                try
                {
                    FechaTimbrado = TimbreFiscalDigital[0].Attributes["FechaTimbrado"].Value;
                }
                catch
                {

                }

                string noCertificadoSAT = "";
                try
                {
                    noCertificadoSAT = TimbreFiscalDigital[0].Attributes["noCertificadoSAT"].Value;
                }
                catch
                {

                }


                try
                {
                    noCertificadoSAT = TimbreFiscalDigital[0].Attributes["NoCertificadoSAT"].Value;
                }
                catch
                {

                }

                string selloSAT = "";
                try
                {
                    selloSAT = TimbreFiscalDigital[0].Attributes["selloSAT"].Value;
                }
                catch
                {

                }

                try
                {
                    selloSAT = TimbreFiscalDigital[0].Attributes["SelloSAT"].Value;
                }
                catch
                {

                }

                string USO_CFDI = "";

                try
                {
                    USO_CFDI = receptor[0].Attributes["UsoCFDI"].Value;
                }
                catch
                {

                }

                try
                {
                    USO_CFDI = receptor[0].Attributes["UsoCFDI"].Value;
                }
                catch
                {

                }


                string VERSION = "";

                try
                {
                    VERSION = comprobante[0].Attributes["version"].Value;
                }
                catch
                {

                }

                try
                {
                    VERSION = comprobante[0].Attributes["Version"].Value;
                }
                catch
                {

                }

                string metodo_pago = "";

                try
                {
                    metodo_pago = comprobante[0].Attributes["MetodoPago"].Value;
                }
                catch
                {

                }

                try
                {
                    metodo_pago = comprobante[0].Attributes["MetodoPago"].Value;
                }
                catch
                {

                }
                string banco = "";
                try
                {
                    banco = comprobante[0].Attributes["NumCtaPago"].Value;
                }
                catch
                {

                }

                try
                {
                    banco = comprobante[0].Attributes["NumCtaPago"].Value;
                }
                catch
                {

                }


                string FormaPago = "";

                try
                {
                    FormaPago = comprobante[0].Attributes["FormaPago"].Value;
                }
                catch
                {

                }

                try
                {
                    FormaPago = comprobante[0].Attributes["formaPago"].Value;
                }
                catch
                {

                }
                string NoCertificado = "";
                try
                {
                    NoCertificado = comprobante[0].Attributes["NoCertificado"].Value;
                }
                catch
                {

                }

                string InvoiceId = "";
                string SerieTr = "";
                string FolioTr = "";
                try
                {
                    SerieTr = comprobante[0].Attributes["Serie"].Value;
                    FolioTr = comprobante[0].Attributes["Folio"].Value;
                    //if (SerieTr.Equals("S22"))
                    //{
                    //    SerieTr = SerieTr.Replace("22","");
                    //}
                    //if (SerieTr.Equals("S23"))
                    //{
                    //    SerieTr = SerieTr.Replace("23", "");
                    //}
                    //if (!Serie.Equals(SerieTr))
                    //{
                    //    Errores.Add("1101- La serie " + Serie + " no es igual a la serie a migrar " + SerieTr);
                    //    return;
                    //}
                    InvoiceId = SerieTr + FolioTr;

                }
                catch
                {

                }

                //Abrir Archivo TXT
                string cadenaoriginal_TFD_1_0 = "";
                //Crear Archivo Temporal
                if (TimbreFiscalDigital != null)
                {
                    string sDirectory = AppDomain.CurrentDomain.BaseDirectory;
                    //Generar cadenaoriginal_TFD_1_0.xslt
                    string nombre_tmp = InvoiceId + "TMP" + DateTime.Now.ToString("ddMMyyyyhhmmss");
                    //XslTransform myXslTrans = new XslTransform();
                    XslCompiledTransform myXslTrans = new XslCompiledTransform();


                    XslCompiledTransform trans = new XslCompiledTransform();
                    XmlUrlResolver resolver = new XmlUrlResolver();

                    resolver.Credentials = System.Net.CredentialCache.DefaultCredentials;
                    XsltSettings settings = new XsltSettings(true, true);

                    //Cargar xslt
                    try
                    {

                        myXslTrans.Load(sDirectory + "/XSLT/cadenaoriginal_TFD_1_1.xslt");
                    }
                    catch (XmlException errores)
                    {
                        MessageBox.Show("Error en " + errores.InnerException.ToString());
                        return;
                    }
                    XmlTextWriter myWriter = new XmlTextWriter(nombre_tmp + ".TXT", null);

                    //Transformar al XML
                    myXslTrans.Transform(TimbreFiscalDigital[0], null, myWriter);

                    myWriter.Close();


                    StreamReader re = File.OpenText(nombre_tmp + ".TXT");
                    string input = null;
                    while ((input = re.ReadLine()) != null)
                    {
                        cadenaoriginal_TFD_1_0 += input;
                    }
                    re.Close();
                    re.Dispose();
                    //Eliminar Archivos Temporales
                    File.Delete(nombre_tmp + ".TXT");
                }
                else
                {
                    ErrorFX.mostrar("El documento " + INVOICE_ID + " no esta timbrado, la cadena no fue generada.",
                        true, true, true);
                }

                //http://www.facturando.mx/blog/index.php/2017/01/26/nuevo-codigo-de-barras-bidimensional-cbb-para-el-cfdi-version-3-3/
                //https://verificacfdi.facturaelectronica.sat.gob.mx/default.aspx&id=F7C0E3BC-B09D-482F-881E-3F6B063DED31&re=AAA010101AAA&rr=XXX010101XXA&tt=125.6&fe=A1345678

                string QR_Code = "https://verificacfdi.facturaelectronica.sat.gob.mx/default.aspx&";
                QR_Code = "https://verificacfdi.facturaelectronica.sat.gob.mx/default.aspx"
                    + "?id=" + UUID
                    + "&re=" + rfcEmisor
                    + "&rr=" + rfcReceptor
                    + "&tt=" + total
                    + "&fe=" + Sello.Substring(Sello.Length - 8, 8);

                //Rodrigo Escalona 22/03/2023 Verificar si existe 
                string EstadoTr = "Migrado Sellada REAL " + DateTime.Now.ToString();
                if (String.IsNullOrEmpty(XML))
                {
                    //Crear Archivo para
                    cSERIE ocSERIE = new cSERIE();
                    ocSERIE.cargar_serie(SerieTr);
                    XML = ocSERIE.DIRECTORIO + @"\" + ocSERIE.ID + @"\" + Fechammmyy(FechaTimbrado);
                    

                }
                //Copiar factura
                if (!ExisteTimbrada(InvoiceId))
                {
                    
                    sSQL = " INSERT INTO " + Globales.VMX_FE_Tabla + " ";
                    sSQL += " ( ";
                    sSQL += " INVOICE_ID, XML,ESTADO, SERIE,Cadena_TFD";
                    sSQL += ",PDF, CADENA,SELLO,QR_Code";
                    sSQL += ",METODO_DE_PAGO, FORMA_DE_PAGO";
                    sSQL += ",CTA_BANCO,noCertificado, USO_CFDI, VERSION,Fecha,UUID )";
                    sSQL += " VALUES ";
                    sSQL += "(";
                    sSQL += " '" + InvoiceId + "','" + XML + "','" + EstadoTr + "','" + SerieTr + "','" + cadenaoriginal_TFD_1_0 + "'";
                    sSQL += ",'" + PDF + "','" + cadenaoriginal_TFD_1_0.Replace("'", "'''") + "','" + Sello + "','" + QR_Code + "'";
                    sSQL += ",'" + metodo_pago + "', '" + FormaPago + "'";
                    sSQL += ", '" + banco + "','" + NoCertificado + "', '" + USO_CFDI + "','" + VERSION + "','" + Fecha + "','" + UUID + "')";
                    sSQL = Globales.actualizar_invoice_id(sSQL);
                    oData.EjecutarConsulta(sSQL);
                }
                else
                {
                    sSQL = " UPDATE " + Globales.VMX_FE_Tabla + " ";
                    sSQL += " SET UUID='" + UUID + "',FechaTimbrado='" + FechaTimbrado + "'";
                    sSQL += ",noCertificadoSAT='" + noCertificadoSAT + "',selloSAT='" + selloSAT + "'";
                    sSQL += ",Cadena_TFD='" + cadenaoriginal_TFD_1_0 + "',QR_Code='" + QR_Code +
                    @"',METODO_DE_PAGO='" + metodo_pago + "',FORMA_DE_PAGO='" + FormaPago + "',CTA_BANCO='" + banco +
                    @"',noCertificado='" + NoCertificado + "', USO_CFDI='" + USO_CFDI + @"', VERSION='" + VERSION + @"' 
                    , Fecha='" + Fecha + "'";
                    sSQL += " WHERE INVOICE_ID='" + InvoiceId + "' ";
                    sSQL = Globales.actualizar_invoice_id(sSQL);
                    oData.EjecutarConsulta(sSQL);
                }

                //Rodrigo Escalona. Copiar el XML
                cFACTURA_BANDEJA ocFACTURA_BANDEJA = new cFACTURA_BANDEJA(oData, OEncapsular.ocEMPRESA);
                if (ocFACTURA_BANDEJA.cargar_ID(InvoiceId, OEncapsular.ocEMPRESA))
                {
                    if (String.IsNullOrEmpty(ocFACTURA_BANDEJA.XML))
                    {
                        //Crear Archivo para
                        cSERIE ocSERIE = new cSERIE();
                        ocSERIE.cargar_serie(SerieTr);
                        ocFACTURA_BANDEJA.XML = ocSERIE.DIRECTORIO + @"\" + ocSERIE.ID + @"\" + Fechammmyy(FechaTimbrado)
                            + @"\" + InvoiceId + ".XML";



                    }

                    if (!File.Exists(ocFACTURA_BANDEJA.XML))
                    {
                        //string filename = Path.GetFileName(ocFACTURA_BANDEJA.XML);
                        try
                        {
                            File.Copy(xml, ocFACTURA_BANDEJA.XML, true);
                        }
                        catch (IOException iox)
                        {
                            ErrorFX.mostrar(iox, true, true, "Migrar - 1217 - 744 ", false);
                        }
                        
                    }

                    //if (!File.Exists(ocFACTURA_BANDEJA.PDF))
                    //{
                    //    ocFACTURA_BANDEJA.generar_pdf(InvoiceId, OEncapsular, formato);
                    //}
                }



            }
            catch (XmlException error)
            {
                ErrorFX.mostrar(error, true, true, "Migrar - 1178 - 744 ", false);
            }

        }
        public string Fechammmyy(string pFecha)
        {
            DateTime Fecha = DateTime.Parse(pFecha.ToString());
            string FechaFormateada = "";
            string[] mMeses = new string[13];

            mMeses[1] = "Ene";
            mMeses[2] = "Feb";
            mMeses[3] = "Mar";
            mMeses[4] = "Abr";
            mMeses[5] = "May";
            mMeses[6] = "Jun";
            mMeses[7] = "Jul";
            mMeses[8] = "Agt";
            mMeses[9] = "Sep";
            mMeses[10] = "Oct";
            mMeses[11] = "Nov";
            mMeses[12] = "Dic";

            int Mes = int.Parse(Fecha.Month.ToString());

            FechaFormateada = mMeses[Mes] + AgregarCero(Fecha.Year.ToString());

            return FechaFormateada;
        }
        public string AgregarCero(string p)
        {

            String regreso = p;

            if (p.Length < 2)

                regreso = "0" + p;

            return regreso;
        }

        public bool generar_pdf(string INVOICE_ID, Encapsular oEncapsular, string formato)
        {
            try
            {
                //MessageBox.Show("Generar PDF");
                //Generar el PDF
                cFACTURA oFACTURA = new cFACTURA(oEncapsular.oData_ERP);
                //MessageBox.Show("Generar Factura");
                oFACTURA.datos(INVOICE_ID, "", "", oEncapsular.ocEMPRESA, "");
                //MessageBox.Show(oData_ERP.sTipo);
                ModeloDocumentosFXContainer dbContext = new ModeloDocumentosFXContainer();
                string conection = dbContext.Database.Connection.ConnectionString;
                oData = new cCONEXCION(conection);

                cGeneracion ocGeneracion = new cGeneracion(oData, oEncapsular.oData_ERP);
                //MessageBox.Show("Generar Factura Bandeja");
                cFACTURA_BANDEJA ocFACTURA_BANDEJA = new cFACTURA_BANDEJA(oEncapsular.oData_ERP, oEncapsular.ocEMPRESA);
                ocFACTURA_BANDEJA.cargar_ID(INVOICE_ID, oEncapsular.ocEMPRESA);
                //Actualizar el Metodo y forma de Pago del XML
                ocFACTURA_BANDEJA.actualizar_datos_xml(ocFACTURA_BANDEJA.XML, oFACTURA, oEncapsular);
                ocFACTURA_BANDEJA.cargar_ID(INVOICE_ID, oEncapsular.ocEMPRESA);
                return ocGeneracion.pdf(INVOICE_ID, oFACTURA, ocFACTURA_BANDEJA, oEncapsular.ocEMPRESA.ROW_ID, formato);
            }
            catch (Exception error)
            {
                ErrorFX.mostrar(error, true, true, "frmPrincipalFX - 18001 ", false);
            }
            return false;

        }

        public bool eliminar()
        {
            if (INVOICE_ID.Trim() != "")
            {
                DialogResult Resultado = MessageBox.Show("¿Esta seguro de eliminar la Factura?", Application.ProductName + "-" + Application.ProductVersion, MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                if (Resultado.ToString() != "Yes")
                {
                    return false;
                }

                sSQL = "DELETE FROM " + Globales.VMX_FE_Tabla + " WHERE INVOICE_ID=" + INVOICE_ID;
                sSQL = Globales.actualizar_invoice_id(sSQL);
                oData.EjecutarConsulta(sSQL);

                return true;
            }
            else
            {
                return false;
            }
        }

        internal void guardarEstado(string invoiceId, string estadop, Encapsular oEncapsular)
        {
            ESTADO = estadop;
            string BD_Auxiliar = oEncapsular.ocEMPRESA.BD_AUXILIAR + ".dbo.";
            if (oEncapsular.ocEMPRESA.BD_AUXILIAR == "")
            {
                BD_Auxiliar = "";
            }
            string sSQL = " UPDATE " + BD_Auxiliar + Globales.VMX_FE_Tabla + " ";
            sSQL += " SET ESTADO='" + ESTADO + @"'
            WHERE INVOICE_ID='" + invoiceId + "' ";
            sSQL = Globales.actualizar_invoice_id(sSQL);
            oEncapsular.oData_ERP.EjecutarConsulta(sSQL);
        }

        internal void actualizar_tipo_timbrado(string IDp, Encapsular oEncapsular, bool @checked)
        {
            try
            {
                if (IDp != "")
                {
                    ScriptFE.actualizar(oData);

                    sSQL = " UPDATE " + Globales.VMX_FE_Tabla + " ";
                    sSQL += " SET timbrado_prueba='" + @checked.ToString() + "' ";
                    sSQL += " WHERE INVOICE_ID='" + IDp + "' ";
                    sSQL = Globales.actualizar_invoice_id(sSQL);
                    oData.EjecutarConsulta(sSQL);

                }
            }
            catch (Exception e)
            {
                ErrorFX.mostrar(e, false, true, false);
            }
        }

        internal void Reactivar(string invoice, cEMPRESA ocEMPRESA)
        {
            try
            {
                if (!String.IsNullOrEmpty(invoice))
                {

                    sSQL = " UPDATE " + Globales.VMX_FE_Tabla + " ";
                    sSQL += " SET INVOICE_ID=INVOICE_ID+'Old' ";
                    sSQL += " WHERE INVOICE_ID='" + invoice + "' ";
                    sSQL = Globales.actualizar_invoice_id(sSQL);
                    oData.EjecutarConsulta(sSQL);

                }
            }
            catch (Exception e)
            {
                throw (e);
            }
        }

        internal bool ExisteTimbrada(string invoiceId)
        {
            try
            {
                bool existe = false;
                sSQL = " SELECT * ";
                sSQL += " FROM " + Globales.VMX_FE_Tabla + " ";
                sSQL += " WHERE " + Globales.VMX_FE_Tabla + ".INVOICE_ID='" + invoiceId + @"' 
                AND (ESTADO like '%Sellada%')
                ";
                DataTable oDataTable = oData.EjecutarConsulta(sSQL);
                if (oDataTable != null)
                {
                    foreach (DataRow oDataRow in oDataTable.Rows)
                    {
                        XML = oDataRow["XML"].ToString();
                        PDF = oDataRow["PDF"].ToString();
                        existe = true;
                    }
                }
                return existe;
            }
            catch (Exception e)
            {
                throw (e);
            }
            
        }
    }
}
