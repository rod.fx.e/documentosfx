﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;

using System.Text;
using System.Windows.Forms;
using System.Net.Mail;
using System.Text.RegularExpressions;
using System.IO;
using Microsoft.Win32;
using System.Runtime.InteropServices;
using System.Globalization;
using System.Reflection;
using System.Xml;
using System.Xml.Xsl;
using System.Xml.XPath;
using System.Xml.Serialization;
using FE_FX.EDICOM_Servicio;
using System.IO.Compression;
using Ionic.Zip;
using System.ServiceModel;
using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;
using System.Security.Cryptography.X509Certificates;
using System.Diagnostics;
using OfficeOpenXml;
using System.Configuration;
using System.Net;
using System.Net.Security;
using Generales;
using ModeloDocumentosFX;
using FE_FX.Clases;

namespace FE_FX
{
    public partial class frmPrincipalFXr2 : Form
    {

        //private cEMPRESA ocEMPRESA;
        private cFACTURA ocFACTURA_general;
        private cCONEXCION oData = new cCONEXCION("");
        //private cCONEXCION oData_ERP = new cCONEXCION("");
        public string VMX_FE_Tabla = "VMX_FE";
        public string BD_Auxiliar = "";
        cUSUARIO ocUSUARIO;
        string USUARIO;
        bool procesar = true;
        bool servicioGeneral = false;

        public frmPrincipalFXr2(string sConn)
        {
            oData = new cCONEXCION(sConn);
            InitializeComponent();
            limpiar();
            EMPRESA_NOMBRE.Text = "Todas las empresas";
            oTimer.Enabled = false;
            CFDI_PRUEBA.Checked = false;
            oTimer.Stop();


        }

        public frmPrincipalFXr2(string sConn, string USUARIOp, bool servicio = false)
        {
            USUARIO = USUARIOp;
            oData = new cCONEXCION(sConn);
            servicioGeneral = servicio;
            InitializeComponent();
            limpiar();
            EMPRESA_NOMBRE.Text = "Todas las empresas";
            //Foco al Tab de Busqieda por defecto
            ribbonTabItem1.Focus();

            progressBar1.Visible = false;
            this.Text = Application.ProductVersion + " " + Application.ProductName;
            Columnas_2(dtgrdGeneral);
            cargar_empresas();

            if (USUARIO != "FE")
            {
                //ocUSUARIO = new cUSUARIO(oData);
                //ocUSUARIO.cargar_ID(USUARIO);
                //if (ocUSUARIO.ADMINISTRADOR == "False")
                //{
                //    ribbonTabItem3.Visible = false;
                //}

                //if (ocUSUARIO.LECTURA == "True")
                //{
                //    ribbonTabItem3.Visible = false;
                //    buttonItem2.Enabled = false;
                //    CAMBIAR.Enabled = false;
                //    CANCELAR_btn.Enabled = false;
                //    VERIFICAR.Enabled = false;
                //    buttonItem9.Enabled = false;
                //    buttonItem11.Enabled = false;
                //}

                //ADDENDA.Enabled = false;
                //if (ocUSUARIO.ADDENDA == "True")
                //{
                //    ADDENDA.Enabled = true;
                //}
                //ADDENDA_INDIVIDUAL.Enabled = false;
                //if (ocUSUARIO.ADDENDA_INDIVIDUAL == "True")
                //{
                //    ADDENDA_INDIVIDUAL.Enabled = true;
                //}
                //CAMBIAR.Enabled = false;
                //if (ocUSUARIO.CAMBIAR == "True")
                //{
                //    CAMBIAR.Enabled = true;
                //}
                //CANCELAR_btn.Enabled = false;
                //if (ocUSUARIO.CANCELAR == "True")
                //{
                //    CANCELAR_btn.Enabled = true;
                //}
                //IMPORTAR.Enabled = false;
                //if (ocUSUARIO.IMPORTAR == "True")
                //{
                //    IMPORTAR.Enabled = true;
                //}
                //PEDIMENTOS_btn.Enabled = false;
                //if (ocUSUARIO.PEDIMENTOS == "True")
                //{
                //    PEDIMENTOS_btn.Enabled = true;
                //}
                //VERIFICAR.Enabled = false;
                //if (ocUSUARIO.VERIFICAR == "True")
                //{
                //    VERIFICAR.Enabled = true;
                //}

                //btnRetencion.Visible = false;
                //if (ocUSUARIO.COMPROBANTE_PAGO == "True")
                //{
                //    btnRetencion.Visible = true;
                //}
            }
            cargar_cfdi_test();


            try
            {
                if (bool.Parse(ConfigurationManager.AppSettings["OCULTAR_checkbox_canceladas"].ToString()))
                {
                    OCULTAR.Visible = false;
                    controlContainerItem10.Visible = false;
                }
            }
            catch
            {

            }
            actualizar_tabla();

            //Activar el servicio

            if (servicio)
            {
                //this.Hide();
                //this.WindowState = FormWindowState.Minimized;
                //this.ShowInTaskbar = false;
                oTimer.Enabled = true;
                CFDI_PRUEBA.Checked = false;
                oTimer.Start();
            }

        }

        public void actualizar_tabla()
        {
            //Buscar Alias de VMX_FE
            try
            {
                string TABLA_VMX_FE = ConfigurationManager.AppSettings["VMX_FE"].ToString();
                if (TABLA_VMX_FE != "")
                {
                    VMX_FE_Tabla = "FXINV000";
                }

            }
            catch
            {
            }
        }

        public string actualizar_invoice_id(string sSQLp)
        {
            //Buscar Alias de VMX_FE
            try
            {
                string VMX_FE_INVOICE_ID = ConfigurationManager.AppSettings["VMX_FE.INVOICE_ID"].ToString();
                if (VMX_FE_INVOICE_ID != "")
                {
                    return sSQLp.Replace("INVOICE_ID", "ID");
                }

            }
            catch
            {
            }
            return sSQLp;
        }

        private void cargar_cfdi_test()
        {
            try
            {
                CFDI_PRUEBA.Checked = bool.Parse(ConfigurationManager.AppSettings["CFDI_TEST"].ToString());
            }
            catch
            {

            }

            try
            {
                PDF_VER.Checked = bool.Parse(ConfigurationManager.AppSettings["PDF_VER"].ToString());
            }
            catch
            {

            }
        }
        private void cargar_empresas()
        {
            ENTITY_ID.Items.Clear();
            ENTITY_ID.Items.Add("Todos");
            cEMPRESA ocEMPRESA = new cEMPRESA(oData);
            foreach (cEMPRESA registro in ocEMPRESA.todos("", true))
            {
                ENTITY_ID.Items.Add(registro);
            }
            if (ENTITY_ID.Items.Count > 0)
            {
                ENTITY_ID.SelectedIndex = 0;
            }
        }

        private void Columnas_2(DataGridView dtg)
        {
            int n = 0;

            n = dtg.Columns.Add("ROW_ID", "ROW_ID");
            dtg.Columns[n].ReadOnly = true;
            dtg.Columns[n].Visible = false;
            dtg.Columns[n].Width = 70;

            n = dtg.Columns.Add("VERSION", "Versión");
            dtg.Columns[n].ReadOnly = true;
            dtg.Columns[n].Visible = true;
            dtg.Columns[n].Width = 30;

            n = dtg.Columns.Add("INVOICE_ID", "Factura");
            dtg.Columns[n].ReadOnly = true;
            dtg.Columns[n].Visible = true;
            dtg.Columns[n].Width = 110;

            n = dtg.Columns.Add("CUSTOMER_ID", "Cliente");
            dtg.Columns[n].ReadOnly = true;
            dtg.Columns[n].Width = 80;

            n = dtg.Columns.Add("CUSTOMER_NAME", "Nombre");
            dtg.Columns[n].ReadOnly = true;
            dtg.Columns[n].Width = 180;

            n = dtg.Columns.Add("TYPE", "Tipo");
            dtg.Columns[n].ReadOnly = true;
            dtg.Columns[n].Width = 40;

            n = dtg.Columns.Add("STATUS", "ST");
            dtg.Columns[n].ReadOnly = true;
            dtg.Columns[n].Width = 40;

            n = dtg.Columns.Add("INVOICE_DATE", "Fecha");
            dtg.Columns[n].ReadOnly = true;
            dtg.Columns[n].Width = 70;

            n = dtg.Columns.Add("CURRENCY_ID", "Moneda");
            dtg.Columns[n].ReadOnly = true;
            dtg.Columns[n].Width = 70;

            n = dtg.Columns.Add("TOTAL_AMOUNT", "Monto");
            dtg.Columns[n].ReadOnly = true;
            dtg.Columns[n].Width = 70;
            dtg.Columns[n].SortMode = DataGridViewColumnSortMode.NotSortable;
            dtg.Columns[n].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;

            n = dtg.Columns.Add("METODO_DE_PAGO", "Metodo de Pago");
            dtg.Columns[n].ReadOnly = true;
            dtg.Columns[n].Width = 120;

            n = dtg.Columns.Add("FORMA_DE_PAGO", "Forma de Pago");
            dtg.Columns[n].ReadOnly = true;
            dtg.Columns[n].Width = 120;

            n = dtg.Columns.Add("USO_CFDI", "USO del CFDI");
            dtg.Columns[n].ReadOnly = true;
            dtg.Columns[n].Width = 100;

            n = dtg.Columns.Add("CTA_BANCO", "CTA Banco");
            dtg.Columns[n].ReadOnly = true;
            dtg.Columns[n].Width = 100;

            n = dtg.Columns.Add("Estado", "Timbrado");
            dtg.Columns[n].ReadOnly = true;
            dtg.Columns[n].Width = 120;

            n = dtg.Columns.Add("Cancelado", "Cancelado");
            dtg.Columns[n].ReadOnly = true;
            dtg.Columns[n].Width = 120;

            n = dtg.Columns.Add("UUID", "UUID");
            dtg.Columns[n].ReadOnly = true;
            dtg.Columns[n].Width = 70;

            n = dtg.Columns.Add("Archivo", "Archivo");
            dtg.Columns[n].ReadOnly = true;
            dtg.Columns[n].Width = 240;

            n = dtg.Columns.Add("PDF_ESTADO", "Impresión PDF");
            dtg.Columns[n].ReadOnly = true;
            dtg.Columns[n].Width = 120;

            n = dtg.Columns.Add("Dummy", "Dummy");
            dtg.Columns[n].Visible = false;
            dtg.Columns[n].Width = 200;



        }


        public byte[] ReadBinaryFile(string fileName)
        {
            if (File.Exists(fileName))
            {
                try
                {
                    ///Open and read a file&#12290;
                    FileStream fileStream = File.OpenRead(fileName);
                    byte[] archivo = ConvertStreamToByteBuffer(fileStream);
                    fileStream.Close();
                    return archivo;
                }
                catch (Exception ex)
                {
                    return new byte[0];
                }
            }
            else
            {
                return new byte[0];
            }
        }

        public byte[] ConvertStreamToByteBuffer(System.IO.Stream theStream)
        {
            int b1;
            System.IO.MemoryStream tempStream = new System.IO.MemoryStream();
            while ((b1 = theStream.ReadByte()) != -1)
            {
                tempStream.WriteByte(((byte)b1));
            }
            return tempStream.ToArray();
        }

        private void frmPrincipalFXr2_Load(object sender, EventArgs e)
        {
            limpiar();
            //cargar();

        }

        private void frmPrincipalFXr2_FormClosed(object sender, FormClosedEventArgs e)
        {
            //Application.Exit();
        }

        private void limpiar()
        {
            DateTime firstDayOfCurrentMonth = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1);
            INICIO.Value = firstDayOfCurrentMonth;
            cargar_formatos();
        }
        private void cargar_formatos()
        {
            cEMPRESA ocEMPRESA = new cEMPRESA(oData);
            ocEMPRESA = ENTITY_ID.SelectedItem as cEMPRESA;
            if (ocEMPRESA != null)
            {
                FORMATO.Items.Clear();
                FORMATO.Text = "";
                cFORMATO ocFORMATO = new cFORMATO();
                foreach (cFORMATO registro in ocFORMATO.todos_empresa("", ocEMPRESA.ROW_ID))
                {
                    FORMATO.Items.Add(registro.ID);
                }
            }
            else
            {
                //Cargar todos los formatos
                FORMATO.Items.Clear();
                FORMATO.Text = "";
                cFORMATO ocFORMATO = new cFORMATO();
                foreach (cFORMATO registro in ocFORMATO.todos(""))
                {
                    FORMATO.Items.Add(registro.ID);
                }
            }
        }

        private void btnRefresh_Click(object sender, EventArgs e)
        {
            cargar(dtgrdGeneral);
        }

        private void toolStripButton3_Click(object sender, EventArgs e)
        {
            ver_xml();
        }
        private void ver_pdf()
        {
            if (dtgrdGeneral.SelectedRows.Count > 0)
            {
                if (ESTADO.Text == "REGENERAR")
                {
                    regenerar_pdf();
                }
                else
                {
                    abrir_pdf();
                }
            }
        }


        private void ver_xml()
        {
            if (dtgrdGeneral.SelectedRows.Count > 0)
            {
                DataGridViewSelectedRowCollection arrSelectedRows = dtgrdGeneral.SelectedRows;
                string ARCHIVO = arrSelectedRows[0].Cells["ARCHIVO"].Value.ToString();
                string directorio = AppDomain.CurrentDomain.BaseDirectory;
                frmFacturaXML oObjeto = new frmFacturaXML(ARCHIVO);
                oObjeto.Show();
            }
        }

        public byte[] LeerArchivoByte(string fileName)
        {
            byte[] buff = null;
            FileStream fs = new FileStream(fileName, FileMode.Open, FileAccess.Read);
            BinaryReader br = new BinaryReader(fs);
            long numBytes = new FileInfo(fileName).Length;
            buff = br.ReadBytes((int)numBytes);
            return buff;
        }

        public string LeerArchivoByte2(string fileName)
        {
            FileStream fs = new FileStream(fileName, FileMode.Open, FileAccess.Read);
            byte[] filebytes = new byte[fs.Length];
            fs.Read(filebytes, 0, Convert.ToInt32(fs.Length));
            string encodedData = Convert.ToBase64String(filebytes, Base64FormattingOptions.InsertLineBreaks);
            return encodedData;
        }

        public char[] LeerArchivoChar(string fileName)
        {
            // Create another FileInfo object and get the Length.
            FileInfo f2 = new FileInfo(fileName);

            char[] buf = new char[f2.Length];
            StreamReader sr = new StreamReader(new FileStream(fileName, FileMode.Open, FileAccess.Read));
            int retval = sr.ReadBlock(buf, 0, int.Parse(f2.Length.ToString()));
            string filereadbuf = new string(buf);
            sr.Close();
            return buf;
        }

        private void generar_CFDI_UUID(int indice, string EDICOM_USUARIO, string EDICOM_PASSWORD, cFACTURA ocFACTURA, cSERIE ocSERIE)
        {
            try
            {
                if (dtgrdGeneral.SelectedRows.Count > 0)
                {

                    //Generar CFDi
                    DataGridViewRow arrSelectedRows = dtgrdGeneral.Rows[indice];
                    string ARCHIVO = arrSelectedRows.Cells["ARCHIVO"].Value.ToString();
                    string UUID = arrSelectedRows.Cells["UUID"].Value.ToString();
                    string ID = arrSelectedRows.Cells["INVOICE_ID"].Value.ToString();
                    string PDF = "";
                    string XML = "";
                    Encapsular oEncapsular = (Encapsular)arrSelectedRows.Tag;
                    cFACTURA_BANDEJA ocFACTURA_BANDEJA = new cFACTURA_BANDEJA(oEncapsular.oData_ERP, oEncapsular.ocEMPRESA);
                    string XML_Archivo = "";
                    if (ARCHIVO == "")
                    {
                        string DIRECTORIO_ARCHIVOS = "";
                        DIRECTORIO_ARCHIVOS += ocSERIE.DIRECTORIO + @"\" + ocSERIE.ID + @"\" + Fechammmyy(ocFACTURA.INVOICE_DATE.ToString());
                        XML = DIRECTORIO_ARCHIVOS;
                        if (Directory.Exists(XML) == false)
                            Directory.CreateDirectory(XML);

                        PDF = XML + @"\" + "pdf";
                        if (Directory.Exists(PDF) == false)
                            Directory.CreateDirectory(PDF);

                        XML += @"\" + "xml";
                        if (Directory.Exists(XML) == false)
                            Directory.CreateDirectory(XML);


                        if (Directory.Exists(DIRECTORIO_ARCHIVOS))
                        {
                            XML_Archivo = XML + @"\" + ocFACTURA.INVOICE_ID + ".xml";
                        }
                        else
                        {
                            XML_Archivo = @"\" + ocFACTURA.INVOICE_ID + ".xml"; ;
                        }

                        XML_Archivo = XML + @"\" + ocFACTURA.INVOICE_ID + ".xml";

                        ARCHIVO = XML_Archivo;
                    }
                    if (UUID == "")
                    {
                        DialogResult Resultado = MessageBox.Show("El UUID esta en blanco, desea escribirlo?", Application.ProductName + "-" + Application.ProductVersion, MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                        if (Resultado.ToString() != "Yes")
                        {
                            return;
                        }
                        else
                        {
                            frmUUID ofrmUUID = new frmUUID();
                            ofrmUUID.ShowDialog();
                            if (ofrmUUID.DialogResult == DialogResult.OK)
                            {
                                UUID = ofrmUUID.UUIDp;
                            }
                            else
                            {
                                return;
                            }
                        }
                    }
                    if (UUID.Trim() == "")
                    {
                        return;
                    }
                    //Llamar el Servicio
                    CFDiClient oCFDiClient = new CFDiClient();
                    byte[] PROCESO;

                    try
                    {
                        toolStripStatusLabel1.Text = "Buscando el UUID " + UUID + " Factura: " + INVOICE_ID;
                        string[] UUID_tr = new string[1];
                        UUID_tr[0] = UUID;

                        PROCESO = oCFDiClient.getCfdiFromUUID(EDICOM_USUARIO, EDICOM_PASSWORD, EDICOM_USUARIO, UUID_tr);

                        ocFACTURA_BANDEJA.cargar_ID(ocFACTURA.INVOICE_ID, oEncapsular.ocEMPRESA);
                        ocFACTURA_BANDEJA.INVOICE_ID = ocFACTURA.INVOICE_ID;
                        ocFACTURA_BANDEJA.XML = ARCHIVO;
                        ocFACTURA_BANDEJA.guardar();

                        //guardar_archivo(PROCESO, ID, ARCHIVO, ocFACTURA, oEncapsular.oData_ERP, oEncapsular.ocEMPRESA, UUID);
                        MessageBox.Show("Aqui va el codigo");
                    }
                    catch (FaultException oException)
                    {

                        CFDiException oCFDiException = new CFDiException();
                        MessageBox.Show(oException.Message, Application.ProductName + "-" + Application.ProductVersion, MessageBoxButtons.OK, MessageBoxIcon.Error);
                        toolStripStatusLabel1.Text = "Error " + DateTime.Now.ToShortDateString() + " " + DateTime.Now.ToLongTimeString();

                        return;
                    }

                    var serializer = new XmlSerializer(typeof(CFDI32.Comprobante));

                    var stream = new FileStream(ARCHIVO, FileMode.Open);

                    var container = serializer.Deserialize(stream) as CFDI32.Comprobante;
                    stream.Close();

                    XPathDocument myXPathDoc = new XPathDocument(ARCHIVO);
                    //XslTransform myXslTrans = new XslTransform();
                    XslCompiledTransform myXslTrans = new XslCompiledTransform();


                    XslCompiledTransform trans = new XslCompiledTransform();
                    XmlUrlResolver resolver = new XmlUrlResolver();

                    resolver.Credentials = System.Net.CredentialCache.DefaultCredentials;
                    XsltSettings settings = new XsltSettings(true, true);
                    string sDirectory = AppDomain.CurrentDomain.BaseDirectory;
                    //Cargar xslt
                    try
                    {
                        //Version 3
                        myXslTrans.Load(sDirectory + "/XSLT/cadenaoriginal_3_2.xslt");
                    }
                    catch (XmlException errores)
                    {
                        MessageBox.Show("Error. Conectarse en el Portal del SAT al " + errores.InnerException.ToString());
                        return;
                    }

                    //Crear Archivo Temporal
                    string nombre_tmp = ocFACTURA.INVOICE_ID + "TMP" + DateTime.Now.ToString("ddMMyyyyhhmmss");
                    XmlTextWriter myWriter = new XmlTextWriter(nombre_tmp + ".TXT", null);

                    //Transformar al XML
                    myXslTrans.Transform(myXPathDoc, null, myWriter);

                    myWriter.Close();
                    string CADENA_ORIGINAL = "";
                    //Abrir Archivo TXT
                    StreamReader re = File.OpenText(nombre_tmp + ".TXT");
                    string input = null;
                    while ((input = re.ReadLine()) != null)
                    {
                        CADENA_ORIGINAL += input;
                    }
                    re.Close();

                    //Eliminar Archivos Temporales
                    File.Delete(nombre_tmp + ".TXT");
                    File.Delete(nombre_tmp + ".XML");

                    //Buscar el &amp; en la Cadena y sustituirlo por &
                    CADENA_ORIGINAL = CADENA_ORIGINAL.Replace("&amp;", "&");
                    string PDF_Archivo = "";
                    PDF_Archivo = PDF + @"\" + ocFACTURA.INVOICE_ID + ".pdf";

                    cCERTIFICADO ocCERTIFICADO = new cCERTIFICADO();
                    ocCERTIFICADO.cargar(ocSERIE.ROW_ID_CERTIFICADO);
                    cGeneracion ocGeneracion = new cGeneracion(oData, oEncapsular.oData_ERP);
                    string SELLO = ocGeneracion.SelloRSA(ocCERTIFICADO.LLAVE, ocCERTIFICADO.PASSWORD, CADENA_ORIGINAL, ocFACTURA.INVOICE_DATE.Year);


                    ocFACTURA_BANDEJA.cargar_banco();
                    ocFACTURA_BANDEJA = new cFACTURA_BANDEJA(oEncapsular.oData_ERP, oEncapsular.ocEMPRESA);
                    ocFACTURA_BANDEJA.cargar_ID(ocFACTURA.INVOICE_ID, oEncapsular.ocEMPRESA);
                    ocFACTURA_BANDEJA.XML = XML_Archivo;
                    ocFACTURA_BANDEJA.PDF = PDF_Archivo;
                    ocFACTURA_BANDEJA.INVOICE_ID = ocFACTURA.INVOICE_ID;
                    ocFACTURA_BANDEJA.ESTADO = "UUID Facturación Electrónica desde UUID";
                    ocFACTURA_BANDEJA.CADENA = CADENA_ORIGINAL;
                    ocFACTURA_BANDEJA.SELLO = SELLO;
                    ocFACTURA_BANDEJA.METODO_DE_PAGO = ocFACTURA_BANDEJA.METODO_DE_PAGO;
                    ocFACTURA_BANDEJA.CTA_BANCO = ocFACTURA_BANDEJA.CTA_BANCO;
                    ocFACTURA_BANDEJA.guardar();

                    generar_pdf(ocFACTURA.INVOICE_ID, oEncapsular);

                }
                else
                {
                    MessageBox.Show("Selecione un registro");
                }
            }
            catch (Exception oException)
            {
                MessageBox.Show(oException.Message, Application.ProductName + "-" + Application.ProductVersion, MessageBoxButtons.OK, MessageBoxIcon.Error);
                toolStripStatusLabel1.Text = "Error " + DateTime.Now.ToShortDateString() + " " + DateTime.Now.ToLongTimeString();

                return;
            }
        }

        public string CreateMD5(string input)
        {
            // Use input string to calculate MD5 hash
            using (System.Security.Cryptography.MD5 md5 = System.Security.Cryptography.MD5.Create())
            {
                byte[] inputBytes = System.Text.Encoding.ASCII.GetBytes(input);
                byte[] hashBytes = md5.ComputeHash(inputBytes);

                // Convert the byte array to hexadecimal string
                StringBuilder sb = new StringBuilder();
                for (int i = 0; i < hashBytes.Length; i++)
                {
                    sb.Append(hashBytes[i].ToString("X2"));
                }
                return sb.ToString().ToLower();
            }
        }

        private void generar_addenda(cFACTURA ocFACTURA, cFACTURA_BANDEJA ocFACTURA_BANDEJA)
        {
            cADDENDA ocADDENDA = new cADDENDA();
            //MessageBox.Show("Addenda Cliente: " + ocFACTURA.CUSTOMER_ID);
            if (ocADDENDA.cargar_CUSTOMER_ID(ocFACTURA.CUSTOMER_ID))
            {
                //
                //MessageBox.Show("Ejecutando Addenda Cliente: " + ocFACTURA.CUSTOMER_ID);
                toolStripStatusLabel1.Text = "Creando Addenda y Anexando";
                //addenda_CROWN(ocFACTURA_BANDEJA.XML);
                //Enviar a ejecutable el XML timbrado por EDICCOM para agregar la Addenda
                //Process p = new Process();
                //p.StartInfo.UseShellExecute = false;
                //MessageBox.Show("Ejecutar: " + ocADDENDA.EJECUTABLE);
                //sMessageBox.Show("XML: " + ocFACTURA_BANDEJA.XML);
                //////p.StartInfo.FileName = ocADDENDA.EJECUTABLE;
                //////p.StartInfo.Arguments = ocFACTURA_BANDEJA.XML;
                //////p.Start();
                if (File.Exists(ocADDENDA.EJECUTABLE))
                {
                    try
                    {
                        System.Diagnostics.Process proc = new System.Diagnostics.Process();
                        proc.EnableRaisingEvents = false;
                        proc.StartInfo.FileName = "\"" + ocADDENDA.EJECUTABLE + "\"";
                        string archivo = "";
                        if (ocFACTURA_BANDEJA.XML.IndexOf(" ", StringComparison.OrdinalIgnoreCase) >= 0)
                        {
                            archivo = "\"" + ocFACTURA_BANDEJA.XML + "\"";
                        }
                        else
                        {
                            archivo = ocFACTURA_BANDEJA.XML;
                        }

                        proc.StartInfo.Arguments = archivo;
                        proc.Start();
                        proc.WaitForExit();

                        ////Process build = new Process();
                        ////build.StartInfo.WorkingDirectory = @"dir";
                        ////build.StartInfo.Arguments = ocFACTURA_BANDEJA.XML;
                        ////build.StartInfo.FileName = ocADDENDA.EJECUTABLE;

                        ////build.StartInfo.UseShellExecute = false;
                        ////build.StartInfo.RedirectStandardOutput = true;
                        ////build.StartInfo.RedirectStandardError = true;
                        ////build.StartInfo.CreateNoWindow = true;
                        ////build.ErrorDataReceived += build_ErrorDataReceived;
                        ////build.OutputDataReceived += build_ErrorDataReceived;
                        ////build.EnableRaisingEvents = true;
                        ////build.Start();
                        ////build.BeginOutputReadLine();
                        ////build.BeginErrorReadLine();

                    }
                    catch (Exception e)
                    {
                        MessageBox.Show(e.ToString());
                    }
                }
                else
                {
                    MessageBox.Show("No existe el procesar Addenda de: " + ocADDENDA.EJECUTABLE);
                }
            }

        }

        // write out info to the display window
        static void build_ErrorDataReceived(object sender, DataReceivedEventArgs e)
        {
            string strMessage = e.Data;
            MessageBox.Show(e.ToString());
        }

        private void toolStripButton7_Click(object sender, EventArgs e)
        {
            generar_FE();
        }

        private bool configuracion_verificar_fecha(DateTime FECHA_FACTURA)
        {
            bool verificar = false;
            DateTime fecha = new DateTime();

            try
            {
                fecha = DateTime.Parse(ConfigurationManager.AppSettings["Fecha_Condicion_Timbrar"].ToString());
                verificar = true;
            }
            catch
            {

            }

            if (verificar)
            {
                if (FECHA_FACTURA.Subtract(fecha).TotalMilliseconds > 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            return true;

        }

        private void generar_FE(string version = "3.2", bool prueba = false)
        {

            try
            {

                if (dtgrdGeneral.SelectedRows.Count > 0)
                {
                    DataGridViewSelectedRowCollection arrSelectedRows = dtgrdGeneral.SelectedRows;
                    ModeloDocumentosFXContainer dbContext = new ModeloDocumentosFXContainer();
                    string conection = dbContext.Database.Connection.ConnectionString;
                    oData = new cCONEXCION(conection);

                    int d = 0;
                    bool existe_error = false;
                    for (int n = arrSelectedRows.Count - 1; n >= 0; n--)
                    {
                        Encapsular oEncapsular = (Encapsular)arrSelectedRows[n].Tag;
                        //string BD_Visual = oEncapsular.oData_ERP.sDatabase + ".dbo.";
                        cargar_bd_auxiliar(oEncapsular);

                        int indice = arrSelectedRows[n].Index;
                        string INVOICE_ID = arrSelectedRows[n].Cells["INVOICE_ID"].Value.ToString();
                        string UUID = arrSelectedRows[n].Cells["UUID"].Value.ToString();
                        string ESTADO = arrSelectedRows[n].Cells["ESTADO"].Value.ToString();
                        string INVOICE_DATE = arrSelectedRows[n].Cells["INVOICE_DATE"].Value.ToString();
                        string STATUS = arrSelectedRows[n].Cells["STATUS"].Value.ToString();
                        //Verificar la direccion de entrega si es el Cliente configurado para la Empresa
                        string CUSTOMER_ID = arrSelectedRows[n].Cells["CUSTOMER_ID"].Value.ToString();
                        string ADDR_NO = "";

                        arrSelectedRows[n].Cells["ESTADO"].Value = "Cargar factura";

                        cFACTURA ocFACTURA = new cFACTURA(oEncapsular.oData_ERP);
                        //BD_Visual = oEncapsular.oData_ERP.sDatabase + ".dbo.";
                        cargar_bd_auxiliar(oEncapsular);

                        //Verificar la fecha
                        arrSelectedRows[n].Cells["ESTADO"].Value = "Verificar fecha de factura";

                        DateTime INVOICE_DATE_tr = DateTime.Now;
                        try
                        {
                            INVOICE_DATE_tr = DateTime.Parse(INVOICE_DATE);
                        }
                        catch
                        {
                            INVOICE_DATE_tr = DateTime.Today;
                        }
                        //MessageBox.Show("Procesar Verificar Fecha");
                        if (configuracion_verificar_fecha(INVOICE_DATE_tr))
                        {
                            if (oEncapsular.ocEMPRESA.CUSTOMER_ID.IndexOf(CUSTOMER_ID) >= 0)
                            {
                                ////
                                ////Desplegar las direccion de despacho del Cliente
                                //frmCUST_ADDRESS frmCUST_ADDRESS = new frmCUST_ADDRESS(oData_ERP.sConn, CUSTOMER_ID, "S");
                                //if (frmCUST_ADDRESS.ShowDialog() == DialogResult.OK)
                                //{
                                //    ADDR_NO = frmCUST_ADDRESS.selecionados[0].Cells["ADDR_NO"].Value.ToString();
                                //}
                                ocFACTURA.cargar_direccion_entrega(INVOICE_ID, oEncapsular.ocEMPRESA);
                                ADDR_NO = ocFACTURA.ADDR_NO;
                                //MessageBox.Show("Verificar 1" + ADDR_NO);
                                if (ADDR_NO == "")
                                {
                                    MessageBox.Show("Este cliente que quiere facturar requiere una Direccion de Entrega para tomar los datos de Facturación. No se puede timbrar.");
                                    return;
                                }
                            }
                            //MessageBox.Show("Verificar " + ADDR_NO);
                            arrSelectedRows[n].Cells["ESTADO"].Value = "Verificar fecha de serie";
                            string SERIE_tr = INVOICE_ID.Replace("0", "").Replace("1", "").Replace("2", "").Replace("3", "").Replace("4", "").Replace("5", "").Replace("6", "").Replace("7", "").Replace("8", "").Replace("9", "").Trim();


                            cSERIE ocSERIE = new cSERIE();


                            //if (!ocSERIE.cargar_serie(SERIE_tr))
                            //{
                            //    MessageBox.Show("La Serie " + SERIE_tr + " no esta configurada.", Application.ProductName + "-" + Application.ProductVersion, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                            //    return;
                            //}



                            if (!ocSERIE.cargar_serie(SERIE_tr, oEncapsular.ocEMPRESA.ROW_ID))
                            {
                                MessageBox.Show("La Serie " + SERIE_tr + " no esta configurada.", Application.ProductName + "-" + Application.ProductVersion, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                                return;
                            }
                            arrSelectedRows[n].Cells["ESTADO"].Value = "Crear XML";
                            //Crear XML
                            if (ESTADO.ToUpper().Contains("TEST"))
                            {
                                UUID = "";
                            }
                            if (ESTADO.Contains("Rechazado"))
                            {
                                UUID = "";
                            }
                            if (ESTADO.Contains("Creada"))
                            {
                                UUID = "";
                            }
                            if (ESTADO.Contains("Generando"))
                            {
                                UUID = "";
                            }
                            //

                            if ((UUID == null) || (UUID == ""))
                            {
                                //Ejecutando el Trigger
                                arrSelectedRows[n].Cells["ESTADO"].Value = "Ejecutando el Trigger";

                                //Verificar que estado en Visual
                                bool timbrar = true;
                                if (STATUS == "X")
                                {
                                    if (UUID != "")
                                    {
                                        if (ESTADO.Contains("Cancelado"))
                                        {
                                            timbrar = false;
                                        }
                                    }
                                    else
                                    {
                                        timbrar = false;
                                    }
                                }
                                //MessageBox.Show("Generar el XML");
                                arrSelectedRows[n].Cells["ESTADO"].Value = "Generando el CFDI Versión: " + version;
                                //Validar fecha 72
                                if (ocFACTURA.actualizar_fecha(INVOICE_ID, prueba))
                                {
                                    if (timbrar)
                                    {
                                        //Error de carga de productos
                                        if (ocFACTURA.errorValidacion)
                                        {
                                            ErrorFX.mostrar(ocFACTURA.errorMensaje, true, true, true);
                                            return;
                                        }

                                        cGeneracion ocGeneracion = new cGeneracion(oData, oEncapsular.oData_ERP);
                                        informacion.Text = "Creando el CFDI " + INVOICE_ID + " Versión: " + version;
                                        Application.DoEvents();

                                        //Guardar el metodo, forma y uso de cfdi
                                        guardarMetodoFormaUso(arrSelectedRows[n],oEncapsular.ocEMPRESA);

                                        if (ocGeneracion.generar(INVOICE_ID, ADDR_NO, oEncapsular.ocEMPRESA.ROW_ID, version))
                                        {
                                            informacion.Text = "Timbrando el CFDI " + INVOICE_ID + " Versión: " + version;
                                            Application.DoEvents();
                                            int opcion = 1;
                                            if (CFDI_PRUEBA.Checked)
                                            {
                                                opcion = 2;
                                            }

                                            bool timbrado = this.timbrar(INVOICE_ID, oEncapsular, opcion);
                                            if (timbrado)
                                            {
                                                //Extraer datos del XML y guardarlo en la bandeja
                                                cFACTURA_BANDEJA ocFACTURA_BANDEJA = new cFACTURA_BANDEJA(oEncapsular.oData_ERP, oEncapsular.ocEMPRESA);
                                                ocFACTURA_BANDEJA.cargar_ID(INVOICE_ID, oEncapsular.ocEMPRESA);
                                                arrSelectedRows[n].Cells["ESTADO"].Value = ocFACTURA_BANDEJA.ESTADO;
                                                ocFACTURA_BANDEJA.actualizar_datos_xml(ocFACTURA_BANDEJA.XML, ocFACTURA, oEncapsular);


                                                informacion.Text = "Creando el PDF " + INVOICE_ID + " Versión: " + version;
                                                Application.DoEvents();
                                                if (generar_pdf(INVOICE_ID, oEncapsular))
                                                {
                                                    if (PDF_VER.Checked)
                                                    {
                                                        string directorio = AppDomain.CurrentDomain.BaseDirectory;
                                                        frmPDF ofrmFacturaXML = new frmPDF(ocFACTURA_BANDEJA.PDF);
                                                        ofrmFacturaXML.Show();
                                                    }
                                                    //Cargar los datos de la bandeja
                                                    cargaFacturaBandeja(indice, INVOICE_ID, oEncapsular);

                                                    if (!CFDI_PRUEBA.Checked)
                                                    {
                                                        //if (ocFACTURA.TYPE == "I")
                                                        //{
                                                        if (bool.Parse(ocSERIE.ENVIO_AUTOMATICO_MAIL))
                                                        {
                                                            enviar(oEncapsular, INVOICE_ID, true);
                                                        }

                                                        //}

                                                        if (ocFACTURA.TYPE == "M")
                                                        {
                                                            if (oEncapsular.ocEMPRESA.IMPRESION_AUTOMATICA_NDC)
                                                            {
                                                                enviar(oEncapsular, INVOICE_ID, true);
                                                            }
                                                        }
                                                    }

                                                    informacion.Text = "Fin del CFDI " + INVOICE_ID + " Versión: " + version;
                                                    Application.DoEvents();
                                                }
                                                else
                                                {
                                                    informacion.Text = "Error en el PDF del CFDI " + INVOICE_ID + " Versión: " + version;
                                                    //Guardar el error en PDF
                                                    ocFACTURA_BANDEJA.actualizar_PDF_ESTADO(INVOICE_ID, "Error Imprimiendo");
                                                }
                                            }
                                            else
                                            {
                                                informacion.Text = "Error en el Timbrado del CFDI " + INVOICE_ID + " Versión: " + version;
                                            }                                            
                                        }
                                        else
                                        {
                                            existe_error = true;
                                            ErrorFX.mostrar("frmPrincipalFXr2 - 1760 " + Environment.NewLine + "Operación abortada", true, false, false);
                                        }
                                    }
                                }
                            }
                            else
                            {
                                MessageBox.Show("La Factura " + INVOICE_ID + " ya esta procesada.", Application.ProductVersion, MessageBoxButtons.OK);
                            }
                        }
                        else
                        {
                            MessageBox.Show("La Factura " + INVOICE_ID + " no puede procesarce. La fecha es mayor a 72 horas.");
                        }
                    }
                    
                    //MessageBox.Show("Las Facturas fueron generadas.",Application.ProductName + "-" + Application.ProductVersion, MessageBoxButtons.OK, MessageBoxIcon.Information);
                    //Actualizar de forma automatica
                    if (Globales.automatico)
                    {
                        if (!existe_error)
                        {
                            cargar(dtgrdGeneral);
                        }
                    }
                    
                }
            }
            catch (Exception e)
            {
                ErrorFX.mostrar(e, true, true, "frmPrincipalFXr2 - 1789 -", false);
            }
            informacion.Text = "";
        }

        private void guardarMetodoFormaUso(DataGridViewRow dataGridViewRow,cEMPRESA ocEMPRESA)
        {
            cFACTURA_BANDEJA ocFACTURA_BANDEJA = new cFACTURA_BANDEJA(oData);
            ocFACTURA_BANDEJA.cargar_ID(dataGridViewRow.Cells["INVOICE_ID"].Value.ToString(), ocEMPRESA);
            ocFACTURA_BANDEJA.INVOICE_ID = dataGridViewRow.Cells["INVOICE_ID"].Value.ToString();
            ocFACTURA_BANDEJA.METODO_DE_PAGO = dataGridViewRow.Cells["METODO_DE_PAGO"].Value.ToString();
            ocFACTURA_BANDEJA.FORMA_DE_PAGO = dataGridViewRow.Cells["FORMA_DE_PAGO"].Value.ToString();
            ocFACTURA_BANDEJA.USO_CFDI = dataGridViewRow.Cells["USO_CFDI"].Value.ToString();
            ocFACTURA_BANDEJA.guardar_metodo();
        }

        private void cargaFacturaBandeja(int n, string factura, Encapsular oEncapsular)
        {
            
            cFACTURA_BANDEJA ocFACTURA_BANDEJA = new cFACTURA_BANDEJA(oEncapsular.oData_ERP, oEncapsular.ocEMPRESA);
            ocFACTURA_BANDEJA.cargar_ID(factura, oEncapsular.ocEMPRESA);

            dtgrdGeneral.Rows[n].Cells["Estado"].Value = ocFACTURA_BANDEJA.ESTADO;
            if (!dtgrdGeneral.Rows[n].Cells["Estado"].Value.ToString().Contains("Sellada"))
            {
                dtgrdGeneral.Rows[n].DefaultCellStyle.BackColor = Color.Yellow;
            }

            dtgrdGeneral.Rows[n].Cells["ARCHIVO"].Value = ocFACTURA_BANDEJA.XML;
            dtgrdGeneral.Rows[n].Cells["VERSION"].Value = ocFACTURA_BANDEJA.VERSION;
            dtgrdGeneral.Rows[n].Cells["UUID"].Value = ocFACTURA_BANDEJA.UUID;
        }

        private void cargar_bd_auxiliar(Encapsular oEncapsular)
        {
            BD_Auxiliar = oEncapsular.ocEMPRESA.BD_AUXILIAR + ".dbo.";
            if (oEncapsular.ocEMPRESA.BD_AUXILIAR == "")
            {
                BD_Auxiliar = "";
            }
        }

        public string validacion(string cadena)
        {
            string devolucion = "";
            devolucion = cadena.ToUpper();

            devolucion = devolucion.Replace("á", "A");
            devolucion = devolucion.Replace("é", "E");
            devolucion = devolucion.Replace("í", "I");
            devolucion = devolucion.Replace("ó", "O");
            devolucion = devolucion.Replace("ú", "U");
            devolucion = devolucion.Replace("Á", "A");
            devolucion = devolucion.Replace("É", "E");
            devolucion = devolucion.Replace("Í", "I");
            devolucion = devolucion.Replace("Ó", "O");
            devolucion = devolucion.Replace("Ú", "U");
            devolucion = devolucion.Replace(">", "");
            devolucion = devolucion.Replace("<", "");
            //devolucion = devolucion.Replace("&", "");
            devolucion = devolucion.Replace("'", "");
            devolucion = devolucion.Replace("#", "");
            devolucion = devolucion.Replace("|", "");
            //devolucion = HtmlEncode(devolucion);


            return devolucion; //Regex.Replace(cadena, "[á,é,í,ó,ú,Á,É,Í,Ó,Ú,#,|,&,',>,<]", "");
        }
        private void cargar(DataGridView dtg, string facturasCargar = "")
        {
            try
            {
                dtgrdGeneral.Rows.Clear();
                DataTable oDataTable = new DataTable();
                if (ENTITY_ID.Text == "Todos")
                {
                    //Navegar por los items
                    foreach (var item in ENTITY_ID.Items)
                    {
                        try
                        {
                            cEMPRESA ocEMPRESAtr = item as cEMPRESA;
                            DataTable oDataTabletr = new DataTable();
                            if (ocEMPRESAtr != null)
                            {
                                cCONEXCION oData_ERPp = new cCONEXCION(ocEMPRESAtr.TIPO, ocEMPRESAtr.SERVIDOR
                                    , ocEMPRESAtr.BD, ocEMPRESAtr.USUARIO_BD, ocEMPRESAtr.PASSWORD_BD);
                                cargar_formatos();
                                oDataTable = cargarFactura(oData_ERPp, ocEMPRESAtr, facturasCargar);
                                cargarDataTable(oDataTable, dtgrdGeneral, oData_ERPp, ocEMPRESAtr, false);
                            }

                        }
                        catch (Exception e)
                        {
                            BitacoraFX.Log(e.Message.ToString());
                        }
                    }
                }
                else
                {
                    cEMPRESA ocEMPRESA = ENTITY_ID.SelectedItem as cEMPRESA;
                    if (ocEMPRESA != null)
                    {
                        EMPRESA_NOMBRE.Text = ocEMPRESA.ID;
                        cCONEXCION oData_ERPp = new cCONEXCION(ocEMPRESA.TIPO, ocEMPRESA.SERVIDOR, ocEMPRESA.BD, ocEMPRESA.USUARIO_BD, ocEMPRESA.PASSWORD_BD);
                        cargar_formatos();
                        oDataTable = cargarFactura(oData_ERPp, ocEMPRESA, facturasCargar);
                        cargarDataTable(oDataTable, dtgrdGeneral, oData_ERPp, ocEMPRESA, false);
                    }
                }

            }
            catch (Exception error)
            {
                ErrorFX.mostrar(error, true, true, "frmPrincipalFXr2 - 1994 - ", true);
            }

        }

        private void cargarDataTable(DataTable oDataTable, DataGridView dtg, cCONEXCION oData_ERPp
            , cEMPRESA ocEMPRESA, bool limpiar = true)
        {
            if (oDataTable != null)
            {
                Application.DoEvents();
                if (limpiar)
                {
                    dtg.Rows.Clear();
                }
                toolStripStatusLabel1.Text = "Cargando ";

                int contador = 0;
                foreach (DataRow oDataRow in oDataTable.Rows)
                {
                    contador++;
                    //Ocultar las que dicen: Creada en Facturación y Status en Visual CANCELADAS
                    //sSQL_Where += " AND NOT( ISNULL((SELECT  VMX_FE.ESTADO FROM VMX_FE WHERE VMX_FE.INVOICE_ID=RECEIVABLE.INVOICE_ID),'') LIKE '%Creada%' AND STATUS='X') ";
                    bool mostrar = true;
                    if (OCULTAR.Checked)
                    {
                        if (oDataRow["STATUS"].ToString().Trim() == "X")
                        {
                            if (oDataRow["UUID"].ToString() == "")
                            {
                                mostrar = false;
                            }
                        }
                    }

                    if (mostrar)
                    {
                        int n = dtg.Rows.Add();

                        for (int i = 0; i < dtg.Columns.Count; i++)
                        {
                            dtg.Rows[n].Cells[i].Value = "";
                        }
                        Encapsular oEncapsular = new Encapsular();
                        oEncapsular.oData_ERP = oData_ERPp;
                        oEncapsular.ocEMPRESA = ocEMPRESA;
                        dtg.Rows[n].Tag = oEncapsular;
                        dtg.Rows[n].Cells["Estado"].Value = oDataRow["ESTADO"].ToString();
                        if (!dtg.Rows[n].Cells["Estado"].Value.ToString().Contains("Sellada"))
                        {
                            dtg.Rows[n].DefaultCellStyle.BackColor = Color.Yellow;
                        }

                        dtg.Rows[n].Cells["ARCHIVO"].Value = oDataRow["ARCHIVO"].ToString();
                        dtg.Rows[n].Cells["CANCELADO"].Value = oDataRow["CANCELADO"].ToString();
                        dtg.Rows[n].Cells["VERSION"].Value = oDataRow["VERSION"].ToString();
                        dtg.Rows[n].Cells["INVOICE_ID"].Value = oDataRow["INVOICE_ID"].ToString();
                        dtg.Rows[n].Cells["CUSTOMER_ID"].Value = oDataRow["CUSTOMER_ID"].ToString();
                        dtg.Rows[n].Cells["CUSTOMER_NAME"].Value = oDataRow["CUSTOMER_NAME"].ToString();
                        dtg.Rows[n].Cells["INVOICE_DATE"].Value = DateTime.Parse(oDataRow["INVOICE_DATE"].ToString()).ToString("dd/MM/yyyy") + " " + DateTime.Parse(oDataRow["CREATE_DATE"].ToString()).ToString("HH:mm:ss"); ; //DateTime.Parse(oDataRow["INVOICE_DATE"].ToString()).ToString("dd/MM/yyyy HH:mm:ss");
                        dtg.Rows[n].Cells["CURRENCY_ID"].Value = oDataRow["CURRENCY_ID"].ToString();
                        dtg.Rows[n].Cells["TOTAL_AMOUNT"].Value = oData.Trunca_y_formatea(decimal.Parse(oDataRow["TOTAL_AMOUNT"].ToString()));
                        dtg.Rows[n].Cells["UUID"].Value = oDataRow["UUID"].ToString();
                        dtg.Rows[n].Cells["STATUS"].Value = oDataRow["STATUS"].ToString();

                        //cFACTURA ocFACTURA = new cFACTURA(oData_ERPp);
                        //ocFACTURA.CUSTOMER_ID = oDataRow["CUSTOMER_ID"].ToString();
                        //ocFACTURA.TYPE = oDataRow["TYPE"].ToString();
                        //ocFACTURA.INVOICE_ID = oDataRow["INVOICE_ID"].ToString();
                        //ocFACTURA.cargar_banco(ocEMPRESA);
                        dtg.Rows[n].Cells["USO_CFDI"].Value = oDataRow["USO_CFDI"].ToString();
                        dtg.Rows[n].Cells["FORMA_DE_PAGO"].Value = oDataRow["FORMA_DE_PAGO"].ToString();
                        dtg.Rows[n].Cells["METODO_DE_PAGO"].Value = oDataRow["METODO_DE_PAGO"].ToString();
                        dtg.Rows[n].Cells["CTA_BANCO"].Value = oDataRow["CTA_BANCO"].ToString();
                        dtg.Rows[n].Cells["PDF_ESTADO"].Value = oDataRow["PDF_ESTADO"].ToString();

                        string TIPO = "";
                        if (oDataRow["TYPE"].ToString() == "I")
                        {
                            TIPO = "I";
                        }
                        else
                        {
                            TIPO = "E";
                        }
                        dtg.Rows[n].Cells["TYPE"].Value = TIPO;
                    }


                }
            }
            Application.DoEvents();
            toolStripStatusLabel1.Text = "Fin ";
        }

        private DataTable cargarFactura(cCONEXCION oData_ERPp, cEMPRESA ocEMPRESA, string facturasCargar = "")
        {
            string BD_Visual = "";
            //string BD_Visual = oData_ERPp.sDatabase + ".dbo.";

            string BD_Auxiliar = "";
            if (ocEMPRESA.BD_AUXILIAR != "")
            {
                BD_Auxiliar = ocEMPRESA.BD_AUXILIAR + ".dbo.";
            }
            //if (ocEMPRESA.TIPO == "ORACLE")
            //{
            //    BD_Visual = "";
            //    BD_Auxiliar = "";
            //}

            string sSQL = "";
            string sSQL_FROM = "";
            string sSQL_Where = @"
            WHERE 1=1 
            AND RECEIVABLE.TYPE<>'U' 
            AND RECEIVABLE.TOTAL_AMOUNT<>0
            ";

            //Agregar la entidad
            if (existeENTITY_ID(oData_ERPp))
            {
                if (ocEMPRESA.ENTITY_ID != "")
                {
                    sSQL_Where += @" AND RECEIVABLE.ENTITY_ID ='" + ocEMPRESA.ENTITY_ID + "'";
                }
            }



            try
            {
                string SERIEExcepcion = ConfigurationManager.AppSettings["SERIEExcepcion"].ToString();
                if (SERIEExcepcion != "")
                {
                    sSQL_Where += @" AND NOT(RECEIVABLE.INVOICE_ID IN (" + SERIEExcepcion + "))";
                }

            }
            catch
            {

            }


            string sSQL_Where_tr = "";
            ocUSUARIO = new cUSUARIO();
            ocUSUARIO.cargar_ID(USUARIO);
            if (ocUSUARIO.SERIE.ToString() != "")
            {


                string[] SERIE_tr = ocUSUARIO.SERIE.Split(',');
                for (int i = 0; i < SERIE_tr.Length; i++)
                {
                    if (sSQL_Where_tr == "")
                    {
                        sSQL_Where_tr = " ( RECEIVABLE.INVOICE_ID LIKE '" + SERIE_tr[i] + "%' ";
                    }
                    else
                    {
                        sSQL_Where_tr += " OR RECEIVABLE.INVOICE_ID LIKE '" + SERIE_tr[i] + "%' ";
                    }
                }
            }
            if (sSQL_Where_tr != "")
            {
                sSQL_Where_tr += ")";
                sSQL_Where += " AND " + sSQL_Where_tr;
            }

            sSQL_FROM = " FROM " + BD_Visual + "RECEIVABLE INNER JOIN " + BD_Visual + "CUSTOMER ON ";
            sSQL_FROM += " RECEIVABLE.CUSTOMER_ID = CUSTOMER.ID ";


            string sSQL_Top = "";

            if (TOPtxt.Text.Trim() != "")
            {
                if (oData_ERPp.sTipo == "SQLSERVER")
                {
                    sSQL_Top = " TOP " + TOPtxt.Text;
                }
                if (oData_ERPp.sTipo == "ORACLE")
                {
                    sSQL_Where += " AND rownum < " + TOPtxt.Text;
                }
            }

            if (INVOICE_ID.Text.Trim() != "")
            {
                sSQL_Where += " AND RECEIVABLE.INVOICE_ID LIKE '%" + INVOICE_ID.Text.Trim() + "%'";
            }
            if (SERIE.Text != "")
            {
                sSQL_Where += " AND RECEIVABLE.INVOICE_ID LIKE '%" + SERIE.Text.Trim() + "%'";
            }

            string FECHA_INICIO = oData.convertir_fecha(INICIO.Value, oData_ERPp.sTipo);
            sSQL_Where += " AND RECEIVABLE.INVOICE_DATE>=" + FECHA_INICIO + "";
            string FECHA_FINAL = oData.convertir_fecha(FINAL.Value, oData_ERPp.sTipo);
            sSQL_Where += " AND RECEIVABLE.INVOICE_DATE<=" + FECHA_FINAL + "";

            if (PENDIENTES.Checked)
            {
                sSQL_Where += " AND ISNULL((SELECT VMX_FE.UUID FROM " + BD_Auxiliar + "VMX_FE VMX_FE  WHERE VMX_FE.INVOICE_ID collate SQL_Latin1_General_CP1_CI_AS=RECEIVABLE.INVOICE_ID AND (NOT(ESTADO LIKE '%Test%') AND NOT(ESTADO LIKE '%Creada%')) ),'')='' ";
            }

            if (ESTADO.Text != "")
            {
                sSQL_Where += " AND ISNULL((SELECT VMX_FE.ESTADO FROM " + BD_Auxiliar + "VMX_FE VMX_FE  WHERE VMX_FE.INVOICE_ID  collate SQL_Latin1_General_CP1_CI_AS=RECEIVABLE.INVOICE_ID),'') LIKE '%" + ESTADO.Text + "%' ";
            }

            if (CUSTOMER_NAME.Text != "")
            {
                sSQL_Where += " AND CUSTOMER.NAME LIKE '%" + CUSTOMER_NAME.Text + "%' ";
            }

            if (CUSTOMER_ID.Text != "")
            {
                sSQL_Where += " AND RECEIVABLE.CUSTOMER_ID LIKE '%" + CUSTOMER_ID.Text + "%' ";
            }
            if (MOSTRAR_PDF_ESTADO.Checked)
            {
                sSQL_Where += " AND ISNULL((SELECT VMX_FE.PDF_ESTADO FROM " + BD_Auxiliar + "VMX_FE VMX_FE  WHERE VMX_FE.INVOICE_ID  collate SQL_Latin1_General_CP1_CI_AS=RECEIVABLE.INVOICE_ID),'') LIKE '%Error%' ";
            }
            try
            {
                string rfcOcultos = ConfigurationManager.AppSettings["rfcOcultos"].ToString();
                if (rfcOcultos != "")
                {
                    sSQL_Where += " AND NOT(CUSTOMER.VAT_REGISTRATION like '" + rfcOcultos + "') ";
                }

            }
            catch
            {
            }


            //Ocultar canceladas que no esten Timbradas
            sSQL_Where += " AND ISNULL((SELECT 'S' FROM " + BD_Auxiliar + "VMX_FE VMX_FE  WHERE VMX_FE.INVOICE_ID  collate SQL_Latin1_General_CP1_CI_AS=RECEIVABLE.INVOICE_ID AND RECEIVABLE.TYPE='X'),'N')='N'   ";

            if (facturasCargar != "")
            {
                sSQL_Where += " AND RECEIVABLE.INVOICE_ID IN (" + facturasCargar + ") ";
            }


            ////Validar entidad
            //if (ocEMPRESA.ENTITY_ID.Contains("7"))
            //{
            //    sSQL_Where += " AND ENTITY_ID like '" + ENTITY_ID.Text + "'";
            //}
            //if (ocEMPRESA.ENTITY_ID.Contains("8"))
            //{
            //    sSQL_Where += " AND SITE_ID like '" + ENTITY_ID.Text + "'";
            //}
            //Importante:
            //CUSTOMER_BANK
            //IBAN=METODO DE PAGO
            //SWIFT=FORMA DE PAGO
            //SWIFT=FORMA DE PAGO
            string sSQL_metodoPago = "(SELECT TOP 1 cb.IBAN FROM CUSTOMER_BANK cb WHERE cb.CUSTOMER_ID = RECEIVABLE.CUSTOMER_ID AND IS_DEFAULT=1)";
            string sSQL_formaPago = "(SELECT TOP 1 cb.SWIFT FROM CUSTOMER_BANK cb WHERE cb.CUSTOMER_ID = RECEIVABLE.CUSTOMER_ID AND IS_DEFAULT=1)";
            string sSQL_UsoCFDI = "(SELECT TOP 1 cb.ADDR_3 FROM CUSTOMER_BANK cb WHERE cb.CUSTOMER_ID = RECEIVABLE.CUSTOMER_ID AND IS_DEFAULT=1)";

            string sSQL_metodoPago_defecto = "(SELECT  TOP 1 VMX_FE.METODO_DE_PAGO FROM " + BD_Auxiliar + @"VMX_FE VMX_FE WHERE VMX_FE.INVOICE_ID collate SQL_Latin1_General_CP1_CI_AS=(SELECT TOP 1 r.INVOICE_ID FROM RECEIVABLE r WHERE r.CUSTOMER_ID=RECEIVABLE.CUSTOMER_ID ORDER BY r.INVOICE_DATE DESC) ORDER BY VMX_FE.ROW_ID DESC)";

            string sSQL_formaPago_defecto = "(SELECT  TOP 1 VMX_FE.FORMA_DE_PAGO FROM " + BD_Auxiliar + @"VMX_FE VMX_FE WHERE VMX_FE.INVOICE_ID collate SQL_Latin1_General_CP1_CI_AS=(SELECT TOP 1 r.INVOICE_ID FROM RECEIVABLE r WHERE r.CUSTOMER_ID=RECEIVABLE.CUSTOMER_ID ORDER BY r.INVOICE_DATE DESC) ORDER BY VMX_FE.ROW_ID DESC)";

            string sSQL_UsoCFDI_defecto = "(SELECT  TOP 1 VMX_FE.USO_CFDI FROM " + BD_Auxiliar + @"VMX_FE VMX_FE WHERE VMX_FE.INVOICE_ID collate SQL_Latin1_General_CP1_CI_AS=(SELECT TOP 1 r.INVOICE_ID FROM RECEIVABLE r WHERE r.CUSTOMER_ID=RECEIVABLE.CUSTOMER_ID ORDER BY r.INVOICE_DATE DESC) ORDER BY VMX_FE.ROW_ID DESC)";

            sSQL = " SELECT " + sSQL_Top + @" RECEIVABLE.INVOICE_ID as INVOICE_ID, RECEIVABLE.CUSTOMER_ID as CUSTOMER_ID ";
            sSQL += ", RECEIVABLE.INVOICE_DATE as INVOICE_DATE, RECEIVABLE.CREATE_DATE as CREATE_DATE, RECEIVABLE.STATUS as STATUS ";
            sSQL += ", (SELECT  TOP 1 VMX_FE.ESTADO FROM " + BD_Auxiliar + @"VMX_FE VMX_FE WHERE VMX_FE.INVOICE_ID  collate SQL_Latin1_General_CP1_CI_AS=RECEIVABLE.INVOICE_ID) as ESTADO ";
            sSQL += ", (SELECT  TOP 1 CUSTOMER.NAME FROM CUSTOMER WHERE CUSTOMER.ID=RECEIVABLE.CUSTOMER_ID) as CUSTOMER_NAME ";
            sSQL += ", (SELECT  TOP 1 VMX_FE.XML FROM " + BD_Auxiliar + @"VMX_FE VMX_FE WHERE VMX_FE.INVOICE_ID  collate SQL_Latin1_General_CP1_CI_AS=RECEIVABLE.INVOICE_ID) as ARCHIVO ";
            sSQL += ", (SELECT  TOP 1 VMX_FE.UUID FROM " + BD_Auxiliar + @"VMX_FE VMX_FE WHERE VMX_FE.INVOICE_ID  collate SQL_Latin1_General_CP1_CI_AS=RECEIVABLE.INVOICE_ID) as UUID ";
            sSQL += ", (SELECT  TOP 1 VMX_FE.CANCELADO FROM " + BD_Auxiliar + @"VMX_FE VMX_FE WHERE VMX_FE.INVOICE_ID collate SQL_Latin1_General_CP1_CI_AS=RECEIVABLE.INVOICE_ID) as CANCELADO ";
            sSQL += ", (SELECT  TOP 1 VMX_FE.PDF_ESTADO FROM " + BD_Auxiliar + @"VMX_FE VMX_FE WHERE VMX_FE.INVOICE_ID collate SQL_Latin1_General_CP1_CI_AS=RECEIVABLE.INVOICE_ID) as PDF_ESTADO ";
            sSQL += ", ISNULL((SELECT  TOP 1 VMX_FE.USO_CFDI FROM " + BD_Auxiliar + @"VMX_FE VMX_FE WHERE VMX_FE.INVOICE_ID collate SQL_Latin1_General_CP1_CI_AS=RECEIVABLE.INVOICE_ID ORDER BY VMX_FE.ROW_ID DESC),ISNULL(" + sSQL_UsoCFDI_defecto + " ," + sSQL_UsoCFDI + ")) as USO_CFDI ";
            sSQL += ", (SELECT  TOP 1 VMX_FE.VERSION FROM " + BD_Auxiliar + @"VMX_FE VMX_FE WHERE VMX_FE.INVOICE_ID collate SQL_Latin1_General_CP1_CI_AS=RECEIVABLE.INVOICE_ID) as VERSION ";
            sSQL += ", ISNULL((SELECT  TOP 1 VMX_FE.METODO_DE_PAGO FROM " + BD_Auxiliar + @"VMX_FE VMX_FE WHERE VMX_FE.INVOICE_ID collate SQL_Latin1_General_CP1_CI_AS=RECEIVABLE.INVOICE_ID ORDER BY VMX_FE.ROW_ID DESC),ISNULL(" + sSQL_metodoPago_defecto + "  ," + sSQL_metodoPago + ")) as METODO_DE_PAGO ";
            sSQL += ", ISNULL((SELECT  TOP 1 VMX_FE.FORMA_DE_PAGO FROM " + BD_Auxiliar + @"VMX_FE VMX_FE WHERE VMX_FE.INVOICE_ID collate SQL_Latin1_General_CP1_CI_AS=RECEIVABLE.INVOICE_ID ORDER BY VMX_FE.ROW_ID DESC),ISNULL(" + sSQL_formaPago_defecto + "  ," + sSQL_formaPago + ")) as FORMA_DE_PAGO ";
            sSQL += ", ISNULL((SELECT  TOP 1 VMX_FE.CTA_BANCO FROM " + BD_Auxiliar + @"VMX_FE VMX_FE WHERE VMX_FE.INVOICE_ID collate SQL_Latin1_General_CP1_CI_AS=RECEIVABLE.INVOICE_ID),'') as CTA_BANCO ";
            sSQL += ", RECEIVABLE.CURRENCY_ID as CURRENCY_ID, RECEIVABLE.STATUS as STATUS, RECEIVABLE.TYPE as TYPE ";
            sSQL += ", RECEIVABLE.TOTAL_AMOUNT  as TOTAL_AMOUNT";
            sSQL += ", CUSTOMER.NAME as NAME ";
            sSQL += sSQL_FROM;

            sSQL += " " + sSQL_Where;

            sSQL += " ORDER BY RECEIVABLE.INVOICE_ID DESC ";

            if (oData_ERPp.sTipo == "ORACLE")
            {
                sSQL = sSQL.Replace("ISNULL(", "NVL(");

                sSQL = sSQL.Replace("collate SQL_Latin1_General_CP1_CI_AS", "");

            }


            limpiarDuplicidad(oData_ERPp, BD_Auxiliar, ocEMPRESA);

            sSQL = aplicarORACLE(sSQL);

            DataTable oDataTable = oData_ERPp.EjecutarConsulta(sSQL); ;


            return oDataTable;
        }

        private bool existeENTITY_ID(cCONEXCION oData_ERPp)
        {
            try
            {
                string sSQL = @"
                SELECT TOP 1 *
                FROM INFORMATION_SCHEMA.COLUMNS
                WHERE 
                [TABLE_NAME] = 'RECEIVABLE'
                AND[COLUMN_NAME] = 'ENTITY_ID'
                ";
                DataTable oDataTable1 = oData_ERPp.EjecutarConsulta(sSQL);
                if (oDataTable1.Rows.Count == 0)
                {
                    return false;
                }
                return true;
            }
            catch (Exception error)
            {
                ErrorFX.mostrar(error, false, false, "frmPrincipalFXr2 - 2318 - ", false);
            }
            return false;
        }

        private string aplicarORACLE(string sSQL)
        {
            string sSQLTemp = sSQL;
            //Buscar Alias de VMX_FE.INVOICE_ID
            try
            {
                string VMX_FE_INVOICE_ID = ConfigurationManager.AppSettings["VMX_FE.INVOICE_ID"].ToString();
                if (VMX_FE_INVOICE_ID != "")
                {
                    sSQLTemp = sSQLTemp.Replace("VMX_FE.INVOICE_ID", "FXINV000.ID");
                }

            }
            catch
            {
            }

            //Buscar Alias de VMX_FE
            try
            {
                string TABLA_VMX_FE = ConfigurationManager.AppSettings["VMX_FE"].ToString();
                if (TABLA_VMX_FE != "")
                {
                    sSQLTemp = sSQLTemp.Replace("VMX_FE", "FXINV000");
                }

            }
            catch
            {
            }

            return sSQLTemp;
        }

        private void limpiarDuplicidad(cCONEXCION oData_ERP, string BD_AUXILIAR, cEMPRESA ocEMPRESA)
        {
            if (ocEMPRESA.TIPO == "ORACLE")
            {
                return;
            }
            //Limpiar duplicidad
            string sSQL_duplicidad = " SELECT COUNT(*),[INVOICE_ID] " +
            " FROM " + BD_AUXILIAR + "[VMX_FE] " +
            " GROUP BY [INVOICE_ID] " +
            " HAVING  COUNT(*)>1 ";
            DataTable oDataTable_duplicidad = oData_ERP.EjecutarConsulta(sSQL_duplicidad);
            if (oDataTable_duplicidad != null)
            {
                foreach (DataRow oDataRow_duplicidad in oDataTable_duplicidad.Rows)
                {
                    sSQL_duplicidad = " DELETE TOP(1) FROM " + BD_AUXILIAR + "VMX_FE WHERE INVOICE_ID='" + oDataRow_duplicidad["INVOICE_ID"].ToString() + "'";
                    oData_ERP.EjecutarConsulta(sSQL_duplicidad);
                }
            }
            //Fin de limpiar duplicidad
        }

        private void toolStripButton9_Click(object sender, EventArgs e)
        {

            abrir_pdf();
        }

        private void abrir_pdf()
        {
            if (dtgrdGeneral.SelectedRows.Count > 0)
            {
                DataGridViewSelectedRowCollection arrSelectedRows = dtgrdGeneral.SelectedRows;

                int d = 0;
                for (int n = arrSelectedRows.Count - 1; n >= 0; n--)
                {
                    string INVOICE_ID = arrSelectedRows[n].Cells["INVOICE_ID"].Value.ToString();
                    Encapsular oEncapsular = (Encapsular)arrSelectedRows[n].Tag;
                    string sDirectory = AppDomain.CurrentDomain.BaseDirectory;
                    cFACTURA_BANDEJA ocFACTURA_BANDEJA = new cFACTURA_BANDEJA(oEncapsular.oData_ERP, oEncapsular.ocEMPRESA);
                    ocFACTURA_BANDEJA.cargar_ID(INVOICE_ID, oEncapsular.ocEMPRESA);

                    string direccion = ocFACTURA_BANDEJA.PDF;

                    if (!File.Exists(direccion))
                    {
                        //direccion = sDirectory + @"\" + direccion;
                        if (!File.Exists(direccion))
                        {
                            MessageBox.Show("No se puede tener acceso a: " + direccion);
                            return;
                        }
                    }

                    //frmFacturaXML ofrmFacturaXML = new frmFacturaXML(direccion);
                    //ofrmFacturaXML.Show();

                    frmPDF ofrmPDF = new frmPDF(direccion);
                    ofrmPDF.Show();
                }
            }
        }

        private bool generar_pdf(string INVOICE_ID, Encapsular oEncapsular)
        {
            try
            {
                //MessageBox.Show("Generar PDF");
                //Generar el PDF
                cFACTURA oFACTURA = new cFACTURA(oEncapsular.oData_ERP);
                //MessageBox.Show("Generar Factura");
                oFACTURA.datos(INVOICE_ID, "", "", oEncapsular.ocEMPRESA, "");
                //MessageBox.Show(oData_ERP.sTipo);
                cGeneracion ocGeneracion = new cGeneracion(oData, oEncapsular.oData_ERP);
                //MessageBox.Show("Generar Factura Bandeja");
                cFACTURA_BANDEJA ocFACTURA_BANDEJA = new cFACTURA_BANDEJA(oEncapsular.oData_ERP, oEncapsular.ocEMPRESA);
                ocFACTURA_BANDEJA.cargar_ID(INVOICE_ID, oEncapsular.ocEMPRESA);
                //Actualizar el Metodo y forma de Pago del XML
                ocFACTURA_BANDEJA.actualizar_datos_xml(ocFACTURA_BANDEJA.XML, oFACTURA, oEncapsular);
                ocFACTURA_BANDEJA.cargar_ID(INVOICE_ID, oEncapsular.ocEMPRESA);
                return ocGeneracion.pdf(INVOICE_ID, oFACTURA, ocFACTURA_BANDEJA, oEncapsular.ocEMPRESA.ROW_ID, FORMATO.Text);
            }
            catch (Exception error)
            {
                ErrorFX.mostrar(error, true, true, "frmPrincipalFXr2 - 18001 ", false);
            }
            return false;

        }

        private void toolStripButton11_Click(object sender, EventArgs e)
        {
            generar_addenda_todos();
        }

        private void generar_addenda_todos()
        {
            if (dtgrdGeneral.SelectedRows.Count > 0)
            {
                DataGridViewSelectedRowCollection arrSelectedRows = dtgrdGeneral.SelectedRows;

                int d = 0;
                for (int n = arrSelectedRows.Count - 1; n >= 0; n--)
                {
                    string INVOICE_ID = arrSelectedRows[n].Cells["INVOICE_ID"].Value.ToString();
                    string sDirectory = AppDomain.CurrentDomain.BaseDirectory;
                    Encapsular oEncapsular = (Encapsular)arrSelectedRows[0].Tag;
                    cFACTURA_BANDEJA ocFACTURA_BANDEJA = new cFACTURA_BANDEJA(oEncapsular.oData_ERP
                        , oEncapsular.ocEMPRESA);
                    ocFACTURA_BANDEJA.cargar_ID(INVOICE_ID, oEncapsular.ocEMPRESA);

                    //Generar la Addenda
                    cFACTURA oFACTURA = new cFACTURA(oData);
                    oFACTURA.datos(INVOICE_ID, "", "", oEncapsular.ocEMPRESA, "");
                }
            }
        }

        private void generar_UUID()
        {
            if (dtgrdGeneral.SelectedRows.Count > 0)
            {
                DataGridViewSelectedRowCollection arrSelectedRows = dtgrdGeneral.SelectedRows;

                int d = 0;
                bool existe_error = false;
                for (int n = arrSelectedRows.Count - 1; n >= 0; n--)
                {

                    string INVOICE_ID = arrSelectedRows[n].Cells["INVOICE_ID"].Value.ToString();
                    Encapsular oEncapsular = (Encapsular)arrSelectedRows[0].Tag;
                    cGeneracion ocGeneracion = new cGeneracion(oData, oEncapsular.oData_ERP);
                    int indice = arrSelectedRows[n].Index;
                    cFACTURA ocFACTURA = new cFACTURA(oEncapsular.oData_ERP);
                    string SERIE_tr = INVOICE_ID.Replace("0", "").Replace("1", "").Replace("2", "").Replace("3", "").Replace("4", "").Replace("5", "").Replace("6", "").Replace("7", "").Replace("8", "").Replace("9", "").Trim();
                    cSERIE ocSERIE = new cSERIE();
                    cEMPRESA ocEMPRESA = new cEMPRESA(oData);
                    ocEMPRESA.cargar((ENTITY_ID.SelectedItem as cEMPRESA).ROW_ID);
                    if (!ocSERIE.cargar_serie(SERIE_tr, ocEMPRESA.ROW_ID))
                    {
                        MessageBox.Show("La Serie " + SERIE_tr + " no esta configurada.", Application.ProductName + "-" + Application.ProductVersion, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                        return;
                    }

                    cCERTIFICADO ocCERTIFICADO = new cCERTIFICADO();
                    ocCERTIFICADO.cargar(ocSERIE.ROW_ID_CERTIFICADO);
                    //cEMPRESA ocEMPRESA = new cEMPRESA(oData);
                    ocEMPRESA.cargar(ocCERTIFICADO.ROW_ID_EMPRESA);


                    ocFACTURA.datos(INVOICE_ID, ocSERIE.ACCOUNT_ID_RETENIDO, "", ocEMPRESA, ocSERIE.ACCOUNT_ID_ANTICIPO);

                    generar_CFDI_UUID(indice, Globales.oCFDI_USUARIO.USUARIO, Globales.oCFDI_USUARIO.PASSWORD, ocFACTURA, ocSERIE);
                }
                if (!existe_error)
                {
                    MessageBox.Show("Las Facturas fueron regeneradas.", Application.ProductName + "-" + Application.ProductVersion, MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
        }


        private void toolStripButton12_Click(object sender, EventArgs e)
        {
            generar_UUID();
        }

        private void buttonItem8_Click(object sender, EventArgs e)
        {
            actualizar();
        }

        private void actualizar()
        {
            oTimer.Stop();
            progressBar1.Visible = true;
            Application.DoEvents();
            cargar(dtgrdGeneral);
            Application.DoEvents();
            progressBar1.Visible = false;

        }

        private void buttonItem2_Click_1(object sender, EventArgs e)
        {
            procesar_factura("3.3");
        }

        public void procesar_factura(string version)
        {
            if (procesar)
            {
                try
                {
                    procesar = false;
                    Application.DoEvents();
                    progressBar1.Visible = true;
                    Application.DoEvents();
                    try
                    {
                        generar_FE(version, CFDI_PRUEBA.Checked);
                    }
                    catch (Exception exception)
                    {
                        procesar = true;
                        ErrorFX.mostrar(exception, false, true, "frmPrincipalFXr2 - 2576 - ", false);
                        toolStripStatusLabel1.Text = "Error " + DateTime.Now.ToShortDateString() + " " + DateTime.Now.ToLongTimeString();
                    }
                    Application.DoEvents();
                    progressBar1.Visible = false;
                    Application.DoEvents();
                    procesar = true;
                }
                catch
                {
                    procesar = true;
                }
                finally
                {
                    procesar = true;
                }
            }

        }

        private void buttonItem14_Click(object sender, EventArgs e)
        {
            ver_pdf();
        }
        private void regenerar_pdf()
        {
            if (dtgrdGeneral.SelectedRows.Count > 0)
            {
                MessageBox.Show("Recuerde que debe cerrar el PDF para poder regenerar el archivo.", Application.ProductName
                    , MessageBoxButtons.OK, MessageBoxIcon.Information);
                DataGridViewSelectedRowCollection arrSelectedRows = dtgrdGeneral.SelectedRows;

                int d = 0;
                for (int n = arrSelectedRows.Count - 1; n >= 0; n--)
                {
                    Encapsular oEncapsular = (Encapsular)arrSelectedRows[n].Tag;
                    string INVOICE_ID = arrSelectedRows[n].Cells["INVOICE_ID"].Value.ToString();
                    generar_pdf(INVOICE_ID, oEncapsular);
                }
                MessageBox.Show("PDF de las facturas regenerados.", Application.ProductName
                    , MessageBoxButtons.OK, MessageBoxIcon.Information);
            }

        }

        private void INVOICE_ID_Click(object sender, EventArgs e)
        {

        }

        private void buttonMargins_Click(object sender, EventArgs e)
        {
            exportar();
        }

        private void exportar()
        {
            var fileName = "Facturas " + DateTime.Now.ToString("yyyy-MM-dd--hh-mm-ss") + ".xlsx";
            if (folderBrowserDialog1.ShowDialog() == DialogResult.OK)
            {
                fileName = folderBrowserDialog1.SelectedPath + "\\" + fileName;
                System.IO.Stream myStream = new System.IO.FileStream(fileName, System.IO.FileMode.Create, System.IO.FileAccess.ReadWrite, System.IO.FileShare.None);

                using (ExcelPackage pck = new ExcelPackage())
                {
                    DataTable oDataTable = new DataTable();

                    oDataTable = GetDataTableFromDGV(dtgrdGeneral);
                    ExcelWorksheet ws1 = pck.Workbook.Worksheets.Add("Escenarios");
                    ws1.Cells["A1"].LoadFromDataTable(oDataTable, true);
                    pck.SaveAs(myStream);
                }
                myStream.Close();
                MessageBox.Show("Exportación del archivo " + Environment.NewLine + fileName + " finalizada.", Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Information);
            }

        }

        private DataTable GetDataTableFromDGV(DataGridView dgv)
        {
            var dt = new DataTable();
            foreach (DataGridViewColumn column in dgv.Columns)
            {
                //if (column.Visible)
                //{
                // You could potentially name the column based on the DGV column name (beware of dupes)
                // or assign a type based on the data type of the data bound to this DGV column.
                dt.Columns.Add(column.HeaderText);
                //}
            }

            object[] cellValues = new object[dgv.Columns.Count];
            foreach (DataGridViewRow row in dgv.Rows)
            {
                for (int i = 0; i < row.Cells.Count; i++)
                {
                    cellValues[i] = row.Cells[i].Value;
                }
                dt.Rows.Add(cellValues);
            }

            return dt;
        }


        //private void exportar(DataGridView datagridview, bool captions)
        //{
        //    object objApp_Late;
        //    object objBook_Late;
        //    object objBooks_Late;
        //    object objSheets_Late;
        //    object objSheet_Late;
        //    object objRange_Late;
        //    object[] Parameters;
        //    string[] headers = new string[datagridview.ColumnCount - 1];
        //    string[] columns = new string[datagridview.ColumnCount - 1];

        //    int i = 0;
        //    int c = 0;
        //    for (c = 0; c < datagridview.ColumnCount - 1; c++)
        //    {
        //        headers[c] = datagridview.Rows[0].Cells[c].OwningColumn.Name.ToString();
        //        i = c + 65;
        //        columns[c] = Convert.ToString((char)i);
        //    }

        //    //try
        //    //{
        //    // Get the class type and instantiate Excel.
        //    Type objClassType;
        //    objClassType = Type.GetTypeFromProgID("Excel.Application");
        //    objApp_Late = Activator.CreateInstance(objClassType);
        //    //Get the workbooks collection.
        //    objBooks_Late = objApp_Late.GetType().InvokeMember("Workbooks",
        //    BindingFlags.GetProperty, null, objApp_Late, null);
        //    //Add a new workbook.
        //    objBook_Late = objBooks_Late.GetType().InvokeMember("Add",
        //    BindingFlags.InvokeMethod, null, objBooks_Late, null);
        //    //Get the worksheets collection.
        //    objSheets_Late = objBook_Late.GetType().InvokeMember("Worksheets",
        //    BindingFlags.GetProperty, null, objBook_Late, null);
        //    //Get the first worksheet.
        //    Parameters = new Object[1];
        //    Parameters[0] = 1;
        //    objSheet_Late = objSheets_Late.GetType().InvokeMember("Item",
        //    BindingFlags.GetProperty, null, objSheets_Late, Parameters);

        //    if (captions)
        //    {
        //        toolStripStatusLabel1.Text = "Exportando Cabeceras ";
        //        // Create the headers in the first row of the sheet
        //        for (c = 0; c < datagridview.ColumnCount - 1; c++)
        //        {
        //            //Get a range object that contains cell.
        //            Parameters = new Object[2];
        //            Parameters[0] = columns[c] + "1";
        //            Parameters[1] = Missing.Value;
        //            objRange_Late = objSheet_Late.GetType().InvokeMember("Range",
        //            BindingFlags.GetProperty, null, objSheet_Late, Parameters);
        //            //Write Headers in cell.
        //            Parameters = new Object[1];
        //            Parameters[0] = headers[c];
        //            objRange_Late.GetType().InvokeMember("Value", BindingFlags.SetProperty, null, objRange_Late, Parameters);
        //        }
        //    }

        //    toolStripStatusLabel1.Text = "Exportando Datos ";

        //    // Now add the data from the grid to the sheet starting in row 2
        //    for (i = 0; i < datagridview.RowCount; i++)
        //    {
        //        for (c = 0; c < datagridview.ColumnCount - 1; c++)
        //        {
        //            //Get a range object that contains cell.
        //            Parameters = new Object[2];
        //            Parameters[0] = columns[c] + Convert.ToString(i + 2);
        //            Parameters[1] = Missing.Value;
        //            objRange_Late = objSheet_Late.GetType().InvokeMember("Range",
        //            BindingFlags.GetProperty, null, objSheet_Late, Parameters);
        //            //Write Headers in cell.
        //            Parameters = new Object[1];

        //            string valor = "";
        //            if (datagridview.Rows[i].Cells[headers[c]].Value != null)
        //            {
        //                valor = datagridview.Rows[i].Cells[headers[c]].Value.ToString();
        //            }

        //            Parameters[0] = valor.ToString();
        //            objRange_Late.GetType().InvokeMember("Value", BindingFlags.SetProperty,
        //            null, objRange_Late, Parameters);
        //        }
        //    }

        //    //Return control of Excel to the user.
        //    Parameters = new Object[1];
        //    Parameters[0] = true;
        //    objApp_Late.GetType().InvokeMember("Visible", BindingFlags.SetProperty,
        //    null, objApp_Late, Parameters);
        //    objApp_Late.GetType().InvokeMember("UserControl", BindingFlags.SetProperty,
        //    null, objApp_Late, Parameters);
        //}

        private void buttonItem13_Click(object sender, EventArgs e)
        {
            generar_UUID();
        }

        private void buttonItem12_Click(object sender, EventArgs e)
        {
            ver_xml();
        }

        private void buttonItem23_Click(object sender, EventArgs e)
        {
            string url = ConfigurationManager.AppSettings["SOPORTE"].ToString(); ;
            System.Diagnostics.Process.Start(url);
        }

        private void buttonItem31_Click(object sender, EventArgs e)
        {
            string url = "TeamViewerQS.exe";
            System.Diagnostics.Process.Start(url);
        }

        private void buttonItem19_Click(object sender, EventArgs e)
        {
            enviar_mail(true);
        }


        private void enviar_mail(bool adjuntar)
        {
            if (dtgrdGeneral.SelectedRows.Count > 0)
            {
                DataGridViewSelectedRowCollection arrSelectedRows = dtgrdGeneral.SelectedRows;
                for (int n = arrSelectedRows.Count - 1; n >= 0; n--)
                {
                    int indice = arrSelectedRows[n].Index;
                    string INVOICE_ID = arrSelectedRows[n].Cells["INVOICE_ID"].Value.ToString();
                    Encapsular oEncapsular = (Encapsular)arrSelectedRows[n].Tag;
                    enviar(oEncapsular, INVOICE_ID, true);

                }
                if (this.Visible)
                {
                    MessageBox.Show("Facturas en Proceso de Envio.");
                }
            }
        }

        private void enviar(Encapsular oEncapsular, string INVOICE_ID, bool adjuntar)
        {
            cFACTURA oFACTURA = new cFACTURA(oEncapsular.oData_ERP);
            oFACTURA.datos(INVOICE_ID, "", "", oEncapsular.ocEMPRESA, "");
            cEMPRESA ocEMPRESA = new cEMPRESA();

            informacion.Text = "Enviando: " + oFACTURA.CONTACT_EMAIL;

            Enviar oEnviar = new Enviar();
            if (oEnviar.enviar(oFACTURA, adjuntar, false, false, oEncapsular, oData))
            {
                informacion.Text = "Enviado a " + oFACTURA.CONTACT_EMAIL;
            }
            else
            {
                informacion.Text = "Error a " + oFACTURA.CONTACT_EMAIL;
            }
        }

        private void buttonItem4_Click(object sender, EventArgs e)
        {
            frmEmpresas oObjeto = new frmEmpresas(oData.sConn);
            oObjeto.ShowDialog();
        }

        private void buttonItem5_Click(object sender, EventArgs e)
        {
            frmSeries oObjeto = new frmSeries(oData.sConn);
            oObjeto.ShowDialog();
        }

        private void buttonItem6_Click(object sender, EventArgs e)
        {
            frmCertificados oObjeto = new frmCertificados(oData.sConn);
            oObjeto.ShowDialog();
        }

        private void ENTITY_ID_SelectedIndexChanged(object sender, EventArgs e)
        {
            actualizar_conexcion();
        }

        private void actualizar_conexcion()
        {
            try
            {
                if (ENTITY_ID.SelectedText == "Todos")
                {
                    EMPRESA_NOMBRE.Text = "Todas las empresas";
                }
                else
                {
                    cEMPRESA ocEMPRESA = ENTITY_ID.SelectedItem as cEMPRESA;
                    if (ocEMPRESA != null)
                    {
                        EMPRESA_NOMBRE.Text = ocEMPRESA.ID;
                        cargar_formatos();
                        cargar(dtgrdGeneral);
                    }
                }
            }
            catch (Exception e)
            {

            }
        }

        private void buttonItem1_Click(object sender, EventArgs e)
        {
            cambiar_facturas();
        }

        private void cambiar_facturas()
        {
            if (dtgrdGeneral.SelectedRows.Count > 0)
            {
                DataGridViewSelectedRowCollection arrSelectedRows = dtgrdGeneral.SelectedRows;
                for (int n = arrSelectedRows.Count - 1; n >= 0; n--)
                {
                    int indice = arrSelectedRows[n].Index;
                    string INVOICE_ID = arrSelectedRows[n].Cells["INVOICE_ID"].Value.ToString();
                    string CUSTOMER_ID = arrSelectedRows[n].Cells["CUSTOMER_ID"].Value.ToString();
                    string UUID = arrSelectedRows[n].Cells["UUID"].Value.ToString();
                    if ((UUID != null) || (UUID != ""))
                    {
                        cCONEXCION oData_tr = oData;
                        Encapsular oEncapsular = (Encapsular)arrSelectedRows[n].Tag;
                        //MessageBox.Show("Antes de cargar");
                        frmMetodo_Cuenta ofrmMetodo_Cuenta = new frmMetodo_Cuenta(oEncapsular.oData_ERP.sConn
                            , oEncapsular.oData_ERP.sTipo, INVOICE_ID, oEncapsular.ocEMPRESA
                            , oData_tr, CUSTOMER_ID);
                        ofrmMetodo_Cuenta.ShowDialog();
                        //oData.leer_datos();
                        //MessageBox.Show("Despues de cargar");
                        cFACTURA_BANDEJA ocFACTURA_BANDEJA = new cFACTURA_BANDEJA(oEncapsular.oData_ERP, oEncapsular.ocEMPRESA);
                        ocFACTURA_BANDEJA.cargar_ID(INVOICE_ID, oEncapsular.ocEMPRESA);
                        arrSelectedRows[n].Cells["METODO_DE_PAGO"].Value = ocFACTURA_BANDEJA.METODO_DE_PAGO;
                        arrSelectedRows[n].Cells["CTA_BANCO"].Value = ocFACTURA_BANDEJA.CTA_BANCO;
                        arrSelectedRows[n].Cells["FORMA_DE_PAGO"].Value = ocFACTURA_BANDEJA.FORMA_DE_PAGO;
                        arrSelectedRows[n].Cells["USO_CFDI"].Value = ocFACTURA_BANDEJA.USO_CFDI;
                    }
                    else
                    {
                        MessageBox.Show("No puede modificar la Factura, se encuetra Timbrada");
                    }
                }
            }
        }

        private void buttonExit_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void buttonItem10_Click(object sender, EventArgs e)
        {
            frmUsuarios oObjeto = new frmUsuarios(oData.sConn);
            oObjeto.ShowDialog();
        }

        private void buttonItem3_Click(object sender, EventArgs e)
        {
            cancelar();
        }
        public bool cancelar(string factura)
        {
            ////cargar_empresas();

            ////cFACTURA_BANDEJA ocFACTURA_BANDEJA = new cFACTURA_BANDEJA(oData_ERP, ocEMPRESA);

            ////if (ocFACTURA_BANDEJA.cargar_ID(factura, ocEMPRESA))
            ////{
            ////    cFACTURA oFACTURA = new cFACTURA(oData_ERP);
            ////    //Validar que este cancelada en Visual
            ////    oFACTURA.datos(factura, "", "", ocEMPRESA, "");
            ////    return cancelar_CFDI(oFACTURA, ocFACTURA_BANDEJA.UUID);
            ////}
            ////return false;
            return false;
        }

        private void cancelar()
        {
            if (dtgrdGeneral.SelectedRows.Count > 0)
            {
                DataGridViewSelectedRowCollection arrSelectedRows = dtgrdGeneral.SelectedRows;
                for (int n = arrSelectedRows.Count - 1; n >= 0; n--)
                {
                    int indice = arrSelectedRows[n].Index;
                    string INVOICE_ID = arrSelectedRows[n].Cells["INVOICE_ID"].Value.ToString();
                    string UUID = arrSelectedRows[n].Cells["UUID"].Value.ToString();

                    if (UUID != "")
                    {
                        Encapsular oEncapsular = (Encapsular)arrSelectedRows[n].Tag;
                        cFACTURA oFACTURA = new cFACTURA(oEncapsular.oData_ERP);
                        //Validar que este cancelada en Visual

                        oFACTURA.datos(INVOICE_ID, "", "", oEncapsular.ocEMPRESA, "");
                        //if (oFACTURA.STATUS == "X")
                        //{
                            if (cancelar_CFDI(oFACTURA, UUID, oEncapsular.oData_ERP))
                            {
                                Enviar oEnviar = new Enviar();
                                oEnviar.enviar(oFACTURA, false, true, true, oEncapsular, oData);
                            }
                        //}
                        //else
                        //{
                        //    MessageBox.Show("La factura " + INVOICE_ID + " debe ser cancelada en Visual.");
                        //    return;
                        //}
                    }

                }
                MessageBox.Show("Facturas canceladas.", Application.ProductName
                   , MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private bool cancelar_CFDI(cFACTURA ocFACTURA, string UUID, cCONEXCION oData_ERP)
        {
            string SERIE_tr = ocFACTURA.INVOICE_ID.Replace("0", "").Replace("1", "").Replace("2", "").Replace("3", "").Replace("4", "").Replace("5", "").Replace("6", "").Replace("7", "").Replace("8", "").Replace("9", "").Trim();
            cSERIE ocSERIE = new cSERIE();
            cEMPRESA ocEMPRESA = new cEMPRESA(oData);
            ocEMPRESA.cargar((ENTITY_ID.SelectedItem as cEMPRESA).ROW_ID);
            if (!ocSERIE.cargar_serie(SERIE_tr, ocEMPRESA.ROW_ID))
            {
                MessageBox.Show("La Serie " + SERIE_tr + " no esta configurada.", Application.ProductName + "-" + Application.ProductVersion, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return false;
            }


            //Cargar Certificado
            cCERTIFICADO ocCERTIFICADO = new cCERTIFICADO();
            ocCERTIFICADO.cargar(ocSERIE.ROW_ID_CERTIFICADO);
            //cEMPRESA ocEMPRESA = new cEMPRESA(oData);
            ocEMPRESA.cargar(ocCERTIFICADO.ROW_ID_EMPRESA);

            //Llamar el Servicio
            CFDiClient oCFDiClient = new CFDiClient();
            string[] uuid = new string[1];
            uuid[0] = UUID;

            ////Cargar los datos de la Compañia apartir del Certificado
            //X509Certificate cert = X509Certificate.CreateFromCertFile(ocCERTIFICADO.CERTIFICADO);
            //byte[] certData = cert.Export(X509ContentType.Pfx, ocCERTIFICADO.PASSWORD);
            //if(File.Exists("TEMPORAL.pfx"))
            //{
            //    File.Delete("TEMPORAL.pfx");
            //}
            ////File.WriteAllBytes("TEMPORAL.pfx", certData);

            //Cargar el archivo pfx creado desde OpenSSL
            string pfx = ocCERTIFICADO.PFX;
            if (!File.Exists(pfx))
            {
                //Validar el directorio

                string directorio = AppDomain.CurrentDomain.BaseDirectory;
                pfx = directorio + @"\" + ocCERTIFICADO.PFX;
                if (!File.Exists(pfx))
                {
                    MessageBox.Show("El PFX " + ocCERTIFICADO.PFX + " no existe."
                        , Application.ProductName + "-" + Application.ProductVersion, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                    return false;
                }
            }
            byte[] pfx_archivo = ReadBinaryFile(pfx);
            try
            {
                CancelaResponse respuesta;
                //Todo: Realizar cancelacion
                //respuesta = oCFDiClient.cancelaCFDi(Globales.oCFDI_USUARIO.USUARIO, Globales.oCFDI_USUARIO.PASSWORD, ocEMPRESA.RFC, uuid, pfx_archivo, ocCERTIFICADO.PASSWORD);
                string tabla = VMX_FE_Tabla;
                if (ocEMPRESA.BD_AUXILIAR != "")
                {
                    tabla = ocEMPRESA.BD_AUXILIAR + ".dbo." + tabla;
                }

                string sSQL = "UPDATE " + tabla + " SET ESTADO='Cancelado EDICOM', CANCELADO='Cancelado EDICOM' ";
                sSQL += " WHERE INVOICE_ID='" + ocFACTURA.INVOICE_ID + "'";
                sSQL = actualizar_invoice_id(sSQL);
                oData_ERP.EjecutarConsulta(sSQL);

                return true;
            }
            catch (FaultException oException)
            {

                CFDiException oCFDiException = new CFDiException();
                MessageBox.Show(oException.Message, Application.ProductName + "-" + Application.ProductVersion, MessageBoxButtons.OK, MessageBoxIcon.Error);
                toolStripStatusLabel1.Text = "Error: Factura NO CANCELADA " + DateTime.Now.ToShortDateString() + " " + DateTime.Now.ToLongTimeString();

                return false;
            }
        }

        private void buttonItem7_Click_1(object sender, EventArgs e)
        {
            enviar_mail(false);
        }

        private void buttonItem9_Click(object sender, EventArgs e)
        {

        }

        private void buttonItem15_Click(object sender, EventArgs e)
        {
            verificar_sat();
        }

        private void verificar_sat()
        {
            if (dtgrdGeneral.SelectedRows.Count > 0)
            {
                DataGridViewSelectedRowCollection arrSelectedRows = dtgrdGeneral.SelectedRows;
                for (int n = arrSelectedRows.Count - 1; n >= 0; n--)
                {
                    int indice = arrSelectedRows[n].Index;
                    string INVOICE_ID = arrSelectedRows[n].Cells["INVOICE_ID"].Value.ToString();
                    string UUID = arrSelectedRows[n].Cells["UUID"].Value.ToString();
                    if (UUID != "")
                    {
                        Encapsular oEncapsular = (Encapsular)arrSelectedRows[n].Tag;

                        cFACTURA oFACTURA = new cFACTURA(oEncapsular.oData_ERP);
                        oFACTURA.datos(INVOICE_ID, "", "", oEncapsular.ocEMPRESA, "");
                        verificar_sat_CFDI(oFACTURA, UUID);
                    }

                }
                MessageBox.Show("Facturas Verificadas.");
            }
            else
            {
                //Cargar si factura
                verificaR_sat_UUID_directo();
            }
        }

        private bool verificaR_sat_UUID_directo()
        {
            string UUID = "";
            frmUUID ofrmUUID = new frmUUID();
            if (ofrmUUID.ShowDialog() == DialogResult.OK)
            {

                string INVOICE_ID = ofrmUUID.INVOICE_IDp;
                UUID = ofrmUUID.UUIDp;
                //cEMPRESA ocEMPRESA = new cEMPRESA(oData);
                cEMPRESA ocEMPRESA = ENTITY_ID.SelectedItem as cEMPRESA;

                //Llamar el Servicio
                CFDiClient oCFDiClient = new CFDiClient();
                string[] uuid = new string[1];
                uuid[0] = UUID;
                byte[] resultado = null;

                try
                {
                    resultado = oCFDiClient.getCfdiAck(Globales.oCFDI_USUARIO.USUARIO, Globales.oCFDI_USUARIO.PASSWORD, uuid);

                    string archivo = "TEMPORAL_" + DateTime.Now.ToString("DDMMYYYYhhmmsss") + ".ZIP";

                    File.WriteAllBytes(archivo, resultado);
                    //Descomprimir
                    String TargetDirectory = "TEMPORAL";
                    using (Ionic.Zip.ZipFile zip = Ionic.Zip.ZipFile.Read(archivo))
                    {
                        zip.ExtractAll(TargetDirectory, Ionic.Zip.ExtractExistingFileAction.OverwriteSilently);
                    }
                    File.Delete(archivo);

                    //Copiar el ARCHIVO Descrompimido por otro
                    string ARCHIVO_descomprimido = TargetDirectory + @"\" + "SIGN_" + INVOICE_ID + ".XML";
                    ARCHIVO_descomprimido = TargetDirectory + @"\" + UUID + "_.XML";


                    string myPath = TargetDirectory;
                    System.Diagnostics.Process prc = new System.Diagnostics.Process();
                    prc.StartInfo.FileName = myPath;
                    prc.Start();

                    return true;
                }
                catch (FaultException oException)
                {

                    CFDiException oCFDiException = new CFDiException();
                    MessageBox.Show(oException.Message, Application.ProductName + "-" + Application.ProductVersion, MessageBoxButtons.OK, MessageBoxIcon.Error);
                    toolStripStatusLabel1.Text = "Error " + DateTime.Now.ToShortDateString() + " " + DateTime.Now.ToLongTimeString();

                    return false;
                }
            }
            return false;
        }

        private bool verificar_sat_CFDI(cFACTURA ocFACTURA, string UUID)
        {
            string SERIE_tr = ocFACTURA.INVOICE_ID.Replace("0", "").Replace("1", "").Replace("2", "").Replace("3", "").Replace("4", "").Replace("5", "").Replace("6", "").Replace("7", "").Replace("8", "").Replace("9", "").Trim();
            cSERIE ocSERIE = new cSERIE();
            cEMPRESA ocEMPRESA = new cEMPRESA(oData);
            ocEMPRESA.cargar((ENTITY_ID.SelectedItem as cEMPRESA).ROW_ID);
            if (!ocSERIE.cargar_serie(SERIE_tr, ocEMPRESA.ROW_ID))
            {
                MessageBox.Show("La Serie " + SERIE_tr + " no esta configurada.", Application.ProductName + "-" + Application.ProductVersion, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return false;
            }
            //Cargar Certificado
            cCERTIFICADO ocCERTIFICADO = new cCERTIFICADO();
            ocCERTIFICADO.cargar(ocSERIE.ROW_ID_CERTIFICADO);
            //cEMPRESA ocEMPRESA = new cEMPRESA(oData);
            ocEMPRESA.cargar(ocCERTIFICADO.ROW_ID_EMPRESA);

            //Llamar el Servicio
            CFDiClient oCFDiClient = new CFDiClient();
            string[] uuid = new string[1];
            uuid[0] = UUID;
            byte[] resultado = null;

            try
            {
                resultado = oCFDiClient.getCfdiAck(Globales.oCFDI_USUARIO.USUARIO, Globales.oCFDI_USUARIO.PASSWORD, uuid);

                string archivo = "TEMPORAL_" + DateTime.Now.ToString("DDMMYYYYhhmmsss") + ".ZIP";

                File.WriteAllBytes(archivo, resultado);
                //Descomprimir
                String TargetDirectory = "TEMPORAL";
                using (Ionic.Zip.ZipFile zip = Ionic.Zip.ZipFile.Read(archivo))
                {
                    zip.ExtractAll(TargetDirectory, Ionic.Zip.ExtractExistingFileAction.OverwriteSilently);
                }
                File.Delete(archivo);

                //Copiar el ARCHIVO Descrompimido por otro
                string ARCHIVO_descomprimido = TargetDirectory + @"\" + "SIGN_" + ocFACTURA.INVOICE_ID + ".XML";

                if (ocFACTURA.UUID != null)
                {
                    ARCHIVO_descomprimido = TargetDirectory + @"\" + ocFACTURA.UUID + "_.XML";
                }



                return true;
            }
            catch (FaultException oException)
            {

                CFDiException oCFDiException = new CFDiException();
                MessageBox.Show(oException.Message, Application.ProductName + "-" + Application.ProductVersion, MessageBoxButtons.OK, MessageBoxIcon.Error);
                toolStripStatusLabel1.Text = "Error " + DateTime.Now.ToShortDateString() + " " + DateTime.Now.ToLongTimeString();

                return false;
            }
            return false;
        }

        private void buttonItem11_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Se procedera a Enviar con Archivos Adjuntos.", Application.ProductName + "-" + Application.ProductVersion, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            enviar_mail(true);
        }

        private void buttonItem16_Click(object sender, EventArgs e)
        {
            frmFormatos oObjeto = new frmFormatos(oData.sConn);
            oObjeto.ShowDialog();
        }

        public string Fechammmyy(string pFecha)
        {
            DateTime Fecha = DateTime.Parse(pFecha.ToString());
            string FechaFormateada = "";
            string[] mMeses = new string[13];

            mMeses[1] = "Ene";
            mMeses[2] = "Feb";
            mMeses[3] = "Mar";
            mMeses[4] = "Abr";
            mMeses[5] = "May";
            mMeses[6] = "Jun";
            mMeses[7] = "Jul";
            mMeses[8] = "Agt";
            mMeses[9] = "Sep";
            mMeses[10] = "Oct";
            mMeses[11] = "Nov";
            mMeses[12] = "Dic";

            int Mes = int.Parse(Fecha.Month.ToString());

            FechaFormateada = mMeses[Mes] + AgregarCero(Fecha.Year.ToString());

            return FechaFormateada;
        }

        public string AgregarCero(string p)
        {

            String regreso = p;

            if (p.Length < 2)

                regreso = "0" + p;

            return regreso;
        }

        private void buttonItem17_Click(object sender, EventArgs e)
        {
            regenerar_pdf();
        }

        private void buttonItem19_Click_1(object sender, EventArgs e)
        {
            pedimentos();
        }

        private void pedimentos()
        {

            if (dtgrdGeneral.SelectedRows.Count > 0)
            {
                DataGridViewSelectedRowCollection arrSelectedRows = dtgrdGeneral.SelectedRows;
                for (int n = arrSelectedRows.Count - 1; n >= 0; n--)
                {
                    Encapsular oEncapsular = (Encapsular)arrSelectedRows[n].Tag;
                    int indice = arrSelectedRows[n].Index;
                    string INVOICE_ID = arrSelectedRows[n].Cells["INVOICE_ID"].Value.ToString();
                    frmPedimentos ofrmPedimentos = new frmPedimentos(oEncapsular.oData_ERP.sConn, INVOICE_ID, oEncapsular.ocEMPRESA, "");
                    ofrmPedimentos.ShowDialog();
                }
            }
        }

        private void buttonItem18_Click(object sender, EventArgs e)
        {

        }

        private void buttonItem18_Click_1(object sender, EventArgs e)
        {
            frmAddendas ofrmAddendas = new frmAddendas(oData.sConn);
            ofrmAddendas.ShowDialog();
        }

        private void buttonItem20_Click(object sender, EventArgs e)
        {
            ModeloDocumentosFXContainer dbContext = new ModeloDocumentosFXContainer();
            try
            {
                string conection = dbContext.Database.Connection.ConnectionString;
                frmImportador ofrmImportador = new frmImportador(conection);
                ofrmImportador.Show();
            }
            catch (Exception err)
            {
                ErrorFX.mostrar(err, false, true, "frmPrincipalFXr2 - 3816 - ", false);
            }
        }

        private void buttonItem21_Click(object sender, EventArgs e)
        {
            addendas();
        }

        private void addendas()
        {

            if (dtgrdGeneral.SelectedRows.Count > 0)
            {
                DataGridViewSelectedRowCollection arrSelectedRows = dtgrdGeneral.SelectedRows;
                for (int n = arrSelectedRows.Count - 1; n >= 0; n--)
                {
                    int indice = arrSelectedRows[n].Index;
                    Encapsular oEncapsular = (Encapsular)arrSelectedRows[n].Tag;

                    string INVOICE_ID = arrSelectedRows[n].Cells["INVOICE_ID"].Value.ToString();

                    cFACTURA_BANDEJA ocFACTURA_BANDEJA = new cFACTURA_BANDEJA(oEncapsular.oData_ERP, oEncapsular.ocEMPRESA);
                    ocFACTURA_BANDEJA.cargar_ID(INVOICE_ID, oEncapsular.ocEMPRESA);

                    cFACTURA ocFACTURA = new cFACTURA(oEncapsular.oData_ERP);
                    ocFACTURA.datos(INVOICE_ID, "", "", oEncapsular.ocEMPRESA, "");
                    generar_addenda(ocFACTURA, ocFACTURA_BANDEJA);

                }
            }
        }

        private void buttonItem29_Click(object sender, EventArgs e)
        {
            //Cargar el manual
            string archivo_temporal = ConfigurationManager.AppSettings["MANUAL"].ToString();
            System.Diagnostics.Process.Start(archivo_temporal);
        }

        private void buttonItem13_Click_1(object sender, EventArgs e)
        {
            //Cargar el manual
            string archivo_temporal = ConfigurationManager.AppSettings["TECNICO"].ToString();
            System.Diagnostics.Process.Start(archivo_temporal);
        }

        private void buttonItem13_Click_2(object sender, EventArgs e)
        {
            //Cargar el manual
            string archivo_temporal = ConfigurationManager.AppSettings["TECNICO"].ToString();
            System.Diagnostics.Process.Start(archivo_temporal);
        }

        private void buttonItem22_Click(object sender, EventArgs e)
        {
            //Cargar el manual
            string archivo_temporal = ConfigurationManager.AppSettings["MAIL_SOPORTE"].ToString();
            System.Diagnostics.Process.Start(archivo_temporal);
        }

        private void oTimer_Tick(object sender, EventArgs e)
        {
            //Actualizar fecha final al GET DATE
            GC.Collect();
            GC.WaitForPendingFinalizers();
            FINAL.Value = DateTime.Now;

            actualizar();
            if (dtgrdGeneral.Rows.Count > 0)
            {
                PDF_VER.Checked = false;
                dtgrdGeneral.SelectAll();
                oTimer.Stop();
                procesar_factura("3.3");
            }
            oTimer.Start();
        }

        private void buttonItem3_Click_1(object sender, EventArgs e)
        {
            ModeloDocumentosFXContainer dbContext = new ModeloDocumentosFXContainer();
            try
            {
                string conection = dbContext.Database.Connection.ConnectionString;
                frmCliente_Asuntos ofrmCliente_Asuntos = new frmCliente_Asuntos(oData.sConn, conection);
                ofrmCliente_Asuntos.ShowDialog();
            }
            catch (Exception err)
            {
                ErrorFX.mostrar(err, false, true, "frmPrincipalFXr2 - 3907 - ", false);
            }

        }

        private void ribbonBar8_ItemClick(object sender, EventArgs e)
        {

        }

        private void buttonItem15_Click_1(object sender, EventArgs e)
        {
            frmComercioExteriorConfiguracion ofrmComercioExteriorConfiguracion = new frmComercioExteriorConfiguracion(oData.sConn);
            ofrmComercioExteriorConfiguracion.ShowDialog();
        }

        private void buttonItem18_Click_2(object sender, EventArgs e)
        {

        }

        private void btnRetencion_Click(object sender, EventArgs e)
        {
            frmRetenciones ofrmCFDIs = new frmRetenciones(oData.sConn);
            ofrmCFDIs.ShowDialog();
        }

        private void buttonItem18_Click_3(object sender, EventArgs e)
        {
            frmPortal ofrmPortal = new frmPortal();
            ofrmPortal.ShowDialog();
        }

        private void buttonItem19_Click_2(object sender, EventArgs e)
        {
            Process.Start("Coves.exe");
        }

        private void buttonItem1_Click_2(object sender, EventArgs e)
        {
            generar_UUID();
        }

        private void buttonItem20_Click_1(object sender, EventArgs e)
        {
            regenerarMasivamente();
        }

        private void regenerarMasivamente()
        {
            frmRegeneracionMasiva o = new frmRegeneracionMasiva();
            if (o.ShowDialog() == DialogResult.OK)
            {
                string facturas_tr = o.facturas;
                if (facturas_tr != "")
                {
                    string[] facturas = facturas_tr.Split(',');
                    string facturaCargar = "";
                    foreach (string factura in facturas)
                    {
                        if (factura != "")
                        {
                            if (facturaCargar != "")
                            {
                                facturaCargar += ",";
                            }
                            facturaCargar += "'" + factura + "'";
                        }

                    }
                    //MessageBox.Show(facturaCargar);
                    TOPtxt.Text = "999";
                    //Cargar las facturas
                    cargar(dtgrdGeneral, facturaCargar);
                    dtgrdGeneral.SelectAll();
                    regenerar_pdf();

                }
            }
        }

        private void dtgrdGeneral_MouseClick(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Right)
            {
                contextMenuStrip1.Show(dtgrdGeneral, new Point(e.X, e.Y));
            }
        }

        private void modoRealToolStripMenuItem_Click(object sender, EventArgs e)
        {
            generar33();
        }

        private void generar33()
        {
            dtgrdGeneral.EndEdit();
            if (dtgrdGeneral.SelectedCells.Count > 0)
            {
                foreach (DataGridViewRow drv in dtgrdGeneral.SelectedRows)
                {

                }
            }
            else
            {
                MessageBox.Show("Seleccione alguna factura.", Application.ProductName + "-" + Application.ProductVersion, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }

        private void modoPruebaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            procesar_factura("3.3");
        }

        private void EMPRESA_NOMBRE_Click(object sender, EventArgs e)
        {

        }

        private void ribbonBar5_ItemClick(object sender, EventArgs e)
        {

        }

        private void buttonItem21_Click_1(object sender, EventArgs e)
        {
            procesar_factura("3.3");
        }

        private void buttonItem24_Click(object sender, EventArgs e)
        {
            procesar_factura("3.2");
        }

        private void pDFToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ver_pdf();
        }

        private void xMLToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ver_xml();
        }

        private void toolStripMenuItem1_Click(object sender, EventArgs e)
        {

        }

        private void regenerarPDFToolStripMenuItem_Click(object sender, EventArgs e)
        {
            regenerar_pdf();
        }

        private void clasificarProductoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            clasificarProductos();
        }

        private void clasificarProductos()
        {
            ClasificadorProductos.formularios.frmProductos o = new ClasificadorProductos.formularios.frmProductos();
            o.Show();
        }

        private void toolStripMenuItem2_Click(object sender, EventArgs e)
        {

        }

        private void exportarToolStripMenuItem_Click(object sender, EventArgs e)
        {
            exportar();
        }

        private void enviarPorCorreoPDFYXMLToolStripMenuItem_Click(object sender, EventArgs e)
        {
            enviar_mail(false);
        }

        private void relacionarDocumentosToolStripMenuItem_Click(object sender, EventArgs e)
        {
            relacionarDocumentos();
        }

        private void relacionarDocumentos()
        {
            try
            {
                if (dtgrdGeneral.SelectedRows.Count > 0)
                {
                    DataGridViewSelectedRowCollection arrSelectedRows = dtgrdGeneral.SelectedRows;
                    for (int n = arrSelectedRows.Count - 1; n >= 0; n--)
                    {
                        Encapsular oEncapsular = (Encapsular)arrSelectedRows[n].Tag;
                        string INVOICE_ID = arrSelectedRows[n].Cells["INVOICE_ID"].Value.ToString();
                        string CUSTOMER_ID = arrSelectedRows[n].Cells["CUSTOMER_ID"].Value.ToString();
                        if (oEncapsular.ocEMPRESA.relacionLinea)
                        {
                            ClasificadorProductos.formularios.frmTipoRelacionNDCnormalSimplificado ofrmTipoRelacionNDC =
                            new ClasificadorProductos.formularios.frmTipoRelacionNDCnormalSimplificado(oEncapsular.ocEMPRESA.ENTITY_ID, INVOICE_ID, CUSTOMER_ID);
                            ofrmTipoRelacionNDC.Show();
                        }
                        else
                        {
                            ClasificadorProductos.formularios.frmTipoRelacionNDCfactura ofrmTipoRelacionNDC =
                            new ClasificadorProductos.formularios.frmTipoRelacionNDCfactura(oEncapsular.ocEMPRESA.ENTITY_ID, INVOICE_ID, CUSTOMER_ID);
                            ofrmTipoRelacionNDC.Show();
                        }

                    }
                }
            }
            catch (Exception e)
            {
                ErrorFX.mostrar(e, true, true,false);
            }
        }

        private void timbrarToolStripMenuItem_Click(object sender, EventArgs e)
        {
            timbrar();
        }
        private bool timbrar(string invoiceId, Encapsular oEncapsular, int opcion)
        {
            try
            {
                Timbrado.Timbrar oTimbrar = new Timbrado.Timbrar();
                cFACTURA_BANDEJA ocFACTURA_BANDEJA = new cFACTURA_BANDEJA(oEncapsular.oData_ERP,oEncapsular.ocEMPRESA);
                ocFACTURA_BANDEJA.cargar_ID(invoiceId, oEncapsular.ocEMPRESA);

                BitacoraFX.Log("LLamado de timbrar la factura");

                bool resultado = oTimbrar.timbrar(opcion, ocFACTURA_BANDEJA.XML, invoiceId,oEncapsular.ocEMPRESA.ROW_ID);
                if (resultado)
                {
                    ocFACTURA_BANDEJA.cargar_ID(invoiceId, oEncapsular.ocEMPRESA);
                    ocFACTURA_BANDEJA.actualizar_estado(invoiceId,oTimbrar.mensaje);
                }
                return resultado;

            }
            catch (Exception e)
            {
                ErrorFX.mostrar(e, true, true, "frmPrincipalFXr2 - 3392 -", true);
            }
            return false;
        }

        private void timbrar()
        {
            try
            {
                if (dtgrdGeneral.SelectedRows.Count > 0)
                {
                    DataGridViewSelectedRowCollection arrSelectedRows = dtgrdGeneral.SelectedRows;
                    for (int n = arrSelectedRows.Count - 1; n >= 0; n--)
                    {
                        Encapsular oEncapsular = (Encapsular)arrSelectedRows[n].Tag;
                        int opcion = 1;
                        if (CFDI_PRUEBA.Checked)
                        {
                            opcion = 2;
                        }
                        timbrar(arrSelectedRows[n].Cells["INVOICE_ID"].Value.ToString(), oEncapsular, opcion);


                    }
                }
            }
            catch (Exception e)
            {
                ErrorFX.mostrar(e, true, true, true);
            }
        }

        private void relacionarDocumentosToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            relacionarDocumentos();
        }
    }
}
