﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.IO;
using System.Text.RegularExpressions;
using OfficeOpenXml;
using Generales;

namespace FE_FX
{
    public partial class frmImportador : Form
    {

        private cCONEXCION oData_ERP = new cCONEXCION("");
        public frmImportador(string sConn)
        {
            oData_ERP.sConn = sConn;
            InitializeComponent();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            cargar_archivo();
        }

        private void cargar_archivo()
        {
            // create an open file dialog
            OpenFileDialog dlgOpen = new OpenFileDialog();

            // set properties for the dialog
            dlgOpen.Title = "Seleccione el Archivo";
            dlgOpen.ShowReadOnly = true;
            dlgOpen.Multiselect = false;
            dlgOpen.Filter = "Archivo de Excel (*.xlsx)|*.xlsx";


            // display the dialog and return results
            if (dlgOpen.ShowDialog() == DialogResult.OK)
            {
                foreach (string s in dlgOpen.FileNames)
                {
                    ARCHIVO.Text = s;
                    cargar();
                }
            }

        }


        private void cargar()
        {
            dtgrdGeneral.Rows.Clear();
            toolStripStatusLabel1.Text = "Abriendo";
            int n;
            int rowIndex = 2;
            FileInfo existingFile = new FileInfo(ARCHIVO.Text);
            using (ExcelPackage package = new ExcelPackage(existingFile))
            {

                ExcelWorksheet worksheet = package.Workbook.Worksheets[1];
                while (worksheet.Cells[rowIndex, 1].Value != null)
                {

                    string FOLIO = worksheet.Cells[rowIndex, 1].Value.ToString();
                    string ADUANA = worksheet.Cells[rowIndex, 2].Value.ToString();
                    string FECHA = worksheet.Cells[rowIndex, 3].Value.ToString();
                    string PEDIMENTO = worksheet.Cells[rowIndex, 4].Value.ToString();

                    n = dtgrdGeneral.Rows.Add();
                    dtgrdGeneral.Rows[n].Cells["FOLIO"].Value = FOLIO;
                    dtgrdGeneral.Rows[n].Cells["ADUANA"].Value = ADUANA;
                    dtgrdGeneral.Rows[n].Cells["FECHA"].Value = FECHA;
                    dtgrdGeneral.Rows[n].Cells["PEDIMENTO"].Value = PEDIMENTO;
                    toolStripStatusLabel1.Text = "Leyendo Fila " + n.ToString();


                    System.Windows.Forms.Application.DoEvents();

                    rowIndex++;

                }

            }
            toolStripStatusLabel1.Text = "Lectura Completa ";
        }

        private void button1_Click(object sender, EventArgs e)
        {
            procesar();
        }

        private void procesar()
        {
            toolStripProgressBar1.Maximum=dtgrdGeneral.Rows.Count+1;
            for (int n = 0; n < dtgrdGeneral.Rows.Count; n++)
            {

                toolStripProgressBar1.Value += 1;

                string FOLIO = dtgrdGeneral.Rows[n].Cells["FOLIO"].Value.ToString();
                string ADUANA = dtgrdGeneral.Rows[n].Cells["ADUANA"].Value.ToString();
                string FECHA = dtgrdGeneral.Rows[n].Cells["FECHA"].Value.ToString();
                string PEDIMENTO = dtgrdGeneral.Rows[n].Cells["PEDIMENTO"].Value.ToString();

                cPEDIMENTO ocPEDIMENTO = new cPEDIMENTO(oData_ERP,null);

                ocPEDIMENTO.INVOICE_ID = FOLIO;
                //Buscar ultima linea de la factura
                ocPEDIMENTO.LINE_NO = "1";
                string sSQL = "SELECT TOP 1 LINE_NO FROM RECEIVABLE_LINE WHERE INVOICE_ID='" + ocPEDIMENTO.INVOICE_ID + "' ORDER BY LINE_NO DESC";
                DataTable oDataTable = oData_ERP.EjecutarConsulta(sSQL);
                foreach (DataRow oDataRow in oDataTable.Rows)
                {
                    ocPEDIMENTO.LINE_NO=oDataRow["LINE_NO"].ToString();
                }
                ocPEDIMENTO.PEDIMENTO = PEDIMENTO;
                ocPEDIMENTO.FECHA = FECHA;
                ocPEDIMENTO.ADUANA = ADUANA;
                if (!ocPEDIMENTO.guardar())
                {
                    dtgrdGeneral.Rows[n].ErrorText = "Pedimento no guardado.";
                }
                toolStripStatusLabel1.Text = "Leyendo Fila " + n.ToString();

                System.Windows.Forms.Application.DoEvents();
            }
            toolStripStatusLabel1.Text = "Fin de Insertar ";
        }
        

    }

}
