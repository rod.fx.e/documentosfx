﻿using System;
using System.Collections.Generic;

using System.Text;
using System.Data.OleDb;
using System.Data.SqlClient;
using System.Data;
using System.Windows;
using System.IO;
using System.Windows.Forms;
using System.Configuration;

namespace FE_FX
{
    class cCONEXCION
    {
 
        public string sConn;  
        public OleDbDataAdapter oDataAdapter;
        public OleDbConnection oConn;  
        public string sTipo;
        public string sServer;
        public string sDatabase;                  
        public string sUsername;                 
        public string sPassword;                  
        public string no_existe_BD="";                  

        public cCONEXCION(string pTipo,string pServer, string pDatabase, string pUsername, string pPassword)
        {
            sTipo = pTipo;
            sServer = pServer;

            sDatabase = pDatabase;
            sUsername = pUsername;
            sPassword = pPassword;

            conectarse();
        }

        public cCONEXCION(string pDatabase, string pUsername, string pPassword)
        {
            if (verificar_existencia(pDatabase))
            {
                sDatabase = pDatabase;
                sUsername = pUsername;
                sPassword = pPassword;
                //sConn = @"Provider=Microsoft.ACE.OLEDB.12.0; Data Source=" + pDatabase + ";Persist Security Info=False";
                //sConn = @"Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + pDatabase + ";Jet OLEDB:Database Password=deskjet$" + (char)34 + "==;";
            }
            else
            {
                no_existe_BD = "ERROR";
            }
        }

        public cCONEXCION()
        {
            leer_datos();
            conectarse();
        }
        public void conectarse()
        {
            switch(sTipo.ToUpper())
            {
                case "SQLSERVER":
                    sConn = @"provider=sqloledb;server=" + sServer + ";database=" + sDatabase + ";uid=" + sUsername + ";pwd=" + sPassword + ";";
                    break;
                case "ACCESS":
                    sConn = @"Provider=Microsoft.ACE.OLEDB.12.0; Data Source=" + sDatabase + ";Persist Security Info=False";
                    break;
                case "ORACLE":
                    sConn = @"Provider = OraOLEDB.Oracle; Data Source = " + sServer + "; User Id = " + sUsername + "; Password = " + sPassword + "; OLEDB.NET=True;";
                    break;
            }
        }
        //Buscar Datos
        public void leer_datos()
        {
            try
            {
                sTipo = "SQLSERVER";//ConfigurationManager.AppSettings["TIPO"].ToString();
                sServer = ConfigurationManager.AppSettings["SERVER"].ToString();
                sDatabase = ConfigurationManager.AppSettings["DATA_BASE"].ToString();
                sUsername = ConfigurationManager.AppSettings["USER"].ToString();
                sPassword = ConfigurationManager.AppSettings["PASSWORD"].ToString();
            }
            catch (Exception e)
            {
                MessageBox.Show(e.ToString());
            }

        }

        public cCONEXCION(string pConn)
        {
             this.sConn = pConn;
            
        }
        public string convertir_fecha_general(DateTime pfecha, string como = "n")
        {
            string fecha = "";
            //SQLServer
            fecha = "CONVERT(DATETIME,'" + pfecha.Year.ToString() + "-" + pfecha.Month.ToString() + "-" + pfecha.Day.ToString() + " " + pfecha.Hour.ToString() + ":" + pfecha.Minute.ToString() + ":" + pfecha.Second.ToString() + "',102)";
            if (como == "i")
            {
                fecha = "CONVERT(DATETIME,'" + pfecha.Year.ToString() + "-" + pfecha.Month.ToString() + "-" + pfecha.Day.ToString() + " 00:00:00',102)";
            }
            if (como == "f")
            {
                fecha = "CONVERT(DATETIME,'" + pfecha.Year.ToString() + "-" + pfecha.Month.ToString() + "-" + pfecha.Day.ToString() + " 23:59:59',102)";
            }
            return fecha;
        }
        public bool verificar_existencia(string sPath)
        {
            if (!File.Exists(sPath))
            {
                MessageBox.Show("Error: La Base de datos no fue encontrada.", Application.ProductName + "-" + Application.ProductVersion,MessageBoxButtons.OK,MessageBoxIcon.Error);
                
            }
            return File.Exists(sPath);
        }
        public bool CrearConexion()
        {
            //MessageBox.Show("Prueba OleDbConnection");
            
            try
            {
                oConn = new OleDbConnection(sConn);
                oConn.Open();
                return true;
            }
            catch (OleDbException e)
            {
                MessageBox.Show(e.Message, "Exepción " + Application.ProductName + "-" + Application.ProductVersion, MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return false;
            }
        }

        public void DestruirConexion()
        {
            oConn = null;
        }

        public DataTable EjecutarConsulta(string pQuery)
        {
            try
            {
                DataTable oDT = new DataTable();
                //Ejecutar para SQL Server
                oDT = new DataTable();
                if (!sConn.Contains("Provider"))
                {
                    sConn = "Provider=SQLOLEDB;" + sConn;
                }

                oConn = new OleDbConnection(sConn);
                oDataAdapter = new OleDbDataAdapter();
                OleDbCommand oOleDbCommand=new OleDbCommand(pQuery, oConn);
                oOleDbCommand.CommandTimeout = 1000000;
                oDataAdapter.SelectCommand = oOleDbCommand;
                oDataAdapter.Fill(oDT);
                oDataAdapter.Dispose();
                DestruirConexion();
                return oDT;
            }
            catch (OleDbException e)
            {
                MessageBox.Show(e.Message + "\n" + pQuery, "Exepción " + Application.ProductName + "-" + Application.ProductVersion, MessageBoxButtons.OK, MessageBoxIcon.Stop);
                //Guardar en base datos
                Bitacora.Log(DateTime.Now.ToShortDateString() + " " + DateTime.Now.ToShortTimeString() + ":"
                                    + e.Message + "\n" + pQuery);
                return null;
            }

        }

        public string Trunca_y_formatea(decimal numero)
        {
            string resultado = "0.0000";
            decimal numero_f = Truncar_decimales(decimal.Parse(numero.ToString()));
            resultado = formato_decimal(numero_f.ToString(), "N");
            return (resultado);
        }

        public decimal Truncar_decimales(decimal numero)
        {
            int numero_truncar = 10000;
            decimal parte_decimal = 0, pasar_a_decimal = 0, resultado = 0;
            int parte_entera, separar_2_primeros_decimales;

            parte_entera = (int)numero;
            if (parte_entera != 0)
            {
                parte_decimal = numero % parte_entera;
            }
            else
            {
                parte_decimal = numero;
            }
            separar_2_primeros_decimales = (int)(parte_decimal * numero_truncar);
            pasar_a_decimal = (decimal)separar_2_primeros_decimales / numero_truncar;
            resultado = (decimal)parte_entera + pasar_a_decimal;

            return (resultado);
        }

        public string formato_decimal(string valor, string con_coma)
        {
            string conversion;
            double Plantilla;
            if (valor.Trim() == "")
            {
                valor = "0";
            }
            Plantilla = double.Parse(valor.ToString());
            if (con_coma == "")
            {
                conversion = Plantilla.ToString("#,###,###,##0.0000");
            }
            else
            {
                conversion = Plantilla.ToString("#########0.0000");
            }
            return conversion;
        }

        public string IsNullNumero(object Expression)
        {
            string valor = Trunca_y_formatea(1);
            if (Expression != null)
            {
                valor = Expression.ToString();
            }
            else
            {
                if (Expression.ToString() == "")
                {
                    valor = Trunca_y_formatea(1);
                }
            }
            return valor;
        }

        public string IsNull(object Expression)
        {
            string valor = "";
            if (Expression != null)
            {
                if (Expression.ToString() == "01/01/1900 00:00:00")
                {
                    valor = "";
                }
                else
                {
                    valor = Expression.ToString();
                }
            }
            return valor;
        }

        public bool IsNumeric(object Expression)
        {
            bool isNum;
            double retNum;

            isNum = Double.TryParse(Convert.ToString(Expression), System.Globalization.NumberStyles.Any, System.Globalization.NumberFormatInfo.InvariantInfo, out retNum);
            return isNum;
        }

        public string convertir_fecha(DateTime pfecha, string TipoP = "SQLSERVER")
        {
            string fecha = "";
            switch (TipoP.ToUpper())
            {
                case "SQLSERVER":
                    fecha = "CONVERT(DATETIME,'" + pfecha.Year.ToString() + "-" + pfecha.Month.ToString() + "-" + pfecha.Day.ToString() + " 00:00:00',102)";
                    break;
                case "ACCESS":
                    fecha = "CONVERT(DATETIME,'" + pfecha.Year.ToString() + "-" + pfecha.Month.ToString() + "-" + pfecha.Day.ToString() + " 00:00:00',102)";
                    break;
                case "ORACLE":
                    //Oracle 
                    string MES = pfecha.Month.ToString();
                    if (MES.Length == 1)
                    {
                        MES = "0" + MES;
                    }

                    string DIA = pfecha.Day.ToString();
                    if (DIA.Length == 1)
                    {
                        DIA = "0" + DIA;
                    }

                    fecha = "TO_DATE('" + DIA + "/" + MES + "/" + pfecha.Year.ToString() + "','DD/MM/YYYY')";
                    break;
            }
            return fecha;
        }

    }
}
