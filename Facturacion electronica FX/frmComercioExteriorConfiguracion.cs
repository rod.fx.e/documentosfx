﻿using DevComponents.DotNetBar;
using OfficeOpenXml;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Text;
using System.Windows.Forms;
using Generales;
using FE_FX.Formularios;

namespace FE_FX
{
    public partial class frmComercioExteriorConfiguracion : Form
    {
        private cCONEXCION oData = new cCONEXCION("");
        private cCCE oObjeto;
        private cEMPRESA ocEMPRESA;
        private cCONEXCION oData_ERP = new cCONEXCION("");

        public frmComercioExteriorConfiguracion(string sConn)
        {
            oData.sConn = sConn;
            InitializeComponent();
            cargar_empresas();
            
        }

        private void button3_Click(object sender, EventArgs e)
        {
            guardar();
        }
        private void guardar()
        {
            ajax_loader.Visible = true;
            oObjeto.UDFVMPRTMNT = UDFVMPRTMNT.Text;
            oObjeto.ROW_ID_EMPRESA = ocEMPRESA.ROW_ID;
            oObjeto.CUSTOMER_EXCEPCIONES = CUSTOMER_EXCEPCIONES.Text;
            oObjeto.PART_ID_EXCEPCIONES = PART_ID_EXCEPCIONES.Text;
            oObjeto.PRODUCTCODE_EXCEPCIONES = PRODUCTCODE_EXCEPCIONES.Text;

            if (oObjeto.guardar())
            {
                toolStripStatusLabel1.Text = "Datos Actualizados " + DateTime.Now.ToShortDateString() + " " + DateTime.Now.ToLongTimeString();
            }
            else
            {
                toolStripStatusLabel1.Text = "Error " + DateTime.Now.ToShortDateString() + " " + DateTime.Now.ToLongTimeString();
            }

            ajax_loader.Visible = false;
        }

        private void cargar_empresas()
        {
            ENTITY_ID.Items.Clear();
            cEMPRESA ocEMPRESA = new cEMPRESA(oData);
            foreach (cEMPRESA registro in ocEMPRESA.todos("", true))
            {
                ENTITY_ID.Items.Add(registro);
            }
            if (ENTITY_ID.Items.Count > 0)
            {
                ENTITY_ID.SelectedIndex = 0;
            }
        }

        private void cargar()
        {
            ajax_loader.Visible = true;
            oObjeto = new cCCE();
            limpiar();
            if (oObjeto.cargar(ocEMPRESA.ROW_ID))
            {
                UDFVMPRTMNT.Text = oObjeto.UDFVMPRTMNT;
                ocEMPRESA.ROW_ID = oObjeto.ROW_ID_EMPRESA;
                CUSTOMER_EXCEPCIONES.Text = oObjeto.CUSTOMER_EXCEPCIONES;
                PART_ID_EXCEPCIONES.Text = oObjeto.PART_ID_EXCEPCIONES;
                PRODUCTCODE_EXCEPCIONES.Text = oObjeto.PRODUCTCODE_EXCEPCIONES;
                button4.Text = oObjeto.validarFracciones(oData_ERP).ToString() + " productos sin fracción."; 
                toolStripStatusLabel1.Text = "Datos Cargados " + DateTime.Now.ToShortDateString() + " " + DateTime.Now.ToLongTimeString();
            }
            ajax_loader.Visible = false;
        }

        private void limpiar()
        {
            UDFVMPRTMNT.Text = "";

        }

        private void button1_Click(object sender, EventArgs e)
        {
            importar_visual();
        }

        private void importar_visual()
        {
            OpenFileDialog openFileDialog1 = new OpenFileDialog();
            openFileDialog1.Filter = "Excel files (*.xlsx)|*.xlsx|All files (*.*)|*.*";
            openFileDialog1.FilterIndex = 1;
            openFileDialog1.RestoreDirectory = true;

            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                try
                {
                    procesar_visual(1, openFileDialog1.FileName);
                }
                catch (Exception)
                {
                    MessageBoxEx.Show("No existe el Archivo seleccionado", Application.ProductName + "-" + Application.ProductVersion, MessageBoxButtons.OK);
                }
            }
        }

        private void procesar_visual(int hoja, string archivo)
        {
            try
            {
                int n;
                int rowIndex = 1;
                FileInfo existingFile = new FileInfo(archivo);
                using (ExcelPackage package = new ExcelPackage(existingFile))
                {
                    ExcelWorksheet worksheet = package.Workbook.Worksheets[hoja];
                    while (worksheet.Cells[rowIndex, 2].Value != null)
                    {
                        toolStripStatusLabel1.Text = "Registro " + rowIndex.ToString();
                        //Agregar las lineas de la cabecera
                        if (rowIndex != 1)
                        {
                            string fraccion=worksheet.Cells[rowIndex, 1].Text;
                            string part = worksheet.Cells[rowIndex, 2].Text;
                            oObjeto.insertarFraccion(fraccion, part,oData_ERP);
                        }
                        rowIndex++;

                    }
                    MessageBoxEx.Show("Archivo Importado", Application.ProductName);
                }
            }
            catch (Exception e)
            {
                MessageBoxEx.Show(e.Message.ToString());
            }
        }

        private void ENTITY_ID_SelectedIndexChanged(object sender, EventArgs e)
        {
            actualizar_conexcion();
        }

        private void actualizar_conexcion()
        {

            ocEMPRESA = ENTITY_ID.SelectedItem as cEMPRESA;
            if (ocEMPRESA != null)
            {

                EMPRESA_NOMBRE.Text = ocEMPRESA.ID;

                oData_ERP = new cCONEXCION(ocEMPRESA.TIPO, ocEMPRESA.SERVIDOR, ocEMPRESA.BD, ocEMPRESA.USUARIO_BD, ocEMPRESA.PASSWORD_BD);
                cargar();
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            crear_plantilla();
        }

        private void crear_plantilla()
        {
            MessageBoxEx.Show("La configuración de  la Plantilla para migrar fracción arancelaría:" + Environment.NewLine
            + "Fila 1 - Encabezado" + Environment.NewLine
            + "Columna 1: Fracción " + Environment.NewLine
            + "Columna 2: Producto " + Environment.NewLine
            + "Si no tiene esta configuración no será cargada la información. "
            + "Será generada la demas información desde Visual. "
            , Application.ProductName + "-" + Application.ProductVersion.ToString()
            , MessageBoxButtons.OK
            );

            var fileName = " Plantilla de Cuentas " + DateTime.Now.ToString("yyyy-MM-dd-hh-mm-ss") + ".xlsx";
            if (folderBrowserDialog1.ShowDialog() == DialogResult.OK)
            {
                fileName = folderBrowserDialog1.SelectedPath + "\\" + fileName;
                System.IO.Stream myStream = new System.IO.FileStream(fileName, System.IO.FileMode.Create, System.IO.FileAccess.ReadWrite, System.IO.FileShare.None);

                using (ExcelPackage pck = new ExcelPackage())
                {

                    ExcelWorksheet ws1 = pck.Workbook.Worksheets.Add("Plantilla");
                    ws1.Cells[1, 1].Value = "Fracción";
                    ws1.Cells[1, 2].Value = "Producto";

                    pck.SaveAs(myStream);
                }
                myStream.Close();
                MessageBoxEx.Show("Creación de plantilla realizada " + Environment.NewLine + fileName + " finalizada.", Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private void button4_Click(object sender, EventArgs e)
        {
            imprimirReporte();
        }

        private void imprimirReporte()
        {
            var fileName = "Productos faltante de fraccion " + DateTime.Now.ToString("yyyy-MM-dd-hh-mm-ss") + ".xlsx";
            if (folderBrowserDialog1.ShowDialog() == DialogResult.OK)
            {
                fileName = folderBrowserDialog1.SelectedPath + "\\" + fileName;
                System.IO.Stream myStream = new System.IO.FileStream(fileName, System.IO.FileMode.Create, System.IO.FileAccess.ReadWrite, System.IO.FileShare.None);

                using (ExcelPackage pck = new ExcelPackage())
                {
                    string TOP = " top 1 ";
                    string ROWNUM = "";
                    if (oData_ERP.sTipo.Contains("Oracle"))
                    {
                        TOP = " ";
                        ROWNUM = " AND ROWNUM=1 ";
                    }

                    string sSQL = " SELECT ID, DESCRIPTION, " +
                    " (SELECT " + TOP  + " RECEIVABLE.INVOICE_ID " +
                    " FROM         RECEIVABLE INNER JOIN " +
                    " RECEIVABLE_LINE ON RECEIVABLE.INVOICE_ID = RECEIVABLE_LINE.INVOICE_ID INNER JOIN " +
                    " CUST_ORDER_LINE ON RECEIVABLE_LINE.CUST_ORDER_ID = CUST_ORDER_LINE.CUST_ORDER_ID AND  " +
                    " RECEIVABLE_LINE.CUST_ORDER_LINE_NO = CUST_ORDER_LINE.LINE_NO " +
                    " WHERE CUST_ORDER_LINE.PART_ID=PART.ID " + ROWNUM + " " +
                    " ORDER BY RECEIVABLE.CREATE_DATE DESC ) as ultima_factura " +
                    " ,  " +
                    " (SELECT " + TOP + " RECEIVABLE.CUSTOMER_ID " +
                    " FROM         RECEIVABLE INNER JOIN " +
                    " RECEIVABLE_LINE ON RECEIVABLE.INVOICE_ID = RECEIVABLE_LINE.INVOICE_ID INNER JOIN " +
                    " CUST_ORDER_LINE ON RECEIVABLE_LINE.CUST_ORDER_ID = CUST_ORDER_LINE.CUST_ORDER_ID AND  " +
                    " RECEIVABLE_LINE.CUST_ORDER_LINE_NO = CUST_ORDER_LINE.LINE_NO " +
                    " WHERE CUST_ORDER_LINE.PART_ID=PART.ID " + ROWNUM + " " +
                    " ORDER BY RECEIVABLE.CREATE_DATE DESC ) as Cliente " +
                    " ,  " +
                    " (SELECT " + TOP + " CUSTOMER.NAME " +
                    " FROM         RECEIVABLE INNER JOIN " +
                    " RECEIVABLE_LINE ON RECEIVABLE.INVOICE_ID = RECEIVABLE_LINE.INVOICE_ID INNER JOIN " +
                    " CUST_ORDER_LINE ON RECEIVABLE_LINE.CUST_ORDER_ID = CUST_ORDER_LINE.CUST_ORDER_ID AND  " +
                    " RECEIVABLE_LINE.CUST_ORDER_LINE_NO = CUST_ORDER_LINE.LINE_NO INNER JOIN " +
                    " CUSTOMER ON RECEIVABLE.CUSTOMER_ID = CUSTOMER.ID " +
                    " WHERE     (CUST_ORDER_LINE.PART_ID = PART.ID) " + ROWNUM + " " +
                    " ORDER BY RECEIVABLE.CREATE_DATE DESC ) as Nombre " +
                    " ,  " +
                    " (SELECT " + TOP + " RECEIVABLE.INVOICE_DATE " +
                    " FROM         RECEIVABLE INNER JOIN " +
                    " RECEIVABLE_LINE ON RECEIVABLE.INVOICE_ID = RECEIVABLE_LINE.INVOICE_ID INNER JOIN " +
                    " CUST_ORDER_LINE ON RECEIVABLE_LINE.CUST_ORDER_ID = CUST_ORDER_LINE.CUST_ORDER_ID AND  " +
                    " RECEIVABLE_LINE.CUST_ORDER_LINE_NO = CUST_ORDER_LINE.LINE_NO " +
                    " WHERE CUST_ORDER_LINE.PART_ID=PART.ID " + ROWNUM + " " +
                    " ORDER BY RECEIVABLE.CREATE_DATE DESC ) as Fecha_Factura " +
                    ",(SELECT TOP 1 WO.CREATE_DATE FROM WORK_ORDER WO WHERE WO.PART_ID=ID AND WO.TYPE='M' AND WO.SUB_ID='0') as Fecha_Creacion"+
                    " FROM PART  " +
                    " WHERE ISNULL((SELECT " + TOP + " 1 FROM USER_DEF_FIELDS UDF  " +
                    " WHERE UDF.PROGRAM_ID='VMPRTMNT'  " +
                    " AND UDF.ID='" + oObjeto.UDFVMPRTMNT + "' " + ROWNUM + " AND UDF.DOCUMENT_ID=PART.ID),0)=0 " +
                    " AND PART.FABRICATED='Y' ";

                    


                    DataTable oDataTable = oData_ERP.EjecutarConsulta(sSQL);

                    ExcelWorksheet ws1 = pck.Workbook.Worksheets.Add("Datos");
                    ws1.Cells["A1"].LoadFromDataTable(oDataTable, true);
                    pck.SaveAs(myStream);
                }
                myStream.Close();
                MessageBoxEx.Show("Exportación del archivo " + Environment.NewLine + fileName + " finalizada.", Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private void button5_Click(object sender, EventArgs e)
        {
            cargarArchivoProductCodeCommodityFraccion();
        }

        private void cargarArchivoProductCodeCommodityFraccion()
        {
            OpenFileDialog openFileDialog1 = new OpenFileDialog();
            openFileDialog1.Filter = "Excel files (*.xlsx)|*.xlsx|All files (*.*)|*.*";
            openFileDialog1.FilterIndex = 1;
            openFileDialog1.RestoreDirectory = true;

            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                try
                {
                    
                    string destinationDirectory = Application.ExecutablePath+"\\cce";
                    File.Copy(openFileDialog1.FileName, destinationDirectory + Path.GetFileName(openFileDialog1.FileName));


                }
                catch (Exception)
                {
                    MessageBoxEx.Show("No existe el Archivo seleccionado", Application.ProductName + "-" + Application.ProductVersion, MessageBoxButtons.OK);
                }
            }
        }

        private void button6_Click(object sender, EventArgs e)
        {
            imprimirTodas();
        }

        private void imprimirTodas()
        {
            var fileName = "Productos y fraccion " + DateTime.Now.ToString("yyyy-MM-dd-hh-mm-ss") + ".xlsx";
            if (folderBrowserDialog1.ShowDialog() == DialogResult.OK)
            {
                fileName = folderBrowserDialog1.SelectedPath + "\\" + fileName;
                System.IO.Stream myStream = new System.IO.FileStream(fileName, System.IO.FileMode.Create, System.IO.FileAccess.ReadWrite, System.IO.FileShare.None);

                using (ExcelPackage pck = new ExcelPackage())
                {
                    string TOP = " top 1 ";
                    string ROWNUM = "";
                    if (oData_ERP.sTipo.Contains("Oracle"))
                    {
                        TOP = " ";
                        ROWNUM = " AND ROWNUM=1 ";
                    }

                    string sSQL = " SELECT ID, DESCRIPTION, " +
                    " (SELECT " + TOP + " RECEIVABLE.INVOICE_ID " +
                    " FROM         RECEIVABLE INNER JOIN " +
                    " RECEIVABLE_LINE ON RECEIVABLE.INVOICE_ID = RECEIVABLE_LINE.INVOICE_ID INNER JOIN " +
                    " CUST_ORDER_LINE ON RECEIVABLE_LINE.CUST_ORDER_ID = CUST_ORDER_LINE.CUST_ORDER_ID AND  " +
                    " RECEIVABLE_LINE.CUST_ORDER_LINE_NO = CUST_ORDER_LINE.LINE_NO " +
                    " WHERE CUST_ORDER_LINE.PART_ID=PART.ID " + ROWNUM + " " +
                    " ORDER BY RECEIVABLE.CREATE_DATE DESC ) as ultima_factura " +
                    " ,  " +
                    " (SELECT " + TOP + " RECEIVABLE.CUSTOMER_ID " +
                    " FROM         RECEIVABLE INNER JOIN " +
                    " RECEIVABLE_LINE ON RECEIVABLE.INVOICE_ID = RECEIVABLE_LINE.INVOICE_ID INNER JOIN " +
                    " CUST_ORDER_LINE ON RECEIVABLE_LINE.CUST_ORDER_ID = CUST_ORDER_LINE.CUST_ORDER_ID AND  " +
                    " RECEIVABLE_LINE.CUST_ORDER_LINE_NO = CUST_ORDER_LINE.LINE_NO " +
                    " WHERE CUST_ORDER_LINE.PART_ID=PART.ID " + ROWNUM + " " +
                    " ORDER BY RECEIVABLE.CREATE_DATE DESC ) as Cliente " +
                    " ,  " +
                    " (SELECT " + TOP + " CUSTOMER.NAME " +
                    " FROM         RECEIVABLE INNER JOIN " +
                    " RECEIVABLE_LINE ON RECEIVABLE.INVOICE_ID = RECEIVABLE_LINE.INVOICE_ID INNER JOIN " +
                    " CUST_ORDER_LINE ON RECEIVABLE_LINE.CUST_ORDER_ID = CUST_ORDER_LINE.CUST_ORDER_ID AND  " +
                    " RECEIVABLE_LINE.CUST_ORDER_LINE_NO = CUST_ORDER_LINE.LINE_NO INNER JOIN " +
                    " CUSTOMER ON RECEIVABLE.CUSTOMER_ID = CUSTOMER.ID " +
                    " WHERE     (CUST_ORDER_LINE.PART_ID = PART.ID) " + ROWNUM + " " +
                    " ORDER BY RECEIVABLE.CREATE_DATE DESC ) as Nombre " +
                    " ,  " +
                    " (SELECT " + TOP + " RECEIVABLE.INVOICE_DATE " +
                    " FROM         RECEIVABLE INNER JOIN " +
                    " RECEIVABLE_LINE ON RECEIVABLE.INVOICE_ID = RECEIVABLE_LINE.INVOICE_ID INNER JOIN " +
                    " CUST_ORDER_LINE ON RECEIVABLE_LINE.CUST_ORDER_ID = CUST_ORDER_LINE.CUST_ORDER_ID AND  " +
                    " RECEIVABLE_LINE.CUST_ORDER_LINE_NO = CUST_ORDER_LINE.LINE_NO " +
                    " WHERE CUST_ORDER_LINE.PART_ID=PART.ID " + ROWNUM + " " +
                    " ORDER BY RECEIVABLE.CREATE_DATE DESC ) as Fecha_Factura " +
                    @",(SELECT TOP 1 WO.CREATE_DATE FROM WORK_ORDER WO WHERE WO.PART_ID=ID AND WO.TYPE='M' AND WO.SUB_ID='0') as Fecha_Creacion
                    , ISNULL((SELECT " + TOP + @" STRING_VAL  FROM USER_DEF_FIELDS UDF WHERE UDF.PROGRAM_ID='VMPRTMNT' AND UDF.ID='" + oObjeto.UDFVMPRTMNT + "' " + ROWNUM + " AND UDF.DOCUMENT_ID=PART.ID),'') as 'FraccionArancelaria'" +
                    " FROM PART  " +
                    " WHERE PART.FABRICATED='Y' ";




                    DataTable oDataTable = oData_ERP.EjecutarConsulta(sSQL);

                    ExcelWorksheet ws1 = pck.Workbook.Worksheets.Add("Datos");
                    ws1.Cells["A1"].LoadFromDataTable(oDataTable, true);
                    pck.SaveAs(myStream);
                }
                myStream.Close();
                MessageBoxEx.Show("Exportación del archivo " + Environment.NewLine + fileName + " finalizada.", Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private void button7_Click(object sender, EventArgs e)
        {
            plantillaPartId();
        }

        private void button11_Click(object sender, EventArgs e)
        {
            plantillaClientes();
        }

        private void plantillaClientes()
        {
            MessageBoxEx.Show("La configuración de  la Plantilla para migrar Clientes:" + Environment.NewLine
            + "Fila 1 - Encabezado" + Environment.NewLine
            + "Columna 1: Customer Id " + Environment.NewLine
            + "Si no tiene esta configuración no será cargada la información. "
            + "Será generada la demas información desde Visual. "
            , Application.ProductName + "-" + Application.ProductVersion.ToString()
            , MessageBoxButtons.OK
            );

            var fileName = " Plantilla de Cliente " + DateTime.Now.ToString("yyyy-MM-dd-hh-mm-ss") + ".xlsx";
            if (folderBrowserDialog1.ShowDialog() == DialogResult.OK)
            {
                fileName = folderBrowserDialog1.SelectedPath + "\\" + fileName;
                System.IO.Stream myStream = new System.IO.FileStream(fileName, System.IO.FileMode.Create, System.IO.FileAccess.ReadWrite, System.IO.FileShare.None);

                using (ExcelPackage pck = new ExcelPackage())
                {

                    ExcelWorksheet ws1 = pck.Workbook.Worksheets.Add("Plantilla");
                    ws1.Cells[1, 1].Value = "CUSTOMER_ID";
                    pck.SaveAs(myStream);
                }
                myStream.Close();
                MessageBoxEx.Show("Creación de plantilla realizada " + Environment.NewLine + fileName + " finalizada.", Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private void plantillaProductCode()
        {
            MessageBoxEx.Show("La configuración de  la Plantilla para migrar Producto Code:" + Environment.NewLine
            + "Fila 1 - Encabezado" + Environment.NewLine
            + "Columna 1: Product Code " + Environment.NewLine
            + "Si no tiene esta configuración no será cargada la información. "
            + "Será generada la demas información desde Visual. "
            , Application.ProductName + "-" + Application.ProductVersion.ToString()
            , MessageBoxButtons.OK
            );

            var fileName = " Plantilla de Product Code " + DateTime.Now.ToString("yyyy-MM-dd-hh-mm-ss") + ".xlsx";
            if (folderBrowserDialog1.ShowDialog() == DialogResult.OK)
            {
                fileName = folderBrowserDialog1.SelectedPath + "\\" + fileName;
                System.IO.Stream myStream = new System.IO.FileStream(fileName, System.IO.FileMode.Create, System.IO.FileAccess.ReadWrite, System.IO.FileShare.None);

                using (ExcelPackage pck = new ExcelPackage())
                {

                    ExcelWorksheet ws1 = pck.Workbook.Worksheets.Add("Plantilla");
                    ws1.Cells[1, 1].Value = "PRODUCT_CODE";
                    pck.SaveAs(myStream);
                }
                myStream.Close();
                MessageBoxEx.Show("Creación de plantilla realizada " + Environment.NewLine + fileName + " finalizada.", Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private void label6_Click(object sender, EventArgs e)
        {

        }

        private void button12_Click(object sender, EventArgs e)
        {
            plantillaProductCode();
        }

        private void plantillaPartId()
        {
            MessageBoxEx.Show("La configuración de  la Plantilla para migrar Part Id:" + Environment.NewLine
            + "Fila 1 - Encabezado" + Environment.NewLine
            + "Columna 1: Part Id " + Environment.NewLine
            + "Si no tiene esta configuración no será cargada la información. "
            + "Será generada la demas información desde Visual. "
            , Application.ProductName + "-" + Application.ProductVersion.ToString()
            , MessageBoxButtons.OK
            );

            var fileName = " Plantilla de Part Id " + DateTime.Now.ToString("yyyy-MM-dd-hh-mm-ss") + ".xlsx";
            if (folderBrowserDialog1.ShowDialog() == DialogResult.OK)
            {
                fileName = folderBrowserDialog1.SelectedPath + "\\" + fileName;
                System.IO.Stream myStream = new System.IO.FileStream(fileName, System.IO.FileMode.Create, System.IO.FileAccess.ReadWrite, System.IO.FileShare.None);

                using (ExcelPackage pck = new ExcelPackage())
                {

                    ExcelWorksheet ws1 = pck.Workbook.Worksheets.Add("Plantilla");
                    ws1.Cells[1, 1].Value = "PART_ID";
                    pck.SaveAs(myStream);
                }
                myStream.Close();
                MessageBoxEx.Show("Creación de plantilla realizada " + Environment.NewLine + fileName + " finalizada.", Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private void button9_Click(object sender, EventArgs e)
        {
            importar_visual();
        }

        private void button9_Click_1(object sender, EventArgs e)
        {
            importarCliente();
        }

        private void importarCliente()
        {
            OpenFileDialog openFileDialog1 = new OpenFileDialog();
            openFileDialog1.Filter = "Excel files (*.xlsx)|*.xlsx|All files (*.*)|*.*";
            openFileDialog1.FilterIndex = 1;
            openFileDialog1.RestoreDirectory = true;

            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                try
                {
                    procesarImportarCliente(1, openFileDialog1.FileName);
                }
                catch (Exception eerr)
                {
                    string mensaje = eerr.Message.ToString();
                    if (eerr.InnerException != null)
                    {
                        mensaje += Environment.NewLine + eerr.InnerException.ToString();
                    }
                    MessageBoxEx.Show("Error cargando archivo. " + Environment.NewLine
                        + mensaje, Application.ProductName + "-" + Application.ProductVersion, MessageBoxButtons.OK);
                }
            }
        }

        private void procesarImportarCliente(int hoja, string archivo)
        {
            try
            {
                int n;
                int rowIndex = 1;
                FileInfo existingFile = new FileInfo(archivo);
                using (ExcelPackage package = new ExcelPackage(existingFile))
                {
                    ExcelWorksheet worksheet = package.Workbook.Worksheets[hoja];
                    string clienteMigrado = "";
                    while (worksheet.Cells[rowIndex, 1].Value != null)
                    {
                        toolStripStatusLabel1.Text = "Registro " + rowIndex.ToString();
                        //Agregar las lineas de la cabecera
                        if (rowIndex != 1)
                        {
                            string cliente = worksheet.Cells[rowIndex, 1].Text;
                            if (clienteMigrado != "")
                            {
                                clienteMigrado += ",";
                            }
                            clienteMigrado += cliente;
                        }
                        rowIndex++;
                    }
                    CUSTOMER_EXCEPCIONES.Text = clienteMigrado;
                    MessageBoxEx.Show(this, "Archivo Importado. Presione modificar para guardar la información.", Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
            catch (Exception e)
            {
                MessageBoxEx.Show(e.Message.ToString());
            }
        }


        private void importarProductCode()
        {
            OpenFileDialog openFileDialog1 = new OpenFileDialog();
            openFileDialog1.Filter = "Excel files (*.xlsx)|*.xlsx|All files (*.*)|*.*";
            openFileDialog1.FilterIndex = 1;
            openFileDialog1.RestoreDirectory = true;

            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                try
                {
                    procesarProductCode(1, openFileDialog1.FileName);
                }
                catch (Exception eerr)
                {
                    string mensaje = eerr.Message.ToString();
                    if (eerr.InnerException != null)
                    {
                        mensaje += Environment.NewLine + eerr.InnerException.ToString();
                    }
                    MessageBoxEx.Show("Error cargando archivo. " + Environment.NewLine
                        + mensaje, Application.ProductName + "-" + Application.ProductVersion, MessageBoxButtons.OK);
                }
            }
        }

        private void procesarProductCode(int hoja, string archivo)
        {
            try
            {
                int n;
                int rowIndex = 1;
                FileInfo existingFile = new FileInfo(archivo);
                using (ExcelPackage package = new ExcelPackage(existingFile))
                {
                    ExcelWorksheet worksheet = package.Workbook.Worksheets[hoja];
                    string productCodeMigrado = "";
                    while (worksheet.Cells[rowIndex, 1].Value != null)
                    {
                        toolStripStatusLabel1.Text = "Registro " + rowIndex.ToString();
                        //Agregar las lineas de la cabecera
                        if (rowIndex != 1)
                        {
                            string cliente = worksheet.Cells[rowIndex, 1].Text;
                            if (productCodeMigrado != "")
                            {
                                productCodeMigrado += ",";
                            }
                            productCodeMigrado += cliente;
                        }
                        rowIndex++;
                    }
                    PRODUCTCODE_EXCEPCIONES.Text = productCodeMigrado;
                    MessageBoxEx.Show(this, "Archivo Importado. Presione modificar para guardar la información.", Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
            catch (Exception e)
            {
                MessageBoxEx.Show(e.Message.ToString());
            }
        }

        private void button8_Click(object sender, EventArgs e)
        {
            importarProductCode();
        }

        private void button7_Click_1(object sender, EventArgs e)
        {
            importarPartId();
        }

        private void importarPartId()
        {
            OpenFileDialog openFileDialog1 = new OpenFileDialog();
            openFileDialog1.Filter = "Excel files (*.xlsx)|*.xlsx|All files (*.*)|*.*";
            openFileDialog1.FilterIndex = 1;
            openFileDialog1.RestoreDirectory = true;

            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                try
                {
                    procesarPartId(1, openFileDialog1.FileName);
                }
                catch (Exception eerr)
                {
                    string mensaje = eerr.Message.ToString();
                    if (eerr.InnerException != null)
                    {
                        mensaje += Environment.NewLine + eerr.InnerException.ToString();
                    }
                    MessageBoxEx.Show("Error cargando archivo. " + Environment.NewLine
                        + mensaje, Application.ProductName + "-" + Application.ProductVersion, MessageBoxButtons.OK);
                }
            }
        }

        private void procesarPartId(int hoja, string archivo)
        {
            try
            {
                int n;
                int rowIndex = 1;
                FileInfo existingFile = new FileInfo(archivo);
                using (ExcelPackage package = new ExcelPackage(existingFile))
                {
                    ExcelWorksheet worksheet = package.Workbook.Worksheets[hoja];
                    string productCodeMigrado = "";
                    while (worksheet.Cells[rowIndex, 1].Value != null)
                    {
                        toolStripStatusLabel1.Text = "Registro " + rowIndex.ToString();
                        //Agregar las lineas de la cabecera
                        if (rowIndex != 1)
                        {
                            string cliente = worksheet.Cells[rowIndex, 1].Text;
                            if (productCodeMigrado != "")
                            {
                                productCodeMigrado += ",";
                            }
                            productCodeMigrado += cliente;
                        }
                        rowIndex++;
                    }
                    this.PART_ID_EXCEPCIONES.Text = productCodeMigrado;
                    MessageBoxEx.Show(this,"Archivo Importado. Presione modificar para guardar la información.", Application.ProductName,MessageBoxButtons.OK,MessageBoxIcon.Information);
                }
            }
            catch (Exception e)
            {
                MessageBoxEx.Show(e.Message.ToString());
            }
        }

        private void button13_Click(object sender, EventArgs e)
        {
            frmCceClientes OfrmCceClientes = new frmCceClientes();
            OfrmCceClientes.Show();
        }
    }
}
