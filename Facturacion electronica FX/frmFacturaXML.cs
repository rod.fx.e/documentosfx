﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.IO;
using System.Text.RegularExpressions;
using System.Diagnostics;

namespace FE_FX
{
    public partial class frmFacturaXML : Form
    {


        public frmFacturaXML(string XMLp)
        {
            InitializeComponent();
            XML.Text = XMLp;
            if (XML.Text.Trim()!="")
            {
                //Verificar que el arhcivo exista
                if (File.Exists(XML.Text))
                {
                    webBrowser1.Navigate(XML.Text);
                }
                else
                {
                    MessageBox.Show("El Archivo " + XML.Text + " no existe, por favor guarde y abra de nuevo la factura.", Application.ProductName + "-" + Application.ProductVersion, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                }
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                string directorio = Path.GetDirectoryName(XML.Text);
                Process.Start(directorio);
            }
            catch
            {

            }
        }
    }
}
