#region Copyright Syncfusion Inc. 2001 - 2017
// Copyright Syncfusion Inc. 2001 - 2017. All rights reserved.
// Use of this code is subject to the terms of our license.
// A copy of the current license can be obtained at any time by e-mailing
// licensing@syncfusion.com. Any infringement will be prosecuted under
// applicable laws. 
#endregion
using FE_FX.EDICOM_Servicio;
using Generales;
using ModeloComprobantePago;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Windows.Forms;

namespace FE_FX.Cancelacion
{
    public partial class frmCancelacionMasiva : Syncfusion.Windows.Forms.MetroForm
    {
        public frmCancelacionMasiva()
        {
            InitializeComponent();
            cargarEmisor();
        }
        private void cargarEmisor()
        {
            emisor.Items.Clear();
            cEMPRESA ocEMPRESA = new cEMPRESA();
            foreach (cEMPRESA registro in ocEMPRESA.todos("", false))
            {
                emisor.Items.Add(registro);
            }
            if (emisor.Items.Count > 0)
            {
                emisor.SelectedIndex = 0;
            }
        }
        private void button2_Click(object sender, EventArgs e)
        {
            cancelar();
        }

        private void cancelar()
        {
            try
            {
                string[] uuidsTr = uuids.Text.Split(',');
                log.Text = "";
                foreach (string uuidTr in uuidsTr)
                {
                    cancelar(uuidTr);
                }
            }
            catch(Exception error)
            {
                ErrorFX.mostrar(error, true, true, true);
            }
        }
        public byte[] ReadBinaryFile(string fileName)
        {
            if (File.Exists(fileName))
            {
                try
                {
                    ///Open and read a file&#12290;
                    FileStream fileStream = File.OpenRead(fileName);
                    byte[] archivo = ConvertStreamToByteBuffer(fileStream);
                    fileStream.Close();
                    return archivo;
                }
                catch (Exception ex)
                {
                    return new byte[0];
                }
            }
            else
            {
                return new byte[0];
            }
        }

        public byte[] ConvertStreamToByteBuffer(System.IO.Stream theStream)
        {
            int b1;
            System.IO.MemoryStream tempStream = new System.IO.MemoryStream();
            while ((b1 = theStream.ReadByte()) != -1)
            {
                tempStream.WriteByte(((byte)b1));
            }
            return tempStream.ToArray();
        }
        private void cancelar(string uuidTr)
        {
            try
            {
                ModeloComprobantePago.documentosfxEntities dbContext = new documentosfxEntities();

                //Llamar el Servicio
                CFDiClient oCFDiClient = new CFDiClient();
                string[] uuid = new string[1];
                uuid[0] = uuidTr;
                //Cargar el archivo pfx creado desde OpenSSL
                string pfx = ocCERTIFICADO.PFX;
                if (!File.Exists(pfx))
                {
                    //Validar el directorio

                    string directorio = AppDomain.CurrentDomain.BaseDirectory;
                    pfx = directorio + @"\" + ocCERTIFICADO.PFX;
                    if (!File.Exists(pfx))
                    {
                        MessageBox.Show("El PFX " + ocCERTIFICADO.PFX + " no existe."
                            , Application.ProductName + "-" + Application.ProductVersion, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                        return;
                    }
                }
                byte[] pfx_archivo = ReadBinaryFile(pfx);
                try
                {
                    CancelaResponse respuesta;
                    //Todo: Realizar cancelacion
                    //respuesta = oCFDiClient.cancelaCFDi(Globales.oCFDI_USUARIO.USUARIO, Globales.oCFDI_USUARIO.PASSWORD, ocEMPRESA.RFC, uuid, pfx_archivo, ocCERTIFICADO.PASSWORD);
                    log.Text = "Comprobante Cancelado "  + uuidTr  + DateTime.Now.ToShortDateString() + " " + DateTime.Now.ToLongTimeString() + Environment.NewLine;
                }
                catch (FaultException oException)
                {

                    CFDiException oCFDiException = new CFDiException();
                    //MessageBox.Show(oException.Message, Application.ProductName + "-" + Application.ProductVersion, MessageBoxButtons.OK, MessageBoxIcon.Error);
                    log.Text = "Error: Comprobante No Cancelado " + uuidTr + DateTime.Now.ToShortDateString() + " " + oException.Message + DateTime.Now.ToLongTimeString() + Environment.NewLine;
                }
                catch (Exception oException)
                {

                    CFDiException oCFDiException = new CFDiException();
                    //MessageBox.Show(oException.Message, Application.ProductName + "-" + Application.ProductVersion, MessageBoxButtons.OK, MessageBoxIcon.Error);
                    log.Text = "Error: Comprobante No Cancelado" + uuidTr + DateTime.Now.ToShortDateString() + " " + DateTime.Now.ToLongTimeString() + oException.Message  + Environment.NewLine;
                }
            }
            catch (Exception error)
            {
                log.Text = "Error: Comprobante No Cancelado" + uuidTr + DateTime.Now.ToShortDateString() + " " + error.Message +  DateTime.Now.ToLongTimeString() + Environment.NewLine;

                ErrorFX.mostrar(error, false, false, false);
            }
        }
        private cEMPRESA ocEMPRESA;
        private cSERIE ocSERIE;
        private cCONEXCION oData_ERP;
        cCERTIFICADO ocCERTIFICADO = new cCERTIFICADO();
        private void cargarSerie()
        {
            try
            {
                ocEMPRESA = new cEMPRESA();
                ocSERIE = new cSERIE();
                ocEMPRESA = (cEMPRESA)emisor.SelectedItem;
                oData_ERP = new cCONEXCION(ocEMPRESA.TIPO, ocEMPRESA.SERVIDOR, ocEMPRESA.BD, ocEMPRESA.USUARIO_BD, ocEMPRESA.PASSWORD_BD);
                foreach (cSERIE registro in ocSERIE.todos_empresa("", ocEMPRESA.ROW_ID, true, false))
                {
                    ocCERTIFICADO = new cCERTIFICADO();
                    ocCERTIFICADO.cargar(registro.ROW_ID_CERTIFICADO);
                    ocSERIE = registro;
                }
            }
            catch (Exception e)
            {
                ErrorFX.mostrar(e, true, true, "frmComprobantePago - 113 ");
            }
        }
        private void emisor_SelectedIndexChanged(object sender, EventArgs e)
        {
            cargarSerie();
        }
    }
}
