﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;

using System.Text;
using System.Windows.Forms;
using System.Text.RegularExpressions;
using Generales;


namespace FE_FX
{
    public partial class frmSerie : Form
    {
        private cCONEXCION oData = new cCONEXCION("");
        private cSERIE oObjeto;
        private bool modificado;

        public frmSerie(string sConn)
        {
            oData.sConn = sConn;
            oObjeto = new cSERIE();
            InitializeComponent();
            ajax_loader.Visible = false;
            limpiar();
        }

        private void cargarCombos()
        {
            ndcUsoCfdi.Items.Clear();
            ndcUsoCfdi.Items.Add(new ComboItem { Name = "Adquisición de mercancias", Value = "G01" });
            ndcUsoCfdi.Items.Add(new ComboItem { Name = "Devoluciones, descuentos o bonificaciones", Value = "G02" });
            ndcUsoCfdi.Items.Add(new ComboItem { Name = "Gastos en general", Value = "G03" });
            ndcUsoCfdi.Items.Add(new ComboItem { Name = "Construcciones", Value = "I01" });
            ndcUsoCfdi.Items.Add(new ComboItem { Name = "Mobilario y equipo de oficina por inversiones", Value = "I02" });
            ndcUsoCfdi.Items.Add(new ComboItem { Name = "Equipo de transporte", Value = "I03" });
            ndcUsoCfdi.Items.Add(new ComboItem { Name = "Equipo de computo y accesorios", Value = "I04" });
            ndcUsoCfdi.Items.Add(new ComboItem { Name = "Dados, troqueles, moldes, matrices y herramental", Value = "I05" });
            ndcUsoCfdi.Items.Add(new ComboItem { Name = "Comunicaciones telefónicas", Value = "I06" });
            ndcUsoCfdi.Items.Add(new ComboItem { Name = "Comunicaciones satelitales", Value = "I07" });
            ndcUsoCfdi.Items.Add(new ComboItem { Name = "Otra maquinaria y equipo", Value = "I08" });
            ndcUsoCfdi.Items.Add(new ComboItem { Name = "Honorarios médicos, dentales y gastos hospitalarios.", Value = "D01" });
            ndcUsoCfdi.Items.Add(new ComboItem { Name = "Gastos médicos por incapacidad o discapacidad", Value = "D02" });
            ndcUsoCfdi.Items.Add(new ComboItem { Name = "Gastos funerales.", Value = "D03" });
            ndcUsoCfdi.Items.Add(new ComboItem { Name = "Donativos.", Value = "D04" });
            ndcUsoCfdi.Items.Add(new ComboItem { Name = "Intereses reales efectivamente pagados por créditos hipotecarios (casa habitación).", Value = "D05" });
            ndcUsoCfdi.Items.Add(new ComboItem { Name = "Aportaciones voluntarias al SAR.", Value = "D06" });
            ndcUsoCfdi.Items.Add(new ComboItem { Name = "Primas por seguros de gastos médicos.", Value = "D07" });
            ndcUsoCfdi.Items.Add(new ComboItem { Name = "Gastos de transportación escolar obligatoria.", Value = "D08" });
            ndcUsoCfdi.Items.Add(new ComboItem { Name = "Depósitos en cuentas para el ahorro, primas que tengan como base planes de pensiones.", Value = "D09" });
            ndcUsoCfdi.Items.Add(new ComboItem { Name = "Pagos por servicios educativos (colegiaturas)", Value = "D10" });
            ComboItem oComboItem = new ComboItem { Name = "Por definir", Value = "P01" };
            ndcUsoCfdi.Items.Add(oComboItem);
            ndcUsoCfdi.SelectedItem = oComboItem;

            ndcMetodoPago.Items.Clear();
            ndcMetodoPago.Items.Add(new ComboItem { Name = "Pago en una sola exhibición", Value = "PUE" });
            ComboItem oComboItemMETODO_DE_PAGO = new ComboItem { Name = "Pago en parcialidades o diferido", Value = "PPD" };
            ndcMetodoPago.Items.Add(oComboItemMETODO_DE_PAGO);
            ndcMetodoPago.SelectedItem = oComboItemMETODO_DE_PAGO;
            

        }

        public frmSerie(string sConn, string ROW_IDp)
        {
            modificado = false;
            oData.sConn = sConn;
            oObjeto = new cSERIE();

            InitializeComponent();
            limpiar();
            oObjeto.ROW_ID = ROW_IDp;
            cargar();
        }

        public void limpiar()
        {
            if (modificado)
            {

                DialogResult Resultado = MessageBox.Show("¿Desea guardar los cambios?", Application.ProductName + "-" + Application.ProductVersion, MessageBoxButtons.YesNo);
                if (Resultado.ToString() == "Yes")
                {
                    guardar();
                }


            }
            modificado = false;
            oObjeto = new cSERIE();
            ID.Text = "";
            ID.Enabled = true;
            FORMATO.Text = "";
            ACCOUNT_ID_RETENIDO.Text = "";
            ACCOUNT_ID_ANTICIPO.Text = "";
            //APROBACION.Text = "";
            //ANIO.Text = "";
            CALLE.Text = "";
            EXTERIOR.Text = "";
            INTERIOR.Text = "";
            COLONIA.Text = "";
            CP.Text = "";
            LOCALIDAD.Text = "";
            REFERENCIA.Text = "";
            PAIS.Text="MEX";
            ESTADO.Text = "";
            MUNICIPIO.Text = "";
            DIRECTORIO.Text = "";
            DE.Text = "";
            CC.Text = "";
            CCO.Text = "";
            ASUNTO.Text = "";
            MENSAJE.Text = "";
            ACTIVO.Checked = true;
            REMITENTE.Text = "";
            ARCHIVO_NOMBRE.Text = "";
            tabControl1.SelectedIndex = 0;
            rfcExcepcion.Text = "";
            cargarCombos();
        }

        public static bool isEmail(string inputEmail)
        {
            if (inputEmail == null || inputEmail.Length == 0)
            {
                throw new ArgumentNullException("inputEmail");
            }

            const string expression = @"^([a-zA-Z0-9_\-\.]+)@((\[[0-9]{1,3}" +
                                      @"\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9\-]+\" +
                                      @".)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$";

            Regex regex = new Regex(expression);
            return regex.IsMatch(inputEmail);
        }

        private void guardar()
        {
            ajax_loader.Visible = true;
            if (ENVIO_AUTOMATICO_MAIL.Checked)
            {
                if (DE.Text.Trim() != "")
                {
                    string[] rEmail = DE.Text.Trim().Split(';');
                    for (int i = 0; i < rEmail.Length; i++)
                    {
                        if (!String.IsNullOrEmpty(rEmail[i].ToString().Trim()))
                        {
                            if (!isEmail(rEmail[i].ToString().Trim()))
                            {
                                MessageBox.Show("El valor introducido en el Campo DE no es una dirección de email con un formato válido.", Application.ProductName + "-" + Application.ProductVersion, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                                return;
                            }
                        }
                    }
                }

                if (PARA.Text.Trim() != "")
                {
                    string[] rEmail = PARA.Text.Trim().Split(';');
                    for (int i = 0; i < rEmail.Length; i++)
                    {
                        if (!String.IsNullOrEmpty(rEmail[i].ToString().Trim()))
                        {
                            if (!isEmail(rEmail[i].ToString().Trim()))
                            {
                                MessageBox.Show("El valor introducido en el Campo PARA no es una dirección de email con un formato válido.", Application.ProductName + "-" + Application.ProductVersion, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                                return;
                            }
                        }
                    }
                }

                if (CC.Text.Trim() != "")
                {
                    string[] rEmail = CC.Text.Trim().Split(';');
                    for (int i = 0; i < rEmail.Length; i++)
                    {
                        if (!String.IsNullOrEmpty(rEmail[i].ToString().Trim()))
                        {
                            if (!isEmail(rEmail[i].ToString().Trim()))
                            {
                                MessageBox.Show("El valor introducido en el Campo CC no es una dirección de email con un formato válido.", Application.ProductName + "-" + Application.ProductVersion, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                                return;
                            }
                        }
                    }
                }

                if (CCO.Text.Trim() != "")
                {
                    string[] rEmail = CCO.Text.Trim().Split(';');
                    for (int i = 0; i < rEmail.Length; i++)
                    {
                        if (!String.IsNullOrEmpty(rEmail[i].ToString().Trim()))
                        {
                            if (!isEmail(rEmail[i].ToString().Trim()))
                            {
                                MessageBox.Show("El valor introducido en el Campo CCO no es una dirección de email con un formato válido.", Application.ProductName + "-" + Application.ProductVersion, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                                return;
                            }
                        }
                    }
                }
            }

            oObjeto.ID = ID.Text;
            oObjeto.FOLIO_INICIAL = FORMATO.Text;
            oObjeto.DIRECTORIO = DIRECTORIO.Text;
            oObjeto.CALLE = CALLE.Text;
            oObjeto.INTERIOR = INTERIOR.Text;
            oObjeto.EXTERIOR = EXTERIOR.Text;
            oObjeto.COLONIA = COLONIA.Text;
            oObjeto.CP = CP.Text;
            oObjeto.LOCALIDAD = LOCALIDAD.Text;
            oObjeto.REFERENCIA = REFERENCIA.Text;
            oObjeto.ESTADO = ESTADO.Text;
            oObjeto.PAIS = PAIS.Text;
            oObjeto.MUNICIPIO = MUNICIPIO.Text;
            oObjeto.DE = DE.Text;
            oObjeto.PARA = PARA.Text;
            oObjeto.CC = CC.Text;
            oObjeto.CCO = CCO.Text;
            oObjeto.ASUNTO = ASUNTO.Text;
            oObjeto.ASUNTO_ADJUNTO = ASUNTO_ADJUNTO.Text;
            oObjeto.ASUNTO_CANCELADO = ASUNTO_CANCELADO.Text;
            oObjeto.rfcExcepcion = rfcExcepcion.Text;
            oObjeto.MENSAJE = MENSAJE.Text;
            oObjeto.MENSAJE_ADJUNTO = MENSAJE_ADJUNTO.Text;
            oObjeto.MENSAJE_CANCELADO=MENSAJE_CANCELADO.Text;
            oObjeto.serieComprobante= serieComprobante.Checked.ToString();
            oObjeto.serieTraslado = serieTraslado.Checked.ToString();
            oObjeto.ACTIVO = ACTIVO.Checked.ToString();
            oObjeto.REMITENTE = REMITENTE.Text;
            oObjeto.ARCHIVO_NOMBRE = ARCHIVO_NOMBRE.Text;
            oObjeto.FORMATO = FORMATO.Text;
            oObjeto.ACCOUNT_ID_RETENIDO = ACCOUNT_ID_RETENIDO.Text;
            oObjeto.ACCOUNT_ID_ANTICIPO = ACCOUNT_ID_ANTICIPO.Text;
            oObjeto.ENVIO_AUTOMATICO_MAIL = ENVIO_AUTOMATICO_MAIL.Checked.ToString();
            oObjeto.CLIENTE_TRASLADO = CLIENTE_TRASLADO.Text;
            oObjeto.ndcClaveProd = ndcClaveProd.Text;
            oObjeto.ndcMetodoPago = ((ComboItem)ndcMetodoPago.SelectedItem).Value.ToString();
            oObjeto.ndcUnidad = ndcUnidad.Text;
            oObjeto.ndcUsoCfdi = ((ComboItem)ndcUsoCfdi.SelectedItem).Value.ToString();
            oObjeto.ndcUsarPropio = ndcUsarPropio.Checked.ToString();
            oObjeto.IMPRESOS = IMPRESOS.Text;
            oObjeto.validarComprobantePago = validarComprobantePago.Checked.ToString();
            oObjeto.UnirCarpeta = UnirCarpeta.Checked;

            if (oObjeto.guardar())
            {
                toolStripStatusLabel1.Text = "Datos Actualizados " + DateTime.Now.ToShortDateString() + " " + DateTime.Now.ToLongTimeString();
                modificado = false;
            }
            else
            {
                toolStripStatusLabel1.Text = "Error " + DateTime.Now.ToShortDateString() + " " + DateTime.Now.ToLongTimeString();
            }

            ajax_loader.Visible = false;
        }




        private void cargar()
        {
            ajax_loader.Visible = true;
            if (oObjeto.cargar(oObjeto.ROW_ID))
            {
                ID.Text = oObjeto.ID;
                FORMATO.Text = oObjeto.FOLIO_INICIAL;

                FORMATO.Text = oObjeto.FORMATO;
                CLIENTE_TRASLADO.Text = oObjeto.CLIENTE_TRASLADO;
                DIRECTORIO.Text = oObjeto.DIRECTORIO;
                CALLE.Text = oObjeto.CALLE;
                INTERIOR.Text = oObjeto.INTERIOR;
                EXTERIOR.Text = oObjeto.EXTERIOR;
                COLONIA.Text = oObjeto.COLONIA;
                CP.Text = oObjeto.CP;
                LOCALIDAD.Text = oObjeto.LOCALIDAD;
                REFERENCIA.Text = oObjeto.REFERENCIA;
                ESTADO.Text = oObjeto.ESTADO;
                PAIS.Text = oObjeto.PAIS;
                MUNICIPIO.Text = oObjeto.MUNICIPIO;
                DE.Text = oObjeto.DE;
                PARA.Text=oObjeto.PARA;
                CC.Text = oObjeto.CC;
                CCO.Text = oObjeto.CCO;
                ASUNTO.Text = oObjeto.ASUNTO;
                MENSAJE.Text = oObjeto.MENSAJE;
                cCERTIFICADO oCERTIFICADO = new cCERTIFICADO();
                oCERTIFICADO.cargar(oObjeto.ROW_ID_CERTIFICADO);
                CERTIFICADO.Text = oCERTIFICADO.ID;
                ACTIVO.Checked = bool.Parse(oObjeto.ACTIVO);
                REMITENTE.Text=oObjeto.REMITENTE;
                ARCHIVO_NOMBRE.Text=oObjeto.ARCHIVO_NOMBRE;
                ACCOUNT_ID_RETENIDO.Text=oObjeto.ACCOUNT_ID_RETENIDO;
                ACCOUNT_ID_ANTICIPO.Text = oObjeto.ACCOUNT_ID_ANTICIPO;
                ENVIO_AUTOMATICO_MAIL.Checked = bool.Parse(oObjeto.ENVIO_AUTOMATICO_MAIL);
                IMPRESOS.Text = oObjeto.IMPRESOS;
                rfcExcepcion.Text = oObjeto.rfcExcepcion;
                try
                {
                    serieComprobante.Checked = bool.Parse(oObjeto.serieComprobante);
                }
                catch
                {

                }

                try
                {
                    serieTraslado.Checked = bool.Parse(oObjeto.serieTraslado);
                }
                catch
                {

                }

                MENSAJE_ADJUNTO.Text = oObjeto.MENSAJE_ADJUNTO;
                MENSAJE_CANCELADO.Text=oObjeto.MENSAJE_CANCELADO;
                ASUNTO_ADJUNTO.Text = oObjeto.ASUNTO_ADJUNTO;
                ASUNTO_CANCELADO.Text = oObjeto.ASUNTO_CANCELADO;


                ndcUsarPropio.Checked = false;
                try
                {
                    ndcUsarPropio.Checked = bool.Parse(oObjeto.ndcUsarPropio);
                }
                catch
                {

                }

                foreach (ComboItem cbi in ndcMetodoPago.Items)
                {
                    if (cbi.Value.Equals(oObjeto.ndcMetodoPago))
                    {
                        ndcMetodoPago.SelectedItem = cbi;
                        break;
                    }
                }

                foreach (ComboItem cbi in ndcUsoCfdi.Items)
                {
                    if (cbi.Value.Equals(oObjeto.ndcUsoCfdi))
                    {
                        ndcUsoCfdi.SelectedItem = cbi;
                        break;
                    }
                }
                
                ndcClaveProd.Text = oObjeto.ndcClaveProd;
                ndcUnidad.Text = oObjeto.ndcUnidad;

                validarComprobantePago.Checked = false;
                try
                {
                    validarComprobantePago.Checked = bool.Parse(oObjeto.validarComprobantePago);
                }
                catch
                {

                }
                UnirCarpeta.Checked = false;
                try
                {
                    UnirCarpeta.Checked = oObjeto.UnirCarpeta;
                }
                catch
                {

                }

                toolStripStatusLabel1.Text = "Datos Cargados " + DateTime.Now.ToShortDateString() + " " + DateTime.Now.ToLongTimeString();
                modificado = false;

            }
            ajax_loader.Visible = false;
        }

        private void button3_Click(object sender, EventArgs e)
        {
            guardar();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            limpiar();
        }

        private void frmUsuario_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (modificado)
            {
                DialogResult Resultado = MessageBox.Show("¿Desea guardar los cambios?", Application.ProductName + "-" + Application.ProductVersion, MessageBoxButtons.YesNo);
                if (Resultado.ToString() == "Yes")
                {
                    guardar();
                }
            }
        }

        private void frmUsuario_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)27)
            {
                this.Close();
            }
            else
            {
                modificado = true;
                if ((e.KeyChar.ToString() == "<")
                                | (e.KeyChar.ToString() == ">")
                                
                                | (e.KeyChar.ToString() == "'")
                                | (e.KeyChar.ToString() == "#")
                                )
                {
                    e.Handled = true;
                }
            }
        }

        private void Texto_Cambiado_TextChanged(object sender, EventArgs e)
        {
            modificado = true;
        }

        private void Solo_Numero_KeyPress(object sender, KeyPressEventArgs e)
        {
            modificado = true;
            if (!char.IsDigit(e.KeyChar) && !char.IsControl(e.KeyChar) && !char.IsPunctuation(e.KeyChar)) e.Handled = true;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (listBoxEleMensaje.Visible)
            {
                listBoxEleMensaje.Visible = false;
            }
            else
            {
                listBoxEleMensaje.Visible = true;
            }
        }

        private void listBoxEleMensaje_DoubleClick(object sender, EventArgs e)
        {
            MENSAJE.Text+="<<"+listBoxEleMensaje.Text.ToString()+">>";
        }

        private void button4_Click(object sender, EventArgs e)
        {
            toolStripStatusLabel1.Text = "Listo ";
            FolderBrowserDialog Folder = new FolderBrowserDialog();
            if (Folder.ShowDialog() == DialogResult.OK)
            {
                DIRECTORIO.Text = Folder.SelectedPath;
            }
        }


        private void button7_Click(object sender, EventArgs e)
        {
            frmCertificados oBuscador = new frmCertificados(oData.sConn, "S");
            if (oBuscador.ShowDialog() == DialogResult.OK)
            {
                string ROW_ID = (string)oBuscador.selecionados[0].Cells[0].Value;
                string ID = (string)oBuscador.selecionados[0].Cells[1].Value;
                oObjeto.ROW_ID_CERTIFICADO = ROW_ID;
                CERTIFICADO.Text = ID;
            }
        }

        private void Verificar(object sender, KeyPressEventArgs e)
        {
            modificado = true;
            if ((e.KeyChar.ToString() == "<")
                            | (e.KeyChar.ToString() == ">")
                            
                            | (e.KeyChar.ToString() == "'")
                            | (e.KeyChar.ToString() == "#")
                            )
            {
                e.Handled = true;
            }
        }

        private void button6_Click(object sender, EventArgs e)
        {
            frmEstados oBuscador = new frmEstados(oData.sConn, "S");
            if (oBuscador.ShowDialog() == DialogResult.OK)
            {
                string ID = (string)oBuscador.selecionados[0].Cells[1].Value;
                ESTADO.Text = ID;
            }
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void button5_Click(object sender, EventArgs e)
        {
            oObjeto.incrementar(oObjeto.ROW_ID);
            cargar();
        }

        private void button5_Click_1(object sender, EventArgs e)
        {
            OpenFileDialog openFileDialog1 = new OpenFileDialog();
            openFileDialog1.Filter = "rpt files (*.rpt)|*.rpt|All files (*.*)|*.*";
            openFileDialog1.FilterIndex = 1;
            openFileDialog1.RestoreDirectory = true;

            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                try
                {
                    FORMATO.Text = openFileDialog1.FileName;
                }
                catch (Exception)
                {
                    MessageBox.Show("No existe el Archivo seleccionado", Application.ProductName + "-" + Application.ProductVersion, MessageBoxButtons.OK);
                }
            }
        }

        private void button9_Click(object sender, EventArgs e)
        {
            eliminar();
        }

        private void eliminar()
        {
            DialogResult Resultado = MessageBox.Show("¿Esta seguro de eliminar?", Application.ProductName + "-" + Application.ProductVersion, MessageBoxButtons.YesNo);
            if (Resultado.ToString() != "Yes")
            {
                return;
            }
            if (oObjeto.eliminar(oObjeto.ROW_ID))
            {
                limpiar();
            }
        }

        private void button10_Click(object sender, EventArgs e)
        {
           duplicar();
        }

        private void duplicar()
        {
            oObjeto.ROW_ID = "";
            oObjeto.ID = "";
            ID.Text = "";
        }

        private void button1_Click_1(object sender, EventArgs e)
        {

        }

        private void listBoxEleMensaje_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void button11_Click(object sender, EventArgs e)
        {
            actualizarImpresos();
        }

        private void actualizarImpresos()
        {
            cSERIE ocSERIE = new cSERIE();
            ocSERIE.actualizarImpresos(oObjeto.ROW_ID, IMPRESOS.Text);
            MessageBox.Show("Datos actualizados", Application.ProductName + "-" + Application.ProductVersion, MessageBoxButtons.OK);
        }
    }
}
