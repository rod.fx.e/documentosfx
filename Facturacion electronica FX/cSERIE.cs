﻿using System;
using System.Collections.Generic;

using System.Text;
using System.Data;
using System.Windows;
using System.Windows.Forms;
using Generales;
using ModeloDocumentosFX;
using System.IO;

namespace FE_FX
{
    public class cSERIE
    {
        private string sSQL = "";
        public string ROW_ID { get; set; }
        public string ID { get; set; }
        public string FOLIO_INICIAL { get; set; }
        public string FOLIO_FINAL { get; set; }
        //public string APROBACION { get; set; }
        //public string ANIO { get; set; }
        public string FORMATO { get; set; }
        public string DIRECTORIO { get; set; }
        public string ARCHIVO_NOMBRE { get; set; }
        public string CALLE { get; set; }
        public string EXTERIOR { get; set; }
        public string INTERIOR { get; set; }
        public string COLONIA { get; set; }
        public string CP { get; set; }
        public string LOCALIDAD { get; set; }
        public string REFERENCIA { get; set; }
        public string PAIS { get; set; }
        public string ESTADO { get; set; }
        public string MUNICIPIO { get; set; }
        public string DE { get; set; }
        public string PARA { get; set; }
        public string CC { get; set; }
        public string CCO { get; set; }
        public string ASUNTO { get; set; }
        public string MENSAJE { get; set; }

        public string ASUNTO_CANCELADO { get; set; }
        public string MENSAJE_CANCELADO { get; set; }

        public string ASUNTO_ADJUNTO { get; set; }
        public string MENSAJE_ADJUNTO { get; set; }


        public string RESTANTES { get; set; }
        public string IMPRESOS { get; set; }
        public string ROW_ID_CERTIFICADO { get; set; }
        public string CERO { get; set; }
        public string REMITENTE { get; set; }
        public string ACTIVO { get; set; }
        public string ACCOUNT_ID_RETENIDO { get; set; }
        public string ACCOUNT_ID_ANTICIPO { get; set; }
        public string ENVIO_AUTOMATICO_MAIL { get; set; }

        public string serieComprobante { get; set; }
        public string serieTraslado { get; set; }

    private cCONEXCION oData = new cCONEXCION("");
        internal string validarComprobantePago;

        public cSERIE()
        {
            ModeloDocumentosFXContainer dbContext = new ModeloDocumentosFXContainer();
            try
            {
                string conection = dbContext.Database.Connection.ConnectionString;
                oData = new cCONEXCION(conection);
            }
            catch (Exception err)
            {
                ErrorFX.mostrar(err, false, true, "cSERIE - 71 - ", false);
            }
            limpiar();
        }

        public void limpiar()
        {
            ROW_ID = "";
            ID = "";
            FOLIO_INICIAL = "";
            FOLIO_FINAL = "";
            //APROBACION = "";
            //ANIO = "";
            FORMATO = "";
            DIRECTORIO = "";
            CALLE = "";
            EXTERIOR = "";
            INTERIOR = "";
            COLONIA = "";
            CP = "";
            LOCALIDAD = "";
            REFERENCIA = "";
            PAIS = "";
            ESTADO = "";
            MUNICIPIO = "";
            DE = "";
            PARA = "";
            CC = "";
            CCO = "";
            ASUNTO = "";
            MENSAJE = "";
            RESTANTES = "0";
            IMPRESOS = "0";
            CERO = "";
            ACTIVO = "";
            ROW_ID_CERTIFICADO = "";
            REMITENTE = "";
            ARCHIVO_NOMBRE = "";
            ACCOUNT_ID_RETENIDO = "";
            ACCOUNT_ID_ANTICIPO = "";
            ASUNTO_CANCELADO = "";
            MENSAJE_CANCELADO = "";
            rfcExcepcion = "";
            ASUNTO_ADJUNTO = "";
            MENSAJE_ADJUNTO = "";
            serieComprobante = "False";
            serieTraslado = "False";
            ENVIO_AUTOMATICO_MAIL = "";
            validarComprobantePago = "False";

        }

        public bool validar_vencimiento()
        {
            //Validar el siguiente folio que no se salga del rango
            if ((decimal.Parse(IMPRESOS) + decimal.Parse(RESTANTES)) > decimal.Parse(FOLIO_FINAL))
            {
                MessageBox.Show("No existen folios disponibles para esta serie.", Application.ProductName + "-" + Application.ProductVersion, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return false;
            }

            return true;
        }

        public List<cSERIE> todos(string BUSQUEDA, bool SOLO_IMPRESOS)
        {

            List<cSERIE> lista = new List<cSERIE>();

            sSQL  = " SELECT * ";
            sSQL += " FROM SERIE ";
            sSQL += " WHERE ID LIKE '%" + BUSQUEDA + "%' ";
            if (SOLO_IMPRESOS)
            {
                sSQL += " AND IMPRESOS<FOLIO_FINAL ";
            }
            DataTable oDataTable = oData.EjecutarConsulta(sSQL);
            foreach (DataRow oDataRow in oDataTable.Rows)
            {
                lista.Add(new cSERIE()
                {
                    ROW_ID = oDataRow["ROW_ID"].ToString()
,
                    ID = oDataRow["ID"].ToString()
,
                    FOLIO_INICIAL = oDataRow["FOLIO_INICIAL"].ToString()
,
                    FOLIO_FINAL = oDataRow["FOLIO_FINAL"].ToString()
,
//                    APROBACION = oDataRow["APROBACION"].ToString()
//,
//                    ANIO = oDataRow["ANIO"].ToString()
//,
                    FORMATO = oDataRow["FORMATO"].ToString()
                    ,
                    ACCOUNT_ID_RETENIDO = oDataRow["ACCOUNT_ID_RETENIDO"].ToString()
                    ,
                    ACCOUNT_ID_ANTICIPO = oDataRow["ACCOUNT_ID_ANTICIPO"].ToString()
,
DIRECTORIO = oDataRow["DIRECTORIO"].ToString()
,
                    CALLE = oDataRow["CALLE"].ToString()
,
                    EXTERIOR = oDataRow["EXTERIOR"].ToString()
,
                    INTERIOR = oDataRow["INTERIOR"].ToString()
,
                    COLONIA = oDataRow["COLONIA"].ToString()
,
                    CP = oDataRow["CP"].ToString()
,
                    LOCALIDAD = oDataRow["LOCALIDAD"].ToString()
,
                    REFERENCIA = oDataRow["REFERENCIA"].ToString()
,
                    PAIS = oDataRow["PAIS"].ToString()
,
                    ESTADO = oDataRow["ESTADO"].ToString()
,
                    MUNICIPIO = oDataRow["MUNICIPIO"].ToString()
,
                    DE = oDataRow["DE"].ToString()
,
                    PARA = oDataRow["PARA"].ToString()
,
                    CC = oDataRow["CC"].ToString()
,
                    CCO = oDataRow["CCO"].ToString()
,
                    ASUNTO = oDataRow["ASUNTO"].ToString()
,
                    MENSAJE = oDataRow["MENSAJE"].ToString()
,
                    RESTANTES = oDataRow["RESTANTES"].ToString()
,
                    IMPRESOS = oDataRow["IMPRESOS"].ToString()
,
                    ROW_ID_CERTIFICADO = oDataRow["ROW_ID_CERTIFICADO"].ToString()
,
                    CERO = oDataRow["CERO"].ToString()
,
                    ACTIVO = oDataRow["ACTIVO"].ToString()
,
                    REMITENTE = oDataRow["REMITENTE"].ToString()
,
                    ARCHIVO_NOMBRE = oDataRow["ARCHIVO_NOMBRE"].ToString()
                    ,
                    ASUNTO_CANCELADO = oDataRow["ASUNTO_CANCELADO"].ToString(),
                    MENSAJE_CANCELADO = oDataRow["MENSAJE_CANCELADO"].ToString(),

                    ASUNTO_ADJUNTO = oDataRow["ASUNTO_ADJUNTO"].ToString(),
                    MENSAJE_ADJUNTO = oDataRow["MENSAJE_ADJUNTO"].ToString(),
                    ENVIO_AUTOMATICO_MAIL = oDataRow["ENVIO_AUTOMATICO_MAIL"].ToString(),
                    serieComprobante = oDataRow["serieComprobante"].ToString(),
                    serieTraslado = oDataRow["serieTraslado"].ToString(),
                    validarComprobantePago = oDataRow["validarComprobantePago"].ToString()
            });
            }

            return lista;

        }

        internal string obtenerSerie(string INVOICE_ID)
        {
            string sSQL = @"
            SELECT * 
            FROM SERIE
            WHERE CHARINDEX(ID,'" + INVOICE_ID.Trim() + @"',0)>0
            ";
            DataTable oDataTable = oData.EjecutarConsulta(sSQL);
            foreach (DataRow oDataRow in oDataTable.Rows)
            {
                return oDataRow["ID"].ToString();                
            }
            return "";
        }

        public List<cSERIE> todos_empresa(string BUSQUEDA,string ROW_ID_EMPRESA, bool serieComprobante = false, bool serieTraslado = false)
        {

            List<cSERIE> lista = new List<cSERIE>();
            if (ROW_ID_EMPRESA == "")
            {
                ROW_ID_EMPRESA = "-1";
            }
            sSQL = " SELECT * ";
            sSQL += " FROM SERIE ";
            sSQL += " WHERE ID LIKE '%" + BUSQUEDA + "%' ";
            sSQL += " AND (SELECT TOP 1 ROW_ID_EMPRESA FROM CERTIFICADO WHERE CERTIFICADO.ROW_ID=SERIE.ROW_ID_CERTIFICADO)=" + ROW_ID_EMPRESA + " ";
            if (serieComprobante)
            {
                sSQL += " AND SERIE.serieComprobante='True' ";
            }

            if (serieTraslado)
            {
                sSQL += " AND SERIE.serieTraslado='True' ";
            }
            DataTable oDataTable = oData.EjecutarConsulta(sSQL);
            foreach (DataRow oDataRow in oDataTable.Rows)
            {
                lista.Add(new cSERIE()
                {
                    ROW_ID = oDataRow["ROW_ID"].ToString()
,
                    ID = oDataRow["ID"].ToString()
,
                    FOLIO_INICIAL = oDataRow["FOLIO_INICIAL"].ToString()
,
                    FOLIO_FINAL = oDataRow["FOLIO_FINAL"].ToString()
,
//                    APROBACION = oDataRow["APROBACION"].ToString()
//,
//                    ANIO = oDataRow["ANIO"].ToString()
//,
ACCOUNT_ID_RETENIDO = oDataRow["ACCOUNT_ID_RETENIDO"].ToString()
,
                    ACCOUNT_ID_ANTICIPO = oDataRow["ACCOUNT_ID_ANTICIPO"].ToString()

 ,FORMATO = oDataRow["FORMATO"].ToString()
,                    DIRECTORIO = oDataRow["DIRECTORIO"].ToString()
,
                    CALLE = oDataRow["CALLE"].ToString()
,
                    EXTERIOR = oDataRow["EXTERIOR"].ToString()
,
                    INTERIOR = oDataRow["INTERIOR"].ToString()
,
                    COLONIA = oDataRow["COLONIA"].ToString()
,
                    CP = oDataRow["CP"].ToString()
,
                    LOCALIDAD = oDataRow["LOCALIDAD"].ToString()
,
                    REFERENCIA = oDataRow["REFERENCIA"].ToString()
,
                    PAIS = oDataRow["PAIS"].ToString()
,
                    ESTADO = oDataRow["ESTADO"].ToString()
,
                    MUNICIPIO = oDataRow["MUNICIPIO"].ToString()
,
                    DE = oDataRow["DE"].ToString()
,
                    PARA = oDataRow["PARA"].ToString()
,
                    CC = oDataRow["CC"].ToString()
,
                    CCO = oDataRow["CCO"].ToString()
,
                    ASUNTO = oDataRow["ASUNTO"].ToString()
,
                    MENSAJE = oDataRow["MENSAJE"].ToString()
,
                    RESTANTES = oDataRow["RESTANTES"].ToString()
,
                    IMPRESOS = oDataRow["IMPRESOS"].ToString()
,
                    ROW_ID_CERTIFICADO = oDataRow["ROW_ID_CERTIFICADO"].ToString()
,
                    CERO = oDataRow["CERO"].ToString()
,
                    ACTIVO = oDataRow["ACTIVO"].ToString()
,
                    REMITENTE = oDataRow["REMITENTE"].ToString()
,
                    ARCHIVO_NOMBRE = oDataRow["ARCHIVO_NOMBRE"].ToString()
                    ,
                    ASUNTO_CANCELADO = oDataRow["ASUNTO_CANCELADO"].ToString(),
                    MENSAJE_CANCELADO = oDataRow["MENSAJE_CANCELADO"].ToString(),

                    ASUNTO_ADJUNTO = oDataRow["ASUNTO_ADJUNTO"].ToString(),
                    MENSAJE_ADJUNTO = oDataRow["MENSAJE_ADJUNTO"].ToString(),
                    ENVIO_AUTOMATICO_MAIL = oDataRow["ENVIO_AUTOMATICO_MAIL"].ToString(),
                    serieComprobante = oDataRow["serieComprobante"].ToString(),
                    serieTraslado = oDataRow["serieTraslado"].ToString(),
                    validarComprobantePago = oDataRow["validarComprobantePago"].ToString()
                });
            }

            return lista;

        }

        public bool cargar(string ROW_IDp)
        {
            sSQL  = " SELECT * ";
            sSQL += " FROM SERIE ";
            sSQL += " WHERE ROW_ID=" + ROW_IDp + " ";
            DataTable oDataTable = oData.EjecutarConsulta(sSQL);
            if (oDataTable != null)
            {
                foreach (DataRow oDataRow in oDataTable.Rows)
                {
                    //Añadir el Registro
                    ROW_ID = oDataRow["ROW_ID"].ToString();
                    ID = oDataRow["ID"].ToString();
                    FOLIO_INICIAL = oDataRow["FOLIO_INICIAL"].ToString();
                    FOLIO_FINAL = oDataRow["FOLIO_FINAL"].ToString();
                    //APROBACION = oDataRow["APROBACION"].ToString();
                    //ANIO = oDataRow["ANIO"].ToString();
                    FORMATO = oDataRow["FORMATO"].ToString();
                    DIRECTORIO = oDataRow["DIRECTORIO"].ToString();
                    CALLE = oDataRow["CALLE"].ToString();
                    EXTERIOR = oDataRow["EXTERIOR"].ToString();
                    INTERIOR = oDataRow["INTERIOR"].ToString();
                    COLONIA = oDataRow["COLONIA"].ToString();
                    CP = oDataRow["CP"].ToString();
                    LOCALIDAD = oDataRow["LOCALIDAD"].ToString();
                    REFERENCIA = oDataRow["REFERENCIA"].ToString();
                    PAIS = oDataRow["PAIS"].ToString();
                    ESTADO = oDataRow["ESTADO"].ToString();
                    MUNICIPIO = oDataRow["MUNICIPIO"].ToString();
                    DE = oDataRow["DE"].ToString();
                    PARA = oDataRow["PARA"].ToString();
                    CC = oDataRow["CC"].ToString();
                    CCO = oDataRow["CCO"].ToString();
                    ASUNTO = oDataRow["ASUNTO"].ToString();
                    MENSAJE = oDataRow["MENSAJE"].ToString();
                    RESTANTES = oDataRow["RESTANTES"].ToString();
                    IMPRESOS = oDataRow["IMPRESOS"].ToString();
                    ROW_ID_CERTIFICADO = oDataRow["ROW_ID_CERTIFICADO"].ToString();
                    ACCOUNT_ID_RETENIDO = oDataRow["ACCOUNT_ID_RETENIDO"].ToString();
                    ACCOUNT_ID_ANTICIPO = oDataRow["ACCOUNT_ID_ANTICIPO"].ToString();
                    CERO = oDataRow["CERO"].ToString();
                    if (CERO.Trim() == "")
                    {
                        CERO = false.ToString();
                    }
                    ACTIVO = oDataRow["ACTIVO"].ToString();
                    REMITENTE = oDataRow["REMITENTE"].ToString();
                    ARCHIVO_NOMBRE = oDataRow["ARCHIVO_NOMBRE"].ToString();

                    ASUNTO_CANCELADO = oDataRow["ASUNTO_CANCELADO"].ToString();
                    MENSAJE_CANCELADO = oDataRow["MENSAJE_CANCELADO"].ToString();

                    ASUNTO_ADJUNTO = oDataRow["ASUNTO_ADJUNTO"].ToString();
                    MENSAJE_ADJUNTO = oDataRow["MENSAJE_ADJUNTO"].ToString();
                    ENVIO_AUTOMATICO_MAIL = oDataRow["ENVIO_AUTOMATICO_MAIL"].ToString();
                    serieComprobante = oDataRow["serieComprobante"].ToString();
                    serieTraslado = oDataRow["serieTraslado"].ToString();

                    CLIENTE_TRASLADO=string.Empty;
                    try
                    {
                        CLIENTE_TRASLADO = oDataRow["CLIENTE_TRASLADO"].ToString();
                    }
                    catch
                    {

                    }

                    UnirCarpeta = false;
                    try
                    {
                        UnirCarpeta = bool.Parse(oDataRow["UnirCarpeta"].ToString());
                    }
                    catch
                    {

                    }
                    


                    ndcMetodoPago = oDataRow["ndcMetodoPago"].ToString();
                    ndcUsoCfdi = oDataRow["ndcUsoCfdi"].ToString();
                    ndcClaveProd = oDataRow["ndcClaveProd"].ToString();
                    ndcUnidad = oDataRow["ndcUnidad"].ToString();
                    ndcUsarPropio = oDataRow["ndcUsarPropio"].ToString();
                    validarComprobantePago = oDataRow["validarComprobantePago"].ToString();
                    return true;
                }
            }
            return false;
        }

        //public bool cargar_serie(string SERIEp)
        //{
        //    sSQL = " SELECT * ";
        //    sSQL += " FROM SERIE ";
        //    sSQL += " WHERE ID='" + SERIEp + "' ";
        //    DataTable oDataTable = oData.EjecutarConsulta(sSQL);
        //    if (oDataTable != null)
        //    {
        //        foreach (DataRow oDataRow in oDataTable.Rows)
        //        {
        //            //Añadir el Registro
        //            ROW_ID = oDataRow["ROW_ID"].ToString();
        //            ID = oDataRow["ID"].ToString();
        //            FOLIO_INICIAL = oDataRow["FOLIO_INICIAL"].ToString();
        //            FOLIO_FINAL = oDataRow["FOLIO_FINAL"].ToString();
        //            //APROBACION = oDataRow["APROBACION"].ToString();
        //            //ANIO = oDataRow["ANIO"].ToString();
        //            ACCOUNT_ID_RETENIDO = oDataRow["ACCOUNT_ID_RETENIDO"].ToString();
        //            FORMATO = oDataRow["FORMATO"].ToString();
        //            DIRECTORIO = oDataRow["DIRECTORIO"].ToString();
        //            CALLE = oDataRow["CALLE"].ToString();
        //            EXTERIOR = oDataRow["EXTERIOR"].ToString();
        //            INTERIOR = oDataRow["INTERIOR"].ToString();
        //            COLONIA = oDataRow["COLONIA"].ToString();
        //            CP = oDataRow["CP"].ToString();
        //            LOCALIDAD = oDataRow["LOCALIDAD"].ToString();
        //            REFERENCIA = oDataRow["REFERENCIA"].ToString();
        //            PAIS = oDataRow["PAIS"].ToString();
        //            ESTADO = oDataRow["ESTADO"].ToString();
        //            MUNICIPIO = oDataRow["MUNICIPIO"].ToString();
        //            DE = oDataRow["DE"].ToString();
        //            CC = oDataRow["CC"].ToString();
        //            CCO = oDataRow["CCO"].ToString();
        //            ASUNTO = oDataRow["ASUNTO"].ToString();
        //            MENSAJE = oDataRow["MENSAJE"].ToString();
        //            RESTANTES = oDataRow["RESTANTES"].ToString();
        //            IMPRESOS = oDataRow["IMPRESOS"].ToString();
        //            ROW_ID_CERTIFICADO = oDataRow["ROW_ID_CERTIFICADO"].ToString();
        //            CERO = oDataRow["CERO"].ToString();
        //            if (CERO.Trim() == "")
        //            {
        //                CERO = false.ToString();
        //            }
        //            ACTIVO = oDataRow["ACTIVO"].ToString();
        //            REMITENTE = oDataRow["REMITENTE"].ToString();
        //            ARCHIVO_NOMBRE = oDataRow["ARCHIVO_NOMBRE"].ToString();

        //            ASUNTO_CANCELADO = oDataRow["ASUNTO_CANCELADO"].ToString();
        //            MENSAJE_CANCELADO = oDataRow["MENSAJE_CANCELADO"].ToString();

        //            ASUNTO_ADJUNTO = oDataRow["ASUNTO_ADJUNTO"].ToString();
        //            MENSAJE_ADJUNTO = oDataRow["MENSAJE_ADJUNTO"].ToString();
        //            ENVIO_AUTOMATICO_MAIL = oDataRow["ENVIO_AUTOMATICO_MAIL"].ToString();

        //            return true;
        //        }
        //    }
        //    return false;
        //}
        //Cargar serie de Traslado
        public bool cargar_serie_traslado()
        {
            sSQL = " SELECT SERIE.* ";
            sSQL += " FROM SERIE INNER JOIN CERTIFICADO ON SERIE.ROW_ID_CERTIFICADO = CERTIFICADO.ROW_ID ";
            sSQL += " WHERE SERIE.serieTraslado='True' ";
            DataTable oDataTable = oData.EjecutarConsulta(sSQL);
            if (oDataTable != null)
            {
                foreach (DataRow oDataRow in oDataTable.Rows)
                {
                    cargar(oDataRow["ROW_ID"].ToString());
                    return true;
                }
            }
            return false;
        }



        public bool cargar_serie(string SERIEp)
        {
            sSQL = " SELECT SERIE.* ";
            sSQL += " FROM SERIE INNER JOIN CERTIFICADO ON SERIE.ROW_ID_CERTIFICADO = CERTIFICADO.ROW_ID ";
            sSQL += " WHERE SERIE.ID='" + SERIEp + "'";
            DataTable oDataTable = oData.EjecutarConsulta(sSQL);
            if (oDataTable != null)
            {
                foreach (DataRow oDataRow in oDataTable.Rows)
                {
                    //Añadir el Registro
                    ROW_ID = oDataRow["ROW_ID"].ToString();
                    ID = oDataRow["ID"].ToString();
                    FOLIO_INICIAL = oDataRow["FOLIO_INICIAL"].ToString();
                    FOLIO_FINAL = oDataRow["FOLIO_FINAL"].ToString();
                    //APROBACION = oDataRow["APROBACION"].ToString();
                    //ANIO = oDataRow["ANIO"].ToString();
                    ACCOUNT_ID_RETENIDO = oDataRow["ACCOUNT_ID_RETENIDO"].ToString();
                    ACCOUNT_ID_ANTICIPO = oDataRow["ACCOUNT_ID_ANTICIPO"].ToString();
                    FORMATO = oDataRow["FORMATO"].ToString();
                    DIRECTORIO = oDataRow["DIRECTORIO"].ToString();
                    CALLE = oDataRow["CALLE"].ToString();
                    EXTERIOR = oDataRow["EXTERIOR"].ToString();
                    INTERIOR = oDataRow["INTERIOR"].ToString();
                    COLONIA = oDataRow["COLONIA"].ToString();
                    CP = oDataRow["CP"].ToString();
                    LOCALIDAD = oDataRow["LOCALIDAD"].ToString();
                    REFERENCIA = oDataRow["REFERENCIA"].ToString();
                    PAIS = oDataRow["PAIS"].ToString();
                    ESTADO = oDataRow["ESTADO"].ToString();
                    MUNICIPIO = oDataRow["MUNICIPIO"].ToString();
                    DE = oDataRow["DE"].ToString();
                    PARA = oDataRow["PARA"].ToString();
                    CC = oDataRow["CC"].ToString();
                    CCO = oDataRow["CCO"].ToString();
                    ASUNTO = oDataRow["ASUNTO"].ToString();
                    MENSAJE = oDataRow["MENSAJE"].ToString();
                    RESTANTES = oDataRow["RESTANTES"].ToString();
                    IMPRESOS = oDataRow["IMPRESOS"].ToString();
                    ROW_ID_CERTIFICADO = oDataRow["ROW_ID_CERTIFICADO"].ToString();
                    CERO = oDataRow["CERO"].ToString();
                    if (CERO.Trim() == "")
                    {
                        CERO = false.ToString();
                    }
                    ACTIVO = oDataRow["ACTIVO"].ToString();
                    REMITENTE = oDataRow["REMITENTE"].ToString();
                    ARCHIVO_NOMBRE = oDataRow["ARCHIVO_NOMBRE"].ToString();

                    ASUNTO_CANCELADO = oDataRow["ASUNTO_CANCELADO"].ToString();
                    MENSAJE_CANCELADO = oDataRow["MENSAJE_CANCELADO"].ToString();

                    ASUNTO_ADJUNTO = oDataRow["ASUNTO_ADJUNTO"].ToString();
                    MENSAJE_ADJUNTO = oDataRow["MENSAJE_ADJUNTO"].ToString();
                    ENVIO_AUTOMATICO_MAIL = oDataRow["ENVIO_AUTOMATICO_MAIL"].ToString();
                    serieComprobante = oDataRow["serieComprobante"].ToString();
                    serieTraslado = oDataRow["serieTraslado"].ToString();

                    ndcMetodoPago = oDataRow["ndcMetodoPago"].ToString();
                    ndcUsoCfdi = oDataRow["ndcUsoCfdi"].ToString();
                    ndcClaveProd = oDataRow["ndcClaveProd"].ToString();
                    ndcUnidad = oDataRow["ndcUnidad"].ToString();
                    ndcUsarPropio = oDataRow["ndcUsarPropio"].ToString();
                    validarComprobantePago = oDataRow["validarComprobantePago"].ToString();
                    CLIENTE_TRASLADO = string.Empty;
                    try
                    {
                        CLIENTE_TRASLADO = oDataRow["CLIENTE_TRASLADO"].ToString();
                    }
                    catch
                    {

                    }
                    UnirCarpeta = false;
                    try
                    {
                        UnirCarpeta = bool.Parse(oDataRow["UnirCarpeta"].ToString());
                    }
                    catch
                    {

                    }
                    return true;
                }
            }
            return false;
        }


        public bool cargar_serie(string SERIEp, string ROW_ID_EMPRESAp)
        {
            sSQL = " SELECT SERIE.* ";
            sSQL += " FROM SERIE INNER JOIN CERTIFICADO ON SERIE.ROW_ID_CERTIFICADO = CERTIFICADO.ROW_ID ";
            sSQL += " WHERE SERIE.ID='" + SERIEp + "' AND CERTIFICADO.ROW_ID_EMPRESA=" + ROW_ID_EMPRESAp + "";

            DataTable oDataTable = oData.EjecutarConsulta(sSQL);
            if (oDataTable != null)
            {
                foreach (DataRow oDataRow in oDataTable.Rows)
                {
                    //Añadir el Registro
                    ROW_ID = oDataRow["ROW_ID"].ToString();
                    ID = oDataRow["ID"].ToString();
                    FOLIO_INICIAL = oDataRow["FOLIO_INICIAL"].ToString();
                    FOLIO_FINAL = oDataRow["FOLIO_FINAL"].ToString();
                    //APROBACION = oDataRow["APROBACION"].ToString();
                    //ANIO = oDataRow["ANIO"].ToString();
                    ACCOUNT_ID_RETENIDO = oDataRow["ACCOUNT_ID_RETENIDO"].ToString();
                    ACCOUNT_ID_ANTICIPO = oDataRow["ACCOUNT_ID_ANTICIPO"].ToString();
                    FORMATO = oDataRow["FORMATO"].ToString();
                    DIRECTORIO = oDataRow["DIRECTORIO"].ToString();
                    CALLE = oDataRow["CALLE"].ToString();
                    EXTERIOR = oDataRow["EXTERIOR"].ToString();
                    INTERIOR = oDataRow["INTERIOR"].ToString();
                    COLONIA = oDataRow["COLONIA"].ToString();
                    CP = oDataRow["CP"].ToString();
                    LOCALIDAD = oDataRow["LOCALIDAD"].ToString();
                    REFERENCIA = oDataRow["REFERENCIA"].ToString();
                    PAIS = oDataRow["PAIS"].ToString();
                    ESTADO = oDataRow["ESTADO"].ToString();
                    MUNICIPIO = oDataRow["MUNICIPIO"].ToString();
                    DE = oDataRow["DE"].ToString();

                    PARA = oDataRow["PARA"].ToString();
                    CC = oDataRow["CC"].ToString();
                    CCO = oDataRow["CCO"].ToString();
                    ASUNTO = oDataRow["ASUNTO"].ToString();
                    MENSAJE = oDataRow["MENSAJE"].ToString();
                    RESTANTES = oDataRow["RESTANTES"].ToString();
                    IMPRESOS = oDataRow["IMPRESOS"].ToString();
                    ROW_ID_CERTIFICADO = oDataRow["ROW_ID_CERTIFICADO"].ToString();
                    CERO = oDataRow["CERO"].ToString();
                    if (CERO.Trim() == "")
                    {
                        CERO = false.ToString();
                    }
                    ACTIVO = oDataRow["ACTIVO"].ToString();
                    REMITENTE = oDataRow["REMITENTE"].ToString();
                    ARCHIVO_NOMBRE = oDataRow["ARCHIVO_NOMBRE"].ToString();
                    
                    ASUNTO_CANCELADO = oDataRow["ASUNTO_CANCELADO"].ToString();
                    MENSAJE_CANCELADO = oDataRow["MENSAJE_CANCELADO"].ToString();

                    ASUNTO_ADJUNTO = oDataRow["ASUNTO_ADJUNTO"].ToString();
                    MENSAJE_ADJUNTO = oDataRow["MENSAJE_ADJUNTO"].ToString();
                    ENVIO_AUTOMATICO_MAIL = oDataRow["ENVIO_AUTOMATICO_MAIL"].ToString();
                    if (String.IsNullOrEmpty(ENVIO_AUTOMATICO_MAIL))
                    {
                        ENVIO_AUTOMATICO_MAIL = "True";
                    }
                    serieComprobante = oDataRow["serieComprobante"].ToString();
                    if (String.IsNullOrEmpty(serieComprobante))
                    {
                        serieComprobante = "True";
                    }


                    serieTraslado = oDataRow["serieTraslado"].ToString();
                    if (String.IsNullOrEmpty(serieTraslado))
                    {
                        serieTraslado = "True";
                    }

                    validarComprobantePago = oDataRow["validarComprobantePago"].ToString();
                    if (String.IsNullOrEmpty(validarComprobantePago))
                    {
                        validarComprobantePago = "False";
                    }
                    ndcMetodoPago = oDataRow["ndcMetodoPago"].ToString();
                    ndcUsoCfdi = oDataRow["ndcUsoCfdi"].ToString();
                    ndcClaveProd = oDataRow["ndcClaveProd"].ToString();
                    ndcUnidad = oDataRow["ndcUnidad"].ToString();
                    ndcUsarPropio = oDataRow["ndcUsarPropio"].ToString();

                    CLIENTE_TRASLADO = "";
                    try
                    {
                        CLIENTE_TRASLADO = oDataRow["CLIENTE_TRASLADO"].ToString();
                    }
                    catch
                    {

                    }
                    UnirCarpeta = false;
                    try
                    {
                        UnirCarpeta = bool.Parse(oDataRow["UnirCarpeta"].ToString());
                    }
                    catch
                    {

                    }
                    return true;
                }
            }
            return false;
        }

        public bool VerificarEscrituraAcceso(string directorio)
        {
            try
            {
                using (FileStream fs = File.Create(
                    Path.Combine(
                        directorio,
                        Path.GetRandomFileName()
                    ),
                    1,
                    FileOptions.DeleteOnClose)
                )
                { }
                return true;
            }
            catch
            {
                return false;
            }
        }

        public cSERIE cargar_ultima_serie(string ROW_IDp)
        {
            cSERIE oSERIE = new cSERIE();
            sSQL = " SELECT * ";
            sSQL += " FROM SERIE ";
            sSQL += " WHERE ROW_ID=" + ROW_IDp + " ";
            DataTable oDataTable = oData.EjecutarConsulta(sSQL);
            if (oDataTable != null)
            {
                foreach (DataRow oDataRow in oDataTable.Rows)
                {
                                //Validar el siguiente folio que no se salga del rango
                    //Añadir el Registro
                    ROW_ID = oDataRow["ROW_ID"].ToString();
                    ID = oDataRow["ID"].ToString();
                    //Buscar la Ultima Serie que no tenga Folios Vencidos
                    sSQL = " SELECT TOP 1 * ";
                    sSQL += " FROM SERIE ";
                    sSQL += " WHERE (SERIE.[ID])='" + ID + "' ";
                    sSQL += " AND ((SERIE.IMPRESOS)<(SERIE.FOLIO_FINAL)) ";
                    DataTable oDataTable1 = oData.EjecutarConsulta(sSQL);
                    if (oDataTable1 != null)
                    {
                        foreach (DataRow oDataRow1 in oDataTable1.Rows)
                        {
                            oSERIE.ROW_ID = oDataRow1["ROW_ID"].ToString();
                            oSERIE.ID = oDataRow1["ID"].ToString();
                            oSERIE.FOLIO_INICIAL = oDataRow1["FOLIO_INICIAL"].ToString();
                            oSERIE.FOLIO_FINAL = oDataRow1["FOLIO_FINAL"].ToString();
                            oSERIE.ACCOUNT_ID_RETENIDO = oDataRow["ACCOUNT_ID_RETENIDO"].ToString();
                            oSERIE.ACCOUNT_ID_ANTICIPO = oDataRow["ACCOUNT_ID_ANTICIPO"].ToString();
                            //oSERIE.APROBACION = oDataRow1["APROBACION"].ToString();
                            //oSERIE.ANIO = oDataRow1["ANIO"].ToString();
                            oSERIE.FORMATO = oDataRow["FORMATO"].ToString();
                            oSERIE.DIRECTORIO = oDataRow1["DIRECTORIO"].ToString();
                            oSERIE.CALLE = oDataRow1["CALLE"].ToString();
                            oSERIE.EXTERIOR = oDataRow1["EXTERIOR"].ToString();
                            oSERIE.INTERIOR = oDataRow1["INTERIOR"].ToString();
                            oSERIE.COLONIA = oDataRow1["COLONIA"].ToString();
                            oSERIE.CP = oDataRow1["CP"].ToString();
                            oSERIE.LOCALIDAD = oDataRow1["LOCALIDAD"].ToString();
                            oSERIE.REFERENCIA = oDataRow1["REFERENCIA"].ToString();
                            oSERIE.PAIS = oDataRow1["PAIS"].ToString();
                            oSERIE.ESTADO = oDataRow1["ESTADO"].ToString();
                            oSERIE.MUNICIPIO = oDataRow1["MUNICIPIO"].ToString();
                            oSERIE.DE = oDataRow1["DE"].ToString();
                            oSERIE.PARA = oDataRow["PARA"].ToString();
                            oSERIE.CC = oDataRow1["CC"].ToString();
                            oSERIE.CCO = oDataRow1["CCO"].ToString();
                            oSERIE.ASUNTO = oDataRow1["ASUNTO"].ToString();
                            oSERIE.MENSAJE = oDataRow1["MENSAJE"].ToString();
                            oSERIE.RESTANTES = oDataRow1["RESTANTES"].ToString();
                            oSERIE.IMPRESOS = oDataRow1["IMPRESOS"].ToString();
                            oSERIE.ROW_ID_CERTIFICADO = oDataRow1["ROW_ID_CERTIFICADO"].ToString();
                            oSERIE.REMITENTE = oDataRow1["REMITENTE"].ToString();
                            //oSERIE.CERO = oDataRow["CERO"].ToString();
                            oSERIE.ARCHIVO_NOMBRE = oDataRow1["ARCHIVO_NOMBRE"].ToString();

                            oSERIE.ASUNTO_CANCELADO = oDataRow1["ASUNTO_CANCELADO"].ToString();
                            oSERIE.MENSAJE_CANCELADO = oDataRow1["MENSAJE_CANCELADO"].ToString();

                            oSERIE.ASUNTO_ADJUNTO = oDataRow1["ASUNTO_ADJUNTO"].ToString();
                            oSERIE.MENSAJE_ADJUNTO = oDataRow1["MENSAJE_ADJUNTO"].ToString();
                            oSERIE.ENVIO_AUTOMATICO_MAIL = oDataRow["ENVIO_AUTOMATICO_MAIL"].ToString();
                            oSERIE.serieComprobante = oDataRow["serieComprobante"].ToString();
                            if (String.IsNullOrEmpty(oSERIE.serieComprobante))
                            {
                                oSERIE.serieComprobante = "True";
                            }

                            oSERIE.serieTraslado = oDataRow["serieTraslado"].ToString();
                            if (String.IsNullOrEmpty(oSERIE.serieTraslado))
                            {
                                oSERIE.serieTraslado = "True";

                            }

                            oSERIE.validarComprobantePago = oDataRow["validarComprobantePago"].ToString();
                            if (String.IsNullOrEmpty(oSERIE.validarComprobantePago))
                            {
                                oSERIE.validarComprobantePago = "False";
                            }
                            oSERIE.CLIENTE_TRASLADO = "";
                            try
                            {
                                oSERIE.CLIENTE_TRASLADO = oDataRow["CLIENTE_TRASLADO"].ToString();
                            }
                            catch
                            {

                            }
                            oSERIE.UnirCarpeta = false;
                            try
                            {
                                oSERIE.UnirCarpeta = bool.Parse(oDataRow["UnirCarpeta"].ToString());
                            }
                            catch
                            {

                            }
                            oSERIE.ndcMetodoPago = oDataRow["ndcMetodoPago"].ToString();
                            oSERIE.ndcUsoCfdi = oDataRow["ndcUsoCfdi"].ToString();
                            oSERIE.ndcClaveProd = oDataRow["ndcClaveProd"].ToString();
                            oSERIE.ndcUnidad = oDataRow["ndcUnidad"].ToString();
                            oSERIE.ndcUsarPropio = oDataRow["ndcUsarPropio"].ToString();

                            return oSERIE;
                        }
                    }
                    else
                    {
                        return null;
                    }
                }
            }
            return null;
        }
        public bool guardar()
        {


            if (ROW_ID_CERTIFICADO == "")
            {
                MessageBox.Show("Selecione un Certificado.", Application.ProductName + "-" + Application.ProductVersion, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return false;
            }

            //sSQL = " SELECT * ";
            //sSQL += " FROM SERIE ";
            //sSQL += " WHERE ID='" + ID + "' AND APROBACION='" + APROBACION + "' AND ROW_ID_CERTIFICADO=" + ROW_ID_CERTIFICADO  + " ";
            //DataTable oDataTable = oData.EjecutarConsulta(sSQL);
            //foreach (DataRow oDataRow in oDataTable.Rows)
            //{
            //    //Añadir el Registro
            //    if (ROW_ID != oDataRow["ROW_ID"].ToString())
            //    {
            //        MessageBox.Show("La serie " + ID + " con la Aprobación " + APROBACION + " ya existe.", Application.ProductName + "-" + Application.ProductVersion, MessageBoxButtons.OK, MessageBoxIcon.Information);
            //        return false;
            //    }

            //}

            if (DIRECTORIO == "")
            {
                MessageBox.Show("El Directorio es un campo requerido.", Application.ProductName + "-" + Application.ProductVersion, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return false;
            }
            if (CALLE == "")
            {
                MessageBox.Show("La calle es un campo requerido.", Application.ProductName + "-" + Application.ProductVersion, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return false;
            }

            if (MUNICIPIO == "")
            {
                MessageBox.Show("El municipio es un campo requerido.", Application.ProductName + "-" + Application.ProductVersion, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return false;
            }

            if (ESTADO == "")
            {
                MessageBox.Show("El estado es un campo requerido.", Application.ProductName + "-" + Application.ProductVersion, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return false;
            }

            if (PAIS == "")
            {
                MessageBox.Show("El país es un campo requerido.", Application.ProductName + "-" + Application.ProductVersion, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return false;
            }

            if (CP == "")
            {
                MessageBox.Show("El código postal es un campo requerido.", Application.ProductName + "-" + Application.ProductVersion, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return false;
            }
            if (CP.Length < 5)
            {
                MessageBox.Show("El código postal debe tener al menos 5 caracteres.", Application.ProductName + "-" + Application.ProductVersion, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return false;
            }
            if (DE.Trim() == "")
            {
                MessageBox.Show("El Origen del Correo Electronico es un campo requerido.", Application.ProductName + "-" + Application.ProductVersion, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return false;
            }


            if (ASUNTO.Trim() == "")
            {
                MessageBox.Show("El Asunto del Correo Electronico es un campo requerido.", Application.ProductName + "-" + Application.ProductVersion, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return false;
            }

            if (MENSAJE.Trim() == "")
            {
                MessageBox.Show("El Mensaje del Correo Electronico es un campo requerido.", Application.ProductName + "-" + Application.ProductVersion, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return false;
            }
            if (!validar_cuentas(ACCOUNT_ID_RETENIDO))
            {
                MessageBox.Show("Verifique las cuentas de Retención alguna no existe.", Application.ProductName + "-" + Application.ProductVersion, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return false;
            }
            ASUNTO = ASUNTO.Replace(Environment.NewLine, "\n");

            if (String.IsNullOrEmpty(IMPRESOS.Trim()))
            {
                IMPRESOS = "0";
            }

            if (ROW_ID == "")
            {




                sSQL = " INSERT INTO SERIE ";
                sSQL += " ( ";
                sSQL += " ID,FORMATO";
                sSQL += ", DIRECTORIO ";
                sSQL += ", CALLE, EXTERIOR, INTERIOR ";
                sSQL += ", COLONIA, CP, LOCALIDAD ";
                sSQL += ", REFERENCIA, PAIS, ESTADO ";
                sSQL += ", MUNICIPIO, DE, CC ";
                sSQL += ", CCO, ASUNTO, MENSAJE ";
                sSQL += ", ROW_ID_CERTIFICADO ";
                sSQL += ", ACTIVO, REMITENTE";
                sSQL += ", ARCHIVO_NOMBRE ";
                sSQL += ",ACCOUNT_ID_RETENIDO,ACCOUNT_ID_ANTICIPO";
                sSQL += ",ASUNTO_CANCELADO,MENSAJE_CANCELADO";
                sSQL += ",ASUNTO_ADJUNTO,MENSAJE_ADJUNTO";
                sSQL += ",ENVIO_AUTOMATICO_MAIL,PARA";
                sSQL += @",rfcExcepcion,serieComprobante,serieTraslado
                        ,ndcMetodoPago,ndcUsoCfdi,ndcClaveProd,ndcUnidad,ndcUsarPropio,validarComprobantePago
                        ,CLIENTE_TRASLADO,UnirCarpeta
                        ";
                sSQL += " )";
                sSQL += " VALUES ";
                sSQL += "(";
                sSQL += "  '" + ID + "','" + FORMATO + "'  ";
                sSQL += ",'" + DIRECTORIO + "'";
                sSQL += ",'" + CALLE+ "','" + EXTERIOR+ "','" + INTERIOR + "'";
                sSQL += ",'" + COLONIA+ "','" + CP+ "','" + LOCALIDAD + "'";
                sSQL += ",'" + REFERENCIA+ "','" + PAIS+ "','" + ESTADO + "'";
                sSQL += ",'" + MUNICIPIO+ "','" + DE+ "','" + CC + "'";
                sSQL += ",'" + CCO+ "','" + ASUNTO+ "','" + MENSAJE + "'";
                sSQL += ", " + ROW_ID_CERTIFICADO + " ";
                sSQL += ",'" + ACTIVO + "','" + REMITENTE + "'";
                sSQL += ",'" + ARCHIVO_NOMBRE + "'";
                sSQL += ",'" + ACCOUNT_ID_RETENIDO + "','" + ACCOUNT_ID_ANTICIPO + "'";
                sSQL += ",'" + ASUNTO_CANCELADO + "','" + MENSAJE_CANCELADO + "'";
                sSQL += ",'" + ASUNTO_ADJUNTO + "','" + MENSAJE_ADJUNTO + "'";
                sSQL += ",'" + ENVIO_AUTOMATICO_MAIL + "','" + PARA + "'";
                sSQL += ",'" + rfcExcepcion + "','" + serieComprobante + "','" + serieTraslado + "'";
                sSQL += ",'" + ndcMetodoPago + "','" + ndcUsoCfdi + "','" + ndcClaveProd 
                    + "','" + ndcUnidad + "','" + ndcUsarPropio + "','" + validarComprobantePago + "'"
                    + ",'" + CLIENTE_TRASLADO + "','" + UnirCarpeta.ToString() + "'"
                    ;
                sSQL += ")";
                DataTable oDataTable = oData.EjecutarConsulta(sSQL);
                if (oDataTable!=null)
                {

                    sSQL = "SELECT TOP 1 MAX(ROW_ID) as ROW_ID FROM SERIE";
                    oDataTable = oData.EjecutarConsulta(sSQL);
                    if (oDataTable != null)
                    {
                        foreach (DataRow oDataRow in oDataTable.Rows)
                        {
                            ROW_ID = oDataRow["ROW_ID"].ToString();
                        }
                    }
                }

            }
            else
            {
                sSQL = " UPDATE SERIE SET ";
                sSQL += "  ID='" + ID + "',FORMATO='" + FORMATO + "'";
                sSQL += ",DIRECTORIO='" + DIRECTORIO + "'";
                sSQL += ",CALLE='" + CALLE+ "',EXTERIOR='" + EXTERIOR+ "',INTERIOR='" + INTERIOR + "'";
                sSQL += ",COLONIA='" + COLONIA+ "',CP='" + CP+ "',LOCALIDAD='" + LOCALIDAD + "'";
                sSQL += ",REFERENCIA='" + REFERENCIA+ "',PAIS='" + PAIS+ "',ESTADO='" + ESTADO + "'";
                sSQL += ",MUNICIPIO='" + MUNICIPIO+ "',DE='" + DE+ "',CC='" + CC + "'";
                sSQL += ",CCO='" + CCO + "',ASUNTO='" + ASUNTO + "',MENSAJE='" + MENSAJE + "'";
                sSQL += ",ROW_ID_CERTIFICADO=" + ROW_ID_CERTIFICADO + " ";
                sSQL += ",CERO='" + CERO + "'";
                sSQL += ",ACTIVO='" + ACTIVO + "',REMITENTE='" + REMITENTE + "'";
                sSQL += ", ARCHIVO_NOMBRE='" + ARCHIVO_NOMBRE  + "' ";
                sSQL += ",ACCOUNT_ID_RETENIDO='" + ACCOUNT_ID_RETENIDO + "',ACCOUNT_ID_ANTICIPO='" + ACCOUNT_ID_ANTICIPO + "'";
                sSQL += ",ASUNTO_CANCELADO='" + ASUNTO_CANCELADO + "',MENSAJE_CANCELADO='" + MENSAJE_CANCELADO + "'";
                sSQL += ",ASUNTO_ADJUNTO='" + ASUNTO_ADJUNTO + "',MENSAJE_ADJUNTO='" + MENSAJE_ADJUNTO + "'";
                sSQL += ",ENVIO_AUTOMATICO_MAIL='" + ENVIO_AUTOMATICO_MAIL + "',PARA='" + PARA + "'";
                sSQL += ",rfcExcepcion='" + rfcExcepcion + "',serieComprobante='" + serieComprobante + "',serieTraslado='" + serieTraslado + "'";
                sSQL += @",ndcMetodoPago='" + ndcMetodoPago + "',ndcUsoCfdi='" + ndcUsoCfdi + "',ndcClaveProd='" + ndcClaveProd + 
                    @"',ndcUnidad='" + ndcUnidad + "',ndcUsarPropio='" + ndcUsarPropio + "',validarComprobantePago='" + validarComprobantePago + "'"
                    + ",CLIENTE_TRASLADO='" + CLIENTE_TRASLADO + "', UnirCarpeta='" + UnirCarpeta.ToString() + "'"
                    ;
                sSQL += " WHERE ROW_ID=" + ROW_ID + " ";
                oData.EjecutarConsulta(sSQL);

            }
            //Si es SQL Server guardar en la tabla del SQL Server 
            //guardar_series(ACCOUNT_ID_RETENIDO);

            return true;

        }

        private bool validar_cuentas(string ACCOUNT_ID_RETENIDOp)
        {
            //Guardar las Series en SQL SERVER
            //Buscar la conexion de la EMPRESA segun el certificado
            cCERTIFICADO ocCERTIFICADO = new cCERTIFICADO();
            if (ocCERTIFICADO.cargar(ROW_ID_CERTIFICADO))
            {
                //Cargar la empresa
                cEMPRESA ocEMPRESA = new cEMPRESA(oData);
                if (ocEMPRESA.cargar(ocCERTIFICADO.ROW_ID_EMPRESA))
                {
                    //Cargar la conexion 
                    cCONEXCION oData_ERP = new cCONEXCION("");
                    oData_ERP = new cCONEXCION(ocEMPRESA.TIPO, ocEMPRESA.SERVIDOR, ocEMPRESA.BD, ocEMPRESA.USUARIO_BD, ocEMPRESA.PASSWORD_BD);
                    
                    string[] cuentas = ACCOUNT_ID_RETENIDOp.Split(',');
                    //En la tabla VMX_FE_SERIES_ACCOUNT guardar si existe
                    for (int i = 0; i < cuentas.Length; i++)
                    {
                        if (cuentas[i] != "")
                        {
                            string sSQL = "SELECT * FROM ACCOUNT WHERE ID='" + cuentas[i] + "'";
                            DataTable oDataTable = oData_ERP.EjecutarConsulta(sSQL);
                            if (oDataTable != null)
                            {
                                if (oDataTable.Rows.Count == 0)
                                {
                                    return false;
                                }
                            }
                        }
                    }
                }
            }
            return true;
        }

        
        private void guardar_series(string ACCOUNT_ID_RETENIDOp)
        {
            //Guardar las Series en SQL SERVER
            //Buscar la conexion de la EMPRESA segun el certificado
            cCERTIFICADO ocCERTIFICADO = new cCERTIFICADO();
            if (ocCERTIFICADO.cargar(ROW_ID_CERTIFICADO))
            {
                //Cargar la empresa
                cEMPRESA ocEMPRESA = new cEMPRESA(oData);
                if (ocEMPRESA.cargar(ocCERTIFICADO.ROW_ID_EMPRESA))
                {
                    //Cargar la conexion 
                    cCONEXCION oData_ERP = new cCONEXCION("");
                    oData_ERP = new cCONEXCION(ocEMPRESA.TIPO, ocEMPRESA.SERVIDOR, ocEMPRESA.BD, ocEMPRESA.USUARIO_BD, ocEMPRESA.PASSWORD_BD);
                    //Solo para SQL SERVER
                    if (ocEMPRESA.TIPO.ToUpper() == "SQLSERVER")
                    {
                        string sSQL = "";
                        string BD_AUXILIAR_tr = "";
                        if (ocEMPRESA.BD_AUXILIAR != "")
                        {
                            BD_AUXILIAR_tr = ocEMPRESA.BD_AUXILIAR + ".dbo.";
                        }
                        sSQL += " SELECT ROW_ID FROM " + BD_AUXILIAR_tr + "VMX_FE_SERIES WHERE ID='" + ID + "'";
                        DataTable oDataTable = oData_ERP.EjecutarConsulta(sSQL);
                        string ROW_ID_SERIE = "";
                        
                        if (oDataTable != null)
                        {
                            foreach (DataRow oDataRow in oDataTable.Rows)
                            {
                                ROW_ID_SERIE = oDataRow["ROW_ID"].ToString();

                            }
                        }

                        sSQL = " DELETE FROM " + BD_AUXILIAR_tr + "VMX_FE_SERIES_ACCOUNT WHERE ROW_ID_SERIE="+ROW_ID_SERIE;
                        oData_ERP.EjecutarConsulta(sSQL);
                        string[] cuentas = ACCOUNT_ID_RETENIDOp.Split(',');
                        //En la tabla VMX_FE_SERIES_ACCOUNT guardar si existe
                        for (int i = 0; i < cuentas.Length; i++)
                        {
                            //Guardar la cuenta si no existe
                            sSQL = "SELECT TOP 1 * FROM " + BD_AUXILIAR_tr + "VMX_FE_SERIES_ACCOUNT WHERE ACCOUNT_ID='" + cuentas[i] + "' AND ROW_ID_SERIE="+ROW_ID_SERIE;
                            oDataTable = oData_ERP.EjecutarConsulta(sSQL);
                            if (oDataTable != null)
                            {
                                if (oDataTable.Rows.Count == 0)
                                {
                                    if (cuentas[i] != "")
                                    {
                                        //Insertar la cuenta que falta
                                        sSQL = " INSERT INTO " + BD_AUXILIAR_tr + "VMX_FE_SERIES_ACCOUNT (ROW_ID_SERIE,ACCOUNT_ID) ";
                                        sSQL += " VALUES (" + ROW_ID_SERIE + ",'" + cuentas[i] + "')";
                                        oData_ERP.EjecutarConsulta(sSQL);
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        public void incrementar(string ROW_ID_SERIEp)
        {

            if (ROW_ID_SERIEp != "")
            {
                sSQL = " UPDATE SERIE SET ";
                sSQL += "  IMPRESOS=IMPRESOS+1 ";
                sSQL += " WHERE ROW_ID=" + ROW_ID_SERIEp + " ";
                oData.EjecutarConsulta(sSQL);
            }
        }
        public string existe(string ROW_ID_SERIEp, string IMPRESOSp)
        {
            //Verificar que no exista
            sSQL = " SELECT * ";
            sSQL += " FROM FACTURA ";
            sSQL += " WHERE ID='" + IMPRESOS + "' AND ROW_ID_SERIE=" + ROW_ID_SERIEp;
            DataTable oDataTable1 = oData.EjecutarConsulta(sSQL);
            if (oDataTable1 != null)
            {
                foreach (DataRow oDataRow1 in oDataTable1.Rows)
                {
                    sSQL = "UPDATE SERIE SET IMPRESOS=IMPRESOS+1 WHERE ROW_ID=" + ROW_ID_SERIEp;
                    oData.EjecutarConsulta(sSQL);
                }
            }
            return "";
        }

        public bool eliminar(string ROW_IDp)
        {
            if (ROW_IDp != "")
            {
                sSQL = "DELETE FROM SERIE WHERE ROW_ID=" + ROW_IDp;
                oData.EjecutarConsulta(sSQL);
                return true;
            }
            return false;

        }

        public string consecutivo(string ROW_ID_SERIEp, bool verificar=true)
        {
            if (ROW_ID_SERIEp != "")
            {
                sSQL = " SELECT * ";
                sSQL += " FROM SERIE ";
                sSQL += " WHERE ROW_ID=" + ROW_ID_SERIEp + " ";
                DataTable oDataTable = oData.EjecutarConsulta(sSQL);
                if (oDataTable != null)
                {
                    foreach (DataRow oDataRow in oDataTable.Rows)
                    {
                        //Añadir el Registro
                        string IMPRESOS=oDataRow["IMPRESOS"].ToString();
                        if (String.IsNullOrEmpty(IMPRESOS))
                        {
                            iniciarImpresos(ROW_ID_SERIEp);
                            IMPRESOS = "0";
                        }
                        string FOLIO_FINAL = oDataRow["FOLIO_FINAL"].ToString();
                        if (oDataRow["CERO"].ToString().Trim() == "True")
                        {
                            IMPRESOS = "0000000000000" + IMPRESOS;
                            IMPRESOS = IMPRESOS.Substring(IMPRESOS.Length - FOLIO_FINAL.Length, FOLIO_FINAL.Length);
                        
                        }
                        
                        //Verificar que exista
                        if (verificar)
                        {
                            sSQL = " SELECT * ";
                            sSQL += " FROM FACTURA ";
                            sSQL += " WHERE ROW_ID_SERIE=" + ROW_ID_SERIEp + " ";
                            sSQL += " AND ID='" + IMPRESOS + "' ";
                            DataTable oDataTable1 = oData.EjecutarConsulta(sSQL);
                            if (oDataTable1 != null)
                            {
                                foreach (DataRow oDataRow1 in oDataTable1.Rows)
                                {
                                    cSERIE ocSERIE = new cSERIE();
                                    ocSERIE.incrementar(ROW_ID_SERIEp);
                                    IMPRESOS = ocSERIE.consecutivo(ROW_ID_SERIEp);
                                }

                            }
                        }

                        if (IMPRESOS == "")
                        {
                            IMPRESOS = "00000";
                        }


                        return IMPRESOS;
                    }
                }
            }
            return "-1";
        }

        public void iniciarImpresos(string ROW_ID_SERIEp)
        {

            if (ROW_ID_SERIEp != "")
            {
                sSQL = " UPDATE SERIE SET ";
                sSQL += "  IMPRESOS='0' ";
                sSQL += " WHERE ROW_ID=" + ROW_ID_SERIEp + " ";
                oData.EjecutarConsulta(sSQL);

            }
        }
        public void actualizarImpresos(string ROW_ID_SERIEp, string IMPRESOSp)
        {

            if (ROW_ID_SERIEp != "")
            {
                sSQL = " UPDATE SERIE SET ";
                sSQL += "  IMPRESOS='" + IMPRESOSp  + "' ";
                sSQL += " WHERE ROW_ID=" + ROW_ID_SERIEp + " ";
                oData.EjecutarConsulta(sSQL);

            }
        }

        public string rfcExcepcion { get; set; }
        public string ndcMetodoPago { get; internal set; }
        public string ndcUsoCfdi { get; internal set; }
        public string ndcClaveProd { get; internal set; }
        public string ndcUnidad { get; internal set; }
        public string ndcUsarPropio { get; internal set; }
        public string CLIENTE_TRASLADO { get; internal set; }
        public bool UnirCarpeta { get; set; }
    }
}
