﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace FE_FX
{
    public partial class frmFraccionArancelaria : Form
    {
        public cce11_v32.c_FraccionArancelaria fraccionArancelaria;
        public string fracciontr, descripcion;

        public frmFraccionArancelaria(string descripcion)
        {
            InitializeComponent();
            label2.Text = descripcion;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            validar(fraccion.Text);
        }

        private void validar(string fraccion)
        {
            try
            {
                //cce11_v32.c_FraccionArancelaria oc_FraccionArancelaria = (cce11_v32.c_FraccionArancelaria)System.Enum.Parse(typeof(cce11_v32.c_FraccionArancelaria), "Item" + fraccion);
                //fraccionArancelaria = oc_FraccionArancelaria;
                fracciontr = fraccion;
                DialogResult = DialogResult.OK;
            }
            catch (Exception exFraccion)
            {
                string mensaje = "Error en CCE. Fracción Arancelaría " + fraccion + ". " + exFraccion.Message.ToString();
                if (exFraccion.InnerException != null)
                {
                    mensaje += Environment.NewLine + exFraccion.InnerException.Message.ToString();
                }
                MessageBox.Show(mensaje, Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error);
                
            }
        }
    }
}
