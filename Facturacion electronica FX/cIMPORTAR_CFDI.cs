﻿using System;
using System.Collections.Generic;

using System.Text;
using System.Data;
using System.Windows;
using System.Windows.Forms;
using Generales;
using ModeloDocumentosFX;

namespace FE_FX
{
    class cIMPORTAR_CFDI
    {
        private string sSQL = "";
        public string ROW_ID { get; set; }
        public string INVOICE_ID { get; set; }
        public string UUID { get; set; }

        private cCONEXCION oData = new cCONEXCION("");

        public cIMPORTAR_CFDI()
        {
            ModeloDocumentosFXContainer dbContext = new ModeloDocumentosFXContainer();
            try
            {
                string conection = dbContext.Database.Connection.ConnectionString;
                oData = new cCONEXCION(conection);
            }
            catch (Exception err)
            {
                ErrorFX.mostrar(err, false, true, "cIMPORTAR_CFDI - 71 - ");
            }
            limpiar();
        }

        public void limpiar()
        {
            ROW_ID = "";
            INVOICE_ID = "";
            UUID = "";
        }

        public List<cIMPORTAR_CFDI> todos(string BUSQUEDA)
        {

            List<cIMPORTAR_CFDI> lista = new List<cIMPORTAR_CFDI>();
            cEMPRESA oEMPRESA = new cEMPRESA(oData);

            sSQL  = " SELECT * ";
            sSQL += " FROM VMX_FE ";
            sSQL += " WHERE INVOICE_ID LIKE '%" + BUSQUEDA + "%' ";
            DataTable oDataTable = oData.EjecutarConsulta(sSQL);
            foreach (DataRow oDataRow in oDataTable.Rows)
            {
                oEMPRESA.cargar(oDataRow["ROW_ID_EMPRESA"].ToString());
                lista.Add(new cIMPORTAR_CFDI()
                {
                    ROW_ID = oDataRow["ROW_ID"].ToString()
                   ,INVOICE_ID = oDataRow["INVOICE_ID"].ToString()
                   ,UUID = oDataRow["UUID"].ToString()                   
                });
            }
            return lista;
        }

        public List<cIMPORTAR_CFDI> todos_empresa(string BUSQUEDA,string ROW_ID_EMPRESAp, bool complementoPago=false)
        {

            List<cIMPORTAR_CFDI> lista = new List<cIMPORTAR_CFDI>();
            cEMPRESA oEMPRESA = new cEMPRESA(oData);
            if (ROW_ID_EMPRESAp=="")
            {
                return lista;
            }
            sSQL = " SELECT * ";
            sSQL += " FROM VMX_FE ";
            sSQL += " WHERE INVOICE_ID LIKE '%" + BUSQUEDA + "%' ";
            sSQL += " AND ROW_ID_EMPRESA=" + ROW_ID_EMPRESAp  + "";
            if (complementoPago)
            {
                sSQL += " AND TIPO='COMPLEMENTO DE PAGO'";
            }
            DataTable oDataTable = oData.EjecutarConsulta(sSQL);
            foreach (DataRow oDataRow in oDataTable.Rows)
            {
                oEMPRESA.cargar(oDataRow["ROW_ID_EMPRESA"].ToString());
                lista.Add(new cIMPORTAR_CFDI()
                {
                    ROW_ID = oDataRow["ROW_ID"].ToString()
                   ,
                    INVOICE_ID = oDataRow["INVOICE_ID"].ToString()
                   ,
                    UUID = oDataRow["UUID"].ToString()
                });
            }
            return lista;
        }


        public cIMPORTAR_CFDI buscar_tipo(string BUSQUEDA,string TIPO)
        {

            cIMPORTAR_CFDI lista = new cIMPORTAR_CFDI();
            cEMPRESA oEMPRESA = new cEMPRESA(oData);
            sSQL = " SELECT TOP 1 * ";
            sSQL += " FROM VMX_FE ";
            sSQL += " WHERE INVOICE_ID LIKE '%" + BUSQUEDA + "%' ";
            sSQL += " AND TIPO='" + TIPO + "' ";
            DataTable oDataTable = oData.EjecutarConsulta(sSQL);
            foreach (DataRow oDataRow in oDataTable.Rows)
            {
                lista.ROW_ID = oDataRow["ROW_ID"].ToString();
                lista. INVOICE_ID = oDataRow["INVOICE_ID"].ToString();
                lista.UUID = oDataRow["UUID"].ToString();

            }
            return lista;
        }


        public bool cargar(string ROW_IDp)
        {
            sSQL  = " SELECT * ";
            sSQL += " FROM VMX_FE ";
            sSQL += " WHERE ROW_ID=" + ROW_IDp + " ";
            DataTable oDataTable = oData.EjecutarConsulta(sSQL);
            if (oDataTable != null)
            {
                foreach (DataRow oDataRow in oDataTable.Rows)
                {
                    //Añadir el Registro
                    ROW_ID = oDataRow["ROW_ID"].ToString();
                    INVOICE_ID = oDataRow["INVOICE_ID"].ToString();
                    UUID = oDataRow["UUID"].ToString();
                    return true;
                }
            }
            return false;
        }

        public bool guardar()
        {


                sSQL = " INSERT INTO VMX_FE ";
                sSQL += " ( ";
                sSQL += " INVOICE_ID, UUID, ESTADO";
                sSQL += " )";
                sSQL += " VALUES ";
                sSQL += "(";
                sSQL += " '" + INVOICE_ID + "','" + UUID + "','Migrado' ";
                sSQL += ")";
                DataTable oDataTable = oData.EjecutarConsulta(sSQL);
                if (oDataTable!=null)
                {

                    sSQL = "SELECT TOP 1 MAX(ROW_ID) as ROW_ID FROM VMX_FE";
                    oDataTable = oData.EjecutarConsulta(sSQL);
                    if (oDataTable != null)
                    {
                        foreach (DataRow oDataRow in oDataTable.Rows)
                        {
                            ROW_ID = oDataRow["ROW_ID"].ToString();
                        }
                    }
                }

            return true;

        }

        public bool eliminar(string ROW_IDp)
        {
            if (ROW_IDp != "")
            {
                sSQL = "DELETE FROM VMX_FE WHERE ROW_ID=" + ROW_IDp;
                oData.EjecutarConsulta(sSQL);
                return true;
            }
            return false;

        }
    }
}
