﻿using System;
using System.Collections.Generic;

using System.Text;
using System.Data;
using System.Windows;
using System.Windows.Forms;
using Generales;

namespace FE_FX
{
    class cCFDI_USUARIO
    {
        private string sSQL = "";
        public string ROW_ID { get; set; }
        public string ROW_ID_EMPRESA { get; set; }
        public string USUARIO { get; set; }
        public string PASSWORD { get; set; }
        public string TIPO { get; set; }

        private cCONEXCION oData = new cCONEXCION("");

        public cCFDI_USUARIO()
        {
            limpiar();
        }

        public cCFDI_USUARIO(cCONEXCION pData)
        {
            oData = pData;
            limpiar();
        }

        public void limpiar()
        {
            ROW_ID = "";
            USUARIO = "";
            PASSWORD = "";
            TIPO = "";
            ROW_ID_EMPRESA = "";
        }

        public bool cargar(string ROW_ID_EMPRESAp)
        {
            sSQL = " SELECT ROW_ID,[USUARIO], [PASSWORD],TIPO,ROW_ID_EMPRESA ";
            sSQL += " FROM CFDI_USUARIO ";
            if (ROW_ID_EMPRESAp.Trim()!="")
            {
                sSQL += " WHERE ROW_ID_EMPRESA=" + ROW_ID_EMPRESAp + " ";
            }
            DataTable oDataTable = oData.EjecutarConsulta(sSQL);
            if (oDataTable != null)
            {
                foreach (DataRow oDataRow in oDataTable.Rows)
                {
                    //Añadir el Registro
                    ROW_ID = oDataRow["ROW_ID"].ToString();
                    USUARIO = oDataRow["USUARIO"].ToString();
                    PASSWORD = oDataRow["PASSWORD"].ToString();
                    TIPO = oDataRow["TIPO"].ToString();
                    ROW_ID_EMPRESA = oDataRow["ROW_ID_EMPRESA"].ToString();
                    return true;
                }
            }
            return false;
        }

        public bool guardar()
        {
            if (USUARIO.Trim() == "")
            {
                MessageBox.Show("El Usuario es un campo requerido.", Application.ProductName + "-" + Application.ProductVersion, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return false;
            }

            if (PASSWORD.Trim() == "")
            {
                MessageBox.Show("El Password es un campo requerido.", Application.ProductName + "-" + Application.ProductVersion, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return false;
            }

            if(ROW_ID_EMPRESA.Trim()=="")
            {
                MessageBox.Show("La Empresa es un campo requerido.", Application.ProductName + "-" + Application.ProductVersion, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return false;
            }

            if (ROW_ID == "")
            {
                sSQL = " INSERT INTO CFDI_USUARIO ";
                sSQL += " ( ";
                sSQL += " USUARIO, [PASSWORD], TIPO,ROW_ID_EMPRESA";
                sSQL += " )";
                sSQL += " VALUES ";
                sSQL += "(";
                sSQL += " '" + USUARIO + "','" + PASSWORD + "','" + TIPO + "'," + ROW_ID_EMPRESA  + " ";
                sSQL += ")";
                DataTable oDataTable = oData.EjecutarConsulta(sSQL);
                if (oDataTable!=null)
                {

                    sSQL = "SELECT TOP 1 MAX(ROW_ID) as ROW_ID FROM CFDI_USUARIO";
                    oDataTable = oData.EjecutarConsulta(sSQL);
                    if (oDataTable != null)
                    {
                        foreach (DataRow oDataRow in oDataTable.Rows)
                        {
                            ROW_ID = oDataRow["ROW_ID"].ToString();
                        }
                    }
                }

            }
            else
            {
                sSQL = " UPDATE CFDI_USUARIO ";
                sSQL += " SET USUARIO='" + USUARIO + "'";
                sSQL += " ,[PASSWORD]='" + PASSWORD + "',TIPO='" + TIPO + "',ROW_ID_EMPRESA=" + ROW_ID_EMPRESA + " ";
                sSQL += " WHERE ROW_ID=" + ROW_ID + " ";
                oData.EjecutarConsulta(sSQL);

            }
            return true;

        }


        public bool eliminar()
        {
            if (ROW_ID.Trim() != "")
            {
                DialogResult Resultado = MessageBox.Show("¿Esta seguro de eliminar el Usuario?", Application.ProductName + "-" + Application.ProductVersion, MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                if (Resultado.ToString() != "Yes")
                {
                    return false;
                }

                if (ROW_ID != "")
                {
                    sSQL = "DELETE FROM CFDI_USUARIO WHERE ROW_ID=" + ROW_ID;
                    oData.EjecutarConsulta(sSQL);
                }
                return true;
            }
            else
            {
                return false;
            }
        }
    }
}
