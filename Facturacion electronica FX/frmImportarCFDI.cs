﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;

using System.Text;
using System.Windows.Forms;
using Generales;
using ModeloDocumentosFX;

namespace FE_FX
{
    public partial class frmImportarCFDI : Form
    {
        private cCONEXCION oData = new cCONEXCION("");
        private cIMPORTAR_CFDI oObjeto;
        private bool modificado;

        public frmImportarCFDI(string sConn)
        {
            oData.sConn = sConn;
            oObjeto = new cIMPORTAR_CFDI();
            InitializeComponent();
            ajax_loader.Visible = false;
            
        }
        public frmImportarCFDI(string sConn, string ROW_IDp)
        {
            modificado = false;
            oData.sConn = sConn;
            oObjeto = new cIMPORTAR_CFDI();

            InitializeComponent();

            limpiar();
            oObjeto.ROW_ID = ROW_IDp;
            cargar();
        }

        public void limpiar()
        {
            if (modificado)
            {

                DialogResult Resultado = MessageBox.Show("¿Desea guardar los cambios?", Application.ProductName + "-" + Application.ProductVersion, MessageBoxButtons.YesNo);
                if (Resultado.ToString() == "Yes")
                {
                    guardar();
                }


            }
            modificado = false;
            oObjeto = new cIMPORTAR_CFDI();
            INVOICE_ID.Text = "";
            UUID.Text = "";
        }

        private void guardar()
        {
            ajax_loader.Visible = true;
            oObjeto.INVOICE_ID = INVOICE_ID.Text;
            oObjeto.UUID = UUID.Text;

            if (oObjeto.guardar())
            {
                toolStripStatusLabel1.Text = "Datos Actualizados " + DateTime.Now.ToShortDateString() + " " + DateTime.Now.ToLongTimeString();
                modificado = false;
            }
            ajax_loader.Visible = false;
        }

        private void cargar()
        {
            ajax_loader.Visible = true;
            if (oObjeto.cargar(oObjeto.ROW_ID))
            {
                INVOICE_ID.Text = oObjeto.INVOICE_ID;
                UUID.Text = oObjeto.UUID;
                toolStripStatusLabel1.Text  = "Datos Cargados " + DateTime.Now.ToShortDateString() + " " + DateTime.Now.ToLongTimeString();
                modificado = false;

            }
            ajax_loader.Visible = false;
        }

        private void button3_Click(object sender, EventArgs e)
        {
            guardar();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            limpiar();
        }

        private void frmUsuario_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (modificado)
            {
                DialogResult Resultado = MessageBox.Show("¿Desea guardar los cambios?", Application.ProductName + "-" + Application.ProductVersion, MessageBoxButtons.YesNo);
                if (Resultado.ToString() == "Yes")
                {
                    guardar();
                }
            }
        }

        private void frmUsuario_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)27)
            {
                this.Close();
            }
            else
            {
                modificado = true;
                if ((e.KeyChar.ToString() == "<")
                                | (e.KeyChar.ToString() == ">")
                                
                                | (e.KeyChar.ToString() == "'")
                                | (e.KeyChar.ToString() == "#")
                                )
                {
                    e.Handled = true;
                }
            }
        }

        private void button7_Click(object sender, EventArgs e)
        {
            eliminar();
        }

        private void eliminar()
        {
            DialogResult Resultado = MessageBox.Show("¿Esta seguro de eliminar?", Application.ProductName + "-" + Application.ProductVersion, MessageBoxButtons.YesNo);
            if (Resultado.ToString() != "Yes")
            {
                return;
            }
            if (oObjeto.eliminar(oObjeto.ROW_ID))
            {
                limpiar();
            }
        }

    }
}
