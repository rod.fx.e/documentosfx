﻿using System;
using System.Collections.Generic;

using System.Text;
using System.Data;
using System.Windows;
using System.Windows.Forms;
using Generales;
using ModeloDocumentosFX;

namespace FE_FX
{
    class cFORMATO
    {
        private string sSQL = "";
        public string ROW_ID { get; set; }
        public string ID { get; set; }
        public string REPORTE { get; set; }
        public string VISTA { get; set; }
        public string TIPO { get; set; }

        public string ACTIVO { get; set; }

        public string ROW_ID_EMPRESA { get; set; }
        public string EMPRESA { get; set; }

        private cCONEXCION oData = new cCONEXCION("");

        public cFORMATO()
        {
            ModeloDocumentosFXContainer dbContext = new ModeloDocumentosFXContainer();
            try
            {
                string conection = dbContext.Database.Connection.ConnectionString;
                oData = new cCONEXCION(conection);
            }
            catch (Exception err)
            {
                ErrorFX.mostrar(err, false, true, "cCERTIFICADO - 71 - ");
            }
            limpiar();
        }

        public void limpiar()
        {
            ROW_ID = "";
            ID = "";
            REPORTE = "";
            VISTA = "";
            TIPO = "";
            ACTIVO = "";
            ROW_ID_EMPRESA = "";
            EMPRESA = "";

        }

        public List<cFORMATO> todos(string BUSQUEDA)
        {

            List<cFORMATO> lista = new List<cFORMATO>();
            cEMPRESA oEMPRESA = new cEMPRESA(oData);

            sSQL  = " SELECT * ";
            sSQL += " FROM FORMATO ";
            sSQL += " WHERE ID LIKE '%" + BUSQUEDA + "%' ";
            DataTable oDataTable = oData.EjecutarConsulta(sSQL);
            foreach (DataRow oDataRow in oDataTable.Rows)
            {
                oEMPRESA.cargar(oDataRow["ROW_ID_EMPRESA"].ToString());
                lista.Add(new cFORMATO()
                {
                    ROW_ID = oDataRow["ROW_ID"].ToString()
                   ,ID = oDataRow["ID"].ToString()
                   ,REPORTE = oDataRow["REPORTE"].ToString()
                   ,VISTA = oDataRow["VISTA"].ToString()
                   ,
                    TIPO = oDataRow["TIPO"].ToString()
                    ,
                    ACTIVO = oDataRow["ACTIVO"].ToString()
                    ,
                    ROW_ID_EMPRESA = oDataRow["ROW_ID_EMPRESA"].ToString()
                    ,
                    EMPRESA = oEMPRESA.ID
                });
            }
            return lista;
        }

        public List<cFORMATO> todos_empresa(string BUSQUEDA,string ROW_ID_EMPRESAp, bool complementoPago=false)
        {

            List<cFORMATO> lista = new List<cFORMATO>();
            cEMPRESA oEMPRESA = new cEMPRESA(oData);
            if (ROW_ID_EMPRESAp=="")
            {
                return lista;
            }
            sSQL = " SELECT * ";
            sSQL += " FROM FORMATO ";
            sSQL += " WHERE ID LIKE '%" + BUSQUEDA + "%' ";
            sSQL += " AND ROW_ID_EMPRESA=" + ROW_ID_EMPRESAp  + "";
            if (complementoPago)
            {
                sSQL += " AND TIPO='COMPLEMENTO DE PAGO'";
            }
            DataTable oDataTable = oData.EjecutarConsulta(sSQL);
            foreach (DataRow oDataRow in oDataTable.Rows)
            {
                oEMPRESA.cargar(oDataRow["ROW_ID_EMPRESA"].ToString());
                lista.Add(new cFORMATO()
                {
                    ROW_ID = oDataRow["ROW_ID"].ToString()
                   ,
                    ID = oDataRow["ID"].ToString()
                   ,
                    REPORTE = oDataRow["REPORTE"].ToString()
                   ,
                    VISTA = oDataRow["VISTA"].ToString()
                   ,
                    TIPO = oDataRow["TIPO"].ToString()
                    ,
                    ACTIVO = oDataRow["ACTIVO"].ToString()
                    ,
                    ROW_ID_EMPRESA = oDataRow["ROW_ID_EMPRESA"].ToString()
                    ,
                    EMPRESA = oEMPRESA.ID
                });
            }
            return lista;
        }


        public cFORMATO buscar_tipo(string BUSQUEDA,string TIPO)
        {

            cFORMATO lista = new cFORMATO();
            cEMPRESA oEMPRESA = new cEMPRESA(oData);
            sSQL = " SELECT TOP 1 * ";
            sSQL += " FROM FORMATO ";
            sSQL += " WHERE ID LIKE '%" + BUSQUEDA + "%' ";
            sSQL += " AND TIPO='" + TIPO + "' ";
            DataTable oDataTable = oData.EjecutarConsulta(sSQL);
            foreach (DataRow oDataRow in oDataTable.Rows)
            {
                oEMPRESA.cargar(oDataRow["ROW_ID_EMPRESA"].ToString());
                lista.ROW_ID_EMPRESA = oEMPRESA.ROW_ID;
                lista.EMPRESA = oEMPRESA.ID;

                lista.ROW_ID = oDataRow["ROW_ID"].ToString();
                lista. ID = oDataRow["ID"].ToString();
                lista.REPORTE = oDataRow["REPORTE"].ToString();
                lista.VISTA = oDataRow["VISTA"].ToString();
                lista.TIPO = oDataRow["TIPO"].ToString();

            }
            return lista;
        }


        public bool cargar(string ROW_IDp)
        {
            sSQL  = " SELECT * ";
            sSQL += " FROM FORMATO ";
            sSQL += " WHERE ROW_ID=" + ROW_IDp + " ";
            DataTable oDataTable = oData.EjecutarConsulta(sSQL);
            if (oDataTable != null)
            {
                foreach (DataRow oDataRow in oDataTable.Rows)
                {
                    //Añadir el Registro
                    ROW_ID = oDataRow["ROW_ID"].ToString();
                    cEMPRESA oEMPRESA = new cEMPRESA(oData);
                    oEMPRESA.cargar(oDataRow["ROW_ID_EMPRESA"].ToString());
                    ROW_ID_EMPRESA = oEMPRESA.ROW_ID;
                    EMPRESA = oEMPRESA.ID;

                    ID = oDataRow["ID"].ToString();
                    REPORTE = oDataRow["REPORTE"].ToString();
                    VISTA = oDataRow["VISTA"].ToString();
                    TIPO = oDataRow["TIPO"].ToString();
                    ACTIVO = oDataRow["ACTIVO"].ToString();
                    return true;
                }
            }
            return false;
        }

        public bool cargar_id(string IDp)
        {
            sSQL = " SELECT * ";
            sSQL += " FROM FORMATO ";
            sSQL += " WHERE ID='" + IDp + "' ";
            DataTable oDataTable = oData.EjecutarConsulta(sSQL);
            if (oDataTable != null)
            {
                foreach (DataRow oDataRow in oDataTable.Rows)
                {
                    //Añadir el Registro
                    ROW_ID = oDataRow["ROW_ID"].ToString();
                    cEMPRESA oEMPRESA = new cEMPRESA(oData);
                    oEMPRESA.cargar(oDataRow["ROW_ID_EMPRESA"].ToString());
                    ROW_ID_EMPRESA = oEMPRESA.ROW_ID;
                    EMPRESA = oEMPRESA.ID;

                    ID = oDataRow["ID"].ToString();
                    REPORTE = oDataRow["REPORTE"].ToString();
                    VISTA = oDataRow["VISTA"].ToString();
                    TIPO = oDataRow["TIPO"].ToString();
                    ACTIVO = oDataRow["ACTIVO"].ToString();
                    return true;
                }
            }
            return false;
        }



        public bool guardar()
        {
            if (ID.Trim() == "")
            {
                MessageBox.Show("El Nombre es un campo requerido.", Application.ProductName + "-" + Application.ProductVersion, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return false;
            }

            if (REPORTE.Trim() == "")
            {
                MessageBox.Show("El Reporte es un campo requerido.", Application.ProductName + "-" + Application.ProductVersion, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return false;
            }

            if (TIPO.Trim() == "")
            {
                MessageBox.Show("El Tipo es un campo requerido.", Application.ProductName + "-" + Application.ProductVersion, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return false;
            }

            if (ROW_ID == "")
            {

                sSQL = " INSERT INTO FORMATO ";
                sSQL += " ( ";
                sSQL += " ID, REPORTE, VISTA, TIPO";
                sSQL += ", ACTIVO,ROW_ID_EMPRESA";
                sSQL += " )";
                sSQL += " VALUES ";
                sSQL += "(";
                sSQL += " '" + ID + "','" + REPORTE + "','" + VISTA + "','" + TIPO + "' ";
                sSQL += ", '" + ACTIVO + "'," + ROW_ID_EMPRESA  + "";
                sSQL += ")";
                DataTable oDataTable = oData.EjecutarConsulta(sSQL);
                if (oDataTable!=null)
                {

                    sSQL = "SELECT TOP 1 MAX(ROW_ID) as ROW_ID FROM FORMATO";
                    oDataTable = oData.EjecutarConsulta(sSQL);
                    if (oDataTable != null)
                    {
                        foreach (DataRow oDataRow in oDataTable.Rows)
                        {
                            ROW_ID = oDataRow["ROW_ID"].ToString();
                        }
                    }
                }

            }
            else
            {
                sSQL = " UPDATE FORMATO ";
                sSQL += " SET ID='" + ID + "'";
                sSQL += ",REPORTE='" + REPORTE + "',VISTA='" + VISTA + "' ,TIPO='" + TIPO + "' ";
                sSQL += ",ACTIVO='" + ACTIVO + "'";
                sSQL += ",ROW_ID_EMPRESA='" + ROW_ID_EMPRESA + "'";
                sSQL += " WHERE ROW_ID=" + ROW_ID + " ";
                oData.EjecutarConsulta(sSQL);

            }
            return true;

        }

        public bool eliminar(string ROW_IDp)
        {
            if (ROW_IDp != "")
            {
                sSQL = "DELETE FROM FORMATO WHERE ROW_ID=" + ROW_IDp;
                oData.EjecutarConsulta(sSQL);
                return true;
            }
            return false;

        }

        internal string buscarNC()
        {
            sSQL = " SELECT ID ";
            sSQL += " FROM FORMATO ";
            sSQL += " WHERE TIPO='NOTA DE CREDITO' ";
            DataTable oDataTable = oData.EjecutarConsulta(sSQL);
            if (oDataTable != null)
            {
                foreach (DataRow oDataRow in oDataTable.Rows)
                {
                    return oDataRow["ID"].ToString();
                }
            }
            return "";
        }
    }
}
