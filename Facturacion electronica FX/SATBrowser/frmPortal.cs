﻿using DevComponents.DotNetBar;
using DevComponents.DotNetBar.Controls;
using FE_FX.CFDI;
using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Globalization;
using System.IO;
using System.Text;
using System.Text.RegularExpressions;
using System.Windows.Forms;
using System.Xml;
using System.Xml.Serialization;
using Generales;

namespace FE_FX
{
    public partial class frmPortal : Form
    {
        private cCONEXCION oData = new cCONEXCION("");
       
        public frmPortal()
        {
            InitializeComponent();            
        }

        //public string getFolder(WebBrowser _wBrowser)
        //{
        //    string str;
        //    bool flag;
        //    string folderFacturasEmitidas = "";
        //    try
        //    {
        //        CLS_GeneralConfig_BAL cLSGeneralConfigBAL = new CLS_GeneralConfig_BAL();
        //        CLS_GeneralConfig_Info generalConfigInfo = cLSGeneralConfigBAL.GetGeneralConfigInfo();
        //        HtmlElementCollection elementsByTagName = _wBrowser.Document.GetElementsByTagName("h2");
        //        IEnumerator enumerator = elementsByTagName.GetEnumerator();
        //        try
        //        {
        //            while (true)
        //            {
        //                flag = enumerator.MoveNext();
        //                if (!flag)
        //                {
        //                    break;
        //                }
        //                HtmlElement current = (HtmlElement)enumerator.Current;
        //                flag = !current.InnerText.Contains("Consultar");
        //                if (!flag)
        //                {
        //                    folderFacturasEmitidas = current.InnerText.Substring(current.InnerText.IndexOf("Consultar") + 9).Trim();
        //                    break;
        //                }
        //            }
        //        }
        //        finally
        //        {
        //            IDisposable disposable = enumerator as IDisposable;
        //            flag = disposable == null;
        //            if (!flag)
        //            {
        //                disposable.Dispose();
        //            }
        //        }
        //        flag = string.IsNullOrEmpty(folderFacturasEmitidas);
        //        if (!flag)
        //        {
        //            string str1 = folderFacturasEmitidas;
        //            if (str1 != null)
        //            {
        //                if (str1 == "Facturas Emitidas")
        //                {
        //                    folderFacturasEmitidas = generalConfigInfo.FolderFacturasEmitidas;
        //                }
        //                else
        //                {
        //                    if (str1 == "Facturas Recibidas")
        //                    {
        //                        folderFacturasEmitidas = generalConfigInfo.FolderFacturasRecibidas;
        //                    }
        //                    else
        //                    {
        //                        if (str1 == "Retenciones Emitidas")
        //                        {
        //                            folderFacturasEmitidas = generalConfigInfo.FolderRetencionesEmitidas;
        //                        }
        //                        else
        //                        {
        //                            if (str1 != "Retenciones Recibidas")
        //                            {
        //                                folderFacturasEmitidas = "";
        //                                str = folderFacturasEmitidas;
        //                                return str;
        //                            }
        //                            folderFacturasEmitidas = generalConfigInfo.FolderRetencionesRecibidas;
        //                        }
        //                    }
        //                }
        //            }
        //            else
        //            {
        //                folderFacturasEmitidas = "";
        //                str = folderFacturasEmitidas;
        //                return str;
        //            }
        //        }
        //    }
        //    catch (Exception exception)
        //    {
        //    }
        //    str = folderFacturasEmitidas;
        //    return str;
        //}


        ////public Dictionary<string, string[]> getResults(WebBrowser _wBrowser)
        ////{
        ////    Match match;
        ////    bool item;
        ////    IDisposable disposable;
        ////    Dictionary<string, string[]> strs = new Dictionary<string, string[]>();
        ////    try
        ////    {
        ////        HtmlElementCollection children = _wBrowser.Document.GetElementById("DivPaginas").Children;
        ////        string innerText = "";
        ////        IEnumerator enumerator = children.GetEnumerator();
        ////        try
        ////        {
        ////            while (true)
        ////            {
        ////                item = enumerator.MoveNext();
        ////                if (!item)
        ////                {
        ////                    break;
        ////                }
        ////                HtmlElement current = (HtmlElement)enumerator.Current;
        ////                HtmlElementCollection elementsByTagName = current.GetElementsByTagName("tr");
        ////                IEnumerator enumerator1 = elementsByTagName.GetEnumerator();
        ////                try
        ////                {
        ////                    while (true)
        ////                    {
        ////                        item = enumerator1.MoveNext();
        ////                        if (!item)
        ////                        {
        ////                            break;
        ////                        }
        ////                        HtmlElement htmlElement = (HtmlElement)enumerator1.Current;
        ////                        item = !(htmlElement.GetElementsByTagName("img")["BtnDescarga"] != null);
        ////                        if (!item)
        ////                        {
        ////                            int num = htmlElement.GetElementsByTagName("img")["BtnDescarga"].OuterHtml.IndexOf("RecuperaCfdi");
        ////                            int num1 = htmlElement.GetElementsByTagName("img")["BtnDescarga"].OuterHtml.IndexOf("','");
        ////                            string str = string.Concat("https://portalcfdi.facturaelectronica.sat.gob.mx/", htmlElement.GetElementsByTagName("img")["BtnDescarga"].OuterHtml.Substring(num, num1 - num));
        ////                            Regex regex = new Regex("[0-9A-F]{8}-[0-9A-F]{4}-[0-9A-F]{4}-[0-9A-F]{4}-[0-9A-F]{12}");
        ////                            IEnumerator enumerator2 = htmlElement.GetElementsByTagName("span").GetEnumerator();
        ////                            try
        ////                            {
        ////                                while (true)
        ////                                {
        ////                                    item = enumerator2.MoveNext();
        ////                                    if (!item)
        ////                                    {
        ////                                        break;
        ////                                    }
        ////                                    HtmlElement current1 = (HtmlElement)enumerator2.Current;
        ////                                    item = string.IsNullOrEmpty(current1.InnerText);
        ////                                    if (!item)
        ////                                    {
        ////                                        match = regex.Match(current1.InnerText);
        ////                                        item = !match.Success;
        ////                                        if (!item)
        ////                                        {
        ////                                            innerText = current1.InnerText;
        ////                                            break;
        ////                                        }
        ////                                    }
        ////                                }
        ////                            }
        ////                            finally
        ////                            {
        ////                                disposable = enumerator2 as IDisposable;
        ////                                item = disposable == null;
        ////                                if (!item)
        ////                                {
        ////                                    disposable.Dispose();
        ////                                }
        ////                            }
        ////                            regex = new Regex("(Vigente|Cancelado)");
        ////                            enumerator2 = htmlElement.GetElementsByTagName("span").GetEnumerator();
        ////                            try
        ////                            {
        ////                                while (true)
        ////                                {
        ////                                    item = enumerator2.MoveNext();
        ////                                    if (!item)
        ////                                    {
        ////                                        break;
        ////                                    }
        ////                                    HtmlElement htmlElement1 = (HtmlElement)enumerator2.Current;
        ////                                    item = string.IsNullOrEmpty(htmlElement1.InnerText);
        ////                                    if (!item)
        ////                                    {
        ////                                        match = regex.Match(htmlElement1.InnerText);
        ////                                        item = !match.Success;
        ////                                        if (!item)
        ////                                        {
        ////                                            string[] strArrays = new string[2];
        ////                                            strArrays[0] = str;
        ////                                            strArrays[1] = htmlElement1.InnerText;
        ////                                            strs.Add(innerText, strArrays);
        ////                                            break;
        ////                                        }
        ////                                    }
        ////                                }
        ////                            }
        ////                            finally
        ////                            {
        ////                                disposable = enumerator2 as IDisposable;
        ////                                item = disposable == null;
        ////                                if (!item)
        ////                                {
        ////                                    disposable.Dispose();
        ////                                }
        ////                            }
        ////                        }
        ////                    }
        ////                }
        ////                finally
        ////                {
        ////                    disposable = enumerator1 as IDisposable;
        ////                    item = disposable == null;
        ////                    if (!item)
        ////                    {
        ////                        disposable.Dispose();
        ////                    }
        ////                }
        ////            }
        ////        }
        ////        finally
        ////        {
        ////            disposable = enumerator as IDisposable;
        ////            item = disposable == null;
        ////            if (!item)
        ////            {
        ////                disposable.Dispose();
        ////            }
        ////        }
        ////    }
        ////    catch (Exception exception)
        ////    {
        ////    }
        ////    Dictionary<string, string[]> strs1 = strs;
        ////    return strs1;
        ////}


        ////public void getResultsDataTable(WebBrowser m_Browser, DataTable dt)
        ////{
        ////    bool item;
        ////    IDisposable disposable;
        ////    bool flag;
        ////    HtmlElementCollection children = m_Browser.Document.GetElementById("DivPaginas").Children;
        ////    IEnumerator enumerator = children.GetEnumerator();
        ////    try
        ////    {
        ////        while (true)
        ////        {
        ////            item = enumerator.MoveNext();
        ////            if (!item)
        ////            {
        ////                break;
        ////            }
        ////            HtmlElement current = (HtmlElement)enumerator.Current;
        ////            HtmlElementCollection elementsByTagName = current.GetElementsByTagName("tr");
        ////            IEnumerator enumerator1 = elementsByTagName.GetEnumerator();
        ////            try
        ////            {
        ////                while (true)
        ////                {
        ////                    item = enumerator1.MoveNext();
        ////                    if (!item)
        ////                    {
        ////                        break;
        ////                    }
        ////                    HtmlElement htmlElement = (HtmlElement)enumerator1.Current;
        ////                    DataRow innerText = dt.NewRow();
        ////                    try
        ////                    {
        ////                        HtmlElementCollection htmlElementCollections = htmlElement.GetElementsByTagName("td");
        ////                        item = !(htmlElementCollections[0].GetElementsByTagName("img")["BtnDescarga"] != null);
        ////                        if (!item)
        ////                        {
        ////                            int num = htmlElementCollections[0].GetElementsByTagName("img")["BtnDescarga"].OuterHtml.IndexOf("RecuperaCfdi");
        ////                            int num1 = htmlElementCollections[0].GetElementsByTagName("img")["BtnDescarga"].OuterHtml.IndexOf("','");
        ////                            innerText[1] = string.Concat("https://portalcfdi.facturaelectronica.sat.gob.mx/", htmlElementCollections[0].GetElementsByTagName("img")["BtnDescarga"].OuterHtml.Substring(num, num1 - num));
        ////                        }
        ////                        int num2 = 2;
        ////                        while (true)
        ////                        {
        ////                            item = num2 < dt.Columns.Count;
        ////                            if (!item)
        ////                            {
        ////                                break;
        ////                            }
        ////                            if (htmlElementCollections[num2 - 1].InnerText == null)
        ////                            {
        ////                                flag = true;
        ////                            }
        ////                            else
        ////                            {
        ////                                flag = !htmlElementCollections[(num2 - 1)].InnerText.Contains("$");
        ////                            }
        ////                            item = flag;
        ////                            if (item)
        ////                            {
        ////                                innerText[num2] = htmlElementCollections[num2 - 1].InnerText;
        ////                            }
        ////                            else
        ////                            {
        ////                                innerText[num2] = Convert.ToDecimal(htmlElementCollections[num2 - 1].InnerText.Replace("$", "").Replace(",", ""));
        ////                            }
        ////                            num2++;
        ////                        }
        ////                        dt.Rows.Add(innerText);
        ////                    }
        ////                    catch (ConstraintException constraintException)
        ////                    {
        ////                    }
        ////                }
        ////            }
        ////            finally
        ////            {
        ////                disposable = enumerator1 as IDisposable;
        ////                item = disposable == null;
        ////                if (!item)
        ////                {
        ////                    disposable.Dispose();
        ////                }
        ////            }
        ////        }
        ////    }
        ////    finally
        ////    {
        ////        disposable = enumerator as IDisposable;
        ////        item = disposable == null;
        ////        if (!item)
        ////        {
        ////            disposable.Dispose();
        ////        }
        ////    }
        ////}

        ////private void descargar(){
        ////    Exception exception;
        ////    CLS_gMaestraInfo xMLInfo;
        ////    xANC180010 _xANC180010;
        ////    xANC190010 _xANC190010;
        ////    bool count;
        ////    string str;
        ////    string origen;
        ////    bool flag;
        ////    bool count1;
        ////    bool flag1;
        ////    bool count2;
        ////    bool flag2;
        ////    bool count3;
        ////    bool flag3;
        ////    try
        ////    {
        ////        base.SetProgramMessage("Procesando");
        ////        this.SplashScreenManager2.ShowWaitForm();
        ////        this.SplashScreenManager2.SetWaitFormCaption("Procesando.");
        ////        CLS_SATBot cLSSATBot = new CLS_SATBot();
        ////        XMLSATHandler xMLSATHandler = new XMLSATHandler();
        ////        CLS_GeneralConfig_BAL cLSGeneralConfigBAL = new CLS_GeneralConfig_BAL();
        ////        DataTable dataTable = CLS_SATBot.FormatTable();
        ////        cLSSATBot.getResultsDataTable(this.SATwebBrowser, dataTable);
        ////        count = dataTable.Rows.Count <= 0;
        ////        if (count)
        ////        {
        ////            base.SetProgramMessage("");
        ////        }
        ////        else
        ////        {
        ////            this.SplashScreenManager2.SetWaitFormCaption("Descargando...");
        ////            string cookie = XMLSATHandler.getCookie(this.SATwebBrowser.Document.Url.ToString());
        ////            List<int> nums = new List<int>();
        ////            string folder = cLSSATBot.getFolder(this.SATwebBrowser);
        ////            int current = 0;
        ////            while (true)
        ////            {
        ////                count = current < dataTable.Rows.Count;
        ////                if (!count)
        ////                {
        ////                    break;
        ////                }
        ////                count = string.IsNullOrEmpty(dataTable.Rows[current]["XML"].ToString());
        ////                if (count)
        ////                {
        ////                    nums.Add(current);
        ////                }
        ////                else
        ////                {
        ////                    try
        ////                    {
        ////                        xMLSATHandler.downloadXML(dataTable.Rows[current]["UUID"].ToString(), folder, dataTable.Rows[current]["XML"].ToString(), cookie);
        ////                    }
        ////                    catch (TimeoutException timeoutException1)
        ////                    {
        ////                        TimeoutException timeoutException = timeoutException1;
        ////                        goto Label0;
        ////                    }
        ////                    catch (Exception exception1)
        ////                    {
        ////                        exception = exception1;
        ////                        goto Label0;
        ////                    }
        ////                }
        ////                base.SetProgramMessage(string.Format("Descargados: {0}", current + 1));
        ////                this.SplashScreenManager2.SetWaitFormDescription(string.Format(" {0} de {1} comprobantes", current + 1, dataTable.Rows.Count));
        ////                Application.DoEvents();
        ////            Label0:
        ////                current++;
        ////            }
        ////            this.SplashScreenManager2.SetWaitFormCaption("Importando al sistema.");
        ////            this.SplashScreenManager2.SetWaitFormDescription("");
        ////            int num = 1;
        ////            CLS_Bitacora_BAL cLSBitacoraBAL = new CLS_Bitacora_BAL();
        ////            CLS_BitacoraInfo cLSBitacoraInfo = new CLS_BitacoraInfo();
        ////            try
        ////            {
        ////                try
        ////                {
        ////                    CLS_CompaniaBAL cLSCompaniaBAL = new CLS_CompaniaBAL();
        ////                    cLSCompaniaBAL.ConnectionString = cLSBitacoraBAL.ConnectionString;
        ////                    CLS_CompaniaBAL cLSCompaniaBAL1 = cLSCompaniaBAL;
        ////                    count = cLSCompaniaBAL1.GetCompaniaInfo() != null;
        ////                    if (count)
        ////                    {
        ////                        DirectoryInfo directoryInfo = new DirectoryInfo(folder);
        ////                        CLS_IoXML_BAL cLSIoXMLBAL = new CLS_IoXML_BAL();
        ////                        CLS_gMaestraSAT_BAL cLSGMaestraSATBAL = new CLS_gMaestraSAT_BAL();
        ////                        CLS_gComproanteSAT_BAL cLSGComproanteSATBAL = new CLS_gComproanteSAT_BAL();
        ////                        int num1 = 0;
        ////                        this.ClientsDataset = new CLS_BookDataset("eClient", "1 > 2", null, cLSGMaestraSATBAL.ConnectionString);
        ////                        this.SuppliersDataset = new CLS_BookDataset("eClient", "1 > 2", null, cLSGMaestraSATBAL.ConnectionString);
        ////                        this.TrabajadorDataset = new CLS_BookDataset("eTrabajador", "1 > 2", null, cLSGMaestraSATBAL.ConnectionString);
        ////                        this.ProductDataset = new CLS_BookDataset("eProduct", "1 > 2", null, cLSGMaestraSATBAL.ConnectionString);
        ////                        this.SupplierProductDataset = new CLS_BookDataset("eProductSupplier", "1 > 2", null, cLSGMaestraSATBAL.ConnectionString);
        ////                        this.TrabajadorProductDataset = new CLS_BookDataset("eProductTrabajador", "1 > 2", null, cLSGMaestraSATBAL.ConnectionString);
        ////                        FileInfo[] files = directoryInfo.GetFiles("*.xml");
        ////                        int num2 = 0;
        ////                        while (true)
        ////                        {
        ////                            count = num2 < (int)files.Length;
        ////                            if (!count)
        ////                            {
        ////                                break;
        ////                            }
        ////                            FileInfo fileInfo = files[num2];
        ////                            try
        ////                            {
        ////                                try
        ////                                {
        ////                                    this.SplashScreenManager2.SetWaitFormDescription(string.Format(" {0} de {1} comprobantes", num, dataTable.Rows.Count));
        ////                                    Application.DoEvents();
        ////                                    xMLInfo = cLSIoXMLBAL.GetXMLInfo(fileInfo.FullName, out num1);
        ////                                    cLSBitacoraInfo = new CLS_BitacoraInfo();
        ////                                    cLSBitacoraInfo = cLSBitacoraBAL.GetEntityObjects(num1);
        ////                                    Comprobante comprobante = Comprobante.LoadFromFile(fileInfo.FullName);
        ////                                    count = xMLInfo != null;
        ////                                    if (count)
        ////                                    {
        ////                                        CLS_ReciboSAT_BAL cLSReciboSATBAL = new CLS_ReciboSAT_BAL();
        ////                                        count = !(xMLInfo.Origen == "Recibos de Nómina");
        ////                                        if (!count)
        ////                                        {
        ////                                            xMLInfo.ClaveNomina = cLSReciboSATBAL.Save(fileInfo.FullName, comprobante);
        ////                                            xMLInfo.TipoNomina = comprobante.Conceptos[0].descripcion;
        ////                                            cLSReciboSATBAL.FillgMaestraObjectWithRecibo(ref xMLInfo, fileInfo.FullName);
        ////                                        }
        ////                                        count = cLSGMaestraSATBAL.ExistComprobante(xMLInfo.uuid);
        ////                                        if (count)
        ////                                        {
        ////                                            xMLInfo = cLSGMaestraSATBAL.GetEntityObjectsByUUID(xMLInfo.uuid, new CLS_gMaestraSATFieldNames());
        ////                                            count = !string.Equals(dataTable.Select(string.Concat("UUID ='", xMLInfo.uuid.ToUpper(), "'"))[0]["Estado"].ToString().ToUpper(), "CANCELADO");
        ////                                            if (count)
        ////                                            {
        ////                                                xMLInfo.Cancelado = false;
        ////                                            }
        ////                                            else
        ////                                            {
        ////                                                xMLInfo.Cancelado = true;
        ////                                            }
        ////                                            cLSGMaestraSATBAL.Save(xMLInfo, null, null);
        ////                                            cLSBitacoraInfo.Message = string.Format("Ya existe el comprobante {0}-{1}", xMLInfo.Serie, xMLInfo.Folio);
        ////                                        }
        ////                                        else
        ////                                        {
        ////                                            xMLInfo.IsValid = true;
        ////                                            count = !string.Equals(dataTable.Select(string.Concat("UUID ='", xMLInfo.uuid.ToUpper(), "'"))[0]["Estado"].ToString().ToUpper(), "CANCELADO");
        ////                                            if (count)
        ////                                            {
        ////                                                xMLInfo.Cancelado = false;
        ////                                            }
        ////                                            else
        ////                                            {
        ////                                                xMLInfo.Cancelado = true;
        ////                                            }
        ////                                            CLS_AdditionalComponentsBAL cLSAdditionalComponentsBAL = new CLS_AdditionalComponentsBAL();
        ////                                            cLSAdditionalComponentsBAL.FillAdditionalBooks(xMLInfo.Origen, fileInfo.FullName, ref this.ClientsDataset, ref this.SuppliersDataset, ref this.TrabajadorDataset, ref this.ProductDataset, ref this.SupplierProductDataset, ref this.TrabajadorProductDataset);
        ////                                            int num3 = cLSGMaestraSATBAL.Save(xMLInfo, null, null);
        ////                                            count = num3 <= 0;
        ////                                            if (!count)
        ////                                            {
        ////                                                CLS_gComprobanteInfo cLSGComprobanteInfo = CLS_IoXMLBAL.CreategComprobanteObject(fileInfo.FullName, num3);
        ////                                                count = cLSGComprobanteInfo != null;
        ////                                                if (count)
        ////                                                {
        ////                                                    cLSGComproanteSATBAL.Save(cLSGComprobanteInfo);
        ////                                                    cLSBitacoraInfo.IdgMaestra = num3;
        ////                                                    cLSBitacoraInfo.Message = "Importado ok";
        ////                                                }
        ////                                                else
        ////                                                {
        ////                                                    throw new Exception("Se ha producido un error por lo que no he podido guardar la informacion del comprobante.");
        ////                                                }
        ////                                            }
        ////                                        }
        ////                                        string xML = xMLInfo.XML;
        ////                                        cLSBitacoraInfo.FileName = fileInfo.Name;
        ////                                        cLSBitacoraInfo.FechaImportacion = DateTime.Now;
        ////                                        CLS_BitacoraInfo cLSBitacoraInfo1 = cLSBitacoraInfo;
        ////                                        if (string.IsNullOrEmpty(xML))
        ////                                        {
        ////                                            str = "";
        ////                                        }
        ////                                        else
        ////                                        {
        ////                                            str = xML;
        ////                                        }
        ////                                        cLSBitacoraInfo1.XML = str;
        ////                                        cLSBitacoraInfo.FullFileName = fileInfo.FullName;
        ////                                        CLS_BitacoraInfo cLSBitacoraInfo2 = cLSBitacoraInfo;
        ////                                        if (string.IsNullOrEmpty(xMLInfo.Origen))
        ////                                        {
        ////                                            origen = "Desconocido";
        ////                                        }
        ////                                        else
        ////                                        {
        ////                                            origen = xMLInfo.Origen;
        ////                                        }
        ////                                        cLSBitacoraInfo2.Origen = origen;
        ////                                        cLSBitacoraInfo.FechaDeEvento = DateTime.Now;
        ////                                        cLSBitacoraInfo.Usuario = GLOBAL.gUsuario.Login;
        ////                                        cLSBitacoraInfo.Evento = "Importación";
        ////                                        File.Delete(fileInfo.FullName);
        ////                                        num++;
        ////                                        count = cLSBitacoraInfo == null;
        ////                                        if (!count)
        ////                                        {
        ////                                            cLSBitacoraBAL.Save(cLSBitacoraInfo);
        ////                                        }
        ////                                    }
        ////                                    else
        ////                                    {
        ////                                        throw new Exception("No he podido crear el objeto gMaestraInfo");
        ////                                    }
        ////                                }
        ////                                catch (Exception exception2)
        ////                                {
        ////                                    exception = exception2;
        ////                                    cLSBitacoraInfo.Message = exception.Message;
        ////                                    count = cLSBitacoraInfo == null;
        ////                                    if (!count)
        ////                                    {
        ////                                        cLSBitacoraBAL.Save(cLSBitacoraInfo);
        ////                                    }
        ////                                }
        ////                            }
        ////                            finally
        ////                            {
        ////                            }
        ////                            num2++;
        ////                        }
        ////                        if (this.ClientsDataset.HeaderTable == null)
        ////                        {
        ////                            flag = true;
        ////                        }
        ////                        else
        ////                        {
        ////                            flag = this.ClientsDataset.HeaderTable.Rows.Count <= 0;
        ////                        }
        ////                        count = flag;
        ////                        if (!count)
        ////                        {
        ////                            count = !this.SplashScreenManager2.IsSplashFormVisible;
        ////                            if (!count)
        ////                            {
        ////                                this.SplashScreenManager2.CloseWaitForm();
        ////                            }
        ////                            count = MessageBox.Show(string.Format("Se han encontrado {0} Clientes. Desea guardarlos?", this.ClientsDataset.HeaderTable.Rows.Count), "Atención", MessageBoxButtons.YesNo, MessageBoxIcon.Asterisk) != DialogResult.Yes;
        ////                            if (!count)
        ////                            {
        ////                                _xANC180010 = new xANC180010();
        ////                                try
        ////                                {
        ////                                    _xANC180010.setDataSource(this.ClientsDataset.HeaderTable);
        ////                                    _xANC180010.Text = "Clientes";
        ////                                    _xANC180010.ShowDialog();
        ////                                }
        ////                                finally
        ////                                {
        ////                                    count = _xANC180010 == null;
        ////                                    if (!count)
        ////                                    {
        ////                                        _xANC180010.Dispose();
        ////                                    }
        ////                                }
        ////                            }
        ////                        }
        ////                        if (this.SuppliersDataset.HeaderTable == null)
        ////                        {
        ////                            count1 = true;
        ////                        }
        ////                        else
        ////                        {
        ////                            count1 = this.SuppliersDataset.HeaderTable.Rows.Count <= 0;
        ////                        }
        ////                        count = count1;
        ////                        if (!count)
        ////                        {
        ////                            count = !this.SplashScreenManager2.IsSplashFormVisible;
        ////                            if (!count)
        ////                            {
        ////                                this.SplashScreenManager2.CloseWaitForm();
        ////                            }
        ////                            count = MessageBox.Show(string.Format("Se han encontrado {0} Proveedores. Desea guardarlos?", this.SuppliersDataset.HeaderTable.Rows.Count), "Atención", MessageBoxButtons.YesNo, MessageBoxIcon.Asterisk) != DialogResult.Yes;
        ////                            if (!count)
        ////                            {
        ////                                _xANC180010 = new xANC180010();
        ////                                try
        ////                                {
        ////                                    _xANC180010.setDataSource(this.SuppliersDataset.HeaderTable);
        ////                                    _xANC180010.Text = "Proveedores";
        ////                                    _xANC180010.ShowDialog();
        ////                                }
        ////                                finally
        ////                                {
        ////                                    count = _xANC180010 == null;
        ////                                    if (!count)
        ////                                    {
        ////                                        _xANC180010.Dispose();
        ////                                    }
        ////                                }
        ////                            }
        ////                        }
        ////                        if (this.TrabajadorDataset.HeaderTable == null)
        ////                        {
        ////                            flag1 = true;
        ////                        }
        ////                        else
        ////                        {
        ////                            flag1 = this.TrabajadorDataset.HeaderTable.Rows.Count <= 0;
        ////                        }
        ////                        count = flag1;
        ////                        if (!count)
        ////                        {
        ////                            count = !this.SplashScreenManager2.IsSplashFormVisible;
        ////                            if (!count)
        ////                            {
        ////                                this.SplashScreenManager2.CloseWaitForm();
        ////                            }
        ////                            count = MessageBox.Show(string.Format("Se han encontrado {0} Trabajadores. Desea guardarlos?", this.TrabajadorDataset.HeaderTable.Rows.Count), "Atención", MessageBoxButtons.YesNo, MessageBoxIcon.Asterisk) != DialogResult.Yes;
        ////                            if (!count)
        ////                            {
        ////                                _xANC180010 = new xANC180010();
        ////                                try
        ////                                {
        ////                                    _xANC180010.setDataSource(this.TrabajadorDataset.HeaderTable);
        ////                                    _xANC180010.Text = "Trabajadores";
        ////                                    _xANC180010.ShowDialog();
        ////                                }
        ////                                finally
        ////                                {
        ////                                    count = _xANC180010 == null;
        ////                                    if (!count)
        ////                                    {
        ////                                        _xANC180010.Dispose();
        ////                                    }
        ////                                }
        ////                            }
        ////                        }
        ////                        if (this.ProductDataset.HeaderTable == null)
        ////                        {
        ////                            count2 = true;
        ////                        }
        ////                        else
        ////                        {
        ////                            count2 = this.ProductDataset.HeaderTable.Rows.Count <= 0;
        ////                        }
        ////                        count = count2;
        ////                        if (!count)
        ////                        {
        ////                            count = !this.SplashScreenManager2.IsSplashFormVisible;
        ////                            if (!count)
        ////                            {
        ////                                this.SplashScreenManager2.CloseWaitForm();
        ////                            }
        ////                            count = MessageBox.Show(string.Format("Se han encontrado {0} Productos. Desea guardarlos?", this.ProductDataset.HeaderTable.Rows.Count), "Atención", MessageBoxButtons.YesNo, MessageBoxIcon.Asterisk) != DialogResult.Yes;
        ////                            if (!count)
        ////                            {
        ////                                _xANC190010 = new xANC190010();
        ////                                try
        ////                                {
        ////                                    _xANC190010.setDataSource(this.ProductDataset.HeaderTable);
        ////                                    _xANC190010.Text = "Productos";
        ////                                    _xANC190010.ShowDialog();
        ////                                }
        ////                                finally
        ////                                {
        ////                                    count = _xANC190010 == null;
        ////                                    if (!count)
        ////                                    {
        ////                                        _xANC190010.Dispose();
        ////                                    }
        ////                                }
        ////                            }
        ////                        }
        ////                        if (this.SupplierProductDataset.HeaderTable == null)
        ////                        {
        ////                            flag2 = true;
        ////                        }
        ////                        else
        ////                        {
        ////                            flag2 = this.SupplierProductDataset.HeaderTable.Rows.Count <= 0;
        ////                        }
        ////                        count = flag2;
        ////                        if (!count)
        ////                        {
        ////                            count = !this.SplashScreenManager2.IsSplashFormVisible;
        ////                            if (!count)
        ////                            {
        ////                                this.SplashScreenManager2.CloseWaitForm();
        ////                            }
        ////                            count = MessageBox.Show(string.Format("Se han encontrado {0} Productos de proveedor. Desea guardarlos?", this.SupplierProductDataset.HeaderTable.Rows.Count), "Atención", MessageBoxButtons.YesNo, MessageBoxIcon.Asterisk) != DialogResult.Yes;
        ////                            if (!count)
        ////                            {
        ////                                _xANC190010 = new xANC190010();
        ////                                try
        ////                                {
        ////                                    _xANC190010.setDataSource(this.SupplierProductDataset.HeaderTable);
        ////                                    _xANC190010.Text = "Productos Proveedores";
        ////                                    _xANC190010.ShowDialog();
        ////                                }
        ////                                finally
        ////                                {
        ////                                    count = _xANC190010 == null;
        ////                                    if (!count)
        ////                                    {
        ////                                        _xANC190010.Dispose();
        ////                                    }
        ////                                }
        ////                            }
        ////                        }
        ////                        if (this.TrabajadorProductDataset.HeaderTable == null)
        ////                        {
        ////                            count3 = true;
        ////                        }
        ////                        else
        ////                        {
        ////                            count3 = this.TrabajadorProductDataset.HeaderTable.Rows.Count <= 0;
        ////                        }
        ////                        count = count3;
        ////                        if (!count)
        ////                        {
        ////                            count = !this.SplashScreenManager2.IsSplashFormVisible;
        ////                            if (!count)
        ////                            {
        ////                                this.SplashScreenManager2.CloseWaitForm();
        ////                            }
        ////                            count = MessageBox.Show(string.Format("Se han encontrado {0} Productos de trabajadores. Desea guardarlos?", this.TrabajadorProductDataset.HeaderTable.Rows.Count), "Atención", MessageBoxButtons.YesNo, MessageBoxIcon.Asterisk) != DialogResult.Yes;
        ////                            if (!count)
        ////                            {
        ////                                _xANC190010 = new xANC190010();
        ////                                try
        ////                                {
        ////                                    _xANC190010.setDataSource(this.TrabajadorProductDataset.HeaderTable);
        ////                                    _xANC190010.Text = "Productos Trabajador";
        ////                                    _xANC190010.ShowDialog();
        ////                                }
        ////                                finally
        ////                                {
        ////                                    count = _xANC190010 == null;
        ////                                    if (!count)
        ////                                    {
        ////                                        _xANC190010.Dispose();
        ////                                    }
        ////                                }
        ////                            }
        ////                        }
        ////                        count = nums.Count <= 0;
        ////                        if (!count)
        ////                        {
        ////                            try
        ////                            {
        ////                                count = this.SplashScreenManager2.IsSplashFormVisible;
        ////                                if (!count)
        ////                                {
        ////                                    this.SplashScreenManager2.ShowWaitForm();
        ////                                }
        ////                                this.SplashScreenManager2.SetWaitFormCaption("Verificando Cancelados.");
        ////                                this.SplashScreenManager2.SetWaitFormDescription("");
        ////                                List<int>.Enumerator enumerator = nums.GetEnumerator();
        ////                                try
        ////                                {
        ////                                    while (true)
        ////                                    {
        ////                                        count = enumerator.MoveNext();
        ////                                        if (!count)
        ////                                        {
        ////                                            break;
        ////                                        }
        ////                                        current = enumerator.Current;
        ////                                        count = !cLSGMaestraSATBAL.ExistComprobante(dataTable.Rows[current]["UUID"].ToString());
        ////                                        if (count)
        ////                                        {
        ////                                            xMLInfo = new CLS_gMaestraInfo();
        ////                                            xMLInfo.uuid = dataTable.Rows[current]["UUID"].ToString();
        ////                                            xMLInfo.RFCEmisor = dataTable.Rows[current]["RFC Emisor"].ToString();
        ////                                            xMLInfo.RFCReceptor = dataTable.Rows[current]["RFC Receptor"].ToString();
        ////                                            xMLInfo.RazonSocialEmisor = dataTable.Rows[current]["UUIRazón Social EmisorD"].ToString();
        ////                                            xMLInfo.RazonSocialReceptor = dataTable.Rows[current]["Razón Social Receptor"].ToString();
        ////                                            xMLInfo.FechaFacturacion = Convert.ToDateTime(dataTable.Rows[current]["Fecha de Emisión"]);
        ////                                            xMLInfo.FechaTimbrado = Convert.ToDateTime(dataTable.Rows[current]["Fecha de Timbrado"]);
        ////                                            xMLInfo.Total = Convert.ToDouble(dataTable.Rows[current]["Total"].ToString().Replace("$", "").Replace(",", "."));
        ////                                            xMLInfo.TipoComprobante = dataTable.Rows[current]["Tipo Comprobante"].ToString().ToUpper();
        ////                                            CLS_gMaestraInfo cLSGMaestraInfo = xMLInfo;
        ////                                            if (string.Equals(dataTable.Rows[current]["Estado"].ToString().ToUpper(), "CANCELADO"))
        ////                                            {
        ////                                                flag3 = true;
        ////                                            }
        ////                                            else
        ////                                            {
        ////                                                flag3 = false;
        ////                                            }
        ////                                            cLSGMaestraInfo.Cancelado = flag3;
        ////                                            cLSGMaestraSATBAL.Save(xMLInfo, null, null);
        ////                                        }
        ////                                        else
        ////                                        {
        ////                                            xMLInfo = cLSGMaestraSATBAL.GetEntityObjectsByUUID(dataTable.Rows[current]["UUID"].ToString(), new CLS_gMaestraSATFieldNames());
        ////                                            xMLInfo.Cancelado = true;
        ////                                            cLSGMaestraSATBAL.Save(xMLInfo, null, null);
        ////                                        }
        ////                                    }
        ////                                }
        ////                                finally
        ////                                {
        ////                                    enumerator.Dispose();
        ////                                }
        ////                            }
        ////                            catch (Exception exception3)
        ////                            {
        ////                            }
        ////                        }
        ////                    }
        ////                    else
        ////                    {
        ////                        throw new Exception("Primero debe establecer la configuración de la empresa.");
        ////                    }
        ////                }
        ////                catch (Exception exception4)
        ////                {
        ////                    exception = exception4;
        ////                    MessageBox.Show(string.Concat(exception.Message, Environment.NewLine, exception.StackTrace), "Informacion", MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
        ////                }
        ////            }
        ////            finally
        ////            {
        ////                count = !this.SplashScreenManager2.IsSplashFormVisible;
        ////                if (!count)
        ////                {
        ////                    this.SplashScreenManager2.CloseWaitForm();
        ////                }
        ////            }
        ////        }
        ////        MessageBox.Show("Proceso Finalizado.", "Información", MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
        ////    }
        ////    catch (Exception exception5)
        ////    {
        ////        exception = exception5;
        ////        count = !this.SplashScreenManager2.IsSplashFormVisible;
        ////        if (count)
        ////        {
        ////            MessageBox.Show(exception.Message, "Información", MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
        ////        }
        ////        else
        ////        {
        ////            this.SplashScreenManager2.CloseWaitForm();
        ////        }
        ////    }
        ////}
    }
}
