﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Windows.Forms;

namespace FE_FX.SATBrowser
{
    class CLS_SATBot
    {
        public CLS_SATBot()
        {
        }

        //    public static DataTable FormatTable()
        //    {
        //        DataTable dataTable = new DataTable();
        //        string[] strArrays = new string[13];
        //        strArrays[0] = "Status";
        //        strArrays[1] = "XML";
        //        strArrays[2] = "UUID";
        //        strArrays[3] = "RFC Emisor";
        //        strArrays[4] = "Razón Social Emisor";
        //        strArrays[5] = "RFC Receptor";
        //        strArrays[6] = "Razón Social Receptor";
        //        strArrays[7] = "Fecha de Emisión";
        //        strArrays[8] = "Fecha de Timbrado";
        //        strArrays[9] = "PAC";
        //        strArrays[10] = "Total";
        //        strArrays[11] = "Tipo Comprobante";
        //        strArrays[12] = "Estado";
        //        string[] strArrays1 = strArrays;
        //        string[] strArrays2 = strArrays1;
        //        int num = 0;
        //        while (true)
        //        {
        //            bool length = num < (int)strArrays2.Length;
        //            if (!length)
        //            {
        //                break;
        //            }
        //            string str = strArrays2[num];
        //            length = string.Compare(str, "Total", true) != 0;
        //            if (length)
        //            {
        //                dataTable.Columns.Add(str);
        //            }
        //            else
        //            {
        //                dataTable.Columns.Add(str, typeof(decimal));
        //            }
        //            num++;
        //        }
        //        DataColumn[] item = new DataColumn[1];
        //        item[0] = dataTable.Columns["UUID"];
        //        UniqueConstraint uniqueConstraint = new UniqueConstraint(item);
        //        dataTable.Constraints.Add(uniqueConstraint);
        //        DataTable dataTable1 = dataTable;
        //        return dataTable1;
        //    }

        //    public string getFolder(WebBrowser _wBrowser)
        //    {
        //        string str;
        //        bool flag;
        //        string folderFacturasEmitidas = "";
        //        try
        //        {
        //            HtmlElementCollection elementsByTagName = _wBrowser.Document.GetElementsByTagName("h2");
        //            IEnumerator enumerator = elementsByTagName.GetEnumerator();
        //            try
        //            {
        //                while (true)
        //                {
        //                    flag = enumerator.MoveNext();
        //                    if (!flag)
        //                    {
        //                        break;
        //                    }
        //                    HtmlElement current = (HtmlElement)enumerator.Current;
        //                    flag = !current.InnerText.Contains("Consultar");
        //                    if (!flag)
        //                    {
        //                        folderFacturasEmitidas = current.InnerText.Substring(current.InnerText.IndexOf("Consultar") + 9).Trim();
        //                        break;
        //                    }
        //                }
        //            }
        //            finally
        //            {
        //                IDisposable disposable = enumerator as IDisposable;
        //                flag = disposable == null;
        //                if (!flag)
        //                {
        //                    disposable.Dispose();
        //                }
        //            }
        //            flag = string.IsNullOrEmpty(folderFacturasEmitidas);
        //            if (!flag)
        //            {
        //                string str1 = folderFacturasEmitidas;
        //                if (str1 != null)
        //                {
        //                    if (str1 == "Facturas Emitidas")
        //                    {
        //                        folderFacturasEmitidas = "FacturasEmitidas";
        //                    }
        //                    else
        //                    {
        //                        if (str1 == "Facturas Recibidas")
        //                        {
        //                            folderFacturasEmitidas = "FacturasRecibidas";
        //                        }
        //                        else
        //                        {
        //                            if (str1 == "Retenciones Emitidas")
        //                            {
        //                                folderFacturasEmitidas = "RetencionesEmitidas";
        //                            }
        //                            else
        //                            {
        //                                if (str1 != "Retenciones Recibidas")
        //                                {
        //                                    folderFacturasEmitidas = "";
        //                                    str = folderFacturasEmitidas;
        //                                    return str;
        //                                }
        //                                folderFacturasEmitidas = "RetencionesRecibidas";
        //                            }
        //                        }
        //                    }
        //                }
        //                else
        //                {
        //                    folderFacturasEmitidas = "";
        //                    str = folderFacturasEmitidas;
        //                    return str;
        //                }
        //            }
        //        }
        //        catch (Exception exception)
        //        {
        //        }
        //        str = folderFacturasEmitidas;
        //        return str;
        //    }

        //    public static CLS_gMaestraInfo GetgMaestraInfoFromData(CLS_gMaestraInfo gMaestraInfo, DataRow Row
        //        , CLS_GeneralConfig_Info gCOnfigInfo, CLS_CompaniaInfo CompanyInfo)
        //    {
        //        DateTime dateTime;
        //        bool flag;
        //        string str;
        //        bool flag1 = gMaestraInfo != null;
        //        if (!flag1)
        //        {
        //            gMaestraInfo = new CLS_gMaestraInfo();
        //        }
        //        CLS_AgrupacionesBAL cLSAgrupacionesBAL = new CLS_AgrupacionesBAL();
        //        gMaestraInfo.uuid = Row["UUID"].ToString();
        //        gMaestraInfo.RFCEmisor = Row["RFC Emisor"].ToString();
        //        gMaestraInfo.RFCReceptor = Row["RFC Receptor"].ToString();
        //        gMaestraInfo.RazonSocialEmisor = Row["Razón Social Emisor"].ToString();
        //        gMaestraInfo.RazonSocialReceptor = Row["Razón Social Receptor"].ToString();
        //        gMaestraInfo.FechaFacturacion = Convert.ToDateTime(Row["Fecha de Emisión"]);
        //        gMaestraInfo.FechaTimbrado = Convert.ToDateTime(Convert.ToDateTime(Row["Fecha de Timbrado"]));
        //        if (string.Equals(gCOnfigInfo.TipoFecha, "Fecha de Timbrado"))
        //        {
        //            dateTime = Convert.ToDateTime(gMaestraInfo.FechaTimbrado);
        //        }
        //        else
        //        {
        //            if (string.IsNullOrEmpty(gCOnfigInfo.TipoFecha))
        //            {
        //                dateTime = Convert.ToDateTime(gMaestraInfo.FechaTimbrado);
        //            }
        //            else
        //            {
        //                dateTime = gMaestraInfo.FechaFacturacion;
        //            }
        //        }
        //        DateTime dateTime1 = dateTime;
        //        gMaestraInfo.Subtotal = Convert.ToDouble(Row["Total"].ToString().Replace("$", "").Replace(",", ""));
        //        gMaestraInfo.Iva = 0;
        //        gMaestraInfo.Total = Convert.ToDouble(Row["Total"].ToString().Replace("$", "").Replace(",", ""));
        //        gMaestraInfo.Moneda = "MXN";
        //        gMaestraInfo.TipoDeCambio = 1;
        //        gMaestraInfo.TipoComprobante = Row["Tipo Comprobante"].ToString().ToLower();
        //        CLS_gMaestraInfo cLSGMaestraInfo = gMaestraInfo;
        //        if (string.Equals(Row["Estado"].ToString().ToUpper(), "CANCELADO"))
        //        {
        //            flag = true;
        //        }
        //        else
        //        {
        //            flag = false;
        //        }
        //        cLSGMaestraInfo.Cancelado = flag;
        //        gMaestraInfo.XML = " ";
        //        gMaestraInfo.Mes = CLS_SATBot.getMonthName(dateTime1.Month);
        //        gMaestraInfo.Año = dateTime1.Year;
        //        CLS_gMaestraInfo cLSGMaestraInfo1 = gMaestraInfo;
        //        if (string.Compare(gMaestraInfo.RFCEmisor, CompanyInfo.Rfc, true) == 0)
        //        {
        //            str = "Clientes";
        //        }
        //        else
        //        {
        //            str = "Proveedores";
        //        }
        //        cLSGMaestraInfo1.Origen = str;
        //        gMaestraInfo.AgrupacionId = cLSAgrupacionesBAL.GetAgrupacionSinClasificar(gMaestraInfo.Origen, gMaestraInfo.TipoComprobante).Id;
        //        CLS_gMaestraInfo cLSGMaestraInfo2 = gMaestraInfo;
        //        return cLSGMaestraInfo2;
        //    }

        //    private static string getMonthName(int month)
        //    {
        //        string str;
        //        int num = month;
        //        switch (num)
        //        {
        //            case 1:
        //                {
        //                    str = "Enero";
        //                    break;
        //                }
        //            case 2:
        //                {
        //                    str = "Febrero";
        //                    break;
        //                }
        //            case 3:
        //                {
        //                    str = "Marzo";
        //                    break;
        //                }
        //            case 4:
        //                {
        //                    str = "Abril";
        //                    break;
        //                }
        //            case 5:
        //                {
        //                    str = "Mayo";
        //                    break;
        //                }
        //            case 6:
        //                {
        //                    str = "Junio";
        //                    break;
        //                }
        //            case 7:
        //                {
        //                    str = "Julio";
        //                    break;
        //                }
        //            case 8:
        //                {
        //                    str = "Agosto";
        //                    break;
        //                }
        //            case 9:
        //                {
        //                    str = "Septiembre";
        //                    break;
        //                }
        //            case 10:
        //                {
        //                    str = "Octubre";
        //                    break;
        //                }
        //            case 11:
        //                {
        //                    str = "Noviembre";
        //                    break;
        //                }
        //            case 12:
        //                {
        //                    str = "Diciembre";
        //                    break;
        //                }
        //            default:
        //                {
        //                    str = "";
        //                    break;
        //                }
        //        }
        //        return str;
        //    }

        //    public Dictionary<string, string[]> getResults(WebBrowser _wBrowser)
        //    {
        //        Match match;
        //        bool item;
        //        IDisposable disposable;
        //        Dictionary<string, string[]> strs = new Dictionary<string, string[]>();
        //        try
        //        {
        //            HtmlElementCollection children = _wBrowser.Document.GetElementById("DivPaginas").Children;
        //            string innerText = "";
        //            IEnumerator enumerator = children.GetEnumerator();
        //            try
        //            {
        //                while (true)
        //                {
        //                    item = enumerator.MoveNext();
        //                    if (!item)
        //                    {
        //                        break;
        //                    }
        //                    HtmlElement current = (HtmlElement)enumerator.Current;
        //                    HtmlElementCollection elementsByTagName = current.GetElementsByTagName("tr");
        //                    IEnumerator enumerator1 = elementsByTagName.GetEnumerator();
        //                    try
        //                    {
        //                        while (true)
        //                        {
        //                            item = enumerator1.MoveNext();
        //                            if (!item)
        //                            {
        //                                break;
        //                            }
        //                            HtmlElement htmlElement = (HtmlElement)enumerator1.Current;
        //                            item = !(htmlElement.GetElementsByTagName("img")["BtnDescarga"] != null);
        //                            if (!item)
        //                            {
        //                                int num = htmlElement.GetElementsByTagName("img")["BtnDescarga"].OuterHtml.IndexOf("RecuperaCfdi");
        //                                int num1 = htmlElement.GetElementsByTagName("img")["BtnDescarga"].OuterHtml.IndexOf("','");
        //                                string str = string.Concat("https://portalcfdi.facturaelectronica.sat.gob.mx/", htmlElement.GetElementsByTagName("img")["BtnDescarga"].OuterHtml.Substring(num, num1 - num));
        //                                Regex regex = new Regex("[0-9A-F]{8}-[0-9A-F]{4}-[0-9A-F]{4}-[0-9A-F]{4}-[0-9A-F]{12}");
        //                                IEnumerator enumerator2 = htmlElement.GetElementsByTagName("span").GetEnumerator();
        //                                try
        //                                {
        //                                    while (true)
        //                                    {
        //                                        item = enumerator2.MoveNext();
        //                                        if (!item)
        //                                        {
        //                                            break;
        //                                        }
        //                                        HtmlElement current1 = (HtmlElement)enumerator2.Current;
        //                                        item = string.IsNullOrEmpty(current1.InnerText);
        //                                        if (!item)
        //                                        {
        //                                            match = regex.Match(current1.InnerText);
        //                                            item = !match.Success;
        //                                            if (!item)
        //                                            {
        //                                                innerText = current1.InnerText;
        //                                                break;
        //                                            }
        //                                        }
        //                                    }
        //                                }
        //                                finally
        //                                {
        //                                    disposable = enumerator2 as IDisposable;
        //                                    item = disposable == null;
        //                                    if (!item)
        //                                    {
        //                                        disposable.Dispose();
        //                                    }
        //                                }
        //                                regex = new Regex("(Vigente|Cancelado)");
        //                                enumerator2 = htmlElement.GetElementsByTagName("span").GetEnumerator();
        //                                try
        //                                {
        //                                    while (true)
        //                                    {
        //                                        item = enumerator2.MoveNext();
        //                                        if (!item)
        //                                        {
        //                                            break;
        //                                        }
        //                                        HtmlElement htmlElement1 = (HtmlElement)enumerator2.Current;
        //                                        item = string.IsNullOrEmpty(htmlElement1.InnerText);
        //                                        if (!item)
        //                                        {
        //                                            match = regex.Match(htmlElement1.InnerText);
        //                                            item = !match.Success;
        //                                            if (!item)
        //                                            {
        //                                                string[] strArrays = new string[2];
        //                                                strArrays[0] = str;
        //                                                strArrays[1] = htmlElement1.InnerText;
        //                                                strs.Add(innerText, strArrays);
        //                                                break;
        //                                            }
        //                                        }
        //                                    }
        //                                }
        //                                finally
        //                                {
        //                                    disposable = enumerator2 as IDisposable;
        //                                    item = disposable == null;
        //                                    if (!item)
        //                                    {
        //                                        disposable.Dispose();
        //                                    }
        //                                }
        //                            }
        //                        }
        //                    }
        //                    finally
        //                    {
        //                        disposable = enumerator1 as IDisposable;
        //                        item = disposable == null;
        //                        if (!item)
        //                        {
        //                            disposable.Dispose();
        //                        }
        //                    }
        //                }
        //            }
        //            finally
        //            {
        //                disposable = enumerator as IDisposable;
        //                item = disposable == null;
        //                if (!item)
        //                {
        //                    disposable.Dispose();
        //                }
        //            }
        //        }
        //        catch (Exception exception)
        //        {
        //        }
        //        Dictionary<string, string[]> strs1 = strs;
        //        return strs1;
        //    }

        //    public void getResultsDataTable(WebBrowser m_Browser, DataTable dt)
        //    {
        //        bool item;
        //        IDisposable disposable;
        //        bool flag;
        //        HtmlElementCollection children = m_Browser.Document.GetElementById("DivPaginas").Children;
        //        IEnumerator enumerator = children.GetEnumerator();
        //        try
        //        {
        //            while (true)
        //            {
        //                item = enumerator.MoveNext();
        //                if (!item)
        //                {
        //                    break;
        //                }
        //                HtmlElement current = (HtmlElement)enumerator.Current;
        //                HtmlElementCollection elementsByTagName = current.GetElementsByTagName("tr");
        //                IEnumerator enumerator1 = elementsByTagName.GetEnumerator();
        //                try
        //                {
        //                    while (true)
        //                    {
        //                        item = enumerator1.MoveNext();
        //                        if (!item)
        //                        {
        //                            break;
        //                        }
        //                        HtmlElement htmlElement = (HtmlElement)enumerator1.Current;
        //                        DataRow innerText = dt.NewRow();
        //                        try
        //                        {
        //                            HtmlElementCollection htmlElementCollections = htmlElement.GetElementsByTagName("td");
        //                            item = !(htmlElementCollections[0].GetElementsByTagName("img")["BtnDescarga"] != null);
        //                            if (!item)
        //                            {
        //                                int num = htmlElementCollections[0].GetElementsByTagName("img")["BtnDescarga"].OuterHtml.IndexOf("RecuperaCfdi");
        //                                int num1 = htmlElementCollections[0].GetElementsByTagName("img")["BtnDescarga"].OuterHtml.IndexOf("','");
        //                                innerText[1] = string.Concat("https://portalcfdi.facturaelectronica.sat.gob.mx/", htmlElementCollections[0].GetElementsByTagName("img")["BtnDescarga"].OuterHtml.Substring(num, num1 - num));
        //                            }
        //                            int num2 = 2;
        //                            while (true)
        //                            {
        //                                item = num2 < dt.Columns.Count;
        //                                if (!item)
        //                                {
        //                                    break;
        //                                }
        //                                if (htmlElementCollections[num2 - 1].InnerText == null)
        //                                {
        //                                    flag = true;
        //                                }
        //                                else
        //                                {
        //                                    flag = !htmlElementCollections[(num2 - 1)].InnerText.Contains("$");
        //                                }
        //                                item = flag;
        //                                if (item)
        //                                {
        //                                    innerText[num2] = htmlElementCollections[num2 - 1].InnerText;
        //                                }
        //                                else
        //                                {
        //                                    innerText[num2] = Convert.ToDecimal(htmlElementCollections[num2 - 1].InnerText.Replace("$", "").Replace(",", ""));
        //                                }
        //                                num2++;
        //                            }
        //                            dt.Rows.Add(innerText);
        //                        }
        //                        catch (ConstraintException constraintException)
        //                        {
        //                        }
        //                    }
        //                }
        //                finally
        //                {
        //                    disposable = enumerator1 as IDisposable;
        //                    item = disposable == null;
        //                    if (!item)
        //                    {
        //                        disposable.Dispose();
        //                    }
        //                }
        //            }
        //        }
        //        finally
        //        {
        //            disposable = enumerator as IDisposable;
        //            item = disposable == null;
        //            if (!item)
        //            {
        //                disposable.Dispose();
        //            }
        //        }
        //    }

        //    public static void ReportRowAsDownloaded(string UUID, DataTable TablaResultados)
        //    {
        //        bool length;
        //        bool tablaResultados = TablaResultados == null;
        //        if (!tablaResultados)
        //        {
        //            tablaResultados = TablaResultados.Columns.Contains("Descargado");
        //            if (!tablaResultados)
        //            {
        //                TablaResultados.Columns.Add("Descargado", typeof(bool));
        //            }
        //            DataRow[] dataRowArray = TablaResultados.Select(string.Format("UUID = '{0}'", UUID), "", DataViewRowState.CurrentRows);
        //            if (dataRowArray == null)
        //            {
        //                length = true;
        //            }
        //            else
        //            {
        //                length = (int)dataRowArray.Length <= 0;
        //            }
        //            tablaResultados = length;
        //            if (!tablaResultados)
        //            {
        //                dataRowArray[0]["Descargado"] = (bool)1;
        //            }
        //        }
        //    }
        //}
    }
}
