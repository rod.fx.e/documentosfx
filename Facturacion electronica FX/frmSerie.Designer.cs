﻿namespace FE_FX
{
    partial class frmSerie
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmSerie));
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.toolStripStatusLabel1 = new System.Windows.Forms.ToolStripStatusLabel();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.PAIS = new System.Windows.Forms.TextBox();
            this.button6 = new System.Windows.Forms.Button();
            this.ESTADO = new System.Windows.Forms.TextBox();
            this.MUNICIPIO = new System.Windows.Forms.TextBox();
            this.label15 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.REFERENCIA = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.LOCALIDAD = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.CP = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.COLONIA = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.INTERIOR = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.EXTERIOR = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.CALLE = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.label23 = new System.Windows.Forms.Label();
            this.PARA = new System.Windows.Forms.TextBox();
            this.tabControl2 = new System.Windows.Forms.TabControl();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.button1 = new System.Windows.Forms.Button();
            this.listBoxEleMensaje = new System.Windows.Forms.ListBox();
            this.ASUNTO = new System.Windows.Forms.TextBox();
            this.label18 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.MENSAJE = new System.Windows.Forms.RichTextBox();
            this.tabPage4 = new System.Windows.Forms.TabPage();
            this.button8 = new System.Windows.Forms.Button();
            this.listBox1 = new System.Windows.Forms.ListBox();
            this.ASUNTO_ADJUNTO = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.MENSAJE_ADJUNTO = new System.Windows.Forms.RichTextBox();
            this.tabPage5 = new System.Windows.Forms.TabPage();
            this.ASUNTO_CANCELADO = new System.Windows.Forms.TextBox();
            this.label16 = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.MENSAJE_CANCELADO = new System.Windows.Forms.RichTextBox();
            this.REMITENTE = new System.Windows.Forms.TextBox();
            this.label14 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.CCO = new System.Windows.Forms.TextBox();
            this.CC = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.DE = new System.Windows.Forms.TextBox();
            this.LbServidor = new System.Windows.Forms.Label();
            this.tabPage6 = new System.Windows.Forms.TabPage();
            this.label3 = new System.Windows.Forms.Label();
            this.CLIENTE_TRASLADO = new System.Windows.Forms.TextBox();
            this.label25 = new System.Windows.Forms.Label();
            this.rfcExcepcion = new System.Windows.Forms.TextBox();
            this.label24 = new System.Windows.Forms.Label();
            this.ACCOUNT_ID_ANTICIPO = new System.Windows.Forms.TextBox();
            this.tabPage7 = new System.Windows.Forms.TabPage();
            this.ndcUsarPropio = new System.Windows.Forms.CheckBox();
            this.label29 = new System.Windows.Forms.Label();
            this.label28 = new System.Windows.Forms.Label();
            this.ndcUnidad = new System.Windows.Forms.TextBox();
            this.ndcClaveProd = new System.Windows.Forms.TextBox();
            this.textBox2 = new System.Windows.Forms.TextBox();
            this.ndcMetodoPago = new System.Windows.Forms.ComboBox();
            this.ndcUsoCfdi = new System.Windows.Forms.ComboBox();
            this.label26 = new System.Windows.Forms.Label();
            this.label27 = new System.Windows.Forms.Label();
            this.tabPage8 = new System.Windows.Forms.TabPage();
            this.validarComprobantePago = new System.Windows.Forms.CheckBox();
            this.ID = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.FORMATO = new System.Windows.Forms.TextBox();
            this.CERTIFICADO = new System.Windows.Forms.TextBox();
            this.button7 = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.ajax_loader = new System.Windows.Forms.PictureBox();
            this.button4 = new System.Windows.Forms.Button();
            this.DIRECTORIO = new System.Windows.Forms.TextBox();
            this.ACTIVO = new System.Windows.Forms.CheckBox();
            this.label20 = new System.Windows.Forms.Label();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.ARCHIVO_NOMBRE = new System.Windows.Forms.TextBox();
            this.label22 = new System.Windows.Forms.Label();
            this.button5 = new System.Windows.Forms.Button();
            this.button9 = new System.Windows.Forms.Button();
            this.ENVIO_AUTOMATICO_MAIL = new System.Windows.Forms.CheckBox();
            this.button10 = new System.Windows.Forms.Button();
            this.serieComprobante = new System.Windows.Forms.CheckBox();
            this.serieTraslado = new System.Windows.Forms.CheckBox();
            this.IMPRESOS = new System.Windows.Forms.TextBox();
            this.label30 = new System.Windows.Forms.Label();
            this.button11 = new System.Windows.Forms.Button();
            this.ACCOUNT_ID_RETENIDO = new System.Windows.Forms.TextBox();
            this.label31 = new System.Windows.Forms.Label();
            this.UnirCarpeta = new System.Windows.Forms.CheckBox();
            this.statusStrip1.SuspendLayout();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.tabPage2.SuspendLayout();
            this.tabControl2.SuspendLayout();
            this.tabPage3.SuspendLayout();
            this.tabPage4.SuspendLayout();
            this.tabPage5.SuspendLayout();
            this.tabPage6.SuspendLayout();
            this.tabPage7.SuspendLayout();
            this.tabPage8.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ajax_loader)).BeginInit();
            this.SuspendLayout();
            // 
            // statusStrip1
            // 
            this.statusStrip1.Dock = System.Windows.Forms.DockStyle.Top;
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripStatusLabel1});
            this.statusStrip1.Location = new System.Drawing.Point(0, 0);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(698, 22);
            this.statusStrip1.TabIndex = 37;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // toolStripStatusLabel1
            // 
            this.toolStripStatusLabel1.Name = "toolStripStatusLabel1";
            this.toolStripStatusLabel1.Size = new System.Drawing.Size(42, 17);
            this.toolStripStatusLabel1.Text = "Estado";
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Controls.Add(this.tabPage6);
            this.tabControl1.Controls.Add(this.tabPage7);
            this.tabControl1.Controls.Add(this.tabPage8);
            this.tabControl1.Location = new System.Drawing.Point(4, 266);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(688, 282);
            this.tabControl1.TabIndex = 441;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.PAIS);
            this.tabPage1.Controls.Add(this.button6);
            this.tabPage1.Controls.Add(this.ESTADO);
            this.tabPage1.Controls.Add(this.MUNICIPIO);
            this.tabPage1.Controls.Add(this.label15);
            this.tabPage1.Controls.Add(this.label13);
            this.tabPage1.Controls.Add(this.REFERENCIA);
            this.tabPage1.Controls.Add(this.label12);
            this.tabPage1.Controls.Add(this.LOCALIDAD);
            this.tabPage1.Controls.Add(this.label11);
            this.tabPage1.Controls.Add(this.CP);
            this.tabPage1.Controls.Add(this.label10);
            this.tabPage1.Controls.Add(this.COLONIA);
            this.tabPage1.Controls.Add(this.label9);
            this.tabPage1.Controls.Add(this.INTERIOR);
            this.tabPage1.Controls.Add(this.label8);
            this.tabPage1.Controls.Add(this.EXTERIOR);
            this.tabPage1.Controls.Add(this.label7);
            this.tabPage1.Controls.Add(this.CALLE);
            this.tabPage1.Controls.Add(this.label2);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(680, 256);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Dirección";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // PAIS
            // 
            this.PAIS.Location = new System.Drawing.Point(61, 79);
            this.PAIS.Name = "PAIS";
            this.PAIS.Size = new System.Drawing.Size(267, 20);
            this.PAIS.TabIndex = 470;
            // 
            // button6
            // 
            this.button6.BackColor = System.Drawing.SystemColors.Control;
            this.button6.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button6.Image = global::FE_FX.Properties.Resources.Buscar;
            this.button6.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button6.Location = new System.Drawing.Point(336, 79);
            this.button6.Name = "button6";
            this.button6.Size = new System.Drawing.Size(71, 22);
            this.button6.TabIndex = 468;
            this.button6.Text = "Estado";
            this.button6.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.button6.UseVisualStyleBackColor = false;
            this.button6.Click += new System.EventHandler(this.button6_Click);
            // 
            // ESTADO
            // 
            this.ESTADO.Location = new System.Drawing.Point(413, 79);
            this.ESTADO.Name = "ESTADO";
            this.ESTADO.Size = new System.Drawing.Size(199, 20);
            this.ESTADO.TabIndex = 469;
            // 
            // MUNICIPIO
            // 
            this.MUNICIPIO.Location = new System.Drawing.Point(61, 106);
            this.MUNICIPIO.Name = "MUNICIPIO";
            this.MUNICIPIO.Size = new System.Drawing.Size(551, 20);
            this.MUNICIPIO.TabIndex = 138;
            this.MUNICIPIO.TextChanged += new System.EventHandler(this.Texto_Cambiado_TextChanged);
            this.MUNICIPIO.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.Verificar);
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(6, 109);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(52, 13);
            this.label15.TabIndex = 137;
            this.label15.Text = "Municipio";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(6, 83);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(29, 13);
            this.label13.TabIndex = 133;
            this.label13.Text = "País";
            // 
            // REFERENCIA
            // 
            this.REFERENCIA.Location = new System.Drawing.Point(399, 54);
            this.REFERENCIA.Name = "REFERENCIA";
            this.REFERENCIA.Size = new System.Drawing.Size(213, 20);
            this.REFERENCIA.TabIndex = 132;
            this.REFERENCIA.TextChanged += new System.EventHandler(this.Texto_Cambiado_TextChanged);
            this.REFERENCIA.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.Verificar);
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(334, 57);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(59, 13);
            this.label12.TabIndex = 131;
            this.label12.Text = "Referencia";
            // 
            // LOCALIDAD
            // 
            this.LOCALIDAD.Location = new System.Drawing.Point(61, 54);
            this.LOCALIDAD.Name = "LOCALIDAD";
            this.LOCALIDAD.Size = new System.Drawing.Size(267, 20);
            this.LOCALIDAD.TabIndex = 130;
            this.LOCALIDAD.TextChanged += new System.EventHandler(this.Texto_Cambiado_TextChanged);
            this.LOCALIDAD.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.Verificar);
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(3, 57);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(53, 13);
            this.label11.TabIndex = 129;
            this.label11.Text = "Localidad";
            // 
            // CP
            // 
            this.CP.Location = new System.Drawing.Point(533, 28);
            this.CP.Name = "CP";
            this.CP.Size = new System.Drawing.Size(79, 20);
            this.CP.TabIndex = 128;
            this.CP.TextChanged += new System.EventHandler(this.Texto_Cambiado_TextChanged);
            this.CP.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.Solo_Numero_KeyPress);
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(500, 31);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(27, 13);
            this.label10.TabIndex = 127;
            this.label10.Text = "C.P.";
            // 
            // COLONIA
            // 
            this.COLONIA.Location = new System.Drawing.Point(61, 28);
            this.COLONIA.Name = "COLONIA";
            this.COLONIA.Size = new System.Drawing.Size(421, 20);
            this.COLONIA.TabIndex = 126;
            this.COLONIA.TextChanged += new System.EventHandler(this.Texto_Cambiado_TextChanged);
            this.COLONIA.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.Verificar);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(2, 31);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(42, 13);
            this.label9.TabIndex = 125;
            this.label9.Text = "Colonia";
            // 
            // INTERIOR
            // 
            this.INTERIOR.Location = new System.Drawing.Point(533, 3);
            this.INTERIOR.Name = "INTERIOR";
            this.INTERIOR.Size = new System.Drawing.Size(79, 20);
            this.INTERIOR.TabIndex = 124;
            this.INTERIOR.TextChanged += new System.EventHandler(this.Texto_Cambiado_TextChanged);
            this.INTERIOR.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.Verificar);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(488, 6);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(39, 13);
            this.label8.TabIndex = 123;
            this.label8.Text = "Interior";
            // 
            // EXTERIOR
            // 
            this.EXTERIOR.Location = new System.Drawing.Point(412, 3);
            this.EXTERIOR.Name = "EXTERIOR";
            this.EXTERIOR.Size = new System.Drawing.Size(70, 20);
            this.EXTERIOR.TabIndex = 122;
            this.EXTERIOR.TextChanged += new System.EventHandler(this.Texto_Cambiado_TextChanged);
            this.EXTERIOR.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.Verificar);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(361, 6);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(42, 13);
            this.label7.TabIndex = 121;
            this.label7.Text = "Exterior";
            // 
            // CALLE
            // 
            this.CALLE.Location = new System.Drawing.Point(61, 3);
            this.CALLE.Name = "CALLE";
            this.CALLE.Size = new System.Drawing.Size(286, 20);
            this.CALLE.TabIndex = 120;
            this.CALLE.TextChanged += new System.EventHandler(this.Texto_Cambiado_TextChanged);
            this.CALLE.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.Verificar);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(2, 6);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(30, 13);
            this.label2.TabIndex = 120;
            this.label2.Text = "Calle";
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.label23);
            this.tabPage2.Controls.Add(this.PARA);
            this.tabPage2.Controls.Add(this.tabControl2);
            this.tabPage2.Controls.Add(this.REMITENTE);
            this.tabPage2.Controls.Add(this.label14);
            this.tabPage2.Controls.Add(this.label19);
            this.tabPage2.Controls.Add(this.CCO);
            this.tabPage2.Controls.Add(this.CC);
            this.tabPage2.Controls.Add(this.label6);
            this.tabPage2.Controls.Add(this.DE);
            this.tabPage2.Controls.Add(this.LbServidor);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(680, 266);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Correo Electrónico";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Location = new System.Drawing.Point(9, 31);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(29, 13);
            this.label23.TabIndex = 446;
            this.label23.Text = "Para";
            // 
            // PARA
            // 
            this.PARA.Location = new System.Drawing.Point(57, 27);
            this.PARA.Name = "PARA";
            this.PARA.Size = new System.Drawing.Size(247, 20);
            this.PARA.TabIndex = 445;
            // 
            // tabControl2
            // 
            this.tabControl2.Controls.Add(this.tabPage3);
            this.tabControl2.Controls.Add(this.tabPage4);
            this.tabControl2.Controls.Add(this.tabPage5);
            this.tabControl2.Location = new System.Drawing.Point(0, 73);
            this.tabControl2.Name = "tabControl2";
            this.tabControl2.SelectedIndex = 0;
            this.tabControl2.Size = new System.Drawing.Size(621, 159);
            this.tabControl2.TabIndex = 444;
            // 
            // tabPage3
            // 
            this.tabPage3.Controls.Add(this.button1);
            this.tabPage3.Controls.Add(this.listBoxEleMensaje);
            this.tabPage3.Controls.Add(this.ASUNTO);
            this.tabPage3.Controls.Add(this.label18);
            this.tabPage3.Controls.Add(this.label17);
            this.tabPage3.Controls.Add(this.MENSAJE);
            this.tabPage3.Location = new System.Drawing.Point(4, 22);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage3.Size = new System.Drawing.Size(613, 133);
            this.tabPage3.TabIndex = 0;
            this.tabPage3.Text = "Normal";
            this.tabPage3.UseVisualStyleBackColor = true;
            // 
            // button1
            // 
            this.button1.BackColor = System.Drawing.SystemColors.Control;
            this.button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button1.Image = global::FE_FX.Properties.Resources.Etiquetas;
            this.button1.Location = new System.Drawing.Point(6, 49);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(38, 30);
            this.button1.TabIndex = 447;
            this.button1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.button1.UseVisualStyleBackColor = false;
            this.button1.Click += new System.EventHandler(this.button1_Click_1);
            // 
            // listBoxEleMensaje
            // 
            this.listBoxEleMensaje.FormattingEnabled = true;
            this.listBoxEleMensaje.Items.AddRange(new object[] {
            "Factura",
            "Fecha Factura",
            "Empresa",
            "Tipo de Documento",
            "Cliente",
            "Contacto",
            "Estado del Documento"});
            this.listBoxEleMensaje.Location = new System.Drawing.Point(54, 30);
            this.listBoxEleMensaje.Name = "listBoxEleMensaje";
            this.listBoxEleMensaje.Size = new System.Drawing.Size(119, 95);
            this.listBoxEleMensaje.TabIndex = 445;
            this.listBoxEleMensaje.Visible = false;
            this.listBoxEleMensaje.SelectedIndexChanged += new System.EventHandler(this.listBoxEleMensaje_SelectedIndexChanged);
            // 
            // ASUNTO
            // 
            this.ASUNTO.Location = new System.Drawing.Point(54, 4);
            this.ASUNTO.Name = "ASUNTO";
            this.ASUNTO.Size = new System.Drawing.Size(553, 20);
            this.ASUNTO.TabIndex = 442;
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(8, 7);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(40, 13);
            this.label18.TabIndex = 444;
            this.label18.Text = "Asunto";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(5, 33);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(47, 13);
            this.label17.TabIndex = 443;
            this.label17.Text = "Mensaje";
            // 
            // MENSAJE
            // 
            this.MENSAJE.Location = new System.Drawing.Point(53, 30);
            this.MENSAJE.Name = "MENSAJE";
            this.MENSAJE.Size = new System.Drawing.Size(554, 105);
            this.MENSAJE.TabIndex = 446;
            this.MENSAJE.Text = "";
            // 
            // tabPage4
            // 
            this.tabPage4.Controls.Add(this.button8);
            this.tabPage4.Controls.Add(this.listBox1);
            this.tabPage4.Controls.Add(this.ASUNTO_ADJUNTO);
            this.tabPage4.Controls.Add(this.label4);
            this.tabPage4.Controls.Add(this.label5);
            this.tabPage4.Controls.Add(this.MENSAJE_ADJUNTO);
            this.tabPage4.Location = new System.Drawing.Point(4, 22);
            this.tabPage4.Name = "tabPage4";
            this.tabPage4.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage4.Size = new System.Drawing.Size(613, 133);
            this.tabPage4.TabIndex = 1;
            this.tabPage4.Text = "Adjunto";
            this.tabPage4.UseVisualStyleBackColor = true;
            // 
            // button8
            // 
            this.button8.BackColor = System.Drawing.SystemColors.Control;
            this.button8.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button8.Image = global::FE_FX.Properties.Resources.Etiquetas;
            this.button8.Location = new System.Drawing.Point(6, 50);
            this.button8.Name = "button8";
            this.button8.Size = new System.Drawing.Size(38, 30);
            this.button8.TabIndex = 453;
            this.button8.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.button8.UseVisualStyleBackColor = false;
            // 
            // listBox1
            // 
            this.listBox1.FormattingEnabled = true;
            this.listBox1.Items.AddRange(new object[] {
            "Factura",
            "Fecha Factura",
            "Empresa",
            "Tipo de Documento",
            "Cliente",
            "Contacto",
            "Estado del Documento"});
            this.listBox1.Location = new System.Drawing.Point(54, 31);
            this.listBox1.Name = "listBox1";
            this.listBox1.Size = new System.Drawing.Size(119, 95);
            this.listBox1.TabIndex = 451;
            this.listBox1.Visible = false;
            // 
            // ASUNTO_ADJUNTO
            // 
            this.ASUNTO_ADJUNTO.Location = new System.Drawing.Point(54, 5);
            this.ASUNTO_ADJUNTO.Name = "ASUNTO_ADJUNTO";
            this.ASUNTO_ADJUNTO.Size = new System.Drawing.Size(553, 20);
            this.ASUNTO_ADJUNTO.TabIndex = 448;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(8, 8);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(40, 13);
            this.label4.TabIndex = 450;
            this.label4.Text = "Asunto";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(5, 34);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(47, 13);
            this.label5.TabIndex = 449;
            this.label5.Text = "Mensaje";
            // 
            // MENSAJE_ADJUNTO
            // 
            this.MENSAJE_ADJUNTO.Location = new System.Drawing.Point(53, 31);
            this.MENSAJE_ADJUNTO.Name = "MENSAJE_ADJUNTO";
            this.MENSAJE_ADJUNTO.Size = new System.Drawing.Size(554, 133);
            this.MENSAJE_ADJUNTO.TabIndex = 452;
            this.MENSAJE_ADJUNTO.Text = "";
            // 
            // tabPage5
            // 
            this.tabPage5.Controls.Add(this.ASUNTO_CANCELADO);
            this.tabPage5.Controls.Add(this.label16);
            this.tabPage5.Controls.Add(this.label21);
            this.tabPage5.Controls.Add(this.MENSAJE_CANCELADO);
            this.tabPage5.Location = new System.Drawing.Point(4, 22);
            this.tabPage5.Name = "tabPage5";
            this.tabPage5.Size = new System.Drawing.Size(613, 133);
            this.tabPage5.TabIndex = 2;
            this.tabPage5.Text = "Cancelado";
            this.tabPage5.UseVisualStyleBackColor = true;
            // 
            // ASUNTO_CANCELADO
            // 
            this.ASUNTO_CANCELADO.Location = new System.Drawing.Point(54, 5);
            this.ASUNTO_CANCELADO.Name = "ASUNTO_CANCELADO";
            this.ASUNTO_CANCELADO.Size = new System.Drawing.Size(553, 20);
            this.ASUNTO_CANCELADO.TabIndex = 453;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(8, 8);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(40, 13);
            this.label16.TabIndex = 455;
            this.label16.Text = "Asunto";
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(5, 34);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(47, 13);
            this.label21.TabIndex = 454;
            this.label21.Text = "Mensaje";
            // 
            // MENSAJE_CANCELADO
            // 
            this.MENSAJE_CANCELADO.Location = new System.Drawing.Point(53, 31);
            this.MENSAJE_CANCELADO.Name = "MENSAJE_CANCELADO";
            this.MENSAJE_CANCELADO.Size = new System.Drawing.Size(554, 133);
            this.MENSAJE_CANCELADO.TabIndex = 456;
            this.MENSAJE_CANCELADO.Text = "";
            // 
            // REMITENTE
            // 
            this.REMITENTE.Location = new System.Drawing.Point(368, 3);
            this.REMITENTE.Name = "REMITENTE";
            this.REMITENTE.Size = new System.Drawing.Size(247, 20);
            this.REMITENTE.TabIndex = 443;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(310, 7);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(55, 13);
            this.label14.TabIndex = 442;
            this.label14.Text = "Remitente";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(9, 56);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(38, 13);
            this.label19.TabIndex = 151;
            this.label19.Text = "C.C.O.";
            // 
            // CCO
            // 
            this.CCO.Location = new System.Drawing.Point(57, 53);
            this.CCO.Name = "CCO";
            this.CCO.Size = new System.Drawing.Size(558, 20);
            this.CCO.TabIndex = 145;
            this.CCO.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.Verificar);
            // 
            // CC
            // 
            this.CC.Location = new System.Drawing.Point(345, 28);
            this.CC.Name = "CC";
            this.CC.Size = new System.Drawing.Size(270, 20);
            this.CC.TabIndex = 144;
            this.CC.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.Verificar);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(312, 30);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(27, 13);
            this.label6.TabIndex = 148;
            this.label6.Text = "C.C.";
            // 
            // DE
            // 
            this.DE.Location = new System.Drawing.Point(57, 3);
            this.DE.Name = "DE";
            this.DE.Size = new System.Drawing.Size(247, 20);
            this.DE.TabIndex = 143;
            this.DE.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.Verificar);
            // 
            // LbServidor
            // 
            this.LbServidor.AutoSize = true;
            this.LbServidor.Location = new System.Drawing.Point(10, 7);
            this.LbServidor.Name = "LbServidor";
            this.LbServidor.Size = new System.Drawing.Size(21, 13);
            this.LbServidor.TabIndex = 147;
            this.LbServidor.Text = "De";
            // 
            // tabPage6
            // 
            this.tabPage6.Controls.Add(this.label3);
            this.tabPage6.Controls.Add(this.CLIENTE_TRASLADO);
            this.tabPage6.Controls.Add(this.label25);
            this.tabPage6.Controls.Add(this.rfcExcepcion);
            this.tabPage6.Controls.Add(this.label24);
            this.tabPage6.Controls.Add(this.ACCOUNT_ID_ANTICIPO);
            this.tabPage6.Location = new System.Drawing.Point(4, 22);
            this.tabPage6.Name = "tabPage6";
            this.tabPage6.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage6.Size = new System.Drawing.Size(680, 266);
            this.tabPage6.TabIndex = 2;
            this.tabPage6.Text = "Otros";
            this.tabPage6.UseVisualStyleBackColor = true;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(7, 61);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(94, 13);
            this.label3.TabIndex = 4;
            this.label3.Text = "Cliente de traslado";
            // 
            // CLIENTE_TRASLADO
            // 
            this.CLIENTE_TRASLADO.Location = new System.Drawing.Point(148, 58);
            this.CLIENTE_TRASLADO.Name = "CLIENTE_TRASLADO";
            this.CLIENTE_TRASLADO.Size = new System.Drawing.Size(470, 20);
            this.CLIENTE_TRASLADO.TabIndex = 5;
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Location = new System.Drawing.Point(6, 35);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(127, 13);
            this.label25.TabIndex = 2;
            this.label25.Text = "RFC excepto de timbrado";
            // 
            // rfcExcepcion
            // 
            this.rfcExcepcion.Location = new System.Drawing.Point(148, 32);
            this.rfcExcepcion.Name = "rfcExcepcion";
            this.rfcExcepcion.Size = new System.Drawing.Size(470, 20);
            this.rfcExcepcion.TabIndex = 3;
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Location = new System.Drawing.Point(6, 9);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(140, 13);
            this.label24.TabIndex = 0;
            this.label24.Text = "Cuenta contable de anticipo";
            // 
            // ACCOUNT_ID_ANTICIPO
            // 
            this.ACCOUNT_ID_ANTICIPO.Location = new System.Drawing.Point(148, 6);
            this.ACCOUNT_ID_ANTICIPO.Name = "ACCOUNT_ID_ANTICIPO";
            this.ACCOUNT_ID_ANTICIPO.Size = new System.Drawing.Size(470, 20);
            this.ACCOUNT_ID_ANTICIPO.TabIndex = 1;
            // 
            // tabPage7
            // 
            this.tabPage7.Controls.Add(this.ndcUsarPropio);
            this.tabPage7.Controls.Add(this.label29);
            this.tabPage7.Controls.Add(this.label28);
            this.tabPage7.Controls.Add(this.ndcUnidad);
            this.tabPage7.Controls.Add(this.ndcClaveProd);
            this.tabPage7.Controls.Add(this.textBox2);
            this.tabPage7.Controls.Add(this.ndcMetodoPago);
            this.tabPage7.Controls.Add(this.ndcUsoCfdi);
            this.tabPage7.Controls.Add(this.label26);
            this.tabPage7.Controls.Add(this.label27);
            this.tabPage7.Location = new System.Drawing.Point(4, 22);
            this.tabPage7.Name = "tabPage7";
            this.tabPage7.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage7.Size = new System.Drawing.Size(680, 266);
            this.tabPage7.TabIndex = 3;
            this.tabPage7.Text = "Nota de crédito";
            this.tabPage7.UseVisualStyleBackColor = true;
            // 
            // ndcUsarPropio
            // 
            this.ndcUsarPropio.AutoSize = true;
            this.ndcUsarPropio.Location = new System.Drawing.Point(103, 30);
            this.ndcUsarPropio.Name = "ndcUsarPropio";
            this.ndcUsarPropio.Size = new System.Drawing.Size(221, 17);
            this.ndcUsarPropio.TabIndex = 471;
            this.ndcUsarPropio.Text = "Al generar NDC usar los siguiente valores";
            this.ndcUsarPropio.UseVisualStyleBackColor = true;
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Location = new System.Drawing.Point(26, 136);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(71, 13);
            this.label29.TabIndex = 7;
            this.label29.Text = "Clave Unidad";
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Location = new System.Drawing.Point(7, 110);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(90, 13);
            this.label28.TabIndex = 5;
            this.label28.Text = "Clave Prod. Serv.";
            // 
            // ndcUnidad
            // 
            this.ndcUnidad.Location = new System.Drawing.Point(103, 133);
            this.ndcUnidad.Name = "ndcUnidad";
            this.ndcUnidad.Size = new System.Drawing.Size(108, 20);
            this.ndcUnidad.TabIndex = 8;
            // 
            // ndcClaveProd
            // 
            this.ndcClaveProd.Location = new System.Drawing.Point(103, 107);
            this.ndcClaveProd.Name = "ndcClaveProd";
            this.ndcClaveProd.Size = new System.Drawing.Size(108, 20);
            this.ndcClaveProd.TabIndex = 6;
            // 
            // textBox2
            // 
            this.textBox2.BackColor = System.Drawing.Color.White;
            this.textBox2.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox2.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F);
            this.textBox2.Location = new System.Drawing.Point(6, 6);
            this.textBox2.Multiline = true;
            this.textBox2.Name = "textBox2";
            this.textBox2.Size = new System.Drawing.Size(664, 18);
            this.textBox2.TabIndex = 0;
            this.textBox2.Text = "Esta configuración se realizara cuando la serie se use para generar un CFDI de Eg" +
    "reso o Nota de crédito";
            // 
            // ndcMetodoPago
            // 
            this.ndcMetodoPago.FormattingEnabled = true;
            this.ndcMetodoPago.Items.AddRange(new object[] {
            "01 Efectivo",
            "02 Cheque nominativo",
            "03 Transferencia electrónica de fondos",
            "04 Tarjetas de crédito",
            "05 Monederos electrónicos",
            "06 Dinero electrónico",
            "07 Tarjetas digitales",
            "08 Vales de despensa",
            "28 Tarjeta de Débito",
            "29 Tarjeta de Servicio",
            "99 Otros",
            "NA"});
            this.ndcMetodoPago.Location = new System.Drawing.Point(103, 53);
            this.ndcMetodoPago.Name = "ndcMetodoPago";
            this.ndcMetodoPago.Size = new System.Drawing.Size(393, 21);
            this.ndcMetodoPago.TabIndex = 2;
            // 
            // ndcUsoCfdi
            // 
            this.ndcUsoCfdi.FormattingEnabled = true;
            this.ndcUsoCfdi.Items.AddRange(new object[] {
            "Pago en una sola exhibición",
            "Pago en parcialidades",
            "Pago a credito"});
            this.ndcUsoCfdi.Location = new System.Drawing.Point(103, 80);
            this.ndcUsoCfdi.Name = "ndcUsoCfdi";
            this.ndcUsoCfdi.Size = new System.Drawing.Size(393, 21);
            this.ndcUsoCfdi.TabIndex = 4;
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Location = new System.Drawing.Point(27, 83);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(70, 13);
            this.label26.TabIndex = 3;
            this.label26.Text = "Uso del CFDI";
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Location = new System.Drawing.Point(11, 56);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(86, 13);
            this.label27.TabIndex = 1;
            this.label27.Text = "Método de Pago";
            // 
            // tabPage8
            // 
            this.tabPage8.Controls.Add(this.validarComprobantePago);
            this.tabPage8.Location = new System.Drawing.Point(4, 22);
            this.tabPage8.Name = "tabPage8";
            this.tabPage8.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage8.Size = new System.Drawing.Size(680, 266);
            this.tabPage8.TabIndex = 4;
            this.tabPage8.Text = "Comprobante de Pago";
            this.tabPage8.UseVisualStyleBackColor = true;
            // 
            // validarComprobantePago
            // 
            this.validarComprobantePago.AutoSize = true;
            this.validarComprobantePago.Location = new System.Drawing.Point(6, 6);
            this.validarComprobantePago.Name = "validarComprobantePago";
            this.validarComprobantePago.Size = new System.Drawing.Size(437, 17);
            this.validarComprobantePago.TabIndex = 471;
            this.validarComprobantePago.Text = "Validar que las facturas a relacionar tenga Metodo de Pago: PPD y Forma de pago: " +
    "99 ";
            this.validarComprobantePago.UseVisualStyleBackColor = true;
            // 
            // ID
            // 
            this.ID.Location = new System.Drawing.Point(57, 59);
            this.ID.Name = "ID";
            this.ID.Size = new System.Drawing.Size(79, 20);
            this.ID.TabIndex = 444;
            this.ID.TextChanged += new System.EventHandler(this.Texto_Cambiado_TextChanged);
            this.ID.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.Verificar);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(5, 62);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(31, 13);
            this.label1.TabIndex = 443;
            this.label1.Text = "Serie";
            // 
            // FORMATO
            // 
            this.FORMATO.Location = new System.Drawing.Point(225, 59);
            this.FORMATO.Name = "FORMATO";
            this.FORMATO.Size = new System.Drawing.Size(181, 20);
            this.FORMATO.TabIndex = 445;
            this.FORMATO.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.Solo_Numero_KeyPress);
            // 
            // CERTIFICADO
            // 
            this.CERTIFICADO.Enabled = false;
            this.CERTIFICADO.Location = new System.Drawing.Point(160, 111);
            this.CERTIFICADO.Name = "CERTIFICADO";
            this.CERTIFICADO.Size = new System.Drawing.Size(528, 20);
            this.CERTIFICADO.TabIndex = 456;
            // 
            // button7
            // 
            this.button7.BackColor = System.Drawing.SystemColors.Control;
            this.button7.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button7.Image = global::FE_FX.Properties.Resources.Buscar;
            this.button7.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button7.Location = new System.Drawing.Point(70, 108);
            this.button7.Name = "button7";
            this.button7.Size = new System.Drawing.Size(84, 23);
            this.button7.TabIndex = 455;
            this.button7.Text = "Certificado";
            this.button7.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.button7.UseVisualStyleBackColor = false;
            this.button7.Click += new System.EventHandler(this.button7_Click);
            // 
            // button3
            // 
            this.button3.BackColor = System.Drawing.SystemColors.Control;
            this.button3.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button3.Image = global::FE_FX.Properties.Resources.Guardar;
            this.button3.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button3.Location = new System.Drawing.Point(80, 30);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(83, 23);
            this.button3.TabIndex = 440;
            this.button3.Text = "Guardar";
            this.button3.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.button3.UseVisualStyleBackColor = false;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // button2
            // 
            this.button2.BackColor = System.Drawing.SystemColors.Control;
            this.button2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button2.Image = global::FE_FX.Properties.Resources.Nuevo;
            this.button2.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button2.Location = new System.Drawing.Point(4, 30);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(70, 23);
            this.button2.TabIndex = 439;
            this.button2.Text = "Nuevo";
            this.button2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.button2.UseVisualStyleBackColor = false;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // ajax_loader
            // 
            this.ajax_loader.AccessibleDescription = "";
            this.ajax_loader.Image = global::FE_FX.Properties.Resources.ajax_loader;
            this.ajax_loader.Location = new System.Drawing.Point(372, 31);
            this.ajax_loader.Name = "ajax_loader";
            this.ajax_loader.Size = new System.Drawing.Size(58, 22);
            this.ajax_loader.TabIndex = 38;
            this.ajax_loader.TabStop = false;
            // 
            // button4
            // 
            this.button4.BackColor = System.Drawing.SystemColors.Control;
            this.button4.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button4.Image = global::FE_FX.Properties.Resources.Buscar;
            this.button4.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button4.Location = new System.Drawing.Point(4, 161);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(108, 23);
            this.button4.TabIndex = 457;
            this.button4.Text = "Directorio";
            this.button4.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.button4.UseVisualStyleBackColor = false;
            this.button4.Click += new System.EventHandler(this.button4_Click);
            // 
            // DIRECTORIO
            // 
            this.DIRECTORIO.Location = new System.Drawing.Point(118, 163);
            this.DIRECTORIO.Name = "DIRECTORIO";
            this.DIRECTORIO.Size = new System.Drawing.Size(570, 20);
            this.DIRECTORIO.TabIndex = 458;
            // 
            // ACTIVO
            // 
            this.ACTIVO.AutoSize = true;
            this.ACTIVO.Location = new System.Drawing.Point(8, 112);
            this.ACTIVO.Name = "ACTIVO";
            this.ACTIVO.Size = new System.Drawing.Size(56, 17);
            this.ACTIVO.TabIndex = 460;
            this.ACTIVO.Text = "Activo";
            this.ACTIVO.UseVisualStyleBackColor = true;
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(1, 135);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(0, 13);
            this.label20.TabIndex = 461;
            // 
            // textBox1
            // 
            this.textBox1.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.textBox1.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F);
            this.textBox1.Location = new System.Drawing.Point(8, 218);
            this.textBox1.Multiline = true;
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(684, 51);
            this.textBox1.TabIndex = 462;
            this.textBox1.Text = resources.GetString("textBox1.Text");
            this.textBox1.TextChanged += new System.EventHandler(this.textBox1_TextChanged);
            // 
            // ARCHIVO_NOMBRE
            // 
            this.ARCHIVO_NOMBRE.Location = new System.Drawing.Point(118, 189);
            this.ARCHIVO_NOMBRE.Name = "ARCHIVO_NOMBRE";
            this.ARCHIVO_NOMBRE.Size = new System.Drawing.Size(570, 20);
            this.ARCHIVO_NOMBRE.TabIndex = 463;
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Location = new System.Drawing.Point(5, 192);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(100, 13);
            this.label22.TabIndex = 464;
            this.label22.Text = "Nombre del Archivo";
            // 
            // button5
            // 
            this.button5.BackColor = System.Drawing.SystemColors.Control;
            this.button5.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button5.Image = global::FE_FX.Properties.Resources.Buscar;
            this.button5.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button5.Location = new System.Drawing.Point(142, 57);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(77, 23);
            this.button5.TabIndex = 465;
            this.button5.Text = "Formato";
            this.button5.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.button5.UseVisualStyleBackColor = false;
            this.button5.Click += new System.EventHandler(this.button5_Click_1);
            // 
            // button9
            // 
            this.button9.BackColor = System.Drawing.SystemColors.Control;
            this.button9.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button9.Image = global::FE_FX.Properties.Resources.Eliminar_Linea;
            this.button9.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button9.Location = new System.Drawing.Point(169, 30);
            this.button9.Name = "button9";
            this.button9.Size = new System.Drawing.Size(83, 23);
            this.button9.TabIndex = 469;
            this.button9.Text = "Eliminar";
            this.button9.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.button9.UseVisualStyleBackColor = false;
            this.button9.Click += new System.EventHandler(this.button9_Click);
            // 
            // ENVIO_AUTOMATICO_MAIL
            // 
            this.ENVIO_AUTOMATICO_MAIL.AutoSize = true;
            this.ENVIO_AUTOMATICO_MAIL.Location = new System.Drawing.Point(412, 61);
            this.ENVIO_AUTOMATICO_MAIL.Name = "ENVIO_AUTOMATICO_MAIL";
            this.ENVIO_AUTOMATICO_MAIL.Size = new System.Drawing.Size(89, 17);
            this.ENVIO_AUTOMATICO_MAIL.TabIndex = 470;
            this.ENVIO_AUTOMATICO_MAIL.Text = "Enviar correo";
            this.ENVIO_AUTOMATICO_MAIL.UseVisualStyleBackColor = true;
            // 
            // button10
            // 
            this.button10.BackColor = System.Drawing.SystemColors.Control;
            this.button10.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button10.Image = global::FE_FX.Properties.Resources.Guardar;
            this.button10.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button10.Location = new System.Drawing.Point(258, 30);
            this.button10.Name = "button10";
            this.button10.Size = new System.Drawing.Size(83, 23);
            this.button10.TabIndex = 440;
            this.button10.Text = "Duplicar";
            this.button10.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.button10.UseVisualStyleBackColor = false;
            this.button10.Click += new System.EventHandler(this.button10_Click);
            // 
            // serieComprobante
            // 
            this.serieComprobante.AutoSize = true;
            this.serieComprobante.Location = new System.Drawing.Point(8, 85);
            this.serieComprobante.Name = "serieComprobante";
            this.serieComprobante.Size = new System.Drawing.Size(181, 17);
            this.serieComprobante.TabIndex = 470;
            this.serieComprobante.Text = "Serie para comprobante de pago";
            this.serieComprobante.UseVisualStyleBackColor = true;
            // 
            // serieTraslado
            // 
            this.serieTraslado.AutoSize = true;
            this.serieTraslado.Location = new System.Drawing.Point(195, 85);
            this.serieTraslado.Name = "serieTraslado";
            this.serieTraslado.Size = new System.Drawing.Size(114, 17);
            this.serieTraslado.TabIndex = 470;
            this.serieTraslado.Text = "Serie para traslado";
            this.serieTraslado.UseVisualStyleBackColor = true;
            // 
            // IMPRESOS
            // 
            this.IMPRESOS.Location = new System.Drawing.Point(576, 59);
            this.IMPRESOS.Name = "IMPRESOS";
            this.IMPRESOS.Size = new System.Drawing.Size(44, 20);
            this.IMPRESOS.TabIndex = 445;
            this.IMPRESOS.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.Solo_Numero_KeyPress);
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Location = new System.Drawing.Point(504, 63);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(66, 13);
            this.label30.TabIndex = 443;
            this.label30.Text = "Consecutivo";
            // 
            // button11
            // 
            this.button11.BackColor = System.Drawing.SystemColors.Control;
            this.button11.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button11.Image = global::FE_FX.Properties.Resources.Guardar;
            this.button11.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button11.Location = new System.Drawing.Point(626, 58);
            this.button11.Name = "button11";
            this.button11.Size = new System.Drawing.Size(31, 23);
            this.button11.TabIndex = 440;
            this.button11.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.button11.UseVisualStyleBackColor = false;
            this.button11.Click += new System.EventHandler(this.button11_Click);
            // 
            // ACCOUNT_ID_RETENIDO
            // 
            this.ACCOUNT_ID_RETENIDO.Location = new System.Drawing.Point(118, 137);
            this.ACCOUNT_ID_RETENIDO.Name = "ACCOUNT_ID_RETENIDO";
            this.ACCOUNT_ID_RETENIDO.Size = new System.Drawing.Size(570, 20);
            this.ACCOUNT_ID_RETENIDO.TabIndex = 456;
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.Location = new System.Drawing.Point(1, 140);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(108, 13);
            this.label31.TabIndex = 466;
            this.label31.Text = "Cuenta de Retención";
            // 
            // UnirCarpeta
            // 
            this.UnirCarpeta.AutoSize = true;
            this.UnirCarpeta.Location = new System.Drawing.Point(478, 85);
            this.UnirCarpeta.Name = "UnirCarpeta";
            this.UnirCarpeta.Size = new System.Drawing.Size(210, 17);
            this.UnirCarpeta.TabIndex = 471;
            this.UnirCarpeta.Text = "Unir el PDF y XML en la misma carpeta";
            this.UnirCarpeta.UseVisualStyleBackColor = true;
            // 
            // frmSerie
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(698, 556);
            this.Controls.Add(this.UnirCarpeta);
            this.Controls.Add(this.serieTraslado);
            this.Controls.Add(this.serieComprobante);
            this.Controls.Add(this.ENVIO_AUTOMATICO_MAIL);
            this.Controls.Add(this.button9);
            this.Controls.Add(this.label31);
            this.Controls.Add(this.button5);
            this.Controls.Add(this.label22);
            this.Controls.Add(this.ARCHIVO_NOMBRE);
            this.Controls.Add(this.textBox1);
            this.Controls.Add(this.label20);
            this.Controls.Add(this.ACTIVO);
            this.Controls.Add(this.DIRECTORIO);
            this.Controls.Add(this.button4);
            this.Controls.Add(this.ACCOUNT_ID_RETENIDO);
            this.Controls.Add(this.CERTIFICADO);
            this.Controls.Add(this.button7);
            this.Controls.Add(this.IMPRESOS);
            this.Controls.Add(this.FORMATO);
            this.Controls.Add(this.ID);
            this.Controls.Add(this.label30);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.tabControl1);
            this.Controls.Add(this.button11);
            this.Controls.Add(this.button10);
            this.Controls.Add(this.button3);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.ajax_loader);
            this.Controls.Add(this.statusStrip1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.KeyPreview = true;
            this.MaximizeBox = false;
            this.Name = "frmSerie";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Serie";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frmUsuario_FormClosing);
            this.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.frmUsuario_KeyPress);
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage1.PerformLayout();
            this.tabPage2.ResumeLayout(false);
            this.tabPage2.PerformLayout();
            this.tabControl2.ResumeLayout(false);
            this.tabPage3.ResumeLayout(false);
            this.tabPage3.PerformLayout();
            this.tabPage4.ResumeLayout(false);
            this.tabPage4.PerformLayout();
            this.tabPage5.ResumeLayout(false);
            this.tabPage5.PerformLayout();
            this.tabPage6.ResumeLayout(false);
            this.tabPage6.PerformLayout();
            this.tabPage7.ResumeLayout(false);
            this.tabPage7.PerformLayout();
            this.tabPage8.ResumeLayout(false);
            this.tabPage8.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ajax_loader)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel1;
        private System.Windows.Forms.PictureBox ajax_loader;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TextBox MUNICIPIO;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.TextBox REFERENCIA;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.TextBox LOCALIDAD;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TextBox CP;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox COLONIA;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox INTERIOR;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox EXTERIOR;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox CALLE;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.TextBox ID;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox FORMATO;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.TextBox CCO;
        private System.Windows.Forms.TextBox CC;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox DE;
        private System.Windows.Forms.Label LbServidor;
        private System.Windows.Forms.TextBox CERTIFICADO;
        private System.Windows.Forms.Button button7;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.TextBox DIRECTORIO;
        private System.Windows.Forms.CheckBox ACTIVO;
        private System.Windows.Forms.Button button6;
        private System.Windows.Forms.TextBox ESTADO;
        private System.Windows.Forms.TextBox REMITENTE;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.TextBox ARCHIVO_NOMBRE;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Button button5;
        private System.Windows.Forms.TabControl tabControl2;
        private System.Windows.Forms.TabPage tabPage3;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.ListBox listBoxEleMensaje;
        private System.Windows.Forms.TextBox ASUNTO;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.RichTextBox MENSAJE;
        private System.Windows.Forms.TabPage tabPage4;
        private System.Windows.Forms.Button button8;
        private System.Windows.Forms.ListBox listBox1;
        private System.Windows.Forms.TextBox ASUNTO_ADJUNTO;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.RichTextBox MENSAJE_ADJUNTO;
        private System.Windows.Forms.TabPage tabPage5;
        private System.Windows.Forms.TextBox ASUNTO_CANCELADO;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.RichTextBox MENSAJE_CANCELADO;
        private System.Windows.Forms.Button button9;
        private System.Windows.Forms.CheckBox ENVIO_AUTOMATICO_MAIL;
        private System.Windows.Forms.Button button10;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.TextBox PARA;
        private System.Windows.Forms.TabPage tabPage6;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.TextBox ACCOUNT_ID_ANTICIPO;
        private System.Windows.Forms.TextBox PAIS;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.TextBox rfcExcepcion;
        private System.Windows.Forms.CheckBox serieComprobante;
        private System.Windows.Forms.CheckBox serieTraslado;
        private System.Windows.Forms.TabPage tabPage7;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.TextBox ndcUnidad;
        private System.Windows.Forms.TextBox ndcClaveProd;
        private System.Windows.Forms.TextBox textBox2;
        private System.Windows.Forms.ComboBox ndcMetodoPago;
        private System.Windows.Forms.ComboBox ndcUsoCfdi;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.CheckBox ndcUsarPropio;
        private System.Windows.Forms.TabPage tabPage8;
        private System.Windows.Forms.CheckBox validarComprobantePago;
        private System.Windows.Forms.TextBox IMPRESOS;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.Button button11;
        private System.Windows.Forms.TextBox ACCOUNT_ID_RETENIDO;
        private System.Windows.Forms.Label label31;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox CLIENTE_TRASLADO;
        private System.Windows.Forms.CheckBox UnirCarpeta;
    }
}