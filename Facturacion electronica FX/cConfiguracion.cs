﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;

namespace FE_FX
{
    public static class cConfiguracion
    {
        public static bool cce { get; set; }

        internal static void cargar()
        {
            cce = false;
            try
            {
                if (bool.Parse(ConfigurationManager.AppSettings["cceHabilitar"].ToString()))
                {
                    cce = true;
                }
            }
            catch
            {

            }

        }
    }
}
