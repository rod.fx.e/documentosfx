﻿//------------------------------------------------------------------------------
// <auto-generated>
//     Este código fue generado por una herramienta.
//     Versión de runtime:4.0.30319.42000
//
//     Los cambios en este archivo podrían causar un comportamiento incorrecto y se perderán si
//     se vuelve a generar el código.
// </auto-generated>
//------------------------------------------------------------------------------
using System.Xml.Serialization;

namespace FE_FX
{
    using System.Xml.Serialization;

    // 
    // This source code was auto-generated by xsd, Version=4.0.30319.33440.
    // 


    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.0.30319.33440")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.sat.gob.mx/esquemas/retencionpago/2")]
    [System.Xml.Serialization.XmlRootAttribute(Namespace = "http://www.sat.gob.mx/esquemas/retencionpago/2", IsNullable = false)]
    public partial class Retenciones
    {

        //14-03-2024 Rodrigo Escalona, Añadir schemaLocation
        [System.Xml.Serialization.XmlAttribute("schemaLocation", Namespace = "http://www.w3.org/2001/XMLSchema-instance")]
        public string schemaLocation = "http://www.sat.gob.mx/esquemas/retencionpago/2 http://www.sat.gob.mx/esquemas/retencionpago/2/retencionpagov2.xsd http://www.sat.gob.mx/esquemas/retencionpago/1/pagosaextranjeros http://www.sat.gob.mx/esquemas/retencionpago/1/pagosaextranjeros/pagosaextranjeros.xsd";


        public RetencionesCfdiRetenRelacionados cfdiRetenRelacionadosField;

        public RetencionesEmisor emisorField;

        public RetencionesReceptor receptorField;

        public RetencionesPeriodo periodoField;

        public RetencionesTotales totalesField;

        public RetencionesComplemento complementoField;

        public RetencionesAddenda addendaField;

        public string versionField;

        public string folioIntField;

        public string selloField;

        public string noCertificadoField;

        public string certificadoField;

        public System.String fechaExpField;

        public string lugarExpRetencField;

        public c_CveRetenc cveRetencField;

        public string descRetencField;

        public Retenciones()
        {
            this.versionField = "2.0";
        }

        /// <remarks/>
        public RetencionesCfdiRetenRelacionados CfdiRetenRelacionados
        {
            get
            {
                return this.cfdiRetenRelacionadosField;
            }
            set
            {
                this.cfdiRetenRelacionadosField = value;
            }
        }

        /// <remarks/>
        public RetencionesEmisor Emisor
        {
            get
            {
                return this.emisorField;
            }
            set
            {
                this.emisorField = value;
            }
        }

        /// <remarks/>
        public RetencionesReceptor Receptor
        {
            get
            {
                return this.receptorField;
            }
            set
            {
                this.receptorField = value;
            }
        }

        /// <remarks/>
        public RetencionesPeriodo Periodo
        {
            get
            {
                return this.periodoField;
            }
            set
            {
                this.periodoField = value;
            }
        }

        /// <remarks/>
        public RetencionesTotales Totales
        {
            get
            {
                return this.totalesField;
            }
            set
            {
                this.totalesField = value;
            }
        }

        /// <remarks/>
        public RetencionesComplemento Complemento
        {
            get
            {
                return this.complementoField;
            }
            set
            {
                this.complementoField = value;
            }
        }

        /// <remarks/>
        public RetencionesAddenda Addenda
        {
            get
            {
                return this.addendaField;
            }
            set
            {
                this.addendaField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string Version
        {
            get
            {
                return this.versionField;
            }
            set
            {
                this.versionField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string FolioInt
        {
            get
            {
                return this.folioIntField;
            }
            set
            {
                this.folioIntField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string Sello
        {
            get
            {
                return this.selloField;
            }
            set
            {
                this.selloField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string NoCertificado
        {
            get
            {
                return this.noCertificadoField;
            }
            set
            {
                this.noCertificadoField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string Certificado
        {
            get
            {
                return this.certificadoField;
            }
            set
            {
                this.certificadoField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public System.String FechaExp
        {
            get
            {
                return this.fechaExpField;
            }
            set
            {
                this.fechaExpField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string LugarExpRetenc
        {
            get
            {
                return this.lugarExpRetencField;
            }
            set
            {
                this.lugarExpRetencField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public c_CveRetenc CveRetenc
        {
            get
            {
                return this.cveRetencField;
            }
            set
            {
                this.cveRetencField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string DescRetenc
        {
            get
            {
                return this.descRetencField;
            }
            set
            {
                this.descRetencField = value;
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.0.30319.33440")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.sat.gob.mx/esquemas/retencionpago/2")]
    public partial class RetencionesCfdiRetenRelacionados
    {

        public string tipoRelacionField;

        public string uUIDField;

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string TipoRelacion
        {
            get
            {
                return this.tipoRelacionField;
            }
            set
            {
                this.tipoRelacionField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string UUID
        {
            get
            {
                return this.uUIDField;
            }
            set
            {
                this.uUIDField = value;
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.0.30319.33440")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.sat.gob.mx/esquemas/retencionpago/2")]
    public partial class RetencionesEmisor
    {

        public string rfcEField;

        public string nomDenRazSocEField;

        public string regimenFiscalEField;

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string RfcE
        {
            get
            {
                return this.rfcEField;
            }
            set
            {
                this.rfcEField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string NomDenRazSocE
        {
            get
            {
                return this.nomDenRazSocEField;
            }
            set
            {
                this.nomDenRazSocEField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string RegimenFiscalE
        {
            get
            {
                return this.regimenFiscalEField;
            }
            set
            {
                this.regimenFiscalEField = value;
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.0.30319.33440")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.sat.gob.mx/esquemas/retencionpago/2")]
    public partial class RetencionesReceptor
    {

        public object itemField;

        public RetencionesReceptorNacionalidadR nacionalidadRField;

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("Extranjero", typeof(RetencionesReceptorExtranjero))]
        [System.Xml.Serialization.XmlElementAttribute("Nacional", typeof(RetencionesReceptorNacional))]
        public object Item
        {
            get
            {
                return this.itemField;
            }
            set
            {
                this.itemField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public RetencionesReceptorNacionalidadR NacionalidadR
        {
            get
            {
                return this.nacionalidadRField;
            }
            set
            {
                this.nacionalidadRField = value;
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.0.30319.33440")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.sat.gob.mx/esquemas/retencionpago/2")]
    public partial class RetencionesReceptorExtranjero
    {

        public string numRegIdTribRField;

        public string nomDenRazSocRField;

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string NumRegIdTribR
        {
            get
            {
                return this.numRegIdTribRField;
            }
            set
            {
                this.numRegIdTribRField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string NomDenRazSocR
        {
            get
            {
                return this.nomDenRazSocRField;
            }
            set
            {
                this.nomDenRazSocRField = value;
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.0.30319.33440")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.sat.gob.mx/esquemas/retencionpago/2")]
    public partial class RetencionesReceptorNacional
    {

        public string rfcRField;

        public string nomDenRazSocRField;

        public string curpRField;

        public string domicilioFiscalRField;

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string RfcR
        {
            get
            {
                return this.rfcRField;
            }
            set
            {
                this.rfcRField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string NomDenRazSocR
        {
            get
            {
                return this.nomDenRazSocRField;
            }
            set
            {
                this.nomDenRazSocRField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string CurpR
        {
            get
            {
                return this.curpRField;
            }
            set
            {
                this.curpRField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string DomicilioFiscalR
        {
            get
            {
                return this.domicilioFiscalRField;
            }
            set
            {
                this.domicilioFiscalRField = value;
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.0.30319.33440")]
    [System.SerializableAttribute()]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.sat.gob.mx/esquemas/retencionpago/2")]
    public enum RetencionesReceptorNacionalidadR
    {

        /// <remarks/>
        Nacional,

        /// <remarks/>
        Extranjero,
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.0.30319.33440")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.sat.gob.mx/esquemas/retencionpago/2")]
    public partial class RetencionesPeriodo
    {

        public string mesIniField;

        public string mesFinField;

        public string ejercicioField;

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string MesIni
        {
            get
            {
                return this.mesIniField;
            }
            set
            {
                this.mesIniField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string MesFin
        {
            get
            {
                return this.mesFinField;
            }
            set
            {
                this.mesFinField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string Ejercicio
        {
            get
            {
                return this.ejercicioField;
            }
            set
            {
                this.ejercicioField = value;
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.0.30319.33440")]
    [System.SerializableAttribute()]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://www.sat.gob.mx/esquemas/retencionpago/1/catalogos")]
    public enum c_Periodo
    {

        /// <remarks/>
        [System.Xml.Serialization.XmlEnumAttribute("01")]
        Item01,

        /// <remarks/>
        [System.Xml.Serialization.XmlEnumAttribute("02")]
        Item02,

        /// <remarks/>
        [System.Xml.Serialization.XmlEnumAttribute("03")]
        Item03,

        /// <remarks/>
        [System.Xml.Serialization.XmlEnumAttribute("04")]
        Item04,

        /// <remarks/>
        [System.Xml.Serialization.XmlEnumAttribute("05")]
        Item05,

        /// <remarks/>
        [System.Xml.Serialization.XmlEnumAttribute("06")]
        Item06,

        /// <remarks/>
        [System.Xml.Serialization.XmlEnumAttribute("07")]
        Item07,

        /// <remarks/>
        [System.Xml.Serialization.XmlEnumAttribute("08")]
        Item08,

        /// <remarks/>
        [System.Xml.Serialization.XmlEnumAttribute("09")]
        Item09,

        /// <remarks/>
        [System.Xml.Serialization.XmlEnumAttribute("10")]
        Item10,

        /// <remarks/>
        [System.Xml.Serialization.XmlEnumAttribute("11")]
        Item11,

        /// <remarks/>
        [System.Xml.Serialization.XmlEnumAttribute("12")]
        Item12,
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.0.30319.33440")]
    [System.SerializableAttribute()]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://www.sat.gob.mx/esquemas/retencionpago/1/catalogos")]
    public enum c_Ejercicio
    {

        /// <remarks/>
        [System.Xml.Serialization.XmlEnumAttribute("2021")]
        Item2021,

        /// <remarks/>
        [System.Xml.Serialization.XmlEnumAttribute("2022")]
        Item2022,

        /// <remarks/>
        [System.Xml.Serialization.XmlEnumAttribute("2023")]
        Item2023,

        /// <remarks/>
        [System.Xml.Serialization.XmlEnumAttribute("2024")]
        Item2024,

        /// <remarks/>
        [System.Xml.Serialization.XmlEnumAttribute("2025")]
        Item2025,

        /// <remarks/>
        [System.Xml.Serialization.XmlEnumAttribute("2026")]
        Item2026,

        /// <remarks/>
        [System.Xml.Serialization.XmlEnumAttribute("2027")]
        Item2027,
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.0.30319.33440")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.sat.gob.mx/esquemas/retencionpago/2")]
    public partial class RetencionesTotales
    {

        public RetencionesTotalesImpRetenidos[] impRetenidosField;

        public decimal montoTotOperacionField;

        public decimal montoTotGravField;

        public decimal montoTotExentField;

        public decimal montoTotRetField;

        public decimal utilidadBimestralField;

        public bool utilidadBimestralFieldSpecified;

        public decimal iSRCorrespondienteField;

        public bool iSRCorrespondienteFieldSpecified;

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("ImpRetenidos")]
        public RetencionesTotalesImpRetenidos[] ImpRetenidos
        {
            get
            {
                return this.impRetenidosField;
            }
            set
            {
                this.impRetenidosField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public decimal MontoTotOperacion
        {
            get
            {
                return this.montoTotOperacionField;
            }
            set
            {
                this.montoTotOperacionField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public decimal MontoTotGrav
        {
            get
            {
                return this.montoTotGravField;
            }
            set
            {
                this.montoTotGravField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public decimal MontoTotExent
        {
            get
            {
                return this.montoTotExentField;
            }
            set
            {
                this.montoTotExentField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public decimal MontoTotRet
        {
            get
            {
                return this.montoTotRetField;
            }
            set
            {
                this.montoTotRetField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public decimal UtilidadBimestral
        {
            get
            {
                return this.utilidadBimestralField;
            }
            set
            {
                this.utilidadBimestralField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool UtilidadBimestralSpecified
        {
            get
            {
                return this.utilidadBimestralFieldSpecified;
            }
            set
            {
                this.utilidadBimestralFieldSpecified = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public decimal ISRCorrespondiente
        {
            get
            {
                return this.iSRCorrespondienteField;
            }
            set
            {
                this.iSRCorrespondienteField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool ISRCorrespondienteSpecified
        {
            get
            {
                return this.iSRCorrespondienteFieldSpecified;
            }
            set
            {
                this.iSRCorrespondienteFieldSpecified = value;
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.0.30319.33440")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.sat.gob.mx/esquemas/retencionpago/2")]
    public partial class RetencionesTotalesImpRetenidos
    {

        public decimal baseRetField;

        public bool baseRetFieldSpecified;

        public string impuestoRetField;

        public decimal montoRetField;

        public c_TipoPagoRet tipoPagoRetField;

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public decimal BaseRet
        {
            get
            {
                return this.baseRetField;
            }
            set
            {
                this.baseRetField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool BaseRetSpecified
        {
            get
            {
                return this.baseRetFieldSpecified;
            }
            set
            {
                this.baseRetFieldSpecified = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string ImpuestoRet
        {
            get
            {
                return this.impuestoRetField;
            }
            set
            {
                this.impuestoRetField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public decimal MontoRet
        {
            get
            {
                return this.montoRetField;
            }
            set
            {
                this.montoRetField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public c_TipoPagoRet TipoPagoRet
        {
            get
            {
                return this.tipoPagoRetField;
            }
            set
            {
                this.tipoPagoRetField = value;
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.0.30319.33440")]
    [System.SerializableAttribute()]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://www.sat.gob.mx/esquemas/retencionpago/1/catalogos")]
    public enum c_TipoPagoRet
    {

        /// <remarks/>
        [System.Xml.Serialization.XmlEnumAttribute("01")]
        Item01,

        /// <remarks/>
        [System.Xml.Serialization.XmlEnumAttribute("02")]
        Item02,

        /// <remarks/>
        [System.Xml.Serialization.XmlEnumAttribute("03")]
        Item03,

        /// <remarks/>
        [System.Xml.Serialization.XmlEnumAttribute("04")]
        Item04,
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.0.30319.33440")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.sat.gob.mx/esquemas/retencionpago/2")]
    public partial class RetencionesComplemento
    {

        public System.Xml.XmlElement[] anyField;

        /// <remarks/>
        [System.Xml.Serialization.XmlAnyElementAttribute()]
        public System.Xml.XmlElement[] Any
        {
            get
            {
                return this.anyField;
            }
            set
            {
                this.anyField = value;
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.0.30319.33440")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.sat.gob.mx/esquemas/retencionpago/2")]
    public partial class RetencionesAddenda
    {

        public System.Xml.XmlElement[] anyField;

        /// <remarks/>
        [System.Xml.Serialization.XmlAnyElementAttribute()]
        public System.Xml.XmlElement[] Any
        {
            get
            {
                return this.anyField;
            }
            set
            {
                this.anyField = value;
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.0.30319.33440")]
    [System.SerializableAttribute()]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://www.sat.gob.mx/esquemas/retencionpago/1/catalogos")]
    public enum c_CveRetenc
    {

        /// <remarks/>
        [System.Xml.Serialization.XmlEnumAttribute("01")]
        Item01,

        /// <remarks/>
        [System.Xml.Serialization.XmlEnumAttribute("02")]
        Item02,

        /// <remarks/>
        [System.Xml.Serialization.XmlEnumAttribute("03")]
        Item03,

        /// <remarks/>
        [System.Xml.Serialization.XmlEnumAttribute("04")]
        Item04,

        /// <remarks/>
        [System.Xml.Serialization.XmlEnumAttribute("05")]
        Item05,

        /// <remarks/>
        [System.Xml.Serialization.XmlEnumAttribute("06")]
        Item06,

        /// <remarks/>
        [System.Xml.Serialization.XmlEnumAttribute("07")]
        Item07,

        /// <remarks/>
        [System.Xml.Serialization.XmlEnumAttribute("08")]
        Item08,

        /// <remarks/>
        [System.Xml.Serialization.XmlEnumAttribute("09")]
        Item09,

        /// <remarks/>
        [System.Xml.Serialization.XmlEnumAttribute("10")]
        Item10,

        /// <remarks/>
        [System.Xml.Serialization.XmlEnumAttribute("11")]
        Item11,

        /// <remarks/>
        [System.Xml.Serialization.XmlEnumAttribute("12")]
        Item12,

        /// <remarks/>
        [System.Xml.Serialization.XmlEnumAttribute("13")]
        Item13,

        /// <remarks/>
        [System.Xml.Serialization.XmlEnumAttribute("14")]
        Item14,

        /// <remarks/>
        [System.Xml.Serialization.XmlEnumAttribute("15")]
        Item15,

        /// <remarks/>
        [System.Xml.Serialization.XmlEnumAttribute("16")]
        Item16,

        /// <remarks/>
        [System.Xml.Serialization.XmlEnumAttribute("17")]
        Item17,

        /// <remarks/>
        [System.Xml.Serialization.XmlEnumAttribute("18")]
        Item18,

        /// <remarks/>
        [System.Xml.Serialization.XmlEnumAttribute("19")]
        Item19,

        /// <remarks/>
        [System.Xml.Serialization.XmlEnumAttribute("20")]
        Item20,

        /// <remarks/>
        [System.Xml.Serialization.XmlEnumAttribute("21")]
        Item21,

        /// <remarks/>
        [System.Xml.Serialization.XmlEnumAttribute("22")]
        Item22,

        /// <remarks/>
        [System.Xml.Serialization.XmlEnumAttribute("23")]
        Item23,

        /// <remarks/>
        [System.Xml.Serialization.XmlEnumAttribute("24")]
        Item24,

        /// <remarks/>
        [System.Xml.Serialization.XmlEnumAttribute("25")]
        Item25,

        /// <remarks/>
        [System.Xml.Serialization.XmlEnumAttribute("26")]
        Item26,

        /// <remarks/>
        [System.Xml.Serialization.XmlEnumAttribute("27")]
        Item27,

        /// <remarks/>
        [System.Xml.Serialization.XmlEnumAttribute("28")]
        Item28,
    }
}
