﻿namespace FE_FX
{
    partial class frmBandejaComprobantePago
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmBandejaComprobantePago));
            this.dtgrdGeneral = new System.Windows.Forms.DataGridView();
            this.toolStripSeparator5 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripSeparator4 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripLabel2 = new System.Windows.Forms.ToolStripLabel();
            this.toolStripLabel1 = new System.Windows.Forms.ToolStripLabel();
            this.toolStripStatusLabel1 = new System.Windows.Forms.ToolStripLabel();
            this.PDF_VER = new System.Windows.Forms.CheckBox();
            this.ribbonControl1 = new DevComponents.DotNetBar.RibbonControl();
            this.ribbonPanel1 = new DevComponents.DotNetBar.RibbonPanel();
            this.label10 = new System.Windows.Forms.Label();
            this.Version = new System.Windows.Forms.ComboBox();
            this.ejecutarAutomatico = new System.Windows.Forms.CheckBox();
            this.button4 = new System.Windows.Forms.Button();
            this.button5 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.INICIO = new System.Windows.Forms.DateTimePicker();
            this.CUSTOMER_NAME = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.FINAL = new System.Windows.Forms.DateTimePicker();
            this.label9 = new System.Windows.Forms.Label();
            this.CHECK_ID = new System.Windows.Forms.TextBox();
            this.estadoFiltro = new System.Windows.Forms.TextBox();
            this.INVOICE_IDs = new System.Windows.Forms.TextBox();
            this.TOPtxt = new System.Windows.Forms.TextBox();
            this.CFDI_PRUEBA = new System.Windows.Forms.CheckBox();
            this.MOSTRAR_PDF_ESTADO = new System.Windows.Forms.CheckBox();
            this.PENDIENTES = new System.Windows.Forms.CheckBox();
            this.buttonFile = new DevComponents.DotNetBar.Office2007StartButton();
            this.menuFileContainer = new DevComponents.DotNetBar.ItemContainer();
            this.buttonItem7 = new DevComponents.DotNetBar.ButtonItem();
            this.buttonItem1 = new DevComponents.DotNetBar.ButtonItem();
            this.buttonItem9 = new DevComponents.DotNetBar.ButtonItem();
            this.buttonItem14 = new DevComponents.DotNetBar.ButtonItem();
            this.menuFileBottomContainer = new DevComponents.DotNetBar.ItemContainer();
            this.buttonExit = new DevComponents.DotNetBar.ButtonItem();
            this.ribbonTabItem1 = new DevComponents.DotNetBar.RibbonTabItem();
            this.informar_procesar = new DevComponents.DotNetBar.LabelItem();
            this.ENTITY_ID = new DevComponents.DotNetBar.ComboBoxItem();
            this.comboItem3 = new DevComponents.Editors.ComboItem();
            this.comboItem4 = new DevComponents.Editors.ComboItem();
            this.EMPRESA_NOMBRE = new DevComponents.DotNetBar.LabelItem();
            this.buttonItem37 = new DevComponents.DotNetBar.ButtonItem();
            this.informacion = new DevComponents.DotNetBar.LabelItem();
            this.ribbonTabItemGroup1 = new DevComponents.DotNetBar.RibbonTabItemGroup();
            this.comboItem1 = new DevComponents.Editors.ComboItem();
            this.comboItem2 = new DevComponents.Editors.ComboItem();
            this.progressBar1 = new System.Windows.Forms.ProgressBar();
            this.controlContainerItem12 = new DevComponents.DotNetBar.ControlContainerItem();
            this.folderBrowserDialog1 = new System.Windows.Forms.FolderBrowserDialog();
            this.oTimer = new System.Windows.Forms.Timer(this.components);
            this.toolStripButton7 = new System.Windows.Forms.ToolStripButton();
            this.styleManager1 = new DevComponents.DotNetBar.StyleManager(this.components);
            this.itemContainer12 = new DevComponents.DotNetBar.ItemContainer();
            this.buttonItem23 = new DevComponents.DotNetBar.ButtonItem();
            this.buttonItem8 = new DevComponents.DotNetBar.ButtonItem();
            this.buttonItem2 = new DevComponents.DotNetBar.ButtonItem();
            this.buttonItem21 = new DevComponents.DotNetBar.ButtonItem();
            this.buttonItem24 = new DevComponents.DotNetBar.ButtonItem();
            this.buttonItem29 = new DevComponents.DotNetBar.ButtonItem();
            this.verToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.pDFToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.xMLToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.enviarPorCorreoPDFYXMLToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.regenerarPDFToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.cancelarToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.exportarToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.cambiarFormaDePagoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.relacionarComplmentoDePagoCanceladoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.generarReporteDeNDCNoAsociadasToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.procesarComplementosAutoajustandoElMontoPagadoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.compararDepositosConComplementoDePagoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.autorelacionarConElMismoPagoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.cFDIAExcelToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.copiarCFDISeleccionadosACarpetaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.generarSinRelacionarNDCToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.generarComprobanteConTCDeCambioToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.rToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.procesarConRegeneraciónDeDatosToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.utilidadesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.migrarCFDIToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.label1 = new System.Windows.Forms.Label();
            this.toolStripButton9 = new System.Windows.Forms.ToolStripButton();
            this.toolStripButton4 = new System.Windows.Forms.ToolStripButton();
            this.btnRefresh = new System.Windows.Forms.ToolStripButton();
            this.toolStripButton3 = new System.Windows.Forms.ToolStripButton();
            this.label7 = new System.Windows.Forms.Label();
            this.nuevoComprobanteDePagoManualToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            ((System.ComponentModel.ISupportInitialize)(this.dtgrdGeneral)).BeginInit();
            this.ribbonControl1.SuspendLayout();
            this.ribbonPanel1.SuspendLayout();
            this.contextMenuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // dtgrdGeneral
            // 
            this.dtgrdGeneral.AllowUserToAddRows = false;
            this.dtgrdGeneral.AllowUserToDeleteRows = false;
            this.dtgrdGeneral.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dtgrdGeneral.BackgroundColor = System.Drawing.Color.White;
            this.dtgrdGeneral.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle4.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle4.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle4.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle4.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dtgrdGeneral.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle4;
            this.dtgrdGeneral.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle5.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle5.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle5.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle5.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle5.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dtgrdGeneral.DefaultCellStyle = dataGridViewCellStyle5;
            this.dtgrdGeneral.Location = new System.Drawing.Point(0, 111);
            this.dtgrdGeneral.Name = "dtgrdGeneral";
            dataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle6.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle6.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle6.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle6.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle6.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle6.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dtgrdGeneral.RowHeadersDefaultCellStyle = dataGridViewCellStyle6;
            this.dtgrdGeneral.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dtgrdGeneral.Size = new System.Drawing.Size(1045, 386);
            this.dtgrdGeneral.TabIndex = 436;
            this.dtgrdGeneral.MouseClick += new System.Windows.Forms.MouseEventHandler(this.dtgrdGeneral_MouseClick);
            // 
            // toolStripSeparator5
            // 
            this.toolStripSeparator5.Name = "toolStripSeparator5";
            this.toolStripSeparator5.Size = new System.Drawing.Size(6, 39);
            // 
            // toolStripSeparator4
            // 
            this.toolStripSeparator4.Name = "toolStripSeparator4";
            this.toolStripSeparator4.Size = new System.Drawing.Size(6, 39);
            // 
            // toolStripLabel2
            // 
            this.toolStripLabel2.Name = "toolStripLabel2";
            this.toolStripLabel2.Size = new System.Drawing.Size(46, 36);
            this.toolStripLabel2.Text = "Factura";
            // 
            // toolStripLabel1
            // 
            this.toolStripLabel1.Name = "toolStripLabel1";
            this.toolStripLabel1.Size = new System.Drawing.Size(148, 36);
            this.toolStripLabel1.Text = "Total de Registros a Cargar";
            // 
            // toolStripStatusLabel1
            // 
            this.toolStripStatusLabel1.Font = new System.Drawing.Font("Arial", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.toolStripStatusLabel1.Name = "toolStripStatusLabel1";
            this.toolStripStatusLabel1.Size = new System.Drawing.Size(0, 36);
            // 
            // PDF_VER
            // 
            this.PDF_VER.AutoSize = true;
            this.PDF_VER.BackColor = System.Drawing.Color.White;
            this.PDF_VER.Location = new System.Drawing.Point(103, 7);
            this.PDF_VER.Name = "PDF_VER";
            this.PDF_VER.Size = new System.Drawing.Size(80, 17);
            this.PDF_VER.TabIndex = 448;
            this.PDF_VER.Text = "Ver el  PDF";
            this.PDF_VER.UseVisualStyleBackColor = false;
            // 
            // ribbonControl1
            // 
            this.ribbonControl1.BackColor = System.Drawing.SystemColors.Control;
            // 
            // 
            // 
            this.ribbonControl1.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.ribbonControl1.CaptionVisible = true;
            this.ribbonControl1.Controls.Add(this.ribbonPanel1);
            this.ribbonControl1.Dock = System.Windows.Forms.DockStyle.Top;
            this.ribbonControl1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ribbonControl1.ForeColor = System.Drawing.Color.Black;
            this.ribbonControl1.Items.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.buttonFile,
            this.ribbonTabItem1,
            this.informar_procesar});
            this.ribbonControl1.KeyTipsFont = new System.Drawing.Font("Tahoma", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ribbonControl1.Location = new System.Drawing.Point(0, 0);
            this.ribbonControl1.MdiSystemItemVisible = false;
            this.ribbonControl1.Name = "ribbonControl1";
            this.ribbonControl1.Padding = new System.Windows.Forms.Padding(0, 0, 0, 2);
            this.ribbonControl1.QuickToolbarItems.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.ENTITY_ID,
            this.EMPRESA_NOMBRE,
            this.buttonItem37,
            this.informacion});
            this.ribbonControl1.Size = new System.Drawing.Size(1045, 112);
            this.ribbonControl1.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.ribbonControl1.SystemText.MaximizeRibbonText = "&Maximize the Ribbon";
            this.ribbonControl1.SystemText.MinimizeRibbonText = "Mi&nimize the Ribbon";
            this.ribbonControl1.SystemText.QatAddItemText = "&Add to Quick Access Toolbar";
            this.ribbonControl1.SystemText.QatCustomizeMenuLabel = "<b>Customize Quick Access Toolbar</b>";
            this.ribbonControl1.SystemText.QatCustomizeText = "&Customize Quick Access Toolbar...";
            this.ribbonControl1.SystemText.QatDialogAddButton = "&Add >>";
            this.ribbonControl1.SystemText.QatDialogCancelButton = "Cancel";
            this.ribbonControl1.SystemText.QatDialogCaption = "Customize Quick Access Toolbar";
            this.ribbonControl1.SystemText.QatDialogCategoriesLabel = "&Choose commands from:";
            this.ribbonControl1.SystemText.QatDialogOkButton = "OK";
            this.ribbonControl1.SystemText.QatDialogPlacementCheckbox = "&Place Quick Access Toolbar below the Ribbon";
            this.ribbonControl1.SystemText.QatDialogRemoveButton = "&Remove";
            this.ribbonControl1.SystemText.QatPlaceAboveRibbonText = "&Place Quick Access Toolbar above the Ribbon";
            this.ribbonControl1.SystemText.QatPlaceBelowRibbonText = "&Place Quick Access Toolbar below the Ribbon";
            this.ribbonControl1.SystemText.QatRemoveItemText = "&Remove from Quick Access Toolbar";
            this.ribbonControl1.TabGroupHeight = 14;
            this.ribbonControl1.TabGroups.AddRange(new DevComponents.DotNetBar.RibbonTabItemGroup[] {
            this.ribbonTabItemGroup1});
            this.ribbonControl1.TabGroupsVisible = true;
            this.ribbonControl1.TabIndex = 473;
            // 
            // ribbonPanel1
            // 
            this.ribbonPanel1.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.ribbonPanel1.Controls.Add(this.label10);
            this.ribbonPanel1.Controls.Add(this.Version);
            this.ribbonPanel1.Controls.Add(this.ejecutarAutomatico);
            this.ribbonPanel1.Controls.Add(this.button4);
            this.ribbonPanel1.Controls.Add(this.button5);
            this.ribbonPanel1.Controls.Add(this.button2);
            this.ribbonPanel1.Controls.Add(this.button1);
            this.ribbonPanel1.Controls.Add(this.label3);
            this.ribbonPanel1.Controls.Add(this.INICIO);
            this.ribbonPanel1.Controls.Add(this.CUSTOMER_NAME);
            this.ribbonPanel1.Controls.Add(this.label5);
            this.ribbonPanel1.Controls.Add(this.label4);
            this.ribbonPanel1.Controls.Add(this.label8);
            this.ribbonPanel1.Controls.Add(this.label2);
            this.ribbonPanel1.Controls.Add(this.label6);
            this.ribbonPanel1.Controls.Add(this.FINAL);
            this.ribbonPanel1.Controls.Add(this.PDF_VER);
            this.ribbonPanel1.Controls.Add(this.label9);
            this.ribbonPanel1.Controls.Add(this.CHECK_ID);
            this.ribbonPanel1.Controls.Add(this.estadoFiltro);
            this.ribbonPanel1.Controls.Add(this.INVOICE_IDs);
            this.ribbonPanel1.Controls.Add(this.TOPtxt);
            this.ribbonPanel1.Controls.Add(this.CFDI_PRUEBA);
            this.ribbonPanel1.Controls.Add(this.MOSTRAR_PDF_ESTADO);
            this.ribbonPanel1.Controls.Add(this.PENDIENTES);
            this.ribbonPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ribbonPanel1.Location = new System.Drawing.Point(0, 55);
            this.ribbonPanel1.Name = "ribbonPanel1";
            this.ribbonPanel1.Padding = new System.Windows.Forms.Padding(3, 0, 3, 3);
            this.ribbonPanel1.Size = new System.Drawing.Size(1045, 55);
            // 
            // 
            // 
            this.ribbonPanel1.Style.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.ribbonPanel1.StyleMouseDown.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.ribbonPanel1.StyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.ribbonPanel1.TabIndex = 1;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.BackColor = System.Drawing.Color.White;
            this.label10.Location = new System.Drawing.Point(191, 33);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(42, 13);
            this.label10.TabIndex = 485;
            this.label10.Text = "Versión";
            // 
            // Version
            // 
            this.Version.AutoCompleteCustomSource.AddRange(new string[] {
            "3.3",
            "4.0"});
            this.Version.Enabled = false;
            this.Version.FormattingEnabled = true;
            this.Version.Items.AddRange(new object[] {
            "3.3",
            "4.0"});
            this.Version.Location = new System.Drawing.Point(239, 28);
            this.Version.Name = "Version";
            this.Version.Size = new System.Drawing.Size(51, 21);
            this.Version.TabIndex = 484;
            this.Version.Text = "4.0";
            // 
            // ejecutarAutomatico
            // 
            this.ejecutarAutomatico.AutoSize = true;
            this.ejecutarAutomatico.Location = new System.Drawing.Point(797, 6);
            this.ejecutarAutomatico.Name = "ejecutarAutomatico";
            this.ejecutarAutomatico.Size = new System.Drawing.Size(128, 17);
            this.ejecutarAutomatico.TabIndex = 482;
            this.ejecutarAutomatico.Text = "Ejecución automatica";
            this.ejecutarAutomatico.UseVisualStyleBackColor = true;
            this.ejecutarAutomatico.CheckedChanged += new System.EventHandler(this.ejecutarAutomatico_CheckedChanged_1);
            // 
            // button4
            // 
            this.button4.BackColor = System.Drawing.Color.ForestGreen;
            this.button4.FlatAppearance.BorderSize = 0;
            this.button4.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button4.ForeColor = System.Drawing.Color.White;
            this.button4.Location = new System.Drawing.Point(945, 2);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(94, 22);
            this.button4.TabIndex = 481;
            this.button4.Text = "Reporte";
            this.button4.UseVisualStyleBackColor = false;
            this.button4.Click += new System.EventHandler(this.button4_Click);
            // 
            // button5
            // 
            this.button5.BackColor = System.Drawing.Color.ForestGreen;
            this.button5.FlatAppearance.BorderSize = 0;
            this.button5.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button5.ForeColor = System.Drawing.Color.White;
            this.button5.Location = new System.Drawing.Point(946, 28);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(94, 22);
            this.button5.TabIndex = 481;
            this.button5.Text = "Detalle";
            this.button5.UseVisualStyleBackColor = false;
            this.button5.Click += new System.EventHandler(this.button5_Click_1);
            // 
            // button2
            // 
            this.button2.BackColor = System.Drawing.Color.Orange;
            this.button2.FlatAppearance.BorderSize = 0;
            this.button2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button2.ForeColor = System.Drawing.Color.White;
            this.button2.Location = new System.Drawing.Point(3, 29);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(94, 22);
            this.button2.TabIndex = 480;
            this.button2.Text = "Procesar";
            this.button2.UseVisualStyleBackColor = false;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // button1
            // 
            this.button1.BackColor = System.Drawing.Color.ForestGreen;
            this.button1.FlatAppearance.BorderSize = 0;
            this.button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button1.ForeColor = System.Drawing.Color.White;
            this.button1.Location = new System.Drawing.Point(3, 3);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(94, 22);
            this.button1.TabIndex = 480;
            this.button1.Text = "Buscar";
            this.button1.UseVisualStyleBackColor = false;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.White;
            this.label3.Location = new System.Drawing.Point(202, 9);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(32, 13);
            this.label3.TabIndex = 440;
            this.label3.Text = "Inicio";
            // 
            // INICIO
            // 
            this.INICIO.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.INICIO.Location = new System.Drawing.Point(239, 5);
            this.INICIO.Name = "INICIO";
            this.INICIO.Size = new System.Drawing.Size(94, 20);
            this.INICIO.TabIndex = 442;
            // 
            // CUSTOMER_NAME
            // 
            this.CUSTOMER_NAME.Location = new System.Drawing.Point(345, 28);
            this.CUSTOMER_NAME.Name = "CUSTOMER_NAME";
            this.CUSTOMER_NAME.Size = new System.Drawing.Size(94, 20);
            this.CUSTOMER_NAME.TabIndex = 474;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.BackColor = System.Drawing.Color.White;
            this.label5.Location = new System.Drawing.Point(296, 32);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(39, 13);
            this.label5.TabIndex = 474;
            this.label5.Text = "Cliente";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.BackColor = System.Drawing.Color.White;
            this.label4.Location = new System.Drawing.Point(338, 9);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(29, 13);
            this.label4.TabIndex = 441;
            this.label4.Text = "Final";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.BackColor = System.Drawing.Color.White;
            this.label8.Location = new System.Drawing.Point(764, 31);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(40, 13);
            this.label8.TabIndex = 473;
            this.label8.Text = "Estado";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.White;
            this.label2.Location = new System.Drawing.Point(578, 31);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(48, 13);
            this.label2.TabIndex = 473;
            this.label2.Text = "Facturas";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.BackColor = System.Drawing.Color.White;
            this.label6.Location = new System.Drawing.Point(444, 32);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(32, 13);
            this.label6.TabIndex = 473;
            this.label6.Text = "Pago";
            // 
            // FINAL
            // 
            this.FINAL.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.FINAL.Location = new System.Drawing.Point(372, 5);
            this.FINAL.Name = "FINAL";
            this.FINAL.Size = new System.Drawing.Size(94, 20);
            this.FINAL.TabIndex = 443;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.BackColor = System.Drawing.Color.White;
            this.label9.Location = new System.Drawing.Point(469, 9);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(14, 13);
            this.label9.TabIndex = 475;
            this.label9.Text = "#";
            // 
            // CHECK_ID
            // 
            this.CHECK_ID.Location = new System.Drawing.Point(478, 28);
            this.CHECK_ID.Name = "CHECK_ID";
            this.CHECK_ID.Size = new System.Drawing.Size(94, 20);
            this.CHECK_ID.TabIndex = 473;
            // 
            // estadoFiltro
            // 
            this.estadoFiltro.Location = new System.Drawing.Point(810, 28);
            this.estadoFiltro.Name = "estadoFiltro";
            this.estadoFiltro.Size = new System.Drawing.Size(115, 20);
            this.estadoFiltro.TabIndex = 473;
            // 
            // INVOICE_IDs
            // 
            this.INVOICE_IDs.Location = new System.Drawing.Point(632, 28);
            this.INVOICE_IDs.Name = "INVOICE_IDs";
            this.INVOICE_IDs.Size = new System.Drawing.Size(128, 20);
            this.INVOICE_IDs.TabIndex = 473;
            // 
            // TOPtxt
            // 
            this.TOPtxt.Location = new System.Drawing.Point(488, 5);
            this.TOPtxt.Name = "TOPtxt";
            this.TOPtxt.Size = new System.Drawing.Size(45, 20);
            this.TOPtxt.TabIndex = 474;
            this.TOPtxt.Text = "100";
            // 
            // CFDI_PRUEBA
            // 
            this.CFDI_PRUEBA.AutoSize = true;
            this.CFDI_PRUEBA.BackColor = System.Drawing.Color.White;
            this.CFDI_PRUEBA.Checked = true;
            this.CFDI_PRUEBA.CheckState = System.Windows.Forms.CheckState.Checked;
            this.CFDI_PRUEBA.Location = new System.Drawing.Point(103, 33);
            this.CFDI_PRUEBA.Name = "CFDI_PRUEBA";
            this.CFDI_PRUEBA.Size = new System.Drawing.Size(74, 17);
            this.CFDI_PRUEBA.TabIndex = 474;
            this.CFDI_PRUEBA.Text = "CFDI Test";
            this.CFDI_PRUEBA.UseVisualStyleBackColor = false;
            // 
            // MOSTRAR_PDF_ESTADO
            // 
            this.MOSTRAR_PDF_ESTADO.AutoSize = true;
            this.MOSTRAR_PDF_ESTADO.BackColor = System.Drawing.Color.White;
            this.MOSTRAR_PDF_ESTADO.Location = new System.Drawing.Point(627, 7);
            this.MOSTRAR_PDF_ESTADO.Name = "MOSTRAR_PDF_ESTADO";
            this.MOSTRAR_PDF_ESTADO.Size = new System.Drawing.Size(147, 17);
            this.MOSTRAR_PDF_ESTADO.TabIndex = 478;
            this.MOSTRAR_PDF_ESTADO.Text = "Mostrar los PDF con Error";
            this.MOSTRAR_PDF_ESTADO.UseVisualStyleBackColor = false;
            this.MOSTRAR_PDF_ESTADO.Visible = false;
            // 
            // PENDIENTES
            // 
            this.PENDIENTES.AutoSize = true;
            this.PENDIENTES.BackColor = System.Drawing.Color.White;
            this.PENDIENTES.Checked = true;
            this.PENDIENTES.CheckState = System.Windows.Forms.CheckState.Checked;
            this.PENDIENTES.Location = new System.Drawing.Point(542, 7);
            this.PENDIENTES.Name = "PENDIENTES";
            this.PENDIENTES.Size = new System.Drawing.Size(79, 17);
            this.PENDIENTES.TabIndex = 474;
            this.PENDIENTES.Text = "Pendientes";
            this.PENDIENTES.UseVisualStyleBackColor = false;
            // 
            // buttonFile
            // 
            this.buttonFile.AutoExpandOnClick = true;
            this.buttonFile.CanCustomize = false;
            this.buttonFile.HotTrackingStyle = DevComponents.DotNetBar.eHotTrackingStyle.Image;
            this.buttonFile.Image = global::FE_FX.Properties.Resources.favicon_96x96;
            this.buttonFile.ImageFixedSize = new System.Drawing.Size(16, 16);
            this.buttonFile.ImagePaddingHorizontal = 0;
            this.buttonFile.ImagePaddingVertical = 1;
            this.buttonFile.Name = "buttonFile";
            this.buttonFile.PopupWidth = 400;
            this.buttonFile.ShowSubItems = false;
            this.buttonFile.SubItems.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.menuFileContainer,
            this.menuFileBottomContainer});
            this.buttonFile.Text = "Comprobante de Pago FX";
            // 
            // menuFileContainer
            // 
            // 
            // 
            // 
            this.menuFileContainer.BackgroundStyle.Class = "RibbonFileMenuContainer";
            this.menuFileContainer.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.menuFileContainer.LayoutOrientation = DevComponents.DotNetBar.eOrientation.Vertical;
            this.menuFileContainer.Name = "menuFileContainer";
            this.menuFileContainer.SubItems.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.buttonItem7,
            this.buttonItem1,
            this.buttonItem9,
            this.buttonItem14});
            // 
            // 
            // 
            this.menuFileContainer.TitleStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // buttonItem7
            // 
            this.buttonItem7.Name = "buttonItem7";
            this.buttonItem7.Text = "Series";
            this.buttonItem7.Click += new System.EventHandler(this.buttonItem7_Click);
            // 
            // buttonItem1
            // 
            this.buttonItem1.Name = "buttonItem1";
            this.buttonItem1.Text = "Empresas";
            this.buttonItem1.Click += new System.EventHandler(this.buttonItem1_Click_1);
            // 
            // buttonItem9
            // 
            this.buttonItem9.Name = "buttonItem9";
            this.buttonItem9.Text = "Certificados";
            this.buttonItem9.Click += new System.EventHandler(this.buttonItem9_Click_1);
            // 
            // buttonItem14
            // 
            this.buttonItem14.Name = "buttonItem14";
            this.buttonItem14.Text = "Formatos";
            this.buttonItem14.Click += new System.EventHandler(this.buttonItem14_Click_1);
            // 
            // menuFileBottomContainer
            // 
            // 
            // 
            // 
            this.menuFileBottomContainer.BackgroundStyle.Class = "RibbonFileMenuBottomContainer";
            this.menuFileBottomContainer.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.menuFileBottomContainer.HorizontalItemAlignment = DevComponents.DotNetBar.eHorizontalItemsAlignment.Right;
            this.menuFileBottomContainer.Name = "menuFileBottomContainer";
            this.menuFileBottomContainer.SubItems.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.buttonExit});
            // 
            // 
            // 
            this.menuFileBottomContainer.TitleStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // buttonExit
            // 
            this.buttonExit.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.buttonExit.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.buttonExit.Image = ((System.Drawing.Image)(resources.GetObject("buttonExit.Image")));
            this.buttonExit.Name = "buttonExit";
            this.buttonExit.SubItemsExpandWidth = 24;
            this.buttonExit.Text = "&Salir";
            this.buttonExit.Click += new System.EventHandler(this.buttonExit_Click);
            // 
            // ribbonTabItem1
            // 
            this.ribbonTabItem1.Checked = true;
            this.ribbonTabItem1.Name = "ribbonTabItem1";
            this.ribbonTabItem1.Panel = this.ribbonPanel1;
            this.ribbonTabItem1.Text = "&Busqueda";
            // 
            // informar_procesar
            // 
            this.informar_procesar.Name = "informar_procesar";
            // 
            // ENTITY_ID
            // 
            this.ENTITY_ID.ComboWidth = 60;
            this.ENTITY_ID.DropDownHeight = 106;
            this.ENTITY_ID.DropDownWidth = 60;
            this.ENTITY_ID.ItemHeight = 17;
            this.ENTITY_ID.Items.AddRange(new object[] {
            this.comboItem3,
            this.comboItem4});
            this.ENTITY_ID.Name = "ENTITY_ID";
            this.ENTITY_ID.SelectedIndexChanged += new System.EventHandler(this.ENTITY_ID_SelectedIndexChanged);
            // 
            // comboItem3
            // 
            this.comboItem3.Text = "SWISSMEX";
            // 
            // comboItem4
            // 
            this.comboItem4.Text = "WIDEMEX";
            // 
            // EMPRESA_NOMBRE
            // 
            this.EMPRESA_NOMBRE.Name = "EMPRESA_NOMBRE";
            this.EMPRESA_NOMBRE.Text = "NOMBRE DE LA EMPRESA";
            // 
            // buttonItem37
            // 
            this.buttonItem37.Name = "buttonItem37";
            this.buttonItem37.Tooltip = "Respaldo de Base de Datos";
            // 
            // informacion
            // 
            this.informacion.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.informacion.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.informacion.Name = "informacion";
            this.informacion.Width = 300;
            // 
            // ribbonTabItemGroup1
            // 
            this.ribbonTabItemGroup1.Color = DevComponents.DotNetBar.eRibbonTabGroupColor.Orange;
            this.ribbonTabItemGroup1.GroupTitle = "Tab Group";
            this.ribbonTabItemGroup1.Name = "ribbonTabItemGroup1";
            // 
            // 
            // 
            this.ribbonTabItemGroup1.Style.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(240)))), ((int)(((byte)(158)))), ((int)(((byte)(159)))));
            this.ribbonTabItemGroup1.Style.BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(249)))), ((int)(((byte)(225)))), ((int)(((byte)(226)))));
            this.ribbonTabItemGroup1.Style.BackColorGradientAngle = 90;
            this.ribbonTabItemGroup1.Style.BorderBottom = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.ribbonTabItemGroup1.Style.BorderBottomWidth = 1;
            this.ribbonTabItemGroup1.Style.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(154)))), ((int)(((byte)(58)))), ((int)(((byte)(59)))));
            this.ribbonTabItemGroup1.Style.BorderLeft = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.ribbonTabItemGroup1.Style.BorderLeftWidth = 1;
            this.ribbonTabItemGroup1.Style.BorderRight = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.ribbonTabItemGroup1.Style.BorderRightWidth = 1;
            this.ribbonTabItemGroup1.Style.BorderTop = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.ribbonTabItemGroup1.Style.BorderTopWidth = 1;
            this.ribbonTabItemGroup1.Style.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.ribbonTabItemGroup1.Style.TextAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Center;
            this.ribbonTabItemGroup1.Style.TextColor = System.Drawing.Color.Black;
            this.ribbonTabItemGroup1.Style.TextLineAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Near;
            // 
            // comboItem1
            // 
            this.comboItem1.Text = "SWISSMEX";
            // 
            // comboItem2
            // 
            this.comboItem2.Text = "WIDEMEX";
            // 
            // progressBar1
            // 
            this.progressBar1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.progressBar1.Location = new System.Drawing.Point(0, 111);
            this.progressBar1.Name = "progressBar1";
            this.progressBar1.Size = new System.Drawing.Size(1045, 18);
            this.progressBar1.Style = System.Windows.Forms.ProgressBarStyle.Continuous;
            this.progressBar1.TabIndex = 477;
            // 
            // controlContainerItem12
            // 
            this.controlContainerItem12.AllowItemResize = false;
            this.controlContainerItem12.MenuVisibility = DevComponents.DotNetBar.eMenuVisibility.VisibleAlways;
            this.controlContainerItem12.Name = "controlContainerItem12";
            // 
            // oTimer
            // 
            this.oTimer.Interval = 5000;
            this.oTimer.Tick += new System.EventHandler(this.oTimer_Tick);
            // 
            // toolStripButton7
            // 
            this.toolStripButton7.Name = "toolStripButton7";
            this.toolStripButton7.Size = new System.Drawing.Size(23, 23);
            // 
            // styleManager1
            // 
            this.styleManager1.ManagerStyle = DevComponents.DotNetBar.eStyle.Metro;
            this.styleManager1.MetroColorParameters = new DevComponents.DotNetBar.Metro.ColorTables.MetroColorGeneratorParameters(System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255))))), System.Drawing.Color.FromArgb(((int)(((byte)(106)))), ((int)(((byte)(56)))), ((int)(((byte)(137))))));
            // 
            // itemContainer12
            // 
            // 
            // 
            // 
            this.itemContainer12.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.itemContainer12.LayoutOrientation = DevComponents.DotNetBar.eOrientation.Vertical;
            this.itemContainer12.Name = "itemContainer12";
            this.itemContainer12.SubItems.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.buttonItem23,
            this.buttonItem8,
            this.buttonItem2,
            this.buttonItem29});
            // 
            // 
            // 
            this.itemContainer12.TitleStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // buttonItem23
            // 
            this.buttonItem23.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.buttonItem23.Image = ((System.Drawing.Image)(resources.GetObject("buttonItem23.Image")));
            this.buttonItem23.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top;
            this.buttonItem23.Name = "buttonItem23";
            this.buttonItem23.Text = "Soporte";
            this.buttonItem23.Click += new System.EventHandler(this.buttonItem23_Click);
            // 
            // buttonItem8
            // 
            this.buttonItem8.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.buttonItem8.Image = ((System.Drawing.Image)(resources.GetObject("buttonItem8.Image")));
            this.buttonItem8.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top;
            this.buttonItem8.Name = "buttonItem8";
            this.buttonItem8.Shortcuts.Add(DevComponents.DotNetBar.eShortcut.CtrlF);
            this.buttonItem8.Text = "&Actualizar";
            this.buttonItem8.Click += new System.EventHandler(this.buttonItem8_Click);
            // 
            // buttonItem2
            // 
            this.buttonItem2.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.buttonItem2.Image = ((System.Drawing.Image)(resources.GetObject("buttonItem2.Image")));
            this.buttonItem2.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top;
            this.buttonItem2.Name = "buttonItem2";
            this.buttonItem2.Shortcuts.Add(DevComponents.DotNetBar.eShortcut.CtrlF);
            this.buttonItem2.SubItems.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.buttonItem21,
            this.buttonItem24});
            this.buttonItem2.Text = "&Procesar";
            this.buttonItem2.Click += new System.EventHandler(this.buttonItem2_Click_1);
            // 
            // buttonItem21
            // 
            this.buttonItem21.Name = "buttonItem21";
            this.buttonItem21.Text = "Versión 3.3";
            this.buttonItem21.Click += new System.EventHandler(this.buttonItem21_Click_1);
            // 
            // buttonItem24
            // 
            this.buttonItem24.Name = "buttonItem24";
            this.buttonItem24.Text = "Versión 3.2";
            this.buttonItem24.Click += new System.EventHandler(this.buttonItem24_Click);
            // 
            // buttonItem29
            // 
            this.buttonItem29.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.buttonItem29.Image = ((System.Drawing.Image)(resources.GetObject("buttonItem29.Image")));
            this.buttonItem29.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top;
            this.buttonItem29.Name = "buttonItem29";
            this.buttonItem29.Text = "Manual";
            this.buttonItem29.Click += new System.EventHandler(this.buttonItem29_Click);
            // 
            // verToolStripMenuItem
            // 
            this.verToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.pDFToolStripMenuItem,
            this.xMLToolStripMenuItem});
            this.verToolStripMenuItem.Name = "verToolStripMenuItem";
            this.verToolStripMenuItem.Size = new System.Drawing.Size(381, 26);
            this.verToolStripMenuItem.Text = "Ver";
            // 
            // pDFToolStripMenuItem
            // 
            this.pDFToolStripMenuItem.Name = "pDFToolStripMenuItem";
            this.pDFToolStripMenuItem.Size = new System.Drawing.Size(98, 22);
            this.pDFToolStripMenuItem.Text = "PDF";
            this.pDFToolStripMenuItem.Click += new System.EventHandler(this.pDFToolStripMenuItem_Click);
            // 
            // xMLToolStripMenuItem
            // 
            this.xMLToolStripMenuItem.Name = "xMLToolStripMenuItem";
            this.xMLToolStripMenuItem.Size = new System.Drawing.Size(98, 22);
            this.xMLToolStripMenuItem.Text = "XML";
            this.xMLToolStripMenuItem.Click += new System.EventHandler(this.xMLToolStripMenuItem_Click);
            // 
            // enviarPorCorreoPDFYXMLToolStripMenuItem
            // 
            this.enviarPorCorreoPDFYXMLToolStripMenuItem.Name = "enviarPorCorreoPDFYXMLToolStripMenuItem";
            this.enviarPorCorreoPDFYXMLToolStripMenuItem.Size = new System.Drawing.Size(381, 26);
            this.enviarPorCorreoPDFYXMLToolStripMenuItem.Text = "Enviar por correo PDF y XML";
            this.enviarPorCorreoPDFYXMLToolStripMenuItem.Click += new System.EventHandler(this.enviarPorCorreoPDFYXMLToolStripMenuItem_Click);
            // 
            // regenerarPDFToolStripMenuItem
            // 
            this.regenerarPDFToolStripMenuItem.Name = "regenerarPDFToolStripMenuItem";
            this.regenerarPDFToolStripMenuItem.Size = new System.Drawing.Size(381, 26);
            this.regenerarPDFToolStripMenuItem.Text = "Regenerar PDF";
            this.regenerarPDFToolStripMenuItem.Click += new System.EventHandler(this.regenerarPDFToolStripMenuItem_Click);
            // 
            // cancelarToolStripMenuItem
            // 
            this.cancelarToolStripMenuItem.Name = "cancelarToolStripMenuItem";
            this.cancelarToolStripMenuItem.Size = new System.Drawing.Size(381, 26);
            this.cancelarToolStripMenuItem.Text = "Cancelar";
            this.cancelarToolStripMenuItem.Click += new System.EventHandler(this.cancelarToolStripMenuItem_Click);
            // 
            // exportarToolStripMenuItem
            // 
            this.exportarToolStripMenuItem.Name = "exportarToolStripMenuItem";
            this.exportarToolStripMenuItem.Size = new System.Drawing.Size(381, 26);
            this.exportarToolStripMenuItem.Text = "Exportar";
            this.exportarToolStripMenuItem.Click += new System.EventHandler(this.exportarToolStripMenuItem_Click);
            // 
            // contextMenuStrip1
            // 
            this.contextMenuStrip1.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.contextMenuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.cambiarFormaDePagoToolStripMenuItem,
            this.verToolStripMenuItem,
            this.enviarPorCorreoPDFYXMLToolStripMenuItem,
            this.regenerarPDFToolStripMenuItem,
            this.cancelarToolStripMenuItem,
            this.exportarToolStripMenuItem,
            this.relacionarComplmentoDePagoCanceladoToolStripMenuItem,
            this.generarReporteDeNDCNoAsociadasToolStripMenuItem,
            this.procesarComplementosAutoajustandoElMontoPagadoToolStripMenuItem,
            this.compararDepositosConComplementoDePagoToolStripMenuItem,
            this.autorelacionarConElMismoPagoToolStripMenuItem,
            this.cFDIAExcelToolStripMenuItem,
            this.copiarCFDISeleccionadosACarpetaToolStripMenuItem,
            this.generarSinRelacionarNDCToolStripMenuItem,
            this.generarComprobanteConTCDeCambioToolStripMenuItem,
            this.rToolStripMenuItem,
            this.procesarConRegeneraciónDeDatosToolStripMenuItem,
            this.utilidadesToolStripMenuItem,
            this.nuevoComprobanteDePagoManualToolStripMenuItem});
            this.contextMenuStrip1.Name = "contextMenuStrip1";
            this.contextMenuStrip1.Size = new System.Drawing.Size(382, 520);
            // 
            // cambiarFormaDePagoToolStripMenuItem
            // 
            this.cambiarFormaDePagoToolStripMenuItem.Name = "cambiarFormaDePagoToolStripMenuItem";
            this.cambiarFormaDePagoToolStripMenuItem.Size = new System.Drawing.Size(381, 26);
            this.cambiarFormaDePagoToolStripMenuItem.Text = "Editar";
            this.cambiarFormaDePagoToolStripMenuItem.Click += new System.EventHandler(this.cambiarFormaDePagoToolStripMenuItem_Click);
            // 
            // relacionarComplmentoDePagoCanceladoToolStripMenuItem
            // 
            this.relacionarComplmentoDePagoCanceladoToolStripMenuItem.Name = "relacionarComplmentoDePagoCanceladoToolStripMenuItem";
            this.relacionarComplmentoDePagoCanceladoToolStripMenuItem.Size = new System.Drawing.Size(381, 26);
            this.relacionarComplmentoDePagoCanceladoToolStripMenuItem.Text = "Relacionar Complemento de Pago";
            this.relacionarComplmentoDePagoCanceladoToolStripMenuItem.Click += new System.EventHandler(this.relacionarComplmentoDePagoCanceladoToolStripMenuItem_Click);
            // 
            // generarReporteDeNDCNoAsociadasToolStripMenuItem
            // 
            this.generarReporteDeNDCNoAsociadasToolStripMenuItem.Name = "generarReporteDeNDCNoAsociadasToolStripMenuItem";
            this.generarReporteDeNDCNoAsociadasToolStripMenuItem.Size = new System.Drawing.Size(381, 26);
            this.generarReporteDeNDCNoAsociadasToolStripMenuItem.Text = "Generar reporte de NDC no asociadas";
            this.generarReporteDeNDCNoAsociadasToolStripMenuItem.Click += new System.EventHandler(this.generarReporteDeNDCNoAsociadasToolStripMenuItem_Click);
            // 
            // procesarComplementosAutoajustandoElMontoPagadoToolStripMenuItem
            // 
            this.procesarComplementosAutoajustandoElMontoPagadoToolStripMenuItem.Name = "procesarComplementosAutoajustandoElMontoPagadoToolStripMenuItem";
            this.procesarComplementosAutoajustandoElMontoPagadoToolStripMenuItem.Size = new System.Drawing.Size(381, 26);
            this.procesarComplementosAutoajustandoElMontoPagadoToolStripMenuItem.Text = "Procesar Complementos autoajustando el Monto pagado";
            this.procesarComplementosAutoajustandoElMontoPagadoToolStripMenuItem.Click += new System.EventHandler(this.procesarComplementosAutoajustandoElMontoPagadoToolStripMenuItem_Click);
            // 
            // compararDepositosConComplementoDePagoToolStripMenuItem
            // 
            this.compararDepositosConComplementoDePagoToolStripMenuItem.Name = "compararDepositosConComplementoDePagoToolStripMenuItem";
            this.compararDepositosConComplementoDePagoToolStripMenuItem.Size = new System.Drawing.Size(381, 26);
            this.compararDepositosConComplementoDePagoToolStripMenuItem.Text = "Comparación CFDI";
            this.compararDepositosConComplementoDePagoToolStripMenuItem.Click += new System.EventHandler(this.compararDepositosConComplementoDePagoToolStripMenuItem_Click);
            // 
            // autorelacionarConElMismoPagoToolStripMenuItem
            // 
            this.autorelacionarConElMismoPagoToolStripMenuItem.Name = "autorelacionarConElMismoPagoToolStripMenuItem";
            this.autorelacionarConElMismoPagoToolStripMenuItem.Size = new System.Drawing.Size(381, 26);
            this.autorelacionarConElMismoPagoToolStripMenuItem.Text = "Autorelacionar con el mismo pago";
            this.autorelacionarConElMismoPagoToolStripMenuItem.Click += new System.EventHandler(this.autorelacionarConElMismoPagoToolStripMenuItem_Click);
            // 
            // cFDIAExcelToolStripMenuItem
            // 
            this.cFDIAExcelToolStripMenuItem.Name = "cFDIAExcelToolStripMenuItem";
            this.cFDIAExcelToolStripMenuItem.Size = new System.Drawing.Size(381, 26);
            this.cFDIAExcelToolStripMenuItem.Text = "CFDI a Excel";
            this.cFDIAExcelToolStripMenuItem.Click += new System.EventHandler(this.cFDIAExcelToolStripMenuItem_Click);
            // 
            // copiarCFDISeleccionadosACarpetaToolStripMenuItem
            // 
            this.copiarCFDISeleccionadosACarpetaToolStripMenuItem.Name = "copiarCFDISeleccionadosACarpetaToolStripMenuItem";
            this.copiarCFDISeleccionadosACarpetaToolStripMenuItem.Size = new System.Drawing.Size(381, 26);
            this.copiarCFDISeleccionadosACarpetaToolStripMenuItem.Text = "Copiar CFDI seleccionados a carpeta";
            this.copiarCFDISeleccionadosACarpetaToolStripMenuItem.Click += new System.EventHandler(this.copiarCFDISeleccionadosACarpetaToolStripMenuItem_Click);
            // 
            // generarSinRelacionarNDCToolStripMenuItem
            // 
            this.generarSinRelacionarNDCToolStripMenuItem.Name = "generarSinRelacionarNDCToolStripMenuItem";
            this.generarSinRelacionarNDCToolStripMenuItem.Size = new System.Drawing.Size(381, 26);
            this.generarSinRelacionarNDCToolStripMenuItem.Text = "Generar omitiendo validación de  NDC";
            this.generarSinRelacionarNDCToolStripMenuItem.Click += new System.EventHandler(this.generarSinRelacionarNDCToolStripMenuItem_Click);
            // 
            // generarComprobanteConTCDeCambioToolStripMenuItem
            // 
            this.generarComprobanteConTCDeCambioToolStripMenuItem.BackColor = System.Drawing.Color.LightSalmon;
            this.generarComprobanteConTCDeCambioToolStripMenuItem.Name = "generarComprobanteConTCDeCambioToolStripMenuItem";
            this.generarComprobanteConTCDeCambioToolStripMenuItem.Size = new System.Drawing.Size(381, 26);
            this.generarComprobanteConTCDeCambioToolStripMenuItem.Text = "Generar Comprobante con TC de Cambio";
            this.generarComprobanteConTCDeCambioToolStripMenuItem.Visible = false;
            this.generarComprobanteConTCDeCambioToolStripMenuItem.Click += new System.EventHandler(this.GenerarComprobanteConTCDeCambioToolStripMenuItem_Click);
            // 
            // rToolStripMenuItem
            // 
            this.rToolStripMenuItem.BackColor = System.Drawing.Color.Khaki;
            this.rToolStripMenuItem.Name = "rToolStripMenuItem";
            this.rToolStripMenuItem.Size = new System.Drawing.Size(381, 26);
            this.rToolStripMenuItem.Text = "Reactivar Pago";
            this.rToolStripMenuItem.Click += new System.EventHandler(this.rToolStripMenuItem_Click);
            // 
            // procesarConRegeneraciónDeDatosToolStripMenuItem
            // 
            this.procesarConRegeneraciónDeDatosToolStripMenuItem.Name = "procesarConRegeneraciónDeDatosToolStripMenuItem";
            this.procesarConRegeneraciónDeDatosToolStripMenuItem.Size = new System.Drawing.Size(381, 26);
            this.procesarConRegeneraciónDeDatosToolStripMenuItem.Text = "Procesar con regeneración de datos";
            this.procesarConRegeneraciónDeDatosToolStripMenuItem.Click += new System.EventHandler(this.procesarConRegeneraciónDeDatosToolStripMenuItem_Click);
            // 
            // utilidadesToolStripMenuItem
            // 
            this.utilidadesToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.migrarCFDIToolStripMenuItem});
            this.utilidadesToolStripMenuItem.Name = "utilidadesToolStripMenuItem";
            this.utilidadesToolStripMenuItem.Size = new System.Drawing.Size(381, 26);
            this.utilidadesToolStripMenuItem.Text = "Utilidades";
            // 
            // migrarCFDIToolStripMenuItem
            // 
            this.migrarCFDIToolStripMenuItem.Name = "migrarCFDIToolStripMenuItem";
            this.migrarCFDIToolStripMenuItem.Size = new System.Drawing.Size(137, 22);
            this.migrarCFDIToolStripMenuItem.Text = "Migrar CFDI";
            this.migrarCFDIToolStripMenuItem.Click += new System.EventHandler(this.migrarCFDIToolStripMenuItem_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(136, 36);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(225, 13);
            this.label1.TabIndex = 479;
            this.label1.Text = "Presione click derecho para ver más opciones";
            // 
            // toolStripButton9
            // 
            this.toolStripButton9.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButton9.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton9.Image")));
            this.toolStripButton9.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton9.Name = "toolStripButton9";
            this.toolStripButton9.Size = new System.Drawing.Size(36, 36);
            this.toolStripButton9.Text = "Abrir PDF";
            this.toolStripButton9.Click += new System.EventHandler(this.toolStripButton9_Click);
            // 
            // toolStripButton4
            // 
            this.toolStripButton4.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButton4.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton4.Image")));
            this.toolStripButton4.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton4.Name = "toolStripButton4";
            this.toolStripButton4.Size = new System.Drawing.Size(36, 36);
            this.toolStripButton4.Text = "Ingresar XML para enviar a EDICOM";
            this.toolStripButton4.Visible = false;
            // 
            // btnRefresh
            // 
            this.btnRefresh.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnRefresh.Image = ((System.Drawing.Image)(resources.GetObject("btnRefresh.Image")));
            this.btnRefresh.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnRefresh.Name = "btnRefresh";
            this.btnRefresh.Size = new System.Drawing.Size(36, 36);
            this.btnRefresh.Text = "Cargar";
            this.btnRefresh.Click += new System.EventHandler(this.btnRefresh_Click);
            // 
            // toolStripButton3
            // 
            this.toolStripButton3.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButton3.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton3.Image")));
            this.toolStripButton3.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton3.Name = "toolStripButton3";
            this.toolStripButton3.Size = new System.Drawing.Size(36, 36);
            this.toolStripButton3.Text = "Ver XML Original";
            this.toolStripButton3.Click += new System.EventHandler(this.toolStripButton3_Click);
            // 
            // label7
            // 
            this.label7.Location = new System.Drawing.Point(449, 36);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(476, 13);
            this.label7.TabIndex = 480;
            this.label7.Text = "Puede seleccionar varios pagos con Shift+Control";
            // 
            // nuevoComprobanteDePagoManualToolStripMenuItem
            // 
            this.nuevoComprobanteDePagoManualToolStripMenuItem.Image = global::FE_FX.Properties.Resources.ic_create_new_folder_black_24dp_1x;
            this.nuevoComprobanteDePagoManualToolStripMenuItem.Name = "nuevoComprobanteDePagoManualToolStripMenuItem";
            this.nuevoComprobanteDePagoManualToolStripMenuItem.Size = new System.Drawing.Size(381, 26);
            this.nuevoComprobanteDePagoManualToolStripMenuItem.Text = "Nuevo Comprobante de Pago Manual";
            this.nuevoComprobanteDePagoManualToolStripMenuItem.Click += new System.EventHandler(this.nuevoComprobanteDePagoManualToolStripMenuItem_Click);
            // 
            // frmBandejaComprobantePago
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(1045, 509);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.progressBar1);
            this.Controls.Add(this.ribbonControl1);
            this.Controls.Add(this.dtgrdGeneral);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.KeyPreview = true;
            this.Name = "frmBandejaComprobantePago";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.frmBandejaComprobantePago_FormClosed);
            this.Load += new System.EventHandler(this.frmBandejaComprobantePago_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dtgrdGeneral)).EndInit();
            this.ribbonControl1.ResumeLayout(false);
            this.ribbonControl1.PerformLayout();
            this.ribbonPanel1.ResumeLayout(false);
            this.ribbonPanel1.PerformLayout();
            this.contextMenuStrip1.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ToolStripButton btnRefresh;
        private System.Windows.Forms.ToolStripLabel toolStripLabel2;
        private System.Windows.Forms.ToolStripButton toolStripButton3;
        private System.Windows.Forms.DataGridView dtgrdGeneral;
        private System.Windows.Forms.ToolStripButton toolStripButton4;
        private System.Windows.Forms.ToolStripButton toolStripButton7;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator4;
        private System.Windows.Forms.ToolStripLabel toolStripLabel1;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator5;
        private System.Windows.Forms.ToolStripButton toolStripButton9;
        private System.Windows.Forms.ToolStripLabel toolStripStatusLabel1;
        private System.Windows.Forms.CheckBox PDF_VER;
        private DevComponents.DotNetBar.RibbonControl ribbonControl1;
        private DevComponents.DotNetBar.RibbonPanel ribbonPanel1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.DateTimePicker INICIO;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.DateTimePicker FINAL;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox INVOICE_IDs;
        private System.Windows.Forms.TextBox TOPtxt;
        private System.Windows.Forms.Label label9;
        private DevComponents.DotNetBar.RibbonTabItem ribbonTabItem1;
        private DevComponents.DotNetBar.Office2007StartButton buttonFile;
        private DevComponents.DotNetBar.ButtonItem buttonItem37;
        private DevComponents.DotNetBar.LabelItem informacion;
        private DevComponents.DotNetBar.RibbonTabItemGroup ribbonTabItemGroup1;
        private System.Windows.Forms.CheckBox CFDI_PRUEBA;
        private System.Windows.Forms.CheckBox PENDIENTES;
        private DevComponents.DotNetBar.ComboBoxItem ENTITY_ID;
        private DevComponents.Editors.ComboItem comboItem3;
        private DevComponents.Editors.ComboItem comboItem4;
        private DevComponents.Editors.ComboItem comboItem1;
        private DevComponents.Editors.ComboItem comboItem2;
        private DevComponents.DotNetBar.LabelItem EMPRESA_NOMBRE;
        private System.Windows.Forms.TextBox CUSTOMER_NAME;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.ProgressBar progressBar1;
        private DevComponents.DotNetBar.ControlContainerItem controlContainerItem12;
        private System.Windows.Forms.FolderBrowserDialog folderBrowserDialog1;
        private System.Windows.Forms.CheckBox MOSTRAR_PDF_ESTADO;
        private DevComponents.DotNetBar.LabelItem informar_procesar;
        private System.Windows.Forms.Timer oTimer;
        private DevComponents.DotNetBar.StyleManager styleManager1;
        private DevComponents.DotNetBar.ItemContainer itemContainer12;
        private DevComponents.DotNetBar.ButtonItem buttonItem23;
        private DevComponents.DotNetBar.ButtonItem buttonItem8;
        private DevComponents.DotNetBar.ButtonItem buttonItem2;
        private DevComponents.DotNetBar.ButtonItem buttonItem21;
        private DevComponents.DotNetBar.ButtonItem buttonItem24;
        private DevComponents.DotNetBar.ButtonItem buttonItem29;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.ToolStripMenuItem verToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem pDFToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem xMLToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem enviarPorCorreoPDFYXMLToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem regenerarPDFToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem cancelarToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem exportarToolStripMenuItem;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip1;
        private DevComponents.DotNetBar.ItemContainer menuFileContainer;
        private DevComponents.DotNetBar.ItemContainer menuFileBottomContainer;
        private DevComponents.DotNetBar.ButtonItem buttonExit;
        private DevComponents.DotNetBar.ButtonItem buttonItem7;
        private DevComponents.DotNetBar.ButtonItem buttonItem1;
        private DevComponents.DotNetBar.ButtonItem buttonItem9;
        private DevComponents.DotNetBar.ButtonItem buttonItem14;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox CHECK_ID;
        private System.Windows.Forms.ToolStripMenuItem cambiarFormaDePagoToolStripMenuItem;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.CheckBox ejecutarAutomatico;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.ToolStripMenuItem relacionarComplmentoDePagoCanceladoToolStripMenuItem;
        private System.Windows.Forms.Button button5;
        private System.Windows.Forms.ToolStripMenuItem generarReporteDeNDCNoAsociadasToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem procesarComplementosAutoajustandoElMontoPagadoToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem compararDepositosConComplementoDePagoToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem autorelacionarConElMismoPagoToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem cFDIAExcelToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem copiarCFDISeleccionadosACarpetaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem generarSinRelacionarNDCToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem generarComprobanteConTCDeCambioToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem rToolStripMenuItem;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox estadoFiltro;
        private System.Windows.Forms.ToolStripMenuItem procesarConRegeneraciónDeDatosToolStripMenuItem;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.ComboBox Version;
        private System.Windows.Forms.ToolStripMenuItem utilidadesToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem migrarCFDIToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem nuevoComprobanteDePagoManualToolStripMenuItem;
    }
}

