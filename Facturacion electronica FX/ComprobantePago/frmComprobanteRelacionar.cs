#region Copyright Syncfusion Inc. 2001 - 2017
// Copyright Syncfusion Inc. 2001 - 2017. All rights reserved.
// Use of this code is subject to the terms of our license.
// A copy of the current license can be obtained at any time by e-mailing
// licensing@syncfusion.com. Any infringement will be prosecuted under
// applicable laws. 
#endregion
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;

using System.Text;
using System.Windows.Forms;
using ModeloDocumentosFX;
using System.Text;
using System.Windows.Forms;
using System.Linq;
using System.Data.Entity.Validation;
using Generales;
using ModeloComprobantePago;
using FE_FX.Clases;

namespace FE_FX.ComprobantePago
{
    public partial class frmComprobanteRelacionar : Syncfusion.Windows.Forms.MetroForm
    {
        string checkId = "";
        string customerId = "";
        int pago_Id = 0;
        bool modificado = false;
        public frmComprobanteRelacionar(string checkIdp, string customerIdp)
        {

            checkId = checkIdp;
            customerId = customerIdp;
            InitializeComponent();
            cargarTipos();
            limpiar();
            cargar();
        }
        private void cargarTipos()
        {
            tipoRelacion.Items.Add(new ComboItem { Name = "Sustituci�n de los CFDI previos", Value = "04" });
            tipoRelacion.SelectedIndex = 0;
        }
        #region Metodos
        private void cargar()
        {
            //Cargar los datos
            ModeloComprobantePago.documentosfxEntities dbContext = new documentosfxEntities();
            try
            {
                dtg.Rows.Clear();
                var result = from b in dbContext.pagoRelacionadoSet
                                         .Where(b => b.checkId == checkId
                                         & b.customerId== customerId
                                         )

                             select b
                                         ;
                if (result != null)
                {
                    List<pagoRelacionado> dt = result.ToList();
                    bool tipoCargado = false;
                    foreach (pagoRelacionado registro in dt)
                    {
                        int n = dtg.Rows.Add();
                        //Cargar el tipo
                        if (!tipoCargado)
                        {
                            foreach (ComboItem cbi in tipoRelacion.Items)
                            {
                                if (cbi.Value as String == registro.tipoRelacion)
                                {
                                    this.tipoRelacion.SelectedItem = cbi;
                                    break;
                                }
                            }
                            tipoCargado = true;
                        }
                        dtg.Rows[n].Tag = registro;
                        dtg.Rows[n].Cells["UUID"].Value = registro.UUID.ToString();
                    }
                }

                dbContext.Dispose();
            }
            catch (Exception err)
            {
                ErrorFX.mostrar(err, false, true, "frmComprobanteRelacionar - 117 - ");
            }
        }

        public void limpiar()
        {
            if (modificado)
            {

                DialogResult Resultado = MessageBox.Show(this, "�Desea guardar los cambios?", Application.ProductName + "-" + Application.ProductVersion, MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                if (Resultado.ToString() == "Yes")
                {
                    guardar();
                }
            }
            modificado = false;
            foreach (var c in this.Controls)
            {
                if (c is TextBox)
                {
                    ((TextBox)c).Text = "";
                }
            }
            dtg.Rows.Clear();
        }
        private void guardar()
        {

            try
            {
                cComprobantePago ocComprobantePago = new cComprobantePago();

                //Eliminar todos las relaciones que tiene
                ocComprobantePago.eliminarRelaciones(checkId, customerId);
                
                
                string tipoRelaciontr = (tipoRelacion.SelectedItem as ComboItem).Value.ToString();
                foreach (DataGridViewRow linea in dtg.Rows)
                {
                    string UUIDtr = linea.Cells["UUID"].Value.ToString();
                    ComboItem selected = (ComboItem)tipoRelacion.SelectedItem;
                    ocComprobantePago.relacionar(UUIDtr, tipoRelaciontr, checkId, customerId);
                }
                
                MessageBox.Show(this, "Datos guardados", Application.ProductName + "-" + Application.ProductVersion, MessageBoxButtons.OK, MessageBoxIcon.Information);

                DialogResult = DialogResult.OK;


            }
            catch (Exception e)
            {
                ErrorFX.mostrar(e, true, true, "frmComprobanteRelacionar - 187 ");
            }
        }

        private void eliminarLinea(DataGridViewRow linea)
        {
            string uuidTr = linea.Cells["UUID"].Value.ToString();
            ModeloComprobantePago.documentosfxEntities dbContext = new documentosfxEntities();
            pagoRelacionado opagoRelacionado = dbContext.pagoRelacionadoSet.Where(
            b => b.checkId == checkId
            & b.customerId == customerId
            & b.UUID.Equals(uuidTr)
            ).FirstOrDefault();
            if (opagoRelacionado == null)
            {
                return;
            }
            //Eliminar
            dbContext.pagoRelacionadoSet.Attach(opagoRelacionado);
            dbContext.pagoRelacionadoSet.Remove(opagoRelacionado);
            dbContext.SaveChanges();

        }

        private void eliminar()
        {
            DialogResult Resultado = MessageBox.Show(this, "�Esta seguro de eliminar?", Application.ProductName + "-" + Application.ProductVersion, MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            if (Resultado.ToString() != "Yes")
            {
                return;
            }
            ModeloComprobantePago.documentosfxEntities dbContext = new documentosfxEntities();
            var pagosTr = dbContext.pagoRelacionadoSet.Where(
            b => b.checkId == checkId
            & b.customerId == customerId
            );
            if (pagosTr == null)
            {
                return;
            }
            //Eliminar
            dbContext.pagoRelacionadoSet.RemoveRange(pagosTr);
            dbContext.SaveChanges();
            MessageBox.Show(this, "Eliminado.", Application.ProductName + "-" + Application.ProductVersion, MessageBoxButtons.OK, MessageBoxIcon.Information);
            limpiar();
        }


        #endregion

        private void buttonAdv1_Click(object sender, EventArgs e)
        {
            limpiar();
        }

        private void btnAddItem_Click(object sender, EventArgs e)
        {
            guardar();
        }

        private void btnDeleteItem_Click(object sender, EventArgs e)
        {
            eliminar();
        }

        private void frmComprobanteRelacionar_FormClosed(object sender, FormClosedEventArgs e)
        {
            DialogResult = DialogResult.Cancel;
        }

        private void buttonAdv3_Click(object sender, EventArgs e)
        {
            eliminarLinea();
        }

        private void eliminarLinea()
        {
            dtg.EndEdit();
            if (dtg.SelectedCells.Count > 0)
            {
                DialogResult Resultado = MessageBox.Show("�Desea eliminar las seleccionada?", Application.ProductName + "-" + Application.ProductVersion, MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                if (Resultado.ToString() != "Yes")
                {
                    return;
                }
                ModeloDocumentosFX.ModeloDocumentosFXContainer dbContext = new ModeloDocumentosFXContainer();
                string conection = dbContext.Database.Connection.ConnectionString;
                cCONEXCION oData = new cCONEXCION(conection);
                foreach (DataGridViewRow drv in dtg.SelectedRows)
                {
                    try
                    {
                        eliminarLinea(drv);
                        //Limpiar lineas que tengan de productos asociadas
                        string invoice_id = drv.Cells["factura"].Value.ToString();
                        dtg.Rows.Remove(drv);
                    }
                    catch (Exception e)
                    {
                        Bitacora.Log(e.Message.ToString());
                    }

                }
            }
            else
            {
                MessageBox.Show("Seleccione alguna l�nea.", Application.ProductName + "-" + Application.ProductVersion, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }


        private void buttonAdv2_Click(object sender, EventArgs e)
        {
            cargarFacturas();
        }

        private void cargarFacturas()
        {
            frmComprobantesPago o = new frmComprobantesPago(customerId);
            if (o.ShowDialog() == DialogResult.OK)
            {
                foreach (DataGridViewRow linea in o.selecionados)
                {
                    int n = dtg.Rows.Add();
                    dtg.Rows[n].Cells["UUID"].Value = linea.Cells["UUID"].Value.ToString();
                }
            }
        }
    }
}
