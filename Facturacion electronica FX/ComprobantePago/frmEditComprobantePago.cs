#region Copyright Syncfusion Inc. 2001 - 2017
// Copyright Syncfusion Inc. 2001 - 2017. All rights reserved.
// Use of this code is subject to the terms of our license.
// A copy of the current license can be obtained at any time by e-mailing
// licensing@syncfusion.com. Any infringement will be prosecuted under
// applicable laws. 
#endregion
using CrystalDecisions.Shared.Json;
using Generales;
using ModeloComprobantePago;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Validation;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Windows.Forms;

namespace FE_FX.ComprobantePago
{
    public partial class frmEditComprobantePago : Syncfusion.Windows.Forms.MetroForm
    {
        ModeloComprobantePago.documentosfxEntities dbContext = new documentosfxEntities();
        public pago oRegistroP;
        public frmEditComprobantePago(pago oPagoP)
        {
            InitializeComponent();
            limpiar();
            oRegistroP = new pago();
            cargarCombo();
            if (oPagoP != null)
            {
                oRegistroP = dbContext.pagoSet.Where(a => a.Id.Equals(oPagoP.Id))
                    .OrderByDescending(b => b.Id)
                    .FirstOrDefault();
                cargar();
            }
        }


        public frmEditComprobantePago(string CheckId, string CustomerId)
        {
            InitializeComponent();
            limpiar();
            oRegistroP = new pago();
            cargarCombo();
            



            oRegistroP = dbContext.pagoSet.Where(a => a.checkId.Equals(CheckId)
            & a.customerId.Equals(CustomerId))
                .OrderByDescending(b => b.Id)
                .FirstOrDefault();

            if (oRegistroP==null)
            {
                //Cargar el �ltimo pago
                oRegistroP = dbContext.pagoSet.Where(a => a.customerId.Equals(CustomerId))
                    .OrderByDescending(b => b.Id)
                    .FirstOrDefault();
            }

            cargar();
            
        }

        private void limpiar()
        {
            Action<Control.ControlCollection> func = null;

            func = (controls) =>
            {
                foreach (Control control in controls)
                    if (control is TextBox)
                        (control as TextBox).Clear();
                    else
                        func(control.Controls);
            };

            func(Controls);
            dtgrdGeneral.Rows.Clear();
        }

        private void cargar()
        {
            try
            {
                if (oRegistroP==null)
                {
                    return;
                }

                if (!String.IsNullOrEmpty(oRegistroP.receptor))
                {
                    receptor.Text = oRegistroP.receptor;
                }

                if (!String.IsNullOrEmpty(oRegistroP.receptorNombre))
                {
                    receptorNombre.Text = oRegistroP.receptorNombre;
                }


                if (!String.IsNullOrEmpty(oRegistroP.formaDePagoP))
                {
                    foreach (ComboItem item in FORMA_DE_PAGO.Items)
                    {
                        if (item.Value.Equals(oRegistroP.formaDePagoP))
                        {
                            FORMA_DE_PAGO.SelectedItem = item;
                            break;
                        }
                    }
                }

                if (!String.IsNullOrEmpty(oRegistroP.NomBancoOrdExt))
                {
                    NomBancoOrdExt.Text = oRegistroP.NomBancoOrdExt;
                }
                if (!String.IsNullOrEmpty(oRegistroP.NumOperacion))
                {
                    NumOperacion.Text = oRegistroP.NumOperacion;
                }
                if (!String.IsNullOrEmpty(oRegistroP.CtaBeneficiario))
                {
                    CtaBeneficiario.Text = oRegistroP.CtaBeneficiario;
                }
                if (!String.IsNullOrEmpty(oRegistroP.CtaOrdenante))
                {
                    CtaOrdenante.Text = oRegistroP.CtaOrdenante;
                }
                if (!String.IsNullOrEmpty(oRegistroP.RfcEmisorCtaBen))
                {
                    RfcEmisorCtaBen.Text = oRegistroP.RfcEmisorCtaBen;
                }
                if (!String.IsNullOrEmpty(oRegistroP.RfcEmisorCtaOrd))
                {
                    RfcEmisorCtaOrd.Text = oRegistroP.RfcEmisorCtaOrd;
                }

                if (oRegistroP.tipoCambioP!=null)
                {
                    this.tipoCambioP.Text = oRegistroP.tipoCambioP.ToString();
                }
                if (this.oRegistroP.monto!=null)
                {
                    total.Text = this.oRegistroP.monto.ToString();                    
                }
                


                cargarLinea();
            }
            catch(Exception error)
            {
                ErrorFX.mostrar(error, true, false, false);
            }
        }

        private void cargarLinea()
        {
            
            dtgrdGeneral.Rows.Clear();
            foreach (pagoLinea linea in oRegistroP.pagoLinea)
            {
                int n = dtgrdGeneral.Rows.Add();
                dtgrdGeneral.Rows[n].Tag = linea;
                dtgrdGeneral.Rows[n].Cells["factura"].Value = linea.factura;
                dtgrdGeneral.Rows[n].Cells["UUID"].Value = linea.UUID;
                dtgrdGeneral.Rows[n].Cells["Moneda"].Value = linea.MonedaDR;
                dtgrdGeneral.Rows[n].Cells["NumParcialidad"].Value = linea.NumParcialidad;
                dtgrdGeneral.Rows[n].Cells["ImpSaldoAnt"].Value = linea.ImpSaldoAnt;
                dtgrdGeneral.Rows[n].Cells["ImpPagado"].Value = linea.ImpPagado;
                dtgrdGeneral.Rows[n].Cells["ImpSaldoInsoluto"].Value = linea.ImpSaldoInsoluto;
                dtgrdGeneral.Rows[n].Cells["TipoCambioDR"].Value = linea.TipoCambioDR;
            }
            calcularTotal();
            //dtgrdGeneral.AutoResizeColumns(DataGridViewAutoSizeColumnsMode.Fill);

            //dtgrdGeneral.DataSource = oRegistroP.pagoLinea;


            dtgrdGeneral.AutoResizeColumns(
                DataGridViewAutoSizeColumnsMode.AllCells);
        }


        private void cargarCombo()
        {

            FORMA_DE_PAGO.Items.Clear();
            FORMA_DE_PAGO.Items.Add(new ComboItem { Name = "Efectivo", Value = "01" });
            FORMA_DE_PAGO.Items.Add(new ComboItem { Name = "Cheque nominativo", Value = "02" });
            FORMA_DE_PAGO.Items.Add(new ComboItem { Name = "Transferencia electr�nica de fondos", Value = "03" });
            FORMA_DE_PAGO.Items.Add(new ComboItem { Name = "Tarjeta de cr�dito", Value = "04" });
            FORMA_DE_PAGO.Items.Add(new ComboItem { Name = "Monedero electr�nico", Value = "05" });
            FORMA_DE_PAGO.Items.Add(new ComboItem { Name = "Dinero electr�nico", Value = "06" });
            FORMA_DE_PAGO.Items.Add(new ComboItem { Name = "Vales de despensa", Value = "08" });
            FORMA_DE_PAGO.Items.Add(new ComboItem { Name = "Daci�n en pago", Value = "12" });
            FORMA_DE_PAGO.Items.Add(new ComboItem { Name = "Pago por subrogaci�n", Value = "13" });
            FORMA_DE_PAGO.Items.Add(new ComboItem { Name = "Pago por consignaci�n", Value = "14" });
            FORMA_DE_PAGO.Items.Add(new ComboItem { Name = "Condonaci�n", Value = "15" });
            FORMA_DE_PAGO.Items.Add(new ComboItem { Name = "Compensaci�n", Value = "17" });
            FORMA_DE_PAGO.Items.Add(new ComboItem { Name = "Novaci�n", Value = "23" });
            FORMA_DE_PAGO.Items.Add(new ComboItem { Name = "Confusi�n", Value = "24" });
            FORMA_DE_PAGO.Items.Add(new ComboItem { Name = "Remisi�n de deuda", Value = "25" });
            FORMA_DE_PAGO.Items.Add(new ComboItem { Name = "Prescripci�n o caducidad", Value = "26" });
            FORMA_DE_PAGO.Items.Add(new ComboItem { Name = "A satisfacci�n del acreedor", Value = "27" });
            FORMA_DE_PAGO.Items.Add(new ComboItem { Name = "Tarjeta de d�bito", Value = "28" });
            FORMA_DE_PAGO.Items.Add(new ComboItem { Name = "Tarjeta de servicios", Value = "29" });
            FORMA_DE_PAGO.Items.Add(new ComboItem { Name = "Aplicaci�n de anticipos", Value = "30" });
            ComboItem oComboItemFORMA_DE_PAGO = new ComboItem { Name = "Transferencia electr�nica de fondos", Value = "03" };
            FORMA_DE_PAGO.Items.Add(oComboItemFORMA_DE_PAGO);
            FORMA_DE_PAGO.SelectedItem = oComboItemFORMA_DE_PAGO;
        }

        private void btnAddItem_Click(object sender, EventArgs e)
        {
            guardar();
        }

        private void guardar()
        {
            try
            {

                if (oRegistroP == null)
                {
                    oRegistroP = new pago();
                }

                if (oRegistroP.Id == 0)
                {
                    oRegistroP = new pago();
                }

                this.oRegistroP.formaDePagoP = ((ComboItem)FORMA_DE_PAGO.SelectedItem).Value.ToString();
                this.oRegistroP.NomBancoOrdExt = NomBancoOrdExt.Text;
                this.oRegistroP.NumOperacion = NumOperacion.Text;

                if (!String.IsNullOrEmpty(CtaBeneficiario.Text))
                {
                    if (CtaBeneficiario.Text.Length < 10)
                    {
                        ErrorFX.mostrar("La Cuenta del Beneficiario puede conformarse desde 10 hasta 50 caracteres.", true, false, false);
                        CtaBeneficiario.Focus();
                        return;
                    }
                }

                //if (!String.IsNullOrEmpty(this.RfcEmisorCtaBen.Text))
                //{

                //        Match match = Regex.Match(RfcEmisorCtaBen.Text, @"[A-Z&�]{3}[0-9]{2}(0[1-9]1[012])(0[1-9][12][0-9]3[01])[A-Z0-9]{2}[0-9A]", RegexOptions.IgnoreCase);
                //        if (!match.Success)
                //        {
                //            ErrorFX.mostrar("El RFC Emisor Cta Benificiario no es RFC ", true, false, false);
                //        RfcEmisorCtaBen.Focus();
                //            return;
                //        }
                //}
                //if (!String.IsNullOrEmpty(this.RfcEmisorCtaOrd.Text))
                //{

                //    Match match = Regex.Match(RfcEmisorCtaOrd.Text, @"[A-Z&�]{3}[0-9]{2}(0[1-9]1[012])(0[1-9][12][0-9]3[01])[A-Z0-9]{2}[0-9A]", RegexOptions.IgnoreCase);
                //    if (!match.Success)
                //    {
                //        ErrorFX.mostrar("El RFC Emisor Cta Ordenante no es RFC ", true, false, false);
                //        RfcEmisorCtaOrd.Focus();
                //        return;
                //    }
                //}

                if (!String.IsNullOrEmpty(CtaOrdenante.Text))
                {



                    if (this.oRegistroP.formaDePagoP.Equals("02"))
                    {
                        Match match = Regex.Match(CtaOrdenante.Text, @"[0-9]{11}|[0-9]{18}", RegexOptions.IgnoreCase);
                        if (!match.Success)
                        {
                            ErrorFX.mostrar("La Cuenta del Ordenante Si es Forma de Pago: " + this.oRegistroP.formaDePagoP + " puede conformarse de 11 n�meros o 18 n�meros", true, false, false);
                            CtaOrdenante.Focus();
                            return;
                        }
                    }

                    if (this.oRegistroP.formaDePagoP.Equals("03"))
                    {
                        Match match = Regex.Match(CtaOrdenante.Text, @"[0-9]{10}|[0-9]{16}|[0-9]{18}", RegexOptions.IgnoreCase);
                        if (!match.Success)
                        {
                            ErrorFX.mostrar("La Cuenta del Ordenante Si es Forma de Pago: " + this.oRegistroP.formaDePagoP + " puede conformarse de 10 n�meros o 16 n�meros o 18 n�meros", true, false, false);
                            CtaOrdenante.Focus();
                            return;
                        }
                    }

                    if (this.oRegistroP.formaDePagoP.Equals("04"))
                    {
                        Match match = Regex.Match(CtaOrdenante.Text, @"[0-9]{16}", RegexOptions.IgnoreCase);
                        if (!match.Success)
                        {
                            ErrorFX.mostrar("La Cuenta del Ordenante Si es Forma de Pago: " + this.oRegistroP.formaDePagoP + " puede conformarse de 16 n�meros", true, false, false);
                            CtaOrdenante.Focus();
                            return;
                        }
                    }

                    if (this.oRegistroP.formaDePagoP.Equals("05"))
                    {
                        Match match = Regex.Match(CtaOrdenante.Text, @"[0-9]{10,11}|[0-9]{15,16}|[0-9]{18}|[A-Z0-9_]{10,50}", RegexOptions.IgnoreCase);
                        if (!match.Success)
                        {
                            ErrorFX.mostrar("La Cuenta del Ordenante Si es Forma de Pago: " + this.oRegistroP.formaDePagoP + " puede conformarse de [0-9]{10,11}|[0-9]{15,16}|[0-9]{18}|[A-Z0-9_]{10,50}", true, false, false);
                            CtaOrdenante.Focus();
                            return;
                        }
                    }
                }

                this.oRegistroP.CtaBeneficiario = CtaBeneficiario.Text;
                this.oRegistroP.CtaOrdenante = CtaOrdenante.Text;
                this.oRegistroP.RfcEmisorCtaBen = RfcEmisorCtaBen.Text;
                this.oRegistroP.RfcEmisorCtaOrd = RfcEmisorCtaOrd.Text;
                if (!String.IsNullOrEmpty(tipoCambioP.Text))
                {
                    if (Generales.Globales.IsNumeric(tipoCambioP.Text))
                    {
                        this.oRegistroP.tipoCambioP = decimal.Parse(tipoCambioP.Text);
                    }
                }

                if (receptor.Text != oRegistroP.receptor)
                {
                    oRegistroP.receptor = receptor.Text;
                }

                if (receptorNombre.Text != oRegistroP.receptorNombre)
                {
                    oRegistroP.receptorNombre = receptorNombre.Text;
                }
                //
                if (oRegistroP.Id == 0)
                {
                    dbContext.pagoSet.Add(oRegistroP);
                    dbContext.SaveChanges();
                }
                //Rodrigo Escalona 
                if (oRegistroP.Id != 0)
                {
                    dbContext.pagoSet.Attach(oRegistroP);
                    dbContext.Entry(oRegistroP).State = EntityState.Modified;
                    dbContext.SaveChanges();
                    if (oRegistroP.editado)
                    {

                        dbContext.Database.ExecuteSqlCommand("DELETE FROM pagoLineaSet WHERE pago_Id=" + oRegistroP.Id.ToString()); ;

                        //Guardar las l�neas
                        try
                        {
                            foreach (DataGridViewRow r in dtgrdGeneral.Rows)
                            {
                                pagoLinea opagoLinea = new pagoLinea();
                                opagoLinea.pago = this.oRegistroP;
                                opagoLinea.factura = r.Cells["factura"].Value.ToString();
                                opagoLinea.UUID = r.Cells["UUID"].Value.ToString();
                                opagoLinea.MonedaDR = r.Cells["Moneda"].Value.ToString();
                                opagoLinea.NumParcialidad = r.Cells["NumParcialidad"].Value.ToString();
                                opagoLinea.ImpSaldoAnt = decimal.Parse(r.Cells["ImpSaldoAnt"].Value.ToString());
                                opagoLinea.ImpPagado = decimal.Parse(r.Cells["ImpPagado"].Value.ToString());
                                opagoLinea.ImpSaldoInsoluto = 0;
                                if (r.Cells["ImpSaldoInsoluto"].Value != null)
                                {
                                    opagoLinea.ImpSaldoInsoluto = decimal.Parse(r.Cells["ImpSaldoInsoluto"].Value.ToString());
                                }
                                opagoLinea.TipoCambioDR = decimal.Parse(r.Cells["TipoCambioDR"].Value.ToString());
                                dbContext.pagoLineaSet.Add(opagoLinea);
                                dbContext.SaveChanges();
                            }
                        }
                        catch (Exception e)
                        {
                            ErrorFX.mostrar(e, true, false, "frmEditComprobantePago - 339 ");
                        }

                    }
                }


                DialogResult = DialogResult.OK;
            }
            catch (DbEntityValidationException error)
            {
                string mensaje = "Shipment - 204 ";
                foreach (var eve in error.EntityValidationErrors)
                {
                    foreach (var ve in eve.ValidationErrors)
                    {
                        mensaje += ve.PropertyName + " Error: " + ve.ErrorMessage + Environment.NewLine;
                    }
                }
                ErrorFX.mostrar(mensaje, true, false, false);
                return;
            }
            catch (Exception error)
            {
                string mensaje = "Comprobante de Pago - 217 ";
                mensaje += error.Message.ToString();
                if (error.InnerException != null)
                {
                    mensaje += " " + error.InnerException.Message.ToString();
                }
                ErrorFX.mostrar(mensaje, true, false, false);
                return;
            }

        }

        private void frmEditComprobantePago_Load(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            agregarLinea();
        }

        private void agregarLinea()
        {
            int n = dtgrdGeneral.Rows.Add();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            eliminar();
        }

        private void eliminar()
        {
            try
            {
                dtgrdGeneral.EndEdit();
                if (dtgrdGeneral.SelectedCells.Count > 0)
                {
                    DialogResult Resultado = MessageBox.Show("�Esta seguro de eliminar las l�neas seleccionadas?", Application.ProductName + "-" + Application.ProductVersion, MessageBoxButtons.YesNo);
                    if (Resultado.ToString() != "Yes")
                    {
                        return;
                    }

                    IEnumerable<DataGridViewRow> selectedRows = dtgrdGeneral.SelectedCells.Cast<DataGridViewCell>()
                                           .Select(cell => cell.OwningRow)
                                           .Distinct();

                    foreach (DataGridViewRow drv in selectedRows)
                    {
                        if (drv.Tag!=null)
                        {
                            pagoLinea pagoLineaTr = (pagoLinea)drv.Tag;
                            dbContext.pagoLineaSet.Remove(pagoLineaTr);
                            dbContext.SaveChanges();
                        }
                        dtgrdGeneral.Rows.Remove(drv);
                    }

                    calcularTotal();
                }
            }
            catch (Exception e)
            {
                ErrorFX.mostrar(e, true, true, "frmEditComprobantePago - 295 ");
            }
        }

        private void dtgrdGeneral_DataBindingComplete(object sender, DataGridViewBindingCompleteEventArgs e)
        {
            dtgrdGeneral.Columns["Id"].Visible = false;
            
        }

        private void calcularTotal()
        {
            decimal totalTr = 0;
            foreach (DataGridViewRow row in dtgrdGeneral.Rows)
            {
                decimal ImpPagadoTr = 0;
                decimal TipoCambioDRTr = 0;
                try
                {
                    ImpPagadoTr = decimal.Parse(row.Cells["ImpPagado"].Value.ToString());
                }
                catch
                {

                }

                try
                {
                    TipoCambioDRTr = decimal.Parse(row.Cells["TipoCambioDR"].Value.ToString());
                }
                catch
                {

                }

                if (TipoCambioDRTr!=0)
                {
                    totalTr += ImpPagadoTr / TipoCambioDRTr;
                }
            }
            total.Text = totalTr.ToString("N4");
        }

        private void dtgrdGeneral_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {

            if (dtgrdGeneral.Rows[e.RowIndex].Tag != null)
            {
                if (String.IsNullOrEmpty(dtgrdGeneral.Rows[e.RowIndex].Cells[e.ColumnIndex].Value.ToString()))
                {
                    return;
                }

                pagoLinea pagoLineaTr = (pagoLinea)dtgrdGeneral.Rows[e.RowIndex].Tag;
                pagoLinea pagoLineaTr2 = dbContext.pagoLineaSet.Where(i => i.Id == pagoLineaTr.Id).FirstOrDefault();

                if (dtgrdGeneral.Columns[e.ColumnIndex].Name == "ImpSaldoAnt")
                {
                    pagoLineaTr2.ImpSaldoAnt = decimal.Parse(dtgrdGeneral.Rows[e.RowIndex].Cells[e.ColumnIndex].Value.ToString());
                    dbContext.Entry(pagoLineaTr2).State = EntityState.Modified;
                    dbContext.SaveChanges();
                }
                if (dtgrdGeneral.Columns[e.ColumnIndex].Name == "ImpSaldoInsoluto")
                {
                    pagoLineaTr2.ImpSaldoInsoluto = decimal.Parse(dtgrdGeneral.Rows[e.RowIndex].Cells[e.ColumnIndex].Value.ToString());
                    dbContext.Entry(pagoLineaTr2).State = EntityState.Modified;
                    dbContext.SaveChanges();
                }
                if (dtgrdGeneral.Columns[e.ColumnIndex].Name == "TipoCambioDR")
                {
                    pagoLineaTr2.TipoCambioDR = decimal.Parse(dtgrdGeneral.Rows[e.RowIndex].Cells[e.ColumnIndex].Value.ToString());
                    dbContext.Entry(pagoLineaTr2).State = EntityState.Modified;
                    dbContext.SaveChanges();
                }

                if (dtgrdGeneral.Columns[e.ColumnIndex].Name == "ImpPagado")
                {
                    pagoLineaTr2.ImpPagado = decimal.Parse(dtgrdGeneral.Rows[e.RowIndex].Cells[e.ColumnIndex].Value.ToString());
                    dbContext.Entry(pagoLineaTr2).State = EntityState.Modified;
                    dbContext.SaveChanges();
                }

                calcularTotal();
            }

        }

        private void button2_Click(object sender, EventArgs e)
        {
            calcularTotal();
        }

        private void label19_Click(object sender, EventArgs e)
        {

        }

        private void button2_Click_1(object sender, EventArgs e)
        {
            calcularLineasATotal();
        }

        private void calcularLineasATotal()
        {
            calcularTotal();
        }

        private void button5_Click(object sender, EventArgs e)
        {
            duplicar();
        }

        private void duplicar()
        {
            if (this.dtgrdGeneral.SelectedRows.Count>0)
            {
                foreach (DataGridViewRow registro in this.dtgrdGeneral.SelectedRows)
                {

                    int n = dtgrdGeneral.Rows.Add();
                    dtgrdGeneral.Rows[n].Tag = registro.Tag;
                    dtgrdGeneral.Rows[n].Cells["factura"].Value = registro.Cells["factura"].Value.ToString();
                    dtgrdGeneral.Rows[n].Cells["UUID"].Value = registro.Cells["UUID"].Value.ToString();
                    dtgrdGeneral.Rows[n].Cells["Moneda"].Value = registro.Cells["Moneda"].Value.ToString();
                    dtgrdGeneral.Rows[n].Cells["NumParcialidad"].Value = "1";
                    dtgrdGeneral.Rows[n].Cells["ImpSaldoAnt"].Value = "0";
                    dtgrdGeneral.Rows[n].Cells["ImpPagado"].Value = "0";
                    dtgrdGeneral.Rows[n].Cells["ImpSaldoInsoluto"].Value = "0";
                    dtgrdGeneral.Rows[n].Cells["TipoCambioDR"].Value = "1";


                }
            }
        }
    }
}
