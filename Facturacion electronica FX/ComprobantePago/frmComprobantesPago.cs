#region Copyright Syncfusion Inc. 2001 - 2017
// Copyright Syncfusion Inc. 2001 - 2017. All rights reserved.
// Use of this code is subject to the terms of our license.
// A copy of the current license can be obtained at any time by e-mailing
// licensing@syncfusion.com. Any infringement will be prosecuted under
// applicable laws. 
#endregion
using Generales;
using ModeloComprobantePago;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace FE_FX.ComprobantePago
{
    public partial class frmComprobantesPago : Syncfusion.Windows.Forms.MetroForm
    {
        string customerID = "";
        public frmComprobantesPago(string customerID)
        {
            this.customerID = customerID;
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            cargar();
        }

        private void cargar()
        {
            try
            {
                ModeloComprobantePago.documentosfxEntities dbContext = new documentosfxEntities();
                var result = from b in dbContext.pagoSet
                            .Where(
                            a =>
                            a.customerId.ToString().Equals(customerID)
                            )
                            .OrderByDescending(a => a.Id)
                             select b;


                List<pago> dt = result.ToList();
                dtg.Rows.Clear();
                foreach (pago registro in dt)
                {
                    if (registro != null)
                    {
                        int n = dtg.Rows.Add();
                        dtg.Rows[n].Tag = registro;
                        dtg.Rows[n].Cells["checkId"].Value = registro.checkId;
                        dtg.Rows[n].Cells["Total"].Value = registro.monto;
                        dtg.Rows[n].Cells["UUID"].Value = registro.uuid;
                    }

                }
                dbContext.Dispose();
                this.dtg.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.DisplayedCells;
            }
            catch (Exception error)
            {
                ErrorFX.mostrar(error, true, true, "frmComprobantesPago - 41 - ", true);
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            seleccionar();
        }
        public DataGridViewSelectedRowCollection selecionados;
        private void seleccionar()
        {
            try
            {

                if (dtg.SelectedRows.Count > 0)
                {
                    DataGridViewSelectedRowCollection arrSelectedRows = dtg.SelectedRows;
                    selecionados = arrSelectedRows;
                    this.DialogResult = DialogResult.OK;
                }
            }
            catch (Exception error)
            {
                ErrorFX.mostrar(error, true, true, "frmComprobantesPago - 93 - ", true);
            }
        }
    }
}
