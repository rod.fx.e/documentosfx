#region Copyright Syncfusion Inc. 2001 - 2017
// Copyright Syncfusion Inc. 2001 - 2017. All rights reserved.
// Use of this code is subject to the terms of our license.
// A copy of the current license can be obtained at any time by e-mailing
// licensing@syncfusion.com. Any infringement will be prosecuted under
// applicable laws. 
#endregion
using CFDI33;
using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;
using FE_FX.Clases;
using FE_FX.EDICOM_Servicio;
using Generales;
using Ionic.Zip;
using ModeloComprobantePago;
using Pago10;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.Entity.Validation;
using System.Drawing;
using System.IO;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Windows.Forms;
using System.Xml;
using System.Xml.Serialization;

namespace FE_FX.ComprobantePago
{
    //TODO: Cargar Serie
    //TODO: Cargar CFDI
    //TODO: Guardar
    //TODO: Timbrar

    public partial class frmComprobantePago : Syncfusion.Windows.Forms.MetroForm
    {
        bool modificado = false;
        private cEMPRESA ocEMPRESA;
        private cSERIE ocSERIE;
        cCERTIFICADO ocCERTIFICADO = new cCERTIFICADO();
        private cCONEXCION oData = new cCONEXCION("");
        private cCONEXCION oData_ERP;
        pago oRegistro = new pago();
        ModeloComprobantePago.documentosfxEntities dbContext = new documentosfxEntities();

        public frmComprobantePago()
        {
            InitializeComponent();
            limpiar();
        }
        public frmComprobantePago(string[] archivos)
        {
            InitializeComponent();
            limpiar();

            cargarArchivos(archivos);
        }
        private void cargarCombos()
        {
            FORMA_DE_PAGO.Items.Clear();
            FORMA_DE_PAGO.Items.Add(new ComboItem { Name = "Efectivo", Value = "01" });
            FORMA_DE_PAGO.Items.Add(new ComboItem { Name = "Cheque nominativo", Value = "02" });
            FORMA_DE_PAGO.Items.Add(new ComboItem { Name = "Transferencia electr�nica de fondos", Value = "03" });
            FORMA_DE_PAGO.Items.Add(new ComboItem { Name = "Tarjeta de cr�dito", Value = "04" });
            FORMA_DE_PAGO.Items.Add(new ComboItem { Name = "Monedero electr�nico", Value = "05" });
            FORMA_DE_PAGO.Items.Add(new ComboItem { Name = "Dinero electr�nico", Value = "06" });
            FORMA_DE_PAGO.Items.Add(new ComboItem { Name = "Vales de despensa", Value = "08" });
            FORMA_DE_PAGO.Items.Add(new ComboItem { Name = "Daci�n en pago", Value = "12" });
            FORMA_DE_PAGO.Items.Add(new ComboItem { Name = "Pago por subrogaci�n", Value = "13" });
            FORMA_DE_PAGO.Items.Add(new ComboItem { Name = "Pago por consignaci�n", Value = "14" });
            FORMA_DE_PAGO.Items.Add(new ComboItem { Name = "Condonaci�n", Value = "15" });
            FORMA_DE_PAGO.Items.Add(new ComboItem { Name = "Compensaci�n", Value = "17" });
            FORMA_DE_PAGO.Items.Add(new ComboItem { Name = "Novaci�n", Value = "23" });
            FORMA_DE_PAGO.Items.Add(new ComboItem { Name = "Confusi�n", Value = "24" });
            FORMA_DE_PAGO.Items.Add(new ComboItem { Name = "Remisi�n de deuda", Value = "25" });
            FORMA_DE_PAGO.Items.Add(new ComboItem { Name = "Prescripci�n o caducidad", Value = "26" });
            FORMA_DE_PAGO.Items.Add(new ComboItem { Name = "A satisfacci�n del acreedor", Value = "27" });
            FORMA_DE_PAGO.Items.Add(new ComboItem { Name = "Tarjeta de d�bito", Value = "28" });
            FORMA_DE_PAGO.Items.Add(new ComboItem { Name = "Tarjeta de servicios", Value = "29" });
            FORMA_DE_PAGO.Items.Add(new ComboItem { Name = "Aplicaci�n de anticipos", Value = "30" });
            ComboItem oComboItemFORMA_DE_PAGO = new ComboItem { Name = "Transferencia electr�nica de fondos", Value = "03" };
            FORMA_DE_PAGO.Items.Add(oComboItemFORMA_DE_PAGO);
            FORMA_DE_PAGO.SelectedItem = oComboItemFORMA_DE_PAGO;

        }

        private void cargarTipos()
        {
            tipoRelacion.Items.Add(new ComboItem { Name = "Sustituci�n de los CFDI previos", Value = "04" });
            tipoRelacion.SelectedIndex = 0;
        }
        private void cargarArchivos(string[] archivos)
        {
            try
            {
                if (archivos.Length>0)
                {
                    foreach (string archivo in archivos)
                    {
                        //Cargar el CFDI
                        cargarCFDI(archivo);
                    }
                }
            }
            catch (Exception e)
            {
                ErrorFX.mostrar(e, true, true, "frmComprobantePago - 59 ");
            }
        }

        private void buttonAdv1_Click(object sender, EventArgs e)
        {
            limpiar();
        }
        private void limpiar()
        {
            if (modificado)
            {

                DialogResult Resultado = MessageBox.Show(this, "�Desea eliminar los cambios?", Application.ProductName + "-" + Application.ProductVersion, MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                if (Resultado == DialogResult.No)
                {
                    return;
                }
            }

            foreach (var c in this.Controls)
            {
                if (c is TextBox)
                {
                    ((TextBox)c).Text = "";
                }
            }
            //Guardar la cabecera
            oRegistro = new pago();
            cargarEmisor();
            cargarCombos();
            cargarTipos();
            estado.SelectedIndex = 0;
            fechaPago.Value = DateTime.Now;
            dtgFolio.Rows.Clear();
        }
        private void emisor_SelectedIndexChanged(object sender, EventArgs e)
        {
            actualizar_conexion();
        }

        private void actualizar_conexion()
        {
            ocEMPRESA = emisor.SelectedItem as cEMPRESA;
            if (ocEMPRESA != null)
            {
                oData_ERP = new cCONEXCION(ocEMPRESA.TIPO, ocEMPRESA.SERVIDOR, ocEMPRESA.BD, ocEMPRESA.USUARIO_BD, ocEMPRESA.PASSWORD_BD);
            }
        }
        private void cargarEmisor()
        {
            emisor.Items.Clear();
            cEMPRESA ocEMPRESA = new cEMPRESA();
            foreach (cEMPRESA registro in ocEMPRESA.todos("", true))
            {
                emisor.Items.Add(registro);
            }
            if (emisor.Items.Count > 0)
            {
                emisor.SelectedIndex = 0;
            }
        }


        private void btnAddItem_Click(object sender, EventArgs e)
        {
            guardar();
        }

        private void guardar()
        {
            string consecutivo=ocSERIE.consecutivo(ocSERIE.ROW_ID, false);
            serie.Text = ocSERIE.ID + consecutivo;
            ocSERIE.incrementar(ocSERIE.ROW_ID);
            try
            {
                string conection = dbContext.Database.Connection.ConnectionString;
                cCONEXCION oData = new cCONEXCION(conection);
                string sSQL = @"
                DELETE 
                FROM pagoLineaSet
                FROM pagoLineaSet INNER JOIN pagoSet ON pagoLineaSet.pago_Id = pagoSet.Id
                WHERE (pagoSet.serie = N'"+ ocSERIE.ID  + "') AND (pagoSet.folio = N'" + consecutivo + @"')
                ";
                oData.EjecutarConsulta(sSQL);
                sSQL = @"
                DELETE FROM pagoSet
                WHERE (pagoSet.serie = N'" + ocSERIE.ID + "') AND (pagoSet.folio = N'" + consecutivo + @"')
                ";
                oData.EjecutarConsulta(sSQL);
                dbContext = new documentosfxEntities();
                
                oRegistro.folio = consecutivo;
                oRegistro.serie = ocSERIE.ID;
                oRegistro.fecha = DateTime.Now;
                oRegistro.estado = estado.Text;
                oRegistro.monedaP = moneda.Text;
                oRegistro.tipoCambioP = decimal.Parse(tipoCambio.Text);
                DateTime fechaPagoTr = fechaPago.Value;
                oRegistro.fechaPago = new DateTime(fechaPagoTr.Year, fechaPagoTr.Month, fechaPagoTr.Day, 12, 0, 0);
                oRegistro.noCertificado = ocCERTIFICADO.NO_CERTIFICADO;
                oRegistro.lugarExpedicion = ocSERIE.CP;
                oRegistro.emisor= ocEMPRESA.RFC;

                oRegistro.checkId = checkId.Text;
                oRegistro.customerId = customerId.Text;

                oRegistro.emisorNombre = ocEMPRESA.ID;
                oRegistro.emisorRegimenFiscal = ocEMPRESA.REGIMEN_FISCAL;
                oRegistro.receptor = receptor.Text;
                oRegistro.receptorNombre = receptorNombre.Text;
                oRegistro.receptorNumRegIdTrib = receptorNumRegIdTrib.Text;
                oRegistro.formaDePagoP = ((ComboItem)FORMA_DE_PAGO.SelectedItem).Value.ToString();

                dbContext.pagoSet.Add(oRegistro);
                
                dbContext.SaveChanges();

                //Rodrigo Escalona 26/08/2020
                //Elimimibar 
                var deleteOrderDetails =
                from details in dbContext.pagoRelacionadoSet
                where details.customerId == customerId.Text 
                & details.checkId == checkId.Text
                select details;

                foreach (var detail in deleteOrderDetails)
                {
                    dbContext.pagoRelacionadoSet.Remove(detail);
                }

                try
                {
                    dbContext.SaveChanges();
                }
                catch (Exception e)
                {
                    Console.WriteLine(e);
                    // Provide for exceptions.
                }

                if (dtgTipoRelacion.Rows.Count>0)
                {
                    foreach(DataGridViewRow r in this.dtgTipoRelacion.Rows)
                    {
                        if (r.Cells["uuidRelacion"].Value != null)
                        {
                            if (!String.IsNullOrEmpty(r.Cells["uuidRelacion"].Value.ToString()))
                            {
                                pagoRelacionado opagoRelacionado = new pagoRelacionado();
                                opagoRelacionado.tipoRelacion = "04";
                                opagoRelacionado.customerId = oRegistro.checkId;
                                opagoRelacionado.checkId = oRegistro.customerId;
                                opagoRelacionado.UUID = r.Cells["uuidRelacion"].Value.ToString();
                                dbContext.pagoRelacionadoSet.Add(opagoRelacionado);
                            }
                        }
                    }
                }



                oRegistro.monto = 0;
                foreach (DataGridViewRow r in this.dtgFolio.Rows)
                {
                    pagoLinea linea = new pagoLinea();
                    linea.folio = r.Cells["folioCol"].Value.ToString();
                    linea.serie = r.Cells["serieCol"].Value.ToString();
                    linea.MonedaDR = r.Cells["monedaCol"].Value.ToString();
                    linea.ImpPagado = decimal.Parse(r.Cells["Aplicado"].Value.ToString());
                    linea.ImpSaldoAnt = decimal.Parse(r.Cells["ImpSaldoAntCol"].Value.ToString());
                    oRegistro.monto += linea.ImpPagado;
                    linea.NumParcialidad = r.Cells["NumParcialidad"].Value.ToString();
                    linea.UUID = r.Cells["uuidCol"].Value.ToString();
                    linea.factura = r.Cells["facturaCol"].Value.ToString();
                    decimal tipoCambioDRtr = 1;
                    tipoCambioDRtr = decimal.Parse(r.Cells["tipoCambioDRCol"].Value.ToString());
                    if (tipoCambioDRtr!=0)
                    {
                        tipoCambioDRtr = Math.Round(1 / tipoCambioDRtr, 2);
                    }
                    linea.TipoCambioDR = tipoCambioDRtr;

                    oRegistro.pagoLinea.Add(linea);
                }

                if (oRegistro.Id == 0)
                {
                    dbContext.pagoSet.Add(oRegistro);
                }
                else
                {
                    dbContext.pagoSet.Attach(oRegistro);
                    dbContext.Entry(oRegistro).State = System.Data.Entity.EntityState.Modified;
                    eliminarBD(false);
                }
                dbContext.SaveChanges();

                modificado = false;

                generar33 oGenerar33 = new generar33();
                CFDI33.Comprobante oComprobante = oGenerar33.generarComprobantePago(oRegistro,null);
                if (oComprobante!=null)
                {
                    oRegistro = dbContext.pagoSet.Where(a => a.Id == oRegistro.Id).FirstOrDefault();
                    xml.Text = oRegistro.xml;
                    timbrar(oComprobante);
                }
                
                MessageBox.Show(this, "Datos guardados", Application.ProductName + "-" + Application.ProductVersion, MessageBoxButtons.OK, MessageBoxIcon.Information);

                DialogResult = DialogResult.OK;


            }
            catch (DbEntityValidationException e)
            {
                foreach (var eve in e.EntityValidationErrors)
                {
                    foreach (var ve in eve.ValidationErrors)
                    {
                        MessageBox.Show(this, ve.PropertyName + " Error: " + ve.ErrorMessage, Application.ProductName
                            , MessageBoxButtons.OK, MessageBoxIcon.Error);
                        return;
                    }
                }
            }
            catch (Exception e)
            {
                ErrorFX.mostrar(e, true, true, "frmComprobantePago - 123 ");
            }
        }
        private bool timbrar(CFDI33.Comprobante oComprobante)
        {
            try
            {
                //Timbrar
                informacion.Text = "Timbrando el XML " + serie.Text;
                bool usar_edicom = false;
                if (estado.Text.Contains("BORRADOR"))
                {
                    usar_edicom = true;
                }
                try
                {
                    string passwordTr = cENCRIPTACION.Decrypt(Globales.oCFDI_USUARIO.PASSWORD);
                    if (usar_edicom)
                    {
                        usar_edicom = this.generar_CFDI(oComprobante, 2, Globales.oCFDI_USUARIO.USUARIO, passwordTr);
                    }
                    else
                    {
                        usar_edicom = this.generar_CFDI(oComprobante, 1, Globales.oCFDI_USUARIO.USUARIO, passwordTr);
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show("312 - Error timbrando en EDICOM" + Environment.NewLine + ex.InnerException.ToString(), Application.ProductVersion, MessageBoxButtons.OK);
                    return false;
                }
                //if (!estado.Text.Contains("BORRADOR"))
                //{
                    guardarCambios();
                    return true;
                //}
                //return false;
            }
            catch (Exception e)
            {
                ErrorFX.mostrar(e, true, true, "frmComprobantePago - 84 ");
            }
            return true;
        }

        private void guardarCambios()
        {
            try
            {
                //Generar el PDF
                //imprimir();
                cComprobantePago ocComprobantePago = new cComprobantePago();
                ocComprobantePago.imprimir(oRegistro, null, true);
                //Aumentar la serie
                if (!estado.Text.Contains("BORRADOR"))
                {
                    ocSERIE.incrementar(ocSERIE.ROW_ID);
                    bloquearControles();
                }

            }
            catch (Exception e)
            {
                ErrorFX.mostrar(e, true, true, "frmComprobantePago - 286 ");
            }
            return;
        }

        private void bloquearControles()
        {
            try
            {
                foreach (var c in this.Controls)
                {
                    if (c is Button)
                    {
                        ((Button)c).Enabled = false;
                    }

                    if (c is ComboBox)
                    {
                        ((ComboBox)c).Enabled = false;
                    }

                    if (c is TextBox)
                    {
                        ((TextBox)c).Enabled = false;
                    }
                }

                btnNuevo.Enabled = btnXML.Enabled = btnPDF.Enabled = btnImprimir.Enabled = true;

            }
            catch (Exception e)
            {
                ErrorFX.mostrar(e, true, true, "frmComprobantePago - 286 ");
            }
        }

        private byte[] ReadBinaryFile(string fileName)
        {
            if (File.Exists(fileName))
            {
                try
                {
                    ///Open and read a file&#12290;
                    FileStream fileStream = File.OpenRead(fileName);
                    byte[] archivo = ConvertStreamToByteBuffer(fileStream);
                    fileStream.Close();
                    return archivo;
                }
                catch (Exception ex)
                {
                    return new byte[0];
                }
            }
            else
            {
                return new byte[0];
            }
        }
        public byte[] ConvertStreamToByteBuffer(System.IO.Stream theStream)
        {
            int b1;
            System.IO.MemoryStream tempStream = new System.IO.MemoryStream();
            while ((b1 = theStream.ReadByte()) != -1)
            {
                tempStream.WriteByte(((byte)b1));
            }
            return tempStream.ToArray();
        }


        public bool generar_CFDI(CFDI33.Comprobante oComprobante, int Opcion, string EDICOM_USUARIO, string EDICOM_PASSWORD)
        {
            try
            {
                //Comprimir Archivo
                string Archivo_Tmp = oComprobante.Serie + oComprobante.Folio + ".xml";

                File.Copy(xml.Text, Archivo_Tmp, true);
                string ARCHIVO_zip = Archivo_Tmp.ToUpper().Replace("XML", "ZIP");
                using (ZipFile zip = new ZipFile())
                {
                    zip.AddFile(Archivo_Tmp);
                    zip.Save(ARCHIVO_zip);
                }
                //Fin de Compresion de Archivo
                File.Delete(Archivo_Tmp);

                byte[] ARCHIVO_Leido = ReadBinaryFile(ARCHIVO_zip);
                File.Delete(ARCHIVO_zip);
                CFDiClient oCFDiClient = new CFDiClient();

                byte[] PROCESO;

                switch (Opcion)
                {
                    case 1:
                        try
                        {
                            informacion.Text = "Generando CFDI ";
                            PROCESO = oCFDiClient.getCfdi(EDICOM_USUARIO, EDICOM_PASSWORD, ARCHIVO_Leido);
                            guardar_archivo(PROCESO, oComprobante.Serie + oComprobante.Folio, xml.Text);
                        }
                        catch (FaultException oException)
                        {

                            CFDiException oCFDiException = new CFDiException();
                            MessageBox.Show(oException.Message, Application.ProductName + "-" + Application.ProductVersion, MessageBoxButtons.OK, MessageBoxIcon.Error);
                            return false;
                        }

                        break;
                    case 2:
                        try
                        {
                            informacion.Text = "Generando CFDI Borrador ";
                            PROCESO = oCFDiClient.getCfdiTest(EDICOM_USUARIO, EDICOM_PASSWORD, ARCHIVO_Leido);
                            guardar_archivo(PROCESO, oComprobante.Serie + oComprobante.Folio, xml.Text);
                        }
                        catch (FaultException oException)
                        {

                            CFDiException oCFDiException = new CFDiException();
                            MessageBox.Show(oException.Message, Application.ProductName + "-" + Application.ProductVersion, MessageBoxButtons.OK, MessageBoxIcon.Error);
                            informacion.Text = "Error " + DateTime.Now.ToShortDateString() + " " + DateTime.Now.ToLongTimeString();
                            return false;
                        }
                        break;
                    case 3:
                        try
                        {
                            informacion.Text = "Generando Timbre CFDI  ";
                            PROCESO = oCFDiClient.getTimbreCfdi(EDICOM_USUARIO, EDICOM_PASSWORD, ARCHIVO_Leido);
                        }
                        catch (Exception oException)
                        {

                            CFDiException oCFDiException = new CFDiException();
                            MessageBox.Show(oException.Message, Application.ProductName + "-" + Application.ProductVersion, MessageBoxButtons.OK, MessageBoxIcon.Error);
                            informacion.Text = "Error " + DateTime.Now.ToShortDateString() + " " + DateTime.Now.ToLongTimeString();
                            return false;
                        }
                        break;
                    case 4:
                        try
                        {
                            informacion.Text = "Generando Timbre CFDI Test ";
                            PROCESO = oCFDiClient.getTimbreCfdiTest(EDICOM_USUARIO, EDICOM_PASSWORD, ARCHIVO_Leido);
                        }
                        catch (Exception oException)
                        {

                            CFDiException oCFDiException = new CFDiException();
                            MessageBox.Show(oException.Message, Application.ProductName + "-" + Application.ProductVersion, MessageBoxButtons.OK, MessageBoxIcon.Error);
                            informacion.Text = "Error " + DateTime.Now.ToShortDateString() + " " + DateTime.Now.ToLongTimeString();
                            return false;
                        }
                        break;
                }

                informacion.Text = "Generado y Sellado en Modo Borrador " + DateTime.Now.Day.ToString() + "/" + DateTime.Now.Month.ToString() + "/" + DateTime.Now.Year.ToString() + " " + DateTime.Now.ToString("HH:mm:ss");
                if (Opcion == 1)
                {
                    informacion.Text = informacion.Text.Replace("Borrador", "");
                }


                return true;
            }
            catch (Exception oException)
            {
                MessageBox.Show("Generando CFDI " + oException.Message, Application.ProductName + "-" + Application.ProductVersion, MessageBoxButtons.OK, MessageBoxIcon.Error);
                informacion.Text = "Error " + DateTime.Now.ToShortDateString() + " " + DateTime.Now.ToLongTimeString();
                return false;
            }
        }

        private void guardar_archivo(byte[] PROCESO, string INVOICE_ID, string ARCHIVO)
        {
            //Guardar los PROCESO

            File.WriteAllBytes("TEMPORAL.ZIP", PROCESO);
            //Descomprimir
            String TargetDirectory = "TEMPORAL";
            using (Ionic.Zip.ZipFile zip = Ionic.Zip.ZipFile.Read("TEMPORAL.ZIP"))
            {
                zip.ExtractAll(TargetDirectory, Ionic.Zip.ExtractExistingFileAction.OverwriteSilently);
            }

            //Copiar el ARCHIVO Descrompimido por otro
            string ARCHIVO_descomprimido = TargetDirectory + @"\" + "SIGN_" + INVOICE_ID + ".XML";

            File.Copy(ARCHIVO_descomprimido, ARCHIVO, true);
            xml.Text = ARCHIVO;
            File.Delete(ARCHIVO_descomprimido);
            File.Delete("TEMPORAL.ZIP");

        }

        private void eliminarBD(bool eliminarCabecera = false)
        {
            try
            {

                string conection = dbContext.Database.Connection.ConnectionString;
                cCONEXCION oData = new cCONEXCION(conection);


                string sSQL = "DELETE FROM pagoLineaSet WHERE pago_Id=" + oRegistro.Id.ToString();
                oData.EjecutarConsulta(sSQL);

                oData.DestruirConexion();
            }
            catch (Exception e)
            {
                ErrorFX.mostrar(e, true, true);
            }
        }

        private void btnDeleteItem_Click(object sender, EventArgs e)
        {
            eliminar();
        }

        private void eliminar()
        {
            try
            {

            }
            catch(Exception e)
            {
                ErrorFX.mostrar(e, true, true, "frmComprobantePago - 139 ");
            }
        }

        private void frmComprobantePago_KeyPress(object sender, KeyPressEventArgs e)
        {
            modificado = true;
        }

        private void button1_Click(object sender, EventArgs e)
        {

        }


        private void button2_Click(object sender, EventArgs e)
        {
            frmVENDOR ofrmVENDOR = new frmVENDOR(oData_ERP.sConn);
            if (ofrmVENDOR.ShowDialog() == DialogResult.OK)
            {
                receptor.Text= (string)ofrmVENDOR.selecionados[0].Cells[2].Value;
                receptorNombre.Text =  (string)ofrmVENDOR.selecionados[0].Cells[1].Value;
            }
        }

        private void button5_Click(object sender, EventArgs e)
        {
            cargarSerie();
        }

        private void button4_Click(object sender, EventArgs e)
        {
            //agregarLinea();
        }

        private void button6_Click(object sender, EventArgs e)
        {
            cargarArchivosCFDI();
        }

        private void cargarArchivosCFDI()
        {
            try
            {
                OpenFileDialog openFileDialog1 = new OpenFileDialog();
                openFileDialog1.Reset();
                openFileDialog1.InitialDirectory = "c:\\";
                openFileDialog1.Filter = "XML CFDI (*.xml)|*.xml|Todos los archivos (*.*)|*.*";
                openFileDialog1.FilterIndex = 2;
                openFileDialog1.RestoreDirectory = true;
                if (openFileDialog1.ShowDialog()== DialogResult.OK)
                {
                    foreach (string file in openFileDialog1.FileNames)
                    {
                        cargarCFDI(file);
                    }
                }
            }
            catch (Exception e)
            {
                ErrorFX.mostrar(e, true, true, "frmComprobantePago - 194 ");
            }
        }

        private void cargarCFDI(string archivo)
        {
            try
            {
                XmlSerializer serializer_CFD_3 = new XmlSerializer(typeof(CFDI33.Comprobante));
                TextReader reader = new StreamReader(archivo);
                CFDI33.Comprobante oComprobante;
                try
                {

                    XmlDocument doc = new XmlDocument();
                    doc.Load(archivo);
                    XmlNodeList TimbreFiscalDigital = doc.GetElementsByTagName("tfd:TimbreFiscalDigital");
                    if (TimbreFiscalDigital!=null)
                    {
                        string UUID = "";

                        try
                        {
                            UUID = TimbreFiscalDigital[0].Attributes["UUID"].Value;
                        }
                        catch
                        {

                        }




                        if (!String.IsNullOrEmpty(UUID))
                        {
                            oComprobante = (CFDI33.Comprobante)serializer_CFD_3.Deserialize(reader);
                            if (bool.Parse(ocSERIE.validarComprobantePago))
                            {
                                if (oComprobante.FormaPago != "99")
                                {
                                    ErrorFX.mostrar(" La factura no cumple con Metodo de pago: PPD y Forma de pago: 99 ", true, true,true);
                                    return;
                                }
                                if (oComprobante.MetodoPago != "PPD")
                                {
                                    ErrorFX.mostrar(" La factura no cumple con Metodo de pago: PPD y Forma de pago: 99 ", true, true, true);
                                }
                            }


                            int n = dtgFolio.Rows.Add();
                            dtgFolio.Rows[n].Cells["totalCol"].Value = oComprobante.Total.ToString("N2");
                            dtgFolio.Rows[n].Cells["Aplicado"].Value = oComprobante.Total.ToString("N2");
                            dtgFolio.Rows[n].Cells["monedaCol"].Value = oComprobante.Moneda.ToString();
                            dtgFolio.Rows[n].Cells["fechaCol"].Value = oComprobante.Fecha.ToString("dd/MM/yyyy");
                            dtgFolio.Rows[n].Cells["uuidCol"].Value = UUID;
                            dtgFolio.Rows[n].Cells["saldoCol"].Value = 0;
                            dtgFolio.Rows[n].Cells["NumParcialidad"].Value = 1;
                            dtgFolio.Rows[n].Cells["tipoCambioDRCol"].Value = oComprobante.TipoCambio;
                            string facturatr = "";
                            if (oComprobante.Serie!=null)
                            {
                                facturatr += oComprobante.Serie;
                                dtgFolio.Rows[n].Cells["serieCol"].Value = oComprobante.Serie;
                            }
                            if (oComprobante.Folio != null)
                            {
                                facturatr += oComprobante.Folio;
                                dtgFolio.Rows[n].Cells["folioCol"].Value = oComprobante.Folio;
                            }
                            dtgFolio.Rows[n].Cells["facturaCol"].Value = facturatr;
                            dtgFolio.Rows[n].Cells["ImpSaldoAntCol"].Value = oComprobante.Total.ToString("N2");
                            receptor.Text = oComprobante.Receptor.Rfc;
                            //Rodrigo Escalona 03/08/2020 Actualizar si es extranjero
                            if (oComprobante.Receptor.Rfc == "XEXX010101000")
                            {
                                //Buscar el NomReTriib
                                if (oComprobante.Receptor.NumRegIdTrib != null)
                                {
                                    receptorNumRegIdTrib.Text = oComprobante.Receptor.NumRegIdTrib;
                                }
                            }
                            receptorNombre.Text = oComprobante.Receptor.Nombre;
                            moneda.Text = oComprobante.Moneda;
                            cargarTC();

                           
                        }

                    }
                    actualizarTotal();
                }
                catch (XmlException xmlex)
                {
                    Bitacora.Log("Errpr :: " + xmlex.ToString());
                    return;
                }
                reader.Dispose();
                reader.Close();
            }
            catch (Exception e)
            {
                ErrorFX.mostrar(e, true, true, "frmComprobantePago - 194 ");
            }
        }

        private void actualizarTotal()
        {
            try
            {
                decimal totalTr = 0;

                foreach (DataGridViewRow drv in dtgFolio.Rows)
                {
                    totalTr += decimal.Parse(drv.Cells["Aplicado"].Value.ToString());


                    decimal aplicadoTr = decimal.Parse(drv.Cells["Aplicado"].Value.ToString());
                    decimal totalColTr = decimal.Parse(drv.Cells["ImpSaldoAntCol"].Value.ToString());
                    drv.Cells["saldoCol"].Value = totalColTr - aplicadoTr;
                    
                }

                total.Text = totalTr.ToString("N2");
                dtgFolio.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.DisplayedCells;
            }
            catch (Exception e)
            {
                ErrorFX.mostrar(e, true, true, "frmComprobantePago - 194 ");
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            eliminarLinea();
        }

        private void eliminarLinea()
        {
            try
            {
                dtgFolio.EndEdit();
                if (dtgFolio.SelectedCells.Count > 0)
                {
                    DialogResult Resultado = MessageBox.Show("�Esta seguro de eliminar las l�neas seleccionadas?", Application.ProductName + "-" + Application.ProductVersion, MessageBoxButtons.YesNo);
                    if (Resultado.ToString() != "Yes")
                    {
                        return;
                    }

                    IEnumerable<DataGridViewRow> selectedRows = dtgFolio.SelectedCells.Cast<DataGridViewCell>()
                                           .Select(cell => cell.OwningRow)
                                           .Distinct();
                    
                    foreach (DataGridViewRow drv in selectedRows)
                    {
                        dtgFolio.Rows.Remove(drv);
                    }
                    actualizarTotal();
                }
            }
            catch (Exception e)
            {
                ErrorFX.mostrar(e, true, true, "frmComprobantePago - 712 ");
            }
        }

        private void emisor_SelectedIndexChanged_1(object sender, EventArgs e)
        {
            cargarSerie();
        }

        private void cargarSerie()
        {
            try
            {
                serie.Text = "";
                ocEMPRESA = new cEMPRESA();
                ocSERIE = new cSERIE();
                ocEMPRESA = (cEMPRESA)emisor.SelectedItem;
                oData_ERP = new cCONEXCION(ocEMPRESA.TIPO, ocEMPRESA.SERVIDOR, ocEMPRESA.BD, ocEMPRESA.USUARIO_BD, ocEMPRESA.PASSWORD_BD);
                foreach (cSERIE registro in ocSERIE.todos_empresa("", ocEMPRESA.ROW_ID,true, false))
                {
                    ocCERTIFICADO = new cCERTIFICADO();
                    ocCERTIFICADO.cargar(registro.ROW_ID_CERTIFICADO);
                    ocSERIE = registro;
                    serie.Text = ocSERIE.ID + ocSERIE.consecutivo(ocSERIE.ROW_ID, false);
                }
                cargarTC();
            }
            catch (Exception e)
            {
                ErrorFX.mostrar(e, true, true, "frmComprobantePago - 113 ");
            }
        }

        private void cargarTC()
        {
            try
            {
                ocEMPRESA = (cEMPRESA)emisor.SelectedItem;
                cCONEXCION oData = new cCONEXCION(ocEMPRESA.SERVIDOR, ocEMPRESA.BD, ocEMPRESA.USUARIO_BD, ocEMPRESA.PASSWORD_BD);
                tipoCambio.Text = "1";
                if (moneda.Text.Contains("USD"))
                {
                    string sSQL = "";
                    sSQL = @"SELECT TOP 1 *
                    FROM CURRENCY_EXCHANGE
                    WHERE (EFFECTIVE_DATE <= " + oData.convertir_fecha(fechaPago.Value) + @")
                    AND CURRENCY_ID='USD' 
                    ORDER BY EFFECTIVE_DATE DESC";
                    DataTable oDataTable = oData.EjecutarConsulta(sSQL);
                    if (oDataTable != null)
                    {
                        foreach (DataRow oDataRow in oDataTable.Rows)
                        {
                            //A�adir el Registro
                            tipoCambio.Text = double.Parse(oDataRow["SELL_RATE"].ToString()).ToString("N4");
                        }
                    }
                }
            }
            catch (Exception e)
            {
                ErrorFX.mostrar(e, true, false, "frmComprobantePago - 687 - ");
            }
        }

        private void btnImprimir_Click(object sender, EventArgs e)
        {
            imprimir();
        }

        private void imprimir()
        {
            cComprobantePago ocComprobantePago = new cComprobantePago();
            ocComprobantePago.imprimir(oRegistro, null,true);
            //Cargar el PDF
            ModeloComprobantePago.documentosfxEntities dbContextTr = new documentosfxEntities();
            pago oRegistroTr = dbContextTr.pagoSet.Where(a => a.Id == oRegistro.Id).FirstOrDefault();
            if (oRegistroTr!=null)
            {
                pdf.Text = oRegistroTr.pdf;
            }
            dbContextTr.Dispose();


        }

        private void dtgFolio_CellEnter(object sender, DataGridViewCellEventArgs e)
        {
            if ((dtgFolio.CurrentCell.ColumnIndex == 7)
                | (dtgFolio.CurrentCell.ColumnIndex == 8)
                | (dtgFolio.CurrentCell.ColumnIndex == 9))
            {
                dtgFolio.BeginEdit(true);
            }
        }

        private void dtgFolio_EditingControlShowing(object sender, DataGridViewEditingControlShowingEventArgs e)
        {
            e.Control.KeyPress -= new KeyPressEventHandler(Column1_KeyPress);
            if ((dtgFolio.CurrentCell.ColumnIndex == 7)
                |
                    (dtgFolio.CurrentCell.ColumnIndex == 8)
                    | (dtgFolio.CurrentCell.ColumnIndex == 9))
            {
                TextBox tb = e.Control as TextBox;
                if (tb != null)
                {
                    tb.KeyPress += new KeyPressEventHandler(Column1_KeyPress);
                }
            }

        }

        private void Column1_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar)
                 && e.KeyChar != '.')
            {
                e.Handled = true;
            }

            // only allow one decimal point
            if (e.KeyChar == '.'
                && (sender as TextBox).Text.IndexOf('.') > -1)
            {
                e.Handled = true;
            }
        }

        private void dtgFolio_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {
            actualizarTotal();
        }

        private void btnXML_Click(object sender, EventArgs e)
        {
            verArchivo(xml.Text);
        }

        private void verArchivo(string archivo)
        {
            if (!File.Exists(archivo))
            {
                return;
            }
            string directorio = AppDomain.CurrentDomain.BaseDirectory;
            frmFacturaXML oObjeto = new frmFacturaXML(archivo);
            oObjeto.Show();
        }

        private void verArchivoPDF(string archivo)
        {
            if (!File.Exists(archivo))
            {
                return;
            }
            string directorio = AppDomain.CurrentDomain.BaseDirectory;
            frmPDF oObjeto = new frmPDF(archivo);
            oObjeto.Show();
        }

        private void btnPDF_Click(object sender, EventArgs e)
        {
            verArchivoPDF(pdf.Text);
        }

        private void button1_Click_1(object sender, EventArgs e)
        {
            //Cargar 
            frmComprobanteCambioForma ofrmComprobanteCambioForma = new frmComprobanteCambioForma(null);
            if(ofrmComprobanteCambioForma.ShowDialog()== DialogResult.OK)
            {
                if (!String.IsNullOrEmpty(ofrmComprobanteCambioForma.oRegistroP.formaDePagoP))
                {
                    oRegistro.formaDePagoP = ofrmComprobanteCambioForma.oRegistroP.formaDePagoP;
                    
                }
                if (!String.IsNullOrEmpty(ofrmComprobanteCambioForma.oRegistroP.NomBancoOrdExt))
                {
                    oRegistro.NomBancoOrdExt = ofrmComprobanteCambioForma.oRegistroP.NomBancoOrdExt;
                }
                if (!String.IsNullOrEmpty(ofrmComprobanteCambioForma.oRegistroP.NumOperacion))
                {
                    oRegistro.NumOperacion = ofrmComprobanteCambioForma.oRegistroP.NumOperacion;
                }
                if (!String.IsNullOrEmpty(ofrmComprobanteCambioForma.oRegistroP.CtaBeneficiario))
                {
                    oRegistro.CtaBeneficiario = ofrmComprobanteCambioForma.oRegistroP.CtaBeneficiario;
                }
                if (!String.IsNullOrEmpty(ofrmComprobanteCambioForma.oRegistroP.CtaOrdenante))
                {
                    oRegistro.CtaOrdenante = ofrmComprobanteCambioForma.oRegistroP.CtaOrdenante;
                }
                if (!String.IsNullOrEmpty(ofrmComprobanteCambioForma.oRegistroP.RfcEmisorCtaBen))
                {
                    oRegistro.RfcEmisorCtaBen = ofrmComprobanteCambioForma.oRegistroP.RfcEmisorCtaBen;
                }
                if (!String.IsNullOrEmpty(ofrmComprobanteCambioForma.oRegistroP.RfcEmisorCtaOrd))
                {
                    oRegistro.RfcEmisorCtaOrd = ofrmComprobanteCambioForma.oRegistroP.RfcEmisorCtaOrd;
                }
                oRegistro.editado = ofrmComprobanteCambioForma.oRegistroP.editado;
            }
        }

        private void button7_Click(object sender, EventArgs e)
        {
            cargarFacturas();
        }

        private void cargarFacturas()
        {

            int n = dtgTipoRelacion.Rows.Add();
            dtgTipoRelacion.Rows[n].Cells["uuidRelacion"].Value = "";
                
            
        }


        private void eliminarLineaTipoRelacion()
        {
            try
            {
                dtgTipoRelacion.EndEdit();
                if (dtgTipoRelacion.SelectedCells.Count > 0)
                {
                    DialogResult Resultado = MessageBox.Show("�Esta seguro de eliminar los uuid relacionados seleccionados?", Application.ProductName + "-" + Application.ProductVersion, MessageBoxButtons.YesNo);
                    if (Resultado.ToString() != "Yes")
                    {
                        return;
                    }

                    IEnumerable<DataGridViewRow> selectedRows = dtgTipoRelacion.SelectedCells.Cast<DataGridViewCell>()
                                           .Select(cell => cell.OwningRow)
                                           .Distinct();

                    foreach (DataGridViewRow drv in selectedRows)
                    {
                        dtgTipoRelacion.Rows.Remove(drv);
                    }
                }
            }
            catch (Exception e)
            {
                ErrorFX.mostrar(e, true, true, "frmComprobantePago - 1058 ");
            }
        }

        private void button4_Click_1(object sender, EventArgs e)
        {
            eliminarLineaTipoRelacion();
        }
    }
}
