#region Copyright Syncfusion Inc. 2001 - 2017
// Copyright Syncfusion Inc. 2001 - 2017. All rights reserved.
// Use of this code is subject to the terms of our license.
// A copy of the current license can be obtained at any time by e-mailing
// licensing@syncfusion.com. Any infringement will be prosecuted under
// applicable laws. 
#endregion
namespace FE_FX.ComprobantePago
{
    partial class frmEditarComprobantePago
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle29 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle33 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle34 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle30 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle31 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle32 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle35 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle41 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle42 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle36 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle37 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle38 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle39 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle40 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmEditarComprobantePago));
            this.btnAddItem = new System.Windows.Forms.Button();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.DtgImpuesto = new System.Windows.Forms.DataGridView();
            this.BaseDR = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ImpuestoDR = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.TipoFactorDR = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.TasaOCuotaDR = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ImporteDR = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.MonedaPago = new System.Windows.Forms.ComboBox();
            this.button4 = new System.Windows.Forms.Button();
            this.label16 = new System.Windows.Forms.Label();
            this.total = new System.Windows.Forms.TextBox();
            this.label15 = new System.Windows.Forms.Label();
            this.button3 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.dtgrdGeneral = new System.Windows.Forms.DataGridView();
            this.factura = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.UUID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Moneda = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.NumParcialidad = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ImpSaldoAnt = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ImpPagado = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ImpSaldoInsoluto = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.TipoCambioDR = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.EquivalenciaDR = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.label3 = new System.Windows.Forms.Label();
            this.RfcEmisorCtaOrd = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.RfcEmisorCtaBen = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.CtaOrdenante = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.tipoCambioP = new System.Windows.Forms.TextBox();
            this.label13 = new System.Windows.Forms.Label();
            this.CtaBeneficiario = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.RegimenFiscalReceptor = new System.Windows.Forms.TextBox();
            this.DomicilioFiscalReceptor = new System.Windows.Forms.TextBox();
            this.receptorNombre = new System.Windows.Forms.TextBox();
            this.receptor = new System.Windows.Forms.TextBox();
            this.NumOperacion = new System.Windows.Forms.TextBox();
            this.label18 = new System.Windows.Forms.Label();
            this.FORMA_DE_PAGO = new System.Windows.Forms.ComboBox();
            this.label17 = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.NomBancoOrdExt = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.editado = new System.Windows.Forms.CheckBox();
            this.button5 = new System.Windows.Forms.Button();
            this.button6 = new System.Windows.Forms.Button();
            this.tabControl1.SuspendLayout();
            this.tabPage2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DtgImpuesto)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtgrdGeneral)).BeginInit();
            this.tabPage1.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnAddItem
            // 
            this.btnAddItem.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(153)))), ((int)(((byte)(181)))), ((int)(((byte)(105)))));
            this.btnAddItem.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnAddItem.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAddItem.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnAddItem.Location = new System.Drawing.Point(12, 12);
            this.btnAddItem.Name = "btnAddItem";
            this.btnAddItem.Size = new System.Drawing.Size(76, 28);
            this.btnAddItem.TabIndex = 20;
            this.btnAddItem.Text = "Guardar";
            this.btnAddItem.UseVisualStyleBackColor = false;
            this.btnAddItem.Click += new System.EventHandler(this.btnAddItem_Click);
            // 
            // tabControl1
            // 
            this.tabControl1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Location = new System.Drawing.Point(12, 46);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(976, 518);
            this.tabControl1.TabIndex = 21;
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.button6);
            this.tabPage2.Controls.Add(this.DtgImpuesto);
            this.tabPage2.Controls.Add(this.MonedaPago);
            this.tabPage2.Controls.Add(this.button5);
            this.tabPage2.Controls.Add(this.button4);
            this.tabPage2.Controls.Add(this.label16);
            this.tabPage2.Controls.Add(this.total);
            this.tabPage2.Controls.Add(this.label15);
            this.tabPage2.Controls.Add(this.button3);
            this.tabPage2.Controls.Add(this.button2);
            this.tabPage2.Controls.Add(this.button1);
            this.tabPage2.Controls.Add(this.dtgrdGeneral);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(968, 492);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "L�neas de Pago";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // DtgImpuesto
            // 
            this.DtgImpuesto.AllowUserToAddRows = false;
            this.DtgImpuesto.AllowUserToDeleteRows = false;
            this.DtgImpuesto.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.DtgImpuesto.BackgroundColor = System.Drawing.Color.White;
            this.DtgImpuesto.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            dataGridViewCellStyle29.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle29.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle29.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle29.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle29.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle29.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle29.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.DtgImpuesto.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle29;
            this.DtgImpuesto.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DtgImpuesto.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.BaseDR,
            this.ImpuestoDR,
            this.TipoFactorDR,
            this.TasaOCuotaDR,
            this.ImporteDR});
            dataGridViewCellStyle33.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle33.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle33.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle33.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle33.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle33.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle33.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.DtgImpuesto.DefaultCellStyle = dataGridViewCellStyle33;
            this.DtgImpuesto.Location = new System.Drawing.Point(3, 362);
            this.DtgImpuesto.Name = "DtgImpuesto";
            dataGridViewCellStyle34.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle34.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle34.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle34.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle34.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle34.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle34.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.DtgImpuesto.RowHeadersDefaultCellStyle = dataGridViewCellStyle34;
            this.DtgImpuesto.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.DtgImpuesto.Size = new System.Drawing.Size(959, 124);
            this.DtgImpuesto.TabIndex = 452;
            // 
            // BaseDR
            // 
            dataGridViewCellStyle30.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle30.Format = "N4";
            dataGridViewCellStyle30.NullValue = "0";
            this.BaseDR.DefaultCellStyle = dataGridViewCellStyle30;
            this.BaseDR.HeaderText = "BaseDR";
            this.BaseDR.Name = "BaseDR";
            // 
            // ImpuestoDR
            // 
            dataGridViewCellStyle31.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle31.Format = "N4";
            dataGridViewCellStyle31.NullValue = "0";
            this.ImpuestoDR.DefaultCellStyle = dataGridViewCellStyle31;
            this.ImpuestoDR.HeaderText = "ImpuestoDR";
            this.ImpuestoDR.Name = "ImpuestoDR";
            // 
            // TipoFactorDR
            // 
            this.TipoFactorDR.HeaderText = "TipoFactorDR";
            this.TipoFactorDR.Name = "TipoFactorDR";
            // 
            // TasaOCuotaDR
            // 
            this.TasaOCuotaDR.HeaderText = "TasaOCuotaDR";
            this.TasaOCuotaDR.Name = "TasaOCuotaDR";
            // 
            // ImporteDR
            // 
            dataGridViewCellStyle32.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle32.Format = "N4";
            dataGridViewCellStyle32.NullValue = "0";
            this.ImporteDR.DefaultCellStyle = dataGridViewCellStyle32;
            this.ImporteDR.HeaderText = "ImporteDR";
            this.ImporteDR.Name = "ImporteDR";
            // 
            // MonedaPago
            // 
            this.MonedaPago.FormattingEnabled = true;
            this.MonedaPago.Items.AddRange(new object[] {
            "USD",
            "MXN"});
            this.MonedaPago.Location = new System.Drawing.Point(290, 10);
            this.MonedaPago.Name = "MonedaPago";
            this.MonedaPago.Size = new System.Drawing.Size(70, 21);
            this.MonedaPago.TabIndex = 448;
            this.MonedaPago.SelectedIndexChanged += new System.EventHandler(this.MonedaPago_SelectedIndexChanged);
            // 
            // button4
            // 
            this.button4.BackColor = System.Drawing.Color.LightSeaGreen;
            this.button4.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button4.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button4.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.button4.Location = new System.Drawing.Point(6, 328);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(181, 28);
            this.button4.TabIndex = 20;
            this.button4.Text = "Calcular impuestos";
            this.button4.UseVisualStyleBackColor = false;
            this.button4.Click += new System.EventHandler(this.button4_Click);
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(238, 14);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(46, 13);
            this.label16.TabIndex = 443;
            this.label16.Text = "Moneda";
            // 
            // total
            // 
            this.total.Location = new System.Drawing.Point(114, 11);
            this.total.Name = "total";
            this.total.Size = new System.Drawing.Size(124, 20);
            this.total.TabIndex = 441;
            this.total.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(77, 15);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(31, 13);
            this.label15.TabIndex = 440;
            this.label15.Text = "Total";
            // 
            // button3
            // 
            this.button3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(219)))), ((int)(((byte)(94)))), ((int)(((byte)(94)))));
            this.button3.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button3.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button3.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.button3.Location = new System.Drawing.Point(41, 6);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(31, 28);
            this.button3.TabIndex = 439;
            this.button3.Text = "-";
            this.button3.UseVisualStyleBackColor = false;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // button2
            // 
            this.button2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(153)))), ((int)(((byte)(181)))), ((int)(((byte)(105)))));
            this.button2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button2.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button2.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.button2.Location = new System.Drawing.Point(366, 5);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(76, 28);
            this.button2.TabIndex = 438;
            this.button2.Text = "Calcular";
            this.button2.UseVisualStyleBackColor = false;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // button1
            // 
            this.button1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(153)))), ((int)(((byte)(181)))), ((int)(((byte)(105)))));
            this.button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button1.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button1.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.button1.Location = new System.Drawing.Point(6, 6);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(31, 28);
            this.button1.TabIndex = 438;
            this.button1.Text = "+";
            this.button1.UseVisualStyleBackColor = false;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // dtgrdGeneral
            // 
            this.dtgrdGeneral.AllowUserToAddRows = false;
            this.dtgrdGeneral.AllowUserToDeleteRows = false;
            this.dtgrdGeneral.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dtgrdGeneral.BackgroundColor = System.Drawing.Color.White;
            this.dtgrdGeneral.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            dataGridViewCellStyle35.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle35.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle35.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle35.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle35.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle35.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle35.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dtgrdGeneral.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle35;
            this.dtgrdGeneral.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dtgrdGeneral.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.factura,
            this.UUID,
            this.Moneda,
            this.NumParcialidad,
            this.ImpSaldoAnt,
            this.ImpPagado,
            this.ImpSaldoInsoluto,
            this.TipoCambioDR,
            this.EquivalenciaDR});
            dataGridViewCellStyle41.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle41.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle41.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle41.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle41.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle41.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle41.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dtgrdGeneral.DefaultCellStyle = dataGridViewCellStyle41;
            this.dtgrdGeneral.Location = new System.Drawing.Point(3, 40);
            this.dtgrdGeneral.Name = "dtgrdGeneral";
            dataGridViewCellStyle42.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle42.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle42.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle42.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle42.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle42.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle42.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dtgrdGeneral.RowHeadersDefaultCellStyle = dataGridViewCellStyle42;
            this.dtgrdGeneral.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dtgrdGeneral.Size = new System.Drawing.Size(956, 282);
            this.dtgrdGeneral.TabIndex = 437;
            this.dtgrdGeneral.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dtgrdGeneral_CellClick);
            this.dtgrdGeneral.CellEndEdit += new System.Windows.Forms.DataGridViewCellEventHandler(this.dtgrdGeneral_CellEndEdit);
            this.dtgrdGeneral.DataBindingComplete += new System.Windows.Forms.DataGridViewBindingCompleteEventHandler(this.dtgrdGeneral_DataBindingComplete);
            // 
            // factura
            // 
            this.factura.HeaderText = "Factura";
            this.factura.Name = "factura";
            // 
            // UUID
            // 
            this.UUID.HeaderText = "UUID";
            this.UUID.Name = "UUID";
            // 
            // Moneda
            // 
            this.Moneda.HeaderText = "Moneda";
            this.Moneda.Name = "Moneda";
            // 
            // NumParcialidad
            // 
            this.NumParcialidad.HeaderText = "NumParcialidad";
            this.NumParcialidad.Name = "NumParcialidad";
            // 
            // ImpSaldoAnt
            // 
            dataGridViewCellStyle36.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle36.Format = "N4";
            dataGridViewCellStyle36.NullValue = "0";
            this.ImpSaldoAnt.DefaultCellStyle = dataGridViewCellStyle36;
            this.ImpSaldoAnt.HeaderText = "ImpSaldoAnt";
            this.ImpSaldoAnt.Name = "ImpSaldoAnt";
            // 
            // ImpPagado
            // 
            dataGridViewCellStyle37.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle37.Format = "N4";
            dataGridViewCellStyle37.NullValue = "0";
            this.ImpPagado.DefaultCellStyle = dataGridViewCellStyle37;
            this.ImpPagado.HeaderText = "ImpPagado";
            this.ImpPagado.Name = "ImpPagado";
            // 
            // ImpSaldoInsoluto
            // 
            dataGridViewCellStyle38.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle38.Format = "N4";
            dataGridViewCellStyle38.NullValue = "0";
            this.ImpSaldoInsoluto.DefaultCellStyle = dataGridViewCellStyle38;
            this.ImpSaldoInsoluto.HeaderText = "ImpSaldoInsoluto";
            this.ImpSaldoInsoluto.Name = "ImpSaldoInsoluto";
            // 
            // TipoCambioDR
            // 
            dataGridViewCellStyle39.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle39.Format = "N4";
            dataGridViewCellStyle39.NullValue = "0";
            this.TipoCambioDR.DefaultCellStyle = dataGridViewCellStyle39;
            this.TipoCambioDR.HeaderText = "TipoCambioDR";
            this.TipoCambioDR.Name = "TipoCambioDR";
            this.TipoCambioDR.Visible = false;
            // 
            // EquivalenciaDR
            // 
            dataGridViewCellStyle40.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle40.NullValue = "0";
            this.EquivalenciaDR.DefaultCellStyle = dataGridViewCellStyle40;
            this.EquivalenciaDR.HeaderText = "EquivalenciaDR";
            this.EquivalenciaDR.Name = "EquivalenciaDR";
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.label3);
            this.tabPage1.Controls.Add(this.RfcEmisorCtaOrd);
            this.tabPage1.Controls.Add(this.label1);
            this.tabPage1.Controls.Add(this.RfcEmisorCtaBen);
            this.tabPage1.Controls.Add(this.label8);
            this.tabPage1.Controls.Add(this.CtaOrdenante);
            this.tabPage1.Controls.Add(this.label11);
            this.tabPage1.Controls.Add(this.label7);
            this.tabPage1.Controls.Add(this.label12);
            this.tabPage1.Controls.Add(this.tipoCambioP);
            this.tabPage1.Controls.Add(this.label13);
            this.tabPage1.Controls.Add(this.CtaBeneficiario);
            this.tabPage1.Controls.Add(this.label10);
            this.tabPage1.Controls.Add(this.label6);
            this.tabPage1.Controls.Add(this.label9);
            this.tabPage1.Controls.Add(this.RegimenFiscalReceptor);
            this.tabPage1.Controls.Add(this.DomicilioFiscalReceptor);
            this.tabPage1.Controls.Add(this.receptorNombre);
            this.tabPage1.Controls.Add(this.receptor);
            this.tabPage1.Controls.Add(this.NumOperacion);
            this.tabPage1.Controls.Add(this.label18);
            this.tabPage1.Controls.Add(this.FORMA_DE_PAGO);
            this.tabPage1.Controls.Add(this.label17);
            this.tabPage1.Controls.Add(this.label22);
            this.tabPage1.Controls.Add(this.label21);
            this.tabPage1.Controls.Add(this.label5);
            this.tabPage1.Controls.Add(this.label2);
            this.tabPage1.Controls.Add(this.NomBancoOrdExt);
            this.tabPage1.Controls.Add(this.label4);
            this.tabPage1.Controls.Add(this.label14);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(968, 492);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Informaci�n General";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(8, 35);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(79, 13);
            this.label3.TabIndex = 0;
            this.label3.Text = "Forma de Pago";
            // 
            // RfcEmisorCtaOrd
            // 
            this.RfcEmisorCtaOrd.Location = new System.Drawing.Point(105, 165);
            this.RfcEmisorCtaOrd.Name = "RfcEmisorCtaOrd";
            this.RfcEmisorCtaOrd.Size = new System.Drawing.Size(384, 20);
            this.RfcEmisorCtaOrd.TabIndex = 6;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(8, 246);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(92, 13);
            this.label1.TabIndex = 8;
            this.label1.Text = "NomBancoOrdExt";
            // 
            // RfcEmisorCtaBen
            // 
            this.RfcEmisorCtaBen.Location = new System.Drawing.Point(105, 321);
            this.RfcEmisorCtaBen.Name = "RfcEmisorCtaBen";
            this.RfcEmisorCtaBen.Size = new System.Drawing.Size(765, 20);
            this.RfcEmisorCtaBen.TabIndex = 15;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(105, 266);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(470, 13);
            this.label8.TabIndex = 10;
            this.label8.Text = "Puede conformarse desde 1 hasta 300 caracteres.  Ejemplo:  NomBancoOrdExt= BANK O" +
    "F TOKY";
            // 
            // CtaOrdenante
            // 
            this.CtaOrdenante.Location = new System.Drawing.Point(105, 282);
            this.CtaOrdenante.Name = "CtaOrdenante";
            this.CtaOrdenante.Size = new System.Drawing.Size(765, 20);
            this.CtaOrdenante.TabIndex = 12;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(105, 305);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(478, 13);
            this.label11.TabIndex = 13;
            this.label11.Text = "Puede conformarse desde 10 hasta 50 caracteres. Ejemplo: CtaOrdenante= 1234567891" +
    "01112131 ";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(8, 168);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(88, 13);
            this.label7.TabIndex = 5;
            this.label7.Text = "RfcEmisorCtaOrd";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(105, 344);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(765, 26);
            this.label12.TabIndex = 16;
            this.label12.Text = resources.GetString("label12.Text");
            // 
            // tipoCambioP
            // 
            this.tipoCambioP.Location = new System.Drawing.Point(105, 412);
            this.tipoCambioP.Name = "tipoCambioP";
            this.tipoCambioP.Size = new System.Drawing.Size(76, 20);
            this.tipoCambioP.TabIndex = 18;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(105, 396);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(486, 13);
            this.label13.TabIndex = 19;
            this.label13.Text = "Puede conformarse desde 10 hasta 50 caracteres. Ejemplo: CtaBeneficiario = 123456" +
    "789101114558 ";
            // 
            // CtaBeneficiario
            // 
            this.CtaBeneficiario.Location = new System.Drawing.Point(105, 373);
            this.CtaBeneficiario.Name = "CtaBeneficiario";
            this.CtaBeneficiario.Size = new System.Drawing.Size(384, 20);
            this.CtaBeneficiario.TabIndex = 18;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(105, 188);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(761, 52);
            this.label10.TabIndex = 7;
            this.label10.Text = resources.GetString("label10.Text");
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(8, 324);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(90, 13);
            this.label6.TabIndex = 14;
            this.label6.Text = "RfcEmisorCtaBen";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(11, 82);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(929, 65);
            this.label9.TabIndex = 4;
            this.label9.Text = resources.GetString("label9.Text");
            // 
            // RegimenFiscalReceptor
            // 
            this.RegimenFiscalReceptor.Location = new System.Drawing.Point(865, 6);
            this.RegimenFiscalReceptor.Name = "RegimenFiscalReceptor";
            this.RegimenFiscalReceptor.Size = new System.Drawing.Size(58, 20);
            this.RegimenFiscalReceptor.TabIndex = 3;
            // 
            // DomicilioFiscalReceptor
            // 
            this.DomicilioFiscalReceptor.Location = new System.Drawing.Point(716, 6);
            this.DomicilioFiscalReceptor.Name = "DomicilioFiscalReceptor";
            this.DomicilioFiscalReceptor.Size = new System.Drawing.Size(58, 20);
            this.DomicilioFiscalReceptor.TabIndex = 3;
            // 
            // receptorNombre
            // 
            this.receptorNombre.Location = new System.Drawing.Point(300, 6);
            this.receptorNombre.Name = "receptorNombre";
            this.receptorNombre.Size = new System.Drawing.Size(324, 20);
            this.receptorNombre.TabIndex = 3;
            // 
            // receptor
            // 
            this.receptor.Location = new System.Drawing.Point(105, 6);
            this.receptor.Name = "receptor";
            this.receptor.Size = new System.Drawing.Size(132, 20);
            this.receptor.TabIndex = 3;
            // 
            // NumOperacion
            // 
            this.NumOperacion.Location = new System.Drawing.Point(105, 59);
            this.NumOperacion.Name = "NumOperacion";
            this.NumOperacion.Size = new System.Drawing.Size(384, 20);
            this.NumOperacion.TabIndex = 3;
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(780, 9);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(79, 13);
            this.label18.TabIndex = 2;
            this.label18.Text = "Regimen Fiscal";
            this.label18.Click += new System.EventHandler(this.label22_Click);
            // 
            // FORMA_DE_PAGO
            // 
            this.FORMA_DE_PAGO.FormattingEnabled = true;
            this.FORMA_DE_PAGO.Items.AddRange(new object[] {
            "Pago en una sola exhibici�n",
            "Pago en parcialidades",
            "Pago a credito"});
            this.FORMA_DE_PAGO.Location = new System.Drawing.Point(105, 32);
            this.FORMA_DE_PAGO.Name = "FORMA_DE_PAGO";
            this.FORMA_DE_PAGO.Size = new System.Drawing.Size(384, 21);
            this.FORMA_DE_PAGO.TabIndex = 1;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(630, 9);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(82, 13);
            this.label17.TabIndex = 2;
            this.label17.Text = "Domicilio Fiscal ";
            this.label17.Click += new System.EventHandler(this.label22_Click);
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Location = new System.Drawing.Point(243, 9);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(51, 13);
            this.label22.TabIndex = 2;
            this.label22.Text = "Receptor";
            this.label22.Click += new System.EventHandler(this.label22_Click);
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(8, 9);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(28, 13);
            this.label21.TabIndex = 2;
            this.label21.Text = "RFC";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(8, 285);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(73, 13);
            this.label5.TabIndex = 11;
            this.label5.Text = "CtaOrdenante";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(8, 62);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(78, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "NumOperacion";
            // 
            // NomBancoOrdExt
            // 
            this.NomBancoOrdExt.Location = new System.Drawing.Point(105, 243);
            this.NomBancoOrdExt.Name = "NomBancoOrdExt";
            this.NomBancoOrdExt.Size = new System.Drawing.Size(765, 20);
            this.NomBancoOrdExt.TabIndex = 9;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(9, 376);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(78, 13);
            this.label4.TabIndex = 17;
            this.label4.Text = "CtaBeneficiario";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(11, 415);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(80, 13);
            this.label14.TabIndex = 17;
            this.label14.Text = "Tipo de cambio";
            // 
            // editado
            // 
            this.editado.AutoSize = true;
            this.editado.Location = new System.Drawing.Point(96, 19);
            this.editado.Name = "editado";
            this.editado.Size = new System.Drawing.Size(119, 17);
            this.editado.TabIndex = 22;
            this.editado.Text = "Editar manualmente";
            this.editado.UseVisualStyleBackColor = true;
            // 
            // button5
            // 
            this.button5.BackColor = System.Drawing.Color.DarkGoldenrod;
            this.button5.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button5.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button5.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.button5.Location = new System.Drawing.Point(193, 328);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(181, 28);
            this.button5.TabIndex = 20;
            this.button5.Text = "Guardar impuestos";
            this.button5.UseVisualStyleBackColor = false;
            this.button5.Click += new System.EventHandler(this.button5_Click);
            // 
            // button6
            // 
            this.button6.BackColor = System.Drawing.Color.DarkOrange;
            this.button6.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button6.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button6.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.button6.Location = new System.Drawing.Point(448, 5);
            this.button6.Name = "button6";
            this.button6.Size = new System.Drawing.Size(205, 28);
            this.button6.TabIndex = 453;
            this.button6.Text = "Calcular impuestos de las lineas seleccionadas";
            this.button6.UseVisualStyleBackColor = false;
            this.button6.Click += new System.EventHandler(this.button6_Click);
            // 
            // frmEditarComprobantePago
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1000, 583);
            this.Controls.Add(this.editado);
            this.Controls.Add(this.tabControl1);
            this.Controls.Add(this.btnAddItem);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MetroColor = System.Drawing.Color.Transparent;
            this.Name = "frmEditarComprobantePago";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Editar";
            this.Load += new System.EventHandler(this.frmEditarComprobantePago_Load);
            this.tabControl1.ResumeLayout(false);
            this.tabPage2.ResumeLayout(false);
            this.tabPage2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DtgImpuesto)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtgrdGeneral)).EndInit();
            this.tabPage1.ResumeLayout(false);
            this.tabPage1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Button btnAddItem;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox RfcEmisorCtaOrd;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox RfcEmisorCtaBen;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox CtaOrdenante;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.TextBox tipoCambioP;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.TextBox CtaBeneficiario;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox NumOperacion;
        private System.Windows.Forms.ComboBox FORMA_DE_PAGO;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox NomBancoOrdExt;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.DataGridView dtgrdGeneral;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.TextBox total;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.ComboBox MonedaPago;
        private System.Windows.Forms.TextBox receptor;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.TextBox receptorNombre;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.DataGridView DtgImpuesto;
        private System.Windows.Forms.CheckBox editado;
        private System.Windows.Forms.DataGridViewTextBoxColumn BaseDR;
        private System.Windows.Forms.DataGridViewTextBoxColumn ImpuestoDR;
        private System.Windows.Forms.DataGridViewTextBoxColumn TipoFactorDR;
        private System.Windows.Forms.DataGridViewTextBoxColumn TasaOCuotaDR;
        private System.Windows.Forms.DataGridViewTextBoxColumn ImporteDR;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.TextBox DomicilioFiscalReceptor;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.TextBox RegimenFiscalReceptor;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.DataGridViewTextBoxColumn factura;
        private System.Windows.Forms.DataGridViewTextBoxColumn UUID;
        private System.Windows.Forms.DataGridViewTextBoxColumn Moneda;
        private System.Windows.Forms.DataGridViewTextBoxColumn NumParcialidad;
        private System.Windows.Forms.DataGridViewTextBoxColumn ImpSaldoAnt;
        private System.Windows.Forms.DataGridViewTextBoxColumn ImpPagado;
        private System.Windows.Forms.DataGridViewTextBoxColumn ImpSaldoInsoluto;
        private System.Windows.Forms.DataGridViewTextBoxColumn TipoCambioDR;
        private System.Windows.Forms.DataGridViewTextBoxColumn EquivalenciaDR;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button5;
        private System.Windows.Forms.Button button6;
    }
}