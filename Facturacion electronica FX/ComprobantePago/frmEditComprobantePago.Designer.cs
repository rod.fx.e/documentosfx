#region Copyright Syncfusion Inc. 2001 - 2017
// Copyright Syncfusion Inc. 2001 - 2017. All rights reserved.
// Use of this code is subject to the terms of our license.
// A copy of the current license can be obtained at any time by e-mailing
// licensing@syncfusion.com. Any infringement will be prosecuted under
// applicable laws. 
#endregion
namespace FE_FX.ComprobantePago
{
    partial class frmEditComprobantePago
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmEditComprobantePago));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle7 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            this.btnAddItem = new System.Windows.Forms.Button();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.label3 = new System.Windows.Forms.Label();
            this.RfcEmisorCtaOrd = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.RfcEmisorCtaBen = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.CtaOrdenante = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.tipoCambioP = new System.Windows.Forms.TextBox();
            this.label13 = new System.Windows.Forms.Label();
            this.CtaBeneficiario = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.receptorNombre = new System.Windows.Forms.TextBox();
            this.receptor = new System.Windows.Forms.TextBox();
            this.NumOperacion = new System.Windows.Forms.TextBox();
            this.FORMA_DE_PAGO = new System.Windows.Forms.ComboBox();
            this.label22 = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.NomBancoOrdExt = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.MonedaPago = new System.Windows.Forms.ComboBox();
            this.textBox2 = new System.Windows.Forms.TextBox();
            this.label17 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.total = new System.Windows.Forms.TextBox();
            this.label15 = new System.Windows.Forms.Label();
            this.button3 = new System.Windows.Forms.Button();
            this.button5 = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.dtgrdGeneral = new System.Windows.Forms.DataGridView();
            this.factura = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.UUID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Moneda = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.NumParcialidad = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ImpSaldoAnt = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ImpPagado = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ImpSaldoInsoluto = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.TipoCambioDR = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.tabPage2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dtgrdGeneral)).BeginInit();
            this.SuspendLayout();
            // 
            // btnAddItem
            // 
            this.btnAddItem.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(153)))), ((int)(((byte)(181)))), ((int)(((byte)(105)))));
            this.btnAddItem.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnAddItem.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAddItem.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnAddItem.Location = new System.Drawing.Point(12, 12);
            this.btnAddItem.Name = "btnAddItem";
            this.btnAddItem.Size = new System.Drawing.Size(76, 28);
            this.btnAddItem.TabIndex = 20;
            this.btnAddItem.Text = "Guardar";
            this.btnAddItem.UseVisualStyleBackColor = false;
            this.btnAddItem.Click += new System.EventHandler(this.btnAddItem_Click);
            // 
            // tabControl1
            // 
            this.tabControl1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Location = new System.Drawing.Point(12, 46);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(976, 518);
            this.tabControl1.TabIndex = 21;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.label3);
            this.tabPage1.Controls.Add(this.RfcEmisorCtaOrd);
            this.tabPage1.Controls.Add(this.label1);
            this.tabPage1.Controls.Add(this.RfcEmisorCtaBen);
            this.tabPage1.Controls.Add(this.label8);
            this.tabPage1.Controls.Add(this.CtaOrdenante);
            this.tabPage1.Controls.Add(this.label11);
            this.tabPage1.Controls.Add(this.label7);
            this.tabPage1.Controls.Add(this.label12);
            this.tabPage1.Controls.Add(this.tipoCambioP);
            this.tabPage1.Controls.Add(this.label13);
            this.tabPage1.Controls.Add(this.CtaBeneficiario);
            this.tabPage1.Controls.Add(this.label10);
            this.tabPage1.Controls.Add(this.label6);
            this.tabPage1.Controls.Add(this.label9);
            this.tabPage1.Controls.Add(this.receptorNombre);
            this.tabPage1.Controls.Add(this.receptor);
            this.tabPage1.Controls.Add(this.NumOperacion);
            this.tabPage1.Controls.Add(this.FORMA_DE_PAGO);
            this.tabPage1.Controls.Add(this.label22);
            this.tabPage1.Controls.Add(this.label21);
            this.tabPage1.Controls.Add(this.label5);
            this.tabPage1.Controls.Add(this.label2);
            this.tabPage1.Controls.Add(this.NomBancoOrdExt);
            this.tabPage1.Controls.Add(this.label4);
            this.tabPage1.Controls.Add(this.label14);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(968, 492);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Informaci�n General";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(8, 35);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(79, 13);
            this.label3.TabIndex = 0;
            this.label3.Text = "Forma de Pago";
            // 
            // RfcEmisorCtaOrd
            // 
            this.RfcEmisorCtaOrd.Location = new System.Drawing.Point(105, 165);
            this.RfcEmisorCtaOrd.Name = "RfcEmisorCtaOrd";
            this.RfcEmisorCtaOrd.Size = new System.Drawing.Size(384, 20);
            this.RfcEmisorCtaOrd.TabIndex = 6;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(8, 246);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(92, 13);
            this.label1.TabIndex = 8;
            this.label1.Text = "NomBancoOrdExt";
            // 
            // RfcEmisorCtaBen
            // 
            this.RfcEmisorCtaBen.Location = new System.Drawing.Point(105, 321);
            this.RfcEmisorCtaBen.Name = "RfcEmisorCtaBen";
            this.RfcEmisorCtaBen.Size = new System.Drawing.Size(765, 20);
            this.RfcEmisorCtaBen.TabIndex = 15;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(105, 266);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(470, 13);
            this.label8.TabIndex = 10;
            this.label8.Text = "Puede conformarse desde 1 hasta 300 caracteres.  Ejemplo:  NomBancoOrdExt= BANK O" +
    "F TOKY";
            // 
            // CtaOrdenante
            // 
            this.CtaOrdenante.Location = new System.Drawing.Point(105, 282);
            this.CtaOrdenante.Name = "CtaOrdenante";
            this.CtaOrdenante.Size = new System.Drawing.Size(765, 20);
            this.CtaOrdenante.TabIndex = 12;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(105, 305);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(478, 13);
            this.label11.TabIndex = 13;
            this.label11.Text = "Puede conformarse desde 10 hasta 50 caracteres. Ejemplo: CtaOrdenante= 1234567891" +
    "01112131 ";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(8, 168);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(88, 13);
            this.label7.TabIndex = 5;
            this.label7.Text = "RfcEmisorCtaOrd";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(105, 344);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(765, 26);
            this.label12.TabIndex = 16;
            this.label12.Text = resources.GetString("label12.Text");
            // 
            // tipoCambioP
            // 
            this.tipoCambioP.Location = new System.Drawing.Point(105, 412);
            this.tipoCambioP.Name = "tipoCambioP";
            this.tipoCambioP.Size = new System.Drawing.Size(76, 20);
            this.tipoCambioP.TabIndex = 18;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(105, 396);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(486, 13);
            this.label13.TabIndex = 19;
            this.label13.Text = "Puede conformarse desde 10 hasta 50 caracteres. Ejemplo: CtaBeneficiario = 123456" +
    "789101114558 ";
            // 
            // CtaBeneficiario
            // 
            this.CtaBeneficiario.Location = new System.Drawing.Point(105, 373);
            this.CtaBeneficiario.Name = "CtaBeneficiario";
            this.CtaBeneficiario.Size = new System.Drawing.Size(384, 20);
            this.CtaBeneficiario.TabIndex = 18;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(105, 188);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(761, 52);
            this.label10.TabIndex = 7;
            this.label10.Text = resources.GetString("label10.Text");
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(8, 324);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(90, 13);
            this.label6.TabIndex = 14;
            this.label6.Text = "RfcEmisorCtaBen";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(11, 82);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(929, 65);
            this.label9.TabIndex = 4;
            this.label9.Text = resources.GetString("label9.Text");
            // 
            // receptorNombre
            // 
            this.receptorNombre.Location = new System.Drawing.Point(300, 6);
            this.receptorNombre.Name = "receptorNombre";
            this.receptorNombre.Size = new System.Drawing.Size(570, 20);
            this.receptorNombre.TabIndex = 3;
            // 
            // receptor
            // 
            this.receptor.Location = new System.Drawing.Point(105, 6);
            this.receptor.Name = "receptor";
            this.receptor.Size = new System.Drawing.Size(132, 20);
            this.receptor.TabIndex = 3;
            // 
            // NumOperacion
            // 
            this.NumOperacion.Location = new System.Drawing.Point(105, 59);
            this.NumOperacion.Name = "NumOperacion";
            this.NumOperacion.Size = new System.Drawing.Size(384, 20);
            this.NumOperacion.TabIndex = 3;
            // 
            // FORMA_DE_PAGO
            // 
            this.FORMA_DE_PAGO.FormattingEnabled = true;
            this.FORMA_DE_PAGO.Items.AddRange(new object[] {
            "Pago en una sola exhibici�n",
            "Pago en parcialidades",
            "Pago a credito"});
            this.FORMA_DE_PAGO.Location = new System.Drawing.Point(105, 32);
            this.FORMA_DE_PAGO.Name = "FORMA_DE_PAGO";
            this.FORMA_DE_PAGO.Size = new System.Drawing.Size(384, 21);
            this.FORMA_DE_PAGO.TabIndex = 1;
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Location = new System.Drawing.Point(243, 9);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(51, 13);
            this.label22.TabIndex = 2;
            this.label22.Text = "Receptor";
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(8, 9);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(28, 13);
            this.label21.TabIndex = 2;
            this.label21.Text = "RFC";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(8, 285);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(73, 13);
            this.label5.TabIndex = 11;
            this.label5.Text = "CtaOrdenante";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(8, 62);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(78, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "NumOperacion";
            // 
            // NomBancoOrdExt
            // 
            this.NomBancoOrdExt.Location = new System.Drawing.Point(105, 243);
            this.NomBancoOrdExt.Name = "NomBancoOrdExt";
            this.NomBancoOrdExt.Size = new System.Drawing.Size(765, 20);
            this.NomBancoOrdExt.TabIndex = 9;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(9, 376);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(78, 13);
            this.label4.TabIndex = 17;
            this.label4.Text = "CtaBeneficiario";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(11, 415);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(80, 13);
            this.label14.TabIndex = 17;
            this.label14.Text = "Tipo de cambio";
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.MonedaPago);
            this.tabPage2.Controls.Add(this.textBox2);
            this.tabPage2.Controls.Add(this.label17);
            this.tabPage2.Controls.Add(this.label16);
            this.tabPage2.Controls.Add(this.total);
            this.tabPage2.Controls.Add(this.label15);
            this.tabPage2.Controls.Add(this.button3);
            this.tabPage2.Controls.Add(this.button5);
            this.tabPage2.Controls.Add(this.button1);
            this.tabPage2.Controls.Add(this.dtgrdGeneral);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(968, 492);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "L�neas de Pago";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // MonedaPago
            // 
            this.MonedaPago.FormattingEnabled = true;
            this.MonedaPago.Items.AddRange(new object[] {
            "USD",
            "MXN"});
            this.MonedaPago.Location = new System.Drawing.Point(607, 10);
            this.MonedaPago.Name = "MonedaPago";
            this.MonedaPago.Size = new System.Drawing.Size(70, 21);
            this.MonedaPago.TabIndex = 448;
            // 
            // textBox2
            // 
            this.textBox2.Location = new System.Drawing.Point(744, 11);
            this.textBox2.Name = "textBox2";
            this.textBox2.Size = new System.Drawing.Size(75, 20);
            this.textBox2.TabIndex = 446;
            this.textBox2.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(683, 15);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(55, 13);
            this.label17.TabIndex = 445;
            this.label17.Text = "T.C. Pago";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(555, 14);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(46, 13);
            this.label16.TabIndex = 443;
            this.label16.Text = "Moneda";
            // 
            // total
            // 
            this.total.Location = new System.Drawing.Point(219, 11);
            this.total.Name = "total";
            this.total.Size = new System.Drawing.Size(124, 20);
            this.total.TabIndex = 441;
            this.total.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(182, 15);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(31, 13);
            this.label15.TabIndex = 440;
            this.label15.Text = "Total";
            // 
            // button3
            // 
            this.button3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(219)))), ((int)(((byte)(94)))), ((int)(((byte)(94)))));
            this.button3.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button3.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button3.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.button3.Location = new System.Drawing.Point(41, 6);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(31, 28);
            this.button3.TabIndex = 439;
            this.button3.Text = "-";
            this.button3.UseVisualStyleBackColor = false;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // button5
            // 
            this.button5.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(153)))), ((int)(((byte)(181)))), ((int)(((byte)(105)))));
            this.button5.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button5.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button5.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.button5.Location = new System.Drawing.Point(78, 5);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(98, 28);
            this.button5.TabIndex = 438;
            this.button5.Text = "Duplicar L�nea";
            this.button5.UseVisualStyleBackColor = false;
            this.button5.Click += new System.EventHandler(this.button5_Click);
            // 
            // button1
            // 
            this.button1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(153)))), ((int)(((byte)(181)))), ((int)(((byte)(105)))));
            this.button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button1.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button1.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.button1.Location = new System.Drawing.Point(6, 6);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(31, 28);
            this.button1.TabIndex = 438;
            this.button1.Text = "+";
            this.button1.UseVisualStyleBackColor = false;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // dtgrdGeneral
            // 
            this.dtgrdGeneral.AllowUserToAddRows = false;
            this.dtgrdGeneral.AllowUserToDeleteRows = false;
            this.dtgrdGeneral.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dtgrdGeneral.BackgroundColor = System.Drawing.Color.White;
            this.dtgrdGeneral.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dtgrdGeneral.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.dtgrdGeneral.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dtgrdGeneral.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.factura,
            this.UUID,
            this.Moneda,
            this.NumParcialidad,
            this.ImpSaldoAnt,
            this.ImpPagado,
            this.ImpSaldoInsoluto,
            this.TipoCambioDR});
            dataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle6.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle6.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle6.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle6.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle6.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle6.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dtgrdGeneral.DefaultCellStyle = dataGridViewCellStyle6;
            this.dtgrdGeneral.Location = new System.Drawing.Point(6, 40);
            this.dtgrdGeneral.Name = "dtgrdGeneral";
            dataGridViewCellStyle7.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle7.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle7.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle7.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle7.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle7.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle7.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dtgrdGeneral.RowHeadersDefaultCellStyle = dataGridViewCellStyle7;
            this.dtgrdGeneral.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dtgrdGeneral.Size = new System.Drawing.Size(956, 446);
            this.dtgrdGeneral.TabIndex = 437;
            this.dtgrdGeneral.CellEndEdit += new System.Windows.Forms.DataGridViewCellEventHandler(this.dtgrdGeneral_CellEndEdit);
            this.dtgrdGeneral.DataBindingComplete += new System.Windows.Forms.DataGridViewBindingCompleteEventHandler(this.dtgrdGeneral_DataBindingComplete);
            // 
            // factura
            // 
            this.factura.HeaderText = "Factura";
            this.factura.Name = "factura";
            // 
            // UUID
            // 
            this.UUID.HeaderText = "UUID";
            this.UUID.Name = "UUID";
            // 
            // Moneda
            // 
            this.Moneda.HeaderText = "Moneda";
            this.Moneda.Name = "Moneda";
            // 
            // NumParcialidad
            // 
            this.NumParcialidad.HeaderText = "NumParcialidad";
            this.NumParcialidad.Name = "NumParcialidad";
            // 
            // ImpSaldoAnt
            // 
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle2.Format = "N4";
            dataGridViewCellStyle2.NullValue = "0";
            this.ImpSaldoAnt.DefaultCellStyle = dataGridViewCellStyle2;
            this.ImpSaldoAnt.HeaderText = "ImpSaldoAnt";
            this.ImpSaldoAnt.Name = "ImpSaldoAnt";
            // 
            // ImpPagado
            // 
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle3.Format = "N4";
            dataGridViewCellStyle3.NullValue = "0";
            this.ImpPagado.DefaultCellStyle = dataGridViewCellStyle3;
            this.ImpPagado.HeaderText = "ImpPagado";
            this.ImpPagado.Name = "ImpPagado";
            // 
            // ImpSaldoInsoluto
            // 
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle4.Format = "N4";
            dataGridViewCellStyle4.NullValue = "0";
            this.ImpSaldoInsoluto.DefaultCellStyle = dataGridViewCellStyle4;
            this.ImpSaldoInsoluto.HeaderText = "ImpSaldoInsoluto";
            this.ImpSaldoInsoluto.Name = "ImpSaldoInsoluto";
            // 
            // TipoCambioDR
            // 
            dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle5.Format = "N4";
            dataGridViewCellStyle5.NullValue = "0";
            this.TipoCambioDR.DefaultCellStyle = dataGridViewCellStyle5;
            this.TipoCambioDR.HeaderText = "TipoCambioDR";
            this.TipoCambioDR.Name = "TipoCambioDR";
            // 
            // frmEditComprobantePago
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1000, 583);
            this.Controls.Add(this.tabControl1);
            this.Controls.Add(this.btnAddItem);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MetroColor = System.Drawing.Color.Transparent;
            this.Name = "frmEditComprobantePago";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Cambio de forma de pago";
            this.Load += new System.EventHandler(this.frmEditComprobantePago_Load);
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage1.PerformLayout();
            this.tabPage2.ResumeLayout(false);
            this.tabPage2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dtgrdGeneral)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Button btnAddItem;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox RfcEmisorCtaOrd;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox RfcEmisorCtaBen;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox CtaOrdenante;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.TextBox tipoCambioP;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.TextBox CtaBeneficiario;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox NumOperacion;
        private System.Windows.Forms.ComboBox FORMA_DE_PAGO;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox NomBancoOrdExt;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.DataGridView dtgrdGeneral;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.DataGridViewTextBoxColumn factura;
        private System.Windows.Forms.DataGridViewTextBoxColumn UUID;
        private System.Windows.Forms.DataGridViewTextBoxColumn Moneda;
        private System.Windows.Forms.DataGridViewTextBoxColumn NumParcialidad;
        private System.Windows.Forms.DataGridViewTextBoxColumn ImpSaldoAnt;
        private System.Windows.Forms.DataGridViewTextBoxColumn ImpPagado;
        private System.Windows.Forms.DataGridViewTextBoxColumn ImpSaldoInsoluto;
        private System.Windows.Forms.DataGridViewTextBoxColumn TipoCambioDR;
        private System.Windows.Forms.TextBox total;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.TextBox textBox2;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.ComboBox MonedaPago;
        private System.Windows.Forms.TextBox receptor;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.TextBox receptorNombre;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Button button5;
    }
}