#region Copyright Syncfusion Inc. 2001 - 2017
// Copyright Syncfusion Inc. 2001 - 2017. All rights reserved.
// Use of this code is subject to the terms of our license.
// A copy of the current license can be obtained at any time by e-mailing
// licensing@syncfusion.com. Any infringement will be prosecuted under
// applicable laws. 
#endregion
using CrystalDecisions.Shared.Json;
using Generales;
using ModeloComprobantePago;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Validation;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Windows.Forms;

namespace FE_FX.ComprobantePago
{
    //Rodrigo Escalona 20/07/2023
    //PAgo de MNX facturs USD
    //http://fxdemos.com/web/ptm/facturas/ROVD930202V40/Comprobante/10272D7D-C1C8-4354-8D28-D8D97E1D8110.XML
    //SELECT * FROM `documentoset` d INNER JOIN pago p ON p.uuid=d.uuid INNER JOIN `documentoset` dp ON dp.uuid=p.uuidPago WHERE d.estado='Pagada' AND d.moneda='USD'ORDER BY d.Id desc LIMIT 0,100

    public partial class frmEditarComprobantePago : Syncfusion.Windows.Forms.MetroForm
    {
        ModeloComprobantePago.documentosfxEntities dbContext = new documentosfxEntities();
        public pago oRegistroP;
        public frmEditarComprobantePago(pago oPagoP)
        {
            InitializeComponent();
            limpiar();
            oRegistroP = new pago();
            cargarCombo();
            if (oPagoP != null)
            {
                oRegistroP = dbContext.pagoSet.Where(a => a.Id.Equals(oPagoP.Id))
                    .OrderByDescending(b => b.Id)
                    .FirstOrDefault();
                cargar();
            }
        }


        public frmEditarComprobantePago(string CheckId, string CustomerId)
        {
            InitializeComponent();
            limpiar();
            oRegistroP = new pago();
            cargarCombo();
            



            oRegistroP = dbContext.pagoSet.Where(a => a.checkId.Equals(CheckId)
            & a.customerId.Equals(CustomerId))
                .OrderByDescending(b => b.Id)
                .FirstOrDefault();

            if (oRegistroP==null)
            {
                //Cargar el �ltimo pago
                oRegistroP = dbContext.pagoSet.Where(a => a.customerId.Equals(CustomerId))
                    .OrderByDescending(b => b.Id)
                    .FirstOrDefault();
            }

            cargar();
            
        }

        private void limpiar()
        {
            Action<Control.ControlCollection> func = null;

            func = (controls) =>
            {
                foreach (Control control in controls)
                    if (control is TextBox)
                        (control as TextBox).Clear();
                    else
                        func(control.Controls);
            };

            func(Controls);
            dtgrdGeneral.Rows.Clear();
        }

        private void cargar()
        {
            try
            {
                if (oRegistroP==null)
                {
                    return;
                }

                if (!String.IsNullOrEmpty(oRegistroP.receptor))
                {
                    receptor.Text = oRegistroP.receptor;
                }

                if (!String.IsNullOrEmpty(oRegistroP.receptorNombre))
                {
                    receptorNombre.Text = oRegistroP.receptorNombre;
                }
                if (!String.IsNullOrEmpty(oRegistroP.DomicilioFiscalReceptor))
                {
                    DomicilioFiscalReceptor.Text = oRegistroP.DomicilioFiscalReceptor;
                }
                if (!String.IsNullOrEmpty(oRegistroP.RegimenFiscalReceptor))
                {
                    RegimenFiscalReceptor.Text = oRegistroP.RegimenFiscalReceptor;
                }


                if (!String.IsNullOrEmpty(oRegistroP.formaDePagoP))
                {
                    foreach (ComboItem item in FORMA_DE_PAGO.Items)
                    {
                        if (item.Value.Equals(oRegistroP.formaDePagoP))
                        {
                            FORMA_DE_PAGO.SelectedItem = item;
                            break;
                        }
                    }
                }

                if (!String.IsNullOrEmpty(oRegistroP.NomBancoOrdExt))
                {
                    NomBancoOrdExt.Text = oRegistroP.NomBancoOrdExt;
                }
                if (!String.IsNullOrEmpty(oRegistroP.NumOperacion))
                {
                    NumOperacion.Text = oRegistroP.NumOperacion;
                }
                if (!String.IsNullOrEmpty(oRegistroP.CtaBeneficiario))
                {
                    CtaBeneficiario.Text = oRegistroP.CtaBeneficiario;
                }
                if (!String.IsNullOrEmpty(oRegistroP.CtaOrdenante))
                {
                    CtaOrdenante.Text = oRegistroP.CtaOrdenante;
                }
                if (!String.IsNullOrEmpty(oRegistroP.RfcEmisorCtaBen))
                {
                    RfcEmisorCtaBen.Text = oRegistroP.RfcEmisorCtaBen;
                }
                if (!String.IsNullOrEmpty(oRegistroP.RfcEmisorCtaOrd))
                {
                    RfcEmisorCtaOrd.Text = oRegistroP.RfcEmisorCtaOrd;
                }

                if (oRegistroP.tipoCambioP!=null)
                {
                    this.tipoCambioP.Text = oRegistroP.tipoCambioP.ToString();
                }
                
                total.Text = this.oRegistroP.monto.ToString();

                editado.Checked = oRegistroP.editado;
                MonedaPago.Text = oRegistroP.monedaP;
                cargarLinea();
            }
            catch(Exception error)
            {
                ErrorFX.mostrar(error, true, false, false);
            }
        }

        private void cargarLinea()
        {
            
            dtgrdGeneral.Rows.Clear();
            foreach (pagoLinea linea in oRegistroP.pagoLinea)
            {
                int n = dtgrdGeneral.Rows.Add();
                dtgrdGeneral.Rows[n].Tag = linea;
                dtgrdGeneral.Rows[n].Cells["factura"].Value = linea.factura;
                dtgrdGeneral.Rows[n].Cells["UUID"].Value = linea.UUID;
                dtgrdGeneral.Rows[n].Cells["Moneda"].Value = linea.MonedaDR;
                dtgrdGeneral.Rows[n].Cells["NumParcialidad"].Value = linea.NumParcialidad;
                dtgrdGeneral.Rows[n].Cells["ImpSaldoAnt"].Value = linea.ImpSaldoAnt;
                dtgrdGeneral.Rows[n].Cells["ImpPagado"].Value = linea.ImpPagado;
                dtgrdGeneral.Rows[n].Cells["ImpSaldoInsoluto"].Value = linea.ImpSaldoInsoluto;
                dtgrdGeneral.Rows[n].Cells["TipoCambioDR"].Value = linea.TipoCambioDR;
                
                dtgrdGeneral.Rows[n].Cells["EquivalenciaDR"].Value = linea.EquivalenciaDR;
            }
           
            dtgrdGeneral.AutoResizeColumns(
                DataGridViewAutoSizeColumnsMode.AllCells);
        }


        private void cargarCombo()
        {

            FORMA_DE_PAGO.Items.Clear();
            FORMA_DE_PAGO.Items.Add(new ComboItem { Name = "Efectivo", Value = "01" });
            FORMA_DE_PAGO.Items.Add(new ComboItem { Name = "Cheque nominativo", Value = "02" });
            FORMA_DE_PAGO.Items.Add(new ComboItem { Name = "Transferencia electr�nica de fondos", Value = "03" });
            FORMA_DE_PAGO.Items.Add(new ComboItem { Name = "Tarjeta de cr�dito", Value = "04" });
            FORMA_DE_PAGO.Items.Add(new ComboItem { Name = "Monedero electr�nico", Value = "05" });
            FORMA_DE_PAGO.Items.Add(new ComboItem { Name = "Dinero electr�nico", Value = "06" });
            FORMA_DE_PAGO.Items.Add(new ComboItem { Name = "Vales de despensa", Value = "08" });
            FORMA_DE_PAGO.Items.Add(new ComboItem { Name = "Daci�n en pago", Value = "12" });
            FORMA_DE_PAGO.Items.Add(new ComboItem { Name = "Pago por subrogaci�n", Value = "13" });
            FORMA_DE_PAGO.Items.Add(new ComboItem { Name = "Pago por consignaci�n", Value = "14" });
            FORMA_DE_PAGO.Items.Add(new ComboItem { Name = "Condonaci�n", Value = "15" });
            FORMA_DE_PAGO.Items.Add(new ComboItem { Name = "Compensaci�n", Value = "17" });
            FORMA_DE_PAGO.Items.Add(new ComboItem { Name = "Novaci�n", Value = "23" });
            FORMA_DE_PAGO.Items.Add(new ComboItem { Name = "Confusi�n", Value = "24" });
            FORMA_DE_PAGO.Items.Add(new ComboItem { Name = "Remisi�n de deuda", Value = "25" });
            FORMA_DE_PAGO.Items.Add(new ComboItem { Name = "Prescripci�n o caducidad", Value = "26" });
            FORMA_DE_PAGO.Items.Add(new ComboItem { Name = "A satisfacci�n del acreedor", Value = "27" });
            FORMA_DE_PAGO.Items.Add(new ComboItem { Name = "Tarjeta de d�bito", Value = "28" });
            FORMA_DE_PAGO.Items.Add(new ComboItem { Name = "Tarjeta de servicios", Value = "29" });
            FORMA_DE_PAGO.Items.Add(new ComboItem { Name = "Aplicaci�n de anticipos", Value = "30" });
            ComboItem oComboItemFORMA_DE_PAGO = new ComboItem { Name = "Transferencia electr�nica de fondos", Value = "03" };
            FORMA_DE_PAGO.Items.Add(oComboItemFORMA_DE_PAGO);
            FORMA_DE_PAGO.SelectedItem = oComboItemFORMA_DE_PAGO;
        }

        private void btnAddItem_Click(object sender, EventArgs e)
        {
            guardar();
        }

        private void guardar()
        {
            try
            {

                if (oRegistroP == null)
                {
                    oRegistroP = new pago();
                }

                if (oRegistroP.Id == 0)
                {
                    oRegistroP = new pago();
                }
                
                this.oRegistroP.formaDePagoP = ((ComboItem)FORMA_DE_PAGO.SelectedItem).Value.ToString();
                this.oRegistroP.NomBancoOrdExt = NomBancoOrdExt.Text;
                this.oRegistroP.NumOperacion = NumOperacion.Text;
                this.oRegistroP.DomicilioFiscalReceptor = DomicilioFiscalReceptor.Text;
                this.oRegistroP.RegimenFiscalReceptor = RegimenFiscalReceptor.Text;


                if (!String.IsNullOrEmpty(CtaBeneficiario.Text))
                {
                    if (CtaBeneficiario.Text.Length < 10)
                    {
                        ErrorFX.mostrar("La Cuenta del Beneficiario puede conformarse desde 10 hasta 50 caracteres.", true, false, false);
                        CtaBeneficiario.Focus();
                        return;
                    }
                }


                if (!String.IsNullOrEmpty(CtaOrdenante.Text))
                {



                    if (this.oRegistroP.formaDePagoP.Equals("02"))
                    {
                        Match match = Regex.Match(CtaOrdenante.Text, @"[0-9]{11}|[0-9]{18}", RegexOptions.IgnoreCase);
                        if (!match.Success)
                        {
                            ErrorFX.mostrar("La Cuenta del Ordenante Si es Forma de Pago: " + this.oRegistroP.formaDePagoP + " puede conformarse de 11 n�meros o 18 n�meros", true, false, false);
                            CtaOrdenante.Focus();
                            return;
                        }
                    }

                    if (this.oRegistroP.formaDePagoP.Equals("03"))
                    {
                        Match match = Regex.Match(CtaOrdenante.Text, @"[0-9]{10}|[0-9]{16}|[0-9]{18}", RegexOptions.IgnoreCase);
                        if (!match.Success)
                        {
                            ErrorFX.mostrar("La Cuenta del Ordenante Si es Forma de Pago: " + this.oRegistroP.formaDePagoP + " puede conformarse de 10 n�meros o 16 n�meros o 18 n�meros", true, false, false);
                            CtaOrdenante.Focus();
                            return;
                        }
                    }

                    if (this.oRegistroP.formaDePagoP.Equals("04"))
                    {
                        Match match = Regex.Match(CtaOrdenante.Text, @"[0-9]{16}", RegexOptions.IgnoreCase);
                        if (!match.Success)
                        {
                            ErrorFX.mostrar("La Cuenta del Ordenante Si es Forma de Pago: " + this.oRegistroP.formaDePagoP + " puede conformarse de 16 n�meros", true, false, false);
                            CtaOrdenante.Focus();
                            return;
                        }
                    }

                    if (this.oRegistroP.formaDePagoP.Equals("05"))
                    {
                        Match match = Regex.Match(CtaOrdenante.Text, @"[0-9]{10,11}|[0-9]{15,16}|[0-9]{18}|[A-Z0-9_]{10,50}", RegexOptions.IgnoreCase);
                        if (!match.Success)
                        {
                            ErrorFX.mostrar("La Cuenta del Ordenante Si es Forma de Pago: " + this.oRegistroP.formaDePagoP + " puede conformarse de [0-9]{10,11}|[0-9]{15,16}|[0-9]{18}|[A-Z0-9_]{10,50}", true, false, false);
                            CtaOrdenante.Focus();
                            return;
                        }
                    }
                }

                this.oRegistroP.CtaBeneficiario = CtaBeneficiario.Text;
                this.oRegistroP.CtaOrdenante = CtaOrdenante.Text;
                this.oRegistroP.RfcEmisorCtaBen = RfcEmisorCtaBen.Text;
                this.oRegistroP.RfcEmisorCtaOrd = RfcEmisorCtaOrd.Text;
                if (!String.IsNullOrEmpty(tipoCambioP.Text))
                {
                    if (Generales.Globales.IsNumeric(tipoCambioP.Text))
                    {
                        this.oRegistroP.tipoCambioP = decimal.Parse(tipoCambioP.Text);
                    }
                }

                if (receptor.Text != oRegistroP.receptor)
                {
                    oRegistroP.receptor = receptor.Text;
                }

                if (receptorNombre.Text != oRegistroP.receptorNombre)
                {
                    oRegistroP.receptorNombre = receptorNombre.Text;
                }
                oRegistroP.editado = editado.Checked;
                oRegistroP.tipoCambioP = decimal.Parse(tipoCambioP.Text);
                oRegistroP.monedaP = MonedaPago.Text;
                oRegistroP.monto = decimal.Parse(total.Text);

                if (oRegistroP.Id == 0)
                {
                    dbContext.pagoSet.Add(oRegistroP);
                }
                else
                {
                    dbContext.pagoSet.Attach(oRegistroP);
                    dbContext.Entry(oRegistroP).State = EntityState.Modified;
                    
                }
                dbContext.SaveChanges();
                try
                {
                    foreach (DataGridViewRow r in dtgrdGeneral.Rows)
                    {
                        pagoLinea opagoLinea = new pagoLinea();
                        if (r.Tag != null)
                        {
                            opagoLinea = (pagoLinea)r.Tag;
                        }
                        opagoLinea.pago = this.oRegistroP;
                        opagoLinea.factura = r.Cells["factura"].Value.ToString();
                        opagoLinea.UUID = r.Cells["UUID"].Value.ToString();
                        opagoLinea.MonedaDR = r.Cells["Moneda"].Value.ToString();
                        opagoLinea.NumParcialidad = r.Cells["NumParcialidad"].Value.ToString();
                        opagoLinea.ImpSaldoAnt = decimal.Parse(r.Cells["ImpSaldoAnt"].Value.ToString());
                        opagoLinea.ImpPagado = decimal.Parse(r.Cells["ImpPagado"].Value.ToString());
                        opagoLinea.ImpSaldoInsoluto = 0;
                        if (r.Cells["ImpSaldoInsoluto"].Value != null)
                        {
                            opagoLinea.ImpSaldoInsoluto = decimal.Parse(r.Cells["ImpSaldoInsoluto"].Value.ToString());
                        }
                        if (r.Cells["TipoCambioDR"].Value != null)
                        {
                            opagoLinea.TipoCambioDR = decimal.Parse(r.Cells["TipoCambioDR"].Value.ToString());
                        }
                        if (r.Cells["EquivalenciaDR"].Value != null)
                        {
                            opagoLinea.EquivalenciaDR = decimal.Parse(r.Cells["EquivalenciaDR"].Value.ToString());
                        }

                        if (opagoLinea.Id == 0)
                        {
                            dbContext.pagoLineaSet.Add(opagoLinea);
                        }
                        else
                        {
                            dbContext.pagoLineaSet.Attach(opagoLinea);
                            dbContext.Entry(opagoLinea).State = EntityState.Modified;
                        }
                        dbContext.SaveChanges();
                    }
                    Clases.cComprobantePago OcComprobanPago = new Clases.cComprobantePago();
                    OcComprobanPago.GenerarImpuestosNuevo(oRegistroP);
                    OcComprobanPago.CalcularTotalesNuevo(oRegistroP);

                    DialogResult = DialogResult.OK;
                }
                catch (Exception e)
                {
                    ErrorFX.mostrar(e, true, false, "frmEditarComprobantePago - 339 ");
                    
                }


                
            }
            catch (DbEntityValidationException error)
            {
                string mensaje = "Shipment - 204 ";
                foreach (var eve in error.EntityValidationErrors)
                {
                    foreach (var ve in eve.ValidationErrors)
                    {
                        mensaje += ve.PropertyName + " Error: " + ve.ErrorMessage + Environment.NewLine;
                    }
                }
                ErrorFX.mostrar(mensaje, true, false, false);
                return;
            }
            catch (Exception error)
            {
                string mensaje = "Comprobante de Pago - 217 ";
                mensaje += error.Message.ToString();
                if (error.InnerException != null)
                {
                    mensaje += " " + error.InnerException.Message.ToString();
                }
                ErrorFX.mostrar(mensaje, true, false, false);
                return;
            }

        }

        private void frmEditarComprobantePago_Load(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            agregarLinea();
        }

        private void agregarLinea()
        {
            int n = dtgrdGeneral.Rows.Add();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            eliminar();
        }

        private void eliminar()
        {
            try
            {
                dtgrdGeneral.EndEdit();
                if (dtgrdGeneral.SelectedCells.Count > 0)
                {
                    DialogResult Resultado = MessageBox.Show("�Esta seguro de eliminar las l�neas seleccionadas?", Application.ProductName + "-" + Application.ProductVersion, MessageBoxButtons.YesNo);
                    if (Resultado.ToString() != "Yes")
                    {
                        return;
                    }

                    IEnumerable<DataGridViewRow> selectedRows = dtgrdGeneral.SelectedCells.Cast<DataGridViewCell>()
                                           .Select(cell => cell.OwningRow)
                                           .Distinct();

                    foreach (DataGridViewRow drv in selectedRows)
                    {
                        if (drv.Tag!=null)
                        {



                            pagoLinea pagoLineaTr = (pagoLinea)drv.Tag;
                            Clases.cComprobantePago.EliminarLinea(pagoLineaTr.Id);

                        }
                        dtgrdGeneral.Rows.Remove(drv);
                    }

                }
            }
            catch (Exception e)
            {
                ErrorFX.mostrar(e, true, true, "frmEditarComprobantePago - 295 ");
            }
        }

        private void dtgrdGeneral_DataBindingComplete(object sender, DataGridViewBindingCompleteEventArgs e)
        {
            dtgrdGeneral.Columns["Id"].Visible = false;
            
        }
        private void dtgrdGeneral_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {
            //Validar si los datos
            //Actualizar

            if (dtgrdGeneral.Rows[e.RowIndex].Tag != null)
            {
                if (String.IsNullOrEmpty(dtgrdGeneral.Rows[e.RowIndex].Cells[e.ColumnIndex].Value.ToString()))
                {
                    return;
                }

                pagoLinea pagoLineaTr = (pagoLinea)dtgrdGeneral.Rows[e.RowIndex].Tag;
                pagoLinea pagoLineaTr2 = dbContext.pagoLineaSet.Where(i => i.Id == pagoLineaTr.Id).FirstOrDefault();

                if (dtgrdGeneral.Columns[e.ColumnIndex].Name == "ImpSaldoAnt")
                {
                    pagoLineaTr2.ImpSaldoAnt = decimal.Parse(dtgrdGeneral.Rows[e.RowIndex].Cells[e.ColumnIndex].Value.ToString());
                    dbContext.Entry(pagoLineaTr2).State = EntityState.Modified;
                    dbContext.SaveChanges();
                }
                if (dtgrdGeneral.Columns[e.ColumnIndex].Name == "ImpSaldoInsoluto")
                {
                    pagoLineaTr2.ImpSaldoInsoluto = decimal.Parse(dtgrdGeneral.Rows[e.RowIndex].Cells[e.ColumnIndex].Value.ToString());
                    dbContext.Entry(pagoLineaTr2).State = EntityState.Modified;
                    dbContext.SaveChanges();
                }
                if (dtgrdGeneral.Columns[e.ColumnIndex].Name == "TipoCambioDR")
                {
                    pagoLineaTr2.TipoCambioDR = decimal.Parse(dtgrdGeneral.Rows[e.RowIndex].Cells[e.ColumnIndex].Value.ToString());
                    dbContext.Entry(pagoLineaTr2).State = EntityState.Modified;
                    dbContext.SaveChanges();
                }

                if (dtgrdGeneral.Columns[e.ColumnIndex].Name == "ImpPagado")
                {
                    pagoLineaTr2.ImpPagado = decimal.Parse(dtgrdGeneral.Rows[e.RowIndex].Cells[e.ColumnIndex].Value.ToString());
                    dbContext.Entry(pagoLineaTr2).State = EntityState.Modified;
                    dbContext.SaveChanges();
                }

            }

        }

        private void dtgrdGeneral_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            CargarImpuestos();
        }

        private void CargarImpuestos()
        {
            if (DtgImpuesto.Rows.Count>0)
            {
                GuardarImpuestos();
            }
            DtgImpuesto.Rows.Clear();
            if (dtgrdGeneral.CurrentCell.RowIndex > -1)
            {
                if (dtgrdGeneral.Rows[dtgrdGeneral.CurrentCell.RowIndex].Tag != null)
                {
                    CargarImpuestoLinea((pagoLinea)dtgrdGeneral.Rows[dtgrdGeneral.CurrentCell.RowIndex].Tag);
                }
                
            }
            
        }

        private void CargarImpuestoLinea(pagoLinea OPagoLinea)
        {
            foreach (pagoLineaImpuesto linea in OPagoLinea.pagoLineaImpuesto)
            {
                int n = DtgImpuesto.Rows.Add();
                DtgImpuesto.Rows[n].Tag = linea;
                DtgImpuesto.Rows[n].Cells["ImporteDR"].Value = linea.ImporteDR;
                DtgImpuesto.Rows[n].Cells["BaseDR"].Value = linea.BaseDR;
                DtgImpuesto.Rows[n].Cells["ImpuestoDR"].Value = linea.ImpuestoDR;
                DtgImpuesto.Rows[n].Cells["TasaOCuotaDR"].Value = linea.TasaOCuotaDR;
                DtgImpuesto.Rows[n].Cells["TipoFactorDR"].Value = linea.TipoFactorDR;

            }

            DtgImpuesto.AutoResizeColumns(
                DataGridViewAutoSizeColumnsMode.AllCells);
        }

        private void GuardarImpuestos()
        {
            foreach (DataGridViewRow r in DtgImpuesto.Rows)
            {
                pagoLineaImpuesto linea = new pagoLineaImpuesto();
                if (r.Tag != null)
                {
                    linea = (pagoLineaImpuesto)r.Tag;
                    linea.ImporteDR = decimal.Parse(r.Cells["ImporteDR"].Value.ToString());
                    linea.BaseDR = decimal.Parse(r.Cells["BaseDR"].Value.ToString());

                    if (linea.Id == 0)
                    {
                        dbContext.pagoLineaImpuestoSet.Add(linea);
                    }
                    else
                    {
                        dbContext.pagoLineaImpuestoSet.Attach(linea);
                        dbContext.Entry(linea).State = EntityState.Modified;
                    }
                    dbContext.SaveChanges();
                }
            }
        }

        private void button4_Click(object sender, EventArgs e)
        {
            CalcularImpuesto();
        }

        private void CalcularImpuesto()
        {
            //Buscar la linea seleccionada
            if (dtgrdGeneral.SelectedRows.Count > 0)
            {
                foreach (DataGridViewRow Row in dtgrdGeneral.SelectedRows)
                {
                    decimal ImpPagadoTr = decimal.Parse(Row.Cells["ImpPagado"].Value.ToString());
                    decimal EquivalenciaDRTr = decimal.Parse(Row.Cells["EquivalenciaDR"].Value.ToString());
                    //ImpPagadoTr = Math.Round(ImpPagadoTr / EquivalenciaDRTr, 2, MidpointRounding.AwayFromZero);

                    if (DtgImpuesto.SelectedRows.Count > 0)
                    {
                        DataGridViewRow RowImpuesto = DtgImpuesto.SelectedRows[0];
                        decimal TasaOCuotaDRTr = decimal.Parse(RowImpuesto.Cells["TasaOCuotaDR"].Value.ToString());
                        decimal BaseDRTr = Math.Round(ImpPagadoTr / 1.16M,4);
                        decimal ImporteDRTr = ImpPagadoTr-BaseDRTr;
                        RowImpuesto.Cells["BaseDR"].Value = BaseDRTr;
                        RowImpuesto.Cells["ImporteDR"].Value = ImporteDRTr;
                    }
                    break;
                }
                
            }
            
        }

        private void label22_Click(object sender, EventArgs e)
        {

        }

        private void MonedaPago_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void button2_Click(object sender, EventArgs e)
        {
            Calcular();
        }

        private void Calcular()
        {
            //Sumar las lineas
            decimal TotalTr = 0;
            foreach(DataGridViewRow RegistroTmp in dtgrdGeneral.Rows)
            {
                decimal TotalLinea = 0;
                decimal ImpPagadoTr = decimal.Parse(RegistroTmp.Cells["ImpPagado"].Value.ToString());
                decimal EquivalenciaDRTr = decimal.Parse(RegistroTmp.Cells["EquivalenciaDR"].Value.ToString());
                string MonedaTr = RegistroTmp.Cells["Moneda"].Value.ToString();

                if (MonedaTr == MonedaPago.Text)
                {
                    TotalLinea = ImpPagadoTr;
                }
                else
                {
                    TotalLinea = ImpPagadoTr/EquivalenciaDRTr;
                }
                TotalTr += TotalLinea;
            }
            total.Text = Math.Round(TotalTr,2, MidpointRounding.AwayFromZero).ToString();


        }

        private void button5_Click(object sender, EventArgs e)
        {
            GuardarImpuestos();
        }

        private void button6_Click(object sender, EventArgs e)
        {
            if (dtgrdGeneral.SelectedRows.Count > 0)
            {
                foreach (DataGridViewRow Row in dtgrdGeneral.SelectedRows)
                {
                    if (Row.Tag != null)
                    {
                        CargarImpuestoLinea((pagoLinea)Row.Tag);
                        DtgImpuesto.SelectAll();
                        CalcularImpuesto();
                        GuardarImpuestos();
                    }
                }

            }
        }
    }
}
