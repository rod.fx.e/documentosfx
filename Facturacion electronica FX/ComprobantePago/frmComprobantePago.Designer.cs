#region Copyright Syncfusion Inc. 2001 - 2017
// Copyright Syncfusion Inc. 2001 - 2017. All rights reserved.
// Use of this code is subject to the terms of our license.
// A copy of the current license can be obtained at any time by e-mailing
// licensing@syncfusion.com. Any infringement will be prosecuted under
// applicable laws. 
#endregion
namespace FE_FX.ComprobantePago
{
    partial class frmComprobantePago
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle7 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle8 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmComprobantePago));
            this.receptor = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.fechaPago = new System.Windows.Forms.DateTimePicker();
            this.textBox3 = new System.Windows.Forms.TextBox();
            this.textBox4 = new System.Windows.Forms.TextBox();
            this.serie = new System.Windows.Forms.TextBox();
            this.btnDeleteItem = new System.Windows.Forms.Button();
            this.btnNuevo = new System.Windows.Forms.Button();
            this.btnAddItem = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.button5 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.dtgFolio = new System.Windows.Forms.DataGridView();
            this.facturaCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.folioCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.serieCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tipoCambioDRCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.uuidCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.fechaCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.totalCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.NumParcialidad = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Aplicado = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ImpSaldoAntCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.monedaCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.saldoCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.button6 = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.emisor = new System.Windows.Forms.ComboBox();
            this.receptorNombre = new System.Windows.Forms.TextBox();
            this.tipoCambio = new System.Windows.Forms.TextBox();
            this.moneda = new System.Windows.Forms.ComboBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.estado = new System.Windows.Forms.ComboBox();
            this.label7 = new System.Windows.Forms.Label();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.label8 = new System.Windows.Forms.Label();
            this.uuid = new System.Windows.Forms.TextBox();
            this.pdf = new System.Windows.Forms.TextBox();
            this.xml = new System.Windows.Forms.TextBox();
            this.btnPDF = new System.Windows.Forms.Button();
            this.btnXML = new System.Windows.Forms.Button();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.LeyendaFiscal = new System.Windows.Forms.TextBox();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.observacionesPDF = new System.Windows.Forms.TextBox();
            this.tabPage4 = new System.Windows.Forms.TabPage();
            this.label14 = new System.Windows.Forms.Label();
            this.customerId = new System.Windows.Forms.TextBox();
            this.checkId = new System.Windows.Forms.TextBox();
            this.label13 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.informacion = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.receptorNumRegIdTrib = new System.Windows.Forms.TextBox();
            this.btnImprimir = new System.Windows.Forms.Button();
            this.FORMA_DE_PAGO = new System.Windows.Forms.ComboBox();
            this.label10 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.total = new System.Windows.Forms.TextBox();
            this.button1 = new System.Windows.Forms.Button();
            this.tabControl2 = new System.Windows.Forms.TabControl();
            this.tabPage5 = new System.Windows.Forms.TabPage();
            this.tabPage6 = new System.Windows.Forms.TabPage();
            this.dtgTipoRelacion = new System.Windows.Forms.DataGridView();
            this.uuidRelacion = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.button4 = new System.Windows.Forms.Button();
            this.button7 = new System.Windows.Forms.Button();
            this.tipoRelacion = new System.Windows.Forms.ComboBox();
            this.label15 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.dtgFolio)).BeginInit();
            this.tabControl1.SuspendLayout();
            this.tabPage3.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.tabPage2.SuspendLayout();
            this.tabPage4.SuspendLayout();
            this.tabControl2.SuspendLayout();
            this.tabPage5.SuspendLayout();
            this.tabPage6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dtgTipoRelacion)).BeginInit();
            this.SuspendLayout();
            // 
            // receptor
            // 
            this.receptor.Location = new System.Drawing.Point(99, 80);
            this.receptor.Name = "receptor";
            this.receptor.Size = new System.Drawing.Size(127, 20);
            this.receptor.TabIndex = 11;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(9, 110);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(79, 13);
            this.label1.TabIndex = 15;
            this.label1.Text = "Fecha de pago";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(448, 112);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(75, 13);
            this.label3.TabIndex = 21;
            this.label3.Text = "Cuenta Origen";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(445, 135);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(80, 13);
            this.label4.TabIndex = 23;
            this.label4.Text = "Cuenta Destino";
            // 
            // fechaPago
            // 
            this.fechaPago.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.fechaPago.Location = new System.Drawing.Point(99, 106);
            this.fechaPago.Name = "fechaPago";
            this.fechaPago.Size = new System.Drawing.Size(127, 20);
            this.fechaPago.TabIndex = 16;
            // 
            // textBox3
            // 
            this.textBox3.Location = new System.Drawing.Point(530, 106);
            this.textBox3.Name = "textBox3";
            this.textBox3.Size = new System.Drawing.Size(116, 20);
            this.textBox3.TabIndex = 22;
            // 
            // textBox4
            // 
            this.textBox4.Location = new System.Drawing.Point(530, 132);
            this.textBox4.Name = "textBox4";
            this.textBox4.Size = new System.Drawing.Size(116, 20);
            this.textBox4.TabIndex = 24;
            // 
            // serie
            // 
            this.serie.Location = new System.Drawing.Point(530, 49);
            this.serie.Name = "serie";
            this.serie.Size = new System.Drawing.Size(116, 20);
            this.serie.TabIndex = 9;
            // 
            // btnDeleteItem
            // 
            this.btnDeleteItem.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(219)))), ((int)(((byte)(94)))), ((int)(((byte)(94)))));
            this.btnDeleteItem.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnDeleteItem.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnDeleteItem.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnDeleteItem.Location = new System.Drawing.Point(177, 12);
            this.btnDeleteItem.Name = "btnDeleteItem";
            this.btnDeleteItem.Size = new System.Drawing.Size(76, 28);
            this.btnDeleteItem.TabIndex = 2;
            this.btnDeleteItem.Text = "Eliminar";
            this.btnDeleteItem.UseVisualStyleBackColor = false;
            this.btnDeleteItem.Click += new System.EventHandler(this.btnDeleteItem_Click);
            // 
            // btnNuevo
            // 
            this.btnNuevo.BackColor = System.Drawing.Color.SteelBlue;
            this.btnNuevo.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnNuevo.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnNuevo.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnNuevo.Location = new System.Drawing.Point(12, 12);
            this.btnNuevo.Name = "btnNuevo";
            this.btnNuevo.Size = new System.Drawing.Size(76, 28);
            this.btnNuevo.TabIndex = 0;
            this.btnNuevo.Text = "Nuevo";
            this.btnNuevo.UseVisualStyleBackColor = false;
            this.btnNuevo.Click += new System.EventHandler(this.buttonAdv1_Click);
            // 
            // btnAddItem
            // 
            this.btnAddItem.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(153)))), ((int)(((byte)(181)))), ((int)(((byte)(105)))));
            this.btnAddItem.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnAddItem.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAddItem.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnAddItem.Location = new System.Drawing.Point(95, 12);
            this.btnAddItem.Name = "btnAddItem";
            this.btnAddItem.Size = new System.Drawing.Size(76, 28);
            this.btnAddItem.TabIndex = 1;
            this.btnAddItem.Text = "Guardar";
            this.btnAddItem.UseVisualStyleBackColor = false;
            this.btnAddItem.Click += new System.EventHandler(this.btnAddItem_Click);
            // 
            // button3
            // 
            this.button3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(219)))), ((int)(((byte)(94)))), ((int)(((byte)(94)))));
            this.button3.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button3.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button3.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.button3.Location = new System.Drawing.Point(12, 164);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(35, 28);
            this.button3.TabIndex = 28;
            this.button3.Text = "-";
            this.button3.UseVisualStyleBackColor = false;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // button5
            // 
            this.button5.BackColor = System.Drawing.Color.DarkKhaki;
            this.button5.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button5.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button5.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.button5.Location = new System.Drawing.Point(448, 44);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(76, 28);
            this.button5.TabIndex = 8;
            this.button5.Text = "Serie";
            this.button5.UseVisualStyleBackColor = false;
            this.button5.Click += new System.EventHandler(this.button5_Click);
            // 
            // button2
            // 
            this.button2.BackColor = System.Drawing.Color.DarkKhaki;
            this.button2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button2.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button2.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.button2.Location = new System.Drawing.Point(12, 75);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(76, 28);
            this.button2.TabIndex = 10;
            this.button2.Text = "Receptor";
            this.button2.UseVisualStyleBackColor = false;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // dtgFolio
            // 
            this.dtgFolio.AllowUserToAddRows = false;
            this.dtgFolio.AllowUserToDeleteRows = false;
            this.dtgFolio.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dtgFolio.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dtgFolio.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.facturaCol,
            this.folioCol,
            this.serieCol,
            this.tipoCambioDRCol,
            this.uuidCol,
            this.fechaCol,
            this.totalCol,
            this.NumParcialidad,
            this.Aplicado,
            this.ImpSaldoAntCol,
            this.monedaCol,
            this.saldoCol});
            this.dtgFolio.Location = new System.Drawing.Point(6, 6);
            this.dtgFolio.Name = "dtgFolio";
            this.dtgFolio.RowHeadersVisible = false;
            this.dtgFolio.Size = new System.Drawing.Size(754, 136);
            this.dtgFolio.TabIndex = 30;
            this.dtgFolio.CellEndEdit += new System.Windows.Forms.DataGridViewCellEventHandler(this.dtgFolio_CellEndEdit);
            this.dtgFolio.CellEnter += new System.Windows.Forms.DataGridViewCellEventHandler(this.dtgFolio_CellEnter);
            this.dtgFolio.EditingControlShowing += new System.Windows.Forms.DataGridViewEditingControlShowingEventHandler(this.dtgFolio_EditingControlShowing);
            // 
            // facturaCol
            // 
            this.facturaCol.HeaderText = "Factura";
            this.facturaCol.Name = "facturaCol";
            this.facturaCol.ReadOnly = true;
            // 
            // folioCol
            // 
            this.folioCol.HeaderText = "Folio";
            this.folioCol.Name = "folioCol";
            this.folioCol.Visible = false;
            this.folioCol.Width = 50;
            // 
            // serieCol
            // 
            this.serieCol.HeaderText = "Serie";
            this.serieCol.Name = "serieCol";
            this.serieCol.ReadOnly = true;
            this.serieCol.Visible = false;
            this.serieCol.Width = 50;
            // 
            // tipoCambioDRCol
            // 
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle1.Format = "N2";
            dataGridViewCellStyle1.NullValue = null;
            this.tipoCambioDRCol.DefaultCellStyle = dataGridViewCellStyle1;
            this.tipoCambioDRCol.HeaderText = "TC";
            this.tipoCambioDRCol.Name = "tipoCambioDRCol";
            this.tipoCambioDRCol.Width = 70;
            // 
            // uuidCol
            // 
            this.uuidCol.HeaderText = "Folio";
            this.uuidCol.Name = "uuidCol";
            this.uuidCol.ReadOnly = true;
            this.uuidCol.Width = 200;
            // 
            // fechaCol
            // 
            this.fechaCol.HeaderText = "Fecha";
            this.fechaCol.Name = "fechaCol";
            this.fechaCol.ReadOnly = true;
            // 
            // totalCol
            // 
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle2.Format = "N2";
            this.totalCol.DefaultCellStyle = dataGridViewCellStyle2;
            this.totalCol.HeaderText = "Total";
            this.totalCol.Name = "totalCol";
            // 
            // NumParcialidad
            // 
            dataGridViewCellStyle3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            dataGridViewCellStyle3.Format = "N0";
            dataGridViewCellStyle3.NullValue = "0";
            this.NumParcialidad.DefaultCellStyle = dataGridViewCellStyle3;
            this.NumParcialidad.HeaderText = "Num.Parcialidad";
            this.NumParcialidad.Name = "NumParcialidad";
            // 
            // Aplicado
            // 
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            dataGridViewCellStyle4.Format = "N2";
            dataGridViewCellStyle4.NullValue = "0";
            this.Aplicado.DefaultCellStyle = dataGridViewCellStyle4;
            this.Aplicado.HeaderText = "Aplicado";
            this.Aplicado.Name = "Aplicado";
            // 
            // ImpSaldoAntCol
            // 
            dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle5.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            dataGridViewCellStyle5.Format = "N2";
            this.ImpSaldoAntCol.DefaultCellStyle = dataGridViewCellStyle5;
            this.ImpSaldoAntCol.HeaderText = "Saldo Anterior";
            this.ImpSaldoAntCol.Name = "ImpSaldoAntCol";
            // 
            // monedaCol
            // 
            dataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.monedaCol.DefaultCellStyle = dataGridViewCellStyle6;
            this.monedaCol.HeaderText = "Moneda";
            this.monedaCol.Name = "monedaCol";
            // 
            // saldoCol
            // 
            dataGridViewCellStyle7.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle7.Format = "N2";
            this.saldoCol.DefaultCellStyle = dataGridViewCellStyle7;
            this.saldoCol.HeaderText = "Saldo";
            this.saldoCol.Name = "saldoCol";
            // 
            // button6
            // 
            this.button6.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(153)))), ((int)(((byte)(181)))), ((int)(((byte)(105)))));
            this.button6.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button6.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button6.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.button6.Location = new System.Drawing.Point(53, 164);
            this.button6.Name = "button6";
            this.button6.Size = new System.Drawing.Size(86, 28);
            this.button6.TabIndex = 29;
            this.button6.Text = "Cargar CFDI";
            this.button6.UseVisualStyleBackColor = false;
            this.button6.Click += new System.EventHandler(this.button6_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(9, 51);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(38, 13);
            this.label2.TabIndex = 6;
            this.label2.Text = "Emisor";
            // 
            // emisor
            // 
            this.emisor.FormattingEnabled = true;
            this.emisor.Location = new System.Drawing.Point(59, 48);
            this.emisor.Name = "emisor";
            this.emisor.Size = new System.Drawing.Size(383, 21);
            this.emisor.TabIndex = 7;
            this.emisor.SelectedIndexChanged += new System.EventHandler(this.emisor_SelectedIndexChanged_1);
            // 
            // receptorNombre
            // 
            this.receptorNombre.Location = new System.Drawing.Point(232, 80);
            this.receptorNombre.Name = "receptorNombre";
            this.receptorNombre.Size = new System.Drawing.Size(210, 20);
            this.receptorNombre.TabIndex = 12;
            // 
            // tipoCambio
            // 
            this.tipoCambio.Location = new System.Drawing.Point(375, 107);
            this.tipoCambio.Name = "tipoCambio";
            this.tipoCambio.Size = new System.Drawing.Size(67, 20);
            this.tipoCambio.TabIndex = 19;
            // 
            // moneda
            // 
            this.moneda.FormattingEnabled = true;
            this.moneda.Items.AddRange(new object[] {
            "USD",
            "MXN"});
            this.moneda.Location = new System.Drawing.Point(285, 107);
            this.moneda.Name = "moneda";
            this.moneda.Size = new System.Drawing.Size(56, 21);
            this.moneda.TabIndex = 18;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(348, 111);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(21, 13);
            this.label6.TabIndex = 20;
            this.label6.Text = "TC";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(232, 111);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(46, 13);
            this.label5.TabIndex = 17;
            this.label5.Text = "Moneda";
            // 
            // estado
            // 
            this.estado.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.estado.FormattingEnabled = true;
            this.estado.Items.AddRange(new object[] {
            "BORRADOR",
            "REAL"});
            this.estado.Location = new System.Drawing.Point(443, 12);
            this.estado.Name = "estado";
            this.estado.Size = new System.Drawing.Size(203, 28);
            this.estado.TabIndex = 5;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(361, 12);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(81, 29);
            this.label7.TabIndex = 4;
            this.label7.Text = "Modo:";
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage3);
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Controls.Add(this.tabPage4);
            this.tabControl1.Location = new System.Drawing.Point(12, 378);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(774, 130);
            this.tabControl1.TabIndex = 31;
            // 
            // tabPage3
            // 
            this.tabPage3.Controls.Add(this.label8);
            this.tabPage3.Controls.Add(this.uuid);
            this.tabPage3.Controls.Add(this.pdf);
            this.tabPage3.Controls.Add(this.xml);
            this.tabPage3.Controls.Add(this.btnPDF);
            this.tabPage3.Controls.Add(this.btnXML);
            this.tabPage3.Location = new System.Drawing.Point(4, 22);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage3.Size = new System.Drawing.Size(766, 104);
            this.tabPage3.TabIndex = 2;
            this.tabPage3.Text = "Informaci�n general";
            this.tabPage3.UseVisualStyleBackColor = true;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(61, 79);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(56, 13);
            this.label8.TabIndex = 4;
            this.label8.Text = "Folio fiscal";
            // 
            // uuid
            // 
            this.uuid.Location = new System.Drawing.Point(123, 75);
            this.uuid.Name = "uuid";
            this.uuid.Size = new System.Drawing.Size(637, 20);
            this.uuid.TabIndex = 5;
            // 
            // pdf
            // 
            this.pdf.Location = new System.Drawing.Point(123, 45);
            this.pdf.Name = "pdf";
            this.pdf.Size = new System.Drawing.Size(637, 20);
            this.pdf.TabIndex = 3;
            // 
            // xml
            // 
            this.xml.Location = new System.Drawing.Point(123, 11);
            this.xml.Name = "xml";
            this.xml.Size = new System.Drawing.Size(637, 20);
            this.xml.TabIndex = 1;
            // 
            // btnPDF
            // 
            this.btnPDF.BackColor = System.Drawing.Color.Olive;
            this.btnPDF.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnPDF.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnPDF.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnPDF.Location = new System.Drawing.Point(6, 40);
            this.btnPDF.Name = "btnPDF";
            this.btnPDF.Size = new System.Drawing.Size(111, 28);
            this.btnPDF.TabIndex = 2;
            this.btnPDF.Text = "Archivo PDF";
            this.btnPDF.UseVisualStyleBackColor = false;
            this.btnPDF.Click += new System.EventHandler(this.btnPDF_Click);
            // 
            // btnXML
            // 
            this.btnXML.BackColor = System.Drawing.Color.Olive;
            this.btnXML.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnXML.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnXML.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnXML.Location = new System.Drawing.Point(6, 6);
            this.btnXML.Name = "btnXML";
            this.btnXML.Size = new System.Drawing.Size(111, 28);
            this.btnXML.TabIndex = 0;
            this.btnXML.Text = "Archivo XML";
            this.btnXML.UseVisualStyleBackColor = false;
            this.btnXML.Click += new System.EventHandler(this.btnXML_Click);
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.LeyendaFiscal);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(766, 104);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Leyenda fiscal";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // LeyendaFiscal
            // 
            this.LeyendaFiscal.Dock = System.Windows.Forms.DockStyle.Fill;
            this.LeyendaFiscal.Location = new System.Drawing.Point(3, 3);
            this.LeyendaFiscal.Multiline = true;
            this.LeyendaFiscal.Name = "LeyendaFiscal";
            this.LeyendaFiscal.Size = new System.Drawing.Size(760, 98);
            this.LeyendaFiscal.TabIndex = 18;
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.observacionesPDF);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(766, 104);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Observaciones en PDF";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // observacionesPDF
            // 
            this.observacionesPDF.Dock = System.Windows.Forms.DockStyle.Fill;
            this.observacionesPDF.Location = new System.Drawing.Point(3, 3);
            this.observacionesPDF.Multiline = true;
            this.observacionesPDF.Name = "observacionesPDF";
            this.observacionesPDF.Size = new System.Drawing.Size(760, 98);
            this.observacionesPDF.TabIndex = 19;
            // 
            // tabPage4
            // 
            this.tabPage4.Controls.Add(this.label14);
            this.tabPage4.Controls.Add(this.customerId);
            this.tabPage4.Controls.Add(this.checkId);
            this.tabPage4.Controls.Add(this.label13);
            this.tabPage4.Controls.Add(this.label12);
            this.tabPage4.Location = new System.Drawing.Point(4, 22);
            this.tabPage4.Name = "tabPage4";
            this.tabPage4.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage4.Size = new System.Drawing.Size(766, 104);
            this.tabPage4.TabIndex = 3;
            this.tabPage4.Text = "Asociar Pago";
            this.tabPage4.UseVisualStyleBackColor = true;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(6, 12);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(365, 13);
            this.label14.TabIndex = 4;
            this.label14.Text = "Aqu� se agrega para quitar el Pago de la Bandeja de Complemento de Pago";
            // 
            // customerId
            // 
            this.customerId.Location = new System.Drawing.Point(51, 67);
            this.customerId.Name = "customerId";
            this.customerId.Size = new System.Drawing.Size(211, 20);
            this.customerId.TabIndex = 3;
            // 
            // checkId
            // 
            this.checkId.Location = new System.Drawing.Point(51, 41);
            this.checkId.Name = "checkId";
            this.checkId.Size = new System.Drawing.Size(211, 20);
            this.checkId.TabIndex = 1;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(6, 70);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(39, 13);
            this.label13.TabIndex = 2;
            this.label13.Text = "Cliente";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(6, 44);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(32, 13);
            this.label12.TabIndex = 0;
            this.label12.Text = "Pago";
            // 
            // informacion
            // 
            this.informacion.BackColor = System.Drawing.Color.DarkGray;
            this.informacion.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.informacion.ForeColor = System.Drawing.Color.Blue;
            this.informacion.Location = new System.Drawing.Point(12, 511);
            this.informacion.Name = "informacion";
            this.informacion.Size = new System.Drawing.Size(774, 28);
            this.informacion.TabIndex = 471;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(451, 83);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(73, 13);
            this.label9.TabIndex = 13;
            this.label9.Text = "Num Reg Trib";
            // 
            // receptorNumRegIdTrib
            // 
            this.receptorNumRegIdTrib.Location = new System.Drawing.Point(530, 80);
            this.receptorNumRegIdTrib.Name = "receptorNumRegIdTrib";
            this.receptorNumRegIdTrib.Size = new System.Drawing.Size(116, 20);
            this.receptorNumRegIdTrib.TabIndex = 14;
            // 
            // btnImprimir
            // 
            this.btnImprimir.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(128)))));
            this.btnImprimir.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnImprimir.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnImprimir.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnImprimir.Location = new System.Drawing.Point(259, 12);
            this.btnImprimir.Name = "btnImprimir";
            this.btnImprimir.Size = new System.Drawing.Size(91, 28);
            this.btnImprimir.TabIndex = 3;
            this.btnImprimir.Text = "Imprimir";
            this.btnImprimir.UseVisualStyleBackColor = false;
            this.btnImprimir.Click += new System.EventHandler(this.btnImprimir_Click);
            // 
            // FORMA_DE_PAGO
            // 
            this.FORMA_DE_PAGO.FormattingEnabled = true;
            this.FORMA_DE_PAGO.Items.AddRange(new object[] {
            "Pago en una sola exhibici�n",
            "Pago en parcialidades",
            "Pago a credito"});
            this.FORMA_DE_PAGO.Location = new System.Drawing.Point(99, 132);
            this.FORMA_DE_PAGO.Name = "FORMA_DE_PAGO";
            this.FORMA_DE_PAGO.Size = new System.Drawing.Size(343, 21);
            this.FORMA_DE_PAGO.TabIndex = 26;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(9, 135);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(79, 13);
            this.label10.TabIndex = 25;
            this.label10.Text = "Forma de Pago";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(145, 167);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(51, 24);
            this.label11.TabIndex = 472;
            this.label11.Text = "Total";
            // 
            // total
            // 
            this.total.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.total.Location = new System.Drawing.Point(202, 163);
            this.total.Name = "total";
            this.total.ReadOnly = true;
            this.total.Size = new System.Drawing.Size(183, 29);
            this.total.TabIndex = 473;
            this.total.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // button1
            // 
            this.button1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(153)))), ((int)(((byte)(181)))), ((int)(((byte)(105)))));
            this.button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button1.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button1.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.button1.Location = new System.Drawing.Point(391, 164);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(255, 28);
            this.button1.TabIndex = 29;
            this.button1.Text = "Cambiar forma de pago y otros";
            this.button1.UseVisualStyleBackColor = false;
            this.button1.Click += new System.EventHandler(this.button1_Click_1);
            // 
            // tabControl2
            // 
            this.tabControl2.Controls.Add(this.tabPage5);
            this.tabControl2.Controls.Add(this.tabPage6);
            this.tabControl2.Location = new System.Drawing.Point(12, 198);
            this.tabControl2.Name = "tabControl2";
            this.tabControl2.SelectedIndex = 0;
            this.tabControl2.Size = new System.Drawing.Size(775, 174);
            this.tabControl2.TabIndex = 474;
            // 
            // tabPage5
            // 
            this.tabPage5.Controls.Add(this.dtgFolio);
            this.tabPage5.Location = new System.Drawing.Point(4, 22);
            this.tabPage5.Name = "tabPage5";
            this.tabPage5.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage5.Size = new System.Drawing.Size(767, 148);
            this.tabPage5.TabIndex = 0;
            this.tabPage5.Text = "Facturas";
            this.tabPage5.UseVisualStyleBackColor = true;
            // 
            // tabPage6
            // 
            this.tabPage6.Controls.Add(this.tipoRelacion);
            this.tabPage6.Controls.Add(this.label15);
            this.tabPage6.Controls.Add(this.button7);
            this.tabPage6.Controls.Add(this.button4);
            this.tabPage6.Controls.Add(this.dtgTipoRelacion);
            this.tabPage6.Location = new System.Drawing.Point(4, 22);
            this.tabPage6.Name = "tabPage6";
            this.tabPage6.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage6.Size = new System.Drawing.Size(767, 148);
            this.tabPage6.TabIndex = 1;
            this.tabPage6.Text = "Tipos de Complementos de Pago";
            this.tabPage6.UseVisualStyleBackColor = true;
            // 
            // dtgTipoRelacion
            // 
            this.dtgTipoRelacion.AllowUserToAddRows = false;
            this.dtgTipoRelacion.AllowUserToDeleteRows = false;
            this.dtgTipoRelacion.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dtgTipoRelacion.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dtgTipoRelacion.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.uuidRelacion});
            this.dtgTipoRelacion.Location = new System.Drawing.Point(6, 41);
            this.dtgTipoRelacion.Name = "dtgTipoRelacion";
            this.dtgTipoRelacion.RowHeadersVisible = false;
            this.dtgTipoRelacion.Size = new System.Drawing.Size(754, 101);
            this.dtgTipoRelacion.TabIndex = 31;
            // 
            // uuidRelacion
            // 
            dataGridViewCellStyle8.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle8.NullValue = null;
            this.uuidRelacion.DefaultCellStyle = dataGridViewCellStyle8;
            this.uuidRelacion.HeaderText = "UUID Relacionado";
            this.uuidRelacion.Name = "uuidRelacion";
            this.uuidRelacion.Width = 400;
            // 
            // button4
            // 
            this.button4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(219)))), ((int)(((byte)(94)))), ((int)(((byte)(94)))));
            this.button4.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button4.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button4.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.button4.Location = new System.Drawing.Point(10, 6);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(35, 30);
            this.button4.TabIndex = 32;
            this.button4.Text = "-";
            this.button4.UseVisualStyleBackColor = false;
            this.button4.Click += new System.EventHandler(this.button4_Click_1);
            // 
            // button7
            // 
            this.button7.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(153)))), ((int)(((byte)(181)))), ((int)(((byte)(105)))));
            this.button7.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button7.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button7.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.button7.Location = new System.Drawing.Point(51, 7);
            this.button7.Name = "button7";
            this.button7.Size = new System.Drawing.Size(35, 28);
            this.button7.TabIndex = 33;
            this.button7.Text = "+";
            this.button7.UseVisualStyleBackColor = false;
            this.button7.Click += new System.EventHandler(this.button7_Click);
            // 
            // tipoRelacion
            // 
            this.tipoRelacion.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tipoRelacion.FormattingEnabled = true;
            this.tipoRelacion.Location = new System.Drawing.Point(186, 11);
            this.tipoRelacion.Name = "tipoRelacion";
            this.tipoRelacion.Size = new System.Drawing.Size(444, 21);
            this.tipoRelacion.TabIndex = 35;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(95, 15);
            this.label15.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(83, 13);
            this.label15.TabIndex = 34;
            this.label15.Text = "Tipo de relaci�n";
            // 
            // frmComprobantePago
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(799, 552);
            this.Controls.Add(this.tabControl2);
            this.Controls.Add(this.total);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.FORMA_DE_PAGO);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.btnImprimir);
            this.Controls.Add(this.informacion);
            this.Controls.Add(this.tabControl1);
            this.Controls.Add(this.estado);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.tipoCambio);
            this.Controls.Add(this.moneda);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.emisor);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.button5);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.button6);
            this.Controls.Add(this.button3);
            this.Controls.Add(this.btnDeleteItem);
            this.Controls.Add(this.btnNuevo);
            this.Controls.Add(this.btnAddItem);
            this.Controls.Add(this.textBox4);
            this.Controls.Add(this.textBox3);
            this.Controls.Add(this.fechaPago);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.receptorNombre);
            this.Controls.Add(this.receptor);
            this.Controls.Add(this.receptorNumRegIdTrib);
            this.Controls.Add(this.serie);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MetroColor = System.Drawing.Color.Transparent;
            this.Name = "frmComprobantePago";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Comprobante de Pago";
            this.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.frmComprobantePago_KeyPress);
            ((System.ComponentModel.ISupportInitialize)(this.dtgFolio)).EndInit();
            this.tabControl1.ResumeLayout(false);
            this.tabPage3.ResumeLayout(false);
            this.tabPage3.PerformLayout();
            this.tabPage1.ResumeLayout(false);
            this.tabPage1.PerformLayout();
            this.tabPage2.ResumeLayout(false);
            this.tabPage2.PerformLayout();
            this.tabPage4.ResumeLayout(false);
            this.tabPage4.PerformLayout();
            this.tabControl2.ResumeLayout(false);
            this.tabPage5.ResumeLayout(false);
            this.tabPage6.ResumeLayout(false);
            this.tabPage6.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dtgTipoRelacion)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.TextBox receptor;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.DateTimePicker fechaPago;
        private System.Windows.Forms.TextBox textBox3;
        private System.Windows.Forms.TextBox textBox4;
        private System.Windows.Forms.TextBox serie;
        private System.Windows.Forms.Button btnDeleteItem;
        private System.Windows.Forms.Button btnNuevo;
        private System.Windows.Forms.Button btnAddItem;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Button button5;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.DataGridView dtgFolio;
        private System.Windows.Forms.Button button6;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ComboBox emisor;
        private System.Windows.Forms.TextBox receptorNombre;
        private System.Windows.Forms.TextBox tipoCambio;
        private System.Windows.Forms.ComboBox moneda;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.ComboBox estado;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage3;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox uuid;
        private System.Windows.Forms.TextBox pdf;
        private System.Windows.Forms.TextBox xml;
        private System.Windows.Forms.Button btnPDF;
        private System.Windows.Forms.Button btnXML;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TextBox LeyendaFiscal;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.TextBox observacionesPDF;
        private System.Windows.Forms.Label informacion;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox receptorNumRegIdTrib;
        private System.Windows.Forms.Button btnImprimir;
        private System.Windows.Forms.ComboBox FORMA_DE_PAGO;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TextBox total;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.TabPage tabPage4;
        private System.Windows.Forms.TextBox customerId;
        private System.Windows.Forms.TextBox checkId;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.DataGridViewTextBoxColumn facturaCol;
        private System.Windows.Forms.DataGridViewTextBoxColumn folioCol;
        private System.Windows.Forms.DataGridViewTextBoxColumn serieCol;
        private System.Windows.Forms.DataGridViewTextBoxColumn tipoCambioDRCol;
        private System.Windows.Forms.DataGridViewTextBoxColumn uuidCol;
        private System.Windows.Forms.DataGridViewTextBoxColumn fechaCol;
        private System.Windows.Forms.DataGridViewTextBoxColumn totalCol;
        private System.Windows.Forms.DataGridViewTextBoxColumn NumParcialidad;
        private System.Windows.Forms.DataGridViewTextBoxColumn Aplicado;
        private System.Windows.Forms.DataGridViewTextBoxColumn ImpSaldoAntCol;
        private System.Windows.Forms.DataGridViewTextBoxColumn monedaCol;
        private System.Windows.Forms.DataGridViewTextBoxColumn saldoCol;
        private System.Windows.Forms.TabControl tabControl2;
        private System.Windows.Forms.TabPage tabPage5;
        private System.Windows.Forms.TabPage tabPage6;
        private System.Windows.Forms.DataGridView dtgTipoRelacion;
        private System.Windows.Forms.DataGridViewTextBoxColumn uuidRelacion;
        private System.Windows.Forms.Button button7;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.ComboBox tipoRelacion;
        private System.Windows.Forms.Label label15;
    }
}