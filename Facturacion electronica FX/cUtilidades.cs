﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FE_FX
{
    class cUtilidades
    {
        internal static bool validarMetodopago(string metodoDePago)
        {
            //Validar que sea solo , y 00
            //Que esten el catalogo
            string[] metodos = metodoDePago.Split(',');
            foreach (string metodo in metodos)
            {
                string[] stringArray = { "01", "02", "03", "04", "05", "06", "08", "28", "29", "99", "NA" };
                //Existe
                bool existe = false;
                foreach (string x in stringArray)
                {
                    if (x.Contains(metodo))
                    {
                        existe = true;
                        break;
                    }
                }
                if (!existe)
                {
                    return false;
                }
            }
            return true;
        }
    }
}
