﻿using System;
using System.Collections.Generic;

using System.Text;
using System.Data;
using System.Windows;
using System.Windows.Forms;
using Generales;
using ModeloDocumentosFX;

namespace FE_FX
{
    class cUSUARIO
    {
        private string sSQL = "";
        public string ROW_ID { get; set; }
        public string NOMBRE { get; set; }
        public string APELLIDO { get; set; }
        public string USUARIO { get; set; }
        public string PASSWORD { get; set; }

        public string ACTIVO { get; set; }

        public string ADMINISTRADOR { get; set; }
        public string LECTURA { get; set; }
        public string SERIE { get; set; }


        public string CANCELAR { get; set; }
        public string VERIFICAR { get; set; }
        public string CAMBIAR { get; set; }
        public string PEDIMENTOS { get; set; }
        public string EXPORTAR { get; set; }
        public string IMPORTAR { get; set; }
        public string ADDENDA { get; set; }
        public string ADDENDA_INDIVIDUAL { get; set; }

        private cCONEXCION oData = new cCONEXCION("");

        public cUSUARIO()
        {
            ModeloDocumentosFXContainer dbContext = new ModeloDocumentosFXContainer();
            try
            {
                string conection = dbContext.Database.Connection.ConnectionString;
                oData = new cCONEXCION(conection);
            }
            catch (Exception err)
            {
                ErrorFX.mostrar(err, false, true, "cCERTIFICADO - 71 - ");
            }
            limpiar();
        }

        public void limpiar()
        {
            ROW_ID = "";
            NOMBRE = "";
            APELLIDO = "";
            USUARIO = "";
            PASSWORD = "";
            ACTIVO = "";
            ADMINISTRADOR="";
            SERIE="";
            LECTURA = "";
            CANCELAR = "";
            VERIFICAR = "";
            CAMBIAR = "";
            PEDIMENTOS = "";
            EXPORTAR = "";
            IMPORTAR = "";
            ADDENDA = "";
            ADDENDA_INDIVIDUAL = "";

        }

        public List<cUSUARIO> todos(string BUSQUEDA)
        {

            List<cUSUARIO> lista = new List<cUSUARIO>();

            sSQL = " SELECT * ";
            sSQL += " FROM USUARIO ";
            sSQL += " WHERE NOMBRE LIKE '%" + BUSQUEDA + "%' ";
            DataTable oDataTable = oData.EjecutarConsulta(sSQL);
            foreach (DataRow oDataRow in oDataTable.Rows)
            {
                lista.Add(new cUSUARIO()
                {
                    ROW_ID = oDataRow["ROW_ID"].ToString()
                   ,NOMBRE = oDataRow["NOMBRE"].ToString()
                   ,APELLIDO = oDataRow["APELLIDO"].ToString()
                   ,USUARIO = oDataRow["USUARIO"].ToString()
                   ,PASSWORD = oDataRow["PASSWORD"].ToString()
                   ,
                    ACTIVO = oDataRow["ACTIVO"].ToString()
            ,
                    ADMINISTRADOR = oDataRow["ADMINISTRADOR"].ToString()
 ,
                    LECTURA = oDataRow["LECTURA"].ToString()
           ,
                    SERIE = oDataRow["SERIE"].ToString()
                    ,
                    CANCELAR = oDataRow["CANCELAR"].ToString(),
                    VERIFICAR = oDataRow["VERIFICAR"].ToString(),
                    CAMBIAR = oDataRow["CAMBIAR"].ToString(),
                    PEDIMENTOS = oDataRow["PEDIMENTOS"].ToString(),
                    EXPORTAR = oDataRow["EXPORTAR"].ToString(),
                    IMPORTAR = oDataRow["IMPORTAR"].ToString(),
                    ADDENDA = oDataRow["ADDENDA"].ToString(),
                    ADDENDA_INDIVIDUAL = oDataRow["ADDENDA_INDIVIDUAL"].ToString()
                });
            }
            return lista;

        }


        public bool login(string USUARIO, string PASSWORD)
        {
            if ((USUARIO == "FE") & (PASSWORD == "FX"))
            {
                return true;
            }
            if(PASSWORD.Trim()=="")
            {
                return false;
            }
            PASSWORD = cENCRIPTACION.Encrypt(PASSWORD);

            sSQL = " SELECT * ";
            sSQL += " FROM USUARIO ";
            sSQL += " WHERE USUARIO='" + USUARIO + "' ";
            sSQL += " AND PASSWORD='" + PASSWORD + "' ";
            DataTable oDataTable = oData.EjecutarConsulta(sSQL);
            if (oDataTable != null)
            {
                foreach (DataRow oDataRow in oDataTable.Rows)
                {
                    //Añadir el Registro
                    ROW_ID = oDataRow["ROW_ID"].ToString();
                    NOMBRE = oDataRow["NOMBRE"].ToString();
                    APELLIDO = oDataRow["APELLIDO"].ToString();
                    USUARIO = oDataRow["USUARIO"].ToString();
                    PASSWORD = cENCRIPTACION.Decrypt(oDataRow["PASSWORD"].ToString());
                    ACTIVO = oDataRow["ACTIVO"].ToString();
                    ADMINISTRADOR = oDataRow["ADMINISTRADOR"].ToString();
                    LECTURA = oDataRow["LECTURA"].ToString();
                    SERIE = oDataRow["SERIE"].ToString();
                    CANCELAR = oDataRow["CANCELAR"].ToString();
                    VERIFICAR = oDataRow["VERIFICAR"].ToString();
                    CAMBIAR = oDataRow["CAMBIAR"].ToString();
                    PEDIMENTOS = oDataRow["PEDIMENTOS"].ToString();
                    EXPORTAR = oDataRow["EXPORTAR"].ToString();
                    IMPORTAR = oDataRow["IMPORTAR"].ToString();
                    ADDENDA = oDataRow["ADDENDA"].ToString();
                    ADDENDA_INDIVIDUAL = oDataRow["ADDENDA_INDIVIDUAL"].ToString();

                    if (bool.Parse(ACTIVO))
                    {
                        return true;
                    }
                    else
                    {
                        MessageBox.Show("El Usuario esta inactivo.", Application.ProductName + "-" + Application.ProductVersion, MessageBoxButtons.OK, MessageBoxIcon.Information);
                        return false;
                    }

                }
                MessageBox.Show("El Usuario o Contraseña incorrecta.", Application.ProductName + "-" + Application.ProductVersion, MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            return false;
        }

        public bool cargar(string ROW_IDp)
        {
            sSQL = " SELECT * ";
            sSQL += " FROM USUARIO ";
            sSQL += " WHERE ROW_ID=" + ROW_IDp + " ";
            DataTable oDataTable = oData.EjecutarConsulta(sSQL);
            if (oDataTable != null)
            {
                foreach (DataRow oDataRow in oDataTable.Rows)
                {
                    //Añadir el Registro
                    ROW_ID = oDataRow["ROW_ID"].ToString();
                    NOMBRE = oDataRow["NOMBRE"].ToString();
                    APELLIDO = oDataRow["APELLIDO"].ToString();
                    USUARIO = oDataRow["USUARIO"].ToString();
                    PASSWORD = cENCRIPTACION.Decrypt(oDataRow["PASSWORD"].ToString());
                    ACTIVO = oDataRow["ACTIVO"].ToString();
                    ADMINISTRADOR = oDataRow["ADMINISTRADOR"].ToString();
                    LECTURA = oDataRow["LECTURA"].ToString();
                    SERIE = oDataRow["SERIE"].ToString();

                    CANCELAR = oDataRow["CANCELAR"].ToString();
                    VERIFICAR = oDataRow["VERIFICAR"].ToString();
                    CAMBIAR = oDataRow["CAMBIAR"].ToString();
                    PEDIMENTOS = oDataRow["PEDIMENTOS"].ToString();
                    EXPORTAR = oDataRow["EXPORTAR"].ToString();
                    IMPORTAR = oDataRow["IMPORTAR"].ToString();
                    ADDENDA = oDataRow["ADDENDA"].ToString();
                    ADDENDA_INDIVIDUAL = oDataRow["ADDENDA_INDIVIDUAL"].ToString();
                    PROFORMA=  oDataRow["PROFORMA"].ToString();
                    COMPROBANTE_PAGO=  oDataRow["COMPROBANTE_PAGO"].ToString();
                    proformaConfiguracion = oDataRow["proformaConfiguracion"].ToString();
                    try
                    {
                        CartaPorte = oDataRow["CartaPorte"].ToString();
                    }
                    catch
                    {

                    }
                    return true;
                }
            }
            return false;
        }

        public bool cargar_ID(string USUARIOp)
        {
            sSQL = " SELECT * ";
            sSQL += " FROM USUARIO ";
            sSQL += " WHERE USUARIO='" + USUARIOp + "' ";
            DataTable oDataTable = oData.EjecutarConsulta(sSQL);
            if (oDataTable != null)
            {
                foreach (DataRow oDataRow in oDataTable.Rows)
                {
                    //Añadir el Registro
                    ROW_ID = oDataRow["ROW_ID"].ToString();
                    NOMBRE = oDataRow["NOMBRE"].ToString();
                    APELLIDO = oDataRow["APELLIDO"].ToString();
                    USUARIO = oDataRow["USUARIO"].ToString();
                    PASSWORD = cENCRIPTACION.Decrypt(oDataRow["PASSWORD"].ToString());
                    ACTIVO = oDataRow["ACTIVO"].ToString();
                    ADMINISTRADOR = oDataRow["ADMINISTRADOR"].ToString();
                    LECTURA = oDataRow["LECTURA"].ToString();
                    SERIE = oDataRow["SERIE"].ToString();

                    CANCELAR = oDataRow["CANCELAR"].ToString();
                    VERIFICAR = oDataRow["VERIFICAR"].ToString();
                    CAMBIAR = oDataRow["CAMBIAR"].ToString();
                    PEDIMENTOS = oDataRow["PEDIMENTOS"].ToString();
                    EXPORTAR = oDataRow["EXPORTAR"].ToString();
                    IMPORTAR = oDataRow["IMPORTAR"].ToString();
                    ADDENDA = oDataRow["ADDENDA"].ToString();
                    ADDENDA_INDIVIDUAL = oDataRow["ADDENDA_INDIVIDUAL"].ToString();
                    try
                    {
                        PROFORMA = oDataRow["PROFORMA"].ToString();
                    }
                    catch
                    {

                    }
                    try
                    {
                        COMPROBANTE_PAGO = oDataRow["COMPROBANTE_PAGO"].ToString();
                    }
                    catch
                    {

                    }
                    try
                    {
                        CartaPorte = oDataRow["CartaPorte"].ToString();
                    }
                    catch
                    {

                    }
                    return true;
                }
            }
            return false;
        }

        public bool eliminar(string ROW_IDp)
        {
            if (ROW_IDp != "")
            {
                sSQL = "DELETE FROM USUARIO WHERE ROW_ID=" + ROW_IDp;
                oData.EjecutarConsulta(sSQL);
                return true;
            }
            return false;

        }

        public bool guardar()
        {
            if(USUARIO.Trim()=="")
            {
                MessageBox.Show("El Usuario debe tener valor.", Application.ProductName + "-" + Application.ProductVersion, MessageBoxButtons.OK, MessageBoxIcon.Information);
                return false;
            }
            if (PASSWORD.Trim()=="")
            {
                MessageBox.Show("El Password debe tener valor.", Application.ProductName + "-" + Application.ProductVersion, MessageBoxButtons.OK, MessageBoxIcon.Information);
                return false;
            }

            PASSWORD = cENCRIPTACION.Encrypt(PASSWORD);




            if (ROW_ID == "")
            {
                sSQL = " INSERT INTO USUARIO ";
                sSQL += " ( ";
                sSQL += " NOMBRE ,APELLIDO ";
                sSQL += ",USUARIO,[PASSWORD],ACTIVO ";
                sSQL += ",ADMINISTRADOR,[SERIE], LECTURA";
                sSQL += ",CANCELAR,VERIFICAR,CAMBIAR";
                sSQL += ",PEDIMENTOS,EXPORTAR,IMPORTAR";
                sSQL += ",ADDENDA,ADDENDA_INDIVIDUAL,PROFORMA, COMPROBANTE_PAGO, proformaConfiguracion, CartaPorte";
                sSQL += " )";
                sSQL += " VALUES ";
                sSQL += "(";
                sSQL += " '" + NOMBRE + "','" + APELLIDO + "' ";
                sSQL += ",'" + USUARIO + "','" + PASSWORD + "','" + ACTIVO + "' ";
                sSQL += ",'" + ADMINISTRADOR + "','" + SERIE + "','" + LECTURA + "'";
                sSQL += ",'" + CANCELAR + "','" + VERIFICAR + "','" + CAMBIAR + "'";
                sSQL += ",'" + PEDIMENTOS + "','" + EXPORTAR + "','" + IMPORTAR + "'";
                sSQL += ",'" + ADDENDA + "','" + ADDENDA_INDIVIDUAL + "','" + PROFORMA + "','" + COMPROBANTE_PAGO + "','" + proformaConfiguracion + "','" + CartaPorte + "'";
                sSQL += ")";
                DataTable oDataTable = oData.EjecutarConsulta(sSQL);
                if (oDataTable != null)
                {

                    sSQL = "SELECT TOP 1 MAX(ROW_ID) as ROW_ID FROM USUARIO";
                    oDataTable = oData.EjecutarConsulta(sSQL);
                    if (oDataTable != null)
                    {
                        foreach (DataRow oDataRow in oDataTable.Rows)
                        {
                            ROW_ID = oDataRow["ROW_ID"].ToString();
                        }
                    }
                }

            }
            else
            {
                sSQL = " UPDATE USUARIO ";
                sSQL += " SET NOMBRE='" + NOMBRE + "'";
                sSQL += " ,APELLIDO='" + APELLIDO + "'";
                sSQL += " ,USUARIO='" + USUARIO + "'";
                sSQL += " ,[PASSWORD]='" + PASSWORD + "'";
                sSQL += ",ACTIVO='" + ACTIVO + "' ";
                sSQL += ",ADMINISTRADOR='" + ADMINISTRADOR + "',SERIE='" + SERIE + "',LECTURA='" + LECTURA + "'";
                sSQL += ",CANCELAR='" + CANCELAR + "',VERIFICAR='" + VERIFICAR + "',CAMBIAR='" + CAMBIAR + "'";
                sSQL += ",PEDIMENTOS='" + PEDIMENTOS + "',EXPORTAR='" + EXPORTAR + "',IMPORTAR='" + IMPORTAR + "'";
                sSQL += ",ADDENDA='" + ADDENDA + "',ADDENDA_INDIVIDUAL='" + ADDENDA_INDIVIDUAL + "',PROFORMA='" + PROFORMA + "'";
                sSQL += ",COMPROBANTE_PAGO='" + COMPROBANTE_PAGO + "', proformaConfiguracion='" + proformaConfiguracion + "', CartaPorte='" + CartaPorte + "'";
                sSQL += " WHERE ROW_ID=" + ROW_ID + " ";
                oData.EjecutarConsulta(sSQL);

            }
            return true;

        }

        public string PROFORMA { get; set; }

        public string COMPROBANTE_PAGO { get; set; }

        public string proformaConfiguracion { get; set; }
        public string CartaPorte { get; set; }
    }
}
