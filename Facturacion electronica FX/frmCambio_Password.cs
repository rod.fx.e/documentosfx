﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Windows.Forms;
using System.Text.RegularExpressions;
using FE_FX.EDICOM_Servicio;
using Generales;
using ModeloDocumentosFX;
using System.Data.Entity.Validation;
using FE_FX.Clases;

namespace FE_FX
{
    public partial class frmCambio_Password : Form
    {
        private CFDI_USUARIO oObjeto;
        private bool modificado;
        string ROW_ID_EMPRESA = "";
        cCONEXCION oData;
        public frmCambio_Password(string sConn, Object oEncapsularp, string ROW_ID_EMPRESAp)
        {
            ROW_ID_EMPRESA = ROW_ID_EMPRESAp;
            modificado = false;
            InitializeComponent();
            limpiar();

            cargar(ROW_ID_EMPRESAp);
        }
        
        public void limpiar()
        {
            if (modificado)
            {

                DialogResult Resultado = MessageBox.Show("¿Desea guardar los cambios?", Application.ProductName + "-" + Application.ProductVersion, MessageBoxButtons.YesNo);
                if (Resultado.ToString() == "Yes")
                {
                    guardar();
                }


            }
            oObjeto = new CFDI_USUARIO();
            modificado = false;
            USUARIO.Text = "";
            PASSWORD.Text = "";
            TIPO.Text = "";
        }

        private void guardar()
        {
            ModeloDocumentosFXContainer dbContext = new ModeloDocumentosFXContainer();
            try
            {
                //Buscar la empresa
                //Cargar la empresa
                if (String.IsNullOrEmpty(ROW_ID_EMPRESA))
                {
                    MessageBox.Show("Seleccione la empresa", Application.ProductName + "-" + Application.ProductVersion, MessageBoxButtons.OK);
                    return;
                }

                CFDI_USUARIO registro = new CFDI_USUARIO();
                modificado = false;
                registro = (from r in dbContext.CFDI_USUARIO
                            .Where(a => a.ROW_ID_EMPRESA.ToString().Equals(ROW_ID_EMPRESA))
                            select r
                            ).FirstOrDefault();


                if (registro == null)
                {
                    registro = new CFDI_USUARIO();
                    registro.USUARIO = USUARIO.Text;
                    //Rodrigo Escalona 04/11/2019 Encriptar el password
                    //registro.PASSWORD = PASSWORD.Text;
                    registro.PASSWORD = cENCRIPTACION.Encrypt(PASSWORD.Text);
                    registro.ROW_ID_EMPRESA = double.Parse(ROW_ID_EMPRESA);
                    registro.TIPO = TIPO.Text;

                    dbContext.CFDI_USUARIO.Add(registro);
                }
                else
                {
                    registro.USUARIO = USUARIO.Text;
                    //Rodrigo Escalona 04/11/2019 Encriptar el password
                    //registro.PASSWORD = PASSWORD.Text;
                    registro.PASSWORD = cENCRIPTACION.Encrypt(PASSWORD.Text);
                    registro.TIPO = TIPO.Text;
                    registro.ROW_ID_EMPRESA = double.Parse(ROW_ID_EMPRESA);
                    dbContext.CFDI_USUARIO.Attach(registro);
                    dbContext.Entry(registro).State = System.Data.Entity.EntityState.Modified;
                }

                dbContext.SaveChanges();
                modificado = false;
                toolStripStatusLabel1.Text = "Datos Actualizados " + DateTime.Now.ToShortDateString() + " " + DateTime.Now.ToLongTimeString();
            }
            catch (DbEntityValidationException e)
            {
                ErrorFX.mostrar(e, true, true,false);
                toolStripStatusLabel1.Text = "Error " + DateTime.Now.ToShortDateString() + " " + DateTime.Now.ToLongTimeString();
            }
        }

        private void cargar(string ROW_IDp)
        {
            ModeloDocumentosFXContainer dbContext = new ModeloDocumentosFXContainer();
            try
            {
                double rowId = double.Parse(ROW_IDp);

                CFDI_USUARIO registro = new CFDI_USUARIO();
                registro = (from r in dbContext.CFDI_USUARIO.Where(b => b.ROW_ID_EMPRESA.ToString().Equals(ROW_ID_EMPRESA))
                            select r).FirstOrDefault();
                if (registro != null)
                {
                    string conection = dbContext.Database.Connection.ConnectionString;
                    //Cargar la empresa
                    EMPRESA oEMPRESA = dbContext.EMPRESA.Where(a => a.ROW_ID.ToString().Equals(ROW_IDp)).FirstOrDefault();

                    EMPRESA.Text = oEMPRESA.ID.ToString();
                    USUARIO.Text = registro.USUARIO;
                    PASSWORD.Text = cENCRIPTACION.Decrypt(registro.PASSWORD);
                    TIPO.Text = registro.TIPO;
                    toolStripStatusLabel1.Text = "Datos Cargados " + DateTime.Now.ToShortDateString() + " " + DateTime.Now.ToLongTimeString();
                    modificado = false;
                }
            }
            catch (Exception e)
            {
                ErrorFX.mostrar(e, true, true, false);
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            if (TIPO.Text == "EDICOM")
            {
                verificar_usuario_EDICOM();
            }
            guardar();
        }
        private void verificar_usuario_EDICOM()
        {
            toolStripStatusLabel1.Text = " Enviando al Servicio EDICOM " + DateTime.Now.ToShortDateString() + " " + DateTime.Now.ToLongTimeString();

            if (NEW_PASSWORD.Text != "")
            {
                //Llamar el Servicio
                CFDiClient oCFDiClient = new CFDiClient();

                try
                {
                    //Rodrigo No cambiar el password por Rod
                    return;
                    //oCFDiClient.changePassword(USUARIO.Text, PASSWORD.Text, NEW_PASSWORD.Text);
                    //PASSWORD.Text = NEW_PASSWORD.Text;
                    //guardar();
                }
                catch (Exception oException)
                {

                    CFDiException oCFDiException = new CFDiException();
                    MessageBox.Show(oException.Message, Application.ProductName + "-" + Application.ProductVersion, MessageBoxButtons.OK, MessageBoxIcon.Error);
                    toolStripStatusLabel1.Text = "Error " + DateTime.Now.ToShortDateString() + " " + DateTime.Now.ToLongTimeString();

                    return;
                }
            }
        }
        private void button2_Click(object sender, EventArgs e)
        {
            limpiar();
        }

        private void frmUsuario_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (modificado)
            {
                DialogResult Resultado = MessageBox.Show("¿Desea guardar los cambios?", Application.ProductName + "-" + Application.ProductVersion, MessageBoxButtons.YesNo);
                if (Resultado.ToString() == "Yes")
                {
                    guardar();
                }
            }
        }

        private void frmUsuario_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)27)
            {
                this.Close();
            }
            else
            {
                modificado = true;
                if ((e.KeyChar.ToString() == "<")
                                | (e.KeyChar.ToString() == ">")
                                
                                | (e.KeyChar.ToString() == "'")
                                | (e.KeyChar.ToString() == "#")
                                )
                {
                    e.Handled = true;
                }
            }
        }

        private void Texto_Cambiado_TextChanged(object sender, EventArgs e)
        {
            modificado = true;
        }

        private void Solo_Numero_KeyPress(object sender, KeyPressEventArgs e)
        {
            modificado = true;
            if (!char.IsDigit(e.KeyChar) && !char.IsControl(e.KeyChar) && !char.IsPunctuation(e.KeyChar)) e.Handled = true;
        }

        private void button7_Click(object sender, EventArgs e)
        {
            frmEmpresas oBuscador = new frmEmpresas("","S");
            if (oBuscador.ShowDialog() == DialogResult.OK)
            {
                string ROW_ID = (string)oBuscador.selecionados[0].Cells[0].Value;
                string ID = (string)oBuscador.selecionados[0].Cells[1].Value;
                oObjeto.ROW_ID_EMPRESA = double.Parse(ROW_ID);
                EMPRESA.Text = ID;
            }
        }

        private void checkBox1_CheckedChanged(object sender, EventArgs e)
        {
            PASSWORD.UseSystemPasswordChar = !checkBox1.Checked;
        }
    }
}
