﻿using Generales;
using OfficeOpenXml;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.Entity.Validation;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace FE_FX.Formularios
{
    public partial class frmCceClientes : Form
    {
        public frmCceClientes()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Cargar();
        }

        private void Cargar()
        {
            try
            {
                ModeloDocumentosFX.ModeloDocumentosFXContainer Db = new ModeloDocumentosFX.ModeloDocumentosFXContainer();
                var result = from b in Db.cceClienteSet
                                             .Where(a => a.CustomerId.Contains(buscar.Text)
                                             )
                                             .OrderBy(a => a.CustomerId)
                             select b;
                dtg.Rows.Clear();
                foreach (ModeloDocumentosFX.cceCliente registro in result)
                {
                    if (registro != null)
                    {
                        int n = dtg.Rows.Add();
                        dtg.Rows[n].Tag = registro;
                        dtg.Rows[n].Cells["CustomerId"].Value = registro.CustomerId;
                        dtg.Rows[n].Cells["Exportacion"].Value = registro.Exportacion;


                    }

                }
                Db.Dispose();
                this.dtg.Columns[0].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
            }
            catch (DbEntityValidationException error)
            {
                foreach (var eve in error.EntityValidationErrors)
                {
                    foreach (var ve in eve.ValidationErrors)
                    {
                        MessageBox.Show(this, ve.PropertyName + " Error: " + ve.ErrorMessage, Application.ProductName
                            , MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
            }
        }

        private void button4_Click(object sender, EventArgs e)
        {
            EliminarSeleccionados();
        }


        private void EliminarSeleccionados()
        {
            try
            {
                dtg.EndEdit();
                if (dtg.SelectedRows.Count > 0)
                {
                    foreach (DataGridViewRow Registro in dtg.SelectedRows)
                    {
                        ModeloDocumentosFX.cceCliente Objeto = (ModeloDocumentosFX.cceCliente)Registro.Tag;
                        ModeloDocumentosFX.ModeloDocumentosFXContainer Db = new ModeloDocumentosFX.ModeloDocumentosFXContainer();
                        Db.cceClienteSet.Attach(Objeto);
                        Db.cceClienteSet.Remove(Objeto);
                        Db.SaveChanges();
                    }
                    Cargar();
                    MessageBox.Show("Proceso realizado", Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                else
                {
                    MessageBox.Show("Seleccione las líneas", Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message.ToString());
            }
        }

        private void button7_Click(object sender, EventArgs e)
        {
            Plantilla();
        }

        private void Plantilla()
        {
            try
            {
                MessageBox.Show("La configuración de  la Plantilla es la siguiente:" + Environment.NewLine
                + "Fila 1 - Encabezado" + Environment.NewLine
                + "Columna 1: CustomerId " + Environment.NewLine
                + "Columna 1: Exportacion " + Environment.NewLine
                + "Si no tiene esta configuración no será cargada la información. "
                , Application.ProductName + "-" + Application.ProductVersion.ToString()
                , MessageBoxButtons.OK
                );
                FolderBrowserDialog folderBrowserDialog1 = new FolderBrowserDialog();

                var fileName = " Plantilla de Clientes Exportación " + DateTime.Now.ToString("yyyy-MM-dd-hh-mm-ss") + ".xlsx";
                if (folderBrowserDialog1.ShowDialog() == DialogResult.OK)
                {
                    fileName = folderBrowserDialog1.SelectedPath + "\\" + fileName;
                    System.IO.Stream myStream = new System.IO.FileStream(fileName, System.IO.FileMode.Create, System.IO.FileAccess.ReadWrite, System.IO.FileShare.None);

                    using (ExcelPackage pck = new ExcelPackage())
                    {

                        ExcelWorksheet ws1 = pck.Workbook.Worksheets.Add("Plantilla");
                        ws1.Cells[1, 1].Value = "CustomerId";
                        ws1.Cells[1, 2].Value = "Exportacion";
                        pck.SaveAs(myStream);
                    }
                    myStream.Close();
                    MessageBox.Show("Creación de plantilla realizada " + Environment.NewLine + fileName + " finalizada.", Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
            catch (Exception e)
            {
                ErrorFX.mostrar(e, true, false,
                "frmCCeClientes - 264 - "
                    );
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            Importar();
        }

        private void Importar()
        {
            Application.DoEvents();
            OpenFileDialog openFileDialog1 = new OpenFileDialog();
            openFileDialog1.Filter = "Excel files (*.xlsx)|*.xlsx|All files (*.*)|*.*";
            openFileDialog1.FilterIndex = 1;
            openFileDialog1.RestoreDirectory = true;

            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                try
                {
                    procesar(dtg, 1, openFileDialog1.FileName);
                }
                catch (Exception)
                {
                    MessageBox.Show("No existe el Archivo seleccionado", Application.ProductName + "-" + Application.ProductVersion, MessageBoxButtons.OK);
                }
                finally
                {
                    Application.DoEvents();
                }
            }
        }

        private void procesar(DataGridView dtg, int hoja, string archivo)
        {
            string tmpFile = Path.GetTempFileName();
            try
            {
                if (!File.Exists(archivo))
                {
                    MessageBox.Show(this, "El archivo " + archivo + " no existe.", Application.ProductName
                        , MessageBoxButtons.OK, MessageBoxIcon.Error);
                }

                File.Delete(tmpFile);
                File.Copy(archivo, tmpFile);

                dtg.Rows.Clear();
                int rowIndex = 2;
                FileInfo existingFile = new FileInfo(tmpFile);
                using (ExcelPackage package = new ExcelPackage(existingFile))
                {
                    ExcelWorksheet worksheet = package.Workbook.Worksheets[hoja];
                    while (worksheet.Cells[rowIndex, 2].Value != null)
                    {
                        try
                        {
                            string Columna1Tr = worksheet.Cells[rowIndex, 1].Text;
                            string Columna2Tr = worksheet.Cells[rowIndex, 2].Text;

                            guardar(Columna1Tr, Columna2Tr);
                        }
                        catch (DbEntityValidationException e)
                        {
                            foreach (var eve in e.EntityValidationErrors)
                            {
                                foreach (var ve in eve.ValidationErrors)
                                {
                                    MessageBox.Show(this, ve.PropertyName + " Error: " + ve.ErrorMessage, Application.ProductName
                                        , MessageBoxButtons.OK, MessageBoxIcon.Error);
                                }
                            }
                        }
                        rowIndex++;
                    }
                }
            }
            catch (Exception e)
            {
                ErrorFX.mostrar(e, true, false,
                "frmProductos - 150 - "
                );
            }
            finally
            {
                File.Delete(tmpFile);
                Cargar();
            }

        }

        private void guardar(string columna1, string columna2)
        {
            ModeloDocumentosFX.ModeloDocumentosFXContainer Db = new ModeloDocumentosFX.ModeloDocumentosFXContainer();
            try
            {
                ModeloDocumentosFX.cceCliente registro = new ModeloDocumentosFX.cceCliente();

                registro = (from r in Db.cceClienteSet.Where
                                        (a => a.CustomerId == columna1)
                            select r).FirstOrDefault();

                if (registro == null)
                {
                    registro = new ModeloDocumentosFX.cceCliente();
                    registro.CustomerId = columna1;
                    registro.Exportacion = columna2;
                    Db.cceClienteSet.Add(registro);
                    Db.SaveChanges();
                }
            }
            catch (DbEntityValidationException e)
            {
                foreach (var eve in e.EntityValidationErrors)
                {
                    foreach (var ve in eve.ValidationErrors)
                    {
                        MessageBox.Show(this, ve.PropertyName + " Error: " + ve.ErrorMessage, Application.ProductName
                            , MessageBoxButtons.OK, MessageBoxIcon.Error);
                        return;
                    }
                }
            }
        }

    }
}
