﻿using Generales;
using OfficeOpenXml;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.Entity.Validation;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace FE_FX.Formularios
{

    public partial class frmLog : Form
    {        
        public frmLog(string invoiceid="")
        {
            InitializeComponent();
            invoiceId.Text = invoiceid;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Cargar();
        }

        private void Cargar()
        {
            try
            {
                ModeloDocumentosFX.ModeloDocumentosFXContainer dbContext = new ModeloDocumentosFX.ModeloDocumentosFXContainer();
                var result = from b in dbContext.LogFXSet
                                             .Where(a => a.INVOICE_ID.Contains(invoiceId.Text)
                                             )
                                             .OrderBy(a => a.INVOICE_ID)
                             select b;

                Dtg.Rows.Clear();
                foreach (ModeloDocumentosFX.LogFX registro in result)
                {
                    if (registro != null)
                    {
                        int n = Dtg.Rows.Add();
                        Dtg.Rows[n].Tag = registro;
                        Dtg.Rows[n].Cells["INVOICE_ID"].Value = registro.INVOICE_ID;
                        Dtg.Rows[n].Cells["fechaCreacion"].Value = registro.fechaCreacion;
                        Dtg.Rows[n].Cells["UUID"].Value = registro.UUID;
                        Dtg.Rows[n].Cells["Mensaje"].Value = registro.Mensaje;
                    }

                }
                dbContext.Dispose();
                this.Dtg.Columns[0].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
            }
            catch (DbEntityValidationException error)
            {
                foreach (var eve in error.EntityValidationErrors)
                {
                    foreach (var ve in eve.ValidationErrors)
                    {
                        MessageBox.Show(this, ve.PropertyName + " Error: " + ve.ErrorMessage, Application.ProductName
                            , MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
            }
        }
    }
}
