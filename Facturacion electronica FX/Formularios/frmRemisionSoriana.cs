﻿using Generales;
using OfficeOpenXml;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace FE_FX.Formularios
{

    public partial class frmRemisionSoriana : Form
    {
        private cCONEXCION oData = new cCONEXCION("");




        public frmRemisionSoriana(string sConn)
        {
            oData.sConn = sConn;
            InitializeComponent();
        }

        private void button2_Click(object sender, EventArgs e)
        {

        }

        private void button2_Click_1(object sender, EventArgs e)
        {
            buscar();
        }

        private void buscar()
        {
            if (String.IsNullOrEmpty(Tienda.Text))
            {
                MessageBox.Show("Ingrese la tienda");
                Tienda.Focus();
                return;
            }
            if (String.IsNullOrEmpty(Cita.Text))
            {
                MessageBox.Show("Ingrese la cita");
                Cita.Focus();
                return;
            }

            frmSHIPPER_LINE oFrm = new frmSHIPPER_LINE(oData.sConn);
            if (oFrm.ShowDialog() == DialogResult.OK)
            {
                agregarLineasListaEmpaque(oFrm.seleccionados);
            }
        }

        List<SorianaPedido> oSorianaPedido = new List<SorianaPedido>();
        List<SorianaRemision> oSorianaRemision = new List<SorianaRemision>();
        List<SorianaArticulo> oSorianaArticulo = new List<SorianaArticulo>();

        private void agregarLineasListaEmpaque(DataGridViewSelectedRowCollection seleccionados)
        {

            foreach (DataGridViewRow transacion in seleccionados)
            {
                SorianaArticulo articulo = new SorianaArticulo();
                articulo.Proveedor = this.Proveedor.Text;
                articulo.Remision = transacion.Cells["Packlist"].Value.ToString();
                articulo.FolioPedido = transacion.Cells["CUSTOMER_PO_REF"].Value.ToString();
                articulo.Tienda = this.Tienda.Text;
                articulo.Codigo = transacion.Cells["CUSTOMER_PART_ID"].Value.ToString();
                articulo.CantidadUnidadCompra = decimal.Parse(transacion.Cells["Cantidad"].Value.ToString());
                articulo.CostoNetoUnidadCompra = decimal.Parse(transacion.Cells["UNIT_PRICE"].Value.ToString());
                articulo.PorcentajeIEPS = 0;
                articulo.PorcentajeIVA = 0;
                oSorianaArticulo.Add(articulo);


                SorianaPedido pedido = new SorianaPedido();
                pedido.Proveedor = this.Proveedor.Text;
                pedido.Remision = transacion.Cells["Packlist"].Value.ToString();
                pedido.FolioPedido = transacion.Cells["CUSTOMER_PO_REF"].Value.ToString();
                pedido.Tienda = this.Tienda.Text;
                pedido.CantidadArticulos = decimal.Parse(transacion.Cells["Cantidad"].Value.ToString());
                oSorianaPedido.Add(pedido);


                SorianaRemision remision = new SorianaRemision();
                remision.Proveedor = this.Proveedor.Text;
                remision.Remision = transacion.Cells["Packlist"].Value.ToString();
                remision.Consecutivo = "0";
                remision.FechaRemision = DateTime.Now.ToString("dd/MM/yyyy");
                remision.Tienda = this.Tienda.Text;
                remision.TipoMoneda = "1";
                remision.TipoBulto = "1";
                remision.EntregaMercancia = "1";
                remision.CumpleReqFiscales = "SI";
                remision.CantidadBultos = transacion.Cells["Cantidad"].Value.ToString();
                remision.Subtotal = "0";
                remision.Descuentos = "0";
                remision.IEPS = "0";
                remision.IVA = "0";
                remision.OtrosImpuestos = "0";
                remision.Total = "0";
                remision.CantidadPedidos = "1";
                remision.FechaEntregaMercancia = DateTime.Now.ToString("dd/MM/yyyy");
                remision.Cita = Cita.Text;
                oSorianaRemision.Add(remision);
            }

            this.dtgSorianaArticulo.DataSource = oSorianaArticulo;
            this.dtgSorianaRemision.DataSource = oSorianaRemision;
            this.dtgSorianaPedido.DataSource = oSorianaPedido;

        }

        private void button1_Click(object sender, EventArgs e)
        {
            generarPlantilla();
        }

        private void generarPlantilla()
        {
            try
            {
                string codeBase = Assembly.GetExecutingAssembly().CodeBase;
                UriBuilder uri = new UriBuilder(codeBase);
                string pathTr = Uri.UnescapeDataString(uri.Path);
                string directorioLocal = Path.GetDirectoryName(pathTr);

                var pathPlantilla = Path.Combine(directorioLocal, "PlantillaSoriana.xlsx");
                var tempfilePlantilla = new FileInfo(pathPlantilla);
                if (!tempfilePlantilla.Exists)
                {
                    MessageBox.Show("La plantilla " + pathPlantilla + " no existe");
                    return;
                }

                string fileName = System.IO.Path.GetTempPath() + Guid.NewGuid().ToString() + ".xlsx";
                var path = Path.Combine(directorioLocal, fileName);
                File.Copy(pathPlantilla, path);


                var tempfile = new FileInfo(path);
                if (!tempfile.Exists)
                {
                    MessageBox.Show("El archivo " + path + " no existe");
                    return;
                }

                //Save the file
                using (var pck = new ExcelPackage(tempfile))
                {
                    var ws = pck.Workbook.Worksheets["Remision"];
                    ws.Cells["A2"].LoadFromCollection(oSorianaRemision);
                    
                    var ws1 = pck.Workbook.Worksheets["Pedidos"];
                    ws1.Cells["A2"].LoadFromCollection(oSorianaPedido);

                    var ws2 = pck.Workbook.Worksheets["Articulos"];
                    ws2.Cells["A2"].LoadFromCollection(oSorianaArticulo);

                    pck.Save();
                }




                //open the file
                Process.Start(tempfile.FullName);

            }
            catch (Exception error)
            {
                string mensaje = "";
                mensaje = error.Message.ToString();
                if (error.InnerException != null)
                {
                    mensaje += " " + error.InnerException.Message.ToString();
                }

                MessageBox.Show(mensaje);
            }
        }

    }



    public class SorianaRemision
    {
        public string Proveedor { get; set; }
        public string Remision { get; set; }
        public string Consecutivo { get; set; }
        public string FechaRemision { get; set; }
        public string Tienda { get; set; }
        public string TipoMoneda { get; set; }
        public string TipoBulto { get; set; }
        public string EntregaMercancia { get; set; }
        public string CumpleReqFiscales { get; set; }
        public string CantidadBultos { get; set; }
        public string Subtotal { get; set; }
        public string Descuentos { get; set; }
        public string IEPS { get; set; }
        public string IVA { get; set; }
        public string OtrosImpuestos { get; set; }
        public string Total { get; set; }
        public string CantidadPedidos { get; set; }
        public string FechaEntregaMercancia { get; set; }
        public string Cita { get; set; }
    }


    public class SorianaPedido
    {
        public string Proveedor { get; set; }
        public string Remision { get; set; }
        public string FolioPedido { get; set; }
        public string Tienda { get; set; }
        public decimal CantidadArticulos { get; set; }
    }
    public class SorianaArticulo
    {
        public string Proveedor { get; set; }
        public string Remision { get; set; }
        public string FolioPedido { get; set; }
        public string Tienda { get; set; }
        public string Codigo { get; set; }
        public decimal CantidadUnidadCompra { get; set; }
        public decimal CostoNetoUnidadCompra { get; set; }
        public decimal PorcentajeIEPS { get; set; }
        public decimal PorcentajeIVA { get; set; }
    }

}
