﻿using FE_FX.Clases;
using Generales;
using OfficeOpenXml;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace FE_FX.Formularios
{

    public partial class frmMigrararCFDI : Form
    {
        private cCONEXCION oData = new cCONEXCION("");
        Encapsular OEncapsular;
        public frmMigrararCFDI(Encapsular oEncapsularp,string sConn)
        { 
            oData = new cCONEXCION(sConn);
            OEncapsular = new Encapsular();
            OEncapsular = oEncapsularp;
            InitializeComponent();
            cargar_empresas();
        }
        private void cargar_empresas()
        {
            ENTITY_ID.Items.Clear();
            cEMPRESA ocEMPRESA = new cEMPRESA(oData);
            foreach (cEMPRESA registro in ocEMPRESA.todos("", true, true))
            {
                ENTITY_ID.Items.Add(registro);
            }
            if (ENTITY_ID.Items.Count > 0)
            {
                ENTITY_ID.SelectedIndex = 0;
            }
        }
        private void ENTITY_ID_SelectedIndexChanged(object sender, EventArgs e)
        {
            actualizar_conexcion();
        }

        private void actualizar_conexcion()
        {
            try
            {
                cEMPRESA ocEMPRESA = ENTITY_ID.SelectedItem as cEMPRESA;
                if (ocEMPRESA != null)
                {
                    cargar_formatos();
                }
            }
            catch (Exception e)
            {
                ErrorFX.mostrar(e, true, true, false);
            }
        }
        private void button2_Click(object sender, EventArgs e)
        {
            SeleccionarVarios();
        }

        private void SeleccionarVarios()
        {
            OpenFileDialog openFileDialog1 = new OpenFileDialog();
            openFileDialog1.Filter = "CFDI (XML)|*.XML";
            openFileDialog1.Multiselect = true;
            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                Dtg.Rows.Clear();
                foreach (String file in openFileDialog1.FileNames)
                {
                    try
                    {
                        int N = Dtg.Rows.Add();
                        Dtg.Rows[N].Cells["Archivo"].Value = file;
                        Dtg.Rows[N].Cells["Estado"].Value = "";
                    }
                    catch (Exception e)
                    {
                        ErrorFX.mostrar(e, true, true, false);
                    }
                }
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Migrar();
        }
        private void cargar_formatos()
        {
            
        cEMPRESA ocEMPRESA = new cEMPRESA(oData);
            ocEMPRESA = ENTITY_ID.SelectedItem as cEMPRESA;
            if (ocEMPRESA != null)
            {
                FORMATO.Items.Clear();
                FORMATO.Text = "";
                cFORMATO ocFORMATO = new cFORMATO();
                foreach (cFORMATO registro in ocFORMATO.todos_empresa("", ocEMPRESA.ROW_ID).Where(a => a.ACTIVO.Equals("True")))
                {
                    FORMATO.Items.Add(registro.ID);
                }
            }
            else
            {
                //Cargar todos los formatos
                FORMATO.Items.Clear();
                FORMATO.Text = "";
                cFORMATO ocFORMATO = new cFORMATO();
                foreach (cFORMATO registro in ocFORMATO.todos("").Where(a => a.ACTIVO.Equals("True")))
                {
                    FORMATO.Items.Add(registro.ID);
                }
            }

            if (FORMATO.Items.Count > 0)
            {
                FORMATO.SelectedIndex = 0;
            }
        }
        private void Migrar()
        {
            try
            {
                cFACTURA_BANDEJA ocFACTURA_BANDEJA = new cFACTURA_BANDEJA(oData);
                foreach (DataGridViewRow Row in Dtg.Rows)
                {
                    ocFACTURA_BANDEJA.Migrar(Row.Cells["Archivo"].Value.ToString(), OEncapsular, FORMATO.Text);
                    if (ocFACTURA_BANDEJA.Errores.Count != 0)
                    {
                        Row.Cells["Estado"].Value = ocFACTURA_BANDEJA.Errores.Aggregate((a, b) => a + ", " + b);
                    }
                    else
                    {
                        Row.Cells["Estado"].Value = "Migrada";
                    }
                }
                
            }
            catch (Exception e)
            {
                ErrorFX.mostrar(e, true, true, false);
            }
        }

        private void Serie_TextChanged(object sender, EventArgs e)
        {

        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void FORMATO_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void label7_Click(object sender, EventArgs e)
        {

        }

        private void Dtg_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }
    }
}
