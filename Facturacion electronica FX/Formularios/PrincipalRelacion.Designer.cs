﻿
namespace FE_FX.Formularios
{
    partial class PrincipalRelacion
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(PrincipalRelacion));
            this.button2 = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.FORMATO = new System.Windows.Forms.ComboBox();
            this.INICIO = new System.Windows.Forms.DateTimePicker();
            this.label7 = new System.Windows.Forms.Label();
            this.CUSTOMER_NAME = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.FINAL = new System.Windows.Forms.DateTimePicker();
            this.PDF_VER = new System.Windows.Forms.CheckBox();
            this.label9 = new System.Windows.Forms.Label();
            this.INVOICE_ID = new System.Windows.Forms.TextBox();
            this.TOPtxt = new System.Windows.Forms.TextBox();
            this.CFDI_PRUEBA = new System.Windows.Forms.CheckBox();
            this.OcultarPruebas = new System.Windows.Forms.CheckBox();
            this.PENDIENTES = new System.Windows.Forms.CheckBox();
            this.progressBar1 = new System.Windows.Forms.ProgressBar();
            this.Dtg = new System.Windows.Forms.DataGridView();
            this.folderBrowserDialog1 = new System.Windows.Forms.FolderBrowserDialog();
            this.contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.procesarToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.timbrarToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.relacionarDocumentosToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.timbrarConComplementoDeComercioExteriorToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem4 = new System.Windows.Forms.ToolStripMenuItem();
            this.generarToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.verComprobantesDePagoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.verToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.pDFToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.xMLToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.cambiarMetodoFormaYUsoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.regenerarPDFToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.cancelarToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.exportarToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.reporteDeRelaciónDeFacturasToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.exportarToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.cancelaciónMasivaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.relacionarToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.relacionarFacturasYFoliosFiscalesToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.porLíneasToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.asociaciónMasivaDeCFDIToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.asociaciónManualmenteToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.asociaciónMasivaDeCFDIGeneralToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.descargarPlantillaAsociaciónMasivaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.catalogosDeConfiguraciónToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.unidadDeMedidaDelSATToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.clasificaciónDeProductosDelSATToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.reactivarFacturaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.cambiarFechaDeFactuarEnVisualToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.modificacionesAVisualToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.modificarDireccionDeEnvioToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.cambiarFechaDeFacturaEnVisualToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.utilidadesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.migrarCFDIToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.relgasDeTimbradoAutomaticoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.Estado = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.EstadoFinal = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Factura = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.FechaFactura = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.RMA = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.TipoRMA = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.FechaRMA = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Pedido = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.FechaPedido = new System.Windows.Forms.DataGridViewTextBoxColumn();
            ((System.ComponentModel.ISupportInitialize)(this.Dtg)).BeginInit();
            this.contextMenuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // button2
            // 
            this.button2.BackColor = System.Drawing.Color.Tomato;
            this.button2.FlatAppearance.BorderSize = 0;
            this.button2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button2.Location = new System.Drawing.Point(12, 38);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(73, 22);
            this.button2.TabIndex = 505;
            this.button2.Text = "Procesar";
            this.button2.UseVisualStyleBackColor = false;
            // 
            // button1
            // 
            this.button1.BackColor = System.Drawing.Color.Goldenrod;
            this.button1.FlatAppearance.BorderSize = 0;
            this.button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button1.Location = new System.Drawing.Point(12, 12);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(73, 22);
            this.button1.TabIndex = 504;
            this.button1.Text = "Buscar";
            this.button1.UseVisualStyleBackColor = false;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.White;
            this.label3.Location = new System.Drawing.Point(214, 16);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(32, 13);
            this.label3.TabIndex = 486;
            this.label3.Text = "Inicio";
            // 
            // FORMATO
            // 
            this.FORMATO.FormattingEnabled = true;
            this.FORMATO.Location = new System.Drawing.Point(691, 12);
            this.FORMATO.Name = "FORMATO";
            this.FORMATO.Size = new System.Drawing.Size(138, 21);
            this.FORMATO.TabIndex = 501;
            // 
            // INICIO
            // 
            this.INICIO.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.INICIO.Location = new System.Drawing.Point(251, 12);
            this.INICIO.Name = "INICIO";
            this.INICIO.Size = new System.Drawing.Size(94, 20);
            this.INICIO.TabIndex = 488;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.BackColor = System.Drawing.Color.White;
            this.label7.Location = new System.Drawing.Point(640, 16);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(45, 13);
            this.label7.TabIndex = 503;
            this.label7.Text = "Formato";
            // 
            // CUSTOMER_NAME
            // 
            this.CUSTOMER_NAME.Location = new System.Drawing.Point(251, 38);
            this.CUSTOMER_NAME.Name = "CUSTOMER_NAME";
            this.CUSTOMER_NAME.Size = new System.Drawing.Size(94, 20);
            this.CUSTOMER_NAME.TabIndex = 497;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.BackColor = System.Drawing.Color.White;
            this.label5.Location = new System.Drawing.Point(209, 42);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(39, 13);
            this.label5.TabIndex = 495;
            this.label5.Text = "Cliente";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.BackColor = System.Drawing.Color.White;
            this.label4.Location = new System.Drawing.Point(350, 16);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(29, 13);
            this.label4.TabIndex = 487;
            this.label4.Text = "Final";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.BackColor = System.Drawing.Color.White;
            this.label6.Location = new System.Drawing.Point(351, 42);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(130, 13);
            this.label6.TabIndex = 491;
            this.label6.Text = "Facturas (separadas por ,)";
            // 
            // FINAL
            // 
            this.FINAL.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.FINAL.Location = new System.Drawing.Point(384, 12);
            this.FINAL.Name = "FINAL";
            this.FINAL.Size = new System.Drawing.Size(94, 20);
            this.FINAL.TabIndex = 489;
            // 
            // PDF_VER
            // 
            this.PDF_VER.AutoSize = true;
            this.PDF_VER.BackColor = System.Drawing.Color.White;
            this.PDF_VER.Location = new System.Drawing.Point(92, 16);
            this.PDF_VER.Name = "PDF_VER";
            this.PDF_VER.Size = new System.Drawing.Size(94, 17);
            this.PDF_VER.TabIndex = 490;
            this.PDF_VER.Text = "Visualizar PDF";
            this.PDF_VER.UseVisualStyleBackColor = false;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.BackColor = System.Drawing.Color.White;
            this.label9.Location = new System.Drawing.Point(481, 16);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(14, 13);
            this.label9.TabIndex = 498;
            this.label9.Text = "#";
            // 
            // INVOICE_ID
            // 
            this.INVOICE_ID.Location = new System.Drawing.Point(484, 38);
            this.INVOICE_ID.Name = "INVOICE_ID";
            this.INVOICE_ID.Size = new System.Drawing.Size(228, 20);
            this.INVOICE_ID.TabIndex = 492;
            // 
            // TOPtxt
            // 
            this.TOPtxt.Location = new System.Drawing.Point(500, 12);
            this.TOPtxt.Name = "TOPtxt";
            this.TOPtxt.Size = new System.Drawing.Size(45, 20);
            this.TOPtxt.TabIndex = 493;
            this.TOPtxt.Text = "100";
            // 
            // CFDI_PRUEBA
            // 
            this.CFDI_PRUEBA.AutoSize = true;
            this.CFDI_PRUEBA.BackColor = System.Drawing.Color.White;
            this.CFDI_PRUEBA.Location = new System.Drawing.Point(92, 41);
            this.CFDI_PRUEBA.Name = "CFDI_PRUEBA";
            this.CFDI_PRUEBA.Size = new System.Drawing.Size(100, 17);
            this.CFDI_PRUEBA.TabIndex = 494;
            this.CFDI_PRUEBA.Text = "Timbrar en Test";
            this.CFDI_PRUEBA.UseVisualStyleBackColor = false;
            // 
            // OcultarPruebas
            // 
            this.OcultarPruebas.AutoSize = true;
            this.OcultarPruebas.BackColor = System.Drawing.Color.White;
            this.OcultarPruebas.Location = new System.Drawing.Point(727, 40);
            this.OcultarPruebas.Name = "OcultarPruebas";
            this.OcultarPruebas.Size = new System.Drawing.Size(102, 17);
            this.OcultarPruebas.TabIndex = 502;
            this.OcultarPruebas.Text = "Ocultar Pruebas";
            this.OcultarPruebas.UseVisualStyleBackColor = false;
            // 
            // PENDIENTES
            // 
            this.PENDIENTES.AutoSize = true;
            this.PENDIENTES.BackColor = System.Drawing.Color.White;
            this.PENDIENTES.Checked = true;
            this.PENDIENTES.CheckState = System.Windows.Forms.CheckState.Checked;
            this.PENDIENTES.Location = new System.Drawing.Point(554, 14);
            this.PENDIENTES.Name = "PENDIENTES";
            this.PENDIENTES.Size = new System.Drawing.Size(79, 17);
            this.PENDIENTES.TabIndex = 496;
            this.PENDIENTES.Text = "Pendientes";
            this.PENDIENTES.UseVisualStyleBackColor = false;
            // 
            // progressBar1
            // 
            this.progressBar1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.progressBar1.Location = new System.Drawing.Point(12, 87);
            this.progressBar1.Name = "progressBar1";
            this.progressBar1.Size = new System.Drawing.Size(1097, 18);
            this.progressBar1.Style = System.Windows.Forms.ProgressBarStyle.Continuous;
            this.progressBar1.TabIndex = 499;
            // 
            // Dtg
            // 
            this.Dtg.AllowUserToAddRows = false;
            this.Dtg.AllowUserToDeleteRows = false;
            this.Dtg.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.Dtg.BackgroundColor = System.Drawing.Color.White;
            this.Dtg.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.Dtg.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.Dtg.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.Dtg.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Estado,
            this.EstadoFinal,
            this.Factura,
            this.FechaFactura,
            this.RMA,
            this.TipoRMA,
            this.FechaRMA,
            this.Pedido,
            this.FechaPedido});
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.Dtg.DefaultCellStyle = dataGridViewCellStyle2;
            this.Dtg.Location = new System.Drawing.Point(12, 66);
            this.Dtg.Name = "Dtg";
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.Dtg.RowHeadersDefaultCellStyle = dataGridViewCellStyle3;
            this.Dtg.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.Dtg.Size = new System.Drawing.Size(1097, 559);
            this.Dtg.TabIndex = 485;
            // 
            // contextMenuStrip1
            // 
            this.contextMenuStrip1.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.contextMenuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.procesarToolStripMenuItem,
            this.toolStripMenuItem4,
            this.verToolStripMenuItem,
            this.cambiarMetodoFormaYUsoToolStripMenuItem,
            this.regenerarPDFToolStripMenuItem,
            this.cancelarToolStripMenuItem,
            this.exportarToolStripMenuItem,
            this.cancelaciónMasivaToolStripMenuItem,
            this.relacionarToolStripMenuItem,
            this.catalogosDeConfiguraciónToolStripMenuItem,
            this.reactivarFacturaToolStripMenuItem,
            this.cambiarFechaDeFactuarEnVisualToolStripMenuItem,
            this.modificacionesAVisualToolStripMenuItem,
            this.utilidadesToolStripMenuItem});
            this.contextMenuStrip1.Name = "contextMenuStrip1";
            this.contextMenuStrip1.Size = new System.Drawing.Size(260, 312);
            // 
            // procesarToolStripMenuItem
            // 
            this.procesarToolStripMenuItem.BackColor = System.Drawing.Color.LightSalmon;
            this.procesarToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.timbrarToolStripMenuItem,
            this.relacionarDocumentosToolStripMenuItem1,
            this.timbrarConComplementoDeComercioExteriorToolStripMenuItem});
            this.procesarToolStripMenuItem.Name = "procesarToolStripMenuItem";
            this.procesarToolStripMenuItem.Size = new System.Drawing.Size(259, 22);
            this.procesarToolStripMenuItem.Text = "CFDI 4.0";
            // 
            // timbrarToolStripMenuItem
            // 
            this.timbrarToolStripMenuItem.Name = "timbrarToolStripMenuItem";
            this.timbrarToolStripMenuItem.Size = new System.Drawing.Size(332, 22);
            this.timbrarToolStripMenuItem.Text = "Timbrar";
            // 
            // relacionarDocumentosToolStripMenuItem1
            // 
            this.relacionarDocumentosToolStripMenuItem1.Name = "relacionarDocumentosToolStripMenuItem1";
            this.relacionarDocumentosToolStripMenuItem1.Size = new System.Drawing.Size(332, 22);
            this.relacionarDocumentosToolStripMenuItem1.Text = "Notas de Credito(Relacionar documentos)";
            // 
            // timbrarConComplementoDeComercioExteriorToolStripMenuItem
            // 
            this.timbrarConComplementoDeComercioExteriorToolStripMenuItem.Name = "timbrarConComplementoDeComercioExteriorToolStripMenuItem";
            this.timbrarConComplementoDeComercioExteriorToolStripMenuItem.Size = new System.Drawing.Size(332, 22);
            this.timbrarConComplementoDeComercioExteriorToolStripMenuItem.Text = "Timbrar con Complemento de Comercio Exterior";
            // 
            // toolStripMenuItem4
            // 
            this.toolStripMenuItem4.BackColor = System.Drawing.Color.LightGreen;
            this.toolStripMenuItem4.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.generarToolStripMenuItem,
            this.verComprobantesDePagoToolStripMenuItem});
            this.toolStripMenuItem4.Name = "toolStripMenuItem4";
            this.toolStripMenuItem4.Size = new System.Drawing.Size(259, 22);
            this.toolStripMenuItem4.Text = "Realizar complemento de pago";
            // 
            // generarToolStripMenuItem
            // 
            this.generarToolStripMenuItem.Name = "generarToolStripMenuItem";
            this.generarToolStripMenuItem.Size = new System.Drawing.Size(202, 22);
            this.generarToolStripMenuItem.Text = "Generar";
            // 
            // verComprobantesDePagoToolStripMenuItem
            // 
            this.verComprobantesDePagoToolStripMenuItem.Name = "verComprobantesDePagoToolStripMenuItem";
            this.verComprobantesDePagoToolStripMenuItem.Size = new System.Drawing.Size(202, 22);
            this.verComprobantesDePagoToolStripMenuItem.Text = "Ver recepciones de pago";
            // 
            // verToolStripMenuItem
            // 
            this.verToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.pDFToolStripMenuItem,
            this.xMLToolStripMenuItem});
            this.verToolStripMenuItem.Name = "verToolStripMenuItem";
            this.verToolStripMenuItem.Size = new System.Drawing.Size(259, 22);
            this.verToolStripMenuItem.Text = "Ver";
            // 
            // pDFToolStripMenuItem
            // 
            this.pDFToolStripMenuItem.Name = "pDFToolStripMenuItem";
            this.pDFToolStripMenuItem.Size = new System.Drawing.Size(98, 22);
            this.pDFToolStripMenuItem.Text = "PDF";
            // 
            // xMLToolStripMenuItem
            // 
            this.xMLToolStripMenuItem.Name = "xMLToolStripMenuItem";
            this.xMLToolStripMenuItem.Size = new System.Drawing.Size(98, 22);
            this.xMLToolStripMenuItem.Text = "XML";
            // 
            // cambiarMetodoFormaYUsoToolStripMenuItem
            // 
            this.cambiarMetodoFormaYUsoToolStripMenuItem.Name = "cambiarMetodoFormaYUsoToolStripMenuItem";
            this.cambiarMetodoFormaYUsoToolStripMenuItem.Size = new System.Drawing.Size(259, 22);
            this.cambiarMetodoFormaYUsoToolStripMenuItem.Text = "Editar";
            // 
            // regenerarPDFToolStripMenuItem
            // 
            this.regenerarPDFToolStripMenuItem.Name = "regenerarPDFToolStripMenuItem";
            this.regenerarPDFToolStripMenuItem.Size = new System.Drawing.Size(259, 22);
            this.regenerarPDFToolStripMenuItem.Text = "Regenerar PDF";
            // 
            // cancelarToolStripMenuItem
            // 
            this.cancelarToolStripMenuItem.BackColor = System.Drawing.Color.OrangeRed;
            this.cancelarToolStripMenuItem.Name = "cancelarToolStripMenuItem";
            this.cancelarToolStripMenuItem.Size = new System.Drawing.Size(259, 22);
            this.cancelarToolStripMenuItem.Text = "Cancelar";
            // 
            // exportarToolStripMenuItem
            // 
            this.exportarToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.reporteDeRelaciónDeFacturasToolStripMenuItem,
            this.exportarToolStripMenuItem1});
            this.exportarToolStripMenuItem.Name = "exportarToolStripMenuItem";
            this.exportarToolStripMenuItem.Size = new System.Drawing.Size(259, 22);
            this.exportarToolStripMenuItem.Text = "Exportar";
            // 
            // reporteDeRelaciónDeFacturasToolStripMenuItem
            // 
            this.reporteDeRelaciónDeFacturasToolStripMenuItem.Name = "reporteDeRelaciónDeFacturasToolStripMenuItem";
            this.reporteDeRelaciónDeFacturasToolStripMenuItem.Size = new System.Drawing.Size(237, 22);
            this.reporteDeRelaciónDeFacturasToolStripMenuItem.Text = "Reporte de relación de facturas";
            // 
            // exportarToolStripMenuItem1
            // 
            this.exportarToolStripMenuItem1.Name = "exportarToolStripMenuItem1";
            this.exportarToolStripMenuItem1.Size = new System.Drawing.Size(237, 22);
            this.exportarToolStripMenuItem1.Text = "Exportar";
            // 
            // cancelaciónMasivaToolStripMenuItem
            // 
            this.cancelaciónMasivaToolStripMenuItem.Name = "cancelaciónMasivaToolStripMenuItem";
            this.cancelaciónMasivaToolStripMenuItem.Size = new System.Drawing.Size(259, 22);
            this.cancelaciónMasivaToolStripMenuItem.Text = "Cancelación masiva";
            // 
            // relacionarToolStripMenuItem
            // 
            this.relacionarToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.relacionarFacturasYFoliosFiscalesToolStripMenuItem1,
            this.porLíneasToolStripMenuItem,
            this.asociaciónMasivaDeCFDIToolStripMenuItem1,
            this.asociaciónManualmenteToolStripMenuItem,
            this.asociaciónMasivaDeCFDIGeneralToolStripMenuItem,
            this.descargarPlantillaAsociaciónMasivaToolStripMenuItem});
            this.relacionarToolStripMenuItem.Name = "relacionarToolStripMenuItem";
            this.relacionarToolStripMenuItem.Size = new System.Drawing.Size(259, 22);
            this.relacionarToolStripMenuItem.Text = "Relacionar";
            // 
            // relacionarFacturasYFoliosFiscalesToolStripMenuItem1
            // 
            this.relacionarFacturasYFoliosFiscalesToolStripMenuItem1.Name = "relacionarFacturasYFoliosFiscalesToolStripMenuItem1";
            this.relacionarFacturasYFoliosFiscalesToolStripMenuItem1.Size = new System.Drawing.Size(270, 22);
            this.relacionarFacturasYFoliosFiscalesToolStripMenuItem1.Text = "Relacionar facturas y folios fiscales";
            // 
            // porLíneasToolStripMenuItem
            // 
            this.porLíneasToolStripMenuItem.Name = "porLíneasToolStripMenuItem";
            this.porLíneasToolStripMenuItem.Size = new System.Drawing.Size(270, 22);
            this.porLíneasToolStripMenuItem.Text = "Por líneas";
            // 
            // asociaciónMasivaDeCFDIToolStripMenuItem1
            // 
            this.asociaciónMasivaDeCFDIToolStripMenuItem1.Name = "asociaciónMasivaDeCFDIToolStripMenuItem1";
            this.asociaciónMasivaDeCFDIToolStripMenuItem1.Size = new System.Drawing.Size(270, 22);
            this.asociaciónMasivaDeCFDIToolStripMenuItem1.Text = "Asociación masiva de CFDI por línea";
            // 
            // asociaciónManualmenteToolStripMenuItem
            // 
            this.asociaciónManualmenteToolStripMenuItem.Name = "asociaciónManualmenteToolStripMenuItem";
            this.asociaciónManualmenteToolStripMenuItem.Size = new System.Drawing.Size(270, 22);
            this.asociaciónManualmenteToolStripMenuItem.Text = "Asociación manualmente";
            // 
            // asociaciónMasivaDeCFDIGeneralToolStripMenuItem
            // 
            this.asociaciónMasivaDeCFDIGeneralToolStripMenuItem.Name = "asociaciónMasivaDeCFDIGeneralToolStripMenuItem";
            this.asociaciónMasivaDeCFDIGeneralToolStripMenuItem.Size = new System.Drawing.Size(270, 22);
            this.asociaciónMasivaDeCFDIGeneralToolStripMenuItem.Text = "Asociación masiva de CFDI general";
            this.asociaciónMasivaDeCFDIGeneralToolStripMenuItem.Visible = false;
            // 
            // descargarPlantillaAsociaciónMasivaToolStripMenuItem
            // 
            this.descargarPlantillaAsociaciónMasivaToolStripMenuItem.Name = "descargarPlantillaAsociaciónMasivaToolStripMenuItem";
            this.descargarPlantillaAsociaciónMasivaToolStripMenuItem.Size = new System.Drawing.Size(270, 22);
            this.descargarPlantillaAsociaciónMasivaToolStripMenuItem.Text = "Descargar plantilla asociación masiva";
            // 
            // catalogosDeConfiguraciónToolStripMenuItem
            // 
            this.catalogosDeConfiguraciónToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.unidadDeMedidaDelSATToolStripMenuItem,
            this.clasificaciónDeProductosDelSATToolStripMenuItem,
            this.relgasDeTimbradoAutomaticoToolStripMenuItem});
            this.catalogosDeConfiguraciónToolStripMenuItem.Name = "catalogosDeConfiguraciónToolStripMenuItem";
            this.catalogosDeConfiguraciónToolStripMenuItem.Size = new System.Drawing.Size(259, 22);
            this.catalogosDeConfiguraciónToolStripMenuItem.Text = "Catalogos de configuración";
            // 
            // unidadDeMedidaDelSATToolStripMenuItem
            // 
            this.unidadDeMedidaDelSATToolStripMenuItem.Name = "unidadDeMedidaDelSATToolStripMenuItem";
            this.unidadDeMedidaDelSATToolStripMenuItem.Size = new System.Drawing.Size(256, 22);
            this.unidadDeMedidaDelSATToolStripMenuItem.Text = "Unidad de medida del SAT";
            // 
            // clasificaciónDeProductosDelSATToolStripMenuItem
            // 
            this.clasificaciónDeProductosDelSATToolStripMenuItem.Name = "clasificaciónDeProductosDelSATToolStripMenuItem";
            this.clasificaciónDeProductosDelSATToolStripMenuItem.Size = new System.Drawing.Size(256, 22);
            this.clasificaciónDeProductosDelSATToolStripMenuItem.Text = "Clasificación de productos del SAT";
            // 
            // reactivarFacturaToolStripMenuItem
            // 
            this.reactivarFacturaToolStripMenuItem.Name = "reactivarFacturaToolStripMenuItem";
            this.reactivarFacturaToolStripMenuItem.Size = new System.Drawing.Size(259, 22);
            this.reactivarFacturaToolStripMenuItem.Text = "Reactivar factura";
            // 
            // cambiarFechaDeFactuarEnVisualToolStripMenuItem
            // 
            this.cambiarFechaDeFactuarEnVisualToolStripMenuItem.Name = "cambiarFechaDeFactuarEnVisualToolStripMenuItem";
            this.cambiarFechaDeFactuarEnVisualToolStripMenuItem.Size = new System.Drawing.Size(259, 22);
            this.cambiarFechaDeFactuarEnVisualToolStripMenuItem.Text = "Cambiar fecha de Factura en Visual";
            // 
            // modificacionesAVisualToolStripMenuItem
            // 
            this.modificacionesAVisualToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.modificarDireccionDeEnvioToolStripMenuItem,
            this.cambiarFechaDeFacturaEnVisualToolStripMenuItem});
            this.modificacionesAVisualToolStripMenuItem.Name = "modificacionesAVisualToolStripMenuItem";
            this.modificacionesAVisualToolStripMenuItem.Size = new System.Drawing.Size(259, 22);
            this.modificacionesAVisualToolStripMenuItem.Text = "Modificaciones a Visual";
            // 
            // modificarDireccionDeEnvioToolStripMenuItem
            // 
            this.modificarDireccionDeEnvioToolStripMenuItem.Name = "modificarDireccionDeEnvioToolStripMenuItem";
            this.modificarDireccionDeEnvioToolStripMenuItem.Size = new System.Drawing.Size(259, 22);
            this.modificarDireccionDeEnvioToolStripMenuItem.Text = "Modificar direccion de envio";
            // 
            // cambiarFechaDeFacturaEnVisualToolStripMenuItem
            // 
            this.cambiarFechaDeFacturaEnVisualToolStripMenuItem.Name = "cambiarFechaDeFacturaEnVisualToolStripMenuItem";
            this.cambiarFechaDeFacturaEnVisualToolStripMenuItem.Size = new System.Drawing.Size(259, 22);
            this.cambiarFechaDeFacturaEnVisualToolStripMenuItem.Text = "Cambiar fecha de Factura en Visual";
            // 
            // utilidadesToolStripMenuItem
            // 
            this.utilidadesToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.migrarCFDIToolStripMenuItem});
            this.utilidadesToolStripMenuItem.Name = "utilidadesToolStripMenuItem";
            this.utilidadesToolStripMenuItem.Size = new System.Drawing.Size(259, 22);
            this.utilidadesToolStripMenuItem.Text = "Utilidades";
            // 
            // migrarCFDIToolStripMenuItem
            // 
            this.migrarCFDIToolStripMenuItem.Name = "migrarCFDIToolStripMenuItem";
            this.migrarCFDIToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.migrarCFDIToolStripMenuItem.Text = "Migrar CFDI";
            // 
            // relgasDeTimbradoAutomaticoToolStripMenuItem
            // 
            this.relgasDeTimbradoAutomaticoToolStripMenuItem.Name = "relgasDeTimbradoAutomaticoToolStripMenuItem";
            this.relgasDeTimbradoAutomaticoToolStripMenuItem.Size = new System.Drawing.Size(256, 22);
            this.relgasDeTimbradoAutomaticoToolStripMenuItem.Text = "Relgas de timbrado automatico";
            // 
            // Estado
            // 
            this.Estado.HeaderText = "Estado";
            this.Estado.Name = "Estado";
            // 
            // EstadoFinal
            // 
            this.EstadoFinal.HeaderText = "EstadoFinal";
            this.EstadoFinal.Name = "EstadoFinal";
            this.EstadoFinal.ReadOnly = true;
            // 
            // Factura
            // 
            this.Factura.HeaderText = "Factura";
            this.Factura.Name = "Factura";
            this.Factura.ReadOnly = true;
            // 
            // FechaFactura
            // 
            this.FechaFactura.HeaderText = "FechaFactura";
            this.FechaFactura.Name = "FechaFactura";
            this.FechaFactura.ReadOnly = true;
            // 
            // RMA
            // 
            this.RMA.HeaderText = "RMA";
            this.RMA.Name = "RMA";
            this.RMA.ReadOnly = true;
            // 
            // TipoRMA
            // 
            this.TipoRMA.HeaderText = "TipoRMA";
            this.TipoRMA.Name = "TipoRMA";
            this.TipoRMA.ReadOnly = true;
            this.TipoRMA.ToolTipText = "Muestra si es Cmpleta o Particial la aplicacion del RMA en la factura";
            // 
            // FechaRMA
            // 
            this.FechaRMA.HeaderText = "FechaRMA";
            this.FechaRMA.Name = "FechaRMA";
            this.FechaRMA.ReadOnly = true;
            // 
            // Pedido
            // 
            this.Pedido.HeaderText = "Pedido";
            this.Pedido.Name = "Pedido";
            this.Pedido.ReadOnly = true;
            // 
            // FechaPedido
            // 
            this.FechaPedido.HeaderText = "FechaPedido";
            this.FechaPedido.Name = "FechaPedido";
            this.FechaPedido.ReadOnly = true;
            // 
            // PrincipalRelacion
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(1121, 637);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.FORMATO);
            this.Controls.Add(this.INICIO);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.CUSTOMER_NAME);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.FINAL);
            this.Controls.Add(this.PDF_VER);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.INVOICE_ID);
            this.Controls.Add(this.TOPtxt);
            this.Controls.Add(this.CFDI_PRUEBA);
            this.Controls.Add(this.OcultarPruebas);
            this.Controls.Add(this.PENDIENTES);
            this.Controls.Add(this.progressBar1);
            this.Controls.Add(this.Dtg);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "PrincipalRelacion";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Timbrado de Facturas";
            ((System.ComponentModel.ISupportInitialize)(this.Dtg)).EndInit();
            this.contextMenuStrip1.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ComboBox FORMATO;
        private System.Windows.Forms.DateTimePicker INICIO;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox CUSTOMER_NAME;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.DateTimePicker FINAL;
        private System.Windows.Forms.CheckBox PDF_VER;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox INVOICE_ID;
        private System.Windows.Forms.TextBox TOPtxt;
        private System.Windows.Forms.CheckBox CFDI_PRUEBA;
        private System.Windows.Forms.CheckBox OcultarPruebas;
        private System.Windows.Forms.CheckBox PENDIENTES;
        private System.Windows.Forms.ProgressBar progressBar1;
        private System.Windows.Forms.DataGridView Dtg;
        private System.Windows.Forms.FolderBrowserDialog folderBrowserDialog1;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip1;
        private System.Windows.Forms.ToolStripMenuItem procesarToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem timbrarToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem relacionarDocumentosToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem timbrarConComplementoDeComercioExteriorToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem4;
        private System.Windows.Forms.ToolStripMenuItem generarToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem verComprobantesDePagoToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem verToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem pDFToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem xMLToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem cambiarMetodoFormaYUsoToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem regenerarPDFToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem cancelarToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem exportarToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem reporteDeRelaciónDeFacturasToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem exportarToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem cancelaciónMasivaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem relacionarToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem relacionarFacturasYFoliosFiscalesToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem porLíneasToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem asociaciónMasivaDeCFDIToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem asociaciónManualmenteToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem asociaciónMasivaDeCFDIGeneralToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem descargarPlantillaAsociaciónMasivaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem catalogosDeConfiguraciónToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem unidadDeMedidaDelSATToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem clasificaciónDeProductosDelSATToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem reactivarFacturaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem cambiarFechaDeFactuarEnVisualToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem modificacionesAVisualToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem modificarDireccionDeEnvioToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem cambiarFechaDeFacturaEnVisualToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem utilidadesToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem migrarCFDIToolStripMenuItem;
        private System.Windows.Forms.DataGridViewTextBoxColumn Estado;
        private System.Windows.Forms.DataGridViewTextBoxColumn EstadoFinal;
        private System.Windows.Forms.DataGridViewTextBoxColumn Factura;
        private System.Windows.Forms.DataGridViewTextBoxColumn FechaFactura;
        private System.Windows.Forms.DataGridViewTextBoxColumn RMA;
        private System.Windows.Forms.DataGridViewTextBoxColumn TipoRMA;
        private System.Windows.Forms.DataGridViewTextBoxColumn FechaRMA;
        private System.Windows.Forms.DataGridViewTextBoxColumn Pedido;
        private System.Windows.Forms.DataGridViewTextBoxColumn FechaPedido;
        private System.Windows.Forms.ToolStripMenuItem relgasDeTimbradoAutomaticoToolStripMenuItem;
    }
}