﻿using System;
using System.Collections.Generic;

using System.Text;
using System.Data;
using System.Windows;
using System.Windows.Forms;
using Generales;

namespace FE_FX
{
    class cfdiPoliza
    {
        private string sSQL = "";
        public string ROW_ID { get; set; }
        public string fecha   { get; set; }
        public string factura  { get; set; }
        public string cuentaContable  { get; set; }
        public string referencia  { get; set; }
        public decimal cargo { get; set; }
        public decimal abono  { get; set; }
        public string estado  { get; set; }

        private cCONEXCION oData = new cCONEXCION("");

        public cfdiPoliza()
        {
            limpiar();
        }

        public cfdiPoliza(cCONEXCION pData)
        {
            oData = pData;
            limpiar();
        }

        public void limpiar()
        {
            ROW_ID = "";
            fecha = DateTime.Now.ToShortDateString();
            factura="";
            cuentaContable="";
            referencia="";
            cargo=0;
            abono=0;
            estado="";

        }

        public List<cfdiPoliza> todos(string BUSQUEDA)
        {

            List<cfdiPoliza> lista = new List<cfdiPoliza>();

            sSQL  = " SELECT * ";
            sSQL += " FROM cfdiPoliza ";
            sSQL += " WHERE factura LIKE '%" + BUSQUEDA + "%' ";
            DataTable oDataTable = oData.EjecutarConsulta(sSQL);
            foreach (DataRow oDataRow in oDataTable.Rows)
            {
                lista.Add(new cfdiPoliza()
                {
                    ROW_ID = oDataRow["ROW_ID"].ToString()
                   ,
                    fecha = oDataRow["fecha"].ToString()
                    ,
                    factura = oDataRow["factura"].ToString()
                    ,
                    cuentaContable = oDataRow["cuentaContable"].ToString()
                    ,
                    referencia = oDataRow["referencia"].ToString()
                    ,
                    cargo = decimal.Parse(oDataRow["cargo"].ToString())
                    ,
                    abono = decimal.Parse(oDataRow["abono"].ToString())
                    ,
                    estado = oDataRow["estado"].ToString()
                });
            }
            return lista;
        }

        public bool cargar(string ROW_ID_EMPRESAp)
        {
            sSQL  = " SELECT * ";
            sSQL += " FROM cfdiPoliza " +
            " WHERE ROW_ID_EMPRESA=" + ROW_ID_EMPRESAp + "";
            DataTable oDataTable = oData.EjecutarConsulta(sSQL);
            if (oDataTable != null)
            {
                foreach (DataRow oDataRow in oDataTable.Rows)
                {
                    //Añadir el Registro
                    ROW_ID = oDataRow["ROW_ID"].ToString();
                    fecha = oDataRow["fecha"].ToString();
                    factura = oDataRow["factura"].ToString();
                    cuentaContable = oDataRow["cuentaContable"].ToString();
                    referencia = oDataRow["referencia"].ToString();
                    cargo = decimal.Parse(oDataRow["cargo"].ToString());
                    abono = decimal.Parse(oDataRow["abono"].ToString());
                    estado = oDataRow["estado"].ToString();
                    return true;
                }
            }
            return false;
        }

        public bool guardar()
        {
            if (fecha.Trim() == "")
            {
                MessageBox.Show("El  es un campo requerido.", Application.ProductName + "-" + Application.ProductVersion, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return false;
            }

            if (ROW_ID == "")
            {

                sSQL = @" INSERT INTO cfdiPoliza 
                    ( 
                    fecha,factura,cuentaContable
                    ,referencia,cargo,abono
                    ,estado
                    )";
                sSQL += " VALUES ";
                sSQL += "(";
                sSQL += " '" + fecha + "','" + factura + "','" + cuentaContable + "'";
                sSQL += ",'" + referencia + "'," + cargo + "," + abono + "";
                sSQL += ",'" + estado + "'";
                sSQL += ")";
                DataTable oDataTable = oData.EjecutarConsulta(sSQL);
                if (oDataTable!=null)
                {

                    sSQL = "SELECT TOP 1 MAX(ROW_ID) as ROW_ID FROM cfdiPoliza";
                    oDataTable = oData.EjecutarConsulta(sSQL);
                    if (oDataTable != null)
                    {
                        foreach (DataRow oDataRow in oDataTable.Rows)
                        {
                            ROW_ID = oDataRow["ROW_ID"].ToString();
                        }
                    }
                }

            }
            else
            {
                sSQL = " UPDATE cfdiPoliza ";
                sSQL += " SET fecha='" + fecha + "',factura='" + factura +
                @"',cuentaContable='" + cuentaContable + @"',referencia='" + referencia + @"'
                ,referencia='" + referencia + @"',cargo='" + cargo + @"'
                ,abono='" + abono + @"',estado='" + estado + @"'
                WHERE ROW_ID=" + ROW_ID + " ";
                oData.EjecutarConsulta(sSQL);

            }
            return true;

        }

        public bool eliminar(string ROW_IDp)
        {
            if (ROW_IDp != "")
            {
                sSQL = "DELETE FROM cfdiPoliza WHERE ROW_ID=" + ROW_IDp;
                oData.EjecutarConsulta(sSQL);
                return true;
            }
            return false;

        }
    }
}
