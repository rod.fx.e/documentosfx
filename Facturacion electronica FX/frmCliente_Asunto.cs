﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;

using System.Text;
using System.Windows.Forms;
using System.IO;
using Generales;

namespace FE_FX
{
    public partial class frmCliente_Asunto : Form
    {
        private cCONEXCION oData = new cCONEXCION("");
        private cCONEXCION oData_ERP = new cCONEXCION("");
        private cCLIENTE_CONFIGURACION oObjeto;
        private bool modificado;

        public frmCliente_Asunto(string sConn, string sConn_ERP)
        {
            oData.sConn = sConn;
            oData_ERP.sConn = sConn_ERP;
            oObjeto = new cCLIENTE_CONFIGURACION(oData);
            InitializeComponent();
            ajax_loader.Visible = false;
            limpiar();
        }
        public frmCliente_Asunto(string sConn, string sConn_ERP, string ROW_IDp)
        {
            modificado = false;
            oData.sConn = sConn;
            oData_ERP.sConn = sConn_ERP;
            oObjeto = new cCLIENTE_CONFIGURACION(oData);

            InitializeComponent();

            limpiar();
            oObjeto.ROW_ID = ROW_IDp;
            cargar();
        }

        public void limpiar()
        {
            if (modificado)
            {

                DialogResult Resultado = MessageBox.Show("¿Desea guardar los cambios?", Application.ProductName + "-" + Application.ProductVersion, MessageBoxButtons.YesNo);
                if (Resultado.ToString() == "Yes")
                {
                    guardar();
                }


            }
            modificado = false;
            oObjeto = new cCLIENTE_CONFIGURACION(oData);
            ASUNTO.Text = "";
            CUSTOMER_ID.Text = "";
            LEYENDA_FISCAL.Text = "";
            MENSAJE.Text = "";
            FORMATO.Text = "";
            ARCHIVO.Text = "";
            CORREO.Text = "";
            paisEntrega.Text = "";

            USO_CFDI.Items.Clear();
            USO_CFDI.Items.Add(new ComboItem { Name = "Adquisición de mercancias", Value = "G01" });
            USO_CFDI.Items.Add(new ComboItem { Name = "Devoluciones, descuentos o bonificaciones", Value = "G02" });
            USO_CFDI.Items.Add(new ComboItem { Name = "Gastos en general", Value = "G03" });
            USO_CFDI.Items.Add(new ComboItem { Name = "Construcciones", Value = "I01" });
            USO_CFDI.Items.Add(new ComboItem { Name = "Mobilario y equipo de oficina por inversiones", Value = "I02" });
            USO_CFDI.Items.Add(new ComboItem { Name = "Equipo de transporte", Value = "I03" });
            USO_CFDI.Items.Add(new ComboItem { Name = "Equipo de computo y accesorios", Value = "I04" });
            USO_CFDI.Items.Add(new ComboItem { Name = "Dados, troqueles, moldes, matrices y herramental", Value = "I05" });
            USO_CFDI.Items.Add(new ComboItem { Name = "Comunicaciones telefónicas", Value = "I06" });
            USO_CFDI.Items.Add(new ComboItem { Name = "Comunicaciones satelitales", Value = "I07" });
            USO_CFDI.Items.Add(new ComboItem { Name = "Otra maquinaria y equipo", Value = "I08" });
            USO_CFDI.Items.Add(new ComboItem { Name = "Honorarios médicos, dentales y gastos hospitalarios.", Value = "D01" });
            USO_CFDI.Items.Add(new ComboItem { Name = "Gastos médicos por incapacidad o discapacidad", Value = "D02" });
            USO_CFDI.Items.Add(new ComboItem { Name = "Gastos funerales.", Value = "D03" });
            USO_CFDI.Items.Add(new ComboItem { Name = "Donativos.", Value = "D04" });
            USO_CFDI.Items.Add(new ComboItem { Name = "Intereses reales efectivamente pagados por créditos hipotecarios (casa habitación).", Value = "D05" });
            USO_CFDI.Items.Add(new ComboItem { Name = "Aportaciones voluntarias al SAR.", Value = "D06" });
            USO_CFDI.Items.Add(new ComboItem { Name = "Primas por seguros de gastos médicos.", Value = "D07" });
            USO_CFDI.Items.Add(new ComboItem { Name = "Gastos de transportación escolar obligatoria.", Value = "D08" });
            USO_CFDI.Items.Add(new ComboItem { Name = "Depósitos en cuentas para el ahorro, primas que tengan como base planes de pensiones.", Value = "D09" });
            USO_CFDI.Items.Add(new ComboItem { Name = "Pagos por servicios educativos (colegiaturas)", Value = "D10" });
            ComboItem oComboItem = new ComboItem { Name = "Por definir", Value = "P01" };
            USO_CFDI.Items.Add(oComboItem);

            ComboItem oComboItemBLanco = new ComboItem { Name = "", Value = "" };
            USO_CFDI.Items.Add(oComboItemBLanco);
            USO_CFDI.SelectedItem = oComboItemBLanco;
        }

        private void guardar()
        {
            ajax_loader.Visible = true;
            oObjeto.ASUNTO = ASUNTO.Text;
            oObjeto.CUSTOMER_ID = CUSTOMER_ID.Text;
            oObjeto.MENSAJE=MENSAJE.Text;
            oObjeto.ARCHIVO=ARCHIVO.Text;
            oObjeto.FORMATO = FORMATO.Text;
            oObjeto.LEYENDA_FISCAL = LEYENDA_FISCAL.Text;
            oObjeto.CORREO = CORREO.Text;
            oObjeto.paisEntregar = paisEntrega.Text;
            oObjeto.iva0 = iva0.Checked;
            oObjeto.ShipTo = ShipTo.Text;
            oObjeto.Europeo= Europeo.Text;
            oObjeto.usoCFDI = "";
            try
            {
                oObjeto.usoCFDI = ((ComboItem)USO_CFDI.SelectedItem).Value.ToString();
            }
            catch
            {

            }

            if (oObjeto.guardar())
            {
                toolStripStatusLabel1.Text = "Datos Actualizados " + DateTime.Now.ToShortDateString() + " " + DateTime.Now.ToLongTimeString();
                modificado = false;
            }
            ajax_loader.Visible = false;
        }

        private void cargar()
        {
            ajax_loader.Visible = true;
            if (oObjeto.cargar(oObjeto.ROW_ID))
            {
                ASUNTO.Text = oObjeto.ASUNTO;
                CUSTOMER_ID.Text = oObjeto.CUSTOMER_ID;
                MENSAJE.Text = oObjeto.MENSAJE;
                ARCHIVO.Text = oObjeto.ARCHIVO;
                LEYENDA_FISCAL.Text = oObjeto.LEYENDA_FISCAL;
                FORMATO.Text = oObjeto.FORMATO;
                CORREO.Text = oObjeto.CORREO;
                paisEntrega.Text = oObjeto.paisEntregar;
                ShipTo.Text = oObjeto.ShipTo;
                try
                {
                    iva0.Checked = oObjeto.iva0;
                }
                catch
                {

                }
                try
                {
                    Europeo.Text = oObjeto.Europeo;
                }
                catch
                {

                }
                try
                {
                    foreach (ComboItem cbi in USO_CFDI.Items)
                    {
                        if (cbi.Value.Equals(oObjeto.usoCFDI))
                        {
                            USO_CFDI.SelectedItem = cbi;
                            break;
                        }
                    }
                }
                catch
                {

                }
                toolStripStatusLabel1.Text  = "Datos Cargados " + DateTime.Now.ToShortDateString() + " " + DateTime.Now.ToLongTimeString();
                modificado = false;

            }
            ajax_loader.Visible = false;
        }

        private void button3_Click(object sender, EventArgs e)
        {
            guardar();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            limpiar();
        }

        private void frmUsuario_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (modificado)
            {
                DialogResult Resultado = MessageBox.Show("¿Desea guardar los cambios?", Application.ProductName + "-" + Application.ProductVersion, MessageBoxButtons.YesNo);
                if (Resultado.ToString() == "Yes")
                {
                    guardar();
                }
            }
        }

        private void frmUsuario_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)27)
            {
                this.Close();
            }
            else
            {
                modificado = true;
                if ((e.KeyChar.ToString() == "<")
                                | (e.KeyChar.ToString() == ">")
                                
                                | (e.KeyChar.ToString() == "'")
                                | (e.KeyChar.ToString() == "#")
                                )
                {
                    e.Handled = true;
                }
            }
        }

        private void button7_Click(object sender, EventArgs e)
        {
            eliminar();
        }

        private void eliminar()
        {
            DialogResult Resultado = MessageBox.Show("¿Esta seguro de eliminar?", Application.ProductName + "-" + Application.ProductVersion, MessageBoxButtons.YesNo);
            if (Resultado.ToString() != "Yes")
            {
                return;
            }
            if (oObjeto.eliminar(oObjeto.ROW_ID))
            {
                limpiar();
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {

            frmCUSTOMER ofrmCUSTOMER = new frmCUSTOMER(oData_ERP.sConn, "S");
            if (ofrmCUSTOMER.ShowDialog() == DialogResult.OK)
            {
                CUSTOMER_ID.Text = ofrmCUSTOMER.selecionados[0].Cells["ID"].Value.ToString();
            }
        }

        private void frmCliente_Asunto_Load(object sender, EventArgs e)
        {

        }

    }
}
