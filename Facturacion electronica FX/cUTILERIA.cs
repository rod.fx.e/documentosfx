﻿using System;
using System.Collections.Generic;

using System.Text;

namespace FE_FX
{
    class cUTILERIA
    {
        private const string consignos = "áàäéèëíìïóòöúùuÁÀÄÉÈËÍÌÏÓÒÖÚÙÜçÇ";
        private const string sinsignos = "aaaeeeiiiooouuuAAAEEEIIIOOOUUUcC";


        public static string removerAcentos(String texto)
        {
            StringBuilder textoSinAcentos = new StringBuilder(texto.Length);
            int indexConAcento;
            foreach (char caracter in texto)
            {
                indexConAcento = consignos.IndexOf(caracter);
                if (indexConAcento > -1)
                    textoSinAcentos.Append(sinsignos.Substring(indexConAcento, 1));
                else
                    textoSinAcentos.Append(caracter);
            }
            return textoSinAcentos.ToString();
        }

        public static bool IsDate(object Expression)
        {
            if (Expression != null)
            {
                if (Expression is DateTime)
                {
                    return true;
                }
                if (Expression is string)
                {
                    try
                    {
                        DateTime dt;
                        DateTime.TryParse(Expression.ToString(), out dt);
                        if (dt != DateTime.MinValue && dt != DateTime.MaxValue)
                            return true;
                        return false;
                    }
                    catch (Exception)
                    {
                    }
                }
            }
            return false;
        }

        public static string Trunca_y_formatea(decimal numero)
        {
            string resultado = "0.0000";
            decimal numero_f = Truncar4decimales(decimal.Parse(numero.ToString()));
            resultado = formato_decimal(numero_f.ToString(), "");
            return (resultado);
        }

        public static string IsNullNumero(object Expression)
        {
            string valor = Trunca_y_formatea(0);
            if (IsNumeric(Expression))
            {
                valor = Expression.ToString();
            }
            else
            {
                valor = Trunca_y_formatea(0);
            }
            return valor;
        }

        public static bool IsBool(string Expression)
        {
            try
            {

                bool resultado = bool.Parse(Expression);
                return true;
            }
            catch
            {
                return false;
            }
        }

        public static bool IsNumeric(object Expression)
        {
            try
            {
                Convert.ToDouble(Expression);
                return true;
            }
            catch
            {
                return false;
            }
        }

        public static string formato_decimal(string valor, string con_coma)
        {
            string conversion;
            double Plantilla;
            if (valor.Trim() == "")
            {
                valor = "0";
            }
            Plantilla = double.Parse(valor.ToString());
            if (con_coma == "")
            {
                conversion = Plantilla.ToString("#,###,###,##0.00");
            }
            else
            {
                conversion = Plantilla.ToString("#########0.00");
            }
            return conversion;
        }

        public static void limpiar_bd()
        {

        }

        public static decimal Truncar4decimales(decimal numero)
        {
            int numero_truncar = 10000;
            decimal parte_decimal = 0, pasar_a_decimal = 0, resultado = 0;
            int parte_entera, separar_2_primeros_decimales;

            parte_entera = (int)numero;
            if (parte_entera != 0)
            {
                parte_decimal = numero % parte_entera;
            }
            else
            {
                parte_decimal = numero;
            }
            separar_2_primeros_decimales = (int)(parte_decimal * numero_truncar);
            pasar_a_decimal = (decimal)separar_2_primeros_decimales / numero_truncar;
            resultado = (decimal)parte_entera + pasar_a_decimal;

            return (resultado);
        }
        public static decimal TruncarNumdecimales(decimal numero, int numero_entero)
        {
            decimal parte_decimal = 0, pasar_a_decimal = 0, resultado = 0;
            int parte_entera, separar_2_primeros_decimales;

            parte_entera = (int)numero;
            if (parte_entera != 0)
            {
                parte_decimal = numero % parte_entera;
            }
            else
            {
                parte_decimal = numero;
            }
            separar_2_primeros_decimales = (int)(parte_decimal * numero_entero);
            pasar_a_decimal = (decimal)separar_2_primeros_decimales / numero_entero;
            resultado = (decimal)parte_entera + pasar_a_decimal;

            return (resultado);
        }


    }
}
