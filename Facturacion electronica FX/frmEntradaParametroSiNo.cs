﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;

using System.Text;
using System.Windows.Forms;
using System.Text.RegularExpressions;

namespace FE_FX
{
    public partial class frmEntradaParametroSiNo : Form
    {
        public string VALORp = "false";
        public frmEntradaParametroSiNo(string mensaje)
        {
            InitializeComponent();
            label1.Text = mensaje;
        }


        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {

            VALORp = "true";
            DialogResult = DialogResult.OK;
            
        }

        private void button2_Click(object sender, EventArgs e)
        {
            VALORp = "false";
            DialogResult = DialogResult.Cancel;
        }
    }
}
