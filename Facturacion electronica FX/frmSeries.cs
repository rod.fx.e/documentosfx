﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Generales;

namespace FE_FX
{
    public partial class frmSeries : Form
    {
        private cCONEXCION oData = new cCONEXCION("");
        string ROW_ID_EMPRESA = "";
        string devolver = "";
        public DataGridViewSelectedRowCollection selecionados;

        public frmSeries(string sConn, string devolverp = "")
        {
            oData.sConn = sConn;
            devolver = devolverp;
            InitializeComponent();
            ajax_loader.Visible = false;
            Columnas(dtgrdGeneral);
        }

        public frmSeries(string sConn, string devolverp,string ROW_ID_EMPRESAp)
        {
            oData.sConn = sConn;
            devolver = devolverp;
            ROW_ID_EMPRESA = ROW_ID_EMPRESAp;
            InitializeComponent();
            ajax_loader.Visible = false;
            Columnas(dtgrdGeneral);
        }

        public frmSeries(cEMPRESA ocEMPRESAp)
        {
            devolver = "";
            InitializeComponent();
            ajax_loader.Visible = false;
            Columnas(dtgrdGeneral);
        }

        private void Columnas(DataGridView dtg)
        {
            int n = 0;

            n = dtg.Columns.Add("ROW_ID", "ROW_ID");
            dtg.Columns[n].ReadOnly = true;
            dtg.Columns[n].Visible = false;
            dtg.Columns[n].Width = 50;

            n = dtg.Columns.Add("CERTIFICADO", "Certificado");
            dtg.Columns[n].ReadOnly = true;
            dtg.Columns[n].Visible = true;
            dtg.Columns[n].Width = 120;

            n = dtg.Columns.Add("ID", "Serie");
            dtg.Columns[n].ReadOnly = true;
            dtg.Columns[n].Visible = true;
            dtg.Columns[n].Width = 60;

            //n = dtg.Columns.Add("APROBACION", "Aprobación");
            //dtg.Columns[n].ReadOnly = true;
            //dtg.Columns[n].Visible = true;
            //dtg.Columns[n].Width = 100;

            //n = dtg.Columns.Add("ANIO", "Año");
            //dtg.Columns[n].ReadOnly = true;
            //dtg.Columns[n].Visible = true;
            //dtg.Columns[n].Width = 50;

            //n = dtg.Columns.Add("FOLIO_INICIAL", "Folio Inicial");
            //dtg.Columns[n].ReadOnly = true;
            //dtg.Columns[n].Visible = true;
            //dtg.Columns[n].Width = 85;

            //n = dtg.Columns.Add("FOLIO_FINAL", "Folio Final");
            //dtg.Columns[n].ReadOnly = true;
            //dtg.Columns[n].Visible = true;
            //dtg.Columns[n].Width = 85;

            //n = dtg.Columns.Add("IMPRESOS", "Prox. Folio");
            //dtg.Columns[n].ReadOnly = true;
            //dtg.Columns[n].Visible = true;
            //dtg.Columns[n].Width = 70;

        }

        private void cargar()
        {
            ajax_loader.Visible = true;
            int n;
            cSERIE oObjeto = new cSERIE();
            toolStripStatusLabel1.Text = "Cargando ";
            dtgrdGeneral.Rows.Clear();
            foreach (cSERIE registro in oObjeto.todos(BUSCAR.Text, false))
            {
                n = dtgrdGeneral.Rows.Add();
                dtgrdGeneral.Rows[n].Cells["ROW_ID"].Value = registro.ROW_ID;
                dtgrdGeneral.Rows[n].Cells["ID"].Value = registro.ID;
                
                cCERTIFICADO oCERTIFICADO = new cCERTIFICADO();
                oCERTIFICADO.cargar(registro.ROW_ID_CERTIFICADO);

                dtgrdGeneral.Rows[n].Cells["CERTIFICADO"].Value = oCERTIFICADO.ID;
                //dtgrdGeneral.Rows[n].Cells["APROBACION"].Value = registro.APROBACION;
                //dtgrdGeneral.Rows[n].Cells["ANIO"].Value = registro.ANIO;
                //dtgrdGeneral.Rows[n].Cells["FOLIO_INICIAL"].Value = registro.FOLIO_INICIAL;
                //dtgrdGeneral.Rows[n].Cells["FOLIO_FINAL"].Value = registro.FOLIO_FINAL;
                //dtgrdGeneral.Rows[n].Cells["IMPRESOS"].Value = registro.IMPRESOS;

            }
            toolStripStatusLabel1.Text = "Total " + dtgrdGeneral.Rows.Count.ToString();
            ajax_loader.Visible = false;
        }


        private void cargar_empresa()
        {
            ajax_loader.Visible = true;
            int n;
            cSERIE oObjeto = new cSERIE();
            toolStripStatusLabel1.Text = "Cargando ";
            dtgrdGeneral.Rows.Clear();
            foreach (cSERIE registro in oObjeto.todos_empresa(BUSCAR.Text,ROW_ID_EMPRESA))
            {
                n = dtgrdGeneral.Rows.Add();
                dtgrdGeneral.Rows[n].Cells["ROW_ID"].Value = registro.ROW_ID;
                dtgrdGeneral.Rows[n].Cells["ID"].Value = registro.ID;

                cCERTIFICADO oCERTIFICADO = new cCERTIFICADO();
                oCERTIFICADO.cargar(registro.ROW_ID_CERTIFICADO);

                dtgrdGeneral.Rows[n].Cells["CERTIFICADO"].Value = oCERTIFICADO.ID;
                //dtgrdGeneral.Rows[n].Cells["APROBACION"].Value = registro.APROBACION;
                //dtgrdGeneral.Rows[n].Cells["ANIO"].Value = registro.ANIO;
                //dtgrdGeneral.Rows[n].Cells["FOLIO_INICIAL"].Value = registro.FOLIO_INICIAL;
                //dtgrdGeneral.Rows[n].Cells["FOLIO_FINAL"].Value = registro.FOLIO_FINAL;
                //dtgrdGeneral.Rows[n].Cells["IMPRESOS"].Value = registro.IMPRESOS;

            }
            toolStripStatusLabel1.Text = "Total " + dtgrdGeneral.Rows.Count.ToString();
            ajax_loader.Visible = false;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            cargar();
        }

        private void BUSCAR_KeyPress(object sender, KeyPressEventArgs e)
        {
            
            if ((e.KeyChar.ToString() == "<")
                | (e.KeyChar.ToString() == ">")
                
                | (e.KeyChar.ToString() == "'")
                | (e.KeyChar.ToString() == "#")
                )
            {
                e.Handled = true;
            }            
            //Precionar Enter buscar
            if (e.KeyChar.ToString() == "\r")
            {
                cargar();
            }
            
        }

        private void frmUsuarios_Load(object sender, EventArgs e)
        {
            if (ROW_ID_EMPRESA.Trim() == "")
            {
                cargar();
            }
            else
            {
                cargar_empresa();
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            frmSerie oObjeto = new frmSerie(oData.sConn);
            oObjeto.ShowDialog();
            cargar();
        }

        private void dtgrdGeneral_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            modificar();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            if (dtgrdGeneral.SelectedRows.Count > 0)
            {
                DataGridViewSelectedRowCollection arrSelectedRows = dtgrdGeneral.SelectedRows;
                string ROW_ID = (string)arrSelectedRows[0].Cells[0].Value;
                frmSerie oObjeto = new frmSerie(oData.sConn, ROW_ID);
                oObjeto.ShowDialog();
                cargar();
            }
        }

        public void modificar()
        {
            if (dtgrdGeneral.SelectedRows.Count > 0)
            {
                DataGridViewSelectedRowCollection arrSelectedRows = dtgrdGeneral.SelectedRows;
                string ROW_ID = (string)arrSelectedRows[0].Cells[0].Value;
                if (devolver == "")
                {

                    frmSerie oObjeto = new frmSerie(oData.sConn, ROW_ID);
                    oObjeto.ShowDialog();
                    cargar();
                }
                else
                {
                    selecionados = arrSelectedRows;
                    this.DialogResult = DialogResult.OK;
                }
            }
        }

        private void frmSeries_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)27)
            {
                this.Close();
            }
            else
            {
                if ((e.KeyChar.ToString() == "<")
                                | (e.KeyChar.ToString() == ">")
                                
                                | (e.KeyChar.ToString() == "'")
                                | (e.KeyChar.ToString() == "#")
                                )
                {
                    e.Handled = true;
                }
            }
        }
    }
}
