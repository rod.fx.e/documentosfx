﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Windows;
using System.Windows.Forms;
using ModeloDocumentosFX;
using Generales;

namespace FE_FX
{
    class cCCE
    {
        private string sSQL = "";
        public string ROW_ID { get; set; }
        public string UDFVMPRTMNT { get; set; }
        public string ROW_ID_EMPRESA { get; set; }
        public string excelProductCommodityFraccion { get; set; }
        private cCONEXCION oData = new cCONEXCION("");

        public cCCE()
        {
            ModeloDocumentosFXContainer dbContext = new ModeloDocumentosFXContainer();
            try
            {
                string conection = dbContext.Database.Connection.ConnectionString;
                oData = new cCONEXCION(conection);
            }
            catch (Exception err)
            {
                ErrorFX.mostrar(err, false, true, "cCCE - 31 - ", false);
            }
            limpiar();
        }

        public bool agregarComplemento(cEMPRESA ocEMPRESA, cFACTURA oFACTURA, decimal totalImpuestosTrasladados)
        {
            try
            {
                cargar(ocEMPRESA.ROW_ID);
                //Configuración de Empresa
                if (ocEMPRESA.ACTIVAR_COMPLEMENTO_CCE)
                {
                    //MessageBox.Show(" Prueba" + oFACTURA.BILL_TO_COUNTRY);
                    if (!oFACTURA.TYPE.ToUpper().Equals("I"))
                    {
                        BitacoraFX.Log("Factura: " + oFACTURA.INVOICE_ID + " CCE sin Tipo M cCCE-45");
                        return false;
                    }
                    //Rodrigo Escalona: Validar si tiene MEX y y para usa


                    //Factura no es  MEX
                    if (!oFACTURA.BILL_TO_COUNTRY.Contains("MEX"))
                    {
                        if (ocEMPRESA.ENTITY_ID.Contains("FIMA"))
                        {
                            frmTipoPedimento o = new frmTipoPedimento();
                            if (o.ShowDialog() == DialogResult.Cancel)
                            {
                                return false;
                            }
                        }
                        //Validar si tiene algun dato en la configuración de Comercio exterior



                        //Validar direccion de entrega
                        oFACTURA.cargar_direccion_entrega(oFACTURA.INVOICE_ID, ocEMPRESA);
                        if (oFACTURA.cargar_pais_entrega(oFACTURA.CUSTOMER_ID, oFACTURA.ADDR_NO).Contains("MEX"))
                        {
                            BitacoraFX.Log("Factura: " + oFACTURA.INVOICE_ID + " CCE sin pais pais_entrega es MEX cCCE-65");
                            return false;
                        }

                        //Validar que todas las líneas tenga al menos un producto
                        int cuentaSinPART = 0;
                        foreach (cLINEA oLine in oFACTURA.LINEAS)
                        {
                            if (String.IsNullOrEmpty(oLine.PART_ID))
                            {
                                cuentaSinPART++;
                            }
                        }
                        if (oFACTURA.LINEAS.Length == cuentaSinPART)
                        {
                            BitacoraFX.Log("Factura: " + oFACTURA.INVOICE_ID + " CCE sin Cantidad de lineas sin Part cCCE-81");
                            return false;
                        }

                        //No tiene impuestos trasladados
                        if (totalImpuestosTrasladados == 0)
                        {
                            //No sea de clientes excepciones
                            if (excepciones(oFACTURA.CUSTOMER_ID))
                            {
                                BitacoraFX.Log("Factura: " + oFACTURA.INVOICE_ID + " CCE sin por excepciones_CUSTOMER cCCE-91");
                                return false;
                            }
                        }

                        //Validar los Part id
                        foreach (cLINEA oLine in oFACTURA.LINEAS)
                        {

                            if (excepciones_PART_ID(oLine.PART_ID.Trim()))
                            {
                                BitacoraFX.Log("Factura: " + oFACTURA.INVOICE_ID + " CCE sin por excepciones_PART_ID cCCE-101");
                                return false;
                            }
                        }

                        //Validar los Product Code
                        foreach (cLINEA oLine in oFACTURA.LINEAS)
                        {
                            if (excepciones_PRODUCT_CODE(oLine.PRODUCT_CODE))
                            {
                                BitacoraFX.Log("Factura: " + oFACTURA.INVOICE_ID + " CCE sin por excepciones_PRODUCT_CODE cCCE-111");
                                return false;
                            }
                        }
                        return true;
                    }
                }
                BitacoraFX.Log("Factura: " + oFACTURA.INVOICE_ID + " CCE sin por ACTIVAR_COMPLEMENTO_CCE cCCE-118");
                return false;
            }
            catch (Exception err)
            {
                ErrorFX.mostrar(err, false, true, "agregarComplemento - 44 - ", false);
            }
            return false;
        }

        public void limpiar()
        {
            ROW_ID = "";
            UDFVMPRTMNT = "";
            ROW_ID_EMPRESA = "";
            excelProductCommodityFraccion = "";
        }

        public List<cCCE> todos(string BUSQUEDA)
        {

            List<cCCE> lista = new List<cCCE>();

            sSQL  = " SELECT * ";
            sSQL += " FROM CCE ";
            sSQL += " WHERE UDFVMPRTMNT LIKE '%" + BUSQUEDA + "%' ";
            DataTable oDataTable = oData.EjecutarConsulta(sSQL);
            foreach (DataRow oDataRow in oDataTable.Rows)
            {
                lista.Add(new cCCE()
                {
                    ROW_ID = oDataRow["ROW_ID"].ToString()
                   ,
                    UDFVMPRTMNT = oDataRow["UDFVMPRTMNT"].ToString()
                    ,
                    ROW_ID_EMPRESA = oDataRow["ROW_ID_EMPRESA"].ToString()
                    ,
                    excelProductCommodityFraccion = oDataRow["excelProductCommodityFraccion"].ToString()

                });
            }
            return lista;
        }

        public bool cargar(string ROW_ID_EMPRESAp)
        {
            sSQL  = " SELECT * ";
            sSQL += " FROM CCE " +
            " WHERE ROW_ID_EMPRESA=" + ROW_ID_EMPRESAp + "";
            DataTable oDataTable = oData.EjecutarConsulta(sSQL);
            if (oDataTable != null)
            {
                foreach (DataRow oDataRow in oDataTable.Rows)
                {
                    //Añadir el Registro
                    ROW_ID = oDataRow["ROW_ID"].ToString();
                    ROW_ID_EMPRESA = oDataRow["ROW_ID_EMPRESA"].ToString();
                    UDFVMPRTMNT = oDataRow["UDFVMPRTMNT"].ToString();
                    PART_ID_EXCEPCIONES = oDataRow["PART_ID_EXCEPCIONES"].ToString();
                    PRODUCTCODE_EXCEPCIONES = oDataRow["PRODUCTCODE_EXCEPCIONES"].ToString();
                    CUSTOMER_EXCEPCIONES = oDataRow["CUSTOMER_EXCEPCIONES"].ToString();
                    excelProductCommodityFraccion = oDataRow["excelProductCommodityFraccion"].ToString();
                    return true;
                }
            }
            return false;
        }

        public bool guardar()
        {
            if (UDFVMPRTMNT.Trim() == "")
            {
                MessageBox.Show("El  es un campo requerido.", Application.ProductName + "-" + Application.ProductVersion, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return false;
            }

            if (ROW_ID == "")
            {
                sSQL = " INSERT INTO CCE ";
                sSQL += " ( ";
                sSQL += " UDFVMPRTMNT,ROW_ID_EMPRESA,CUSTOMER_EXCEPCIONES,PART_ID_EXCEPCIONES,PRODUCTCODE_EXCEPCIONES, excelProductCommodityFraccion";
                sSQL += " )";
                sSQL += " VALUES ";
                sSQL += "(";
                sSQL += " '" + UDFVMPRTMNT + "','" + ROW_ID_EMPRESA + 
                    @"','" + CUSTOMER_EXCEPCIONES + "','" + PART_ID_EXCEPCIONES + "','" + PRODUCTCODE_EXCEPCIONES + "','" + excelProductCommodityFraccion + "'";
                sSQL += ")";
                DataTable oDataTable = oData.EjecutarConsulta(sSQL);
                if (oDataTable!=null)
                {

                    sSQL = "SELECT TOP 1 MAX(ROW_ID) as ROW_ID FROM CCE";
                    oDataTable = oData.EjecutarConsulta(sSQL);
                    if (oDataTable != null)
                    {
                        foreach (DataRow oDataRow in oDataTable.Rows)
                        {
                            ROW_ID = oDataRow["ROW_ID"].ToString();
                        }
                    }
                }

            }
            else
            {
                sSQL = " UPDATE CCE ";
                sSQL += " SET UDFVMPRTMNT='" + UDFVMPRTMNT + "',ROW_ID_EMPRESA=" + ROW_ID_EMPRESA + 
                    @",CUSTOMER_EXCEPCIONES='" + CUSTOMER_EXCEPCIONES + "',PRODUCTCODE_EXCEPCIONES='" + PRODUCTCODE_EXCEPCIONES + "',PART_ID_EXCEPCIONES='" + PART_ID_EXCEPCIONES + "', excelProductCommodityFraccion='" + excelProductCommodityFraccion + "'";
                sSQL += " WHERE ROW_ID=" + ROW_ID + " ";
                oData.EjecutarConsulta(sSQL);

            }
            return true;

        }

        public bool eliminar(string ROW_IDp)
        {
            if (ROW_IDp != "")
            {
                sSQL = "DELETE FROM CCE WHERE ROW_ID=" + ROW_IDp;
                oData.EjecutarConsulta(sSQL);
                return true;
            }
            return false;

        }
        internal int validarFracciones(cCONEXCION oData_ERP)
        {
            string sSQL = "SELECT  COUNT(*) as Cuenta " +
                " FROM PART " +
                " WHERE ISNULL((SELECT TOP 1 1 FROM USER_DEF_FIELDS UDF WHERE UDF.PROGRAM_ID='VMPRTMNT' AND UDF.ID='" + UDFVMPRTMNT + "' AND UDF.DOCUMENT_ID=PART.ID),0)=0" +
                " AND PART.FABRICATED='Y'";

            if (oData_ERP.sTipo.Contains("ORA"))
            {
                sSQL = "SELECT  COUNT(*) as Cuenta " +
                " FROM PART " +
                " WHERE NVL((SELECT 1 FROM USER_DEF_FIELDS UDF WHERE ROWNUM=1 AND UDF.PROGRAM_ID='VMPRTMNT' AND UDF.ID='" + UDFVMPRTMNT + "' AND UDF.DOCUMENT_ID=PART.ID),0)=0" +
                " AND PART.FABRICATED='Y'";

            }
            DataTable oDataTable = oData_ERP.EjecutarConsulta(sSQL);
            if (oDataTable != null)
            {
                if (oDataTable.Rows.Count != 0)
                {
                    foreach (DataRow oDataRow in oDataTable.Rows)
                    {
                        return int.Parse(oDataRow["Cuenta"].ToString());
                    }
                }
            }
            return 0;

        }

        public string noIdentificacionTmp { get; set; }
        internal string obtenerFraccion(string part, string INVOICE_ID
            , string LINE_NO, cCONEXCION oData_ERP, string descripcion)
        {
            string sSQL = "";
            DataTable oDataTable;

            if (UDFVMPRTMNT.Contains("USER_"))
            {
                try
                {
                    //Verficar la existencia si no actualizar
                    sSQL = "SELECT " + UDFVMPRTMNT + " as UDFVMPRTMNT " +
                    " FROM PART " +
                    " WHERE ID='" + part + "' ";
                    oDataTable = oData_ERP.EjecutarConsulta(sSQL);
                    if (oDataTable != null)
                    {
                        foreach (DataRow oDataRow in oDataTable.Rows)
                        {
                            if (String.IsNullOrEmpty(oDataRow["UDFVMPRTMNT"].ToString()))
                            {
                                MessageBox.Show("El " + UDFVMPRTMNT  + " de " + part + " se encuentra en blanco, por favor ingrese la fracción arancelaría.");
                                return "";
                            }
                            return oDataRow["UDFVMPRTMNT"].ToString().Replace(".000000", "");
                        }
                    }
                }
                catch (Exception err)
                {
                    ErrorFX.mostrar(err, false, true, "- cCCE - 313 - Buscar en Part - ", false);
                }
            }

            if (UDFVMPRTMNT.Contains("HTS_CODE"))
            {
                //Verficar la existencia si no actualizar
                sSQL = "SELECT HTS_CODE " +
                " FROM PART " +
                " WHERE ID='" + part + "' ";
                oDataTable = oData_ERP.EjecutarConsulta(sSQL);
                if (oDataTable != null)
                {
                    foreach (DataRow oDataRow in oDataTable.Rows)
                    {
                        if (String.IsNullOrEmpty(oDataRow["HTS_CODE"].ToString()))
                        {
                            MessageBox.Show("El HTS Code de " + part + " se encuentra en blanco, por favor ingrese la fracción arancelaría.");
                            return "";
                        }
                        return oDataRow["HTS_CODE"].ToString().Replace(".000000", "");
                    }
                }
                //Buscar en la linea de la factura
                if (INVOICE_ID != "")
                {
                    sSQL = @"
                    SELECT HTS_CODE
                    FROM RECEIVABLE_LINE
                    WHERE INVOICE_ID='" + INVOICE_ID + @"' AND LINE_NO=" + LINE_NO;
                    oDataTable = oData_ERP.EjecutarConsulta(sSQL);
                    if (oDataTable != null)
                    {
                        foreach (DataRow oDataRow in oDataTable.Rows)
                        {
                            if (!String.IsNullOrEmpty(oDataRow["HTS_CODE"].ToString()))
                            {
                                //MessageBox.Show("El HTS Code de " + part + " se encuentra en blanco en la línea de la factura, por favor ingrese la fracción arancelaría.");
                                //return "";
                                return oDataRow["HTS_CODE"].ToString().Replace(".000000", "");
                            }
                            
                        }
                    }
                }
                frmFraccionArancelaria ofrmFraccionArancelaria = new frmFraccionArancelaria(descripcion);
                if (ofrmFraccionArancelaria.ShowDialog() == DialogResult.OK)
                {
                    return ofrmFraccionArancelaria.fracciontr;

                }
                return "SIN HTS";
            }

            string tipoDato = buscarTipoDato(oData_ERP);

            //Verficar la existencia si no actualizar
            sSQL = "SELECT STRING_VAL,NUMBER_VAL " +
            " FROM USER_DEF_FIELDS " +
            " WHERE PROGRAM_ID='VMPRTMNT' " +
            " AND ID='" + UDFVMPRTMNT + "' AND DOCUMENT_ID='" + part + "'";
            oDataTable = oData_ERP.EjecutarConsulta(sSQL);
            if (oDataTable != null)
            {
                foreach (DataRow oDataRow in oDataTable.Rows)
                {
                    if (tipoDato.Contains("STRING_VAL"))
                    {
                        return oDataRow["STRING_VAL"].ToString().Replace(".000000", "");
                    }
                    if (tipoDato.Contains("NUMBER_VAL"))
                    {
                        return oDataRow["NUMBER_VAL"].ToString().Replace(".000000","");
                    }
                }
            }
            return "";
        }
        internal string buscarTipoDato(cCONEXCION oData_ERP)
        {
            //Validar que columna es 3
            string tipoDato = "STRING_VAL";
            string sSQL = @" SELECT DATA_TYPE  
            FROM USER_DEF_FIELDS
            WHERE [PROGRAM_ID]='VMPRTMNT' AND ID='" + UDFVMPRTMNT + @"' AND DOCUMENT_ID IS NULL";
            DataTable oDataTable = oData_ERP.EjecutarConsulta(sSQL);
            if (oDataTable != null)
            {
                if (oDataTable.Rows.Count != 0)
                {
                    foreach (DataRow oDataRow in oDataTable.Rows)
                    {
                        switch (oDataRow["DATA_TYPE"].ToString())
                        {
                            case "3":
                                tipoDato = "NUMBER_VAL";
                                break;
                            default:
                                tipoDato = "STRING_VAL";
                                break;
                        }
                    }
                }
            }
            return tipoDato;
        }

        internal void insertarFraccion(string fraccion, string part, cCONEXCION oData_ERP)
        {

            string tipoDato = buscarTipoDato(oData_ERP);
            //Verficar la existencia si no actualizar
            string sSQL = "SELECT * " +
            " FROM USER_DEF_FIELDS " +
            " WHERE [PROGRAM_ID]='VMPRTMNT' " +
            " AND ID='" + UDFVMPRTMNT + "' AND DOCUMENT_ID='" + part.Trim()+ "'";
            DataTable oDataTable = oData_ERP.EjecutarConsulta(sSQL);
            if (oDataTable != null)
            {
                if (oDataTable.Rows.Count == 0)
                {
                    sSQL = " INSERT INTO [USER_DEF_FIELDS] " +
                    " ([PROGRAM_ID],[ID],[DOCUMENT_ID],[" + tipoDato + "]) " + 
                    " VALUES " +
                    " ('VMPRTMNT','" + UDFVMPRTMNT + "','" + part + "','" + fraccion +  "') ";
                }
                else
                {
                    sSQL = "UPDATE USER_DEF_FIELDS SET [" + tipoDato + "]='" + fraccion + "'  " +
                    " WHERE [PROGRAM_ID]='VMPRTMNT' AND DOCUMENT_ID='" + part + "'" +
                    " AND ID='" + UDFVMPRTMNT + "'";
                }
                oData_ERP.EjecutarConsulta(sSQL);
            }
        }

        internal bool excepciones(string p)
        {
            if (CUSTOMER_EXCEPCIONES != null)
            {
                var stringArray = CUSTOMER_EXCEPCIONES.Split(',');
                int pos = Array.IndexOf(stringArray, p);
                if (pos > -1)
                {
                    return true;
                }
            }
                return false;
        }

        internal bool excepciones_PART_ID(string p)
        {
            if (String.IsNullOrEmpty(p))
            {
                return false;
            }
            if (!String.IsNullOrEmpty(PART_ID_EXCEPCIONES))
            {
                var stringArray = PART_ID_EXCEPCIONES.Split(',');
                int pos = Array.IndexOf(stringArray, p);
                if (pos > -1)
                {
                    return true;
                }
            }

            return false;
        }

        internal bool excepciones_PRODUCT_CODE(string p)
        {
            if (String.IsNullOrEmpty(p))
            {
                return false;
            }
            if (!String.IsNullOrEmpty(PRODUCTCODE_EXCEPCIONES))
            {
                var stringArray = PRODUCTCODE_EXCEPCIONES.Split(',');
                int pos = Array.IndexOf(stringArray, p);
                if (pos > -1)
                {
                    return true;
                }
            }
            return false;

        }

        public string CUSTOMER_EXCEPCIONES { get; set; }
        public string PART_ID_EXCEPCIONES { get; set; }
        public string PRODUCTCODE_EXCEPCIONES { get; set; }

    }
}
