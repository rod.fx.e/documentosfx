﻿namespace FE_FX
{
    partial class frmTraslado
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmTraslado));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle7 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle8 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.informacion = new System.Windows.Forms.ToolStripStatusLabel();
            this.comboItem4 = new DevComponents.Editors.ComboItem();
            this.comboItem5 = new DevComponents.Editors.ComboItem();
            this.comboItem6 = new DevComponents.Editors.ComboItem();
            this.buttonX17 = new DevComponents.DotNetBar.ButtonX();
            this.labelX13 = new DevComponents.DotNetBar.LabelX();
            this.UUID = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.labelX12 = new DevComponents.DotNetBar.LabelX();
            this.MONEDA = new DevComponents.DotNetBar.Controls.ComboBoxEx();
            this.labelX8 = new DevComponents.DotNetBar.LabelX();
            this.FECHA = new System.Windows.Forms.DateTimePicker();
            this.OBSERVACIONES = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.ESTADO = new DevComponents.DotNetBar.Controls.ComboBoxEx();
            this.comboItem3 = new DevComponents.Editors.ComboItem();
            this.comboItem1 = new DevComponents.Editors.ComboItem();
            this.comboItem2 = new DevComponents.Editors.ComboItem();
            this.SERIE = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.TIPO_CAMBIO = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.labelX6 = new DevComponents.DotNetBar.LabelX();
            this.IMPUESTO_DATO = new DevComponents.DotNetBar.Controls.ComboBoxEx();
            this.comboItem7 = new DevComponents.Editors.ComboItem();
            this.comboItem8 = new DevComponents.Editors.ComboItem();
            this.comboItem9 = new DevComponents.Editors.ComboItem();
            this.comboItem10 = new DevComponents.Editors.ComboItem();
            this.comboItem12 = new DevComponents.Editors.ComboItem();
            this.ID = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.comboItem14 = new DevComponents.Editors.ComboItem();
            this.comboItem15 = new DevComponents.Editors.ComboItem();
            this.comboItem16 = new DevComponents.Editors.ComboItem();
            this.IMPUESTO = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.RETENCION = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.labelX4 = new DevComponents.DotNetBar.LabelX();
            this.TOTAL = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.labelX3 = new DevComponents.DotNetBar.LabelX();
            this.labelX2 = new DevComponents.DotNetBar.LabelX();
            this.SUBTOTAL = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.labelX1 = new DevComponents.DotNetBar.LabelX();
            this.ENTITY_ID = new DevComponents.DotNetBar.Controls.ComboBoxEx();
            this.comboItem17 = new DevComponents.Editors.ComboItem();
            this.comboItem18 = new DevComponents.Editors.ComboItem();
            this.comboItem19 = new DevComponents.Editors.ComboItem();
            this.labelX11 = new DevComponents.DotNetBar.LabelX();
            this.EMPRESA_NOMBRE = new DevComponents.DotNetBar.LabelX();
            this.labelX14 = new DevComponents.DotNetBar.LabelX();
            this.rfc = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.labelX15 = new DevComponents.DotNetBar.LabelX();
            this.IEPStr = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.labelX17 = new DevComponents.DotNetBar.LabelX();
            this.labelX18 = new DevComponents.DotNetBar.LabelX();
            this.DESCUENTO = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.PATH = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.labelX19 = new DevComponents.DotNetBar.LabelX();
            this.folderBrowserDialog1 = new System.Windows.Forms.FolderBrowserDialog();
            this.FORMATO_IMPRESION = new DevComponents.DotNetBar.Controls.ComboBoxEx();
            this.btnEmisor = new DevComponents.DotNetBar.ButtonX();
            this.buttonX10 = new DevComponents.DotNetBar.ButtonX();
            this.buttonX11 = new DevComponents.DotNetBar.ButtonX();
            this.buttonX9 = new DevComponents.DotNetBar.ButtonX();
            this.buttonX7 = new DevComponents.DotNetBar.ButtonX();
            this.buttonX5 = new DevComponents.DotNetBar.ButtonX();
            this.buttonX2 = new DevComponents.DotNetBar.ButtonX();
            this.buttonX1 = new DevComponents.DotNetBar.ButtonX();
            this.buttonX12 = new DevComponents.DotNetBar.ButtonX();
            this.motivoDescuento = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.labelX28 = new DevComponents.DotNetBar.LabelX();
            this.notasPDF = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.labelX30 = new DevComponents.DotNetBar.LabelX();
            this.labelX31 = new DevComponents.DotNetBar.LabelX();
            this.totalSinDescuento = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.buttonX16 = new DevComponents.DotNetBar.ButtonX();
            this.buttonX19 = new DevComponents.DotNetBar.ButtonX();
            this.receptor = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.labelX16 = new DevComponents.DotNetBar.LabelX();
            this.motivoTraslado = new System.Windows.Forms.ComboBox();
            this.USO_CFDI = new System.Windows.Forms.ComboBox();
            this.label7 = new System.Windows.Forms.Label();
            this.subdivision = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.labelX7 = new DevComponents.DotNetBar.LabelX();
            this.button1 = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.pegarDatosDesdeLíneaDeExcelToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.pegarDatosParaComercioExteriorDesdePlantillaDeExcelToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.copiarDatosDelReceptorAlDestinatarioToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.button3 = new System.Windows.Forms.Button();
            this.button4 = new System.Windows.Forms.Button();
            this.tabPage10 = new System.Windows.Forms.TabPage();
            this.label1 = new System.Windows.Forms.Label();
            this.tabPage9 = new System.Windows.Forms.TabPage();
            this.label14 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.RegimenFiscalReceptor = new System.Windows.Forms.TextBox();
            this.DomicilioFiscalReceptor = new System.Windows.Forms.TextBox();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.errorTimbrado = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.tabPage6 = new System.Windows.Forms.TabPage();
            this.labelX33 = new DevComponents.DotNetBar.LabelX();
            this.facturarA = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.tabPage4 = new System.Windows.Forms.TabPage();
            this.labelX29 = new DevComponents.DotNetBar.LabelX();
            this.direccionEmbarque = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.button2 = new System.Windows.Forms.Button();
            this.tabControl2 = new System.Windows.Forms.TabControl();
            this.tabPage7 = new System.Windows.Forms.TabPage();
            this.ReceptorLocalidad = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.labelX44 = new DevComponents.DotNetBar.LabelX();
            this.ReceptorColonia = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.ReceptorMunicipio = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.labelX42 = new DevComponents.DotNetBar.LabelX();
            this.labelX43 = new DevComponents.DotNetBar.LabelX();
            this.CodigoPostal = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.labelX26 = new DevComponents.DotNetBar.LabelX();
            this.EstadoCE = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.labelX27 = new DevComponents.DotNetBar.LabelX();
            this.Ciudad = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.labelX5 = new DevComponents.DotNetBar.LabelX();
            this.noInterior = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.labelX25 = new DevComponents.DotNetBar.LabelX();
            this.labelX22 = new DevComponents.DotNetBar.LabelX();
            this.Pais = new DevComponents.DotNetBar.Controls.ComboBoxEx();
            this.comboItem13 = new DevComponents.Editors.ComboItem();
            this.comboItem23 = new DevComponents.Editors.ComboItem();
            this.comboItem11 = new DevComponents.Editors.ComboItem();
            this.comboItem20 = new DevComponents.Editors.ComboItem();
            this.noExterior = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.labelX24 = new DevComponents.DotNetBar.LabelX();
            this.calle = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.labelX23 = new DevComponents.DotNetBar.LabelX();
            this.tabPage8 = new System.Windows.Forms.TabPage();
            this.DestinatarioNombre = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.DestinatarioNumRegIdTrib = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.labelX39 = new DevComponents.DotNetBar.LabelX();
            this.labelX38 = new DevComponents.DotNetBar.LabelX();
            this.DestinatarioCP = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.labelX9 = new DevComponents.DotNetBar.LabelX();
            this.DestinatarioEstado = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.labelX10 = new DevComponents.DotNetBar.LabelX();
            this.DestinatarioColonia = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.DestinatarioMunicipio = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.labelX41 = new DevComponents.DotNetBar.LabelX();
            this.labelX32 = new DevComponents.DotNetBar.LabelX();
            this.DestinatarioInterior = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.labelX34 = new DevComponents.DotNetBar.LabelX();
            this.labelX35 = new DevComponents.DotNetBar.LabelX();
            this.DestinatarioPais = new DevComponents.DotNetBar.Controls.ComboBoxEx();
            this.comboItem21 = new DevComponents.Editors.ComboItem();
            this.comboItem22 = new DevComponents.Editors.ComboItem();
            this.comboItem39 = new DevComponents.Editors.ComboItem();
            this.comboItem40 = new DevComponents.Editors.ComboItem();
            this.DestinatarioExterior = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.labelX36 = new DevComponents.DotNetBar.LabelX();
            this.DestinatarioLocalidad = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.DestinatarioCalle = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.labelX40 = new DevComponents.DotNetBar.LabelX();
            this.labelX37 = new DevComponents.DotNetBar.LabelX();
            this.NumRegIdTrib = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.Incoterm = new DevComponents.DotNetBar.Controls.ComboBoxEx();
            this.comboItem24 = new DevComponents.Editors.ComboItem();
            this.comboItem25 = new DevComponents.Editors.ComboItem();
            this.comboItem26 = new DevComponents.Editors.ComboItem();
            this.comboItem27 = new DevComponents.Editors.ComboItem();
            this.comboItem28 = new DevComponents.Editors.ComboItem();
            this.comboItem29 = new DevComponents.Editors.ComboItem();
            this.comboItem30 = new DevComponents.Editors.ComboItem();
            this.comboItem31 = new DevComponents.Editors.ComboItem();
            this.comboItem32 = new DevComponents.Editors.ComboItem();
            this.comboItem33 = new DevComponents.Editors.ComboItem();
            this.comboItem34 = new DevComponents.Editors.ComboItem();
            this.comboItem35 = new DevComponents.Editors.ComboItem();
            this.comboItem36 = new DevComponents.Editors.ComboItem();
            this.comboItem37 = new DevComponents.Editors.ComboItem();
            this.comboItem38 = new DevComponents.Editors.ComboItem();
            this.labelX21 = new DevComponents.DotNetBar.LabelX();
            this.labelX20 = new DevComponents.DotNetBar.LabelX();
            this.activarCE = new System.Windows.Forms.CheckBox();
            this.tabPage5 = new System.Windows.Forms.TabPage();
            this.label23 = new System.Windows.Forms.Label();
            this.PARA = new System.Windows.Forms.TextBox();
            this.ASUNTO = new System.Windows.Forms.TextBox();
            this.MENSAJE = new System.Windows.Forms.RichTextBox();
            this.label18 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.buttonX18 = new DevComponents.DotNetBar.ButtonX();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.dtgrdGeneral = new DevComponents.DotNetBar.Controls.DataGridViewX();
            this.cantidad = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.unidad = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.descripcion = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.noIdentificacion = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.valorUnitario = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.importe = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.fraccionArancelaria = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.NumeroPedimentoInformacionAduanera = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.informacionAduaneraFecha = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.informacionAduaneraAduana = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.informacionAduaneraNumero = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ClaveProdServ = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ClaveUnidad = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Submodelo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Marca = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Modelo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.NumeroSerie = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CantidadAduana = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ValorUnitarioAduana = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ValorDolares = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.NumeroPedimento = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage11 = new System.Windows.Forms.TabPage();
            this.tipoRelacion = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.button5 = new System.Windows.Forms.Button();
            this.button6 = new System.Windows.Forms.Button();
            this.DtgRelacion = new System.Windows.Forms.DataGridView();
            this.UUIDRelacionar = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.IgualarEmisor = new System.Windows.Forms.CheckBox();
            this.comboItem41 = new DevComponents.Editors.ComboItem();
            this.statusStrip1.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.contextMenuStrip1.SuspendLayout();
            this.tabPage10.SuspendLayout();
            this.tabPage9.SuspendLayout();
            this.tabPage2.SuspendLayout();
            this.tabPage6.SuspendLayout();
            this.tabPage4.SuspendLayout();
            this.tabPage3.SuspendLayout();
            this.tabControl2.SuspendLayout();
            this.tabPage7.SuspendLayout();
            this.tabPage8.SuspendLayout();
            this.tabPage5.SuspendLayout();
            this.tabPage1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dtgrdGeneral)).BeginInit();
            this.tabControl1.SuspendLayout();
            this.tabPage11.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DtgRelacion)).BeginInit();
            this.SuspendLayout();
            // 
            // statusStrip1
            // 
            this.statusStrip1.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.informacion});
            this.statusStrip1.Location = new System.Drawing.Point(0, 596);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(885, 22);
            this.statusStrip1.TabIndex = 56;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // informacion
            // 
            this.informacion.Name = "informacion";
            this.informacion.Size = new System.Drawing.Size(42, 17);
            this.informacion.Text = "Estado";
            // 
            // comboItem4
            // 
            this.comboItem4.Text = "FECHA";
            // 
            // comboItem5
            // 
            this.comboItem5.Text = "FACTURA";
            // 
            // comboItem6
            // 
            this.comboItem6.Text = "CLIENTE";
            // 
            // buttonX17
            // 
            this.buttonX17.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.buttonX17.BackColor = System.Drawing.Color.YellowGreen;
            this.buttonX17.ColorTable = DevComponents.DotNetBar.eButtonColor.Flat;
            this.buttonX17.Font = new System.Drawing.Font("Segoe UI", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonX17.ImagePosition = DevComponents.DotNetBar.eImagePosition.Right;
            this.buttonX17.Location = new System.Drawing.Point(823, 168);
            this.buttonX17.Name = "buttonX17";
            this.buttonX17.Size = new System.Drawing.Size(47, 20);
            this.buttonX17.Style = DevComponents.DotNetBar.eDotNetBarStyle.Metro;
            this.buttonX17.TabIndex = 27;
            this.buttonX17.Text = "Ver XML";
            this.buttonX17.Click += new System.EventHandler(this.buttonX17_Click_1);
            // 
            // labelX13
            // 
            this.labelX13.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX13.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX13.ForeColor = System.Drawing.Color.Black;
            this.labelX13.Location = new System.Drawing.Point(17, 171);
            this.labelX13.Name = "labelX13";
            this.labelX13.Size = new System.Drawing.Size(63, 15);
            this.labelX13.TabIndex = 24;
            this.labelX13.Text = "Folio Fiscal";
            // 
            // UUID
            // 
            this.UUID.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            // 
            // 
            // 
            this.UUID.Border.Class = "TextBoxBorder";
            this.UUID.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.UUID.ForeColor = System.Drawing.Color.Black;
            this.UUID.Location = new System.Drawing.Point(81, 168);
            this.UUID.Name = "UUID";
            this.UUID.ReadOnly = true;
            this.UUID.Size = new System.Drawing.Size(271, 20);
            this.UUID.TabIndex = 25;
            // 
            // labelX12
            // 
            this.labelX12.AutoSize = true;
            this.labelX12.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX12.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX12.ForeColor = System.Drawing.Color.Black;
            this.labelX12.Location = new System.Drawing.Point(17, 121);
            this.labelX12.Name = "labelX12";
            this.labelX12.Size = new System.Drawing.Size(42, 15);
            this.labelX12.TabIndex = 18;
            this.labelX12.Text = "Moneda";
            this.labelX12.Click += new System.EventHandler(this.labelX12_Click);
            // 
            // MONEDA
            // 
            this.MONEDA.AutoCompleteCustomSource.AddRange(new string[] {
            "AED",
            "AFN",
            "ALL",
            "AMD",
            "ANG",
            "AOA",
            "ARS",
            "AUD",
            "AWG",
            "AZN",
            "BAM",
            "BBD",
            "BDT",
            "BGN",
            "BHD",
            "BIF",
            "BMD",
            "BND",
            "BOB",
            "BOV",
            "BRL",
            "BSD",
            "BTN",
            "BWP",
            "BYR",
            "BZD",
            "CAD",
            "CDF",
            "CHF",
            "CLF",
            "CLP",
            "CNY",
            "COP",
            "COU",
            "CRC",
            "CSD",
            "CUP",
            "CUC",
            "CVE",
            "CZK",
            "DJF",
            "DKK",
            "DOP",
            "DZD",
            "EGP",
            "ERN",
            "ETB",
            "EUR",
            "FJD",
            "FKP",
            "GBP",
            "GEL",
            "GHS",
            "GIP",
            "GMD",
            "GNF",
            "GTQ",
            "GYD",
            "HKD",
            "HNL",
            "HRK",
            "HTG",
            "HUF",
            "IDR",
            "ILS",
            "INR",
            "IQD",
            "IRR",
            "ISK",
            "JMD",
            "JOD",
            "JPY",
            "KES",
            "KGS",
            "KHR",
            "KMF",
            "KPW",
            "KRW",
            "KWD",
            "KYD",
            "KZT",
            "LAK",
            "LBP",
            "LKR",
            "LRD",
            "LSL",
            "LTL",
            "LVL",
            "LYD",
            "MAD",
            "MDL",
            "MGA",
            "MKD",
            "MMK",
            "MNT",
            "MOP",
            "MRO",
            "MUR",
            "MVR",
            "MWK",
            "MXN",
            "MXV",
            "MYR",
            "MZN",
            "NAD",
            "NGN",
            "NIO",
            "NOK",
            "NPR",
            "NZD",
            "OMR",
            "PAB",
            "PEN",
            "PGK",
            "PHP",
            "PKR",
            "PLN",
            "PYG",
            "QAR",
            "RON",
            "RUB",
            "RWF",
            "SAR",
            "SBD",
            "SCR",
            "SDG",
            "SEK",
            "SGD",
            "SHP",
            "SLL",
            "SOS",
            "SRD",
            "STD",
            "SYP",
            "SZL",
            "THB",
            "TJS",
            "TMT",
            "TND",
            "TOP",
            "TRY",
            "TTD",
            "TWD",
            "TZS",
            "UAH",
            "UGX",
            "USD",
            "USN",
            "USS",
            "UYU",
            "UZS",
            "VEF",
            "VND",
            "VUV",
            "WST",
            "XAF",
            "XAG",
            "XAU",
            "XBA",
            "XBB",
            "XBC",
            "XBD",
            "XCD",
            "XDR",
            "XFO",
            "XFU",
            "XOF",
            "XPD",
            "XPF",
            "XPT",
            "XTS",
            "XXX",
            "YER",
            "ZAR",
            "ZMW",
            "ZWL"});
            this.MONEDA.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.MONEDA.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource;
            this.MONEDA.DisplayMember = "Text";
            this.MONEDA.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.MONEDA.ForeColor = System.Drawing.Color.Black;
            this.MONEDA.FormattingEnabled = true;
            this.MONEDA.ItemHeight = 16;
            this.MONEDA.Location = new System.Drawing.Point(69, 117);
            this.MONEDA.Name = "MONEDA";
            this.MONEDA.Size = new System.Drawing.Size(91, 22);
            this.MONEDA.TabIndex = 19;
            this.MONEDA.Text = "USD";
            this.MONEDA.SelectedIndexChanged += new System.EventHandler(this.MONEDA_SelectedIndexChanged_1);
            // 
            // labelX8
            // 
            this.labelX8.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX8.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX8.ForeColor = System.Drawing.Color.Black;
            this.labelX8.Location = new System.Drawing.Point(642, 13);
            this.labelX8.Name = "labelX8";
            this.labelX8.Size = new System.Drawing.Size(55, 22);
            this.labelX8.TabIndex = 42;
            this.labelX8.Text = "Impuesto";
            this.labelX8.TextAlignment = System.Drawing.StringAlignment.Far;
            // 
            // FECHA
            // 
            this.FECHA.BackColor = System.Drawing.Color.White;
            this.FECHA.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FECHA.ForeColor = System.Drawing.Color.Black;
            this.FECHA.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.FECHA.Location = new System.Drawing.Point(203, 89);
            this.FECHA.Name = "FECHA";
            this.FECHA.Size = new System.Drawing.Size(93, 22);
            this.FECHA.TabIndex = 14;
            this.FECHA.ValueChanged += new System.EventHandler(this.FECHA_ValueChanged_1);
            // 
            // OBSERVACIONES
            // 
            this.OBSERVACIONES.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            // 
            // 
            // 
            this.OBSERVACIONES.Border.Class = "TextBoxBorder";
            this.OBSERVACIONES.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.OBSERVACIONES.ForeColor = System.Drawing.Color.Black;
            this.OBSERVACIONES.Location = new System.Drawing.Point(7, 44);
            this.OBSERVACIONES.Multiline = true;
            this.OBSERVACIONES.Name = "OBSERVACIONES";
            this.OBSERVACIONES.Size = new System.Drawing.Size(631, 56);
            this.OBSERVACIONES.TabIndex = 41;
            this.OBSERVACIONES.WatermarkText = "Escriba aquí las leyenda físcal generales...";
            // 
            // ESTADO
            // 
            this.ESTADO.DisplayMember = "Text";
            this.ESTADO.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.ESTADO.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ESTADO.ForeColor = System.Drawing.Color.Black;
            this.ESTADO.FormattingEnabled = true;
            this.ESTADO.ItemHeight = 16;
            this.ESTADO.Items.AddRange(new object[] {
            this.comboItem3,
            this.comboItem1,
            this.comboItem2});
            this.ESTADO.Location = new System.Drawing.Point(302, 89);
            this.ESTADO.Name = "ESTADO";
            this.ESTADO.Size = new System.Drawing.Size(98, 22);
            this.ESTADO.TabIndex = 15;
            this.ESTADO.Text = "BORRADOR";
            // 
            // comboItem3
            // 
            this.comboItem3.Text = "BORRADOR";
            // 
            // comboItem1
            // 
            this.comboItem1.FontStyle = System.Drawing.FontStyle.Bold;
            this.comboItem1.Text = "TIMBRADO";
            // 
            // comboItem2
            // 
            this.comboItem2.FontStyle = System.Drawing.FontStyle.Bold;
            this.comboItem2.ForeColor = System.Drawing.Color.Red;
            this.comboItem2.Text = "CANCELADO";
            // 
            // SERIE
            // 
            this.SERIE.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            // 
            // 
            // 
            this.SERIE.Border.Class = "TextBoxBorder";
            this.SERIE.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.SERIE.Enabled = false;
            this.SERIE.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.SERIE.ForeColor = System.Drawing.Color.Black;
            this.SERIE.Location = new System.Drawing.Point(69, 89);
            this.SERIE.Margin = new System.Windows.Forms.Padding(0);
            this.SERIE.Name = "SERIE";
            this.SERIE.Size = new System.Drawing.Size(64, 22);
            this.SERIE.TabIndex = 12;
            this.SERIE.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // TIPO_CAMBIO
            // 
            this.TIPO_CAMBIO.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            // 
            // 
            // 
            this.TIPO_CAMBIO.Border.Class = "TextBoxBorder";
            this.TIPO_CAMBIO.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.TIPO_CAMBIO.ForeColor = System.Drawing.Color.Black;
            this.TIPO_CAMBIO.Location = new System.Drawing.Point(199, 118);
            this.TIPO_CAMBIO.Name = "TIPO_CAMBIO";
            this.TIPO_CAMBIO.Size = new System.Drawing.Size(65, 20);
            this.TIPO_CAMBIO.TabIndex = 21;
            this.TIPO_CAMBIO.Text = "0";
            // 
            // labelX6
            // 
            this.labelX6.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX6.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX6.ForeColor = System.Drawing.Color.Black;
            this.labelX6.Location = new System.Drawing.Point(166, 118);
            this.labelX6.Name = "labelX6";
            this.labelX6.Size = new System.Drawing.Size(27, 20);
            this.labelX6.TabIndex = 20;
            this.labelX6.Text = "TC";
            this.labelX6.TextAlignment = System.Drawing.StringAlignment.Far;
            // 
            // IMPUESTO_DATO
            // 
            this.IMPUESTO_DATO.DisplayMember = "Text";
            this.IMPUESTO_DATO.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.IMPUESTO_DATO.ForeColor = System.Drawing.Color.Black;
            this.IMPUESTO_DATO.FormattingEnabled = true;
            this.IMPUESTO_DATO.ItemHeight = 16;
            this.IMPUESTO_DATO.Items.AddRange(new object[] {
            this.comboItem7,
            this.comboItem8,
            this.comboItem9,
            this.comboItem10,
            this.comboItem12});
            this.IMPUESTO_DATO.Location = new System.Drawing.Point(703, 13);
            this.IMPUESTO_DATO.Name = "IMPUESTO_DATO";
            this.IMPUESTO_DATO.Size = new System.Drawing.Size(142, 22);
            this.IMPUESTO_DATO.TabIndex = 43;
            this.IMPUESTO_DATO.Text = "0%";
            this.IMPUESTO_DATO.SelectedIndexChanged += new System.EventHandler(this.IMPUESTO_DATO_SelectedIndexChanged);
            // 
            // comboItem7
            // 
            this.comboItem7.Text = "16%";
            // 
            // comboItem8
            // 
            this.comboItem8.Text = "RETENCION 16%";
            // 
            // comboItem9
            // 
            this.comboItem9.Text = "0%";
            // 
            // comboItem10
            // 
            this.comboItem10.Text = "11%";
            // 
            // comboItem12
            // 
            this.comboItem12.Text = "RETENCION 11%";
            // 
            // ID
            // 
            this.ID.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            // 
            // 
            // 
            this.ID.Border.Class = "TextBoxBorder";
            this.ID.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.ID.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ID.ForeColor = System.Drawing.Color.Black;
            this.ID.Location = new System.Drawing.Point(133, 89);
            this.ID.Margin = new System.Windows.Forms.Padding(0);
            this.ID.Name = "ID";
            this.ID.Size = new System.Drawing.Size(67, 22);
            this.ID.TabIndex = 13;
            this.ID.Text = "000000";
            this.ID.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // comboItem14
            // 
            this.comboItem14.Text = "FACTURA";
            // 
            // comboItem15
            // 
            this.comboItem15.Text = "NOTA DE CREDITO";
            // 
            // comboItem16
            // 
            this.comboItem16.Text = "RECIBO HONORARIOS";
            // 
            // IMPUESTO
            // 
            this.IMPUESTO.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            // 
            // 
            // 
            this.IMPUESTO.Border.Class = "TextBoxBorder";
            this.IMPUESTO.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.IMPUESTO.ForeColor = System.Drawing.Color.Black;
            this.IMPUESTO.Location = new System.Drawing.Point(703, 78);
            this.IMPUESTO.Name = "IMPUESTO";
            this.IMPUESTO.ReadOnly = true;
            this.IMPUESTO.Size = new System.Drawing.Size(142, 20);
            this.IMPUESTO.TabIndex = 49;
            this.IMPUESTO.Text = "0";
            this.IMPUESTO.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // RETENCION
            // 
            this.RETENCION.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            // 
            // 
            // 
            this.RETENCION.Border.Class = "TextBoxBorder";
            this.RETENCION.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.RETENCION.ForeColor = System.Drawing.Color.Black;
            this.RETENCION.Location = new System.Drawing.Point(703, 120);
            this.RETENCION.Name = "RETENCION";
            this.RETENCION.ReadOnly = true;
            this.RETENCION.Size = new System.Drawing.Size(142, 20);
            this.RETENCION.TabIndex = 53;
            this.RETENCION.Text = "0";
            this.RETENCION.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // labelX4
            // 
            this.labelX4.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX4.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX4.ForeColor = System.Drawing.Color.Black;
            this.labelX4.Location = new System.Drawing.Point(641, 120);
            this.labelX4.Name = "labelX4";
            this.labelX4.Size = new System.Drawing.Size(56, 20);
            this.labelX4.TabIndex = 52;
            this.labelX4.Text = "Retención";
            this.labelX4.TextAlignment = System.Drawing.StringAlignment.Far;
            // 
            // TOTAL
            // 
            this.TOTAL.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            // 
            // 
            // 
            this.TOTAL.Border.Class = "TextBoxBorder";
            this.TOTAL.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.TOTAL.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TOTAL.ForeColor = System.Drawing.Color.Black;
            this.TOTAL.Location = new System.Drawing.Point(703, 141);
            this.TOTAL.Name = "TOTAL";
            this.TOTAL.ReadOnly = true;
            this.TOTAL.Size = new System.Drawing.Size(142, 29);
            this.TOTAL.TabIndex = 55;
            this.TOTAL.Text = "0";
            this.TOTAL.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // labelX3
            // 
            this.labelX3.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX3.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX3.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelX3.ForeColor = System.Drawing.Color.Black;
            this.labelX3.Location = new System.Drawing.Point(641, 143);
            this.labelX3.Name = "labelX3";
            this.labelX3.Size = new System.Drawing.Size(56, 27);
            this.labelX3.TabIndex = 54;
            this.labelX3.Text = "Total";
            this.labelX3.TextAlignment = System.Drawing.StringAlignment.Far;
            // 
            // labelX2
            // 
            this.labelX2.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX2.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX2.ForeColor = System.Drawing.Color.Black;
            this.labelX2.Location = new System.Drawing.Point(641, 78);
            this.labelX2.Name = "labelX2";
            this.labelX2.Size = new System.Drawing.Size(56, 20);
            this.labelX2.TabIndex = 48;
            this.labelX2.Text = "Impuesto";
            this.labelX2.TextAlignment = System.Drawing.StringAlignment.Far;
            // 
            // SUBTOTAL
            // 
            this.SUBTOTAL.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            // 
            // 
            // 
            this.SUBTOTAL.Border.Class = "TextBoxBorder";
            this.SUBTOTAL.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.SUBTOTAL.ForeColor = System.Drawing.Color.Black;
            this.SUBTOTAL.Location = new System.Drawing.Point(703, 36);
            this.SUBTOTAL.Name = "SUBTOTAL";
            this.SUBTOTAL.ReadOnly = true;
            this.SUBTOTAL.Size = new System.Drawing.Size(142, 20);
            this.SUBTOTAL.TabIndex = 45;
            this.SUBTOTAL.Text = "0";
            this.SUBTOTAL.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // labelX1
            // 
            this.labelX1.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX1.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX1.ForeColor = System.Drawing.Color.Black;
            this.labelX1.Location = new System.Drawing.Point(641, 36);
            this.labelX1.Name = "labelX1";
            this.labelX1.Size = new System.Drawing.Size(56, 20);
            this.labelX1.TabIndex = 44;
            this.labelX1.Text = "Subtotal";
            this.labelX1.TextAlignment = System.Drawing.StringAlignment.Far;
            // 
            // ENTITY_ID
            // 
            this.ENTITY_ID.DisplayMember = "Text";
            this.ENTITY_ID.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.ENTITY_ID.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ENTITY_ID.ForeColor = System.Drawing.Color.Black;
            this.ENTITY_ID.FormattingEnabled = true;
            this.ENTITY_ID.ItemHeight = 16;
            this.ENTITY_ID.Items.AddRange(new object[] {
            this.comboItem17,
            this.comboItem18,
            this.comboItem19});
            this.ENTITY_ID.Location = new System.Drawing.Point(69, 62);
            this.ENTITY_ID.Name = "ENTITY_ID";
            this.ENTITY_ID.Size = new System.Drawing.Size(331, 22);
            this.ENTITY_ID.TabIndex = 9;
            this.ENTITY_ID.SelectedIndexChanged += new System.EventHandler(this.ENTITY_ID_SelectedIndexChanged);
            // 
            // comboItem17
            // 
            this.comboItem17.Text = "BORRADOR";
            // 
            // comboItem18
            // 
            this.comboItem18.FontStyle = System.Drawing.FontStyle.Bold;
            this.comboItem18.Text = "TIMBRADO";
            // 
            // comboItem19
            // 
            this.comboItem19.FontStyle = System.Drawing.FontStyle.Bold;
            this.comboItem19.ForeColor = System.Drawing.Color.Red;
            this.comboItem19.Text = "CANCELADO";
            // 
            // labelX11
            // 
            this.labelX11.AutoSize = true;
            this.labelX11.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX11.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX11.ForeColor = System.Drawing.Color.Black;
            this.labelX11.Location = new System.Drawing.Point(13, 66);
            this.labelX11.Name = "labelX11";
            this.labelX11.Size = new System.Drawing.Size(37, 15);
            this.labelX11.TabIndex = 8;
            this.labelX11.Text = "Emisor";
            // 
            // EMPRESA_NOMBRE
            // 
            this.EMPRESA_NOMBRE.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.EMPRESA_NOMBRE.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.EMPRESA_NOMBRE.ForeColor = System.Drawing.Color.Black;
            this.EMPRESA_NOMBRE.Location = new System.Drawing.Point(205, 66);
            this.EMPRESA_NOMBRE.Name = "EMPRESA_NOMBRE";
            this.EMPRESA_NOMBRE.Size = new System.Drawing.Size(195, 15);
            this.EMPRESA_NOMBRE.TabIndex = 10;
            // 
            // labelX14
            // 
            this.labelX14.AutoSize = true;
            this.labelX14.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX14.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX14.ForeColor = System.Drawing.Color.Black;
            this.labelX14.Location = new System.Drawing.Point(17, 146);
            this.labelX14.Name = "labelX14";
            this.labelX14.Size = new System.Drawing.Size(43, 15);
            this.labelX14.TabIndex = 22;
            this.labelX14.Text = "Formato";
            this.labelX14.Click += new System.EventHandler(this.labelX12_Click);
            // 
            // rfc
            // 
            this.rfc.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            // 
            // 
            // 
            this.rfc.Border.Class = "TextBoxBorder";
            this.rfc.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.rfc.ForeColor = System.Drawing.Color.Black;
            this.rfc.Location = new System.Drawing.Point(531, 89);
            this.rfc.Name = "rfc";
            this.rfc.Size = new System.Drawing.Size(183, 20);
            this.rfc.TabIndex = 31;
            this.rfc.Text = "XEXX010101000";
            this.rfc.TextChanged += new System.EventHandler(this.rfc_TextChanged);
            // 
            // labelX15
            // 
            this.labelX15.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX15.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX15.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelX15.ForeColor = System.Drawing.Color.Black;
            this.labelX15.Location = new System.Drawing.Point(499, 91);
            this.labelX15.Name = "labelX15";
            this.labelX15.Size = new System.Drawing.Size(45, 17);
            this.labelX15.TabIndex = 30;
            this.labelX15.Text = "RFC";
            // 
            // IEPStr
            // 
            this.IEPStr.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            // 
            // 
            // 
            this.IEPStr.Border.Class = "TextBoxBorder";
            this.IEPStr.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.IEPStr.ForeColor = System.Drawing.Color.Black;
            this.IEPStr.Location = new System.Drawing.Point(703, 99);
            this.IEPStr.Name = "IEPStr";
            this.IEPStr.ReadOnly = true;
            this.IEPStr.Size = new System.Drawing.Size(142, 20);
            this.IEPStr.TabIndex = 51;
            this.IEPStr.Text = "0";
            this.IEPStr.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // labelX17
            // 
            this.labelX17.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX17.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX17.ForeColor = System.Drawing.Color.Black;
            this.labelX17.Location = new System.Drawing.Point(641, 100);
            this.labelX17.Name = "labelX17";
            this.labelX17.Size = new System.Drawing.Size(56, 19);
            this.labelX17.TabIndex = 50;
            this.labelX17.Text = "IEPS";
            this.labelX17.TextAlignment = System.Drawing.StringAlignment.Far;
            // 
            // labelX18
            // 
            this.labelX18.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX18.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX18.ForeColor = System.Drawing.Color.Black;
            this.labelX18.Location = new System.Drawing.Point(641, 57);
            this.labelX18.Name = "labelX18";
            this.labelX18.Size = new System.Drawing.Size(56, 20);
            this.labelX18.TabIndex = 46;
            this.labelX18.Text = "Descuento";
            this.labelX18.TextAlignment = System.Drawing.StringAlignment.Far;
            // 
            // DESCUENTO
            // 
            this.DESCUENTO.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            // 
            // 
            // 
            this.DESCUENTO.Border.Class = "TextBoxBorder";
            this.DESCUENTO.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.DESCUENTO.ForeColor = System.Drawing.Color.Black;
            this.DESCUENTO.Location = new System.Drawing.Point(703, 57);
            this.DESCUENTO.Name = "DESCUENTO";
            this.DESCUENTO.Size = new System.Drawing.Size(142, 20);
            this.DESCUENTO.TabIndex = 47;
            this.DESCUENTO.Text = "0";
            this.DESCUENTO.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // PATH
            // 
            this.PATH.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            // 
            // 
            // 
            this.PATH.Border.Class = "TextBoxBorder";
            this.PATH.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.PATH.ForeColor = System.Drawing.Color.Black;
            this.PATH.Location = new System.Drawing.Point(405, 168);
            this.PATH.Name = "PATH";
            this.PATH.ReadOnly = true;
            this.PATH.Size = new System.Drawing.Size(412, 20);
            this.PATH.TabIndex = 25;
            // 
            // labelX19
            // 
            this.labelX19.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX19.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX19.ForeColor = System.Drawing.Color.Black;
            this.labelX19.Location = new System.Drawing.Point(358, 170);
            this.labelX19.Name = "labelX19";
            this.labelX19.Size = new System.Drawing.Size(41, 15);
            this.labelX19.TabIndex = 24;
            this.labelX19.Text = "Archivo";
            // 
            // FORMATO_IMPRESION
            // 
            this.FORMATO_IMPRESION.DisplayMember = "Text";
            this.FORMATO_IMPRESION.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.FORMATO_IMPRESION.ForeColor = System.Drawing.Color.Black;
            this.FORMATO_IMPRESION.FormattingEnabled = true;
            this.FORMATO_IMPRESION.ItemHeight = 16;
            this.FORMATO_IMPRESION.Location = new System.Drawing.Point(69, 143);
            this.FORMATO_IMPRESION.Name = "FORMATO_IMPRESION";
            this.FORMATO_IMPRESION.Size = new System.Drawing.Size(330, 22);
            this.FORMATO_IMPRESION.TabIndex = 598;
            this.FORMATO_IMPRESION.WatermarkText = "Formato de impresión";
            // 
            // btnEmisor
            // 
            this.btnEmisor.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btnEmisor.ColorTable = DevComponents.DotNetBar.eButtonColor.Office2007WithBackground;
            this.btnEmisor.Image = global::FE_FX.Properties.Resources.ic_search_black_18dp_1x;
            this.btnEmisor.Location = new System.Drawing.Point(417, 64);
            this.btnEmisor.Name = "btnEmisor";
            this.btnEmisor.Size = new System.Drawing.Size(74, 20);
            this.btnEmisor.Style = DevComponents.DotNetBar.eDotNetBarStyle.Metro;
            this.btnEmisor.TabIndex = 29;
            this.btnEmisor.Text = "Receptor";
            this.btnEmisor.TextAlignment = DevComponents.DotNetBar.eButtonTextAlignment.Left;
            this.btnEmisor.Click += new System.EventHandler(this.buttonX4_Click);
            // 
            // buttonX10
            // 
            this.buttonX10.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.buttonX10.BackColor = System.Drawing.Color.DarkSeaGreen;
            this.buttonX10.ColorTable = DevComponents.DotNetBar.eButtonColor.Blue;
            this.buttonX10.Font = new System.Drawing.Font("Segoe UI", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonX10.Image = global::FE_FX.Properties.Resources.ic_label_outline_black_24dp_1x;
            this.buttonX10.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top;
            this.buttonX10.Location = new System.Drawing.Point(254, 12);
            this.buttonX10.Name = "buttonX10";
            this.buttonX10.Size = new System.Drawing.Size(42, 41);
            this.buttonX10.Style = DevComponents.DotNetBar.eDotNetBarStyle.Metro;
            this.buttonX10.TabIndex = 5;
            this.buttonX10.Text = "Insertar";
            this.buttonX10.Click += new System.EventHandler(this.buttonX10_Click);
            // 
            // buttonX11
            // 
            this.buttonX11.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.buttonX11.BackColor = System.Drawing.Color.DarkSeaGreen;
            this.buttonX11.ColorTable = DevComponents.DotNetBar.eButtonColor.Blue;
            this.buttonX11.Font = new System.Drawing.Font("Segoe UI", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonX11.Image = global::FE_FX.Properties.Resources.ic_delete_sweep_black_24dp_1x;
            this.buttonX11.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top;
            this.buttonX11.Location = new System.Drawing.Point(298, 12);
            this.buttonX11.Name = "buttonX11";
            this.buttonX11.Size = new System.Drawing.Size(42, 41);
            this.buttonX11.Style = DevComponents.DotNetBar.eDotNetBarStyle.Metro;
            this.buttonX11.TabIndex = 6;
            this.buttonX11.Text = "Eliminar";
            this.buttonX11.Click += new System.EventHandler(this.buttonX11_Click_1);
            // 
            // buttonX9
            // 
            this.buttonX9.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.buttonX9.BackColor = System.Drawing.Color.DarkSeaGreen;
            this.buttonX9.ColorTable = DevComponents.DotNetBar.eButtonColor.Blue;
            this.buttonX9.Font = new System.Drawing.Font("Segoe UI", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonX9.Image = global::FE_FX.Properties.Resources.ic_cancel_black_24dp_1x;
            this.buttonX9.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top;
            this.buttonX9.Location = new System.Drawing.Point(176, 12);
            this.buttonX9.Name = "buttonX9";
            this.buttonX9.Size = new System.Drawing.Size(42, 41);
            this.buttonX9.Style = DevComponents.DotNetBar.eDotNetBarStyle.Metro;
            this.buttonX9.TabIndex = 4;
            this.buttonX9.Text = "Cancelar";
            // 
            // buttonX7
            // 
            this.buttonX7.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.buttonX7.BackColor = System.Drawing.Color.DarkSeaGreen;
            this.buttonX7.ColorTable = DevComponents.DotNetBar.eButtonColor.Blue;
            this.buttonX7.Font = new System.Drawing.Font("Segoe UI", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonX7.Image = global::FE_FX.Properties.Resources.ic_local_printshop_black_24dp_1x;
            this.buttonX7.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top;
            this.buttonX7.Location = new System.Drawing.Point(133, 12);
            this.buttonX7.Name = "buttonX7";
            this.buttonX7.Size = new System.Drawing.Size(41, 41);
            this.buttonX7.Style = DevComponents.DotNetBar.eDotNetBarStyle.Metro;
            this.buttonX7.TabIndex = 3;
            this.buttonX7.Text = "Imprimir";
            this.buttonX7.Click += new System.EventHandler(this.buttonX7_Click_1);
            // 
            // buttonX5
            // 
            this.buttonX5.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.buttonX5.BackColor = System.Drawing.Color.DarkSeaGreen;
            this.buttonX5.ColorTable = DevComponents.DotNetBar.eButtonColor.Blue;
            this.buttonX5.Font = new System.Drawing.Font("Segoe UI", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonX5.Image = global::FE_FX.Properties.Resources.ic_save_black_24dp_1x;
            this.buttonX5.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top;
            this.buttonX5.Location = new System.Drawing.Point(56, 12);
            this.buttonX5.Name = "buttonX5";
            this.buttonX5.Size = new System.Drawing.Size(42, 41);
            this.buttonX5.Style = DevComponents.DotNetBar.eDotNetBarStyle.Metro;
            this.buttonX5.TabIndex = 1;
            this.buttonX5.Text = "Guardar";
            this.buttonX5.Click += new System.EventHandler(this.buttonX5_Click);
            // 
            // buttonX2
            // 
            this.buttonX2.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.buttonX2.BackColor = System.Drawing.Color.DeepSkyBlue;
            this.buttonX2.ColorTable = DevComponents.DotNetBar.eButtonColor.Blue;
            this.buttonX2.Font = new System.Drawing.Font("Segoe UI", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonX2.Image = global::FE_FX.Properties.Resources.ic_create_new_folder_black_24dp_1x;
            this.buttonX2.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top;
            this.buttonX2.Location = new System.Drawing.Point(12, 12);
            this.buttonX2.Name = "buttonX2";
            this.buttonX2.Size = new System.Drawing.Size(42, 41);
            this.buttonX2.Style = DevComponents.DotNetBar.eDotNetBarStyle.Metro;
            this.buttonX2.TabIndex = 0;
            this.buttonX2.Text = "Nuevo";
            this.buttonX2.Click += new System.EventHandler(this.buttonX2_Click_1);
            // 
            // buttonX1
            // 
            this.buttonX1.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.buttonX1.BackColor = System.Drawing.Color.DarkSeaGreen;
            this.buttonX1.ColorTable = DevComponents.DotNetBar.eButtonColor.Blue;
            this.buttonX1.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonX1.Image = global::FE_FX.Properties.Resources.ic_search_black_18dp_1x;
            this.buttonX1.Location = new System.Drawing.Point(12, 89);
            this.buttonX1.Name = "buttonX1";
            this.buttonX1.Size = new System.Drawing.Size(54, 22);
            this.buttonX1.Style = DevComponents.DotNetBar.eDotNetBarStyle.Metro;
            this.buttonX1.TabIndex = 11;
            this.buttonX1.Text = "Serie";
            this.buttonX1.TextAlignment = DevComponents.DotNetBar.eButtonTextAlignment.Right;
            this.buttonX1.Click += new System.EventHandler(this.buttonX1_Click);
            // 
            // buttonX12
            // 
            this.buttonX12.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.buttonX12.BackColor = System.Drawing.Color.DarkSeaGreen;
            this.buttonX12.ColorTable = DevComponents.DotNetBar.eButtonColor.Blue;
            this.buttonX12.Font = new System.Drawing.Font("Segoe UI", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonX12.Image = global::FE_FX.Properties.Resources.ic_delete_sweep_black_24dp_1x;
            this.buttonX12.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top;
            this.buttonX12.Location = new System.Drawing.Point(342, 12);
            this.buttonX12.Name = "buttonX12";
            this.buttonX12.Size = new System.Drawing.Size(42, 41);
            this.buttonX12.Style = DevComponents.DotNetBar.eDotNetBarStyle.Metro;
            this.buttonX12.TabIndex = 605;
            this.buttonX12.Text = "Importar";
            // 
            // motivoDescuento
            // 
            this.motivoDescuento.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            // 
            // 
            // 
            this.motivoDescuento.Border.Class = "TextBoxBorder";
            this.motivoDescuento.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.motivoDescuento.ForeColor = System.Drawing.Color.Black;
            this.motivoDescuento.Location = new System.Drawing.Point(363, 16);
            this.motivoDescuento.Multiline = true;
            this.motivoDescuento.Name = "motivoDescuento";
            this.motivoDescuento.Size = new System.Drawing.Size(275, 20);
            this.motivoDescuento.TabIndex = 607;
            this.motivoDescuento.WatermarkText = "Escriba aquí el motivo de descuento";
            // 
            // labelX28
            // 
            this.labelX28.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX28.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX28.ForeColor = System.Drawing.Color.Black;
            this.labelX28.Location = new System.Drawing.Point(249, 16);
            this.labelX28.Name = "labelX28";
            this.labelX28.Size = new System.Drawing.Size(108, 20);
            this.labelX28.TabIndex = 608;
            this.labelX28.Text = "Motivo de descuento";
            // 
            // notasPDF
            // 
            this.notasPDF.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            // 
            // 
            // 
            this.notasPDF.Border.Class = "TextBoxBorder";
            this.notasPDF.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.notasPDF.ForeColor = System.Drawing.Color.Black;
            this.notasPDF.Location = new System.Drawing.Point(8, 122);
            this.notasPDF.Multiline = true;
            this.notasPDF.Name = "notasPDF";
            this.notasPDF.Size = new System.Drawing.Size(631, 50);
            this.notasPDF.TabIndex = 610;
            this.notasPDF.Text = "THIS INVOICE IS ONLY FOR CUSTOMS PURPOSES - NOT FOR CUSTOMER REFERENCE";
            this.notasPDF.WatermarkText = "Escriba aquí notas en el PDF ... ";
            // 
            // labelX30
            // 
            this.labelX30.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX30.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX30.ForeColor = System.Drawing.Color.Black;
            this.labelX30.Location = new System.Drawing.Point(8, 101);
            this.labelX30.Name = "labelX30";
            this.labelX30.Size = new System.Drawing.Size(100, 19);
            this.labelX30.TabIndex = 611;
            this.labelX30.Text = "Notas en el PDF";
            // 
            // labelX31
            // 
            this.labelX31.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX31.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX31.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelX31.ForeColor = System.Drawing.Color.Black;
            this.labelX31.Location = new System.Drawing.Point(8, 11);
            this.labelX31.Name = "labelX31";
            this.labelX31.Size = new System.Drawing.Size(120, 27);
            this.labelX31.TabIndex = 613;
            this.labelX31.Text = "Total sin descuento";
            this.labelX31.TextAlignment = System.Drawing.StringAlignment.Far;
            // 
            // totalSinDescuento
            // 
            this.totalSinDescuento.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            // 
            // 
            // 
            this.totalSinDescuento.Border.Class = "TextBoxBorder";
            this.totalSinDescuento.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.totalSinDescuento.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.totalSinDescuento.ForeColor = System.Drawing.Color.Black;
            this.totalSinDescuento.Location = new System.Drawing.Point(134, 12);
            this.totalSinDescuento.Name = "totalSinDescuento";
            this.totalSinDescuento.ReadOnly = true;
            this.totalSinDescuento.Size = new System.Drawing.Size(109, 25);
            this.totalSinDescuento.TabIndex = 614;
            this.totalSinDescuento.Text = "0";
            this.totalSinDescuento.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // buttonX16
            // 
            this.buttonX16.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.buttonX16.BackColor = System.Drawing.Color.DarkSeaGreen;
            this.buttonX16.ColorTable = DevComponents.DotNetBar.eButtonColor.Blue;
            this.buttonX16.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonX16.Location = new System.Drawing.Point(416, 12);
            this.buttonX16.Name = "buttonX16";
            this.buttonX16.Size = new System.Drawing.Size(82, 41);
            this.buttonX16.Style = DevComponents.DotNetBar.eDotNetBarStyle.Metro;
            this.buttonX16.TabIndex = 615;
            this.buttonX16.Text = "Descargar XML";
            this.buttonX16.TextAlignment = DevComponents.DotNetBar.eButtonTextAlignment.Left;
            this.buttonX16.Click += new System.EventHandler(this.buttonX16_Click);
            // 
            // buttonX19
            // 
            this.buttonX19.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.buttonX19.BackColor = System.Drawing.Color.DarkSeaGreen;
            this.buttonX19.ColorTable = DevComponents.DotNetBar.eButtonColor.Blue;
            this.buttonX19.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonX19.Location = new System.Drawing.Point(504, 12);
            this.buttonX19.Name = "buttonX19";
            this.buttonX19.Size = new System.Drawing.Size(81, 41);
            this.buttonX19.Style = DevComponents.DotNetBar.eDotNetBarStyle.Metro;
            this.buttonX19.TabIndex = 616;
            this.buttonX19.Text = "Descargar PDF";
            this.buttonX19.TextAlignment = DevComponents.DotNetBar.eButtonTextAlignment.Left;
            this.buttonX19.Click += new System.EventHandler(this.buttonX19_Click);
            // 
            // receptor
            // 
            this.receptor.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            // 
            // 
            // 
            this.receptor.Border.Class = "TextBoxBorder";
            this.receptor.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.receptor.ForeColor = System.Drawing.Color.Black;
            this.receptor.Location = new System.Drawing.Point(497, 64);
            this.receptor.Name = "receptor";
            this.receptor.Size = new System.Drawing.Size(277, 20);
            this.receptor.TabIndex = 31;
            // 
            // labelX16
            // 
            this.labelX16.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX16.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX16.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelX16.ForeColor = System.Drawing.Color.Black;
            this.labelX16.Location = new System.Drawing.Point(418, 119);
            this.labelX16.Name = "labelX16";
            this.labelX16.Size = new System.Drawing.Size(100, 19);
            this.labelX16.TabIndex = 617;
            this.labelX16.Text = "Motivo de traslado";
            // 
            // motivoTraslado
            // 
            this.motivoTraslado.FormattingEnabled = true;
            this.motivoTraslado.Items.AddRange(new object[] {
            "01-Envío de mercancias facturadas con anterioridad",
            "02-Reubicación de mercancías propias",
            "03-Envío de mercancías objeto de contrato de consignación",
            "04-Envío de mercancías para posterior enajenación",
            "05-Envío de mercancías propiedad de terceros",
            "99-Otros"});
            this.motivoTraslado.Location = new System.Drawing.Point(516, 118);
            this.motivoTraslado.Name = "motivoTraslado";
            this.motivoTraslado.Size = new System.Drawing.Size(354, 21);
            this.motivoTraslado.TabIndex = 618;
            this.motivoTraslado.Text = "03-Envío de mercancías objeto de contrato de consignación";
            // 
            // USO_CFDI
            // 
            this.USO_CFDI.FormattingEnabled = true;
            this.USO_CFDI.Items.AddRange(new object[] {
            "Pago en una sola exhibición",
            "Pago en parcialidades",
            "Pago a credito"});
            this.USO_CFDI.Location = new System.Drawing.Point(493, 144);
            this.USO_CFDI.Name = "USO_CFDI";
            this.USO_CFDI.Size = new System.Drawing.Size(258, 21);
            this.USO_CFDI.TabIndex = 620;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(417, 147);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(70, 13);
            this.label7.TabIndex = 619;
            this.label7.Text = "Uso del CFDI";
            // 
            // subdivision
            // 
            this.subdivision.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            // 
            // 
            // 
            this.subdivision.Border.Class = "TextBoxBorder";
            this.subdivision.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.subdivision.ForeColor = System.Drawing.Color.Black;
            this.subdivision.Location = new System.Drawing.Point(780, 89);
            this.subdivision.Name = "subdivision";
            this.subdivision.Size = new System.Drawing.Size(90, 20);
            this.subdivision.TabIndex = 31;
            this.subdivision.Text = "0";
            // 
            // labelX7
            // 
            this.labelX7.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX7.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX7.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelX7.ForeColor = System.Drawing.Color.Black;
            this.labelX7.Location = new System.Drawing.Point(719, 91);
            this.labelX7.Name = "labelX7";
            this.labelX7.Size = new System.Drawing.Size(59, 17);
            this.labelX7.TabIndex = 30;
            this.labelX7.Text = "Sudivisión";
            // 
            // button1
            // 
            this.button1.BackColor = System.Drawing.Color.Green;
            this.button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button1.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button1.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.button1.Location = new System.Drawing.Point(591, 12);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(86, 41);
            this.button1.TabIndex = 621;
            this.button1.Text = "Importar";
            this.button1.UseVisualStyleBackColor = false;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.groupBox1.Controls.Add(this.IMPUESTO_DATO);
            this.groupBox1.Controls.Add(this.labelX8);
            this.groupBox1.Controls.Add(this.labelX1);
            this.groupBox1.Controls.Add(this.labelX2);
            this.groupBox1.Controls.Add(this.labelX18);
            this.groupBox1.Controls.Add(this.labelX17);
            this.groupBox1.Controls.Add(this.labelX3);
            this.groupBox1.Controls.Add(this.labelX4);
            this.groupBox1.Controls.Add(this.totalSinDescuento);
            this.groupBox1.Controls.Add(this.SUBTOTAL);
            this.groupBox1.Controls.Add(this.labelX31);
            this.groupBox1.Controls.Add(this.labelX30);
            this.groupBox1.Controls.Add(this.DESCUENTO);
            this.groupBox1.Controls.Add(this.notasPDF);
            this.groupBox1.Controls.Add(this.TOTAL);
            this.groupBox1.Controls.Add(this.labelX28);
            this.groupBox1.Controls.Add(this.RETENCION);
            this.groupBox1.Controls.Add(this.motivoDescuento);
            this.groupBox1.Controls.Add(this.IMPUESTO);
            this.groupBox1.Controls.Add(this.IEPStr);
            this.groupBox1.Controls.Add(this.OBSERVACIONES);
            this.groupBox1.Location = new System.Drawing.Point(17, 405);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(858, 180);
            this.groupBox1.TabIndex = 622;
            this.groupBox1.TabStop = false;
            // 
            // contextMenuStrip1
            // 
            this.contextMenuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.pegarDatosDesdeLíneaDeExcelToolStripMenuItem,
            this.pegarDatosParaComercioExteriorDesdePlantillaDeExcelToolStripMenuItem,
            this.copiarDatosDelReceptorAlDestinatarioToolStripMenuItem});
            this.contextMenuStrip1.Name = "contextMenuStrip1";
            this.contextMenuStrip1.Size = new System.Drawing.Size(384, 70);
            // 
            // pegarDatosDesdeLíneaDeExcelToolStripMenuItem
            // 
            this.pegarDatosDesdeLíneaDeExcelToolStripMenuItem.Name = "pegarDatosDesdeLíneaDeExcelToolStripMenuItem";
            this.pegarDatosDesdeLíneaDeExcelToolStripMenuItem.Size = new System.Drawing.Size(383, 22);
            this.pegarDatosDesdeLíneaDeExcelToolStripMenuItem.Text = "Pegar datos desde línea de Plantilla de Excel";
            this.pegarDatosDesdeLíneaDeExcelToolStripMenuItem.Click += new System.EventHandler(this.pegarDatosDesdeLíneaDeExcelToolStripMenuItem_Click);
            // 
            // pegarDatosParaComercioExteriorDesdePlantillaDeExcelToolStripMenuItem
            // 
            this.pegarDatosParaComercioExteriorDesdePlantillaDeExcelToolStripMenuItem.Name = "pegarDatosParaComercioExteriorDesdePlantillaDeExcelToolStripMenuItem";
            this.pegarDatosParaComercioExteriorDesdePlantillaDeExcelToolStripMenuItem.Size = new System.Drawing.Size(383, 22);
            this.pegarDatosParaComercioExteriorDesdePlantillaDeExcelToolStripMenuItem.Text = "Pegar datos para Comercio Exterior desde Plantilla de Excel";
            this.pegarDatosParaComercioExteriorDesdePlantillaDeExcelToolStripMenuItem.Click += new System.EventHandler(this.pegarDatosParaComercioExteriorDesdePlantillaDeExcelToolStripMenuItem_Click);
            // 
            // copiarDatosDelReceptorAlDestinatarioToolStripMenuItem
            // 
            this.copiarDatosDelReceptorAlDestinatarioToolStripMenuItem.Name = "copiarDatosDelReceptorAlDestinatarioToolStripMenuItem";
            this.copiarDatosDelReceptorAlDestinatarioToolStripMenuItem.Size = new System.Drawing.Size(383, 22);
            this.copiarDatosDelReceptorAlDestinatarioToolStripMenuItem.Text = "Copiar datos del Receptor al Destinatario";
            this.copiarDatosDelReceptorAlDestinatarioToolStripMenuItem.Click += new System.EventHandler(this.copiarDatosDelReceptorAlDestinatarioToolStripMenuItem_Click);
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(416, 88);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(75, 23);
            this.button3.TabIndex = 623;
            this.button3.Text = "Extranjero";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // button4
            // 
            this.button4.Location = new System.Drawing.Point(780, 62);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(93, 23);
            this.button4.TabIndex = 624;
            this.button4.Text = "Copiar destinatario";
            this.button4.UseVisualStyleBackColor = true;
            this.button4.Click += new System.EventHandler(this.button4_Click);
            // 
            // tabPage10
            // 
            this.tabPage10.Controls.Add(this.label1);
            this.tabPage10.Location = new System.Drawing.Point(4, 22);
            this.tabPage10.Name = "tabPage10";
            this.tabPage10.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage10.Size = new System.Drawing.Size(853, 178);
            this.tabPage10.TabIndex = 8;
            this.tabPage10.Text = "Propietario";
            this.tabPage10.UseVisualStyleBackColor = true;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(5, 3);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(266, 91);
            this.label1.TabIndex = 0;
            this.label1.Text = resources.GetString("label1.Text");
            // 
            // tabPage9
            // 
            this.tabPage9.Controls.Add(this.label14);
            this.tabPage9.Controls.Add(this.label11);
            this.tabPage9.Controls.Add(this.RegimenFiscalReceptor);
            this.tabPage9.Controls.Add(this.DomicilioFiscalReceptor);
            this.tabPage9.Location = new System.Drawing.Point(4, 22);
            this.tabPage9.Name = "tabPage9";
            this.tabPage9.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage9.Size = new System.Drawing.Size(853, 178);
            this.tabPage9.TabIndex = 7;
            this.tabPage9.Text = "Datos del Receptor";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(284, 9);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(135, 13);
            this.label14.TabIndex = 30;
            this.label14.Text = "Regimen fiscal del receptor";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(8, 9);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(135, 13);
            this.label11.TabIndex = 31;
            this.label11.Text = "Domicilio fiscal del receptor";
            // 
            // RegimenFiscalReceptor
            // 
            this.RegimenFiscalReceptor.Location = new System.Drawing.Point(425, 6);
            this.RegimenFiscalReceptor.Name = "RegimenFiscalReceptor";
            this.RegimenFiscalReceptor.Size = new System.Drawing.Size(113, 20);
            this.RegimenFiscalReceptor.TabIndex = 28;
            this.RegimenFiscalReceptor.Text = "616";
            // 
            // DomicilioFiscalReceptor
            // 
            this.DomicilioFiscalReceptor.Location = new System.Drawing.Point(149, 6);
            this.DomicilioFiscalReceptor.Name = "DomicilioFiscalReceptor";
            this.DomicilioFiscalReceptor.Size = new System.Drawing.Size(132, 20);
            this.DomicilioFiscalReceptor.TabIndex = 29;
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.errorTimbrado);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(853, 178);
            this.tabPage2.TabIndex = 6;
            this.tabPage2.Text = "Error Timbrado";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // errorTimbrado
            // 
            this.errorTimbrado.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            // 
            // 
            // 
            this.errorTimbrado.Border.Class = "TextBoxBorder";
            this.errorTimbrado.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.errorTimbrado.Dock = System.Windows.Forms.DockStyle.Fill;
            this.errorTimbrado.ForeColor = System.Drawing.Color.Black;
            this.errorTimbrado.Location = new System.Drawing.Point(3, 3);
            this.errorTimbrado.Multiline = true;
            this.errorTimbrado.Name = "errorTimbrado";
            this.errorTimbrado.Size = new System.Drawing.Size(847, 172);
            this.errorTimbrado.TabIndex = 612;
            // 
            // tabPage6
            // 
            this.tabPage6.Controls.Add(this.labelX33);
            this.tabPage6.Controls.Add(this.facturarA);
            this.tabPage6.Location = new System.Drawing.Point(4, 22);
            this.tabPage6.Name = "tabPage6";
            this.tabPage6.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage6.Size = new System.Drawing.Size(853, 178);
            this.tabPage6.TabIndex = 5;
            this.tabPage6.Text = "Facturar a";
            this.tabPage6.UseVisualStyleBackColor = true;
            // 
            // labelX33
            // 
            this.labelX33.AutoSize = true;
            this.labelX33.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX33.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX33.ForeColor = System.Drawing.Color.Black;
            this.labelX33.Location = new System.Drawing.Point(6, 6);
            this.labelX33.Name = "labelX33";
            this.labelX33.Size = new System.Drawing.Size(52, 15);
            this.labelX33.TabIndex = 614;
            this.labelX33.Text = "Facturar a";
            // 
            // facturarA
            // 
            this.facturarA.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.facturarA.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            // 
            // 
            // 
            this.facturarA.Border.Class = "TextBoxBorder";
            this.facturarA.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.facturarA.ForeColor = System.Drawing.Color.Black;
            this.facturarA.Location = new System.Drawing.Point(6, 24);
            this.facturarA.Multiline = true;
            this.facturarA.Name = "facturarA";
            this.facturarA.Size = new System.Drawing.Size(845, 168);
            this.facturarA.TabIndex = 613;
            this.facturarA.WatermarkText = "Escriba aquí facturar a...";
            // 
            // tabPage4
            // 
            this.tabPage4.Controls.Add(this.labelX29);
            this.tabPage4.Controls.Add(this.direccionEmbarque);
            this.tabPage4.Location = new System.Drawing.Point(4, 22);
            this.tabPage4.Name = "tabPage4";
            this.tabPage4.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage4.Size = new System.Drawing.Size(853, 178);
            this.tabPage4.TabIndex = 3;
            this.tabPage4.Text = "Dirección de embarque";
            this.tabPage4.UseVisualStyleBackColor = true;
            // 
            // labelX29
            // 
            this.labelX29.AutoSize = true;
            this.labelX29.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX29.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX29.ForeColor = System.Drawing.Color.Black;
            this.labelX29.Location = new System.Drawing.Point(6, 6);
            this.labelX29.Name = "labelX29";
            this.labelX29.Size = new System.Drawing.Size(116, 15);
            this.labelX29.TabIndex = 612;
            this.labelX29.Text = "Dirección de embarque";
            // 
            // direccionEmbarque
            // 
            this.direccionEmbarque.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.direccionEmbarque.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            // 
            // 
            // 
            this.direccionEmbarque.Border.Class = "TextBoxBorder";
            this.direccionEmbarque.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.direccionEmbarque.ForeColor = System.Drawing.Color.Black;
            this.direccionEmbarque.Location = new System.Drawing.Point(6, 24);
            this.direccionEmbarque.Multiline = true;
            this.direccionEmbarque.Name = "direccionEmbarque";
            this.direccionEmbarque.Size = new System.Drawing.Size(845, 168);
            this.direccionEmbarque.TabIndex = 611;
            this.direccionEmbarque.WatermarkText = "Escriba aquí la dirección de embarque...";
            // 
            // tabPage3
            // 
            this.tabPage3.Controls.Add(this.button2);
            this.tabPage3.Controls.Add(this.tabControl2);
            this.tabPage3.Controls.Add(this.NumRegIdTrib);
            this.tabPage3.Controls.Add(this.Incoterm);
            this.tabPage3.Controls.Add(this.labelX21);
            this.tabPage3.Controls.Add(this.labelX20);
            this.tabPage3.Controls.Add(this.activarCE);
            this.tabPage3.Location = new System.Drawing.Point(4, 22);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Size = new System.Drawing.Size(853, 178);
            this.tabPage3.TabIndex = 2;
            this.tabPage3.Text = "Comercio Exterior";
            this.tabPage3.UseVisualStyleBackColor = true;
            this.tabPage3.MouseClick += new System.Windows.Forms.MouseEventHandler(this.tabPage3_MouseClick);
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(444, 22);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(57, 23);
            this.button2.TabIndex = 19;
            this.button2.Text = "Cargar";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // tabControl2
            // 
            this.tabControl2.Controls.Add(this.tabPage7);
            this.tabControl2.Controls.Add(this.tabPage8);
            this.tabControl2.Location = new System.Drawing.Point(5, 51);
            this.tabControl2.Name = "tabControl2";
            this.tabControl2.SelectedIndex = 0;
            this.tabControl2.Size = new System.Drawing.Size(840, 115);
            this.tabControl2.TabIndex = 18;
            // 
            // tabPage7
            // 
            this.tabPage7.Controls.Add(this.ReceptorLocalidad);
            this.tabPage7.Controls.Add(this.labelX44);
            this.tabPage7.Controls.Add(this.ReceptorColonia);
            this.tabPage7.Controls.Add(this.ReceptorMunicipio);
            this.tabPage7.Controls.Add(this.labelX42);
            this.tabPage7.Controls.Add(this.labelX43);
            this.tabPage7.Controls.Add(this.CodigoPostal);
            this.tabPage7.Controls.Add(this.labelX26);
            this.tabPage7.Controls.Add(this.EstadoCE);
            this.tabPage7.Controls.Add(this.labelX27);
            this.tabPage7.Controls.Add(this.Ciudad);
            this.tabPage7.Controls.Add(this.labelX5);
            this.tabPage7.Controls.Add(this.noInterior);
            this.tabPage7.Controls.Add(this.labelX25);
            this.tabPage7.Controls.Add(this.labelX22);
            this.tabPage7.Controls.Add(this.Pais);
            this.tabPage7.Controls.Add(this.noExterior);
            this.tabPage7.Controls.Add(this.labelX24);
            this.tabPage7.Controls.Add(this.calle);
            this.tabPage7.Controls.Add(this.labelX23);
            this.tabPage7.Location = new System.Drawing.Point(4, 22);
            this.tabPage7.Name = "tabPage7";
            this.tabPage7.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage7.Size = new System.Drawing.Size(832, 89);
            this.tabPage7.TabIndex = 0;
            this.tabPage7.Text = "Receptor";
            this.tabPage7.UseVisualStyleBackColor = true;
            // 
            // ReceptorLocalidad
            // 
            this.ReceptorLocalidad.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            // 
            // 
            // 
            this.ReceptorLocalidad.Border.Class = "TextBoxBorder";
            this.ReceptorLocalidad.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.ReceptorLocalidad.ForeColor = System.Drawing.Color.Black;
            this.ReceptorLocalidad.Location = new System.Drawing.Point(343, 58);
            this.ReceptorLocalidad.Name = "ReceptorLocalidad";
            this.ReceptorLocalidad.Size = new System.Drawing.Size(69, 20);
            this.ReceptorLocalidad.TabIndex = 51;
            // 
            // labelX44
            // 
            this.labelX44.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX44.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX44.ForeColor = System.Drawing.Color.Black;
            this.labelX44.Location = new System.Drawing.Point(281, 61);
            this.labelX44.Name = "labelX44";
            this.labelX44.Size = new System.Drawing.Size(56, 15);
            this.labelX44.TabIndex = 50;
            this.labelX44.Text = "Localidad";
            // 
            // ReceptorColonia
            // 
            this.ReceptorColonia.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            // 
            // 
            // 
            this.ReceptorColonia.Border.Class = "TextBoxBorder";
            this.ReceptorColonia.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.ReceptorColonia.ForeColor = System.Drawing.Color.Black;
            this.ReceptorColonia.Location = new System.Drawing.Point(200, 60);
            this.ReceptorColonia.Name = "ReceptorColonia";
            this.ReceptorColonia.Size = new System.Drawing.Size(69, 20);
            this.ReceptorColonia.TabIndex = 48;
            // 
            // ReceptorMunicipio
            // 
            this.ReceptorMunicipio.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            // 
            // 
            // 
            this.ReceptorMunicipio.Border.Class = "TextBoxBorder";
            this.ReceptorMunicipio.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.ReceptorMunicipio.ForeColor = System.Drawing.Color.Black;
            this.ReceptorMunicipio.Location = new System.Drawing.Point(62, 60);
            this.ReceptorMunicipio.Name = "ReceptorMunicipio";
            this.ReceptorMunicipio.Size = new System.Drawing.Size(86, 20);
            this.ReceptorMunicipio.TabIndex = 49;
            // 
            // labelX42
            // 
            this.labelX42.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX42.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX42.ForeColor = System.Drawing.Color.Black;
            this.labelX42.Location = new System.Drawing.Point(154, 63);
            this.labelX42.Name = "labelX42";
            this.labelX42.Size = new System.Drawing.Size(48, 15);
            this.labelX42.TabIndex = 46;
            this.labelX42.Text = "Colonia";
            // 
            // labelX43
            // 
            this.labelX43.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX43.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX43.ForeColor = System.Drawing.Color.Black;
            this.labelX43.Location = new System.Drawing.Point(11, 63);
            this.labelX43.Name = "labelX43";
            this.labelX43.Size = new System.Drawing.Size(48, 15);
            this.labelX43.TabIndex = 47;
            this.labelX43.Text = "Municipio";
            // 
            // CodigoPostal
            // 
            this.CodigoPostal.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            // 
            // 
            // 
            this.CodigoPostal.Border.Class = "TextBoxBorder";
            this.CodigoPostal.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.CodigoPostal.ForeColor = System.Drawing.Color.Black;
            this.CodigoPostal.Location = new System.Drawing.Point(324, 34);
            this.CodigoPostal.Name = "CodigoPostal";
            this.CodigoPostal.Size = new System.Drawing.Size(151, 20);
            this.CodigoPostal.TabIndex = 29;
            this.CodigoPostal.Text = "72855-0000";
            // 
            // labelX26
            // 
            this.labelX26.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX26.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX26.ForeColor = System.Drawing.Color.Black;
            this.labelX26.Location = new System.Drawing.Point(248, 37);
            this.labelX26.Name = "labelX26";
            this.labelX26.Size = new System.Drawing.Size(70, 15);
            this.labelX26.TabIndex = 28;
            this.labelX26.Text = "Código Postal";
            // 
            // EstadoCE
            // 
            this.EstadoCE.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            // 
            // 
            // 
            this.EstadoCE.Border.Class = "TextBoxBorder";
            this.EstadoCE.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.EstadoCE.ForeColor = System.Drawing.Color.Black;
            this.EstadoCE.Location = new System.Drawing.Point(61, 34);
            this.EstadoCE.Name = "EstadoCE";
            this.EstadoCE.Size = new System.Drawing.Size(177, 20);
            this.EstadoCE.TabIndex = 27;
            this.EstadoCE.Text = "AR";
            // 
            // labelX27
            // 
            this.labelX27.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX27.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX27.ForeColor = System.Drawing.Color.Black;
            this.labelX27.Location = new System.Drawing.Point(11, 37);
            this.labelX27.Name = "labelX27";
            this.labelX27.Size = new System.Drawing.Size(44, 15);
            this.labelX27.TabIndex = 26;
            this.labelX27.Text = "Estado";
            // 
            // Ciudad
            // 
            this.Ciudad.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            // 
            // 
            // 
            this.Ciudad.Border.Class = "TextBoxBorder";
            this.Ciudad.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.Ciudad.ForeColor = System.Drawing.Color.Black;
            this.Ciudad.Location = new System.Drawing.Point(537, 35);
            this.Ciudad.Name = "Ciudad";
            this.Ciudad.Size = new System.Drawing.Size(139, 20);
            this.Ciudad.TabIndex = 31;
            // 
            // labelX5
            // 
            this.labelX5.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX5.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX5.ForeColor = System.Drawing.Color.Black;
            this.labelX5.Location = new System.Drawing.Point(480, 39);
            this.labelX5.Name = "labelX5";
            this.labelX5.Size = new System.Drawing.Size(56, 15);
            this.labelX5.TabIndex = 30;
            this.labelX5.Text = "Ciudad";
            // 
            // noInterior
            // 
            this.noInterior.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            // 
            // 
            // 
            this.noInterior.Border.Class = "TextBoxBorder";
            this.noInterior.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.noInterior.ForeColor = System.Drawing.Color.Black;
            this.noInterior.Location = new System.Drawing.Point(657, 5);
            this.noInterior.Name = "noInterior";
            this.noInterior.Size = new System.Drawing.Size(139, 20);
            this.noInterior.TabIndex = 25;
            // 
            // labelX25
            // 
            this.labelX25.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX25.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX25.ForeColor = System.Drawing.Color.Black;
            this.labelX25.Location = new System.Drawing.Point(600, 9);
            this.labelX25.Name = "labelX25";
            this.labelX25.Size = new System.Drawing.Size(42, 15);
            this.labelX25.TabIndex = 24;
            this.labelX25.Text = "Interior";
            // 
            // labelX22
            // 
            this.labelX22.AutoSize = true;
            this.labelX22.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX22.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX22.ForeColor = System.Drawing.Color.Black;
            this.labelX22.Location = new System.Drawing.Point(10, 10);
            this.labelX22.Name = "labelX22";
            this.labelX22.Size = new System.Drawing.Size(24, 15);
            this.labelX22.TabIndex = 18;
            this.labelX22.Text = "País";
            // 
            // Pais
            // 
            this.Pais.DisplayMember = "Text";
            this.Pais.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.Pais.ForeColor = System.Drawing.Color.Black;
            this.Pais.FormattingEnabled = true;
            this.Pais.ItemHeight = 16;
            this.Pais.Items.AddRange(new object[] {
            this.comboItem13,
            this.comboItem23,
            this.comboItem11,
            this.comboItem20,
            this.comboItem41});
            this.Pais.Location = new System.Drawing.Point(46, 6);
            this.Pais.Name = "Pais";
            this.Pais.Size = new System.Drawing.Size(78, 22);
            this.Pais.TabIndex = 19;
            this.Pais.Text = "USA";
            // 
            // comboItem13
            // 
            this.comboItem13.Text = "USA";
            // 
            // comboItem23
            // 
            this.comboItem23.Text = "CAN";
            // 
            // comboItem11
            // 
            this.comboItem11.Text = "JPN";
            // 
            // comboItem20
            // 
            this.comboItem20.Text = "MEX";
            // 
            // noExterior
            // 
            this.noExterior.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            // 
            // 
            // 
            this.noExterior.Border.Class = "TextBoxBorder";
            this.noExterior.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.noExterior.ForeColor = System.Drawing.Color.Black;
            this.noExterior.Location = new System.Drawing.Point(444, 6);
            this.noExterior.Name = "noExterior";
            this.noExterior.Size = new System.Drawing.Size(150, 20);
            this.noExterior.TabIndex = 23;
            this.noExterior.Text = "310";
            // 
            // labelX24
            // 
            this.labelX24.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX24.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX24.ForeColor = System.Drawing.Color.Black;
            this.labelX24.Location = new System.Drawing.Point(368, 9);
            this.labelX24.Name = "labelX24";
            this.labelX24.Size = new System.Drawing.Size(70, 15);
            this.labelX24.TabIndex = 22;
            this.labelX24.Text = "Exterior";
            // 
            // calle
            // 
            this.calle.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            // 
            // 
            // 
            this.calle.Border.Class = "TextBoxBorder";
            this.calle.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.calle.ForeColor = System.Drawing.Color.Black;
            this.calle.Location = new System.Drawing.Point(181, 6);
            this.calle.Name = "calle";
            this.calle.Size = new System.Drawing.Size(177, 20);
            this.calle.TabIndex = 21;
            this.calle.Text = "Pennintong Drive";
            // 
            // labelX23
            // 
            this.labelX23.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX23.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX23.ForeColor = System.Drawing.Color.Black;
            this.labelX23.Location = new System.Drawing.Point(131, 9);
            this.labelX23.Name = "labelX23";
            this.labelX23.Size = new System.Drawing.Size(44, 15);
            this.labelX23.TabIndex = 20;
            this.labelX23.Text = "Calle";
            // 
            // tabPage8
            // 
            this.tabPage8.Controls.Add(this.DestinatarioNombre);
            this.tabPage8.Controls.Add(this.DestinatarioNumRegIdTrib);
            this.tabPage8.Controls.Add(this.labelX39);
            this.tabPage8.Controls.Add(this.labelX38);
            this.tabPage8.Controls.Add(this.DestinatarioCP);
            this.tabPage8.Controls.Add(this.labelX9);
            this.tabPage8.Controls.Add(this.DestinatarioEstado);
            this.tabPage8.Controls.Add(this.labelX10);
            this.tabPage8.Controls.Add(this.DestinatarioColonia);
            this.tabPage8.Controls.Add(this.DestinatarioMunicipio);
            this.tabPage8.Controls.Add(this.labelX41);
            this.tabPage8.Controls.Add(this.labelX32);
            this.tabPage8.Controls.Add(this.DestinatarioInterior);
            this.tabPage8.Controls.Add(this.labelX34);
            this.tabPage8.Controls.Add(this.labelX35);
            this.tabPage8.Controls.Add(this.DestinatarioPais);
            this.tabPage8.Controls.Add(this.DestinatarioExterior);
            this.tabPage8.Controls.Add(this.labelX36);
            this.tabPage8.Controls.Add(this.DestinatarioLocalidad);
            this.tabPage8.Controls.Add(this.DestinatarioCalle);
            this.tabPage8.Controls.Add(this.labelX40);
            this.tabPage8.Controls.Add(this.labelX37);
            this.tabPage8.Location = new System.Drawing.Point(4, 22);
            this.tabPage8.Name = "tabPage8";
            this.tabPage8.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage8.Size = new System.Drawing.Size(832, 89);
            this.tabPage8.TabIndex = 1;
            this.tabPage8.Text = "Destinatario";
            this.tabPage8.UseVisualStyleBackColor = true;
            // 
            // DestinatarioNombre
            // 
            this.DestinatarioNombre.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            // 
            // 
            // 
            this.DestinatarioNombre.Border.Class = "TextBoxBorder";
            this.DestinatarioNombre.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.DestinatarioNombre.ForeColor = System.Drawing.Color.Black;
            this.DestinatarioNombre.Location = new System.Drawing.Point(74, 6);
            this.DestinatarioNombre.Name = "DestinatarioNombre";
            this.DestinatarioNombre.Size = new System.Drawing.Size(749, 20);
            this.DestinatarioNombre.TabIndex = 47;
            this.DestinatarioNombre.Text = "STARK MANUFACTURING LLC";
            // 
            // DestinatarioNumRegIdTrib
            // 
            this.DestinatarioNumRegIdTrib.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            // 
            // 
            // 
            this.DestinatarioNumRegIdTrib.Border.Class = "TextBoxBorder";
            this.DestinatarioNumRegIdTrib.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.DestinatarioNumRegIdTrib.ForeColor = System.Drawing.Color.Black;
            this.DestinatarioNumRegIdTrib.Location = new System.Drawing.Point(288, 31);
            this.DestinatarioNumRegIdTrib.Name = "DestinatarioNumRegIdTrib";
            this.DestinatarioNumRegIdTrib.Size = new System.Drawing.Size(110, 20);
            this.DestinatarioNumRegIdTrib.TabIndex = 47;
            this.DestinatarioNumRegIdTrib.Text = "050531673";
            // 
            // labelX39
            // 
            this.labelX39.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX39.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX39.ForeColor = System.Drawing.Color.Black;
            this.labelX39.Location = new System.Drawing.Point(6, 7);
            this.labelX39.Name = "labelX39";
            this.labelX39.Size = new System.Drawing.Size(157, 15);
            this.labelX39.TabIndex = 46;
            this.labelX39.Text = "Destinatario";
            // 
            // labelX38
            // 
            this.labelX38.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX38.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX38.ForeColor = System.Drawing.Color.Black;
            this.labelX38.Location = new System.Drawing.Point(125, 32);
            this.labelX38.Name = "labelX38";
            this.labelX38.Size = new System.Drawing.Size(157, 15);
            this.labelX38.TabIndex = 46;
            this.labelX38.Text = "Número de Registro Tributarío";
            // 
            // DestinatarioCP
            // 
            this.DestinatarioCP.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            // 
            // 
            // 
            this.DestinatarioCP.Border.Class = "TextBoxBorder";
            this.DestinatarioCP.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.DestinatarioCP.ForeColor = System.Drawing.Color.Black;
            this.DestinatarioCP.Location = new System.Drawing.Point(478, 58);
            this.DestinatarioCP.Name = "DestinatarioCP";
            this.DestinatarioCP.Size = new System.Drawing.Size(82, 20);
            this.DestinatarioCP.TabIndex = 43;
            this.DestinatarioCP.Text = "72855-0000";
            // 
            // labelX9
            // 
            this.labelX9.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX9.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX9.ForeColor = System.Drawing.Color.Black;
            this.labelX9.Location = new System.Drawing.Point(402, 63);
            this.labelX9.Name = "labelX9";
            this.labelX9.Size = new System.Drawing.Size(70, 15);
            this.labelX9.TabIndex = 42;
            this.labelX9.Text = "Código Postal";
            // 
            // DestinatarioEstado
            // 
            this.DestinatarioEstado.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            // 
            // 
            // 
            this.DestinatarioEstado.Border.Class = "TextBoxBorder";
            this.DestinatarioEstado.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.DestinatarioEstado.ForeColor = System.Drawing.Color.Black;
            this.DestinatarioEstado.Location = new System.Drawing.Point(298, 59);
            this.DestinatarioEstado.Name = "DestinatarioEstado";
            this.DestinatarioEstado.Size = new System.Drawing.Size(100, 20);
            this.DestinatarioEstado.TabIndex = 41;
            this.DestinatarioEstado.Text = "AR";
            // 
            // labelX10
            // 
            this.labelX10.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX10.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX10.ForeColor = System.Drawing.Color.Black;
            this.labelX10.Location = new System.Drawing.Point(257, 61);
            this.labelX10.Name = "labelX10";
            this.labelX10.Size = new System.Drawing.Size(44, 15);
            this.labelX10.TabIndex = 40;
            this.labelX10.Text = "Estado";
            // 
            // DestinatarioColonia
            // 
            this.DestinatarioColonia.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            // 
            // 
            // 
            this.DestinatarioColonia.Border.Class = "TextBoxBorder";
            this.DestinatarioColonia.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.DestinatarioColonia.ForeColor = System.Drawing.Color.Black;
            this.DestinatarioColonia.Location = new System.Drawing.Point(754, 58);
            this.DestinatarioColonia.Name = "DestinatarioColonia";
            this.DestinatarioColonia.Size = new System.Drawing.Size(69, 20);
            this.DestinatarioColonia.TabIndex = 45;
            // 
            // DestinatarioMunicipio
            // 
            this.DestinatarioMunicipio.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            // 
            // 
            // 
            this.DestinatarioMunicipio.Border.Class = "TextBoxBorder";
            this.DestinatarioMunicipio.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.DestinatarioMunicipio.ForeColor = System.Drawing.Color.Black;
            this.DestinatarioMunicipio.Location = new System.Drawing.Point(616, 58);
            this.DestinatarioMunicipio.Name = "DestinatarioMunicipio";
            this.DestinatarioMunicipio.Size = new System.Drawing.Size(86, 20);
            this.DestinatarioMunicipio.TabIndex = 45;
            // 
            // labelX41
            // 
            this.labelX41.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX41.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX41.ForeColor = System.Drawing.Color.Black;
            this.labelX41.Location = new System.Drawing.Point(708, 61);
            this.labelX41.Name = "labelX41";
            this.labelX41.Size = new System.Drawing.Size(48, 15);
            this.labelX41.TabIndex = 44;
            this.labelX41.Text = "Colonia";
            // 
            // labelX32
            // 
            this.labelX32.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX32.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX32.ForeColor = System.Drawing.Color.Black;
            this.labelX32.Location = new System.Drawing.Point(565, 61);
            this.labelX32.Name = "labelX32";
            this.labelX32.Size = new System.Drawing.Size(48, 15);
            this.labelX32.TabIndex = 44;
            this.labelX32.Text = "Municipio";
            // 
            // DestinatarioInterior
            // 
            this.DestinatarioInterior.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            // 
            // 
            // 
            this.DestinatarioInterior.Border.Class = "TextBoxBorder";
            this.DestinatarioInterior.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.DestinatarioInterior.ForeColor = System.Drawing.Color.Black;
            this.DestinatarioInterior.Location = new System.Drawing.Point(186, 58);
            this.DestinatarioInterior.Name = "DestinatarioInterior";
            this.DestinatarioInterior.Size = new System.Drawing.Size(65, 20);
            this.DestinatarioInterior.TabIndex = 39;
            // 
            // labelX34
            // 
            this.labelX34.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX34.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX34.ForeColor = System.Drawing.Color.Black;
            this.labelX34.Location = new System.Drawing.Point(138, 61);
            this.labelX34.Name = "labelX34";
            this.labelX34.Size = new System.Drawing.Size(42, 15);
            this.labelX34.TabIndex = 38;
            this.labelX34.Text = "Interior";
            // 
            // labelX35
            // 
            this.labelX35.AutoSize = true;
            this.labelX35.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX35.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX35.ForeColor = System.Drawing.Color.Black;
            this.labelX35.Location = new System.Drawing.Point(10, 32);
            this.labelX35.Name = "labelX35";
            this.labelX35.Size = new System.Drawing.Size(24, 15);
            this.labelX35.TabIndex = 32;
            this.labelX35.Text = "País";
            // 
            // DestinatarioPais
            // 
            this.DestinatarioPais.DisplayMember = "Text";
            this.DestinatarioPais.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.DestinatarioPais.ForeColor = System.Drawing.Color.Black;
            this.DestinatarioPais.FormattingEnabled = true;
            this.DestinatarioPais.ItemHeight = 16;
            this.DestinatarioPais.Items.AddRange(new object[] {
            this.comboItem21,
            this.comboItem22,
            this.comboItem39,
            this.comboItem40});
            this.DestinatarioPais.Location = new System.Drawing.Point(46, 28);
            this.DestinatarioPais.Name = "DestinatarioPais";
            this.DestinatarioPais.Size = new System.Drawing.Size(73, 22);
            this.DestinatarioPais.TabIndex = 33;
            this.DestinatarioPais.Text = "USA";
            // 
            // comboItem21
            // 
            this.comboItem21.Text = "USA";
            // 
            // comboItem22
            // 
            this.comboItem22.Text = "CAN";
            // 
            // comboItem39
            // 
            this.comboItem39.Text = "JPN";
            // 
            // comboItem40
            // 
            this.comboItem40.Text = "MEX";
            // 
            // DestinatarioExterior
            // 
            this.DestinatarioExterior.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            // 
            // 
            // 
            this.DestinatarioExterior.Border.Class = "TextBoxBorder";
            this.DestinatarioExterior.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.DestinatarioExterior.ForeColor = System.Drawing.Color.Black;
            this.DestinatarioExterior.Location = new System.Drawing.Point(58, 58);
            this.DestinatarioExterior.Name = "DestinatarioExterior";
            this.DestinatarioExterior.Size = new System.Drawing.Size(74, 20);
            this.DestinatarioExterior.TabIndex = 37;
            // 
            // labelX36
            // 
            this.labelX36.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX36.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX36.ForeColor = System.Drawing.Color.Black;
            this.labelX36.Location = new System.Drawing.Point(8, 61);
            this.labelX36.Name = "labelX36";
            this.labelX36.Size = new System.Drawing.Size(70, 15);
            this.labelX36.TabIndex = 36;
            this.labelX36.Text = "Exterior";
            // 
            // DestinatarioLocalidad
            // 
            this.DestinatarioLocalidad.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            // 
            // 
            // 
            this.DestinatarioLocalidad.Border.Class = "TextBoxBorder";
            this.DestinatarioLocalidad.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.DestinatarioLocalidad.ForeColor = System.Drawing.Color.Black;
            this.DestinatarioLocalidad.Location = new System.Drawing.Point(754, 30);
            this.DestinatarioLocalidad.Name = "DestinatarioLocalidad";
            this.DestinatarioLocalidad.Size = new System.Drawing.Size(69, 20);
            this.DestinatarioLocalidad.TabIndex = 35;
            // 
            // DestinatarioCalle
            // 
            this.DestinatarioCalle.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            // 
            // 
            // 
            this.DestinatarioCalle.Border.Class = "TextBoxBorder";
            this.DestinatarioCalle.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.DestinatarioCalle.ForeColor = System.Drawing.Color.Black;
            this.DestinatarioCalle.Location = new System.Drawing.Point(443, 30);
            this.DestinatarioCalle.Name = "DestinatarioCalle";
            this.DestinatarioCalle.Size = new System.Drawing.Size(243, 20);
            this.DestinatarioCalle.TabIndex = 35;
            this.DestinatarioCalle.Text = "Pennintong Drive";
            // 
            // labelX40
            // 
            this.labelX40.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX40.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX40.ForeColor = System.Drawing.Color.Black;
            this.labelX40.Location = new System.Drawing.Point(692, 33);
            this.labelX40.Name = "labelX40";
            this.labelX40.Size = new System.Drawing.Size(56, 15);
            this.labelX40.TabIndex = 34;
            this.labelX40.Text = "Localidad";
            // 
            // labelX37
            // 
            this.labelX37.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX37.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX37.ForeColor = System.Drawing.Color.Black;
            this.labelX37.Location = new System.Drawing.Point(407, 33);
            this.labelX37.Name = "labelX37";
            this.labelX37.Size = new System.Drawing.Size(30, 15);
            this.labelX37.TabIndex = 34;
            this.labelX37.Text = "Calle";
            // 
            // NumRegIdTrib
            // 
            this.NumRegIdTrib.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            // 
            // 
            // 
            this.NumRegIdTrib.Border.Class = "TextBoxBorder";
            this.NumRegIdTrib.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.NumRegIdTrib.ForeColor = System.Drawing.Color.Black;
            this.NumRegIdTrib.Location = new System.Drawing.Point(318, 24);
            this.NumRegIdTrib.Name = "NumRegIdTrib";
            this.NumRegIdTrib.Size = new System.Drawing.Size(120, 20);
            this.NumRegIdTrib.TabIndex = 3;
            this.NumRegIdTrib.Text = "050531673";
            this.NumRegIdTrib.Leave += new System.EventHandler(this.NumRegIdTrib_Leave);
            // 
            // Incoterm
            // 
            this.Incoterm.DisplayMember = "Text";
            this.Incoterm.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.Incoterm.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Incoterm.ForeColor = System.Drawing.Color.Black;
            this.Incoterm.FormattingEnabled = true;
            this.Incoterm.ItemHeight = 16;
            this.Incoterm.Items.AddRange(new object[] {
            this.comboItem24,
            this.comboItem25,
            this.comboItem26,
            this.comboItem27,
            this.comboItem28,
            this.comboItem29,
            this.comboItem30,
            this.comboItem31,
            this.comboItem32,
            this.comboItem33,
            this.comboItem34,
            this.comboItem35,
            this.comboItem36,
            this.comboItem37,
            this.comboItem38});
            this.Incoterm.Location = new System.Drawing.Point(55, 23);
            this.Incoterm.Name = "Incoterm";
            this.Incoterm.Size = new System.Drawing.Size(78, 22);
            this.Incoterm.TabIndex = 1;
            // 
            // comboItem24
            // 
            this.comboItem24.Text = "CFR";
            // 
            // comboItem25
            // 
            this.comboItem25.Text = "CIF";
            // 
            // comboItem26
            // 
            this.comboItem26.Text = "CPT";
            // 
            // comboItem27
            // 
            this.comboItem27.Text = "CIP";
            // 
            // comboItem28
            // 
            this.comboItem28.Text = "DAF";
            // 
            // comboItem29
            // 
            this.comboItem29.Text = "DAP";
            // 
            // comboItem30
            // 
            this.comboItem30.Text = "DAT\r\n";
            // 
            // comboItem31
            // 
            this.comboItem31.Text = "DES";
            // 
            // comboItem32
            // 
            this.comboItem32.Text = "DEQ";
            // 
            // comboItem33
            // 
            this.comboItem33.Text = "DDU";
            // 
            // comboItem34
            // 
            this.comboItem34.Text = "DDP";
            // 
            // comboItem35
            // 
            this.comboItem35.Text = "EXW";
            // 
            // comboItem36
            // 
            this.comboItem36.Text = "FCA";
            // 
            // comboItem37
            // 
            this.comboItem37.Text = "FAS";
            // 
            // comboItem38
            // 
            this.comboItem38.Text = "FOB";
            // 
            // labelX21
            // 
            this.labelX21.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX21.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX21.ForeColor = System.Drawing.Color.Black;
            this.labelX21.Location = new System.Drawing.Point(155, 26);
            this.labelX21.Name = "labelX21";
            this.labelX21.Size = new System.Drawing.Size(157, 15);
            this.labelX21.TabIndex = 2;
            this.labelX21.Text = "Número de Registro Tributarío";
            // 
            // labelX20
            // 
            this.labelX20.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX20.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX20.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelX20.ForeColor = System.Drawing.Color.Black;
            this.labelX20.Location = new System.Drawing.Point(5, 26);
            this.labelX20.Name = "labelX20";
            this.labelX20.Size = new System.Drawing.Size(44, 17);
            this.labelX20.TabIndex = 0;
            this.labelX20.Text = "Incoterm";
            // 
            // activarCE
            // 
            this.activarCE.AutoSize = true;
            this.activarCE.Checked = true;
            this.activarCE.CheckState = System.Windows.Forms.CheckState.Checked;
            this.activarCE.Location = new System.Drawing.Point(3, 3);
            this.activarCE.Name = "activarCE";
            this.activarCE.Size = new System.Drawing.Size(231, 17);
            this.activarCE.TabIndex = 0;
            this.activarCE.Text = "Activar complemento de Comercion Exterior";
            this.activarCE.UseVisualStyleBackColor = true;
            // 
            // tabPage5
            // 
            this.tabPage5.Controls.Add(this.label23);
            this.tabPage5.Controls.Add(this.PARA);
            this.tabPage5.Controls.Add(this.ASUNTO);
            this.tabPage5.Controls.Add(this.MENSAJE);
            this.tabPage5.Controls.Add(this.label18);
            this.tabPage5.Controls.Add(this.label17);
            this.tabPage5.Controls.Add(this.buttonX18);
            this.tabPage5.Location = new System.Drawing.Point(4, 22);
            this.tabPage5.Name = "tabPage5";
            this.tabPage5.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage5.Size = new System.Drawing.Size(853, 178);
            this.tabPage5.TabIndex = 4;
            this.tabPage5.Text = "Enviar Correo";
            this.tabPage5.UseVisualStyleBackColor = true;
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Location = new System.Drawing.Point(6, 30);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(29, 13);
            this.label23.TabIndex = 621;
            this.label23.Text = "Para";
            // 
            // PARA
            // 
            this.PARA.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.PARA.Location = new System.Drawing.Point(67, 27);
            this.PARA.Name = "PARA";
            this.PARA.Size = new System.Drawing.Size(784, 20);
            this.PARA.TabIndex = 620;
            // 
            // ASUNTO
            // 
            this.ASUNTO.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.ASUNTO.Location = new System.Drawing.Point(67, 4);
            this.ASUNTO.Name = "ASUNTO";
            this.ASUNTO.Size = new System.Drawing.Size(784, 20);
            this.ASUNTO.TabIndex = 617;
            // 
            // MENSAJE
            // 
            this.MENSAJE.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.MENSAJE.Location = new System.Drawing.Point(67, 56);
            this.MENSAJE.Name = "MENSAJE";
            this.MENSAJE.Size = new System.Drawing.Size(784, 136);
            this.MENSAJE.TabIndex = 622;
            this.MENSAJE.Text = "";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(6, 7);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(40, 13);
            this.label18.TabIndex = 619;
            this.label18.Text = "Asunto";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(6, 56);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(47, 13);
            this.label17.TabIndex = 618;
            this.label17.Text = "Mensaje";
            // 
            // buttonX18
            // 
            this.buttonX18.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.buttonX18.BackColor = System.Drawing.Color.Navy;
            this.buttonX18.ColorTable = DevComponents.DotNetBar.eButtonColor.Flat;
            this.buttonX18.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonX18.Location = new System.Drawing.Point(6, 78);
            this.buttonX18.Name = "buttonX18";
            this.buttonX18.Size = new System.Drawing.Size(55, 22);
            this.buttonX18.Style = DevComponents.DotNetBar.eDotNetBarStyle.OfficeXP;
            this.buttonX18.TabIndex = 616;
            this.buttonX18.Text = "Enviar";
            this.buttonX18.Click += new System.EventHandler(this.buttonX18_Click_1);
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.dtgrdGeneral);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(853, 178);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Líneas";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // dtgrdGeneral
            // 
            this.dtgrdGeneral.AllowUserToAddRows = false;
            this.dtgrdGeneral.AllowUserToDeleteRows = false;
            this.dtgrdGeneral.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dtgrdGeneral.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.dtgrdGeneral.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dtgrdGeneral.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.dtgrdGeneral.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dtgrdGeneral.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.cantidad,
            this.unidad,
            this.descripcion,
            this.noIdentificacion,
            this.valorUnitario,
            this.importe,
            this.fraccionArancelaria,
            this.NumeroPedimentoInformacionAduanera,
            this.informacionAduaneraFecha,
            this.informacionAduaneraAduana,
            this.informacionAduaneraNumero,
            this.ClaveProdServ,
            this.ClaveUnidad,
            this.Submodelo,
            this.Marca,
            this.Modelo,
            this.NumeroSerie,
            this.CantidadAduana,
            this.ValorUnitarioAduana,
            this.ValorDolares,
            this.NumeroPedimento});
            dataGridViewCellStyle7.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle7.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            dataGridViewCellStyle7.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle7.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle7.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle7.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle7.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dtgrdGeneral.DefaultCellStyle = dataGridViewCellStyle7;
            this.dtgrdGeneral.EnableHeadersVisualStyles = false;
            this.dtgrdGeneral.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(208)))), ((int)(((byte)(215)))), ((int)(((byte)(229)))));
            this.dtgrdGeneral.Location = new System.Drawing.Point(3, 3);
            this.dtgrdGeneral.Name = "dtgrdGeneral";
            this.dtgrdGeneral.ReadOnly = true;
            dataGridViewCellStyle8.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle8.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle8.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle8.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle8.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle8.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle8.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dtgrdGeneral.RowHeadersDefaultCellStyle = dataGridViewCellStyle8;
            this.dtgrdGeneral.RowHeadersVisible = false;
            this.dtgrdGeneral.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dtgrdGeneral.Size = new System.Drawing.Size(844, 155);
            this.dtgrdGeneral.TabIndex = 40;
            this.dtgrdGeneral.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dtgrdGeneral_CellDoubleClick_1);
            this.dtgrdGeneral.MouseClick += new System.Windows.Forms.MouseEventHandler(this.dtgrdGeneral_MouseClick);
            this.dtgrdGeneral.MouseLeave += new System.EventHandler(this.dtgrdGeneral_MouseLeave);
            // 
            // cantidad
            // 
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.cantidad.DefaultCellStyle = dataGridViewCellStyle2;
            this.cantidad.HeaderText = "Cantidad";
            this.cantidad.Name = "cantidad";
            this.cantidad.ReadOnly = true;
            this.cantidad.Width = 60;
            // 
            // unidad
            // 
            this.unidad.HeaderText = "Unidad";
            this.unidad.Name = "unidad";
            this.unidad.ReadOnly = true;
            this.unidad.Width = 60;
            // 
            // descripcion
            // 
            this.descripcion.HeaderText = "Descripción";
            this.descripcion.Name = "descripcion";
            this.descripcion.ReadOnly = true;
            this.descripcion.Width = 240;
            // 
            // noIdentificacion
            // 
            this.noIdentificacion.HeaderText = "No Identificacion";
            this.noIdentificacion.Name = "noIdentificacion";
            this.noIdentificacion.ReadOnly = true;
            this.noIdentificacion.Width = 120;
            // 
            // valorUnitario
            // 
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.valorUnitario.DefaultCellStyle = dataGridViewCellStyle3;
            this.valorUnitario.HeaderText = "Valor Unitario";
            this.valorUnitario.Name = "valorUnitario";
            this.valorUnitario.ReadOnly = true;
            this.valorUnitario.Width = 110;
            // 
            // importe
            // 
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.importe.DefaultCellStyle = dataGridViewCellStyle4;
            this.importe.HeaderText = "Importe";
            this.importe.Name = "importe";
            this.importe.ReadOnly = true;
            // 
            // fraccionArancelaria
            // 
            this.fraccionArancelaria.HeaderText = "Fracción";
            this.fraccionArancelaria.Name = "fraccionArancelaria";
            this.fraccionArancelaria.ReadOnly = true;
            // 
            // NumeroPedimentoInformacionAduanera
            // 
            this.NumeroPedimentoInformacionAduanera.HeaderText = "Información Aduanera";
            this.NumeroPedimentoInformacionAduanera.Name = "NumeroPedimentoInformacionAduanera";
            this.NumeroPedimentoInformacionAduanera.ReadOnly = true;
            this.NumeroPedimentoInformacionAduanera.Width = 150;
            // 
            // informacionAduaneraFecha
            // 
            this.informacionAduaneraFecha.HeaderText = "Fecha";
            this.informacionAduaneraFecha.Name = "informacionAduaneraFecha";
            this.informacionAduaneraFecha.ReadOnly = true;
            // 
            // informacionAduaneraAduana
            // 
            this.informacionAduaneraAduana.HeaderText = "Aduana";
            this.informacionAduaneraAduana.Name = "informacionAduaneraAduana";
            this.informacionAduaneraAduana.ReadOnly = true;
            // 
            // informacionAduaneraNumero
            // 
            this.informacionAduaneraNumero.HeaderText = "Número";
            this.informacionAduaneraNumero.Name = "informacionAduaneraNumero";
            this.informacionAduaneraNumero.ReadOnly = true;
            // 
            // ClaveProdServ
            // 
            this.ClaveProdServ.HeaderText = "ClaveProdServ";
            this.ClaveProdServ.Name = "ClaveProdServ";
            this.ClaveProdServ.ReadOnly = true;
            // 
            // ClaveUnidad
            // 
            this.ClaveUnidad.HeaderText = "ClaveUnidad";
            this.ClaveUnidad.Name = "ClaveUnidad";
            this.ClaveUnidad.ReadOnly = true;
            // 
            // Submodelo
            // 
            this.Submodelo.HeaderText = "Submodelo";
            this.Submodelo.Name = "Submodelo";
            this.Submodelo.ReadOnly = true;
            // 
            // Marca
            // 
            this.Marca.HeaderText = "Marca";
            this.Marca.Name = "Marca";
            this.Marca.ReadOnly = true;
            // 
            // Modelo
            // 
            this.Modelo.HeaderText = "Modelo";
            this.Modelo.Name = "Modelo";
            this.Modelo.ReadOnly = true;
            // 
            // NumeroSerie
            // 
            this.NumeroSerie.HeaderText = "NumeroSerie";
            this.NumeroSerie.Name = "NumeroSerie";
            this.NumeroSerie.ReadOnly = true;
            // 
            // CantidadAduana
            // 
            this.CantidadAduana.HeaderText = "Cantidad Aduana";
            this.CantidadAduana.Name = "CantidadAduana";
            this.CantidadAduana.ReadOnly = true;
            this.CantidadAduana.Width = 130;
            // 
            // ValorUnitarioAduana
            // 
            dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.ValorUnitarioAduana.DefaultCellStyle = dataGridViewCellStyle5;
            this.ValorUnitarioAduana.HeaderText = "Valor Unitario Aduana";
            this.ValorUnitarioAduana.Name = "ValorUnitarioAduana";
            this.ValorUnitarioAduana.ReadOnly = true;
            this.ValorUnitarioAduana.Width = 150;
            // 
            // ValorDolares
            // 
            dataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.ValorDolares.DefaultCellStyle = dataGridViewCellStyle6;
            this.ValorDolares.HeaderText = "Valor Dolares";
            this.ValorDolares.Name = "ValorDolares";
            this.ValorDolares.ReadOnly = true;
            // 
            // NumeroPedimento
            // 
            this.NumeroPedimento.HeaderText = "Número Pedimento";
            this.NumeroPedimento.Name = "NumeroPedimento";
            this.NumeroPedimento.ReadOnly = true;
            this.NumeroPedimento.Width = 200;
            // 
            // tabControl1
            // 
            this.tabControl1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage5);
            this.tabControl1.Controls.Add(this.tabPage3);
            this.tabControl1.Controls.Add(this.tabPage4);
            this.tabControl1.Controls.Add(this.tabPage6);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Controls.Add(this.tabPage9);
            this.tabControl1.Controls.Add(this.tabPage10);
            this.tabControl1.Controls.Add(this.tabPage11);
            this.tabControl1.Location = new System.Drawing.Point(13, 195);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(861, 204);
            this.tabControl1.TabIndex = 604;
            // 
            // tabPage11
            // 
            this.tabPage11.Controls.Add(this.tipoRelacion);
            this.tabPage11.Controls.Add(this.label2);
            this.tabPage11.Controls.Add(this.button5);
            this.tabPage11.Controls.Add(this.button6);
            this.tabPage11.Controls.Add(this.DtgRelacion);
            this.tabPage11.Location = new System.Drawing.Point(4, 22);
            this.tabPage11.Name = "tabPage11";
            this.tabPage11.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage11.Size = new System.Drawing.Size(853, 178);
            this.tabPage11.TabIndex = 9;
            this.tabPage11.Text = "Relacionar";
            this.tabPage11.UseVisualStyleBackColor = true;
            // 
            // tipoRelacion
            // 
            this.tipoRelacion.FormattingEnabled = true;
            this.tipoRelacion.Location = new System.Drawing.Point(104, 35);
            this.tipoRelacion.Name = "tipoRelacion";
            this.tipoRelacion.Size = new System.Drawing.Size(546, 21);
            this.tipoRelacion.TabIndex = 12;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(13, 39);
            this.label2.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(83, 13);
            this.label2.TabIndex = 11;
            this.label2.Text = "Tipo de relación";
            // 
            // button5
            // 
            this.button5.BackColor = System.Drawing.Color.Tomato;
            this.button5.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button5.Location = new System.Drawing.Point(89, 6);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(75, 23);
            this.button5.TabIndex = 10;
            this.button5.Text = "Eliminar";
            this.button5.UseVisualStyleBackColor = false;
            this.button5.Click += new System.EventHandler(this.button5_Click);
            // 
            // button6
            // 
            this.button6.BackColor = System.Drawing.Color.YellowGreen;
            this.button6.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button6.Location = new System.Drawing.Point(8, 6);
            this.button6.Name = "button6";
            this.button6.Size = new System.Drawing.Size(75, 23);
            this.button6.TabIndex = 9;
            this.button6.Text = "Agregar";
            this.button6.UseVisualStyleBackColor = false;
            this.button6.Click += new System.EventHandler(this.button6_Click);
            // 
            // DtgRelacion
            // 
            this.DtgRelacion.AllowUserToAddRows = false;
            this.DtgRelacion.AllowUserToDeleteRows = false;
            this.DtgRelacion.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DtgRelacion.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.UUIDRelacionar});
            this.DtgRelacion.Location = new System.Drawing.Point(8, 62);
            this.DtgRelacion.Name = "DtgRelacion";
            this.DtgRelacion.Size = new System.Drawing.Size(839, 110);
            this.DtgRelacion.TabIndex = 0;
            // 
            // UUIDRelacionar
            // 
            this.UUIDRelacionar.HeaderText = "Folio Fiscal";
            this.UUIDRelacionar.Name = "UUIDRelacionar";
            this.UUIDRelacionar.Width = 350;
            // 
            // IgualarEmisor
            // 
            this.IgualarEmisor.AutoSize = true;
            this.IgualarEmisor.Checked = true;
            this.IgualarEmisor.CheckState = System.Windows.Forms.CheckState.Checked;
            this.IgualarEmisor.Location = new System.Drawing.Point(757, 146);
            this.IgualarEmisor.Name = "IgualarEmisor";
            this.IgualarEmisor.Size = new System.Drawing.Size(113, 17);
            this.IgualarEmisor.TabIndex = 625;
            this.IgualarEmisor.Text = "Igualar con Emisor";
            this.IgualarEmisor.UseVisualStyleBackColor = true;
            // 
            // comboItem41
            // 
            this.comboItem41.Text = "KOR";
            // 
            // frmTraslado
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(885, 618);
            this.Controls.Add(this.IgualarEmisor);
            this.Controls.Add(this.button4);
            this.Controls.Add(this.button3);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.USO_CFDI);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.motivoTraslado);
            this.Controls.Add(this.labelX16);
            this.Controls.Add(this.buttonX19);
            this.Controls.Add(this.buttonX16);
            this.Controls.Add(this.buttonX12);
            this.Controls.Add(this.tabControl1);
            this.Controls.Add(this.FORMATO_IMPRESION);
            this.Controls.Add(this.ENTITY_ID);
            this.Controls.Add(this.buttonX17);
            this.Controls.Add(this.receptor);
            this.Controls.Add(this.subdivision);
            this.Controls.Add(this.rfc);
            this.Controls.Add(this.labelX19);
            this.Controls.Add(this.labelX13);
            this.Controls.Add(this.PATH);
            this.Controls.Add(this.UUID);
            this.Controls.Add(this.labelX7);
            this.Controls.Add(this.labelX15);
            this.Controls.Add(this.btnEmisor);
            this.Controls.Add(this.buttonX10);
            this.Controls.Add(this.buttonX11);
            this.Controls.Add(this.buttonX9);
            this.Controls.Add(this.buttonX7);
            this.Controls.Add(this.buttonX5);
            this.Controls.Add(this.buttonX2);
            this.Controls.Add(this.labelX11);
            this.Controls.Add(this.EMPRESA_NOMBRE);
            this.Controls.Add(this.labelX14);
            this.Controls.Add(this.labelX12);
            this.Controls.Add(this.MONEDA);
            this.Controls.Add(this.FECHA);
            this.Controls.Add(this.ESTADO);
            this.Controls.Add(this.SERIE);
            this.Controls.Add(this.TIPO_CAMBIO);
            this.Controls.Add(this.labelX6);
            this.Controls.Add(this.buttonX1);
            this.Controls.Add(this.ID);
            this.Controls.Add(this.statusStrip1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.KeyPreview = true;
            this.MaximizeBox = false;
            this.Name = "frmTraslado";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Traslado CFDI 4.0";
            this.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.frmFormatos_KeyPress);
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.contextMenuStrip1.ResumeLayout(false);
            this.tabPage10.ResumeLayout(false);
            this.tabPage10.PerformLayout();
            this.tabPage9.ResumeLayout(false);
            this.tabPage9.PerformLayout();
            this.tabPage2.ResumeLayout(false);
            this.tabPage6.ResumeLayout(false);
            this.tabPage6.PerformLayout();
            this.tabPage4.ResumeLayout(false);
            this.tabPage4.PerformLayout();
            this.tabPage3.ResumeLayout(false);
            this.tabPage3.PerformLayout();
            this.tabControl2.ResumeLayout(false);
            this.tabPage7.ResumeLayout(false);
            this.tabPage7.PerformLayout();
            this.tabPage8.ResumeLayout(false);
            this.tabPage8.PerformLayout();
            this.tabPage5.ResumeLayout(false);
            this.tabPage5.PerformLayout();
            this.tabPage1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dtgrdGeneral)).EndInit();
            this.tabControl1.ResumeLayout(false);
            this.tabPage11.ResumeLayout(false);
            this.tabPage11.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DtgRelacion)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStripStatusLabel informacion;
        private DevComponents.Editors.ComboItem comboItem4;
        private DevComponents.Editors.ComboItem comboItem5;
        private DevComponents.Editors.ComboItem comboItem6;
        private DevComponents.DotNetBar.ButtonX buttonX17;
        private DevComponents.DotNetBar.LabelX labelX13;
        private DevComponents.DotNetBar.Controls.TextBoxX UUID;
        private DevComponents.DotNetBar.ButtonX btnEmisor;
        private DevComponents.DotNetBar.ButtonX buttonX10;
        private DevComponents.DotNetBar.ButtonX buttonX11;
        private DevComponents.DotNetBar.ButtonX buttonX9;
        private DevComponents.DotNetBar.ButtonX buttonX7;
        private DevComponents.DotNetBar.ButtonX buttonX5;
        private DevComponents.DotNetBar.ButtonX buttonX2;
        private DevComponents.DotNetBar.LabelX labelX12;
        private DevComponents.DotNetBar.Controls.ComboBoxEx MONEDA;
        private DevComponents.DotNetBar.LabelX labelX8;
        private System.Windows.Forms.DateTimePicker FECHA;
        private DevComponents.DotNetBar.Controls.TextBoxX OBSERVACIONES;
        private DevComponents.DotNetBar.Controls.ComboBoxEx ESTADO;
        private DevComponents.Editors.ComboItem comboItem3;
        private DevComponents.Editors.ComboItem comboItem1;
        private DevComponents.Editors.ComboItem comboItem2;
        private DevComponents.DotNetBar.Controls.TextBoxX SERIE;
        private DevComponents.DotNetBar.Controls.TextBoxX TIPO_CAMBIO;
        private DevComponents.DotNetBar.LabelX labelX6;
        private DevComponents.DotNetBar.Controls.ComboBoxEx IMPUESTO_DATO;
        private DevComponents.Editors.ComboItem comboItem7;
        private DevComponents.Editors.ComboItem comboItem8;
        private DevComponents.Editors.ComboItem comboItem9;
        private DevComponents.Editors.ComboItem comboItem10;
        private DevComponents.Editors.ComboItem comboItem12;
        private DevComponents.DotNetBar.ButtonX buttonX1;
        private DevComponents.DotNetBar.Controls.TextBoxX ID;
        private DevComponents.Editors.ComboItem comboItem14;
        private DevComponents.Editors.ComboItem comboItem15;
        private DevComponents.Editors.ComboItem comboItem16;
        private DevComponents.DotNetBar.Controls.TextBoxX IMPUESTO;
        private DevComponents.DotNetBar.Controls.TextBoxX RETENCION;
        private DevComponents.DotNetBar.LabelX labelX4;
        private DevComponents.DotNetBar.Controls.TextBoxX TOTAL;
        private DevComponents.DotNetBar.LabelX labelX3;
        private DevComponents.DotNetBar.LabelX labelX2;
        private DevComponents.DotNetBar.Controls.TextBoxX SUBTOTAL;
        private DevComponents.DotNetBar.LabelX labelX1;
        private DevComponents.DotNetBar.Controls.ComboBoxEx ENTITY_ID;
        private DevComponents.Editors.ComboItem comboItem17;
        private DevComponents.Editors.ComboItem comboItem18;
        private DevComponents.Editors.ComboItem comboItem19;
        private DevComponents.DotNetBar.LabelX labelX11;
        private DevComponents.DotNetBar.LabelX EMPRESA_NOMBRE;
        private DevComponents.DotNetBar.LabelX labelX14;
        private DevComponents.DotNetBar.Controls.TextBoxX rfc;
        private DevComponents.DotNetBar.LabelX labelX15;
        private DevComponents.DotNetBar.Controls.TextBoxX IEPStr;
        private DevComponents.DotNetBar.LabelX labelX17;
        private DevComponents.DotNetBar.LabelX labelX18;
        private DevComponents.DotNetBar.Controls.TextBoxX DESCUENTO;
        private DevComponents.DotNetBar.Controls.TextBoxX PATH;
        private DevComponents.DotNetBar.LabelX labelX19;
        private System.Windows.Forms.FolderBrowserDialog folderBrowserDialog1;
        private DevComponents.DotNetBar.Controls.ComboBoxEx FORMATO_IMPRESION;
        private DevComponents.DotNetBar.ButtonX buttonX12;
        private DevComponents.DotNetBar.Controls.TextBoxX motivoDescuento;
        private DevComponents.DotNetBar.LabelX labelX28;
        private DevComponents.DotNetBar.Controls.TextBoxX notasPDF;
        private DevComponents.DotNetBar.LabelX labelX30;
        private DevComponents.DotNetBar.LabelX labelX31;
        private DevComponents.DotNetBar.Controls.TextBoxX totalSinDescuento;
        private DevComponents.DotNetBar.ButtonX buttonX16;
        private DevComponents.DotNetBar.ButtonX buttonX19;
        private DevComponents.DotNetBar.Controls.TextBoxX receptor;
        private DevComponents.DotNetBar.LabelX labelX16;
        private System.Windows.Forms.ComboBox motivoTraslado;
        private System.Windows.Forms.ComboBox USO_CFDI;
        private System.Windows.Forms.Label label7;
        private DevComponents.DotNetBar.Controls.TextBoxX subdivision;
        private DevComponents.DotNetBar.LabelX labelX7;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip1;
        private System.Windows.Forms.ToolStripMenuItem pegarDatosDesdeLíneaDeExcelToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem pegarDatosParaComercioExteriorDesdePlantillaDeExcelToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem copiarDatosDelReceptorAlDestinatarioToolStripMenuItem;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.TabPage tabPage10;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TabPage tabPage9;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TextBox RegimenFiscalReceptor;
        private System.Windows.Forms.TextBox DomicilioFiscalReceptor;
        private System.Windows.Forms.TabPage tabPage2;
        private DevComponents.DotNetBar.Controls.TextBoxX errorTimbrado;
        private System.Windows.Forms.TabPage tabPage6;
        private DevComponents.DotNetBar.LabelX labelX33;
        private DevComponents.DotNetBar.Controls.TextBoxX facturarA;
        private System.Windows.Forms.TabPage tabPage4;
        private DevComponents.DotNetBar.LabelX labelX29;
        private DevComponents.DotNetBar.Controls.TextBoxX direccionEmbarque;
        private System.Windows.Forms.TabPage tabPage3;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.TabControl tabControl2;
        private System.Windows.Forms.TabPage tabPage7;
        private DevComponents.DotNetBar.Controls.TextBoxX ReceptorLocalidad;
        private DevComponents.DotNetBar.LabelX labelX44;
        private DevComponents.DotNetBar.Controls.TextBoxX ReceptorColonia;
        private DevComponents.DotNetBar.Controls.TextBoxX ReceptorMunicipio;
        private DevComponents.DotNetBar.LabelX labelX42;
        private DevComponents.DotNetBar.LabelX labelX43;
        private DevComponents.DotNetBar.Controls.TextBoxX CodigoPostal;
        private DevComponents.DotNetBar.LabelX labelX26;
        private DevComponents.DotNetBar.Controls.TextBoxX EstadoCE;
        private DevComponents.DotNetBar.LabelX labelX27;
        private DevComponents.DotNetBar.Controls.TextBoxX Ciudad;
        private DevComponents.DotNetBar.LabelX labelX5;
        private DevComponents.DotNetBar.Controls.TextBoxX noInterior;
        private DevComponents.DotNetBar.LabelX labelX25;
        private DevComponents.DotNetBar.LabelX labelX22;
        private DevComponents.DotNetBar.Controls.ComboBoxEx Pais;
        private DevComponents.Editors.ComboItem comboItem13;
        private DevComponents.Editors.ComboItem comboItem23;
        private DevComponents.Editors.ComboItem comboItem11;
        private DevComponents.Editors.ComboItem comboItem20;
        private DevComponents.DotNetBar.Controls.TextBoxX noExterior;
        private DevComponents.DotNetBar.LabelX labelX24;
        private DevComponents.DotNetBar.Controls.TextBoxX calle;
        private DevComponents.DotNetBar.LabelX labelX23;
        private System.Windows.Forms.TabPage tabPage8;
        private DevComponents.DotNetBar.Controls.TextBoxX DestinatarioNombre;
        private DevComponents.DotNetBar.Controls.TextBoxX DestinatarioNumRegIdTrib;
        private DevComponents.DotNetBar.LabelX labelX39;
        private DevComponents.DotNetBar.LabelX labelX38;
        private DevComponents.DotNetBar.Controls.TextBoxX DestinatarioCP;
        private DevComponents.DotNetBar.LabelX labelX9;
        private DevComponents.DotNetBar.Controls.TextBoxX DestinatarioEstado;
        private DevComponents.DotNetBar.LabelX labelX10;
        private DevComponents.DotNetBar.Controls.TextBoxX DestinatarioColonia;
        private DevComponents.DotNetBar.Controls.TextBoxX DestinatarioMunicipio;
        private DevComponents.DotNetBar.LabelX labelX41;
        private DevComponents.DotNetBar.LabelX labelX32;
        private DevComponents.DotNetBar.Controls.TextBoxX DestinatarioInterior;
        private DevComponents.DotNetBar.LabelX labelX34;
        private DevComponents.DotNetBar.LabelX labelX35;
        private DevComponents.DotNetBar.Controls.ComboBoxEx DestinatarioPais;
        private DevComponents.Editors.ComboItem comboItem21;
        private DevComponents.Editors.ComboItem comboItem22;
        private DevComponents.Editors.ComboItem comboItem39;
        private DevComponents.Editors.ComboItem comboItem40;
        private DevComponents.DotNetBar.Controls.TextBoxX DestinatarioExterior;
        private DevComponents.DotNetBar.LabelX labelX36;
        private DevComponents.DotNetBar.Controls.TextBoxX DestinatarioLocalidad;
        private DevComponents.DotNetBar.Controls.TextBoxX DestinatarioCalle;
        private DevComponents.DotNetBar.LabelX labelX40;
        private DevComponents.DotNetBar.LabelX labelX37;
        private DevComponents.DotNetBar.Controls.TextBoxX NumRegIdTrib;
        private DevComponents.DotNetBar.Controls.ComboBoxEx Incoterm;
        private DevComponents.Editors.ComboItem comboItem24;
        private DevComponents.Editors.ComboItem comboItem25;
        private DevComponents.Editors.ComboItem comboItem26;
        private DevComponents.Editors.ComboItem comboItem27;
        private DevComponents.Editors.ComboItem comboItem28;
        private DevComponents.Editors.ComboItem comboItem29;
        private DevComponents.Editors.ComboItem comboItem30;
        private DevComponents.Editors.ComboItem comboItem31;
        private DevComponents.Editors.ComboItem comboItem32;
        private DevComponents.Editors.ComboItem comboItem33;
        private DevComponents.Editors.ComboItem comboItem34;
        private DevComponents.Editors.ComboItem comboItem35;
        private DevComponents.Editors.ComboItem comboItem36;
        private DevComponents.Editors.ComboItem comboItem37;
        private DevComponents.Editors.ComboItem comboItem38;
        private DevComponents.DotNetBar.LabelX labelX21;
        private DevComponents.DotNetBar.LabelX labelX20;
        private System.Windows.Forms.CheckBox activarCE;
        private System.Windows.Forms.TabPage tabPage5;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.TextBox PARA;
        private System.Windows.Forms.TextBox ASUNTO;
        private System.Windows.Forms.RichTextBox MENSAJE;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label17;
        private DevComponents.DotNetBar.ButtonX buttonX18;
        private System.Windows.Forms.TabPage tabPage1;
        private DevComponents.DotNetBar.Controls.DataGridViewX dtgrdGeneral;
        private System.Windows.Forms.DataGridViewTextBoxColumn cantidad;
        private System.Windows.Forms.DataGridViewTextBoxColumn unidad;
        private System.Windows.Forms.DataGridViewTextBoxColumn descripcion;
        private System.Windows.Forms.DataGridViewTextBoxColumn noIdentificacion;
        private System.Windows.Forms.DataGridViewTextBoxColumn valorUnitario;
        private System.Windows.Forms.DataGridViewTextBoxColumn importe;
        private System.Windows.Forms.DataGridViewTextBoxColumn fraccionArancelaria;
        private System.Windows.Forms.DataGridViewTextBoxColumn NumeroPedimentoInformacionAduanera;
        private System.Windows.Forms.DataGridViewTextBoxColumn informacionAduaneraFecha;
        private System.Windows.Forms.DataGridViewTextBoxColumn informacionAduaneraAduana;
        private System.Windows.Forms.DataGridViewTextBoxColumn informacionAduaneraNumero;
        private System.Windows.Forms.DataGridViewTextBoxColumn ClaveProdServ;
        private System.Windows.Forms.DataGridViewTextBoxColumn ClaveUnidad;
        private System.Windows.Forms.DataGridViewTextBoxColumn Submodelo;
        private System.Windows.Forms.DataGridViewTextBoxColumn Marca;
        private System.Windows.Forms.DataGridViewTextBoxColumn Modelo;
        private System.Windows.Forms.DataGridViewTextBoxColumn NumeroSerie;
        private System.Windows.Forms.DataGridViewTextBoxColumn CantidadAduana;
        private System.Windows.Forms.DataGridViewTextBoxColumn ValorUnitarioAduana;
        private System.Windows.Forms.DataGridViewTextBoxColumn ValorDolares;
        private System.Windows.Forms.DataGridViewTextBoxColumn NumeroPedimento;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.CheckBox IgualarEmisor;
        private System.Windows.Forms.TabPage tabPage11;
        private System.Windows.Forms.DataGridView DtgRelacion;
        private System.Windows.Forms.Button button5;
        private System.Windows.Forms.Button button6;
        private System.Windows.Forms.DataGridViewTextBoxColumn UUIDRelacionar;
        private System.Windows.Forms.ComboBox tipoRelacion;
        private System.Windows.Forms.Label label2;
        private DevComponents.Editors.ComboItem comboItem41;
    }
}