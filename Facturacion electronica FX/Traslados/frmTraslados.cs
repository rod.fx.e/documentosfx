﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Text;
using System.Windows.Forms;
using System.Xml.Serialization;
using Generales;
using System.Xml;
using FE_FX.EDICOM_Servicio;
using System.ServiceModel;
using FE_FX.CFDI;
using CFDI33;
using CrystalDecisions.CrystalReports.Engine;
using DevComponents.DotNetBar;
using System.Xml.XPath;
using System.Xml.Xsl;
using com.google.zxing.qrcode;
using CrystalDecisions.Shared;
using ModeloDocumentosFX;

namespace FE_FX
{
    public partial class frmTraslados : Form
    {
        string devolver = "";
        public DataGridViewSelectedRowCollection selecionados;
        private cCONEXCION oData_ERP = new cCONEXCION("");
        private cCONEXCION oData = new cCONEXCION("");
        string tipo = "";
        private cEMPRESA ocEMPRESA;
        private cSERIE ocSERIE;

        public frmTraslados()
        {
            ModeloDocumentosFXContainer Db = new ModeloDocumentosFXContainer();
            string Conexion = Db.Database.Connection.ConnectionString;
            oData = new cCONEXCION(Conexion);
            ocSERIE = new cSERIE();
            ocEMPRESA = new cEMPRESA();
            InitializeComponent();
            DateTime inicioTr = DateTime.Now.AddDays(-7);
            INICIO.Value = inicioTr;

            FINAL.Value = DateTime.Now;

            cargar_empresas();
            
        }
        private void cargar_empresas()
        {
            ENTITY_ID.Items.Clear();
            cEMPRESA ocEMPRESA = new cEMPRESA(oData);
            foreach (cEMPRESA registro in ocEMPRESA.todos("", true))
            {
                ENTITY_ID.Items.Add(registro);
            }
            if (ENTITY_ID.Items.Count > 0)
            {
                ENTITY_ID.SelectedIndex = 0;
            }

            cargarSerieTraslado();
        }

        private void cargarSerieTraslado()
        {
            //Cargar las serie de traslado
            if (ocSERIE.cargar_serie_traslado())
            {
                ID.Text = ocSERIE.ID;

                //cargarSerie(ocSERIE.ID);
            }

        }

        private void cargar()
        {
            int n;
            cADDENDA oObjeto = new cADDENDA();
            informacion.Text = "Cargando ";
            dtgrdGeneral.Rows.Clear();
            informacion.Text = "Total " + dtgrdGeneral.Rows.Count.ToString();

        }

        private void button1_Click(object sender, EventArgs e)
        {
            cargar();
        }
        private void ENTITY_ID_SelectedIndexChanged(object sender, EventArgs e)
        {
            actualizar_conexcion();
        }

        private void actualizar_conexcion()
        {

            ocEMPRESA = ENTITY_ID.SelectedItem as cEMPRESA;
            if (ocEMPRESA != null)
            {
                EMPRESA_NOMBRE.Text = ocEMPRESA.ID;
                oData_ERP = new cCONEXCION("SQLSERVER",ocEMPRESA.SERVIDOR, ocEMPRESA.BD, ocEMPRESA.USUARIO_BD, ocEMPRESA.PASSWORD_BD);
            }
        }
        private void BUSCAR_KeyPress(object sender, KeyPressEventArgs e)
        {
            //Precionar Enter buscar
            if (e.KeyChar.ToString() == "\r")
            {
                cargar();
            }
            
        }

        private void frmUsuarios_Load(object sender, EventArgs e)
        {
            cargar();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            frmAddenda oObjeto = new frmAddenda(oData.sConn);
            oObjeto.ShowDialog();
            cargar();
        }

        private void dtgrdGeneral_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            modificar();
        }

        public void modificar()
        {
            if (dtgrdGeneral.SelectedRows.Count > 0)
            {
                DataGridViewSelectedRowCollection arrSelectedRows = dtgrdGeneral.SelectedRows;
                string ARCHIVO = arrSelectedRows[0].Cells["ARCHIVO"].Value.ToString();
                string directorio = AppDomain.CurrentDomain.BaseDirectory;
                frmFacturaXML oObjeto = new frmFacturaXML(ARCHIVO);
                oObjeto.Show();
            }
        }

        private void frmFormatos_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)27)
            {
                this.Close();
            }
            else
            {
                if ((e.KeyChar.ToString() == "<")
                                | (e.KeyChar.ToString() == ">")
                                
                                | (e.KeyChar.ToString() == "'")
                                | (e.KeyChar.ToString() == "#")
                                )
                {
                    e.Handled = true;
                }
            }
        }

        private void buttonX2_Click(object sender, EventArgs e)
        {
            frmTraslado o = new frmTraslado(oData.sConn,"",-1);
            o.ShowDialog();
            cargar();
        }

        private void buttonX1_Click(object sender, EventArgs e)
        {
            cargar_archivos();
        }

        private void buttonX3_Click(object sender, EventArgs e)
        {
            buscarSerie();
        }

        private void buscarSerie()
        {

            frmSeries oBuscador = new frmSeries(oData.sConn, "S", ocEMPRESA.ROW_ID);
            if (oBuscador.ShowDialog() == DialogResult.OK)
            {
                string ROW_ID = (string)oBuscador.selecionados[0].Cells["ROW_ID"].Value;
                string IDtr = (string)oBuscador.selecionados[0].Cells["ID"].Value;
                ID.Text = IDtr;

                cargarSerie(IDtr);
                
            }
        }

        private void cargarSerie(string IDtr)
        {
            ocSERIE = new cSERIE();

            ocSERIE.cargar_serie(IDtr);
            ID.Text = ocSERIE.consecutivo(ocSERIE.ROW_ID, false);
            directorio.Text = ocSERIE.DIRECTORIO + @"\" + ocSERIE.ID + @"\";
            cargar_archivos();

        }

        private void cargar_archivos()
        {
            dtgrdGeneral.Rows.Clear();
            if (!Directory.Exists(directorio.Text))
            {
                return;
            }



            string mensajeError = "";
            proformaCEE.DATOSproformaDataTable tabla = new proformaCEE.DATOSproformaDataTable();
            //Cargar archivos
            foreach (string archivo in Directory.GetFiles(directorio.Text, "*.xml", SearchOption.AllDirectories))
            {
                try
                {
                    XmlSerializer serializer_CFD_3 = new XmlSerializer(typeof(CFDI33.Comprobante));
                    TextReader reader = new StreamReader(archivo);
                    CFDI33.Comprobante oComprobante;
                    oComprobante = (CFDI33.Comprobante)serializer_CFD_3.Deserialize(reader);

                    XmlDocument docXML = new XmlDocument();
                    docXML.Load(archivo);
                    XmlNodeList TimbreFiscalDigital = docXML.GetElementsByTagName("tfd:TimbreFiscalDigital");
                    string TimbreFiscalDigitaltr = "";
                    if (TimbreFiscalDigital != null)
                    {
                        if (TimbreFiscalDigital[0] != null)
                        {
                            TimbreFiscalDigitaltr = TimbreFiscalDigital[0].Attributes["UUID"].Value;
                        }
                    }

                    tabla.AddDATOSproformaRow(TimbreFiscalDigitaltr
                        , oComprobante.Total,oComprobante.Receptor.Rfc
                        ,oComprobante.Fecha,oComprobante.Serie,oComprobante.Folio,archivo,
                        oComprobante.Receptor.Nombre
                        );

                }
                catch (Exception error)
                {
                    string mensaje = "";
                    mensaje = error.Message.ToString();
                    if (error.InnerException != null)
                    {
                        mensaje += " " + error.InnerException.Message.ToString();
                    }
                    mensajeError = mensaje;
                }
            }

            //Cargar tabla
            var dataQuery =
                        from tr in tabla
                        where tr.fecha >= INICIO.Value & tr.fecha <= FINAL.Value
                        select tr;
            foreach (proformaCEE.DATOSproformaRow r in dataQuery)
            {
                int n = dtgrdGeneral.Rows.Add();
                dtgrdGeneral.Rows[n].Cells["archivo"].Value = r.archivo;
                dtgrdGeneral.Rows[n].Cells["folio"].Value = r.folio;
                dtgrdGeneral.Rows[n].Cells["fecha"].Value = r.fecha;
                dtgrdGeneral.Rows[n].Cells["rfcReceptor"].Value = r.rfcReceptor;
                dtgrdGeneral.Rows[n].Cells["nombreReceptor"].Value = r.nombreReceptor;
                dtgrdGeneral.Rows[n].Cells["total"].Value = r.total;
                dtgrdGeneral.Rows[n].Cells["UUID"].Value = r.uuid;
            }

            dtgrdGeneral.AutoResizeColumns(DataGridViewAutoSizeColumnsMode.AllCells);
        }

        private void buttonX5_Click(object sender, EventArgs e)
        {
            cargarDirectorio();
        }

        private void cargarDirectorio()
        {
            if (folderBrowserDialog1.ShowDialog() == DialogResult.OK)
            {
                directorio.Text = folderBrowserDialog1.SelectedPath;
                cargar_archivos();
            }
        }

        private void dtgrdGeneral_MouseClick(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Right)
            {
                contextMenuStrip1.Show(dtgrdGeneral, new Point(e.X, e.Y));
            }
        }

        private void cancelarToolStripMenuItem_Click(object sender, EventArgs e)
        {
            cancelarSeleccionadas();
        }
        private void cancelarSeleccionadas()
        {
            try
            {
                if (dtgrdGeneral.SelectedRows.Count > 0)
                {
                    DataGridViewSelectedRowCollection arrSelectedRows = dtgrdGeneral.SelectedRows;
                    for (int n = arrSelectedRows.Count - 1; n >= 0; n--)
                    {
                        
                        cancelar(arrSelectedRows[n].Cells["UUID"].Value.ToString()
                            , arrSelectedRows[n].Cells["NumCert"].Value.ToString(), arrSelectedRows[n].Index);
                    }
                }
            }
            catch (Exception e)
            {
                ErrorFX.mostrar(e, true, true, true);
            }
        }

        public byte[] ReadBinaryFile(string fileName)
        {
            if (File.Exists(fileName))
            {
                try
                {
                    ///Open and read a file&#12290;
                    FileStream fileStream = File.OpenRead(fileName);
                    byte[] archivo = ConvertStreamToByteBuffer(fileStream);
                    fileStream.Close();
                    return archivo;
                }
                catch (Exception ex)
                {
                    return new byte[0];
                }
            }
            else
            {
                return new byte[0];
            }
        }

        public byte[] ConvertStreamToByteBuffer(System.IO.Stream theStream)
        {
            int b1;
            System.IO.MemoryStream tempStream = new System.IO.MemoryStream();
            while ((b1 = theStream.ReadByte()) != -1)
            {
                tempStream.WriteByte(((byte)b1));
            }
            return tempStream.ToArray();
        }


        private void cancelar(string uuidp, string numeroCertificado, int indice)
        {
            //NumCert
            //Llamar el Servicio
            CFDiClient oCFDiClient = new CFDiClient();
            string[] uuid = new string[1];
            uuid[0] = uuidp;
            cCERTIFICADO ocCERTIFICADO = new cCERTIFICADO();

            string pfx=ocCERTIFICADO.getPFX(numeroCertificado);
            if (String.IsNullOrEmpty(pfx))
            {
                MessageBox.Show("El PFX no existe para el certificado " + numeroCertificado
                                        , Application.ProductName + "-" + Application.ProductVersion, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return;
            }

            //Cargar el archivo pfx creado desde OpenSSL
            
            if (!File.Exists(pfx))
            {
                //Validar el directorio

                string directorio = AppDomain.CurrentDomain.BaseDirectory;
                pfx = directorio + @"\" + ocCERTIFICADO.PFX;
                if (!File.Exists(pfx))
                {
                    MessageBox.Show("El PFX " + ocCERTIFICADO.PFX + " no existe."
                        , Application.ProductName + "-" + Application.ProductVersion, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                    return;
                }
            }
            byte[] pfx_archivo = ReadBinaryFile(pfx);
            try
            {
                CancelaResponse respuesta;
                //Todo: Realizar cancelacion
                //respuesta = oCFDiClient.cancelaCFDiRetenciones(Globales.oCFDI_USUARIO.USUARIO, cENCRIPTACION.Decrypt(Globales.oCFDI_USUARIO.PASSWORD), ocEMPRESA.RFC, uuid, pfx_archivo, ocCERTIFICADO.PASSWORD);
                return;
            }
            catch (FaultException oException)
            {

                CFDiException oCFDiException = new CFDiException();
                MessageBox.Show(oException.Message, Application.ProductName + "-" + Application.ProductVersion, MessageBoxButtons.OK, MessageBoxIcon.Error);
                informacion.Text = "Error: Factura NO CANCELADA " + DateTime.Now.ToShortDateString() + " " + DateTime.Now.ToLongTimeString();
                return;
            }
        }
        private string generarCadenaOriginal(string archivo)
        {
            XPathDocument myXPathDoc = new XPathDocument(archivo);
            //XslTransform myXslTrans = new XslTransform();
            XslCompiledTransform myXslTrans = new XslCompiledTransform();
            XslCompiledTransform trans = new XslCompiledTransform();
            XmlUrlResolver resolver = new XmlUrlResolver();

            resolver.Credentials = System.Net.CredentialCache.DefaultCredentials;
            XsltSettings settings = new XsltSettings(true, true);
            string sDirectory = AppDomain.CurrentDomain.BaseDirectory;
            //Cargar xslt
            try
            {
                //Version 3
                myXslTrans.Load(sDirectory + "/XSLT/cadenaoriginal_3_2.xslt");
            }
            catch (XmlException errores)
            {
                MessageBox.Show("Error. Conectarse en el Portal del SAT al " + errores.InnerException.ToString());
                return "";
            }

            //Crear Archivo Temporal
            string nombre_tmp = "TMP" + DateTime.Now.ToString("ddMMyyyyhhmmss");
            XmlTextWriter myWriter = new XmlTextWriter(nombre_tmp + ".TXT", null);

            //Transformar al XML
            myXslTrans.Transform(myXPathDoc, null, myWriter);

            myWriter.Close();
            string CADENA_ORIGINAL = "";
            //Abrir Archivo TXT
            StreamReader re = File.OpenText(nombre_tmp + ".TXT");
            string input = null;
            while ((input = re.ReadLine()) != null)
            {
                CADENA_ORIGINAL += input;
            }
            re.Close();

            return CADENA_ORIGINAL;
        }

        private string generarCadenaTFD(string archivo)
        {
            string nombre_tmp = "TMP" + DateTime.Now.ToString("ddMMyyyyhhmmss");
            XmlDocument doc = new XmlDocument();
            doc.Load(archivo);
            XslCompiledTransform myXslTrans = new XslCompiledTransform();
            XslCompiledTransform trans = new XslCompiledTransform();
            XmlUrlResolver resolver = new XmlUrlResolver();

            resolver.Credentials = System.Net.CredentialCache.DefaultCredentials;
            XsltSettings settings = new XsltSettings(true, true);

            //Cargar xslt
            try
            {
                myXslTrans.Load(AppDomain.CurrentDomain.BaseDirectory + "/XSLT/cadenaoriginal_TFD_1_1.xslt");
            }
            catch (XmlException errores)
            {
                MessageBox.Show("Error en " + errores.InnerException.ToString());
                return "";
            }

            //Crear Archivo Temporal

            XmlTextWriter myWriter = new XmlTextWriter(nombre_tmp + ".TXT", null);
            XmlNodeList TimbreFiscalDigital = doc.GetElementsByTagName("tfd:TimbreFiscalDigital");
            //Transformar al XML
            myXslTrans.Transform(TimbreFiscalDigital[0], null, myWriter);

            myWriter.Close();

            //Abrir Archivo TXT
            string cadenaoriginal_TFD_1_0 = "";
            StreamReader re = File.OpenText(nombre_tmp + ".TXT");
            string input = null;
            while ((input = re.ReadLine()) != null)
            {
                cadenaoriginal_TFD_1_0 += input;
            }
            re.Close();

            //Eliminar Archivos Temporales
            File.Delete(nombre_tmp + ".TXT");
            return cadenaoriginal_TFD_1_0;

        }

        private byte[] generarImagen(string archivo)
        {
            //?re=XAXX010101000&rr=XAXX010101000&tt=1234567890.123456&id=ad662d33-6934-459c-a128-BDf0393f0f44
            string QR_Code = "";
            XmlSerializer serializer_CFD_3 = new XmlSerializer(typeof(CFDI33.Comprobante));
            TextReader reader = new StreamReader(archivo);
            try
            {
                if (!File.Exists(archivo))
                {
                    MessageBox.Show("El CFDI " + archivo + " no existe");
                    return null;
                }
                //Abrir el archivo y copiar los sellos
                XmlDocument doc = new XmlDocument();
                doc.Load(archivo);

                XmlNodeList TimbreFiscalDigital = doc.GetElementsByTagName("tfd:TimbreFiscalDigital");
                XmlNodeList receptor = doc.GetElementsByTagName("cfdi:Receptor");
                XmlNodeList emisor = doc.GetElementsByTagName("cfdi:Emisor");
                XmlNodeList comprobante = doc.GetElementsByTagName("cfdi:Comprobante");



                string Sello = "";

                try
                {
                    Sello = comprobante[0].Attributes["Sello"].Value;
                }
                catch
                {

                }

                string rfcReceptor = "";

                try
                {
                    rfcReceptor = receptor[0].Attributes["Rfc"].Value;
                }
                catch
                {

                }

                string rfcEmisor = "";

                try
                {
                    rfcEmisor = emisor[0].Attributes["Rfc"].Value;
                }
                catch
                {

                }

                string total = "";

                try
                {
                    total = comprobante[0].Attributes["Total"].Value;
                }
                catch
                {

                }

                string UUID = "";

                try
                {
                    UUID = TimbreFiscalDigital[0].Attributes["UUID"].Value;
                }
                catch
                {

                }
                string FechaTimbrado = "";

                try
                {
                    FechaTimbrado = TimbreFiscalDigital[0].Attributes["FechaTimbrado"].Value;
                }
                catch
                {

                }

                string noCertificadoSAT = "";
                try
                {
                    noCertificadoSAT = TimbreFiscalDigital[0].Attributes["noCertificadoSAT"].Value;
                }
                catch
                {

                }


                try
                {
                    noCertificadoSAT = TimbreFiscalDigital[0].Attributes["NoCertificadoSAT"].Value;
                }
                catch
                {

                }

                string selloSAT = "";
                try
                {
                    selloSAT = TimbreFiscalDigital[0].Attributes["selloSAT"].Value;
                }
                catch
                {

                }

                try
                {
                    selloSAT = TimbreFiscalDigital[0].Attributes["SelloSAT"].Value;
                }
                catch
                {

                }

                string USO_CFDI = "";

                try
                {
                    USO_CFDI = receptor[0].Attributes["UsoCFDI"].Value;
                }
                catch
                {

                }

                try
                {
                    USO_CFDI = receptor[0].Attributes["UsoCFDI"].Value;
                }
                catch
                {

                }


                string VERSION = "";

                try
                {
                    VERSION = comprobante[0].Attributes["version"].Value;
                }
                catch
                {

                }

                try
                {
                    VERSION = comprobante[0].Attributes["Version"].Value;
                }
                catch
                {

                }

                string metodo_pago = "";

                try
                {
                    metodo_pago = comprobante[0].Attributes["MetodoPago"].Value;
                }
                catch
                {

                }

                try
                {
                    metodo_pago = comprobante[0].Attributes["MetodoPago"].Value;
                }
                catch
                {

                }
                string banco = "";
                try
                {
                    banco = comprobante[0].Attributes["NumCtaPago"].Value;
                }
                catch
                {

                }

                try
                {
                    banco = comprobante[0].Attributes["NumCtaPago"].Value;
                }
                catch
                {

                }


                string FormaPago = "";

                try
                {
                    FormaPago = comprobante[0].Attributes["FormaPago"].Value;
                }
                catch
                {

                }

                try
                {
                    FormaPago = comprobante[0].Attributes["formaPago"].Value;
                }
                catch
                {

                }
                string NoCertificado = "";
                try
                {
                    NoCertificado = comprobante[0].Attributes["NoCertificado"].Value;
                }
                catch
                {

                }
                //Abrir Archivo TXT
                string cadenaoriginal_TFD_1_0 = "";
                //Crear Archivo Temporal
                if (TimbreFiscalDigital != null)
                {
                    string sDirectory = AppDomain.CurrentDomain.BaseDirectory;
                    //Generar cadenaoriginal_TFD_1_0.xslt
                    string nombre_tmp = "TMP" + DateTime.Now.ToString("ddMMyyyyhhmmss");
                    //XslTransform myXslTrans = new XslTransform();
                    XslCompiledTransform myXslTrans = new XslCompiledTransform();


                    XslCompiledTransform trans = new XslCompiledTransform();
                    XmlUrlResolver resolver = new XmlUrlResolver();

                    resolver.Credentials = System.Net.CredentialCache.DefaultCredentials;
                    XsltSettings settings = new XsltSettings(true, true);

                    //Cargar xslt
                    try
                    {

                        myXslTrans.Load(sDirectory + "/XSLT/cadenaoriginal_TFD_1_1.xslt");
                    }
                    catch (XmlException errores)
                    {
                        MessageBox.Show("Error en " + errores.InnerException.ToString());
                        return null;
                    }
                    XmlTextWriter myWriter = new XmlTextWriter(nombre_tmp + ".TXT", null);

                    //Transformar al XML
                    myXslTrans.Transform(TimbreFiscalDigital[0], null, myWriter);

                    myWriter.Close();


                    StreamReader re = File.OpenText(nombre_tmp + ".TXT");
                    string input = null;
                    while ((input = re.ReadLine()) != null)
                    {
                        cadenaoriginal_TFD_1_0 += input;
                    }
                    re.Close();
                    re.Dispose();
                    //Eliminar Archivos Temporales
                    File.Delete(nombre_tmp + ".TXT");
                }

                QR_Code = "https://verificacfdi.facturaelectronica.sat.gob.mx/default.aspx"
                    + "?id=" + UUID
                    + "&re=" + rfcEmisor
                    + "&rr=" + rfcReceptor
                    + "&tt=" + total
                    + "&fe=" + Sello.Substring(Sello.Length - 8, 8);

                //XmlDocument doc = new XmlDocument();
                //doc.Load(archivo);


                //XmlNodeList TimbreFiscalDigital = doc.GetElementsByTagName("tfd:TimbreFiscalDigital");

                //string UUID = "";
                //UUID = TimbreFiscalDigital[0].Attributes["UUID"].Value;

                //oComprobanteLocal = (Comprobante)serializer_CFD_3.Deserialize(reader);

                //string QR_Code = "https://verificacfdi.facturaelectronica.sat.gob.mx/default.aspx&";
                //QR_Code = "https://verificacfdi.facturaelectronica.sat.gob.mx/default.aspx"
                //    + "?id=" + UUID
                //    + "&re=" + rfcEmisor
                //    + "&rr=" + rfcReceptor
                //    + "&tt=" + total
                //    + "&fe=" + Sello.Substring(Sello.Length - 8, 8);

                //QR_Code += "?re=" + oComprobanteLocal.Emisor.Rfc;
                //QR_Code += "&rr=" + oComprobanteLocal.Receptor.Rfc;
                //QR_Code += "&tt=" + oComprobanteLocal.Total.ToString();
                //QR_Code += "&id=" + UUID;

            }
            catch (Exception xmlex)
            {
                Bitacora.Log("Errpr :: " + xmlex.ToString());
                return null;
            }
            int size = 320;
            QRCodeWriter writer = new QRCodeWriter();
            com.google.zxing.common.ByteMatrix matrix;
            matrix = writer.encode(QR_Code, com.google.zxing.BarcodeFormat.QR_CODE, size, size, null);
            Bitmap img = new Bitmap(size, size);
            Color Color = Color.FromArgb(0, 0, 0);

            for (int y = 0; y < matrix.Height; ++y)
            {
                for (int x = 0; x < matrix.Width; ++x)
                {
                    Color pixelColor = img.GetPixel(x, y);

                    //Find the colour of the dot
                    if (matrix.get_Renamed(x, y) == -1)
                    {
                        img.SetPixel(x, y, Color.White);
                    }
                    else
                    {
                        img.SetPixel(x, y, Color.Black);
                    }
                }
            }


            string nombre = Path.GetTempPath() + "QRP_TEMP.BMP";
            img.Save(nombre);

            FileStream fs = new FileStream(nombre, System.IO.FileMode.Open, System.IO.FileAccess.Read);
            byte[] Image = new byte[fs.Length];
            fs.Read(Image, 0, Convert.ToInt32(fs.Length));
            int image_32 = Convert.ToInt32(fs.Length);
            fs.Close();
            File.Delete(nombre);

            return Image;

        }

        private void imprimirToolStripMenuItem_Click(object sender, EventArgs e)
        {
            imprimir();
        }
        private void imprimir(bool verPDF = true)
        {
            ReportDocument oReport = new ReportDocument();
            string path = "";
            OpenFileDialog openFileDialog1 = new OpenFileDialog
            {
                InitialDirectory = @"D:\",
                Title = "CFDI ",

                CheckFileExists = true,
                CheckPathExists = true,

                DefaultExt = "xml",
                Filter = "CFDI XML files (*.xml)|*.xml",
                FilterIndex = 2,
                RestoreDirectory = true,

                ReadOnlyChecked = true,
                ShowReadOnly = true
            };

            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                path = openFileDialog1.FileName;
            }

            string FORMATO = "TRASLADO.rpt";

            this.Text = "Formato: " + FORMATO;
            if (!File.Exists(FORMATO))
            {
                MessageBoxEx.Show("El Reporte " + FORMATO + " no existe en su directorio. La factura no fue Generada.", Application.ProductName + "-" + Application.ProductVersion, MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            
            try
            {
                XmlSerializer serializer_CFD_3 = new XmlSerializer(typeof(CFDI33.Comprobante));
                TextReader reader = new StreamReader(path);
                CFDI33.Comprobante oComprobanteLocal = (CFDI33.Comprobante)serializer_CFD_3.Deserialize(reader);
                //cfdiCE.comprobanteDataTable ocomprobanteDataTable = new cfdiCE.comprobanteDataTable();
                //ocomprobanteDataTable.AddcomprobanteRow(
                //    oComprobanteLocal.Emisor.rfc, oComprobanteLocal.Emisor.nombre, oComprobanteLocal.Emisor.RegimenFiscal[0].Regimen.ToString(), oComprobanteLocal.LugarExpedicion
                //    , oComprobanteLocal.Receptor.rfc, oComprobanteLocal.Receptor.nombre
                //    , 0
                //    , decimal.Parse(oComprobanteLocal.subTotal.ToString()), decimal.Parse(oComprobanteLocal.Impuestos.totalImpuestosTrasladados.ToString()), decimal.Parse(oComprobanteLocal.total.ToString())
                //    );
                //cfdiCE.conceptosDataTable oconceptosDataTable = new cfdiCE.conceptosDataTable();
                //foreach (CFDI33.ComprobanteConcepto concepto in oComprobanteLocal.Conceptos)
                //{
                //    oconceptosDataTable.AddconceptosRow(concepto.Cantidad, concepto.Descripcion
                //        , concepto.NoIdentificacion, concepto.ValorUnitario, concepto.Importe, 0,
                //        concepto.ClaveUnidad);
                //}

                //string pathXML= System.IO.Path.GetDirectoryName(path);
                //string pathFormato = System.IO.Path.GetDirectoryName(FORMATO);
                //string filenameXML = Path.GetFileName(path);
                //File.Copy(pathXML+@"/"+ filenameXML, pathFormato + @"/" + filenameXML);

                oReport.Load(FORMATO);
                //DataSet oDataSet = new DataSet();
                ////oDataSet.ReadXml(PATH.Text);
                //oReport.SetDataSource(oDataSet);
                DataSet reportData = new DataSet();
                reportData.ReadXml(path);
                oReport.SetDataSource(reportData);

                XmlDocument doc = new XmlDocument();
                doc.Load(path);
                XmlNodeList TimbreFiscalDigital = doc.GetElementsByTagName("tfd:TimbreFiscalDigital");
                if (TimbreFiscalDigital == null)
                {
                    ErrorFX.mostrar("El archivo: " + path + " No esta timbrado.", true, true, false);
                    return;
                }
                string UUID = "";
                UUID = TimbreFiscalDigital[0].Attributes["UUID"].Value;
                string FechaTimbrado = "";
                FechaTimbrado = TimbreFiscalDigital[0].Attributes["FechaTimbrado"].Value;
                string noCertificadoSAT = "";
                noCertificadoSAT = TimbreFiscalDigital[0].Attributes["NoCertificadoSAT"].Value;
                string selloSAT = "";
                selloSAT = TimbreFiscalDigital[0].Attributes["SelloSAT"].Value;

                byte[] image = generarImagen(path);
                string cadenaOriginal = generarCadenaOriginal(path);
                string cadenaTFD = generarCadenaTFD(path);
                cfdiCE.DATOSDataTable oDATOS = new cfdiCE.DATOSDataTable();

                XmlNodeList Emisor = doc.GetElementsByTagName("cfdi:Emisor");
                string RegimenFiscal = "";
                if (Emisor != null)
                {
                    RegimenFiscal = Emisor[0].Attributes["RegimenFiscal"].Value;
                }

                XmlNodeList Receptor = doc.GetElementsByTagName("cfdi:Receptor");
                string UsoCFDI = "";
                if (Receptor != null)
                {
                    UsoCFDI = Receptor[0].Attributes["UsoCFDI"].Value;
                }

                string NumRegIdTrib = "";
                if (Receptor != null)
                {
                    try
                    {
                        NumRegIdTrib = Receptor[0].Attributes["NumRegIdTrib"].Value;
                    }
                    catch
                    {

                    }
                }

                XmlNodeList Comprobante = doc.GetElementsByTagName("cfdi:Comprobante");
                string LugarExpedicion = "";
                if (Comprobante != null)
                {
                    LugarExpedicion = Comprobante[0].Attributes["LugarExpedicion"].Value;
                }

                oDATOS.AddDATOSRow(image, cadenaOriginal, cadenaTFD, ""
                    , "", 0
                    , oComprobanteLocal.Emisor.Rfc, oComprobanteLocal.Emisor.Nombre, oComprobanteLocal.Emisor.RegimenFiscal
                    , oComprobanteLocal.LugarExpedicion
                    , oComprobanteLocal.Receptor.Rfc, oComprobanteLocal.Receptor.Nombre
                    , oComprobanteLocal.Fecha.ToString(), oComprobanteLocal.Serie, oComprobanteLocal.Folio.ToString(), oComprobanteLocal.NoCertificado.ToString()
                    , oComprobanteLocal.TipoDeComprobante.ToString(), 0, 0
                    , oComprobanteLocal.Total
                    , UUID, "", FechaTimbrado, noCertificadoSAT, selloSAT
                    , "", RegimenFiscal,UsoCFDI, LugarExpedicion
                    , NumRegIdTrib
                    );



                ConnectionInfo oConnectionInfo = new ConnectionInfo();
                oConnectionInfo.ServerName = path;
                oConnectionInfo.DatabaseName = "NewDataset";
                oConnectionInfo.UserID = "";
                oConnectionInfo.Password = "";
                CrystalDecisions.Shared.TableLogOnInfo crpTableLogOnInfo = new TableLogOnInfo();
                CrystalDecisions.CrystalReports.Engine.Tables tables = oReport.Database.Tables;
                foreach (CrystalDecisions.CrystalReports.Engine.Table table in tables)
                {
                    CrystalDecisions.Shared.TableLogOnInfo tableLogonInfo = table.LogOnInfo;

                    tableLogonInfo.ConnectionInfo = oConnectionInfo;
                    table.ApplyLogOnInfo(tableLogonInfo);
                }
                oReport.DataSourceConnections[0].SetConnection(path, "NewDataset", "", "");
                DataSet ds = new DataSet();
                ds.ReadXml(path);

                foreach (CrystalDecisions.CrystalReports.Engine.Table tabla in oReport.Database.Tables)
                {
                    oReport.Database.Tables[tabla.Name].LogOnInfo.ConnectionInfo.ServerName = path;
                    oReport.Database.Tables[tabla.Name].SetDataSource(ds);
                    //tabla.LogOnInfo.ConnectionInfo.ServerName = path;
                    //tabla.LogOnInfo.ConnectionInfo.Type = CrystalDecisions.Shared.ConnectionInfoType.MetaData;
                }



                if (oReport.Database.Tables.Count > 0)
                {
                    //oReport.SetDataSource(oDataSet);
                    foreach (CrystalDecisions.CrystalReports.Engine.Table tabla in oReport.Database.Tables)
                    {


                        if (tabla.Name.ToString() == "TimbreFiscalDigital")
                        {
                            oReport.Database.Tables["TimbreFiscalDigital"].SetDataSource(ds.Tables["TimbreFiscalDigital"]);
                        }
                        if (tabla.Name.ToString() == "CfdiRelacionados")
                        {
                            if (ds.Tables["CfdiRelacionados"] != null)
                            {
                                oReport.Database.Tables["CfdiRelacionados"].SetDataSource(ds.Tables["CfdiRelacionados"]);
                            }
                            else
                            {
                                DataTable oDataTableTmP = new DataTable();
                                oReport.Database.Tables["CfdiRelacionados"].SetDataSource(oDataTableTmP);
                            }
                        }


                        if (tabla.Name.ToString() == "CfdiRelacionado")
                        {
                            if (ds.Tables["CfdiRelacionado"] != null)
                            {
                                oReport.Database.Tables["CfdiRelacionado"].SetDataSource(ds.Tables["CfdiRelacionado"]);
                            }
                            else
                            {
                                DataTable oDataTableTmP = new DataTable();
                                oReport.Database.Tables["CfdiRelacionado"].SetDataSource(oDataTableTmP);
                            }
                        }
                        if (tabla.Name.ToString() == "Complemento")
                        {
                            oReport.Database.Tables["Complemento"].SetDataSource(ds.Tables["Complemento"]);
                        }
                        if (tabla.Name.ToString() == "Comprobante")
                        {
                            oReport.Database.Tables["Comprobante"].SetDataSource(ds.Tables["Comprobante"]);
                        }
                        if (tabla.Name.ToString() == "Conceptos")
                        {
                            oReport.Database.Tables["Conceptos"].SetDataSource(ds.Tables["Conceptos"]);
                        }
                        if (tabla.Name.ToString() == "Concepto")
                        {
                            oReport.Database.Tables["Concepto"].SetDataSource(ds.Tables["Concepto"]);
                        }
                        if (tabla.Name.ToString() == "Emisor")
                        {
                            oReport.Database.Tables["Emisor"].SetDataSource(ds.Tables[1]);
                        }
                        if (tabla.Name.ToString() == "InformacionAduanera")
                        {
                            oReport.Database.Tables["InformacionAduanera"].SetDataSource(ds.Tables["InformacionAduanera"]);
                        }
                        if (tabla.Name.ToString() == "Receptor")
                        {
                            oReport.Database.Tables["Receptor"].SetDataSource(ds.Tables[2]);
                        }
                        if (tabla.Name.ToString() == "Complemento")
                        {
                            oReport.Database.Tables["Complemento"].SetDataSource(ds.Tables["Complemento"]);
                        }
                        if (tabla.Name.ToString() == "DoctoRelacionado")
                        {
                            oReport.Database.Tables["DoctoRelacionado"].SetDataSource(ds.Tables["DoctoRelacionado"]);
                        }
                        if (tabla.Name.ToString() == "Pago")
                        {
                            oReport.Database.Tables["Pago"].SetDataSource(ds.Tables["Pago"]);
                        }
                        if (tabla.Name.ToString() == "Pagos")
                        {
                            oReport.Database.Tables["Pagos"].SetDataSource(ds.Tables["Pagos"]);
                        }
                    }

                }

                foreach (ParameterFieldDefinition definition in oReport.DataDefinition.ParameterFields)
                {
                    ParameterValues myvals = new ParameterValues();
                    ParameterDiscreteValue myDiscrete = new ParameterDiscreteValue();
                    switch (definition.ParameterFieldName)
                    {
                        case "cadenaSAT":
                            //if (!String.IsNullOrEmpty(xml))
                            //{
                            myDiscrete.Value = cadenaTFD;
                            myvals.Add(myDiscrete);
                            definition.CurrentValues.Add(myDiscrete);
                            definition.ApplyCurrentValues(myvals);
                            //}
                            break;
                    }
                }
                try
                {
                    ExportOptions CrExportOptions = new ExportOptions();
                    DiskFileDestinationOptions CrDiskFileDestinationOptions = new DiskFileDestinationOptions();
                    PdfRtfWordFormatOptions CrFormatTypeOptions = new PdfRtfWordFormatOptions();
                    string pdftr = path.Replace(".xml", ".pdf");
                    pdftr = pdftr.Replace(@"\xml", @"\pdf");
                    CrDiskFileDestinationOptions.DiskFileName = pdftr;
                    CrExportOptions = oReport.ExportOptions;
                    CrExportOptions.ExportDestinationType = ExportDestinationType.DiskFile;
                    CrExportOptions.ExportFormatType = ExportFormatType.PortableDocFormat;
                    CrExportOptions.DestinationOptions = CrDiskFileDestinationOptions;
                    CrExportOptions.FormatOptions = CrFormatTypeOptions;
                    oReport.Export();
                }
                catch (Exception error)
                {
                    string mensaje = "Impresion . ";
                    mensaje += error.Message.ToString();
                    if (error.InnerException != null)
                    {
                        mensaje += " " + error.InnerException.Message.ToString();
                    }

                    MessageBox.Show(mensaje);
                }
                if (verPDF)
                {
                    frmCFDIImpresion ofrmReporte = new frmCFDIImpresion(oReport);
                    ofrmReporte.ShowDialog();
                }
            }
            catch (Exception error)
            {
                string mensaje = error.Message.ToString();
                if (error.InnerException != null)
                {
                    mensaje += Environment.NewLine + error.InnerException.ToString();
                }
                MessageBox.Show(mensaje);
            }


        }

        private void buttonX4_Click(object sender, EventArgs e)
        {
            imprimir();
        }

        private void buttonX6_Click(object sender, EventArgs e)
        {
            cargar_archivos();
        }

        private void buttonX7_Click(object sender, EventArgs e)
        {
            aumentarSerie();
        }

        private void aumentarSerie()
        {
            if (ocSERIE!=null)
            {
                ocSERIE.incrementar(ocSERIE.ROW_ID);
                cargarSerie(ocSERIE.ID);
            }

        }

        private void button1_Click_1(object sender, EventArgs e)
        {
            //ImpresionCFDI oImpresionCFDI = new ImpresionCFDI();
            //oImpresionCFDI.traslado();

            frmTraslados oCe = new frmTraslados();
            oCe.imprimiNuevo();
        }

        public void imprimiNuevo(bool verPDF = true, string pathP=""
            , string nombreReceptor = "", string rfcReceptor = "", string observaciones=""
            , string entregar="")
        {
            ReportDocument oReport = new ReportDocument();
            string path = "";
            if (!String.IsNullOrEmpty(pathP))
            {
                if (File.Exists(pathP))
                {
                    path = pathP;
                }
            }
            else
            {
                OpenFileDialog openFileDialog1 = new OpenFileDialog
                {
                    InitialDirectory = @"D:\",
                    Title = "CFDI ",
                    CheckFileExists = true,
                    CheckPathExists = true,
                    DefaultExt = "xml",
                    Filter = "CFDI XML files (*.xml)|*.xml",
                    FilterIndex = 2,
                    RestoreDirectory = true,
                    ReadOnlyChecked = true,
                    ShowReadOnly = true
                };

                if (openFileDialog1.ShowDialog() == DialogResult.OK)
                {
                    path = openFileDialog1.FileName;
                }

            }

            string FORMATO = "TRASLADOds.rpt";

            this.Text = "Formato: " + FORMATO;
            if (!File.Exists(FORMATO))
            {
                MessageBoxEx.Show("El Reporte " + FORMATO + " no existe en su directorio. La factura no fue Generada.", Application.ProductName + "-" + Application.ProductVersion, MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            try
            {

                XmlSerializer serializer_CFD_3 = new XmlSerializer(typeof(CFDI40.Comprobante));
                TextReader reader = new StreamReader(path);
                CFDI40.Comprobante oComprobanteLocal = (CFDI40.Comprobante)serializer_CFD_3.Deserialize(reader);
                reader.Close();
                oReport.Load(FORMATO);
                
                XmlDocument doc = new XmlDocument();
                doc.Load(path);
                XmlNodeList TimbreFiscalDigital = doc.GetElementsByTagName("tfd:TimbreFiscalDigital");
                if (TimbreFiscalDigital == null)
                {
                    ErrorFX.mostrar("El archivo: " + path + " No esta timbrado.", true, true, false);
                    return;
                }
                string UUID = "";
                UUID = TimbreFiscalDigital[0].Attributes["UUID"].Value;
                string FechaTimbrado = "";
                FechaTimbrado = TimbreFiscalDigital[0].Attributes["FechaTimbrado"].Value;
                string noCertificadoSAT = "";
                noCertificadoSAT = TimbreFiscalDigital[0].Attributes["NoCertificadoSAT"].Value;
                string selloSAT = "";
                selloSAT = TimbreFiscalDigital[0].Attributes["SelloSAT"].Value;

                XmlNodeList Comprobante = doc.GetElementsByTagName("cfdi:Comprobante");
                string LugarExpedicion = "";
                if (Comprobante != null)
                {
                    LugarExpedicion = Comprobante[0].Attributes["LugarExpedicion"].Value;
                }                
                
                byte[] image = generarImagen(path);
                string cadenaOriginal = generarCadenaOriginal(path);
                string cadenaTFD = generarCadenaTFD(path);


                XmlNodeList Emisor = doc.GetElementsByTagName("cfdi:Emisor");
                string RegimenFiscal = "";
                if (Emisor != null)
                {
                    RegimenFiscal = Emisor[0].Attributes["RegimenFiscal"].Value;
                }

                XmlNodeList Receptor = doc.GetElementsByTagName("cfdi:Receptor");
                string UsoCFDI = "";
                if (Receptor != null)
                {
                    UsoCFDI = Receptor[0].Attributes["UsoCFDI"].Value;
                }
                string NumRegIdTrib = "";
                if (Receptor != null)
                {
                    try
                    {
                        NumRegIdTrib = Receptor[0].Attributes["NumRegIdTrib"].Value;
                    }
                    catch
                    {

                    }
                }

                XmlNodeList CEEDestinatario = doc.GetElementsByTagName("cce11:Destinatario");
                if (CEEDestinatario != null)
                {
                    try
                    {
                        if (nombreReceptor == string.Empty)
                        {
                            nombreReceptor = CEEDestinatario[0].Attributes["Nombre"].Value;
                        }
                        
                    }
                    catch
                    {

                    }

                    try
                    {
                        //nombreReceptor = CEEDestinatario[0].Attributes["Nombre"].Value;
                        if (nombreReceptor == string.Empty)
                        {
                            rfcReceptor = CEEDestinatario[0].Attributes["NumRegIdTrib"].Value;
                        }
                    }
                    catch
                    {

                    } 
                }

                XmlNodeList cce20Receptor = doc.GetElementsByTagName("cce20:Receptor");
                if (cce20Receptor != null)
                {
                    try
                    {
                        if (nombreReceptor == string.Empty)
                        {
                            nombreReceptor = cce20Receptor[0].Attributes["Nombre"].Value;
                        }
                    }
                    catch
                    {

                    }

                    try
                    {
                        //nombreReceptor = CEEDestinatario[0].Attributes["Nombre"].Value;
                        if (nombreReceptor == string.Empty)
                        {
                            rfcReceptor = cce20Receptor[0].Attributes["NumRegIdTrib"].Value;
                        }
                    }
                    catch
                    {

                    }
                }

                XmlNodeList leyendasFisc = doc.GetElementsByTagName("leyendasFisc:Leyenda");
                string LeyendaFiscalTr = string.Empty;
                if (leyendasFisc != null)
                {
                    try
                    {
                        LeyendaFiscalTr = leyendasFisc[0].Attributes["textoLeyenda"].Value;
                    }
                    catch
                    {

                    }
                }

                XmlNodeList CEEDestinatario20 = doc.GetElementsByTagName("cce20:Destinatario");
                if (CEEDestinatario20 != null)
                {
                    try
                    {
                        if (nombreReceptor == string.Empty)
                        {
                            nombreReceptor = CEEDestinatario20[0].Attributes["Nombre"].Value;
                        }
                    }
                    catch
                    {

                    }

                    try
                    {
                        //nombreReceptor = CEEDestinatario[0].Attributes["Nombre"].Value;
                        if (nombreReceptor == string.Empty)
                        {
                            rfcReceptor = CEEDestinatario20[0].Attributes["NumRegIdTrib"].Value;
                        }
                    }
                    catch
                    {

                    }


                }
                cfdiCE ocfdiCE = new cfdiCE();
                ocfdiCE.DATOS.AddDATOSRow(image, cadenaOriginal, cadenaTFD, ""
                    , "", 0
                    , oComprobanteLocal.Emisor.Rfc, oComprobanteLocal.Emisor.Nombre, oComprobanteLocal.Emisor.RegimenFiscal.ToString()
                    , oComprobanteLocal.LugarExpedicion
                    , nombreReceptor!="" ? nombreReceptor:oComprobanteLocal.Receptor.Nombre
                    //, rfcReceptor != "" ? rfcReceptor:oComprobanteLocal.Receptor.Rfc
                    , rfcReceptor
                    , oComprobanteLocal.Fecha.ToString(), oComprobanteLocal.Serie, oComprobanteLocal.Folio.ToString(), oComprobanteLocal.NoCertificado.ToString()
                    , oComprobanteLocal.TipoDeComprobante.ToString(), 0, 0
                    , oComprobanteLocal.Total
                    , UUID, "", FechaTimbrado, noCertificadoSAT, selloSAT
                    , "",RegimenFiscal,UsoCFDI, LugarExpedicion
                    , NumRegIdTrib
                    );

                int Consecutivo = 1;
                foreach (CFDI40.ComprobanteConcepto concepto in oComprobanteLocal.Conceptos)
                {
                    ocfdiCE.conceptos.AddconceptosRow(concepto.Cantidad, concepto.Descripcion.Replace("\n", "").Replace("\r", "")
                        , concepto.NoIdentificacion, concepto.ValorUnitario, concepto.Importe, 0,
                        concepto.ClaveUnidad,concepto.ClaveProdServ,concepto.ClaveUnidad, Consecutivo,"");
                    Consecutivo++;
                }



                oReport.SetDataSource(ocfdiCE);

                foreach (ParameterFieldDefinition definition in oReport.DataDefinition.ParameterFields)
                {
                    ParameterValues myvals = new ParameterValues();
                    ParameterDiscreteValue myDiscrete = new ParameterDiscreteValue();
                    switch (definition.ParameterFieldName)
                    {
                        case "cadenaSAT":
                            //if (!String.IsNullOrEmpty(xml))
                            //{
                            myDiscrete.Value = cadenaTFD;
                            myvals.Add(myDiscrete);
                            definition.CurrentValues.Add(myDiscrete);
                            definition.ApplyCurrentValues(myvals);
                            //}
                            break;
                        case "LeyendaFiscal":
                            //if (!String.IsNullOrEmpty(xml))
                            //{
                            myDiscrete.Value = LeyendaFiscalTr;
                            myvals.Add(myDiscrete);
                            definition.CurrentValues.Add(myDiscrete);
                            definition.ApplyCurrentValues(myvals);
                            //}
                            break;
                        case "Entregar":
                            //if (!String.IsNullOrEmpty(xml))
                            //{
                            myDiscrete.Value = entregar;
                            myvals.Add(myDiscrete);
                            definition.CurrentValues.Add(myDiscrete);
                            definition.ApplyCurrentValues(myvals);
                            //}
                            break;
                    }
                }
                try
                {
                    ExportOptions CrExportOptions = new ExportOptions();
                    DiskFileDestinationOptions CrDiskFileDestinationOptions = new DiskFileDestinationOptions();
                    PdfRtfWordFormatOptions CrFormatTypeOptions = new PdfRtfWordFormatOptions();
                    string pdftr = path.Replace(".xml", ".pdf");
                    pdftr = pdftr.Replace(@"\xml", @"\pdf");
                    CrDiskFileDestinationOptions.DiskFileName = pdftr;
                    CrExportOptions = oReport.ExportOptions;
                    CrExportOptions.ExportDestinationType = ExportDestinationType.DiskFile;
                    CrExportOptions.ExportFormatType = ExportFormatType.PortableDocFormat;
                    CrExportOptions.DestinationOptions = CrDiskFileDestinationOptions;
                    CrExportOptions.FormatOptions = CrFormatTypeOptions;
                    oReport.Export();
                }
                catch (Exception error)
                {
                    string mensaje = "Impresion . ";
                    mensaje += error.Message.ToString();
                    if (error.InnerException != null)
                    {
                        mensaje += " " + error.InnerException.Message.ToString();
                    }

                    MessageBox.Show(mensaje);
                }
                if (verPDF)
                {
                    frmCFDIImpresion ofrmReporte = new frmCFDIImpresion(oReport);
                    ofrmReporte.ShowDialog();
                }
                
            }
            catch (Exception error)
            {
                string mensaje = error.Message.ToString();
                if (error.InnerException != null)
                {
                    mensaje += Environment.NewLine + error.InnerException.ToString();
                }
                MessageBox.Show(mensaje);
            }


        }

        private void pDFToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ver_pdf();
        }

        private void xMLToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ver_xml();
        }

        private void ver_pdf()
        {
            if (dtgrdGeneral.SelectedRows.Count > 0)
            {
                abrir_archivo();
            }
        }

        private void abrir_archivo()
        {
            if (dtgrdGeneral.SelectedRows.Count > 0)
            {
                DataGridViewSelectedRowCollection arrSelectedRows = dtgrdGeneral.SelectedRows;
                string archivoTr = arrSelectedRows[0].Cells["archivo"].Value.ToString();
                if (!File.Exists(archivoTr))
                {
                    //direccion = sDirectory + @"\" + direccion;
                    if (!File.Exists(archivoTr))
                    {
                        MessageBox.Show("No se puede tener acceso a: " + archivoTr);
                        return;
                    }
                }
                frmPDF ofrmPDF = new frmPDF(archivoTr);
                ofrmPDF.Show();
                
            }
        }


        private void ver_xml()
        {
            if (dtgrdGeneral.SelectedRows.Count > 0)
            {
                DataGridViewSelectedRowCollection arrSelectedRows = dtgrdGeneral.SelectedRows;
                string archivoTr = arrSelectedRows[0].Cells["archivo"].Value.ToString();
                if (!File.Exists(archivoTr))
                {
                    //direccion = sDirectory + @"\" + direccion;
                    if (!File.Exists(archivoTr))
                    {
                        MessageBox.Show("No se puede tener acceso a: " + archivoTr);
                        return;
                    }
                }
                frmFacturaXML oObjeto = new frmFacturaXML(archivoTr);
                oObjeto.Show();
            }
        }
    }
}
