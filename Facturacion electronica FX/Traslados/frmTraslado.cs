﻿using System;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Xml.Xsl;
using System.Xml;
using System.Xml.Serialization;
using System.Security.Cryptography.X509Certificates;
using System.IO;
using System.Globalization;
using System.Xml.XPath;
using System.Text.RegularExpressions;
using Ionic.Zip;
using FE_FX.EDICOM_Servicio;
using System.ServiceModel;
using DevComponents.DotNetBar;
using CrystalDecisions.CrystalReports.Engine;
using FE_FX.CFDI;
using com.google.zxing.qrcode;
using CrystalDecisions.Shared;
using System.Net.Mail;
using Generales;
using FE_FX.Clases;
using OfficeOpenXml;
using System.Configuration;

namespace FE_FX
{
    public partial class frmTraslado : Form
    {

        string devolver = "";
        public DataGridViewSelectedRowCollection selecionados;
        string ARCHIVO = "";
        private cEMPRESA ocEMPRESA;
        private cSERIE ocSERIE;
        private cCONEXCION oData = new cCONEXCION("");
        private cCONEXCION oData_ERP = new cCONEXCION("");
        cCERTIFICADO ocCERTIFICADO = new cCERTIFICADO();
        CFDI40.Comprobante oComprobante = new CFDI40.Comprobante();
        private CFDI.cPolizaConfiguracion ocPolizaConfiguracion = new CFDI.cPolizaConfiguracion();
        string sDirectory = AppDomain.CurrentDomain.BaseDirectory + "cPolizaConfiguracion";
        string sDirectoryCE = AppDomain.CurrentDomain.BaseDirectory + "cCEProveedor_";
        string ENTITY_IDlocal = "";
        int indiceSeleccionado = 0;
        Generar40 OGenerar40 = new Generar40();
        bool TrabajarIndependiente = false;
        public frmTraslado(string sConn, string ENTITY_IDp, int indiceSeleccionadop)
        {
            try
            {
                TrabajarIndependiente = bool.Parse(ConfigurationManager.AppSettings["TrabajarIndependiente"].ToString());
            }
            catch
            {

            }

            oData = new cCONEXCION(sConn);
            oData.sConn = sConn;
            ocCERTIFICADO = new cCERTIFICADO();
            InitializeComponent();

            cargar_empresas();
            ENTITY_IDlocal = ENTITY_IDp;
            indiceSeleccionado = indiceSeleccionadop;
            cargarFormatoImpresion();
            FECHA.Value = DateTime.Today;
            cargarMoneda();
            cargarIncoterm();
            cargarCombos();
            cargarSeriePorDefecto();
            //Cargar empresa 
            if (ENTITY_IDp != "")
            {
                for (int i = 0; i < ENTITY_ID.Items.Count; i++)
                {
                    if (ENTITY_IDp == ENTITY_ID.Items[i].ToString())
                    {
                        ENTITY_ID.SelectedItem = ENTITY_ID.Items[i];
                    }
                }
            }
            CargarTipos();

        }

        private void CargarTipos()
        {
            tipoRelacion.Items.Add(new ComboItem { Name = "Nota de crédito de los documentos relacionados", Value = "01" });
            tipoRelacion.Items.Add(new ComboItem { Name = "Nota de débito de los documentos relacionados", Value = "02" });
            tipoRelacion.Items.Add(new ComboItem { Name = "Devolución de mercancía sobre facturas o traslados previos", Value = "03" });
            tipoRelacion.Items.Add(new ComboItem { Name = "Sustitución de los CFDI previos", Value = "04" });
            tipoRelacion.Items.Add(new ComboItem { Name = "Traslados de mercancias facturados previamente", Value = "05" });
            tipoRelacion.Items.Add(new ComboItem { Name = "Factura generada por los traslados previos", Value = "06" });
            tipoRelacion.Items.Add(new ComboItem { Name = "CFDI por aplicación de anticipo", Value = "07" });
            tipoRelacion.SelectedIndex = 0;
        }

        private void cargarSeriePorDefecto()
        {
            ocSERIE = new cSERIE();
            if (ocSERIE.cargar_serie_traslado())
            {
                cargarSerie(ocSERIE);
            }
        }

        private void cargarCombos()
        {
            USO_CFDI.Items.Clear();
            USO_CFDI.Items.Add(new ComboItem { Name = "Adquisición de mercancias", Value = "G01" });
            USO_CFDI.Items.Add(new ComboItem { Name = "Devoluciones, descuentos o bonificaciones", Value = "G02" });
            USO_CFDI.Items.Add(new ComboItem { Name = "Gastos en general", Value = "G03" });
            USO_CFDI.Items.Add(new ComboItem { Name = "Construcciones", Value = "I01" });
            USO_CFDI.Items.Add(new ComboItem { Name = "Mobilario y equipo de oficina por inversiones", Value = "I02" });
            USO_CFDI.Items.Add(new ComboItem { Name = "Equipo de transporte", Value = "I03" });
            USO_CFDI.Items.Add(new ComboItem { Name = "Equipo de computo y accesorios", Value = "I04" });
            USO_CFDI.Items.Add(new ComboItem { Name = "Dados, troqueles, moldes, matrices y herramental", Value = "I05" });
            USO_CFDI.Items.Add(new ComboItem { Name = "Comunicaciones telefónicas", Value = "I06" });
            USO_CFDI.Items.Add(new ComboItem { Name = "Comunicaciones satelitales", Value = "I07" });
            USO_CFDI.Items.Add(new ComboItem { Name = "Otra maquinaria y equipo", Value = "I08" });
            USO_CFDI.Items.Add(new ComboItem { Name = "Honorarios médicos, dentales y gastos hospitalarios.", Value = "D01" });
            USO_CFDI.Items.Add(new ComboItem { Name = "Gastos médicos por incapacidad o discapacidad", Value = "D02" });
            USO_CFDI.Items.Add(new ComboItem { Name = "Gastos funerales.", Value = "D03" });
            USO_CFDI.Items.Add(new ComboItem { Name = "Donativos.", Value = "D04" });
            USO_CFDI.Items.Add(new ComboItem { Name = "Intereses reales efectivamente pagados por créditos hipotecarios (casa habitación).", Value = "D05" });
            USO_CFDI.Items.Add(new ComboItem { Name = "Aportaciones voluntarias al SAR.", Value = "D06" });
            USO_CFDI.Items.Add(new ComboItem { Name = "Primas por seguros de gastos médicos.", Value = "D07" });
            USO_CFDI.Items.Add(new ComboItem { Name = "Gastos de transportación escolar obligatoria.", Value = "D08" });
            USO_CFDI.Items.Add(new ComboItem { Name = "Depósitos en cuentas para el ahorro, primas que tengan como base planes de pensiones.", Value = "D09" });
            USO_CFDI.Items.Add(new ComboItem { Name = "Pagos por servicios educativos (colegiaturas)", Value = "D10" });
            //ComboItem oComboItem = new ComboItem { Name = "Por definir", Value = "P01" };
            ComboItem oComboItemS01 = new ComboItem { Name = "Sin efectos fiscales", Value = "S01" };
            USO_CFDI.Items.Add(oComboItemS01);
            USO_CFDI.SelectedItem = oComboItemS01;
        }

        //private void cargarSeriePorDefecto()
        //{
        //    ocSERIE = new cSERIE();
        //    //if (SocPolizaConfiguracion.SerieDefecto != "")
        //    //{
        //    //    //Validar el primer
        //    //    string serieAcargar = "";
        //    //    string[] serieTRs = ocPolizaConfiguracion.SerieDefecto.Split(',');
        //    //    if (serieTRs.Length == 0)
        //    //    {
        //    //        serieAcargar = ocPolizaConfiguracion.SerieDefecto;
        //    //    }
        //    //    else
        //    //    {
        //    //        int o = 0;
        //    //        foreach (string serieTR in serieTRs)
        //    //        {
        //    //            if (o == indiceSeleccionado)
        //    //            {
        //    //                ocSERIE.cargar_serie(serieTR);
        //    //                serieAcargar = serieTR;

        //    //                break;
        //    //            }
        //    //            o++;

        //    //        }
        //    //    }
        //    //    ocSERIE.cargar_serie(serieAcargar);
        //    //    ID.Text = ocSERIE.consecutivo(ocSERIE.ROW_ID, false);
        //    //    SERIE.Text = ocSERIE.ID;
        //    //}
        //    //if(ocPolizaConfiguracion.FormatoDefecto!=""){
        //    //    FORMATO_IMPRESION.Text = ocPolizaConfiguracion.FormatoDefecto;
        //    //}

        //}

        private void cargarIncoterm()
        {
            //Incoterm.Items.Clear();
            Incoterm.SelectedIndex = 0;
        }

        private void cargarFormatoImpresion()
        {
            string[] filePaths = Directory.GetFiles(System.IO.Path.GetDirectoryName(Application.ExecutablePath), "*.rpt");
            FORMATO_IMPRESION.Items.Clear();
            foreach (string archivo in filePaths)
            {
                FORMATO_IMPRESION.Items.Add(System.IO.Path.GetFileName(archivo));
                FORMATO_IMPRESION.Text = System.IO.Path.GetFileName(archivo);
            }
        }

        private void cargarArchivo(string pathArchivo)
        {
            Comprobante oComprobante;
            //Pasear el comprobante
            try
            {
                XmlSerializer serializer_CFD_3 = new XmlSerializer(typeof(CFDI40.Comprobante));
                TextReader reader = new StreamReader(pathArchivo);
                oComprobante = (Comprobante)serializer_CFD_3.Deserialize(reader);
                reader.Dispose();
                reader.Close();



            }
            catch (Exception e)
            {
                MessageBoxEx.Show("Cargar Comprobante: El archivo " + pathArchivo + " no es un CFDI válido.");
            }
        }


        private void cargar()
        {
            int n;
            cADDENDA oObjeto = new cADDENDA();
            informacion.Text = "Cargando ";
            if (dtgrdGeneral.Rows.Count > 0)
            {
                DialogResult Resultado = MessageBox.Show("Existen líneas desea limpiarlas?", Application.ProductName + "-" + Application.ProductVersion, MessageBoxButtons.YesNo);
                if (Resultado == DialogResult.Yes)
                {
                    dtgrdGeneral.Rows.Clear();
                }
            }

            informacion.Text = "Total " + dtgrdGeneral.Rows.Count.ToString();

        }

        private void cargar_empresas()
        {
            ENTITY_ID.Items.Clear();
            cEMPRESA ocEMPRESA = new cEMPRESA(oData);
            foreach (cEMPRESA registro in ocEMPRESA.todos("", true))
            {
                ENTITY_ID.Items.Add(registro);
            }
            if (ENTITY_ID.Items.Count > 0)
            {
                ENTITY_ID.SelectedIndex = 0;
            }
        }

        private void frmFormatos_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)27)
            {
                this.Close();
            }
            else
            {
                if ((e.KeyChar.ToString() == "<")
                                | (e.KeyChar.ToString() == ">")

                                | (e.KeyChar.ToString() == "'")
                                | (e.KeyChar.ToString() == "#")
                                )
                {
                    e.Handled = true;
                }
            }
        }

        private void buttonX4_Click(object sender, EventArgs e)
        {
            frmVENDOR ofrmVENDOR = new frmVENDOR(oData_ERP.sConn);
            if (ofrmVENDOR.ShowDialog() == DialogResult.OK)
            {
                try
                {
                    rfc.Text = (string)ofrmVENDOR.selecionados[0].Cells[2].Value;
                    string numeroRegistro = "";
                    rfc.Text = "XEXX010101000";
                    if (ofrmVENDOR.selecionados[0].Cells[3].Value != null)
                    {
                        numeroRegistro = ofrmVENDOR.selecionados[0].Cells[3].Value.ToString();
                    }
                    if (numeroRegistro == "")
                    {
                        rfc.Text = "XEXX010101000";
                    }
                    else
                    {
                        NumRegIdTrib.Text = numeroRegistro;
                    }
                }
                catch
                {

                }
                receptor.Text = (string)ofrmVENDOR.selecionados[0].Cells[1].Value;
                if (ofrmVENDOR.selecionados[0].Cells[3].Value != null)
                {
                    NumRegIdTrib.Text = "";
                    try
                    {
                        NumRegIdTrib.Text = (string)ofrmVENDOR.selecionados[0].Cells[3].Value;
                    }
                    catch
                    {

                    }
                }

                cargarCE();
            }
        }

        private void ENTITY_ID_SelectedIndexChanged(object sender, EventArgs e)
        {
            actualizar_conexcion();
        }

        private void actualizar_conexcion()
        {
            ocEMPRESA = ENTITY_ID.SelectedItem as cEMPRESA;
            if (ocEMPRESA != null)
            {
                EMPRESA_NOMBRE.Text = ocEMPRESA.ID;
                oData_ERP = new cCONEXCION(ocEMPRESA.TIPO, ocEMPRESA.SERVIDOR, ocEMPRESA.BD, ocEMPRESA.USUARIO_BD, ocEMPRESA.PASSWORD_BD);
                cargar();
            }
        }

        private void buttonX2_Click_1(object sender, EventArgs e)
        {
            nuevo();
        }

        private void nuevo()
        {
            dtgrdGeneral.Rows.Clear();
            SUBTOTAL.Text = decimal.Parse("0").ToString("####.00");
            TOTAL.Text = decimal.Parse("0").ToString("####.00");
            calcular();
            oComprobante = new CFDI40.Comprobante();
            ocSERIE = new cSERIE();
            if (ocSERIE.cargar_serie_traslado())
            {
                cargarSerie(ocSERIE);
            }
        }

        private void labelX12_Click(object sender, EventArgs e)
        {

        }

        private void cargarMoneda()
        {
            //if (oData_ERP != null)
            //{

            //    string sSQL = "SELECT SHORT_NAME FROM CURRENCY";
            //    DataTable oDataTable = oData_ERP.EjecutarConsulta(sSQL);
            //    if (oDataTable != null)
            //    {
            //        foreach (DataRow oDataRow1 in oDataTable.Rows)
            //        {
            //            MONEDA.Items.Add(oDataRow1["SHORT_NAME"].ToString());
            //        }
            //    }
            //}
        }

        private void buttonX1_Click(object sender, EventArgs e)
        {
            buscarSerie();
        }

        private void buscarSerie()
        {

            frmSeries oBuscador = new frmSeries(oData.sConn, "S", ocEMPRESA.ROW_ID);
            if (oBuscador.ShowDialog() == DialogResult.OK)
            {
                string ROW_ID = (string)oBuscador.selecionados[0].Cells["ROW_ID"].Value;
                string IDtr = (string)oBuscador.selecionados[0].Cells["ID"].Value;
                SERIE.Text = IDtr;

                ocSERIE = new cSERIE();

                ocSERIE.cargar_serie(IDtr);
                cargarSerie(ocSERIE);

            }
        }

        private void cargarSerie(cSERIE ocSERIE)
        {
            ID.Text = ocSERIE.consecutivo(ocSERIE.ROW_ID, false);
            ocEMPRESA = new cEMPRESA(oData);
            ocCERTIFICADO = new cCERTIFICADO();
            ocCERTIFICADO.cargar(ocSERIE.ROW_ID_CERTIFICADO);
            ocEMPRESA.cargar(ocCERTIFICADO.ROW_ID_EMPRESA);

            actualizar_conexcion();
        }

        private void buttonX10_Click(object sender, EventArgs e)
        {
            insertarConcepto();
        }

        private void insertarConcepto()
        {
            frmCFDIConceptoCCE oFacturaLinea = new frmCFDIConceptoCCE(oData_ERP.sConn, ocEMPRESA.ROW_ID
                , oData.sConn);
            if (oFacturaLinea.ShowDialog() == DialogResult.OK)
            {
                insertar_linea(oFacturaLinea.cantidadP, oFacturaLinea.unidadP, oFacturaLinea.descripcionP
                    , oFacturaLinea.valorUnitarioP, oFacturaLinea.importeP, oFacturaLinea.noIdentificacionP
                    , oFacturaLinea.fraccionArancelariaP
                    , oFacturaLinea.aduanaP, oFacturaLinea.fechaP, oFacturaLinea.numeroP
                    , oFacturaLinea.ClaveProdServP, oFacturaLinea.ClaveUnidadP, oFacturaLinea.SubmodeloP, oFacturaLinea.MarcaP
                    , oFacturaLinea.ModeloP, oFacturaLinea.NumeroSerieP
                    , oFacturaLinea.ValorUnitarioAduanaP, oFacturaLinea.ValorDolaresP
                    , oFacturaLinea.CantidadAduanaP
                    );

            }
        }

        private void insertar_linea(decimal cantidad, string unidad, string descripcion
    , decimal valorUnitario, decimal importe, string noIdentificacion
            , string fraccion
            , string aduana, string fecha, string numero
            , string ClaveProdServ, string ClaveUnidad, string Submodelo, string Marca, string Modelo, string NumeroSerie
            , string ValorUnitarioAduana, string ValorDolares, string CantidadAduana
    )
        {
            int n;
            n = dtgrdGeneral.Rows.Add();
            for (int i = 0; i < dtgrdGeneral.Columns.Count; i++)
            {
                dtgrdGeneral.Rows[n].Cells[i].Value = "";
            }

            dtgrdGeneral.Rows[n].Cells["cantidad"].Value = cantidad.ToString();
            dtgrdGeneral.Rows[n].Cells["unidad"].Value = unidad;
            dtgrdGeneral.Rows[n].Cells["descripcion"].Value = descripcion;
            dtgrdGeneral.Rows[n].Cells["valorUnitario"].Value = valorUnitario.ToString();
            dtgrdGeneral.Rows[n].Cells["importe"].Value = importe.ToString();
            dtgrdGeneral.Rows[n].Cells["noIdentificacion"].Value = noIdentificacion;
            dtgrdGeneral.Rows[n].Cells["fraccionArancelaria"].Value = fraccion;

            dtgrdGeneral.Rows[n].Cells["ClaveProdServ"].Value = ClaveProdServ;
            dtgrdGeneral.Rows[n].Cells["ClaveUnidad"].Value = ClaveUnidad;
            dtgrdGeneral.Rows[n].Cells["Submodelo"].Value = Submodelo;
            dtgrdGeneral.Rows[n].Cells["Marca"].Value = Marca;
            dtgrdGeneral.Rows[n].Cells["Modelo"].Value = Modelo;
            dtgrdGeneral.Rows[n].Cells["NumeroSerie"].Value = NumeroSerie;

            dtgrdGeneral.Rows[n].Cells["informacionAduaneraAduana"].Value = aduana;
            dtgrdGeneral.Rows[n].Cells["informacionAduaneraFecha"].Value = fecha;
            dtgrdGeneral.Rows[n].Cells["informacionAduaneraNumero"].Value = numero;

            dtgrdGeneral.Rows[n].Cells["ValorUnitarioAduana"].Value = ValorUnitarioAduana;
            dtgrdGeneral.Rows[n].Cells["ValorDolares"].Value = ValorDolares;

            dtgrdGeneral.Rows[n].Cells["CantidadAduana"].Value = CantidadAduana;

            calcular();
        }

        private void calcular()
        {
            double SUBTOTAL_tr = 0;
            double IMPUESTO_tr = 0;
            double RETENCION_tr = 0;
            double DESCUENTO_tr = 0;
            double TOTAL_tr = 0;
            for (int i = 0; i < dtgrdGeneral.Rows.Count; i++)
            {
                SUBTOTAL_tr += double.Parse(dtgrdGeneral.Rows[i].Cells["importe"].Value.ToString().Replace("$", ""));
            }
            //Calculos de los IEPS
            double IEPS = double.Parse(IEPStr.Text);
            double IMPUESTO_IEPS = 0;
            switch (IMPUESTO_DATO.SelectedIndex)
            {
                case 0:
                    //IVA 16%
                    IMPUESTO_tr = SUBTOTAL_tr * 0.16;
                    IMPUESTO_IEPS = IEPS * 0.16;
                    break;
                case 1:
                    //RETENCION IVA 16%
                    IMPUESTO_tr = SUBTOTAL_tr * 0.16;
                    RETENCION_tr = IMPUESTO_tr;
                    IMPUESTO_IEPS = IEPS * 0.16;
                    break;
                case 2:
                    //0%
                    IMPUESTO_tr = SUBTOTAL_tr * 0;
                    IEPS += IEPS * 0;
                    break;
                case 3:
                    //11%
                    IMPUESTO_tr = SUBTOTAL_tr * 0.11;
                    IMPUESTO_IEPS = IEPS * 0.11;
                    break;
                case 4:
                    //RETENCION 11%
                    IMPUESTO_tr = SUBTOTAL_tr * 0.11;
                    RETENCION_tr = IMPUESTO_tr;
                    IMPUESTO_IEPS = IEPS * 0.11;
                    break;
            }

            DESCUENTO_tr = 0;
            if (Globales.IsNumeric(DESCUENTO.Text))
            {

                DESCUENTO_tr = double.Parse(DESCUENTO.Text.ToString());
                DESCUENTO.Text = cUTILERIA.Trunca_y_formatea(decimal.Parse(DESCUENTO_tr.ToString()));
            }
            //if (frmTrasladoAutomatico.Checked)
            //{
            //    DESCUENTO.Text = cUTILERIA.Trunca_y_formatea(decimal.Parse(SUBTOTAL_tr.ToString()));
            //    DESCUENTO_tr = double.Parse(SUBTOTAL_tr.ToString());
            //}

            IMPUESTO_tr += IMPUESTO_IEPS;

            TOTAL_tr = (Math.Round(SUBTOTAL_tr, 3) + IMPUESTO_tr + IEPS) - RETENCION_tr - DESCUENTO_tr;

            SUBTOTAL.Text = SUBTOTAL_tr.ToString(); // cUTILERIA.Trunca_y_formatea(decimal.Parse(SUBTOTAL_tr.ToString()));
            IMPUESTO.Text = IMPUESTO_tr.ToString();  //cUTILERIA.Trunca_y_formatea(decimal.Parse(IMPUESTO_tr.ToString()));
            RETENCION.Text = RETENCION_tr.ToString(); //cUTILERIA.Trunca_y_formatea(decimal.Parse(RETENCION_tr.ToString()));

            IMPUESTO.Text = cUTILERIA.Trunca_y_formatea(decimal.Parse(IMPUESTO_tr.ToString()));

            TOTAL.Text = cUTILERIA.Trunca_y_formatea(decimal.Parse(TOTAL_tr.ToString()));

            //Formatear
            SUBTOTAL_tr = Math.Round(SUBTOTAL_tr, 3);
            SUBTOTAL.Text = cUTILERIA.Trunca_y_formatea(decimal.Parse(SUBTOTAL_tr.ToString()));
            TOTAL.Text = cUTILERIA.Trunca_y_formatea(decimal.Parse(TOTAL_tr.ToString()));
            calcularTotalSinDescuento();
        }

        private void calcularTotalSinDescuento()
        {
            decimal DESCUENTO_tr = 0;
            for (int i = 0; i < dtgrdGeneral.Rows.Count; i++)
            {
                decimal importetr = decimal.Parse(dtgrdGeneral.Rows[i].Cells["importe"].Value.ToString().Replace("$", ""));
                if (importetr > 0)
                {
                    DESCUENTO_tr += importetr;
                }
            }
            totalSinDescuento.Text = oData.Trunca_y_formatea(DESCUENTO_tr);
        }

        private void dtgrdGeneral_CellDoubleClick_1(object sender, DataGridViewCellEventArgs e)
        {
            editar_linea();
        }


        private void editar_linea()
        {
            if (dtgrdGeneral.SelectedRows.Count > 0)
            {
                DataGridViewSelectedRowCollection arrSelectedRows = dtgrdGeneral.SelectedRows;
                //string ROW_ID = (string)arrSelectedRows[0].Cells[0].Value;

                frmCFDIConceptoCCE oFacturaLinea = new frmCFDIConceptoCCE(
                    decimal.Parse(arrSelectedRows[0].Cells["cantidad"].Value.ToString())
                    , arrSelectedRows[0].Cells["unidad"].Value.ToString()
                    , arrSelectedRows[0].Cells["noIdentificacion"].Value.ToString()
                    , arrSelectedRows[0].Cells["descripcion"].Value.ToString()
                    , decimal.Parse(arrSelectedRows[0].Cells["valorUnitario"].Value.ToString())
                    , decimal.Parse(arrSelectedRows[0].Cells["importe"].Value.ToString())
                    , arrSelectedRows[0].Cells["fraccionArancelaria"].Value.ToString()
                    , oData_ERP.sConn
                    , ocEMPRESA.ROW_ID
                    , oData.sConn
                    , arrSelectedRows[0].Cells["informacionAduaneraAduana"].Value.ToString()
                    , arrSelectedRows[0].Cells["informacionAduaneraFecha"].Value.ToString()
                    , arrSelectedRows[0].Cells["informacionAduaneraNumero"].Value.ToString()
                    , arrSelectedRows[0].Cells["ClaveProdServ"].Value.ToString()
                    , arrSelectedRows[0].Cells["ClaveUnidad"].Value.ToString()
                    , arrSelectedRows[0].Cells["Submodelo"].Value.ToString()
                    , arrSelectedRows[0].Cells["Marca"].Value.ToString()
                    , arrSelectedRows[0].Cells["Modelo"].Value.ToString()
                    , arrSelectedRows[0].Cells["NumeroSerie"].Value.ToString()
                    , arrSelectedRows[0].Cells["ValorUnitarioAduana"].Value.ToString()
                    , arrSelectedRows[0].Cells["ValorDolares"].Value.ToString()
                    , arrSelectedRows[0].Cells["CantidadAduana"].Value.ToString()
                    );
                if (oFacturaLinea.ShowDialog() == DialogResult.OK)
                {
                    arrSelectedRows[0].Cells["cantidad"].Value = oFacturaLinea.cantidadP;
                    arrSelectedRows[0].Cells["unidad"].Value = oFacturaLinea.unidadP;
                    arrSelectedRows[0].Cells["noIdentificacion"].Value = oFacturaLinea.noIdentificacionP;
                    arrSelectedRows[0].Cells["descripcion"].Value = oFacturaLinea.descripcionP;
                    arrSelectedRows[0].Cells["valorUnitario"].Value = oFacturaLinea.valorUnitarioP;
                    arrSelectedRows[0].Cells["importe"].Value = oFacturaLinea.importeP;
                    arrSelectedRows[0].Cells["fraccionArancelaria"].Value = oFacturaLinea.fraccionArancelariaP;

                    arrSelectedRows[0].Cells["informacionAduaneraAduana"].Value = oFacturaLinea.aduanaP;
                    arrSelectedRows[0].Cells["informacionAduaneraFecha"].Value = oFacturaLinea.fechaP;
                    arrSelectedRows[0].Cells["informacionAduaneraNumero"].Value = oFacturaLinea.numeroP;

                    arrSelectedRows[0].Cells["ClaveProdServ"].Value = oFacturaLinea.ClaveProdServP;
                    arrSelectedRows[0].Cells["ClaveUnidad"].Value = oFacturaLinea.ClaveUnidadP;
                    arrSelectedRows[0].Cells["Submodelo"].Value = oFacturaLinea.SubmodeloP;
                    arrSelectedRows[0].Cells["Marca"].Value = oFacturaLinea.MarcaP;
                    arrSelectedRows[0].Cells["Modelo"].Value = oFacturaLinea.ModeloP;
                    arrSelectedRows[0].Cells["NumeroSerie"].Value = oFacturaLinea.NumeroSerieP;

                    arrSelectedRows[0].Cells["ValorUnitarioAduana"].Value = oFacturaLinea.ValorUnitarioAduanaP;
                    arrSelectedRows[0].Cells["ValorDolares"].Value = oFacturaLinea.ValorDolaresP;

                    arrSelectedRows[0].Cells["CantidadAduana"].Value = oFacturaLinea.CantidadAduanaP;

                    calcular();
                }

            }
        }

        private void IMPUESTO_DATO_SelectedIndexChanged(object sender, EventArgs e)
        {
            calcular();
        }

        private void buttonX5_Click(object sender, EventArgs e)
        {
            if (generar())
            {
                if (timbrar())
                {

                    imprimir(false);
                    guardarCE();

                }
            }
        }

        private void guardarCE()
        {
            if (NumRegIdTrib.Text != "")
            {
                string sDirectoryCE_tr = AppDomain.CurrentDomain.BaseDirectory + "cCEProveedor_" + NumRegIdTrib.Text;
                XmlSerializer serializer = new XmlSerializer(typeof(cCEProveedor));
                StreamWriter writer = new StreamWriter(sDirectoryCE_tr);
                cCEProveedor ocCEProveedor = new cCEProveedor();
                ocCEProveedor.Calle = calle.Text;
                ocCEProveedor.CodigoPostal = CodigoPostal.Text;
                ocCEProveedor.Estado = EstadoCE.Text;
                ocCEProveedor.NoExterior = noExterior.Text;
                ocCEProveedor.NoInterior = noInterior.Text;
                ocCEProveedor.NumRegIdTrib = NumRegIdTrib.Text;
                ocCEProveedor.Pais = Pais.Text;

                serializer.Serialize(writer, ocCEProveedor);
                writer.Close();
            }

        }


        private void cargarCE()
        {
            if (NumRegIdTrib.Text != "")
            {
                XmlSerializer serializer = new XmlSerializer(typeof(cCEProveedor));
                try
                {
                    string sDirectoryCE_tr = AppDomain.CurrentDomain.BaseDirectory + "cCEProveedor_" + NumRegIdTrib.Text;

                    TextReader reader = new StreamReader(sDirectory);
                    cCEProveedor ocCEProveedor = new cCEProveedor();
                    ocCEProveedor = (cCEProveedor)serializer.Deserialize(reader);
                    reader.Close();

                    calle.Text = ocCEProveedor.Calle;
                    CodigoPostal.Text = ocCEProveedor.CodigoPostal;
                    EstadoCE.Text = ocCEProveedor.Estado;
                    noExterior.Text = ocCEProveedor.NoExterior;
                    noInterior.Text = ocCEProveedor.NoInterior;
                    NumRegIdTrib.Text = ocCEProveedor.NumRegIdTrib;
                    Pais.Text = ocCEProveedor.Pais;


                    DestinatarioCalle.Text = ocCEProveedor.Calle;
                    DestinatarioCP.Text = ocCEProveedor.CodigoPostal;
                    DestinatarioEstado.Text = ocCEProveedor.Estado;
                    DestinatarioNumRegIdTrib.Text = NumRegIdTrib.Text;
                    DestinatarioNombre.Text = receptor.Text;
                    DestinatarioPais.Text = ocCEProveedor.Pais;

                }
                catch (Exception oException)
                {
                    ErrorFX.mostrar(oException, false, true, "frmTraslado - ");
                }
            }
        }

        private bool timbrar()
        {
            try
            {
                //Timbrar
                informacion.Text = "Timbrando el XML " + ID.Text;
                bool usar_edicom = false;
                int opcion = 1;
                if (ESTADO.Text.Contains("BORRADOR"))
                {
                    usar_edicom = true;
                    opcion = 2;
                }
                try
                {
                    //Buscar usuarios de EDICOM
                    bool existe = Globales.cargarTimbrado("EDICOM");
                    if (!existe)
                    {

                        string factura = oComprobante.Serie + oComprobante.Folio;
                        return timbrarSolucionFactible(opcion, PATH.Text, factura);
                    }
                    else
                    {
                        if (usar_edicom)
                        {
                            usar_edicom = this.generar_CFDI(2, Globales.oCFDI_USUARIO.USUARIO, cENCRIPTACION.Decrypt(Globales.oCFDI_USUARIO.PASSWORD));
                        }
                        else
                        {
                            usar_edicom = this.generar_CFDI(1, Globales.oCFDI_USUARIO.USUARIO, cENCRIPTACION.Decrypt(Globales.oCFDI_USUARIO.PASSWORD));
                            if (usar_edicom)
                            {
                                ocSERIE.incrementar(ocSERIE.ROW_ID);
                            }
                        }
                    }

                }
                catch (Exception ex)
                {
                    MessageBox.Show("Error timbrando en EDICOM" + Environment.NewLine + ex.InnerException.ToString(), Application.ProductVersion, MessageBoxButtons.OK);
                    return false;
                }
                if (!ESTADO.Text.Contains("BORRADOR"))
                {
                    return true;
                }
                return false;
            }
            catch (Exception oException)
            {
                ErrorFX.mostrar(oException, true, true, "frmTraslado - 781 - ");
            }
            return false;
        }

        private bool timbrarSolucionFactible(int opcion, string archivo, string factura)
        {
            string prod_endpoint = "TimbradoEndpoint_PRODUCCION";
            string test_endpoint = "TimbradoEndpoint_TESTING";
            if (!File.Exists(archivo))
            {
                ErrorFX.mostrar("El archivo: " + archivo + " no existe", true, true, false);
                return false;
            }
            //Si recibe error 417 deberá descomentar la linea a continuación
            System.Net.ServicePointManager.Expect100Continue = false;

            //El paquete o namespace en el que se encuentran las clases
            //será el que se define al agregar la referencia al WebService,
            //en este ejemplo es: com.sf.ws.Timbrado

            Solucion_Factible.TimbradoPortTypeClient portClient = null;
            portClient = (opcion == 1)
                ? new Solucion_Factible.TimbradoPortTypeClient(prod_endpoint)
                : portClient = new Solucion_Factible.TimbradoPortTypeClient(test_endpoint);

            try
            {
                System.Console.WriteLine("Sending request...");
                System.Console.WriteLine("EndPoint = " + portClient.Endpoint.Address);

                //Comprimir Archivo
                string Archivo_Tmp = factura + ".xml";
                File.Copy(archivo, Archivo_Tmp, true);
                string ARCHIVO_zip = Archivo_Tmp.ToUpper().Replace("XML", "ZIP");
                using (ZipFile zip = new ZipFile())
                {
                    zip.AddFile(Archivo_Tmp);
                    zip.Save(ARCHIVO_zip);
                }
                //Fin de Compresion de Archivo
                File.Delete(Archivo_Tmp);

                byte[] ARCHIVO_Leido = ReadBinaryFile(ARCHIVO_zip);
                File.Delete(ARCHIVO_zip);
                Solucion_Factible.CFDICertificacion response;
                if (opcion == 2)
                {
                    response = portClient.timbrar("testing@solucionfactible.com", "timbrado.SF.16672", ARCHIVO_Leido, true);
                }
                else
                {
                    //Cargar los datos del SL
                    Globales.cargarUsuarioSL();
                    response = portClient.timbrar(Globales.oCFDI_USUARIO.USUARIO, Globales.oCFDI_USUARIO.PASSWORD, ARCHIVO_Leido, true);

                }

                System.Console.WriteLine("Información de la transacción");
                System.Console.WriteLine(response.status);
                System.Console.WriteLine(response.mensaje);
                System.Console.WriteLine("Resultados recibidos" + response.resultados.Length);

                Solucion_Factible.CFDIResultadoCertificacion[] resultados = response.resultados;
                if (resultados[0] != null)
                {
                    if (resultados[0].status == 200)
                    {
                        guardarArchivoSolucionFactible(resultados[0].cfdiTimbrado, archivo, factura);
                    }
                    else
                    {
                        Exception oException = new Exception(resultados[0].mensaje);
                        cargarErrorCompleto(oException);
                        ErrorFX.mostrar(oException, true, true, "Factura: " + factura, true);
                        return false;
                    }
                }
                return true;
            }
            catch (Exception oException)
            {
                cargarErrorCompleto(oException);
                ErrorFX.mostrar(oException, true, true, "frmCFDIProforma.cs - 2231 - Timbrado Solución Factible", true);
                return false;
            }
        }
        private void cargarErrorCompleto(Exception error)
        {
            string mensaje = error.Message.ToString();
            if (error.InnerException != null)
            {
                mensaje += Environment.NewLine + error.InnerException.Message.ToString();
            }
            mensaje = mensaje.Replace("'", "");
            this.errorTimbrado.Text = mensaje;
        }
        private void guardarArchivoSolucionFactible(byte[] PROCESO, string archivo, string factura)
        {
            try
            {
                File.WriteAllBytes(archivo, PROCESO);

            }
            catch (Exception oException)
            {
                ErrorFX.mostrar(oException, true, true, "frmTraslado.cs - 863 - Solucion Factible", true);
            }

        }

        public bool generar_CFDI(int Opcion, string EDICOM_USUARIO, string EDICOM_PASSWORD)
        {
            try
            {
                //Comprimir Archivo
                string Archivo_Tmp = oComprobante.Serie + oComprobante.Folio + ".xml";
                File.Copy(ARCHIVO, Archivo_Tmp, true);
                string ARCHIVO_zip = Archivo_Tmp.ToUpper().Replace("XML", "ZIP");
                using (ZipFile zip = new ZipFile())
                {
                    zip.AddFile(Archivo_Tmp);
                    zip.Save(ARCHIVO_zip);
                }
                //Fin de Compresion de Archivo
                File.Delete(Archivo_Tmp);

                byte[] ARCHIVO_Leido = ReadBinaryFile(ARCHIVO_zip);
                File.Delete(ARCHIVO_zip);
                CFDiClient oCFDiClient = new CFDiClient();

                byte[] PROCESO;
                string MENSAJE = "";

                switch (Opcion)
                {
                    case 1:
                        try
                        {
                            informacion.Text = "Generando CFDI ";
                            MENSAJE = "";
                            PROCESO = oCFDiClient.getCfdi(EDICOM_USUARIO, EDICOM_PASSWORD, ARCHIVO_Leido);
                            guardar_archivo(PROCESO, oComprobante.Serie + oComprobante.Folio, ARCHIVO);
                        }
                        catch (FaultException oException)
                        {

                            CFDiException oCFDiException = new CFDiException();
                            MessageBox.Show(oException.Message, Application.ProductName + "-" + Application.ProductVersion, MessageBoxButtons.OK, MessageBoxIcon.Error);
                            errorTimbrado.Text = "Error " + oException.Message + DateTime.Now.ToShortDateString() + " " + DateTime.Now.ToLongTimeString();
                            return false;
                        }

                        break;
                    case 2:
                        try
                        {
                            informacion.Text = "Generando CFDI Test ";
                            MENSAJE = " TEST ";
                            PROCESO = oCFDiClient.getCfdiTest(EDICOM_USUARIO, EDICOM_PASSWORD, ARCHIVO_Leido);
                            guardar_archivo(PROCESO, oComprobante.Serie + oComprobante.Folio, ARCHIVO);
                        }
                        catch (FaultException oException)
                        {

                            CFDiException oCFDiException = new CFDiException();
                            MessageBox.Show(oException.Message, Application.ProductName + "-" + Application.ProductVersion, MessageBoxButtons.OK, MessageBoxIcon.Error);
                            informacion.Text = "Error " + DateTime.Now.ToShortDateString() + " " + DateTime.Now.ToLongTimeString();
                            errorTimbrado.Text = "Error " + oException.Message + DateTime.Now.ToShortDateString() + " " + DateTime.Now.ToLongTimeString();
                            return false;
                        }
                        break;
                    case 3:
                        try
                        {
                            informacion.Text = "Generando Timbre CFDI  ";
                            PROCESO = oCFDiClient.getTimbreCfdi(EDICOM_USUARIO, EDICOM_PASSWORD, ARCHIVO_Leido);
                        }
                        catch (Exception oException)
                        {

                            CFDiException oCFDiException = new CFDiException();
                            MessageBox.Show(oException.Message, Application.ProductName + "-" + Application.ProductVersion, MessageBoxButtons.OK, MessageBoxIcon.Error);
                            informacion.Text = "Error " + DateTime.Now.ToShortDateString() + " " + DateTime.Now.ToLongTimeString();
                            return false;
                        }
                        break;
                    case 4:
                        try
                        {
                            informacion.Text = "Generando Timbre CFDI Test ";
                            PROCESO = oCFDiClient.getTimbreCfdiTest(EDICOM_USUARIO, EDICOM_PASSWORD, ARCHIVO_Leido);
                        }
                        catch (Exception oException)
                        {

                            CFDiException oCFDiException = new CFDiException();
                            MessageBox.Show(oException.Message, Application.ProductName + "-" + Application.ProductVersion, MessageBoxButtons.OK, MessageBoxIcon.Error);
                            informacion.Text = "Error " + DateTime.Now.ToShortDateString() + " " + DateTime.Now.ToLongTimeString();
                            return false;
                        }
                        break;
                }

                informacion.Text = "Generado y Sellado";
                return true;
            }
            catch (Exception oException)
            {
                MessageBox.Show("929 - Generando CFDI " + oException.Message, Application.ProductName + "-" + Application.ProductVersion, MessageBoxButtons.OK, MessageBoxIcon.Error);
                informacion.Text = "Error " + DateTime.Now.ToShortDateString() + " " + DateTime.Now.ToLongTimeString();
                return false;
            }
        }

        private void guardar_archivo(byte[] PROCESO, string INVOICE_ID, string ARCHIVO)
        {
            try
            {

                //Guardar los PROCESO

                File.WriteAllBytes("TEMPORAL.ZIP", PROCESO);
                //Descomprimir
                String TargetDirectory = "TEMPORAL";
                using (Ionic.Zip.ZipFile zip = Ionic.Zip.ZipFile.Read("TEMPORAL.ZIP"))
                {
                    zip.ExtractAll(TargetDirectory, Ionic.Zip.ExtractExistingFileAction.OverwriteSilently);
                }

                //Copiar el ARCHIVO Descrompimido por otro
                string ARCHIVO_descomprimido = TargetDirectory + @"\" + "SIGN_" + INVOICE_ID + ".XML";

                File.Copy(ARCHIVO_descomprimido, ARCHIVO, true);
                PATH.Text = ARCHIVO;
                File.Delete(ARCHIVO_descomprimido);
                File.Delete("TEMPORAL.ZIP");

            }
            catch (Exception oException)
            {
                ErrorFX.mostrar(oException, true, true, "frmTraslado - 781 - ");
            }

        }



        private bool FileExists(string pPath)
        {
            string sPath = pPath;
            try
            {
                if (File.Exists(sPath))
                {
                    return true;
                }
                else
                {
                    return false;
                }

            }
            catch
            {
                return false;
            }
        }

        public string validacion(string cadena)
        {
            string devolucion = "";
            devolucion = cadena.ToUpper();

            devolucion = devolucion.Replace("á", "A");
            devolucion = devolucion.Replace("é", "E");
            devolucion = devolucion.Replace("í", "I");
            devolucion = devolucion.Replace("ó", "O");
            devolucion = devolucion.Replace("ú", "U");
            devolucion = devolucion.Replace("Á", "A");
            devolucion = devolucion.Replace("É", "E");
            devolucion = devolucion.Replace("Í", "I");
            devolucion = devolucion.Replace("Ó", "O");
            devolucion = devolucion.Replace("Ú", "U");
            devolucion = devolucion.Replace(">", "");
            devolucion = devolucion.Replace("<", "");
            //devolucion = devolucion.Replace("&", "");
            devolucion = devolucion.Replace("'", "");
            devolucion = devolucion.Replace("#", "");
            devolucion = devolucion.Replace("|", "");
            //devolucion = HtmlEncode(devolucion);

            devolucion = Regex.Replace(devolucion, @"[\u0000-\u001F]", string.Empty);


            return devolucion;
        }




        private byte[] ReadBinaryFile(string fileName)
        {
            if (File.Exists(fileName))
            {
                try
                {
                    ///Open and read a file&#12290;
                    FileStream fileStream = File.OpenRead(fileName);
                    byte[] archivo = ConvertStreamToByteBuffer(fileStream);
                    fileStream.Close();
                    return archivo;
                }
                catch (Exception ex)
                {
                    return new byte[0];
                }
            }
            else
            {
                return new byte[0];
            }
        }
        public byte[] ConvertStreamToByteBuffer(System.IO.Stream theStream)
        {
            int b1;
            System.IO.MemoryStream tempStream = new System.IO.MemoryStream();
            while ((b1 = theStream.ReadByte()) != -1)
            {
                tempStream.WriteByte(((byte)b1));
            }
            return tempStream.ToArray();
        }
        public bool generar()
        {
            try
            {
                //Valor por defecto del valorUnitario es de 2
                string INVOICE_ID = ID.Text;
                string sDirectory = AppDomain.CurrentDomain.BaseDirectory;
                if (ocSERIE == null)
                {
                    MessageBox.Show("Seleccione la serie", Application.ProductName + "-" + Application.ProductVersion, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                    return false;
                }
                if (String.IsNullOrEmpty(rfc.Text))
                {
                    MessageBox.Show("Seleccione el RFC ", Application.ProductName + "-" + Application.ProductVersion, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                    return false;
                }
                if (dtgrdGeneral.Rows.Count == 0)
                {
                    MessageBox.Show("Ingrese al menos 1 línea", Application.ProductName + "-" + Application.ProductVersion, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                    return false;
                }


                //Cargar Certificado
                ocCERTIFICADO = new cCERTIFICADO();
                ocCERTIFICADO.cargar(ocSERIE.ROW_ID_CERTIFICADO);
                cEMPRESA ocEMPRESA = new cEMPRESA(oData);
                ocEMPRESA.cargar(ocCERTIFICADO.ROW_ID_EMPRESA);


                if (!FileExists(sDirectory + "/XSLT/cadenaoriginal_4_0.xslt"))
                {
                    MessageBox.Show("No se tiene acceso al archivo "
                        + Environment.NewLine
                        + sDirectory + "/XSLT/cadenaoriginal_4_0.xslt ", Application.ProductName + " - " + Application.ProductVersion, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);

                    return false;
                }
                //MessageBox.Show("Cargado XLT");
                XmlSerializerNamespaces myNamespaces = new XmlSerializerNamespaces();
                myNamespaces.Add("cfdi", "http://www.sat.gob.mx/cfd/4");
                //MessageBox.Show("Cargado XLT " + ocCERTIFICADO.CERTIFICADO);
                if (!File.Exists(ocCERTIFICADO.CERTIFICADO))
                {
                    MessageBox.Show("No se tiene acceso al archivo " + ocCERTIFICADO.CERTIFICADO + "", Application.ProductName + "-" + Application.ProductVersion, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                    return false;
                }
                X509Certificate cert = X509Certificate.CreateFromCertFile(ocCERTIFICADO.CERTIFICADO);
                string NoSerie = cert.GetSerialNumberString();

                StringBuilder SerieHex = new StringBuilder();
                for (int i = 0; i <= NoSerie.Length - 2; i += 2)
                {
                    SerieHex.Append(Convert.ToString(Convert.ToChar(Int32.Parse(NoSerie.Substring(i, 2), System.Globalization.NumberStyles.HexNumber))));
                }
                //MessageBox.Show("Comprobante");
                string NO_CERTIFICADO = SerieHex.ToString();

                DateTime INVOICE_DATE = FECHA.Value;
                IFormatProvider culture = new CultureInfo("es-MX", true);

                DateTime CREATE_DATE = DateTime.Now;

                INVOICE_DATE = DateTime.Parse(INVOICE_DATE.Day.ToString() + "/" + INVOICE_DATE.Month.ToString() + "/" + INVOICE_DATE.Year.ToString() + " " + CREATE_DATE.ToString("HH:mm:ss"), culture);

                oComprobante.Fecha = DateTime.Parse("2018-02-21T23:00:00");// INVOICE_DATE; // INVOICE_DATE;//FECHA_FACTURA;

                //MessageBox.Show("Comprobante1");
                XmlSerializer serializer = new XmlSerializer(typeof(CFDI40.Comprobante));

                oComprobante = new CFDI40.Comprobante();

                DateTime FECHA_FACTURA = new DateTime();

                //Serie
                oComprobante.Serie = ocSERIE.ID;
                //Folio
                oComprobante.Folio = ID.Text;
                oComprobante.Moneda = MONEDA.Text;
                oComprobante.TipoCambioSpecified = true;
                oComprobante.TipoCambio = Math.Round(decimal.Parse(TIPO_CAMBIO.Text), 4);
                oComprobante.Fecha = DateTime.Parse(DateTime.Now.Year.ToString() + "-" + DateTime.Now.Month.ToString() + "-" + DateTime.Now.Day.ToString() + "T00:00:00"); ; //DateTime.Now;
                //if ((oComprobante.condicionesDePago == "")
                //    ||
                //    (oComprobante.condicionesDePago == null))
                //{
                //    oComprobante.condicionesDePago = "PAGO EN UNA SOLA EXHIBICION";
                //}


                //CDFI 3.2
                oComprobante.LugarExpedicion = ocSERIE.CP;

                INVOICE_DATE = FECHA.Value;
                //oComprobante.Fecha = DateTime.Today.AddHours(-1);

                oComprobante.TipoDeComprobante = CFDI40.c_TipoDeComprobante.T;
                oComprobante.MetodoPago = null;
                oComprobante.Exportacion = "01";

                if (decimal.Parse(DESCUENTO.Text) != 0)
                {
                    oComprobante.Descuento = decimal.Parse(DESCUENTO.Text);
                    oComprobante.DescuentoSpecified = true;
                }


                cert = X509Certificate.CreateFromCertFile(ocCERTIFICADO.CERTIFICADO);
                string Certificado64 = System.Convert.ToBase64String(cert.GetRawCertData());
                oComprobante.Certificado = Certificado64;
                //MessageBox.Show("Comprobante9");
                //Version 3
                oComprobante.NoCertificado = validacion(NO_CERTIFICADO);
                //SubTotal
                oComprobante.SubTotal = 0;
                oComprobante.Descuento = 0;
                oComprobante.Total = 0;

                //Elemento Emisor
                CFDI40.ComprobanteEmisor oComprobanteEmisor = new CFDI40.ComprobanteEmisor();

                //Cargar los datos de la Compañia apartir del Certificado
                cert = X509Certificate.CreateFromCertFile(ocCERTIFICADO.CERTIFICADO);
                string Dato = cert.GetName();
                //MessageBox.Show("Comprobante11");

                char[] delimiterChars = { ',' };
                string[] TipoCer = Dato.Split(delimiterChars);

                //RFC Posicion 4
                string RFC_Certificado = TipoCer[4];
                char[] RFC_Delimiter = { '=' };
                string[] RFC_Arreglo = RFC_Certificado.Split(RFC_Delimiter);
                string RFC_Arreglo_Certificado = RFC_Arreglo[1];
                RFC_Arreglo_Certificado = RFC_Arreglo_Certificado.Replace("\"", "");
                RFC_Arreglo_Certificado = RFC_Arreglo_Certificado.Replace("/", "");
                RFC_Arreglo_Certificado = RFC_Arreglo_Certificado.Trim();
                //MessageBox.Show("Comprobante12");
                //RFC
                oComprobanteEmisor.Rfc = validacion(ocEMPRESA.RFC);
                //Nombre
                oComprobanteEmisor.Nombre = validacion(ocEMPRESA.ID);
                oComprobanteEmisor.Nombre = Generar40.LimpiarNombre(oComprobanteEmisor.Nombre);
                //Regimen Fiscal
                oComprobanteEmisor.RegimenFiscal = CFDI40.c_RegimenFiscal.Item601;

                oComprobante.Emisor = oComprobanteEmisor;
                //MessageBox.Show("Comprobante15");


                //Elemento Receptor
                CFDI40.ComprobanteReceptor oComprobanteReceptor = new CFDI40.ComprobanteReceptor();
                oComprobanteReceptor.Rfc = validacion(rfc.Text);
                oComprobanteReceptor.Nombre = receptor.Text;
                oComprobanteReceptor.DomicilioFiscalReceptor = CodigoPostal.Text;
                if (oComprobanteReceptor.Rfc.Equals("XAXX010101000")
                        || oComprobanteReceptor.Rfc.Equals("XEXX010101000"))
                {
                    oComprobanteReceptor.DomicilioFiscalReceptor = oComprobante.LugarExpedicion;
                    oComprobanteReceptor.RegimenFiscalReceptor = CFDI40.c_RegimenFiscal.Item616;
                    oComprobanteReceptor.UsoCFDI = CFDI40.c_UsoCFDI.S01;

                }
                else
                {
                    try
                    {
                        oComprobanteReceptor.UsoCFDI = CFDI40.c_UsoCFDI.G03;
                        string UsoCFIDTr = ((ComboItem)USO_CFDI.SelectedItem).Value.ToString();
                        if (USO_CFDI.SelectedItem != null)
                        {
                            oComprobanteReceptor.UsoCFDI = (CFDI40.c_UsoCFDI)System.Enum.Parse(typeof(CFDI40.c_UsoCFDI)
                                    , UsoCFIDTr);
                        }
                    }
                    catch (Exception error)
                    {
                        ErrorFX.mostrar(error, true, true, "Traslado CFDI4.0 - 1228 - UsoCFDI ", true);
                    }
                }

                //oComprobanteReceptor.Rfc = oComprobanteEmisor.Rfc;



                if (!String.IsNullOrEmpty(NumRegIdTrib.Text))
                {
                    //NumRegIdTrib
                    oComprobanteReceptor.NumRegIdTrib = NumRegIdTrib.Text;
                    if (!String.IsNullOrEmpty(Pais.Text))
                    {
                        try
                        {
                            oComprobanteReceptor.ResidenciaFiscal = (CFDI40.c_Pais)System.Enum.Parse(typeof(CFDI40.c_Pais)
                                        , Pais.Text);
                            oComprobanteReceptor.ResidenciaFiscalSpecified = true;

                        }
                        catch (Exception error)
                        {
                            ErrorFX.mostrar(error, true, true, "Traslado CFDI4.0 - 1246 - Seccion de Domicilio del Receptor ", true);
                            return false;
                        }
                    }
                }
                if (!rfc.Text.Equals("XEXX010101000"))
                {
                    oComprobanteReceptor.ResidenciaFiscalSpecified = false;
                    oComprobanteReceptor.NumRegIdTrib = null;
                    oComprobanteReceptor.RegimenFiscalReceptor = CFDI40.c_RegimenFiscal.Item601;
                }


                

                //Receptor a ver si con eso funcoina
                //Esto funcoina bien no se porque EDICOM lo permite
                if (IgualarEmisor.Checked)
                {
                    oComprobante.Receptor = new CFDI40.ComprobanteReceptor();
                    oComprobante.Receptor.Rfc = oComprobante.Emisor.Rfc;
                    oComprobante.Receptor.Nombre = oComprobante.Emisor.Nombre;
                    oComprobante.Receptor.UsoCFDI = CFDI40.c_UsoCFDI.P01;

                    oComprobante.Receptor.UsoCFDI = CFDI40.c_UsoCFDI.G03;
                    try
                    {

                        oComprobante.Receptor.UsoCFDI = (CFDI40.c_UsoCFDI)System.Enum.Parse(typeof(CFDI40.c_UsoCFDI)
                                , ((ComboItem)USO_CFDI.SelectedItem).Value.ToString());

                    }
                    catch (Exception error)
                    {
                        ErrorFX.mostrar(error, true, true, "Generar40 - 190 - UsoCFDI ", true);
                    }
                    oComprobante.Receptor.DomicilioFiscalReceptor = oComprobante.LugarExpedicion;
                    oComprobante.Receptor.RegimenFiscalReceptor = CFDI40.c_RegimenFiscal.Item601;

                }
                else
                {
                    oComprobante.Receptor = oComprobanteReceptor;
                }
                //if (oComprobante.Receptor.Rfc.Equals("XEXX010101000"))
                //{
                //    oComprobante.Receptor.RegimenFiscalReceptor = CFDI40.c_RegimenFiscal.Item616;
                //}


                //Agregar direccion de Destino


                //Elemento Conceptos
                CFDI40.ComprobanteConcepto[] oComprobanteConceptos;
                oComprobanteConceptos = new CFDI40.ComprobanteConcepto[dtgrdGeneral.Rows.Count];

                int index = 0;
                foreach (DataGridViewRow row in this.dtgrdGeneral.Rows)
                {

                    //MessageBox.Show("Comprobante18");
                    CFDI40.ComprobanteConcepto oComprobanteConcepto = new CFDI40.ComprobanteConcepto();
                    //oComprobanteConcepto.cantidad = Math.Abs(decimal.Parse(row.Cells["cantidad"].Value.ToString()));
                    decimal cantidadTr = 0;
                    if (Globales.IsNumeric(row.Cells["cantidad"].Value))
                    {
                        cantidadTr = decimal.Parse(row.Cells["cantidad"].Value.ToString());
                    }
                    oComprobanteConcepto.Cantidad = cantidadTr;

                    string unidadTr = "";
                    if (row.Cells["unidad"].Value != null)
                    {
                        unidadTr = row.Cells["unidad"].Value.ToString();
                    }
                    if (!String.IsNullOrEmpty(unidadTr))
                    {

                        oComprobanteConcepto.Unidad = unidadTr;
                    }
                    

                    string ClaveUnidadTr = "";
                    if (row.Cells["ClaveUnidad"].Value!=null)
                    {
                        ClaveUnidadTr= row.Cells["ClaveUnidad"].Value.ToString();
                    }
                    if (!String.IsNullOrEmpty(ClaveUnidadTr))
                    {
                        oComprobanteConcepto.ClaveUnidad = ClaveUnidadTr;
                    }
                    switch (oComprobanteConcepto.ClaveUnidad)
                    {
                        case "H87":
                            oComprobanteConcepto.Unidad = "Pieza";
                            break;
                        case "EA":
                            oComprobanteConcepto.Unidad = "Elemento";
                            break;
                        case "E48":
                            oComprobanteConcepto.Unidad = "Unidad de Servicio";
                            break;
                        case "ACT":
                            oComprobanteConcepto.Unidad = "Actividad";
                            break;
                        case "KGM":
                            oComprobanteConcepto.Unidad = "Kilogramo";
                            break;
                        case "E51":
                            oComprobanteConcepto.Unidad = "Trabajo";
                            break;
                        case "A9":
                            oComprobanteConcepto.Unidad = "Tarifa";
                            break;
                        case "MTR":
                            oComprobanteConcepto.Unidad = "Metro";
                            break;
                        case "AB":
                            oComprobanteConcepto.Unidad = "Paquete a granel";
                            break;
                        case "BB":
                            oComprobanteConcepto.Unidad = "Caja base";
                            break;
                        case "KT":
                            oComprobanteConcepto.Unidad = "Kit";
                            break;
                        case "SET":
                            oComprobanteConcepto.Unidad = "Conjunto";
                            break;
                        case "LTR":
                            oComprobanteConcepto.Unidad = "Litro";
                            break;
                        case "XBX":
                            oComprobanteConcepto.Unidad = "Caja";
                            break;
                        case "MON":
                            oComprobanteConcepto.Unidad = "Mes";
                            break;
                        case "HUR":
                            oComprobanteConcepto.Unidad = "Hora";
                            break;
                        case "MTK":
                            oComprobanteConcepto.Unidad = "Metro cuadrado";
                            break;
                        case "11":
                            oComprobanteConcepto.Unidad = "Equipos";
                            break;
                        case "MGM":
                            oComprobanteConcepto.Unidad = "Miligramo";
                            break;
                        case "XPK":
                            oComprobanteConcepto.Unidad = "Paquete";
                            break;
                        case "XKI":
                            oComprobanteConcepto.Unidad = "Kit (Conjunto de piezas)";
                            break;
                        case "AS":
                            oComprobanteConcepto.Unidad = "Variedad";
                            break;
                        case "GRM":
                            oComprobanteConcepto.Unidad = "Gramo";
                            break;
                        case "PR":
                            oComprobanteConcepto.Unidad = "Par";
                            break;
                        case "DPC":
                            oComprobanteConcepto.Unidad = "Docenas de piezas";
                            break;
                        case "xun":
                            oComprobanteConcepto.Unidad = "Unidad";
                            break;
                        case "DAY":
                            oComprobanteConcepto.Unidad = "Día";
                            break;
                        case "XLT":
                            oComprobanteConcepto.Unidad = "Lote";
                            break;
                        case "10":
                            oComprobanteConcepto.Unidad = "Grupos";
                            break;
                        case "MLT":
                            oComprobanteConcepto.Unidad = "Mililitro";
                            break;
                        case "E54":
                            oComprobanteConcepto.Unidad = "Viaje";
                            break;
                        default:
                            // Manejo de claves no encontradas
                            break;
                    }



                    if (!String.IsNullOrEmpty(row.Cells["NumeroPedimentoInformacionAduanera"].Value.ToString()))
                    {
                        oComprobanteConcepto.InformacionAduanera = new CFDI40.ComprobanteConceptoInformacionAduanera[1];
                        oComprobanteConcepto.InformacionAduanera[0] = new CFDI40.ComprobanteConceptoInformacionAduanera();
                        oComprobanteConcepto.InformacionAduanera[0].NumeroPedimento = row.Cells["NumeroPedimentoInformacionAduanera"].Value.ToString();

                        //Ejemplo de pedimento: 10 47 3807 8003832
                    }


                    oComprobanteConcepto.ClaveProdServ = row.Cells["ClaveProdServ"].Value.ToString();

                    if (row.Cells["noIdentificacion"].Value.ToString() != "")
                    {
                        oComprobanteConcepto.NoIdentificacion = row.Cells["noIdentificacion"].Value.ToString();
                    }

                    oComprobanteConcepto.Descripcion = row.Cells["descripcion"].Value.ToString();
                    //oComprobanteConcepto.valorUnitario = Math.Abs(decimal.Parse(row.Cells["valorUnitario"].Value.ToString()));
                    oComprobanteConcepto.ValorUnitario = decimal.Parse(row.Cells["valorUnitario"].Value.ToString());
                    //oComprobanteConcepto.importe = Math.Abs(decimal.Parse(row.Cells["importe"].Value.ToString()));
                    oComprobanteConcepto.Importe = decimal.Parse(row.Cells["importe"].Value.ToString());
                    oComprobanteConcepto.ObjetoImp = "01";
                    //Tiene información aduanera
                    //if (row.Cells["informacionAduaneraAduana"].Value.ToString() != "")
                    //{
                    //    t_InformacionAduanera[] ot_InformacionAduanera;
                    //    ot_InformacionAduanera = new t_InformacionAduanera[1];
                    //    for (int t = 0; t < 1; t++)
                    //    {
                    //        ot_InformacionAduanera[t] = new t_InformacionAduanera();
                    //        ot_InformacionAduanera[t].aduana = row.Cells["informacionAduaneraAduana"].Value.ToString();
                    //        ot_InformacionAduanera[t].fecha = DateTime.Parse(row.Cells["informacionAduanerFecha"].Value.ToString());
                    //        ot_InformacionAduanera[t].numero = row.Cells["informacionAduanerNumero"].Value.ToString();
                    //    }
                    //    oComprobanteConcepto.Items = ot_InformacionAduanera;
                    //}

                    oComprobanteConceptos[index] = new CFDI40.ComprobanteConcepto();
                    oComprobanteConceptos[index] = oComprobanteConcepto;
                    index++;
                }
                oComprobante.Conceptos = oComprobanteConceptos;

                //Rodrigo Escalona: 25/08/2023 Agregar relacion
                if (DtgRelacion.Rows.Count > 0)
                {
                    try
                    {
                        CFDI40.ComprobanteCfdiRelacionados oComprobanteCfdiRelacionados = null;
                        oComprobanteCfdiRelacionados = new CFDI40.ComprobanteCfdiRelacionados();
                        string tipoRelaciontr = (tipoRelacion.SelectedItem as ComboItem).Value.ToString();
                        oComprobanteCfdiRelacionados.CfdiRelacionado =
                            new CFDI40.ComprobanteCfdiRelacionadosCfdiRelacionado[DtgRelacion.Rows.Count];
                        int i = 0;
                        foreach (DataGridViewRow Relaciones in DtgRelacion.Rows)
                        {
                            switch (tipoRelaciontr)
                            {
                                case "01":
                                    oComprobanteCfdiRelacionados.TipoRelacion = CFDI40.c_TipoRelacion.Item01;
                                    break;
                                case "02":
                                    oComprobanteCfdiRelacionados.TipoRelacion = CFDI40.c_TipoRelacion.Item02;
                                    break;
                                case "03":
                                    oComprobanteCfdiRelacionados.TipoRelacion = CFDI40.c_TipoRelacion.Item03;
                                    break;
                                case "04":
                                    oComprobanteCfdiRelacionados.TipoRelacion = CFDI40.c_TipoRelacion.Item04;
                                    break;
                                case "05":
                                    oComprobanteCfdiRelacionados.TipoRelacion = CFDI40.c_TipoRelacion.Item05;
                                    break;
                                case "06":
                                    oComprobanteCfdiRelacionados.TipoRelacion = CFDI40.c_TipoRelacion.Item06;
                                    break;
                                case "07":
                                    oComprobanteCfdiRelacionados.TipoRelacion = CFDI40.c_TipoRelacion.Item07;
                                    break;
                                case "08":
                                    oComprobanteCfdiRelacionados.TipoRelacion = CFDI40.c_TipoRelacion.Item08;
                                    break;
                                case "09":
                                    oComprobanteCfdiRelacionados.TipoRelacion = CFDI40.c_TipoRelacion.Item09;
                                    break;
                            }
                            oComprobanteCfdiRelacionados.CfdiRelacionado[i] = new CFDI40.ComprobanteCfdiRelacionadosCfdiRelacionado();
                            oComprobanteCfdiRelacionados.CfdiRelacionado[i].UUID = Relaciones.Cells["UUIDRelacionar"].Value.ToString();

                            i++;
                        }

                        oComprobante.CfdiRelacionados = new CFDI40.ComprobanteCfdiRelacionados[1];
                        oComprobante.CfdiRelacionados[0] = new CFDI40.ComprobanteCfdiRelacionados();
                        oComprobante.CfdiRelacionados[0] = oComprobanteCfdiRelacionados;
                    }
                    catch (Exception exLeyendaFiscal)
                    {
                        MessageBox.Show("Error en CFDI Relacionados. " + exLeyendaFiscal.Message.ToString()
                            , Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error);
                        return false;
                    }
                }



                //Agregar leyendaFiscal
                int totalComplementos = 0;
                XmlDocument oLeyenda = null;
                try
                {
                    if (!String.IsNullOrEmpty(OBSERVACIONES.Text))
                    {
                        totalComplementos++;
                        CFDI40.ComprobanteComplemento o = new CFDI40.ComprobanteComplemento();
                        CFDI40.ComprobanteImpuestosRetencion[] oComprobanteImpuestosRetencion = new CFDI40.ComprobanteImpuestosRetencion[1];
                        LeyendasFiscales oLeyendasFiscales = new LeyendasFiscales();
                        oLeyendasFiscales.Leyenda = new LeyendasFiscalesLeyenda[1];
                        oLeyendasFiscales.Leyenda[0] = new LeyendasFiscalesLeyenda();
                        oLeyendasFiscales.Leyenda[0].textoLeyenda = OBSERVACIONES.Text;

                        oLeyenda = new XmlDocument();
                        XmlSerializerNamespaces myNamespaces_leyenda = new XmlSerializerNamespaces();
                        myNamespaces_leyenda.Add("leyendasFisc", "http://www.sat.gob.mx/leyendasFiscales");

                        using (XmlWriter writer = oLeyenda.CreateNavigator().AppendChild())
                        {
                            new XmlSerializer(oLeyendasFiscales.GetType()).Serialize(writer, oLeyendasFiscales, myNamespaces_leyenda);
                        }
                        totalComplementos++;

                    }
                }
                catch (Exception exLeyendaFiscal)
                {
                    MessageBox.Show("Error en Leyenda Fiscal. " + exLeyendaFiscal.Message.ToString()
                        , Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return false;
                }


                XmlElement oCCE = null;
                if (activarCE.Checked)
                {
                    try
                    {
                        Encapsular oEncapsular = new Encapsular();
                        oEncapsular.ocEMPRESA = ocEMPRESA;
                        oEncapsular.oData_ERP = oData_ERP;
                        oCCE = complementoCCE(oComprobante, true, oEncapsular);
                        
                        if (oCCE != null)
                        {
                            //oComprobante.Receptor.NumRegIdTrib = NumRegIdTrib.Text;
                            //oComprobante.Receptor.ResidenciaFiscal = OGenerar40.ObtenerResidenciaFiscal(Pais.Text);
                            //oComprobante.Receptor.ResidenciaFiscalSpecified = true;
                            //oComprobante.Receptor.RegimenFiscalReceptor = CFDI40.c_RegimenFiscal.Item616;
                            //oComprobante.Receptor.UsoCFDI = CFDI40.c_UsoCFDI.S01;
                            oComprobante.Exportacion = "02";//CFDI40.c_Exportacion.Item02;
                            oComprobante.TipoCambio = OGenerar40.OtroTruncar(decimal.Parse(TIPO_CAMBIO.Text), 4);
                            oComprobante.TipoCambioSpecified = true;

                            totalComplementos++;
                        }
                        //totalComplementos++;
                    }
                    catch (Exception exLeyendaFiscal)
                    {
                        MessageBox.Show("Error en Leyenda Fiscal. " + exLeyendaFiscal.Message.ToString()
                            , Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error);
                        return false;
                    }
                }
                

                if (totalComplementos != 0)
                {
                    oComprobante.Complemento = new CFDI40.ComprobanteComplemento();
                    //oComprobante.Complemento[0] = new CFDI40.ComprobanteComplemento();
                    oComprobante.Complemento.Any = new XmlElement[totalComplementos];
                    totalComplementos = 0;
                    if (oLeyenda != null)
                    {
                        oComprobante.Complemento.Any[totalComplementos] = oLeyenda.DocumentElement;
                        totalComplementos++;

                    }
                    if (oCCE != null)
                    {
                        oComprobante.Complemento.Any[totalComplementos] = oCCE;
                        totalComplementos++;
                    }
                }



                XslCompiledTransform myXslTrans = new XslCompiledTransform();
                XslCompiledTransform trans = new XslCompiledTransform();
                string nombre_tmp = INVOICE_ID + "TMP" + DateTime.Now.ToString("ddMMyyyyhhmmss");
                if (File.Exists(nombre_tmp + ".XML"))
                {
                    File.Delete(nombre_tmp + ".XML");
                }

                XPathDocument myXPathDoc;
                try
                {

                    TextWriter writer = new StreamWriter(nombre_tmp + ".XML");
                    serializer.Serialize(writer, oComprobante, myNamespaces);
                    writer.Close();
                    cGeneracion ocGeneracion2 = new cGeneracion(oData, oData_ERP);
                    ocGeneracion2.parsar_d1p1(nombre_tmp + ".XML");

                    myXPathDoc = new XPathDocument(nombre_tmp + ".XML");
                    //XslTransform myXslTrans = new XslTransform();
                    myXslTrans = new XslCompiledTransform();

                    trans = new XslCompiledTransform();
                    XmlUrlResolver resolver = new XmlUrlResolver();

                    resolver.Credentials = System.Net.CredentialCache.DefaultCredentials;
                    XsltSettings settings = new XsltSettings(true, true);
                }
                catch (XmlException errores)
                {
                    MessageBox.Show("Error en " + errores.InnerException.ToString());
                    return false;
                }
                //Cargar xslt
                try
                {
                    //Version 3
                    myXslTrans.Load(sDirectory + "/XSLT/cadenaoriginal_4_0.xslt");
                }
                catch (XmlException errores)
                {
                    MessageBox.Show("Error en " + errores.InnerException.ToString());
                    return false;
                }

                //Crear Archivo Temporal

                XmlTextWriter myWriter = new XmlTextWriter(nombre_tmp + ".TXT", null);

                //Transformar al XML
                myXslTrans.Transform(myXPathDoc, null, myWriter);

                myWriter.Close();

                //Abrir Archivo TXT
                string CADENA_ORIGINAL = "";
                StreamReader re = File.OpenText(nombre_tmp + ".TXT");
                string input = null;
                while ((input = re.ReadLine()) != null)
                {
                    CADENA_ORIGINAL += input;
                }
                re.Close();

                //Eliminar Archivos Temporales
                File.Delete(nombre_tmp + ".TXT");
                File.Delete(nombre_tmp + ".XML");

                //Buscar el &amp; en la Cadena y sustituirlo por &
                CADENA_ORIGINAL = CADENA_ORIGINAL.Replace("&amp;", "&");
                cGeneracion ocGeneracion = new cGeneracion(oData, oData_ERP);
                oComprobante.Sello = ocGeneracion.SelloRSA(ocCERTIFICADO.LLAVE, ocCERTIFICADO.PASSWORD, CADENA_ORIGINAL, oComprobante.Fecha.Year); ;

                string DIRECTORIO_ARCHIVOS = "";
                //if (!Directory.Exists(ocSERIE.DIRECTORIO))
                //{
                //    ocSERIE.DIRECTORIO=Application.StartupPath;
                //}


                DIRECTORIO_ARCHIVOS += ocSERIE.DIRECTORIO + @"\" + ocSERIE.ID + @"\" + ocGeneracion.Fechammmyy(oComprobante.Fecha.ToString());
                string XML = DIRECTORIO_ARCHIVOS;

                string PDF = XML + @"\" + "pdf";
                if (!Directory.Exists(PDF))
                {
                    try
                    {

                        Directory.CreateDirectory(PDF);
                    }
                    catch (IOException excep)
                    {
                        MessageBox.Show(excep.Message.ToString() + Environment.NewLine + "Creando directorio de PDF"
                            , Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error);
                        return false;
                    }
                }

                XML += @"\" + "xml";
                if (Directory.Exists(XML) == false)
                    Directory.CreateDirectory(XML);

                string XML_Archivo = "";

                string nombre_archivo = ocSERIE.ID + INVOICE_ID;

                if (Directory.Exists(DIRECTORIO_ARCHIVOS))
                {
                    XML_Archivo = XML + @"\" + nombre_archivo + ".xml";
                }
                else
                {
                    XML_Archivo = @"\" + nombre_archivo + ".xml"; ;
                }

                XML_Archivo = XML + @"\" + nombre_archivo + ".xml";

                TextWriter writerXML = new StreamWriter(XML_Archivo);
                serializer.Serialize(writerXML, oComprobante, myNamespaces);
                writerXML.Close();
                
                Parsar_d1p1(XML_Archivo, false);

                ARCHIVO = XML_Archivo;
                PATH.Text = ARCHIVO;

                return true;
            }
            catch (Exception errores)
            {
                string inner = "";
                if (errores.InnerException != null)
                {
                    inner = errores.InnerException.Message.ToString();
                }
                MessageBox.Show("Error en generación. " + errores.Message.ToString()
                    + Environment.NewLine
                    + inner);
                return false;

            }
        }

        public void Parsar_d1p1(string ARCHIVO, bool leyendaFiscal)
        {
            Generar40.ReplaceInFile(ARCHIVO, "d1p1", "xsi");
            Generar40.ReplaceInFile(ARCHIVO, "-06:00", "");
            Generar40.ReplaceInFile(ARCHIVO, "xsi:cfdi=\"http://www.sat.gob.mx/cfd/4\"", "");
            //Generar40.ReplaceInFile(ARCHIVO, "xmlns:cce11=\"http://www.sat.gob.mx/ComercioExterior11\" ", "");

            //Generar40.ReplaceInFile(ARCHIVO, "xsi:cfdi=\"http://www.sat.gob.mx/cfd/4\"", "");
            //Generar40.ReplaceInFile(ARCHIVO, "d1p1:schemaLocation=\"http://www.sat.gob.mx/cfd/4 http://www.sat.gob.mx/sitio_internet/cfd/4/cfdv40.xsd\"","");
            //Generar40.ReplaceInFile(ARCHIVO, "d1p1:cfdi=\"http://www.sat.gob.mx/cfd/4\"", "");


        }
        private XmlElement complementoCCE(CFDI40.Comprobante oComprobante, bool cce, Encapsular oEncapsular)
        {
            //Complemento de Comercio Exterior
            if (oComprobante.TipoDeComprobante== CFDI40.c_TipoDeComprobante.T)
            {
                try
                {
                    if (cce)
                    {
                        ComercioExterior oComercioExterior = new ComercioExterior();
                        //oComercioExterior.ClaveDePedimentoSpecified = true;
                        //oComercioExterior.ClaveDePedimento = c_ClavePedimento.A1;
                        oComercioExterior.ClaveDePedimento = "A1";
                        oComercioExterior.CertificadoOrigen = 0;
                        //oComercioExterior.TipoOperacion = "2";// c_TipoOperacion.Item2;
                        //oComercioExterior.Subdivision = 0;
                        if (!String.IsNullOrEmpty(subdivision.Text))
                        {
                            string subdivisionTr = oData.IsNullNumero(subdivision.Text);
                            //oComercioExterior.Subdivision = int.Parse(subdivisionTr);
                        }

                        //oComercioExterior.SubdivisionSpecified = true;
                        oComercioExterior.Incoterm = Incoterm.Text;
                        oComercioExterior.CertificadoOrigen = 0;
                        //oComercioExterior.CertificadoOrigenSpecified = true;
                        //oComercioExterior.TipoCambioUSDSpecified = true;
                        oComercioExterior.TipoCambioUSD = oComprobante.TipoCambio;
                        oComercioExterior.TotalUSD = Math.Round(oComprobante.Total, 2).ToString();
                        //oComercioExterior.TotalUSDSpecified = true;
                        oComercioExterior.Emisor = new ComercioExteriorEmisor();
                        oComercioExterior.Emisor.Domicilio = new ComercioExteriorEmisorDomicilio();

                        if (oComprobante.TipoDeComprobante== CFDI40.c_TipoDeComprobante.T)
                        {
                            oComercioExterior.MotivoTraslado = motivoTraslado.Text.Substring(0, 2);
                        }



                        //Domicilio Fiscal
                        if (oEncapsular.ocEMPRESA.CALLE != "")
                        {
                            oComercioExterior.Emisor.Domicilio.Calle = validacion(oEncapsular.ocEMPRESA.CALLE);
                        }
                        if (oEncapsular.ocEMPRESA.EXTERIOR != "")
                        {
                            oComercioExterior.Emisor.Domicilio.NumeroExterior = validacion(oEncapsular.ocEMPRESA.EXTERIOR);
                        }
                        if (oEncapsular.ocEMPRESA.INTERIOR != "")
                        {
                            oComercioExterior.Emisor.Domicilio.NumeroInterior = validacion(oEncapsular.ocEMPRESA.INTERIOR);
                        }
                        if (oEncapsular.ocEMPRESA.COLONIA != "")
                        {
                            //c_Colonia oc_Colonia = (c_Colonia)System.Enum.Parse(typeof(c_Colonia), "Item" + oEncapsular.ocEMPRESA.COLONIA);

                            //oComercioExterior.Emisor.Domicilio.Colonia = oc_Colonia;
                            //oComercioExterior.Emisor.Domicilio.ColoniaSpecified = true;
                            oComercioExterior.Emisor.Domicilio.Colonia = oEncapsular.ocEMPRESA.COLONIA;
                        }
                        if (oEncapsular.ocEMPRESA.LOCALIDAD != "")
                        {
                            //c_Localidad oc_Localidad = (c_Localidad)System.Enum.Parse(typeof(c_Localidad), "Item" + oEncapsular.ocEMPRESA.LOCALIDAD);
                            //oComercioExterior.Emisor.Domicilio.Localidad = oc_Localidad;
                            //oComercioExterior.Emisor.Domicilio.LocalidadSpecified = true;
                            oComercioExterior.Emisor.Domicilio.Localidad = oEncapsular.ocEMPRESA.LOCALIDAD;
                        }
                        if (oEncapsular.ocEMPRESA.REFERENCIA != "")
                        {
                            oComercioExterior.Emisor.Domicilio.Referencia = validacion(oEncapsular.ocEMPRESA.REFERENCIA);
                        }
                        if (oEncapsular.ocEMPRESA.MUNICIPIO != "")
                        {

                            //c_Municipio oc_Municipio = (c_Municipio)System.Enum.Parse(typeof(c_Municipio), "Item" + oEncapsular.ocEMPRESA.MUNICIPIO);
                            //oComercioExterior.Emisor.Domicilio.Municipio = oc_Municipio;
                            //oComercioExterior.Emisor.Domicilio.MunicipioSpecified = true;
                            oComercioExterior.Emisor.Domicilio.Municipio = oEncapsular.ocEMPRESA.MUNICIPIO;
                        }
                        if (oEncapsular.ocEMPRESA.ESTADO != "")
                        {

                            //c_Estado oc_Estado = (c_Estado)System.Enum.Parse(typeof(c_Estado), oEncapsular.ocEMPRESA.ESTADO);
                            //oComercioExterior.Emisor.Domicilio.Estado = oc_Estado;
                            oComercioExterior.Emisor.Domicilio.Estado = oEncapsular.ocEMPRESA.ESTADO;

                        }
                        if (oEncapsular.ocEMPRESA.PAIS != "")
                        {
                            oComercioExterior.Emisor.Domicilio.Pais = "MEX";
                        }
                        if (oEncapsular.ocEMPRESA.CP != "")
                        {
                            oComercioExterior.Emisor.Domicilio.CodigoPostal = oEncapsular.ocEMPRESA.CP;
                        }
                        oComercioExterior.TipoCambioUSD = decimal.Round(oComercioExterior.TipoCambioUSD, 4);



                        //Receptor
                        oComercioExterior.Receptor = new ComercioExteriorReceptor();
                        oComercioExterior.Receptor.NumRegIdTrib = NumRegIdTrib.Text;
                        oComercioExterior.Receptor.Domicilio = new ComercioExteriorReceptorDomicilio();
                        oComercioExterior.Receptor.Domicilio.Calle = calle.Text;
                        oComercioExterior.Receptor.Domicilio.NumeroExterior = noExterior.Text;
                        oComercioExterior.Receptor.Domicilio.NumeroInterior = noInterior.Text;
                        oComercioExterior.Receptor.Domicilio.Pais = Pais.Text;
                        oComercioExterior.Receptor.Domicilio.Localidad = oComercioExterior.Receptor.Domicilio.Localidad;
                        oComercioExterior.Receptor.Domicilio.CodigoPostal = CodigoPostal.Text;
                        oComercioExterior.Receptor.Domicilio.Estado = EstadoCE.Text;

                        if (String.IsNullOrEmpty(noExterior.Text))
                        {
                            oComercioExterior.Receptor.Domicilio.NumeroExterior = null;
                        }
                        if (String.IsNullOrEmpty(noInterior.Text))
                        {
                            oComercioExterior.Receptor.Domicilio.NumeroInterior = null;
                        }
                        if (!String.IsNullOrEmpty(ReceptorMunicipio.Text))
                        {
                            oComercioExterior.Receptor.Domicilio.Municipio = ReceptorMunicipio.Text;
                        }
                        if (!String.IsNullOrEmpty(ReceptorColonia.Text))
                        {
                            oComercioExterior.Receptor.Domicilio.Colonia = ReceptorColonia.Text;
                        }
                        if (!String.IsNullOrEmpty(ReceptorLocalidad.Text))
                        {
                            oComercioExterior.Receptor.Domicilio.Localidad = ReceptorLocalidad.Text;
                        }
                        //Fin de receptor

                        //Destinatario
                        //Version 1.1
                        oComercioExterior.Destinatario = new ComercioExteriorDestinatario[1];
                        oComercioExterior.Destinatario[0] = new ComercioExteriorDestinatario();
                        oComercioExterior.Destinatario[0].Nombre = DestinatarioNombre.Text;
                        oComercioExterior.Destinatario[0].NumRegIdTrib = DestinatarioNumRegIdTrib.Text;
                        oComercioExterior.Destinatario[0].Domicilio = new ComercioExteriorDestinatarioDomicilio[1];
                        oComercioExterior.Destinatario[0].Domicilio[0] = new ComercioExteriorDestinatarioDomicilio();
                        oComercioExterior.Destinatario[0].Domicilio[0].Calle = DestinatarioCalle.Text;
                        oComercioExterior.Destinatario[0].Domicilio[0].Pais = DestinatarioPais.Text;
                        oComercioExterior.Destinatario[0].Domicilio[0].Estado = DestinatarioEstado.Text;
                        oComercioExterior.Destinatario[0].Domicilio[0].Municipio = DestinatarioMunicipio.Text;
                        oComercioExterior.Destinatario[0].Domicilio[0].Colonia = DestinatarioColonia.Text;
                        oComercioExterior.Destinatario[0].Domicilio[0].CodigoPostal = DestinatarioCP.Text;
                        oComercioExterior.Destinatario[0].Domicilio[0].Localidad = DestinatarioLocalidad.Text;

                        if (String.IsNullOrEmpty(DestinatarioLocalidad.Text))
                        {
                            oComercioExterior.Destinatario[0].Domicilio[0].Localidad = null;
                        }
                        if (String.IsNullOrEmpty(DestinatarioColonia.Text))
                        {
                            oComercioExterior.Destinatario[0].Domicilio[0].Colonia = null;
                        }
                        if (String.IsNullOrEmpty(DestinatarioMunicipio.Text))
                        {
                            oComercioExterior.Destinatario[0].Domicilio[0].Municipio = null;
                        }
                        if (String.IsNullOrEmpty(DestinatarioEstado.Text))
                        {
                            oComercioExterior.Destinatario[0].Domicilio[0].Estado = null;
                        }
                        if (oComprobante.TipoDeComprobante== CFDI40.c_TipoDeComprobante.T)
                        {
                            if ((oComercioExterior.MotivoTraslado.Contains("05")))
                            {
                                oComercioExterior.Propietario = new ComercioExteriorPropietario[1];
                                oComercioExterior.Propietario[0] = new ComercioExteriorPropietario();
                                oComercioExterior.Propietario[0].NumRegIdTrib = NumRegIdTrib.Text;
                                oComercioExterior.Propietario[0].ResidenciaFiscal = Pais.Text;
                            }
                        }




                        oComercioExterior.Mercancias = new ComercioExteriorMercancia[oComprobante.Conceptos.Length+1];
                        int i = 0;
                        decimal TotalUSD = 0;
                        foreach (CFDI40.ComprobanteConcepto concepto in oComprobante.Conceptos)
                        {

                            //Truncar el importe
                            oComprobante.Conceptos[i].Importe = concepto.Importe;

                            

                            decimal CantidadAduana = 0;
                            if (Generales.Globales.IsNumeric(dtgrdGeneral.Rows[i].Cells["cantidad"].Value))
                            {
                                CantidadAduana = decimal.Parse(dtgrdGeneral.Rows[i].Cells["cantidad"].Value.ToString());
                            }

                            if (Generales.Globales.IsNumeric(dtgrdGeneral.Rows[i].Cells["CantidadAduana"].Value))
                            {
                                CantidadAduana = decimal.Parse(dtgrdGeneral.Rows[i].Cells["CantidadAduana"].Value.ToString());
                            }

                            if (dtgrdGeneral.Rows[i].Cells["NumeroPedimento"].Value != null)
                            {
                                if (!String.IsNullOrEmpty(dtgrdGeneral.Rows[i].Cells["NumeroPedimento"].Value.ToString()))
                                {
                                    oComprobante.Conceptos[i].InformacionAduanera = new CFDI40.ComprobanteConceptoInformacionAduanera[1];
                                    oComprobante.Conceptos[i].InformacionAduanera[0] = new CFDI40.ComprobanteConceptoInformacionAduanera();
                                    oComprobante.Conceptos[i].InformacionAduanera[0].NumeroPedimento = dtgrdGeneral.Rows[i].Cells["NumeroPedimento"].Value.ToString();
                                }
                            }


                            //Comercio Exterior

                            oComercioExterior.Mercancias[i] = new ComercioExteriorMercancia();
                            oComercioExterior.Mercancias[i].CantidadAduanaSpecified = true;

                            oComercioExterior.Mercancias[i].CantidadAduana = CantidadAduana;
                            oComercioExterior.Mercancias[i].ValorUnitarioAduanaSpecified = true;

                            decimal ValorUnitarioAduanaTr = 0;
                            decimal ValorDolaresTr = 0;
                            if (Generales.Globales.IsNumeric(dtgrdGeneral.Rows[i].Cells["valorUnitario"].Value.ToString()))
                            {
                                ValorUnitarioAduanaTr = decimal.Parse(dtgrdGeneral.Rows[i].Cells["valorUnitario"].Value.ToString());
                                ValorDolaresTr = ValorUnitarioAduanaTr;
                            }

                            if (Generales.Globales.IsNumeric(dtgrdGeneral.Rows[i].Cells["ValorUnitarioAduana"].Value))
                            {
                                ValorUnitarioAduanaTr = decimal.Parse(dtgrdGeneral.Rows[i].Cells["ValorUnitarioAduana"].Value.ToString());
                            }
                                
                            oComercioExterior.Mercancias[i].ValorUnitarioAduana = Math.Round(ValorUnitarioAduanaTr, 6); //concepto.ValorUnitario;//Math.Round(decimal.Parse("15000.00"), 2);

                            
                            if (Generales.Globales.IsNumeric(dtgrdGeneral.Rows[i].Cells["ValorDolares"].Value))
                            {
                                ValorDolaresTr = decimal.Parse(dtgrdGeneral.Rows[i].Cells["ValorDolares"].Value.ToString());
                            }

                            oComercioExterior.Mercancias[i].ValorDolares = Math.Round(ValorDolaresTr, 4);//concepto.Importe; //oComercioExterior.Mercancias[i].ValorUnitarioAduana;
                            
                            
                            TotalUSD += oComercioExterior.Mercancias[i].ValorDolares;

                            string noIdentificacionTr = "";
                            if (dtgrdGeneral.Rows[i].Cells["noIdentificacion"].Value != null)
                            {
                                noIdentificacionTr = dtgrdGeneral.Rows[i].Cells["noIdentificacion"].Value.ToString().Trim();
                            }
                            oComercioExterior.Mercancias[i].NoIdentificacion = noIdentificacionTr;

                            string fraccionArancelariaTr = "";
                            if (dtgrdGeneral.Rows[i].Cells["fraccionArancelaria"].Value!=null)
                            {
                                fraccionArancelariaTr = dtgrdGeneral.Rows[i].Cells["fraccionArancelaria"].Value.ToString().Trim().Replace(" ", "");
                            }
                            oComercioExterior.Mercancias[i].FraccionArancelaria = fraccionArancelariaTr;

                            

                            

                            generar33 ogenerar33 = new generar33();
                            string fracciontr = "00" + cUMFracciones.obtenerUM(oComercioExterior.Mercancias[i].FraccionArancelaria);

                            if (fracciontr.Trim() == "00")
                            {
                                ErrorFX.mostrar("generar33 - 1552 - La fracción arancelaría no existe.", true, true, true);
                                return null;
                            }



                            oComercioExterior.Mercancias[i].UnidadAduana = (fracciontr).Substring(fracciontr.Length - 2, 2);

                            if (oComercioExterior.Mercancias[i].UnidadAduana.Contains("00"))
                            {
                                oComercioExterior.Mercancias[i].UnidadAduana = "01";
                            }

                            if (dtgrdGeneral.Rows[i].Cells["NumeroSerie"].Value.ToString() != "")
                            {
                                oComercioExterior.Mercancias[i].DescripcionesEspecificas = new ComercioExteriorMercanciaDescripcionesEspecificas[1];
                                oComercioExterior.Mercancias[i].DescripcionesEspecificas[0] = new ComercioExteriorMercanciaDescripcionesEspecificas();
                                oComercioExterior.Mercancias[i].DescripcionesEspecificas[0].NumeroSerie = dtgrdGeneral.Rows[i].Cells["NumeroSerie"].Value.ToString();
                                if (dtgrdGeneral.Rows[i].Cells["Marca"].Value.ToString() != "")
                                {
                                    oComercioExterior.Mercancias[i].DescripcionesEspecificas[0].Marca = dtgrdGeneral.Rows[i].Cells["Marca"].Value.ToString();
                                }
                                if (dtgrdGeneral.Rows[i].Cells["Modelo"].Value.ToString() != "")
                                {
                                    oComercioExterior.Mercancias[i].DescripcionesEspecificas[0].Modelo = dtgrdGeneral.Rows[i].Cells["Modelo"].Value.ToString();
                                }
                                if (dtgrdGeneral.Rows[i].Cells["Submodelo"].Value.ToString() != "")
                                {
                                    oComercioExterior.Mercancias[i].DescripcionesEspecificas[0].SubModelo = dtgrdGeneral.Rows[i].Cells["Submodelo"].Value.ToString();
                                }
                            }


                            i++;

                        }

                        oComercioExterior.TotalUSD = String.Format("{0:0.00}", TotalUSD);

                        //Math.Round(decimal.Parse(TotalUSD.ToString()), 2).ToString();// Math.Round(decimal.Parse(TotalUSD.ToString("F")), 2, MidpointRounding.AwayFromZero);
                        
                        
                        //oComprobante.SubTotal = oComercioExterior.TotalUSD;
                        //oComprobante.Total = oComercioExterior.TotalUSD;
                        XmlDocument doc = new XmlDocument();
                        XmlSerializerNamespaces myNamespaces_comercio = new XmlSerializerNamespaces();
                        //Version 11
                        myNamespaces_comercio.Add("cce20", "http://www.sat.gob.mx/ComercioExterior20");
                        using (XmlWriter writer = doc.CreateNavigator().AppendChild())
                        {
                            new XmlSerializer(oComercioExterior.GetType()).Serialize(writer, oComercioExterior, myNamespaces_comercio);
                        }
                        return doc.DocumentElement;
                    }
                }
                catch (Exception exCCE)
                {
                    ErrorFX.mostrar(exCCE, true, true, "generar33 - 1446", true);
                    return null;
                }
            }
            return null;
        }


        private void labelX13_Click(object sender, EventArgs e)
        {

        }

        private void buttonX18_Click(object sender, EventArgs e)
        {
            verArchivo(PATH.Text);
        }

        private void buttonX17_Click(object sender, EventArgs e)
        {
            verArchivo(PATH.Text);
        }

        private void verArchivo(string archivo)
        {
            string directorio = AppDomain.CurrentDomain.BaseDirectory;
            if (File.Exists(archivo))
            {
                frmFacturaXML oObjeto = new frmFacturaXML(archivo);
                oObjeto.Show();
            }
        }

        private void buttonX11_Click(object sender, EventArgs e)
        {
            eliminarLinea();
        }

        private void eliminarLinea()
        {
            dtgrdGeneral.EndEdit();
            if (dtgrdGeneral.SelectedRows.Count > 0)
            {
                DialogResult Resultado = MessageBox.Show("¿Esta seguro de eliminar las líneas seleccionadas?", Application.ProductName + "-" + Application.ProductVersion, MessageBoxButtons.YesNo);
                if (Resultado.ToString() != "Yes")
                {
                    return;
                }
                foreach (DataGridViewRow drv in dtgrdGeneral.SelectedRows)
                {
                    dtgrdGeneral.Rows.Remove(drv);
                }
                calcular();
            }
        }

        private void buttonX7_Click(object sender, EventArgs e)
        {
            imprimir();
        }
        private void imprimir(bool verPDF = true)
        {
            frmTraslados oCe = new frmTraslados();
            if (File.Exists(PATH.Text))
            {
                oCe.imprimiNuevo(verPDF, PATH.Text, receptor.Text, rfc.Text);
            }
            else
            {
                MessageBox.Show(this, "No esta timbrado, verifique el timbre en prueba o real, no existe el xml", Application.ProductName
                        , MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }



        //private void imprimir(bool verPDF = true)
        //{
        //    ReportDocument oReport = new ReportDocument();

        //    string FORMATO = FORMATO_IMPRESION.Text;
        //    if (!File.Exists(FORMATO))
        //    {
        //        MessageBoxEx.Show("El Reporte " + FORMATO + " no existe en su directorio. La factura no fue Generada.", Application.ProductName + "-" + Application.ProductVersion, MessageBoxButtons.OK, MessageBoxIcon.Error);
        //        return;
        //    }
        //    if (!File.Exists(PATH.Text))
        //    {
        //        return;
        //    }
        //    try
        //    {
        //        XmlSerializer serializer_CFD_3 = new XmlSerializer(typeof(CFDI40.Comprobante));
        //        TextReader reader = new StreamReader(PATH.Text);
        //        CFDI40.Comprobante oComprobanteLocal = (CFDI40.Comprobante)serializer_CFD_3.Deserialize(reader);
        //        //cfdiCE.comprobanteDataTable ocomprobanteDataTable = new cfdiCE.comprobanteDataTable();
        //        //ocomprobanteDataTable.AddcomprobanteRow(
        //        //    oComprobanteLocal.Emisor.rfc, oComprobanteLocal.Emisor.nombre, oComprobanteLocal.Emisor.RegimenFiscal[0].Regimen.ToString(), oComprobanteLocal.LugarExpedicion
        //        //    , oComprobanteLocal.Receptor.rfc, oComprobanteLocal.Receptor.nombre
        //        //    , 0
        //        //    , decimal.Parse(oComprobanteLocal.subTotal.ToString()), decimal.Parse(oComprobanteLocal.Impuestos.totalImpuestosTrasladados.ToString()), decimal.Parse(oComprobanteLocal.total.ToString())
        //        //    );
        //        cfdiCE.conceptosDataTable oconceptosDataTable = new cfdiCE.conceptosDataTable();
        //        foreach (CFDI40.ComprobanteConcepto concepto in oComprobanteLocal.Conceptos)
        //        {
        //            oconceptosDataTable.AddconceptosRow(concepto.Cantidad, concepto.Descripcion
        //                , concepto.NoIdentificacion, concepto.ValorUnitario, concepto.Importe, 0,
        //                concepto.ClaveUnidad);
        //        }

        //        oReport.Load(FORMATO);
        //        //DataSet oDataSet = new DataSet();
        //        //oDataSet.ReadXml(PATH.Text);
        //        //oReport.SetDataSource(oDataSet);
        //        XmlDocument doc = new XmlDocument();
        //        doc.Load(PATH.Text);
        //        XmlNodeList TimbreFiscalDigital = doc.GetElementsByTagName("tfd:TimbreFiscalDigital");
        //        if (TimbreFiscalDigital==null)
        //        {
        //            ErrorFX.mostrar("El archivo: " + PATH.Text + " No esta timbrado.", true, true, false);
        //            return;
        //        }
        //        string UUID = "";
        //        UUID = TimbreFiscalDigital[0].Attributes["UUID"].Value;
        //        string FechaTimbrado = "";
        //        FechaTimbrado = TimbreFiscalDigital[0].Attributes["FechaTimbrado"].Value;
        //        string noCertificadoSAT = "";
        //        noCertificadoSAT = TimbreFiscalDigital[0].Attributes["NoCertificadoSAT"].Value;
        //        string selloSAT = "";
        //        selloSAT = TimbreFiscalDigital[0].Attributes["SelloSAT"].Value;

        //        byte[] image = generarImagen(PATH.Text);
        //        string cadenaOriginal = generarCadenaOriginal(PATH.Text);
        //        string cadenaTFD = generarCadenaTFD(PATH.Text);
        //        cfdiCE.DATOSDataTable oDATOS = new cfdiCE.DATOSDataTable();
        //        oDATOS.AddDATOSRow(image, cadenaOriginal, cadenaTFD, direccionEmbarque.Text
        //            , notasPDF.Text, 0
        //            , oComprobanteLocal.Emisor.Rfc, oComprobanteLocal.Emisor.Nombre, oComprobanteLocal.Emisor.RegimenFiscal
        //            , oComprobanteLocal.LugarExpedicion
        //            , oComprobanteLocal.Receptor.Rfc, oComprobanteLocal.Receptor.Nombre
        //            , oComprobanteLocal.Fecha.ToString(), oComprobanteLocal.Serie, oComprobanteLocal.Folio.ToString(), oComprobanteLocal.NoCertificado.ToString()
        //            , oComprobanteLocal.TipoDeComprobante.ToString(), 0, 0
        //            , oComprobanteLocal.Total
        //            , UUID, "", FechaTimbrado, noCertificadoSAT, selloSAT
        //            , facturarA.Text
        //            );



        //        //ConnectionInfo oConnectionInfo=new ConnectionInfo();
        //        //oConnectionInfo.ServerName=PATH.Text;
        //        //oConnectionInfo.DatabaseName = "NewDataset";
        //        //oConnectionInfo.UserID = "";
        //        //oConnectionInfo.Password = "";
        //        //CrystalDecisions.Shared.TableLogOnInfo crpTableLogOnInfo= new TableLogOnInfo();
        //        //foreach(CrystalDecisions.CrystalReports.Engine.Table tabla in oReport.Database.Tables ){
        //        //    tabla.LogOnInfo.ConnectionInfo.ServerName=PATH.Text;
        //        //    tabla.LogOnInfo.ConnectionInfo.Type = CrystalDecisions.Shared.ConnectionInfoType.MetaData;
        //        //}


        //        if (oReport.Database.Tables.Count > 0)
        //        {
        //            //oReport.SetDataSource(oDataSet);
        //            foreach (CrystalDecisions.CrystalReports.Engine.Table tabla in oReport.Database.Tables)
        //            {
        //                if (tabla.Name.ToString() == "DATOS")
        //                {
        //                    oReport.Database.Tables["DATOS"].SetDataSource((System.Data.DataTable)oDATOS);
        //                }
        //                if (tabla.Name.ToString() == "conceptos")
        //                {
        //                    oReport.Database.Tables["conceptos"].SetDataSource((System.Data.DataTable)oconceptosDataTable);
        //                }
        //                //if (tabla.Name.ToString() == "Comprobante")
        //                //{
        //                //    oReport.Database.Tables["Comprobante"].SetDataSource(oDataSet.Tables["cfdi:Comprobante"]);
        //                //}
        //                //if (tabla.Name.ToString() == "Conceptos")
        //                //{
        //                //    oReport.Database.Tables["Conceptos"].SetDataSource(oDataSet.Tables["cfdi:Conceptos"]);
        //                //}
        //                //if (tabla.Name.ToString() == "Concepto")
        //                //{
        //                //    oReport.Database.Tables["Concepto"].SetDataSource(oDataSet.Tables["cfdi:Concepto"]);
        //                //}
        //                //if (tabla.Name.ToString() == "Traslados")
        //                //{
        //                //    oReport.Database.Tables["Traslados"].SetDataSource(oDataSet.Tables["cfdi:Traslados"]);
        //                //}
        //                //if (tabla.Name.ToString() == "Traslado")
        //                //{
        //                //    oReport.Database.Tables["Traslado"].SetDataSource(oDataSet.Tables["cfdi:Traslado"]);
        //                //}
        //                //if (tabla.Name.ToString() == "Impuestos")
        //                //{
        //                //    oReport.Database.Tables["Impuestos"].SetDataSource(oDataSet.Tables["cfdi:Impuestos"]);
        //                //}
        //                //if (tabla.Name.ToString() == "RegimenFiscal")
        //                //{
        //                //    oReport.Database.Tables["RegimenFiscal"].SetDataSource(oDataSet.Tables["cfdi:RegimenFiscal"]);
        //                //}
        //                //if (tabla.Name.ToString() == "TimbreFiscalDigital")
        //                //{
        //                //    oReport.Database.Tables["TimbreFiscalDigital"].SetDataSource(oDataSet.Tables["cfdi:TimbreFiscalDigital"]);
        //                //}
        //                //if (tabla.Name.ToString() == "Complemento")
        //                //{
        //                //    oReport.Database.Tables["Complemento"].SetDataSource(oDataSet.Tables["cfdi:Complemento"]);
        //                //}
        //                //if (tabla.Name.ToString() == "Emisor")
        //                //{
        //                //    oReport.Database.Tables["Emisor"].SetDataSource(oDataSet.Tables["cfdi:Emisor"]);
        //                //}
        //                //if (tabla.Name.ToString() == "Receptor")
        //                //{
        //                //    if (oDataSet.Tables["cfdi:Receptor"] != null)
        //                //    {
        //                //        oReport.Database.Tables["Receptor"].SetDataSource(oDataSet.Tables["cfdi:Receptor"]);
        //                //    }

        //                //}
        //            }

        //        }


        //        try
        //        {
        //            ExportOptions CrExportOptions = new ExportOptions();
        //            DiskFileDestinationOptions CrDiskFileDestinationOptions = new DiskFileDestinationOptions();
        //            PdfRtfWordFormatOptions CrFormatTypeOptions = new PdfRtfWordFormatOptions();
        //            string pdftr = PATH.Text.Replace(".xml", ".pdf");
        //            pdftr = pdftr.Replace(@"\xml", @"\pdf");
        //            CrDiskFileDestinationOptions.DiskFileName = pdftr;
        //            CrExportOptions = oReport.ExportOptions;
        //            CrExportOptions.ExportDestinationType = ExportDestinationType.DiskFile;
        //            CrExportOptions.ExportFormatType = ExportFormatType.PortableDocFormat;
        //            CrExportOptions.DestinationOptions = CrDiskFileDestinationOptions;
        //            CrExportOptions.FormatOptions = CrFormatTypeOptions;
        //            oReport.Export();
        //        }
        //        catch (Exception e)
        //        {
        //            MessageBox.Show(e.Message.ToString());
        //        }
        //        if (verPDF)
        //        {
        //            frmCFDIImpresion ofrmReporte = new frmCFDIImpresion(oReport);
        //            ofrmReporte.ShowDialog();
        //        }
        //    }
        //    catch (Exception err)
        //    {
        //        MessageBox.Show(err.Message.ToString());
        //    }


        //}
        private void MONEDA_SelectedIndexChanged(object sender, EventArgs e)
        {
            cargarTC();
        }

        private void FECHA_ValueChanged(object sender, EventArgs e)
        {
            cargarTC();
        }

        private void cargarTC()
        {
            string sSQL = @"SELECT TOP 1 SELL_RATE
                FROM  CURRENCY_EXCHANGE
                WHERE CURRENCY_ID = '" + MONEDA.Text + @"'  
                AND EFFECTIVE_DATE <= " + oData.convertir_fecha_general(FECHA.Value);

            DataTable oDataTableR = oData_ERP.EjecutarConsulta(sSQL);
            if (oDataTableR != null)
            {
                foreach (DataRow oDataRow1 in oDataTableR.Rows)
                {
                    TIPO_CAMBIO.Text = oDataRow1["SELL_RATE"].ToString();
                    return;
                }


            }
            TIPO_CAMBIO.Text = "1";
        }

        private void buttonX17_Click_1(object sender, EventArgs e)
        {
            verArchivo(PATH.Text);
        }

        private void labelX16_Click(object sender, EventArgs e)
        {

        }

        private void cargarLineaDescuento()
        {

            //Sumatoria
            //decimal importe_tr = 0;
            int rows = dtgrdGeneral.Rows.Count;
            for (int i = 0; i < rows; i++)
            {
                DataGridViewRow registro = dtgrdGeneral.Rows[i];
                decimal valorUnitariotr = decimal.Parse(registro.Cells["valorUnitario"].Value.ToString().Replace("$", ""));
                decimal importetr = decimal.Parse(registro.Cells["importe"].Value.ToString().Replace("$", ""));

                int n = dtgrdGeneral.Rows.Add();
                dtgrdGeneral.Rows[n].Cells["cantidad"].Value = registro.Cells["cantidad"].Value;
                dtgrdGeneral.Rows[n].Cells["descripcion"].Value = ocPolizaConfiguracion.ComentarioDescuento;
                dtgrdGeneral.Rows[n].Cells["unidad"].Value = "NA";
                dtgrdGeneral.Rows[n].Cells["valorUnitario"].Value = (valorUnitariotr * -1).ToString();
                dtgrdGeneral.Rows[n].Cells["importe"].Value = (importetr * -1).ToString();
                dtgrdGeneral.Rows[n].Cells["noIdentificacion"].Value = "";//registro.Cells["noIdentificacion"].Value;
                dtgrdGeneral.Rows[n].Cells["fraccionArancelaria"].Value = registro.Cells["fraccionArancelaria"].Value;


            }
            calcular();
        }

        private void cargarLineaDescuento(int nlinea)
        {

            //Sumatoria
            //decimal importe_tr = 0;

            DataGridViewRow registro = dtgrdGeneral.Rows[nlinea];
            decimal valorUnitariotr = decimal.Parse(registro.Cells["valorUnitario"].Value.ToString().Replace("$", ""));
            decimal importetr = decimal.Parse(registro.Cells["importe"].Value.ToString().Replace("$", ""));

            int n = dtgrdGeneral.Rows.Add();
            dtgrdGeneral.Rows[n].Cells["cantidad"].Value = registro.Cells["cantidad"].Value;
            dtgrdGeneral.Rows[n].Cells["descripcion"].Value = ocPolizaConfiguracion.ComentarioDescuento;
            dtgrdGeneral.Rows[n].Cells["unidad"].Value = "NA";
            dtgrdGeneral.Rows[n].Cells["valorUnitario"].Value = (valorUnitariotr * -1).ToString();
            dtgrdGeneral.Rows[n].Cells["importe"].Value = (importetr * -1).ToString();
            dtgrdGeneral.Rows[n].Cells["noIdentificacion"].Value = "";//registro.Cells["noIdentificacion"].Value;
            dtgrdGeneral.Rows[n].Cells["fraccionArancelaria"].Value = registro.Cells["fraccionArancelaria"].Value;

        }

        private void buttonX11_Click_1(object sender, EventArgs e)
        {
            eliminarLinea();
        }

        private void NumRegIdTrib_Leave(object sender, EventArgs e)
        {
            if (NumRegIdTrib.Text != "")
            {
                cargarCE();
            }
        }

        private void buttonX7_Click_1(object sender, EventArgs e)
        {
            imprimir();
        }

        private void frmTrasladoAutomatico_Click(object sender, EventArgs e)
        {
            calcular();
        }

        private void buttonX16_Click(object sender, EventArgs e)
        {
            verArchivo(PATH.Text);

        }

        private void buttonX18_Click_1(object sender, EventArgs e)
        {
            enviar_mail(true);
        }

        private void enviar_mail(bool adjuntar)
        {
            // Specify the message content.
            MailMessage message = new MailMessage();
            try
            {
                cEMPRESA ocEMPRESA = new cEMPRESA(oData);
                ocEMPRESA.cargar((ENTITY_ID.SelectedItem as cEMPRESA).ROW_ID);
                ocEMPRESA.cargar(ocCERTIFICADO.ROW_ID_EMPRESA);
                if (!bool.Parse(ocEMPRESA.ENVIO_CORREO))
                {
                    return;
                }
                ocEMPRESA.cargar(ocCERTIFICADO.ROW_ID_EMPRESA);
                //Validar el envio automatico

                string FROM_CORREO = "";

                if (isEmail(PARA.Text))
                {
                    FROM_CORREO = PARA.Text;
                }
                else
                {
                    MessageBox.Show("El para " + PARA.Text + " no es correo.", Application.ProductName + "-" + Application.ProductVersion, MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }

                informacion.Text = "Configurando Envio " + " a las " + DateTime.Now.ToShortDateString() + " " + DateTime.Now.ToLongTimeString();

                MailAddress from = new MailAddress(FROM_CORREO, ocSERIE.REMITENTE,
                                System.Text.Encoding.UTF8);


                message.From = from;
                //Destinatarios.                
                //TO
                string[] rEmail = PARA.Text.Split(';');
                for (int i = 0; i < rEmail.Length; i++)
                {
                    if (!String.IsNullOrEmpty(rEmail[i].ToString().Trim()))
                    {
                        if (isEmail(rEmail[i].ToString().Trim()))
                        {
                            message.To.Add(rEmail[i].ToString().Trim());
                        }
                    }
                }

                if (message.To.Count == 0)
                {
                    MessageBox.Show("El correo no puede enviarse, no existen destinatarios.", Application.ProductName + "-" + Application.ProductVersion, MessageBoxButtons.OK, MessageBoxIcon.Error);
                    informacion.Text = "Error envio de Correo";
                    return;
                }

                //CC
                string[] rEmailCC = ocSERIE.CC.Split(';');
                for (int i = 0; i < rEmailCC.Length; i++)
                {
                    if (!String.IsNullOrEmpty(rEmailCC[i].ToString().Trim()))
                    {
                        if (isEmail(rEmailCC[i].ToString().Trim()))
                        {
                            message.CC.Add(rEmailCC[i].ToString().Trim());
                        }
                    }
                }

                //CCO
                string[] rEmailCCO = ocSERIE.CCO.Split(';');
                for (int i = 0; i < rEmailCC.Length; i++)
                {
                    if (!String.IsNullOrEmpty(rEmailCCO[i].ToString().Trim()))
                    {
                        if (isEmail(rEmailCCO[i].ToString().Trim()))
                        {
                            message.Bcc.Add(rEmailCCO[i].ToString().Trim());
                        }
                    }
                }


                informacion.Text = "Enviando " + " a las " + DateTime.Now.ToShortDateString() + " " + DateTime.Now.ToLongTimeString();

                if (message.To.Count == 0)
                {
                    MessageBox.Show("El correo no puede enviarse, no existen destinatarios.", Application.ProductName + "-" + Application.ProductVersion, MessageBoxButtons.OK, MessageBoxIcon.Error);
                    informacion.Text = "Error";
                    return;
                }
                //Configurar Mensaje
                string mensaje_enviar = MENSAJE.Text.Replace("<<NUMFACTURA>>", ID.Text);
                string asunto_enviar = ASUNTO.Text.Replace("<<NUMFACTURA>>", ID.Text);

                message.Body = mensaje_enviar;
                message.BodyEncoding = System.Text.Encoding.UTF8;

                message.Subject = asunto_enviar;
                message.SubjectEncoding = System.Text.Encoding.UTF8;
                message.IsBodyHtml = true;
                message.Priority = MailPriority.High;

                //Añadir Atachment
                Attachment data;
                string pdf = PATH.Text.Replace("xml", "pdf");
                string xml = PATH.Text;
                string factura = SERIE.Text + ID.Text;

                //Adjuntar los Archivos XML y PDF
                if (!String.IsNullOrEmpty(xml.ToString().Trim()))
                {
                    if (File.Exists(xml.ToString().Trim()))
                    {
                        data = new Attachment(xml.ToString().Trim());
                        data.Name = factura + ".xml";
                        message.Attachments.Add(data);
                    }
                    else
                    {
                        MessageBox.Show("El Archivo " + xml + " no existe, no es posible enviar el mail.", Application.ProductName + "-" + Application.ProductVersion, MessageBoxButtons.OK, MessageBoxIcon.Error);
                        return;
                    }
                }


                if (!String.IsNullOrEmpty(pdf.ToString().Trim()))
                {
                    if (File.Exists(pdf.ToString().Trim()))
                    {
                        data = new Attachment(pdf.ToString().Trim());
                        data.Name = factura + ".pdf";
                        message.Attachments.Add(data);
                    }
                    else
                    {
                        MessageBox.Show("El Archivo " + pdf + " no existe, no es posible enviar el mail.", Application.ProductName + "-" + Application.ProductVersion, MessageBoxButtons.OK, MessageBoxIcon.Error);
                        return;
                    }
                }


                //Configurar SMPT
                SmtpClient client = new SmtpClient(ocEMPRESA.SMTP, int.Parse(ocEMPRESA.PUERTO));
                client.SendCompleted += new SendCompletedEventHandler(client_SendCompleted);
                //Configurar Crendiciales de Salida
                if (ocEMPRESA.SMTP.Trim() == "")
                {
                    MessageBox.Show("La configuración del correo electrónico esta erroneo.", Application.ProductName + "-" + Application.ProductVersion, MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }
                client.Host = ocEMPRESA.SMTP;
                client.Port = int.Parse(ocEMPRESA.PUERTO);
                client.DeliveryMethod = SmtpDeliveryMethod.Network;

                client.UseDefaultCredentials = bool.Parse(ocEMPRESA.CRENDENCIALES.ToString());
                client.EnableSsl = bool.Parse(ocEMPRESA.SSL.ToString());
                client.Timeout = 2000000;
                System.Net.NetworkCredential credenciales = new System.Net.NetworkCredential(ocEMPRESA.USUARIO, ocEMPRESA.PASSWORD_EMAIL);
                client.Credentials = credenciales;

                //            ServicePointManager.ServerCertificateValidationCallback =
                //delegate(object sender, X509Certificate certificate, X509Chain chain,
                //   SslPolicyErrors sslPolicyErrors) { return true; };

                object userState = "";
                userState = "";
                client.Send(message);
            }
            catch (System.Net.Mail.SmtpException ex)
            {
                MessageBox.Show("Envio de correo: " + ex.Message, Application.ProductName + "-" + Application.ProductVersion, MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            message.Dispose();
            message = null;
            return;
        }

        public void client_SendCompleted(object sender, AsyncCompletedEventArgs e)
        {
            try
            {
                string VALORES = (string)e.UserState;
                if (e.Error != null)
                {
                    MessageBox.Show("Envio de correo: " + e.Error.Message.ToString());

                    //MessageBox.Show("Error de Envio " + e.Error.InnerException.ToString());
                }
                else
                {
                    //MessageBox.Show("Enviado " + " a las " + DateTime.Now.ToShortDateString() + " " + DateTime.Now.ToLongTimeString());
                    informacion.Text = "Enviado " + " a las " + DateTime.Now.ToShortDateString() + " " + DateTime.Now.ToLongTimeString();
                }
            }
            catch (Exception error_envio)
            {
                MessageBox.Show(error_envio.Data.ToString() + Environment.NewLine + error_envio.Source.ToString());
            }

        }

        public static bool isEmail(string inputEmail)
        {
            if (inputEmail == null || inputEmail.Length == 0)
            {
                return false;
            }

            const string expression = @"^([a-zA-Z0-9_\-\.]+)@((\[[0-9]{1,3}" +
                                      @"\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9\-]+\" +
                                      @".)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$";

            Regex regex = new Regex(expression);
            return regex.IsMatch(inputEmail);
        }

        private void buttonX19_Click(object sender, EventArgs e)
        {
            //verArchivo(PATH.Text.Replace("xml", "pdf"));
            frmTraslados oCe = new frmTraslados();
            oCe.imprimiNuevo(true,this.PATH.Text,receptor.Text,this.rfc.Text,OBSERVACIONES.Text,
                DestinatarioNombre.Text + " " +
                DestinatarioCalle.Text + " " +
                DestinatarioInterior.Text + " " +
                DestinatarioEstado.Text + " " + 
                DestinatarioCP.Text + " "

                );
        }

        private void FECHA_ValueChanged_1(object sender, EventArgs e)
        {
            buscaTCvisual();
        }

        private void buscaTCvisual()
        {
            try
            {
                TIPO_CAMBIO.Text = "1";
                if (MONEDA.Text.Contains("USD"))
                {
                    if (TrabajarIndependiente)
                    {
                        TipoCambioMultiple.Clases.Banxico OBanxico = new TipoCambioMultiple.Clases.Banxico();
                        informacion.Text = "Cargando...";
                        informacion.ForeColor = Color.Black;
                        decimal TipoCambioTr = OBanxico.TipoCambio(FECHA.Value);
                        TIPO_CAMBIO.Text = TipoCambioTr.ToString("N4");
                    }
                    else
                    {
                        string sSQL = "";
                        sSQL = @"SELECT TOP 1 *
                        FROM CURRENCY_EXCHANGE
                        WHERE (EFFECTIVE_DATE <= " + oData.convertir_fecha(FECHA.Value) + @")
                        AND CURRENCY_ID='USD' 
                        ORDER BY EFFECTIVE_DATE DESC";
                        DataTable oDataTable = oData_ERP.EjecutarConsulta(sSQL);
                        if (oDataTable != null)
                        {
                            foreach (DataRow oDataRow in oDataTable.Rows)
                            {
                                //Añadir el Registro
                                TIPO_CAMBIO.Text = oDataRow["SELL_RATE"].ToString();
                            }
                        }
                    }
                }

            }
            catch
            {

            }
        }

        private void MONEDA_SelectedIndexChanged_1(object sender, EventArgs e)
        {
            buscaTCvisual();
        }

        public decimal otroTruncar(decimal valor, cEMPRESA oEMPRESA)
        {
            try
            {

                decimal result = 0;
                string[] xs = valor.ToString().Split('.');

                string decimalparte = xs[1] + "0000000000000000000000";
                if (oEMPRESA.APROXIMACION == "Truncar")
                {
                    result = decimal.Parse(xs[0] + "." + decimalparte.Substring(0, Math.Min(decimalparte.Length, int.Parse(oEMPRESA.VALORES_DECIMALES))));
                }
                else
                {
                    result = Math.Round(valor, int.Parse(oEMPRESA.VALORES_DECIMALES));
                    xs = result.ToString().Split('.');
                    if (xs[1].Length != int.Parse(oEMPRESA.VALORES_DECIMALES))
                    {
                        decimalparte = xs[1] + "0000000000000000000000";
                        result = decimal.Parse(xs[0] + "." + decimalparte.Substring(0, int.Parse(oEMPRESA.VALORES_DECIMALES)));
                    }
                }
                return result;
            }
            catch
            {
                return valor;
            }
        }

        public decimal otroTruncar(decimal valor, int decimales = 4)
        {
            try
            {

                decimal result = 0;
                string[] xs = valor.ToString().Split('.');

                string decimalparte = xs[1] + "0000000000000000000000";
                result = decimal.Parse(xs[0] + "." + decimalparte.Substring(0, Math.Min(decimalparte.Length, decimales)));

                return result;
            }
            catch
            {
                return valor;
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            importar();
        }


        private void importar()
        {
            try
            {
                Application.DoEvents();
                OpenFileDialog openFileDialog1 = new OpenFileDialog();
                openFileDialog1.Filter = "Excel files (*.xlsx)|*.xlsx|All files (*.*)|*.*";
                openFileDialog1.FilterIndex = 1;
                openFileDialog1.RestoreDirectory = true;

                if (openFileDialog1.ShowDialog() == DialogResult.OK)
                {
                    try
                    {
                        procesar(this.dtgrdGeneral, 1, openFileDialog1.FileName);
                    }
                    catch (Exception)
                    {
                        MessageBox.Show("No existe el Archivo seleccionado", Application.ProductName + "-" + Application.ProductVersion, MessageBoxButtons.OK);
                    }
                    finally
                    {
                        Application.DoEvents();
                    }
                }
            }
            catch (Exception e)
            {
                ErrorFX.mostrar(e, true, true, "frmTraslado - 485 ");
            }
        }

        private void procesar(DataGridView dtg, int hoja, string archivo)
        {
            string tmpFile = Path.GetTempFileName();
            try
            {
                if (!File.Exists(archivo))
                {
                    MessageBox.Show(this, "El archivo " + archivo + " no existe.", Application.ProductName
                        , MessageBoxButtons.OK, MessageBoxIcon.Error);
                }

                File.Delete(tmpFile);
                File.Copy(archivo, tmpFile);

                dtg.Rows.Clear();
                int rowIndex = 2;
                FileInfo existingFile = new FileInfo(tmpFile);
                using (ExcelPackage package = new ExcelPackage(existingFile))
                {
                    try
                    {
                        ExcelWorksheet worksheet = package.Workbook.Worksheets[hoja];
                        while (worksheet.Cells[rowIndex, 2].Value != null)
                        {
                            try
                            {
                                string Cnt = worksheet.Cells[rowIndex, 1].Text;
                                string ClaveProdServ = worksheet.Cells[rowIndex, 2].Text;
                                string NoIdentificacion = worksheet.Cells[rowIndex, 3].Text;
                                string ClaveUnidad = worksheet.Cells[rowIndex, 4].Text;
                                string Unidad = worksheet.Cells[rowIndex, 5].Text;
                                string Descripcion = worksheet.Cells[rowIndex, 6].Text;
                                string ValorUnitario = worksheet.Cells[rowIndex, 7].Text;
                                string Importe = worksheet.Cells[rowIndex, 8].Text;
                                string NumeroPedimento = worksheet.Cells[rowIndex, 9].Text;

                                string fraccionArancelariaTr = worksheet.Cells[rowIndex, 10].Text;
                                string CantidadAduanaTr = worksheet.Cells[rowIndex, 11].Text;
                                string ValorUnitarioAduanaTr = worksheet.Cells[rowIndex, 12].Text;

                                //Validar el NumeroPedimento
                                if (!String.IsNullOrEmpty(NumeroPedimento))
                                {
                                    if (NumeroPedimento.Length != 15 + 6)
                                    {
                                        string mensajetr = @"Se  debe registrar  el  número  del  pedimento  correspondiente  a  la importación del bien, el cual se integra de izquierda a derecha de la siguiente manera: Últimos 2 dígitos del año de validación seguidos por dos espacios,  2 dígitos de la aduana de despacho seguidos por dos  espacios, 4 dígitos del número de la patente seguidos por dos espacios, 1 dígito que corresponde al último dígito del año en curso, salvo que se trate de un pedimento consolidado, iniciado en el año inmediato anterior o del pedimento original de unarectificación, 6 dígitos de la numeración progresiva por aduana. Se  debe  registrar  la  información  en  este  campo  cuando  el CFDI no contenga el complemento de comercio exterior (es una venta de primera mano nacional). Para validar la estructura de este campo puede consultar la documentación técnica publicada en el Portal del SAT.
                                        Ejemplo:  " + Environment.NewLine + @"
                                        10  47  3807  8003832
                                        Usted tiene este: " + Environment.NewLine + @"
                                        " + NumeroPedimento + @" " + Environment.NewLine + @"
                                        Haga la correción
                                        ";
                                        MessageBox.Show(this, "El Numero de Pedimento esta erroneo debe ser " +
                                            Environment.NewLine +
                                            mensajetr
                                            , Application.ProductName
                                                                , MessageBoxButtons.OK, MessageBoxIcon.Error);
                                        return;
                                    }
                                }


                                int n = dtg.Rows.Add();
                                for (int i=0;i<dtg.ColumnCount; i++)
                                {
                                    dtg.Rows[n].Cells[i].Value = string.Empty;
                                }

                                dtg.Rows[n].Cells["cantidad"].Value = Cnt;
                                dtg.Rows[n].Cells["ClaveProdServ"].Value = ClaveProdServ;
                                dtg.Rows[n].Cells["noIdentificacion"].Value = NoIdentificacion;
                                dtg.Rows[n].Cells["ClaveUnidad"].Value = ClaveUnidad;
                                dtg.Rows[n].Cells["unidad"].Value = Unidad;
                                dtg.Rows[n].Cells["descripcion"].Value = Descripcion;
                                dtg.Rows[n].Cells["valorUnitario"].Value = ValorUnitario;
                                dtg.Rows[n].Cells["importe"].Value = Importe;
                                dtg.Rows[n].Cells["NumeroPedimentoInformacionAduanera"].Value = NumeroPedimento;

                                dtg.Rows[n].Cells["fraccionArancelaria"].Value = fraccionArancelariaTr;
                                dtg.Rows[n].Cells["CantidadAduana"].Value = CantidadAduanaTr;
                                dtg.Rows[n].Cells["ValorUnitarioAduana"].Value = ValorUnitarioAduanaTr;

                                //Rodrigo Escalpna 04/01/2020
                                try
                                {
                                    if (Generales.Globales.IsNumeric(CantidadAduanaTr) &
                                        Generales.Globales.IsNumeric(ValorUnitarioAduanaTr)
                                        )
                                    {
                                        decimal valorDolaresTr = (decimal.Parse(CantidadAduanaTr)
                                            * decimal.Parse(ValorUnitarioAduanaTr));

                                        dtg.Rows[n].Cells["ValorDolares"].Value = String.Format("{0:0.00}", valorDolaresTr);
                                    }
                                }
                                catch (Exception e)
                                {
                                    ErrorFX.mostrar(e, true, true, "frmCFDIProforma - 3099 Valor de Dolares ");
                                }
                                


                            }
                            catch (Exception e)
                            {
                                ErrorFX.mostrar(e, true, true, "frmCFDIProforma - 2940 ");
                            }
                            rowIndex++;
                        }
                    }
                    catch
                    {
                        MessageBox.Show("El archivo esta corrupto, abralo y guardelo de nuevo."
                            , Application.ProductName + "-" + Application.ProductVersion, MessageBoxButtons.OK, MessageBoxIcon.Error);

                    }
                }
            }
            catch (Exception e)
            {
                ErrorFX.mostrar(e, true, false,
                "frmCFDIProforma - 2956 - "
                );
            }
            finally
            {
                File.Delete(tmpFile);
            }

        }

        private void rfc_TextChanged(object sender, EventArgs e)
        {
            validarRFCExtranjero();
        }
        private void validarRFCExtranjero()
        {
            string rfcTr = rfc.Text.Trim();
            if (rfcTr.Equals("XEXX010101000"))
            {
                activarCE.Checked = true;
            }
            else
            {
                activarCE.Checked = false;
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            cargarCE();
        }

        private void pegarDatosDesdeLíneaDeExcelToolStripMenuItem_Click(object sender, EventArgs e)
        {
            PegarDatosLineaExcel();
        }

        private void PegarDatosLineaExcel()
        {
            string LineaClipBoard=Clipboard.GetText();
            string MensajeError = string.Empty;
            string[] Lineas = LineaClipBoard.Split('\n');
            int LineaContador = 0;
            foreach (string LineaTr in Lineas) {
                try
                {
                    if (!String.IsNullOrEmpty(LineaTr))
                    {
                        LineaContador++;
                        string[] Linea = LineaTr.Split('\t');
                        decimal ValorDolaresP = decimal.Parse(Linea[11]) * decimal.Parse(Linea[10]);

                        insertar_linea(decimal.Parse(Linea[0]), Generar40.Validacion(Linea[3]), Generar40.Validacion(Linea[5])
                            , decimal.Parse(Linea[6]), decimal.Parse(Linea[7]), Generar40.Validacion(Linea[2])
                            , Generar40.Validacion(Linea[9])
                            , string.Empty, string.Empty, string.Empty
                            , Generar40.Validacion(Linea[1]), Generar40.Validacion(Linea[3]), string.Empty, string.Empty
                            , string.Empty, string.Empty
                            , Linea[11], ValorDolaresP.ToString()
                            , Linea[10]
                            );
                    }
                    
                }
                catch(Exception Error)
                {
                    MensajeError += "Error en la línea " + LineaContador.ToString() + " verifique los datos númericos" + Environment.NewLine;
                }
            }
            if (!String.IsNullOrEmpty(MensajeError))
            {
                ErrorFX.mostrar(MensajeError, true, true, true);
            }
            //for (int x = 0; x < Linea.Length; x++)
            //{
                /*
Cnt	ClaveProdServ	NoIdentificacion	ClaveUnidad	Unidad	Descripcion	ValorUnitario	Importe	NumeroPedimento	fraccionArancelaria	CantidadAduana	ValorUnitarioAduana
35	30102006	SAMPLES_	H87	PIEZA	PARTES DE MUESTRAS DE ALUMINIO 	0.5	17.5		7606129900	35	0.5
                */
                
            //}

        }

        private void dtgrdGeneral_MouseLeave(object sender, EventArgs e)
        {

        }

        private void dtgrdGeneral_MouseClick(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Right)
            {
                contextMenuStrip1.Show(dtgrdGeneral, new Point(e.X, e.Y));
            }
        }

        private void tabPage3_MouseClick(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Right)
            {
                contextMenuStrip1.Show(dtgrdGeneral, new Point(e.X, e.Y));
            }
        }

        private void pegarDatosParaComercioExteriorDesdePlantillaDeExcelToolStripMenuItem_Click(object sender, EventArgs e)
        {

            PegarDatosComercioExteriorExcel();
        }

        private void PegarDatosComercioExteriorExcel()
        {
            string LineaClipBoard = Clipboard.GetText();
            string MensajeError = string.Empty;
            string[] Linea = LineaClipBoard.Split('\n');
            try
            {
                Incoterm.Text = Generar40.Validacion(Linea[0]);
                NumRegIdTrib.Text = Generar40.Validacion(Linea[1]);
                Pais.Text = Generar40.Validacion(Linea[2]);
                calle.Text = Generar40.Validacion(Linea[3]);
                noExterior.Text = Generar40.Validacion(Linea[4]);
                EstadoCE.Text = Generar40.Validacion(Linea[5]);
                CodigoPostal.Text = Generar40.Validacion(Linea[6]);
                Ciudad.Text = Generar40.Validacion(Linea[7]);
            }
            catch (Exception Error)
            {
                MensajeError += "Error en datos verifique los datos copiados" + Environment.NewLine;
            }
            
            if (!String.IsNullOrEmpty(MensajeError))
            {
                ErrorFX.mostrar(MensajeError, true, true, true);
            }


        }

        private void copiarDatosDelReceptorAlDestinatarioToolStripMenuItem_Click(object sender, EventArgs e)
        {
            AccionCopiarDatosReceptorDestinatario();
        }

        private void AccionCopiarDatosReceptorDestinatario()
        {
            DestinatarioNumRegIdTrib.Text= NumRegIdTrib.Text;
            DestinatarioPais.Text= Pais.Text;
            DestinatarioCalle.Text= calle.Text;
            DestinatarioInterior.Text = noInterior.Text;
            DestinatarioEstado.Text=EstadoCE.Text;
            DestinatarioCP.Text=CodigoPostal.Text;
            DestinatarioExterior.Text = noExterior.Text;
            DestinatarioNombre.Text= receptor.Text;
        }

        private void button3_Click(object sender, EventArgs e)
        {
            rfc.Text = "XEXX010101000";
        }

        private void button4_Click(object sender, EventArgs e)
        {
            DestinatarioNombre.Text = receptor.Text;
        }

        private void button6_Click(object sender, EventArgs e)
        {
            AgregarLineaRelacion();
        }

        private void AgregarLineaRelacion()
        {
            DtgRelacion.Rows.Add();
        }

        private void button5_Click(object sender, EventArgs e)
        {
            EliminarLineaRelacion();
        }

        private void EliminarLineaRelacion()
        {
            DtgRelacion.EndEdit();
            if (DtgRelacion.SelectedCells.Count > 0)
            {
                DialogResult Resultado = MessageBox.Show("¿Desea eliminar las seleccionada?"
                    , Application.ProductName + "-" + Application.ProductVersion
                    , MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                if (Resultado != DialogResult.Yes)
                {
                    return;
                }
                
                foreach (DataGridViewRow drv in DtgRelacion.SelectedRows)
                {
                    DtgRelacion.Rows.Remove(drv);                    
                }                
            }
            else
            {
                MessageBox.Show("Seleccione alguna línea.", Application.ProductName + "-" + Application.ProductVersion, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }
    }
}
