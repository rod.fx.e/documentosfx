﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;

using System.Text;
using System.IO;
using System.Windows.Forms;
using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;
using Generales;
namespace FE_FX
{
    public partial class frmReporte : Form
    {
        ReportDocument oReport;
        private cCONEXCION oData = new cCONEXCION("");
        private string INVOICE;
        private string USUARIO;

        public frmReporte(string sConn, string INVOICE_p)
        {
            oData.sConn = sConn;
            INVOICE = INVOICE_p;

            InitializeComponent();
        }

        public void cargar()
        {
            oReport = new ReportDocument();
            string sSQL = "";
            sSQL = "";

            sSQL = " SELECT * ";
            sSQL += " FROM MK_FACTURA ";
            sSQL += " WHERE INVOICE='" + INVOICE + "' ";

            oReport.Load(Application.StartupPath + "\\FACTURA.rpt");

            oReport.SetDataSource(oData.EjecutarConsulta(sSQL));

            crViewer.ReportSource = oReport;
            crViewer.Refresh();

        }

        private void frmReporte_Load(object sender, EventArgs e)
        {
            cargar();
        }
    }
}
