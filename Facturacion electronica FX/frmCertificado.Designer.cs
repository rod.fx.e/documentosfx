﻿namespace FE_FX
{
    partial class frmCertificado
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmCertificado));
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.toolStripStatusLabel1 = new System.Windows.Forms.ToolStripStatusLabel();
            this.label1 = new System.Windows.Forms.Label();
            this.ID = new System.Windows.Forms.TextBox();
            this.EMPRESA = new System.Windows.Forms.TextBox();
            this.CERTIFICADO = new System.Windows.Forms.TextBox();
            this.NO_CERTIFICADO = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.FECHA_DESDE = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.FECHA_HASTA = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.LLAVE = new System.Windows.Forms.TextBox();
            this.PASSWORD = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.button4 = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.button7 = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.ajax_loader = new System.Windows.Forms.PictureBox();
            this.ACTIVO = new System.Windows.Forms.CheckBox();
            this.button5 = new System.Windows.Forms.Button();
            this.PFX = new System.Windows.Forms.TextBox();
            this.button9 = new System.Windows.Forms.Button();
            this.button6 = new System.Windows.Forms.Button();
            this.checkBox1 = new System.Windows.Forms.CheckBox();
            this.statusStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ajax_loader)).BeginInit();
            this.SuspendLayout();
            // 
            // statusStrip1
            // 
            this.statusStrip1.Dock = System.Windows.Forms.DockStyle.Top;
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripStatusLabel1});
            this.statusStrip1.Location = new System.Drawing.Point(0, 0);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(835, 22);
            this.statusStrip1.TabIndex = 0;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // toolStripStatusLabel1
            // 
            this.toolStripStatusLabel1.Name = "toolStripStatusLabel1";
            this.toolStripStatusLabel1.Size = new System.Drawing.Size(42, 17);
            this.toolStripStatusLabel1.Text = "Estado";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 71);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(57, 13);
            this.label1.TabIndex = 3;
            this.label1.Text = "Certificado";
            // 
            // ID
            // 
            this.ID.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.ID.Location = new System.Drawing.Point(95, 68);
            this.ID.Name = "ID";
            this.ID.Size = new System.Drawing.Size(733, 20);
            this.ID.TabIndex = 4;
            this.ID.TextChanged += new System.EventHandler(this.ID_TextChanged);
            this.ID.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.Validar_Caracteres);
            // 
            // EMPRESA
            // 
            this.EMPRESA.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.EMPRESA.Enabled = false;
            this.EMPRESA.Location = new System.Drawing.Point(95, 96);
            this.EMPRESA.Name = "EMPRESA";
            this.EMPRESA.Size = new System.Drawing.Size(733, 20);
            this.EMPRESA.TabIndex = 6;
            this.EMPRESA.TextChanged += new System.EventHandler(this.ID_TextChanged);
            // 
            // CERTIFICADO
            // 
            this.CERTIFICADO.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.CERTIFICADO.Enabled = false;
            this.CERTIFICADO.Location = new System.Drawing.Point(95, 126);
            this.CERTIFICADO.Name = "CERTIFICADO";
            this.CERTIFICADO.Size = new System.Drawing.Size(733, 20);
            this.CERTIFICADO.TabIndex = 8;
            this.CERTIFICADO.TextChanged += new System.EventHandler(this.ID_TextChanged);
            // 
            // NO_CERTIFICADO
            // 
            this.NO_CERTIFICADO.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.NO_CERTIFICADO.Enabled = false;
            this.NO_CERTIFICADO.Location = new System.Drawing.Point(95, 152);
            this.NO_CERTIFICADO.Name = "NO_CERTIFICADO";
            this.NO_CERTIFICADO.Size = new System.Drawing.Size(733, 20);
            this.NO_CERTIFICADO.TabIndex = 10;
            this.NO_CERTIFICADO.TextChanged += new System.EventHandler(this.ID_TextChanged);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(12, 155);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(24, 13);
            this.label2.TabIndex = 9;
            this.label2.Text = "No.";
            // 
            // FECHA_DESDE
            // 
            this.FECHA_DESDE.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.FECHA_DESDE.Enabled = false;
            this.FECHA_DESDE.Location = new System.Drawing.Point(95, 178);
            this.FECHA_DESDE.Name = "FECHA_DESDE";
            this.FECHA_DESDE.Size = new System.Drawing.Size(733, 20);
            this.FECHA_DESDE.TabIndex = 12;
            this.FECHA_DESDE.TextChanged += new System.EventHandler(this.ID_TextChanged);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(10, 181);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(38, 13);
            this.label3.TabIndex = 11;
            this.label3.Text = "Desde";
            // 
            // FECHA_HASTA
            // 
            this.FECHA_HASTA.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.FECHA_HASTA.Enabled = false;
            this.FECHA_HASTA.Location = new System.Drawing.Point(95, 204);
            this.FECHA_HASTA.Name = "FECHA_HASTA";
            this.FECHA_HASTA.Size = new System.Drawing.Size(733, 20);
            this.FECHA_HASTA.TabIndex = 14;
            this.FECHA_HASTA.TextChanged += new System.EventHandler(this.ID_TextChanged);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(10, 207);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(35, 13);
            this.label4.TabIndex = 13;
            this.label4.Text = "Hasta";
            // 
            // LLAVE
            // 
            this.LLAVE.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.LLAVE.Enabled = false;
            this.LLAVE.Location = new System.Drawing.Point(95, 230);
            this.LLAVE.Name = "LLAVE";
            this.LLAVE.Size = new System.Drawing.Size(733, 20);
            this.LLAVE.TabIndex = 16;
            this.LLAVE.TextChanged += new System.EventHandler(this.ID_TextChanged);
            // 
            // PASSWORD
            // 
            this.PASSWORD.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.PASSWORD.Location = new System.Drawing.Point(95, 256);
            this.PASSWORD.Name = "PASSWORD";
            this.PASSWORD.Size = new System.Drawing.Size(240, 20);
            this.PASSWORD.TabIndex = 18;
            this.PASSWORD.TextChanged += new System.EventHandler(this.ID_TextChanged);
            this.PASSWORD.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.Validar_Caracteres);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(12, 259);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(73, 13);
            this.label5.TabIndex = 17;
            this.label5.Text = "Clave Privada";
            // 
            // button4
            // 
            this.button4.BackColor = System.Drawing.SystemColors.Control;
            this.button4.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button4.Image = global::FE_FX.Properties.Resources.Buscar;
            this.button4.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button4.Location = new System.Drawing.Point(5, 228);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(84, 23);
            this.button4.TabIndex = 15;
            this.button4.Text = "LLave";
            this.button4.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.button4.UseVisualStyleBackColor = false;
            this.button4.Click += new System.EventHandler(this.button4_Click);
            // 
            // button1
            // 
            this.button1.BackColor = System.Drawing.SystemColors.Control;
            this.button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button1.Image = global::FE_FX.Properties.Resources.Buscar;
            this.button1.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button1.Location = new System.Drawing.Point(5, 124);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(84, 23);
            this.button1.TabIndex = 7;
            this.button1.Text = "Certificado";
            this.button1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.button1.UseVisualStyleBackColor = false;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // button7
            // 
            this.button7.BackColor = System.Drawing.SystemColors.Control;
            this.button7.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button7.Image = global::FE_FX.Properties.Resources.Buscar;
            this.button7.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button7.Location = new System.Drawing.Point(5, 94);
            this.button7.Name = "button7";
            this.button7.Size = new System.Drawing.Size(84, 23);
            this.button7.TabIndex = 5;
            this.button7.Text = "Empresa";
            this.button7.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.button7.UseVisualStyleBackColor = false;
            this.button7.Click += new System.EventHandler(this.button7_Click);
            // 
            // button3
            // 
            this.button3.BackColor = System.Drawing.SystemColors.Control;
            this.button3.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button3.Image = global::FE_FX.Properties.Resources.Guardar;
            this.button3.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button3.Location = new System.Drawing.Point(80, 32);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(83, 23);
            this.button3.TabIndex = 2;
            this.button3.Text = "Guardar";
            this.button3.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.button3.UseVisualStyleBackColor = false;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // button2
            // 
            this.button2.BackColor = System.Drawing.SystemColors.Control;
            this.button2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button2.Image = global::FE_FX.Properties.Resources.Nuevo;
            this.button2.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button2.Location = new System.Drawing.Point(4, 32);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(70, 23);
            this.button2.TabIndex = 1;
            this.button2.Text = "Nuevo";
            this.button2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.button2.UseVisualStyleBackColor = false;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // ajax_loader
            // 
            this.ajax_loader.AccessibleDescription = "";
            this.ajax_loader.Image = global::FE_FX.Properties.Resources.ajax_loader;
            this.ajax_loader.Location = new System.Drawing.Point(277, 33);
            this.ajax_loader.Name = "ajax_loader";
            this.ajax_loader.Size = new System.Drawing.Size(58, 22);
            this.ajax_loader.TabIndex = 38;
            this.ajax_loader.TabStop = false;
            // 
            // ACTIVO
            // 
            this.ACTIVO.AutoSize = true;
            this.ACTIVO.Location = new System.Drawing.Point(95, 315);
            this.ACTIVO.Name = "ACTIVO";
            this.ACTIVO.Size = new System.Drawing.Size(56, 17);
            this.ACTIVO.TabIndex = 462;
            this.ACTIVO.Text = "Activo";
            this.ACTIVO.UseVisualStyleBackColor = true;
            // 
            // button5
            // 
            this.button5.BackColor = System.Drawing.SystemColors.Control;
            this.button5.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button5.Image = global::FE_FX.Properties.Resources.Buscar;
            this.button5.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button5.Location = new System.Drawing.Point(5, 282);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(84, 23);
            this.button5.TabIndex = 463;
            this.button5.Text = "PFX";
            this.button5.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.button5.UseVisualStyleBackColor = false;
            this.button5.Click += new System.EventHandler(this.button5_Click);
            // 
            // PFX
            // 
            this.PFX.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.PFX.Location = new System.Drawing.Point(95, 282);
            this.PFX.Name = "PFX";
            this.PFX.Size = new System.Drawing.Size(733, 20);
            this.PFX.TabIndex = 18;
            this.PFX.TextChanged += new System.EventHandler(this.ID_TextChanged);
            this.PFX.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.Validar_Caracteres);
            // 
            // button9
            // 
            this.button9.BackColor = System.Drawing.SystemColors.Control;
            this.button9.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button9.Image = global::FE_FX.Properties.Resources.Eliminar_Linea;
            this.button9.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button9.Location = new System.Drawing.Point(169, 32);
            this.button9.Name = "button9";
            this.button9.Size = new System.Drawing.Size(83, 23);
            this.button9.TabIndex = 470;
            this.button9.Text = "Eliminar";
            this.button9.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.button9.UseVisualStyleBackColor = false;
            this.button9.Click += new System.EventHandler(this.button9_Click);
            // 
            // button6
            // 
            this.button6.BackColor = System.Drawing.SystemColors.Control;
            this.button6.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button6.Image = global::FE_FX.Properties.Resources.Buscar;
            this.button6.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button6.Location = new System.Drawing.Point(5, 311);
            this.button6.Name = "button6";
            this.button6.Size = new System.Drawing.Size(84, 23);
            this.button6.TabIndex = 463;
            this.button6.Text = "Validar PFX";
            this.button6.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.button6.UseVisualStyleBackColor = false;
            this.button6.Click += new System.EventHandler(this.button6_Click);
            // 
            // checkBox1
            // 
            this.checkBox1.AutoSize = true;
            this.checkBox1.Location = new System.Drawing.Point(341, 258);
            this.checkBox1.Name = "checkBox1";
            this.checkBox1.Size = new System.Drawing.Size(98, 17);
            this.checkBox1.TabIndex = 471;
            this.checkBox1.Text = "Ver contraseña";
            this.checkBox1.UseVisualStyleBackColor = true;
            this.checkBox1.CheckedChanged += new System.EventHandler(this.checkBox1_CheckedChanged);
            // 
            // frmCertificado
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(835, 345);
            this.Controls.Add(this.checkBox1);
            this.Controls.Add(this.button9);
            this.Controls.Add(this.button6);
            this.Controls.Add(this.button5);
            this.Controls.Add(this.ACTIVO);
            this.Controls.Add(this.PFX);
            this.Controls.Add(this.PASSWORD);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.LLAVE);
            this.Controls.Add(this.button4);
            this.Controls.Add(this.FECHA_HASTA);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.FECHA_DESDE);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.NO_CERTIFICADO);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.CERTIFICADO);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.EMPRESA);
            this.Controls.Add(this.button7);
            this.Controls.Add(this.ID);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.button3);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.ajax_loader);
            this.Controls.Add(this.statusStrip1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.KeyPreview = true;
            this.MaximizeBox = false;
            this.Name = "frmCertificado";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Certificado";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frmUsuario_FormClosing);
            this.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.frmUsuario_KeyPress);
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ajax_loader)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel1;
        private System.Windows.Forms.PictureBox ajax_loader;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox ID;
        private System.Windows.Forms.Button button7;
        private System.Windows.Forms.TextBox EMPRESA;
        private System.Windows.Forms.TextBox CERTIFICADO;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.TextBox NO_CERTIFICADO;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox FECHA_DESDE;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox FECHA_HASTA;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox LLAVE;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.TextBox PASSWORD;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.CheckBox ACTIVO;
        private System.Windows.Forms.Button button5;
        private System.Windows.Forms.TextBox PFX;
        private System.Windows.Forms.Button button9;
        private System.Windows.Forms.Button button6;
        private System.Windows.Forms.CheckBox checkBox1;
    }
}