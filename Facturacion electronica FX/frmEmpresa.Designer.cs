﻿namespace FE_FX
{
    partial class frmEmpresa
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmEmpresa));
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.toolStripStatusLabel1 = new System.Windows.Forms.ToolStripStatusLabel();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.PAIS = new System.Windows.Forms.TextBox();
            this.button6 = new System.Windows.Forms.Button();
            this.ESTADO = new System.Windows.Forms.TextBox();
            this.MUNICIPIO = new System.Windows.Forms.TextBox();
            this.label15 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.REFERENCIA = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.LOCALIDAD = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.CP = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.COLONIA = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.INTERIOR = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.EXTERIOR = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.CALLE = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.checkBox1 = new System.Windows.Forms.CheckBox();
            this.ENVIO_CORREO = new System.Windows.Forms.CheckBox();
            this.label6 = new System.Windows.Forms.Label();
            this.button11 = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.CREDENCIALES = new System.Windows.Forms.CheckBox();
            this.ErrorCorreo = new System.Windows.Forms.CheckBox();
            this.SSL = new System.Windows.Forms.CheckBox();
            this.PASSWORD_EMAIL = new System.Windows.Forms.TextBox();
            this.SMTP = new System.Windows.Forms.TextBox();
            this.USUARIO = new System.Windows.Forms.TextBox();
            this.PUERTO = new System.Windows.Forms.TextBox();
            this.LbPuerto = new System.Windows.Forms.Label();
            this.LbPass = new System.Windows.Forms.Label();
            this.LbUsuario = new System.Windows.Forms.Label();
            this.LbServidor = new System.Windows.Forms.Label();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.verPassword = new System.Windows.Forms.CheckBox();
            this.APROXIMACION = new System.Windows.Forms.ComboBox();
            this.VALORES_DECIMALES = new System.Windows.Forms.TextBox();
            this.label26 = new System.Windows.Forms.Label();
            this.label27 = new System.Windows.Forms.Label();
            this.TIPO = new System.Windows.Forms.ComboBox();
            this.button5 = new System.Windows.Forms.Button();
            this.label19 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.PASSWORD_BD = new System.Windows.Forms.TextBox();
            this.USUARIO_BD = new System.Windows.Forms.TextBox();
            this.BD = new System.Windows.Forms.TextBox();
            this.label18 = new System.Windows.Forms.Label();
            this.SERVIDOR = new System.Windows.Forms.TextBox();
            this.label16 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.tabPage4 = new System.Windows.Forms.TabPage();
            this.label33 = new System.Windows.Forms.Label();
            this.TipoRelacionRMA = new System.Windows.Forms.TextBox();
            this.label30 = new System.Windows.Forms.Label();
            this.UsarPartNotationCliente = new System.Windows.Forms.TextBox();
            this.label29 = new System.Windows.Forms.Label();
            this.UsarCustomerPartDescripcionCliente = new System.Windows.Forms.TextBox();
            this.UsarDescripcionPersonalizadaCampo = new System.Windows.Forms.TextBox();
            this.DetenerAutomaticoRelacionado = new System.Windows.Forms.CheckBox();
            this.DetenerRelacionRMA = new System.Windows.Forms.CheckBox();
            this.UsarPartNotation = new System.Windows.Forms.CheckBox();
            this.UsarCustomerPartDescripcion = new System.Windows.Forms.CheckBox();
            this.UsarDescripcionPersonalizada = new System.Windows.Forms.CheckBox();
            this.label24 = new System.Windows.Forms.Label();
            this.CUSTOMER_ESPECIFICACION_LINEA = new System.Windows.Forms.TextBox();
            this.label25 = new System.Windows.Forms.Label();
            this.button9 = new System.Windows.Forms.Button();
            this.label21 = new System.Windows.Forms.Label();
            this.CUSTOMER_ID = new System.Windows.Forms.TextBox();
            this.label20 = new System.Windows.Forms.Label();
            this.tabPage5 = new System.Windows.Forms.TabPage();
            this.IMPRESION_AUTOMATICA_NDC = new System.Windows.Forms.CheckBox();
            this.IMPRESORA = new System.Windows.Forms.ComboBox();
            this.IMPRESION_AUTOMATICA = new System.Windows.Forms.CheckBox();
            this.tabPage6 = new System.Windows.Forms.TabPage();
            this.checkBox2 = new System.Windows.Forms.CheckBox();
            this.NDCRequiereRelacion = new System.Windows.Forms.CheckBox();
            this.ActivarComplementoCartaPorte = new System.Windows.Forms.CheckBox();
            this.PrioridadPartFiltro = new System.Windows.Forms.TextBox();
            this.CUSTOM_NOMBRE_CFDI = new System.Windows.Forms.TextBox();
            this.RAZON_SOLO_CLIENTE = new System.Windows.Forms.CheckBox();
            this.AGREGAR_LOTE = new System.Windows.Forms.CheckBox();
            this.PRIORIDAD_PART = new System.Windows.Forms.CheckBox();
            this.relacionLinea = new System.Windows.Forms.CheckBox();
            this.USAR_COMPLEMENTO_ADDR3 = new System.Windows.Forms.CheckBox();
            this.DESCUENTO_LINEAS = new System.Windows.Forms.CheckBox();
            this.label32 = new System.Windows.Forms.Label();
            this.ACTIVAR_COMPLEMENTO_CCE = new System.Windows.Forms.CheckBox();
            this.label31 = new System.Windows.Forms.Label();
            this.tabPage7 = new System.Windows.Forms.TabPage();
            this.portal = new System.Windows.Forms.TextBox();
            this.label28 = new System.Windows.Forms.Label();
            this.ID = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.RFC = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.ACTIVO = new System.Windows.Forms.CheckBox();
            this.button4 = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.ajax_loader = new System.Windows.Forms.PictureBox();
            this.REGIMEN_FISCAL = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.ENTITY_ID = new System.Windows.Forms.ComboBox();
            this.button7 = new System.Windows.Forms.Button();
            this.button10 = new System.Windows.Forms.Button();
            this.button8 = new System.Windows.Forms.Button();
            this.label22 = new System.Windows.Forms.Label();
            this.BD_AUXILIAR = new System.Windows.Forms.TextBox();
            this.label23 = new System.Windows.Forms.Label();
            this.SITE_ID = new System.Windows.Forms.TextBox();
            this.automatizacion = new System.Windows.Forms.CheckBox();
            this.statusStrip1.SuspendLayout();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.tabPage2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.tabPage3.SuspendLayout();
            this.tabPage4.SuspendLayout();
            this.tabPage5.SuspendLayout();
            this.tabPage6.SuspendLayout();
            this.tabPage7.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ajax_loader)).BeginInit();
            this.SuspendLayout();
            // 
            // statusStrip1
            // 
            this.statusStrip1.Dock = System.Windows.Forms.DockStyle.Top;
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripStatusLabel1});
            this.statusStrip1.Location = new System.Drawing.Point(0, 0);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(645, 22);
            this.statusStrip1.TabIndex = 0;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // toolStripStatusLabel1
            // 
            this.toolStripStatusLabel1.Name = "toolStripStatusLabel1";
            this.toolStripStatusLabel1.Size = new System.Drawing.Size(42, 17);
            this.toolStripStatusLabel1.Text = "Estado";
            // 
            // tabControl1
            // 
            this.tabControl1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Controls.Add(this.tabPage3);
            this.tabControl1.Controls.Add(this.tabPage4);
            this.tabControl1.Controls.Add(this.tabPage5);
            this.tabControl1.Controls.Add(this.tabPage6);
            this.tabControl1.Controls.Add(this.tabPage7);
            this.tabControl1.Location = new System.Drawing.Point(4, 156);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(634, 380);
            this.tabControl1.TabIndex = 21;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.PAIS);
            this.tabPage1.Controls.Add(this.button6);
            this.tabPage1.Controls.Add(this.ESTADO);
            this.tabPage1.Controls.Add(this.MUNICIPIO);
            this.tabPage1.Controls.Add(this.label15);
            this.tabPage1.Controls.Add(this.label13);
            this.tabPage1.Controls.Add(this.REFERENCIA);
            this.tabPage1.Controls.Add(this.label12);
            this.tabPage1.Controls.Add(this.LOCALIDAD);
            this.tabPage1.Controls.Add(this.label11);
            this.tabPage1.Controls.Add(this.CP);
            this.tabPage1.Controls.Add(this.label10);
            this.tabPage1.Controls.Add(this.COLONIA);
            this.tabPage1.Controls.Add(this.label9);
            this.tabPage1.Controls.Add(this.INTERIOR);
            this.tabPage1.Controls.Add(this.label8);
            this.tabPage1.Controls.Add(this.EXTERIOR);
            this.tabPage1.Controls.Add(this.label7);
            this.tabPage1.Controls.Add(this.CALLE);
            this.tabPage1.Controls.Add(this.label2);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(626, 354);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Dirección";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // PAIS
            // 
            this.PAIS.Location = new System.Drawing.Point(67, 89);
            this.PAIS.Name = "PAIS";
            this.PAIS.Size = new System.Drawing.Size(267, 20);
            this.PAIS.TabIndex = 15;
            // 
            // button6
            // 
            this.button6.BackColor = System.Drawing.SystemColors.Control;
            this.button6.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button6.Image = global::FE_FX.Properties.Resources.Buscar;
            this.button6.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button6.Location = new System.Drawing.Point(340, 89);
            this.button6.Name = "button6";
            this.button6.Size = new System.Drawing.Size(71, 22);
            this.button6.TabIndex = 16;
            this.button6.Text = "Estado";
            this.button6.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.button6.UseVisualStyleBackColor = false;
            // 
            // ESTADO
            // 
            this.ESTADO.Location = new System.Drawing.Point(417, 89);
            this.ESTADO.Name = "ESTADO";
            this.ESTADO.Size = new System.Drawing.Size(199, 20);
            this.ESTADO.TabIndex = 17;
            // 
            // MUNICIPIO
            // 
            this.MUNICIPIO.Location = new System.Drawing.Point(66, 115);
            this.MUNICIPIO.Name = "MUNICIPIO";
            this.MUNICIPIO.Size = new System.Drawing.Size(551, 20);
            this.MUNICIPIO.TabIndex = 19;
            this.MUNICIPIO.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.Verificar);
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(11, 118);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(52, 13);
            this.label15.TabIndex = 18;
            this.label15.Text = "Municipio";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(11, 92);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(29, 13);
            this.label13.TabIndex = 14;
            this.label13.Text = "País";
            // 
            // REFERENCIA
            // 
            this.REFERENCIA.Location = new System.Drawing.Point(404, 63);
            this.REFERENCIA.Name = "REFERENCIA";
            this.REFERENCIA.Size = new System.Drawing.Size(213, 20);
            this.REFERENCIA.TabIndex = 13;
            this.REFERENCIA.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.Verificar);
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(339, 66);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(59, 13);
            this.label12.TabIndex = 12;
            this.label12.Text = "Referencia";
            // 
            // LOCALIDAD
            // 
            this.LOCALIDAD.Location = new System.Drawing.Point(66, 63);
            this.LOCALIDAD.Name = "LOCALIDAD";
            this.LOCALIDAD.Size = new System.Drawing.Size(267, 20);
            this.LOCALIDAD.TabIndex = 11;
            this.LOCALIDAD.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.Verificar);
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(8, 66);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(53, 13);
            this.label11.TabIndex = 10;
            this.label11.Text = "Localidad";
            // 
            // CP
            // 
            this.CP.Location = new System.Drawing.Point(538, 37);
            this.CP.Name = "CP";
            this.CP.Size = new System.Drawing.Size(79, 20);
            this.CP.TabIndex = 9;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(505, 40);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(27, 13);
            this.label10.TabIndex = 8;
            this.label10.Text = "C.P.";
            // 
            // COLONIA
            // 
            this.COLONIA.Location = new System.Drawing.Point(66, 37);
            this.COLONIA.Name = "COLONIA";
            this.COLONIA.Size = new System.Drawing.Size(421, 20);
            this.COLONIA.TabIndex = 7;
            this.COLONIA.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.Verificar);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(7, 40);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(42, 13);
            this.label9.TabIndex = 6;
            this.label9.Text = "Colonia";
            // 
            // INTERIOR
            // 
            this.INTERIOR.Location = new System.Drawing.Point(538, 12);
            this.INTERIOR.Name = "INTERIOR";
            this.INTERIOR.Size = new System.Drawing.Size(79, 20);
            this.INTERIOR.TabIndex = 5;
            this.INTERIOR.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.Verificar);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(493, 15);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(39, 13);
            this.label8.TabIndex = 4;
            this.label8.Text = "Interior";
            // 
            // EXTERIOR
            // 
            this.EXTERIOR.Location = new System.Drawing.Point(417, 12);
            this.EXTERIOR.Name = "EXTERIOR";
            this.EXTERIOR.Size = new System.Drawing.Size(70, 20);
            this.EXTERIOR.TabIndex = 3;
            this.EXTERIOR.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.Verificar);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(366, 15);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(42, 13);
            this.label7.TabIndex = 2;
            this.label7.Text = "Exterior";
            // 
            // CALLE
            // 
            this.CALLE.Location = new System.Drawing.Point(66, 12);
            this.CALLE.Name = "CALLE";
            this.CALLE.Size = new System.Drawing.Size(286, 20);
            this.CALLE.TabIndex = 1;
            this.CALLE.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.Verificar);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(7, 15);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(30, 13);
            this.label2.TabIndex = 0;
            this.label2.Text = "Calle";
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.checkBox1);
            this.tabPage2.Controls.Add(this.ENVIO_CORREO);
            this.tabPage2.Controls.Add(this.label6);
            this.tabPage2.Controls.Add(this.button11);
            this.tabPage2.Controls.Add(this.button1);
            this.tabPage2.Controls.Add(this.CREDENCIALES);
            this.tabPage2.Controls.Add(this.ErrorCorreo);
            this.tabPage2.Controls.Add(this.SSL);
            this.tabPage2.Controls.Add(this.PASSWORD_EMAIL);
            this.tabPage2.Controls.Add(this.SMTP);
            this.tabPage2.Controls.Add(this.USUARIO);
            this.tabPage2.Controls.Add(this.PUERTO);
            this.tabPage2.Controls.Add(this.LbPuerto);
            this.tabPage2.Controls.Add(this.LbPass);
            this.tabPage2.Controls.Add(this.LbUsuario);
            this.tabPage2.Controls.Add(this.LbServidor);
            this.tabPage2.Controls.Add(this.pictureBox1);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(626, 354);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Configuración de Envio Correo Electrónico";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // checkBox1
            // 
            this.checkBox1.AutoSize = true;
            this.checkBox1.Location = new System.Drawing.Point(99, 133);
            this.checkBox1.Name = "checkBox1";
            this.checkBox1.Size = new System.Drawing.Size(98, 17);
            this.checkBox1.TabIndex = 13;
            this.checkBox1.Text = "Ver contraseña";
            this.checkBox1.UseVisualStyleBackColor = true;
            this.checkBox1.CheckedChanged += new System.EventHandler(this.checkBox1_CheckedChanged);
            // 
            // ENVIO_CORREO
            // 
            this.ENVIO_CORREO.AutoSize = true;
            this.ENVIO_CORREO.Location = new System.Drawing.Point(99, 110);
            this.ENVIO_CORREO.Name = "ENVIO_CORREO";
            this.ENVIO_CORREO.Size = new System.Drawing.Size(152, 17);
            this.ENVIO_CORREO.TabIndex = 12;
            this.ENVIO_CORREO.Text = "Enviar Correo Electronico?";
            this.ENVIO_CORREO.UseVisualStyleBackColor = true;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(336, 87);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(170, 13);
            this.label6.TabIndex = 11;
            this.label6.Text = "Espere mientras se envie el Correo";
            // 
            // button11
            // 
            this.button11.Location = new System.Drawing.Point(99, 179);
            this.button11.Name = "button11";
            this.button11.Size = new System.Drawing.Size(203, 23);
            this.button11.TabIndex = 10;
            this.button11.Text = "Realizar Prueba de Error";
            this.button11.UseVisualStyleBackColor = true;
            this.button11.Click += new System.EventHandler(this.button11_Click);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(99, 81);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(203, 23);
            this.button1.TabIndex = 10;
            this.button1.Text = "Realizar Prueba";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // CREDENCIALES
            // 
            this.CREDENCIALES.AutoSize = true;
            this.CREDENCIALES.Location = new System.Drawing.Point(311, 57);
            this.CREDENCIALES.Name = "CREDENCIALES";
            this.CREDENCIALES.Size = new System.Drawing.Size(184, 17);
            this.CREDENCIALES.TabIndex = 9;
            this.CREDENCIALES.Text = "Servidor Requiere Autentificación";
            this.CREDENCIALES.UseVisualStyleBackColor = true;
            // 
            // ErrorCorreo
            // 
            this.ErrorCorreo.AutoSize = true;
            this.ErrorCorreo.Location = new System.Drawing.Point(99, 156);
            this.ErrorCorreo.Name = "ErrorCorreo";
            this.ErrorCorreo.Size = new System.Drawing.Size(198, 17);
            this.ErrorCorreo.TabIndex = 6;
            this.ErrorCorreo.Text = "Usar esta cuenta para enviar errores";
            this.ErrorCorreo.UseVisualStyleBackColor = true;
            // 
            // SSL
            // 
            this.SSL.AutoSize = true;
            this.SSL.Location = new System.Drawing.Point(311, 33);
            this.SSL.Name = "SSL";
            this.SSL.Size = new System.Drawing.Size(220, 17);
            this.SSL.TabIndex = 6;
            this.SSL.Text = "El Servidor requiere una conexión segura";
            this.SSL.UseVisualStyleBackColor = true;
            // 
            // PASSWORD_EMAIL
            // 
            this.PASSWORD_EMAIL.Location = new System.Drawing.Point(99, 55);
            this.PASSWORD_EMAIL.Name = "PASSWORD_EMAIL";
            this.PASSWORD_EMAIL.Size = new System.Drawing.Size(203, 20);
            this.PASSWORD_EMAIL.TabIndex = 8;
            this.PASSWORD_EMAIL.UseSystemPasswordChar = true;
            this.PASSWORD_EMAIL.TextChanged += new System.EventHandler(this.Texto_Cambiado_TextChanged);
            // 
            // SMTP
            // 
            this.SMTP.Location = new System.Drawing.Point(99, 6);
            this.SMTP.Name = "SMTP";
            this.SMTP.Size = new System.Drawing.Size(203, 20);
            this.SMTP.TabIndex = 1;
            this.SMTP.TextChanged += new System.EventHandler(this.Texto_Cambiado_TextChanged);
            // 
            // USUARIO
            // 
            this.USUARIO.Location = new System.Drawing.Point(99, 30);
            this.USUARIO.Name = "USUARIO";
            this.USUARIO.Size = new System.Drawing.Size(203, 20);
            this.USUARIO.TabIndex = 5;
            this.USUARIO.TextChanged += new System.EventHandler(this.Texto_Cambiado_TextChanged);
            // 
            // PUERTO
            // 
            this.PUERTO.Location = new System.Drawing.Point(399, 6);
            this.PUERTO.Name = "PUERTO";
            this.PUERTO.Size = new System.Drawing.Size(56, 20);
            this.PUERTO.TabIndex = 3;
            this.PUERTO.Text = "0";
            this.PUERTO.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.PUERTO.TextChanged += new System.EventHandler(this.Texto_Cambiado_TextChanged);
            this.PUERTO.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.Solo_Numero_KeyPress);
            // 
            // LbPuerto
            // 
            this.LbPuerto.AutoSize = true;
            this.LbPuerto.Location = new System.Drawing.Point(308, 9);
            this.LbPuerto.Name = "LbPuerto";
            this.LbPuerto.Size = new System.Drawing.Size(85, 13);
            this.LbPuerto.TabIndex = 2;
            this.LbPuerto.Text = "Puerto de Salida";
            // 
            // LbPass
            // 
            this.LbPass.AutoSize = true;
            this.LbPass.Location = new System.Drawing.Point(8, 58);
            this.LbPass.Name = "LbPass";
            this.LbPass.Size = new System.Drawing.Size(61, 13);
            this.LbPass.TabIndex = 7;
            this.LbPass.Text = "Contraseña";
            // 
            // LbUsuario
            // 
            this.LbUsuario.AutoSize = true;
            this.LbUsuario.Location = new System.Drawing.Point(8, 33);
            this.LbUsuario.Name = "LbUsuario";
            this.LbUsuario.Size = new System.Drawing.Size(43, 13);
            this.LbUsuario.TabIndex = 4;
            this.LbUsuario.Text = "Usuario";
            // 
            // LbServidor
            // 
            this.LbServidor.AutoSize = true;
            this.LbServidor.Location = new System.Drawing.Point(8, 9);
            this.LbServidor.Name = "LbServidor";
            this.LbServidor.Size = new System.Drawing.Size(85, 13);
            this.LbServidor.TabIndex = 0;
            this.LbServidor.Text = "Servidor (SMTP)";
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(311, 84);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(22, 19);
            this.pictureBox1.TabIndex = 135;
            this.pictureBox1.TabStop = false;
            // 
            // tabPage3
            // 
            this.tabPage3.Controls.Add(this.verPassword);
            this.tabPage3.Controls.Add(this.APROXIMACION);
            this.tabPage3.Controls.Add(this.VALORES_DECIMALES);
            this.tabPage3.Controls.Add(this.label26);
            this.tabPage3.Controls.Add(this.label27);
            this.tabPage3.Controls.Add(this.TIPO);
            this.tabPage3.Controls.Add(this.button5);
            this.tabPage3.Controls.Add(this.label19);
            this.tabPage3.Controls.Add(this.label17);
            this.tabPage3.Controls.Add(this.PASSWORD_BD);
            this.tabPage3.Controls.Add(this.USUARIO_BD);
            this.tabPage3.Controls.Add(this.BD);
            this.tabPage3.Controls.Add(this.label18);
            this.tabPage3.Controls.Add(this.SERVIDOR);
            this.tabPage3.Controls.Add(this.label16);
            this.tabPage3.Controls.Add(this.label14);
            this.tabPage3.Location = new System.Drawing.Point(4, 22);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage3.Size = new System.Drawing.Size(626, 354);
            this.tabPage3.TabIndex = 2;
            this.tabPage3.Text = "Conexión";
            this.tabPage3.UseVisualStyleBackColor = true;
            // 
            // verPassword
            // 
            this.verPassword.AutoSize = true;
            this.verPassword.Location = new System.Drawing.Point(233, 112);
            this.verPassword.Name = "verPassword";
            this.verPassword.Size = new System.Drawing.Size(98, 17);
            this.verPassword.TabIndex = 484;
            this.verPassword.Text = "Ver contraseña";
            this.verPassword.UseVisualStyleBackColor = true;
            this.verPassword.CheckedChanged += new System.EventHandler(this.verPassword_CheckedChanged);
            // 
            // APROXIMACION
            // 
            this.APROXIMACION.FormattingEnabled = true;
            this.APROXIMACION.Items.AddRange(new object[] {
            "Truncar",
            "Redondear"});
            this.APROXIMACION.Location = new System.Drawing.Point(532, 5);
            this.APROXIMACION.Name = "APROXIMACION";
            this.APROXIMACION.Size = new System.Drawing.Size(86, 21);
            this.APROXIMACION.TabIndex = 480;
            // 
            // VALORES_DECIMALES
            // 
            this.VALORES_DECIMALES.Location = new System.Drawing.Point(532, 32);
            this.VALORES_DECIMALES.Name = "VALORES_DECIMALES";
            this.VALORES_DECIMALES.Size = new System.Drawing.Size(86, 20);
            this.VALORES_DECIMALES.TabIndex = 483;
            this.VALORES_DECIMALES.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.Solo_Numero_KeyPress);
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Location = new System.Drawing.Point(418, 35);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(92, 13);
            this.label26.TabIndex = 482;
            this.label26.Text = "Valores decimales";
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Location = new System.Drawing.Point(418, 9);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(108, 13);
            this.label27.TabIndex = 481;
            this.label27.Text = "Tipo de aproximación";
            // 
            // TIPO
            // 
            this.TIPO.FormattingEnabled = true;
            this.TIPO.Items.AddRange(new object[] {
            "SQLSERVER",
            "ACCESS",
            "ORACLE"});
            this.TIPO.Location = new System.Drawing.Point(93, 5);
            this.TIPO.Name = "TIPO";
            this.TIPO.Size = new System.Drawing.Size(124, 21);
            this.TIPO.TabIndex = 467;
            // 
            // button5
            // 
            this.button5.Location = new System.Drawing.Point(223, 4);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(108, 23);
            this.button5.TabIndex = 479;
            this.button5.Text = "Probar Conexión";
            this.button5.UseVisualStyleBackColor = true;
            this.button5.Click += new System.EventHandler(this.button5_Click);
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(10, 113);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(53, 13);
            this.label19.TabIndex = 478;
            this.label19.Text = "Password";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(10, 87);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(43, 13);
            this.label17.TabIndex = 477;
            this.label17.Text = "Usuario";
            // 
            // PASSWORD_BD
            // 
            this.PASSWORD_BD.Location = new System.Drawing.Point(93, 110);
            this.PASSWORD_BD.Name = "PASSWORD_BD";
            this.PASSWORD_BD.Size = new System.Drawing.Size(134, 20);
            this.PASSWORD_BD.TabIndex = 476;
            this.PASSWORD_BD.UseSystemPasswordChar = true;
            // 
            // USUARIO_BD
            // 
            this.USUARIO_BD.Location = new System.Drawing.Point(93, 84);
            this.USUARIO_BD.Name = "USUARIO_BD";
            this.USUARIO_BD.Size = new System.Drawing.Size(238, 20);
            this.USUARIO_BD.TabIndex = 475;
            // 
            // BD
            // 
            this.BD.Location = new System.Drawing.Point(93, 58);
            this.BD.Name = "BD";
            this.BD.Size = new System.Drawing.Size(238, 20);
            this.BD.TabIndex = 473;
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(10, 61);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(77, 13);
            this.label18.TabIndex = 472;
            this.label18.Text = "Base de Datos";
            // 
            // SERVIDOR
            // 
            this.SERVIDOR.Location = new System.Drawing.Point(93, 32);
            this.SERVIDOR.Name = "SERVIDOR";
            this.SERVIDOR.Size = new System.Drawing.Size(238, 20);
            this.SERVIDOR.TabIndex = 471;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(10, 35);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(46, 13);
            this.label16.TabIndex = 469;
            this.label16.Text = "Servidor";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(10, 9);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(28, 13);
            this.label14.TabIndex = 467;
            this.label14.Text = "Tipo";
            // 
            // tabPage4
            // 
            this.tabPage4.Controls.Add(this.label33);
            this.tabPage4.Controls.Add(this.TipoRelacionRMA);
            this.tabPage4.Controls.Add(this.label30);
            this.tabPage4.Controls.Add(this.UsarPartNotationCliente);
            this.tabPage4.Controls.Add(this.label29);
            this.tabPage4.Controls.Add(this.UsarCustomerPartDescripcionCliente);
            this.tabPage4.Controls.Add(this.UsarDescripcionPersonalizadaCampo);
            this.tabPage4.Controls.Add(this.DetenerAutomaticoRelacionado);
            this.tabPage4.Controls.Add(this.DetenerRelacionRMA);
            this.tabPage4.Controls.Add(this.UsarPartNotation);
            this.tabPage4.Controls.Add(this.UsarCustomerPartDescripcion);
            this.tabPage4.Controls.Add(this.UsarDescripcionPersonalizada);
            this.tabPage4.Controls.Add(this.label24);
            this.tabPage4.Controls.Add(this.CUSTOMER_ESPECIFICACION_LINEA);
            this.tabPage4.Controls.Add(this.label25);
            this.tabPage4.Controls.Add(this.button9);
            this.tabPage4.Controls.Add(this.label21);
            this.tabPage4.Controls.Add(this.CUSTOMER_ID);
            this.tabPage4.Controls.Add(this.label20);
            this.tabPage4.Location = new System.Drawing.Point(4, 22);
            this.tabPage4.Name = "tabPage4";
            this.tabPage4.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage4.Size = new System.Drawing.Size(626, 354);
            this.tabPage4.TabIndex = 3;
            this.tabPage4.Text = "Otros";
            this.tabPage4.UseVisualStyleBackColor = true;
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.Location = new System.Drawing.Point(283, 288);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(262, 13);
            this.label33.TabIndex = 485;
            this.label33.Text = "Tipo de relación por defecto del RMA aplicado Parcial";
            // 
            // TipoRelacionRMA
            // 
            this.TipoRelacionRMA.Location = new System.Drawing.Point(551, 285);
            this.TipoRelacionRMA.Name = "TipoRelacionRMA";
            this.TipoRelacionRMA.Size = new System.Drawing.Size(67, 20);
            this.TipoRelacionRMA.TabIndex = 484;
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Location = new System.Drawing.Point(6, 243);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(343, 13);
            this.label30.TabIndex = 483;
            this.label30.Text = "Clientes para agregar notas la línea de factura, si desea todos escriba *";
            // 
            // UsarPartNotationCliente
            // 
            this.UsarPartNotationCliente.Location = new System.Drawing.Point(9, 259);
            this.UsarPartNotationCliente.Name = "UsarPartNotationCliente";
            this.UsarPartNotationCliente.Size = new System.Drawing.Size(609, 20);
            this.UsarPartNotationCliente.TabIndex = 482;
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Location = new System.Drawing.Point(6, 182);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(445, 13);
            this.label29.TabIndex = 481;
            this.label29.Text = "Clientes para agregar número de parte del cliente la línea de factura, si desea t" +
    "odos escriba *";
            // 
            // UsarCustomerPartDescripcionCliente
            // 
            this.UsarCustomerPartDescripcionCliente.Location = new System.Drawing.Point(9, 198);
            this.UsarCustomerPartDescripcionCliente.Name = "UsarCustomerPartDescripcionCliente";
            this.UsarCustomerPartDescripcionCliente.Size = new System.Drawing.Size(609, 20);
            this.UsarCustomerPartDescripcionCliente.TabIndex = 480;
            // 
            // UsarDescripcionPersonalizadaCampo
            // 
            this.UsarDescripcionPersonalizadaCampo.Location = new System.Drawing.Point(381, 136);
            this.UsarDescripcionPersonalizadaCampo.Name = "UsarDescripcionPersonalizadaCampo";
            this.UsarDescripcionPersonalizadaCampo.Size = new System.Drawing.Size(237, 20);
            this.UsarDescripcionPersonalizadaCampo.TabIndex = 478;
            // 
            // DetenerAutomaticoRelacionado
            // 
            this.DetenerAutomaticoRelacionado.AutoSize = true;
            this.DetenerAutomaticoRelacionado.Location = new System.Drawing.Point(6, 310);
            this.DetenerAutomaticoRelacionado.Name = "DetenerAutomaticoRelacionado";
            this.DetenerAutomaticoRelacionado.Size = new System.Drawing.Size(351, 17);
            this.DetenerAutomaticoRelacionado.TabIndex = 479;
            this.DetenerAutomaticoRelacionado.Text = "Detener en automatico comentario: RELACIONADO A LA FACTURA";
            this.DetenerAutomaticoRelacionado.UseVisualStyleBackColor = true;
            this.DetenerAutomaticoRelacionado.CheckedChanged += new System.EventHandler(this.UsarPartNotation_CheckedChanged);
            // 
            // DetenerRelacionRMA
            // 
            this.DetenerRelacionRMA.AutoSize = true;
            this.DetenerRelacionRMA.Location = new System.Drawing.Point(6, 287);
            this.DetenerRelacionRMA.Name = "DetenerRelacionRMA";
            this.DetenerRelacionRMA.Size = new System.Drawing.Size(214, 17);
            this.DetenerRelacionRMA.TabIndex = 479;
            this.DetenerRelacionRMA.Text = "Relacionar RMA aplicado Parcialmente ";
            this.DetenerRelacionRMA.UseVisualStyleBackColor = true;
            this.DetenerRelacionRMA.CheckedChanged += new System.EventHandler(this.UsarPartNotation_CheckedChanged);
            // 
            // UsarPartNotation
            // 
            this.UsarPartNotation.AutoSize = true;
            this.UsarPartNotation.Location = new System.Drawing.Point(6, 224);
            this.UsarPartNotation.Name = "UsarPartNotation";
            this.UsarPartNotation.Size = new System.Drawing.Size(223, 17);
            this.UsarPartNotation.TabIndex = 479;
            this.UsarPartNotation.Text = "Usar notas de producto como descripción";
            this.UsarPartNotation.UseVisualStyleBackColor = true;
            this.UsarPartNotation.CheckedChanged += new System.EventHandler(this.UsarPartNotation_CheckedChanged);
            // 
            // UsarCustomerPartDescripcion
            // 
            this.UsarCustomerPartDescripcion.AutoSize = true;
            this.UsarCustomerPartDescripcion.Location = new System.Drawing.Point(6, 162);
            this.UsarCustomerPartDescripcion.Name = "UsarCustomerPartDescripcion";
            this.UsarCustomerPartDescripcion.Size = new System.Drawing.Size(236, 17);
            this.UsarCustomerPartDescripcion.TabIndex = 479;
            this.UsarCustomerPartDescripcion.Text = "Usar descripción número de parte del cliente";
            this.UsarCustomerPartDescripcion.UseVisualStyleBackColor = true;
            this.UsarCustomerPartDescripcion.CheckedChanged += new System.EventHandler(this.UsarCustomerPartDescripcion_CheckedChanged);
            // 
            // UsarDescripcionPersonalizada
            // 
            this.UsarDescripcionPersonalizada.AutoSize = true;
            this.UsarDescripcionPersonalizada.Location = new System.Drawing.Point(6, 139);
            this.UsarDescripcionPersonalizada.Name = "UsarDescripcionPersonalizada";
            this.UsarDescripcionPersonalizada.Size = new System.Drawing.Size(373, 17);
            this.UsarDescripcionPersonalizada.TabIndex = 479;
            this.UsarDescripcionPersonalizada.Text = "Usar descripción personalizada indique el campo del maestro de producto";
            this.UsarDescripcionPersonalizada.UseVisualStyleBackColor = true;
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Location = new System.Drawing.Point(6, 73);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(389, 13);
            this.label24.TabIndex = 475;
            this.label24.Text = "Clientes para agregar espeficaciónde la línea de factura, si desea todos escriba " +
    "*";
            // 
            // CUSTOMER_ESPECIFICACION_LINEA
            // 
            this.CUSTOMER_ESPECIFICACION_LINEA.Location = new System.Drawing.Point(9, 89);
            this.CUSTOMER_ESPECIFICACION_LINEA.Name = "CUSTOMER_ESPECIFICACION_LINEA";
            this.CUSTOMER_ESPECIFICACION_LINEA.Size = new System.Drawing.Size(609, 20);
            this.CUSTOMER_ESPECIFICACION_LINEA.TabIndex = 474;
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Location = new System.Drawing.Point(6, 112);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(295, 13);
            this.label25.TabIndex = 473;
            this.label25.Text = "Clientes separados por coma (,) para aplicar la especificación";
            // 
            // button9
            // 
            this.button9.BackColor = System.Drawing.SystemColors.Control;
            this.button9.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button9.Image = global::FE_FX.Properties.Resources.Guardar;
            this.button9.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button9.Location = new System.Drawing.Point(56, 47);
            this.button9.Name = "button9";
            this.button9.Size = new System.Drawing.Size(136, 23);
            this.button9.TabIndex = 471;
            this.button9.Text = "Asuntos por Clientes";
            this.button9.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.button9.UseVisualStyleBackColor = false;
            this.button9.Click += new System.EventHandler(this.button9_Click);
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(6, 11);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(44, 13);
            this.label21.TabIndex = 470;
            this.label21.Text = "Clientes";
            // 
            // CUSTOMER_ID
            // 
            this.CUSTOMER_ID.Location = new System.Drawing.Point(56, 8);
            this.CUSTOMER_ID.Name = "CUSTOMER_ID";
            this.CUSTOMER_ID.Size = new System.Drawing.Size(562, 20);
            this.CUSTOMER_ID.TabIndex = 469;
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(53, 31);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(405, 13);
            this.label20.TabIndex = 3;
            this.label20.Text = "Clientes separados por coma (,) para buscar las Direcciones de Embarque y Factura" +
    "r";
            // 
            // tabPage5
            // 
            this.tabPage5.Controls.Add(this.IMPRESION_AUTOMATICA_NDC);
            this.tabPage5.Controls.Add(this.IMPRESORA);
            this.tabPage5.Controls.Add(this.IMPRESION_AUTOMATICA);
            this.tabPage5.Location = new System.Drawing.Point(4, 22);
            this.tabPage5.Name = "tabPage5";
            this.tabPage5.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage5.Size = new System.Drawing.Size(626, 354);
            this.tabPage5.TabIndex = 4;
            this.tabPage5.Text = "Impresión";
            this.tabPage5.UseVisualStyleBackColor = true;
            // 
            // IMPRESION_AUTOMATICA_NDC
            // 
            this.IMPRESION_AUTOMATICA_NDC.AutoSize = true;
            this.IMPRESION_AUTOMATICA_NDC.Location = new System.Drawing.Point(6, 31);
            this.IMPRESION_AUTOMATICA_NDC.Name = "IMPRESION_AUTOMATICA_NDC";
            this.IMPRESION_AUTOMATICA_NDC.Size = new System.Drawing.Size(212, 17);
            this.IMPRESION_AUTOMATICA_NDC.TabIndex = 468;
            this.IMPRESION_AUTOMATICA_NDC.Text = "Impresión automatica de NDC al timbrar";
            this.IMPRESION_AUTOMATICA_NDC.UseVisualStyleBackColor = true;
            // 
            // IMPRESORA
            // 
            this.IMPRESORA.FormattingEnabled = true;
            this.IMPRESORA.Location = new System.Drawing.Point(262, 6);
            this.IMPRESORA.Name = "IMPRESORA";
            this.IMPRESORA.Size = new System.Drawing.Size(245, 21);
            this.IMPRESORA.TabIndex = 467;
            // 
            // IMPRESION_AUTOMATICA
            // 
            this.IMPRESION_AUTOMATICA.AutoSize = true;
            this.IMPRESION_AUTOMATICA.Location = new System.Drawing.Point(6, 8);
            this.IMPRESION_AUTOMATICA.Name = "IMPRESION_AUTOMATICA";
            this.IMPRESION_AUTOMATICA.Size = new System.Drawing.Size(227, 17);
            this.IMPRESION_AUTOMATICA.TabIndex = 462;
            this.IMPRESION_AUTOMATICA.Text = "Impresión automatica de facturas al timbrar";
            this.IMPRESION_AUTOMATICA.UseVisualStyleBackColor = true;
            // 
            // tabPage6
            // 
            this.tabPage6.Controls.Add(this.checkBox2);
            this.tabPage6.Controls.Add(this.NDCRequiereRelacion);
            this.tabPage6.Controls.Add(this.ActivarComplementoCartaPorte);
            this.tabPage6.Controls.Add(this.PrioridadPartFiltro);
            this.tabPage6.Controls.Add(this.CUSTOM_NOMBRE_CFDI);
            this.tabPage6.Controls.Add(this.RAZON_SOLO_CLIENTE);
            this.tabPage6.Controls.Add(this.AGREGAR_LOTE);
            this.tabPage6.Controls.Add(this.PRIORIDAD_PART);
            this.tabPage6.Controls.Add(this.relacionLinea);
            this.tabPage6.Controls.Add(this.USAR_COMPLEMENTO_ADDR3);
            this.tabPage6.Controls.Add(this.DESCUENTO_LINEAS);
            this.tabPage6.Controls.Add(this.label32);
            this.tabPage6.Controls.Add(this.ACTIVAR_COMPLEMENTO_CCE);
            this.tabPage6.Controls.Add(this.label31);
            this.tabPage6.Location = new System.Drawing.Point(4, 22);
            this.tabPage6.Name = "tabPage6";
            this.tabPage6.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage6.Size = new System.Drawing.Size(626, 354);
            this.tabPage6.TabIndex = 5;
            this.tabPage6.Text = "Complementos";
            this.tabPage6.UseVisualStyleBackColor = true;
            // 
            // checkBox2
            // 
            this.checkBox2.AutoSize = true;
            this.checkBox2.Location = new System.Drawing.Point(6, 214);
            this.checkBox2.Name = "checkBox2";
            this.checkBox2.Size = new System.Drawing.Size(246, 17);
            this.checkBox2.TabIndex = 482;
            this.checkBox2.Text = "NDC Restringir, que tenga asociación de CFDI";
            this.checkBox2.UseVisualStyleBackColor = true;
            // 
            // NDCRequiereRelacion
            // 
            this.NDCRequiereRelacion.AutoSize = true;
            this.NDCRequiereRelacion.Location = new System.Drawing.Point(6, 191);
            this.NDCRequiereRelacion.Name = "NDCRequiereRelacion";
            this.NDCRequiereRelacion.Size = new System.Drawing.Size(200, 17);
            this.NDCRequiereRelacion.TabIndex = 482;
            this.NDCRequiereRelacion.Text = "El timbrado de NDC requiere relación";
            this.NDCRequiereRelacion.UseVisualStyleBackColor = true;
            // 
            // ActivarComplementoCartaPorte
            // 
            this.ActivarComplementoCartaPorte.AutoSize = true;
            this.ActivarComplementoCartaPorte.Location = new System.Drawing.Point(6, 168);
            this.ActivarComplementoCartaPorte.Name = "ActivarComplementoCartaPorte";
            this.ActivarComplementoCartaPorte.Size = new System.Drawing.Size(195, 17);
            this.ActivarComplementoCartaPorte.TabIndex = 481;
            this.ActivarComplementoCartaPorte.Text = "Activar complemento de Carta porte";
            this.ActivarComplementoCartaPorte.UseVisualStyleBackColor = true;
            // 
            // PrioridadPartFiltro
            // 
            this.PrioridadPartFiltro.Location = new System.Drawing.Point(250, 97);
            this.PrioridadPartFiltro.Name = "PrioridadPartFiltro";
            this.PrioridadPartFiltro.Size = new System.Drawing.Size(368, 20);
            this.PrioridadPartFiltro.TabIndex = 480;
            // 
            // CUSTOM_NOMBRE_CFDI
            // 
            this.CUSTOM_NOMBRE_CFDI.Location = new System.Drawing.Point(179, 50);
            this.CUSTOM_NOMBRE_CFDI.Name = "CUSTOM_NOMBRE_CFDI";
            this.CUSTOM_NOMBRE_CFDI.Size = new System.Drawing.Size(439, 20);
            this.CUSTOM_NOMBRE_CFDI.TabIndex = 39;
            // 
            // RAZON_SOLO_CLIENTE
            // 
            this.RAZON_SOLO_CLIENTE.AutoSize = true;
            this.RAZON_SOLO_CLIENTE.Location = new System.Drawing.Point(6, 145);
            this.RAZON_SOLO_CLIENTE.Name = "RAZON_SOLO_CLIENTE";
            this.RAZON_SOLO_CLIENTE.Size = new System.Drawing.Size(317, 17);
            this.RAZON_SOLO_CLIENTE.TabIndex = 479;
            this.RAZON_SOLO_CLIENTE.Text = "Razón del CFDI solo del cliente (no la dirección de embarque)";
            this.RAZON_SOLO_CLIENTE.UseVisualStyleBackColor = true;
            // 
            // AGREGAR_LOTE
            // 
            this.AGREGAR_LOTE.AutoSize = true;
            this.AGREGAR_LOTE.Location = new System.Drawing.Point(6, 122);
            this.AGREGAR_LOTE.Name = "AGREGAR_LOTE";
            this.AGREGAR_LOTE.Size = new System.Drawing.Size(159, 17);
            this.AGREGAR_LOTE.TabIndex = 478;
            this.AGREGAR_LOTE.Text = "Agregar Lote en descripción";
            this.AGREGAR_LOTE.UseVisualStyleBackColor = true;
            // 
            // PRIORIDAD_PART
            // 
            this.PRIORIDAD_PART.AutoSize = true;
            this.PRIORIDAD_PART.Location = new System.Drawing.Point(6, 99);
            this.PRIORIDAD_PART.Name = "PRIORIDAD_PART";
            this.PRIORIDAD_PART.Size = new System.Drawing.Size(202, 17);
            this.PRIORIDAD_PART.TabIndex = 476;
            this.PRIORIDAD_PART.Text = "Prioridad de Customer Part luego Part";
            this.PRIORIDAD_PART.UseVisualStyleBackColor = true;
            // 
            // relacionLinea
            // 
            this.relacionLinea.AutoSize = true;
            this.relacionLinea.Location = new System.Drawing.Point(6, 29);
            this.relacionLinea.Name = "relacionLinea";
            this.relacionLinea.Size = new System.Drawing.Size(180, 17);
            this.relacionLinea.TabIndex = 1;
            this.relacionLinea.Text = "Relacionar las NDC a nivel línea";
            this.relacionLinea.UseVisualStyleBackColor = true;
            // 
            // USAR_COMPLEMENTO_ADDR3
            // 
            this.USAR_COMPLEMENTO_ADDR3.AutoSize = true;
            this.USAR_COMPLEMENTO_ADDR3.Location = new System.Drawing.Point(6, 52);
            this.USAR_COMPLEMENTO_ADDR3.Name = "USAR_COMPLEMENTO_ADDR3";
            this.USAR_COMPLEMENTO_ADDR3.Size = new System.Drawing.Size(167, 17);
            this.USAR_COMPLEMENTO_ADDR3.TabIndex = 477;
            this.USAR_COMPLEMENTO_ADDR3.Text = "Usar complemento de nombre";
            this.USAR_COMPLEMENTO_ADDR3.UseVisualStyleBackColor = true;
            // 
            // DESCUENTO_LINEAS
            // 
            this.DESCUENTO_LINEAS.AutoSize = true;
            this.DESCUENTO_LINEAS.Location = new System.Drawing.Point(6, 76);
            this.DESCUENTO_LINEAS.Name = "DESCUENTO_LINEAS";
            this.DESCUENTO_LINEAS.Size = new System.Drawing.Size(456, 17);
            this.DESCUENTO_LINEAS.TabIndex = 472;
            this.DESCUENTO_LINEAS.Text = "Aplicar el descuento sobre las líneas, esto implica que el descuento no vendrá de" +
    "sglozado.";
            this.DESCUENTO_LINEAS.UseVisualStyleBackColor = true;
            // 
            // label32
            // 
            this.label32.AutoSize = true;
            this.label32.Location = new System.Drawing.Point(285, 33);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(333, 13);
            this.label32.TabIndex = 18;
            this.label32.Text = "Usar NAME, ADDR_1,ADDR_2 o ADDR_3 para agregarlo al nombre";
            // 
            // ACTIVAR_COMPLEMENTO_CCE
            // 
            this.ACTIVAR_COMPLEMENTO_CCE.AutoSize = true;
            this.ACTIVAR_COMPLEMENTO_CCE.Location = new System.Drawing.Point(6, 6);
            this.ACTIVAR_COMPLEMENTO_CCE.Name = "ACTIVAR_COMPLEMENTO_CCE";
            this.ACTIVAR_COMPLEMENTO_CCE.Size = new System.Drawing.Size(225, 17);
            this.ACTIVAR_COMPLEMENTO_CCE.TabIndex = 0;
            this.ACTIVAR_COMPLEMENTO_CCE.Text = "Activar complemento de Comercio Exterior";
            this.ACTIVAR_COMPLEMENTO_CCE.UseVisualStyleBackColor = true;
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.Location = new System.Drawing.Point(214, 102);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(29, 13);
            this.label31.TabIndex = 18;
            this.label31.Text = "Filtro";
            // 
            // tabPage7
            // 
            this.tabPage7.Controls.Add(this.portal);
            this.tabPage7.Controls.Add(this.label28);
            this.tabPage7.Location = new System.Drawing.Point(4, 22);
            this.tabPage7.Name = "tabPage7";
            this.tabPage7.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage7.Size = new System.Drawing.Size(626, 354);
            this.tabPage7.TabIndex = 6;
            this.tabPage7.Text = "Portal";
            this.tabPage7.UseVisualStyleBackColor = true;
            // 
            // portal
            // 
            this.portal.Location = new System.Drawing.Point(49, 6);
            this.portal.Name = "portal";
            this.portal.Size = new System.Drawing.Size(571, 20);
            this.portal.TabIndex = 9;
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Location = new System.Drawing.Point(9, 9);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(34, 13);
            this.label28.TabIndex = 8;
            this.label28.Text = "Portal";
            // 
            // ID
            // 
            this.ID.Location = new System.Drawing.Point(258, 56);
            this.ID.Name = "ID";
            this.ID.Size = new System.Drawing.Size(380, 20);
            this.ID.TabIndex = 10;
            this.ID.TextChanged += new System.EventHandler(this.Texto_Cambiado_TextChanged);
            this.ID.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.Verificar);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(204, 60);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(48, 13);
            this.label1.TabIndex = 9;
            this.label1.Text = "Empresa";
            // 
            // RFC
            // 
            this.RFC.Location = new System.Drawing.Point(57, 82);
            this.RFC.Name = "RFC";
            this.RFC.Size = new System.Drawing.Size(137, 20);
            this.RFC.TabIndex = 12;
            this.RFC.TextChanged += new System.EventHandler(this.Texto_Cambiado_TextChanged);
            this.RFC.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.Verificar);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(5, 85);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(28, 13);
            this.label3.TabIndex = 11;
            this.label3.Text = "RFC";
            // 
            // ACTIVO
            // 
            this.ACTIVO.AutoSize = true;
            this.ACTIVO.Location = new System.Drawing.Point(207, 84);
            this.ACTIVO.Name = "ACTIVO";
            this.ACTIVO.Size = new System.Drawing.Size(56, 17);
            this.ACTIVO.TabIndex = 13;
            this.ACTIVO.Text = "Activo";
            this.ACTIVO.UseVisualStyleBackColor = true;
            // 
            // button4
            // 
            this.button4.BackColor = System.Drawing.SystemColors.Control;
            this.button4.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button4.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button4.Location = new System.Drawing.Point(389, 28);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(183, 23);
            this.button4.TabIndex = 5;
            this.button4.Text = "CFDI Configuración Proveedor";
            this.button4.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.button4.UseVisualStyleBackColor = false;
            this.button4.Click += new System.EventHandler(this.button4_Click);
            // 
            // button3
            // 
            this.button3.BackColor = System.Drawing.SystemColors.Control;
            this.button3.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button3.Image = global::FE_FX.Properties.Resources.Guardar;
            this.button3.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button3.Location = new System.Drawing.Point(80, 27);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(83, 23);
            this.button3.TabIndex = 2;
            this.button3.Text = "Guardar";
            this.button3.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.button3.UseVisualStyleBackColor = false;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // button2
            // 
            this.button2.BackColor = System.Drawing.SystemColors.Control;
            this.button2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button2.Image = global::FE_FX.Properties.Resources.Nuevo;
            this.button2.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button2.Location = new System.Drawing.Point(4, 27);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(70, 23);
            this.button2.TabIndex = 1;
            this.button2.Text = "Nuevo";
            this.button2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.button2.UseVisualStyleBackColor = false;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // ajax_loader
            // 
            this.ajax_loader.AccessibleDescription = "";
            this.ajax_loader.Image = global::FE_FX.Properties.Resources.ajax_loader;
            this.ajax_loader.Location = new System.Drawing.Point(578, 29);
            this.ajax_loader.Name = "ajax_loader";
            this.ajax_loader.Size = new System.Drawing.Size(58, 22);
            this.ajax_loader.TabIndex = 38;
            this.ajax_loader.TabStop = false;
            // 
            // REGIMEN_FISCAL
            // 
            this.REGIMEN_FISCAL.Location = new System.Drawing.Point(92, 107);
            this.REGIMEN_FISCAL.Name = "REGIMEN_FISCAL";
            this.REGIMEN_FISCAL.Size = new System.Drawing.Size(546, 20);
            this.REGIMEN_FISCAL.TabIndex = 19;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(7, 110);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(79, 13);
            this.label4.TabIndex = 18;
            this.label4.Text = "Regimen Fiscal";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(5, 59);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(43, 13);
            this.label5.TabIndex = 6;
            this.label5.Text = "Entidad";
            // 
            // ENTITY_ID
            // 
            this.ENTITY_ID.FormattingEnabled = true;
            this.ENTITY_ID.Location = new System.Drawing.Point(57, 55);
            this.ENTITY_ID.Name = "ENTITY_ID";
            this.ENTITY_ID.Size = new System.Drawing.Size(103, 21);
            this.ENTITY_ID.TabIndex = 7;
            // 
            // button7
            // 
            this.button7.BackColor = System.Drawing.SystemColors.Control;
            this.button7.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button7.Image = global::FE_FX.Properties.Resources.Eliminar_Linea;
            this.button7.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button7.Location = new System.Drawing.Point(169, 27);
            this.button7.Name = "button7";
            this.button7.Size = new System.Drawing.Size(83, 23);
            this.button7.TabIndex = 3;
            this.button7.Text = "Eliminar";
            this.button7.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.button7.UseVisualStyleBackColor = false;
            this.button7.Click += new System.EventHandler(this.button7_Click);
            // 
            // button10
            // 
            this.button10.BackColor = System.Drawing.SystemColors.Control;
            this.button10.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button10.Image = global::FE_FX.Properties.Resources.Guardar;
            this.button10.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button10.Location = new System.Drawing.Point(258, 27);
            this.button10.Name = "button10";
            this.button10.Size = new System.Drawing.Size(83, 23);
            this.button10.TabIndex = 4;
            this.button10.Text = "Duplicar";
            this.button10.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.button10.UseVisualStyleBackColor = false;
            this.button10.Click += new System.EventHandler(this.button10_Click);
            // 
            // button8
            // 
            this.button8.BackColor = System.Drawing.SystemColors.Control;
            this.button8.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button8.Image = global::FE_FX.Properties.Resources.Actualizar;
            this.button8.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button8.Location = new System.Drawing.Point(166, 55);
            this.button8.Name = "button8";
            this.button8.Size = new System.Drawing.Size(28, 23);
            this.button8.TabIndex = 8;
            this.button8.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.button8.UseVisualStyleBackColor = false;
            this.button8.Click += new System.EventHandler(this.button8_Click_1);
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Location = new System.Drawing.Point(414, 85);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(113, 13);
            this.label22.TabIndex = 16;
            this.label22.Text = "Base de Datos Auxiliar";
            // 
            // BD_AUXILIAR
            // 
            this.BD_AUXILIAR.Location = new System.Drawing.Point(533, 82);
            this.BD_AUXILIAR.Name = "BD_AUXILIAR";
            this.BD_AUXILIAR.Size = new System.Drawing.Size(105, 20);
            this.BD_AUXILIAR.TabIndex = 17;
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Location = new System.Drawing.Point(269, 84);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(25, 13);
            this.label23.TabIndex = 14;
            this.label23.Text = "Site";
            // 
            // SITE_ID
            // 
            this.SITE_ID.Location = new System.Drawing.Point(307, 82);
            this.SITE_ID.Name = "SITE_ID";
            this.SITE_ID.Size = new System.Drawing.Size(101, 20);
            this.SITE_ID.TabIndex = 15;
            // 
            // automatizacion
            // 
            this.automatizacion.AutoSize = true;
            this.automatizacion.Location = new System.Drawing.Point(4, 133);
            this.automatizacion.Name = "automatizacion";
            this.automatizacion.Size = new System.Drawing.Size(154, 17);
            this.automatizacion.TabIndex = 20;
            this.automatizacion.Text = "Ejecutar en automatización";
            this.automatizacion.UseVisualStyleBackColor = true;
            // 
            // frmEmpresa
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(645, 548);
            this.Controls.Add(this.automatizacion);
            this.Controls.Add(this.SITE_ID);
            this.Controls.Add(this.BD_AUXILIAR);
            this.Controls.Add(this.label23);
            this.Controls.Add(this.label22);
            this.Controls.Add(this.button8);
            this.Controls.Add(this.button10);
            this.Controls.Add(this.button7);
            this.Controls.Add(this.ENTITY_ID);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.REGIMEN_FISCAL);
            this.Controls.Add(this.button4);
            this.Controls.Add(this.ACTIVO);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.RFC);
            this.Controls.Add(this.ID);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.tabControl1);
            this.Controls.Add(this.button3);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.ajax_loader);
            this.Controls.Add(this.statusStrip1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.KeyPreview = true;
            this.MaximizeBox = false;
            this.Name = "frmEmpresa";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Empresa";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frmUsuario_FormClosing);
            this.Load += new System.EventHandler(this.frmEmpresa_Load);
            this.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.frmUsuario_KeyPress);
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage1.PerformLayout();
            this.tabPage2.ResumeLayout(false);
            this.tabPage2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.tabPage3.ResumeLayout(false);
            this.tabPage3.PerformLayout();
            this.tabPage4.ResumeLayout(false);
            this.tabPage4.PerformLayout();
            this.tabPage5.ResumeLayout(false);
            this.tabPage5.PerformLayout();
            this.tabPage6.ResumeLayout(false);
            this.tabPage6.PerformLayout();
            this.tabPage7.ResumeLayout(false);
            this.tabPage7.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ajax_loader)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel1;
        private System.Windows.Forms.PictureBox ajax_loader;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.CheckBox CREDENCIALES;
        private System.Windows.Forms.CheckBox SSL;
        private System.Windows.Forms.TextBox PASSWORD_EMAIL;
        private System.Windows.Forms.TextBox SMTP;
        private System.Windows.Forms.TextBox USUARIO;
        private System.Windows.Forms.TextBox PUERTO;
        private System.Windows.Forms.Label LbPuerto;
        private System.Windows.Forms.Label LbPass;
        private System.Windows.Forms.Label LbUsuario;
        private System.Windows.Forms.Label LbServidor;
        private System.Windows.Forms.TextBox ID;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox RFC;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox MUNICIPIO;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.TextBox REFERENCIA;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.TextBox LOCALIDAD;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TextBox CP;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox COLONIA;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox INTERIOR;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox EXTERIOR;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox CALLE;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.CheckBox ACTIVO;
        private System.Windows.Forms.Button button6;
        private System.Windows.Forms.TextBox ESTADO;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.TextBox REGIMEN_FISCAL;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.ComboBox ENTITY_ID;
        private System.Windows.Forms.TabPage tabPage3;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.TextBox USUARIO_BD;
        private System.Windows.Forms.TextBox BD;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.TextBox SERVIDOR;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.TextBox PASSWORD_BD;
        private System.Windows.Forms.Button button5;
        private System.Windows.Forms.ComboBox TIPO;
        private System.Windows.Forms.Button button7;
        private System.Windows.Forms.CheckBox ENVIO_CORREO;
        private System.Windows.Forms.Button button10;
        private System.Windows.Forms.TabPage tabPage4;
        private System.Windows.Forms.TextBox CUSTOMER_ID;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Button button8;
        private System.Windows.Forms.Button button9;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.TextBox BD_AUXILIAR;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.TextBox SITE_ID;
        private System.Windows.Forms.CheckBox DESCUENTO_LINEAS;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.TextBox CUSTOMER_ESPECIFICACION_LINEA;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.TabPage tabPage5;
        private System.Windows.Forms.CheckBox IMPRESION_AUTOMATICA;
        private System.Windows.Forms.ComboBox IMPRESORA;
        private System.Windows.Forms.CheckBox IMPRESION_AUTOMATICA_NDC;
        private System.Windows.Forms.TabPage tabPage6;
        private System.Windows.Forms.CheckBox ACTIVAR_COMPLEMENTO_CCE;
        private System.Windows.Forms.TextBox PAIS;
        private System.Windows.Forms.ComboBox APROXIMACION;
        private System.Windows.Forms.TextBox VALORES_DECIMALES;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.CheckBox checkBox1;
        private System.Windows.Forms.CheckBox relacionLinea;
        private System.Windows.Forms.CheckBox verPassword;
        private System.Windows.Forms.CheckBox automatizacion;
        private System.Windows.Forms.TabPage tabPage7;
        private System.Windows.Forms.TextBox portal;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.CheckBox PRIORIDAD_PART;
        private System.Windows.Forms.CheckBox USAR_COMPLEMENTO_ADDR3;
        private System.Windows.Forms.CheckBox AGREGAR_LOTE;
        private System.Windows.Forms.CheckBox RAZON_SOLO_CLIENTE;
        private System.Windows.Forms.TextBox CUSTOM_NOMBRE_CFDI;
        private System.Windows.Forms.TextBox UsarDescripcionPersonalizadaCampo;
        private System.Windows.Forms.CheckBox UsarDescripcionPersonalizada;
        private System.Windows.Forms.CheckBox UsarCustomerPartDescripcion;
        private System.Windows.Forms.CheckBox UsarPartNotation;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.TextBox UsarCustomerPartDescripcionCliente;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.TextBox UsarPartNotationCliente;
        private System.Windows.Forms.TextBox PrioridadPartFiltro;
        private System.Windows.Forms.Label label31;
        private System.Windows.Forms.CheckBox ActivarComplementoCartaPorte;
        private System.Windows.Forms.CheckBox NDCRequiereRelacion;
        private System.Windows.Forms.Label label32;
        private System.Windows.Forms.CheckBox checkBox2;
        private System.Windows.Forms.CheckBox ErrorCorreo;
        private System.Windows.Forms.Button button11;
        private System.Windows.Forms.CheckBox DetenerRelacionRMA;
        private System.Windows.Forms.Label label33;
        private System.Windows.Forms.TextBox TipoRelacionRMA;
        private System.Windows.Forms.CheckBox DetenerAutomaticoRelacionado;
    }
}