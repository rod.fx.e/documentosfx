﻿using System;
using System.Collections.Generic;

using System.Text;
using System.Data;
using System.Windows;
using System.Windows.Forms;
using Generales;

namespace FE_FX
{
    class cESTADO
    {
        private string sSQL = "";
        public string ROW_ID { get; set; }
        public string ID { get; set; }
        public string ACTIVO { get; set; }

        private cCONEXCION oData = new cCONEXCION("");

        public cESTADO()
        {
            limpiar();
        }

        public cESTADO(cCONEXCION pData)
        {
            oData = pData;
            limpiar();
        }

        public void limpiar()
        {
            ROW_ID = "";
            ID = "";
            ACTIVO = "";
        }

        public bool eliminar(string ROW_IDp)
        {
            if (ROW_IDp != "")
            {
                sSQL = "DELETE FROM ESTADO WHERE ROW_ID=" + ROW_IDp;
                oData.EjecutarConsulta(sSQL);
                return true;
            }
            return false;

        }

        public List<cESTADO> todos(string BUSQUEDA)
        {

            List<cESTADO> lista = new List<cESTADO>();

            sSQL  = " SELECT * ";
            sSQL += " FROM ESTADO ";
            sSQL += " WHERE ID LIKE '%" + BUSQUEDA + "%' ";
            DataTable oDataTable = oData.EjecutarConsulta(sSQL);
            foreach (DataRow oDataRow in oDataTable.Rows)
            {
                lista.Add(new cESTADO()
                {
                    ROW_ID = oDataRow["ROW_ID"].ToString()
                   ,ID = oDataRow["ID"].ToString()
                   ,
                    ACTIVO = oDataRow["ACTIVO"].ToString()
                });
            }
            return lista;
        }

        public bool cargar(string ROW_IDp)
        {
            sSQL  = " SELECT * ";
            sSQL += " FROM ESTADO ";
            sSQL += " WHERE ROW_ID=" + ROW_IDp + " ";
            DataTable oDataTable = oData.EjecutarConsulta(sSQL);
            if (oDataTable != null)
            {
                foreach (DataRow oDataRow in oDataTable.Rows)
                {
                    //Añadir el Registro
                    ROW_ID = oDataRow["ROW_ID"].ToString();
                    ID = oDataRow["ID"].ToString();
                    ACTIVO = oDataRow["ACTIVO"].ToString();
                    return true;
                }
            }
            return false;
        }

        public bool guardar()
        {
            if (ID.Trim() == "")
            {
                MessageBox.Show("El Nombre es un campo requerido.", Application.ProductName + "-" + Application.ProductVersion, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return false;
            }



            if (ROW_ID == "")
            {

                //Verificar que no este repetido
                foreach (cESTADO registro in todos(ID))
                {
                    MessageBox.Show("El Estado " + ID + " ya existe.", Application.ProductName + "-" + Application.ProductVersion, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                    return false;
                }

                sSQL = " INSERT INTO ESTADO ";
                sSQL += " ( ";
                sSQL += " ID";
                sSQL += ", ACTIVO";
                sSQL += " )";
                sSQL += " VALUES ";
                sSQL += "(";
                sSQL += " '" + ID + "'";
                sSQL += ", '" + ACTIVO + "'";
                sSQL += ")";
                DataTable oDataTable = oData.EjecutarConsulta(sSQL);
                if (oDataTable!=null)
                {

                    sSQL = "SELECT TOP 1 MAX(ROW_ID) as ROW_ID FROM ESTADO";
                    oDataTable = oData.EjecutarConsulta(sSQL);
                    if (oDataTable != null)
                    {
                        foreach (DataRow oDataRow in oDataTable.Rows)
                        {
                            ROW_ID = oDataRow["ROW_ID"].ToString();
                        }
                    }
                }

            }
            else
            {
                sSQL = " UPDATE ESTADO ";
                sSQL += " SET ID='" + ID + "'";
                sSQL += ",ACTIVO='" + ACTIVO + "'";
                sSQL += " WHERE ROW_ID=" + ROW_ID + " ";
                oData.EjecutarConsulta(sSQL);

            }
            return true;

        }
    }
}
