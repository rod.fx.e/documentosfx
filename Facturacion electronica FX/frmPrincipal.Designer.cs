﻿namespace FE_FX
{
    partial class frmPrincipal
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmPrincipal));
            this.dtgrdGeneral = new System.Windows.Forms.DataGridView();
            this.toolStripSeparator5 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripSeparator4 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripLabel2 = new System.Windows.Forms.ToolStripLabel();
            this.toolStripLabel1 = new System.Windows.Forms.ToolStripLabel();
            this.toolStripStatusLabel1 = new System.Windows.Forms.ToolStripLabel();
            this.PDF_VER = new System.Windows.Forms.CheckBox();
            this.ribbonControl1 = new DevComponents.DotNetBar.RibbonControl();
            this.ribbonPanel1 = new DevComponents.DotNetBar.RibbonPanel();
            this.ribbonBar5 = new DevComponents.DotNetBar.RibbonBar();
            this.itemContainer3 = new DevComponents.DotNetBar.ItemContainer();
            this.itemContainer9 = new DevComponents.DotNetBar.ItemContainer();
            this.itemContainer4 = new DevComponents.DotNetBar.ItemContainer();
            this.itemContainer10 = new DevComponents.DotNetBar.ItemContainer();
            this.itemContainer11 = new DevComponents.DotNetBar.ItemContainer();
            this.itemContainer13 = new DevComponents.DotNetBar.ItemContainer();
            this.itemContainer12 = new DevComponents.DotNetBar.ItemContainer();
            this.ribbonBar1 = new DevComponents.DotNetBar.RibbonBar();
            this.label3 = new System.Windows.Forms.Label();
            this.INICIO = new System.Windows.Forms.DateTimePicker();
            this.label4 = new System.Windows.Forms.Label();
            this.FINAL = new System.Windows.Forms.DateTimePicker();
            this.label6 = new System.Windows.Forms.Label();
            this.INVOICE_ID = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.SERIE = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.TOPtxt = new System.Windows.Forms.TextBox();
            this.CFDI_PRUEBA = new System.Windows.Forms.CheckBox();
            this.PENDIENTES = new System.Windows.Forms.CheckBox();
            this.label1 = new System.Windows.Forms.Label();
            this.ESTADO = new System.Windows.Forms.ComboBox();
            this.label7 = new System.Windows.Forms.Label();
            this.FORMATO = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.CUSTOMER_ID = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.CUSTOMER_NAME = new System.Windows.Forms.TextBox();
            this.MOSTRAR_PDF_ESTADO = new System.Windows.Forms.CheckBox();
            this.OCULTAR = new System.Windows.Forms.CheckBox();
            this.itemContainer8 = new DevComponents.DotNetBar.ItemContainer();
            this.itemContainer5 = new DevComponents.DotNetBar.ItemContainer();
            this.itemContainer6 = new DevComponents.DotNetBar.ItemContainer();
            this.controlContainerItem16 = new DevComponents.DotNetBar.ControlContainerItem();
            this.controlContainerItem17 = new DevComponents.DotNetBar.ControlContainerItem();
            this.controlContainerItem18 = new DevComponents.DotNetBar.ControlContainerItem();
            this.controlContainerItem19 = new DevComponents.DotNetBar.ControlContainerItem();
            this.controlContainerItem1 = new DevComponents.DotNetBar.ControlContainerItem();
            this.itemContainer7 = new DevComponents.DotNetBar.ItemContainer();
            this.controlContainerItem20 = new DevComponents.DotNetBar.ControlContainerItem();
            this.controlContainerItem21 = new DevComponents.DotNetBar.ControlContainerItem();
            this.controlContainerItem22 = new DevComponents.DotNetBar.ControlContainerItem();
            this.controlContainerItem23 = new DevComponents.DotNetBar.ControlContainerItem();
            this.controlContainerItem25 = new DevComponents.DotNetBar.ControlContainerItem();
            this.controlContainerItem26 = new DevComponents.DotNetBar.ControlContainerItem();
            this.controlContainerItem2 = new DevComponents.DotNetBar.ControlContainerItem();
            this.itemContainer1 = new DevComponents.DotNetBar.ItemContainer();
            this.controlContainerItem3 = new DevComponents.DotNetBar.ControlContainerItem();
            this.controlContainerItem4 = new DevComponents.DotNetBar.ControlContainerItem();
            this.controlContainerItem5 = new DevComponents.DotNetBar.ControlContainerItem();
            this.controlContainerItem11 = new DevComponents.DotNetBar.ControlContainerItem();
            this.controlContainerItem13 = new DevComponents.DotNetBar.ControlContainerItem();
            this.itemContainer2 = new DevComponents.DotNetBar.ItemContainer();
            this.controlContainerItem6 = new DevComponents.DotNetBar.ControlContainerItem();
            this.controlContainerItem7 = new DevComponents.DotNetBar.ControlContainerItem();
            this.controlContainerItem8 = new DevComponents.DotNetBar.ControlContainerItem();
            this.controlContainerItem9 = new DevComponents.DotNetBar.ControlContainerItem();
            this.itemContainer14 = new DevComponents.DotNetBar.ItemContainer();
            this.controlContainerItem14 = new DevComponents.DotNetBar.ControlContainerItem();
            this.itemContainer15 = new DevComponents.DotNetBar.ItemContainer();
            this.controlContainerItem10 = new DevComponents.DotNetBar.ControlContainerItem();
            this.ribbonPanel3 = new DevComponents.DotNetBar.RibbonPanel();
            this.ribbonBar8 = new DevComponents.DotNetBar.RibbonBar();
            this.ribbonTabItem1 = new DevComponents.DotNetBar.RibbonTabItem();
            this.ribbonTabItem3 = new DevComponents.DotNetBar.RibbonTabItem();
            this.informar_procesar = new DevComponents.DotNetBar.LabelItem();
            this.ENTITY_ID = new DevComponents.DotNetBar.ComboBoxItem();
            this.comboItem3 = new DevComponents.Editors.ComboItem();
            this.comboItem4 = new DevComponents.Editors.ComboItem();
            this.EMPRESA_NOMBRE = new DevComponents.DotNetBar.LabelItem();
            this.buttonItem37 = new DevComponents.DotNetBar.ButtonItem();
            this.informacion = new DevComponents.DotNetBar.LabelItem();
            this.ribbonTabItemGroup1 = new DevComponents.DotNetBar.RibbonTabItemGroup();
            this.comboItem1 = new DevComponents.Editors.ComboItem();
            this.comboItem2 = new DevComponents.Editors.ComboItem();
            this.progressBar1 = new System.Windows.Forms.ProgressBar();
            this.controlContainerItem12 = new DevComponents.DotNetBar.ControlContainerItem();
            this.folderBrowserDialog1 = new System.Windows.Forms.FolderBrowserDialog();
            this.oTimer = new System.Windows.Forms.Timer();
            this.buttonItem7 = new DevComponents.DotNetBar.ButtonItem();
            this.buttonItem9 = new DevComponents.DotNetBar.ButtonItem();
            this.buttonItem11 = new DevComponents.DotNetBar.ButtonItem();
            this.buttonItem17 = new DevComponents.DotNetBar.ButtonItem();
            this.buttonItem20 = new DevComponents.DotNetBar.ButtonItem();
            this.buttonItem1 = new DevComponents.DotNetBar.ButtonItem();
            this.buttonItem12 = new DevComponents.DotNetBar.ButtonItem();
            this.buttonItem14 = new DevComponents.DotNetBar.ButtonItem();
            this.CANCELAR_btn = new DevComponents.DotNetBar.ButtonItem();
            this.VERIFICAR = new DevComponents.DotNetBar.ButtonItem();
            this.CAMBIAR = new DevComponents.DotNetBar.ButtonItem();
            this.PEDIMENTOS_btn = new DevComponents.DotNetBar.ButtonItem();
            this.EXPORTAR_btn = new DevComponents.DotNetBar.ButtonItem();
            this.IMPORTAR = new DevComponents.DotNetBar.ButtonItem();
            this.ADDENDA = new DevComponents.DotNetBar.ButtonItem();
            this.ADDENDA_INDIVIDUAL = new DevComponents.DotNetBar.ButtonItem();
            this.buttonItem23 = new DevComponents.DotNetBar.ButtonItem();
            this.buttonItem29 = new DevComponents.DotNetBar.ButtonItem();
            this.btnProformas = new DevComponents.DotNetBar.ButtonItem();
            this.buttonItem2 = new DevComponents.DotNetBar.ButtonItem();
            this.buttonItem8 = new DevComponents.DotNetBar.ButtonItem();
            this.btnRetencion = new DevComponents.DotNetBar.ButtonItem();
            this.buttonItem4 = new DevComponents.DotNetBar.ButtonItem();
            this.buttonItem5 = new DevComponents.DotNetBar.ButtonItem();
            this.buttonItem6 = new DevComponents.DotNetBar.ButtonItem();
            this.buttonItem3 = new DevComponents.DotNetBar.ButtonItem();
            this.buttonItem10 = new DevComponents.DotNetBar.ButtonItem();
            this.buttonItem16 = new DevComponents.DotNetBar.ButtonItem();
            this.buttonItem13 = new DevComponents.DotNetBar.ButtonItem();
            this.buttonItem22 = new DevComponents.DotNetBar.ButtonItem();
            this.buttonItem15 = new DevComponents.DotNetBar.ButtonItem();
            this.buttonItem18 = new DevComponents.DotNetBar.ButtonItem();
            this.buttonItem19 = new DevComponents.DotNetBar.ButtonItem();
            this.buttonFile = new DevComponents.DotNetBar.Office2007StartButton();
            this.menuFileContainer = new DevComponents.DotNetBar.ItemContainer();
            this.menuFileBottomContainer = new DevComponents.DotNetBar.ItemContainer();
            this.buttonExit = new DevComponents.DotNetBar.ButtonItem();
            this.toolStripButton7 = new System.Windows.Forms.ToolStripButton();
            this.toolStripButton9 = new System.Windows.Forms.ToolStripButton();
            this.toolStripButton4 = new System.Windows.Forms.ToolStripButton();
            this.btnRefresh = new System.Windows.Forms.ToolStripButton();
            this.toolStripButton3 = new System.Windows.Forms.ToolStripButton();
            ((System.ComponentModel.ISupportInitialize)(this.dtgrdGeneral)).BeginInit();
            this.ribbonControl1.SuspendLayout();
            this.ribbonPanel1.SuspendLayout();
            this.ribbonBar1.SuspendLayout();
            this.ribbonPanel3.SuspendLayout();
            this.SuspendLayout();
            // 
            // dtgrdGeneral
            // 
            this.dtgrdGeneral.AllowUserToAddRows = false;
            this.dtgrdGeneral.AllowUserToDeleteRows = false;
            this.dtgrdGeneral.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dtgrdGeneral.BackgroundColor = System.Drawing.Color.White;
            this.dtgrdGeneral.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dtgrdGeneral.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.dtgrdGeneral.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dtgrdGeneral.DefaultCellStyle = dataGridViewCellStyle2;
            this.dtgrdGeneral.Location = new System.Drawing.Point(0, 197);
            this.dtgrdGeneral.Name = "dtgrdGeneral";
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dtgrdGeneral.RowHeadersDefaultCellStyle = dataGridViewCellStyle3;
            this.dtgrdGeneral.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dtgrdGeneral.Size = new System.Drawing.Size(983, 303);
            this.dtgrdGeneral.TabIndex = 436;
            // 
            // toolStripSeparator5
            // 
            this.toolStripSeparator5.Name = "toolStripSeparator5";
            this.toolStripSeparator5.Size = new System.Drawing.Size(6, 39);
            // 
            // toolStripSeparator4
            // 
            this.toolStripSeparator4.Name = "toolStripSeparator4";
            this.toolStripSeparator4.Size = new System.Drawing.Size(6, 39);
            // 
            // toolStripLabel2
            // 
            this.toolStripLabel2.Name = "toolStripLabel2";
            this.toolStripLabel2.Size = new System.Drawing.Size(46, 36);
            this.toolStripLabel2.Text = "Factura";
            // 
            // toolStripLabel1
            // 
            this.toolStripLabel1.Name = "toolStripLabel1";
            this.toolStripLabel1.Size = new System.Drawing.Size(148, 36);
            this.toolStripLabel1.Text = "Total de Registros a Cargar";
            // 
            // toolStripStatusLabel1
            // 
            this.toolStripStatusLabel1.Font = new System.Drawing.Font("Arial", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.toolStripStatusLabel1.Name = "toolStripStatusLabel1";
            this.toolStripStatusLabel1.Size = new System.Drawing.Size(0, 36);
            // 
            // PDF_VER
            // 
            this.PDF_VER.AutoSize = true;
            this.PDF_VER.Location = new System.Drawing.Point(331, 4);
            this.PDF_VER.Name = "PDF_VER";
            this.PDF_VER.Size = new System.Drawing.Size(94, 17);
            this.PDF_VER.TabIndex = 448;
            this.PDF_VER.Text = "Visualizar PDF";
            this.PDF_VER.UseVisualStyleBackColor = true;
            this.PDF_VER.CheckedChanged += new System.EventHandler(this.PDF_VER_CheckedChanged);
            // 
            // ribbonControl1
            // 
            this.ribbonControl1.BackColor = System.Drawing.SystemColors.Control;
            // 
            // 
            // 
            this.ribbonControl1.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.ribbonControl1.CaptionVisible = true;
            this.ribbonControl1.Controls.Add(this.ribbonPanel1);
            this.ribbonControl1.Controls.Add(this.ribbonPanel3);
            this.ribbonControl1.Dock = System.Windows.Forms.DockStyle.Top;
            this.ribbonControl1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ribbonControl1.Items.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.buttonFile,
            this.ribbonTabItem1,
            this.ribbonTabItem3,
            this.informar_procesar});
            this.ribbonControl1.KeyTipsFont = new System.Drawing.Font("Tahoma", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ribbonControl1.Location = new System.Drawing.Point(0, 0);
            this.ribbonControl1.MdiSystemItemVisible = false;
            this.ribbonControl1.Name = "ribbonControl1";
            this.ribbonControl1.Padding = new System.Windows.Forms.Padding(0, 0, 0, 2);
            this.ribbonControl1.QuickToolbarItems.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.ENTITY_ID,
            this.EMPRESA_NOMBRE,
            this.buttonItem37,
            this.informacion});
            this.ribbonControl1.Size = new System.Drawing.Size(983, 196);
            this.ribbonControl1.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.ribbonControl1.SystemText.MaximizeRibbonText = "&Maximize the Ribbon";
            this.ribbonControl1.SystemText.MinimizeRibbonText = "Mi&nimize the Ribbon";
            this.ribbonControl1.SystemText.QatAddItemText = "&Add to Quick Access Toolbar";
            this.ribbonControl1.SystemText.QatCustomizeMenuLabel = "<b>Customize Quick Access Toolbar</b>";
            this.ribbonControl1.SystemText.QatCustomizeText = "&Customize Quick Access Toolbar...";
            this.ribbonControl1.SystemText.QatDialogAddButton = "&Add >>";
            this.ribbonControl1.SystemText.QatDialogCancelButton = "Cancel";
            this.ribbonControl1.SystemText.QatDialogCaption = "Customize Quick Access Toolbar";
            this.ribbonControl1.SystemText.QatDialogCategoriesLabel = "&Choose commands from:";
            this.ribbonControl1.SystemText.QatDialogOkButton = "OK";
            this.ribbonControl1.SystemText.QatDialogPlacementCheckbox = "&Place Quick Access Toolbar below the Ribbon";
            this.ribbonControl1.SystemText.QatDialogRemoveButton = "&Remove";
            this.ribbonControl1.SystemText.QatPlaceAboveRibbonText = "&Place Quick Access Toolbar above the Ribbon";
            this.ribbonControl1.SystemText.QatPlaceBelowRibbonText = "&Place Quick Access Toolbar below the Ribbon";
            this.ribbonControl1.SystemText.QatRemoveItemText = "&Remove from Quick Access Toolbar";
            this.ribbonControl1.TabGroupHeight = 14;
            this.ribbonControl1.TabGroups.AddRange(new DevComponents.DotNetBar.RibbonTabItemGroup[] {
            this.ribbonTabItemGroup1});
            this.ribbonControl1.TabGroupsVisible = true;
            this.ribbonControl1.TabIndex = 473;
            // 
            // ribbonPanel1
            // 
            this.ribbonPanel1.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.ribbonPanel1.Controls.Add(this.ribbonBar5);
            this.ribbonPanel1.Controls.Add(this.ribbonBar1);
            this.ribbonPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ribbonPanel1.Location = new System.Drawing.Point(0, 55);
            this.ribbonPanel1.Name = "ribbonPanel1";
            this.ribbonPanel1.Padding = new System.Windows.Forms.Padding(3, 0, 3, 3);
            this.ribbonPanel1.Size = new System.Drawing.Size(983, 139);
            // 
            // 
            // 
            this.ribbonPanel1.Style.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.ribbonPanel1.StyleMouseDown.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.ribbonPanel1.StyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.ribbonPanel1.TabIndex = 1;
            // 
            // ribbonBar5
            // 
            this.ribbonBar5.AutoOverflowEnabled = true;
            // 
            // 
            // 
            this.ribbonBar5.BackgroundMouseOverStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.ribbonBar5.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.ribbonBar5.ContainerControlProcessDialogKey = true;
            this.ribbonBar5.DialogLauncherVisible = true;
            this.ribbonBar5.Dock = System.Windows.Forms.DockStyle.Left;
            this.ribbonBar5.Items.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.itemContainer3,
            this.itemContainer9,
            this.itemContainer4,
            this.itemContainer10,
            this.itemContainer11,
            this.itemContainer13,
            this.itemContainer12,
            this.btnProformas});
            this.ribbonBar5.Location = new System.Drawing.Point(458, 0);
            this.ribbonBar5.Name = "ribbonBar5";
            this.ribbonBar5.Size = new System.Drawing.Size(501, 136);
            this.ribbonBar5.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.ribbonBar5.TabIndex = 5;
            this.ribbonBar5.Text = "Opciones";
            // 
            // 
            // 
            this.ribbonBar5.TitleStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.ribbonBar5.TitleStyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // itemContainer3
            // 
            // 
            // 
            // 
            this.itemContainer3.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.itemContainer3.LayoutOrientation = DevComponents.DotNetBar.eOrientation.Vertical;
            this.itemContainer3.Name = "itemContainer3";
            this.itemContainer3.SubItems.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.buttonItem7,
            this.buttonItem9,
            this.buttonItem11,
            this.buttonItem17,
            this.buttonItem1});
            // 
            // 
            // 
            this.itemContainer3.TitleStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // itemContainer9
            // 
            // 
            // 
            // 
            this.itemContainer9.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.itemContainer9.LayoutOrientation = DevComponents.DotNetBar.eOrientation.Vertical;
            this.itemContainer9.Name = "itemContainer9";
            this.itemContainer9.SubItems.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.buttonItem12,
            this.buttonItem14});
            // 
            // 
            // 
            this.itemContainer9.TitleStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // itemContainer4
            // 
            // 
            // 
            // 
            this.itemContainer4.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.itemContainer4.LayoutOrientation = DevComponents.DotNetBar.eOrientation.Vertical;
            this.itemContainer4.Name = "itemContainer4";
            this.itemContainer4.SubItems.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.CANCELAR_btn,
            this.VERIFICAR});
            // 
            // 
            // 
            this.itemContainer4.TitleStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // itemContainer10
            // 
            // 
            // 
            // 
            this.itemContainer10.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.itemContainer10.LayoutOrientation = DevComponents.DotNetBar.eOrientation.Vertical;
            this.itemContainer10.Name = "itemContainer10";
            this.itemContainer10.SubItems.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.CAMBIAR,
            this.PEDIMENTOS_btn});
            // 
            // 
            // 
            this.itemContainer10.TitleStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // itemContainer11
            // 
            // 
            // 
            // 
            this.itemContainer11.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.itemContainer11.LayoutOrientation = DevComponents.DotNetBar.eOrientation.Vertical;
            this.itemContainer11.Name = "itemContainer11";
            this.itemContainer11.SubItems.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.EXPORTAR_btn,
            this.IMPORTAR});
            // 
            // 
            // 
            this.itemContainer11.TitleStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // itemContainer13
            // 
            // 
            // 
            // 
            this.itemContainer13.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.itemContainer13.LayoutOrientation = DevComponents.DotNetBar.eOrientation.Vertical;
            this.itemContainer13.Name = "itemContainer13";
            this.itemContainer13.SubItems.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.ADDENDA,
            this.ADDENDA_INDIVIDUAL});
            // 
            // 
            // 
            this.itemContainer13.TitleStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // itemContainer12
            // 
            // 
            // 
            // 
            this.itemContainer12.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.itemContainer12.LayoutOrientation = DevComponents.DotNetBar.eOrientation.Vertical;
            this.itemContainer12.Name = "itemContainer12";
            this.itemContainer12.SubItems.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.buttonItem23,
            this.buttonItem29});
            // 
            // 
            // 
            this.itemContainer12.TitleStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // ribbonBar1
            // 
            this.ribbonBar1.AutoOverflowEnabled = false;
            // 
            // 
            // 
            this.ribbonBar1.BackgroundMouseOverStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.ribbonBar1.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.ribbonBar1.ContainerControlProcessDialogKey = true;
            this.ribbonBar1.Controls.Add(this.label3);
            this.ribbonBar1.Controls.Add(this.INICIO);
            this.ribbonBar1.Controls.Add(this.label4);
            this.ribbonBar1.Controls.Add(this.FINAL);
            this.ribbonBar1.Controls.Add(this.PDF_VER);
            this.ribbonBar1.Controls.Add(this.label6);
            this.ribbonBar1.Controls.Add(this.INVOICE_ID);
            this.ribbonBar1.Controls.Add(this.label8);
            this.ribbonBar1.Controls.Add(this.SERIE);
            this.ribbonBar1.Controls.Add(this.label9);
            this.ribbonBar1.Controls.Add(this.TOPtxt);
            this.ribbonBar1.Controls.Add(this.CFDI_PRUEBA);
            this.ribbonBar1.Controls.Add(this.PENDIENTES);
            this.ribbonBar1.Controls.Add(this.label1);
            this.ribbonBar1.Controls.Add(this.ESTADO);
            this.ribbonBar1.Controls.Add(this.label7);
            this.ribbonBar1.Controls.Add(this.FORMATO);
            this.ribbonBar1.Controls.Add(this.label2);
            this.ribbonBar1.Controls.Add(this.CUSTOMER_ID);
            this.ribbonBar1.Controls.Add(this.label5);
            this.ribbonBar1.Controls.Add(this.CUSTOMER_NAME);
            this.ribbonBar1.Controls.Add(this.MOSTRAR_PDF_ESTADO);
            this.ribbonBar1.Controls.Add(this.OCULTAR);
            this.ribbonBar1.DialogLauncherVisible = true;
            this.ribbonBar1.Dock = System.Windows.Forms.DockStyle.Left;
            this.ribbonBar1.Items.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.itemContainer8,
            this.itemContainer5});
            this.ribbonBar1.Location = new System.Drawing.Point(3, 0);
            this.ribbonBar1.Name = "ribbonBar1";
            this.ribbonBar1.Size = new System.Drawing.Size(455, 136);
            this.ribbonBar1.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.ribbonBar1.TabIndex = 4;
            this.ribbonBar1.Text = "Busqueda";
            // 
            // 
            // 
            this.ribbonBar1.TitleStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.ribbonBar1.TitleStyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(62, 4);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(32, 13);
            this.label3.TabIndex = 440;
            this.label3.Text = "Inicio";
            // 
            // INICIO
            // 
            this.INICIO.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.INICIO.Location = new System.Drawing.Point(99, 4);
            this.INICIO.Name = "INICIO";
            this.INICIO.Size = new System.Drawing.Size(94, 20);
            this.INICIO.TabIndex = 442;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(198, 4);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(29, 13);
            this.label4.TabIndex = 441;
            this.label4.Text = "Final";
            // 
            // FINAL
            // 
            this.FINAL.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.FINAL.Location = new System.Drawing.Point(232, 4);
            this.FINAL.Name = "FINAL";
            this.FINAL.Size = new System.Drawing.Size(94, 20);
            this.FINAL.TabIndex = 443;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(62, 27);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(43, 13);
            this.label6.TabIndex = 473;
            this.label6.Text = "Factura";
            // 
            // INVOICE_ID
            // 
            this.INVOICE_ID.Location = new System.Drawing.Point(110, 27);
            this.INVOICE_ID.Name = "INVOICE_ID";
            this.INVOICE_ID.Size = new System.Drawing.Size(83, 20);
            this.INVOICE_ID.TabIndex = 473;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(198, 27);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(31, 13);
            this.label8.TabIndex = 473;
            this.label8.Text = "Serie";
            // 
            // SERIE
            // 
            this.SERIE.Location = new System.Drawing.Point(234, 27);
            this.SERIE.Name = "SERIE";
            this.SERIE.Size = new System.Drawing.Size(41, 20);
            this.SERIE.TabIndex = 473;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(280, 27);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(14, 13);
            this.label9.TabIndex = 475;
            this.label9.Text = "#";
            // 
            // TOPtxt
            // 
            this.TOPtxt.Location = new System.Drawing.Point(299, 27);
            this.TOPtxt.Name = "TOPtxt";
            this.TOPtxt.Size = new System.Drawing.Size(29, 20);
            this.TOPtxt.TabIndex = 474;
            this.TOPtxt.Text = "25";
            // 
            // CFDI_PRUEBA
            // 
            this.CFDI_PRUEBA.AutoSize = true;
            this.CFDI_PRUEBA.Checked = true;
            this.CFDI_PRUEBA.CheckState = System.Windows.Forms.CheckState.Checked;
            this.CFDI_PRUEBA.Location = new System.Drawing.Point(333, 27);
            this.CFDI_PRUEBA.Name = "CFDI_PRUEBA";
            this.CFDI_PRUEBA.Size = new System.Drawing.Size(74, 17);
            this.CFDI_PRUEBA.TabIndex = 474;
            this.CFDI_PRUEBA.Text = "CFDI Test";
            this.CFDI_PRUEBA.UseVisualStyleBackColor = true;
            // 
            // PENDIENTES
            // 
            this.PENDIENTES.AutoSize = true;
            this.PENDIENTES.Checked = true;
            this.PENDIENTES.CheckState = System.Windows.Forms.CheckState.Checked;
            this.PENDIENTES.Location = new System.Drawing.Point(62, 50);
            this.PENDIENTES.Name = "PENDIENTES";
            this.PENDIENTES.Size = new System.Drawing.Size(79, 17);
            this.PENDIENTES.TabIndex = 474;
            this.PENDIENTES.Text = "Pendientes";
            this.PENDIENTES.UseVisualStyleBackColor = true;
            this.PENDIENTES.Visible = false;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(146, 50);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(39, 13);
            this.label1.TabIndex = 474;
            this.label1.Text = "Timbre";
            // 
            // ESTADO
            // 
            this.ESTADO.FormattingEnabled = true;
            this.ESTADO.Location = new System.Drawing.Point(190, 50);
            this.ESTADO.Name = "ESTADO";
            this.ESTADO.Size = new System.Drawing.Size(75, 21);
            this.ESTADO.TabIndex = 475;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(270, 50);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(45, 13);
            this.label7.TabIndex = 479;
            this.label7.Text = "Formato";
            // 
            // FORMATO
            // 
            this.FORMATO.FormattingEnabled = true;
            this.FORMATO.Location = new System.Drawing.Point(320, 50);
            this.FORMATO.Name = "FORMATO";
            this.FORMATO.Size = new System.Drawing.Size(121, 21);
            this.FORMATO.TabIndex = 478;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(62, 74);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(39, 13);
            this.label2.TabIndex = 474;
            this.label2.Text = "Cliente";
            // 
            // CUSTOMER_ID
            // 
            this.CUSTOMER_ID.Location = new System.Drawing.Point(106, 74);
            this.CUSTOMER_ID.Name = "CUSTOMER_ID";
            this.CUSTOMER_ID.Size = new System.Drawing.Size(87, 20);
            this.CUSTOMER_ID.TabIndex = 475;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(198, 74);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(44, 13);
            this.label5.TabIndex = 474;
            this.label5.Text = "Nombre";
            // 
            // CUSTOMER_NAME
            // 
            this.CUSTOMER_NAME.Location = new System.Drawing.Point(247, 74);
            this.CUSTOMER_NAME.Name = "CUSTOMER_NAME";
            this.CUSTOMER_NAME.Size = new System.Drawing.Size(115, 20);
            this.CUSTOMER_NAME.TabIndex = 474;
            // 
            // MOSTRAR_PDF_ESTADO
            // 
            this.MOSTRAR_PDF_ESTADO.AutoSize = true;
            this.MOSTRAR_PDF_ESTADO.Location = new System.Drawing.Point(62, 97);
            this.MOSTRAR_PDF_ESTADO.Name = "MOSTRAR_PDF_ESTADO";
            this.MOSTRAR_PDF_ESTADO.Size = new System.Drawing.Size(147, 17);
            this.MOSTRAR_PDF_ESTADO.TabIndex = 478;
            this.MOSTRAR_PDF_ESTADO.Text = "Mostrar los PDF con Error";
            this.MOSTRAR_PDF_ESTADO.UseVisualStyleBackColor = true;
            this.MOSTRAR_PDF_ESTADO.Visible = false;
            // 
            // OCULTAR
            // 
            this.OCULTAR.AutoSize = true;
            this.OCULTAR.Checked = true;
            this.OCULTAR.CheckState = System.Windows.Forms.CheckState.Checked;
            this.OCULTAR.Location = new System.Drawing.Point(214, 97);
            this.OCULTAR.Name = "OCULTAR";
            this.OCULTAR.Size = new System.Drawing.Size(232, 17);
            this.OCULTAR.TabIndex = 476;
            this.OCULTAR.Tag = " Canceladas en Visual no Timbradas";
            this.OCULTAR.Text = "Ocultar Canceladas en Visual no Timbradas";
            this.OCULTAR.UseVisualStyleBackColor = true;
            // 
            // itemContainer8
            // 
            // 
            // 
            // 
            this.itemContainer8.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.itemContainer8.LayoutOrientation = DevComponents.DotNetBar.eOrientation.Vertical;
            this.itemContainer8.Name = "itemContainer8";
            this.itemContainer8.SubItems.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.buttonItem2,
            this.buttonItem8});
            // 
            // 
            // 
            this.itemContainer8.TitleStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // itemContainer5
            // 
            // 
            // 
            // 
            this.itemContainer5.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.itemContainer5.LayoutOrientation = DevComponents.DotNetBar.eOrientation.Vertical;
            this.itemContainer5.Name = "itemContainer5";
            this.itemContainer5.SubItems.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.itemContainer6,
            this.itemContainer7,
            this.itemContainer1,
            this.itemContainer2,
            this.itemContainer14});
            // 
            // 
            // 
            this.itemContainer5.TitleStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // itemContainer6
            // 
            // 
            // 
            // 
            this.itemContainer6.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.itemContainer6.Name = "itemContainer6";
            this.itemContainer6.SubItems.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.controlContainerItem16,
            this.controlContainerItem17,
            this.controlContainerItem18,
            this.controlContainerItem19,
            this.controlContainerItem1});
            // 
            // 
            // 
            this.itemContainer6.TitleStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // controlContainerItem16
            // 
            this.controlContainerItem16.AllowItemResize = false;
            this.controlContainerItem16.Control = this.label3;
            this.controlContainerItem16.MenuVisibility = DevComponents.DotNetBar.eMenuVisibility.VisibleAlways;
            this.controlContainerItem16.Name = "controlContainerItem16";
            // 
            // controlContainerItem17
            // 
            this.controlContainerItem17.AllowItemResize = false;
            this.controlContainerItem17.Control = this.INICIO;
            this.controlContainerItem17.MenuVisibility = DevComponents.DotNetBar.eMenuVisibility.VisibleAlways;
            this.controlContainerItem17.Name = "controlContainerItem17";
            // 
            // controlContainerItem18
            // 
            this.controlContainerItem18.AllowItemResize = false;
            this.controlContainerItem18.Control = this.label4;
            this.controlContainerItem18.MenuVisibility = DevComponents.DotNetBar.eMenuVisibility.VisibleAlways;
            this.controlContainerItem18.Name = "controlContainerItem18";
            // 
            // controlContainerItem19
            // 
            this.controlContainerItem19.AllowItemResize = false;
            this.controlContainerItem19.Control = this.FINAL;
            this.controlContainerItem19.MenuVisibility = DevComponents.DotNetBar.eMenuVisibility.VisibleAlways;
            this.controlContainerItem19.Name = "controlContainerItem19";
            // 
            // controlContainerItem1
            // 
            this.controlContainerItem1.AllowItemResize = false;
            this.controlContainerItem1.Control = this.PDF_VER;
            this.controlContainerItem1.MenuVisibility = DevComponents.DotNetBar.eMenuVisibility.VisibleAlways;
            this.controlContainerItem1.Name = "controlContainerItem1";
            // 
            // itemContainer7
            // 
            // 
            // 
            // 
            this.itemContainer7.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.itemContainer7.Name = "itemContainer7";
            this.itemContainer7.SubItems.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.controlContainerItem20,
            this.controlContainerItem21,
            this.controlContainerItem22,
            this.controlContainerItem23,
            this.controlContainerItem25,
            this.controlContainerItem26,
            this.controlContainerItem2});
            // 
            // 
            // 
            this.itemContainer7.TitleStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // controlContainerItem20
            // 
            this.controlContainerItem20.AllowItemResize = false;
            this.controlContainerItem20.Control = this.label6;
            this.controlContainerItem20.MenuVisibility = DevComponents.DotNetBar.eMenuVisibility.VisibleAlways;
            this.controlContainerItem20.Name = "controlContainerItem20";
            // 
            // controlContainerItem21
            // 
            this.controlContainerItem21.AllowItemResize = false;
            this.controlContainerItem21.Control = this.INVOICE_ID;
            this.controlContainerItem21.MenuVisibility = DevComponents.DotNetBar.eMenuVisibility.VisibleAlways;
            this.controlContainerItem21.Name = "controlContainerItem21";
            // 
            // controlContainerItem22
            // 
            this.controlContainerItem22.AllowItemResize = false;
            this.controlContainerItem22.Control = this.label8;
            this.controlContainerItem22.MenuVisibility = DevComponents.DotNetBar.eMenuVisibility.VisibleAlways;
            this.controlContainerItem22.Name = "controlContainerItem22";
            // 
            // controlContainerItem23
            // 
            this.controlContainerItem23.AllowItemResize = false;
            this.controlContainerItem23.Control = this.SERIE;
            this.controlContainerItem23.MenuVisibility = DevComponents.DotNetBar.eMenuVisibility.VisibleAlways;
            this.controlContainerItem23.Name = "controlContainerItem23";
            // 
            // controlContainerItem25
            // 
            this.controlContainerItem25.AllowItemResize = false;
            this.controlContainerItem25.Control = this.label9;
            this.controlContainerItem25.MenuVisibility = DevComponents.DotNetBar.eMenuVisibility.VisibleAlways;
            this.controlContainerItem25.Name = "controlContainerItem25";
            // 
            // controlContainerItem26
            // 
            this.controlContainerItem26.AllowItemResize = false;
            this.controlContainerItem26.Control = this.TOPtxt;
            this.controlContainerItem26.MenuVisibility = DevComponents.DotNetBar.eMenuVisibility.VisibleAlways;
            this.controlContainerItem26.Name = "controlContainerItem26";
            // 
            // controlContainerItem2
            // 
            this.controlContainerItem2.AllowItemResize = false;
            this.controlContainerItem2.Control = this.CFDI_PRUEBA;
            this.controlContainerItem2.MenuVisibility = DevComponents.DotNetBar.eMenuVisibility.VisibleAlways;
            this.controlContainerItem2.Name = "controlContainerItem2";
            // 
            // itemContainer1
            // 
            // 
            // 
            // 
            this.itemContainer1.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.itemContainer1.Name = "itemContainer1";
            this.itemContainer1.SubItems.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.controlContainerItem3,
            this.controlContainerItem4,
            this.controlContainerItem5,
            this.controlContainerItem11,
            this.controlContainerItem13});
            // 
            // 
            // 
            this.itemContainer1.TitleStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // controlContainerItem3
            // 
            this.controlContainerItem3.AllowItemResize = false;
            this.controlContainerItem3.Control = this.PENDIENTES;
            this.controlContainerItem3.MenuVisibility = DevComponents.DotNetBar.eMenuVisibility.VisibleAlways;
            this.controlContainerItem3.Name = "controlContainerItem3";
            // 
            // controlContainerItem4
            // 
            this.controlContainerItem4.AllowItemResize = false;
            this.controlContainerItem4.Control = this.label1;
            this.controlContainerItem4.MenuVisibility = DevComponents.DotNetBar.eMenuVisibility.VisibleAlways;
            this.controlContainerItem4.Name = "controlContainerItem4";
            // 
            // controlContainerItem5
            // 
            this.controlContainerItem5.AllowItemResize = false;
            this.controlContainerItem5.Control = this.ESTADO;
            this.controlContainerItem5.MenuVisibility = DevComponents.DotNetBar.eMenuVisibility.VisibleAlways;
            this.controlContainerItem5.Name = "controlContainerItem5";
            // 
            // controlContainerItem11
            // 
            this.controlContainerItem11.AllowItemResize = false;
            this.controlContainerItem11.Control = this.label7;
            this.controlContainerItem11.MenuVisibility = DevComponents.DotNetBar.eMenuVisibility.VisibleAlways;
            this.controlContainerItem11.Name = "controlContainerItem11";
            // 
            // controlContainerItem13
            // 
            this.controlContainerItem13.AllowItemResize = false;
            this.controlContainerItem13.Control = this.FORMATO;
            this.controlContainerItem13.MenuVisibility = DevComponents.DotNetBar.eMenuVisibility.VisibleAlways;
            this.controlContainerItem13.Name = "controlContainerItem13";
            // 
            // itemContainer2
            // 
            // 
            // 
            // 
            this.itemContainer2.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.itemContainer2.Name = "itemContainer2";
            this.itemContainer2.SubItems.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.controlContainerItem6,
            this.controlContainerItem7,
            this.controlContainerItem8,
            this.controlContainerItem9});
            // 
            // 
            // 
            this.itemContainer2.TitleStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // controlContainerItem6
            // 
            this.controlContainerItem6.AllowItemResize = false;
            this.controlContainerItem6.Control = this.label2;
            this.controlContainerItem6.MenuVisibility = DevComponents.DotNetBar.eMenuVisibility.VisibleAlways;
            this.controlContainerItem6.Name = "controlContainerItem6";
            // 
            // controlContainerItem7
            // 
            this.controlContainerItem7.AllowItemResize = false;
            this.controlContainerItem7.Control = this.CUSTOMER_ID;
            this.controlContainerItem7.MenuVisibility = DevComponents.DotNetBar.eMenuVisibility.VisibleAlways;
            this.controlContainerItem7.Name = "controlContainerItem7";
            // 
            // controlContainerItem8
            // 
            this.controlContainerItem8.AllowItemResize = false;
            this.controlContainerItem8.Control = this.label5;
            this.controlContainerItem8.MenuVisibility = DevComponents.DotNetBar.eMenuVisibility.VisibleAlways;
            this.controlContainerItem8.Name = "controlContainerItem8";
            // 
            // controlContainerItem9
            // 
            this.controlContainerItem9.AllowItemResize = false;
            this.controlContainerItem9.Control = this.CUSTOMER_NAME;
            this.controlContainerItem9.MenuVisibility = DevComponents.DotNetBar.eMenuVisibility.VisibleAlways;
            this.controlContainerItem9.Name = "controlContainerItem9";
            // 
            // itemContainer14
            // 
            // 
            // 
            // 
            this.itemContainer14.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.itemContainer14.Name = "itemContainer14";
            this.itemContainer14.SubItems.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.controlContainerItem14,
            this.itemContainer15});
            // 
            // 
            // 
            this.itemContainer14.TitleStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // controlContainerItem14
            // 
            this.controlContainerItem14.AllowItemResize = false;
            this.controlContainerItem14.Control = this.MOSTRAR_PDF_ESTADO;
            this.controlContainerItem14.MenuVisibility = DevComponents.DotNetBar.eMenuVisibility.VisibleAlways;
            this.controlContainerItem14.Name = "controlContainerItem14";
            // 
            // itemContainer15
            // 
            // 
            // 
            // 
            this.itemContainer15.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.itemContainer15.Name = "itemContainer15";
            this.itemContainer15.SubItems.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.controlContainerItem10});
            // 
            // 
            // 
            this.itemContainer15.TitleStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // controlContainerItem10
            // 
            this.controlContainerItem10.AllowItemResize = false;
            this.controlContainerItem10.Control = this.OCULTAR;
            this.controlContainerItem10.MenuVisibility = DevComponents.DotNetBar.eMenuVisibility.VisibleAlways;
            this.controlContainerItem10.Name = "controlContainerItem10";
            // 
            // ribbonPanel3
            // 
            this.ribbonPanel3.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.ribbonPanel3.Controls.Add(this.ribbonBar8);
            this.ribbonPanel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ribbonPanel3.Location = new System.Drawing.Point(0, 55);
            this.ribbonPanel3.Name = "ribbonPanel3";
            this.ribbonPanel3.Padding = new System.Windows.Forms.Padding(3, 0, 3, 3);
            this.ribbonPanel3.Size = new System.Drawing.Size(983, 139);
            // 
            // 
            // 
            this.ribbonPanel3.Style.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.ribbonPanel3.StyleMouseDown.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.ribbonPanel3.StyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.ribbonPanel3.TabIndex = 3;
            this.ribbonPanel3.Visible = false;
            // 
            // ribbonBar8
            // 
            this.ribbonBar8.AutoOverflowEnabled = true;
            // 
            // 
            // 
            this.ribbonBar8.BackgroundMouseOverStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.ribbonBar8.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.ribbonBar8.ContainerControlProcessDialogKey = true;
            this.ribbonBar8.Dock = System.Windows.Forms.DockStyle.Left;
            this.ribbonBar8.Items.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.btnRetencion,
            this.buttonItem4,
            this.buttonItem5,
            this.buttonItem6,
            this.buttonItem3,
            this.buttonItem10,
            this.buttonItem16,
            this.buttonItem13,
            this.buttonItem22,
            this.buttonItem15,
            this.buttonItem18,
            this.buttonItem19});
            this.ribbonBar8.ItemSpacing = 4;
            this.ribbonBar8.Location = new System.Drawing.Point(3, 0);
            this.ribbonBar8.Name = "ribbonBar8";
            this.ribbonBar8.Size = new System.Drawing.Size(880, 136);
            this.ribbonBar8.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.ribbonBar8.TabIndex = 4;
            this.ribbonBar8.Text = "Opciones de Configuración";
            // 
            // 
            // 
            this.ribbonBar8.TitleStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.ribbonBar8.TitleStyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.ribbonBar8.ItemClick += new System.EventHandler(this.ribbonBar8_ItemClick);
            // 
            // ribbonTabItem1
            // 
            this.ribbonTabItem1.Checked = true;
            this.ribbonTabItem1.Name = "ribbonTabItem1";
            this.ribbonTabItem1.Panel = this.ribbonPanel1;
            this.ribbonTabItem1.Text = "&Busqueda";
            // 
            // ribbonTabItem3
            // 
            this.ribbonTabItem3.Name = "ribbonTabItem3";
            this.ribbonTabItem3.Panel = this.ribbonPanel3;
            this.ribbonTabItem3.Text = "Configuración";
            this.ribbonTabItem3.Click += new System.EventHandler(this.ribbonTabItem3_Click);
            // 
            // informar_procesar
            // 
            this.informar_procesar.Name = "informar_procesar";
            // 
            // ENTITY_ID
            // 
            this.ENTITY_ID.ComboWidth = 60;
            this.ENTITY_ID.DropDownHeight = 106;
            this.ENTITY_ID.DropDownWidth = 60;
            this.ENTITY_ID.ItemHeight = 17;
            this.ENTITY_ID.Items.AddRange(new object[] {
            this.comboItem3,
            this.comboItem4});
            this.ENTITY_ID.Name = "ENTITY_ID";
            this.ENTITY_ID.SelectedIndexChanged += new System.EventHandler(this.ENTITY_ID_SelectedIndexChanged);
            // 
            // comboItem3
            // 
            this.comboItem3.Text = "SWISSMEX";
            // 
            // comboItem4
            // 
            this.comboItem4.Text = "WIDEMEX";
            // 
            // EMPRESA_NOMBRE
            // 
            this.EMPRESA_NOMBRE.Name = "EMPRESA_NOMBRE";
            this.EMPRESA_NOMBRE.Text = "NOMBRE DE LA EMPRESA";
            // 
            // buttonItem37
            // 
            this.buttonItem37.Name = "buttonItem37";
            this.buttonItem37.Tooltip = "Respaldo de Base de Datos";
            // 
            // informacion
            // 
            this.informacion.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.informacion.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.informacion.Name = "informacion";
            this.informacion.Width = 300;
            // 
            // ribbonTabItemGroup1
            // 
            this.ribbonTabItemGroup1.Color = DevComponents.DotNetBar.eRibbonTabGroupColor.Orange;
            this.ribbonTabItemGroup1.GroupTitle = "Tab Group";
            this.ribbonTabItemGroup1.Name = "ribbonTabItemGroup1";
            // 
            // 
            // 
            this.ribbonTabItemGroup1.Style.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(240)))), ((int)(((byte)(158)))), ((int)(((byte)(159)))));
            this.ribbonTabItemGroup1.Style.BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(249)))), ((int)(((byte)(225)))), ((int)(((byte)(226)))));
            this.ribbonTabItemGroup1.Style.BackColorGradientAngle = 90;
            this.ribbonTabItemGroup1.Style.BorderBottom = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.ribbonTabItemGroup1.Style.BorderBottomWidth = 1;
            this.ribbonTabItemGroup1.Style.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(154)))), ((int)(((byte)(58)))), ((int)(((byte)(59)))));
            this.ribbonTabItemGroup1.Style.BorderLeft = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.ribbonTabItemGroup1.Style.BorderLeftWidth = 1;
            this.ribbonTabItemGroup1.Style.BorderRight = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.ribbonTabItemGroup1.Style.BorderRightWidth = 1;
            this.ribbonTabItemGroup1.Style.BorderTop = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.ribbonTabItemGroup1.Style.BorderTopWidth = 1;
            this.ribbonTabItemGroup1.Style.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.ribbonTabItemGroup1.Style.TextAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Center;
            this.ribbonTabItemGroup1.Style.TextColor = System.Drawing.Color.Black;
            this.ribbonTabItemGroup1.Style.TextLineAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Near;
            // 
            // comboItem1
            // 
            this.comboItem1.Text = "SWISSMEX";
            // 
            // comboItem2
            // 
            this.comboItem2.Text = "WIDEMEX";
            // 
            // progressBar1
            // 
            this.progressBar1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.progressBar1.Location = new System.Drawing.Point(0, 198);
            this.progressBar1.Name = "progressBar1";
            this.progressBar1.Size = new System.Drawing.Size(983, 18);
            this.progressBar1.Style = System.Windows.Forms.ProgressBarStyle.Continuous;
            this.progressBar1.TabIndex = 477;
            // 
            // controlContainerItem12
            // 
            this.controlContainerItem12.AllowItemResize = false;
            this.controlContainerItem12.MenuVisibility = DevComponents.DotNetBar.eMenuVisibility.VisibleAlways;
            this.controlContainerItem12.Name = "controlContainerItem12";
            // 
            // oTimer
            // 
            this.oTimer.Interval = 5000;
            this.oTimer.Tick += new System.EventHandler(this.oTimer_Tick);
            // 
            // buttonItem7
            // 
            this.buttonItem7.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.buttonItem7.Image = ((System.Drawing.Image)(resources.GetObject("buttonItem7.Image")));
            this.buttonItem7.Name = "buttonItem7";
            this.buttonItem7.Text = "Enviar";
            this.buttonItem7.Click += new System.EventHandler(this.buttonItem7_Click_1);
            // 
            // buttonItem9
            // 
            this.buttonItem9.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.buttonItem9.Image = ((System.Drawing.Image)(resources.GetObject("buttonItem9.Image")));
            this.buttonItem9.Name = "buttonItem9";
            this.buttonItem9.Text = "Enviar Cancelado";
            this.buttonItem9.Click += new System.EventHandler(this.buttonItem9_Click);
            // 
            // buttonItem11
            // 
            this.buttonItem11.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.buttonItem11.Image = ((System.Drawing.Image)(resources.GetObject("buttonItem11.Image")));
            this.buttonItem11.Name = "buttonItem11";
            this.buttonItem11.Text = "Enviar Adjunto";
            this.buttonItem11.Click += new System.EventHandler(this.buttonItem11_Click);
            // 
            // buttonItem17
            // 
            this.buttonItem17.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.buttonItem17.Image = global::FE_FX.Properties.Resources.Reporte_Mensual;
            this.buttonItem17.Name = "buttonItem17";
            this.buttonItem17.SubItems.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.buttonItem20});
            this.buttonItem17.Text = "Regenerar PDF";
            this.buttonItem17.Click += new System.EventHandler(this.buttonItem17_Click);
            // 
            // buttonItem20
            // 
            this.buttonItem20.Name = "buttonItem20";
            this.buttonItem20.Text = "Regenaración Masiva";
            this.buttonItem20.Click += new System.EventHandler(this.buttonItem20_Click_1);
            // 
            // buttonItem1
            // 
            this.buttonItem1.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.buttonItem1.Image = global::FE_FX.Properties.Resources._1424223388_699356_icon_148_tag_add_16;
            this.buttonItem1.Name = "buttonItem1";
            this.buttonItem1.Text = "Complemento";
            this.buttonItem1.Click += new System.EventHandler(this.buttonItem1_Click_2);
            // 
            // buttonItem12
            // 
            this.buttonItem12.Image = ((System.Drawing.Image)(resources.GetObject("buttonItem12.Image")));
            this.buttonItem12.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top;
            this.buttonItem12.Name = "buttonItem12";
            this.buttonItem12.Text = "XML";
            this.buttonItem12.Tooltip = "Ver XML";
            this.buttonItem12.Click += new System.EventHandler(this.buttonItem12_Click);
            // 
            // buttonItem14
            // 
            this.buttonItem14.Image = ((System.Drawing.Image)(resources.GetObject("buttonItem14.Image")));
            this.buttonItem14.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top;
            this.buttonItem14.Name = "buttonItem14";
            this.buttonItem14.Text = "PDF";
            this.buttonItem14.Tooltip = "Ver PDF";
            this.buttonItem14.Click += new System.EventHandler(this.buttonItem14_Click);
            // 
            // CANCELAR_btn
            // 
            this.CANCELAR_btn.Image = ((System.Drawing.Image)(resources.GetObject("CANCELAR_btn.Image")));
            this.CANCELAR_btn.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top;
            this.CANCELAR_btn.Name = "CANCELAR_btn";
            this.CANCELAR_btn.Text = "Cancelar";
            this.CANCELAR_btn.Click += new System.EventHandler(this.buttonItem3_Click);
            // 
            // VERIFICAR
            // 
            this.VERIFICAR.Image = ((System.Drawing.Image)(resources.GetObject("VERIFICAR.Image")));
            this.VERIFICAR.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top;
            this.VERIFICAR.Name = "VERIFICAR";
            this.VERIFICAR.Text = "Verificar";
            this.VERIFICAR.Click += new System.EventHandler(this.buttonItem15_Click);
            // 
            // CAMBIAR
            // 
            this.CAMBIAR.Image = ((System.Drawing.Image)(resources.GetObject("CAMBIAR.Image")));
            this.CAMBIAR.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top;
            this.CAMBIAR.Name = "CAMBIAR";
            this.CAMBIAR.Text = "Cambiar";
            this.CAMBIAR.Tooltip = "Cambia las Cuentas Bancarias o Metodo de Deposito de la Factura";
            this.CAMBIAR.Click += new System.EventHandler(this.buttonItem1_Click);
            // 
            // PEDIMENTOS_btn
            // 
            this.PEDIMENTOS_btn.Image = ((System.Drawing.Image)(resources.GetObject("PEDIMENTOS_btn.Image")));
            this.PEDIMENTOS_btn.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top;
            this.PEDIMENTOS_btn.Name = "PEDIMENTOS_btn";
            this.PEDIMENTOS_btn.Text = "Pedimentos";
            this.PEDIMENTOS_btn.Tooltip = "Pedimentos";
            this.PEDIMENTOS_btn.Click += new System.EventHandler(this.buttonItem19_Click_1);
            // 
            // EXPORTAR_btn
            // 
            this.EXPORTAR_btn.Image = ((System.Drawing.Image)(resources.GetObject("EXPORTAR_btn.Image")));
            this.EXPORTAR_btn.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top;
            this.EXPORTAR_btn.Name = "EXPORTAR_btn";
            this.EXPORTAR_btn.Text = "Exportar";
            this.EXPORTAR_btn.Click += new System.EventHandler(this.buttonMargins_Click);
            // 
            // IMPORTAR
            // 
            this.IMPORTAR.Image = global::FE_FX.Properties.Resources.Backup;
            this.IMPORTAR.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top;
            this.IMPORTAR.Name = "IMPORTAR";
            this.IMPORTAR.Text = "Importar";
            this.IMPORTAR.Click += new System.EventHandler(this.buttonItem20_Click);
            // 
            // ADDENDA
            // 
            this.ADDENDA.Image = global::FE_FX.Properties.Resources.Certificado;
            this.ADDENDA.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top;
            this.ADDENDA.Name = "ADDENDA";
            this.ADDENDA.Text = "Addenda";
            this.ADDENDA.Click += new System.EventHandler(this.buttonItem18_Click_1);
            // 
            // ADDENDA_INDIVIDUAL
            // 
            this.ADDENDA_INDIVIDUAL.Image = global::FE_FX.Properties.Resources.Certificado;
            this.ADDENDA_INDIVIDUAL.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top;
            this.ADDENDA_INDIVIDUAL.Name = "ADDENDA_INDIVIDUAL";
            this.ADDENDA_INDIVIDUAL.Text = "Addenda Individual";
            this.ADDENDA_INDIVIDUAL.Click += new System.EventHandler(this.buttonItem21_Click);
            // 
            // buttonItem23
            // 
            this.buttonItem23.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.buttonItem23.Image = ((System.Drawing.Image)(resources.GetObject("buttonItem23.Image")));
            this.buttonItem23.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top;
            this.buttonItem23.Name = "buttonItem23";
            this.buttonItem23.Text = "Soporte";
            this.buttonItem23.Click += new System.EventHandler(this.buttonItem23_Click);
            // 
            // buttonItem29
            // 
            this.buttonItem29.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.buttonItem29.Image = ((System.Drawing.Image)(resources.GetObject("buttonItem29.Image")));
            this.buttonItem29.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top;
            this.buttonItem29.Name = "buttonItem29";
            this.buttonItem29.Text = "Manual";
            this.buttonItem29.Click += new System.EventHandler(this.buttonItem29_Click);
            // 
            // btnProformas
            // 
            this.btnProformas.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.btnProformas.Image = global::FE_FX.Properties.Resources._1470191260_Report;
            this.btnProformas.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top;
            this.btnProformas.Name = "btnProformas";
            this.btnProformas.Shortcuts.Add(DevComponents.DotNetBar.eShortcut.CtrlF);
            this.btnProformas.Text = "Proformas";
            this.btnProformas.Click += new System.EventHandler(this.btnProformas_Click);
            // 
            // buttonItem2
            // 
            this.buttonItem2.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.buttonItem2.Image = ((System.Drawing.Image)(resources.GetObject("buttonItem2.Image")));
            this.buttonItem2.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top;
            this.buttonItem2.Name = "buttonItem2";
            this.buttonItem2.Shortcuts.Add(DevComponents.DotNetBar.eShortcut.CtrlF);
            this.buttonItem2.Text = "&Procesar";
            this.buttonItem2.Click += new System.EventHandler(this.buttonItem2_Click_1);
            // 
            // buttonItem8
            // 
            this.buttonItem8.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.buttonItem8.Image = ((System.Drawing.Image)(resources.GetObject("buttonItem8.Image")));
            this.buttonItem8.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top;
            this.buttonItem8.Name = "buttonItem8";
            this.buttonItem8.Shortcuts.Add(DevComponents.DotNetBar.eShortcut.CtrlF);
            this.buttonItem8.Text = "&Actualizar";
            this.buttonItem8.Click += new System.EventHandler(this.buttonItem8_Click);
            // 
            // btnRetencion
            // 
            this.btnRetencion.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.btnRetencion.Image = global::FE_FX.Properties.Resources._1470676261_Html;
            this.btnRetencion.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top;
            this.btnRetencion.Name = "btnRetencion";
            this.btnRetencion.Shortcuts.Add(DevComponents.DotNetBar.eShortcut.CtrlF);
            this.btnRetencion.Text = "Retenciones";
            this.btnRetencion.Click += new System.EventHandler(this.btnRetencion_Click);
            // 
            // buttonItem4
            // 
            this.buttonItem4.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.buttonItem4.Image = global::FE_FX.Properties.Resources.Empresas;
            this.buttonItem4.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top;
            this.buttonItem4.Name = "buttonItem4";
            this.buttonItem4.Shortcuts.Add(DevComponents.DotNetBar.eShortcut.CtrlF);
            this.buttonItem4.Text = "Empresas";
            this.buttonItem4.Click += new System.EventHandler(this.buttonItem4_Click);
            // 
            // buttonItem5
            // 
            this.buttonItem5.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.buttonItem5.Image = global::FE_FX.Properties.Resources.Serie;
            this.buttonItem5.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top;
            this.buttonItem5.Name = "buttonItem5";
            this.buttonItem5.Shortcuts.Add(DevComponents.DotNetBar.eShortcut.CtrlF);
            this.buttonItem5.Text = "Series";
            this.buttonItem5.Click += new System.EventHandler(this.buttonItem5_Click);
            // 
            // buttonItem6
            // 
            this.buttonItem6.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.buttonItem6.Image = global::FE_FX.Properties.Resources.Certificado;
            this.buttonItem6.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top;
            this.buttonItem6.Name = "buttonItem6";
            this.buttonItem6.Shortcuts.Add(DevComponents.DotNetBar.eShortcut.CtrlF);
            this.buttonItem6.Text = "Certificados";
            this.buttonItem6.Click += new System.EventHandler(this.buttonItem6_Click);
            // 
            // buttonItem3
            // 
            this.buttonItem3.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.buttonItem3.Image = global::FE_FX.Properties.Resources.Cliente;
            this.buttonItem3.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top;
            this.buttonItem3.Name = "buttonItem3";
            this.buttonItem3.Shortcuts.Add(DevComponents.DotNetBar.eShortcut.CtrlF);
            this.buttonItem3.Text = "Asuntos del clientes";
            this.buttonItem3.Click += new System.EventHandler(this.buttonItem3_Click_1);
            // 
            // buttonItem10
            // 
            this.buttonItem10.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.buttonItem10.Image = global::FE_FX.Properties.Resources.Usuarios;
            this.buttonItem10.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top;
            this.buttonItem10.Name = "buttonItem10";
            this.buttonItem10.Shortcuts.Add(DevComponents.DotNetBar.eShortcut.CtrlF);
            this.buttonItem10.Text = "Usuarios";
            this.buttonItem10.Click += new System.EventHandler(this.buttonItem10_Click);
            // 
            // buttonItem16
            // 
            this.buttonItem16.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.buttonItem16.Image = global::FE_FX.Properties.Resources.Reporte;
            this.buttonItem16.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top;
            this.buttonItem16.Name = "buttonItem16";
            this.buttonItem16.Shortcuts.Add(DevComponents.DotNetBar.eShortcut.CtrlF);
            this.buttonItem16.Text = "Formatos";
            this.buttonItem16.Click += new System.EventHandler(this.buttonItem16_Click);
            // 
            // buttonItem13
            // 
            this.buttonItem13.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.buttonItem13.Image = ((System.Drawing.Image)(resources.GetObject("buttonItem13.Image")));
            this.buttonItem13.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top;
            this.buttonItem13.Name = "buttonItem13";
            this.buttonItem13.Text = "Manual Técnico";
            this.buttonItem13.Click += new System.EventHandler(this.buttonItem13_Click_2);
            // 
            // buttonItem22
            // 
            this.buttonItem22.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.buttonItem22.Image = ((System.Drawing.Image)(resources.GetObject("buttonItem22.Image")));
            this.buttonItem22.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top;
            this.buttonItem22.Name = "buttonItem22";
            this.buttonItem22.Text = "Mail de Soporte";
            this.buttonItem22.Click += new System.EventHandler(this.buttonItem22_Click);
            // 
            // buttonItem15
            // 
            this.buttonItem15.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.buttonItem15.Image = global::FE_FX.Properties.Resources.Usuarios;
            this.buttonItem15.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top;
            this.buttonItem15.Name = "buttonItem15";
            this.buttonItem15.Shortcuts.Add(DevComponents.DotNetBar.eShortcut.CtrlF);
            this.buttonItem15.Text = "Comercio Exterior";
            this.buttonItem15.Click += new System.EventHandler(this.buttonItem15_Click_1);
            // 
            // buttonItem18
            // 
            this.buttonItem18.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.buttonItem18.Image = global::FE_FX.Properties.Resources._1482194683_Internet_Line_02;
            this.buttonItem18.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top;
            this.buttonItem18.Name = "buttonItem18";
            this.buttonItem18.Shortcuts.Add(DevComponents.DotNetBar.eShortcut.CtrlF);
            this.buttonItem18.Text = "SAT Navegador";
            this.buttonItem18.Click += new System.EventHandler(this.buttonItem18_Click_3);
            // 
            // buttonItem19
            // 
            this.buttonItem19.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.buttonItem19.Image = global::FE_FX.Properties.Resources.if_icon_102_document_file_xml_315587;
            this.buttonItem19.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top;
            this.buttonItem19.Name = "buttonItem19";
            this.buttonItem19.Shortcuts.Add(DevComponents.DotNetBar.eShortcut.CtrlF);
            this.buttonItem19.Text = "COVE\'s 3.2";
            this.buttonItem19.Click += new System.EventHandler(this.buttonItem19_Click_2);
            // 
            // buttonFile
            // 
            this.buttonFile.AutoExpandOnClick = true;
            this.buttonFile.CanCustomize = false;
            this.buttonFile.HotTrackingStyle = DevComponents.DotNetBar.eHotTrackingStyle.Image;
            this.buttonFile.Image = global::FE_FX.Properties.Resources.favicon_96x96;
            this.buttonFile.ImageFixedSize = new System.Drawing.Size(16, 16);
            this.buttonFile.ImagePaddingHorizontal = 0;
            this.buttonFile.ImagePaddingVertical = 1;
            this.buttonFile.Name = "buttonFile";
            this.buttonFile.ShowSubItems = false;
            this.buttonFile.SubItems.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.menuFileContainer});
            this.buttonFile.Text = "F&ile";
            // 
            // menuFileContainer
            // 
            // 
            // 
            // 
            this.menuFileContainer.BackgroundStyle.Class = "RibbonFileMenuContainer";
            this.menuFileContainer.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.menuFileContainer.LayoutOrientation = DevComponents.DotNetBar.eOrientation.Vertical;
            this.menuFileContainer.Name = "menuFileContainer";
            this.menuFileContainer.SubItems.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.menuFileBottomContainer});
            // 
            // 
            // 
            this.menuFileContainer.TitleStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // menuFileBottomContainer
            // 
            // 
            // 
            // 
            this.menuFileBottomContainer.BackgroundStyle.Class = "RibbonFileMenuBottomContainer";
            this.menuFileBottomContainer.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.menuFileBottomContainer.HorizontalItemAlignment = DevComponents.DotNetBar.eHorizontalItemsAlignment.Right;
            this.menuFileBottomContainer.Name = "menuFileBottomContainer";
            this.menuFileBottomContainer.SubItems.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.buttonExit});
            // 
            // 
            // 
            this.menuFileBottomContainer.TitleStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // buttonExit
            // 
            this.buttonExit.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.buttonExit.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.buttonExit.Image = ((System.Drawing.Image)(resources.GetObject("buttonExit.Image")));
            this.buttonExit.Name = "buttonExit";
            this.buttonExit.SubItemsExpandWidth = 24;
            this.buttonExit.Text = "&Salir";
            this.buttonExit.Click += new System.EventHandler(this.buttonExit_Click);
            // 
            // toolStripButton7
            // 
            this.toolStripButton7.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButton7.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton7.Image")));
            this.toolStripButton7.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton7.Name = "toolStripButton7";
            this.toolStripButton7.Size = new System.Drawing.Size(36, 36);
            this.toolStripButton7.Text = "Generar Facturación Eletrónica";
            this.toolStripButton7.Click += new System.EventHandler(this.toolStripButton7_Click);
            // 
            // toolStripButton9
            // 
            this.toolStripButton9.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButton9.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton9.Image")));
            this.toolStripButton9.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton9.Name = "toolStripButton9";
            this.toolStripButton9.Size = new System.Drawing.Size(36, 36);
            this.toolStripButton9.Text = "Abrir PDF";
            this.toolStripButton9.Click += new System.EventHandler(this.toolStripButton9_Click);
            // 
            // toolStripButton4
            // 
            this.toolStripButton4.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButton4.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton4.Image")));
            this.toolStripButton4.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton4.Name = "toolStripButton4";
            this.toolStripButton4.Size = new System.Drawing.Size(36, 36);
            this.toolStripButton4.Text = "Ingresar XML para enviar a EDICOM";
            this.toolStripButton4.Visible = false;
            // 
            // btnRefresh
            // 
            this.btnRefresh.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnRefresh.Image = ((System.Drawing.Image)(resources.GetObject("btnRefresh.Image")));
            this.btnRefresh.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnRefresh.Name = "btnRefresh";
            this.btnRefresh.Size = new System.Drawing.Size(36, 36);
            this.btnRefresh.Text = "Cargar";
            this.btnRefresh.Click += new System.EventHandler(this.btnRefresh_Click);
            // 
            // toolStripButton3
            // 
            this.toolStripButton3.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButton3.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton3.Image")));
            this.toolStripButton3.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton3.Name = "toolStripButton3";
            this.toolStripButton3.Size = new System.Drawing.Size(36, 36);
            this.toolStripButton3.Text = "Ver XML Original";
            this.toolStripButton3.Click += new System.EventHandler(this.toolStripButton3_Click);
            // 
            // frmPrincipal
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(983, 512);
            this.Controls.Add(this.progressBar1);
            this.Controls.Add(this.ribbonControl1);
            this.Controls.Add(this.dtgrdGeneral);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.KeyPreview = true;
            this.Name = "frmPrincipal";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.frmPrincipal_FormClosed);
            this.Load += new System.EventHandler(this.frmPrincipal_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dtgrdGeneral)).EndInit();
            this.ribbonControl1.ResumeLayout(false);
            this.ribbonControl1.PerformLayout();
            this.ribbonPanel1.ResumeLayout(false);
            this.ribbonBar1.ResumeLayout(false);
            this.ribbonBar1.PerformLayout();
            this.ribbonPanel3.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ToolStripButton btnRefresh;
        private System.Windows.Forms.ToolStripLabel toolStripLabel2;
        private System.Windows.Forms.ToolStripButton toolStripButton3;
        private System.Windows.Forms.DataGridView dtgrdGeneral;
        private System.Windows.Forms.ToolStripButton toolStripButton4;
        private System.Windows.Forms.ToolStripButton toolStripButton7;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator4;
        private System.Windows.Forms.ToolStripLabel toolStripLabel1;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator5;
        private System.Windows.Forms.ToolStripButton toolStripButton9;
        private System.Windows.Forms.ToolStripLabel toolStripStatusLabel1;
        private System.Windows.Forms.CheckBox PDF_VER;
        private DevComponents.DotNetBar.RibbonControl ribbonControl1;
        private DevComponents.DotNetBar.RibbonPanel ribbonPanel1;
        private DevComponents.DotNetBar.RibbonBar ribbonBar5;
        private DevComponents.DotNetBar.ButtonItem buttonItem12;
        private DevComponents.DotNetBar.ButtonItem EXPORTAR_btn;
        private DevComponents.DotNetBar.ButtonItem buttonItem23;
        private DevComponents.DotNetBar.ButtonItem buttonItem29;
        private DevComponents.DotNetBar.RibbonBar ribbonBar1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.DateTimePicker INICIO;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.DateTimePicker FINAL;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox INVOICE_ID;
        private System.Windows.Forms.TextBox SERIE;
        private System.Windows.Forms.TextBox TOPtxt;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private DevComponents.DotNetBar.ButtonItem buttonItem2;
        private DevComponents.DotNetBar.ButtonItem buttonItem8;
        private DevComponents.DotNetBar.ItemContainer itemContainer5;
        private DevComponents.DotNetBar.ItemContainer itemContainer6;
        private DevComponents.DotNetBar.ControlContainerItem controlContainerItem16;
        private DevComponents.DotNetBar.ControlContainerItem controlContainerItem17;
        private DevComponents.DotNetBar.ControlContainerItem controlContainerItem18;
        private DevComponents.DotNetBar.ControlContainerItem controlContainerItem19;
        private DevComponents.DotNetBar.ItemContainer itemContainer7;
        private DevComponents.DotNetBar.ControlContainerItem controlContainerItem20;
        private DevComponents.DotNetBar.ControlContainerItem controlContainerItem21;
        private DevComponents.DotNetBar.ControlContainerItem controlContainerItem22;
        private DevComponents.DotNetBar.ControlContainerItem controlContainerItem23;
        private DevComponents.DotNetBar.ControlContainerItem controlContainerItem25;
        private DevComponents.DotNetBar.ControlContainerItem controlContainerItem26;
        private DevComponents.DotNetBar.RibbonPanel ribbonPanel3;
        private DevComponents.DotNetBar.RibbonBar ribbonBar8;
        private DevComponents.DotNetBar.ButtonItem buttonItem4;
        private DevComponents.DotNetBar.ButtonItem buttonItem5;
        private DevComponents.DotNetBar.ButtonItem buttonItem6;
        private DevComponents.DotNetBar.RibbonTabItem ribbonTabItem1;
        private DevComponents.DotNetBar.RibbonTabItem ribbonTabItem3;
        private DevComponents.DotNetBar.Office2007StartButton buttonFile;
        private DevComponents.DotNetBar.ItemContainer menuFileContainer;
        private DevComponents.DotNetBar.ItemContainer menuFileBottomContainer;
        private DevComponents.DotNetBar.ButtonItem buttonExit;
        private DevComponents.DotNetBar.ButtonItem buttonItem37;
        private DevComponents.DotNetBar.LabelItem informacion;
        private DevComponents.DotNetBar.RibbonTabItemGroup ribbonTabItemGroup1;
        private DevComponents.DotNetBar.ButtonItem buttonItem14;
        private System.Windows.Forms.CheckBox CFDI_PRUEBA;
        private DevComponents.DotNetBar.ControlContainerItem controlContainerItem1;
        private DevComponents.DotNetBar.ControlContainerItem controlContainerItem2;
        private System.Windows.Forms.CheckBox PENDIENTES;
        private DevComponents.DotNetBar.ComboBoxItem ENTITY_ID;
        private DevComponents.Editors.ComboItem comboItem3;
        private DevComponents.Editors.ComboItem comboItem4;
        private DevComponents.Editors.ComboItem comboItem1;
        private DevComponents.Editors.ComboItem comboItem2;
        private DevComponents.DotNetBar.LabelItem EMPRESA_NOMBRE;
        private DevComponents.DotNetBar.ButtonItem CAMBIAR;
        private DevComponents.DotNetBar.ButtonItem buttonItem10;
        private System.Windows.Forms.ComboBox ESTADO;
        private System.Windows.Forms.Label label1;
        private DevComponents.DotNetBar.ItemContainer itemContainer1;
        private DevComponents.DotNetBar.ControlContainerItem controlContainerItem3;
        private DevComponents.DotNetBar.ControlContainerItem controlContainerItem4;
        private DevComponents.DotNetBar.ControlContainerItem controlContainerItem5;
        private DevComponents.DotNetBar.ButtonItem CANCELAR_btn;
        private System.Windows.Forms.TextBox CUSTOMER_NAME;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox CUSTOMER_ID;
        private System.Windows.Forms.Label label2;
        private DevComponents.DotNetBar.ItemContainer itemContainer2;
        private DevComponents.DotNetBar.ControlContainerItem controlContainerItem6;
        private DevComponents.DotNetBar.ControlContainerItem controlContainerItem7;
        private DevComponents.DotNetBar.ControlContainerItem controlContainerItem8;
        private DevComponents.DotNetBar.ControlContainerItem controlContainerItem9;
        private DevComponents.DotNetBar.ItemContainer itemContainer3;
        private DevComponents.DotNetBar.ButtonItem buttonItem9;
        private DevComponents.DotNetBar.ButtonItem buttonItem7;
        private DevComponents.DotNetBar.ButtonItem buttonItem11;
        private DevComponents.DotNetBar.ItemContainer itemContainer4;
        private DevComponents.DotNetBar.ButtonItem VERIFICAR;
        private System.Windows.Forms.CheckBox OCULTAR;
        private System.Windows.Forms.ProgressBar progressBar1;
        private DevComponents.DotNetBar.ItemContainer itemContainer8;
        private DevComponents.DotNetBar.ItemContainer itemContainer9;
        private System.Windows.Forms.Label label7;
        private DevComponents.DotNetBar.ControlContainerItem controlContainerItem11;
        private DevComponents.DotNetBar.ControlContainerItem controlContainerItem12;
        private System.Windows.Forms.ComboBox FORMATO;
        private DevComponents.DotNetBar.ControlContainerItem controlContainerItem13;
        private DevComponents.DotNetBar.ButtonItem buttonItem16;
        private DevComponents.DotNetBar.ButtonItem buttonItem17;
        private DevComponents.DotNetBar.ItemContainer itemContainer10;
        private DevComponents.DotNetBar.ItemContainer itemContainer11;
        private DevComponents.DotNetBar.ButtonItem PEDIMENTOS_btn;
        private DevComponents.DotNetBar.ItemContainer itemContainer12;
        private DevComponents.DotNetBar.ItemContainer itemContainer13;
        private DevComponents.DotNetBar.ButtonItem ADDENDA;
        private DevComponents.DotNetBar.ButtonItem IMPORTAR;
        private DevComponents.DotNetBar.ButtonItem ADDENDA_INDIVIDUAL;
        private System.Windows.Forms.FolderBrowserDialog folderBrowserDialog1;
        private DevComponents.DotNetBar.ButtonItem buttonItem13;
        private DevComponents.DotNetBar.ButtonItem buttonItem22;
        private System.Windows.Forms.CheckBox MOSTRAR_PDF_ESTADO;
        private DevComponents.DotNetBar.ItemContainer itemContainer14;
        private DevComponents.DotNetBar.ControlContainerItem controlContainerItem14;
        private DevComponents.DotNetBar.ItemContainer itemContainer15;
        private DevComponents.DotNetBar.ControlContainerItem controlContainerItem10;
        private DevComponents.DotNetBar.LabelItem informar_procesar;
        private DevComponents.DotNetBar.ButtonItem buttonItem1;
        private System.Windows.Forms.Timer oTimer;
        private DevComponents.DotNetBar.ButtonItem buttonItem3;
        private DevComponents.DotNetBar.ButtonItem buttonItem15;
        private DevComponents.DotNetBar.ButtonItem btnRetencion;
        private DevComponents.DotNetBar.ButtonItem btnProformas;
        private DevComponents.DotNetBar.ButtonItem buttonItem18;
        private DevComponents.DotNetBar.ButtonItem buttonItem19;
        private DevComponents.DotNetBar.ButtonItem buttonItem20;
    }
}

