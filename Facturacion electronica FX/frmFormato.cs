﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;

using System.Text;
using System.Windows.Forms;
using Generales;
using ModeloDocumentosFX;

namespace FE_FX
{
    public partial class frmFormato : Form
    {
        private cCONEXCION oData = new cCONEXCION("");
        private cFORMATO oObjeto;
        private bool modificado;

        public frmFormato(string sConn)
        {
            oData.sConn = sConn;
            oObjeto = new cFORMATO();
            InitializeComponent();
            ajax_loader.Visible = false;
            
        }
        public frmFormato(string sConn, string ROW_IDp)
        {
            modificado = false;
            oData.sConn = sConn;
            oObjeto = new cFORMATO();

            InitializeComponent();

            limpiar();
            oObjeto.ROW_ID = ROW_IDp;
            cargar();
        }

        public void limpiar()
        {
            if (modificado)
            {

                DialogResult Resultado = MessageBox.Show("¿Desea guardar los cambios?", Application.ProductName + "-" + Application.ProductVersion, MessageBoxButtons.YesNo);
                if (Resultado.ToString() == "Yes")
                {
                    guardar();
                }


            }
            modificado = false;
            oObjeto = new cFORMATO();
            ID.Text = "";
            VISTA.Text = "";
            REPORTE.Text = "";
            TIPO.SelectedIndex = 0;
        }

        private void guardar()
        {
            if (String.IsNullOrEmpty(oObjeto.ROW_ID_EMPRESA))
            {
                MessageBox.Show("Seleccione la empresa", Application.ProductName + "-" + Application.ProductVersion, MessageBoxButtons.OK);
                button1.PerformClick();
                return;
            }
            ajax_loader.Visible = true;
            oObjeto.ID = ID.Text;
            oObjeto.VISTA = VISTA.Text;
            oObjeto.REPORTE = REPORTE.Text;
            oObjeto.TIPO = TIPO.Text;
            oObjeto.ACTIVO = ACTIVO.Checked.ToString();

            if (oObjeto.guardar())
            {
                toolStripStatusLabel1.Text = "Datos Actualizados " + DateTime.Now.ToShortDateString() + " " + DateTime.Now.ToLongTimeString();
                modificado = false;
            }
            ajax_loader.Visible = false;
        }

        private void cargar()
        {
            ajax_loader.Visible = true;
            if (oObjeto.cargar(oObjeto.ROW_ID))
            {
                ID.Text = oObjeto.ID;
                VISTA.Text = oObjeto.VISTA;
                REPORTE.Text = oObjeto.REPORTE;
                TIPO.Text = oObjeto.TIPO;

                this.EMPRESA.Text = oObjeto.EMPRESA;

                ACTIVO.Checked = bool.Parse(oObjeto.ACTIVO);

                toolStripStatusLabel1.Text  = "Datos Cargados " + DateTime.Now.ToShortDateString() + " " + DateTime.Now.ToLongTimeString();
                modificado = false;

            }
            ajax_loader.Visible = false;
        }

        private void button3_Click(object sender, EventArgs e)
        {
            guardar();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            limpiar();
        }

        private void frmUsuario_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (modificado)
            {
                DialogResult Resultado = MessageBox.Show("¿Desea guardar los cambios?", Application.ProductName + "-" + Application.ProductVersion, MessageBoxButtons.YesNo);
                if (Resultado.ToString() == "Yes")
                {
                    guardar();
                }
            }
        }

        private void frmUsuario_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)27)
            {
                this.Close();
            }
            else
            {
                modificado = true;
                if ((e.KeyChar.ToString() == "<")
                                | (e.KeyChar.ToString() == ">")
                                
                                | (e.KeyChar.ToString() == "'")
                                | (e.KeyChar.ToString() == "#")
                                )
                {
                    e.Handled = true;
                }
            }
        }

        private void button7_Click(object sender, EventArgs e)
        {
            eliminar();
        }

        private void eliminar()
        {
            DialogResult Resultado = MessageBox.Show("¿Esta seguro de eliminar?", Application.ProductName + "-" + Application.ProductVersion, MessageBoxButtons.YesNo);
            if (Resultado.ToString() != "Yes")
            {
                return;
            }
            if (oObjeto.eliminar(oObjeto.ROW_ID))
            {
                limpiar();
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            ModeloDocumentosFXContainer dbContext = new ModeloDocumentosFXContainer();
            string conection = dbContext.Database.Connection.ConnectionString;

            frmEmpresas oBuscador = new frmEmpresas("S","S");
            if (oBuscador.ShowDialog() == DialogResult.OK)
            {
                string ROW_ID = (string)oBuscador.selecionados[0].Cells[0].Value;
                string ID = (string)oBuscador.selecionados[0].Cells[1].Value;
                oObjeto.ROW_ID_EMPRESA = ROW_ID;
                EMPRESA.Text = ID;
            }
        }

    }
}
