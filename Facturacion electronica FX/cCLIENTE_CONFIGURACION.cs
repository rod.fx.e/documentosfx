﻿using System;
using System.Collections.Generic;

using System.Text;
using System.Data;
using System.Windows;
using System.Windows.Forms;
using Generales;

namespace FE_FX
{
    //ALTER TABLE CLIENTE_CONFIGURACION ADD usoCFDI varchar(50)
    class cCLIENTE_CONFIGURACION
    {
        private string sSQL = "";
        public string ROW_ID { get; set; }
        public string CUSTOMER_ID { get; set; }
        public string ASUNTO { get; set; }
        public string LEYENDA_FISCAL { get; set; }
        public string MENSAJE { get; set; }
        public string ARCHIVO { get; set; }
        public string paisEntregar { get; set; }
        public string ShipTo { get; set; }
        public string usoCFDI { get; set; }
        public string Europeo { get; set; }
        

        private cCONEXCION oData = new cCONEXCION("");
        public string CORREO;
        public bool iva0;

        public cCLIENTE_CONFIGURACION()
        {
            limpiar();
        }

        public cCLIENTE_CONFIGURACION(cCONEXCION pData)
        {
            oData = pData;
            limpiar();
        }

        public void limpiar()
        {
            ROW_ID = "";
            CUSTOMER_ID = "";
            ASUNTO = "";
            MENSAJE="";
            ARCHIVO="";
            LEYENDA_FISCAL = "";
            CORREO = "";
            FORMATO = "";
            ShipTo = "";
            paisEntregar = "";
            iva0 = false;
            usoCFDI = "";
            Europeo = string.Empty;
        }

        public List<cCLIENTE_CONFIGURACION> todos(string BUSQUEDA, string top="")
        {

            List<cCLIENTE_CONFIGURACION> lista = new List<cCLIENTE_CONFIGURACION>();

            sSQL = " SELECT * ";
            if (!String.IsNullOrEmpty(top))
            {
                sSQL = " SELECT TOP " + top  + " * ";
            }
            sSQL += " FROM CLIENTE_CONFIGURACION ";
            sSQL += " WHERE CUSTOMER_ID LIKE '%" + BUSQUEDA + "%' ";

            DataTable oDataTable = oData.EjecutarConsulta(sSQL);
            foreach (DataRow oDataRow in oDataTable.Rows)
            {
                cCLIENTE_CONFIGURACION oCLIENTE_CONFIGURACION = new cCLIENTE_CONFIGURACION(oData);
                oCLIENTE_CONFIGURACION.cargar(oDataRow["ROW_ID"].ToString());
                lista.Add(oCLIENTE_CONFIGURACION);
            }
            return lista;
        }


        public bool cargar_CUSTOMER_ID(string CUSTOMER_IDp, string COUNTRYp="", string shipTop = "")
        {
            sSQL = " SELECT * ";
            sSQL += " FROM CLIENTE_CONFIGURACION ";
            sSQL += " WHERE CUSTOMER_ID='" + CUSTOMER_IDp + "' ";
            if (COUNTRYp != "")
            {
                sSQL += " AND paisEntregar='" + COUNTRYp + "' "; 
            }

            BitacoraFX.Log("cCLIENTE_CONFIGURACION-94-LeyendaFiscal-cargar_CUSTOMER_ID - "+ sSQL);

            DataTable oDataTable = oData.EjecutarConsulta(sSQL);
            if (oDataTable != null)
            {
                foreach (DataRow oDataRow in oDataTable.Rows)
                {
                    string ShipToTr = oDataRow["ShipTo"].ToString();
                    if (String.IsNullOrEmpty(ShipToTr))
                    {
                        cargar(oDataRow["ROW_ID"].ToString());
                        BitacoraFX.Log("cCLIENTE_CONFIGURACION-105-LeyendaFiscal-cargar_CUSTOMER_ID - " + oDataRow["ROW_ID"].ToString());
                        return true;
                    }
                    else
                    {
                        if (ShipToTr.Contains(shipTop))
                        {
                            cargar(oDataRow["ROW_ID"].ToString());
                            BitacoraFX.Log("cCLIENTE_CONFIGURACION-113-LeyendaFiscal-cargar_CUSTOMER_ID - " + oDataRow["ROW_ID"].ToString());
                            return true;
                        }
                    }
                    //if (!String.IsNullOrEmpty(shipTop))
                    //{
                    //    sSQL += " AND ShipTo like '" + shipTop + "' ";
                    //}


                    
                }
            }
            else
            {
                BitacoraFX.Log("cCLIENTE_CONFIGURACION-94-LeyendaFiscal-cargar_CUSTOMER_ID - Sin Datos");
            }
            return false;
        }

        public bool cargar(string ROW_IDp)
        {
            sSQL  = " SELECT * ";
            sSQL += " FROM CLIENTE_CONFIGURACION ";
            sSQL += " WHERE ROW_ID=" + ROW_IDp + " ";
            BitacoraFX.Log("CLIENTE_CONFIGURACION-134 " + sSQL);
            DataTable oDataTable = oData.EjecutarConsulta(sSQL);
            if (oDataTable != null)
            {
                foreach (DataRow oDataRow in oDataTable.Rows)
                {
                    //Añadir el Registro
                    try
                    {
                        ROW_ID = oDataRow["ROW_ID"].ToString();
                        CUSTOMER_ID = oDataRow["CUSTOMER_ID"].ToString();
                        ASUNTO = oDataRow["ASUNTO"].ToString();
                        MENSAJE = oDataRow["MENSAJE"].ToString();
                        ARCHIVO = oDataRow["ARCHIVO"].ToString();
                        LEYENDA_FISCAL = oDataRow["LEYENDA_FISCAL"].ToString();
                        BitacoraFX.Log("CLIENTE_CONFIGURACION-147 " + sSQL);
                        FORMATO = oDataRow["FORMATO"].ToString();
                        CORREO = oDataRow["CORREO"].ToString();
                        ShipTo = string.Empty;
                    }
                    catch(Exception Error)
                    {
                        BitacoraFX.Log("cCLIENTE_CONFIGURACION-160-LeyendaFiscal-cargar - "+ Error.Message);
                    }

                    try
                    {
                        ShipTo = oDataRow["ShipTo"].ToString();
                    }
                    catch (Exception Error)
                    {
                        BitacoraFX.Log("cCLIENTE_CONFIGURACION-168-LeyendaFiscal-cargar - ShipTo " + Error.Message);
                    }

                    paisEntregar = oDataRow["paisEntregar"].ToString();
                    try
                    {
                        iva0 = bool.Parse(oDataRow["iva0"].ToString());
                    }
                    catch (Exception Error)
                    {
                        BitacoraFX.Log("cCLIENTE_CONFIGURACION-180-LeyendaFiscal-cargar - iva0 " + Error.Message);
                    }

                    try
                    {
                        usoCFDI = oDataRow["usoCFDI"].ToString();
                    }
                    catch (Exception Error)
                    {
                        BitacoraFX.Log("cCLIENTE_CONFIGURACION-160-LeyendaFiscal-cargar - usoCFDI " + Error.Message);
                    }
                    try
                    {
                        Europeo = oDataRow["Europeo"].ToString();
                    }
                    catch (Exception Error)
                    {
                        BitacoraFX.Log("cCLIENTE_CONFIGURACION-196-LeyendaFiscal-cargar Europeo- " + Error.Message);
                    }
                    return true;
                }
            }
            oDataTable.Dispose();
            return false;
        }

        public bool guardar()
        {


            if (CUSTOMER_ID == "")
            {
                MessageBox.Show("Selecione un Cliente.", Application.ProductName + "-" + Application.ProductVersion, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return false;
            }
            if (ROW_ID == "")
            {

                sSQL = " INSERT INTO CLIENTE_CONFIGURACION ";
                sSQL += " ( ";
                sSQL += " CUSTOMER_ID,ASUNTO";
                sSQL += ",MENSAJE,ARCHIVO";
                sSQL += ",LEYENDA_FISCAL, FORMATO, CORREO,paisEntregar,iva0,usoCFDI,ShipTo,Europeo";
                sSQL += " )";
                sSQL += " VALUES ";
                sSQL += "(";
                sSQL += "  '" + CUSTOMER_ID + "','" + ASUNTO + "'  ";
                sSQL += ",'" + MENSAJE + "','" + ARCHIVO + "'";
                sSQL += ",'" + LEYENDA_FISCAL + "','" + FORMATO + "','" + CORREO + 
                    @"','" + paisEntregar + "','" + iva0.ToString() + "','" + usoCFDI + "','" + ShipTo + "','" + Europeo + "'";
                sSQL += ")";
                DataTable oDataTable = oData.EjecutarConsulta(sSQL);
                if (oDataTable!=null)
                {

                    sSQL = "SELECT TOP 1 MAX(ROW_ID) as ROW_ID FROM CLIENTE_CONFIGURACION";
                    oDataTable = oData.EjecutarConsulta(sSQL);
                    if (oDataTable != null)
                    {
                        foreach (DataRow oDataRow in oDataTable.Rows)
                        {
                            ROW_ID = oDataRow["ROW_ID"].ToString();
                        }
                    }
                }

            }
            else
            {
                sSQL = " UPDATE CLIENTE_CONFIGURACION SET ";
                sSQL += "  CUSTOMER_ID='" + CUSTOMER_ID + "',ASUNTO='" + ASUNTO + "'";
                sSQL += ",MENSAJE='" + MENSAJE + "',ARCHIVO='" + ARCHIVO + "'";
                sSQL += ",LEYENDA_FISCAL='" + LEYENDA_FISCAL  + "'";
                sSQL += ",FORMATO='" + FORMATO + "',CORREO='" + CORREO + 
                    @"',paisEntregar='" + paisEntregar + "',iva0='" + iva0.ToString() + "', usoCFDI='" + usoCFDI + @"'
                ,ShipTo='" + ShipTo + "',Europeo='" + Europeo + "'";
                sSQL += " WHERE ROW_ID=" + ROW_ID + " ";
                oData.EjecutarConsulta(sSQL);
            }
            return true;

        }

        public bool eliminar(string ROW_IDp)
        {
            if (ROW_IDp != "")
            {
                sSQL = "DELETE FROM CLIENTE_CONFIGURACION WHERE ROW_ID=" + ROW_IDp;
                oData.EjecutarConsulta(sSQL);
                return true;
            }
            return false;

        }


        public string FORMATO { get; set; }
    }
}
