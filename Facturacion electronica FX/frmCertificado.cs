﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;

using System.Text;
using System.Windows.Forms;
using CryptoSysPKI;
using System.IO;
using System.Security.Cryptography;
using System.Security.Cryptography.X509Certificates;
using System.Diagnostics;
using Generales;
using ModeloDocumentosFX;

namespace FE_FX
{
    public partial class frmCertificado : Form
    {
        private cCONEXCION oData = new cCONEXCION("");
        private cCERTIFICADO oObjeto;
        private bool modificado;

        public frmCertificado(string sConn)
        {
            oData.sConn = sConn;
            oObjeto = new cCERTIFICADO();
            InitializeComponent();
            ajax_loader.Visible = false;
            
        }
        public frmCertificado(string sConn, string ROW_IDp)
        {
            modificado = false;
            oData.sConn = sConn;
            oObjeto = new cCERTIFICADO();

            InitializeComponent();

            limpiar();
            oObjeto.ROW_ID = ROW_IDp;
            cargar();
        }

        public void limpiar()
        {
            if (modificado)
            {

                DialogResult Resultado = MessageBox.Show("¿Desea guardar los cambios?", Application.ProductName + "-" + Application.ProductVersion, MessageBoxButtons.YesNo);
                if (Resultado.ToString() == "Yes")
                {
                    guardar();
                }


            }
            modificado = false;
            oObjeto = new cCERTIFICADO();
            ID.Text = "";
            EMPRESA.Text = "";
            CERTIFICADO.Text = "";
            NO_CERTIFICADO.Text = "";
            FECHA_DESDE.Text = "";
            FECHA_HASTA.Text = "";
            LLAVE.Text = "";
            PASSWORD.Text = "";
        }

        private void guardar()
        {
            ajax_loader.Visible = true;

            oObjeto.ID = ID.Text;
            oObjeto.CERTIFICADO = CERTIFICADO.Text;
            oObjeto.NO_CERTIFICADO = NO_CERTIFICADO.Text;
            oObjeto.LLAVE = LLAVE.Text;
            oObjeto.PASSWORD = PASSWORD.Text;
            oObjeto.FECHA_DESDE = FECHA_DESDE.Text;
            oObjeto.FECHA_HASTA = FECHA_HASTA.Text;
            oObjeto.ACTIVO = ACTIVO.Checked.ToString();
            if (PFX.Text.Trim() == "")
            {
                crear_pfx();
            }
            oObjeto.PFX = PFX.Text;

            if (oObjeto.guardar())
            {
                toolStripStatusLabel1.Text = "Datos Actualizados " + DateTime.Now.ToShortDateString() + " " + DateTime.Now.ToLongTimeString();
                
                modificado = false;
            }
            else
            {
                toolStripStatusLabel1.Text = "Error " + DateTime.Now.ToShortDateString() + " " + DateTime.Now.ToLongTimeString();
                
            }

            ajax_loader.Visible = false;
        }

        private void cargar()
        {
            ajax_loader.Visible = true;
            if (oObjeto.cargar(oObjeto.ROW_ID))
            {
                ID.Text = oObjeto.ID;
                CERTIFICADO.Text = oObjeto.CERTIFICADO;
                NO_CERTIFICADO.Text = oObjeto.NO_CERTIFICADO;
                LLAVE.Text = oObjeto.LLAVE;
                PASSWORD.Text = oObjeto.PASSWORD;
                FECHA_DESDE.Text = oObjeto.FECHA_DESDE;
                FECHA_HASTA.Text = oObjeto.FECHA_HASTA;
                EMPRESA.Text = oObjeto.EMPRESA;
                PFX.Text = oObjeto.PFX;
                ACTIVO.Checked = bool.Parse(oObjeto.ACTIVO);

                toolStripStatusLabel1.Text = "Datos Cargados " + DateTime.Now.ToShortDateString() + " " + DateTime.Now.ToLongTimeString();
                modificado = false;

            }
            ajax_loader.Visible = false;
        }

        private void button3_Click(object sender, EventArgs e)
        {
            guardar();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            limpiar();
        }

        private void frmUsuario_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (modificado)
            {
                DialogResult Resultado = MessageBox.Show("¿Desea guardar los cambios?", Application.ProductName + "-" + Application.ProductVersion, MessageBoxButtons.YesNo);
                if (Resultado.ToString() == "Yes")
                {
                    guardar();
                }
            }
        }

        private void frmUsuario_KeyPress(object sender, KeyPressEventArgs e)
        {

            if (e.KeyChar == (char)27)
            {
                this.Close();
            }
            else
            {
                modificado = true;
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            OpenFileDialog openFileDialog1 = new OpenFileDialog();

            //            openFileDialog1.InitialDirectory = "c:\\";
            openFileDialog1.Filter = "cer files (*.cer)|*.cer|All files (*.*)|*.*";
            openFileDialog1.FilterIndex = 1;
            openFileDialog1.RestoreDirectory = true;

            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                try
                {
                    CERTIFICADO.Text = openFileDialog1.FileName;
                    if (ValidaCertificado() == false)
                    {
                        CERTIFICADO.Focus();
                    }
                }
                catch (Exception)
                {
                    MessageBox.Show("No existe el Archivo seleccionado", Application.ProductName + "-" + Application.ProductVersion, MessageBoxButtons.OK);
                }
            }
        }



        private bool ValidaCertificado()
        {
            string Certificate = CERTIFICADO.Text.Trim();
            char[] delimiterChars = { '.' };
            string Text = CERTIFICADO.Text.ToString();
            string[] TipoCer = Text.Split(delimiterChars);

            if (TipoCer[TipoCer.Length - 1].ToString() != "cer")
            {
                MessageBox.Show("El archivo para el certificado debe tener la extensión .cer", Application.ProductName + "-" + Application.ProductVersion, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                CERTIFICADO.Text = "";
                return false;
            }
            else if (System.IO.File.Exists(CERTIFICADO.Text.Trim()) == false)
            {
                MessageBox.Show("No existe el archivo:" + CERTIFICADO.Text.Trim(), Application.ProductName + "-" + Application.ProductVersion, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                CERTIFICADO.Text = "";
                return false;
            }
            else
            {
                try
                {
                    X509Certificate cert = X509Certificate.CreateFromCertFile(Certificate);
                    string NoSerie = cert.GetSerialNumberString();

                    string Dato = cert.GetName();

                    FECHA_DESDE.Text = cert.GetEffectiveDateString();

                    FECHA_HASTA.Text = cert.GetExpirationDateString();

                    StringBuilder SerieHex = new StringBuilder();
                    for (int i = 0; i <= NoSerie.Length - 2; i += 2)
                    {
                        SerieHex.Append(Convert.ToString(Convert.ToChar(Int32.Parse(NoSerie.Substring(i, 2), System.Globalization.NumberStyles.HexNumber))));
                    }

                    NO_CERTIFICADO.Text = SerieHex.ToString();

                    return true;
                }

                catch (CryptographicException)
                {
                    MessageBox.Show("No es un certificado valido", Application.ProductName + "-" + Application.ProductVersion, MessageBoxButtons.OK, MessageBoxIcon.Information);
                    CERTIFICADO.Text = "";
                    CERTIFICADO.Focus();
                    return false;
                }
            }
        }

        private void button4_Click(object sender, EventArgs e)
        {
            OpenFileDialog openFileDialog1 = new OpenFileDialog();
            openFileDialog1.Filter = "key files (*.key)|*.key|All files (*.*)|*.*";
            openFileDialog1.FilterIndex = 1;
            openFileDialog1.RestoreDirectory = true;
            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                try
                {
                    LLAVE.Text = openFileDialog1.FileName;
                }
                catch (Exception)
                {
                    MessageBox.Show("No existe el archivo seleccionado", Application.ProductName + "-" + Application.ProductVersion, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                }
            }
        }

        private void ID_TextChanged(object sender, EventArgs e)
        {
            modificado = true;
        }

        private void button7_Click(object sender, EventArgs e)
        {
            ModeloDocumentosFXContainer dbContext = new ModeloDocumentosFXContainer();
            string conection = dbContext.Database.Connection.ConnectionString;
            frmEmpresas oBuscador = new frmEmpresas(conection, "S");
            if (oBuscador.ShowDialog() == DialogResult.OK)
            {
                string ROW_ID = (string)oBuscador.selecionados[0].Cells[0].Value;
                string ID = (string)oBuscador.selecionados[0].Cells[1].Value;
                oObjeto.ROW_ID_EMPRESA = ROW_ID;
                EMPRESA.Text = ID;
            }
        }

        private void Validar_Caracteres(object sender, KeyPressEventArgs e)
        {

            modificado = true;
            if ((e.KeyChar.ToString() == "<")
                | (e.KeyChar.ToString() == ">")
                
                | (e.KeyChar.ToString() == "'")
                | (e.KeyChar.ToString() == "#")
                )
            {
                e.Handled = true;
            }

        }

        private void button9_Click(object sender, EventArgs e)
        {
            eliminar();
        }

        private void eliminar()
        {
            DialogResult Resultado = MessageBox.Show("¿Esta seguro de eliminar?", Application.ProductName + "-" + Application.ProductVersion, MessageBoxButtons.YesNo);
            if (Resultado.ToString() != "Yes")
            {
                return;
            }
            if (oObjeto.eliminar(oObjeto.ROW_ID))
            {
                limpiar();
            }
        }

        private void button5_Click(object sender, EventArgs e)
        {
            if(PFX.Text.Trim()=="")
            {
                crear_pfx();
            }
        }

        private void crear_pfx()
        {
            //Crear pfx de forma Manual
            //openssl x509 -inform DER -in aaa010101aaa_csd_01.cer -out certificado.pem
            //openssl pkcs8 -inform DER -in aaa010101aaa_csd_01.key -passin pass:a0123456789 -out llave.pem
            //openssl pkcs12 -export -out CER_KEY.pfx -inkey llave.pem -in certificado.pem -passout pass:a0123456789
            
            toolStripStatusLabel1.Text = "Creando PFX para Cancelación.";
            string nombre_archivo = DateTime.Now.Day.ToString() + "_" + DateTime.Now.Month.ToString()+
                "_" + DateTime.Now.Year.ToString()+".pfx";
            //X509Certificate cert = X509Certificate.CreateFromCertFile(CERTIFICADO.Text);
            //cert.
            //byte[] certData = cert.Export(X509ContentType.Pkcs12, PASSWORD.Text);

            //if (File.Exists(nombre_archivo))
            //{
            //    File.Delete(nombre_archivo);
            //}
            //File.WriteAllBytes(nombre_archivo, certData);
            Pfx.MakeFile(nombre_archivo,CERTIFICADO.Text,LLAVE.Text,PASSWORD.Text,"",false);
            PFX.Text = nombre_archivo;
            toolStripStatusLabel1.Text = "Fin PFX para Cancelación.";
        }

        private void button6_Click(object sender, EventArgs e)
        {
            validar_pfx();
        }

        private void validar_pfx()
        {
            if (!Pfx.SignatureIsValid(PFX.Text,PASSWORD.Text))
            {
                MessageBox.Show("PFX Invalido.");
            }
        }

        private void checkBox1_CheckedChanged(object sender, EventArgs e)
        {
            PASSWORD.UseSystemPasswordChar = !checkBox1.Checked;
            if (checkBox1.Checked)
            {
                PASSWORD.PasswordChar = '*';
            }
            else
            {
                PASSWORD.PasswordChar = '\0';
            }
        }
    }
}
