﻿namespace FE_FX
{
    partial class frmUsuario
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmUsuario));
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.toolStripStatusLabel1 = new System.Windows.Forms.ToolStripStatusLabel();
            this.button3 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.ajax_loader = new System.Windows.Forms.PictureBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.NOMBRE = new System.Windows.Forms.TextBox();
            this.APELLIDO = new System.Windows.Forms.TextBox();
            this.USUARIO = new System.Windows.Forms.TextBox();
            this.PASSWORD = new System.Windows.Forms.TextBox();
            this.ACTIVO = new System.Windows.Forms.CheckBox();
            this.ADMINISTRADOR = new System.Windows.Forms.CheckBox();
            this.SERIE = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.LECTURA = new System.Windows.Forms.CheckBox();
            this.button7 = new System.Windows.Forms.Button();
            this.CANCELAR = new System.Windows.Forms.CheckBox();
            this.VERIFICAR = new System.Windows.Forms.CheckBox();
            this.CAMBIAR = new System.Windows.Forms.CheckBox();
            this.PEDIMENTOS = new System.Windows.Forms.CheckBox();
            this.EXPORTAR = new System.Windows.Forms.CheckBox();
            this.ADDENDA = new System.Windows.Forms.CheckBox();
            this.ADDENDA_INDIVIDUAL = new System.Windows.Forms.CheckBox();
            this.IMPORTAR = new System.Windows.Forms.CheckBox();
            this.PROFORMA = new System.Windows.Forms.CheckBox();
            this.COMPROBANTE_PAGO = new System.Windows.Forms.CheckBox();
            this.proformaConfiguracion = new System.Windows.Forms.CheckBox();
            this.CartaPorte = new System.Windows.Forms.CheckBox();
            this.statusStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ajax_loader)).BeginInit();
            this.SuspendLayout();
            // 
            // statusStrip1
            // 
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripStatusLabel1});
            this.statusStrip1.Location = new System.Drawing.Point(0, 336);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(321, 22);
            this.statusStrip1.TabIndex = 37;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // toolStripStatusLabel1
            // 
            this.toolStripStatusLabel1.Name = "toolStripStatusLabel1";
            this.toolStripStatusLabel1.Size = new System.Drawing.Size(42, 17);
            this.toolStripStatusLabel1.Text = "Estado";
            // 
            // button3
            // 
            this.button3.BackColor = System.Drawing.SystemColors.Control;
            this.button3.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button3.Image = global::FE_FX.Properties.Resources.Guardar;
            this.button3.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button3.Location = new System.Drawing.Point(80, 7);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(83, 23);
            this.button3.TabIndex = 440;
            this.button3.Text = "Guardar";
            this.button3.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.button3.UseVisualStyleBackColor = false;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // button2
            // 
            this.button2.BackColor = System.Drawing.SystemColors.Control;
            this.button2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button2.Image = global::FE_FX.Properties.Resources.Nuevo;
            this.button2.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button2.Location = new System.Drawing.Point(4, 7);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(70, 23);
            this.button2.TabIndex = 439;
            this.button2.Text = "Nuevo";
            this.button2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.button2.UseVisualStyleBackColor = false;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // ajax_loader
            // 
            this.ajax_loader.AccessibleDescription = "";
            this.ajax_loader.Image = global::FE_FX.Properties.Resources.ajax_loader;
            this.ajax_loader.Location = new System.Drawing.Point(258, 8);
            this.ajax_loader.Name = "ajax_loader";
            this.ajax_loader.Size = new System.Drawing.Size(58, 22);
            this.ajax_loader.TabIndex = 38;
            this.ajax_loader.TabStop = false;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(13, 48);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(44, 13);
            this.label1.TabIndex = 441;
            this.label1.Text = "Nombre";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(12, 73);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(44, 13);
            this.label2.TabIndex = 442;
            this.label2.Text = "Apellido";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(11, 99);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(43, 13);
            this.label3.TabIndex = 443;
            this.label3.Text = "Usuario";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(1, 125);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(53, 13);
            this.label4.TabIndex = 444;
            this.label4.Text = "Password";
            // 
            // NOMBRE
            // 
            this.NOMBRE.Location = new System.Drawing.Point(63, 45);
            this.NOMBRE.Name = "NOMBRE";
            this.NOMBRE.Size = new System.Drawing.Size(253, 20);
            this.NOMBRE.TabIndex = 445;
            this.NOMBRE.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.frmUsuario_KeyPress);
            // 
            // APELLIDO
            // 
            this.APELLIDO.Location = new System.Drawing.Point(63, 70);
            this.APELLIDO.Name = "APELLIDO";
            this.APELLIDO.Size = new System.Drawing.Size(253, 20);
            this.APELLIDO.TabIndex = 446;
            this.APELLIDO.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.frmUsuario_KeyPress);
            // 
            // USUARIO
            // 
            this.USUARIO.Location = new System.Drawing.Point(63, 96);
            this.USUARIO.Name = "USUARIO";
            this.USUARIO.Size = new System.Drawing.Size(253, 20);
            this.USUARIO.TabIndex = 447;
            this.USUARIO.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.frmUsuario_KeyPress);
            // 
            // PASSWORD
            // 
            this.PASSWORD.Location = new System.Drawing.Point(63, 122);
            this.PASSWORD.Name = "PASSWORD";
            this.PASSWORD.PasswordChar = '*';
            this.PASSWORD.Size = new System.Drawing.Size(253, 20);
            this.PASSWORD.TabIndex = 448;
            this.PASSWORD.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.frmUsuario_KeyPress);
            // 
            // ACTIVO
            // 
            this.ACTIVO.AutoSize = true;
            this.ACTIVO.Location = new System.Drawing.Point(63, 148);
            this.ACTIVO.Name = "ACTIVO";
            this.ACTIVO.Size = new System.Drawing.Size(56, 17);
            this.ACTIVO.TabIndex = 463;
            this.ACTIVO.Text = "Activo";
            this.ACTIVO.UseVisualStyleBackColor = true;
            // 
            // ADMINISTRADOR
            // 
            this.ADMINISTRADOR.AutoSize = true;
            this.ADMINISTRADOR.Location = new System.Drawing.Point(138, 148);
            this.ADMINISTRADOR.Name = "ADMINISTRADOR";
            this.ADMINISTRADOR.Size = new System.Drawing.Size(89, 17);
            this.ADMINISTRADOR.TabIndex = 464;
            this.ADMINISTRADOR.Text = "Administrador";
            this.ADMINISTRADOR.UseVisualStyleBackColor = true;
            // 
            // SERIE
            // 
            this.SERIE.Location = new System.Drawing.Point(63, 309);
            this.SERIE.Name = "SERIE";
            this.SERIE.Size = new System.Drawing.Size(253, 20);
            this.SERIE.TabIndex = 465;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(23, 312);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(31, 13);
            this.label5.TabIndex = 466;
            this.label5.Text = "Serie";
            // 
            // LECTURA
            // 
            this.LECTURA.AutoSize = true;
            this.LECTURA.Location = new System.Drawing.Point(238, 148);
            this.LECTURA.Name = "LECTURA";
            this.LECTURA.Size = new System.Drawing.Size(62, 17);
            this.LECTURA.TabIndex = 467;
            this.LECTURA.Text = "Lectura";
            this.LECTURA.UseVisualStyleBackColor = true;
            // 
            // button7
            // 
            this.button7.BackColor = System.Drawing.SystemColors.Control;
            this.button7.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button7.Image = global::FE_FX.Properties.Resources.Eliminar_Linea;
            this.button7.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button7.Location = new System.Drawing.Point(169, 7);
            this.button7.Name = "button7";
            this.button7.Size = new System.Drawing.Size(83, 23);
            this.button7.TabIndex = 468;
            this.button7.Text = "Eliminar";
            this.button7.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.button7.UseVisualStyleBackColor = false;
            this.button7.Click += new System.EventHandler(this.button7_Click);
            // 
            // CANCELAR
            // 
            this.CANCELAR.AutoSize = true;
            this.CANCELAR.Location = new System.Drawing.Point(63, 171);
            this.CANCELAR.Name = "CANCELAR";
            this.CANCELAR.Size = new System.Drawing.Size(68, 17);
            this.CANCELAR.TabIndex = 469;
            this.CANCELAR.Text = "Cancelar";
            this.CANCELAR.UseVisualStyleBackColor = true;
            // 
            // VERIFICAR
            // 
            this.VERIFICAR.AutoSize = true;
            this.VERIFICAR.Location = new System.Drawing.Point(137, 171);
            this.VERIFICAR.Name = "VERIFICAR";
            this.VERIFICAR.Size = new System.Drawing.Size(64, 17);
            this.VERIFICAR.TabIndex = 470;
            this.VERIFICAR.Text = "Verificar";
            this.VERIFICAR.UseVisualStyleBackColor = true;
            // 
            // CAMBIAR
            // 
            this.CAMBIAR.AutoSize = true;
            this.CAMBIAR.Location = new System.Drawing.Point(238, 171);
            this.CAMBIAR.Name = "CAMBIAR";
            this.CAMBIAR.Size = new System.Drawing.Size(64, 17);
            this.CAMBIAR.TabIndex = 471;
            this.CAMBIAR.Text = "Cambiar";
            this.CAMBIAR.UseVisualStyleBackColor = true;
            // 
            // PEDIMENTOS
            // 
            this.PEDIMENTOS.AutoSize = true;
            this.PEDIMENTOS.Location = new System.Drawing.Point(137, 194);
            this.PEDIMENTOS.Name = "PEDIMENTOS";
            this.PEDIMENTOS.Size = new System.Drawing.Size(81, 17);
            this.PEDIMENTOS.TabIndex = 472;
            this.PEDIMENTOS.Text = "Pedimentos";
            this.PEDIMENTOS.UseVisualStyleBackColor = true;
            // 
            // EXPORTAR
            // 
            this.EXPORTAR.AutoSize = true;
            this.EXPORTAR.Location = new System.Drawing.Point(63, 194);
            this.EXPORTAR.Name = "EXPORTAR";
            this.EXPORTAR.Size = new System.Drawing.Size(65, 17);
            this.EXPORTAR.TabIndex = 474;
            this.EXPORTAR.Text = "Exportar";
            this.EXPORTAR.UseVisualStyleBackColor = true;
            // 
            // ADDENDA
            // 
            this.ADDENDA.AutoSize = true;
            this.ADDENDA.Location = new System.Drawing.Point(63, 217);
            this.ADDENDA.Name = "ADDENDA";
            this.ADDENDA.Size = new System.Drawing.Size(69, 17);
            this.ADDENDA.TabIndex = 475;
            this.ADDENDA.Text = "Addenda";
            this.ADDENDA.UseVisualStyleBackColor = true;
            // 
            // ADDENDA_INDIVIDUAL
            // 
            this.ADDENDA_INDIVIDUAL.AutoSize = true;
            this.ADDENDA_INDIVIDUAL.Location = new System.Drawing.Point(137, 217);
            this.ADDENDA_INDIVIDUAL.Name = "ADDENDA_INDIVIDUAL";
            this.ADDENDA_INDIVIDUAL.Size = new System.Drawing.Size(117, 17);
            this.ADDENDA_INDIVIDUAL.TabIndex = 476;
            this.ADDENDA_INDIVIDUAL.Text = "Addenda Individual";
            this.ADDENDA_INDIVIDUAL.UseVisualStyleBackColor = true;
            // 
            // IMPORTAR
            // 
            this.IMPORTAR.AutoSize = true;
            this.IMPORTAR.Location = new System.Drawing.Point(236, 194);
            this.IMPORTAR.Name = "IMPORTAR";
            this.IMPORTAR.Size = new System.Drawing.Size(64, 17);
            this.IMPORTAR.TabIndex = 477;
            this.IMPORTAR.Text = "Importar";
            this.IMPORTAR.UseVisualStyleBackColor = true;
            // 
            // PROFORMA
            // 
            this.PROFORMA.AutoSize = true;
            this.PROFORMA.Location = new System.Drawing.Point(64, 240);
            this.PROFORMA.Name = "PROFORMA";
            this.PROFORMA.Size = new System.Drawing.Size(68, 17);
            this.PROFORMA.TabIndex = 478;
            this.PROFORMA.Text = "Proforma";
            this.PROFORMA.UseVisualStyleBackColor = true;
            // 
            // COMPROBANTE_PAGO
            // 
            this.COMPROBANTE_PAGO.AutoSize = true;
            this.COMPROBANTE_PAGO.Location = new System.Drawing.Point(137, 240);
            this.COMPROBANTE_PAGO.Name = "COMPROBANTE_PAGO";
            this.COMPROBANTE_PAGO.Size = new System.Drawing.Size(75, 17);
            this.COMPROBANTE_PAGO.TabIndex = 479;
            this.COMPROBANTE_PAGO.Text = "Retención";
            this.COMPROBANTE_PAGO.UseVisualStyleBackColor = true;
            // 
            // proformaConfiguracion
            // 
            this.proformaConfiguracion.AutoSize = true;
            this.proformaConfiguracion.Location = new System.Drawing.Point(64, 263);
            this.proformaConfiguracion.Name = "proformaConfiguracion";
            this.proformaConfiguracion.Size = new System.Drawing.Size(135, 17);
            this.proformaConfiguracion.TabIndex = 480;
            this.proformaConfiguracion.Text = "Proforma configuración";
            this.proformaConfiguracion.UseVisualStyleBackColor = true;
            // 
            // CartaPorte
            // 
            this.CartaPorte.AutoSize = true;
            this.CartaPorte.Location = new System.Drawing.Point(64, 286);
            this.CartaPorte.Name = "CartaPorte";
            this.CartaPorte.Size = new System.Drawing.Size(79, 17);
            this.CartaPorte.TabIndex = 481;
            this.CartaPorte.Text = "Carta Porte";
            this.CartaPorte.UseVisualStyleBackColor = true;
            // 
            // frmUsuario
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(321, 358);
            this.Controls.Add(this.CartaPorte);
            this.Controls.Add(this.proformaConfiguracion);
            this.Controls.Add(this.COMPROBANTE_PAGO);
            this.Controls.Add(this.PROFORMA);
            this.Controls.Add(this.IMPORTAR);
            this.Controls.Add(this.ADDENDA_INDIVIDUAL);
            this.Controls.Add(this.ADDENDA);
            this.Controls.Add(this.EXPORTAR);
            this.Controls.Add(this.PEDIMENTOS);
            this.Controls.Add(this.CAMBIAR);
            this.Controls.Add(this.VERIFICAR);
            this.Controls.Add(this.CANCELAR);
            this.Controls.Add(this.button7);
            this.Controls.Add(this.LECTURA);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.SERIE);
            this.Controls.Add(this.ADMINISTRADOR);
            this.Controls.Add(this.ACTIVO);
            this.Controls.Add(this.PASSWORD);
            this.Controls.Add(this.USUARIO);
            this.Controls.Add(this.APELLIDO);
            this.Controls.Add(this.NOMBRE);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.button3);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.ajax_loader);
            this.Controls.Add(this.statusStrip1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.KeyPreview = true;
            this.MaximizeBox = false;
            this.Name = "frmUsuario";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Usuario";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frmUsuario_FormClosing);
            this.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.frmUsuario_KeyPress);
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ajax_loader)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel1;
        private System.Windows.Forms.PictureBox ajax_loader;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox NOMBRE;
        private System.Windows.Forms.TextBox APELLIDO;
        private System.Windows.Forms.TextBox USUARIO;
        private System.Windows.Forms.TextBox PASSWORD;
        private System.Windows.Forms.CheckBox ACTIVO;
        private System.Windows.Forms.CheckBox ADMINISTRADOR;
        private System.Windows.Forms.TextBox SERIE;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.CheckBox LECTURA;
        private System.Windows.Forms.Button button7;
        private System.Windows.Forms.CheckBox CANCELAR;
        private System.Windows.Forms.CheckBox VERIFICAR;
        private System.Windows.Forms.CheckBox CAMBIAR;
        private System.Windows.Forms.CheckBox PEDIMENTOS;
        private System.Windows.Forms.CheckBox EXPORTAR;
        private System.Windows.Forms.CheckBox ADDENDA;
        private System.Windows.Forms.CheckBox ADDENDA_INDIVIDUAL;
        private System.Windows.Forms.CheckBox IMPORTAR;
        private System.Windows.Forms.CheckBox PROFORMA;
        private System.Windows.Forms.CheckBox COMPROBANTE_PAGO;
        private System.Windows.Forms.CheckBox proformaConfiguracion;
        private System.Windows.Forms.CheckBox CartaPorte;
    }
}