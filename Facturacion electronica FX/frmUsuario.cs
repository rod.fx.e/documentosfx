﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;

using System.Text;
using System.Windows.Forms;
using Generales;

namespace FE_FX
{
    public partial class frmUsuario : Form
    {
        private cCONEXCION oData = new cCONEXCION("");
        private cUSUARIO oObjeto;
        private bool modificado;

        public frmUsuario(string sConn)
        {
            oData.sConn = sConn;
            oObjeto = new cUSUARIO();
            InitializeComponent();
            ajax_loader.Visible = false;

            if (cConfiguracionGeneral.calcular_manualmente)
            {
                PROFORMA.Visible = false;
            }
            
        }
        public frmUsuario(string sConn, string ROW_IDp)
        {
            modificado = false;
            oData.sConn = sConn;
            oObjeto = new cUSUARIO();

            InitializeComponent();
            if (cConfiguracionGeneral.calcular_manualmente)
            {
                PROFORMA.Visible = false;
            }
            limpiar();
            oObjeto.ROW_ID = ROW_IDp;
            cargar();
        }

        public void limpiar()
        {
            if (modificado)
            {

                DialogResult Resultado = MessageBox.Show("¿Desea guardar los cambios?", Application.ProductName + "-" + Application.ProductVersion, MessageBoxButtons.YesNo);
                if (Resultado.ToString() == "Yes")
                {
                    guardar();
                }


            }
            modificado = false;
            oObjeto = new cUSUARIO();
            NOMBRE.Text = "";
            APELLIDO.Text = "";
            USUARIO.Text = "";
            PASSWORD.Text = "";
        }

        private void guardar()
        {
            ajax_loader.Visible = true;
            oObjeto.NOMBRE = NOMBRE.Text;
            oObjeto.APELLIDO = APELLIDO.Text;
            oObjeto.USUARIO = USUARIO.Text;
            oObjeto.PASSWORD = PASSWORD.Text;
            oObjeto.ACTIVO = ACTIVO.Checked.ToString();
            oObjeto.ADMINISTRADOR = ADMINISTRADOR.Checked.ToString();
            oObjeto.SERIE = SERIE.Text;
            oObjeto.LECTURA = LECTURA.Checked.ToString();

            oObjeto.CANCELAR = CANCELAR.Checked.ToString();
            oObjeto.VERIFICAR = VERIFICAR.Checked.ToString();
            oObjeto.CAMBIAR = CAMBIAR.Checked.ToString();
            oObjeto.PEDIMENTOS = PEDIMENTOS.Checked.ToString();
            oObjeto.EXPORTAR = EXPORTAR.Checked.ToString();
            oObjeto.IMPORTAR = IMPORTAR.Checked.ToString();
            oObjeto.ADDENDA = ADDENDA.Checked.ToString();
            oObjeto.ADDENDA_INDIVIDUAL = ADDENDA_INDIVIDUAL.Checked.ToString();
            oObjeto.PROFORMA = PROFORMA.Checked.ToString();
            oObjeto.COMPROBANTE_PAGO = COMPROBANTE_PAGO.Checked.ToString();

            oObjeto.proformaConfiguracion = proformaConfiguracion.Checked.ToString();


            oObjeto.CartaPorte = CartaPorte.Checked.ToString();


            if (oObjeto.guardar())
            {
                toolStripStatusLabel1.Text = "Datos Actualizados " + DateTime.Now.ToShortDateString() + " " + DateTime.Now.ToLongTimeString();
                modificado = false;
            }
            ajax_loader.Visible = false;
        }

        private void cargar()
        {
            ajax_loader.Visible = true;
            if (oObjeto.cargar(oObjeto.ROW_ID))
            {
                NOMBRE.Text = oObjeto.NOMBRE;
                APELLIDO.Text = oObjeto.APELLIDO;
                USUARIO.Text = oObjeto.USUARIO;
                PASSWORD.Text = oObjeto.PASSWORD;
                ACTIVO.Checked = bool.Parse(oObjeto.ACTIVO);
                ADMINISTRADOR.Checked = bool.Parse(oObjeto.ADMINISTRADOR);
                SERIE.Text = oObjeto.SERIE;
                LECTURA.Checked = bool.Parse(oObjeto.LECTURA);


                ACTIVO.Checked = bool.Parse(oObjeto.ACTIVO);
                ADMINISTRADOR.Checked = bool.Parse(oObjeto.ADMINISTRADOR);
                SERIE.Text = oObjeto.SERIE;
                LECTURA.Checked = bool.Parse(oObjeto.LECTURA);
                if (oObjeto.CANCELAR != "")
                {
                    CANCELAR.Checked = bool.Parse(oObjeto.CANCELAR);
                }
                if (oObjeto.VERIFICAR!="")
                {
                    VERIFICAR.Checked = bool.Parse(oObjeto.VERIFICAR);
                }
                if (oObjeto.CAMBIAR != "")
                {
                    CAMBIAR.Checked = bool.Parse(oObjeto.CAMBIAR);
                }
                if (oObjeto.PEDIMENTOS != "")
                {
                    PEDIMENTOS.Checked = bool.Parse(oObjeto.PEDIMENTOS);
                }
                if (oObjeto.EXPORTAR != "")
                {
                    EXPORTAR.Checked = bool.Parse(oObjeto.EXPORTAR);
                }
                if (oObjeto.IMPORTAR != "")
                {
                    IMPORTAR.Checked = bool.Parse(oObjeto.IMPORTAR);
                }
                if (oObjeto.ADDENDA != "")
                {
                    ADDENDA.Checked = bool.Parse(oObjeto.ADDENDA);
                }
                if (oObjeto.ADDENDA_INDIVIDUAL != "")
                {
                    ADDENDA_INDIVIDUAL.Checked = bool.Parse(oObjeto.ADDENDA_INDIVIDUAL);
                }
                if (oObjeto.PROFORMA != "")
                {
                    PROFORMA.Checked = bool.Parse(oObjeto.PROFORMA);
                }

                if (oObjeto.COMPROBANTE_PAGO != "")
                {
                    COMPROBANTE_PAGO.Checked = bool.Parse(oObjeto.COMPROBANTE_PAGO);
                }
                proformaConfiguracion.Checked = false;
                if (oObjeto.proformaConfiguracion != "")
                {
                    proformaConfiguracion.Checked = bool.Parse(oObjeto.proformaConfiguracion);
                }

                if (oObjeto.CartaPorte != "")
                {
                    CartaPorte.Checked = bool.Parse(oObjeto.CartaPorte);
                }

                toolStripStatusLabel1.Text  = "Datos Cargados " + DateTime.Now.ToShortDateString() + " " + DateTime.Now.ToLongTimeString();
                modificado = false;

            }
            ajax_loader.Visible = false;
        }

        private void button3_Click(object sender, EventArgs e)
        {
            guardar();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            limpiar();
        }

        private void frmUsuario_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (modificado)
            {
                DialogResult Resultado = MessageBox.Show("¿Desea guardar los cambios?", Application.ProductName + "-" + Application.ProductVersion, MessageBoxButtons.YesNo);
                if (Resultado.ToString() == "Yes")
                {
                    guardar();
                }
            }
        }

        private void frmUsuario_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)27)
            {
                this.Close();
            }
            else
            {
                modificado = true;
                if ((e.KeyChar.ToString() == "<")
                                | (e.KeyChar.ToString() == ">")
                                
                                | (e.KeyChar.ToString() == "'")
                                | (e.KeyChar.ToString() == "#")
                                )
                {
                    e.Handled = true;
                }
            }
        }

        private void button7_Click(object sender, EventArgs e)
        {
            eliminar();
        }

        private void eliminar()
        {
            DialogResult Resultado = MessageBox.Show("¿Esta seguro de eliminar?", Application.ProductName + "-" + Application.ProductVersion, MessageBoxButtons.YesNo);
            if (Resultado.ToString() != "Yes")
            {
                return;
            }
            if (oObjeto.eliminar(oObjeto.ROW_ID))
            {
                limpiar();
            }
        }
    }
}
