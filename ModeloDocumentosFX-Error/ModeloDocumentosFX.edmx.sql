
-- --------------------------------------------------
-- Entity Designer DDL Script for SQL Server 2005, 2008, 2012 and Azure
-- --------------------------------------------------
-- Date Created: 05/31/2018 13:30:02
-- Generated from EDMX file: D:\Proyectos\DocumentosFX\SAT\ImportarCatalogos\ModeloDocumentosFX\ModeloDocumentosFX.edmx
-- --------------------------------------------------

SET QUOTED_IDENTIFIER OFF;
GO
USE [documentosfx];
GO
IF SCHEMA_ID(N'dbo') IS NULL EXECUTE(N'CREATE SCHEMA [dbo]');
GO

-- --------------------------------------------------
-- Dropping existing FOREIGN KEY constraints
-- --------------------------------------------------

IF OBJECT_ID(N'[dbo].[FK_socioNegocionotificacion]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[notificacionSet] DROP CONSTRAINT [FK_socioNegocionotificacion];
GO
IF OBJECT_ID(N'[dbo].[FK_socioNegocioconfiguracion]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[configuracionNotificacionSet] DROP CONSTRAINT [FK_socioNegocioconfiguracion];
GO
IF OBJECT_ID(N'[dbo].[FK_socioNegociodocumento]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[documentoSet] DROP CONSTRAINT [FK_socioNegociodocumento];
GO

IF OBJECT_ID(N'[dbo].[FK_socioNegociousuario]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[usuarioSocioNegocioSet] DROP CONSTRAINT [FK_socioNegociousuario];
GO
IF OBJECT_ID(N'[dbo].[FK_usuariodispositivo]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[dispositivoSet] DROP CONSTRAINT [FK_usuariodispositivo];
GO
IF OBJECT_ID(N'[dbo].[FK_socioNegocioconfiguracionConexion]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[configuracionConexionSet] DROP CONSTRAINT [FK_socioNegocioconfiguracionConexion];
GO
IF OBJECT_ID(N'[dbo].[FK_socioNegocioconfiguracionBanco]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[configuracionBancoSet] DROP CONSTRAINT [FK_socioNegocioconfiguracionBanco];
GO
IF OBJECT_ID(N'[dbo].[FK_depositodepositoLinea]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[depositoLineaSet] DROP CONSTRAINT [FK_depositodepositoLinea];
GO

-- --------------------------------------------------
-- Dropping existing tables
-- --------------------------------------------------

IF OBJECT_ID(N'[dbo].[socioNegocioSet]', 'U') IS NOT NULL
    DROP TABLE [dbo].[socioNegocioSet];
GO
IF OBJECT_ID(N'[dbo].[documentoSet]', 'U') IS NOT NULL
    DROP TABLE [dbo].[documentoSet];
GO
IF OBJECT_ID(N'[dbo].[notificacionSet]', 'U') IS NOT NULL
    DROP TABLE [dbo].[notificacionSet];
GO
IF OBJECT_ID(N'[dbo].[configuracionNotificacionSet]', 'U') IS NOT NULL
    DROP TABLE [dbo].[configuracionNotificacionSet];
GO
IF OBJECT_ID(N'[dbo].[usuarioSocioNegocioSet]', 'U') IS NOT NULL
    DROP TABLE [dbo].[usuarioSocioNegocioSet];
GO
IF OBJECT_ID(N'[dbo].[dispositivoSet]', 'U') IS NOT NULL
    DROP TABLE [dbo].[dispositivoSet];
GO
IF OBJECT_ID(N'[dbo].[usuariosSet]', 'U') IS NOT NULL
    DROP TABLE [dbo].[usuariosSet];
GO
IF OBJECT_ID(N'[dbo].[configuracionConexionSet]', 'U') IS NOT NULL
    DROP TABLE [dbo].[configuracionConexionSet];
GO
IF OBJECT_ID(N'[dbo].[configuracionProductoSet]', 'U') IS NOT NULL
    DROP TABLE [dbo].[configuracionProductoSet];
GO
IF OBJECT_ID(N'[dbo].[configuracionIncumplidosSet]', 'U') IS NOT NULL
    DROP TABLE [dbo].[configuracionIncumplidosSet];
GO
IF OBJECT_ID(N'[dbo].[configuracionBancoSet]', 'U') IS NOT NULL
    DROP TABLE [dbo].[configuracionBancoSet];
GO
IF OBJECT_ID(N'[dbo].[VMX_FE]', 'U') IS NOT NULL
    DROP TABLE [dbo].[VMX_FE];
GO
IF OBJECT_ID(N'[dbo].[depositoSet]', 'U') IS NOT NULL
    DROP TABLE [dbo].[depositoSet];
GO
IF OBJECT_ID(N'[dbo].[depositoLineaSet]', 'U') IS NOT NULL
    DROP TABLE [dbo].[depositoLineaSet];
GO
IF OBJECT_ID(N'[dbo].[configuracionCCESet]', 'U') IS NOT NULL
    DROP TABLE [dbo].[configuracionCCESet];
GO
IF OBJECT_ID(N'[dbo].[empresaLocalSet]', 'U') IS NOT NULL
    DROP TABLE [dbo].[empresaLocalSet];
GO
IF OBJECT_ID(N'[dbo].[configuracionUMSet]', 'U') IS NOT NULL
    DROP TABLE [dbo].[configuracionUMSet];
GO
IF OBJECT_ID(N'[dbo].[configuracionFraccionArancelariaSet]', 'U') IS NOT NULL
    DROP TABLE [dbo].[configuracionFraccionArancelariaSet];
GO
IF OBJECT_ID(N'[dbo].[ndcFacturaSet]', 'U') IS NOT NULL
    DROP TABLE [dbo].[ndcFacturaSet];
GO
IF OBJECT_ID(N'[dbo].[CFDI_USUARIO]', 'U') IS NOT NULL
    DROP TABLE [dbo].[CFDI_USUARIO];
GO
IF OBJECT_ID(N'[dbo].[EMPRESA]', 'U') IS NOT NULL
    DROP TABLE [dbo].[EMPRESA];
GO
IF OBJECT_ID(N'[dbo].[ndcFacturaLineaSet]', 'U') IS NOT NULL
    DROP TABLE [dbo].[ndcFacturaLineaSet];
GO
IF OBJECT_ID(N'[dbo].[cxpConciliacionSet]', 'U') IS NOT NULL
    DROP TABLE [dbo].[cxpConciliacionSet];
GO
IF OBJECT_ID(N'[dbo].[configuracionUMproductoSet]', 'U') IS NOT NULL
    DROP TABLE [dbo].[configuracionUMproductoSet];
GO
IF OBJECT_ID(N'[dbo].[comprobantePagoSet]', 'U') IS NOT NULL
    DROP TABLE [dbo].[comprobantePagoSet];
GO

-- --------------------------------------------------
-- Creating all tables
-- --------------------------------------------------

-- Creating table 'socioNegocioSet'
CREATE TABLE [dbo].[socioNegocioSet] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [rfc] nvarchar(13)  NOT NULL,
    [razon] nvarchar(max)  NOT NULL,
    [password] nvarchar(max)  NOT NULL,
    [correoElectronico] nvarchar(max)  NOT NULL,
    [estado] nvarchar(max)  NOT NULL
);
GO

-- Creating table 'documentoSet'
CREATE TABLE [dbo].[documentoSet] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [uuid] nvarchar(60)  NOT NULL,
    [tipoComprobante] nvarchar(60)  NOT NULL,
    [fechaCreacion] nvarchar(max)  NOT NULL,
    [uuidPadre] nvarchar(60)  NOT NULL,
    [voucher] nvarchar(60)  NOT NULL,
    [fechaFactura] datetime  NOT NULL,
    [fechaAutorizacion] datetime  NOT NULL,
    [fechaEstimada] datetime  NOT NULL,
    [fechaIngreso] datetime  NOT NULL,
    [oc] nvarchar(40)  NOT NULL,
    [pedido] nvarchar(40)  NOT NULL,
    [estado] nvarchar(max)  NULL,
    [total] decimal(16,4)  NOT NULL,
    [moneda] nvarchar(15)  NOT NULL,
    [factura] nvarchar(100)  NOT NULL,
    [vendorId] nvarchar(30)  NOT NULL,
    [rfc] nvarchar(30)  NOT NULL,
    [proceso] tinyint  NULL,
    [rfcEmisor_Id] int  NOT NULL,
    [rfcReceptor_Id] int  NOT NULL
);
GO

-- Creating table 'notificacionSet'
CREATE TABLE [dbo].[notificacionSet] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [mensaje] nvarchar(max)  NOT NULL,
    [asunto] nvarchar(max)  NOT NULL,
    [fechaCreacion] datetime  NULL,
    [fechaEnvio] datetime  NULL,
    [socioNegocio_Id] int  NOT NULL
);
GO

-- Creating table 'configuracionNotificacionSet'
CREATE TABLE [dbo].[configuracionNotificacionSet] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [bienvenidaMensaje] nvarchar(max)  NOT NULL,
    [bienvenidaAsunto] nvarchar(max)  NOT NULL,
    [aceptacionAsunto] nvarchar(max)  NOT NULL,
    [aceptacionMensaje] nvarchar(max)  NOT NULL,
    [rechazoMensaje] nvarchar(max)  NOT NULL,
    [rechazoAsunto] nvarchar(max)  NOT NULL,
    [validacionMensaje] nvarchar(max)  NOT NULL,
    [validacionAsunto] nvarchar(max)  NOT NULL,
    [autorizacionAsunto] nvarchar(max)  NOT NULL,
    [autorizacionMensaje] nvarchar(max)  NOT NULL,
    [recuperacionMensaje] nvarchar(max)  NOT NULL,
    [recuperacionAsunto] nvarchar(max)  NOT NULL,
    [socioNegocio_Id] int  NOT NULL
);
GO

-- Creating table 'usuarioSocioNegocioSet'
CREATE TABLE [dbo].[usuarioSocioNegocioSet] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [correoElectronico] nvarchar(max)  NOT NULL,
    [password] nvarchar(max)  NOT NULL,
    [estado] nvarchar(max)  NOT NULL,
    [socioNegocio_Id] int  NOT NULL
);
GO

-- Creating table 'dispositivoSet'
CREATE TABLE [dbo].[dispositivoSet] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [deviceToken] nvarchar(max)  NOT NULL,
    [os] nvarchar(max)  NOT NULL,
    [usuario_Id] int  NOT NULL
);
GO

-- Creating table 'usuariosSet'
CREATE TABLE [dbo].[usuariosSet] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [nombreCompleto] nvarchar(250)  NOT NULL,
    [usuario] nvarchar(20)  NOT NULL,
    [usuarioVISUAL] nvarchar(20)  NOT NULL,
    [password] nvarchar(20)  NOT NULL,
    [permisoDocumentosSolo] bit  NOT NULL,
    [permisoCuentas] bit  NOT NULL,
    [notificacionCorreo] bit  NOT NULL,
    [notificacionREQ] bit  NOT NULL,
    [notificacionOC] bit  NOT NULL,
    [notificacionREC] bit  NOT NULL,
    [notificacionCXP] bit  NOT NULL,
    [notificacionPAG] bit  NOT NULL,
    [notificacionBAN] bit  NOT NULL,
    [notificacionGJ] bit  NOT NULL,
    [permisoAcceso] bit  NOT NULL,
    [permisoUsuarios] bit  NOT NULL,
    [correoElectronico] nvarchar(150)  NOT NULL,
    [facturacionElectronica] bit  NOT NULL,
    [contabilidadElectronica] bit  NOT NULL,
    [cuentasCobrar] bit  NOT NULL,
    [cuentasPagar] bit  NOT NULL,
    [configuracion] bit  NOT NULL
);
GO

-- Creating table 'configuracionConexionSet'
CREATE TABLE [dbo].[configuracionConexionSet] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [tipo] nvarchar(100)  NOT NULL,
    [servidor] nvarchar(100)  NOT NULL,
    [baseDatos] nvarchar(100)  NOT NULL,
    [usuario] nvarchar(100)  NOT NULL,
    [password] nvarchar(100)  NOT NULL,
    [visualSite] nvarchar(100)  NULL,
    [visualEntity] nvarchar(100)  NULL,
    [visualBDAuxiliar] nvarchar(100)  NULL,
    [socioNegocio_Id] int  NOT NULL
);
GO

-- Creating table 'configuracionProductoSet'
CREATE TABLE [dbo].[configuracionProductoSet] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [fechaCreacion] datetime  NOT NULL,
    [producto] nvarchar(max)  NOT NULL,
    [clasificacionSAT] nvarchar(max)  NOT NULL,
    [unidadSAT] nvarchar(50)  NULL
);
GO

-- Creating table 'configuracionIncumplidosSet'
CREATE TABLE [dbo].[configuracionIncumplidosSet] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [rfc] nvarchar(max)  NOT NULL,
    [fechaCreacion] datetime  NOT NULL,
    [nombre] nvarchar(max)  NOT NULL
);
GO

-- Creating table 'configuracionBancoSet'
CREATE TABLE [dbo].[configuracionBancoSet] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [banco] nvarchar(100)  NOT NULL,
    [CLABE] nvarchar(100)  NOT NULL,
    [cuenta] nvarchar(100)  NOT NULL,
    [socioNegocio_Id] int  NOT NULL
);
GO

-- Creating table 'VMX_FE'
CREATE TABLE [dbo].[VMX_FE] (
    [ROW_ID] bigint IDENTITY(1,1) NOT NULL,
    [INVOICE_ID] varchar(30)  NULL,
    [SELLO] varchar(max)  NULL,
    [PDF] varchar(250)  NULL,
    [XML] varchar(250)  NULL,
    [SERIE] varchar(250)  NULL,
    [FORMATO] varchar(250)  NULL,
    [PEDIMENTO] varchar(50)  NULL,
    [FECHA_PEDIMENTO] datetime  NULL,
    [ADUANA] varchar(50)  NULL,
    [CREATE_DATE] datetime  NULL,
    [UPDATE_DATE] datetime  NULL,
    [FORMA_DE_PAGO] nvarchar(max)  NULL,
    [METODO_DE_PAGO] nvarchar(max)  NULL,
    [CTA_BANCO] nvarchar(max)  NULL,
    [UUID] varchar(max)  NULL,
    [FechaTimbrado] varchar(max)  NULL,
    [noCertificadoSAT] varchar(max)  NULL,
    [selloSAT] varchar(max)  NULL,
    [Cadena_TFD] varchar(max)  NULL,
    [QR_Code] varchar(max)  NULL,
    [IMAGEN_QR_Code] varbinary(max)  NULL,
    [CADENA] varchar(max)  NULL,
    [ESTADO] varchar(max)  NULL,
    [CANCELADO] varchar(max)  NULL,
    [noCertificado] varchar(max)  NULL,
    [ADDR_NO] varchar(max)  NULL,
    [PEDIMENTOS] varchar(max)  NULL,
    [PDF_ESTADO] varchar(max)  NULL
);
GO

-- Creating table 'depositoSet'
CREATE TABLE [dbo].[depositoSet] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [fechaCreacion] datetime  NOT NULL,
    [fechaDeposito] nvarchar(max)  NOT NULL,
    [banco] nvarchar(max)  NOT NULL,
    [archivo] nvarchar(max)  NOT NULL,
    [estado] nvarchar(max)  NOT NULL,
    [usuarioCreador] nvarchar(max)  NOT NULL,
    [VISUALid] nvarchar(max)  NOT NULL
);
GO

-- Creating table 'depositoLineaSet'
CREATE TABLE [dbo].[depositoLineaSet] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [factura] datetime  NOT NULL,
    [monto] nvarchar(max)  NOT NULL,
    [deposito_Id] int  NOT NULL
);
GO

-- Creating table 'configuracionCCESet'
CREATE TABLE [dbo].[configuracionCCESet] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [emtityId] nvarchar(max)  NULL,
    [excepcionCliente] nvarchar(max)  NULL,
    [excepcionProduct] nvarchar(max)  NULL,
    [excepcionArticulo] nvarchar(max)  NULL,
    [UDFVMPRTMNT] nvarchar(max)  NULL
);
GO

-- Creating table 'empresaLocalSet'
CREATE TABLE [dbo].[empresaLocalSet] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [rfc] nvarchar(13)  NOT NULL,
    [razon] nvarchar(max)  NOT NULL,
    [estado] nvarchar(max)  NOT NULL,
    [regimenFiscal] nvarchar(max)  NULL
);
GO

-- Creating table 'configuracionUMSet'
CREATE TABLE [dbo].[configuracionUMSet] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [UnidadMedida] nvarchar(50)  NOT NULL,
    [SAT] nvarchar(50)  NULL,
    [CE] nvarchar(50)  NULL,
    [FactorCE] decimal(16,8)  NOT NULL
);
GO

-- Creating table 'configuracionFraccionArancelariaSet'
CREATE TABLE [dbo].[configuracionFraccionArancelariaSet] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [fraccionArancelaria] nvarchar(50)  NOT NULL,
    [UnidadMedad] nvarchar(50)  NULL
);
GO

-- Creating table 'ndcFacturaSet'
CREATE TABLE [dbo].[ndcFacturaSet] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [tipoRelacion] nvarchar(50)  NULL,
    [INVOICE_ID] nvarchar(50)  NULL,
    [UUID] nvarchar(100)  NULL,
    [INVOICE_ID_relacionada] nvarchar(100)  NULL
);
GO

-- Creating table 'CFDI_USUARIO'
CREATE TABLE [dbo].[CFDI_USUARIO] (
    [ROW_ID] int  NOT NULL,
    [USUARIO] nvarchar(max)  NULL,
    [PASSWORD] nvarchar(max)  NULL,
    [TIPO] nvarchar(max)  NULL,
    [ROW_ID_EMPRESA] float  NULL,
    [USUARIO_PRUEBA] nvarchar(max)  NULL,
    [PASSWORD_PRUEBA] nvarchar(max)  NULL
);
GO

-- Creating table 'EMPRESA'
CREATE TABLE [dbo].[EMPRESA] (
    [ROW_ID] int  NOT NULL,
    [ENTITY_ID] nvarchar(255)  NULL,
    [ID] nvarchar(255)  NULL,
    [RFC] nvarchar(255)  NULL,
    [CALLE] nvarchar(255)  NULL,
    [EXTERIOR] nvarchar(255)  NULL,
    [INTERIOR] nvarchar(255)  NULL,
    [COLONIA] nvarchar(255)  NULL,
    [CP] nvarchar(255)  NULL,
    [LOCALIDAD] nvarchar(255)  NULL,
    [REFERENCIA] nvarchar(255)  NULL,
    [PAIS] nvarchar(255)  NULL,
    [ESTADO] nvarchar(255)  NULL,
    [MUNICIPIO] nvarchar(255)  NULL,
    [SMTP] nvarchar(255)  NULL,
    [USUARIO] nvarchar(255)  NULL,
    [PASSWORD_EMAIL] nvarchar(255)  NULL,
    [PUERTO] nvarchar(255)  NULL,
    [SSL] nvarchar(255)  NULL,
    [CRENDENCIALES] nvarchar(255)  NULL,
    [ACTIVO] nvarchar(max)  NULL,
    [REGIMEN_FISCAL] nvarchar(max)  NULL,
    [TIPO] nvarchar(255)  NULL,
    [SERVIDOR] nvarchar(255)  NULL,
    [BD] nvarchar(255)  NULL,
    [USUARIO_BD] nvarchar(255)  NULL,
    [PASSWORD_BD] nvarchar(255)  NULL,
    [CUSTOMER_ID] nvarchar(max)  NULL,
    [ENVIO_CORREO] nvarchar(max)  NULL,
    [SITE_ID] nvarchar(max)  NULL,
    [BD_AUXILIAR] nvarchar(max)  NULL,
    [DESCUENTO_LINEA] nvarchar(max)  NULL,
    [CUSTOMER_ESPECIFICACION_LINEA] nvarchar(max)  NULL,
    [IMPRESION_AUTOMATICA] nvarchar(max)  NULL,
    [IMPRESORA] nvarchar(max)  NULL,
    [IMPRESION_AUTOMATICA_NDC] nvarchar(max)  NULL,
    [ACTIVAR_COMPLEMENTO_CCE] nvarchar(max)  NULL,
    [APROXIMACION] nvarchar(max)  NULL,
    [VALORES_DECIMALES] nvarchar(max)  NULL,
    [relacionLinea] nvarchar(10)  NOT NULL
);
GO

-- Creating table 'ndcFacturaLineaSet'
CREATE TABLE [dbo].[ndcFacturaLineaSet] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [INVOICE_ID] nvarchar(50)  NULL,
    [LINE_NO] nvarchar(50)  NULL,
    [invoice_id_relacionado] nvarchar(50)  NULL,
    [invoice_id_relacionado_linea] nvarchar(50)  NULL,
    [PART_ID] nvarchar(50)  NULL,
    [clasificacion] nvarchar(50)  NULL
);
GO

-- Creating table 'cxpConciliacionSet'
CREATE TABLE [dbo].[cxpConciliacionSet] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [uuid] nvarchar(max)  NOT NULL,
    [fechaCreacion] datetime  NOT NULL,
    [oc] nvarchar(50)  NULL,
    [usuario] nvarchar(max)  NULL,
    [voucherId] nvarchar(100)  NULL,
    [rfcEmisor] nvarchar(100)  NULL,
    [tipoDocumento] nvarchar(max)  NULL
);
GO

-- Creating table 'configuracionUMproductoSet'
CREATE TABLE [dbo].[configuracionUMproductoSet] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [producto] nvarchar(50)  NOT NULL,
    [UnidadMedida] nvarchar(50)  NOT NULL,
    [SAT] nvarchar(50)  NULL,
    [CE] nvarchar(50)  NULL,
    [FactorCE] decimal(16,8)  NOT NULL
);
GO

-- Creating table 'comprobantePagoSet'
CREATE TABLE [dbo].[comprobantePagoSet] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [rfcEmisor] nvarchar(15)  NOT NULL,
    [rfcReceptor] nvarchar(15)  NOT NULL
);
GO

-- --------------------------------------------------
-- Creating all PRIMARY KEY constraints
-- --------------------------------------------------

-- Creating primary key on [Id] in table 'socioNegocioSet'
ALTER TABLE [dbo].[socioNegocioSet]
ADD CONSTRAINT [PK_socioNegocioSet]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'documentoSet'
ALTER TABLE [dbo].[documentoSet]
ADD CONSTRAINT [PK_documentoSet]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'notificacionSet'
ALTER TABLE [dbo].[notificacionSet]
ADD CONSTRAINT [PK_notificacionSet]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'configuracionNotificacionSet'
ALTER TABLE [dbo].[configuracionNotificacionSet]
ADD CONSTRAINT [PK_configuracionNotificacionSet]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'usuarioSocioNegocioSet'
ALTER TABLE [dbo].[usuarioSocioNegocioSet]
ADD CONSTRAINT [PK_usuarioSocioNegocioSet]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'dispositivoSet'
ALTER TABLE [dbo].[dispositivoSet]
ADD CONSTRAINT [PK_dispositivoSet]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'usuariosSet'
ALTER TABLE [dbo].[usuariosSet]
ADD CONSTRAINT [PK_usuariosSet]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'configuracionConexionSet'
ALTER TABLE [dbo].[configuracionConexionSet]
ADD CONSTRAINT [PK_configuracionConexionSet]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'configuracionProductoSet'
ALTER TABLE [dbo].[configuracionProductoSet]
ADD CONSTRAINT [PK_configuracionProductoSet]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'configuracionIncumplidosSet'
ALTER TABLE [dbo].[configuracionIncumplidosSet]
ADD CONSTRAINT [PK_configuracionIncumplidosSet]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'configuracionBancoSet'
ALTER TABLE [dbo].[configuracionBancoSet]
ADD CONSTRAINT [PK_configuracionBancoSet]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [ROW_ID] in table 'VMX_FE'
ALTER TABLE [dbo].[VMX_FE]
ADD CONSTRAINT [PK_VMX_FE]
    PRIMARY KEY CLUSTERED ([ROW_ID] ASC);
GO

-- Creating primary key on [Id] in table 'depositoSet'
ALTER TABLE [dbo].[depositoSet]
ADD CONSTRAINT [PK_depositoSet]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'depositoLineaSet'
ALTER TABLE [dbo].[depositoLineaSet]
ADD CONSTRAINT [PK_depositoLineaSet]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'configuracionCCESet'
ALTER TABLE [dbo].[configuracionCCESet]
ADD CONSTRAINT [PK_configuracionCCESet]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'empresaLocalSet'
ALTER TABLE [dbo].[empresaLocalSet]
ADD CONSTRAINT [PK_empresaLocalSet]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'configuracionUMSet'
ALTER TABLE [dbo].[configuracionUMSet]
ADD CONSTRAINT [PK_configuracionUMSet]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'configuracionFraccionArancelariaSet'
ALTER TABLE [dbo].[configuracionFraccionArancelariaSet]
ADD CONSTRAINT [PK_configuracionFraccionArancelariaSet]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'ndcFacturaSet'
ALTER TABLE [dbo].[ndcFacturaSet]
ADD CONSTRAINT [PK_ndcFacturaSet]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [ROW_ID] in table 'CFDI_USUARIO'
ALTER TABLE [dbo].[CFDI_USUARIO]
ADD CONSTRAINT [PK_CFDI_USUARIO]
    PRIMARY KEY CLUSTERED ([ROW_ID] ASC);
GO

-- Creating primary key on [ROW_ID] in table 'EMPRESA'
ALTER TABLE [dbo].[EMPRESA]
ADD CONSTRAINT [PK_EMPRESA]
    PRIMARY KEY CLUSTERED ([ROW_ID] ASC);
GO

-- Creating primary key on [Id] in table 'ndcFacturaLineaSet'
ALTER TABLE [dbo].[ndcFacturaLineaSet]
ADD CONSTRAINT [PK_ndcFacturaLineaSet]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'cxpConciliacionSet'
ALTER TABLE [dbo].[cxpConciliacionSet]
ADD CONSTRAINT [PK_cxpConciliacionSet]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'configuracionUMproductoSet'
ALTER TABLE [dbo].[configuracionUMproductoSet]
ADD CONSTRAINT [PK_configuracionUMproductoSet]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'comprobantePagoSet'
ALTER TABLE [dbo].[comprobantePagoSet]
ADD CONSTRAINT [PK_comprobantePagoSet]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- --------------------------------------------------
-- Creating all FOREIGN KEY constraints
-- --------------------------------------------------

-- Creating foreign key on [socioNegocio_Id] in table 'notificacionSet'
ALTER TABLE [dbo].[notificacionSet]
ADD CONSTRAINT [FK_socioNegocionotificacion]
    FOREIGN KEY ([socioNegocio_Id])
    REFERENCES [dbo].[socioNegocioSet]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_socioNegocionotificacion'
CREATE INDEX [IX_FK_socioNegocionotificacion]
ON [dbo].[notificacionSet]
    ([socioNegocio_Id]);
GO

-- Creating foreign key on [socioNegocio_Id] in table 'configuracionNotificacionSet'
ALTER TABLE [dbo].[configuracionNotificacionSet]
ADD CONSTRAINT [FK_socioNegocioconfiguracion]
    FOREIGN KEY ([socioNegocio_Id])
    REFERENCES [dbo].[socioNegocioSet]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_socioNegocioconfiguracion'
CREATE INDEX [IX_FK_socioNegocioconfiguracion]
ON [dbo].[configuracionNotificacionSet]
    ([socioNegocio_Id]);
GO

-- Creating foreign key on [rfcEmisor_Id] in table 'documentoSet'
ALTER TABLE [dbo].[documentoSet]
ADD CONSTRAINT [FK_socioNegociodocumento]
    FOREIGN KEY ([rfcEmisor_Id])
    REFERENCES [dbo].[socioNegocioSet]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_socioNegociodocumento'
CREATE INDEX [IX_FK_socioNegociodocumento]
ON [dbo].[documentoSet]
    ([rfcEmisor_Id]);
GO


-- Creating foreign key on [socioNegocio_Id] in table 'usuarioSocioNegocioSet'
ALTER TABLE [dbo].[usuarioSocioNegocioSet]
ADD CONSTRAINT [FK_socioNegociousuario]
    FOREIGN KEY ([socioNegocio_Id])
    REFERENCES [dbo].[socioNegocioSet]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_socioNegociousuario'
CREATE INDEX [IX_FK_socioNegociousuario]
ON [dbo].[usuarioSocioNegocioSet]
    ([socioNegocio_Id]);
GO

-- Creating foreign key on [usuario_Id] in table 'dispositivoSet'
ALTER TABLE [dbo].[dispositivoSet]
ADD CONSTRAINT [FK_usuariodispositivo]
    FOREIGN KEY ([usuario_Id])
    REFERENCES [dbo].[usuarioSocioNegocioSet]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_usuariodispositivo'
CREATE INDEX [IX_FK_usuariodispositivo]
ON [dbo].[dispositivoSet]
    ([usuario_Id]);
GO

-- Creating foreign key on [socioNegocio_Id] in table 'configuracionConexionSet'
ALTER TABLE [dbo].[configuracionConexionSet]
ADD CONSTRAINT [FK_socioNegocioconfiguracionConexion]
    FOREIGN KEY ([socioNegocio_Id])
    REFERENCES [dbo].[socioNegocioSet]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_socioNegocioconfiguracionConexion'
CREATE INDEX [IX_FK_socioNegocioconfiguracionConexion]
ON [dbo].[configuracionConexionSet]
    ([socioNegocio_Id]);
GO

-- Creating foreign key on [socioNegocio_Id] in table 'configuracionBancoSet'
ALTER TABLE [dbo].[configuracionBancoSet]
ADD CONSTRAINT [FK_socioNegocioconfiguracionBanco]
    FOREIGN KEY ([socioNegocio_Id])
    REFERENCES [dbo].[socioNegocioSet]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_socioNegocioconfiguracionBanco'
CREATE INDEX [IX_FK_socioNegocioconfiguracionBanco]
ON [dbo].[configuracionBancoSet]
    ([socioNegocio_Id]);
GO

-- Creating foreign key on [deposito_Id] in table 'depositoLineaSet'
ALTER TABLE [dbo].[depositoLineaSet]
ADD CONSTRAINT [FK_depositodepositoLinea]
    FOREIGN KEY ([deposito_Id])
    REFERENCES [dbo].[depositoSet]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_depositodepositoLinea'
CREATE INDEX [IX_FK_depositodepositoLinea]
ON [dbo].[depositoLineaSet]
    ([deposito_Id]);
GO

-- --------------------------------------------------
-- Script has ended
-- --------------------------------------------------