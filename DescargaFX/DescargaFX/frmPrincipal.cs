﻿using DescargaFX.Class;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Security.Cryptography.X509Certificates;
using System.Web;

namespace DescargaFX
{
    public partial class frmPrincipal : Form
    {
        static byte[] pfx = File.ReadAllBytes(@"EAUR810302714.pfx");
        static string password = "abc123";

        static string urlAutentica = "https://cfdidescargamasivasolicitud.clouda.sat.gob.mx/Autenticacion/Autenticacion.svc";
        static string urlAutenticaAction = "http://DescargaMasivaTerceros.gob.mx/IAutenticacion/Autentica";

        static string urlSolicitud = "https://cfdidescargamasivasolicitud.clouda.sat.gob.mx/SolicitaDescargaService.svc";
        static string urlSolicitudAction = "http://DescargaMasivaTerceros.sat.gob.mx/ISolicitaDescargaService/SolicitaDescarga";

        static string urlVerificarSolicitud = "https://cfdidescargamasivasolicitud.clouda.sat.gob.mx/VerificaSolicitudDescargaService.svc";
        static string urlVerificarSolicitudAction = "http://DescargaMasivaTerceros.sat.gob.mx/IVerificaSolicitudDescargaService/VerificaSolicitudDescarga";

        static string urlDescargarSolicitud = "https://cfdidescargamasiva.clouda.sat.gob.mx/DescargaMasivaService.svc";
        static string urlDescargarSolicitudAction = "http://DescargaMasivaTerceros.sat.gob.mx/IDescargaMasivaTercerosService/Descargar";

        //static string RfcEmisor = "EAUR810302714";
        //static string RfcReceptor = "CJP100730T39";
        //static string FechaInicial = "2020-10-01";
        //static string FechaFinal = "2020-10-31";
        X509Certificate2 certifcate;
        public frmPrincipal()
        {
            InitializeComponent();
            //Obtener Certificados
            certifcate = ObtenerX509Certificado(pfx);
            autentificar();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            solicitar();
        }

        private void autentificar()
        {
            
            //Obtener Token
            string token = ObtenerToken(certifcate);
            autorization.Text = String.Format("WRAP access_token=\"{0}\"", System.Net.WebUtility.UrlDecode(token));
            Console.WriteLine("Token: " + token);
        }

        private void solicitar()
        {
            //Validar Solicitud
            ValidarSolicitud(certifcate, autorization.Text, idSolicitud.Text);


        }

        private static X509Certificate2 ObtenerX509Certificado(byte[] pfx)
        {
            return new X509Certificate2(pfx, password, X509KeyStorageFlags.MachineKeySet |
                            X509KeyStorageFlags.PersistKeySet |
                            X509KeyStorageFlags.Exportable);
        }

        private static void GuardarSolicitud(string idPaquete, string descargaResponse)
        {
            string path = "./Paquetes/";
            byte[] file = Convert.FromBase64String(descargaResponse);
            Directory.CreateDirectory(path);

            using (FileStream fs = File.Create(path + idPaquete + ".zip", file.Length))
            {
                fs.Write(file, 0, file.Length);
            }
            Console.WriteLine("FileCreated: " + path + idPaquete + ".gzip");
        }

        private string DescargarSolicitud(X509Certificate2 certifcate, string autorization, string idPaquete)
        {
            DescargarSolicitud descargarSolicitud = new DescargarSolicitud(urlDescargarSolicitud, urlDescargarSolicitudAction);
            string xmlDescarga = descargarSolicitud.Generate(certifcate, RfcSolicitante.Text, idPaquete);
            return descargarSolicitud.Send(autorization);
        }

        private void ValidarSolicitud(X509Certificate2 certifcate, string autorization, string idSolicitud)
        {
            solicitudInformacion.Text = "Espere mientras se procesa....";
            VerificaSolicitud verifica = new VerificaSolicitud(urlVerificarSolicitud, urlVerificarSolicitudAction);
            verifica.Generate(certifcate, RfcSolicitante.Text, idSolicitud);
            verifica.Send(autorization);
            solicitudInformacion.Text = DateTime.Now.ToLongTimeString() + Environment.NewLine + "EstadoSolicitud: " + verifica.EstadoSolicitud.ToString() + " " 
                +"CodEstatus: " + verifica.CodEstatus.ToString() + " " 
                +"CodigoEstadoSolicitud: " + verifica.CodigoEstadoSolicitud.ToString() + " " 
                +"NumeroCFDIs: " + verifica.NumeroCFDIs.ToString() + Environment.NewLine 
                +"Mensaje: " + verifica.Mensaje.ToString();
                
                ;
            if (!string.IsNullOrEmpty(verifica.paquete))
            {
                idPaquete.Text = verifica.paquete;
            }
        }

        private string GenerarSolicitud(X509Certificate2 certifcate, string autorization)
        {
            this.solicitudError.Text = "";

            Solicitud solicitud = new Solicitud(urlSolicitud, urlSolicitudAction);
            string xmlSolicitud = solicitud.Generate(certifcate, RfcEmisor.Text, RfcReceptor.Text
                , RfcSolicitante.Text, FechaInicial.Value.ToString("yyyy-MM-dd")
                , FechaFinal.Value.ToString("yyyy-MM-dd"));
            string solicitudTr=solicitud.Send(autorization);

            if (!String.IsNullOrEmpty(solicitudTr))
            {
                this.idSolicitud.Text = solicitudTr;
            }
            else
            {
                this.solicitudError.Text = "CodEstatus: " + solicitud.CodEstatus + Environment.NewLine + "Mensaje: " + solicitud.Mensaje;
            }

            return solicitudTr;
        }

        private static string ObtenerToken(X509Certificate2 certifcate)
        {
            Autenticacion service = new Autenticacion(urlAutentica, urlAutenticaAction);
            string xml = service.Generate(certifcate);
            return service.Send();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            autentificar();
        }

        private void button4_Click(object sender, EventArgs e)
        {
            generarSolicitud();
        }

        private void generarSolicitud()
        {
            //Generar Solicitud
            this.idSolicitud.Text = "";
            string idSolicitud = GenerarSolicitud(certifcate, autorization.Text);

            Console.WriteLine("IdSolicitud: " + idSolicitud);
        }

        private void button3_Click(object sender, EventArgs e)
        {
            descargarPaquete();
        }

        private void descargarPaquete()
        {
            if (!string.IsNullOrEmpty(idPaquete.Text))
            {
                //Descargar Solicitud
                string descargaResponse = DescargarSolicitud(certifcate, autorization.Text, idPaquete.Text);

                GuardarSolicitud(idPaquete.Text, descargaResponse);
            }
            Console.ReadLine();
        }
    }
}
