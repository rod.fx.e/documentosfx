﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DescargaFX.Class
{
    class Certificado
    {
        public string serialNumber { get; set; }
        public string issuer { get; set; }
        public string certificado { get; set; }
    }
}
