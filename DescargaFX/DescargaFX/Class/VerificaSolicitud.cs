﻿using System;
using System.Security.Cryptography.X509Certificates;
using System.Xml;

namespace DescargaFX.Class
{
    internal class VerificaSolicitud : SoapRequestBase
    {
        public VerificaSolicitud(string url, string action)
           : base(url, action)
        {
        }

        public short EstadoSolicitud { get; set; }
        public short CodEstatus { get; set; }
        public short CodigoEstadoSolicitud { get; set; }
        public short NumeroCFDIs { get; set; }
        public string Mensaje { get; set; }
        public string paquete { get; set; }

        #region Crea el XML para enviar
        public string Generate(X509Certificate2 certificate, string rfcSolicitante, string idSolicitud)
        {
            //string canonicalTimestamp = "<des:VerificaSolicitudDescarga xmlns:des=\"http://DescargaMasivaTerceros.sat.gob.mx\">"
            //    + "<des:solicitud IdSolicitud=\"" + idSolicitud + "\" RfcSolicitante=\"" + rfcSolicitante + ">"
            //    + "</des:solicitud>"
            //    + "</des:VerificaSolicitudDescarga>";
            //Rodrigo Escalona: 01/12/2022
            string canonicalTimestamp = "<des:VerificaSolicitudDescarga xmlns:des=\"http://DescargaMasivaTerceros.sat.gob.mx\">"                
                + "</des:VerificaSolicitudDescarga>";
            if (!String.IsNullOrEmpty(idSolicitud))
            {
                canonicalTimestamp = "<des:VerificaSolicitudDescarga xmlns:des=\"http://DescargaMasivaTerceros.sat.gob.mx\">"
                    + "<des:solicitud IdSolicitud=\"" + idSolicitud + "\" RfcSolicitante=\"" + rfcSolicitante + ">"
                    + "</des:solicitud>"
                    + "</des:VerificaSolicitudDescarga>";
            }
            //< !--Optional:-->
            // < des:solicitud IdSolicitud = "4E80345D-917F-40BB-A98F-4A73939343C5"
            //RfcSolicitante = "AXT940727FP8" >
            //< !--Optional:-->

            string digest = CreateDigest(canonicalTimestamp);

            string canonicalSignedInfo = @"<SignedInfo xmlns=""http://www.w3.org/2000/09/xmldsig#"">" +
                                            @"<CanonicalizationMethod Algorithm=""http://www.w3.org/2001/10/xml-exc-c14n#""></CanonicalizationMethod>" +
                                            @"<SignatureMethod Algorithm=""http://www.w3.org/2000/09/xmldsig#rsa-sha1""></SignatureMethod>" +
                                            @"<Reference URI=""#_0"">" +
                                               "<Transforms>" +
                                                  @"<Transform Algorithm=""http://www.w3.org/2001/10/xml-exc-c14n#""></Transform>" +
                                               "</Transforms>" +
                                               @"<DigestMethod Algorithm=""http://www.w3.org/2000/09/xmldsig#sha1""></DigestMethod>" +
                                               "<DigestValue>" + digest + "</DigestValue>" +
                                            "</Reference>" +
                                         "</SignedInfo>";
            string signature = Sign(canonicalSignedInfo, certificate);
            string soap_request = @"<s:Envelope xmlns:s=""http://schemas.xmlsoap.org/soap/envelope/"" xmlns:u=""http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd"" xmlns:des=""http://DescargaMasivaTerceros.sat.gob.mx"" xmlns:xd=""http://www.w3.org/2000/09/xmldsig#"">" +
                        @"<s:Header/>" +
                        @"<s:Body>" +
                            @"<des:VerificaSolicitudDescarga>" +
                                @"<des:solicitud " +
                                @"IdSolicitud=""" + idSolicitud +
                                @""" RfcSolicitante=""" + rfcSolicitante +
                                @""">" +
                                                    @"<Signature xmlns=""http://www.w3.org/2000/09/xmldsig#"">" +
                                                    @"<SignedInfo>" +
                                                    @"<CanonicalizationMethod Algorithm=""http://www.w3.org/2001/10/xml-exc-c14n#""/>" +
                                                    @"<SignatureMethod Algorithm=""http://www.w3.org/2000/09/xmldsig#rsa-sha1""/>" +
                                                    @"<Reference URI=""#_0"">" +
                                                        @"<Transforms>" +
                                                        @"<Transform Algorithm=""http://www.w3.org/2001/10/xml-exc-c14n#""/>" +
                                                        @"</Transforms>" +
                                                        @"<DigestMethod Algorithm=""http://www.w3.org/2000/09/xmldsig#sha1""/>" +
                                                        @"<DigestValue>" + digest + @"</DigestValue>" +
                                                    @"</Reference>" +
                                                    @"</SignedInfo>" +
                                                    @"<SignatureValue>" + signature + "</SignatureValue>" +
                                                    @"<KeyInfo>" +
                                                        @"<X509Data>" +
                                                            @"<X509IssuerSerial>" +
                                                                @"<X509IssuerName>" + certificate.Issuer +
                                                                @"</X509IssuerName>" +
                                                                @"<X509SerialNumber>" + certificate.SerialNumber +
                                                                @"</X509SerialNumber>" +
                                                            @"</X509IssuerSerial>" +
                                                            @"<X509Certificate>" + Convert.ToBase64String(certificate.RawData) + "</X509Certificate>" +
                                                        @"</X509Data>" +
                                                    @"</KeyInfo>" +
                                                    @"</Signature>" +
                                                    @"</des:solicitud>" +
                                                @"</des:VerificaSolicitudDescarga>" +
                                            @"</s:Body>" +
                                            @"</s:Envelope>";
            xml = soap_request;
            return soap_request;
        }
        #endregion
        public override string GetResult(XmlDocument xmlDoc)
        {
            string s = string.Empty;

            EstadoSolicitud = Convert.ToInt16(xmlDoc.GetElementsByTagName("VerificaSolicitudDescargaResult")[0].Attributes["EstadoSolicitud"].Value);
            CodEstatus = Convert.ToInt16(xmlDoc.GetElementsByTagName("VerificaSolicitudDescargaResult")[0].Attributes["CodEstatus"].Value);
            

            try
            {
                if (EstadoSolicitud != 0)
                {
                    NumeroCFDIs = Convert.ToInt16(xmlDoc.GetElementsByTagName("VerificaSolicitudDescargaResult")[0].Attributes["NumeroCFDIs"].Value);
                    Mensaje = xmlDoc.GetElementsByTagName("VerificaSolicitudDescargaResult")[0].Attributes["Mensaje"].Value.ToString();
                    CodigoEstadoSolicitud = Convert.ToInt16(xmlDoc.GetElementsByTagName("VerificaSolicitudDescargaResult")[0].Attributes["CodigoEstadoSolicitud"].Value);

                    if (xmlDoc.GetElementsByTagName("IdsPaquetes") != null)
                    {
                        if (xmlDoc.GetElementsByTagName("IdsPaquetes").Count != 0)
                        {
                            //Cargar los Paquetes separados por , 
                            paquete = string.Empty;
                            string PaquetesTr = xmlDoc.GetElementsByTagName("IdsPaquetes")[0].InnerXml;
                            Bitacora.Log("Paquetes(s)"+ PaquetesTr);
                            paquete = PaquetesTr;

                            //foreach (string PaquetesTr in xmlDoc.GetElementsByTagName("IdsPaquetes"))
                            //{
                            //    if (!String.IsNullOrEmpty(paquete))
                            //    {
                            //        paquete += ",";
                            //    }
                            //    paquete += PaquetesTr;
                            //}

                        }
                    }
                    


                }
                else
                {

                }
            }
            catch
            {

            }

            return s;
        }
    }
}
