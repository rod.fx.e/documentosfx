﻿using OfficeOpenXml;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Windows.Forms;

namespace DescargaFX.Class
{
    public sealed class ExcelFX
    {
        public static void exportar(DataGridView dtgp, string titulo, DataGridView dtgpDOS = null, string hojaTitulo = null, string hojaDos = null)
        {
            FolderBrowserDialog folderBrowserDialog1 = new FolderBrowserDialog();
            var fileName = titulo + DateTime.Now.ToString("yyyy-MM-dd-hh-mm-ss") + ".xlsx";
            fileName = fileName.Replace(":", "");
            fileName = fileName.Replace("&", "");
            fileName = fileName.Replace(@"\", "");
            fileName = fileName.Replace("/", "");

            if (folderBrowserDialog1.ShowDialog() == DialogResult.OK)
            {
                fileName = folderBrowserDialog1.SelectedPath + "\\" + fileName;
                //fileName = fileName.Replace("/", "");

                //string invalidChars = new string(Path.GetInvalidFileNameChars()) + new string(Path.GetInvalidPathChars());
                //foreach (char c in invalidChars)
                //{
                //    fileName = fileName.Replace(c.ToString(), "");
                //}


                System.IO.Stream myStream = new System.IO.FileStream(fileName, System.IO.FileMode.Create, System.IO.FileAccess.ReadWrite, System.IO.FileShare.None);

                using (ExcelPackage pck = new ExcelPackage())
                {
                    DataTable oDataTable = new DataTable();

                    oDataTable = GetDataTableFromDGV(dtgp);
                    if (oDataTable.Rows.Count == 0)
                    {
                        return;
                    }
                    ExcelWorksheet ws1;
                    if (hojaTitulo != null)
                    {
                        ws1 = pck.Workbook.Worksheets.Add(hojaTitulo);
                    }
                    else
                    {
                        ws1 = pck.Workbook.Worksheets.Add("Detalle");
                    }
                    ws1.Cells["A1"].LoadFromDataTable(oDataTable, true, OfficeOpenXml.Table.TableStyles.Light13);

                    if (dtgpDOS != null)
                    {
                        oDataTable = new DataTable();
                        oDataTable = GetDataTableFromDGV(dtgpDOS);
                        if (oDataTable.Rows.Count == 0)
                        {
                            return;
                        }
                        ExcelWorksheet ws2 = pck.Workbook.Worksheets.Add(hojaDos);
                        ws2.Cells["A1"].LoadFromDataTable(oDataTable, true, OfficeOpenXml.Table.TableStyles.Light13);

                    }

                    pck.SaveAs(myStream);
                }
                myStream.Close();
                DialogResult Resultado = MessageBox.Show("Exportación del archivo " + Environment.NewLine + fileName + " finalizada." + Environment.NewLine + "¿Desea abrirlo?", Application.ProductName + "-" + Application.ProductVersion, MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                if (Resultado.ToString() != "Yes")
                {
                    return;
                }

                FileInfo fi = new FileInfo(fileName);
                if (fi.Exists)
                {
                    System.Diagnostics.Process.Start(fileName);
                }
            }

        }


        public static DataTable GetDataTableFromDGV(DataGridView dgv)
        {
            var dt = new DataTable();
            int CantidadColumnas = 0;
            foreach (DataGridViewColumn column in dgv.Columns)
            {
                if (column.Visible)
                {
                    CantidadColumnas++;
                    if (column.ValueType != null)
                    {
                        switch (column.ValueType.Name)
                        {
                            case "String":
                                dt.Columns.Add(column.HeaderText);
                                break;
                            case "Int32":
                                dt.Columns.Add(column.HeaderText, typeof(Int32));
                                break;
                            case "Decimal":
                                dt.Columns.Add(column.HeaderText, typeof(Decimal));
                                break;
                            case "Double":
                                dt.Columns.Add(column.HeaderText, typeof(Double));
                                break;
                            case "DateTime":
                                dt.Columns.Add(column.HeaderText, typeof(DateTime));
                                break;
                            default:
                                dt.Columns.Add(column.HeaderText);
                                break;
                        }
                    }
                    else
                    {


                        dt.Columns.Add(column.HeaderText);
                    }
                }

                //}
                //dt.Columns.Add(column.HeaderText);
            }

            object[] cellValues = new object[CantidadColumnas];
            foreach (DataGridViewRow row in dgv.Rows)
            {
                for (int i = 0; i < row.Cells.Count; i++)
                {
                    if (dgv.Columns[i].Visible)
                    {
                        cellValues[i] = row.Cells[i].Value;
                    }

                }
                dt.Rows.Add(cellValues);
            }
            return dt;
        }

    }

}
