﻿using Ionic.Zip;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Mail;
using System.Security.AccessControl;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DescargaFX.Class
{
    public static class Globales
    {
        public static cUsuario Usuario = new cUsuario();
        public static string PathPaquetes = AppDomain.CurrentDomain.BaseDirectory + @"Paquetes\";
        public static bool IsNumeric(this string text)
        {
            double test;
            return double.TryParse(text, out test);
        }
        public static bool IsValid(string emailaddress)
        {
            try
            {
                if(String.IsNullOrEmpty(emailaddress)){
                    return false;
                }

                MailAddress m = new MailAddress(emailaddress);

                return true;
            }
            catch (FormatException)
            {
                return false;
            }
        }
        public static bool IsDate(Object obj)
        {

            try
            {
                string strDate = obj.ToString();
                DateTime dt = DateTime.Parse(strDate);
                return true;
            }
            catch
            {
                return false;
            }
        }

        public static bool CarpetaDescargaBloquear()
        {
            try
            {
                string folderPath = PathPaquetes;
                string adminUserName = Environment.UserName;// getting your adminUserName
                DirectorySecurity ds = Directory.GetAccessControl(folderPath);
                FileSystemAccessRule fsa = new FileSystemAccessRule(adminUserName, FileSystemRights.FullControl, AccessControlType.Deny);
                ds.AddAccessRule(fsa);
                Directory.SetAccessControl(folderPath, ds);
            }
            catch (Exception Error)
            {
                string Mensaje = Error.Message.ToString();
                if (Error.InnerException != null)
                {
                    Mensaje += Environment.NewLine + Error.InnerException.ToString();
                }
                MessageBox.Show(Mensaje, Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            return false;
        }
        public static bool CarpetaDescargaDesbloquear()
        {
            try
            {
                string folderPath = PathPaquetes;
                string adminUserName = Environment.UserName;// getting your adminUserName
                DirectorySecurity ds = Directory.GetAccessControl(folderPath);
                FileSystemAccessRule fsa = new FileSystemAccessRule(adminUserName, FileSystemRights.FullControl, AccessControlType.Deny);
                ds.RemoveAccessRule(fsa);
                Directory.SetAccessControl(folderPath, ds);
            }
            catch (Exception Error)
            {
                string Mensaje = Error.Message.ToString();
                if (Error.InnerException != null)
                {
                    Mensaje += Environment.NewLine + Error.InnerException.ToString();
                }
                MessageBox.Show(Mensaje, Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            return false;
        }
        public static bool Comprimir(string archivo)
        {
            try
            {
                using (ZipFile zip = new ZipFile())
                {

                    zip.Password = ObtenerPassword();
                    zip.AddFile(archivo);
                    string ArchivoComprimidoPassword= Path.GetFileNameWithoutExtension(archivo)+"fx.zip";
                    zip.Save(PathPaquetes+ ArchivoComprimidoPassword);
                }
                return true;
            }
            catch (Exception Error)
            {
                string Mensaje = Error.Message.ToString();
                if (Error.InnerException != null)
                {
                    Mensaje += Environment.NewLine + Error.InnerException.ToString();
                }
                MessageBox.Show(Mensaje, Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            return false;
        }

        private static string ObtenerPassword()
        {
            ModeloDescarga.DescargaFXContainer Db = new ModeloDescarga.DescargaFXContainer();
            ModeloDescarga.DescargaConfiguracion Registro = Db.DescargaConfiguracionSet.FirstOrDefault();
            if (Registro == null)
            {
                return "FxGroUp%";
            }
            return Registro.PasswordZip;
        }

        public static bool Descomprimir(string archivo)
        {
            return true;
            try
            {

            }
            catch (Exception Error)
            {
                string Mensaje = Error.Message.ToString();
                if (Error.InnerException != null)
                {
                    Mensaje += Environment.NewLine + Error.InnerException.ToString();
                }
                MessageBox.Show(Mensaje, Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
    }

}
