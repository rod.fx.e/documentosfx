﻿using System;
using System.Security.Cryptography.X509Certificates;
using System.Xml;

namespace DescargaFX.Class
{
    internal class SolicitudFX : SoapRequestBase
    {
        public string CodEstatus { get; set; }
        public string Mensaje { get; set; }

        public SolicitudFX(string url, string action)
           : base(url, action)
        {
        }

        #region Crea el XML para enviar
        public string Generate(X509Certificate2 certificate, string rfcEmisor, string rfcReceptor, string rfcSolicitante, string fechaInicial = "", string fechaFinal = "", string tipoSolicitud = "CFDI")
        {
            FixFecha(fechaInicial, fechaFinal, out fechaInicial, out fechaFinal);


                string canonicalTimestamp = "<SolicitaDescarga xmlns:des=\"http://DescargaMasivaTerceros.sat.gob.mx\">"
                    + "<solicitud RfcEmisor=\"" + rfcEmisor
                    + "\" RfcSolicitante=\"" + rfcSolicitante
                    + "\" FechaInicial=\"" + fechaInicial
                    + "\" FechaFinal=\"" + fechaFinal
                    + "\" TipoComprobante=\"" + tipoSolicitud
                    + "\" TipoSolicitud=\"" + tipoSolicitud
                    + "\">"
                    + "<RfcReceptores><RfcReceptor>" + rfcReceptor + "</RfcReceptor></RfcReceptores>"
                    + "</solicitud>"
                    + "</SolicitaDescarga>";
                string digest = CreateDigest(canonicalTimestamp);
                string canonicalSignedInfo = @"<SignedInfo xmlns=""http://www.w3.org/2000/09/xmldsig#"">" +
                                    @"<CanonicalizationMethod Algorithm=""http://www.w3.org/2001/10/xml-exc-c14n#""></CanonicalizationMethod>" +
                                    @"<SignatureMethod Algorithm=""http://www.w3.org/2000/09/xmldsig#rsa-sha1""></SignatureMethod>" +
                                    @"<Reference URI=""#_0"">" +
                                       "<Transforms>" +
                                          @"<Transform Algorithm=""http://www.w3.org/2001/10/xml-exc-c14n#""></Transform>" +
                                       "</Transforms>" +
                                       @"<DigestMethod Algorithm=""http://www.w3.org/2000/09/xmldsig#sha1""></DigestMethod>" +
                                       "<DigestValue>" + digest + "</DigestValue>" +
                                    "</Reference>" +
                                 "</SignedInfo>";

                string signature = Sign(canonicalSignedInfo, certificate);

                string soap_request = @"<s:Envelope xmlns:s=""http://schemas.xmlsoap.org/soap/envelope/""
                                               xmlns:xd=""http://www.w3.org/2000/09/xmldsig#"">" +
                            @"<s:Header/>" +
                            @"<s:Body>" +
                                @"<SolicitaDescarga xmlns=""http://DescargaMasivaTerceros.sat.gob.mx"" >" +
                                    @"<solicitud RfcEmisor=""" + rfcEmisor +
                                    @""" RfcSolicitante=""" + rfcSolicitante +
                                    @""" FechaInicial=""" + fechaInicial +
                                    @""" FechaFinal =""" + fechaFinal +
                                    @""" TipoSolicitud=""" + tipoSolicitud +
                                    @""">" + "<RfcReceptores><RfcReceptor>" + rfcReceptor + "</RfcReceptor></RfcReceptores>" +
                                                        @"<Signature xmlns=""http://www.w3.org/2000/09/xmldsig#"">" +
                                                        @"<SignedInfo>" +
                                                        @"<CanonicalizationMethod Algorithm=""http://www.w3.org/2001/10/xml-exc-c14n#""/>" +
                                                        @"<SignatureMethod Algorithm=""http://www.w3.org/2000/09/xmldsig#rsa-sha1""/>" +
                                                        @"<Reference URI=""#_0"">" +
                                                            @"<Transforms>" +
                                                            @"<Transform Algorithm=""http://www.w3.org/2001/10/xml-exc-c14n#""/>" +
                                                            @"</Transforms>" +
                                                            @"<DigestMethod Algorithm=""http://www.w3.org/2000/09/xmldsig#sha1""/>" +
                                                            @"<DigestValue>" + digest + @"</DigestValue>" +
                                                        @"</Reference>" +
                                                        @"</SignedInfo>" +
                                                        @"<SignatureValue>" + signature + "</SignatureValue>" +
                                                        @"<KeyInfo>" +
                                                            @"<X509Data>" +
                                                                @"<X509IssuerSerial>" +
                                                                    @"<X509IssuerName>" + certificate.Issuer +
                                                                    @"</X509IssuerName>" +
                                                                    @"<X509SerialNumber>" + certificate.SerialNumber +
                                                                    @"</X509SerialNumber>" +
                                                                @"</X509IssuerSerial>" +
                                                                @"<X509Certificate>" + Convert.ToBase64String(certificate.RawData) + "</X509Certificate>" +
                                                            @"</X509Data>" +
                                                        @"</KeyInfo>" +
                                                        @"</Signature>" +
                                                        @"</solicitud>" +
                                                    @"</SolicitaDescarga>" +
                                                @"</s:Body>" +
                                                @"</s:Envelope>";


                xml = soap_request;
                return soap_request;

            /*
            string parteXMLEmisor = "RfcEmisor";
            //string rfc = rfcEmisor;
            string parteXMLReceptor = string.Empty;
            if (!String.IsNullOrEmpty(rfcReceptor))
            {
                parteXMLEmisor = string.Empty;
                //parteXML = "RfcReceptor";
                //rfc = rfcReceptor;
                parteXMLReceptor = @"
                <des:RfcReceptores>
                    <des:RfcReceptor>" + rfcReceptor + @"</des:RfcReceptor>
                </des:RfcReceptores>
                ";
            }

            string canonicalTimestamp = "<des:SolicitaDescarga xmlns:des=\"http://DescargaMasivaTerceros.sat.gob.mx\">"
                + "<des:solicitud RfcSolicitante=\"" + rfcSolicitante +
                "\" FechaInicial=\"" + fechaInicial + "\" FechaFinal=\"" + fechaFinal + "\" TipoSolicitud=\"CFDI\" RfcACuentaTerceros=\"\" >"
                + parteXMLReceptor
                + "</des:solicitud>"
                + "</des:SolicitaDescarga>";

            string digest = CreateDigest(canonicalTimestamp);



            string canonicalSignedInfo = @"<SignedInfo xmlns=""http://www.w3.org/2000/09/xmldsig#"">" +
                                            @"<CanonicalizationMethod Algorithm=""http://www.w3.org/2001/10/xml-exc-c14n#""></CanonicalizationMethod>" +
                                            @"<SignatureMethod Algorithm=""http://www.w3.org/2000/09/xmldsig#rsa-sha1""></SignatureMethod>" +
                                            @"<Reference URI=""#_0"">" +
                                               "<Transforms>" +
                                                  @"<Transform Algorithm=""http://www.w3.org/2001/10/xml-exc-c14n#""></Transform>" +
                                               "</Transforms>" +
                                               @"<DigestMethod Algorithm=""http://www.w3.org/2000/09/xmldsig#sha1""></DigestMethod>" +
                                               "<DigestValue>" + digest + "</DigestValue>" +
                                            "</Reference>" +
                                         "</SignedInfo>";
            string signature = Sign(canonicalSignedInfo, certificate);
            string soap_request = @"<s:Envelope xmlns:s=""http://schemas.xmlsoap.org/soap/envelope/"" xmlns:u=""http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd"" xmlns:des=""http://DescargaMasivaTerceros.sat.gob.mx"" xmlns:xd=""http://www.w3.org/2000/09/xmldsig#"">" +
                        @"<s:Header/>" +
                        @"<s:Body>" +
                            @"<des:SolicitaDescarga>" +
                                @"<des:solicitud " + parteXMLEmisor + @"=""" + rfcEmisor +
                                @""" RfcSolicitante=""" + rfcSolicitante +
                                @""" FechaInicial=""" + fechaInicial +
                                @""" FechaFinal =""" + fechaFinal +
                                @""" TipoSolicitud=""" + tipoSolicitud +
                                @""">" + parteXMLReceptor + 
                                                    @"<Signature xmlns=""http://www.w3.org/2000/09/xmldsig#"">" +
                                                    @"<SignedInfo>" +
                                                    @"<CanonicalizationMethod Algorithm=""http://www.w3.org/2001/10/xml-exc-c14n#""/>" +
                                                    @"<SignatureMethod Algorithm=""http://www.w3.org/2000/09/xmldsig#rsa-sha1""/>" +
                                                    @"<Reference URI=""#_0"">" +
                                                        @"<Transforms>" +
                                                        @"<Transform Algorithm=""http://www.w3.org/2001/10/xml-exc-c14n#""/>" +
                                                        @"</Transforms>" +
                                                        @"<DigestMethod Algorithm=""http://www.w3.org/2000/09/xmldsig#sha1""/>" +
                                                        @"<DigestValue>" + digest + @"</DigestValue>" +
                                                    @"</Reference>" +
                                                    @"</SignedInfo>" +
                                                    @"<SignatureValue>" + signature + "</SignatureValue>" +
                                                    @"<KeyInfo>" +
                                                        @"<X509Data>" +
                                                            @"<X509IssuerSerial>" +
                                                                @"<X509IssuerName>" + certificate.Issuer +
                                                                @"</X509IssuerName>" +
                                                                @"<X509SerialNumber>" + certificate.SerialNumber +
                                                                @"</X509SerialNumber>" +
                                                            @"</X509IssuerSerial>" +
                                                            @"<X509Certificate>" + Convert.ToBase64String(certificate.RawData) + "</X509Certificate>" +
                                                        @"</X509Data>" +
                                                    @"</KeyInfo>" +
                                                    @"</Signature>" +
                                                    @"</des:solicitud>" +
                                                @"</des:SolicitaDescarga>" +
                                            @"</s:Body>" +
                                            @"</s:Envelope>";
            xml = soap_request;
            return soap_request;
            */
        }
#endregion
        private void FixFecha(string fechaInicial1, string fechaFinal1, out string fechaInicial, out string fechaFinal)
        {
            fechaInicial = fechaInicial1 + "T00:00:00";
            fechaFinal = fechaFinal1 + "T23:59:59";
        }
        public override string GetResult(XmlDocument xmlDoc)
        {
            try
            {

                if (xmlDoc.GetElementsByTagName("SolicitaDescargaResult") != null)
                {
                    if (xmlDoc.GetElementsByTagName("SolicitaDescargaResult").Count != 0)
                    {
                       
                        var IdSolicitud=xmlDoc.GetElementsByTagName("SolicitaDescargaResult")[0].Attributes["IdSolicitud"]?.Value;
                        if (IdSolicitud!=null)
                        {
                            return IdSolicitud.ToString();
                        }
                        // SolicitaDescargaResponse xmlns = "http://DescargaMasivaTerceros.sat.gob.mx" >
                        //< SolicitaDescargaResult CodEstatus = "301" Mensaje = "XML Mal Formado" />                        
                        //</ SolicitaDescargaResponse >                        
                        CodEstatus = xmlDoc.GetElementsByTagName("SolicitaDescargaResult")[0].Attributes["CodEstatus"]?.Value;
                        Mensaje = xmlDoc.GetElementsByTagName("SolicitaDescargaResult")[0].Attributes["Mensaje"]?.Value;
                    }
                }
                
            }
            catch
            {

            }
            return string.Empty;
        }
    }
}
