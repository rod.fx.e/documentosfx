﻿using DescargaFX.Class;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Security.Cryptography.X509Certificates;
using System.Web;

namespace DescargaFX
{
    public partial class frmPrincipalNuevo : Form
    {
        //static byte[] pfx = File.ReadAllBytes(@"SME010619IS0.pfx");
        //static byte[] pfx = File.ReadAllBytes(@"FMI84121997A.pfx");
        static byte[] pfx = File.ReadAllBytes(@"BME9802073WA.pfx");
        

        static string password = "abc123";
        //static string  = "SME010619IS0";

        static string urlAutentica = "https://cfdidescargamasivasolicitud.clouda.sat.gob.mx/Autenticacion/Autenticacion.svc";
        static string urlAutenticaAction = "http://DescargaMasivaTerceros.gob.mx/IAutenticacion/Autentica";

        static string urlSolicitud = "https://cfdidescargamasivasolicitud.clouda.sat.gob.mx/SolicitaDescargaService.svc";
        static string urlSolicitudAction = "http://DescargaMasivaTerceros.sat.gob.mx/ISolicitaDescargaService/SolicitaDescarga";

        static string urlVerificarSolicitud = "https://cfdidescargamasivasolicitud.clouda.sat.gob.mx/VerificaSolicitudDescargaService.svc";
        static string urlVerificarSolicitudAction = "http://DescargaMasivaTerceros.sat.gob.mx/IVerificaSolicitudDescargaService/VerificaSolicitudDescarga";

        static string urlDescargarSolicitud = "https://cfdidescargamasiva.clouda.sat.gob.mx/DescargaMasivaService.svc";
        static string urlDescargarSolicitudAction = "http://DescargaMasivaTerceros.sat.gob.mx/IDescargaMasivaTercerosService/Descargar";

        X509Certificate2 certifcate;
        public frmPrincipalNuevo()
        {
            InitializeComponent();

            //SACRED
            //this.RfcSolicitante.Text = "SME010619IS0";
            //pfx = File.ReadAllBytes(@"SME010619IS0.pfx");

            this.RfcSolicitante.Text = "BME9802073WA";
            pfx = File.ReadAllBytes(@"BME9802073WA.pfx");

            //Obtener Certificados
            certifcate = ObtenerX509Certificado(pfx);
            autentificar();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            solicitar();
        }

        private void autentificar()
        {
            
            //Obtener Token
            string token = ObtenerToken(certifcate);
            autorization.Text = String.Format("WRAP access_token=\"{0}\"", System.Net.WebUtility.UrlDecode(token));
            Console.WriteLine("Token: " + token);
        }

        private void solicitar()
        {
            //Validar Solicitud
            ValidarSolicitud(certifcate, autorization.Text, idSolicitud.Text);


        }

        private static X509Certificate2 ObtenerX509Certificado(byte[] pfx)
        {
            return new X509Certificate2(pfx, password, X509KeyStorageFlags.MachineKeySet |
                            X509KeyStorageFlags.PersistKeySet |
                            X509KeyStorageFlags.Exportable);
        }

        private void GuardarSolicitud(string idPaquete, string descargaResponse, DateTime inicio, DateTime fin)
        {
            string path = "./Paquetes/";
            byte[] file = Convert.FromBase64String(descargaResponse);
            Directory.CreateDirectory(path);
            string tipoTr = "Emitido";
            if (rdTipo1.Checked)
            {
                tipoTr = "Recibido";
            }


            string nombreArchivo = RfcSolicitante.Text + "_" + tipoTr + "_"+ DateTime.Now.ToString("yyyy-MM-dd_HHmm")+ "_" 
                + inicio.ToString("yyyy-MM-dd") + "_"
                + fin.ToString("yyyy-MM-dd")
                ;
            if (file.Length>0)
            {
                using (FileStream fs = File.Create(path + nombreArchivo + ".zip", file.Length))
                {
                    fs.Write(file, 0, file.Length);
                }
                Console.WriteLine("FileCreated: " + path + nombreArchivo + ".zip");

            }

            //Descomprimir();

        }

        private string DescargarSolicitud(X509Certificate2 certifcate, string autorization, string idPaquete)
        {
            DescargarSolicitud descargarSolicitud = new DescargarSolicitud(urlDescargarSolicitud, urlDescargarSolicitudAction);
            string xmlDescarga = descargarSolicitud.Generate(certifcate, RfcSolicitante.Text, idPaquete);
            return descargarSolicitud.Send(autorization);
        }

        private void ValidarSolicitud(X509Certificate2 certifcate, string autorization, string idSolicitud)
        {
            solicitudInformacion.Text = "Espere mientras se procesa....";
            VerificaSolicitud verifica = new VerificaSolicitud(urlVerificarSolicitud, urlVerificarSolicitudAction);
            verifica.Generate(certifcate, RfcSolicitante.Text, idSolicitud);
            verifica.Send(autorization);
            solicitudInformacion.Text = DateTime.Now.ToLongTimeString() + Environment.NewLine + "EstadoSolicitud: " + verifica.EstadoSolicitud.ToString() + " "
                + "CodEstatus: " + verifica.CodEstatus.ToString() + " "
                + "CodigoEstadoSolicitud: " + verifica.CodigoEstadoSolicitud.ToString() + " "
                + "NumeroCFDIs: " + verifica.NumeroCFDIs.ToString();
            if (verifica.Mensaje!=null)
            {
                solicitudInformacion.Text += Environment.NewLine + "Mensaje: " + verifica.Mensaje.ToString();
            }
                
                ;
            if (!string.IsNullOrEmpty(verifica.paquete))
            {
                idPaquete.Text = verifica.paquete;
                descargarPaquete();
            }
        }

        private string GenerarSolicitud(X509Certificate2 certifcate, string autorization)
        {
            this.solicitudError.Text = "";
            string RfcEmisor = "";
            string RfcReceptor = "";

            if (rdTipo1.Checked)
            {
                RfcReceptor = RfcSolicitante.Text;
            }
            if (rdTipo2.Checked)
            {
                RfcEmisor = RfcSolicitante.Text;
            }

            SolicitudFX solicitud = new SolicitudFX(urlSolicitud, urlSolicitudAction);



            string xmlSolicitud = solicitud.Generate(certifcate,RfcEmisor, RfcReceptor
                , RfcSolicitante.Text, FechaInicial.Value.ToString("yyyy-MM-dd")
                , FechaFinal.Value.ToString("yyyy-MM-dd"));


            string solicitudTr=solicitud.Send(autorization);

            if (!String.IsNullOrEmpty(solicitudTr))
            {
                this.idSolicitud.Text = solicitudTr;
            }
            else
            {
                this.solicitudError.Text = "CodEstatus: " + solicitud.CodEstatus + Environment.NewLine + "Mensaje: " + solicitud.Mensaje;
            }

            return solicitudTr;
        }

        private static string ObtenerToken(X509Certificate2 certifcate)
        {
            Autenticacion service = new Autenticacion(urlAutentica, urlAutenticaAction);
            string xml = service.Generate(certifcate);
            return service.Send();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            autentificar();
        }

        private void button4_Click(object sender, EventArgs e)
        {
            generarSolicitud();
        }

        private void generarSolicitud()
        {
            //Generar Solicitud
            this.idSolicitud.Text = "";
            string idSolicitud = GenerarSolicitud(certifcate, autorization.Text);

            Console.WriteLine("IdSolicitud: " + idSolicitud);
        }

        private void button3_Click(object sender, EventArgs e)
        {
            descargarPaquete();
        }

        private void descargarPaquete()
        {
            if (!string.IsNullOrEmpty(idPaquete.Text))
            {
                //Descargar Solicitud
                string descargaResponse = DescargarSolicitud(certifcate, autorization.Text, idPaquete.Text);
                GuardarSolicitud(idPaquete.Text, descargaResponse, this.FechaInicial.Value, this.FechaFinal.Value);

                MessageBox.Show("Archivo(s) descargados");
            }
        }
        #region Solicitud
        private int timer3iempo = 60;
        private void timer3ValidarTiempo()
        {

            if (!String.IsNullOrEmpty(idSolicitud.Text))
            {
                timer3iempo++;
                if (timer3iempo==60)
                {
                    timer3iempo = 0;
                    solicitar();
                }                
            }
            else
            {
                timer3iempo = 0;
            }
        }

        #endregion
        private void timer3_Tick(object sender, EventArgs e)
        {
            timer3ValidarTiempo();
        }

        private void checkBox3_CheckedChanged(object sender, EventArgs e)
        {
            timer3.Enabled = true;
            timer3.Start();
        }

        private void frmPrincipalNuevo_Load(object sender, EventArgs e)
        {
            timer3.Start();
        }
    }
}
