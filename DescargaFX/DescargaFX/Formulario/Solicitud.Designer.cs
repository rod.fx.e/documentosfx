﻿
namespace DescargaFX.Formulario
{
    partial class Solicitud
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Solicitud));
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.SolicitudBuscar = new System.Windows.Forms.TextBox();
            this.RfcSolicitante = new System.Windows.Forms.ComboBox();
            this.rdTipo2 = new System.Windows.Forms.RadioButton();
            this.rdTipo1 = new System.Windows.Forms.RadioButton();
            this.label1 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.FechaFinal = new System.Windows.Forms.DateTimePicker();
            this.FechaInicial = new System.Windows.Forms.DateTimePicker();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.button4 = new System.Windows.Forms.Button();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.SolicitudBuscar);
            this.groupBox1.Controls.Add(this.RfcSolicitante);
            this.groupBox1.Controls.Add(this.rdTipo2);
            this.groupBox1.Controls.Add(this.rdTipo1);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.FechaFinal);
            this.groupBox1.Controls.Add(this.FechaInicial);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Location = new System.Drawing.Point(12, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(725, 71);
            this.groupBox1.TabIndex = 486;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Filtros";
            // 
            // SolicitudBuscar
            // 
            this.SolicitudBuscar.Location = new System.Drawing.Point(92, 40);
            this.SolicitudBuscar.Name = "SolicitudBuscar";
            this.SolicitudBuscar.Size = new System.Drawing.Size(600, 20);
            this.SolicitudBuscar.TabIndex = 485;
            // 
            // RfcSolicitante
            // 
            this.RfcSolicitante.FormattingEnabled = true;
            this.RfcSolicitante.Location = new System.Drawing.Point(92, 13);
            this.RfcSolicitante.Name = "RfcSolicitante";
            this.RfcSolicitante.Size = new System.Drawing.Size(185, 21);
            this.RfcSolicitante.TabIndex = 6;
            this.RfcSolicitante.SelectedIndexChanged += new System.EventHandler(this.RfcSolicitante_SelectedIndexChanged);
            // 
            // rdTipo2
            // 
            this.rdTipo2.AutoSize = true;
            this.rdTipo2.Checked = true;
            this.rdTipo2.Location = new System.Drawing.Point(283, 15);
            this.rdTipo2.Name = "rdTipo2";
            this.rdTipo2.Size = new System.Drawing.Size(56, 17);
            this.rdTipo2.TabIndex = 4;
            this.rdTipo2.TabStop = true;
            this.rdTipo2.Text = "Emisor";
            this.rdTipo2.UseVisualStyleBackColor = true;
            // 
            // rdTipo1
            // 
            this.rdTipo1.AutoSize = true;
            this.rdTipo1.Location = new System.Drawing.Point(345, 15);
            this.rdTipo1.Name = "rdTipo1";
            this.rdTipo1.Size = new System.Drawing.Size(69, 17);
            this.rdTipo1.TabIndex = 4;
            this.rdTipo1.Text = "Receptor";
            this.rdTipo1.UseVisualStyleBackColor = true;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(39, 43);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(47, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "Solicitud";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(6, 17);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(80, 13);
            this.label5.TabIndex = 1;
            this.label5.Text = "RFC Solicitante";
            // 
            // FechaFinal
            // 
            this.FechaFinal.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.FechaFinal.Location = new System.Drawing.Point(595, 13);
            this.FechaFinal.Name = "FechaFinal";
            this.FechaFinal.Size = new System.Drawing.Size(97, 20);
            this.FechaFinal.TabIndex = 3;
            this.FechaFinal.Value = new System.DateTime(2020, 11, 30, 0, 0, 0, 0);
            // 
            // FechaInicial
            // 
            this.FechaInicial.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.FechaInicial.Location = new System.Drawing.Point(458, 13);
            this.FechaInicial.Name = "FechaInicial";
            this.FechaInicial.Size = new System.Drawing.Size(101, 20);
            this.FechaInicial.TabIndex = 3;
            this.FechaInicial.Value = new System.DateTime(2020, 11, 1, 0, 0, 0, 0);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(420, 17);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(32, 13);
            this.label3.TabIndex = 1;
            this.label3.Text = "Inicio";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(568, 17);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(21, 13);
            this.label4.TabIndex = 1;
            this.label4.Text = "Fin";
            // 
            // button4
            // 
            this.button4.BackColor = System.Drawing.SystemColors.Control;
            this.button4.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button4.Image = global::DescargaFX.Properties.Resources.Guardar;
            this.button4.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button4.Location = new System.Drawing.Point(12, 89);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(75, 23);
            this.button4.TabIndex = 487;
            this.button4.Text = "Guardar";
            this.button4.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.button4.UseVisualStyleBackColor = false;
            this.button4.Click += new System.EventHandler(this.button4_Click);
            // 
            // Solicitud
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(754, 125);
            this.Controls.Add(this.button4);
            this.Controls.Add(this.groupBox1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "Solicitud";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Solicitud";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.ComboBox RfcSolicitante;
        private System.Windows.Forms.RadioButton rdTipo2;
        private System.Windows.Forms.RadioButton rdTipo1;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.DateTimePicker FechaFinal;
        private System.Windows.Forms.DateTimePicker FechaInicial;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.TextBox SolicitudBuscar;
        private System.Windows.Forms.Label label1;
    }
}