﻿using CFDI33;
using DescargaFX.Class;
using Generales;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml;
using System.Xml.Linq;
using System.Xml.Serialization;

namespace DescargaFX.Formulario
{
    public partial class frmDescarga : Form
    {
        static byte[] pfx;


        static string password = "abc123";

        static string urlAutentica = "https://cfdidescargamasivasolicitud.clouda.sat.gob.mx/Autenticacion/Autenticacion.svc";
        static string urlAutenticaAction = "http://DescargaMasivaTerceros.gob.mx/IAutenticacion/Autentica";

        static string urlSolicitud = "https://cfdidescargamasivasolicitud.clouda.sat.gob.mx/SolicitaDescargaService.svc";
        static string urlSolicitudAction = "http://DescargaMasivaTerceros.sat.gob.mx/ISolicitaDescargaService/SolicitaDescarga";

        static string urlVerificarSolicitud = "https://cfdidescargamasivasolicitud.clouda.sat.gob.mx/VerificaSolicitudDescargaService.svc";
        static string urlVerificarSolicitudAction = "http://DescargaMasivaTerceros.sat.gob.mx/IVerificaSolicitudDescargaService/VerificaSolicitudDescarga";

        static string urlDescargarSolicitud = "https://cfdidescargamasiva.clouda.sat.gob.mx/DescargaMasivaService.svc";
        static string urlDescargarSolicitudAction = "http://DescargaMasivaTerceros.sat.gob.mx/IDescargaMasivaTercerosService/Descargar";

        X509Certificate2 certifcate;

        public frmDescarga()
        {
            InitializeComponent();
            this.Text = Application.ProductName + " " + Application.ProductVersion + " By FX Group";
            //Validar Nomina si no tiene permisos
            if (!DescargaFX.Class.Globales.Usuario.Nomina)
            {
                tabControl1.TabPages.Remove(tabPage2);
            }
            Limpiar();
            CargarPFX();
        }

        private void Limpiar()
        {
            FechaInicial.Value = FechaFinal.Value = DateTime.Now;

            tabControl1.SelectedTab = this.tabPage1;
            
        }

        private void CargarPFX()
        {
            var ext = new List<string> { "pfx" };
            var myFiles = Directory
                .EnumerateFiles(AppDomain.CurrentDomain.BaseDirectory, "*.*", SearchOption.AllDirectories)
                .Where(s => ext.Contains(Path.GetExtension(s).TrimStart('.').ToLowerInvariant()));

            if (myFiles!=null)
            {
                RfcSolicitante.Items.Clear();
                foreach (var Item in myFiles)
                {
                    string filename = Path.GetFileName(Item);
                    filename = filename.Replace(".pfx","");
                    RfcSolicitante.Items.Add(filename);
                    RfcSolicitante.SelectedIndex = 0;
                }

                
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            Procesar(SolicitudTexto.Text);
        }

        String idSolicitud = String.Empty;
        private void Procesar(string idSolicitudAnterior="")
        {
            try
            {
                //Validar que este completo
                if (this.FechaInicial.Value.Year > 2022)
                {
                    MessageBox.Show("La licencia fue otorgada para hasta el año 2022 " + Environment.NewLine 
                        + "- Producto FX Group. www.fxgroup.com.mx - correo: licencias@fxgroup.com.mx - Telefono: +52 4498948398 - Licencia expirada ");
                    return;
                }


                if (this.FechaInicial.Value> this.FechaFinal.Value)
                {
                    MessageBox.Show(this, "La Fecha inicial no puede ser mayor a la final",Application.ProductName
                        , MessageBoxButtons.OK, MessageBoxIcon.Error );
                    return;
                }

                //BtnDescarga.Visible = false;
                if (LeerPFX())
                {
                    solicitudInformacion.Text = DateTime.Now.ToString("dd/MM/yyy HH:mm:ss") + " Iniciando proceso de descarga ";
                    if (!String.IsNullOrEmpty(idSolicitudAnterior))
                    {
                        idSolicitud = idSolicitudAnterior;                        
                        timer3.Enabled = true;
                        timer3.Start();
                    }
                    else
                    {
                        idSolicitud = GenerarSolicitud(certifcate, Autorization);
                        if (!String.IsNullOrEmpty(idSolicitud))
                        {
                            timer3.Enabled = true;
                            timer3.Start();
                        }
                        else
                        {
                            BtnDescarga.Visible = true;
                        }
                    }
                    SolicitudActual.Text = idSolicitud;
                }
                else
                {
                    BtnDescarga.Visible = true;
                }
            }
            catch (Exception Error)
            {
                string Mensaje = Error.Message.ToString();
                if (Error.InnerException != null)
                {
                    Mensaje += Environment.NewLine + Error.InnerException.ToString();
                }
                BitacoraFX.Log(Mensaje);

            }

        }
        private string GenerarSolicitud(X509Certificate2 certifcate, string autorization)
        {
            try
            {
                idSolicitud = String.Empty;
                string RfcEmisor = "";
                string RfcReceptor = "";

                if (rdTipo1.Checked)
                {
                    RfcReceptor = RfcSolicitante.Text;
                }
                if (rdTipo2.Checked)
                {
                    RfcEmisor = RfcSolicitante.Text;
                }

                SolicitudFX solicitud = new SolicitudFX(urlSolicitud, urlSolicitudAction);


                string xmlSolicitud = solicitud.Generate(certifcate, RfcEmisor, RfcReceptor
                    , RfcSolicitante.Text, FechaInicial.Value.ToString("yyyy-MM-dd")
                    , FechaFinal.Value.ToString("yyyy-MM-dd"));


                string solicitudTr = solicitud.Send(autorization);

                if (!String.IsNullOrEmpty(solicitudTr))
                {
                    idSolicitud = solicitudTr;
                }
                else
                {
                    string mensaje = "CodEstatus: " + solicitud.CodEstatus + Environment.NewLine + "Mensaje: " + solicitud.Mensaje;
                    solicitudInformacion.Text = DateTime.Now.ToString("dd/MM/yyy HH:mm:ss") + " Solicitud no realizada " + mensaje;
                }
                return solicitudTr;
            }
            catch (Exception Error)
            {
                string Mensaje = Error.Message.ToString();
                if (Error.InnerException != null)
                {
                    Mensaje += Environment.NewLine + Error.InnerException.ToString();
                }
                BitacoraFX.Log(Mensaje);

            }
            return string.Empty;
        }

        #region Solicitud
        private int timer3iempo = 0;
        private void timer3ValidarTiempo()
        {

            if (!String.IsNullOrEmpty(idSolicitud))
            {
                solicitudInformacion.Text = DateTime.Now.ToString("dd/MM/yyy HH:mm:ss") + " Verificación al SAT en  " +  timer3iempo.ToString() + " segundos ";
                timer3iempo++;
                if (timer3iempo == 10)
                {
                    timer3iempo = 0;
                    ValidarSolicitud(certifcate, Autorization, idSolicitud);
                }
            }
            else
            {
                timer3.Stop();
                BtnDescarga.Visible = true;
                timer3iempo = 0;
            }
        }
        String idPaquete = string.Empty;
        private void ValidarSolicitud(X509Certificate2 certifcate, string autorization, string idSolicitud)
        {
            solicitudInformacion.Text = "Espere mientras se procesa....";
            VerificaSolicitud verifica = new VerificaSolicitud(urlVerificarSolicitud, urlVerificarSolicitudAction);
            verifica.Generate(certifcate, RfcSolicitante.Text, idSolicitud);
            verifica.Send(autorization);
            solicitudInformacion.Text = DateTime.Now.ToLongTimeString() + Environment.NewLine + "EstadoSolicitud: " + verifica.EstadoSolicitud.ToString() + " "
                + "CodEstatus: " + verifica.CodEstatus.ToString() + " "
                + "CodigoEstadoSolicitud: " + verifica.CodigoEstadoSolicitud.ToString() + " "
                + "NumeroCFDIs: " + verifica.NumeroCFDIs.ToString();

            //Token Invalido actualizarñp
            if (verifica.CodEstatus == 300)
            {
                solicitudInformacion.Text += Environment.NewLine + "Mensaje: Token Invalido";
                Autentificar();
            }
            else {
                if (verifica.Mensaje != null)
                {
                    ErroresProceso.Text += Environment.NewLine + "Mensaje: " + verifica.Mensaje.ToString() + " " + DateTime.Now.ToLongTimeString();
                }
                ErroresProceso.Text += Environment.NewLine + "EstadoSolicitud: " + verifica.EstadoSolicitud.ToString() + " " + DateTime.Now.ToLongTimeString();
                ErroresProceso.Text += Environment.NewLine + "Solicitud Codigo: " + verifica.CodEstatus.ToString() + " " + DateTime.Now.ToLongTimeString();
                ErroresProceso.SelectionStart = ErroresProceso.Text.Length;
                ErroresProceso.ScrollToCaret();
                if (!string.IsNullOrEmpty(verifica.paquete))
                {
                    timer3.Stop();
                    idPaquete = verifica.paquete;
                    DescargarPaquete();
                }
            }
        
            
        }
        private void DescargarPaquete()
        {
            if (!string.IsNullOrEmpty(idPaquete))
            {
                //Descargar Solicitud
                string descargaResponse = DescargarSolicitud(certifcate, Autorization, idPaquete);
                GuardarSolicitud(idPaquete, descargaResponse, this.FechaInicial.Value, this.FechaFinal.Value);

            }
        }
        string path = "./Paquetes/";
        private void GuardarSolicitud(string idPaquete, string descargaResponse, DateTime inicio, DateTime fin)
        {
            
            byte[] file = Convert.FromBase64String(descargaResponse);
            Directory.CreateDirectory(path);
            string tipoTr = "Emitido";
            if (rdTipo1.Checked)
            {
                tipoTr = "Recibido";
            }


            string nombreArchivo = RfcSolicitante.Text + "_" + tipoTr + "_" + DateTime.Now.ToString("yyyy-MM-dd_HHmm") + "_"
                + inicio.ToString("yyyy-MM-dd") + "_"
                + fin.ToString("yyyy-MM-dd")
                + ".zip"
                ;
            string NombreCompleto = path + nombreArchivo;
            //if (File.Exists(NombreCompleto))
            //{
            //    File.Delete(NombreCompleto);
            //}
            if (file.Length > 0)
            {
                using (FileStream fs = File.Create(NombreCompleto, file.Length))
                {
                    fs.Write(file, 0, file.Length);
                    fs.Close();
                    fs.Dispose();
                }
                Console.WriteLine("FileCreated: " + NombreCompleto);
                Descomprimir(NombreCompleto);
            }
            
        }

        private void Descomprimir(string zipPath)
        {
            string DirectoryDestiny = "/Paquete/" + Path.GetFileNameWithoutExtension(zipPath);
            if (Directory.Exists(DirectoryDestiny))
            {
                Directory.Delete(DirectoryDestiny,true);
            }

            ZipFile.ExtractToDirectory(zipPath, DirectoryDestiny);

            CargarCFDI(DirectoryDestiny);

        }

        private void CargarCFDI(string DirectoryDestiny)
        {
            this.Dtg.Rows.Clear();
            this.DtgNomina.Rows.Clear();

            if (Directory.Exists(DirectoryDestiny))
            {
                string[] allfiles = System.IO.Directory.GetFiles(DirectoryDestiny, "*.xml"
                    , System.IO.SearchOption.AllDirectories);
                foreach (string filename in allfiles)
                {
                    ProcesarCFDI(filename);
                }
            }
            Dtg.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.AllCells;
            Dtg.Columns[Dtg.ColumnCount - 1].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
            BtnExportar.Visible = true;
        }

        private void ProcesarCFDI(string filename)
        {

            int n = 0;
            
            //Pasear el comprobante
            try
            {
                CFDI33.Comprobante oComprobante;
                XmlSerializer serializer_CFD_3 = new XmlSerializer(typeof(CFDI33.Comprobante));
                TextReader reader = new StreamReader(filename);
                oComprobante = (CFDI33.Comprobante)serializer_CFD_3.Deserialize(reader);
                reader.Dispose();
                reader.Close();
                //Validar Nomina si no tiene permisos
                if (DescargaFX.Class.Globales.Usuario.Nomina)
                {
                    if (ExisteNomina(oComprobante))
                    {
                        //Agregar a DtgNomina
                        AgregarFila(DtgNomina,oComprobante,filename);
                    }
                    else
                    {
                        //Agregar a Dtg
                        AgregarFila(Dtg, oComprobante, filename);
                    }
                }
                else
                {
                    if (ExisteNomina(oComprobante))
                    {
                        return;
                    }
                    else
                    {
                        //Agregar a Dtg
                        AgregarFila(Dtg, oComprobante, filename);
                    }
                }
            }
            catch
            {
                try
                {
                    XmlSerializer serializer_CFD_40 = new XmlSerializer(typeof(CFDI40.Comprobante));
                    TextReader reader = new StreamReader(filename);
                    CFDI40.Comprobante oComprobante = (CFDI40.Comprobante)serializer_CFD_40.Deserialize(reader);
                    reader.Dispose();
                    reader.Close();
                    //Validar Nomina si no tiene permisos
                    if (DescargaFX.Class.Globales.Usuario.Nomina)
                    {
                        if (ExisteNomina40(oComprobante))
                        {
                            //Agregar a DtgNomina
                            AgregarFila40(DtgNomina, oComprobante, filename);
                        }
                        else
                        {
                            //Agregar a Dtg
                            AgregarFila40(Dtg, oComprobante, filename);
                        }
                    }
                    else
                    {
                        if (ExisteNomina40(oComprobante))
                        {
                            return;
                        }
                        else
                        {
                            //Agregar a Dtg
                            AgregarFila40(Dtg, oComprobante, filename);
                        }
                    }
                }
                catch (Exception Error)
                {
                    Dtg.Rows[n].ErrorText = "Cargar Comprobante Automatica: El archivo " + filename + " no es un CFDI válido.";

                    solicitudInformacion.Text = DateTime.Now.ToString() + " No puede ingresar al SAT " + Environment.NewLine
                        + Error.Message;
                    ;
                    string Mensaje = Error.Message.ToString();
                    if (Error.InnerException != null)
                    {
                        Mensaje += Environment.NewLine + Error.InnerException.ToString();
                    }
                    BitacoraFX.Log(Mensaje);

            }
            }

        }
        private void AgregarFila(DataGridView ODtg, CFDI33.Comprobante oComprobante, string filename)
        {
            int n = this.Dtg.Rows.Add();

            ODtg.Rows[n].Tag = filename;
            ODtg.Rows[n].Cells["Version"].Value = oComprobante.Version;

            ODtg.Rows[n].Cells["RFCEmisor"].Value = oComprobante.Emisor.Rfc;
            ODtg.Rows[n].Cells["Emisor"].Value = oComprobante.Emisor.Nombre;


            ODtg.Rows[n].Cells["rfcReceptorCFDI"].Value = oComprobante.Receptor.Rfc;
            ODtg.Rows[n].Cells["nombreReceptorCFDI"].Value = oComprobante.Receptor.Nombre;

            ODtg.Rows[n].Cells["Total"].Value = oComprobante.Total.ToString();
            ODtg.Rows[n].Cells["tipoComprobante"].Value = oComprobante.TipoDeComprobante;
            ODtg.Rows[n].Cells["Moneda"].Value = oComprobante.Moneda;

            ODtg.Rows[n].Cells["Folio"].Value = oComprobante.Folio;
            ODtg.Rows[n].Cells["Serie"].Value = oComprobante.Serie;

            //Determinar si es Timbrado
            if (oComprobante.Complemento != null)
            {
                foreach (CFDI33.ComprobanteComplemento complemento in oComprobante.Complemento)
                {
                    foreach (XmlElement elemento in complemento.Any)
                    {
                        //Timbrado XmlElement elemento
                        if (elemento.OuterXml.Contains("tfd:TimbreFiscalDigital"))
                        {
                            var doc = XDocument.Parse(elemento.OuterXml);
                            string uuidtr = doc.Root.Attributes("UUID").First().Value.ToString();
                            ODtg.Rows[n].Cells["uuidCFDI"].Value = uuidtr;
                            string FechaTimbradotr = doc.Root.Attributes("FechaTimbrado").First().Value.ToString();
                            ODtg.Rows[n].Cells["timbradoCFDI"].Value = FechaTimbradotr;
                            string RfcProvCertiftr = doc.Root.Attributes("RfcProvCertif").First().Value.ToString();
                            ODtg.Rows[n].Cells["RfcProvCertif"].Value = RfcProvCertiftr;
                        }
                    }
                }
            }
            ODtg.Rows[n].Cells["estadoCFDI"].Value = "Valido";
        }

        private void AgregarFila40(DataGridView ODtg, CFDI40.Comprobante oComprobante, string filename)
        {
            int n = this.Dtg.Rows.Add();

            ODtg.Rows[n].Tag = filename;
            ODtg.Rows[n].Cells["Version"].Value = oComprobante.Version;
            ODtg.Rows[n].Cells["RFCEmisor"].Value = oComprobante.Emisor.Rfc;
            ODtg.Rows[n].Cells["Emisor"].Value = oComprobante.Emisor.Nombre;


            ODtg.Rows[n].Cells["rfcReceptorCFDI"].Value = oComprobante.Receptor.Rfc;
            ODtg.Rows[n].Cells["nombreReceptorCFDI"].Value = oComprobante.Receptor.Nombre;

            ODtg.Rows[n].Cells["Total"].Value = oComprobante.Total.ToString();
            ODtg.Rows[n].Cells["tipoComprobante"].Value = oComprobante.TipoDeComprobante;
            ODtg.Rows[n].Cells["Moneda"].Value = oComprobante.Moneda;

            ODtg.Rows[n].Cells["Folio"].Value = oComprobante.Folio;
            ODtg.Rows[n].Cells["Serie"].Value = oComprobante.Serie;

            //Determinar si es Timbrado
            if (oComprobante.Complemento != null)
            {
                foreach (XmlElement elemento in oComprobante.Complemento.Any)
                {
                    //Timbrado XmlElement elemento
                    if (elemento.OuterXml.Contains("tfd:TimbreFiscalDigital"))
                    {
                        var doc = XDocument.Parse(elemento.OuterXml);
                        string uuidtr = doc.Root.Attributes("UUID").First().Value.ToString();
                        ODtg.Rows[n].Cells["uuidCFDI"].Value = uuidtr;
                        string FechaTimbradotr = doc.Root.Attributes("FechaTimbrado").First().Value.ToString();
                        ODtg.Rows[n].Cells["timbradoCFDI"].Value = FechaTimbradotr;
                        string RfcProvCertiftr = doc.Root.Attributes("RfcProvCertif").First().Value.ToString();
                        ODtg.Rows[n].Cells["RfcProvCertif"].Value = RfcProvCertiftr;
                    }
                }                
            }
            ODtg.Rows[n].Cells["estadoCFDI"].Value = "Valido";
        }
        private bool ExisteNomina40(CFDI40.Comprobante oComprobante)
        {
            if (oComprobante.Complemento != null)
            { 
                foreach (XmlElement elemento in oComprobante.Complemento.Any)
                {
                    //Timbrado XmlElement elemento
                    if (elemento.OuterXml.Contains("nomina12:Nomina"))
                    {
                        return true;
                    }
                }
            }
            return false;
        }


        private bool ExisteNomina(Comprobante oComprobante)
        {
            if (oComprobante.Complemento != null)
            {
                foreach (CFDI33.ComprobanteComplemento complemento in oComprobante.Complemento)
                {
                    foreach (XmlElement elemento in complemento.Any)
                    {
                        //Timbrado XmlElement elemento
                        if (elemento.OuterXml.Contains("nomina12:Nomina"))
                        {
                            return true;
                        }
                    }
                }
            }
            return false;
        }

        private string DescargarSolicitud(X509Certificate2 certifcate, string autorization, string idPaquete)
        {
            DescargarSolicitud descargarSolicitud = new DescargarSolicitud(urlDescargarSolicitud, urlDescargarSolicitudAction);
            string xmlDescarga = descargarSolicitud.Generate(certifcate, RfcSolicitante.Text, idPaquete);
            return descargarSolicitud.Send(autorization);
        }
        #endregion

        private void RfcSolicitante_SelectedIndexChanged(object sender, EventArgs e)
        {
            LeerPFX();
        }

        private bool LeerPFX()
        {
            string file = RfcSolicitante.Text + ".pfx";
            if (File.Exists(file))
            {
                pfx= File.ReadAllBytes(file);
                //Obtener Certificados
                certifcate = ObtenerX509Certificado(pfx);
                return Autentificar();
            }
            return false;
        }

        private static X509Certificate2 ObtenerX509Certificado(byte[] pfx)
        {
            return new X509Certificate2(pfx, password, X509KeyStorageFlags.MachineKeySet |
                            X509KeyStorageFlags.PersistKeySet |
                            X509KeyStorageFlags.Exportable);
        }

        String Autorization = String.Empty;
        private bool Autentificar()
        {
            try
            {
                //Obtener Token
                string token = ObtenerToken(certifcate);
                Autorization = String.Format("WRAP access_token=\"{0}\"", System.Net.WebUtility.UrlDecode(token));
                Console.WriteLine("Token: " + token);
                return true;
            }
            catch(Exception Error)
            {
                solicitudInformacion.Text = DateTime.Now.ToString() + " No puede ingresar al SAT " + Environment.NewLine
                    + Error.Message;
                    ;
                string Mensaje = Error.Message.ToString();
                if (Error.InnerException != null)
                {
                    Mensaje += Environment.NewLine + Error.InnerException.ToString();
                }
                BitacoraFX.Log(Mensaje);
            
            }
            return false;
        }

        private static string ObtenerToken(X509Certificate2 certifcate)
        {
            Autenticacion service = new Autenticacion(urlAutentica, urlAutenticaAction);
            service.Generate(certifcate);
            return service.Send();
        }

        private void timer3_Tick(object sender, EventArgs e)
        {
            timer3ValidarTiempo();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Exportar();
        }

        private void Exportar()
        {
            if (tabControl1.SelectedTab == this.tabPage1)
            {
                DescargaFX.Class.ExcelFX.exportar(Dtg, "CFDI");
            }
            if (tabControl1.SelectedTab == this.tabPage2)
            {
                DescargaFX.Class.ExcelFX.exportar(DtgNomina, "CFDINomina");
            }
        }

        private void frmDescarga_Load(object sender, EventArgs e)
        {

        }

        private void button1_Click_1(object sender, EventArgs e)
        {
            OpenFileDialog ofd = new OpenFileDialog();
            ofd.Filter = "Zip files (.zip)|*.zip";
            ofd.ShowDialog();

            Descomprimir(ofd.FileName);

            
        }

    }
}
