﻿using CFDI33;
using DescargaFX.Class;
using Generales;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.Entity.Validation;
using System.Drawing;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml;
using System.Xml.Linq;
using System.Xml.Serialization;

namespace DescargaFX.Formulario
{
    public partial class Solicitud : Form
    {
        static byte[] pfx;

        string directorioPFX = AppDomain.CurrentDomain.BaseDirectory + @"\descargafx\";

        static string password = "abc123";

        static string urlAutentica = "https://cfdidescargamasivasolicitud.clouda.sat.gob.mx/Autenticacion/Autenticacion.svc";
        static string urlAutenticaAction = "http://DescargaMasivaTerceros.gob.mx/IAutenticacion/Autentica";

        static string urlSolicitud = "https://cfdidescargamasivasolicitud.clouda.sat.gob.mx/SolicitaDescargaService.svc";
        static string urlSolicitudAction = "http://DescargaMasivaTerceros.sat.gob.mx/ISolicitaDescargaService/SolicitaDescarga";

        static string urlVerificarSolicitud = "https://cfdidescargamasivasolicitud.clouda.sat.gob.mx/VerificaSolicitudDescargaService.svc";
        static string urlVerificarSolicitudAction = "http://DescargaMasivaTerceros.sat.gob.mx/IVerificaSolicitudDescargaService/VerificaSolicitudDescarga";

        static string urlDescargarSolicitud = "https://cfdidescargamasiva.clouda.sat.gob.mx/DescargaMasivaService.svc";
        static string urlDescargarSolicitudAction = "http://DescargaMasivaTerceros.sat.gob.mx/IDescargaMasivaTercerosService/Descargar";

        X509Certificate2 certifcate;
        string filenameGeneral = string.Empty;
        public Solicitud()
        {
            InitializeComponent();
            CargarPFX();
            Iniciar();
        }

        private void Iniciar()
        {
            FechaInicial.Value = FechaFinal.Value = DateTime.Now;
        }

        private void CargarPFX()
        {
            var ext = new List<string> { "pfx" };
            if (!Directory.Exists(directorioPFX))
            {
                Directory.CreateDirectory(directorioPFX);
            }

            var myFiles = Directory
                .EnumerateFiles(directorioPFX, "*.*", SearchOption.AllDirectories)
                .Where(s => ext.Contains(Path.GetExtension(s).TrimStart('.').ToLowerInvariant()));

            if (myFiles != null)
            {
                RfcSolicitante.Items.Clear();
                foreach (var Item in myFiles)
                {
                    string filename = Path.GetFileName(Item);
                    filenameGeneral = filename;
                    filename = filename.Replace(".pfx", "");
                    RfcSolicitante.Items.Add(filename);
                    RfcSolicitante.SelectedIndex = 0;
                }


            }
        }

        private void button4_Click(object sender, EventArgs e)
        {
            GenerarSolicitud(certifcate, Autorization);
        }
        private void GenerarSolicitud(X509Certificate2 certifcate, string autorization)
        {
            try
            {
                SolicitudBuscar.Text = String.Empty;
                string RfcEmisor = "";
                string RfcReceptor = "";

                if (rdTipo1.Checked)
                {
                    RfcReceptor = RfcSolicitante.Text;
                }
                if (rdTipo2.Checked)
                {
                    RfcEmisor = RfcSolicitante.Text;
                }

                SolicitudFX solicitud = new SolicitudFX(urlSolicitud, urlSolicitudAction);


                string xmlSolicitud = solicitud.Generate(certifcate, RfcEmisor, RfcReceptor
                    , RfcSolicitante.Text, FechaInicial.Value.ToString("yyyy-MM-dd")
                    , FechaFinal.Value.ToString("yyyy-MM-dd"));


                string solicitudTr = solicitud.Send(autorization);

                if (!String.IsNullOrEmpty(solicitudTr))
                {
                    SolicitudBuscar.Text = solicitudTr;
                    //Guardar en la base de datos y cerrar
                    Guardar();
                }
                else
                {
                    string mensaje = "CodEstatus: " + solicitud.CodEstatus + Environment.NewLine + "Mensaje: " + solicitud.Mensaje;
                    SolicitudBuscar.Text += DateTime.Now.ToString("dd/MM/yyy HH:mm:ss") + " Solicitud no realizada " + mensaje + Environment.NewLine;
                }
            }
            catch (Exception Error)
            {
                string Mensaje = Error.Message.ToString();
                if (Error.InnerException != null)
                {
                    Mensaje += Environment.NewLine + Error.InnerException.ToString();
                }
                SolicitudBuscar.Text += Mensaje;
                BitacoraFX.Log(Mensaje);

            }
        }

        private void Guardar()
        {
            ModeloDescarga.DescargaFXContainer Db = new ModeloDescarga.DescargaFXContainer();
            try
            {
                ModeloDescarga.DescargaSolicitud Registro = new ModeloDescarga.DescargaSolicitud();
                Registro.TotalCFDI = 0;
                Registro.Inicio = FechaInicial.Value;
                Registro.Fin = FechaFinal.Value;
                Registro.Tipo  = "Emisor";
                if (rdTipo1.Checked)
                {
                    Registro.Tipo = "Receptor";
                }
                Registro.Creado = DateTime.Now;
                Registro.Estado = "Solicitado";
                Registro.Solicitud = SolicitudBuscar.Text;
                if (Class.Globales.Usuario == null)
                {
                    Class.Globales.Usuario = new cUsuario();
                    Class.Globales.Usuario.Email = "DocumentosFX";
                }
                Registro.Usuario = Class.Globales.Usuario.Email;
                if (Registro.Usuario==null)
                {
                    Registro.Usuario = "DocumentosFX";
                }
                Registro.ArchivoPFX = filenameGeneral;
                Registro.Rfc = RfcSolicitante.Text;
                Db.DescargaSolicitudSet.Add(Registro);
                Db.SaveChanges();
                MessageBox.Show(this, "Datos guardados", Application.ProductName + "-" + Application.ProductVersion, MessageBoxButtons.OK, MessageBoxIcon.Information);
                this.Close();
            }
            catch (DbEntityValidationException e)
            {
                foreach (var eve in e.EntityValidationErrors)
                {
                    foreach (var ve in eve.ValidationErrors)
                    {
                        MessageBox.Show(this, ve.PropertyName + " Error: " + ve.ErrorMessage, Application.ProductName
                            , MessageBoxButtons.OK, MessageBoxIcon.Error);
                        return;
                    }
                }
            }
        }

        private void RfcSolicitante_SelectedIndexChanged(object sender, EventArgs e)
        {

            LeerPFX();
        }

        private bool LeerPFX()
        {
            string file = directorioPFX+RfcSolicitante.Text + ".pfx";
            if (File.Exists(file))
            {
                pfx = File.ReadAllBytes(file);
                //Obtener Certificados
                certifcate = ObtenerX509Certificado(pfx);
                return Autentificar();
            }
            else
            {
                MessageBox.Show(this, "El archivo " + file+ " no existe, no puede generar la solicitud ", Application.ProductName + "-" + Application.ProductVersion, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            return false;
        }


        private static X509Certificate2 ObtenerX509Certificado(byte[] pfx)
        {
            return new X509Certificate2(pfx, password, X509KeyStorageFlags.MachineKeySet |
                            X509KeyStorageFlags.PersistKeySet |
                            X509KeyStorageFlags.Exportable);
        }
        private static string ObtenerToken(X509Certificate2 certifcate)
        {
            Autenticacion service = new Autenticacion(urlAutentica, urlAutenticaAction);
            service.Generate(certifcate);
            return service.Send();
        }
        String Autorization = String.Empty;
        private bool Autentificar()
        {
            try
            {
                //Obtener Token
                string token = ObtenerToken(certifcate);
                Autorization = String.Format("WRAP access_token=\"{0}\"", System.Net.WebUtility.UrlDecode(token));
                Console.WriteLine("Token: " + token);
                return true;
            }
            catch (Exception Error)
            {
                string Mensaje = Error.Message.ToString();
                if (Error.InnerException != null)
                {
                    Mensaje += Environment.NewLine + Error.InnerException.ToString();
                }
                BitacoraFX.Log(Mensaje);

            }
            return false;
        }

    }
}
