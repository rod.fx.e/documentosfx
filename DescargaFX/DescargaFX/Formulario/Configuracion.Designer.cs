﻿
namespace DescargaFX.Formulario
{
    partial class Configuracion
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Configuracion));
            this.VerificarAutomatica = new System.Windows.Forms.CheckBox();
            this.btnAddItem = new System.Windows.Forms.Button();
            this.PasswordZip = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.PasswordZipConfirmacion = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // VerificarAutomatica
            // 
            this.VerificarAutomatica.AutoSize = true;
            this.VerificarAutomatica.Location = new System.Drawing.Point(15, 110);
            this.VerificarAutomatica.Name = "VerificarAutomatica";
            this.VerificarAutomatica.Size = new System.Drawing.Size(277, 17);
            this.VerificarAutomatica.TabIndex = 13;
            this.VerificarAutomatica.Text = "Verificar automanticamente las descargas pendientes";
            this.VerificarAutomatica.UseVisualStyleBackColor = true;
            // 
            // btnAddItem
            // 
            this.btnAddItem.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(153)))), ((int)(((byte)(181)))), ((int)(((byte)(105)))));
            this.btnAddItem.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnAddItem.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAddItem.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnAddItem.Location = new System.Drawing.Point(12, 12);
            this.btnAddItem.Name = "btnAddItem";
            this.btnAddItem.Size = new System.Drawing.Size(91, 28);
            this.btnAddItem.TabIndex = 8;
            this.btnAddItem.Text = "Guardar";
            this.btnAddItem.UseVisualStyleBackColor = false;
            this.btnAddItem.Click += new System.EventHandler(this.btnAddItem_Click);
            // 
            // PasswordZip
            // 
            this.PasswordZip.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.PasswordZip.Location = new System.Drawing.Point(151, 48);
            this.PasswordZip.Name = "PasswordZip";
            this.PasswordZip.PasswordChar = '*';
            this.PasswordZip.Size = new System.Drawing.Size(137, 20);
            this.PasswordZip.TabIndex = 10;
            this.PasswordZip.UseSystemPasswordChar = true;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 51);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(133, 13);
            this.label1.TabIndex = 9;
            this.label1.Text = "Contraseña de compresión";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(12, 77);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(68, 13);
            this.label2.TabIndex = 9;
            this.label2.Text = "Confirmación";
            // 
            // PasswordZipConfirmacion
            // 
            this.PasswordZipConfirmacion.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.PasswordZipConfirmacion.Location = new System.Drawing.Point(151, 74);
            this.PasswordZipConfirmacion.Name = "PasswordZipConfirmacion";
            this.PasswordZipConfirmacion.PasswordChar = '*';
            this.PasswordZipConfirmacion.Size = new System.Drawing.Size(137, 20);
            this.PasswordZipConfirmacion.TabIndex = 10;
            this.PasswordZipConfirmacion.UseSystemPasswordChar = true;
            // 
            // Configuracion
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(300, 141);
            this.Controls.Add(this.VerificarAutomatica);
            this.Controls.Add(this.btnAddItem);
            this.Controls.Add(this.PasswordZipConfirmacion);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.PasswordZip);
            this.Controls.Add(this.label1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "Configuracion";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Configuracion";
            this.TopMost = true;
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.CheckBox VerificarAutomatica;
        private System.Windows.Forms.Button btnAddItem;
        private System.Windows.Forms.TextBox PasswordZip;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox PasswordZipConfirmacion;
    }
}