﻿using CFDI33;
using DescargaFX.Class;
using Generales;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml;
using System.Xml.Linq;
using System.Xml.Serialization;

namespace DescargaFX.Formulario
{
    public partial class Detalle : Form
    {
        ModeloDescarga.DescargaSolicitud Registro = new ModeloDescarga.DescargaSolicitud();
        public Detalle(int id)
        {
            InitializeComponent();
            Cargar(id);
        }
        public Detalle()
        {
            InitializeComponent();
        }
        public Detalle(TipoCarga tipoCarga,string directorio)
        {
            InitializeComponent();
            if (tipoCarga== TipoCarga.Directorio)
            {
                CargarCFDI(directorio);
            }
            if (tipoCarga == TipoCarga.Archivo)
            {
                ProcesarCFDI(directorio);
            }
        }
        public Detalle(string directorio)
        {
            InitializeComponent();
            CargarCFDI(directorio);
        }
        private void Cargar(int id)
        {
            ModeloDescarga.DescargaFXContainer Db = new ModeloDescarga.DescargaFXContainer();
            Registro = Db.DescargaSolicitudSet.Where(a => a.Id == id).FirstOrDefault();
            Descomprimir(Registro.Archivo);
        }

        private void Descomprimir(string zipPath)
        {
            string DirectoryDestiny = Class.Globales.PathPaquetes + Path.GetFileNameWithoutExtension(zipPath);
            string FileSource = Class.Globales.PathPaquetes + zipPath;
            if (!Directory.Exists(DirectoryDestiny))
            {
                //Directory.Delete(DirectoryDestiny, true);
                ZipFile.ExtractToDirectory(FileSource, DirectoryDestiny);
            }
            //
            CargarCFDI(DirectoryDestiny);
            //Eliminar la carpeta
            //if (Directory.Exists(DirectoryDestiny))
            //{
            //    Directory.Delete(DirectoryDestiny, true);
            //}
        }
        private void CargarCFDI(string DirectoryDestiny)
        {
            this.Dtg.Rows.Clear();
            this.DtgNomina.Rows.Clear();

            if (Directory.Exists(DirectoryDestiny))
            {
                string[] allfiles = System.IO.Directory.GetFiles(DirectoryDestiny, "*.xml"
                    , System.IO.SearchOption.AllDirectories);
                TotalLabel.Text = "Total de CFDI: " + allfiles.Length.ToString();
                foreach (string filename in allfiles)
                {
                    ProcesarCFDI(filename);
                }
            }
            Dtg.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.AllCells;
            Dtg.Columns[Dtg.ColumnCount - 1].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
            TotalLabel.Text = "Total de CFDI: " + Dtg.Rows.Count.ToString();
        }

        private void ProcesarCFDI(string filename)
        {
            //Pasear el comprobante
            try
            {
                TotalLabel.Text = "Total de CFDI: 1 ";

                CFDI33.Comprobante oComprobante;
                XmlSerializer serializer_CFD_3 = new XmlSerializer(typeof(CFDI33.Comprobante));
                TextReader reader = new StreamReader(filename);
                oComprobante = (CFDI33.Comprobante)serializer_CFD_3.Deserialize(reader);
                reader.Dispose();
                reader.Close();
                Procesar33(oComprobante, filename);
            }
            catch
            {
                try
                {
                    XmlSerializer serializer_CFD_40 = new XmlSerializer(typeof(CFDI40.Comprobante));
                    TextReader reader = new StreamReader(filename);
                    CFDI40.Comprobante oComprobante = (CFDI40.Comprobante)serializer_CFD_40.Deserialize(reader);
                    reader.Dispose();
                    reader.Close();
                    Procesar40(oComprobante, filename);
                }
                catch (Exception Error)
                {
                    string MensajeTr = "Error cargando CFDI General ";
                    if (Error.InnerException!=null)
                    {
                        MensajeTr += " " + Error.InnerException;
                    }

                    Dtg.Rows[n].ErrorText = MensajeTr;                    
                    string Mensaje = Error.Message.ToString();
                    if (Error.InnerException != null)
                    {
                        Mensaje += Environment.NewLine + Error.InnerException.ToString();
                    }
                    BitacoraFX.Log(Mensaje);
                }
            }

        }

        private void Procesar40(CFDI40.Comprobante oComprobante, string filename)
        {
            try
            {
                string TipoDeComprobanteTr = ObtenerTipoComprobante(filename);
                if (TipoDeComprobanteTr.ToUpper().Equals("P"))
                {
                    return;
                }

                if (ExisteValesdedespensa(filename))
                {
                    return;
                }
                if (ExisteNomina(filename))
                {
                    return;
                }
                //Validar Nomina si no tiene permisos
                if (DescargaFX.Class.Globales.Usuario.Nomina)
                {
                    if (ExisteNomina40(oComprobante))
                    {
                        //Agregar a DtgNomina
                        AgregarFila40(DtgNomina, oComprobante, filename);
                    }
                    else
                    {
                        //Agregar a Dtg
                        AgregarFila40(Dtg, oComprobante, filename);
                    }
                }
                else
                {
                    if (ExisteNomina40(oComprobante))
                    {
                        return;
                    }
                    else
                    {
                        //Agregar a Dtg
                        AgregarFila40(Dtg, oComprobante, filename);
                    }
                }
            }
            catch (Exception Error)
            {
                string MensajeTr = "Error cargando CFDI 4.0 ";
                if (Error.InnerException != null)
                {
                    MensajeTr += " " + Error.InnerException;
                }

                Dtg.Rows[n].ErrorText = MensajeTr;
                string Mensaje = Error.Message.ToString();
                if (Error.InnerException != null)
                {
                    Mensaje += Environment.NewLine + Error.InnerException.ToString();
                }
                BitacoraFX.Log(Mensaje);

            }
        }

        private void Procesar33(CFDI33.Comprobante oComprobante, string filename)
        {
            try
            {
                string TipoDeComprobanteTr = ObtenerTipoComprobante(filename);
                if (TipoDeComprobanteTr.ToUpper().Equals("P")){
                    //ProcesarComprobantePago33(filename);
                }
                if (TipoDeComprobanteTr.ToUpper().Equals("N"))
                {
                    //if (ExisteNomina(oComprobante))
                    //{
                    //    //Agregar a DtgNomina
                    //    AgregarFila(DtgNomina, oComprobante, filename);
                    //}
                }
                if (ExisteValesdedespensa(filename))
                {
                    return;
                }
                if (ExisteNomina(filename))
                {
                    return;
                }
                //Validar Nomina si no tiene permisos
                if (DescargaFX.Class.Globales.Usuario.Nomina)
                {
                    if (ExisteNomina(oComprobante))
                    {
                        //Agregar a DtgNomina
                        AgregarFila(DtgNomina, oComprobante, filename);
                    }
                    else
                    {
                        //Agregar a Dtg
                        AgregarFila(Dtg, oComprobante, filename);
                    }
                }
                else
                {
                    if (ExisteNomina(oComprobante))
                    {
                        return;
                    }
                    else
                    {
                        //Agregar a Dtg
                        AgregarFila(Dtg, oComprobante, filename);
                    }
                }
            }
            catch (Exception Error)
            {
                string MensajeTr = "Error cargando CFDI 3.3 ";
                if (Error.InnerException != null)
                {
                    MensajeTr += " " + Error.InnerException;
                }

                Dtg.Rows[n].ErrorText = MensajeTr;
                string Mensaje = Error.Message.ToString();
                if (Error.InnerException != null)
                {
                    Mensaje += Environment.NewLine + Error.InnerException.ToString();
                }
                BitacoraFX.Log(Mensaje);

            }
        }

        private void AgregarFila(DataGridView ODtg, CFDI33.Comprobante oComprobante, string filename)
        {
            n = this.Dtg.Rows.Add();

            ODtg.Rows[n].Tag = filename;
            ODtg.Rows[n].Cells["Version"].Value = oComprobante.Version;

            ODtg.Rows[n].Cells["RFCEmisor"].Value = oComprobante.Emisor.Rfc;
            ODtg.Rows[n].Cells["Emisor"].Value = oComprobante.Emisor.Nombre;
            ODtg.Rows[n].Cells["RegimenFiscal"].Value = oComprobante.Emisor.RegimenFiscal;

            ODtg.Rows[n].Cells["FormaPago"].Value = oComprobante.FormaPago;
            ODtg.Rows[n].Cells["SubTotal"].Value = oComprobante.SubTotal.ToString();
            ODtg.Rows[n].Cells["TipoCambio"].Value = oComprobante.TipoCambio.ToString();
            
            ODtg.Rows[n].Cells["rfcReceptorCFDI"].Value = oComprobante.Receptor.Rfc;
            ODtg.Rows[n].Cells["nombreReceptorCFDI"].Value = oComprobante.Receptor.Nombre;

            ODtg.Rows[n].Cells["Total"].Value = oComprobante.Total.ToString();
            ODtg.Rows[n].Cells["tipoComprobante"].Value = oComprobante.TipoDeComprobante;
            ODtg.Rows[n].Cells["Moneda"].Value = oComprobante.Moneda;

            ODtg.Rows[n].Cells["Folio"].Value = oComprobante.Folio;
            ODtg.Rows[n].Cells["Serie"].Value = oComprobante.Serie;

            ODtg.Rows[n].Cells["LugarExpedicion"].Value = oComprobante.LugarExpedicion;
            ODtg.Rows[n].Cells["MetodoPago"].Value = oComprobante.MetodoPago;

            //Cargar Impuestos
            AgregarImpuestos(n, ODtg, oComprobante);
            AgregarRelacionados(n, ODtg, oComprobante);
            AgregarConceptos(n, ODtg, oComprobante);

            //Determinar si es Timbrado
            if (oComprobante.Complemento != null)
            {
                foreach (CFDI33.ComprobanteComplemento complemento in oComprobante.Complemento)
                {
                    foreach (XmlElement elemento in complemento.Any)
                    {
                        //Timbrado XmlElement elemento
                        if (elemento.OuterXml.Contains("tfd:TimbreFiscalDigital"))
                        {
                            var doc = XDocument.Parse(elemento.OuterXml);
                            string uuidtr = doc.Root.Attributes("UUID").First().Value.ToString();
                            ODtg.Rows[n].Cells["uuidCFDI"].Value = uuidtr;
                            string FechaTimbradotr = doc.Root.Attributes("FechaTimbrado").First().Value.ToString();
                            ODtg.Rows[n].Cells["timbradoCFDI"].Value = FechaTimbradotr;
                            string RfcProvCertiftr = doc.Root.Attributes("RfcProvCertif").First().Value.ToString();
                            ODtg.Rows[n].Cells["RfcProvCertif"].Value = RfcProvCertiftr;
                        }
                    }
                }
            }
            ODtg.Rows[n].Cells["estadoCFDI"].Value = "Valido";
        }
        private string ObtenerTipoComprobante(string filename)
        {
            try
            {
                XmlDocument doc = new XmlDocument();
                doc.Load(filename);
                XmlNodeList comprobante = doc.GetElementsByTagName("cfdi:Comprobante");
                string TipoComprobanteTr = "";

                try
                {
                    TipoComprobanteTr = comprobante[0].Attributes["TipoDeComprobante"].Value;
                }
                catch
                {

                }

                return TipoComprobanteTr;

            }
            catch (Exception Error)
            {
                string MensajeTr = "Error cargando Obtener Tipo Comprobante ";
                if (Error.InnerException != null)
                {
                    MensajeTr += " " + Error.InnerException;
                }

                Dtg.Rows[n].ErrorText = MensajeTr;
                string Mensaje = Error.Message.ToString();
                if (Error.InnerException != null)
                {
                    Mensaje += Environment.NewLine + Error.InnerException.ToString();
                }
                BitacoraFX.Log(Mensaje);

            }

            return string.Empty;
        }
        private void AgregarImpuestos(int n, DataGridView oDtg, Comprobante oComprobante)
        {
            if (oComprobante.Impuestos != null)
            {
                oDtg.Rows[n].Cells["TotalImpuestosRetenidos"].Value = oComprobante.Impuestos.TotalImpuestosRetenidos;
                oDtg.Rows[n].Cells["TotalImpuestosTrasladados"].Value = oComprobante.Impuestos.TotalImpuestosTrasladados;
            }

            decimal Impuesto1 = 0;
            decimal Impuesto2 = 0;
            decimal Impuesto3 = 0;
            decimal Impuesto4 = 0;
            decimal Retencion1 = 0;
            decimal Retencion016 = 0;
            decimal Retencion010 = 0;
            decimal Retencion0 = 0;
            decimal Retencion012500 = 0;
            decimal Retencion0106667 = 0;
            //Cargar las Lineas
            foreach (CFDI33.ComprobanteConcepto Concepto in oComprobante.Conceptos)
            {
                if (Concepto.Impuestos != null)
                {
                    if (Concepto.Impuestos.Traslados != null)
                    {
                        foreach (CFDI33.ComprobanteConceptoImpuestosTraslado Traslado in Concepto.Impuestos.Traslados)
                        {
                            if (Traslado.TasaOCuota == decimal.Parse("0.16"))
                            {
                                Impuesto1 += Traslado.Importe;
                            }
                            else
                            {
                                if (Traslado.TasaOCuota == decimal.Parse("0.08"))
                                {
                                    Impuesto4 += Traslado.Importe;
                                }
                                else
                                {
                                    Impuesto3 += Traslado.Importe;
                                }
                            }
                        }
                    }

                    if (Concepto.Impuestos.Retenciones != null)
                    {
                        foreach (CFDI33.ComprobanteConceptoImpuestosRetencion Retencion in Concepto.Impuestos.Retenciones)
                        {
                            if (Retencion.TasaOCuota == decimal.Parse("0.04"))
                            {
                                Impuesto2 += Retencion.Importe;
                            }
                            else
                            {
                                if (Retencion.TasaOCuota == decimal.Parse("0.16"))
                                {
                                    Retencion016 += Retencion.Importe;
                                }
                                else
                                {
                                    if (Retencion.TasaOCuota == decimal.Parse("0.0"))
                                    {
                                        Retencion0 += Retencion.Importe;
                                    }
                                    else
                                    {
                                        if (Retencion.TasaOCuota == decimal.Parse("0.1"))
                                        {
                                            Retencion010 += Retencion.Importe;
                                        }
                                        else
                                        {
                                            if ((Retencion.TasaOCuota == decimal.Parse("0.106667")) ||
                                                (Retencion.TasaOCuota== decimal.Parse("0.1067"))
                                                )
                                            {
                                                Retencion0106667 += Retencion.Importe;
                                            }
                                            else
                                            {
                                                if (Retencion.TasaOCuota == decimal.Parse("0.106666"))
                                                {
                                                    Retencion0106667 += Retencion.Importe;
                                                }
                                                else
                                                {
                                                    if (Retencion.TasaOCuota == decimal.Parse("0.012500"))
                                                    {
                                                        Retencion012500 += Retencion.Importe;
                                                    }
                                                    else
                                                    {
                                                        Retencion1 += Retencion.Importe;
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                    
                }
            }
            oDtg.Rows[n].Cells["Retencion1"].Value = Retencion1;
            oDtg.Rows[n].Cells["Impuesto4"].Value = Impuesto4;
            oDtg.Rows[n].Cells["Impuesto3"].Value = Impuesto3;
            oDtg.Rows[n].Cells["Impuesto2"].Value = Impuesto2;
            oDtg.Rows[n].Cells["Impuesto1"].Value = Impuesto1;
            oDtg.Rows[n].Cells["Retencion016"].Value = Retencion016;
            oDtg.Rows[n].Cells["Retencion010"].Value = Retencion010;
            oDtg.Rows[n].Cells["Retencion0"].Value = Retencion0;
            oDtg.Rows[n].Cells["Retencion012500"].Value = Retencion012500;
            oDtg.Rows[n].Cells["Retencion0106667"].Value = Retencion0106667;

        }
        
        int n = 0;
        private void AgregarFila40(DataGridView ODtg, CFDI40.Comprobante oComprobante, string filename)
        {
            n = this.Dtg.Rows.Add();

            ODtg.Rows[n].Tag = filename;
            ODtg.Rows[n].Cells["Version"].Value = oComprobante.Version;
            ODtg.Rows[n].Cells["RFCEmisor"].Value = oComprobante.Emisor.Rfc;
            ODtg.Rows[n].Cells["Emisor"].Value = oComprobante.Emisor.Nombre;
            ODtg.Rows[n].Cells["FormaPago"].Value = oComprobante.FormaPago;
            ODtg.Rows[n].Cells["SubTotal"].Value = oComprobante.SubTotal.ToString();
            ODtg.Rows[n].Cells["TipoCambio"].Value = oComprobante.TipoCambio.ToString();

            ODtg.Rows[n].Cells["LugarExpedicion"].Value = oComprobante.LugarExpedicion;
            ODtg.Rows[n].Cells["MetodoPago"].Value = oComprobante.MetodoPago;
            ODtg.Rows[n].Cells["RegimenFiscal"].Value = oComprobante.Emisor.RegimenFiscal;
            
            ODtg.Rows[n].Cells["rfcReceptorCFDI"].Value = oComprobante.Receptor.Rfc;
            ODtg.Rows[n].Cells["nombreReceptorCFDI"].Value = oComprobante.Receptor.Nombre;

            
            ODtg.Rows[n].Cells["Total"].Value = oComprobante.Total.ToString();
            ODtg.Rows[n].Cells["tipoComprobante"].Value = oComprobante.TipoDeComprobante;
            ODtg.Rows[n].Cells["Moneda"].Value = oComprobante.Moneda;

            ODtg.Rows[n].Cells["Folio"].Value = oComprobante.Folio;
            ODtg.Rows[n].Cells["Serie"].Value = oComprobante.Serie;

            //Cargar Impuestos
            AgregarImpuestos40(n, ODtg, oComprobante);
            AgregarRelacionados40(n, ODtg, oComprobante);
            AgregarConceptos40(n, ODtg, oComprobante);

            //Determinar si es Timbrado
            if (oComprobante.Complemento != null)
            {
                foreach (XmlElement elemento in oComprobante.Complemento.Any)
                {
                    //Timbrado XmlElement elemento
                    if (elemento.OuterXml.Contains("tfd:TimbreFiscalDigital"))
                    {
                        var doc = XDocument.Parse(elemento.OuterXml);
                        string uuidtr = doc.Root.Attributes("UUID").First().Value.ToString();
                        ODtg.Rows[n].Cells["uuidCFDI"].Value = uuidtr;
                        string FechaTimbradotr = doc.Root.Attributes("FechaTimbrado").First().Value.ToString();
                        ODtg.Rows[n].Cells["timbradoCFDI"].Value = FechaTimbradotr;
                        string RfcProvCertiftr = doc.Root.Attributes("RfcProvCertif").First().Value.ToString();
                        ODtg.Rows[n].Cells["RfcProvCertif"].Value = RfcProvCertiftr;
                    }
                }
            }
            ODtg.Rows[n].Cells["estadoCFDI"].Value = "Valido";
        }
        private void AgregarRelacionados(int n, DataGridView oDtg, CFDI33.Comprobante oComprobante)
        {
            if (oComprobante.CfdiRelacionados != null)
            {
                oDtg.Rows[n].Cells["TipoRelacion"].Value = oComprobante.CfdiRelacionados.TipoRelacion;
                string RelacionadosTr = string.Empty;
                foreach (CFDI33.ComprobanteCfdiRelacionadosCfdiRelacionado Linea in oComprobante.CfdiRelacionados.CfdiRelacionado)
                {
                    if (!String.IsNullOrEmpty(RelacionadosTr))
                    {
                        RelacionadosTr += ",";
                    }
                    RelacionadosTr += Linea.UUID;
                }
                oDtg.Rows[n].Cells["Relacionados"].Value = RelacionadosTr;
            }
        }

        private void AgregarConceptos(int n, DataGridView oDtg, Comprobante oComprobante)
        {
            oDtg.Rows[n].Cells["Conceptos"].Value = string.Empty;
            string ConceptosTr = string.Empty;
            //Cargar las Lineas
            foreach (CFDI33.ComprobanteConcepto Concepto in oComprobante.Conceptos)
            {
                //ClaveProdServ: 11151500  Cantidad: 100  valorUnitario: 14.27  Importe: 1427.00  Descripción: Almohadilla de fibra 6"x9" marron g.extr
                string ConceptoTr = "ClaveProdServ: " + Concepto.ClaveProdServ.ToString()
                    + " " + " Cantidad:" + Concepto.Cantidad.ToString()
                    + " " + " ClaveUnidad:" + Concepto.ClaveUnidad.ToString()
                    + " " + " valorUnitario:" + Concepto.ValorUnitario.ToString()
                    + " " + " Importe:" + Concepto.Importe.ToString()
                    + " " + " Descripción:" + Concepto.Descripcion.ToString()
                    + Environment.NewLine;
                ConceptosTr += ConceptoTr;
            }
            oDtg.Rows[n].Cells["Conceptos"].Value = ConceptosTr;
        }

        private void AgregarConceptos40(int n, DataGridView oDtg, CFDI40.Comprobante oComprobante)
        {
            oDtg.Rows[n].Cells["Conceptos"].Value = string.Empty;
            String ConceptosTr = String.Empty;
            //Cargar las Lineas
            foreach (CFDI40.ComprobanteConcepto Concepto in oComprobante.Conceptos)
            {
                if (!String.IsNullOrEmpty(ConceptosTr))
                {
                    ConceptosTr+= Environment.NewLine;
                }
                //ClaveProdServ: 11151500  Cantidad: 100  valorUnitario: 14.27  Importe: 1427.00  Descripción: Almohadilla de fibra 6"x9" marron g.extr
                string ConceptoTr = "ClaveProdServ: " + Concepto.ClaveProdServ.ToString()
                    + " " + " Cantidad:" + Concepto.Cantidad.ToString()
                    + " " + " ClaveUnidad:" + Concepto.ClaveUnidad.ToString()
                    + " " + " valorUnitario:" + Concepto.ValorUnitario.ToString()
                    + " " + " Importe:" + Concepto.Importe.ToString()
                    + " " + " Descripción:" + Concepto.Descripcion.ToString();

                ConceptosTr += ConceptoTr;
            }

            oDtg.Rows[n].Cells["Conceptos"].Value = ConceptosTr;
        }


        private void AgregarRelacionados40(int n, DataGridView oDtg, CFDI40.Comprobante oComprobante)
        {
            if (oComprobante.CfdiRelacionados != null)
            {
                string RelacionadosTr = string.Empty;
                foreach (CFDI40.ComprobanteCfdiRelacionados Linea in oComprobante.CfdiRelacionados)
                {
                    oDtg.Rows[n].Cells["TipoRelacion"].Value = Linea.TipoRelacion;
                    foreach (CFDI40.ComprobanteCfdiRelacionadosCfdiRelacionado CfdiLinea in Linea.CfdiRelacionado)
                    {
                        if (!String.IsNullOrEmpty(RelacionadosTr))
                        {
                            RelacionadosTr += ",";
                        }
                        RelacionadosTr += CfdiLinea.UUID;
                    }
                    oDtg.Rows[n].Cells["Relacionados"].Value = RelacionadosTr;
                }
                
            }

        }

        private void AgregarImpuestos40(int n, DataGridView oDtg, CFDI40.Comprobante oComprobante)
        {
            if (oComprobante.Impuestos != null)
            {
                oDtg.Rows[n].Cells["TotalImpuestosRetenidos"].Value = oComprobante.Impuestos.TotalImpuestosRetenidos;
                oDtg.Rows[n].Cells["TotalImpuestosTrasladados"].Value = oComprobante.Impuestos.TotalImpuestosTrasladados;
            }
            
            decimal Impuesto1 = 0;
            decimal Impuesto2 = 0;
            decimal Impuesto3 = 0;
            decimal Retencion1 = 0;
            decimal Impuesto4 = 0;
            decimal Retencion016 = 0;
            decimal Retencion010 = 0;
            decimal Retencion0 = 0;
            decimal Retencion012500 = 0;
            decimal Retencion0106667 = 0;
            //Cargar las Lineas
            foreach (CFDI40.ComprobanteConcepto Concepto in oComprobante.Conceptos)
            {
                if (Concepto.Impuestos != null)
                {
                    if (Concepto.Impuestos.Traslados != null)
                    {
                        foreach (CFDI40.ComprobanteConceptoImpuestosTraslado Traslado in Concepto.Impuestos.Traslados)
                        {
                            if (Traslado.TasaOCuota == decimal.Parse("0.16"))
                            {
                                Impuesto1 += Traslado.Importe;
                            }
                            else
                            {
                                if (Traslado.TasaOCuota == decimal.Parse("0.08"))
                                {
                                    Impuesto4 += Traslado.Importe;
                                }
                                else
                                {
                                    Impuesto3 += Traslado.Importe;
                                }
                            }
                        }
                    }
                    if (Concepto.Impuestos.Retenciones != null)
                    {
                        foreach (CFDI40.ComprobanteConceptoImpuestosRetencion Retencion in Concepto.Impuestos.Retenciones)
                        {
                            if (Retencion.TasaOCuota == decimal.Parse("0.04"))
                            {
                                Impuesto2 += Retencion.Importe;
                            }
                            else
                            {
                                if (Retencion.TasaOCuota == decimal.Parse("0.16"))
                                {
                                    Retencion016 += Retencion.Importe;
                                }
                                else
                                {
                                    if (Retencion.TasaOCuota == decimal.Parse("0.0"))
                                    {
                                        Retencion0 += Retencion.Importe;
                                    }
                                    else
                                    {
                                        if (Retencion.TasaOCuota == decimal.Parse("0.1"))
                                        {
                                            Retencion010 += Retencion.Importe;
                                        }
                                        else
                                        {
                                            if (Retencion.TasaOCuota == decimal.Parse("0.106667"))
                                            {
                                                Retencion0106667 += Retencion.Importe;
                                            }
                                            else
                                            {
                                                if ((Retencion.TasaOCuota == decimal.Parse("0.106667")) ||
                                                    (Retencion.TasaOCuota == decimal.Parse("0.1067"))
                                                    )
                                                { 
                                                    Retencion0106667 += Retencion.Importe;
                                                }
                                                else
                                                {
                                                    if (Retencion.TasaOCuota == decimal.Parse("0.012500"))
                                                    {
                                                        Retencion012500 += Retencion.Importe;
                                                    }
                                                    else
                                                    {
                                                        Retencion1 += Retencion.Importe;
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                    
                }
            }
            oDtg.Rows[n].Cells["Impuesto4"].Value = Impuesto4;
            oDtg.Rows[n].Cells["Retencion1"].Value = Retencion1;
            oDtg.Rows[n].Cells["Impuesto3"].Value = Impuesto3;
            oDtg.Rows[n].Cells["Impuesto1"].Value = Impuesto1;
            oDtg.Rows[n].Cells["Impuesto2"].Value = Impuesto2;
            oDtg.Rows[n].Cells["Retencion016"].Value = Retencion016;
            oDtg.Rows[n].Cells["Retencion010"].Value = Retencion010;
            oDtg.Rows[n].Cells["Retencion0"].Value = Retencion0;
            oDtg.Rows[n].Cells["Retencion012500"].Value = Retencion012500;
            oDtg.Rows[n].Cells["Retencion0106667"].Value = Retencion0106667;
        }

        private bool ExisteValesdedespensa(string filename)
        {
            XmlDocument doc = new XmlDocument();
            doc.Load(filename);
            XmlNodeList Valesdedespensa = doc.GetElementsByTagName("valesdedespensa:ValesDeDespensa");
            if (Valesdedespensa!=null)
            {
                if (Valesdedespensa.Count==0)
                {
                    return false;
                }
                else
                {
                    return true;
                }
            }
            return false;
        }

        private bool ExisteNomina(string filename)
        {
            XmlDocument doc = new XmlDocument();
            doc.Load(filename);
            XmlNodeList Nodo = doc.GetElementsByTagName("nomina12:Nomina");
            if (Nodo != null)
            {
                if (Nodo.Count == 0)
                {
                    return false;
                }
                else
                {
                    return true;
                }
            }
            return false;
        }
        private bool ExisteNomina40(CFDI40.Comprobante oComprobante)
        {
            if (oComprobante.Complemento != null)
            {
                foreach (XmlElement elemento in oComprobante.Complemento.Any)
                {
                    //Timbrado XmlElement elemento
                    if (elemento.OuterXml.Contains("nomina12:Nomina"))
                    {
                        return true;
                    }
                }
            }
            return false;
        }


        private bool ExisteNomina(Comprobante oComprobante)
        {
            if (oComprobante.Complemento != null)
            {
                foreach (CFDI33.ComprobanteComplemento complemento in oComprobante.Complemento)
                {
                    foreach (XmlElement elemento in complemento.Any)
                    {
                        //Timbrado XmlElement elemento
                        if (elemento.OuterXml.Contains("nomina12:Nomina"))
                        {
                            return true;
                        }
                    }
                }
            }
            return false;
        }

        private void BtnExportar_Click(object sender, EventArgs e)
        {
            Exportar();
        }
        private void Exportar()
        {
            if (tabControl1.SelectedTab == this.tabPage1)
            {
                DescargaFX.Class.ExcelFX.exportar(Dtg, "CFDI");
            }
            if (tabControl1.SelectedTab == this.tabPage2)
            {
                DescargaFX.Class.ExcelFX.exportar(DtgNomina, "CFDINomina");
            }
        }

        private void exportarAExcelToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            Exportar();
        }

        private void Dtg_MouseClick(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Right)
            {
                contextMenuStrip2.Show(Dtg, new Point(e.X, e.Y));
            }
        }

        private void verXMLToolStripMenuItem_Click(object sender, EventArgs e)
        {
            VerXML();
        }

        private void VerXML()
        {
            Dtg.EndEdit();
            if (Dtg.SelectedCells.Count > 0)
            {
                DialogResult Resultado = MessageBox.Show("¿Desea ver las seleccionada?"
                    , Application.ProductName + "-" + Application.ProductVersion
                    , MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                if (Resultado != DialogResult.Yes)
                {
                    return;
                }

                foreach (DataGridViewRow drv in Dtg.SelectedRows)
                {
                    try
                    {
                        string ArchivoTr = drv.Tag.ToString();
                        if (File.Exists(ArchivoTr))
                        {
                            System.Diagnostics.Process.Start("iexplore.exe", ArchivoTr);
                        }
                    }
                    catch (Exception e)
                    {
                        Bitacora.Log(e.Message.ToString());
                    }
                }
            }
            else
            {
                MessageBox.Show("Seleccione alguna línea.", Application.ProductName + "-" + Application.ProductVersion, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            SeleccionarArchivos();
        }

        private void SeleccionarArchivos()
        {
            OpenFileDialog openFileDialog1 = new OpenFileDialog
            {
                Title = "CFDI ",

                CheckFileExists = true,
                CheckPathExists = true,

                DefaultExt = "xml",
                Filter = "xml o cfdi (*.xml)|*.xml",
                FilterIndex = 2,
                RestoreDirectory = true,

                ReadOnlyChecked = true,
                ShowReadOnly = true
            };
            openFileDialog1.Multiselect = true;
            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                foreach (String file in openFileDialog1.FileNames)
                {
                    try
                    {
                        ProcesarCFDI(file);
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show("Error: " + ex.Message);
                    }

                }
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            SeleccionarDirectorio();
        }

        private void SeleccionarDirectorio()
        {
            OpenFileDialog folderBrowser = new OpenFileDialog();
            // Set validate names and check file exists to false otherwise windows will
            // not let you select "Folder Selection."
            folderBrowser.ValidateNames = false;
            folderBrowser.CheckFileExists = false;
            folderBrowser.CheckPathExists = true;
            // Always default to Folder Selection.
            folderBrowser.FileName = "Folder Selection.";
            if (folderBrowser.ShowDialog() == DialogResult.OK)
            {
                string folderPath = Path.GetDirectoryName(folderBrowser.FileName);
                CargarCFDI(folderPath);
            }
        }
    }
}
