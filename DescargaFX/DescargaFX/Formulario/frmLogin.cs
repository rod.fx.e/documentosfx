﻿using DescargaFX.Class;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DescargaFX.Formulario
{
    public partial class frmLogin: Form
    {
        public frmLogin()
        {
            InitializeComponent();
        }

        private void BtnExportar_Click(object sender, EventArgs e)
        {
            Verificar();
        }

        private void Verificar()
        {
            if (System.Diagnostics.Debugger.IsAttached)
            {
                Globales.Usuario.Nomina = true;
                Globales.Usuario.Email = "rod@fxgroup.com.mx";
                DialogResult = DialogResult.OK;
                return;
            }

            if (!Globales.IsValid(Usuario.Text))
            {
                return;
            }

            Globales.Usuario = new cUsuario();
            Globales.Usuario.Nomina = false;
            Globales.Usuario.Email = Usuario.Text;
            if (Usuario.Text.Equals("patricia.vazquez@bradfordcompany.com"))
            {
                Globales.Usuario.Nomina = true;
            }
            if (Usuario.Text.Equals("rod@fxgroup.com.mx"))
            {
                Globales.Usuario.Nomina = true;
            }
            if (Usuario.Text.Equals("Eleazar.Cardona@bradfordcompany.com"))
            {
                Globales.Usuario.Nomina = true;
            }





            if (!Password.Text.Equals("Brad1")
                & !Password.Text.Equals("nom"))
            {
                MessageBox.Show(this, "Contraseña incorrecta", Application.ProductName, MessageBoxButtons.OK
                    , MessageBoxIcon.Error);
                return;
            }

            DialogResult = DialogResult.OK;
        }

        private void Usuario_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)13)
            {
                Password.Focus();
            }
        }

        private void Password_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)13)
            {
                Verificar();
            }
        }
    }
}
