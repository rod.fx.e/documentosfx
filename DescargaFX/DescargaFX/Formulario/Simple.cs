﻿using CFDI33;
using DescargaFX.Class;
using Generales;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml;
using System.Xml.Linq;
using System.Xml.Serialization;

namespace DescargaFX.Formulario
{
    public partial class Simple : Form
    {
        static byte[] pfx;


        static string password = "abc123";

        static string urlAutentica = "https://cfdidescargamasivasolicitud.clouda.sat.gob.mx/Autenticacion/Autenticacion.svc";
        static string urlAutenticaAction = "http://DescargaMasivaTerceros.gob.mx/IAutenticacion/Autentica";

        static string urlSolicitud = "https://cfdidescargamasivasolicitud.clouda.sat.gob.mx/SolicitaDescargaService.svc";
        static string urlSolicitudAction = "http://DescargaMasivaTerceros.sat.gob.mx/ISolicitaDescargaService/SolicitaDescarga";

        static string urlVerificarSolicitud = "https://cfdidescargamasivasolicitud.clouda.sat.gob.mx/VerificaSolicitudDescargaService.svc";
        static string urlVerificarSolicitudAction = "http://DescargaMasivaTerceros.sat.gob.mx/IVerificaSolicitudDescargaService/VerificaSolicitudDescarga";

        static string urlDescargarSolicitud = "https://cfdidescargamasiva.clouda.sat.gob.mx/DescargaMasivaService.svc";
        static string urlDescargarSolicitudAction = "http://DescargaMasivaTerceros.sat.gob.mx/IDescargaMasivaTercerosService/Descargar";

        X509Certificate2 certifcate;

        public Simple()
        {
            InitializeComponent();
            CargarPFX();
        }
        private void CargarPFX()
        {
            var ext = new List<string> { "pfx" };
            var myFiles = Directory
                .EnumerateFiles(AppDomain.CurrentDomain.BaseDirectory, "*.*", SearchOption.AllDirectories)
                .Where(s => ext.Contains(Path.GetExtension(s).TrimStart('.').ToLowerInvariant()));

            if (myFiles != null)
            {
                RfcSolicitante.Items.Clear();
                foreach (var Item in myFiles)
                {
                    string filename = Path.GetFileName(Item);
                    filename = filename.Replace(".pfx", "");
                    RfcSolicitante.Items.Add(filename);
                    RfcSolicitante.SelectedIndex = 0;
                }


            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            Autentificar();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            GenerarSolicitud(certifcate, Autorization);
        }

        #region Logica
        private void ValidarSolicitud(X509Certificate2 certifcate, string autorization, string idSolicitud)
        {
            Log.Text+= "Espere mientras se procesa...." + Environment.NewLine;
            VerificaSolicitud verifica = new VerificaSolicitud(urlVerificarSolicitud, urlVerificarSolicitudAction);
            verifica.Generate(certifcate, RfcSolicitante.Text, idSolicitud);
            verifica.Send(autorization);
            Log.Text += DateTime.Now.ToLongTimeString() + Environment.NewLine + "EstadoSolicitud: " + verifica.EstadoSolicitud.ToString() + " "
                + "CodEstatus: " + verifica.CodEstatus.ToString() + " "
                + "CodigoEstadoSolicitud: " + verifica.CodigoEstadoSolicitud.ToString() + " "
                + "NumeroCFDIs: " + verifica.NumeroCFDIs.ToString();

            //Token Invalido actualizarñp
            if (verifica.CodEstatus == 300)
            {
                Log.Text += Environment.NewLine + "Mensaje: Token Invalido";
                Autentificar();
            }
            else
            {
                if (verifica.Mensaje != null)
                {
                    Log.Text += Environment.NewLine + "Mensaje: " + verifica.Mensaje.ToString() + " " + DateTime.Now.ToLongTimeString();
                }
                Log.Text += Environment.NewLine + "EstadoSolicitud: " + verifica.EstadoSolicitud.ToString() + " " + DateTime.Now.ToLongTimeString();
                Log.Text += Environment.NewLine + "Solicitud Codigo: " + verifica.CodEstatus.ToString() + " " + DateTime.Now.ToLongTimeString();
                Log.SelectionStart = Log.Text.Length;
                Log.ScrollToCaret();                
            }
        }

        private void GenerarSolicitud(X509Certificate2 certifcate, string autorization)
        {
            try
            {
                SolicitudBuscar.Text = String.Empty;
                string RfcEmisor = "";
                string RfcReceptor = "";

                if (rdTipo1.Checked)
                {
                    RfcReceptor = RfcSolicitante.Text;
                }
                if (rdTipo2.Checked)
                {
                    RfcEmisor = RfcSolicitante.Text;
                }

                SolicitudFX solicitud = new SolicitudFX(urlSolicitud, urlSolicitudAction);


                string xmlSolicitud = solicitud.Generate(certifcate, RfcEmisor, RfcReceptor
                    , RfcSolicitante.Text, FechaInicial.Value.ToString("yyyy-MM-dd")
                    , FechaFinal.Value.ToString("yyyy-MM-dd"));


                string solicitudTr = solicitud.Send(autorization);

                if (!String.IsNullOrEmpty(solicitudTr))
                {
                    SolicitudBuscar.Text = solicitudTr;
                }
                else
                {
                    string mensaje = "CodEstatus: " + solicitud.CodEstatus + Environment.NewLine + "Mensaje: " + solicitud.Mensaje;
                    SolicitudBuscar.Text+= DateTime.Now.ToString("dd/MM/yyy HH:mm:ss") + " Solicitud no realizada " + mensaje + Environment.NewLine;
                }
            }
            catch (Exception Error)
            {
                string Mensaje = Error.Message.ToString();
                if (Error.InnerException != null)
                {
                    Mensaje += Environment.NewLine + Error.InnerException.ToString();
                }
                SolicitudBuscar.Text += Mensaje;
                BitacoraFX.Log(Mensaje);

            }
        }

        #endregion

        private void RfcSolicitante_SelectedIndexChanged(object sender, EventArgs e)
        {
            LeerPFX();
        }

        private bool LeerPFX()
        {
            string file = RfcSolicitante.Text + ".pfx";
            if (File.Exists(file))
            {
                pfx = File.ReadAllBytes(file);
                //Obtener Certificados
                certifcate = ObtenerX509Certificado(pfx);
                return Autentificar();
            }
            return false;
        }

        private static X509Certificate2 ObtenerX509Certificado(byte[] pfx)
        {
            return new X509Certificate2(pfx, password, X509KeyStorageFlags.MachineKeySet |
                            X509KeyStorageFlags.PersistKeySet |
                            X509KeyStorageFlags.Exportable);
        }
        private static string ObtenerToken(X509Certificate2 certifcate)
        {
            Autenticacion service = new Autenticacion(urlAutentica, urlAutenticaAction);
            service.Generate(certifcate);
            return service.Send();
        }
        String Autorization = String.Empty;
        private bool Autentificar()
        {
            try
            {
                //Obtener Token
                string token = ObtenerToken(certifcate);
                Autorization = String.Format("WRAP access_token=\"{0}\"", System.Net.WebUtility.UrlDecode(token));
                this.DatoAutorization.Text = Autorization;
                Console.WriteLine("Token: " + token);
                return true;
            }
            catch (Exception Error)
            {
                Log.Text+= DateTime.Now.ToString() + " No puede ingresar al SAT " + Environment.NewLine
                    + Error.Message;
                ;
                string Mensaje = Error.Message.ToString();
                if (Error.InnerException != null)
                {
                    Mensaje += Environment.NewLine + Error.InnerException.ToString();
                }
                BitacoraFX.Log(Mensaje);

            }
            return false;
        }

        private void button4_Click(object sender, EventArgs e)
        {
            ValidarSolicitud(certifcate, DatoAutorization.Text, SolicitudBuscar.Text);
        }
    }
}
