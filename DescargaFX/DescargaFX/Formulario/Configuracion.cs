﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.Entity.Validation;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DescargaFX.Formulario
{
    public partial class Configuracion : Form
    {
        public ModeloDescarga.DescargaConfiguracion Registro;

        public Configuracion()
        {
            InitializeComponent();
            Cargar();
        }

        private void btnAddItem_Click(object sender, EventArgs e)
        {
            Guardar();
        }

        #region Metodos
        private void Cargar()
        {
            ModeloDescarga.DescargaFXContainer Db = new ModeloDescarga.DescargaFXContainer();
            Registro = Db.DescargaConfiguracionSet.FirstOrDefault();
            if (Registro == null)
            {
                return;
            }
            PasswordZip.Text = Registro.PasswordZip;
            VerificarAutomatica.Checked = Registro.VerificarAutomatica;
            Db.Dispose();
        }

        private void Guardar()
        {
            try
            {
                if (String.IsNullOrEmpty(PasswordZip.Text))
                {
                    PasswordZip.SelectAll();
                    PasswordZip.Focus();
                    MessageBox.Show(this, "El password no debe estar vacio", Application.ProductName
                            , MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }
                if (String.IsNullOrEmpty(PasswordZipConfirmacion.Text))
                {
                    PasswordZipConfirmacion.SelectAll();
                    PasswordZipConfirmacion.Focus();
                    MessageBox.Show(this, "La confirmación de password no debe estar vacia", Application.ProductName
                            , MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }
                if (PasswordZipConfirmacion.Text.Trim()!= PasswordZip.Text.Trim())
                {
                    PasswordZip.SelectAll();
                    PasswordZip.Focus();
                    MessageBox.Show(this, "La confirmació no coincide", Application.ProductName
                            , MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }

                ModeloDescarga.DescargaFXContainer Db = new ModeloDescarga.DescargaFXContainer();
                ModeloDescarga.DescargaConfiguracion RegistroTmp = new ModeloDescarga.DescargaConfiguracion();

                RegistroTmp.PasswordZip = PasswordZip.Text.Trim();
                RegistroTmp.VerificarAutomatica = VerificarAutomatica.Checked;
                if (Registro == null)
                {
                    Db.DescargaConfiguracionSet.Add(RegistroTmp);
                }
                else
                {
                    RegistroTmp = Db.DescargaConfiguracionSet.FirstOrDefault();
                    RegistroTmp.PasswordZip = PasswordZip.Text;
                    RegistroTmp.VerificarAutomatica = VerificarAutomatica.Checked;
                    Db.DescargaConfiguracionSet.Attach(RegistroTmp);
                    Db.Entry(RegistroTmp).State = System.Data.Entity.EntityState.Modified;
                }

                Db.SaveChanges();
                Registro = RegistroTmp;

            }
            catch (DbEntityValidationException e)
            {
                foreach (var eve in e.EntityValidationErrors)
                {
                    foreach (var ve in eve.ValidationErrors)
                    {
                        MessageBox.Show(this, ve.PropertyName + " Error: " + ve.ErrorMessage, Application.ProductName
                            , MessageBoxButtons.OK, MessageBoxIcon.Error);
                        return;
                    }
                }
            }
        }
        #endregion

    }
}
