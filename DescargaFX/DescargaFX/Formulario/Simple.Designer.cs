﻿
namespace DescargaFX.Formulario
{
    partial class Simple
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.button2 = new System.Windows.Forms.Button();
            this.SolicitudBuscar = new System.Windows.Forms.TextBox();
            this.button3 = new System.Windows.Forms.Button();
            this.DatoAutorization = new System.Windows.Forms.TextBox();
            this.button4 = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.RfcSolicitante = new System.Windows.Forms.ComboBox();
            this.rdTipo2 = new System.Windows.Forms.RadioButton();
            this.rdTipo1 = new System.Windows.Forms.RadioButton();
            this.label5 = new System.Windows.Forms.Label();
            this.FechaFinal = new System.Windows.Forms.DateTimePicker();
            this.FechaInicial = new System.Windows.Forms.DateTimePicker();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.Log = new System.Windows.Forms.TextBox();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // button2
            // 
            this.button2.BackColor = System.Drawing.SystemColors.Control;
            this.button2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button2.Image = global::DescargaFX.Properties.Resources.Guardar;
            this.button2.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button2.Location = new System.Drawing.Point(12, 94);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(220, 23);
            this.button2.TabIndex = 482;
            this.button2.Text = "Solicitar";
            this.button2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.button2.UseVisualStyleBackColor = false;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // SolicitudBuscar
            // 
            this.SolicitudBuscar.Location = new System.Drawing.Point(238, 96);
            this.SolicitudBuscar.Name = "SolicitudBuscar";
            this.SolicitudBuscar.Size = new System.Drawing.Size(475, 20);
            this.SolicitudBuscar.TabIndex = 484;
            // 
            // button3
            // 
            this.button3.BackColor = System.Drawing.SystemColors.Control;
            this.button3.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button3.Image = global::DescargaFX.Properties.Resources.Guardar;
            this.button3.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button3.Location = new System.Drawing.Point(12, 65);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(220, 23);
            this.button3.TabIndex = 483;
            this.button3.Text = "Regenerar Token de Autorización";
            this.button3.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.button3.UseVisualStyleBackColor = false;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // DatoAutorization
            // 
            this.DatoAutorization.Location = new System.Drawing.Point(238, 67);
            this.DatoAutorization.Name = "DatoAutorization";
            this.DatoAutorization.Size = new System.Drawing.Size(475, 20);
            this.DatoAutorization.TabIndex = 484;
            // 
            // button4
            // 
            this.button4.BackColor = System.Drawing.SystemColors.Control;
            this.button4.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button4.Image = global::DescargaFX.Properties.Resources.Guardar;
            this.button4.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button4.Location = new System.Drawing.Point(12, 123);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(220, 23);
            this.button4.TabIndex = 481;
            this.button4.Text = "Verificar y Descargar";
            this.button4.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.button4.UseVisualStyleBackColor = false;
            this.button4.Click += new System.EventHandler(this.button4_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.RfcSolicitante);
            this.groupBox1.Controls.Add(this.rdTipo2);
            this.groupBox1.Controls.Add(this.rdTipo1);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.FechaFinal);
            this.groupBox1.Controls.Add(this.FechaInicial);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Location = new System.Drawing.Point(12, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(701, 47);
            this.groupBox1.TabIndex = 485;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Filtros";
            // 
            // RfcSolicitante
            // 
            this.RfcSolicitante.FormattingEnabled = true;
            this.RfcSolicitante.Location = new System.Drawing.Point(92, 13);
            this.RfcSolicitante.Name = "RfcSolicitante";
            this.RfcSolicitante.Size = new System.Drawing.Size(185, 21);
            this.RfcSolicitante.TabIndex = 6;
            this.RfcSolicitante.SelectedIndexChanged += new System.EventHandler(this.RfcSolicitante_SelectedIndexChanged);
            // 
            // rdTipo2
            // 
            this.rdTipo2.AutoSize = true;
            this.rdTipo2.Checked = true;
            this.rdTipo2.Location = new System.Drawing.Point(283, 15);
            this.rdTipo2.Name = "rdTipo2";
            this.rdTipo2.Size = new System.Drawing.Size(56, 17);
            this.rdTipo2.TabIndex = 4;
            this.rdTipo2.TabStop = true;
            this.rdTipo2.Text = "Emisor";
            this.rdTipo2.UseVisualStyleBackColor = true;
            // 
            // rdTipo1
            // 
            this.rdTipo1.AutoSize = true;
            this.rdTipo1.Location = new System.Drawing.Point(345, 15);
            this.rdTipo1.Name = "rdTipo1";
            this.rdTipo1.Size = new System.Drawing.Size(69, 17);
            this.rdTipo1.TabIndex = 4;
            this.rdTipo1.Text = "Receptor";
            this.rdTipo1.UseVisualStyleBackColor = true;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(6, 17);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(80, 13);
            this.label5.TabIndex = 1;
            this.label5.Text = "RFC Solicitante";
            // 
            // FechaFinal
            // 
            this.FechaFinal.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.FechaFinal.Location = new System.Drawing.Point(595, 13);
            this.FechaFinal.Name = "FechaFinal";
            this.FechaFinal.Size = new System.Drawing.Size(97, 20);
            this.FechaFinal.TabIndex = 3;
            this.FechaFinal.Value = new System.DateTime(2020, 11, 30, 0, 0, 0, 0);
            // 
            // FechaInicial
            // 
            this.FechaInicial.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.FechaInicial.Location = new System.Drawing.Point(458, 13);
            this.FechaInicial.Name = "FechaInicial";
            this.FechaInicial.Size = new System.Drawing.Size(101, 20);
            this.FechaInicial.TabIndex = 3;
            this.FechaInicial.Value = new System.DateTime(2020, 11, 1, 0, 0, 0, 0);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(420, 17);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(32, 13);
            this.label3.TabIndex = 1;
            this.label3.Text = "Inicio";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(568, 17);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(21, 13);
            this.label4.TabIndex = 1;
            this.label4.Text = "Fin";
            // 
            // Log
            // 
            this.Log.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.Log.Location = new System.Drawing.Point(12, 152);
            this.Log.Multiline = true;
            this.Log.Name = "Log";
            this.Log.Size = new System.Drawing.Size(701, 349);
            this.Log.TabIndex = 486;
            // 
            // Simple
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(727, 513);
            this.Controls.Add(this.Log);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.button4);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.DatoAutorization);
            this.Controls.Add(this.SolicitudBuscar);
            this.Controls.Add(this.button3);
            this.Name = "Simple";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Simple";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.TextBox SolicitudBuscar;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.TextBox DatoAutorization;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.ComboBox RfcSolicitante;
        private System.Windows.Forms.RadioButton rdTipo2;
        private System.Windows.Forms.RadioButton rdTipo1;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.DateTimePicker FechaFinal;
        private System.Windows.Forms.DateTimePicker FechaInicial;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox Log;
    }
}