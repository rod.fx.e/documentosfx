﻿using DescargaFX.Class;
using Generales;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.Entity.Validation;
using System.Drawing;
using System.Globalization;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
//https://bitsbyta.blogspot.com/2011/01/lock-and-unlock-folder-cnet.html
//C# protect folder password

namespace DescargaFX
{
    public partial class frmPrincipalNuevo2 : Form
    {
        static byte[] pfx;


        static string password = "abc123";

        static string urlAutentica = "https://cfdidescargamasivasolicitud.clouda.sat.gob.mx/Autenticacion/Autenticacion.svc";
        static string urlAutenticaAction = "http://DescargaMasivaTerceros.gob.mx/IAutenticacion/Autentica";

        static string urlSolicitud = "https://cfdidescargamasivasolicitud.clouda.sat.gob.mx/SolicitaDescargaService.svc";
        static string urlSolicitudAction = "http://DescargaMasivaTerceros.sat.gob.mx/ISolicitaDescargaService/SolicitaDescarga";

        static string urlVerificarSolicitud = "https://cfdidescargamasivasolicitud.clouda.sat.gob.mx/VerificaSolicitudDescargaService.svc";
        static string urlVerificarSolicitudAction = "http://DescargaMasivaTerceros.sat.gob.mx/IVerificaSolicitudDescargaService/VerificaSolicitudDescarga";

        static string urlDescargarSolicitud = "https://cfdidescargamasiva.clouda.sat.gob.mx/DescargaMasivaService.svc";
        static string urlDescargarSolicitudAction = "http://DescargaMasivaTerceros.sat.gob.mx/IDescargaMasivaTercerosService/Descargar";

        X509Certificate2 certifcate;

        public frmPrincipalNuevo2(string usuario = "")
        {

            if (String.IsNullOrEmpty(usuario))
            {
                if (Class.Globales.Usuario == null)
                {
                    Class.Globales.Usuario = new cUsuario();
                    Class.Globales.Usuario.Email = usuario;
                }
            }
            CultureInfo ci = new CultureInfo("es-MX");
  
            Application.CurrentCulture = ci;
            CultureInfo.DefaultThreadCurrentCulture = ci;
            CultureInfo.DefaultThreadCurrentUICulture = ci;

            Thread.CurrentThread.CurrentCulture = CultureInfo.CreateSpecificCulture(ci.Name);
            Thread.CurrentThread.CurrentUICulture = CultureInfo.CreateSpecificCulture(ci.Name);


            InitializeComponent();
            VerificarConfiguracion();
            //Class.Globales.CarpetaDescargaBloquear();
        }

        private void VerificarConfiguracion()
        {
            ModeloDescarga.DescargaFXContainer Db = new ModeloDescarga.DescargaFXContainer();
            ModeloDescarga.DescargaConfiguracion Registro = Db.DescargaConfiguracionSet.FirstOrDefault();
            if (Registro == null)
            {
                Formulario.Configuracion O = new Formulario.Configuracion();
                O.ShowDialog();
            }
            if (String.IsNullOrEmpty(Registro.PasswordZip))
            {
                Formulario.Configuracion O = new Formulario.Configuracion();
                O.ShowDialog();
            }

            VerificarAutomatica.Checked = Registro.VerificarAutomatica;
        }

        private void Cargar()
        {
            ModeloDescarga.DescargaFXContainer  Db = new ModeloDescarga.DescargaFXContainer();
            try
            {
                var result = Db.DescargaSolicitudSet
                             .Where(a => a.Solicitud.Contains(buscar.Text)
                             )
                             .OrderByDescending(a=>a.UltimaVerificacion);
                Dtg.Rows.Clear();
                foreach (ModeloDescarga.DescargaSolicitud Registro in result)
                {
                    int N = Dtg.Rows.Add();
                    Dtg.Rows[N].Tag = Registro.Id;
                    Dtg.Rows[N].Cells["Id"].Value = Registro.Id;
                    Dtg.Rows[N].Cells["Creado"].Value = Registro.Creado;
                    Dtg.Rows[N].Cells["Usuario"].Value = Registro.Usuario;
                    Dtg.Rows[N].Cells["Inicio"].Value = Registro.Inicio.ToShortDateString();
                    Dtg.Rows[N].Cells["Fin"].Value = Registro.Fin.ToShortDateString();
                    Dtg.Rows[N].Cells["Estado"].Value = Registro.Estado;
                    Dtg.Rows[N].Cells["Solicitud"].Value = Registro.Solicitud;

                    Dtg.Rows[N].Cells["Rfc"].Value = Registro.Rfc;
                    Dtg.Rows[N].Cells["Tipo"].Value = Registro.Tipo;


                    Dtg.Rows[N].Cells["TotalCFDI"].Value = Registro.TotalCFDI;
                    if (Registro.Paquete!= null){
                        Dtg.Rows[N].Cells["Paquete"].Value = Registro.Paquete;
                    }

                    if (Registro.UltimaVerificacion != null)
                    {
                        DateTime ODateTime = DateTime.Parse(Registro.UltimaVerificacion.Value.ToString());
                        Dtg.Rows[N].Cells["UltimaVerificacion"].Value = Registro.UltimaVerificacion;
                    }
                }
                //Dtg.DataSource = result;
                Dtg.AutoResizeColumns(DataGridViewAutoSizeColumnsMode.AllCells);

            }
            catch (DbEntityValidationException e)
            {
                foreach (var eve in e.EntityValidationErrors)
                {
                    foreach (var ve in eve.ValidationErrors)
                    {
                        MessageBox.Show(this, ve.PropertyName + " Error: " + ve.ErrorMessage, Application.ProductName
                            , MessageBoxButtons.OK, MessageBoxIcon.Error);
                        return;
                    }
                }
            }
            catch (Exception e)
            {
                MessageBox.Show(this, e.Message, Application.ProductName
                    , MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
                
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {

        }

        private void button2_Click_1(object sender, EventArgs e)
        {
            Cargar();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Nuevo();
        }

        private void Nuevo()
        {
            Formulario.Solicitud OSolicitud = new Formulario.Solicitud();
            OSolicitud.Show();
        }

        private void Verificar_CheckedChanged(object sender, EventArgs e)
        {
            if (VerificarAutomatica.Checked)
            {
                timer1.Start();
            }
            else
            {
                timer1.Stop();
            }
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            ValidarTiempo();
        }

        private int tiempo = 0;
        private void ValidarTiempo()
        {
            //Validar cada Registro
               
            tiempo++;
            if (tiempo == 600000)
            {
                timer1.Stop();
                //Crear autorizacion

                foreach (DataGridViewRow Registro in Dtg.Rows)                
                {
                    int idTr = int.Parse(Registro.Cells["Id"].Value.ToString());
                    VerificarRegistro(idTr);
                }
                Cargar();
                
                tiempo = 0;
                timer1.Start();
            }
        }

        private void VerificarRegistro(int idTr)
        {
            ModeloDescarga.DescargaFXContainer Db = new ModeloDescarga.DescargaFXContainer();
            ModeloDescarga.DescargaSolicitud RegistroTr = Db.DescargaSolicitudSet.Where(a => a.Id == idTr).FirstOrDefault();
            if (RegistroTr != null)
            {
                if (RegistroTr.Estado == "Solicitado")
                {
                    if (LeerPFX(RegistroTr.Rfc))
                    {
                        ValidarSolicitud(certifcate, Autorization, RegistroTr);
                    }
                }
                else
                {
                    
                }
            }
        }
        #region Autorizacion
        String Autorization = String.Empty;
        private bool LeerPFX(string rfcSolicitante)
        {
            string file = rfcSolicitante + ".pfx";
            if (File.Exists(file))
            {
                pfx = File.ReadAllBytes(file);
                //Obtener Certificados
                certifcate = ObtenerX509Certificado(pfx);
                return Autentificar();
            }
            else
            {
                this.toolStripStatusLabel1.Text = "El archivo " + file + " no existe en el directorio";
            }
            return false;
        }
        private static X509Certificate2 ObtenerX509Certificado(byte[] pfx)
        {
            return new X509Certificate2(pfx, password, X509KeyStorageFlags.MachineKeySet |
                            X509KeyStorageFlags.PersistKeySet |
                            X509KeyStorageFlags.Exportable);
        }

        private static string ObtenerToken(X509Certificate2 certifcate)
        {
            Autenticacion service = new Autenticacion(urlAutentica, urlAutenticaAction);
            service.Generate(certifcate);
            return service.Send();
        }
        private bool Autentificar()
        {
            try
            {
                //Obtener Token
                string token = ObtenerToken(certifcate);
                Autorization = String.Format("WRAP access_token=\"{0}\"", System.Net.WebUtility.UrlDecode(token));
                Console.WriteLine("Token: " + token);
                return true;
            }
            catch (Exception Error)
            {
                solicitudInformacion.Text += DateTime.Now.ToString() + " No puede ingresar al SAT " + Environment.NewLine
                    + urlAutentica + Environment.NewLine
                    + urlAutentica + Environment.NewLine
                    + Error.Message + Environment.NewLine;
                ;
                string Mensaje = Error.Message.ToString();
                if (Error.InnerException != null)
                {
                    Mensaje += Environment.NewLine + Error.InnerException.ToString();
                }
                solicitudInformacion.Text += Mensaje;
                BitacoraFX.Log(Mensaje);

            }
            return false;
        }

        #endregion
        #region ValidarDescargar
        private void ValidarSolicitud(X509Certificate2 certifcate, string autorization, ModeloDescarga.DescargaSolicitud registro)
        {
            solicitudInformacion.Text = "Espere mientras se procesa....";
            VerificaSolicitud verifica = new VerificaSolicitud(urlVerificarSolicitud, urlVerificarSolicitudAction);
            verifica.Generate(certifcate, registro.Rfc, registro.Solicitud);
            verifica.Send(autorization);
            string PaqueteTr = string.Empty;
            if (!String.IsNullOrEmpty(verifica.paquete))
            {
                PaqueteTr = verifica.paquete;
            }
            solicitudInformacion.Text = DateTime.Now.ToLongTimeString() + Environment.NewLine + "EstadoSolicitud: " + verifica.EstadoSolicitud.ToString() + " "
                + "CodEstatus: " + verifica.CodEstatus.ToString() + " "
                + "CodigoEstadoSolicitud: " + verifica.CodigoEstadoSolicitud.ToString() + " "
                + "NumeroCFDIs: " + verifica.NumeroCFDIs.ToString() + Environment.NewLine
                + "Paquete: " + PaqueteTr;

            //Token Invalido actualizarñp
            if (verifica.CodEstatus == 300)
            {
                solicitudInformacion.Text += Environment.NewLine + "Mensaje: Token Invalido";
                Autentificar();
            }
            else
            {
                if (verifica.Mensaje != null)
                {
                    solicitudInformacion.Text += Environment.NewLine + "Mensaje: " + verifica.Mensaje.ToString() + " " + DateTime.Now.ToLongTimeString();
                }
                solicitudInformacion.Text += Environment.NewLine + "EstadoSolicitud: " + verifica.EstadoSolicitud.ToString() + " " + DateTime.Now.ToLongTimeString();
                solicitudInformacion.Text += Environment.NewLine + "Solicitud Codigo: " + verifica.CodEstatus.ToString() + " " + Environment.NewLine
                    + DateTime.Now.ToLongTimeString();
                solicitudInformacion.SelectionStart = solicitudInformacion.Text.Length;
                solicitudInformacion.ScrollToCaret();

                //Actualizar el estado y el paquete y las cantidad de CFDI
                ModeloDescarga.DescargaFXContainer Db = new ModeloDescarga.DescargaFXContainer();
                int IdTr = registro.Id;
                ModeloDescarga.DescargaSolicitud Registro = Db.DescargaSolicitudSet.Where(a => a.Id == IdTr).FirstOrDefault();


                if (!string.IsNullOrEmpty(verifica.paquete))
                {
                    if (Registro!=null)
                    {
                        Registro.Estado = "Verificado";
                        Registro.Paquete = verifica.paquete;
                        Registro.TotalCFDI = int.Parse(verifica.NumeroCFDIs.ToString());
                        Db.DescargaSolicitudSet.Attach(Registro);
                        Db.Entry(Registro).State = System.Data.Entity.EntityState.Modified;
                        Db.SaveChanges();
                        DescargarPaquete(Registro);
                    }                    
                }
                else
                {
                    Registro.UltimaVerificacion = DateTime.Now;
                    Db.DescargaSolicitudSet.Attach(Registro);
                    Db.Entry(Registro).State = System.Data.Entity.EntityState.Modified;
                    Db.SaveChanges();
                }
            }


        }
        

        private void DescargarPaquete(ModeloDescarga.DescargaSolicitud registro)
        {
            if (!string.IsNullOrEmpty(registro.Paquete))
            {
                if (LeerPFX(registro.Rfc))
                {
                    //Descargar Solicitud
                    string descargaResponse = DescargarSolicitud(certifcate, Autorization, registro.Paquete, registro);
                    GuardarSolicitud(descargaResponse, registro);
                }

            }
        }
        private string DescargarSolicitud(X509Certificate2 certifcate, string autorization, string idPaquete
            , ModeloDescarga.DescargaSolicitud registro)
        {
            DescargarSolicitud descargarSolicitud = new DescargarSolicitud(urlDescargarSolicitud, urlDescargarSolicitudAction);
            string xmlDescarga = descargarSolicitud.Generate(certifcate, registro.Rfc, idPaquete);
            return descargarSolicitud.Send(autorization);
        }
        private void GuardarSolicitud(string descargaResponse, ModeloDescarga.DescargaSolicitud registro)
        {

            byte[] file = Convert.FromBase64String(descargaResponse);
            Directory.CreateDirectory(Class.Globales.PathPaquetes);

            string nombreArchivo = registro.Rfc + "_" + registro.Tipo + "_" + DateTime.Now.ToString("yyyy-MM-dd_HHmm") + "_"
                + registro.Inicio.ToString("yyyy-MM-dd") + "_"
                + registro.Fin.ToString("yyyy-MM-dd")
                + ".zip"
                ;
            string NombreCompleto = Class.Globales.PathPaquetes + nombreArchivo;
            if (File.Exists(NombreCompleto))
            {
                File.Delete(NombreCompleto);
            }
            if (file.Length > 0)
            {
                using (FileStream fs = File.Create(NombreCompleto, file.Length))
                {
                    fs.Write(file, 0, file.Length);
                    fs.Close();
                    fs.Dispose();
                }
                //Actualizar el estado y el paquete y las cantidad de CFDI
                ModeloDescarga.DescargaFXContainer Db = new ModeloDescarga.DescargaFXContainer();
                int IdTr = registro.Id;
                ModeloDescarga.DescargaSolicitud Registro = Db.DescargaSolicitudSet.Where(a => a.Id == IdTr).FirstOrDefault();
                if (Registro != null)
                {
                    Registro.Estado = "Descargado";
                    Registro.Archivo = nombreArchivo;
                    Db.DescargaSolicitudSet.Attach(Registro);
                    Db.Entry(Registro).State = System.Data.Entity.EntityState.Modified;
                    Db.SaveChanges();
                }
            }

        }

        #endregion

        private void verificarToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            ProcesarSeleccionados();
        }

        private void ProcesarSeleccionados()
        {
            Dtg.EndEdit();
            if (Dtg.SelectedCells.Count > 0)
            {
                DialogResult Resultado = MessageBox.Show("¿Desea procesar las seleccionada?"
                    , Application.ProductName + "-" + Application.ProductVersion
                    , MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                if (Resultado != DialogResult.Yes)
                {
                    return;
                }
                
                foreach (DataGridViewRow drv in Dtg.SelectedRows)
                {
                    try
                    {
                        int Idtr = int.Parse(drv.Tag.ToString());
                        VerificarRegistro(Idtr);
                    }
                    catch (Exception e)
                    {
                        Bitacora.Log(e.Message.ToString());
                    }
                }
                Cargar();
            }
            else
            {
                MessageBox.Show("Seleccione alguna línea.", Application.ProductName + "-" + Application.ProductVersion, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }

        private void Dtg_MouseClick(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Right)
            {
                contextMenuStrip1.Show(Dtg, new Point(e.X, e.Y));
            }
        }

        private void Dtg_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            CargarRegistro(e);
        }

        private void CargarRegistro(DataGridViewCellEventArgs e)
        {
            if (e.RowIndex > -1)
            {
                int IdTr = (int)Dtg.Rows[e.RowIndex].Tag;
                Formulario.Detalle O = new Formulario.Detalle(IdTr);
                O.Show();

            }
        }

        private void descargarToolStripMenuItem_Click(object sender, EventArgs e)
        {
            DescargarSeleccionados();
        }

        private void DescargarSeleccionados()
        {
            Dtg.EndEdit();
            if (Dtg.SelectedCells.Count > 0)
            {
                DialogResult Resultado = MessageBox.Show("¿Desea descargar las seleccionada?"
                    , Application.ProductName + "-" + Application.ProductVersion
                    , MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                if (Resultado != DialogResult.Yes)
                {
                    return;
                }

                foreach (DataGridViewRow drv in Dtg.SelectedRows)
                {
                    try
                    {
                        int Idtr = int.Parse(drv.Tag.ToString());
                        ModeloDescarga.DescargaFXContainer Db = new ModeloDescarga.DescargaFXContainer();
                        ModeloDescarga.DescargaSolicitud Registro = Db.DescargaSolicitudSet.Where(a => a.Id == Idtr).FirstOrDefault();
                        if (Registro != null)
                        {
                            if(Registro.Estado != "Descargado")
                            {
                                DescargarPaquete(Registro);
                            }                            
                        }
                    }
                    catch (Exception e)
                    {
                        ErrorFX.mostrar(e, true, false);
                        Bitacora.Log(e.Message.ToString());
                    }
                }
                Cargar();
            }
            else
            {
                MessageBox.Show("Seleccione alguna línea.", Application.ProductName + "-" + Application.ProductVersion, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }

        private void button3_Click_1(object sender, EventArgs e)
        {
            Formulario.Detalle O = new Formulario.Detalle();
            O.Show();
        }

        private void button4_Click(object sender, EventArgs e)
        {
            Formulario.Configuracion O = new Formulario.Configuracion();
            O.Show();
        }
    }
}
