﻿namespace DescargaFX
{
    partial class frmPrincipalNuevo
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmPrincipalNuevo));
            this.button1 = new System.Windows.Forms.Button();
            this.FechaInicial = new System.Windows.Forms.DateTimePicker();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.FechaFinal = new System.Windows.Forms.DateTimePicker();
            this.label5 = new System.Windows.Forms.Label();
            this.idSolicitud = new System.Windows.Forms.TextBox();
            this.autorization = new System.Windows.Forms.TextBox();
            this.button2 = new System.Windows.Forms.Button();
            this.button4 = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.idPaquete = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.textBox2 = new System.Windows.Forms.TextBox();
            this.solicitudInformacion = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.solicitudError = new System.Windows.Forms.TextBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.rdTipo2 = new System.Windows.Forms.RadioButton();
            this.rdTipo1 = new System.Windows.Forms.RadioButton();
            this.RfcSolicitante = new System.Windows.Forms.TextBox();
            this.timer3 = new System.Windows.Forms.Timer(this.components);
            this.timer3Info = new System.Windows.Forms.TextBox();
            this.checkBox3 = new System.Windows.Forms.CheckBox();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(11, 220);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(187, 23);
            this.button1.TabIndex = 0;
            this.button1.Text = "3. Verificar solicitud lista";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // FechaInicial
            // 
            this.FechaInicial.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.FechaInicial.Location = new System.Drawing.Point(50, 19);
            this.FechaInicial.Name = "FechaInicial";
            this.FechaInicial.Size = new System.Drawing.Size(101, 20);
            this.FechaInicial.TabIndex = 3;
            this.FechaInicial.Value = new System.DateTime(2020, 11, 1, 0, 0, 0, 0);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(12, 25);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(32, 13);
            this.label3.TabIndex = 1;
            this.label3.Text = "Inicio";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(160, 26);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(21, 13);
            this.label4.TabIndex = 1;
            this.label4.Text = "Fin";
            // 
            // FechaFinal
            // 
            this.FechaFinal.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.FechaFinal.Location = new System.Drawing.Point(187, 20);
            this.FechaFinal.Name = "FechaFinal";
            this.FechaFinal.Size = new System.Drawing.Size(97, 20);
            this.FechaFinal.TabIndex = 3;
            this.FechaFinal.Value = new System.DateTime(2020, 11, 30, 0, 0, 0, 0);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(300, 23);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(80, 13);
            this.label5.TabIndex = 1;
            this.label5.Text = "RFC Solicitante";
            // 
            // idSolicitud
            // 
            this.idSolicitud.Location = new System.Drawing.Point(198, 114);
            this.idSolicitud.Name = "idSolicitud";
            this.idSolicitud.Size = new System.Drawing.Size(487, 20);
            this.idSolicitud.TabIndex = 2;
            // 
            // autorization
            // 
            this.autorization.Location = new System.Drawing.Point(130, 76);
            this.autorization.Multiline = true;
            this.autorization.Name = "autorization";
            this.autorization.Size = new System.Drawing.Size(555, 23);
            this.autorization.TabIndex = 2;
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(11, 76);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(113, 23);
            this.button2.TabIndex = 0;
            this.button2.Text = "1. Autorización";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // button4
            // 
            this.button4.Location = new System.Drawing.Point(11, 111);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(113, 23);
            this.button4.TabIndex = 4;
            this.button4.Text = "2. Solicitar";
            this.button4.UseVisualStyleBackColor = true;
            this.button4.Click += new System.EventHandler(this.button4_Click);
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(11, 403);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(188, 23);
            this.button3.TabIndex = 6;
            this.button3.Text = "4. Descargar paquete";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // idPaquete
            // 
            this.idPaquete.Location = new System.Drawing.Point(86, 377);
            this.idPaquete.Name = "idPaquete";
            this.idPaquete.Size = new System.Drawing.Size(599, 20);
            this.idPaquete.TabIndex = 5;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(130, 116);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(47, 13);
            this.label6.TabIndex = 1;
            this.label6.Text = "Solicitud";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(12, 380);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(47, 13);
            this.label7.TabIndex = 1;
            this.label7.Text = "Paquete";
            // 
            // textBox2
            // 
            this.textBox2.Location = new System.Drawing.Point(11, 432);
            this.textBox2.Multiline = true;
            this.textBox2.Name = "textBox2";
            this.textBox2.Size = new System.Drawing.Size(674, 158);
            this.textBox2.TabIndex = 2;
            // 
            // solicitudInformacion
            // 
            this.solicitudInformacion.Location = new System.Drawing.Point(11, 272);
            this.solicitudInformacion.Multiline = true;
            this.solicitudInformacion.Name = "solicitudInformacion";
            this.solicitudInformacion.Size = new System.Drawing.Size(674, 93);
            this.solicitudInformacion.TabIndex = 2;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(12, 149);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(29, 13);
            this.label9.TabIndex = 1;
            this.label9.Text = "Error";
            // 
            // solicitudError
            // 
            this.solicitudError.Location = new System.Drawing.Point(46, 149);
            this.solicitudError.Multiline = true;
            this.solicitudError.Name = "solicitudError";
            this.solicitudError.Size = new System.Drawing.Size(639, 65);
            this.solicitudError.TabIndex = 2;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.rdTipo2);
            this.groupBox1.Controls.Add(this.rdTipo1);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.RfcSolicitante);
            this.groupBox1.Controls.Add(this.FechaFinal);
            this.groupBox1.Controls.Add(this.FechaInicial);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Location = new System.Drawing.Point(11, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(674, 58);
            this.groupBox1.TabIndex = 7;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Filtros";
            // 
            // rdTipo2
            // 
            this.rdTipo2.AutoSize = true;
            this.rdTipo2.Checked = true;
            this.rdTipo2.Location = new System.Drawing.Point(488, 21);
            this.rdTipo2.Name = "rdTipo2";
            this.rdTipo2.Size = new System.Drawing.Size(56, 17);
            this.rdTipo2.TabIndex = 4;
            this.rdTipo2.TabStop = true;
            this.rdTipo2.Text = "Emisor";
            this.rdTipo2.UseVisualStyleBackColor = true;
            // 
            // rdTipo1
            // 
            this.rdTipo1.AutoSize = true;
            this.rdTipo1.Location = new System.Drawing.Point(550, 21);
            this.rdTipo1.Name = "rdTipo1";
            this.rdTipo1.Size = new System.Drawing.Size(69, 17);
            this.rdTipo1.TabIndex = 4;
            this.rdTipo1.Text = "Receptor";
            this.rdTipo1.UseVisualStyleBackColor = true;
            // 
            // RfcSolicitante
            // 
            this.RfcSolicitante.Location = new System.Drawing.Point(386, 20);
            this.RfcSolicitante.Name = "RfcSolicitante";
            this.RfcSolicitante.Size = new System.Drawing.Size(96, 20);
            this.RfcSolicitante.TabIndex = 2;
            this.RfcSolicitante.Text = "BME9802073WA";
            // 
            // timer3
            // 
            this.timer3.Interval = 1000;
            this.timer3.Tick += new System.EventHandler(this.timer3_Tick);
            // 
            // timer3Info
            // 
            this.timer3Info.Location = new System.Drawing.Point(204, 222);
            this.timer3Info.Multiline = true;
            this.timer3Info.Name = "timer3Info";
            this.timer3Info.Size = new System.Drawing.Size(481, 21);
            this.timer3Info.TabIndex = 8;
            // 
            // checkBox3
            // 
            this.checkBox3.AutoSize = true;
            this.checkBox3.Checked = true;
            this.checkBox3.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBox3.Location = new System.Drawing.Point(10, 249);
            this.checkBox3.Name = "checkBox3";
            this.checkBox3.Size = new System.Drawing.Size(189, 17);
            this.checkBox3.TabIndex = 9;
            this.checkBox3.Text = "Descarga Automatica cada minuto";
            this.checkBox3.UseVisualStyleBackColor = true;
            this.checkBox3.CheckedChanged += new System.EventHandler(this.checkBox3_CheckedChanged);
            // 
            // frmPrincipalNuevo
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(704, 602);
            this.Controls.Add(this.checkBox3);
            this.Controls.Add(this.timer3Info);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.button3);
            this.Controls.Add(this.idPaquete);
            this.Controls.Add(this.button4);
            this.Controls.Add(this.solicitudInformacion);
            this.Controls.Add(this.autorization);
            this.Controls.Add(this.textBox2);
            this.Controls.Add(this.solicitudError);
            this.Controls.Add(this.idSolicitud);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.button1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "frmPrincipalNuevo";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Descarga de CFDI";
            this.Load += new System.EventHandler(this.frmPrincipalNuevo_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.DateTimePicker FechaInicial;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.DateTimePicker FechaFinal;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox idSolicitud;
        private System.Windows.Forms.TextBox autorization;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.TextBox idPaquete;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox textBox2;
        private System.Windows.Forms.TextBox solicitudInformacion;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox solicitudError;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.RadioButton rdTipo2;
        private System.Windows.Forms.RadioButton rdTipo1;
        private System.Windows.Forms.TextBox RfcSolicitante;
        private System.Windows.Forms.Timer timer3;
        private System.Windows.Forms.TextBox timer3Info;
        private System.Windows.Forms.CheckBox checkBox3;
    }
}

