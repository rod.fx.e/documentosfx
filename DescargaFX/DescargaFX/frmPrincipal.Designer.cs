﻿namespace DescargaFX
{
    partial class frmPrincipal
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.button1 = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.RfcSolicitante = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.RfcEmisor = new System.Windows.Forms.TextBox();
            this.FechaInicial = new System.Windows.Forms.DateTimePicker();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.FechaFinal = new System.Windows.Forms.DateTimePicker();
            this.RfcReceptor = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.idSolicitud = new System.Windows.Forms.TextBox();
            this.autorization = new System.Windows.Forms.TextBox();
            this.button2 = new System.Windows.Forms.Button();
            this.button4 = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.idPaquete = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.textBox2 = new System.Windows.Forms.TextBox();
            this.solicitudInformacion = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.solicitudError = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(12, 188);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(187, 23);
            this.button1.TabIndex = 0;
            this.button1.Text = "3. Verificar solicitud lista";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(13, 13);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(80, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "RFC Solicitante";
            // 
            // RfcSolicitante
            // 
            this.RfcSolicitante.Location = new System.Drawing.Point(99, 10);
            this.RfcSolicitante.Name = "RfcSolicitante";
            this.RfcSolicitante.Size = new System.Drawing.Size(123, 20);
            this.RfcSolicitante.TabIndex = 2;
            this.RfcSolicitante.Text = "EAUR810302714";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(436, 13);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(62, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "RFC Emisor";
            // 
            // RfcEmisor
            // 
            this.RfcEmisor.Location = new System.Drawing.Point(504, 10);
            this.RfcEmisor.Name = "RfcEmisor";
            this.RfcEmisor.Size = new System.Drawing.Size(219, 20);
            this.RfcEmisor.TabIndex = 2;
            // 
            // FechaInicial
            // 
            this.FechaInicial.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.FechaInicial.Location = new System.Drawing.Point(66, 36);
            this.FechaInicial.Name = "FechaInicial";
            this.FechaInicial.Size = new System.Drawing.Size(101, 20);
            this.FechaInicial.TabIndex = 3;
            this.FechaInicial.Value = new System.DateTime(2020, 11, 1, 0, 0, 0, 0);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(13, 42);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(32, 13);
            this.label3.TabIndex = 1;
            this.label3.Text = "Inicio";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(173, 42);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(21, 13);
            this.label4.TabIndex = 1;
            this.label4.Text = "Fin";
            // 
            // FechaFinal
            // 
            this.FechaFinal.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.FechaFinal.Location = new System.Drawing.Point(206, 36);
            this.FechaFinal.Name = "FechaFinal";
            this.FechaFinal.Size = new System.Drawing.Size(97, 20);
            this.FechaFinal.TabIndex = 3;
            this.FechaFinal.Value = new System.DateTime(2020, 11, 30, 0, 0, 0, 0);
            // 
            // RfcReceptor
            // 
            this.RfcReceptor.Location = new System.Drawing.Point(309, 10);
            this.RfcReceptor.Name = "RfcReceptor";
            this.RfcReceptor.Size = new System.Drawing.Size(121, 20);
            this.RfcReceptor.TabIndex = 2;
            this.RfcReceptor.Text = "EAUR810302714";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(228, 13);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(75, 13);
            this.label5.TabIndex = 1;
            this.label5.Text = "RFC Receptor";
            // 
            // idSolicitud
            // 
            this.idSolicitud.Location = new System.Drawing.Point(375, 91);
            this.idSolicitud.Name = "idSolicitud";
            this.idSolicitud.Size = new System.Drawing.Size(348, 20);
            this.idSolicitud.TabIndex = 2;
            // 
            // autorization
            // 
            this.autorization.Location = new System.Drawing.Point(12, 91);
            this.autorization.Multiline = true;
            this.autorization.Name = "autorization";
            this.autorization.Size = new System.Drawing.Size(301, 91);
            this.autorization.TabIndex = 2;
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(12, 62);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(113, 23);
            this.button2.TabIndex = 0;
            this.button2.Text = "1. Autorización";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // button4
            // 
            this.button4.Location = new System.Drawing.Point(325, 62);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(113, 23);
            this.button4.TabIndex = 4;
            this.button4.Text = "2. Solicitar";
            this.button4.UseVisualStyleBackColor = true;
            this.button4.Click += new System.EventHandler(this.button4_Click);
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(12, 345);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(188, 23);
            this.button3.TabIndex = 6;
            this.button3.Text = "4. Descargar paquete";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // idPaquete
            // 
            this.idPaquete.Location = new System.Drawing.Point(66, 319);
            this.idPaquete.Name = "idPaquete";
            this.idPaquete.Size = new System.Drawing.Size(657, 20);
            this.idPaquete.TabIndex = 5;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(322, 94);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(47, 13);
            this.label6.TabIndex = 1;
            this.label6.Text = "Solicitud";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(13, 401);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(47, 13);
            this.label7.TabIndex = 1;
            this.label7.Text = "Paquete";
            // 
            // textBox2
            // 
            this.textBox2.Location = new System.Drawing.Point(12, 374);
            this.textBox2.Multiline = true;
            this.textBox2.Name = "textBox2";
            this.textBox2.Size = new System.Drawing.Size(711, 138);
            this.textBox2.TabIndex = 2;
            // 
            // solicitudInformacion
            // 
            this.solicitudInformacion.Location = new System.Drawing.Point(12, 217);
            this.solicitudInformacion.Multiline = true;
            this.solicitudInformacion.Name = "solicitudInformacion";
            this.solicitudInformacion.Size = new System.Drawing.Size(711, 96);
            this.solicitudInformacion.TabIndex = 2;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(322, 120);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(29, 13);
            this.label9.TabIndex = 1;
            this.label9.Text = "Error";
            // 
            // solicitudError
            // 
            this.solicitudError.Location = new System.Drawing.Point(375, 117);
            this.solicitudError.Multiline = true;
            this.solicitudError.Name = "solicitudError";
            this.solicitudError.Size = new System.Drawing.Size(348, 65);
            this.solicitudError.TabIndex = 2;
            // 
            // frmPrincipal
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(732, 523);
            this.Controls.Add(this.button3);
            this.Controls.Add(this.idPaquete);
            this.Controls.Add(this.button4);
            this.Controls.Add(this.FechaFinal);
            this.Controls.Add(this.FechaInicial);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.RfcReceptor);
            this.Controls.Add(this.solicitudInformacion);
            this.Controls.Add(this.autorization);
            this.Controls.Add(this.textBox2);
            this.Controls.Add(this.solicitudError);
            this.Controls.Add(this.idSolicitud);
            this.Controls.Add(this.RfcEmisor);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.RfcSolicitante);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.button1);
            this.Name = "frmPrincipal";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Descarga de CFDI";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox RfcSolicitante;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox RfcEmisor;
        private System.Windows.Forms.DateTimePicker FechaInicial;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.DateTimePicker FechaFinal;
        private System.Windows.Forms.TextBox RfcReceptor;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox idSolicitud;
        private System.Windows.Forms.TextBox autorization;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.TextBox idPaquete;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox textBox2;
        private System.Windows.Forms.TextBox solicitudInformacion;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox solicitudError;
    }
}

