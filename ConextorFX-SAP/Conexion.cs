﻿using Sap.Data.Hana;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace ConectorFX.SAP
{
    class Conexion
    {
        public string Tipo;
        public string Server;
        public string UserID;
        public string Password;
        public string Schema;

        public SqlDataAdapter oDataAdapter;
        public SqlConnection oConn;
        public string sConn = string.Empty;

        public Conexion()
        {

            if (ConfigurationManager.AppSettings["SQLServer"] != null)
            {
                sConn = ConfigurationManager.AppSettings["SQLServer"].ToString();
                Schema = "dbo";
                CrearConexion();
            }
            else
            {
                //Hanna
                leer_datos();
            }
            
        }
        public bool CrearConexion()
        {
            oConn = new SqlConnection(sConn);
            try
            {
                oConn.Open();
                return true;
            }
            catch (SqlException error)
            {
                string Mensaje = error.Message.ToString();
                if (error.InnerException != null)
                {
                    Mensaje += Environment.NewLine + error.InnerException.ToString() + Environment.NewLine
                        + "Conexion.cs 54";
                }
                throw new Exception(Mensaje);
            }
        }

        public void leer_datos()
        {
            try
            {
                Tipo = ConfigurationManager.AppSettings["Tipo"].ToString();
                Server = ConfigurationManager.AppSettings["Server"].ToString();
                UserID = ConfigurationManager.AppSettings["UserID"].ToString();
                Password = ConfigurationManager.AppSettings["Password"].ToString();
                Schema = ConfigurationManager.AppSettings["Schema"].ToString();
            }
            catch (Exception e)
            {
                throw new Exception(e.Message.ToString() + " Conexion 35", e);
            }

        }

        public DataTable EjecutarConsulta(string pQuery)
        {
            try
            {
                DataTable oDT = new DataTable();
                //Ejecutar para SQL Server
                oDT = new DataTable();

                if (!String.IsNullOrEmpty(sConn))
                {
                    //SQL Server
                    oConn = new SqlConnection(sConn);
                    oDataAdapter = new SqlDataAdapter();
                    pQuery = pQuery.Replace("\"", "");
                    SqlCommand oSqlCommand = new SqlCommand(pQuery, oConn);
                    oSqlCommand.CommandTimeout = 10000000;
                    oDataAdapter.SelectCommand = oSqlCommand;
                    oDataAdapter.Fill(oDT);
                }
                else
                {
                    //Hanna
                    HanaConnection conn = new HanaConnection
                    (@"Server = " + Server + "; UserID = " + UserID + "; Password = " + Password + "");
                    conn.Open();
                    HanaDataAdapter oHanaDataAdapter = new HanaDataAdapter(pQuery, conn);
                    oHanaDataAdapter.Fill(oDT);
                    oHanaDataAdapter.Dispose();
                    conn.Close();
                }
                
                return oDT;
            }
            catch (HanaException e)
            {
                MessageBox.Show(e.Message + "\n" + pQuery, "Exepción " + Application.ProductName + "-" + Application.ProductVersion, MessageBoxButtons.OK, MessageBoxIcon.Stop);
                //Guardar en base datos
                Bitacora.Log(DateTime.Now.ToShortDateString() + " " + DateTime.Now.ToShortTimeString() + ":"
                                    + e.Message + "\n" + pQuery);
                return null;
            }

        }

    }

}
