#region Copyright Syncfusion Inc. 2001 - 2017
// Copyright Syncfusion Inc. 2001 - 2017. All rights reserved.
// Use of this code is subject to the terms of our license.
// A copy of the current license can be obtained at any time by e-mailing
// licensing@syncfusion.com. Any infringement will be prosecuted under
// applicable laws. 
#endregion
using ConectorFX.SAP;
using FacturasSincronizador;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Windows.Forms;

namespace ConextorFX_SAP
{
    public partial class frmPrincipal : Syncfusion.Windows.Forms.MetroForm
    {
        int segundos = 3600;
        string horaInicialtr = "";
        string horaFinaltr = "";

        public frmPrincipal()
        {
            InitializeComponent();

            this.notifyIcon.BalloonTipIcon = System.Windows.Forms.ToolTipIcon.Info;
            this.notifyIcon.BalloonTipText = Application.ProductName;
            this.notifyIcon.BalloonTipTitle = "Sincronizador minimizado";
            this.notifyIcon.Text = "Conector - minimizado";



            try
            {
                horaInicialtr = ConfigurationManager.AppSettings["horaInicial"].ToString();
            }
            catch
            {

            }

            try
            {
                horaFinaltr = ConfigurationManager.AppSettings["horaFinal"].ToString();
            }
            catch
            {

            }
            cargarSegundos();
        }

        private void cargarSegundos()
        {
            try
            {
                segundos = int.Parse(ConfigurationManager.AppSettings["segundos"]);
            }
            catch
            {

            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            cargar();
        }
        DataTable oDtClientes;
        BackgroundWorker bgwClientes;

        private void actualizarClientes()
        {
            if (oDtClientes != null)
            {
                informacion.Text = "Actualizando proveedores " + DateTime.Now.ToShortDateString() + " " + DateTime.Now.ToShortTimeString();
                bgwClientes = new BackgroundWorker();
                bgwClientes.WorkerSupportsCancellation = true;
                bgwClientes.WorkerReportsProgress = true;
                bgwClientes.DoWork += new DoWorkEventHandler(bgwClientes_DoWork);
                bgwClientes.ProgressChanged += new ProgressChangedEventHandler(bgwClientes_ProgressChanged);
                bgwClientes.RunWorkerCompleted += new RunWorkerCompletedEventHandler(bgwClientes_RunWorkerCompleted);
                bgwClientes.RunWorkerAsync();
            }

        }
        private void bgwClientes_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            informacion.Text = e.UserState.ToString();
            this.notifyIcon.Text = informacionPago.Text;
        }
        private void bgwClientes_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            asigar_texto_actualizacion("Proveedores finalizado:" + DateTime.Now.ToShortDateString() + " " + DateTime.Now.ToShortTimeString());
            informacion.Text = "Proveedores finalizado " + DateTime.Now.ToShortDateString() + " " + DateTime.Now.ToShortTimeString();
            this.notifyIcon.Text = informacionPago.Text;
        }
        private void bgwClientes_DoWork(object sender, DoWorkEventArgs e)
        {
            //Navegar por todas las empresas configuradas
            ingresar_clientes();
            asigar_texto_actualizacion("Proveedores:" + DateTime.Now.ToShortDateString() + " " + DateTime.Now.ToShortTimeString());
        }
        private void asigar_texto_actualizacion(string texto)
        {
            if (textoHora.InvokeRequired)
            {
                MethodInvoker invoker = new MethodInvoker(delegate
                {
                    textoHora.Text = texto + " " + DateTime.Now.ToShortDateString() + " " + DateTime.Now.ToShortTimeString();
                });
                textoHora.Invoke(invoker);
            }
            else
            {
                textoHora.Text = texto;
            }
        }
        private void ingresar_clientes()
        {
            try
            {
                if (oDtClientes != null)
                {
                    //Paginar
                    var results = from myRow in oDtClientes.AsEnumerable()
                                  select myRow;
                    DataTable boundTable = results.CopyToDataTable<DataRow>();
                    int pageSize = 30;
                    int totalCount = boundTable.Rows.Count;
                    int numberOfPages = (int)Math.Ceiling((double)totalCount / pageSize);
                    for (int i = 0; i < numberOfPages; i++)
                    {
                        var consulta = results.Skip(i * pageSize).Take(pageSize);
                        DataTable tabla_guardar = consulta.CopyToDataTable<DataRow>();
                        bgwClientes.ReportProgress(0, "Actualizando Proveedores (#" + pageSize.ToString() + ") " + i.ToString() + " para " + numberOfPages.ToString());

                        Application.DoEvents();
                        string archivo = Path.GetTempFileName();
                        string json = JsonConvert.SerializeObject(tabla_guardar, Newtonsoft.Json.Formatting.None);//.Replace("[","").Replace("]","");
                        System.IO.File.WriteAllText(archivo, json);
                        string uri = GetURLService() + "conector/socionegociosMasivo";
                        System.Net.WebClient Client = new System.Net.WebClient();
                        Client.Headers.Add("user-agent", "Mozilla/5.0 (Windows NT 10.0; WOW64; Trident/7.0; rv:11.0) like Gecko");

                        Client.Headers.Add("Content-Type", "binary/octet-stream");
                        byte[] result = Client.UploadFile(uri, "POST", archivo);
                        string resultado = System.Text.Encoding.UTF8.GetString(result, 0, result.Length);
                        Bitacora.Log(resultado + archivo);
                        File.Delete(archivo);
                        Application.DoEvents();
                    }
                }
            }
            catch (Exception error)
            {
                string mensaje = "";
                mensaje = error.Message.ToString();
                if (error.InnerException != null)
                {
                    mensaje += " " + error.InnerException.Message.ToString();
                }
                Bitacora.Log(error.Message.ToString() + Environment.NewLine + mensaje);
            }
        }


        private string GetURLService()
        {
            return ConfigurationManager.AppSettings["direccion"].ToString();
        }
        private void cargar()
        {
            string sSQL = "";
            try
            {
                //TODO: Cargar por defecto
                //,'99' as ""Forma Pago""
                //,'PPD' as ""Metodo de Pago""
                //,'P01' as ""Uso CFDI""


                ConexionODBC oConexion = new ConexionODBC();
                //Clientes
                sSQL = @"SELECT T0.""CardCode"" as ""erpid"", T0.""CardName"" as ""razon"", T0.""E_Mail"" as ""email"", T0.""LicTradNum"" as ""rfc""
                , T0.""CreditLine"" as ""credito_limite"", T1.""ExtraDays"" as ""credito_dias"", T0.""VatIdUnCmp"" as ""taxId""
                FROM """ + oConexion.Schema + @""".OCRD T0 
                INNER JOIN """ + oConexion.Schema + @""".OCTG T1 ON T1.""GroupNum""=T0.""GroupNum""
                WHERE T0.""CardType""='S'
                ";
                if (!String.IsNullOrEmpty(filtroId.Text))
                {
                    sSQL += @" AND T0.""CardCode"" like '%" + filtroId.Text + "%' ";
                }
                if (!String.IsNullOrEmpty(filtroNombre.Text))
                {
                    sSQL += @" AND T0.""CardName"" like '%" + filtroNombre.Text + "%' ";
                }
                if (!String.IsNullOrEmpty(filtroRFC.Text))
                {
                    sSQL += @" AND T0.""LicTradNum"" like '%" + filtroRFC.Text + "%' ";
                }

                Bitacora.Log(" frmPrincipal 213 Antes de Conuslta  " + sSQL + Environment.NewLine);

                oDtClientes = oConexion.EjecutarConsulta(sSQL);

                if (oDtClientes != null)
                {
                    Bitacora.Log(" frmPrincipal 200  " + sSQL + Environment.NewLine);
                    this.dtgProveedores.DataSource = oDtClientes;
                    informacion.Text = "Cantidad de Proveedores " + oDtClientes.Rows.Count.ToString() + " " + DateTime.Now.ToShortDateString() + " " + DateTime.Now.ToShortTimeString();

                }
            }
            catch (Exception error)
            {
                Bitacora.Log(error.Message.ToString() + " frmPrincipal 200  " + sSQL + Environment.NewLine);
            }
        }

        private void frmPrincipal_Resize(object sender, EventArgs e)
        {
            if (this.WindowState == FormWindowState.Minimized)
            {
                notifyIcon.Visible = true;
                notifyIcon.ShowBalloonTip(3000);
                this.ShowInTaskbar = false;
            }
        }

        private void notifyIcon_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            this.WindowState = FormWindowState.Normal;
            this.ShowInTaskbar = true;
            notifyIcon.Visible = false;
        }

        private void frmPrincipal_FormClosed(object sender, FormClosedEventArgs e)
        {
            Application.Exit();
        }
        private int counter = 0;
        private void ejecutarAutomatico_CheckedChanged(object sender, EventArgs e)
        {
            if (ejecutarAutomatico.Checked)
            {

                counter = 0;
                timer1.Start();
            }
            else
            {
                counter = 0;
                timer1.Stop();
            }
        }
        private void timer1_Tick(object sender, EventArgs e)
        {
            if (ejecutarAutomatico.Checked)
            {
                counter++;

                proxima.Text = (segundos - counter).ToString();


                if (counter == segundos)
                {
                    timer1.Stop();
                    inicioOC.Checked = finOC.Checked =
                        cxpInicio.Checked = cxpFin.Checked =
                        pagoInicio.Checked = pagoFin.Checked
                        = true;

                    DateTime inicioTr = new DateTime(DateTime.Now.Year, DateTime.Now.Month
                        , DateTime.Now.Day, 0, 0, 0);

                    DateTime finTr = new DateTime(DateTime.Now.Year, DateTime.Now.Month
                        , DateTime.Now.Day, 23, 59, 59);

                    timer1.Stop();
                    counter = 0;

                    inicioOC.Value = pagoInicio.Value = cxpInicio.Value = ingresadoInicio.Value = this.tipoCambioInicio.Value = inicioTr;
                    cxpFin.Value = finOC.Value = pagoFin.Value = ingresadoFinal.Value = this.tipoCambioFin.Value = finTr;


                    counter = 0;
                    cargarGeneral();
                    proxima.Text = (segundos - counter).ToString();


                    timer1.Start();
                }

            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            actualizarClientes();
        }
        private void cargarGeneral()
        {
            this.timer1.Stop();

            cargar();
            cargarOC();
            cargarCxP();
            cargarPago();
            cargarIngresado();
            cargarTC();

            actualizarClientes();

            while (bgwClientes.IsBusy)
            {
                Application.DoEvents();
                asigar_texto_actualizacion("Esperando sincronizar Proveedores ");
            }
            actualizarOC();

            while (bgwOC.IsBusy)
            {
                Application.DoEvents();
                asigar_texto_actualizacion("Esperando sincronizar OC's ");
            }
            actualizarCxP();


            while (this.bgwCxP.IsBusy)
            {
                Application.DoEvents();
                asigar_texto_actualizacion("Esperando sincronizar CxP's ");
            }
            actualizarPago();


            while (this.bgwPago.IsBusy)
            {
                Application.DoEvents();
                asigar_texto_actualizacion("Esperando sincronizar Pagos ");
            }
            actualizarIngresado();

            while (this.bgwIngresado.IsBusy)
            {
                Application.DoEvents();
                asigar_texto_actualizacion("Esperando sincronizar Ingresos ");
            }

            actualizarTC();

            while (this.bgwTC.IsBusy)
            {
                Application.DoEvents();
                this.asigar_texto_actualizacion_tc("Esperando sincronizar TC ");
            }
            /*
            ivar date = DateTime.Now;
            if (date.Hour >= 20 || date.Hour < 8)
            {
                var DateTime8 = date.Date.AddHours((date.Hour > 8) ? 24 + 8 : 8);
                TimeSpan diff = DateTime8 - date;
            }
            */


            asigar_texto_actualizacion("Proceso de sincronización finalizado ");
            this.timer1.Start();
        }
        private void button4_Click(object sender, EventArgs e)
        {
            cargarOC();
        }

        DataTable oDtOC;
        BackgroundWorker bgwOC;

        private void actualizarOC()
        {
            if (oDtOC != null)
            {
                informacionOC.Text = "Actualizando OC " + DateTime.Now.ToShortDateString() + " " + DateTime.Now.ToShortTimeString();
                bgwOC = new BackgroundWorker();
                bgwOC.WorkerSupportsCancellation = true;
                bgwOC.WorkerReportsProgress = true;
                bgwOC.DoWork += new DoWorkEventHandler(bgwOC_DoWork);
                bgwOC.ProgressChanged += new ProgressChangedEventHandler(bgwOC_ProgressChanged);
                bgwOC.RunWorkerCompleted += new RunWorkerCompletedEventHandler(bgwOC_RunWorkerCompleted);
                bgwOC.RunWorkerAsync();
            }

        }

        private void bgwOC_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            informacionOC.Text = e.UserState.ToString();
            this.notifyIcon.Text = informacionPago.Text;
        }
        private void bgwOC_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            asigar_texto_actualizacionOC("OC's finalizado:" + DateTime.Now.ToShortDateString() + " " + DateTime.Now.ToShortTimeString());
            informacionOC.Text = "OC's finalizado " + DateTime.Now.ToShortDateString() + " " + DateTime.Now.ToShortTimeString();
            this.notifyIcon.Text = informacionPago.Text;
        }
        private void bgwOC_DoWork(object sender, DoWorkEventArgs e)
        {
            //Navegar por todas las empresas configuradas
            ingresar_oc();
            asigar_texto_actualizacion("OC:" + DateTime.Now.ToShortDateString() + " " + DateTime.Now.ToShortTimeString());
        }
        private void asigar_texto_actualizacionOC(string texto)
        {
            if (textoHora.InvokeRequired)
            {
                MethodInvoker invoker = new MethodInvoker(delegate
                {
                    textoHora.Text = texto + " " + DateTime.Now.ToShortDateString() + " " + DateTime.Now.ToShortTimeString();
                });
                textoHora.Invoke(invoker);
            }
            else
            {
                textoHora.Text = texto;
            }
        }
        private void ingresar_oc()
        {
            try
            {
                if (oDtOC != null)
                {
                    //Paginar
                    var results = from myRow in oDtOC.AsEnumerable()
                                  select myRow;
                    DataTable boundTable = results.CopyToDataTable<DataRow>();
                    int pageSize = 30;
                    int totalCount = boundTable.Rows.Count;
                    int numberOfPages = (int)Math.Ceiling((double)totalCount / pageSize);
                    for (int i = 0; i < numberOfPages; i++)
                    {
                        var consulta = results.Skip(i * pageSize).Take(pageSize);
                        DataTable tabla_guardar = consulta.CopyToDataTable<DataRow>();
                        bgwOC.ReportProgress(0, "Actualizando OC (#" + pageSize.ToString() + ") " + i.ToString() + " para " + numberOfPages.ToString());

                        Application.DoEvents();
                        string archivo = Path.GetTempFileName();
                        string json = JsonConvert.SerializeObject(tabla_guardar, Newtonsoft.Json.Formatting.None);//.Replace("[","").Replace("]","");
                        System.IO.File.WriteAllText(archivo, json);
                        string uri = GetURLService() + "Conector/ocmontoMasivo";
                        System.Net.WebClient Client = new System.Net.WebClient();
                        try
                        {
                            Client.Headers.Add("user-agent", "Mozilla/5.0 (Windows NT 10.0; WOW64; Trident/7.0; rv:11.0) like Gecko");


                            Client.Headers.Add("Content-Type", "binary/octet-stream");
                            byte[] result = Client.UploadFile(uri, "POST", archivo);
                            string resultado = System.Text.Encoding.UTF8.GetString(result, 0, result.Length);
                            Bitacora.Log(resultado + archivo);
                        }
                        catch (WebException webex)
                        {
                            WebResponse errResp = webex.Response;
                            using (Stream respStream = errResp.GetResponseStream())
                            {
                                StreamReader reader = new StreamReader(respStream);
                                string text = reader.ReadToEnd();
                                Bitacora.Log(text + archivo);
                            }
                        }
                        File.Delete(archivo);
                        Application.DoEvents();
                    }
                }
            }
            catch (Exception error)
            {
                string mensaje = "";
                mensaje = error.Message.ToString();
                if (error.InnerException != null)
                {
                    mensaje += " " + error.InnerException.Message.ToString();
                }
                Bitacora.Log(" frmPrincipal 386  " + error.Message.ToString() + Environment.NewLine + mensaje);
            }
        }


        private void cargarOC()
        {
            string sSQL = "";
            try
            {

                ConexionODBC oConexion = new ConexionODBC();
                //Clientes
                sSQL = @"SELECT T0.""DocNum"" as ""oc"", T1.""LicTradNum"" as ""rfc"", T0.""DocTotal"" as ""monto""

                FROM """ + oConexion.Schema + @""".OPOR T0  
                INNER JOIN """ + oConexion.Schema + @""".OCRD T1 ON T1.""CardCode""=T0.""CardCode"" 
                WHERE 1=1
                ";
                if (!String.IsNullOrEmpty(filtroOC.Text))
                {
                    sSQL += @" AND T0.""DocNum"" like '%" + filtroOC.Text + "%' ";
                }
                if (!String.IsNullOrEmpty(filtroRazon.Text))
                {
                    sSQL += @" AND T1.""CardName"" like '%" + filtroRazon.Text + "%' ";
                }
                if (!String.IsNullOrEmpty(filtroOCrfc.Text))
                {
                    sSQL += @" AND T1.""LicTradNum"" like '%" + filtroOCrfc.Text + "%' ";
                }

                if (inicioOC.Checked & finOC.Checked)
                {
                    sSQL += @" 
                            AND 
                            (
                                (T0.""UpdateDate"">='" + inicioOC.Value.ToString("yyyy-MM-dd") + @"' AND T0.""UpdateDate""<='" + finOC.Value.ToString("yyyy-MM-dd") + @"')
                                OR
                                (T0.""DocDate"">='" + inicioOC.Value.ToString("yyyy-MM-dd") + @"' AND T0.""DocDate""<='" + finOC.Value.ToString("yyyy-MM-dd") + @"')
                                OR
                                (T0.""CreateDate"">='" + inicioOC.Value.ToString("yyyy-MM-dd") + @"' AND T0.""CreateDate""<='" + finOC.Value.ToString("yyyy-MM-dd") + @"')
                            )
                        ";
                }
                else
                {
                    if (inicioOC.Checked)
                    {
                        sSQL += @" AND T0.""DocDate"">='" + inicioOC.Value.ToString("yyyy-MM-dd") + "'  ";
                    }
                    if (finOC.Checked)
                    {
                        sSQL += @" AND T0.""DocDate""<='" + finOC.Value.ToString("yyyy-MM-dd") + "' ";
                    }
                }


                oDtOC = oConexion.EjecutarConsulta(sSQL);
                if (oDtOC != null)
                {
                    Bitacora.Log(" frmPrincipal 421  " + sSQL + Environment.NewLine);
                    this.dtgOC.DataSource = oDtOC;
                    informacionOC.Text = "Cantidad de OC " + oDtOC.Rows.Count.ToString() + " " + DateTime.Now.ToShortDateString() + " " + DateTime.Now.ToShortTimeString();

                }
            }
            catch (Exception error)
            {
                Bitacora.Log(error.Message.ToString() + " frmPrincipal 429  " + sSQL + Environment.NewLine);
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            actualizarOC();
        }

        private void button6_Click(object sender, EventArgs e)
        {
            cargarCxP();
        }

        #region estadoCxPNuevo
        DataTable oDtCxPNuevo;
        BackgroundWorker bgwCxPNuevo;
        private void estadoCxPDefinitivo()
        {
            string sSQL = "";
            try
            {

                ConexionODBC oConexion = new ConexionODBC();

                sSQL = @"
                SELECT
                UCASE(T4.""U_COK1_01FOLIOUUID"") as ""uuid""
                , T3.""CardCode""
                , T3.""DocNum""
                , T3.""DocEntry""
                , T3.""DocDate""
                , T3.""CANCELED""
                , T3.""DocTotal""
                , T1.""LicTradNum"" as ""rfc""
                , TablaPago.""DocNum"" as ""Pago""
                , 
                CASE
                WHEN IFNULL(TablaPago.""DocNum"", 0) <> 0 THEN 'Pagado'
                ELSE 'Registrado'
                END
                as ""Pago""
                FROM """ + oConexion.Schema + @""".OPCH T3
                INNER JOIN """ + oConexion.Schema + @""".""@COK1_01RECPFACTU"" T4 ON T4.""U_COK1_01DOCENTDOC"" = T3.""DocEntry""
                INNER JOIN """ + oConexion.Schema + @""".OCRD T1 ON T1.""CardCode"" = T3.""CardCode""
                LEFT JOIN
                (
                SELECT
                T10.""CardCode"", T11.""InvType"", T11.""DocEntry"", T10.""DocNum""
                FROM """ + oConexion.Schema + @""".""OVPM"" T10
                INNER JOIN """ + oConexion.Schema + @""".""VPM2"" T11 ON T10.""DocNum"" = T11.""DocNum""
                INNER JOIN """ + oConexion.Schema + @""".""OPCH"" T12 ON T11.""DocEntry"" = T12.""DocEntry""
                WHERE T11.""InvType"" IN(18, 19)
                ) as TablaPago ON TablaPago.""DocEntry"" = T3.""DocEntry""
                WHERE 1=1
                ";

                if (!String.IsNullOrEmpty(pagoId.Text))
                {
                    sSQL += @" AND T3.""DocNum"" like '%" + pagoId.Text + "%' ";
                }
                if (!String.IsNullOrEmpty(pagoRfc.Text))
                {
                    sSQL += @" AND T3.""CardCode"" like '%" + pagoRfc.Text + "%' ";
                }
                if (!String.IsNullOrEmpty(pagoRfc.Text))
                {
                    sSQL += @" AND T1.""LicTradNum"" like '%" + pagoRfc.Text + "%' ";
                }
                if (!String.IsNullOrEmpty(pagoUUID.Text))
                {
                    sSQL += @" AND UCASE(T4.""U_COK1_01FOLIOUUID"") like '%" + pagoUUID.Text + "%' ";
                }

                if (pagoInicio.Checked & pagoFin.Checked)
                {
                    sSQL += @" 
                            AND 
                            (
                                (T3.""UpdateDate"">='" + pagoInicio.Value.ToString("yyyy-MM-dd") + @"' AND T3.""UpdateDate""<='" + pagoFin.Value.ToString("yyyy-MM-dd") + @"')
                                OR
                                (T3.""DocDate"">='" + pagoInicio.Value.ToString("yyyy-MM-dd") + @"' AND T3.""DocDate""<='" + pagoFin.Value.ToString("yyyy-MM-dd") + @"')
                                OR
                                (T3.""CreateDate"">='" + pagoInicio.Value.ToString("yyyy-MM-dd") + @"' AND T3.""CreateDate""<='" + pagoFin.Value.ToString("yyyy-MM-dd") + @"')
                            )
                        ";

                    sSQL += @" 
                            AND 
                            (
                                (T3.""UpdateDate"">='" + pagoInicio.Value.ToString("yyyy-MM-dd") + @"' AND T3.""UpdateDate""<='" + pagoFin.Value.ToString("yyyy-MM-dd") + @"')
                                OR
                                (T3.""DocDate"">='" + pagoInicio.Value.ToString("yyyy-MM-dd") + @"' AND T3.""DocDate""<='" + pagoFin.Value.ToString("yyyy-MM-dd") + @"')
                                OR
                                (T3.""CreateDate"">='" + pagoInicio.Value.ToString("yyyy-MM-dd") + @"' AND T3.""CreateDate""<='" + pagoFin.Value.ToString("yyyy-MM-dd") + @"')
                            )
                        ";
                }
                else
                {
                    if (pagoInicio.Checked)
                    {
                        sSQL += @" AND T3.""DocDate"">='" + pagoInicio.Value.ToString("yyyy-MM-dd") + "' ";
                    }
                    if (pagoFin.Checked)
                    {
                        sSQL += @" AND T3.""DocDate""<='" + pagoFin.Value.ToString("yyyy-MM-dd") + "' ";
                    }
                }

                dtgEstadoNuevo.DataSource = null;
                oDtCxPNuevo = oConexion.EjecutarConsulta(sSQL);
                if (oDtCxPNuevo != null)
                {
                    Bitacora.Log(" frmPrincipal 670 EstadoNuevo  " + sSQL + Environment.NewLine);
                    this.dtgEstadoNuevo.DataSource = oDtCxP;
                    informacionCxP.Text = "Cantidad de Pagos Nuevos " + dtgEstadoNuevo.Rows.Count.ToString() + " " 
                        + DateTime.Now.ToShortDateString() + " " + DateTime.Now.ToShortTimeString();

                }
            }
            catch (Exception error)
            {
                Bitacora.Log(error.Message.ToString() + " frmPrincipal estadoCxPDefinitivo  " + sSQL + Environment.NewLine);
            }
        }

        #endregion estadoCxPNuevo

        private void cargarCxP()
        {
            string sSQL = "";
            try
            {

                ConexionODBC oConexion = new ConexionODBC();

                //voucher
                //rfc
                //invoice
                //estado
                //fechaEstimada
                //uuid

                sSQL = @"SELECT 
                T0.""DocNum"" as ""voucher""
                , T1.""LicTradNum"" as ""rfc""
                , T0.""NumAtCard"" as ""invoice""
                , 'Registrada' as ""estado""
                , T0.""DocDueDate"" as ""fechaEstimada""
                , T2.""U_COK1_01FOLIOUUID"" as ""uuid""
                FROM """ + oConexion.Schema + @""".OPCH T0  
                INNER JOIN """ + oConexion.Schema + @""".OCRD T1 ON T1.""CardCode""=T0.""CardCode"" 
                INNER JOIN """ + oConexion.Schema + @""".""@COK1_01RECPFACTU"" T2 ON T2.""U_COK1_01DOCENTDOC""=T0.""DocEntry""
                WHERE 1=1
                ";

                string sSQLAnticipo = @"SELECT 
                T0.""DocNum"" as ""voucher""
                , T1.""LicTradNum"" as ""rfc""
                , T0.""NumAtCard"" as ""invoice""
                , 'Registrada' as ""estado""
                , T0.""DocDueDate"" as ""fechaEstimada""
                , T2.""U_COK1_01FOLIOUUID"" as ""uuid""
                FROM """ + oConexion.Schema + @""".ODPO T0  
                INNER JOIN """ + oConexion.Schema + @""".OCRD T1 ON T1.""CardCode""=T0.""CardCode"" 
                INNER JOIN """ + oConexion.Schema + @""".""@COK1_01RECPFACTU"" T2 ON T2.""U_COK1_01DOCENTDOC""=T0.""DocEntry""
                WHERE 1=1
                ";

                if (!String.IsNullOrEmpty(cxpId.Text))
                {
                    sSQL += @" AND T0.""DocNum"" like '%" + cxpId.Text + "%' ";
                    sSQLAnticipo += @" AND T0.""DocNum"" like '%" + cxpId.Text + "%' ";
                }
                if (!String.IsNullOrEmpty(cxpId.Text))
                {
                    sSQL += @" AND T0.""DocNum"" like '%" + cxpId.Text + "%' ";
                    sSQLAnticipo += @" AND T0.""DocNum"" like '%" + cxpId.Text + "%' ";
                }
                if (!String.IsNullOrEmpty(cxpCliente.Text))
                {
                    sSQL += @" AND T0.""CardCode"" like '%" + cxpCliente.Text + "%' ";
                    sSQLAnticipo += @" AND T0.""CardCode"" like '%" + cxpCliente.Text + "%' ";
                }
                if (!String.IsNullOrEmpty(cxpRfc.Text))
                {
                    sSQL += @" AND T1.""LicTradNum"" like '%" + cxpRfc.Text + "%' ";
                    sSQLAnticipo += @" AND T1.""LicTradNum"" like '%" + cxpRfc.Text + "%' ";
                }
                if (!String.IsNullOrEmpty(cxpUUID.Text))
                {
                    sSQL += @" AND T2.""U_COK1_01FOLIOUUID"" like '%" + cxpUUID.Text + "%' ";
                    sSQLAnticipo += @" AND T2.""U_COK1_01FOLIOUUID"" like '%" + cxpUUID.Text + "%' ";
                }
                if (cxpInicio.Checked & cxpFin.Checked)
                {
                    sSQL += @" 
                            AND 
                            (
                                (T0.""UpdateDate"">='" + cxpInicio.Value.ToString("yyyy-MM-dd") + @"' AND T0.""UpdateDate""<='" + cxpFin.Value.ToString("yyyy-MM-dd") + @"')
                                OR
                                (T0.""DocDate"">='" + cxpInicio.Value.ToString("yyyy-MM-dd") + @"' AND T0.""DocDate""<='" + cxpFin.Value.ToString("yyyy-MM-dd") + @"')
                                OR
                                (T0.""CreateDate"">='" + cxpInicio.Value.ToString("yyyy-MM-dd") + @"' AND T0.""CreateDate""<='" + cxpFin.Value.ToString("yyyy-MM-dd") + @"')
                            )
                        ";

                    sSQLAnticipo += @" 
                            AND 
                            (
                                (T0.""UpdateDate"">='" + cxpInicio.Value.ToString("yyyy-MM-dd") + @"' AND T0.""UpdateDate""<='" + cxpFin.Value.ToString("yyyy-MM-dd") + @"')
                                OR
                                (T0.""DocDate"">='" + cxpInicio.Value.ToString("yyyy-MM-dd") + @"' AND T0.""DocDate""<='" + cxpFin.Value.ToString("yyyy-MM-dd") + @"')
                                OR
                                (T0.""CreateDate"">='" + cxpInicio.Value.ToString("yyyy-MM-dd") + @"' AND T0.""CreateDate""<='" + cxpFin.Value.ToString("yyyy-MM-dd") + @"')
                            )
                        ";
                }
                else
                {
                    if (cxpInicio.Checked)
                    {
                        sSQL += @" AND T0.""DocDate"">='" + cxpInicio.Value.ToString("yyyy-MM-dd") + "' ";
                        sSQLAnticipo += @" AND T0.""DocDate"">='" + cxpInicio.Value.ToString("yyyy-MM-dd") + "' ";
                    }
                    if (cxpFin.Checked)
                    {
                        sSQL += @" AND T0.""DocDate""<='" + cxpFin.Value.ToString("yyyy-MM-dd") + "' ";
                        sSQLAnticipo += @" AND T0.""DocDate""<='" + cxpFin.Value.ToString("yyyy-MM-dd") + "' ";
                    }
                }


                string sSQLgeneral = sSQL + " UNION ALL " + sSQLAnticipo;


                oDtCxP = oConexion.EjecutarConsulta(sSQLgeneral);
                if (oDtCxP != null)
                {
                    Bitacora.Log(" frmPrincipal 474  " + sSQL + Environment.NewLine);
                    this.dtgCxP.DataSource = oDtCxP;
                    informacionCxP.Text = "Cantidad de CxP " + oDtCxP.Rows.Count.ToString() + " " + DateTime.Now.ToShortDateString() + " " + DateTime.Now.ToShortTimeString();

                }
            }
            catch (Exception error)
            {
                Bitacora.Log(error.Message.ToString() + " frmPrincipal 429  " + sSQL + Environment.NewLine);
            }
        }

        private void button5_Click(object sender, EventArgs e)
        {
            actualizarCxP();
        }


        DataTable oDtCxP;
        BackgroundWorker bgwCxP;

        private void actualizarCxP()
        {
            if (oDtCxP != null)
            {
                informacion.Text = "Actualizando CxP " + DateTime.Now.ToShortDateString() + " " + DateTime.Now.ToShortTimeString();
                bgwCxP = new BackgroundWorker();
                bgwCxP.WorkerSupportsCancellation = true;
                bgwCxP.WorkerReportsProgress = true;
                bgwCxP.DoWork += new DoWorkEventHandler(bgwCxP_DoWork);
                bgwCxP.ProgressChanged += new ProgressChangedEventHandler(bgwCxP_ProgressChanged);
                bgwCxP.RunWorkerCompleted += new RunWorkerCompletedEventHandler(bgwCxP_RunWorkerCompleted);
                bgwCxP.RunWorkerAsync();
            }

        }

        private void bgwCxP_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            informacionCxP.Text = e.UserState.ToString();
            this.notifyIcon.Text = informacionPago.Text;
        }
        private void bgwCxP_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            asigar_texto_actualizacionOC("CxP's finalizado:" + DateTime.Now.ToShortDateString() + " " + DateTime.Now.ToShortTimeString());
            informacionCxP.Text = "CxP's finalizado " + DateTime.Now.ToShortDateString() + " " + DateTime.Now.ToShortTimeString();
            this.notifyIcon.Text = informacionPago.Text;
        }
        private void bgwCxP_DoWork(object sender, DoWorkEventArgs e)
        {
            //Navegar por todas las empresas configuradas
            ingresar_cxp();
            asigar_texto_actualizacion("CxP:" + DateTime.Now.ToShortDateString() + " " + DateTime.Now.ToShortTimeString());
        }

        private void ingresar_cxp()
        {
            try
            {
                if (oDtCxP != null)
                {
                    //Paginar
                    var results = from myRow in oDtCxP.AsEnumerable()
                                  select myRow;
                    DataTable boundTable = results.CopyToDataTable<DataRow>();
                    int pageSize = 30;
                    int totalCount = boundTable.Rows.Count;
                    int numberOfPages = (int)Math.Ceiling((double)totalCount / pageSize);
                    for (int i = 0; i < numberOfPages; i++)
                    {
                        var consulta = results.Skip(i * pageSize).Take(pageSize);
                        DataTable tabla_guardar = consulta.CopyToDataTable<DataRow>();
                        bgwCxP.ReportProgress(0, "Actualizando CxP (#" + pageSize.ToString() + ") " + i.ToString() + " para " + numberOfPages.ToString());

                        Application.DoEvents();
                        string archivo = Path.GetTempFileName();
                        string json = JsonConvert.SerializeObject(tabla_guardar, Newtonsoft.Json.Formatting.None);//.Replace("[","").Replace("]","");
                        System.IO.File.WriteAllText(archivo, json);
                        string uri = GetURLService() + "Conector/payableMasivoUUID";
                        System.Net.WebClient Client = new System.Net.WebClient();
                        Client.Headers.Add("user-agent", "Mozilla/5.0 (Windows NT 10.0; WOW64; Trident/7.0; rv:11.0) like Gecko");

                        Client.Headers.Add("Content-Type", "binary/octet-stream");
                        byte[] result = Client.UploadFile(uri, "POST", archivo);
                        string resultado = System.Text.Encoding.UTF8.GetString(result, 0, result.Length);
                        Bitacora.Log(resultado + archivo);
                        File.Delete(archivo);
                        Application.DoEvents();
                    }
                }
            }
            catch (Exception error)
            {
                string mensaje = "";
                mensaje = error.Message.ToString();
                if (error.InnerException != null)
                {
                    mensaje += " " + error.InnerException.Message.ToString();
                }
                Bitacora.Log(" frmPrincipal 591  " + error.Message.ToString() + Environment.NewLine + mensaje);
            }
        }

        DataTable oDtPago;
        BackgroundWorker bgwPago;

        private void cargarPago()
        {
            string sSQL = "";
            try
            {

                //banco*
                //cuenta*
                //rfc*
                //pago*
                //factura
                //moneda*
                //monto*
                //voucher*
                //montofactura*
                //tipocambio*
                //fechapago*
                //parcialidad*
                //saldoinsoluto*
                //uuid*
                //rfcEmisor

                ConexionODBC oConexion = new ConexionODBC();

                sSQL = @"SELECT DISTINCT
                T0.""DocNum"" as ""pago""
                , T0.""BankCode"" as ""banco""
                , T5.""U_COK1_01CTAORIG"" as ""cuenta""
                , T1.""LicTradNum"" as ""rfc""
                , T0.""TrsfrDate"" as ""fechapago""
                , T0.""DocTotal"" as ""monto""
                , T0.""DocCurr"" as ""moneda""
                , T2.""DocNum"" as ""voucher""
                , T2.""DocRate"" as ""tipocambio""
                , T2.""SumApplied"" as ""montofactura""
                , UCASE(T4.""U_COK1_01FOLIOUUID"") as ""uuid""
                , 0 as ""saldoinsoluto""
                , 1 as ""parcialidad""
                FROM """ + oConexion.Schema + @""".OVPM T0  
                INNER JOIN """ + oConexion.Schema + @""".OCRD T1 ON T1.""CardCode""=T0.""CardCode"" 
                INNER JOIN """ + oConexion.Schema + @""".VPM2 T2 ON T2.""DocNum""=T0.""DocEntry"" 
                INNER JOIN """ + oConexion.Schema + @""".OPCH T3 ON T3.""DocEntry""=T2.""DocEntry"" AND T3.""CardCode""=T0.""CardCode""  
                INNER JOIN """ + oConexion.Schema + @""".""@COK1_01RECPFACTU"" T4 ON T4.""U_COK1_01DOCENTDOC""=T3.""DocEntry""
                LEFT JOIN """ + oConexion.Schema + @""".""@COK1_01TRANSFPOL"" T5 ON T0.""TransId""=T5.""U_COK1_01TRNSIDTSF""  AND T5.""U_COK1_01RFCTRNSF""=T1.""LicTradNum""
                WHERE 1=1
                ";
                if (!String.IsNullOrEmpty(pagoId.Text))
                {
                    sSQL += @" AND T0.""DocNum"" like '%" + pagoId.Text + "%' ";
                }
                if (!String.IsNullOrEmpty(pagoRazon.Text))
                {
                    sSQL += @" AND T1.""CardName"" like '%" + pagoRazon.Text + "%' ";
                }
                if (!String.IsNullOrEmpty(pagoRfc.Text))
                {
                    sSQL += @" AND T1.""LicTradNum"" like '%" + pagoRfc.Text + "%' ";
                }

                if (pagoInicio.Checked & pagoFin.Checked)
                {
                    sSQL += @" 
                            AND 
                            (
                                (T0.""UpdateDate"">='" + pagoInicio.Value.ToString("yyyy-MM-dd") + @"' AND T0.""UpdateDate""<='" + pagoFin.Value.ToString("yyyy-MM-dd") + @"')
                                OR
                                (T0.""DocDate"">='" + pagoInicio.Value.ToString("yyyy-MM-dd") + @"' AND T0.""DocDate""<='" + pagoFin.Value.ToString("yyyy-MM-dd") + @"')
                                OR
                                (T0.""CreateDate"">='" + pagoInicio.Value.ToString("yyyy-MM-dd") + @"' AND T0.""CreateDate""<='" + pagoFin.Value.ToString("yyyy-MM-dd") + @"')
                            )
                        ";
                }
                else
                {
                    if (pagoInicio.Checked)
                    {
                        sSQL += @" AND T0.""DocDate"">='" + pagoInicio.Value.ToString("yyyy-MM-dd") + "' ";
                    }
                    if (pagoFin.Checked)
                    {
                        sSQL += @" AND T0.""DocDate""<='" + pagoFin.Value.ToString("yyyy-MM-dd") + "' ";
                    }
                }

                if (!String.IsNullOrEmpty(pagoProveedor.Text))
                {
                    sSQL += @" AND T0.""CardCode"" like '%" + pagoProveedor.Text + "%' ";
                }
                if (!String.IsNullOrEmpty(pagoUUID.Text))
                {
                    sSQL += @" AND T4.""U_COK1_01FOLIOUUID"" like '%" + pagoUUID.Text + "%' ";
                }

                oDtPago = oConexion.EjecutarConsulta(sSQL);
                if (oDtPago != null)
                {
                    Bitacora.Log(" frmPrincipal 651  " + sSQL + Environment.NewLine);
                    this.dtgPago.DataSource = oDtPago;
                    informacionPago.Text = "Cantidad de Pago " + oDtPago.Rows.Count.ToString() + " " + DateTime.Now.ToShortDateString() + " " + DateTime.Now.ToShortTimeString();

                }
            }
            catch (Exception error)
            {
                Bitacora.Log(error.Message.ToString() + " frmPrincipal 659  " + sSQL + Environment.NewLine);
            }
        }

        private void button8_Click(object sender, EventArgs e)
        {
            cargarPago();
            cargarPagoResumen();
            estadoCxPDefinitivo();
        }

        private void button7_Click(object sender, EventArgs e)
        {
            actualizarPago();
        }

        private void actualizarPago()
        {
            if (oDtPago != null)
            {
                informacionPago.Text = "Actualizando Pago " + DateTime.Now.ToShortDateString() + " " + DateTime.Now.ToShortTimeString();
                bgwPago = new BackgroundWorker();
                bgwPago.WorkerSupportsCancellation = true;
                bgwPago.WorkerReportsProgress = true;
                bgwPago.DoWork += new DoWorkEventHandler(bgwPago_DoWork);
                bgwPago.ProgressChanged += new ProgressChangedEventHandler(bgwPago_ProgressChanged);
                bgwPago.RunWorkerCompleted += new RunWorkerCompletedEventHandler(bgwPago_RunWorkerCompleted);
                bgwPago.RunWorkerAsync();
            }
        }

        private void bgwPago_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            informacionPago.Text = e.UserState.ToString();
            this.notifyIcon.Text = informacionPago.Text;
        }
        private void bgwPago_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            asigar_texto_actualizacionOC("Pagos finalizado:" + DateTime.Now.ToShortDateString() + " " + DateTime.Now.ToShortTimeString());
            informacionPago.Text = "Pagos finalizado " + DateTime.Now.ToShortDateString() + " " + DateTime.Now.ToShortTimeString();
            this.notifyIcon.Text = informacionPago.Text;
        }
        private void bgwPago_DoWork(object sender, DoWorkEventArgs e)
        {
            //Navegar por todas las empresas configuradas
            ingresar_pago();
            asigar_texto_actualizacion("Pago:" + DateTime.Now.ToShortDateString() + " " + DateTime.Now.ToShortTimeString());
        }

        private void ingresar_pago()
        {
            try
            {
                if (oDtPago != null)
                {
                    //Paginar
                    var results = from myRow in oDtPago.AsEnumerable()
                                  select myRow;
                    DataTable boundTable = results.CopyToDataTable<DataRow>();
                    int pageSize = 30;
                    int totalCount = boundTable.Rows.Count;
                    int numberOfPages = (int)Math.Ceiling((double)totalCount / pageSize);
                    for (int i = 0; i < numberOfPages; i++)
                    {
                        var consulta = results.Skip(i * pageSize).Take(pageSize);
                        DataTable tabla_guardar = consulta.CopyToDataTable<DataRow>();
                        bgwPago.ReportProgress(0, "Actualizando Pago (#" + pageSize.ToString() + ") " + i.ToString() + " para " + numberOfPages.ToString());

                        Application.DoEvents();
                        string archivo = Path.GetTempFileName();
                        string json = JsonConvert.SerializeObject(tabla_guardar, Newtonsoft.Json.Formatting.None);//.Replace("[","").Replace("]","");
                        System.IO.File.WriteAllText(archivo, json);
                        string uri = GetURLService() + "Conector/listapagoMasivoServicio";
                        System.Net.WebClient Client = new System.Net.WebClient();
                        Client.Headers.Add("user-agent", "Mozilla/5.0 (Windows NT 10.0; WOW64; Trident/7.0; rv:11.0) like Gecko");
                        Client.Headers.Add("Content-Type", "binary/octet-stream");
                        byte[] result = Client.UploadFile(uri, "POST", archivo);
                        string resultado = System.Text.Encoding.UTF8.GetString(result, 0, result.Length);
                        Bitacora.Log(resultado + archivo);
                        File.Delete(archivo);
                        Application.DoEvents();
                    }
                }
            }
            catch (Exception error)
            {
                string mensaje = "";
                mensaje = error.Message.ToString();
                if (error.InnerException != null)
                {
                    mensaje += " " + error.InnerException.Message.ToString();
                }
                Bitacora.Log(" frmPrincipal 752  " + error.Message.ToString() + Environment.NewLine + mensaje);
            }
        }

        private void button9_Click(object sender, EventArgs e)
        {
            cargarGeneral();
        }

        private void button11_Click(object sender, EventArgs e)
        {
            cargarIngresado();
        }

        private void button10_Click(object sender, EventArgs e)
        {
            actualizarIngresado();
        }
        DataTable oDtIngresado;
        BackgroundWorker bgwIngresado;
        private void cargarIngresado()
        {
            string sSQL = "";
            try
            {


                ConexionODBC oConexion = new ConexionODBC();


                //                SELECT DISTINCT
                //T0."DocNum" as "Ingreso"
                //, T1."LicTradNum" as "rfc"
                //, T0."DocTotal" as "monto"
                //, T4."U_COK1_01FOLIOUUID" as "uuid"
                //FROM "OPDN" T0
                //INNER JOIN "OCRD" T1 ON T1."CardCode" = T0."CardCode"
                //INNER JOIN "@COK1_01RECPFACTU" T4 ON T4."U_COK1_01DOCENTDOC" = T0."DocEntry"
                //INNER JOIN "@COK1_01TRANSFPOL" T5 ON T5."U_COK1_01MONTOTRSF" = T0."DocTotal" AND T5."U_COK1_01RFCTRNSF" = T1."LicTradNum"


                //            SELECT T0.[CardCode] as [BP Code], T0.[CardName] as [BP Name], 
                //T0.[DocNum] as [PO Number], T1.[ItemCode] as [Stock Item], 
                //T1.[Quantity], T1.[OpenQty], 
                //SUM(isnull(T3.[Quantity],0)) as 'Received Qty', 
                //T1.[Quantity]-SUM(isnull(T3.[Quantity],0)) as [Left to Deliver]
                //    FROM dbo.OPOR T0
                //INNER JOIN dbo.POR1 T1 ON T0.DocEntry = T1.DocEntry
                //LEFT JOIN dbo.PDN1 T3 ON T3.BaseEntry = T0.DocEntry AND T3.BaseLine = T1.Linenum
                //LEFT JOIN dbo.OPDN T2 ON T2.DocEntry = T3.DocEntry
                //WHERE T0.[DocDate]  >=[%0] AND T0.[DocDate] <=[%1] AND T0.[CardName] =[% 2]
                //GROUP BY T0.[CardCode], T0.[CardName], T0.[DocNum], T1.[ItemCode],
                //T1.[Quantity], T1.[OpenQty]

                sSQL = @"SELECT DISTINCT
                T0.""DocNum"" as ""oc""
                ,T2.""DocNum"" as ""recibo""
                ,T11.""LicTradNum"" as ""rfc""
                ,T2.""DocDate"" as ""Fecha""
                FROM """ + oConexion.Schema + @""".OPOR T0  
                INNER JOIN """ + oConexion.Schema + @""".OCRD T11 ON T11.""CardCode""=T0.""CardCode"" 
                INNER JOIN """ + oConexion.Schema + @""".POR1 T1 ON T1.""DocEntry""=T0.""DocEntry"" 
                INNER JOIN """ + oConexion.Schema + @""".PDN1 T3 ON T3.""BaseEntry""=T0.""DocEntry"" AND T3.""BaseLine"" = T1.""LineNum""
                INNER JOIN """ + oConexion.Schema + @""".OPDN T2 ON T2.""DocEntry""=T3.""DocEntry"" 
                WHERE 1=1
                ";
                if (!String.IsNullOrEmpty(ingresadoId.Text))
                {
                    sSQL += @" AND T2.""DocNum"" like '%" + ingresadoId.Text + "%' ";
                }
                if (!String.IsNullOrEmpty(ingresadoRazon.Text))
                {
                    sSQL += @" AND T11.""CardName"" like '%" + ingresadoRazon.Text + "%' ";
                }
                if (!String.IsNullOrEmpty(this.ingresadoRfc.Text))
                {
                    sSQL += @" AND T11.""LicTradNum"" like '%" + ingresadoRfc.Text + "%' ";
                }

                if (ingresadoInicio.Checked & ingresadoFinal.Checked)
                {
                    sSQL += @" 
                            AND 
                            (
                                (T2.""UpdateDate"">='" + ingresadoInicio.Value.ToString("yyyy-MM-dd") + @"' AND T2.""UpdateDate""<='" + ingresadoFinal.Value.ToString("yyyy-MM-dd") + @"')
                                OR
                                (T2.""DocDate"">='" + ingresadoInicio.Value.ToString("yyyy-MM-dd") + @"' AND T2.""DocDate""<='" + ingresadoFinal.Value.ToString("yyyy-MM-dd") + @"')
                                OR
                                (T2.""CreateDate"">='" + ingresadoInicio.Value.ToString("yyyy-MM-dd") + @"' AND T2.""CreateDate""<='" + ingresadoFinal.Value.ToString("yyyy-MM-dd") + @"')
                            )
                        ";
                }
                else
                {
                    if (ingresadoInicio.Checked)
                    {
                        sSQL += @" AND T2.""DocDate"">='" + ingresadoInicio.Value.ToString("yyyy-MM-dd") + "' ";
                    }
                    if (ingresadoFinal.Checked)
                    {
                        sSQL += @" AND T2.""DocDate""<='" + ingresadoFinal.Value.ToString("yyyy-MM-dd") + "' ";
                    }
                }

                if (!String.IsNullOrEmpty(ingresadoCardCode.Text))
                {
                    sSQL += @" AND T0.""CardCode"" like '%" + ingresadoCardCode.Text + "%' ";
                }
                if (!String.IsNullOrEmpty(ingresadoOC.Text))
                {
                    sSQL += @" AND T0.""DocNum"" like '%" + ingresadoOC.Text + "%' ";
                }

                oDtIngresado = oConexion.EjecutarConsulta(sSQL);
                if (oDtIngresado != null)
                {
                    Bitacora.Log(" frmPrincipal 1018  " + sSQL + Environment.NewLine);
                    this.dtgIngresado.DataSource = oDtIngresado;
                    informacionIngresado.Text = "Cantidad de Ingresos " + oDtIngresado.Rows.Count.ToString() + " " + DateTime.Now.ToShortDateString() + " " + DateTime.Now.ToShortTimeString();

                }
            }
            catch (Exception error)
            {
                string mensaje = "";
                mensaje = error.Message.ToString();
                if (error.InnerException != null)
                {
                    mensaje += " " + error.InnerException.Message.ToString();
                }
                Bitacora.Log(error.Message.ToString() + " frmPrincipal 659  " + sSQL + Environment.NewLine + mensaje);
            }
        }


        private void actualizarIngresado()
        {
            if (oDtIngresado != null)
            {
                informacionIngresado.Text = "Actualizando Ingresado " + DateTime.Now.ToShortDateString() + " " + DateTime.Now.ToShortTimeString();
                bgwIngresado = new BackgroundWorker();
                bgwIngresado.WorkerSupportsCancellation = true;
                bgwIngresado.WorkerReportsProgress = true;
                bgwIngresado.DoWork += new DoWorkEventHandler(bgwIngresado_DoWork);
                bgwIngresado.ProgressChanged += new ProgressChangedEventHandler(bgwIngresado_ProgressChanged);
                bgwIngresado.RunWorkerCompleted += new RunWorkerCompletedEventHandler(bgwIngresado_RunWorkerCompleted);
                bgwIngresado.RunWorkerAsync();
            }

        }

        private void bgwIngresado_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            informacionIngresado.Text = e.UserState.ToString();
            this.notifyIcon.Text = informacionIngresado.Text;
        }
        private void bgwIngresado_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            asigar_texto_actualizacionOC("Ingresados finalizado:" + DateTime.Now.ToShortDateString() + " " + DateTime.Now.ToShortTimeString());
            informacionIngresado.Text = "Ingresados finalizado " + DateTime.Now.ToShortDateString() + " " + DateTime.Now.ToShortTimeString();
            this.notifyIcon.Text = informacionIngresado.Text;
        }
        private void bgwIngresado_DoWork(object sender, DoWorkEventArgs e)
        {
            //Navegar por todas las empresas configuradas
            ingresar_Ingresado();
            asigar_texto_actualizacion("Ingresado:" + DateTime.Now.ToShortDateString() + " " + DateTime.Now.ToShortTimeString());
        }

        private void ingresar_Ingresado()
        {
            try
            {
                if (this.oDtIngresado != null)
                {
                    //Paginar
                    var results = from myRow in oDtIngresado.AsEnumerable()
                                  select myRow;
                    DataTable boundTable = results.CopyToDataTable<DataRow>();
                    int pageSize = 30;
                    int totalCount = boundTable.Rows.Count;
                    int numberOfPages = (int)Math.Ceiling((double)totalCount / pageSize);
                    for (int i = 0; i < numberOfPages; i++)
                    {
                        var consulta = results.Skip(i * pageSize).Take(pageSize);
                        DataTable tabla_guardar = consulta.CopyToDataTable<DataRow>();
                        bgwIngresado.ReportProgress(0, "Actualizando Ingresado (#" + pageSize.ToString() + ") " + i.ToString() + " para " + numberOfPages.ToString());

                        Application.DoEvents();
                        string archivo = Path.GetTempFileName();
                        string json = JsonConvert.SerializeObject(tabla_guardar, Newtonsoft.Json.Formatting.None);//.Replace("[","").Replace("]","");
                        System.IO.File.WriteAllText(archivo, json);
                        string uri = GetURLService() + "Conector/listaOCIngresadoMasivoServicio";
                        System.Net.WebClient Client = new System.Net.WebClient();
                        Client.Headers.Add("user-agent", "Mozilla/5.0 (Windows NT 10.0; WOW64; Trident/7.0; rv:11.0) like Gecko");

                        Client.Headers.Add("Content-Type", "binary/octet-stream");
                        byte[] result = Client.UploadFile(uri, "POST", archivo);
                        string resultado = System.Text.Encoding.UTF8.GetString(result, 0, result.Length);
                        Bitacora.Log(resultado + archivo);
                        File.Delete(archivo);
                        Application.DoEvents();
                    }
                }
            }
            catch (Exception error)
            {
                string mensaje = "";
                mensaje = error.Message.ToString();
                if (error.InnerException != null)
                {
                    mensaje += " " + error.InnerException.Message.ToString();
                }
                Bitacora.Log(" frmPrincipal 1103  " + mensaje);
            }
        }

        private void tabPage2_Click(object sender, EventArgs e)
        {

        }

        private void button13_Click(object sender, EventArgs e)
        {
            cargarTC();
        }

        DataTable oDtTC;
        BackgroundWorker bgwTC;
        private void cargarTC()
        {
            string sSQL = "";
            try
            {


                ConexionODBC oConexion = new ConexionODBC();
                sSQL = @"SELECT
                T0.""Rate"" as ""Rate""
                ,T0.""RateDate"" as ""Fecha""
                FROM """ + oConexion.Schema + @""".ORTT T0  
                WHERE 1=1 AND T0.""Currency""='USD'
                ";

                if (ingresadoInicio.Checked & ingresadoFinal.Checked)
                {
                    sSQL += @" 
                            AND 
                            (
                                (T0.""RateDate"">='" + this.tipoCambioInicio.Value.ToString("yyyy-MM-dd") + @"' AND T0.""RateDate""<='" + this.tipoCambioFin.Value.ToString("yyyy-MM-dd") + @"')
                            )
                        ";
                }

                oDtTC = oConexion.EjecutarConsulta(sSQL);
                if (oDtTC != null)
                {
                    Bitacora.Log(" frmPrincipal 1229  " + sSQL + Environment.NewLine);
                    this.dtgTC.DataSource = oDtTC;
                    informacionTC.Text = "Cantidad de TC " + oDtIngresado.Rows.Count.ToString() + " " + DateTime.Now.ToShortDateString() + " " + DateTime.Now.ToShortTimeString();

                }
            }
            catch (Exception error)
            {
                string mensaje = "";
                mensaje = error.Message.ToString();
                if (error.InnerException != null)
                {
                    mensaje += " " + error.InnerException.Message.ToString();
                }
                Bitacora.Log(error.Message.ToString() + " frmPrincipal 659  " + sSQL + Environment.NewLine + mensaje);
            }
        }


        private void actualizarTC()
        {
            if (oDtTC != null)
            {
                informacionTC.Text = "Actualizando Tipo de Cambio " + DateTime.Now.ToShortDateString() + " " + DateTime.Now.ToShortTimeString();
                bgwTC = new BackgroundWorker();
                bgwTC.WorkerSupportsCancellation = true;
                bgwTC.WorkerReportsProgress = true;
                bgwTC.DoWork += new DoWorkEventHandler(bgwTC_DoWork);
                bgwTC.ProgressChanged += new ProgressChangedEventHandler(bgwTC_ProgressChanged);
                bgwTC.RunWorkerCompleted += new RunWorkerCompletedEventHandler(bgwTC_RunWorkerCompleted);
                bgwTC.RunWorkerAsync();
            }

        }
        private void bgwTC_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            informacionTC.Text = e.UserState.ToString();
            this.notifyIcon.Text = informacionTC.Text;
        }
        private void bgwTC_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            asigar_texto_actualizacion_tc("Tipo de Cambio:" + DateTime.Now.ToShortDateString() + " " + DateTime.Now.ToShortTimeString());
            informacionTC.Text = "Tipo de Cambio finalizado " + DateTime.Now.ToShortDateString() + " " + DateTime.Now.ToShortTimeString();
            this.notifyIcon.Text = informacionTC.Text;
        }
        private void bgwTC_DoWork(object sender, DoWorkEventArgs e)
        {
            //Navegar por todas las empresas configuradas
            ingresar_tc();
            asigar_texto_actualizacion_tc("Tipo de Cambio:" + DateTime.Now.ToShortDateString() + " " + DateTime.Now.ToShortTimeString());
        }
        private void ingresar_tc()
        {
            try
            {
                if (this.oDtTC != null)
                {
                    //Paginar
                    var results = from myRow in oDtTC.AsEnumerable()
                                  select myRow;
                    DataTable boundTable = results.CopyToDataTable<DataRow>();
                    int pageSize = 30;
                    int totalCount = boundTable.Rows.Count;
                    int numberOfPages = (int)Math.Ceiling((double)totalCount / pageSize);
                    for (int i = 0; i < numberOfPages; i++)
                    {
                        var consulta = results.Skip(i * pageSize).Take(pageSize);
                        DataTable tabla_guardar = consulta.CopyToDataTable<DataRow>();
                        bgwTC.ReportProgress(0, "Actualizando TC (#" + pageSize.ToString() + ") " + i.ToString() + " para " + numberOfPages.ToString());

                        Application.DoEvents();
                        string archivo = Path.GetTempFileName();
                        string json = JsonConvert.SerializeObject(tabla_guardar, Newtonsoft.Json.Formatting.None);//.Replace("[","").Replace("]","");
                        System.IO.File.WriteAllText(archivo, json);
                        string uri = GetURLService() + "Conector/tcMasivo";
                        System.Net.WebClient Client = new System.Net.WebClient();
                        try
                        {
                            Client.Headers.Add("user-agent", "Mozilla/5.0 (Windows NT 10.0; WOW64; Trident/7.0; rv:11.0) like Gecko");
                            Client.Headers.Add("Content-Type", "binary/octet-stream");
                            byte[] result = Client.UploadFile(uri, "POST", archivo);
                            string resultado = System.Text.Encoding.UTF8.GetString(result, 0, result.Length);
                            Bitacora.Log(resultado + archivo);
                        }
                        catch (WebException webex)
                        {
                            WebResponse errResp = webex.Response;
                            using (Stream respStream = errResp.GetResponseStream())
                            {
                                StreamReader reader = new StreamReader(respStream);
                                string text = reader.ReadToEnd();
                                Bitacora.Log(text + archivo);
                            }
                        }
                        File.Delete(archivo);
                        Application.DoEvents();
                    }
                }
            }
            catch (Exception error)
            {
                string mensaje = "";
                mensaje = error.Message.ToString();
                if (error.InnerException != null)
                {
                    mensaje += " " + error.InnerException.Message.ToString();
                }
                Bitacora.Log(" frmPrincipal 1344  " + error.Message.ToString() + Environment.NewLine + mensaje);
            }
        }

        private void asigar_texto_actualizacion_tc(string texto)
        {
            if (textoHora.InvokeRequired)
            {
                MethodInvoker invoker = new MethodInvoker(delegate
                {
                    textoHora.Text = texto + " " + DateTime.Now.ToShortDateString() + " " + DateTime.Now.ToShortTimeString();
                });
                textoHora.Invoke(invoker);
            }
            else
            {
                textoHora.Text = texto;
            }
        }

        private void button12_Click(object sender, EventArgs e)
        {
            actualizarTC();
        }

        private void button14_Click(object sender, EventArgs e)
        {
            actualizarPagoResumen();
        }
        #region PagoResumen
        DataTable oDtPagoResumen;
        BackgroundWorker bgwPagoResumen;
        private void cargarPagoResumen(bool cargarDtRegistroPendiente = false)
        {
            string sSQL = "";
            try
            {

                //banco*
                //cuenta*
                //rfc*
                //pago*
                //factura
                //moneda*
                //monto*
                //voucher*
                //montofactura*
                //tipocambio*
                //fechapago*
                //parcialidad*
                //saldoinsoluto*
                //uuid*
                //rfcEmisor

                ConexionODBC oConexion = new ConexionODBC();

                sSQL = @"SELECT DISTINCT
                UCASE(T4.""U_COK1_01FOLIOUUID"") as ""uuid""
                FROM """ + oConexion.Schema + @""".OVPM T0  
                INNER JOIN """ + oConexion.Schema + @""".OCRD T1 ON T1.""CardCode""=T0.""CardCode"" 
                INNER JOIN """ + oConexion.Schema + @""".VPM2 T2 ON T2.""DocNum""=T0.""DocEntry"" 
                INNER JOIN """ + oConexion.Schema + @""".OPCH T3 ON T3.""DocEntry""=T2.""DocEntry"" AND T3.""CardCode""=T0.""CardCode""  
                INNER JOIN """ + oConexion.Schema + @""".""@COK1_01RECPFACTU"" T4 ON T4.""U_COK1_01DOCENTDOC""=T3.""DocEntry""
                LEFT JOIN """ + oConexion.Schema + @""".""@COK1_01TRANSFPOL"" T5 ON T0.""TransId""=T5.""U_COK1_01TRNSIDTSF""  AND T5.""U_COK1_01RFCTRNSF""=T1.""LicTradNum""
                WHERE 1=1
                ";

                if (cargarDtRegistroPendiente)
                {
                    if (dtRegistrdosPendientes != null)
                    {

                        string commaSeparatedString = String.Join("','", dtRegistrdosPendientes.AsEnumerable().Select(x => x.Field<string>("uuid").ToString()).ToArray());
                        sSQL += @" AND UCASE(T4.""U_COK1_01FOLIOUUID"") IN ('" + commaSeparatedString + "')";
                    }



                }
                else
                {
                    if (!String.IsNullOrEmpty(pagoId.Text))
                    {
                        sSQL += @" AND T0.""DocNum"" like '%" + pagoId.Text + "%' ";
                    }
                    if (!String.IsNullOrEmpty(pagoRazon.Text))
                    {
                        sSQL += @" AND T1.""CardName"" like '%" + pagoRazon.Text + "%' ";
                    }
                    if (!String.IsNullOrEmpty(pagoRfc.Text))
                    {
                        sSQL += @" AND T1.""LicTradNum"" like '%" + pagoRfc.Text + "%' ";
                    }

                    if (pagoInicio.Checked & pagoFin.Checked)
                    {
                        sSQL += @" 
                            AND 
                            (
                                (T0.""UpdateDate"">='" + pagoInicio.Value.ToString("yyyy-MM-dd") + @"' AND T0.""UpdateDate""<='" + pagoFin.Value.ToString("yyyy-MM-dd") + @"')
                                OR
                                (T0.""DocDate"">='" + pagoInicio.Value.ToString("yyyy-MM-dd") + @"' AND T0.""DocDate""<='" + pagoFin.Value.ToString("yyyy-MM-dd") + @"')
                                OR
                                (T0.""CreateDate"">='" + pagoInicio.Value.ToString("yyyy-MM-dd") + @"' AND T0.""CreateDate""<='" + pagoFin.Value.ToString("yyyy-MM-dd") + @"')
                            )
                        ";
                    }
                    else
                    {
                        if (pagoInicio.Checked)
                        {
                            sSQL += @" AND T0.""DocDate"">='" + pagoInicio.Value.ToString("yyyy-MM-dd") + "' ";
                        }
                        if (pagoFin.Checked)
                        {
                            sSQL += @" AND T0.""DocDate""<='" + pagoFin.Value.ToString("yyyy-MM-dd") + "' ";
                        }
                    }

                    if (!String.IsNullOrEmpty(pagoProveedor.Text))
                    {
                        sSQL += @" AND T0.""CardCode"" like '%" + pagoProveedor.Text + "%' ";
                    }
                    if (!String.IsNullOrEmpty(pagoUUID.Text))
                    {
                        sSQL += @" AND T4.""U_COK1_01FOLIOUUID"" like '%" + pagoUUID.Text + "%' ";
                    }
                }

                oDtPagoResumen = oConexion.EjecutarConsulta(sSQL);
                if (oDtPagoResumen != null)
                {
                    Bitacora.Log(" frmPrincipal 1454  " + sSQL + Environment.NewLine);
                    this.dtgPagoResumen.DataSource = oDtPagoResumen;
                    informacionPago.Text = "Cantidad de Pago Resumen" + oDtPago.Rows.Count.ToString() + " " + DateTime.Now.ToShortDateString() + " " + DateTime.Now.ToShortTimeString();

                }
            }
            catch (Exception error)
            {
                Bitacora.Log(error.Message.ToString() + " frmPrincipal 659  " + sSQL + Environment.NewLine);
            }
        }

        private void actualizarPagoResumen()
        {
            if (oDtPagoResumen != null)
            {
                pagoLog.Text = "";
                informacionPago.Text = "Actualizando Pago Resumen" + DateTime.Now.ToShortDateString() + " " + DateTime.Now.ToShortTimeString();
                asginaPagoLog(informacionPago.Text);
                bgwPagoResumen = new BackgroundWorker();
                bgwPagoResumen.WorkerSupportsCancellation = true;
                bgwPagoResumen.WorkerReportsProgress = true;
                bgwPagoResumen.DoWork += new DoWorkEventHandler(bgwPagoResumen_DoWork);
                bgwPagoResumen.ProgressChanged += new ProgressChangedEventHandler(bgwPagoResumen_ProgressChanged);
                bgwPagoResumen.RunWorkerCompleted += new RunWorkerCompletedEventHandler(bgwPagoResumen_RunWorkerCompleted);
                bgwPagoResumen.RunWorkerAsync();
            }
        }
        private void bgwPagoResumen_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            informacionPago.Text = e.UserState.ToString();
            this.notifyIcon.Text = informacionPago.Text;
        }
        private void bgwPagoResumen_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            asigar_texto_actualizacionPago("Pagos Resumen finalizado:" + DateTime.Now.ToShortDateString() + " " + DateTime.Now.ToShortTimeString());
            informacionPago.Text = "Pagos Resumen finalizado " + DateTime.Now.ToShortDateString() + " " + DateTime.Now.ToShortTimeString();
            asginaPagoLog(informacionPago.Text);
            this.notifyIcon.Text = informacionPago.Text;
        }
        private void asigar_texto_actualizacionPago(string texto)
        {
            if (informacionPago.InvokeRequired)
            {
                MethodInvoker invoker = new MethodInvoker(delegate
                {
                    informacionPago.Text = texto + " " + DateTime.Now.ToShortDateString() + " " + DateTime.Now.ToShortTimeString();
                });
                informacionPago.Invoke(invoker);
            }
            else
            {
                informacionPago.Text = texto;
            }
        }
        private void asginaPagoLog(string texto)
        {
            if (this.pagoLog.InvokeRequired)
            {
                MethodInvoker invoker = new MethodInvoker(delegate
                {
                    pagoLog.Text += texto + " " + DateTime.Now.ToShortDateString() + " "
                    + DateTime.Now.ToShortTimeString() + Environment.NewLine;
                    pagoLog.SelectionStart = pagoLog.Text.Length;
                    pagoLog.ScrollToCaret();
                });
                pagoLog.Invoke(invoker);
            }
            else
            {
                pagoLog.Text += texto + " " + DateTime.Now.ToShortDateString() + " " + Environment.NewLine;
                pagoLog.SelectionStart = pagoLog.Text.Length;
                pagoLog.ScrollToCaret();
            }
        }


        private void bgwPagoResumen_DoWork(object sender, DoWorkEventArgs e)
        {
            //Navegar por todas las empresas configuradas
            ingresar_pagoResumen();
            asigar_texto_actualizacion("Pago Resumen:" + DateTime.Now.ToShortDateString() + " " + DateTime.Now.ToShortTimeString());
        }
        private void ingresar_pagoResumen()
        {
            try
            {
                if (oDtPagoResumen != null)
                {
                    asginaPagoLog("Inicio envio del Resumen ");
                    //Paginar
                    var results = from myRow in oDtPagoResumen.AsEnumerable()
                                  select myRow;
                    DataTable boundTable = results.CopyToDataTable<DataRow>();
                    int pageSize = 30;
                    int totalCount = boundTable.Rows.Count;
                    int numberOfPages = (int)Math.Ceiling((double)totalCount / pageSize);
                    for (int i = 0; i < numberOfPages; i++)
                    {
                        var consulta = results.Skip(i * pageSize).Take(pageSize);
                        DataTable tabla_guardar = consulta.CopyToDataTable<DataRow>();
                        bgwPagoResumen.ReportProgress(0, "Actualizando Pago Resumen (#" + pageSize.ToString() + ") " + i.ToString() + " para " + numberOfPages.ToString());

                        Application.DoEvents();
                        string archivo = Path.GetTempFileName();
                        string json = JsonConvert.SerializeObject(tabla_guardar, Newtonsoft.Json.Formatting.None);//.Replace("[","").Replace("]","");
                        System.IO.File.WriteAllText(archivo, json);
                        string uri = GetURLService() + "Conector/listaPagoMasivoResumen";
                        System.Net.WebClient Client = new System.Net.WebClient();
                        Client.Headers.Add("user-agent", "Mozilla/5.0 (Windows NT 10.0; WOW64; Trident/7.0; rv:11.0) like Gecko");
                        Client.Headers.Add("Content-Type", "binary/octet-stream");
                        byte[] result = Client.UploadFile(uri, "POST", archivo);
                        string resultado = System.Text.Encoding.UTF8.GetString(result, 0, result.Length);
                        asginaPagoLog("Pago Resumen Resultado: " + resultado + Environment.NewLine);
                        Bitacora.Log(resultado + archivo);
                        File.Delete(archivo);
                        Application.DoEvents();
                    }
                }
            }
            catch (Exception error)
            {
                string mensaje = "";
                mensaje = error.Message.ToString();
                if (error.InnerException != null)
                {
                    mensaje += " " + error.InnerException.Message.ToString();
                }
                asginaPagoLog("Pago Resumen Error: " + mensaje + Environment.NewLine);
                Bitacora.Log(" frmPrincipal 752  " + error.Message.ToString() + Environment.NewLine + mensaje);
            }
        }

        #endregion PagoResumen

        private void button15_Click(object sender, EventArgs e)
        {
            cargarPagoPendienteResumen();
        }

        #region PagoPendienteResumen
        DataTable oDtPagoPendienteResumen;
        BackgroundWorker bgwPagoPendienteResumen;
        private void cargarPagoPendienteResumen()
        {
            //Descargar de la direccion URL
            button2.Enabled = false;
            bgwPagoPendienteResumen = new BackgroundWorker();
            bgwPagoPendienteResumen.WorkerSupportsCancellation = true;
            bgwPagoPendienteResumen.WorkerReportsProgress = true;
            bgwPagoPendienteResumen.DoWork += new DoWorkEventHandler(bgwPagoPendienteResumen_DoWork);
            bgwPagoPendienteResumen.ProgressChanged += new ProgressChangedEventHandler(bgwPagoPendienteResumen_ProgressChanged);
            bgwPagoPendienteResumen.RunWorkerCompleted += new RunWorkerCompletedEventHandler(bgwbgwPagoPendienteResumen_RunWorkerCompleted);
            bgwPagoPendienteResumen.RunWorkerAsync();
        }
        private void bgwPagoPendienteResumen_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            asigar_texto_actualizacionPago(e.UserState.ToString());
        }
        private void bgwPagoPendienteResumen_DoWork(object sender, DoWorkEventArgs e)
        {
            try
            {
                //Descargar los archivos guardarlo donde deben estar 
                string url = ConfigurationManager.AppSettings["direccion"].ToString() + "Conector/pagosObtenerPendientes/";
                // You need to post the data as key value pairs:
                string postData = "rfc=" + pagoRfc.Text;
                byte[] byteArray = Encoding.UTF8.GetBytes(postData);

                // Post the data to the right place.
                Uri target = new Uri(url);
                WebRequest request = WebRequest.Create(target);

                request.Method = "POST";
                request.ContentType = "application/x-www-form-urlencoded";
                request.ContentLength = byteArray.Length;

                using (var dataStream = request.GetRequestStream())
                {
                    dataStream.Write(byteArray, 0, byteArray.Length);
                }

                using (var response = (HttpWebResponse)request.GetResponse())
                {
                    Console.WriteLine("Error code: {0}", response.StatusCode);
                    using (Stream data = response.GetResponseStream())
                    using (var reader = new StreamReader(data))
                    {
                        string text = reader.ReadToEnd();
                        //DataTable table = (DataTable)JsonConvert.DeserializeObject(text, (typeof(Documento)));

                        oDtPagoPendienteResumen = JsonConvert.DeserializeObject<DataTable>(text);
                        dtgPagoPendiente.DataSource = oDtPagoPendienteResumen;

                    }
                }
            }
            catch (Exception error)
            {
                string mensaje = "";
                mensaje = error.Message.ToString();
                if (error.InnerException != null)
                {
                    mensaje += " " + error.InnerException.Message.ToString();
                }
                Bitacora.Log(error.Message.ToString() + Environment.NewLine + mensaje);
            }
        }


        private void bgwbgwPagoPendienteResumen_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            asigar_texto_actualizacionPago("Pagos Pendiente Resumen finalizado:" + DateTime.Now.ToShortDateString() + " " + DateTime.Now.ToShortTimeString());
            informacionPago.Text = "Pagos Resumen Pendiente finalizado " + DateTime.Now.ToShortDateString() + " " + DateTime.Now.ToShortTimeString();
            asginaPagoLog(informacionPago.Text);
            this.notifyIcon.Text = informacionPago.Text;
        }
        #endregion PagoPendienteResumen

        private void button16_Click(object sender, EventArgs e)
        {
            cargarRegistradosPendientes();
        }

        DataTable dtRegistrdosPendientes = null;
        private void cargarRegistradosPendientes()
        {

            string url = ConfigurationManager.AppSettings["direccion"].ToString() + "Sync/registradopendiente/";
            try
            {
                dtRegistrdosPendientes = null;
                // You need to post the data as key value pairs:
                string postData = string.Empty;
                postData = "&rfc=" + this.pagoRfc.Text
                + "&razon=" + this.pagoRazon.Text
                + "&uuid=" + this.pagoUUID.Text
                + "&folio=" + this.pagoId.Text
                + "&proveedor=" + this.pagoProveedor.Text;
                ;
                byte[] byteArray = Encoding.UTF8.GetBytes(postData);

                // Post the data to the right place.
                Uri target = new Uri(url);
                WebRequest request = WebRequest.Create(target);

                request.Method = "POST";
                request.ContentType = "application/x-www-form-urlencoded";
                request.ContentLength = byteArray.Length;

                using (var dataStream = request.GetRequestStream())
                {
                    dataStream.Write(byteArray, 0, byteArray.Length);
                }

                using (var response = (HttpWebResponse)request.GetResponse())
                {
                    Console.WriteLine("Error code: {0}", response.StatusCode);
                    using (Stream data = response.GetResponseStream())
                    using (var reader = new StreamReader(data))
                    {
                        string text = reader.ReadToEnd();
                        //DataTable table = (DataTable)JsonConvert.DeserializeObject(text, (typeof(Documento)));

                        DataTable table = JsonConvert.DeserializeObject<DataTable>(text);
                        dtRegistrdosPendientes = new DataTable();
                        dtRegistrdosPendientes = table;
                        if (dtRegistrdosPendientes != null)
                        {
                            this.dtgRegistradoPendiente.DataSource = dtRegistrdosPendientes;
                            informacionPago.Text = "Cantidad de Pago Resumen" + dtgRegistradoPendiente.Rows.Count.ToString() + " "
                                + DateTime.Now.ToShortDateString() + " " + DateTime.Now.ToShortTimeString();

                            cargarPagoResumen(true);
                            actualizarPagoResumen();
                        }

                    }
                }

            }
            catch (Exception error)
            {
                string mensaje = "";
                mensaje = error.Message.ToString();
                if (error.InnerException != null)
                {
                    mensaje += " " + error.InnerException.Message.ToString();
                }
                Bitacora.Log(error.Message.ToString() + Environment.NewLine + mensaje);
            }
        }

        private void button17_Click(object sender, EventArgs e)
        {
            ActualizarRecibo();
        }



        private void ActualizarRecibo()
        {
            if (oDtIngresado != null)
            {
                informacionRecibo.Text = "Actualizando Recibo " + DateTime.Now.ToShortDateString() + " " + DateTime.Now.ToShortTimeString();
                bgwRecibo = new BackgroundWorker();
                bgwRecibo.WorkerSupportsCancellation = true;
                bgwRecibo.WorkerReportsProgress = true;
                bgwRecibo.DoWork += new DoWorkEventHandler(bgwRecibo_DoWork);
                bgwRecibo.ProgressChanged += new ProgressChangedEventHandler(bgwRecibo_ProgressChanged);
                bgwRecibo.RunWorkerCompleted += new RunWorkerCompletedEventHandler(bgwRecibo_RunWorkerCompleted);
                bgwRecibo.RunWorkerAsync();
            }

        }

        private void bgwRecibo_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            informacionRecibo.Text = e.UserState.ToString();
            this.notifyIcon.Text = informacionRecibo.Text;
        }
        private void bgwRecibo_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            asigar_texto_actualizacionOC("Recibos finalizado:" + DateTime.Now.ToShortDateString() + " " + DateTime.Now.ToShortTimeString());
            informacionRecibo.Text = "Recibos finalizado " + DateTime.Now.ToShortDateString() + " " + DateTime.Now.ToShortTimeString();
            this.notifyIcon.Text = informacionRecibo.Text;
        }
        private void bgwRecibo_DoWork(object sender, DoWorkEventArgs e)
        {
            //Navegar por todas las empresas configuradas
            ingresar_Recibo();
            asigar_texto_actualizacion("Recibo:" + DateTime.Now.ToShortDateString() + " " + DateTime.Now.ToShortTimeString());
        }

        private void ingresar_Recibo()
        {
            try
            {
                if (this.oDtRecibo != null)
                {
                    //Paginar
                    var results = from myRow in oDtRecibo.AsEnumerable()
                                  select myRow;
                    DataTable boundTable = results.CopyToDataTable<DataRow>();
                    int pageSize = 30;
                    int totalCount = boundTable.Rows.Count;
                    int numberOfPages = (int)Math.Ceiling((double)totalCount / pageSize);
                    for (int i = 0; i < numberOfPages; i++)
                    {
                        var consulta = results.Skip(i * pageSize).Take(pageSize);
                        DataTable tabla_guardar = consulta.CopyToDataTable<DataRow>();
                        bgwRecibo.ReportProgress(0, "Actualizando Recibo (#" + pageSize.ToString() + ") " + i.ToString() + " para " + numberOfPages.ToString());

                        Application.DoEvents();
                        string archivo = Path.GetTempFileName();
                        string json = JsonConvert.SerializeObject(tabla_guardar, Newtonsoft.Json.Formatting.None);//.Replace("[","").Replace("]","");
                        System.IO.File.WriteAllText(archivo, json);
                        string uri = GetURLService() + "Conector/ReciboMasivoServicio";
                        System.Net.WebClient Client = new System.Net.WebClient();
                        Client.Headers.Add("user-agent", "Mozilla/5.0 (Windows NT 10.0; WOW64; Trident/7.0; rv:11.0) like Gecko");

                        Client.Headers.Add("Content-Type", "binary/octet-stream");
                        byte[] result = Client.UploadFile(uri, "POST", archivo);
                        string resultado = System.Text.Encoding.UTF8.GetString(result, 0, result.Length);
                        Bitacora.Log(resultado + archivo);
                        File.Delete(archivo);
                        Application.DoEvents();
                    }
                }
            }
            catch (Exception error)
            {
                string mensaje = "";
                mensaje = error.Message.ToString();
                if (error.InnerException != null)
                {
                    mensaje += " " + error.InnerException.Message.ToString();
                }
                Bitacora.Log(" frmPrincipal 1103  " + mensaje);
            }
        }



        private void button18_Click(object sender, EventArgs e)
        {
            CargarRecibo();
        }
        DataTable oDtRecibo;
        BackgroundWorker bgwRecibo;
        private void CargarRecibo()
        {
            string sSQL = "";
            try
            {


                ConexionODBC oConexion = new ConexionODBC();
                sSQL = @"SELECT DISTINCT
                T0.""DocNum"" as ""oc""
                ,T2.""DocNum"" as ""recibo""
                ,T11.""LicTradNum"" as ""rfc""
                ,T2.""DocDate"" as ""Fecha""
                FROM """ + oConexion.Schema + @""".OPOR T0  
                INNER JOIN """ + oConexion.Schema + @""".OCRD T11 ON T11.""CardCode""=T0.""CardCode"" 
                INNER JOIN """ + oConexion.Schema + @""".POR1 T1 ON T1.""DocEntry""=T0.""DocEntry"" 
                INNER JOIN """ + oConexion.Schema + @""".PDN1 T3 ON T3.""BaseEntry""=T0.""DocEntry"" AND T3.""BaseLine"" = T1.""LineNum""
                INNER JOIN """ + oConexion.Schema + @""".OPDN T2 ON T2.""DocEntry""=T3.""DocEntry"" 
                WHERE 1=1
                ";
                if (!String.IsNullOrEmpty(ingresadoId.Text))
                {
                    sSQL += @" AND T2.""DocNum"" like '%" + ingresadoId.Text + "%' ";
                }
                if (!String.IsNullOrEmpty(ingresadoRazon.Text))
                {
                    sSQL += @" AND T11.""CardName"" like '%" + ingresadoRazon.Text + "%' ";
                }
                if (!String.IsNullOrEmpty(this.ingresadoRfc.Text))
                {
                    sSQL += @" AND T11.""LicTradNum"" like '%" + ingresadoRfc.Text + "%' ";
                }

                if (ingresadoInicio.Checked & ingresadoFinal.Checked)
                {
                    sSQL += @" 
                            AND 
                            (
                                (T2.""UpdateDate"">='" + ingresadoInicio.Value.ToString("yyyy-MM-dd") + @"' AND T2.""UpdateDate""<='" + ingresadoFinal.Value.ToString("yyyy-MM-dd") + @"')
                                OR
                                (T2.""DocDate"">='" + ingresadoInicio.Value.ToString("yyyy-MM-dd") + @"' AND T2.""DocDate""<='" + ingresadoFinal.Value.ToString("yyyy-MM-dd") + @"')
                                OR
                                (T2.""CreateDate"">='" + ingresadoInicio.Value.ToString("yyyy-MM-dd") + @"' AND T2.""CreateDate""<='" + ingresadoFinal.Value.ToString("yyyy-MM-dd") + @"')
                            )
                        ";
                }
                else
                {
                    if (ingresadoInicio.Checked)
                    {
                        sSQL += @" AND T2.""DocDate"">='" + ingresadoInicio.Value.ToString("yyyy-MM-dd") + "' ";
                    }
                    if (ingresadoFinal.Checked)
                    {
                        sSQL += @" AND T2.""DocDate""<='" + ingresadoFinal.Value.ToString("yyyy-MM-dd") + "' ";
                    }
                }

                if (!String.IsNullOrEmpty(ingresadoCardCode.Text))
                {
                    sSQL += @" AND T0.""CardCode"" like '%" + ingresadoCardCode.Text + "%' ";
                }
                if (!String.IsNullOrEmpty(ingresadoOC.Text))
                {
                    sSQL += @" AND T0.""DocNum"" like '%" + ingresadoOC.Text + "%' ";
                }

                oDtRecibo = oConexion.EjecutarConsulta(sSQL);
                if (oDtRecibo != null)
                {
                    Bitacora.Log(" frmPrincipal 1018  " + sSQL + Environment.NewLine);
                    this.dtgRecibo.DataSource = oDtRecibo;
                    informacionRecibo.Text = "Cantidad de Ingresos " + oDtRecibo.Rows.Count.ToString() + " " + DateTime.Now.ToShortDateString() + " " + DateTime.Now.ToShortTimeString();

                }
            }
            catch (Exception error)
            {
                string mensaje = "";
                mensaje = error.Message.ToString();
                if (error.InnerException != null)
                {
                    mensaje += " " + error.InnerException.Message.ToString();
                }
                Bitacora.Log(error.Message.ToString() + " frmPrincipal 659  " + sSQL + Environment.NewLine + mensaje);
            }
        }


        //private void vaciarDatos(DataTable table)
        //{
        //    try
        //    {
        //        dtPagosRegistradosPendientes.Rows.Clear();
        //        foreach (DataRow registro in table.Rows)
        //        {
        //            string uuidtr = registro["UUID"].ToString();
        //            int n = dtPagosRegistradosPendientes.Rows.Add();
        //            dtPagosRegistradosPendientes.Rows[n].Cells["Estadop"].Value = registro["estado"].ToString();
        //            dtPagosRegistradosPendientes.Rows[n].Cells["UUIDp"].Value = registro["UUID"].ToString();
        //        }



        //    }
        //    catch (Exception error)
        //    {
        //        string mensaje = "";
        //        mensaje = error.Message.ToString();
        //        if (error.InnerException != null)
        //        {
        //            mensaje += " " + error.InnerException.Message.ToString();
        //        }
        //        Bitacora.Log(error.Message.ToString() + Environment.NewLine + mensaje);
        //    }
        //}


        /*
        #region RegistradosPendientes
        BackgroundWorker bgwRegistradosPendientes;
        private void syncArchivos()
        {
            button2.Enabled = false;
            bgwRegistradosPendientes = new BackgroundWorker();
            bgwRegistradosPendientes.WorkerSupportsCancellation = true;
            bgwRegistradosPendientes.WorkerReportsProgress = true;
            bgwRegistradosPendientes.DoWork += new DoWorkEventHandler(bgwRegistradosPendientes_DoWork);
            bgwRegistradosPendientes.ProgressChanged += new ProgressChangedEventHandler(bgwRegistradosPendientes_ProgressChanged);
            bgwRegistradosPendientes.RunWorkerCompleted += new RunWorkerCompletedEventHandler(bgwRegistradosPendientes_RunWorkerCompleted);
            bgwRegistradosPendientes.RunWorkerAsync();
        }

        private void bgwRegistradosPendientes_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            asigar_texto_actualizacion("Registrados y Pendientes:" + DateTime.Now.ToShortDateString() + " " + DateTime.Now.ToShortTimeString());
            textoHora.Text = "Fin Registrados y Pendientes " + DateTime.Now.ToShortDateString() + " " + DateTime.Now.ToShortTimeString();
            button16.Enabled = true;
        }

        #endregion
        */
    }
}
