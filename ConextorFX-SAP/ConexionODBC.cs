﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.Odbc;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace FacturasSincronizador
{
    //TODO
    class ConexionODBC
    {
        public string SConn = "DRIVER={HDBODBC};UID=B1ADMIN;PWD=7SkyOne*NmNmODg1Mm;SERVERNODE=10.161.60.152:30015;DATABASENAME=SK1;CURRENTSCHEMA=PTM_2022_FINAL";

        public string Schema;

        public ConexionODBC()
        {
            try
            {
                SConn = ConfigurationManager.ConnectionStrings["PTM"].ConnectionString;
                if (String.IsNullOrEmpty(SConn))
                {
                    SConn = ConfigurationManager.AppSettings["PTM"].ToString();
                }
            }
            catch (Exception Error)
            {
                Bitacora.Log(Error.Message.ToString() + " SConn 32  " + Environment.NewLine);
            }


            try
            {
                Schema = "PTM_2022_FINAL";// ConfigurationManager.AppSettings["Schema"].ToString();
            }
            catch (Exception Error)
            {
                Bitacora.Log(Error.Message.ToString() + " SConn 45  " + Environment.NewLine);
            }
            
        }

        public DataTable EjecutarConsulta(string pQuery)
        {
            try
            {
                DataTable oDT = new DataTable();
                //Ejecutar para SQL Server
                oDT = new DataTable();
                OdbcConnection conn = new OdbcConnection(SConn);
                conn.Open();
                OdbcDataAdapter oOdbcDataAdapter = new OdbcDataAdapter(pQuery, conn);                
                oOdbcDataAdapter.Fill(oDT);
                oOdbcDataAdapter.Dispose();
                conn.Close();
                return oDT;
            }
            catch (OdbcException e)
            {
                Bitacora.Log(DateTime.Now.ToShortDateString() + " " + DateTime.Now.ToShortTimeString() + ":"
                                    + e.Message + "\n" + pQuery + "\n La conexion es: " + SConn);
                return null;
            }

        }

    }

}
