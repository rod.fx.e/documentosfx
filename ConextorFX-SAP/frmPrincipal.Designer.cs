#region Copyright Syncfusion Inc. 2001 - 2017
// Copyright Syncfusion Inc. 2001 - 2017. All rights reserved.
// Use of this code is subject to the terms of our license.
// A copy of the current license can be obtained at any time by e-mailing
// licensing@syncfusion.com. Any infringement will be prosecuted under
// applicable laws. 
#endregion
namespace ConextorFX_SAP
{
    partial class frmPrincipal
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmPrincipal));
            this.tab = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.filtroNombre = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.filtroId = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.button2 = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.dtgProveedores = new System.Windows.Forms.DataGridView();
            this.informacion = new System.Windows.Forms.TextBox();
            this.filtroRFC = new System.Windows.Forms.TextBox();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.filtroRazon = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.finOC = new System.Windows.Forms.DateTimePicker();
            this.inicioOC = new System.Windows.Forms.DateTimePicker();
            this.label8 = new System.Windows.Forms.Label();
            this.filtroOC = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.button3 = new System.Windows.Forms.Button();
            this.button4 = new System.Windows.Forms.Button();
            this.dtgOC = new System.Windows.Forms.DataGridView();
            this.informacionOC = new System.Windows.Forms.TextBox();
            this.filtroOCrfc = new System.Windows.Forms.TextBox();
            this.tabPage14 = new System.Windows.Forms.TabPage();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.label31 = new System.Windows.Forms.Label();
            this.dateTimePicker1 = new System.Windows.Forms.DateTimePicker();
            this.dateTimePicker2 = new System.Windows.Forms.DateTimePicker();
            this.label32 = new System.Windows.Forms.Label();
            this.ReciboTxt = new System.Windows.Forms.TextBox();
            this.label33 = new System.Windows.Forms.Label();
            this.label36 = new System.Windows.Forms.Label();
            this.label37 = new System.Windows.Forms.Label();
            this.button17 = new System.Windows.Forms.Button();
            this.button18 = new System.Windows.Forms.Button();
            this.dtgRecibo = new System.Windows.Forms.DataGridView();
            this.informacionRecibo = new System.Windows.Forms.TextBox();
            this.textBox4 = new System.Windows.Forms.TextBox();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.cxpUUID = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.cxpFin = new System.Windows.Forms.DateTimePicker();
            this.cxpInicio = new System.Windows.Forms.DateTimePicker();
            this.label10 = new System.Windows.Forms.Label();
            this.cxpCliente = new System.Windows.Forms.TextBox();
            this.cxpId = new System.Windows.Forms.TextBox();
            this.label18 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.button5 = new System.Windows.Forms.Button();
            this.button6 = new System.Windows.Forms.Button();
            this.dtgCxP = new System.Windows.Forms.DataGridView();
            this.informacionCxP = new System.Windows.Forms.TextBox();
            this.cxpRfc = new System.Windows.Forms.TextBox();
            this.tabPage4 = new System.Windows.Forms.TabPage();
            this.pagosTotalPendientes = new System.Windows.Forms.TextBox();
            this.button16 = new System.Windows.Forms.Button();
            this.button15 = new System.Windows.Forms.Button();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage8 = new System.Windows.Forms.TabPage();
            this.dtgPago = new System.Windows.Forms.DataGridView();
            this.tabPage9 = new System.Windows.Forms.TabPage();
            this.dtgPagoResumen = new System.Windows.Forms.DataGridView();
            this.tabPage11 = new System.Windows.Forms.TabPage();
            this.dtgPagoPendiente = new System.Windows.Forms.DataGridView();
            this.tabPage10 = new System.Windows.Forms.TabPage();
            this.pagoLog = new System.Windows.Forms.TextBox();
            this.tabPage12 = new System.Windows.Forms.TabPage();
            this.dtgRegistradoPendiente = new System.Windows.Forms.DataGridView();
            this.tabPage13 = new System.Windows.Forms.TabPage();
            this.dtgEstadoNuevo = new System.Windows.Forms.DataGridView();
            this.pagoRazon = new System.Windows.Forms.TextBox();
            this.label22 = new System.Windows.Forms.Label();
            this.pagoUUID = new System.Windows.Forms.TextBox();
            this.pagoProveedor = new System.Windows.Forms.TextBox();
            this.label20 = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.pagoFin = new System.Windows.Forms.DateTimePicker();
            this.pagoInicio = new System.Windows.Forms.DateTimePicker();
            this.label15 = new System.Windows.Forms.Label();
            this.pagoId = new System.Windows.Forms.TextBox();
            this.label16 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.button14 = new System.Windows.Forms.Button();
            this.button7 = new System.Windows.Forms.Button();
            this.button8 = new System.Windows.Forms.Button();
            this.informacionPago = new System.Windows.Forms.TextBox();
            this.pagoRfc = new System.Windows.Forms.TextBox();
            this.tabPage6 = new System.Windows.Forms.TabPage();
            this.ingresadoRazon = new System.Windows.Forms.TextBox();
            this.label23 = new System.Windows.Forms.Label();
            this.ingresadoOC = new System.Windows.Forms.TextBox();
            this.ingresadoCardCode = new System.Windows.Forms.TextBox();
            this.label24 = new System.Windows.Forms.Label();
            this.label25 = new System.Windows.Forms.Label();
            this.label26 = new System.Windows.Forms.Label();
            this.ingresadoFinal = new System.Windows.Forms.DateTimePicker();
            this.ingresadoInicio = new System.Windows.Forms.DateTimePicker();
            this.label27 = new System.Windows.Forms.Label();
            this.ingresadoId = new System.Windows.Forms.TextBox();
            this.label28 = new System.Windows.Forms.Label();
            this.label29 = new System.Windows.Forms.Label();
            this.button10 = new System.Windows.Forms.Button();
            this.button11 = new System.Windows.Forms.Button();
            this.dtgIngresado = new System.Windows.Forms.DataGridView();
            this.informacionIngresado = new System.Windows.Forms.TextBox();
            this.ingresadoRfc = new System.Windows.Forms.TextBox();
            this.tabPage7 = new System.Windows.Forms.TabPage();
            this.label34 = new System.Windows.Forms.Label();
            this.tipoCambioFin = new System.Windows.Forms.DateTimePicker();
            this.tipoCambioInicio = new System.Windows.Forms.DateTimePicker();
            this.label35 = new System.Windows.Forms.Label();
            this.button12 = new System.Windows.Forms.Button();
            this.button13 = new System.Windows.Forms.Button();
            this.dtgTC = new System.Windows.Forms.DataGridView();
            this.informacionTC = new System.Windows.Forms.TextBox();
            this.tabPage5 = new System.Windows.Forms.TabPage();
            this.logText = new System.Windows.Forms.TextBox();
            this.ejecutarAutomatico = new System.Windows.Forms.CheckBox();
            this.label7 = new System.Windows.Forms.Label();
            this.proxima = new System.Windows.Forms.TextBox();
            this.textoHora = new System.Windows.Forms.TextBox();
            this.notifyIcon = new System.Windows.Forms.NotifyIcon(this.components);
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.button9 = new System.Windows.Forms.Button();
            this.ejecutarAutomaticoIngreso = new System.Windows.Forms.CheckBox();
            this.proximaIngreso = new System.Windows.Forms.TextBox();
            this.label30 = new System.Windows.Forms.Label();
            this.tab.SuspendLayout();
            this.tabPage1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dtgProveedores)).BeginInit();
            this.tabPage2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dtgOC)).BeginInit();
            this.tabPage14.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dtgRecibo)).BeginInit();
            this.tabPage3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dtgCxP)).BeginInit();
            this.tabPage4.SuspendLayout();
            this.tabControl1.SuspendLayout();
            this.tabPage8.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dtgPago)).BeginInit();
            this.tabPage9.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dtgPagoResumen)).BeginInit();
            this.tabPage11.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dtgPagoPendiente)).BeginInit();
            this.tabPage10.SuspendLayout();
            this.tabPage12.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dtgRegistradoPendiente)).BeginInit();
            this.tabPage13.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dtgEstadoNuevo)).BeginInit();
            this.tabPage6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dtgIngresado)).BeginInit();
            this.tabPage7.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dtgTC)).BeginInit();
            this.tabPage5.SuspendLayout();
            this.SuspendLayout();
            // 
            // tab
            // 
            this.tab.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tab.Controls.Add(this.tabPage1);
            this.tab.Controls.Add(this.tabPage2);
            this.tab.Controls.Add(this.tabPage14);
            this.tab.Controls.Add(this.tabPage3);
            this.tab.Controls.Add(this.tabPage4);
            this.tab.Controls.Add(this.tabPage6);
            this.tab.Controls.Add(this.tabPage7);
            this.tab.Controls.Add(this.tabPage5);
            this.tab.Location = new System.Drawing.Point(12, 84);
            this.tab.Name = "tab";
            this.tab.SelectedIndex = 0;
            this.tab.Size = new System.Drawing.Size(1000, 391);
            this.tab.TabIndex = 4;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.filtroNombre);
            this.tabPage1.Controls.Add(this.label3);
            this.tabPage1.Controls.Add(this.filtroId);
            this.tabPage1.Controls.Add(this.label2);
            this.tabPage1.Controls.Add(this.label1);
            this.tabPage1.Controls.Add(this.button2);
            this.tabPage1.Controls.Add(this.button1);
            this.tabPage1.Controls.Add(this.dtgProveedores);
            this.tabPage1.Controls.Add(this.informacion);
            this.tabPage1.Controls.Add(this.filtroRFC);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(992, 365);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Proveedores";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // filtroNombre
            // 
            this.filtroNombre.Location = new System.Drawing.Point(269, 8);
            this.filtroNombre.Name = "filtroNombre";
            this.filtroNombre.Size = new System.Drawing.Size(191, 20);
            this.filtroNombre.TabIndex = 4;
            // 
            // label3
            // 
            this.label3.Location = new System.Drawing.Point(222, 11);
            this.label3.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(56, 15);
            this.label3.TabIndex = 3;
            this.label3.Text = "Nombre";
            // 
            // filtroId
            // 
            this.filtroId.Location = new System.Drawing.Point(115, 8);
            this.filtroId.Name = "filtroId";
            this.filtroId.Size = new System.Drawing.Size(102, 20);
            this.filtroId.TabIndex = 2;
            // 
            // label2
            // 
            this.label2.Location = new System.Drawing.Point(95, 11);
            this.label2.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(29, 15);
            this.label2.TabIndex = 1;
            this.label2.Text = "Id";
            // 
            // label1
            // 
            this.label1.Location = new System.Drawing.Point(465, 11);
            this.label1.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(29, 15);
            this.label1.TabIndex = 5;
            this.label1.Text = "RFC";
            // 
            // button2
            // 
            this.button2.BackColor = System.Drawing.Color.YellowGreen;
            this.button2.FlatAppearance.BorderSize = 0;
            this.button2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button2.ForeColor = System.Drawing.Color.White;
            this.button2.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.button2.Location = new System.Drawing.Point(9, 33);
            this.button2.Margin = new System.Windows.Forms.Padding(2);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(82, 24);
            this.button2.TabIndex = 7;
            this.button2.Text = "Sincronizar";
            this.button2.UseVisualStyleBackColor = false;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // button1
            // 
            this.button1.BackColor = System.Drawing.Color.Goldenrod;
            this.button1.FlatAppearance.BorderSize = 0;
            this.button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button1.ForeColor = System.Drawing.Color.White;
            this.button1.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.button1.Location = new System.Drawing.Point(9, 5);
            this.button1.Margin = new System.Windows.Forms.Padding(2);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(82, 24);
            this.button1.TabIndex = 0;
            this.button1.Text = "Buscar";
            this.button1.UseVisualStyleBackColor = false;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // dtgProveedores
            // 
            this.dtgProveedores.AllowUserToAddRows = false;
            this.dtgProveedores.AllowUserToDeleteRows = false;
            this.dtgProveedores.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dtgProveedores.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dtgProveedores.Location = new System.Drawing.Point(9, 62);
            this.dtgProveedores.Name = "dtgProveedores";
            this.dtgProveedores.ReadOnly = true;
            this.dtgProveedores.Size = new System.Drawing.Size(977, 297);
            this.dtgProveedores.TabIndex = 9;
            // 
            // informacion
            // 
            this.informacion.Location = new System.Drawing.Point(96, 34);
            this.informacion.Name = "informacion";
            this.informacion.ReadOnly = true;
            this.informacion.Size = new System.Drawing.Size(528, 20);
            this.informacion.TabIndex = 8;
            // 
            // filtroRFC
            // 
            this.filtroRFC.Location = new System.Drawing.Point(499, 8);
            this.filtroRFC.Name = "filtroRFC";
            this.filtroRFC.Size = new System.Drawing.Size(125, 20);
            this.filtroRFC.TabIndex = 6;
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.filtroRazon);
            this.tabPage2.Controls.Add(this.label4);
            this.tabPage2.Controls.Add(this.finOC);
            this.tabPage2.Controls.Add(this.inicioOC);
            this.tabPage2.Controls.Add(this.label8);
            this.tabPage2.Controls.Add(this.filtroOC);
            this.tabPage2.Controls.Add(this.label5);
            this.tabPage2.Controls.Add(this.label13);
            this.tabPage2.Controls.Add(this.label6);
            this.tabPage2.Controls.Add(this.button3);
            this.tabPage2.Controls.Add(this.button4);
            this.tabPage2.Controls.Add(this.dtgOC);
            this.tabPage2.Controls.Add(this.informacionOC);
            this.tabPage2.Controls.Add(this.filtroOCrfc);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(992, 365);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "OC";
            this.tabPage2.UseVisualStyleBackColor = true;
            this.tabPage2.Click += new System.EventHandler(this.tabPage2_Click);
            // 
            // filtroRazon
            // 
            this.filtroRazon.Location = new System.Drawing.Point(726, 9);
            this.filtroRazon.Name = "filtroRazon";
            this.filtroRazon.Size = new System.Drawing.Size(260, 20);
            this.filtroRazon.TabIndex = 10;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(387, 13);
            this.label4.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(32, 13);
            this.label4.TabIndex = 5;
            this.label4.Text = "Inicio";
            // 
            // finOC
            // 
            this.finOC.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.finOC.Location = new System.Drawing.Point(569, 9);
            this.finOC.Margin = new System.Windows.Forms.Padding(2);
            this.finOC.Name = "finOC";
            this.finOC.ShowCheckBox = true;
            this.finOC.Size = new System.Drawing.Size(113, 20);
            this.finOC.TabIndex = 8;
            // 
            // inicioOC
            // 
            this.inicioOC.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.inicioOC.Location = new System.Drawing.Point(422, 9);
            this.inicioOC.Margin = new System.Windows.Forms.Padding(2);
            this.inicioOC.Name = "inicioOC";
            this.inicioOC.ShowCheckBox = true;
            this.inicioOC.Size = new System.Drawing.Size(118, 20);
            this.inicioOC.TabIndex = 6;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(544, 13);
            this.label8.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(21, 13);
            this.label8.TabIndex = 7;
            this.label8.Text = "Fin";
            // 
            // filtroOC
            // 
            this.filtroOC.Location = new System.Drawing.Point(114, 9);
            this.filtroOC.Name = "filtroOC";
            this.filtroOC.Size = new System.Drawing.Size(102, 20);
            this.filtroOC.TabIndex = 2;
            // 
            // label5
            // 
            this.label5.Location = new System.Drawing.Point(94, 12);
            this.label5.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(29, 15);
            this.label5.TabIndex = 1;
            this.label5.Text = "Id";
            // 
            // label13
            // 
            this.label13.Location = new System.Drawing.Point(686, 12);
            this.label13.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(49, 15);
            this.label13.TabIndex = 9;
            this.label13.Text = "Raz�n";
            // 
            // label6
            // 
            this.label6.Location = new System.Drawing.Point(220, 12);
            this.label6.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(29, 15);
            this.label6.TabIndex = 3;
            this.label6.Text = "RFC";
            // 
            // button3
            // 
            this.button3.BackColor = System.Drawing.Color.YellowGreen;
            this.button3.FlatAppearance.BorderSize = 0;
            this.button3.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button3.ForeColor = System.Drawing.Color.White;
            this.button3.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.button3.Location = new System.Drawing.Point(8, 34);
            this.button3.Margin = new System.Windows.Forms.Padding(2);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(82, 24);
            this.button3.TabIndex = 11;
            this.button3.Text = "Sincronizar";
            this.button3.UseVisualStyleBackColor = false;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // button4
            // 
            this.button4.BackColor = System.Drawing.Color.Goldenrod;
            this.button4.FlatAppearance.BorderSize = 0;
            this.button4.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button4.ForeColor = System.Drawing.Color.White;
            this.button4.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.button4.Location = new System.Drawing.Point(8, 6);
            this.button4.Margin = new System.Windows.Forms.Padding(2);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(82, 24);
            this.button4.TabIndex = 0;
            this.button4.Text = "Buscar";
            this.button4.UseVisualStyleBackColor = false;
            this.button4.Click += new System.EventHandler(this.button4_Click);
            // 
            // dtgOC
            // 
            this.dtgOC.AllowUserToAddRows = false;
            this.dtgOC.AllowUserToDeleteRows = false;
            this.dtgOC.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dtgOC.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dtgOC.Location = new System.Drawing.Point(8, 63);
            this.dtgOC.Name = "dtgOC";
            this.dtgOC.ReadOnly = true;
            this.dtgOC.Size = new System.Drawing.Size(978, 296);
            this.dtgOC.TabIndex = 13;
            // 
            // informacionOC
            // 
            this.informacionOC.Location = new System.Drawing.Point(95, 35);
            this.informacionOC.Name = "informacionOC";
            this.informacionOC.ReadOnly = true;
            this.informacionOC.Size = new System.Drawing.Size(587, 20);
            this.informacionOC.TabIndex = 12;
            // 
            // filtroOCrfc
            // 
            this.filtroOCrfc.Location = new System.Drawing.Point(254, 9);
            this.filtroOCrfc.Name = "filtroOCrfc";
            this.filtroOCrfc.Size = new System.Drawing.Size(125, 20);
            this.filtroOCrfc.TabIndex = 4;
            // 
            // tabPage14
            // 
            this.tabPage14.Controls.Add(this.textBox1);
            this.tabPage14.Controls.Add(this.label31);
            this.tabPage14.Controls.Add(this.dateTimePicker1);
            this.tabPage14.Controls.Add(this.dateTimePicker2);
            this.tabPage14.Controls.Add(this.label32);
            this.tabPage14.Controls.Add(this.ReciboTxt);
            this.tabPage14.Controls.Add(this.label33);
            this.tabPage14.Controls.Add(this.label36);
            this.tabPage14.Controls.Add(this.label37);
            this.tabPage14.Controls.Add(this.button17);
            this.tabPage14.Controls.Add(this.button18);
            this.tabPage14.Controls.Add(this.dtgRecibo);
            this.tabPage14.Controls.Add(this.informacionRecibo);
            this.tabPage14.Controls.Add(this.textBox4);
            this.tabPage14.Location = new System.Drawing.Point(4, 22);
            this.tabPage14.Name = "tabPage14";
            this.tabPage14.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage14.Size = new System.Drawing.Size(992, 365);
            this.tabPage14.TabIndex = 7;
            this.tabPage14.Text = "Recibo";
            this.tabPage14.UseVisualStyleBackColor = true;
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(725, 9);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(260, 20);
            this.textBox1.TabIndex = 24;
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.Location = new System.Drawing.Point(386, 13);
            this.label31.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(32, 13);
            this.label31.TabIndex = 19;
            this.label31.Text = "Inicio";
            // 
            // dateTimePicker1
            // 
            this.dateTimePicker1.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dateTimePicker1.Location = new System.Drawing.Point(568, 9);
            this.dateTimePicker1.Margin = new System.Windows.Forms.Padding(2);
            this.dateTimePicker1.Name = "dateTimePicker1";
            this.dateTimePicker1.ShowCheckBox = true;
            this.dateTimePicker1.Size = new System.Drawing.Size(113, 20);
            this.dateTimePicker1.TabIndex = 22;
            // 
            // dateTimePicker2
            // 
            this.dateTimePicker2.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dateTimePicker2.Location = new System.Drawing.Point(421, 9);
            this.dateTimePicker2.Margin = new System.Windows.Forms.Padding(2);
            this.dateTimePicker2.Name = "dateTimePicker2";
            this.dateTimePicker2.ShowCheckBox = true;
            this.dateTimePicker2.Size = new System.Drawing.Size(118, 20);
            this.dateTimePicker2.TabIndex = 20;
            // 
            // label32
            // 
            this.label32.AutoSize = true;
            this.label32.Location = new System.Drawing.Point(543, 13);
            this.label32.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(21, 13);
            this.label32.TabIndex = 21;
            this.label32.Text = "Fin";
            // 
            // ReciboTxt
            // 
            this.ReciboTxt.Location = new System.Drawing.Point(113, 9);
            this.ReciboTxt.Name = "ReciboTxt";
            this.ReciboTxt.Size = new System.Drawing.Size(102, 20);
            this.ReciboTxt.TabIndex = 16;
            // 
            // label33
            // 
            this.label33.Location = new System.Drawing.Point(93, 12);
            this.label33.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(29, 15);
            this.label33.TabIndex = 15;
            this.label33.Text = "Id";
            // 
            // label36
            // 
            this.label36.Location = new System.Drawing.Point(685, 12);
            this.label36.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label36.Name = "label36";
            this.label36.Size = new System.Drawing.Size(49, 15);
            this.label36.TabIndex = 23;
            this.label36.Text = "Raz�n";
            // 
            // label37
            // 
            this.label37.Location = new System.Drawing.Point(219, 12);
            this.label37.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label37.Name = "label37";
            this.label37.Size = new System.Drawing.Size(29, 15);
            this.label37.TabIndex = 17;
            this.label37.Text = "RFC";
            // 
            // button17
            // 
            this.button17.BackColor = System.Drawing.Color.YellowGreen;
            this.button17.FlatAppearance.BorderSize = 0;
            this.button17.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button17.ForeColor = System.Drawing.Color.White;
            this.button17.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.button17.Location = new System.Drawing.Point(7, 34);
            this.button17.Margin = new System.Windows.Forms.Padding(2);
            this.button17.Name = "button17";
            this.button17.Size = new System.Drawing.Size(82, 24);
            this.button17.TabIndex = 25;
            this.button17.Text = "Sincronizar";
            this.button17.UseVisualStyleBackColor = false;
            this.button17.Click += new System.EventHandler(this.button17_Click);
            // 
            // button18
            // 
            this.button18.BackColor = System.Drawing.Color.Goldenrod;
            this.button18.FlatAppearance.BorderSize = 0;
            this.button18.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button18.ForeColor = System.Drawing.Color.White;
            this.button18.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.button18.Location = new System.Drawing.Point(7, 6);
            this.button18.Margin = new System.Windows.Forms.Padding(2);
            this.button18.Name = "button18";
            this.button18.Size = new System.Drawing.Size(82, 24);
            this.button18.TabIndex = 14;
            this.button18.Text = "Buscar";
            this.button18.UseVisualStyleBackColor = false;
            this.button18.Click += new System.EventHandler(this.button18_Click);
            // 
            // dtgRecibo
            // 
            this.dtgRecibo.AllowUserToAddRows = false;
            this.dtgRecibo.AllowUserToDeleteRows = false;
            this.dtgRecibo.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dtgRecibo.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dtgRecibo.Location = new System.Drawing.Point(7, 63);
            this.dtgRecibo.Name = "dtgRecibo";
            this.dtgRecibo.ReadOnly = true;
            this.dtgRecibo.Size = new System.Drawing.Size(978, 296);
            this.dtgRecibo.TabIndex = 27;
            // 
            // informacionRecibo
            // 
            this.informacionRecibo.Location = new System.Drawing.Point(94, 35);
            this.informacionRecibo.Name = "informacionRecibo";
            this.informacionRecibo.ReadOnly = true;
            this.informacionRecibo.Size = new System.Drawing.Size(587, 20);
            this.informacionRecibo.TabIndex = 26;
            // 
            // textBox4
            // 
            this.textBox4.Location = new System.Drawing.Point(253, 9);
            this.textBox4.Name = "textBox4";
            this.textBox4.Size = new System.Drawing.Size(125, 20);
            this.textBox4.TabIndex = 18;
            // 
            // tabPage3
            // 
            this.tabPage3.Controls.Add(this.cxpUUID);
            this.tabPage3.Controls.Add(this.label9);
            this.tabPage3.Controls.Add(this.cxpFin);
            this.tabPage3.Controls.Add(this.cxpInicio);
            this.tabPage3.Controls.Add(this.label10);
            this.tabPage3.Controls.Add(this.cxpCliente);
            this.tabPage3.Controls.Add(this.cxpId);
            this.tabPage3.Controls.Add(this.label18);
            this.tabPage3.Controls.Add(this.label11);
            this.tabPage3.Controls.Add(this.label19);
            this.tabPage3.Controls.Add(this.label12);
            this.tabPage3.Controls.Add(this.button5);
            this.tabPage3.Controls.Add(this.button6);
            this.tabPage3.Controls.Add(this.dtgCxP);
            this.tabPage3.Controls.Add(this.informacionCxP);
            this.tabPage3.Controls.Add(this.cxpRfc);
            this.tabPage3.Location = new System.Drawing.Point(4, 22);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage3.Size = new System.Drawing.Size(992, 365);
            this.tabPage3.TabIndex = 2;
            this.tabPage3.Text = "CxP";
            this.tabPage3.UseVisualStyleBackColor = true;
            // 
            // cxpUUID
            // 
            this.cxpUUID.Location = new System.Drawing.Point(579, 35);
            this.cxpUUID.Name = "cxpUUID";
            this.cxpUUID.Size = new System.Drawing.Size(260, 20);
            this.cxpUUID.TabIndex = 28;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(544, 12);
            this.label9.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(32, 13);
            this.label9.TabIndex = 32;
            this.label9.Text = "Inicio";
            // 
            // cxpFin
            // 
            this.cxpFin.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.cxpFin.Location = new System.Drawing.Point(726, 8);
            this.cxpFin.Margin = new System.Windows.Forms.Padding(2);
            this.cxpFin.Name = "cxpFin";
            this.cxpFin.ShowCheckBox = true;
            this.cxpFin.Size = new System.Drawing.Size(113, 20);
            this.cxpFin.TabIndex = 34;
            // 
            // cxpInicio
            // 
            this.cxpInicio.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.cxpInicio.Location = new System.Drawing.Point(579, 8);
            this.cxpInicio.Margin = new System.Windows.Forms.Padding(2);
            this.cxpInicio.Name = "cxpInicio";
            this.cxpInicio.ShowCheckBox = true;
            this.cxpInicio.Size = new System.Drawing.Size(118, 20);
            this.cxpInicio.TabIndex = 35;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(701, 12);
            this.label10.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(21, 13);
            this.label10.TabIndex = 33;
            this.label10.Text = "Fin";
            // 
            // cxpCliente
            // 
            this.cxpCliente.Location = new System.Drawing.Point(158, 9);
            this.cxpCliente.Name = "cxpCliente";
            this.cxpCliente.Size = new System.Drawing.Size(80, 20);
            this.cxpCliente.TabIndex = 26;
            // 
            // cxpId
            // 
            this.cxpId.Location = new System.Drawing.Point(269, 8);
            this.cxpId.Name = "cxpId";
            this.cxpId.Size = new System.Drawing.Size(102, 20);
            this.cxpId.TabIndex = 26;
            // 
            // label18
            // 
            this.label18.Location = new System.Drawing.Point(95, 11);
            this.label18.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(58, 15);
            this.label18.TabIndex = 25;
            this.label18.Text = "Proveedor";
            // 
            // label11
            // 
            this.label11.Location = new System.Drawing.Point(249, 11);
            this.label11.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(29, 15);
            this.label11.TabIndex = 25;
            this.label11.Text = "Id";
            // 
            // label19
            // 
            this.label19.Location = new System.Drawing.Point(544, 38);
            this.label19.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(39, 15);
            this.label19.TabIndex = 27;
            this.label19.Text = "UUID";
            // 
            // label12
            // 
            this.label12.Location = new System.Drawing.Point(375, 11);
            this.label12.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(29, 15);
            this.label12.TabIndex = 27;
            this.label12.Text = "RFC";
            // 
            // button5
            // 
            this.button5.BackColor = System.Drawing.Color.YellowGreen;
            this.button5.FlatAppearance.BorderSize = 0;
            this.button5.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button5.ForeColor = System.Drawing.Color.White;
            this.button5.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.button5.Location = new System.Drawing.Point(7, 34);
            this.button5.Margin = new System.Windows.Forms.Padding(2);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(82, 24);
            this.button5.TabIndex = 29;
            this.button5.Text = "Sincronizar";
            this.button5.UseVisualStyleBackColor = false;
            this.button5.Click += new System.EventHandler(this.button5_Click);
            // 
            // button6
            // 
            this.button6.BackColor = System.Drawing.Color.Goldenrod;
            this.button6.FlatAppearance.BorderSize = 0;
            this.button6.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button6.ForeColor = System.Drawing.Color.White;
            this.button6.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.button6.Location = new System.Drawing.Point(7, 6);
            this.button6.Margin = new System.Windows.Forms.Padding(2);
            this.button6.Name = "button6";
            this.button6.Size = new System.Drawing.Size(82, 24);
            this.button6.TabIndex = 24;
            this.button6.Text = "Buscar";
            this.button6.UseVisualStyleBackColor = false;
            this.button6.Click += new System.EventHandler(this.button6_Click);
            // 
            // dtgCxP
            // 
            this.dtgCxP.AllowUserToAddRows = false;
            this.dtgCxP.AllowUserToDeleteRows = false;
            this.dtgCxP.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dtgCxP.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dtgCxP.Location = new System.Drawing.Point(7, 63);
            this.dtgCxP.Name = "dtgCxP";
            this.dtgCxP.ReadOnly = true;
            this.dtgCxP.Size = new System.Drawing.Size(978, 296);
            this.dtgCxP.TabIndex = 31;
            // 
            // informacionCxP
            // 
            this.informacionCxP.Location = new System.Drawing.Point(94, 35);
            this.informacionCxP.Name = "informacionCxP";
            this.informacionCxP.ReadOnly = true;
            this.informacionCxP.Size = new System.Drawing.Size(440, 20);
            this.informacionCxP.TabIndex = 30;
            // 
            // cxpRfc
            // 
            this.cxpRfc.Location = new System.Drawing.Point(409, 8);
            this.cxpRfc.Name = "cxpRfc";
            this.cxpRfc.Size = new System.Drawing.Size(125, 20);
            this.cxpRfc.TabIndex = 28;
            // 
            // tabPage4
            // 
            this.tabPage4.Controls.Add(this.pagosTotalPendientes);
            this.tabPage4.Controls.Add(this.button16);
            this.tabPage4.Controls.Add(this.button15);
            this.tabPage4.Controls.Add(this.tabControl1);
            this.tabPage4.Controls.Add(this.pagoRazon);
            this.tabPage4.Controls.Add(this.label22);
            this.tabPage4.Controls.Add(this.pagoUUID);
            this.tabPage4.Controls.Add(this.pagoProveedor);
            this.tabPage4.Controls.Add(this.label20);
            this.tabPage4.Controls.Add(this.label21);
            this.tabPage4.Controls.Add(this.label14);
            this.tabPage4.Controls.Add(this.pagoFin);
            this.tabPage4.Controls.Add(this.pagoInicio);
            this.tabPage4.Controls.Add(this.label15);
            this.tabPage4.Controls.Add(this.pagoId);
            this.tabPage4.Controls.Add(this.label16);
            this.tabPage4.Controls.Add(this.label17);
            this.tabPage4.Controls.Add(this.button14);
            this.tabPage4.Controls.Add(this.button7);
            this.tabPage4.Controls.Add(this.button8);
            this.tabPage4.Controls.Add(this.informacionPago);
            this.tabPage4.Controls.Add(this.pagoRfc);
            this.tabPage4.Location = new System.Drawing.Point(4, 22);
            this.tabPage4.Name = "tabPage4";
            this.tabPage4.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage4.Size = new System.Drawing.Size(992, 365);
            this.tabPage4.TabIndex = 3;
            this.tabPage4.Text = "Pagos";
            this.tabPage4.UseVisualStyleBackColor = true;
            // 
            // pagosTotalPendientes
            // 
            this.pagosTotalPendientes.Location = new System.Drawing.Point(270, 63);
            this.pagosTotalPendientes.Name = "pagosTotalPendientes";
            this.pagosTotalPendientes.ReadOnly = true;
            this.pagosTotalPendientes.Size = new System.Drawing.Size(224, 20);
            this.pagosTotalPendientes.TabIndex = 57;
            // 
            // button16
            // 
            this.button16.BackColor = System.Drawing.Color.MediumBlue;
            this.button16.FlatAppearance.BorderSize = 0;
            this.button16.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button16.ForeColor = System.Drawing.Color.White;
            this.button16.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.button16.Location = new System.Drawing.Point(270, 34);
            this.button16.Margin = new System.Windows.Forms.Padding(2);
            this.button16.Name = "button16";
            this.button16.Size = new System.Drawing.Size(224, 24);
            this.button16.TabIndex = 56;
            this.button16.Text = "Sincronizar solo Registrados o Pendientes";
            this.button16.UseVisualStyleBackColor = false;
            this.button16.Click += new System.EventHandler(this.button16_Click);
            // 
            // button15
            // 
            this.button15.BackColor = System.Drawing.Color.DarkSeaGreen;
            this.button15.FlatAppearance.BorderSize = 0;
            this.button15.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button15.ForeColor = System.Drawing.Color.White;
            this.button15.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.button15.Location = new System.Drawing.Point(184, 34);
            this.button15.Margin = new System.Windows.Forms.Padding(2);
            this.button15.Name = "button15";
            this.button15.Size = new System.Drawing.Size(82, 24);
            this.button15.TabIndex = 55;
            this.button15.Text = "Pendientes";
            this.button15.UseVisualStyleBackColor = false;
            this.button15.Click += new System.EventHandler(this.button15_Click);
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage8);
            this.tabControl1.Controls.Add(this.tabPage9);
            this.tabControl1.Controls.Add(this.tabPage11);
            this.tabControl1.Controls.Add(this.tabPage10);
            this.tabControl1.Controls.Add(this.tabPage12);
            this.tabControl1.Controls.Add(this.tabPage13);
            this.tabControl1.Location = new System.Drawing.Point(7, 89);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(979, 270);
            this.tabControl1.TabIndex = 54;
            // 
            // tabPage8
            // 
            this.tabPage8.Controls.Add(this.dtgPago);
            this.tabPage8.Location = new System.Drawing.Point(4, 22);
            this.tabPage8.Name = "tabPage8";
            this.tabPage8.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage8.Size = new System.Drawing.Size(971, 244);
            this.tabPage8.TabIndex = 0;
            this.tabPage8.Text = "Detalle";
            this.tabPage8.UseVisualStyleBackColor = true;
            // 
            // dtgPago
            // 
            this.dtgPago.AllowUserToAddRows = false;
            this.dtgPago.AllowUserToDeleteRows = false;
            this.dtgPago.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dtgPago.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dtgPago.Location = new System.Drawing.Point(6, 6);
            this.dtgPago.Name = "dtgPago";
            this.dtgPago.ReadOnly = true;
            this.dtgPago.Size = new System.Drawing.Size(959, 232);
            this.dtgPago.TabIndex = 43;
            // 
            // tabPage9
            // 
            this.tabPage9.Controls.Add(this.dtgPagoResumen);
            this.tabPage9.Location = new System.Drawing.Point(4, 22);
            this.tabPage9.Name = "tabPage9";
            this.tabPage9.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage9.Size = new System.Drawing.Size(971, 244);
            this.tabPage9.TabIndex = 1;
            this.tabPage9.Text = "Resumen";
            this.tabPage9.UseVisualStyleBackColor = true;
            // 
            // dtgPagoResumen
            // 
            this.dtgPagoResumen.AllowUserToAddRows = false;
            this.dtgPagoResumen.AllowUserToDeleteRows = false;
            this.dtgPagoResumen.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dtgPagoResumen.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dtgPagoResumen.Location = new System.Drawing.Point(6, 6);
            this.dtgPagoResumen.Name = "dtgPagoResumen";
            this.dtgPagoResumen.ReadOnly = true;
            this.dtgPagoResumen.Size = new System.Drawing.Size(959, 232);
            this.dtgPagoResumen.TabIndex = 44;
            // 
            // tabPage11
            // 
            this.tabPage11.Controls.Add(this.dtgPagoPendiente);
            this.tabPage11.Location = new System.Drawing.Point(4, 22);
            this.tabPage11.Name = "tabPage11";
            this.tabPage11.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage11.Size = new System.Drawing.Size(971, 244);
            this.tabPage11.TabIndex = 3;
            this.tabPage11.Text = "Pendientes";
            this.tabPage11.UseVisualStyleBackColor = true;
            // 
            // dtgPagoPendiente
            // 
            this.dtgPagoPendiente.AllowUserToAddRows = false;
            this.dtgPagoPendiente.AllowUserToDeleteRows = false;
            this.dtgPagoPendiente.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dtgPagoPendiente.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dtgPagoPendiente.Location = new System.Drawing.Point(6, 6);
            this.dtgPagoPendiente.Name = "dtgPagoPendiente";
            this.dtgPagoPendiente.ReadOnly = true;
            this.dtgPagoPendiente.Size = new System.Drawing.Size(959, 232);
            this.dtgPagoPendiente.TabIndex = 45;
            // 
            // tabPage10
            // 
            this.tabPage10.Controls.Add(this.pagoLog);
            this.tabPage10.Location = new System.Drawing.Point(4, 22);
            this.tabPage10.Name = "tabPage10";
            this.tabPage10.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage10.Size = new System.Drawing.Size(971, 244);
            this.tabPage10.TabIndex = 2;
            this.tabPage10.Text = "Log de Transferencias";
            this.tabPage10.UseVisualStyleBackColor = true;
            // 
            // pagoLog
            // 
            this.pagoLog.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pagoLog.Location = new System.Drawing.Point(3, 3);
            this.pagoLog.Multiline = true;
            this.pagoLog.Name = "pagoLog";
            this.pagoLog.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.pagoLog.Size = new System.Drawing.Size(965, 238);
            this.pagoLog.TabIndex = 0;
            // 
            // tabPage12
            // 
            this.tabPage12.Controls.Add(this.dtgRegistradoPendiente);
            this.tabPage12.Location = new System.Drawing.Point(4, 22);
            this.tabPage12.Name = "tabPage12";
            this.tabPage12.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage12.Size = new System.Drawing.Size(971, 244);
            this.tabPage12.TabIndex = 4;
            this.tabPage12.Text = "Registrados y Pendientes";
            this.tabPage12.UseVisualStyleBackColor = true;
            // 
            // dtgRegistradoPendiente
            // 
            this.dtgRegistradoPendiente.AllowUserToAddRows = false;
            this.dtgRegistradoPendiente.AllowUserToDeleteRows = false;
            this.dtgRegistradoPendiente.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dtgRegistradoPendiente.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dtgRegistradoPendiente.Location = new System.Drawing.Point(6, 6);
            this.dtgRegistradoPendiente.Name = "dtgRegistradoPendiente";
            this.dtgRegistradoPendiente.ReadOnly = true;
            this.dtgRegistradoPendiente.Size = new System.Drawing.Size(959, 232);
            this.dtgRegistradoPendiente.TabIndex = 46;
            // 
            // tabPage13
            // 
            this.tabPage13.Controls.Add(this.dtgEstadoNuevo);
            this.tabPage13.Location = new System.Drawing.Point(4, 22);
            this.tabPage13.Name = "tabPage13";
            this.tabPage13.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage13.Size = new System.Drawing.Size(971, 244);
            this.tabPage13.TabIndex = 5;
            this.tabPage13.Text = "Nuevo Estado";
            this.tabPage13.UseVisualStyleBackColor = true;
            // 
            // dtgEstadoNuevo
            // 
            this.dtgEstadoNuevo.AllowUserToAddRows = false;
            this.dtgEstadoNuevo.AllowUserToDeleteRows = false;
            this.dtgEstadoNuevo.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dtgEstadoNuevo.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dtgEstadoNuevo.Location = new System.Drawing.Point(6, 6);
            this.dtgEstadoNuevo.Name = "dtgEstadoNuevo";
            this.dtgEstadoNuevo.ReadOnly = true;
            this.dtgEstadoNuevo.Size = new System.Drawing.Size(959, 232);
            this.dtgEstadoNuevo.TabIndex = 44;
            // 
            // pagoRazon
            // 
            this.pagoRazon.Location = new System.Drawing.Point(725, 10);
            this.pagoRazon.Name = "pagoRazon";
            this.pagoRazon.Size = new System.Drawing.Size(261, 20);
            this.pagoRazon.TabIndex = 53;
            // 
            // label22
            // 
            this.label22.Location = new System.Drawing.Point(685, 13);
            this.label22.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(49, 15);
            this.label22.TabIndex = 52;
            this.label22.Text = "Raz�n";
            // 
            // pagoUUID
            // 
            this.pagoUUID.Location = new System.Drawing.Point(725, 37);
            this.pagoUUID.Name = "pagoUUID";
            this.pagoUUID.Size = new System.Drawing.Size(261, 20);
            this.pagoUUID.TabIndex = 51;
            // 
            // pagoProveedor
            // 
            this.pagoProveedor.Location = new System.Drawing.Point(568, 37);
            this.pagoProveedor.Name = "pagoProveedor";
            this.pagoProveedor.Size = new System.Drawing.Size(113, 20);
            this.pagoProveedor.TabIndex = 49;
            // 
            // label20
            // 
            this.label20.Location = new System.Drawing.Point(506, 40);
            this.label20.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(58, 15);
            this.label20.TabIndex = 48;
            this.label20.Text = "Proveedor";
            // 
            // label21
            // 
            this.label21.Location = new System.Drawing.Point(681, 40);
            this.label21.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(39, 15);
            this.label21.TabIndex = 50;
            this.label21.Text = "UUID";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(386, 13);
            this.label14.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(32, 13);
            this.label14.TabIndex = 44;
            this.label14.Text = "Inicio";
            // 
            // pagoFin
            // 
            this.pagoFin.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.pagoFin.Location = new System.Drawing.Point(568, 9);
            this.pagoFin.Margin = new System.Windows.Forms.Padding(2);
            this.pagoFin.Name = "pagoFin";
            this.pagoFin.ShowCheckBox = true;
            this.pagoFin.Size = new System.Drawing.Size(113, 20);
            this.pagoFin.TabIndex = 46;
            // 
            // pagoInicio
            // 
            this.pagoInicio.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.pagoInicio.Location = new System.Drawing.Point(421, 9);
            this.pagoInicio.Margin = new System.Windows.Forms.Padding(2);
            this.pagoInicio.Name = "pagoInicio";
            this.pagoInicio.ShowCheckBox = true;
            this.pagoInicio.Size = new System.Drawing.Size(118, 20);
            this.pagoInicio.TabIndex = 47;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(543, 13);
            this.label15.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(21, 13);
            this.label15.TabIndex = 45;
            this.label15.Text = "Fin";
            // 
            // pagoId
            // 
            this.pagoId.Location = new System.Drawing.Point(113, 9);
            this.pagoId.Name = "pagoId";
            this.pagoId.Size = new System.Drawing.Size(102, 20);
            this.pagoId.TabIndex = 38;
            // 
            // label16
            // 
            this.label16.Location = new System.Drawing.Point(93, 12);
            this.label16.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(29, 15);
            this.label16.TabIndex = 37;
            this.label16.Text = "Id";
            // 
            // label17
            // 
            this.label17.Location = new System.Drawing.Point(219, 12);
            this.label17.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(29, 15);
            this.label17.TabIndex = 39;
            this.label17.Text = "RFC";
            // 
            // button14
            // 
            this.button14.BackColor = System.Drawing.Color.DarkSeaGreen;
            this.button14.FlatAppearance.BorderSize = 0;
            this.button14.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button14.ForeColor = System.Drawing.Color.White;
            this.button14.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.button14.Location = new System.Drawing.Point(96, 34);
            this.button14.Margin = new System.Windows.Forms.Padding(2);
            this.button14.Name = "button14";
            this.button14.Size = new System.Drawing.Size(82, 24);
            this.button14.TabIndex = 41;
            this.button14.Text = "Resumen";
            this.button14.UseVisualStyleBackColor = false;
            this.button14.Click += new System.EventHandler(this.button14_Click);
            // 
            // button7
            // 
            this.button7.BackColor = System.Drawing.Color.YellowGreen;
            this.button7.FlatAppearance.BorderSize = 0;
            this.button7.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button7.ForeColor = System.Drawing.Color.White;
            this.button7.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.button7.Location = new System.Drawing.Point(7, 34);
            this.button7.Margin = new System.Windows.Forms.Padding(2);
            this.button7.Name = "button7";
            this.button7.Size = new System.Drawing.Size(82, 24);
            this.button7.TabIndex = 41;
            this.button7.Text = "Detalle";
            this.button7.UseVisualStyleBackColor = false;
            this.button7.Click += new System.EventHandler(this.button7_Click);
            // 
            // button8
            // 
            this.button8.BackColor = System.Drawing.Color.Goldenrod;
            this.button8.FlatAppearance.BorderSize = 0;
            this.button8.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button8.ForeColor = System.Drawing.Color.White;
            this.button8.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.button8.Location = new System.Drawing.Point(7, 6);
            this.button8.Margin = new System.Windows.Forms.Padding(2);
            this.button8.Name = "button8";
            this.button8.Size = new System.Drawing.Size(82, 24);
            this.button8.TabIndex = 36;
            this.button8.Text = "Buscar";
            this.button8.UseVisualStyleBackColor = false;
            this.button8.Click += new System.EventHandler(this.button8_Click);
            // 
            // informacionPago
            // 
            this.informacionPago.Location = new System.Drawing.Point(7, 63);
            this.informacionPago.Name = "informacionPago";
            this.informacionPago.ReadOnly = true;
            this.informacionPago.Size = new System.Drawing.Size(259, 20);
            this.informacionPago.TabIndex = 42;
            // 
            // pagoRfc
            // 
            this.pagoRfc.Location = new System.Drawing.Point(253, 9);
            this.pagoRfc.Name = "pagoRfc";
            this.pagoRfc.Size = new System.Drawing.Size(125, 20);
            this.pagoRfc.TabIndex = 40;
            // 
            // tabPage6
            // 
            this.tabPage6.Controls.Add(this.ingresadoRazon);
            this.tabPage6.Controls.Add(this.label23);
            this.tabPage6.Controls.Add(this.ingresadoOC);
            this.tabPage6.Controls.Add(this.ingresadoCardCode);
            this.tabPage6.Controls.Add(this.label24);
            this.tabPage6.Controls.Add(this.label25);
            this.tabPage6.Controls.Add(this.label26);
            this.tabPage6.Controls.Add(this.ingresadoFinal);
            this.tabPage6.Controls.Add(this.ingresadoInicio);
            this.tabPage6.Controls.Add(this.label27);
            this.tabPage6.Controls.Add(this.ingresadoId);
            this.tabPage6.Controls.Add(this.label28);
            this.tabPage6.Controls.Add(this.label29);
            this.tabPage6.Controls.Add(this.button10);
            this.tabPage6.Controls.Add(this.button11);
            this.tabPage6.Controls.Add(this.dtgIngresado);
            this.tabPage6.Controls.Add(this.informacionIngresado);
            this.tabPage6.Controls.Add(this.ingresadoRfc);
            this.tabPage6.Location = new System.Drawing.Point(4, 22);
            this.tabPage6.Name = "tabPage6";
            this.tabPage6.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage6.Size = new System.Drawing.Size(992, 365);
            this.tabPage6.TabIndex = 5;
            this.tabPage6.Text = "Ingresados";
            this.tabPage6.UseVisualStyleBackColor = true;
            // 
            // ingresadoRazon
            // 
            this.ingresadoRazon.Location = new System.Drawing.Point(725, 10);
            this.ingresadoRazon.Name = "ingresadoRazon";
            this.ingresadoRazon.Size = new System.Drawing.Size(152, 20);
            this.ingresadoRazon.TabIndex = 71;
            // 
            // label23
            // 
            this.label23.Location = new System.Drawing.Point(685, 13);
            this.label23.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(49, 15);
            this.label23.TabIndex = 70;
            this.label23.Text = "Raz�n";
            // 
            // ingresadoOC
            // 
            this.ingresadoOC.Location = new System.Drawing.Point(567, 34);
            this.ingresadoOC.Name = "ingresadoOC";
            this.ingresadoOC.Size = new System.Drawing.Size(310, 20);
            this.ingresadoOC.TabIndex = 69;
            // 
            // ingresadoCardCode
            // 
            this.ingresadoCardCode.Location = new System.Drawing.Point(442, 34);
            this.ingresadoCardCode.Name = "ingresadoCardCode";
            this.ingresadoCardCode.Size = new System.Drawing.Size(80, 20);
            this.ingresadoCardCode.TabIndex = 67;
            // 
            // label24
            // 
            this.label24.Location = new System.Drawing.Point(381, 37);
            this.label24.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(58, 15);
            this.label24.TabIndex = 66;
            this.label24.Text = "Proveedor";
            // 
            // label25
            // 
            this.label25.Location = new System.Drawing.Point(525, 37);
            this.label25.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(39, 15);
            this.label25.TabIndex = 68;
            this.label25.Text = "OC";
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Location = new System.Drawing.Point(386, 13);
            this.label26.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(32, 13);
            this.label26.TabIndex = 62;
            this.label26.Text = "Inicio";
            // 
            // ingresadoFinal
            // 
            this.ingresadoFinal.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.ingresadoFinal.Location = new System.Drawing.Point(568, 9);
            this.ingresadoFinal.Margin = new System.Windows.Forms.Padding(2);
            this.ingresadoFinal.Name = "ingresadoFinal";
            this.ingresadoFinal.ShowCheckBox = true;
            this.ingresadoFinal.Size = new System.Drawing.Size(113, 20);
            this.ingresadoFinal.TabIndex = 64;
            // 
            // ingresadoInicio
            // 
            this.ingresadoInicio.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.ingresadoInicio.Location = new System.Drawing.Point(421, 9);
            this.ingresadoInicio.Margin = new System.Windows.Forms.Padding(2);
            this.ingresadoInicio.Name = "ingresadoInicio";
            this.ingresadoInicio.ShowCheckBox = true;
            this.ingresadoInicio.Size = new System.Drawing.Size(118, 20);
            this.ingresadoInicio.TabIndex = 65;
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Location = new System.Drawing.Point(543, 13);
            this.label27.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(21, 13);
            this.label27.TabIndex = 63;
            this.label27.Text = "Fin";
            // 
            // ingresadoId
            // 
            this.ingresadoId.Location = new System.Drawing.Point(113, 9);
            this.ingresadoId.Name = "ingresadoId";
            this.ingresadoId.Size = new System.Drawing.Size(102, 20);
            this.ingresadoId.TabIndex = 56;
            // 
            // label28
            // 
            this.label28.Location = new System.Drawing.Point(93, 12);
            this.label28.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(29, 15);
            this.label28.TabIndex = 55;
            this.label28.Text = "Id";
            // 
            // label29
            // 
            this.label29.Location = new System.Drawing.Point(219, 12);
            this.label29.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(29, 15);
            this.label29.TabIndex = 57;
            this.label29.Text = "RFC";
            // 
            // button10
            // 
            this.button10.BackColor = System.Drawing.Color.YellowGreen;
            this.button10.FlatAppearance.BorderSize = 0;
            this.button10.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button10.ForeColor = System.Drawing.Color.White;
            this.button10.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.button10.Location = new System.Drawing.Point(7, 34);
            this.button10.Margin = new System.Windows.Forms.Padding(2);
            this.button10.Name = "button10";
            this.button10.Size = new System.Drawing.Size(82, 24);
            this.button10.TabIndex = 59;
            this.button10.Text = "Sincronizar";
            this.button10.UseVisualStyleBackColor = false;
            this.button10.Click += new System.EventHandler(this.button10_Click);
            // 
            // button11
            // 
            this.button11.BackColor = System.Drawing.Color.Goldenrod;
            this.button11.FlatAppearance.BorderSize = 0;
            this.button11.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button11.ForeColor = System.Drawing.Color.White;
            this.button11.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.button11.Location = new System.Drawing.Point(7, 6);
            this.button11.Margin = new System.Windows.Forms.Padding(2);
            this.button11.Name = "button11";
            this.button11.Size = new System.Drawing.Size(82, 24);
            this.button11.TabIndex = 54;
            this.button11.Text = "Buscar";
            this.button11.UseVisualStyleBackColor = false;
            this.button11.Click += new System.EventHandler(this.button11_Click);
            // 
            // dtgIngresado
            // 
            this.dtgIngresado.AllowUserToAddRows = false;
            this.dtgIngresado.AllowUserToDeleteRows = false;
            this.dtgIngresado.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dtgIngresado.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dtgIngresado.Location = new System.Drawing.Point(7, 63);
            this.dtgIngresado.Name = "dtgIngresado";
            this.dtgIngresado.ReadOnly = true;
            this.dtgIngresado.Size = new System.Drawing.Size(978, 296);
            this.dtgIngresado.TabIndex = 61;
            // 
            // informacionIngresado
            // 
            this.informacionIngresado.Location = new System.Drawing.Point(94, 34);
            this.informacionIngresado.Name = "informacionIngresado";
            this.informacionIngresado.ReadOnly = true;
            this.informacionIngresado.Size = new System.Drawing.Size(284, 20);
            this.informacionIngresado.TabIndex = 60;
            // 
            // ingresadoRfc
            // 
            this.ingresadoRfc.Location = new System.Drawing.Point(253, 9);
            this.ingresadoRfc.Name = "ingresadoRfc";
            this.ingresadoRfc.Size = new System.Drawing.Size(125, 20);
            this.ingresadoRfc.TabIndex = 58;
            // 
            // tabPage7
            // 
            this.tabPage7.Controls.Add(this.label34);
            this.tabPage7.Controls.Add(this.tipoCambioFin);
            this.tabPage7.Controls.Add(this.tipoCambioInicio);
            this.tabPage7.Controls.Add(this.label35);
            this.tabPage7.Controls.Add(this.button12);
            this.tabPage7.Controls.Add(this.button13);
            this.tabPage7.Controls.Add(this.dtgTC);
            this.tabPage7.Controls.Add(this.informacionTC);
            this.tabPage7.Location = new System.Drawing.Point(4, 22);
            this.tabPage7.Name = "tabPage7";
            this.tabPage7.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage7.Size = new System.Drawing.Size(992, 365);
            this.tabPage7.TabIndex = 6;
            this.tabPage7.Text = "Tipo de cambio";
            this.tabPage7.UseVisualStyleBackColor = true;
            // 
            // label34
            // 
            this.label34.AutoSize = true;
            this.label34.Location = new System.Drawing.Point(93, 14);
            this.label34.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(32, 13);
            this.label34.TabIndex = 80;
            this.label34.Text = "Inicio";
            // 
            // tipoCambioFin
            // 
            this.tipoCambioFin.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.tipoCambioFin.Location = new System.Drawing.Point(277, 10);
            this.tipoCambioFin.Margin = new System.Windows.Forms.Padding(2);
            this.tipoCambioFin.Name = "tipoCambioFin";
            this.tipoCambioFin.ShowCheckBox = true;
            this.tipoCambioFin.Size = new System.Drawing.Size(113, 20);
            this.tipoCambioFin.TabIndex = 82;
            // 
            // tipoCambioInicio
            // 
            this.tipoCambioInicio.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.tipoCambioInicio.Location = new System.Drawing.Point(129, 10);
            this.tipoCambioInicio.Margin = new System.Windows.Forms.Padding(2);
            this.tipoCambioInicio.Name = "tipoCambioInicio";
            this.tipoCambioInicio.ShowCheckBox = true;
            this.tipoCambioInicio.Size = new System.Drawing.Size(118, 20);
            this.tipoCambioInicio.TabIndex = 83;
            // 
            // label35
            // 
            this.label35.AutoSize = true;
            this.label35.Location = new System.Drawing.Point(252, 14);
            this.label35.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label35.Name = "label35";
            this.label35.Size = new System.Drawing.Size(21, 13);
            this.label35.TabIndex = 81;
            this.label35.Text = "Fin";
            // 
            // button12
            // 
            this.button12.BackColor = System.Drawing.Color.YellowGreen;
            this.button12.FlatAppearance.BorderSize = 0;
            this.button12.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button12.ForeColor = System.Drawing.Color.White;
            this.button12.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.button12.Location = new System.Drawing.Point(7, 34);
            this.button12.Margin = new System.Windows.Forms.Padding(2);
            this.button12.Name = "button12";
            this.button12.Size = new System.Drawing.Size(82, 24);
            this.button12.TabIndex = 77;
            this.button12.Text = "Sincronizar";
            this.button12.UseVisualStyleBackColor = false;
            this.button12.Click += new System.EventHandler(this.button12_Click);
            // 
            // button13
            // 
            this.button13.BackColor = System.Drawing.Color.Goldenrod;
            this.button13.FlatAppearance.BorderSize = 0;
            this.button13.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button13.ForeColor = System.Drawing.Color.White;
            this.button13.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.button13.Location = new System.Drawing.Point(7, 6);
            this.button13.Margin = new System.Windows.Forms.Padding(2);
            this.button13.Name = "button13";
            this.button13.Size = new System.Drawing.Size(82, 24);
            this.button13.TabIndex = 72;
            this.button13.Text = "Buscar";
            this.button13.UseVisualStyleBackColor = false;
            this.button13.Click += new System.EventHandler(this.button13_Click);
            // 
            // dtgTC
            // 
            this.dtgTC.AllowUserToAddRows = false;
            this.dtgTC.AllowUserToDeleteRows = false;
            this.dtgTC.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dtgTC.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dtgTC.Location = new System.Drawing.Point(7, 63);
            this.dtgTC.Name = "dtgTC";
            this.dtgTC.ReadOnly = true;
            this.dtgTC.Size = new System.Drawing.Size(978, 296);
            this.dtgTC.TabIndex = 79;
            // 
            // informacionTC
            // 
            this.informacionTC.Location = new System.Drawing.Point(94, 37);
            this.informacionTC.Name = "informacionTC";
            this.informacionTC.ReadOnly = true;
            this.informacionTC.Size = new System.Drawing.Size(587, 20);
            this.informacionTC.TabIndex = 78;
            // 
            // tabPage5
            // 
            this.tabPage5.Controls.Add(this.logText);
            this.tabPage5.Location = new System.Drawing.Point(4, 22);
            this.tabPage5.Name = "tabPage5";
            this.tabPage5.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage5.Size = new System.Drawing.Size(992, 365);
            this.tabPage5.TabIndex = 4;
            this.tabPage5.Text = "Log Eventos";
            this.tabPage5.UseVisualStyleBackColor = true;
            // 
            // logText
            // 
            this.logText.Dock = System.Windows.Forms.DockStyle.Fill;
            this.logText.Location = new System.Drawing.Point(3, 3);
            this.logText.Multiline = true;
            this.logText.Name = "logText";
            this.logText.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.logText.Size = new System.Drawing.Size(986, 359);
            this.logText.TabIndex = 0;
            // 
            // ejecutarAutomatico
            // 
            this.ejecutarAutomatico.AutoSize = true;
            this.ejecutarAutomatico.Location = new System.Drawing.Point(12, 12);
            this.ejecutarAutomatico.Name = "ejecutarAutomatico";
            this.ejecutarAutomatico.Size = new System.Drawing.Size(128, 17);
            this.ejecutarAutomatico.TabIndex = 0;
            this.ejecutarAutomatico.Text = "Ejecuci�n automatica";
            this.ejecutarAutomatico.UseVisualStyleBackColor = true;
            this.ejecutarAutomatico.CheckedChanged += new System.EventHandler(this.ejecutarAutomatico_CheckedChanged);
            // 
            // label7
            // 
            this.label7.Location = new System.Drawing.Point(149, 12);
            this.label7.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(93, 15);
            this.label7.TabIndex = 1;
            this.label7.Text = "Proxima ejecuci�n";
            // 
            // proxima
            // 
            this.proxima.Location = new System.Drawing.Point(246, 9);
            this.proxima.Margin = new System.Windows.Forms.Padding(2);
            this.proxima.Name = "proxima";
            this.proxima.ReadOnly = true;
            this.proxima.Size = new System.Drawing.Size(62, 20);
            this.proxima.TabIndex = 2;
            // 
            // textoHora
            // 
            this.textoHora.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textoHora.Location = new System.Drawing.Point(12, 34);
            this.textoHora.Name = "textoHora";
            this.textoHora.ReadOnly = true;
            this.textoHora.Size = new System.Drawing.Size(1000, 20);
            this.textoHora.TabIndex = 3;
            // 
            // notifyIcon
            // 
            this.notifyIcon.Icon = ((System.Drawing.Icon)(resources.GetObject("notifyIcon.Icon")));
            this.notifyIcon.Text = "notifyIcon1";
            this.notifyIcon.Visible = true;
            this.notifyIcon.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.notifyIcon_MouseDoubleClick);
            // 
            // timer1
            // 
            this.timer1.Interval = 1000;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // button9
            // 
            this.button9.BackColor = System.Drawing.Color.Goldenrod;
            this.button9.FlatAppearance.BorderSize = 0;
            this.button9.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button9.ForeColor = System.Drawing.Color.White;
            this.button9.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.button9.Location = new System.Drawing.Point(312, 7);
            this.button9.Margin = new System.Windows.Forms.Padding(2);
            this.button9.Name = "button9";
            this.button9.Size = new System.Drawing.Size(141, 24);
            this.button9.TabIndex = 5;
            this.button9.Text = "Sincronizar todos";
            this.button9.UseVisualStyleBackColor = false;
            this.button9.Click += new System.EventHandler(this.button9_Click);
            // 
            // ejecutarAutomaticoIngreso
            // 
            this.ejecutarAutomaticoIngreso.AutoSize = true;
            this.ejecutarAutomaticoIngreso.Location = new System.Drawing.Point(12, 60);
            this.ejecutarAutomaticoIngreso.Name = "ejecutarAutomaticoIngreso";
            this.ejecutarAutomaticoIngreso.Size = new System.Drawing.Size(256, 17);
            this.ejecutarAutomaticoIngreso.TabIndex = 0;
            this.ejecutarAutomaticoIngreso.Text = "Ejecuci�n automatica sincronizaci�n de Ingresos";
            this.ejecutarAutomaticoIngreso.UseVisualStyleBackColor = true;
            this.ejecutarAutomaticoIngreso.CheckedChanged += new System.EventHandler(this.ejecutarAutomatico_CheckedChanged);
            // 
            // proximaIngreso
            // 
            this.proximaIngreso.Location = new System.Drawing.Point(369, 59);
            this.proximaIngreso.Margin = new System.Windows.Forms.Padding(2);
            this.proximaIngreso.Name = "proximaIngreso";
            this.proximaIngreso.ReadOnly = true;
            this.proximaIngreso.Size = new System.Drawing.Size(84, 20);
            this.proximaIngreso.TabIndex = 2;
            // 
            // label30
            // 
            this.label30.Location = new System.Drawing.Point(272, 62);
            this.label30.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(93, 15);
            this.label30.TabIndex = 1;
            this.label30.Text = "Proxima ejecuci�n";
            // 
            // frmPrincipal
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1024, 487);
            this.Controls.Add(this.button9);
            this.Controls.Add(this.label30);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.proximaIngreso);
            this.Controls.Add(this.proxima);
            this.Controls.Add(this.ejecutarAutomaticoIngreso);
            this.Controls.Add(this.ejecutarAutomatico);
            this.Controls.Add(this.textoHora);
            this.Controls.Add(this.tab);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MetroColor = System.Drawing.Color.Transparent;
            this.Name = "frmPrincipal";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Conector FX";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.frmPrincipal_FormClosed);
            this.Resize += new System.EventHandler(this.frmPrincipal_Resize);
            this.tab.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dtgProveedores)).EndInit();
            this.tabPage2.ResumeLayout(false);
            this.tabPage2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dtgOC)).EndInit();
            this.tabPage14.ResumeLayout(false);
            this.tabPage14.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dtgRecibo)).EndInit();
            this.tabPage3.ResumeLayout(false);
            this.tabPage3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dtgCxP)).EndInit();
            this.tabPage4.ResumeLayout(false);
            this.tabPage4.PerformLayout();
            this.tabControl1.ResumeLayout(false);
            this.tabPage8.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dtgPago)).EndInit();
            this.tabPage9.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dtgPagoResumen)).EndInit();
            this.tabPage11.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dtgPagoPendiente)).EndInit();
            this.tabPage10.ResumeLayout(false);
            this.tabPage10.PerformLayout();
            this.tabPage12.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dtgRegistradoPendiente)).EndInit();
            this.tabPage13.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dtgEstadoNuevo)).EndInit();
            this.tabPage6.ResumeLayout(false);
            this.tabPage6.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dtgIngresado)).EndInit();
            this.tabPage7.ResumeLayout(false);
            this.tabPage7.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dtgTC)).EndInit();
            this.tabPage5.ResumeLayout(false);
            this.tabPage5.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TabControl tab;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.DataGridView dtgProveedores;
        private System.Windows.Forms.TextBox filtroRFC;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.CheckBox ejecutarAutomatico;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox proxima;
        private System.Windows.Forms.TextBox informacion;
        private System.Windows.Forms.TextBox textoHora;
        private System.Windows.Forms.NotifyIcon notifyIcon;
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.TextBox filtroNombre;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox filtroId;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.DateTimePicker finOC;
        private System.Windows.Forms.DateTimePicker inicioOC;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox filtroOC;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.DataGridView dtgOC;
        private System.Windows.Forms.TextBox informacionOC;
        private System.Windows.Forms.TextBox filtroOCrfc;
        private System.Windows.Forms.TabPage tabPage3;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.DateTimePicker cxpFin;
        private System.Windows.Forms.DateTimePicker cxpInicio;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox cxpId;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Button button5;
        private System.Windows.Forms.Button button6;
        private System.Windows.Forms.DataGridView dtgCxP;
        private System.Windows.Forms.TextBox informacionCxP;
        private System.Windows.Forms.TextBox cxpRfc;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.TextBox filtroRazon;
        private System.Windows.Forms.TabPage tabPage4;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.DateTimePicker pagoFin;
        private System.Windows.Forms.DateTimePicker pagoInicio;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.TextBox pagoId;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Button button7;
        private System.Windows.Forms.Button button8;
        private System.Windows.Forms.DataGridView dtgPago;
        private System.Windows.Forms.TextBox informacionPago;
        private System.Windows.Forms.TextBox pagoRfc;
        private System.Windows.Forms.TextBox cxpCliente;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.TextBox cxpUUID;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.TextBox pagoUUID;
        private System.Windows.Forms.TextBox pagoProveedor;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.TextBox pagoRazon;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Button button9;
        private System.Windows.Forms.TabPage tabPage5;
        private System.Windows.Forms.TextBox logText;
        private System.Windows.Forms.TabPage tabPage6;
        private System.Windows.Forms.TextBox ingresadoRazon;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.TextBox ingresadoOC;
        private System.Windows.Forms.TextBox ingresadoCardCode;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.DateTimePicker ingresadoFinal;
        private System.Windows.Forms.DateTimePicker ingresadoInicio;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.TextBox ingresadoId;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.Button button10;
        private System.Windows.Forms.Button button11;
        private System.Windows.Forms.DataGridView dtgIngresado;
        private System.Windows.Forms.TextBox informacionIngresado;
        private System.Windows.Forms.TextBox ingresadoRfc;
        private System.Windows.Forms.CheckBox ejecutarAutomaticoIngreso;
        private System.Windows.Forms.TextBox proximaIngreso;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.TabPage tabPage7;
        private System.Windows.Forms.Label label34;
        private System.Windows.Forms.DateTimePicker tipoCambioFin;
        private System.Windows.Forms.DateTimePicker tipoCambioInicio;
        private System.Windows.Forms.Label label35;
        private System.Windows.Forms.Button button12;
        private System.Windows.Forms.Button button13;
        private System.Windows.Forms.DataGridView dtgTC;
        private System.Windows.Forms.TextBox informacionTC;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage8;
        private System.Windows.Forms.TabPage tabPage9;
        private System.Windows.Forms.DataGridView dtgPagoResumen;
        private System.Windows.Forms.Button button14;
        private System.Windows.Forms.TabPage tabPage10;
        private System.Windows.Forms.TextBox pagoLog;
        private System.Windows.Forms.Button button15;
        private System.Windows.Forms.TabPage tabPage11;
        private System.Windows.Forms.DataGridView dtgPagoPendiente;
        private System.Windows.Forms.Button button16;
        private System.Windows.Forms.TabPage tabPage12;
        private System.Windows.Forms.DataGridView dtgRegistradoPendiente;
        private System.Windows.Forms.TextBox pagosTotalPendientes;
        private System.Windows.Forms.TabPage tabPage13;
        private System.Windows.Forms.DataGridView dtgEstadoNuevo;
        private System.Windows.Forms.TabPage tabPage14;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Label label31;
        private System.Windows.Forms.DateTimePicker dateTimePicker1;
        private System.Windows.Forms.DateTimePicker dateTimePicker2;
        private System.Windows.Forms.Label label32;
        private System.Windows.Forms.TextBox ReciboTxt;
        private System.Windows.Forms.Label label33;
        private System.Windows.Forms.Label label36;
        private System.Windows.Forms.Label label37;
        private System.Windows.Forms.Button button17;
        private System.Windows.Forms.Button button18;
        private System.Windows.Forms.DataGridView dtgRecibo;
        private System.Windows.Forms.TextBox informacionRecibo;
        private System.Windows.Forms.TextBox textBox4;
    }
}